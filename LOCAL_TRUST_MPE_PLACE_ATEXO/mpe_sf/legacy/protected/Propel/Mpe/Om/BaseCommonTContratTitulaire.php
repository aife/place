<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcession;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionQuery;
use Application\Propel\Mpe\CommonEchangeDoc;
use Application\Propel\Mpe\CommonEchangeDocQuery;
use Application\Propel\Mpe\CommonMarche;
use Application\Propel\Mpe\CommonMarcheQuery;
use Application\Propel\Mpe\CommonModificationContrat;
use Application\Propel\Mpe\CommonModificationContratQuery;
use Application\Propel\Mpe\CommonTConsLotContrat;
use Application\Propel\Mpe\CommonTConsLotContratQuery;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTContactContratQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulairePeer;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTDonneesConsultation;
use Application\Propel\Mpe\CommonTDonneesConsultationQuery;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Propel\Mpe\CommonTypeContratConcessionPivot;
use Application\Propel\Mpe\CommonTypeContratConcessionPivotQuery;
use Application\Propel\Mpe\CommonTypeContratPivot;
use Application\Propel\Mpe\CommonTypeContratPivotQuery;
use Application\Propel\Mpe\CommonTypeProcedure;
use Application\Propel\Mpe\CommonTypeProcedureConcessionPivot;
use Application\Propel\Mpe\CommonTypeProcedureConcessionPivotQuery;
use Application\Propel\Mpe\CommonTypeProcedureQuery;

/**
 * Base class that represents a row from the 't_contrat_titulaire' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTContratTitulaire extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTContratTitulairePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTContratTitulairePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_contrat_titulaire field.
     * @var        int
     */
    protected $id_contrat_titulaire;

    /**
     * The value for the id_type_contrat field.
     * @var        int
     */
    protected $id_type_contrat;

    /**
     * The value for the id_contact_contrat field.
     * @var        int
     */
    protected $id_contact_contrat;

    /**
     * The value for the numero_contrat field.
     * @var        string
     */
    protected $numero_contrat;

    /**
     * The value for the lien_ac_sad field.
     * @var        int
     */
    protected $lien_ac_sad;

    /**
     * The value for the id_contrat_multi field.
     * @var        int
     */
    protected $id_contrat_multi;

    /**
     * The value for the organisme field.
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the old_service_id field.
     * @var        int
     */
    protected $old_service_id;

    /**
     * The value for the id_titulaire field.
     * @var        int
     */
    protected $id_titulaire;

    /**
     * The value for the id_titulaire_etab field.
     * @var        int
     */
    protected $id_titulaire_etab;

    /**
     * The value for the id_offre field.
     * @var        int
     */
    protected $id_offre;

    /**
     * The value for the type_depot_reponse field.
     * @var        string
     */
    protected $type_depot_reponse;

    /**
     * The value for the objet_contrat field.
     * @var        string
     */
    protected $objet_contrat;

    /**
     * The value for the intitule field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $intitule;

    /**
     * The value for the montant_contrat field.
     * @var        double
     */
    protected $montant_contrat;

    /**
     * The value for the id_tranche_budgetaire field.
     * @var        int
     */
    protected $id_tranche_budgetaire;

    /**
     * The value for the montant_max_estime field.
     * @var        double
     */
    protected $montant_max_estime;

    /**
     * The value for the publication_montant field.
     * @var        int
     */
    protected $publication_montant;

    /**
     * The value for the id_motif_non_publication_montant field.
     * @var        int
     */
    protected $id_motif_non_publication_montant;

    /**
     * The value for the desc_motif_non_publication_montant field.
     * @var        string
     */
    protected $desc_motif_non_publication_montant;

    /**
     * The value for the publication_contrat field.
     * @var        int
     */
    protected $publication_contrat;

    /**
     * The value for the num_ej field.
     * @var        string
     */
    protected $num_ej;

    /**
     * The value for the statutej field.
     * @var        string
     */
    protected $statutej;

    /**
     * The value for the num_long_oeap field.
     * @var        string
     */
    protected $num_long_oeap;

    /**
     * The value for the reference_libre field.
     * @var        string
     */
    protected $reference_libre;

    /**
     * The value for the statut_contrat field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $statut_contrat;

    /**
     * The value for the categorie field.
     * @var        int
     */
    protected $categorie;

    /**
     * The value for the ccag_applicable field.
     * @var        int
     */
    protected $ccag_applicable;

    /**
     * The value for the clause_sociale field.
     * @var        string
     */
    protected $clause_sociale;

    /**
     * The value for the clause_sociale_condition_execution field.
     * @var        string
     */
    protected $clause_sociale_condition_execution;

    /**
     * The value for the clause_sociale_insertion field.
     * @var        string
     */
    protected $clause_sociale_insertion;

    /**
     * The value for the clause_sociale_ateliers_proteges field.
     * @var        string
     */
    protected $clause_sociale_ateliers_proteges;

    /**
     * The value for the clause_sociale_siae field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $clause_sociale_siae;

    /**
     * The value for the clause_sociale_ess field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $clause_sociale_ess;

    /**
     * The value for the clause_environnementale field.
     * @var        string
     */
    protected $clause_environnementale;

    /**
     * The value for the clause_env_specs_techniques field.
     * @var        string
     */
    protected $clause_env_specs_techniques;

    /**
     * The value for the clause_env_cond_execution field.
     * @var        string
     */
    protected $clause_env_cond_execution;

    /**
     * The value for the clause_env_criteres_select field.
     * @var        string
     */
    protected $clause_env_criteres_select;

    /**
     * The value for the date_prevue_notification field.
     * @var        string
     */
    protected $date_prevue_notification;

    /**
     * The value for the date_prevue_fin_contrat field.
     * @var        string
     */
    protected $date_prevue_fin_contrat;

    /**
     * The value for the date_prevue_max_fin_contrat field.
     * @var        string
     */
    protected $date_prevue_max_fin_contrat;

    /**
     * The value for the date_notification field.
     * @var        string
     */
    protected $date_notification;

    /**
     * The value for the date_fin_contrat field.
     * @var        string
     */
    protected $date_fin_contrat;

    /**
     * The value for the date_max_fin_contrat field.
     * @var        string
     */
    protected $date_max_fin_contrat;

    /**
     * The value for the date_attribution field.
     * @var        string
     */
    protected $date_attribution;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_modification field.
     * @var        string
     */
    protected $date_modification;

    /**
     * The value for the envoi_interface field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $envoi_interface;

    /**
     * The value for the contrat_class_key field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $contrat_class_key;

    /**
     * The value for the pme_pmi field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $pme_pmi;

    /**
     * The value for the reference_consultation field.
     * @var        string
     */
    protected $reference_consultation;

    /**
     * The value for the hors_passation field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $hors_passation;

    /**
     * The value for the id_agent field.
     * @var        int
     */
    protected $id_agent;

    /**
     * The value for the nom_agent field.
     * @var        string
     */
    protected $nom_agent;

    /**
     * The value for the prenom_agent field.
     * @var        string
     */
    protected $prenom_agent;

    /**
     * The value for the lieu_execution field.
     * @var        string
     */
    protected $lieu_execution;

    /**
     * The value for the code_cpv_1 field.
     * @var        string
     */
    protected $code_cpv_1;

    /**
     * The value for the code_cpv_2 field.
     * @var        string
     */
    protected $code_cpv_2;

    /**
     * The value for the marche_insertion field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $marche_insertion;

    /**
     * The value for the clause_specification_technique field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $clause_specification_technique;

    /**
     * The value for the procedure_passation_pivot field.
     * @var        string
     */
    protected $procedure_passation_pivot;

    /**
     * The value for the nom_lieu_principal_execution field.
     * @var        string
     */
    protected $nom_lieu_principal_execution;

    /**
     * The value for the code_lieu_principal_execution field.
     * @var        string
     */
    protected $code_lieu_principal_execution;

    /**
     * The value for the type_code_lieu_principal_execution field.
     * @var        string
     */
    protected $type_code_lieu_principal_execution;

    /**
     * The value for the duree_initiale_contrat field.
     * @var        int
     */
    protected $duree_initiale_contrat;

    /**
     * The value for the forme_prix field.
     * @var        string
     */
    protected $forme_prix;

    /**
     * The value for the date_publication_initiale_de field.
     * Note: this column has a database default value of: NULL
     * @var        string
     */
    protected $date_publication_initiale_de;

    /**
     * The value for the num_id_unique_marche_public field.
     * @var        string
     */
    protected $num_id_unique_marche_public;

    /**
     * The value for the libelle_type_contrat_pivot field.
     * @var        string
     */
    protected $libelle_type_contrat_pivot;

    /**
     * The value for the siret_pa_accord_cadre field.
     * @var        string
     */
    protected $siret_pa_accord_cadre;

    /**
     * The value for the ac_marche_subsequent field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $ac_marche_subsequent;

    /**
     * The value for the libelle_type_procedure_mpe field.
     * @var        string
     */
    protected $libelle_type_procedure_mpe;

    /**
     * The value for the nb_total_propositions_lot field.
     * @var        int
     */
    protected $nb_total_propositions_lot;

    /**
     * The value for the nb_total_propositions_demat_lot field.
     * @var        int
     */
    protected $nb_total_propositions_demat_lot;

    /**
     * The value for the marche_defense field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $marche_defense;

    /**
     * The value for the siret field.
     * @var        string
     */
    protected $siret;

    /**
     * The value for the nom_entite_acheteur field.
     * @var        string
     */
    protected $nom_entite_acheteur;

    /**
     * The value for the statut_publication_sn field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $statut_publication_sn;

    /**
     * The value for the date_publication_sn field.
     * @var        string
     */
    protected $date_publication_sn;

    /**
     * The value for the erreur_sn field.
     * @var        string
     */
    protected $erreur_sn;

    /**
     * The value for the date_modification_sn field.
     * @var        string
     */
    protected $date_modification_sn;

    /**
     * The value for the id_type_procedure_pivot field.
     * @var        int
     */
    protected $id_type_procedure_pivot;

    /**
     * The value for the id_type_procedure_concession_pivot field.
     * @var        int
     */
    protected $id_type_procedure_concession_pivot;

    /**
     * The value for the id_type_contrat_pivot field.
     * @var        int
     */
    protected $id_type_contrat_pivot;

    /**
     * The value for the id_type_contrat_concession_pivot field.
     * @var        int
     */
    protected $id_type_contrat_concession_pivot;

    /**
     * The value for the montant_subvention_publique field.
     * @var        double
     */
    protected $montant_subvention_publique;

    /**
     * The value for the date_debut_execution field.
     * @var        string
     */
    protected $date_debut_execution;

    /**
     * The value for the uuid field.
     * @var        string
     */
    protected $uuid;

    /**
     * The value for the marche_innovant field.
     * @var        boolean
     */
    protected $marche_innovant;

    /**
     * The value for the service_id field.
     * @var        string
     */
    protected $service_id;

    /**
     * @var        CommonTypeContratConcessionPivot
     */
    protected $aCommonTypeContratConcessionPivot;

    /**
     * @var        CommonTypeContratPivot
     */
    protected $aCommonTypeContratPivot;

    /**
     * @var        CommonTypeProcedureConcessionPivot
     */
    protected $aCommonTypeProcedureConcessionPivot;

    /**
     * @var        CommonTContactContrat
     */
    protected $aCommonTContactContrat;

    /**
     * @var        CommonTContratTitulaire
     */
    protected $aCommonTContratTitulaireRelatedByIdContratMulti;

    /**
     * @var        CommonTTypeContrat
     */
    protected $aCommonTTypeContrat;

    /**
     * @var        CommonTypeProcedure
     */
    protected $aCommonTypeProcedure;

    /**
     * @var        PropelObjectCollection|CommonMarche[] Collection to store aggregation of CommonMarche objects.
     */
    protected $collCommonMarches;
    protected $collCommonMarchesPartial;

    /**
     * @var        PropelObjectCollection|CommonDonneesAnnuellesConcession[] Collection to store aggregation of CommonDonneesAnnuellesConcession objects.
     */
    protected $collCommonDonneesAnnuellesConcessions;
    protected $collCommonDonneesAnnuellesConcessionsPartial;

    /**
     * @var        PropelObjectCollection|CommonEchangeDoc[] Collection to store aggregation of CommonEchangeDoc objects.
     */
    protected $collCommonEchangeDocs;
    protected $collCommonEchangeDocsPartial;

    /**
     * @var        PropelObjectCollection|CommonModificationContrat[] Collection to store aggregation of CommonModificationContrat objects.
     */
    protected $collCommonModificationContrats;
    protected $collCommonModificationContratsPartial;

    /**
     * @var        PropelObjectCollection|CommonTConsLotContrat[] Collection to store aggregation of CommonTConsLotContrat objects.
     */
    protected $collCommonTConsLotContrats;
    protected $collCommonTConsLotContratsPartial;

    /**
     * @var        PropelObjectCollection|CommonTContratTitulaire[] Collection to store aggregation of CommonTContratTitulaire objects.
     */
    protected $collCommonTContratTitulairesRelatedByIdContratTitulaire;
    protected $collCommonTContratTitulairesRelatedByIdContratTitulairePartial;

    /**
     * @var        PropelObjectCollection|CommonTDonneesConsultation[] Collection to store aggregation of CommonTDonneesConsultation objects.
     */
    protected $collCommonTDonneesConsultations;
    protected $collCommonTDonneesConsultationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonMarchesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonDonneesAnnuellesConcessionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangeDocsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonModificationContratsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTConsLotContratsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTContratTitulairesRelatedByIdContratTitulaireScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTDonneesConsultationsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->intitule = '';
        $this->statut_contrat = 0;
        $this->clause_sociale_siae = '0';
        $this->clause_sociale_ess = '0';
        $this->envoi_interface = '0';
        $this->contrat_class_key = 1;
        $this->pme_pmi = 0;
        $this->hors_passation = 0;
        $this->marche_insertion = false;
        $this->clause_specification_technique = '0';
        $this->date_publication_initiale_de = NULL;
        $this->ac_marche_subsequent = false;
        $this->marche_defense = '0';
        $this->statut_publication_sn = 0;
    }

    /**
     * Initializes internal state of BaseCommonTContratTitulaire object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_contrat_titulaire] column value.
     *
     * @return int
     */
    public function getIdContratTitulaire()
    {

        return $this->id_contrat_titulaire;
    }

    /**
     * Get the [id_type_contrat] column value.
     *
     * @return int
     */
    public function getIdTypeContrat()
    {

        return $this->id_type_contrat;
    }

    /**
     * Get the [id_contact_contrat] column value.
     *
     * @return int
     */
    public function getIdContactContrat()
    {

        return $this->id_contact_contrat;
    }

    /**
     * Get the [numero_contrat] column value.
     *
     * @return string
     */
    public function getNumeroContrat()
    {

        return $this->numero_contrat;
    }

    /**
     * Get the [lien_ac_sad] column value.
     *
     * @return int
     */
    public function getLienAcSad()
    {

        return $this->lien_ac_sad;
    }

    /**
     * Get the [id_contrat_multi] column value.
     *
     * @return int
     */
    public function getIdContratMulti()
    {

        return $this->id_contrat_multi;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [old_service_id] column value.
     *
     * @return int
     */
    public function getOldServiceId()
    {

        return $this->old_service_id;
    }

    /**
     * Get the [id_titulaire] column value.
     *
     * @return int
     */
    public function getIdTitulaire()
    {

        return $this->id_titulaire;
    }

    /**
     * Get the [id_titulaire_etab] column value.
     *
     * @return int
     */
    public function getIdTitulaireEtab()
    {

        return $this->id_titulaire_etab;
    }

    /**
     * Get the [id_offre] column value.
     *
     * @return int
     */
    public function getIdOffre()
    {

        return $this->id_offre;
    }

    /**
     * Get the [type_depot_reponse] column value.
     *
     * @return string
     */
    public function getTypeDepotReponse()
    {

        return $this->type_depot_reponse;
    }

    /**
     * Get the [objet_contrat] column value.
     *
     * @return string
     */
    public function getObjetContrat()
    {

        return $this->objet_contrat;
    }

    /**
     * Get the [intitule] column value.
     *
     * @return string
     */
    public function getIntitule()
    {

        return $this->intitule;
    }

    /**
     * Get the [montant_contrat] column value.
     *
     * @return double
     */
    public function getMontantContrat()
    {

        return $this->montant_contrat;
    }

    /**
     * Get the [id_tranche_budgetaire] column value.
     *
     * @return int
     */
    public function getIdTrancheBudgetaire()
    {

        return $this->id_tranche_budgetaire;
    }

    /**
     * Get the [montant_max_estime] column value.
     *
     * @return double
     */
    public function getMontantMaxEstime()
    {

        return $this->montant_max_estime;
    }

    /**
     * Get the [publication_montant] column value.
     *
     * @return int
     */
    public function getPublicationMontant()
    {

        return $this->publication_montant;
    }

    /**
     * Get the [id_motif_non_publication_montant] column value.
     *
     * @return int
     */
    public function getIdMotifNonPublicationMontant()
    {

        return $this->id_motif_non_publication_montant;
    }

    /**
     * Get the [desc_motif_non_publication_montant] column value.
     *
     * @return string
     */
    public function getDescMotifNonPublicationMontant()
    {

        return $this->desc_motif_non_publication_montant;
    }

    /**
     * Get the [publication_contrat] column value.
     *
     * @return int
     */
    public function getPublicationContrat()
    {

        return $this->publication_contrat;
    }

    /**
     * Get the [num_ej] column value.
     *
     * @return string
     */
    public function getNumEj()
    {

        return $this->num_ej;
    }

    /**
     * Get the [statutej] column value.
     *
     * @return string
     */
    public function getStatutej()
    {

        return $this->statutej;
    }

    /**
     * Get the [num_long_oeap] column value.
     *
     * @return string
     */
    public function getNumLongOeap()
    {

        return $this->num_long_oeap;
    }

    /**
     * Get the [reference_libre] column value.
     *
     * @return string
     */
    public function getReferenceLibre()
    {

        return $this->reference_libre;
    }

    /**
     * Get the [statut_contrat] column value.
     *
     * @return int
     */
    public function getStatutContrat()
    {

        return $this->statut_contrat;
    }

    /**
     * Get the [categorie] column value.
     *
     * @return int
     */
    public function getCategorie()
    {

        return $this->categorie;
    }

    /**
     * Get the [ccag_applicable] column value.
     *
     * @return int
     */
    public function getCcagApplicable()
    {

        return $this->ccag_applicable;
    }

    /**
     * Get the [clause_sociale] column value.
     *
     * @return string
     */
    public function getClauseSociale()
    {

        return $this->clause_sociale;
    }

    /**
     * Get the [clause_sociale_condition_execution] column value.
     *
     * @return string
     */
    public function getClauseSocialeConditionExecution()
    {

        return $this->clause_sociale_condition_execution;
    }

    /**
     * Get the [clause_sociale_insertion] column value.
     *
     * @return string
     */
    public function getClauseSocialeInsertion()
    {

        return $this->clause_sociale_insertion;
    }

    /**
     * Get the [clause_sociale_ateliers_proteges] column value.
     *
     * @return string
     */
    public function getClauseSocialeAteliersProteges()
    {

        return $this->clause_sociale_ateliers_proteges;
    }

    /**
     * Get the [clause_sociale_siae] column value.
     *
     * @return string
     */
    public function getClauseSocialeSiae()
    {

        return $this->clause_sociale_siae;
    }

    /**
     * Get the [clause_sociale_ess] column value.
     *
     * @return string
     */
    public function getClauseSocialeEss()
    {

        return $this->clause_sociale_ess;
    }

    /**
     * Get the [clause_environnementale] column value.
     *
     * @return string
     */
    public function getClauseEnvironnementale()
    {

        return $this->clause_environnementale;
    }

    /**
     * Get the [clause_env_specs_techniques] column value.
     *
     * @return string
     */
    public function getClauseEnvSpecsTechniques()
    {

        return $this->clause_env_specs_techniques;
    }

    /**
     * Get the [clause_env_cond_execution] column value.
     *
     * @return string
     */
    public function getClauseEnvCondExecution()
    {

        return $this->clause_env_cond_execution;
    }

    /**
     * Get the [clause_env_criteres_select] column value.
     *
     * @return string
     */
    public function getClauseEnvCriteresSelect()
    {

        return $this->clause_env_criteres_select;
    }

    /**
     * Get the [optionally formatted] temporal [date_prevue_notification] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatePrevueNotification($format = '%d/%m/%Y')
    {
        if ($this->date_prevue_notification === null) {
            return null;
        }

        if ($this->date_prevue_notification === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_prevue_notification);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_prevue_notification, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_prevue_fin_contrat] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatePrevueFinContrat($format = '%d/%m/%Y')
    {
        if ($this->date_prevue_fin_contrat === null) {
            return null;
        }

        if ($this->date_prevue_fin_contrat === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_prevue_fin_contrat);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_prevue_fin_contrat, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_prevue_max_fin_contrat] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatePrevueMaxFinContrat($format = '%d/%m/%Y')
    {
        if ($this->date_prevue_max_fin_contrat === null) {
            return null;
        }

        if ($this->date_prevue_max_fin_contrat === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_prevue_max_fin_contrat);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_prevue_max_fin_contrat, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_notification] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateNotification($format = '%d/%m/%Y')
    {
        if ($this->date_notification === null) {
            return null;
        }

        if ($this->date_notification === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_notification);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_notification, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_fin_contrat] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateFinContrat($format = '%d/%m/%Y')
    {
        if ($this->date_fin_contrat === null) {
            return null;
        }

        if ($this->date_fin_contrat === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_fin_contrat);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_fin_contrat, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_max_fin_contrat] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateMaxFinContrat($format = '%d/%m/%Y')
    {
        if ($this->date_max_fin_contrat === null) {
            return null;
        }

        if ($this->date_max_fin_contrat === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_max_fin_contrat);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_max_fin_contrat, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_attribution] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateAttribution($format = '%d/%m/%Y')
    {
        if ($this->date_attribution === null) {
            return null;
        }

        if ($this->date_attribution === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_attribution);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_attribution, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_modification] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateModification($format = 'Y-m-d H:i:s')
    {
        if ($this->date_modification === null) {
            return null;
        }

        if ($this->date_modification === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_modification);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_modification, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [envoi_interface] column value.
     *
     * @return string
     */
    public function getEnvoiInterface()
    {

        return $this->envoi_interface;
    }

    /**
     * Get the [contrat_class_key] column value.
     *
     * @return int
     */
    public function getContratClassKey()
    {

        return $this->contrat_class_key;
    }

    /**
     * Get the [pme_pmi] column value.
     *
     * @return int
     */
    public function getPmePmi()
    {

        return $this->pme_pmi;
    }

    /**
     * Get the [reference_consultation] column value.
     *
     * @return string
     */
    public function getReferenceConsultation()
    {

        return $this->reference_consultation;
    }

    /**
     * Get the [hors_passation] column value.
     *
     * @return int
     */
    public function getHorsPassation()
    {

        return $this->hors_passation;
    }

    /**
     * Get the [id_agent] column value.
     *
     * @return int
     */
    public function getIdAgent()
    {

        return $this->id_agent;
    }

    /**
     * Get the [nom_agent] column value.
     *
     * @return string
     */
    public function getNomAgent()
    {

        return $this->nom_agent;
    }

    /**
     * Get the [prenom_agent] column value.
     *
     * @return string
     */
    public function getPrenomAgent()
    {

        return $this->prenom_agent;
    }

    /**
     * Get the [lieu_execution] column value.
     *
     * @return string
     */
    public function getLieuExecution()
    {

        return $this->lieu_execution;
    }

    /**
     * Get the [code_cpv_1] column value.
     *
     * @return string
     */
    public function getCodeCpv1()
    {

        return $this->code_cpv_1;
    }

    /**
     * Get the [code_cpv_2] column value.
     *
     * @return string
     */
    public function getCodeCpv2()
    {

        return $this->code_cpv_2;
    }

    /**
     * Get the [marche_insertion] column value.
     *
     * @return boolean
     */
    public function getMarcheInsertion()
    {

        return $this->marche_insertion;
    }

    /**
     * Get the [clause_specification_technique] column value.
     *
     * @return string
     */
    public function getClauseSpecificationTechnique()
    {

        return $this->clause_specification_technique;
    }

    /**
     * Get the [procedure_passation_pivot] column value.
     *
     * @return string
     */
    public function getProcedurePassationPivot()
    {

        return $this->procedure_passation_pivot;
    }

    /**
     * Get the [nom_lieu_principal_execution] column value.
     *
     * @return string
     */
    public function getNomLieuPrincipalExecution()
    {

        return $this->nom_lieu_principal_execution;
    }

    /**
     * Get the [code_lieu_principal_execution] column value.
     *
     * @return string
     */
    public function getCodeLieuPrincipalExecution()
    {

        return $this->code_lieu_principal_execution;
    }

    /**
     * Get the [type_code_lieu_principal_execution] column value.
     *
     * @return string
     */
    public function getTypeCodeLieuPrincipalExecution()
    {

        return $this->type_code_lieu_principal_execution;
    }

    /**
     * Get the [duree_initiale_contrat] column value.
     *
     * @return int
     */
    public function getDureeInitialeContrat()
    {

        return $this->duree_initiale_contrat;
    }

    /**
     * Get the [forme_prix] column value.
     *
     * @return string
     */
    public function getFormePrix()
    {

        return $this->forme_prix;
    }

    /**
     * Get the [optionally formatted] temporal [date_publication_initiale_de] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatePublicationInitialeDe($format = 'Y-m-d H:i:s')
    {
        if ($this->date_publication_initiale_de === null) {
            return null;
        }

        if ($this->date_publication_initiale_de === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_publication_initiale_de);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_publication_initiale_de, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [num_id_unique_marche_public] column value.
     *
     * @return string
     */
    public function getNumIdUniqueMarchePublic()
    {

        return $this->num_id_unique_marche_public;
    }

    /**
     * Get the [libelle_type_contrat_pivot] column value.
     *
     * @return string
     */
    public function getLibelleTypeContratPivot()
    {

        return $this->libelle_type_contrat_pivot;
    }

    /**
     * Get the [siret_pa_accord_cadre] column value.
     *
     * @return string
     */
    public function getSiretPaAccordCadre()
    {

        return $this->siret_pa_accord_cadre;
    }

    /**
     * Get the [ac_marche_subsequent] column value.
     *
     * @return boolean
     */
    public function getAcMarcheSubsequent()
    {

        return $this->ac_marche_subsequent;
    }

    /**
     * Get the [libelle_type_procedure_mpe] column value.
     *
     * @return string
     */
    public function getLibelleTypeProcedureMpe()
    {

        return $this->libelle_type_procedure_mpe;
    }

    /**
     * Get the [nb_total_propositions_lot] column value.
     *
     * @return int
     */
    public function getNbTotalPropositionsLot()
    {

        return $this->nb_total_propositions_lot;
    }

    /**
     * Get the [nb_total_propositions_demat_lot] column value.
     *
     * @return int
     */
    public function getNbTotalPropositionsDematLot()
    {

        return $this->nb_total_propositions_demat_lot;
    }

    /**
     * Get the [marche_defense] column value.
     *
     * @return string
     */
    public function getMarcheDefense()
    {

        return $this->marche_defense;
    }

    /**
     * Get the [siret] column value.
     *
     * @return string
     */
    public function getSiret()
    {

        return $this->siret;
    }

    /**
     * Get the [nom_entite_acheteur] column value.
     *
     * @return string
     */
    public function getNomEntiteAcheteur()
    {

        return $this->nom_entite_acheteur;
    }

    /**
     * Get the [statut_publication_sn] column value.
     *
     * @return int
     */
    public function getStatutPublicationSn()
    {

        return $this->statut_publication_sn;
    }

    /**
     * Get the [optionally formatted] temporal [date_publication_sn] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatePublicationSn($format = 'Y-m-d H:i:s')
    {
        if ($this->date_publication_sn === null) {
            return null;
        }

        if ($this->date_publication_sn === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_publication_sn);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_publication_sn, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [erreur_sn] column value.
     *
     * @return string
     */
    public function getErreurSn()
    {

        return $this->erreur_sn;
    }

    /**
     * Get the [optionally formatted] temporal [date_modification_sn] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateModificationSn($format = 'Y-m-d H:i:s')
    {
        if ($this->date_modification_sn === null) {
            return null;
        }

        if ($this->date_modification_sn === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_modification_sn);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_modification_sn, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [id_type_procedure_pivot] column value.
     *
     * @return int
     */
    public function getIdTypeProcedurePivot()
    {

        return $this->id_type_procedure_pivot;
    }

    /**
     * Get the [id_type_procedure_concession_pivot] column value.
     *
     * @return int
     */
    public function getIdTypeProcedureConcessionPivot()
    {

        return $this->id_type_procedure_concession_pivot;
    }

    /**
     * Get the [id_type_contrat_pivot] column value.
     *
     * @return int
     */
    public function getIdTypeContratPivot()
    {

        return $this->id_type_contrat_pivot;
    }

    /**
     * Get the [id_type_contrat_concession_pivot] column value.
     *
     * @return int
     */
    public function getIdTypeContratConcessionPivot()
    {

        return $this->id_type_contrat_concession_pivot;
    }

    /**
     * Get the [montant_subvention_publique] column value.
     *
     * @return double
     */
    public function getMontantSubventionPublique()
    {

        return $this->montant_subvention_publique;
    }

    /**
     * Get the [optionally formatted] temporal [date_debut_execution] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateDebutExecution($format = '%d/%m/%Y')
    {
        if ($this->date_debut_execution === null) {
            return null;
        }

        if ($this->date_debut_execution === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_debut_execution);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_debut_execution, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [uuid] column value.
     *
     * @return string
     */
    public function getUuid()
    {

        return $this->uuid;
    }

    /**
     * Get the [marche_innovant] column value.
     *
     * @return boolean
     */
    public function getMarcheInnovant()
    {

        return $this->marche_innovant;
    }

    /**
     * Get the [service_id] column value.
     *
     * @return string
     */
    public function getServiceId()
    {

        return $this->service_id;
    }

    /**
     * Set the value of [id_contrat_titulaire] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdContratTitulaire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_contrat_titulaire !== $v) {
            $this->id_contrat_titulaire = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE;
        }


        return $this;
    } // setIdContratTitulaire()

    /**
     * Set the value of [id_type_contrat] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdTypeContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_contrat !== $v) {
            $this->id_type_contrat = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_TYPE_CONTRAT;
        }

        if ($this->aCommonTTypeContrat !== null && $this->aCommonTTypeContrat->getIdTypeContrat() !== $v) {
            $this->aCommonTTypeContrat = null;
        }


        return $this;
    } // setIdTypeContrat()

    /**
     * Set the value of [id_contact_contrat] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdContactContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_contact_contrat !== $v) {
            $this->id_contact_contrat = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_CONTACT_CONTRAT;
        }

        if ($this->aCommonTContactContrat !== null && $this->aCommonTContactContrat->getIdContactContrat() !== $v) {
            $this->aCommonTContactContrat = null;
        }


        return $this;
    } // setIdContactContrat()

    /**
     * Set the value of [numero_contrat] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setNumeroContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->numero_contrat !== $v) {
            $this->numero_contrat = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::NUMERO_CONTRAT;
        }


        return $this;
    } // setNumeroContrat()

    /**
     * Set the value of [lien_ac_sad] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setLienAcSad($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->lien_ac_sad !== $v) {
            $this->lien_ac_sad = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::LIEN_AC_SAD;
        }


        return $this;
    } // setLienAcSad()

    /**
     * Set the value of [id_contrat_multi] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdContratMulti($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_contrat_multi !== $v) {
            $this->id_contrat_multi = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_CONTRAT_MULTI;
        }

        if ($this->aCommonTContratTitulaireRelatedByIdContratMulti !== null && $this->aCommonTContratTitulaireRelatedByIdContratMulti->getIdContratTitulaire() !== $v) {
            $this->aCommonTContratTitulaireRelatedByIdContratMulti = null;
        }


        return $this;
    } // setIdContratMulti()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [old_service_id] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setOldServiceId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_service_id !== $v) {
            $this->old_service_id = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::OLD_SERVICE_ID;
        }


        return $this;
    } // setOldServiceId()

    /**
     * Set the value of [id_titulaire] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdTitulaire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_titulaire !== $v) {
            $this->id_titulaire = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_TITULAIRE;
        }


        return $this;
    } // setIdTitulaire()

    /**
     * Set the value of [id_titulaire_etab] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdTitulaireEtab($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_titulaire_etab !== $v) {
            $this->id_titulaire_etab = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_TITULAIRE_ETAB;
        }


        return $this;
    } // setIdTitulaireEtab()

    /**
     * Set the value of [id_offre] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdOffre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_offre !== $v) {
            $this->id_offre = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_OFFRE;
        }


        return $this;
    } // setIdOffre()

    /**
     * Set the value of [type_depot_reponse] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setTypeDepotReponse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_depot_reponse !== $v) {
            $this->type_depot_reponse = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::TYPE_DEPOT_REPONSE;
        }


        return $this;
    } // setTypeDepotReponse()

    /**
     * Set the value of [objet_contrat] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setObjetContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->objet_contrat !== $v) {
            $this->objet_contrat = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::OBJET_CONTRAT;
        }


        return $this;
    } // setObjetContrat()

    /**
     * Set the value of [intitule] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIntitule($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->intitule !== $v) {
            $this->intitule = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::INTITULE;
        }


        return $this;
    } // setIntitule()

    /**
     * Set the value of [montant_contrat] column.
     *
     * @param double $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setMontantContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->montant_contrat !== $v) {
            $this->montant_contrat = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::MONTANT_CONTRAT;
        }


        return $this;
    } // setMontantContrat()

    /**
     * Set the value of [id_tranche_budgetaire] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdTrancheBudgetaire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_tranche_budgetaire !== $v) {
            $this->id_tranche_budgetaire = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_TRANCHE_BUDGETAIRE;
        }


        return $this;
    } // setIdTrancheBudgetaire()

    /**
     * Set the value of [montant_max_estime] column.
     *
     * @param double $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setMontantMaxEstime($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->montant_max_estime !== $v) {
            $this->montant_max_estime = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::MONTANT_MAX_ESTIME;
        }


        return $this;
    } // setMontantMaxEstime()

    /**
     * Set the value of [publication_montant] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setPublicationMontant($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->publication_montant !== $v) {
            $this->publication_montant = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::PUBLICATION_MONTANT;
        }


        return $this;
    } // setPublicationMontant()

    /**
     * Set the value of [id_motif_non_publication_montant] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdMotifNonPublicationMontant($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_motif_non_publication_montant !== $v) {
            $this->id_motif_non_publication_montant = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_MOTIF_NON_PUBLICATION_MONTANT;
        }


        return $this;
    } // setIdMotifNonPublicationMontant()

    /**
     * Set the value of [desc_motif_non_publication_montant] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDescMotifNonPublicationMontant($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->desc_motif_non_publication_montant !== $v) {
            $this->desc_motif_non_publication_montant = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::DESC_MOTIF_NON_PUBLICATION_MONTANT;
        }


        return $this;
    } // setDescMotifNonPublicationMontant()

    /**
     * Set the value of [publication_contrat] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setPublicationContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->publication_contrat !== $v) {
            $this->publication_contrat = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::PUBLICATION_CONTRAT;
        }


        return $this;
    } // setPublicationContrat()

    /**
     * Set the value of [num_ej] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setNumEj($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->num_ej !== $v) {
            $this->num_ej = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::NUM_EJ;
        }


        return $this;
    } // setNumEj()

    /**
     * Set the value of [statutej] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setStatutej($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->statutej !== $v) {
            $this->statutej = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::STATUTEJ;
        }


        return $this;
    } // setStatutej()

    /**
     * Set the value of [num_long_oeap] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setNumLongOeap($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->num_long_oeap !== $v) {
            $this->num_long_oeap = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::NUM_LONG_OEAP;
        }


        return $this;
    } // setNumLongOeap()

    /**
     * Set the value of [reference_libre] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setReferenceLibre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->reference_libre !== $v) {
            $this->reference_libre = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::REFERENCE_LIBRE;
        }


        return $this;
    } // setReferenceLibre()

    /**
     * Set the value of [statut_contrat] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setStatutContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->statut_contrat !== $v) {
            $this->statut_contrat = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::STATUT_CONTRAT;
        }


        return $this;
    } // setStatutContrat()

    /**
     * Set the value of [categorie] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCategorie($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->categorie !== $v) {
            $this->categorie = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CATEGORIE;
        }


        return $this;
    } // setCategorie()

    /**
     * Set the value of [ccag_applicable] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCcagApplicable($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->ccag_applicable !== $v) {
            $this->ccag_applicable = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CCAG_APPLICABLE;
        }


        return $this;
    } // setCcagApplicable()

    /**
     * Set the value of [clause_sociale] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setClauseSociale($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_sociale !== $v) {
            $this->clause_sociale = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CLAUSE_SOCIALE;
        }


        return $this;
    } // setClauseSociale()

    /**
     * Set the value of [clause_sociale_condition_execution] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setClauseSocialeConditionExecution($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_sociale_condition_execution !== $v) {
            $this->clause_sociale_condition_execution = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CLAUSE_SOCIALE_CONDITION_EXECUTION;
        }


        return $this;
    } // setClauseSocialeConditionExecution()

    /**
     * Set the value of [clause_sociale_insertion] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setClauseSocialeInsertion($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_sociale_insertion !== $v) {
            $this->clause_sociale_insertion = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CLAUSE_SOCIALE_INSERTION;
        }


        return $this;
    } // setClauseSocialeInsertion()

    /**
     * Set the value of [clause_sociale_ateliers_proteges] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setClauseSocialeAteliersProteges($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_sociale_ateliers_proteges !== $v) {
            $this->clause_sociale_ateliers_proteges = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CLAUSE_SOCIALE_ATELIERS_PROTEGES;
        }


        return $this;
    } // setClauseSocialeAteliersProteges()

    /**
     * Set the value of [clause_sociale_siae] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setClauseSocialeSiae($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_sociale_siae !== $v) {
            $this->clause_sociale_siae = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CLAUSE_SOCIALE_SIAE;
        }


        return $this;
    } // setClauseSocialeSiae()

    /**
     * Set the value of [clause_sociale_ess] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setClauseSocialeEss($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_sociale_ess !== $v) {
            $this->clause_sociale_ess = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CLAUSE_SOCIALE_ESS;
        }


        return $this;
    } // setClauseSocialeEss()

    /**
     * Set the value of [clause_environnementale] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setClauseEnvironnementale($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_environnementale !== $v) {
            $this->clause_environnementale = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CLAUSE_ENVIRONNEMENTALE;
        }


        return $this;
    } // setClauseEnvironnementale()

    /**
     * Set the value of [clause_env_specs_techniques] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setClauseEnvSpecsTechniques($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_env_specs_techniques !== $v) {
            $this->clause_env_specs_techniques = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CLAUSE_ENV_SPECS_TECHNIQUES;
        }


        return $this;
    } // setClauseEnvSpecsTechniques()

    /**
     * Set the value of [clause_env_cond_execution] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setClauseEnvCondExecution($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_env_cond_execution !== $v) {
            $this->clause_env_cond_execution = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CLAUSE_ENV_COND_EXECUTION;
        }


        return $this;
    } // setClauseEnvCondExecution()

    /**
     * Set the value of [clause_env_criteres_select] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setClauseEnvCriteresSelect($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_env_criteres_select !== $v) {
            $this->clause_env_criteres_select = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CLAUSE_ENV_CRITERES_SELECT;
        }


        return $this;
    } // setClauseEnvCriteresSelect()

    /**
     * Sets the value of [date_prevue_notification] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDatePrevueNotification($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_prevue_notification !== null || $dt !== null) {
            $currentDateAsString = ($this->date_prevue_notification !== null && $tmpDt = new DateTime($this->date_prevue_notification)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_prevue_notification = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION;
            }
        } // if either are not null


        return $this;
    } // setDatePrevueNotification()

    /**
     * Sets the value of [date_prevue_fin_contrat] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDatePrevueFinContrat($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_prevue_fin_contrat !== null || $dt !== null) {
            $currentDateAsString = ($this->date_prevue_fin_contrat !== null && $tmpDt = new DateTime($this->date_prevue_fin_contrat)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_prevue_fin_contrat = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_PREVUE_FIN_CONTRAT;
            }
        } // if either are not null


        return $this;
    } // setDatePrevueFinContrat()

    /**
     * Sets the value of [date_prevue_max_fin_contrat] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDatePrevueMaxFinContrat($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_prevue_max_fin_contrat !== null || $dt !== null) {
            $currentDateAsString = ($this->date_prevue_max_fin_contrat !== null && $tmpDt = new DateTime($this->date_prevue_max_fin_contrat)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_prevue_max_fin_contrat = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_PREVUE_MAX_FIN_CONTRAT;
            }
        } // if either are not null


        return $this;
    } // setDatePrevueMaxFinContrat()

    /**
     * Sets the value of [date_notification] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDateNotification($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_notification !== null || $dt !== null) {
            $currentDateAsString = ($this->date_notification !== null && $tmpDt = new DateTime($this->date_notification)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_notification = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_NOTIFICATION;
            }
        } // if either are not null


        return $this;
    } // setDateNotification()

    /**
     * Sets the value of [date_fin_contrat] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDateFinContrat($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_fin_contrat !== null || $dt !== null) {
            $currentDateAsString = ($this->date_fin_contrat !== null && $tmpDt = new DateTime($this->date_fin_contrat)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_fin_contrat = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_FIN_CONTRAT;
            }
        } // if either are not null


        return $this;
    } // setDateFinContrat()

    /**
     * Sets the value of [date_max_fin_contrat] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDateMaxFinContrat($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_max_fin_contrat !== null || $dt !== null) {
            $currentDateAsString = ($this->date_max_fin_contrat !== null && $tmpDt = new DateTime($this->date_max_fin_contrat)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_max_fin_contrat = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_MAX_FIN_CONTRAT;
            }
        } // if either are not null


        return $this;
    } // setDateMaxFinContrat()

    /**
     * Sets the value of [date_attribution] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDateAttribution($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_attribution !== null || $dt !== null) {
            $currentDateAsString = ($this->date_attribution !== null && $tmpDt = new DateTime($this->date_attribution)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_attribution = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_ATTRIBUTION;
            }
        } // if either are not null


        return $this;
    } // setDateAttribution()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Sets the value of [date_modification] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDateModification($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_modification !== null || $dt !== null) {
            $currentDateAsString = ($this->date_modification !== null && $tmpDt = new DateTime($this->date_modification)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_modification = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_MODIFICATION;
            }
        } // if either are not null


        return $this;
    } // setDateModification()

    /**
     * Set the value of [envoi_interface] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setEnvoiInterface($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->envoi_interface !== $v) {
            $this->envoi_interface = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ENVOI_INTERFACE;
        }


        return $this;
    } // setEnvoiInterface()

    /**
     * Set the value of [contrat_class_key] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setContratClassKey($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->contrat_class_key !== $v) {
            $this->contrat_class_key = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CONTRAT_CLASS_KEY;
        }


        return $this;
    } // setContratClassKey()

    /**
     * Set the value of [pme_pmi] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setPmePmi($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pme_pmi !== $v) {
            $this->pme_pmi = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::PME_PMI;
        }


        return $this;
    } // setPmePmi()

    /**
     * Set the value of [reference_consultation] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setReferenceConsultation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->reference_consultation !== $v) {
            $this->reference_consultation = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::REFERENCE_CONSULTATION;
        }


        return $this;
    } // setReferenceConsultation()

    /**
     * Set the value of [hors_passation] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setHorsPassation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->hors_passation !== $v) {
            $this->hors_passation = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::HORS_PASSATION;
        }


        return $this;
    } // setHorsPassation()

    /**
     * Set the value of [id_agent] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_agent !== $v) {
            $this->id_agent = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_AGENT;
        }


        return $this;
    } // setIdAgent()

    /**
     * Set the value of [nom_agent] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setNomAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_agent !== $v) {
            $this->nom_agent = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::NOM_AGENT;
        }


        return $this;
    } // setNomAgent()

    /**
     * Set the value of [prenom_agent] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setPrenomAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prenom_agent !== $v) {
            $this->prenom_agent = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::PRENOM_AGENT;
        }


        return $this;
    } // setPrenomAgent()

    /**
     * Set the value of [lieu_execution] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setLieuExecution($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->lieu_execution !== $v) {
            $this->lieu_execution = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::LIEU_EXECUTION;
        }


        return $this;
    } // setLieuExecution()

    /**
     * Set the value of [code_cpv_1] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCodeCpv1($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code_cpv_1 !== $v) {
            $this->code_cpv_1 = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CODE_CPV_1;
        }


        return $this;
    } // setCodeCpv1()

    /**
     * Set the value of [code_cpv_2] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCodeCpv2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code_cpv_2 !== $v) {
            $this->code_cpv_2 = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CODE_CPV_2;
        }


        return $this;
    } // setCodeCpv2()

    /**
     * Sets the value of the [marche_insertion] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setMarcheInsertion($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->marche_insertion !== $v) {
            $this->marche_insertion = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::MARCHE_INSERTION;
        }


        return $this;
    } // setMarcheInsertion()

    /**
     * Set the value of [clause_specification_technique] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setClauseSpecificationTechnique($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->clause_specification_technique !== $v) {
            $this->clause_specification_technique = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CLAUSE_SPECIFICATION_TECHNIQUE;
        }


        return $this;
    } // setClauseSpecificationTechnique()

    /**
     * Set the value of [procedure_passation_pivot] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setProcedurePassationPivot($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->procedure_passation_pivot !== $v) {
            $this->procedure_passation_pivot = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::PROCEDURE_PASSATION_PIVOT;
        }


        return $this;
    } // setProcedurePassationPivot()

    /**
     * Set the value of [nom_lieu_principal_execution] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setNomLieuPrincipalExecution($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_lieu_principal_execution !== $v) {
            $this->nom_lieu_principal_execution = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::NOM_LIEU_PRINCIPAL_EXECUTION;
        }


        return $this;
    } // setNomLieuPrincipalExecution()

    /**
     * Set the value of [code_lieu_principal_execution] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCodeLieuPrincipalExecution($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code_lieu_principal_execution !== $v) {
            $this->code_lieu_principal_execution = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::CODE_LIEU_PRINCIPAL_EXECUTION;
        }


        return $this;
    } // setCodeLieuPrincipalExecution()

    /**
     * Set the value of [type_code_lieu_principal_execution] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setTypeCodeLieuPrincipalExecution($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_code_lieu_principal_execution !== $v) {
            $this->type_code_lieu_principal_execution = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::TYPE_CODE_LIEU_PRINCIPAL_EXECUTION;
        }


        return $this;
    } // setTypeCodeLieuPrincipalExecution()

    /**
     * Set the value of [duree_initiale_contrat] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDureeInitialeContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->duree_initiale_contrat !== $v) {
            $this->duree_initiale_contrat = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::DUREE_INITIALE_CONTRAT;
        }


        return $this;
    } // setDureeInitialeContrat()

    /**
     * Set the value of [forme_prix] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setFormePrix($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->forme_prix !== $v) {
            $this->forme_prix = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::FORME_PRIX;
        }


        return $this;
    } // setFormePrix()

    /**
     * Sets the value of [date_publication_initiale_de] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDatePublicationInitialeDe($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_publication_initiale_de !== null || $dt !== null) {
            $currentDateAsString = ($this->date_publication_initiale_de !== null && $tmpDt = new DateTime($this->date_publication_initiale_de)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ( ($currentDateAsString !== $newDateAsString) // normalized values don't match
                || ($dt->format('Y-m-d H:i:s') === NULL) // or the entered value matches the default
                 ) {
                $this->date_publication_initiale_de = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_PUBLICATION_INITIALE_DE;
            }
        } // if either are not null


        return $this;
    } // setDatePublicationInitialeDe()

    /**
     * Set the value of [num_id_unique_marche_public] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setNumIdUniqueMarchePublic($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->num_id_unique_marche_public !== $v) {
            $this->num_id_unique_marche_public = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::NUM_ID_UNIQUE_MARCHE_PUBLIC;
        }


        return $this;
    } // setNumIdUniqueMarchePublic()

    /**
     * Set the value of [libelle_type_contrat_pivot] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setLibelleTypeContratPivot($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_type_contrat_pivot !== $v) {
            $this->libelle_type_contrat_pivot = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::LIBELLE_TYPE_CONTRAT_PIVOT;
        }


        return $this;
    } // setLibelleTypeContratPivot()

    /**
     * Set the value of [siret_pa_accord_cadre] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setSiretPaAccordCadre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->siret_pa_accord_cadre !== $v) {
            $this->siret_pa_accord_cadre = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::SIRET_PA_ACCORD_CADRE;
        }


        return $this;
    } // setSiretPaAccordCadre()

    /**
     * Sets the value of the [ac_marche_subsequent] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setAcMarcheSubsequent($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->ac_marche_subsequent !== $v) {
            $this->ac_marche_subsequent = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::AC_MARCHE_SUBSEQUENT;
        }


        return $this;
    } // setAcMarcheSubsequent()

    /**
     * Set the value of [libelle_type_procedure_mpe] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setLibelleTypeProcedureMpe($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_type_procedure_mpe !== $v) {
            $this->libelle_type_procedure_mpe = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::LIBELLE_TYPE_PROCEDURE_MPE;
        }


        return $this;
    } // setLibelleTypeProcedureMpe()

    /**
     * Set the value of [nb_total_propositions_lot] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setNbTotalPropositionsLot($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->nb_total_propositions_lot !== $v) {
            $this->nb_total_propositions_lot = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_LOT;
        }


        return $this;
    } // setNbTotalPropositionsLot()

    /**
     * Set the value of [nb_total_propositions_demat_lot] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setNbTotalPropositionsDematLot($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->nb_total_propositions_demat_lot !== $v) {
            $this->nb_total_propositions_demat_lot = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_DEMAT_LOT;
        }


        return $this;
    } // setNbTotalPropositionsDematLot()

    /**
     * Set the value of [marche_defense] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setMarcheDefense($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->marche_defense !== $v) {
            $this->marche_defense = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::MARCHE_DEFENSE;
        }


        return $this;
    } // setMarcheDefense()

    /**
     * Set the value of [siret] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setSiret($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->siret !== $v) {
            $this->siret = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::SIRET;
        }


        return $this;
    } // setSiret()

    /**
     * Set the value of [nom_entite_acheteur] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setNomEntiteAcheteur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_entite_acheteur !== $v) {
            $this->nom_entite_acheteur = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::NOM_ENTITE_ACHETEUR;
        }


        return $this;
    } // setNomEntiteAcheteur()

    /**
     * Set the value of [statut_publication_sn] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setStatutPublicationSn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->statut_publication_sn !== $v) {
            $this->statut_publication_sn = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::STATUT_PUBLICATION_SN;
        }


        return $this;
    } // setStatutPublicationSn()

    /**
     * Sets the value of [date_publication_sn] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDatePublicationSn($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_publication_sn !== null || $dt !== null) {
            $currentDateAsString = ($this->date_publication_sn !== null && $tmpDt = new DateTime($this->date_publication_sn)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_publication_sn = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_PUBLICATION_SN;
            }
        } // if either are not null


        return $this;
    } // setDatePublicationSn()

    /**
     * Set the value of [erreur_sn] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setErreurSn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->erreur_sn !== $v) {
            $this->erreur_sn = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ERREUR_SN;
        }


        return $this;
    } // setErreurSn()

    /**
     * Sets the value of [date_modification_sn] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDateModificationSn($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_modification_sn !== null || $dt !== null) {
            $currentDateAsString = ($this->date_modification_sn !== null && $tmpDt = new DateTime($this->date_modification_sn)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_modification_sn = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_MODIFICATION_SN;
            }
        } // if either are not null


        return $this;
    } // setDateModificationSn()

    /**
     * Set the value of [id_type_procedure_pivot] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdTypeProcedurePivot($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_procedure_pivot !== $v) {
            $this->id_type_procedure_pivot = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT;
        }

        if ($this->aCommonTypeProcedure !== null && $this->aCommonTypeProcedure->getIdTypeProcedure() !== $v) {
            $this->aCommonTypeProcedure = null;
        }


        return $this;
    } // setIdTypeProcedurePivot()

    /**
     * Set the value of [id_type_procedure_concession_pivot] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdTypeProcedureConcessionPivot($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_procedure_concession_pivot !== $v) {
            $this->id_type_procedure_concession_pivot = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT;
        }

        if ($this->aCommonTypeProcedureConcessionPivot !== null && $this->aCommonTypeProcedureConcessionPivot->getId() !== $v) {
            $this->aCommonTypeProcedureConcessionPivot = null;
        }


        return $this;
    } // setIdTypeProcedureConcessionPivot()

    /**
     * Set the value of [id_type_contrat_pivot] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdTypeContratPivot($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_contrat_pivot !== $v) {
            $this->id_type_contrat_pivot = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT;
        }

        if ($this->aCommonTypeContratPivot !== null && $this->aCommonTypeContratPivot->getId() !== $v) {
            $this->aCommonTypeContratPivot = null;
        }


        return $this;
    } // setIdTypeContratPivot()

    /**
     * Set the value of [id_type_contrat_concession_pivot] column.
     *
     * @param int $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setIdTypeContratConcessionPivot($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_contrat_concession_pivot !== $v) {
            $this->id_type_contrat_concession_pivot = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT;
        }

        if ($this->aCommonTypeContratConcessionPivot !== null && $this->aCommonTypeContratConcessionPivot->getId() !== $v) {
            $this->aCommonTypeContratConcessionPivot = null;
        }


        return $this;
    } // setIdTypeContratConcessionPivot()

    /**
     * Set the value of [montant_subvention_publique] column.
     *
     * @param double $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setMontantSubventionPublique($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->montant_subvention_publique !== $v) {
            $this->montant_subvention_publique = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::MONTANT_SUBVENTION_PUBLIQUE;
        }


        return $this;
    } // setMontantSubventionPublique()

    /**
     * Sets the value of [date_debut_execution] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setDateDebutExecution($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_debut_execution !== null || $dt !== null) {
            $currentDateAsString = ($this->date_debut_execution !== null && $tmpDt = new DateTime($this->date_debut_execution)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_debut_execution = $newDateAsString;
                $this->modifiedColumns[] = CommonTContratTitulairePeer::DATE_DEBUT_EXECUTION;
            }
        } // if either are not null


        return $this;
    } // setDateDebutExecution()

    /**
     * Set the value of [uuid] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setUuid($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->uuid !== $v) {
            $this->uuid = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::UUID;
        }


        return $this;
    } // setUuid()

    /**
     * Sets the value of the [marche_innovant] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setMarcheInnovant($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->marche_innovant !== $v) {
            $this->marche_innovant = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::MARCHE_INNOVANT;
        }


        return $this;
    } // setMarcheInnovant()

    /**
     * Set the value of [service_id] column.
     *
     * @param string $v new value
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setServiceId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->service_id !== $v) {
            $this->service_id = $v;
            $this->modifiedColumns[] = CommonTContratTitulairePeer::SERVICE_ID;
        }


        return $this;
    } // setServiceId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->intitule !== '') {
                return false;
            }

            if ($this->statut_contrat !== 0) {
                return false;
            }

            if ($this->clause_sociale_siae !== '0') {
                return false;
            }

            if ($this->clause_sociale_ess !== '0') {
                return false;
            }

            if ($this->envoi_interface !== '0') {
                return false;
            }

            if ($this->contrat_class_key !== 1) {
                return false;
            }

            if ($this->pme_pmi !== 0) {
                return false;
            }

            if ($this->hors_passation !== 0) {
                return false;
            }

            if ($this->marche_insertion !== false) {
                return false;
            }

            if ($this->clause_specification_technique !== '0') {
                return false;
            }

            if ($this->date_publication_initiale_de !== NULL) {
                return false;
            }

            if ($this->ac_marche_subsequent !== false) {
                return false;
            }

            if ($this->marche_defense !== '0') {
                return false;
            }

            if ($this->statut_publication_sn !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_contrat_titulaire = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_type_contrat = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->id_contact_contrat = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->numero_contrat = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->lien_ac_sad = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->id_contrat_multi = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->organisme = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->old_service_id = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->id_titulaire = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->id_titulaire_etab = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->id_offre = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->type_depot_reponse = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->objet_contrat = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->intitule = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->montant_contrat = ($row[$startcol + 14] !== null) ? (double) $row[$startcol + 14] : null;
            $this->id_tranche_budgetaire = ($row[$startcol + 15] !== null) ? (int) $row[$startcol + 15] : null;
            $this->montant_max_estime = ($row[$startcol + 16] !== null) ? (double) $row[$startcol + 16] : null;
            $this->publication_montant = ($row[$startcol + 17] !== null) ? (int) $row[$startcol + 17] : null;
            $this->id_motif_non_publication_montant = ($row[$startcol + 18] !== null) ? (int) $row[$startcol + 18] : null;
            $this->desc_motif_non_publication_montant = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->publication_contrat = ($row[$startcol + 20] !== null) ? (int) $row[$startcol + 20] : null;
            $this->num_ej = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->statutej = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->num_long_oeap = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->reference_libre = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->statut_contrat = ($row[$startcol + 25] !== null) ? (int) $row[$startcol + 25] : null;
            $this->categorie = ($row[$startcol + 26] !== null) ? (int) $row[$startcol + 26] : null;
            $this->ccag_applicable = ($row[$startcol + 27] !== null) ? (int) $row[$startcol + 27] : null;
            $this->clause_sociale = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->clause_sociale_condition_execution = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->clause_sociale_insertion = ($row[$startcol + 30] !== null) ? (string) $row[$startcol + 30] : null;
            $this->clause_sociale_ateliers_proteges = ($row[$startcol + 31] !== null) ? (string) $row[$startcol + 31] : null;
            $this->clause_sociale_siae = ($row[$startcol + 32] !== null) ? (string) $row[$startcol + 32] : null;
            $this->clause_sociale_ess = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->clause_environnementale = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->clause_env_specs_techniques = ($row[$startcol + 35] !== null) ? (string) $row[$startcol + 35] : null;
            $this->clause_env_cond_execution = ($row[$startcol + 36] !== null) ? (string) $row[$startcol + 36] : null;
            $this->clause_env_criteres_select = ($row[$startcol + 37] !== null) ? (string) $row[$startcol + 37] : null;
            $this->date_prevue_notification = ($row[$startcol + 38] !== null) ? (string) $row[$startcol + 38] : null;
            $this->date_prevue_fin_contrat = ($row[$startcol + 39] !== null) ? (string) $row[$startcol + 39] : null;
            $this->date_prevue_max_fin_contrat = ($row[$startcol + 40] !== null) ? (string) $row[$startcol + 40] : null;
            $this->date_notification = ($row[$startcol + 41] !== null) ? (string) $row[$startcol + 41] : null;
            $this->date_fin_contrat = ($row[$startcol + 42] !== null) ? (string) $row[$startcol + 42] : null;
            $this->date_max_fin_contrat = ($row[$startcol + 43] !== null) ? (string) $row[$startcol + 43] : null;
            $this->date_attribution = ($row[$startcol + 44] !== null) ? (string) $row[$startcol + 44] : null;
            $this->date_creation = ($row[$startcol + 45] !== null) ? (string) $row[$startcol + 45] : null;
            $this->date_modification = ($row[$startcol + 46] !== null) ? (string) $row[$startcol + 46] : null;
            $this->envoi_interface = ($row[$startcol + 47] !== null) ? (string) $row[$startcol + 47] : null;
            $this->contrat_class_key = ($row[$startcol + 48] !== null) ? (int) $row[$startcol + 48] : null;
            $this->pme_pmi = ($row[$startcol + 49] !== null) ? (int) $row[$startcol + 49] : null;
            $this->reference_consultation = ($row[$startcol + 50] !== null) ? (string) $row[$startcol + 50] : null;
            $this->hors_passation = ($row[$startcol + 51] !== null) ? (int) $row[$startcol + 51] : null;
            $this->id_agent = ($row[$startcol + 52] !== null) ? (int) $row[$startcol + 52] : null;
            $this->nom_agent = ($row[$startcol + 53] !== null) ? (string) $row[$startcol + 53] : null;
            $this->prenom_agent = ($row[$startcol + 54] !== null) ? (string) $row[$startcol + 54] : null;
            $this->lieu_execution = ($row[$startcol + 55] !== null) ? (string) $row[$startcol + 55] : null;
            $this->code_cpv_1 = ($row[$startcol + 56] !== null) ? (string) $row[$startcol + 56] : null;
            $this->code_cpv_2 = ($row[$startcol + 57] !== null) ? (string) $row[$startcol + 57] : null;
            $this->marche_insertion = ($row[$startcol + 58] !== null) ? (boolean) $row[$startcol + 58] : null;
            $this->clause_specification_technique = ($row[$startcol + 59] !== null) ? (string) $row[$startcol + 59] : null;
            $this->procedure_passation_pivot = ($row[$startcol + 60] !== null) ? (string) $row[$startcol + 60] : null;
            $this->nom_lieu_principal_execution = ($row[$startcol + 61] !== null) ? (string) $row[$startcol + 61] : null;
            $this->code_lieu_principal_execution = ($row[$startcol + 62] !== null) ? (string) $row[$startcol + 62] : null;
            $this->type_code_lieu_principal_execution = ($row[$startcol + 63] !== null) ? (string) $row[$startcol + 63] : null;
            $this->duree_initiale_contrat = ($row[$startcol + 64] !== null) ? (int) $row[$startcol + 64] : null;
            $this->forme_prix = ($row[$startcol + 65] !== null) ? (string) $row[$startcol + 65] : null;
            $this->date_publication_initiale_de = ($row[$startcol + 66] !== null) ? (string) $row[$startcol + 66] : null;
            $this->num_id_unique_marche_public = ($row[$startcol + 67] !== null) ? (string) $row[$startcol + 67] : null;
            $this->libelle_type_contrat_pivot = ($row[$startcol + 68] !== null) ? (string) $row[$startcol + 68] : null;
            $this->siret_pa_accord_cadre = ($row[$startcol + 69] !== null) ? (string) $row[$startcol + 69] : null;
            $this->ac_marche_subsequent = ($row[$startcol + 70] !== null) ? (boolean) $row[$startcol + 70] : null;
            $this->libelle_type_procedure_mpe = ($row[$startcol + 71] !== null) ? (string) $row[$startcol + 71] : null;
            $this->nb_total_propositions_lot = ($row[$startcol + 72] !== null) ? (int) $row[$startcol + 72] : null;
            $this->nb_total_propositions_demat_lot = ($row[$startcol + 73] !== null) ? (int) $row[$startcol + 73] : null;
            $this->marche_defense = ($row[$startcol + 74] !== null) ? (string) $row[$startcol + 74] : null;
            $this->siret = ($row[$startcol + 75] !== null) ? (string) $row[$startcol + 75] : null;
            $this->nom_entite_acheteur = ($row[$startcol + 76] !== null) ? (string) $row[$startcol + 76] : null;
            $this->statut_publication_sn = ($row[$startcol + 77] !== null) ? (int) $row[$startcol + 77] : null;
            $this->date_publication_sn = ($row[$startcol + 78] !== null) ? (string) $row[$startcol + 78] : null;
            $this->erreur_sn = ($row[$startcol + 79] !== null) ? (string) $row[$startcol + 79] : null;
            $this->date_modification_sn = ($row[$startcol + 80] !== null) ? (string) $row[$startcol + 80] : null;
            $this->id_type_procedure_pivot = ($row[$startcol + 81] !== null) ? (int) $row[$startcol + 81] : null;
            $this->id_type_procedure_concession_pivot = ($row[$startcol + 82] !== null) ? (int) $row[$startcol + 82] : null;
            $this->id_type_contrat_pivot = ($row[$startcol + 83] !== null) ? (int) $row[$startcol + 83] : null;
            $this->id_type_contrat_concession_pivot = ($row[$startcol + 84] !== null) ? (int) $row[$startcol + 84] : null;
            $this->montant_subvention_publique = ($row[$startcol + 85] !== null) ? (double) $row[$startcol + 85] : null;
            $this->date_debut_execution = ($row[$startcol + 86] !== null) ? (string) $row[$startcol + 86] : null;
            $this->uuid = ($row[$startcol + 87] !== null) ? (string) $row[$startcol + 87] : null;
            $this->marche_innovant = ($row[$startcol + 88] !== null) ? (boolean) $row[$startcol + 88] : null;
            $this->service_id = ($row[$startcol + 89] !== null) ? (string) $row[$startcol + 89] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 90; // 90 = CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTContratTitulaire object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTTypeContrat !== null && $this->id_type_contrat !== $this->aCommonTTypeContrat->getIdTypeContrat()) {
            $this->aCommonTTypeContrat = null;
        }
        if ($this->aCommonTContactContrat !== null && $this->id_contact_contrat !== $this->aCommonTContactContrat->getIdContactContrat()) {
            $this->aCommonTContactContrat = null;
        }
        if ($this->aCommonTContratTitulaireRelatedByIdContratMulti !== null && $this->id_contrat_multi !== $this->aCommonTContratTitulaireRelatedByIdContratMulti->getIdContratTitulaire()) {
            $this->aCommonTContratTitulaireRelatedByIdContratMulti = null;
        }
        if ($this->aCommonTypeProcedure !== null && $this->id_type_procedure_pivot !== $this->aCommonTypeProcedure->getIdTypeProcedure()) {
            $this->aCommonTypeProcedure = null;
        }
        if ($this->aCommonTypeProcedureConcessionPivot !== null && $this->id_type_procedure_concession_pivot !== $this->aCommonTypeProcedureConcessionPivot->getId()) {
            $this->aCommonTypeProcedureConcessionPivot = null;
        }
        if ($this->aCommonTypeContratPivot !== null && $this->id_type_contrat_pivot !== $this->aCommonTypeContratPivot->getId()) {
            $this->aCommonTypeContratPivot = null;
        }
        if ($this->aCommonTypeContratConcessionPivot !== null && $this->id_type_contrat_concession_pivot !== $this->aCommonTypeContratConcessionPivot->getId()) {
            $this->aCommonTypeContratConcessionPivot = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTContratTitulairePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonTypeContratConcessionPivot = null;
            $this->aCommonTypeContratPivot = null;
            $this->aCommonTypeProcedureConcessionPivot = null;
            $this->aCommonTContactContrat = null;
            $this->aCommonTContratTitulaireRelatedByIdContratMulti = null;
            $this->aCommonTTypeContrat = null;
            $this->aCommonTypeProcedure = null;
            $this->collCommonMarches = null;

            $this->collCommonDonneesAnnuellesConcessions = null;

            $this->collCommonEchangeDocs = null;

            $this->collCommonModificationContrats = null;

            $this->collCommonTConsLotContrats = null;

            $this->collCommonTContratTitulairesRelatedByIdContratTitulaire = null;

            $this->collCommonTDonneesConsultations = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTContratTitulaireQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTContratTitulairePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTContratTitulairePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTypeContratConcessionPivot !== null) {
                if ($this->aCommonTypeContratConcessionPivot->isModified() || $this->aCommonTypeContratConcessionPivot->isNew()) {
                    $affectedRows += $this->aCommonTypeContratConcessionPivot->save($con);
                }
                $this->setCommonTypeContratConcessionPivot($this->aCommonTypeContratConcessionPivot);
            }

            if ($this->aCommonTypeContratPivot !== null) {
                if ($this->aCommonTypeContratPivot->isModified() || $this->aCommonTypeContratPivot->isNew()) {
                    $affectedRows += $this->aCommonTypeContratPivot->save($con);
                }
                $this->setCommonTypeContratPivot($this->aCommonTypeContratPivot);
            }

            if ($this->aCommonTypeProcedureConcessionPivot !== null) {
                if ($this->aCommonTypeProcedureConcessionPivot->isModified() || $this->aCommonTypeProcedureConcessionPivot->isNew()) {
                    $affectedRows += $this->aCommonTypeProcedureConcessionPivot->save($con);
                }
                $this->setCommonTypeProcedureConcessionPivot($this->aCommonTypeProcedureConcessionPivot);
            }

            if ($this->aCommonTContactContrat !== null) {
                if ($this->aCommonTContactContrat->isModified() || $this->aCommonTContactContrat->isNew()) {
                    $affectedRows += $this->aCommonTContactContrat->save($con);
                }
                $this->setCommonTContactContrat($this->aCommonTContactContrat);
            }

            if ($this->aCommonTContratTitulaireRelatedByIdContratMulti !== null) {
                if ($this->aCommonTContratTitulaireRelatedByIdContratMulti->isModified() || $this->aCommonTContratTitulaireRelatedByIdContratMulti->isNew()) {
                    $affectedRows += $this->aCommonTContratTitulaireRelatedByIdContratMulti->save($con);
                }
                $this->setCommonTContratTitulaireRelatedByIdContratMulti($this->aCommonTContratTitulaireRelatedByIdContratMulti);
            }

            if ($this->aCommonTTypeContrat !== null) {
                if ($this->aCommonTTypeContrat->isModified() || $this->aCommonTTypeContrat->isNew()) {
                    $affectedRows += $this->aCommonTTypeContrat->save($con);
                }
                $this->setCommonTTypeContrat($this->aCommonTTypeContrat);
            }

            if ($this->aCommonTypeProcedure !== null) {
                if ($this->aCommonTypeProcedure->isModified() || $this->aCommonTypeProcedure->isNew()) {
                    $affectedRows += $this->aCommonTypeProcedure->save($con);
                }
                $this->setCommonTypeProcedure($this->aCommonTypeProcedure);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonMarchesScheduledForDeletion !== null) {
                if (!$this->commonMarchesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonMarcheQuery::create()
                        ->filterByPrimaryKeys($this->commonMarchesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonMarchesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonMarches !== null) {
                foreach ($this->collCommonMarches as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonDonneesAnnuellesConcessionsScheduledForDeletion !== null) {
                if (!$this->commonDonneesAnnuellesConcessionsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonDonneesAnnuellesConcessionsScheduledForDeletion as $commonDonneesAnnuellesConcession) {
                        // need to save related object because we set the relation to null
                        $commonDonneesAnnuellesConcession->save($con);
                    }
                    $this->commonDonneesAnnuellesConcessionsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonDonneesAnnuellesConcessions !== null) {
                foreach ($this->collCommonDonneesAnnuellesConcessions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonEchangeDocsScheduledForDeletion !== null) {
                if (!$this->commonEchangeDocsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangeDocsScheduledForDeletion as $commonEchangeDoc) {
                        // need to save related object because we set the relation to null
                        $commonEchangeDoc->save($con);
                    }
                    $this->commonEchangeDocsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchangeDocs !== null) {
                foreach ($this->collCommonEchangeDocs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonModificationContratsScheduledForDeletion !== null) {
                if (!$this->commonModificationContratsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonModificationContratsScheduledForDeletion as $commonModificationContrat) {
                        // need to save related object because we set the relation to null
                        $commonModificationContrat->save($con);
                    }
                    $this->commonModificationContratsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonModificationContrats !== null) {
                foreach ($this->collCommonModificationContrats as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTConsLotContratsScheduledForDeletion !== null) {
                if (!$this->commonTConsLotContratsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTConsLotContratQuery::create()
                        ->filterByPrimaryKeys($this->commonTConsLotContratsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTConsLotContratsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTConsLotContrats !== null) {
                foreach ($this->collCommonTConsLotContrats as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTContratTitulairesRelatedByIdContratTitulaireScheduledForDeletion !== null) {
                if (!$this->commonTContratTitulairesRelatedByIdContratTitulaireScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTContratTitulaireQuery::create()
                        ->filterByPrimaryKeys($this->commonTContratTitulairesRelatedByIdContratTitulaireScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTContratTitulairesRelatedByIdContratTitulaireScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTContratTitulairesRelatedByIdContratTitulaire !== null) {
                foreach ($this->collCommonTContratTitulairesRelatedByIdContratTitulaire as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTDonneesConsultationsScheduledForDeletion !== null) {
                if (!$this->commonTDonneesConsultationsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTDonneesConsultationQuery::create()
                        ->filterByPrimaryKeys($this->commonTDonneesConsultationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTDonneesConsultationsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTDonneesConsultations !== null) {
                foreach ($this->collCommonTDonneesConsultations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE;
        if (null !== $this->id_contrat_titulaire) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_contrat_titulaire`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TYPE_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`id_contact_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::NUMERO_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`numero_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::LIEN_AC_SAD)) {
            $modifiedColumns[':p' . $index++]  = '`lien_AC_SAD`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_CONTRAT_MULTI)) {
            $modifiedColumns[':p' . $index++]  = '`id_contrat_multi`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::OLD_SERVICE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`old_service_id`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TITULAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_titulaire`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TITULAIRE_ETAB)) {
            $modifiedColumns[':p' . $index++]  = '`id_titulaire_etab`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_OFFRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_offre`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::TYPE_DEPOT_REPONSE)) {
            $modifiedColumns[':p' . $index++]  = '`type_depot_reponse`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::OBJET_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`objet_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::INTITULE)) {
            $modifiedColumns[':p' . $index++]  = '`intitule`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::MONTANT_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`montant_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TRANCHE_BUDGETAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_tranche_budgetaire`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::MONTANT_MAX_ESTIME)) {
            $modifiedColumns[':p' . $index++]  = '`montant_max_estime`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::PUBLICATION_MONTANT)) {
            $modifiedColumns[':p' . $index++]  = '`publication_montant`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_MOTIF_NON_PUBLICATION_MONTANT)) {
            $modifiedColumns[':p' . $index++]  = '`id_motif_non_publication_montant`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DESC_MOTIF_NON_PUBLICATION_MONTANT)) {
            $modifiedColumns[':p' . $index++]  = '`desc_motif_non_publication_montant`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::PUBLICATION_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`publication_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::NUM_EJ)) {
            $modifiedColumns[':p' . $index++]  = '`num_EJ`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::STATUTEJ)) {
            $modifiedColumns[':p' . $index++]  = '`statutEJ`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::NUM_LONG_OEAP)) {
            $modifiedColumns[':p' . $index++]  = '`num_long_OEAP`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::REFERENCE_LIBRE)) {
            $modifiedColumns[':p' . $index++]  = '`reference_libre`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::STATUT_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`statut_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CATEGORIE)) {
            $modifiedColumns[':p' . $index++]  = '`categorie`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CCAG_APPLICABLE)) {
            $modifiedColumns[':p' . $index++]  = '`ccag_applicable`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE)) {
            $modifiedColumns[':p' . $index++]  = '`clause_sociale`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE_CONDITION_EXECUTION)) {
            $modifiedColumns[':p' . $index++]  = '`clause_sociale_condition_execution`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE_INSERTION)) {
            $modifiedColumns[':p' . $index++]  = '`clause_sociale_insertion`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE_ATELIERS_PROTEGES)) {
            $modifiedColumns[':p' . $index++]  = '`clause_sociale_ateliers_proteges`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE_SIAE)) {
            $modifiedColumns[':p' . $index++]  = '`clause_sociale_siae`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE_ESS)) {
            $modifiedColumns[':p' . $index++]  = '`clause_sociale_ess`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_ENVIRONNEMENTALE)) {
            $modifiedColumns[':p' . $index++]  = '`clause_environnementale`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_ENV_SPECS_TECHNIQUES)) {
            $modifiedColumns[':p' . $index++]  = '`clause_env_specs_techniques`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_ENV_COND_EXECUTION)) {
            $modifiedColumns[':p' . $index++]  = '`clause_env_cond_execution`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_ENV_CRITERES_SELECT)) {
            $modifiedColumns[':p' . $index++]  = '`clause_env_criteres_select`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_prevue_notification`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_PREVUE_FIN_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`date_prevue_fin_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_PREVUE_MAX_FIN_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`date_prevue_max_fin_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_NOTIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_notification`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_FIN_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`date_fin_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_MAX_FIN_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`date_max_fin_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_ATTRIBUTION)) {
            $modifiedColumns[':p' . $index++]  = '`date_attribution`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ENVOI_INTERFACE)) {
            $modifiedColumns[':p' . $index++]  = '`envoi_interface`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CONTRAT_CLASS_KEY)) {
            $modifiedColumns[':p' . $index++]  = '`contrat_class_key`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::PME_PMI)) {
            $modifiedColumns[':p' . $index++]  = '`pme_pmi`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::REFERENCE_CONSULTATION)) {
            $modifiedColumns[':p' . $index++]  = '`reference_consultation`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::HORS_PASSATION)) {
            $modifiedColumns[':p' . $index++]  = '`hors_passation`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_agent`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::NOM_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`nom_agent`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::PRENOM_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`prenom_agent`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::LIEU_EXECUTION)) {
            $modifiedColumns[':p' . $index++]  = '`lieu_execution`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CODE_CPV_1)) {
            $modifiedColumns[':p' . $index++]  = '`code_cpv_1`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CODE_CPV_2)) {
            $modifiedColumns[':p' . $index++]  = '`code_cpv_2`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::MARCHE_INSERTION)) {
            $modifiedColumns[':p' . $index++]  = '`marche_insertion`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SPECIFICATION_TECHNIQUE)) {
            $modifiedColumns[':p' . $index++]  = '`clause_specification_technique`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::PROCEDURE_PASSATION_PIVOT)) {
            $modifiedColumns[':p' . $index++]  = '`procedure_passation_pivot`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::NOM_LIEU_PRINCIPAL_EXECUTION)) {
            $modifiedColumns[':p' . $index++]  = '`nom_lieu_principal_execution`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::CODE_LIEU_PRINCIPAL_EXECUTION)) {
            $modifiedColumns[':p' . $index++]  = '`code_lieu_principal_execution`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::TYPE_CODE_LIEU_PRINCIPAL_EXECUTION)) {
            $modifiedColumns[':p' . $index++]  = '`type_code_lieu_principal_execution`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DUREE_INITIALE_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`duree_initiale_contrat`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::FORME_PRIX)) {
            $modifiedColumns[':p' . $index++]  = '`forme_prix`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_PUBLICATION_INITIALE_DE)) {
            $modifiedColumns[':p' . $index++]  = '`date_publication_initiale_de`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::NUM_ID_UNIQUE_MARCHE_PUBLIC)) {
            $modifiedColumns[':p' . $index++]  = '`num_id_unique_marche_public`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::LIBELLE_TYPE_CONTRAT_PIVOT)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_type_contrat_pivot`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::SIRET_PA_ACCORD_CADRE)) {
            $modifiedColumns[':p' . $index++]  = '`siret_pa_accord_cadre`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::AC_MARCHE_SUBSEQUENT)) {
            $modifiedColumns[':p' . $index++]  = '`ac_marche_subsequent`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::LIBELLE_TYPE_PROCEDURE_MPE)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_type_procedure_mpe`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_LOT)) {
            $modifiedColumns[':p' . $index++]  = '`nb_total_propositions_lot`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_DEMAT_LOT)) {
            $modifiedColumns[':p' . $index++]  = '`nb_total_propositions_demat_lot`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::MARCHE_DEFENSE)) {
            $modifiedColumns[':p' . $index++]  = '`marche_defense`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::SIRET)) {
            $modifiedColumns[':p' . $index++]  = '`siret`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::NOM_ENTITE_ACHETEUR)) {
            $modifiedColumns[':p' . $index++]  = '`nom_entite_acheteur`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::STATUT_PUBLICATION_SN)) {
            $modifiedColumns[':p' . $index++]  = '`statut_publication_sn`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_PUBLICATION_SN)) {
            $modifiedColumns[':p' . $index++]  = '`date_publication_sn`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ERREUR_SN)) {
            $modifiedColumns[':p' . $index++]  = '`erreur_sn`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_MODIFICATION_SN)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification_sn`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_procedure_pivot`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_procedure_concession_pivot`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_contrat_pivot`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_contrat_concession_pivot`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::MONTANT_SUBVENTION_PUBLIQUE)) {
            $modifiedColumns[':p' . $index++]  = '`montant_subvention_publique`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_DEBUT_EXECUTION)) {
            $modifiedColumns[':p' . $index++]  = '`date_debut_execution`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::UUID)) {
            $modifiedColumns[':p' . $index++]  = '`uuid`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::MARCHE_INNOVANT)) {
            $modifiedColumns[':p' . $index++]  = '`marche_innovant`';
        }
        if ($this->isColumnModified(CommonTContratTitulairePeer::SERVICE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`service_id`';
        }

        $sql = sprintf(
            'INSERT INTO `t_contrat_titulaire` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_contrat_titulaire`':
                        $stmt->bindValue($identifier, $this->id_contrat_titulaire, PDO::PARAM_INT);
                        break;
                    case '`id_type_contrat`':
                        $stmt->bindValue($identifier, $this->id_type_contrat, PDO::PARAM_INT);
                        break;
                    case '`id_contact_contrat`':
                        $stmt->bindValue($identifier, $this->id_contact_contrat, PDO::PARAM_INT);
                        break;
                    case '`numero_contrat`':
                        $stmt->bindValue($identifier, $this->numero_contrat, PDO::PARAM_STR);
                        break;
                    case '`lien_AC_SAD`':
                        $stmt->bindValue($identifier, $this->lien_ac_sad, PDO::PARAM_INT);
                        break;
                    case '`id_contrat_multi`':
                        $stmt->bindValue($identifier, $this->id_contrat_multi, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`old_service_id`':
                        $stmt->bindValue($identifier, $this->old_service_id, PDO::PARAM_INT);
                        break;
                    case '`id_titulaire`':
                        $stmt->bindValue($identifier, $this->id_titulaire, PDO::PARAM_INT);
                        break;
                    case '`id_titulaire_etab`':
                        $stmt->bindValue($identifier, $this->id_titulaire_etab, PDO::PARAM_INT);
                        break;
                    case '`id_offre`':
                        $stmt->bindValue($identifier, $this->id_offre, PDO::PARAM_INT);
                        break;
                    case '`type_depot_reponse`':
                        $stmt->bindValue($identifier, $this->type_depot_reponse, PDO::PARAM_STR);
                        break;
                    case '`objet_contrat`':
                        $stmt->bindValue($identifier, $this->objet_contrat, PDO::PARAM_STR);
                        break;
                    case '`intitule`':
                        $stmt->bindValue($identifier, $this->intitule, PDO::PARAM_STR);
                        break;
                    case '`montant_contrat`':
                        $stmt->bindValue($identifier, $this->montant_contrat, PDO::PARAM_STR);
                        break;
                    case '`id_tranche_budgetaire`':
                        $stmt->bindValue($identifier, $this->id_tranche_budgetaire, PDO::PARAM_INT);
                        break;
                    case '`montant_max_estime`':
                        $stmt->bindValue($identifier, $this->montant_max_estime, PDO::PARAM_STR);
                        break;
                    case '`publication_montant`':
                        $stmt->bindValue($identifier, $this->publication_montant, PDO::PARAM_INT);
                        break;
                    case '`id_motif_non_publication_montant`':
                        $stmt->bindValue($identifier, $this->id_motif_non_publication_montant, PDO::PARAM_INT);
                        break;
                    case '`desc_motif_non_publication_montant`':
                        $stmt->bindValue($identifier, $this->desc_motif_non_publication_montant, PDO::PARAM_STR);
                        break;
                    case '`publication_contrat`':
                        $stmt->bindValue($identifier, $this->publication_contrat, PDO::PARAM_INT);
                        break;
                    case '`num_EJ`':
                        $stmt->bindValue($identifier, $this->num_ej, PDO::PARAM_STR);
                        break;
                    case '`statutEJ`':
                        $stmt->bindValue($identifier, $this->statutej, PDO::PARAM_STR);
                        break;
                    case '`num_long_OEAP`':
                        $stmt->bindValue($identifier, $this->num_long_oeap, PDO::PARAM_STR);
                        break;
                    case '`reference_libre`':
                        $stmt->bindValue($identifier, $this->reference_libre, PDO::PARAM_STR);
                        break;
                    case '`statut_contrat`':
                        $stmt->bindValue($identifier, $this->statut_contrat, PDO::PARAM_INT);
                        break;
                    case '`categorie`':
                        $stmt->bindValue($identifier, $this->categorie, PDO::PARAM_INT);
                        break;
                    case '`ccag_applicable`':
                        $stmt->bindValue($identifier, $this->ccag_applicable, PDO::PARAM_INT);
                        break;
                    case '`clause_sociale`':
                        $stmt->bindValue($identifier, $this->clause_sociale, PDO::PARAM_STR);
                        break;
                    case '`clause_sociale_condition_execution`':
                        $stmt->bindValue($identifier, $this->clause_sociale_condition_execution, PDO::PARAM_STR);
                        break;
                    case '`clause_sociale_insertion`':
                        $stmt->bindValue($identifier, $this->clause_sociale_insertion, PDO::PARAM_STR);
                        break;
                    case '`clause_sociale_ateliers_proteges`':
                        $stmt->bindValue($identifier, $this->clause_sociale_ateliers_proteges, PDO::PARAM_STR);
                        break;
                    case '`clause_sociale_siae`':
                        $stmt->bindValue($identifier, $this->clause_sociale_siae, PDO::PARAM_STR);
                        break;
                    case '`clause_sociale_ess`':
                        $stmt->bindValue($identifier, $this->clause_sociale_ess, PDO::PARAM_STR);
                        break;
                    case '`clause_environnementale`':
                        $stmt->bindValue($identifier, $this->clause_environnementale, PDO::PARAM_STR);
                        break;
                    case '`clause_env_specs_techniques`':
                        $stmt->bindValue($identifier, $this->clause_env_specs_techniques, PDO::PARAM_STR);
                        break;
                    case '`clause_env_cond_execution`':
                        $stmt->bindValue($identifier, $this->clause_env_cond_execution, PDO::PARAM_STR);
                        break;
                    case '`clause_env_criteres_select`':
                        $stmt->bindValue($identifier, $this->clause_env_criteres_select, PDO::PARAM_STR);
                        break;
                    case '`date_prevue_notification`':
                        $stmt->bindValue($identifier, $this->date_prevue_notification, PDO::PARAM_STR);
                        break;
                    case '`date_prevue_fin_contrat`':
                        $stmt->bindValue($identifier, $this->date_prevue_fin_contrat, PDO::PARAM_STR);
                        break;
                    case '`date_prevue_max_fin_contrat`':
                        $stmt->bindValue($identifier, $this->date_prevue_max_fin_contrat, PDO::PARAM_STR);
                        break;
                    case '`date_notification`':
                        $stmt->bindValue($identifier, $this->date_notification, PDO::PARAM_STR);
                        break;
                    case '`date_fin_contrat`':
                        $stmt->bindValue($identifier, $this->date_fin_contrat, PDO::PARAM_STR);
                        break;
                    case '`date_max_fin_contrat`':
                        $stmt->bindValue($identifier, $this->date_max_fin_contrat, PDO::PARAM_STR);
                        break;
                    case '`date_attribution`':
                        $stmt->bindValue($identifier, $this->date_attribution, PDO::PARAM_STR);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_modification`':
                        $stmt->bindValue($identifier, $this->date_modification, PDO::PARAM_STR);
                        break;
                    case '`envoi_interface`':
                        $stmt->bindValue($identifier, $this->envoi_interface, PDO::PARAM_STR);
                        break;
                    case '`contrat_class_key`':
                        $stmt->bindValue($identifier, $this->contrat_class_key, PDO::PARAM_INT);
                        break;
                    case '`pme_pmi`':
                        $stmt->bindValue($identifier, $this->pme_pmi, PDO::PARAM_INT);
                        break;
                    case '`reference_consultation`':
                        $stmt->bindValue($identifier, $this->reference_consultation, PDO::PARAM_STR);
                        break;
                    case '`hors_passation`':
                        $stmt->bindValue($identifier, $this->hors_passation, PDO::PARAM_INT);
                        break;
                    case '`id_agent`':
                        $stmt->bindValue($identifier, $this->id_agent, PDO::PARAM_INT);
                        break;
                    case '`nom_agent`':
                        $stmt->bindValue($identifier, $this->nom_agent, PDO::PARAM_STR);
                        break;
                    case '`prenom_agent`':
                        $stmt->bindValue($identifier, $this->prenom_agent, PDO::PARAM_STR);
                        break;
                    case '`lieu_execution`':
                        $stmt->bindValue($identifier, $this->lieu_execution, PDO::PARAM_STR);
                        break;
                    case '`code_cpv_1`':
                        $stmt->bindValue($identifier, $this->code_cpv_1, PDO::PARAM_STR);
                        break;
                    case '`code_cpv_2`':
                        $stmt->bindValue($identifier, $this->code_cpv_2, PDO::PARAM_STR);
                        break;
                    case '`marche_insertion`':
                        $stmt->bindValue($identifier, (int) $this->marche_insertion, PDO::PARAM_INT);
                        break;
                    case '`clause_specification_technique`':
                        $stmt->bindValue($identifier, $this->clause_specification_technique, PDO::PARAM_STR);
                        break;
                    case '`procedure_passation_pivot`':
                        $stmt->bindValue($identifier, $this->procedure_passation_pivot, PDO::PARAM_STR);
                        break;
                    case '`nom_lieu_principal_execution`':
                        $stmt->bindValue($identifier, $this->nom_lieu_principal_execution, PDO::PARAM_STR);
                        break;
                    case '`code_lieu_principal_execution`':
                        $stmt->bindValue($identifier, $this->code_lieu_principal_execution, PDO::PARAM_STR);
                        break;
                    case '`type_code_lieu_principal_execution`':
                        $stmt->bindValue($identifier, $this->type_code_lieu_principal_execution, PDO::PARAM_STR);
                        break;
                    case '`duree_initiale_contrat`':
                        $stmt->bindValue($identifier, $this->duree_initiale_contrat, PDO::PARAM_INT);
                        break;
                    case '`forme_prix`':
                        $stmt->bindValue($identifier, $this->forme_prix, PDO::PARAM_STR);
                        break;
                    case '`date_publication_initiale_de`':
                        $stmt->bindValue($identifier, $this->date_publication_initiale_de, PDO::PARAM_STR);
                        break;
                    case '`num_id_unique_marche_public`':
                        $stmt->bindValue($identifier, $this->num_id_unique_marche_public, PDO::PARAM_STR);
                        break;
                    case '`libelle_type_contrat_pivot`':
                        $stmt->bindValue($identifier, $this->libelle_type_contrat_pivot, PDO::PARAM_STR);
                        break;
                    case '`siret_pa_accord_cadre`':
                        $stmt->bindValue($identifier, $this->siret_pa_accord_cadre, PDO::PARAM_STR);
                        break;
                    case '`ac_marche_subsequent`':
                        $stmt->bindValue($identifier, (int) $this->ac_marche_subsequent, PDO::PARAM_INT);
                        break;
                    case '`libelle_type_procedure_mpe`':
                        $stmt->bindValue($identifier, $this->libelle_type_procedure_mpe, PDO::PARAM_STR);
                        break;
                    case '`nb_total_propositions_lot`':
                        $stmt->bindValue($identifier, $this->nb_total_propositions_lot, PDO::PARAM_INT);
                        break;
                    case '`nb_total_propositions_demat_lot`':
                        $stmt->bindValue($identifier, $this->nb_total_propositions_demat_lot, PDO::PARAM_INT);
                        break;
                    case '`marche_defense`':
                        $stmt->bindValue($identifier, $this->marche_defense, PDO::PARAM_STR);
                        break;
                    case '`siret`':
                        $stmt->bindValue($identifier, $this->siret, PDO::PARAM_STR);
                        break;
                    case '`nom_entite_acheteur`':
                        $stmt->bindValue($identifier, $this->nom_entite_acheteur, PDO::PARAM_STR);
                        break;
                    case '`statut_publication_sn`':
                        $stmt->bindValue($identifier, $this->statut_publication_sn, PDO::PARAM_INT);
                        break;
                    case '`date_publication_sn`':
                        $stmt->bindValue($identifier, $this->date_publication_sn, PDO::PARAM_STR);
                        break;
                    case '`erreur_sn`':
                        $stmt->bindValue($identifier, $this->erreur_sn, PDO::PARAM_STR);
                        break;
                    case '`date_modification_sn`':
                        $stmt->bindValue($identifier, $this->date_modification_sn, PDO::PARAM_STR);
                        break;
                    case '`id_type_procedure_pivot`':
                        $stmt->bindValue($identifier, $this->id_type_procedure_pivot, PDO::PARAM_INT);
                        break;
                    case '`id_type_procedure_concession_pivot`':
                        $stmt->bindValue($identifier, $this->id_type_procedure_concession_pivot, PDO::PARAM_INT);
                        break;
                    case '`id_type_contrat_pivot`':
                        $stmt->bindValue($identifier, $this->id_type_contrat_pivot, PDO::PARAM_INT);
                        break;
                    case '`id_type_contrat_concession_pivot`':
                        $stmt->bindValue($identifier, $this->id_type_contrat_concession_pivot, PDO::PARAM_INT);
                        break;
                    case '`montant_subvention_publique`':
                        $stmt->bindValue($identifier, $this->montant_subvention_publique, PDO::PARAM_STR);
                        break;
                    case '`date_debut_execution`':
                        $stmt->bindValue($identifier, $this->date_debut_execution, PDO::PARAM_STR);
                        break;
                    case '`uuid`':
                        $stmt->bindValue($identifier, $this->uuid, PDO::PARAM_STR);
                        break;
                    case '`marche_innovant`':
                        $stmt->bindValue($identifier, (int) $this->marche_innovant, PDO::PARAM_INT);
                        break;
                    case '`service_id`':
                        $stmt->bindValue($identifier, $this->service_id, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdContratTitulaire($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTypeContratConcessionPivot !== null) {
                if (!$this->aCommonTypeContratConcessionPivot->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTypeContratConcessionPivot->getValidationFailures());
                }
            }

            if ($this->aCommonTypeContratPivot !== null) {
                if (!$this->aCommonTypeContratPivot->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTypeContratPivot->getValidationFailures());
                }
            }

            if ($this->aCommonTypeProcedureConcessionPivot !== null) {
                if (!$this->aCommonTypeProcedureConcessionPivot->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTypeProcedureConcessionPivot->getValidationFailures());
                }
            }

            if ($this->aCommonTContactContrat !== null) {
                if (!$this->aCommonTContactContrat->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTContactContrat->getValidationFailures());
                }
            }

            if ($this->aCommonTContratTitulaireRelatedByIdContratMulti !== null) {
                if (!$this->aCommonTContratTitulaireRelatedByIdContratMulti->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTContratTitulaireRelatedByIdContratMulti->getValidationFailures());
                }
            }

            if ($this->aCommonTTypeContrat !== null) {
                if (!$this->aCommonTTypeContrat->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTTypeContrat->getValidationFailures());
                }
            }

            if ($this->aCommonTypeProcedure !== null) {
                if (!$this->aCommonTypeProcedure->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTypeProcedure->getValidationFailures());
                }
            }


            if (($retval = CommonTContratTitulairePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonMarches !== null) {
                    foreach ($this->collCommonMarches as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonDonneesAnnuellesConcessions !== null) {
                    foreach ($this->collCommonDonneesAnnuellesConcessions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonEchangeDocs !== null) {
                    foreach ($this->collCommonEchangeDocs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonModificationContrats !== null) {
                    foreach ($this->collCommonModificationContrats as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTConsLotContrats !== null) {
                    foreach ($this->collCommonTConsLotContrats as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTContratTitulairesRelatedByIdContratTitulaire !== null) {
                    foreach ($this->collCommonTContratTitulairesRelatedByIdContratTitulaire as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTDonneesConsultations !== null) {
                    foreach ($this->collCommonTDonneesConsultations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTContratTitulairePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdContratTitulaire();
                break;
            case 1:
                return $this->getIdTypeContrat();
                break;
            case 2:
                return $this->getIdContactContrat();
                break;
            case 3:
                return $this->getNumeroContrat();
                break;
            case 4:
                return $this->getLienAcSad();
                break;
            case 5:
                return $this->getIdContratMulti();
                break;
            case 6:
                return $this->getOrganisme();
                break;
            case 7:
                return $this->getOldServiceId();
                break;
            case 8:
                return $this->getIdTitulaire();
                break;
            case 9:
                return $this->getIdTitulaireEtab();
                break;
            case 10:
                return $this->getIdOffre();
                break;
            case 11:
                return $this->getTypeDepotReponse();
                break;
            case 12:
                return $this->getObjetContrat();
                break;
            case 13:
                return $this->getIntitule();
                break;
            case 14:
                return $this->getMontantContrat();
                break;
            case 15:
                return $this->getIdTrancheBudgetaire();
                break;
            case 16:
                return $this->getMontantMaxEstime();
                break;
            case 17:
                return $this->getPublicationMontant();
                break;
            case 18:
                return $this->getIdMotifNonPublicationMontant();
                break;
            case 19:
                return $this->getDescMotifNonPublicationMontant();
                break;
            case 20:
                return $this->getPublicationContrat();
                break;
            case 21:
                return $this->getNumEj();
                break;
            case 22:
                return $this->getStatutej();
                break;
            case 23:
                return $this->getNumLongOeap();
                break;
            case 24:
                return $this->getReferenceLibre();
                break;
            case 25:
                return $this->getStatutContrat();
                break;
            case 26:
                return $this->getCategorie();
                break;
            case 27:
                return $this->getCcagApplicable();
                break;
            case 28:
                return $this->getClauseSociale();
                break;
            case 29:
                return $this->getClauseSocialeConditionExecution();
                break;
            case 30:
                return $this->getClauseSocialeInsertion();
                break;
            case 31:
                return $this->getClauseSocialeAteliersProteges();
                break;
            case 32:
                return $this->getClauseSocialeSiae();
                break;
            case 33:
                return $this->getClauseSocialeEss();
                break;
            case 34:
                return $this->getClauseEnvironnementale();
                break;
            case 35:
                return $this->getClauseEnvSpecsTechniques();
                break;
            case 36:
                return $this->getClauseEnvCondExecution();
                break;
            case 37:
                return $this->getClauseEnvCriteresSelect();
                break;
            case 38:
                return $this->getDatePrevueNotification();
                break;
            case 39:
                return $this->getDatePrevueFinContrat();
                break;
            case 40:
                return $this->getDatePrevueMaxFinContrat();
                break;
            case 41:
                return $this->getDateNotification();
                break;
            case 42:
                return $this->getDateFinContrat();
                break;
            case 43:
                return $this->getDateMaxFinContrat();
                break;
            case 44:
                return $this->getDateAttribution();
                break;
            case 45:
                return $this->getDateCreation();
                break;
            case 46:
                return $this->getDateModification();
                break;
            case 47:
                return $this->getEnvoiInterface();
                break;
            case 48:
                return $this->getContratClassKey();
                break;
            case 49:
                return $this->getPmePmi();
                break;
            case 50:
                return $this->getReferenceConsultation();
                break;
            case 51:
                return $this->getHorsPassation();
                break;
            case 52:
                return $this->getIdAgent();
                break;
            case 53:
                return $this->getNomAgent();
                break;
            case 54:
                return $this->getPrenomAgent();
                break;
            case 55:
                return $this->getLieuExecution();
                break;
            case 56:
                return $this->getCodeCpv1();
                break;
            case 57:
                return $this->getCodeCpv2();
                break;
            case 58:
                return $this->getMarcheInsertion();
                break;
            case 59:
                return $this->getClauseSpecificationTechnique();
                break;
            case 60:
                return $this->getProcedurePassationPivot();
                break;
            case 61:
                return $this->getNomLieuPrincipalExecution();
                break;
            case 62:
                return $this->getCodeLieuPrincipalExecution();
                break;
            case 63:
                return $this->getTypeCodeLieuPrincipalExecution();
                break;
            case 64:
                return $this->getDureeInitialeContrat();
                break;
            case 65:
                return $this->getFormePrix();
                break;
            case 66:
                return $this->getDatePublicationInitialeDe();
                break;
            case 67:
                return $this->getNumIdUniqueMarchePublic();
                break;
            case 68:
                return $this->getLibelleTypeContratPivot();
                break;
            case 69:
                return $this->getSiretPaAccordCadre();
                break;
            case 70:
                return $this->getAcMarcheSubsequent();
                break;
            case 71:
                return $this->getLibelleTypeProcedureMpe();
                break;
            case 72:
                return $this->getNbTotalPropositionsLot();
                break;
            case 73:
                return $this->getNbTotalPropositionsDematLot();
                break;
            case 74:
                return $this->getMarcheDefense();
                break;
            case 75:
                return $this->getSiret();
                break;
            case 76:
                return $this->getNomEntiteAcheteur();
                break;
            case 77:
                return $this->getStatutPublicationSn();
                break;
            case 78:
                return $this->getDatePublicationSn();
                break;
            case 79:
                return $this->getErreurSn();
                break;
            case 80:
                return $this->getDateModificationSn();
                break;
            case 81:
                return $this->getIdTypeProcedurePivot();
                break;
            case 82:
                return $this->getIdTypeProcedureConcessionPivot();
                break;
            case 83:
                return $this->getIdTypeContratPivot();
                break;
            case 84:
                return $this->getIdTypeContratConcessionPivot();
                break;
            case 85:
                return $this->getMontantSubventionPublique();
                break;
            case 86:
                return $this->getDateDebutExecution();
                break;
            case 87:
                return $this->getUuid();
                break;
            case 88:
                return $this->getMarcheInnovant();
                break;
            case 89:
                return $this->getServiceId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTContratTitulaire'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTContratTitulaire'][$this->getPrimaryKey()] = true;
        $keys = CommonTContratTitulairePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdContratTitulaire(),
            $keys[1] => $this->getIdTypeContrat(),
            $keys[2] => $this->getIdContactContrat(),
            $keys[3] => $this->getNumeroContrat(),
            $keys[4] => $this->getLienAcSad(),
            $keys[5] => $this->getIdContratMulti(),
            $keys[6] => $this->getOrganisme(),
            $keys[7] => $this->getOldServiceId(),
            $keys[8] => $this->getIdTitulaire(),
            $keys[9] => $this->getIdTitulaireEtab(),
            $keys[10] => $this->getIdOffre(),
            $keys[11] => $this->getTypeDepotReponse(),
            $keys[12] => $this->getObjetContrat(),
            $keys[13] => $this->getIntitule(),
            $keys[14] => $this->getMontantContrat(),
            $keys[15] => $this->getIdTrancheBudgetaire(),
            $keys[16] => $this->getMontantMaxEstime(),
            $keys[17] => $this->getPublicationMontant(),
            $keys[18] => $this->getIdMotifNonPublicationMontant(),
            $keys[19] => $this->getDescMotifNonPublicationMontant(),
            $keys[20] => $this->getPublicationContrat(),
            $keys[21] => $this->getNumEj(),
            $keys[22] => $this->getStatutej(),
            $keys[23] => $this->getNumLongOeap(),
            $keys[24] => $this->getReferenceLibre(),
            $keys[25] => $this->getStatutContrat(),
            $keys[26] => $this->getCategorie(),
            $keys[27] => $this->getCcagApplicable(),
            $keys[28] => $this->getClauseSociale(),
            $keys[29] => $this->getClauseSocialeConditionExecution(),
            $keys[30] => $this->getClauseSocialeInsertion(),
            $keys[31] => $this->getClauseSocialeAteliersProteges(),
            $keys[32] => $this->getClauseSocialeSiae(),
            $keys[33] => $this->getClauseSocialeEss(),
            $keys[34] => $this->getClauseEnvironnementale(),
            $keys[35] => $this->getClauseEnvSpecsTechniques(),
            $keys[36] => $this->getClauseEnvCondExecution(),
            $keys[37] => $this->getClauseEnvCriteresSelect(),
            $keys[38] => $this->getDatePrevueNotification(),
            $keys[39] => $this->getDatePrevueFinContrat(),
            $keys[40] => $this->getDatePrevueMaxFinContrat(),
            $keys[41] => $this->getDateNotification(),
            $keys[42] => $this->getDateFinContrat(),
            $keys[43] => $this->getDateMaxFinContrat(),
            $keys[44] => $this->getDateAttribution(),
            $keys[45] => $this->getDateCreation(),
            $keys[46] => $this->getDateModification(),
            $keys[47] => $this->getEnvoiInterface(),
            $keys[48] => $this->getContratClassKey(),
            $keys[49] => $this->getPmePmi(),
            $keys[50] => $this->getReferenceConsultation(),
            $keys[51] => $this->getHorsPassation(),
            $keys[52] => $this->getIdAgent(),
            $keys[53] => $this->getNomAgent(),
            $keys[54] => $this->getPrenomAgent(),
            $keys[55] => $this->getLieuExecution(),
            $keys[56] => $this->getCodeCpv1(),
            $keys[57] => $this->getCodeCpv2(),
            $keys[58] => $this->getMarcheInsertion(),
            $keys[59] => $this->getClauseSpecificationTechnique(),
            $keys[60] => $this->getProcedurePassationPivot(),
            $keys[61] => $this->getNomLieuPrincipalExecution(),
            $keys[62] => $this->getCodeLieuPrincipalExecution(),
            $keys[63] => $this->getTypeCodeLieuPrincipalExecution(),
            $keys[64] => $this->getDureeInitialeContrat(),
            $keys[65] => $this->getFormePrix(),
            $keys[66] => $this->getDatePublicationInitialeDe(),
            $keys[67] => $this->getNumIdUniqueMarchePublic(),
            $keys[68] => $this->getLibelleTypeContratPivot(),
            $keys[69] => $this->getSiretPaAccordCadre(),
            $keys[70] => $this->getAcMarcheSubsequent(),
            $keys[71] => $this->getLibelleTypeProcedureMpe(),
            $keys[72] => $this->getNbTotalPropositionsLot(),
            $keys[73] => $this->getNbTotalPropositionsDematLot(),
            $keys[74] => $this->getMarcheDefense(),
            $keys[75] => $this->getSiret(),
            $keys[76] => $this->getNomEntiteAcheteur(),
            $keys[77] => $this->getStatutPublicationSn(),
            $keys[78] => $this->getDatePublicationSn(),
            $keys[79] => $this->getErreurSn(),
            $keys[80] => $this->getDateModificationSn(),
            $keys[81] => $this->getIdTypeProcedurePivot(),
            $keys[82] => $this->getIdTypeProcedureConcessionPivot(),
            $keys[83] => $this->getIdTypeContratPivot(),
            $keys[84] => $this->getIdTypeContratConcessionPivot(),
            $keys[85] => $this->getMontantSubventionPublique(),
            $keys[86] => $this->getDateDebutExecution(),
            $keys[87] => $this->getUuid(),
            $keys[88] => $this->getMarcheInnovant(),
            $keys[89] => $this->getServiceId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonTypeContratConcessionPivot) {
                $result['CommonTypeContratConcessionPivot'] = $this->aCommonTypeContratConcessionPivot->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTypeContratPivot) {
                $result['CommonTypeContratPivot'] = $this->aCommonTypeContratPivot->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTypeProcedureConcessionPivot) {
                $result['CommonTypeProcedureConcessionPivot'] = $this->aCommonTypeProcedureConcessionPivot->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTContactContrat) {
                $result['CommonTContactContrat'] = $this->aCommonTContactContrat->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTContratTitulaireRelatedByIdContratMulti) {
                $result['CommonTContratTitulaireRelatedByIdContratMulti'] = $this->aCommonTContratTitulaireRelatedByIdContratMulti->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTTypeContrat) {
                $result['CommonTTypeContrat'] = $this->aCommonTTypeContrat->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTypeProcedure) {
                $result['CommonTypeProcedure'] = $this->aCommonTypeProcedure->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonMarches) {
                $result['CommonMarches'] = $this->collCommonMarches->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonDonneesAnnuellesConcessions) {
                $result['CommonDonneesAnnuellesConcessions'] = $this->collCommonDonneesAnnuellesConcessions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonEchangeDocs) {
                $result['CommonEchangeDocs'] = $this->collCommonEchangeDocs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonModificationContrats) {
                $result['CommonModificationContrats'] = $this->collCommonModificationContrats->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTConsLotContrats) {
                $result['CommonTConsLotContrats'] = $this->collCommonTConsLotContrats->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTContratTitulairesRelatedByIdContratTitulaire) {
                $result['CommonTContratTitulairesRelatedByIdContratTitulaire'] = $this->collCommonTContratTitulairesRelatedByIdContratTitulaire->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTDonneesConsultations) {
                $result['CommonTDonneesConsultations'] = $this->collCommonTDonneesConsultations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTContratTitulairePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdContratTitulaire($value);
                break;
            case 1:
                $this->setIdTypeContrat($value);
                break;
            case 2:
                $this->setIdContactContrat($value);
                break;
            case 3:
                $this->setNumeroContrat($value);
                break;
            case 4:
                $this->setLienAcSad($value);
                break;
            case 5:
                $this->setIdContratMulti($value);
                break;
            case 6:
                $this->setOrganisme($value);
                break;
            case 7:
                $this->setOldServiceId($value);
                break;
            case 8:
                $this->setIdTitulaire($value);
                break;
            case 9:
                $this->setIdTitulaireEtab($value);
                break;
            case 10:
                $this->setIdOffre($value);
                break;
            case 11:
                $this->setTypeDepotReponse($value);
                break;
            case 12:
                $this->setObjetContrat($value);
                break;
            case 13:
                $this->setIntitule($value);
                break;
            case 14:
                $this->setMontantContrat($value);
                break;
            case 15:
                $this->setIdTrancheBudgetaire($value);
                break;
            case 16:
                $this->setMontantMaxEstime($value);
                break;
            case 17:
                $this->setPublicationMontant($value);
                break;
            case 18:
                $this->setIdMotifNonPublicationMontant($value);
                break;
            case 19:
                $this->setDescMotifNonPublicationMontant($value);
                break;
            case 20:
                $this->setPublicationContrat($value);
                break;
            case 21:
                $this->setNumEj($value);
                break;
            case 22:
                $this->setStatutej($value);
                break;
            case 23:
                $this->setNumLongOeap($value);
                break;
            case 24:
                $this->setReferenceLibre($value);
                break;
            case 25:
                $this->setStatutContrat($value);
                break;
            case 26:
                $this->setCategorie($value);
                break;
            case 27:
                $this->setCcagApplicable($value);
                break;
            case 28:
                $this->setClauseSociale($value);
                break;
            case 29:
                $this->setClauseSocialeConditionExecution($value);
                break;
            case 30:
                $this->setClauseSocialeInsertion($value);
                break;
            case 31:
                $this->setClauseSocialeAteliersProteges($value);
                break;
            case 32:
                $this->setClauseSocialeSiae($value);
                break;
            case 33:
                $this->setClauseSocialeEss($value);
                break;
            case 34:
                $this->setClauseEnvironnementale($value);
                break;
            case 35:
                $this->setClauseEnvSpecsTechniques($value);
                break;
            case 36:
                $this->setClauseEnvCondExecution($value);
                break;
            case 37:
                $this->setClauseEnvCriteresSelect($value);
                break;
            case 38:
                $this->setDatePrevueNotification($value);
                break;
            case 39:
                $this->setDatePrevueFinContrat($value);
                break;
            case 40:
                $this->setDatePrevueMaxFinContrat($value);
                break;
            case 41:
                $this->setDateNotification($value);
                break;
            case 42:
                $this->setDateFinContrat($value);
                break;
            case 43:
                $this->setDateMaxFinContrat($value);
                break;
            case 44:
                $this->setDateAttribution($value);
                break;
            case 45:
                $this->setDateCreation($value);
                break;
            case 46:
                $this->setDateModification($value);
                break;
            case 47:
                $this->setEnvoiInterface($value);
                break;
            case 48:
                $this->setContratClassKey($value);
                break;
            case 49:
                $this->setPmePmi($value);
                break;
            case 50:
                $this->setReferenceConsultation($value);
                break;
            case 51:
                $this->setHorsPassation($value);
                break;
            case 52:
                $this->setIdAgent($value);
                break;
            case 53:
                $this->setNomAgent($value);
                break;
            case 54:
                $this->setPrenomAgent($value);
                break;
            case 55:
                $this->setLieuExecution($value);
                break;
            case 56:
                $this->setCodeCpv1($value);
                break;
            case 57:
                $this->setCodeCpv2($value);
                break;
            case 58:
                $this->setMarcheInsertion($value);
                break;
            case 59:
                $this->setClauseSpecificationTechnique($value);
                break;
            case 60:
                $this->setProcedurePassationPivot($value);
                break;
            case 61:
                $this->setNomLieuPrincipalExecution($value);
                break;
            case 62:
                $this->setCodeLieuPrincipalExecution($value);
                break;
            case 63:
                $this->setTypeCodeLieuPrincipalExecution($value);
                break;
            case 64:
                $this->setDureeInitialeContrat($value);
                break;
            case 65:
                $this->setFormePrix($value);
                break;
            case 66:
                $this->setDatePublicationInitialeDe($value);
                break;
            case 67:
                $this->setNumIdUniqueMarchePublic($value);
                break;
            case 68:
                $this->setLibelleTypeContratPivot($value);
                break;
            case 69:
                $this->setSiretPaAccordCadre($value);
                break;
            case 70:
                $this->setAcMarcheSubsequent($value);
                break;
            case 71:
                $this->setLibelleTypeProcedureMpe($value);
                break;
            case 72:
                $this->setNbTotalPropositionsLot($value);
                break;
            case 73:
                $this->setNbTotalPropositionsDematLot($value);
                break;
            case 74:
                $this->setMarcheDefense($value);
                break;
            case 75:
                $this->setSiret($value);
                break;
            case 76:
                $this->setNomEntiteAcheteur($value);
                break;
            case 77:
                $this->setStatutPublicationSn($value);
                break;
            case 78:
                $this->setDatePublicationSn($value);
                break;
            case 79:
                $this->setErreurSn($value);
                break;
            case 80:
                $this->setDateModificationSn($value);
                break;
            case 81:
                $this->setIdTypeProcedurePivot($value);
                break;
            case 82:
                $this->setIdTypeProcedureConcessionPivot($value);
                break;
            case 83:
                $this->setIdTypeContratPivot($value);
                break;
            case 84:
                $this->setIdTypeContratConcessionPivot($value);
                break;
            case 85:
                $this->setMontantSubventionPublique($value);
                break;
            case 86:
                $this->setDateDebutExecution($value);
                break;
            case 87:
                $this->setUuid($value);
                break;
            case 88:
                $this->setMarcheInnovant($value);
                break;
            case 89:
                $this->setServiceId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTContratTitulairePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdContratTitulaire($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdTypeContrat($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdContactContrat($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNumeroContrat($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setLienAcSad($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setIdContratMulti($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setOrganisme($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setOldServiceId($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setIdTitulaire($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setIdTitulaireEtab($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setIdOffre($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setTypeDepotReponse($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setObjetContrat($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setIntitule($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setMontantContrat($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setIdTrancheBudgetaire($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setMontantMaxEstime($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setPublicationMontant($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setIdMotifNonPublicationMontant($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setDescMotifNonPublicationMontant($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setPublicationContrat($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setNumEj($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setStatutej($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setNumLongOeap($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setReferenceLibre($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setStatutContrat($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setCategorie($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setCcagApplicable($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setClauseSociale($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setClauseSocialeConditionExecution($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setClauseSocialeInsertion($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setClauseSocialeAteliersProteges($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setClauseSocialeSiae($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setClauseSocialeEss($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setClauseEnvironnementale($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setClauseEnvSpecsTechniques($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setClauseEnvCondExecution($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setClauseEnvCriteresSelect($arr[$keys[37]]);
        if (array_key_exists($keys[38], $arr)) $this->setDatePrevueNotification($arr[$keys[38]]);
        if (array_key_exists($keys[39], $arr)) $this->setDatePrevueFinContrat($arr[$keys[39]]);
        if (array_key_exists($keys[40], $arr)) $this->setDatePrevueMaxFinContrat($arr[$keys[40]]);
        if (array_key_exists($keys[41], $arr)) $this->setDateNotification($arr[$keys[41]]);
        if (array_key_exists($keys[42], $arr)) $this->setDateFinContrat($arr[$keys[42]]);
        if (array_key_exists($keys[43], $arr)) $this->setDateMaxFinContrat($arr[$keys[43]]);
        if (array_key_exists($keys[44], $arr)) $this->setDateAttribution($arr[$keys[44]]);
        if (array_key_exists($keys[45], $arr)) $this->setDateCreation($arr[$keys[45]]);
        if (array_key_exists($keys[46], $arr)) $this->setDateModification($arr[$keys[46]]);
        if (array_key_exists($keys[47], $arr)) $this->setEnvoiInterface($arr[$keys[47]]);
        if (array_key_exists($keys[48], $arr)) $this->setContratClassKey($arr[$keys[48]]);
        if (array_key_exists($keys[49], $arr)) $this->setPmePmi($arr[$keys[49]]);
        if (array_key_exists($keys[50], $arr)) $this->setReferenceConsultation($arr[$keys[50]]);
        if (array_key_exists($keys[51], $arr)) $this->setHorsPassation($arr[$keys[51]]);
        if (array_key_exists($keys[52], $arr)) $this->setIdAgent($arr[$keys[52]]);
        if (array_key_exists($keys[53], $arr)) $this->setNomAgent($arr[$keys[53]]);
        if (array_key_exists($keys[54], $arr)) $this->setPrenomAgent($arr[$keys[54]]);
        if (array_key_exists($keys[55], $arr)) $this->setLieuExecution($arr[$keys[55]]);
        if (array_key_exists($keys[56], $arr)) $this->setCodeCpv1($arr[$keys[56]]);
        if (array_key_exists($keys[57], $arr)) $this->setCodeCpv2($arr[$keys[57]]);
        if (array_key_exists($keys[58], $arr)) $this->setMarcheInsertion($arr[$keys[58]]);
        if (array_key_exists($keys[59], $arr)) $this->setClauseSpecificationTechnique($arr[$keys[59]]);
        if (array_key_exists($keys[60], $arr)) $this->setProcedurePassationPivot($arr[$keys[60]]);
        if (array_key_exists($keys[61], $arr)) $this->setNomLieuPrincipalExecution($arr[$keys[61]]);
        if (array_key_exists($keys[62], $arr)) $this->setCodeLieuPrincipalExecution($arr[$keys[62]]);
        if (array_key_exists($keys[63], $arr)) $this->setTypeCodeLieuPrincipalExecution($arr[$keys[63]]);
        if (array_key_exists($keys[64], $arr)) $this->setDureeInitialeContrat($arr[$keys[64]]);
        if (array_key_exists($keys[65], $arr)) $this->setFormePrix($arr[$keys[65]]);
        if (array_key_exists($keys[66], $arr)) $this->setDatePublicationInitialeDe($arr[$keys[66]]);
        if (array_key_exists($keys[67], $arr)) $this->setNumIdUniqueMarchePublic($arr[$keys[67]]);
        if (array_key_exists($keys[68], $arr)) $this->setLibelleTypeContratPivot($arr[$keys[68]]);
        if (array_key_exists($keys[69], $arr)) $this->setSiretPaAccordCadre($arr[$keys[69]]);
        if (array_key_exists($keys[70], $arr)) $this->setAcMarcheSubsequent($arr[$keys[70]]);
        if (array_key_exists($keys[71], $arr)) $this->setLibelleTypeProcedureMpe($arr[$keys[71]]);
        if (array_key_exists($keys[72], $arr)) $this->setNbTotalPropositionsLot($arr[$keys[72]]);
        if (array_key_exists($keys[73], $arr)) $this->setNbTotalPropositionsDematLot($arr[$keys[73]]);
        if (array_key_exists($keys[74], $arr)) $this->setMarcheDefense($arr[$keys[74]]);
        if (array_key_exists($keys[75], $arr)) $this->setSiret($arr[$keys[75]]);
        if (array_key_exists($keys[76], $arr)) $this->setNomEntiteAcheteur($arr[$keys[76]]);
        if (array_key_exists($keys[77], $arr)) $this->setStatutPublicationSn($arr[$keys[77]]);
        if (array_key_exists($keys[78], $arr)) $this->setDatePublicationSn($arr[$keys[78]]);
        if (array_key_exists($keys[79], $arr)) $this->setErreurSn($arr[$keys[79]]);
        if (array_key_exists($keys[80], $arr)) $this->setDateModificationSn($arr[$keys[80]]);
        if (array_key_exists($keys[81], $arr)) $this->setIdTypeProcedurePivot($arr[$keys[81]]);
        if (array_key_exists($keys[82], $arr)) $this->setIdTypeProcedureConcessionPivot($arr[$keys[82]]);
        if (array_key_exists($keys[83], $arr)) $this->setIdTypeContratPivot($arr[$keys[83]]);
        if (array_key_exists($keys[84], $arr)) $this->setIdTypeContratConcessionPivot($arr[$keys[84]]);
        if (array_key_exists($keys[85], $arr)) $this->setMontantSubventionPublique($arr[$keys[85]]);
        if (array_key_exists($keys[86], $arr)) $this->setDateDebutExecution($arr[$keys[86]]);
        if (array_key_exists($keys[87], $arr)) $this->setUuid($arr[$keys[87]]);
        if (array_key_exists($keys[88], $arr)) $this->setMarcheInnovant($arr[$keys[88]]);
        if (array_key_exists($keys[89], $arr)) $this->setServiceId($arr[$keys[89]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTContratTitulairePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE)) $criteria->add(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $this->id_contrat_titulaire);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TYPE_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::ID_TYPE_CONTRAT, $this->id_type_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::ID_CONTACT_CONTRAT, $this->id_contact_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::NUMERO_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::NUMERO_CONTRAT, $this->numero_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::LIEN_AC_SAD)) $criteria->add(CommonTContratTitulairePeer::LIEN_AC_SAD, $this->lien_ac_sad);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_CONTRAT_MULTI)) $criteria->add(CommonTContratTitulairePeer::ID_CONTRAT_MULTI, $this->id_contrat_multi);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ORGANISME)) $criteria->add(CommonTContratTitulairePeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonTContratTitulairePeer::OLD_SERVICE_ID)) $criteria->add(CommonTContratTitulairePeer::OLD_SERVICE_ID, $this->old_service_id);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TITULAIRE)) $criteria->add(CommonTContratTitulairePeer::ID_TITULAIRE, $this->id_titulaire);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TITULAIRE_ETAB)) $criteria->add(CommonTContratTitulairePeer::ID_TITULAIRE_ETAB, $this->id_titulaire_etab);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_OFFRE)) $criteria->add(CommonTContratTitulairePeer::ID_OFFRE, $this->id_offre);
        if ($this->isColumnModified(CommonTContratTitulairePeer::TYPE_DEPOT_REPONSE)) $criteria->add(CommonTContratTitulairePeer::TYPE_DEPOT_REPONSE, $this->type_depot_reponse);
        if ($this->isColumnModified(CommonTContratTitulairePeer::OBJET_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::OBJET_CONTRAT, $this->objet_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::INTITULE)) $criteria->add(CommonTContratTitulairePeer::INTITULE, $this->intitule);
        if ($this->isColumnModified(CommonTContratTitulairePeer::MONTANT_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::MONTANT_CONTRAT, $this->montant_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TRANCHE_BUDGETAIRE)) $criteria->add(CommonTContratTitulairePeer::ID_TRANCHE_BUDGETAIRE, $this->id_tranche_budgetaire);
        if ($this->isColumnModified(CommonTContratTitulairePeer::MONTANT_MAX_ESTIME)) $criteria->add(CommonTContratTitulairePeer::MONTANT_MAX_ESTIME, $this->montant_max_estime);
        if ($this->isColumnModified(CommonTContratTitulairePeer::PUBLICATION_MONTANT)) $criteria->add(CommonTContratTitulairePeer::PUBLICATION_MONTANT, $this->publication_montant);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_MOTIF_NON_PUBLICATION_MONTANT)) $criteria->add(CommonTContratTitulairePeer::ID_MOTIF_NON_PUBLICATION_MONTANT, $this->id_motif_non_publication_montant);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DESC_MOTIF_NON_PUBLICATION_MONTANT)) $criteria->add(CommonTContratTitulairePeer::DESC_MOTIF_NON_PUBLICATION_MONTANT, $this->desc_motif_non_publication_montant);
        if ($this->isColumnModified(CommonTContratTitulairePeer::PUBLICATION_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::PUBLICATION_CONTRAT, $this->publication_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::NUM_EJ)) $criteria->add(CommonTContratTitulairePeer::NUM_EJ, $this->num_ej);
        if ($this->isColumnModified(CommonTContratTitulairePeer::STATUTEJ)) $criteria->add(CommonTContratTitulairePeer::STATUTEJ, $this->statutej);
        if ($this->isColumnModified(CommonTContratTitulairePeer::NUM_LONG_OEAP)) $criteria->add(CommonTContratTitulairePeer::NUM_LONG_OEAP, $this->num_long_oeap);
        if ($this->isColumnModified(CommonTContratTitulairePeer::REFERENCE_LIBRE)) $criteria->add(CommonTContratTitulairePeer::REFERENCE_LIBRE, $this->reference_libre);
        if ($this->isColumnModified(CommonTContratTitulairePeer::STATUT_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::STATUT_CONTRAT, $this->statut_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CATEGORIE)) $criteria->add(CommonTContratTitulairePeer::CATEGORIE, $this->categorie);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CCAG_APPLICABLE)) $criteria->add(CommonTContratTitulairePeer::CCAG_APPLICABLE, $this->ccag_applicable);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE)) $criteria->add(CommonTContratTitulairePeer::CLAUSE_SOCIALE, $this->clause_sociale);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE_CONDITION_EXECUTION)) $criteria->add(CommonTContratTitulairePeer::CLAUSE_SOCIALE_CONDITION_EXECUTION, $this->clause_sociale_condition_execution);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE_INSERTION)) $criteria->add(CommonTContratTitulairePeer::CLAUSE_SOCIALE_INSERTION, $this->clause_sociale_insertion);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE_ATELIERS_PROTEGES)) $criteria->add(CommonTContratTitulairePeer::CLAUSE_SOCIALE_ATELIERS_PROTEGES, $this->clause_sociale_ateliers_proteges);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE_SIAE)) $criteria->add(CommonTContratTitulairePeer::CLAUSE_SOCIALE_SIAE, $this->clause_sociale_siae);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SOCIALE_ESS)) $criteria->add(CommonTContratTitulairePeer::CLAUSE_SOCIALE_ESS, $this->clause_sociale_ess);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_ENVIRONNEMENTALE)) $criteria->add(CommonTContratTitulairePeer::CLAUSE_ENVIRONNEMENTALE, $this->clause_environnementale);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_ENV_SPECS_TECHNIQUES)) $criteria->add(CommonTContratTitulairePeer::CLAUSE_ENV_SPECS_TECHNIQUES, $this->clause_env_specs_techniques);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_ENV_COND_EXECUTION)) $criteria->add(CommonTContratTitulairePeer::CLAUSE_ENV_COND_EXECUTION, $this->clause_env_cond_execution);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_ENV_CRITERES_SELECT)) $criteria->add(CommonTContratTitulairePeer::CLAUSE_ENV_CRITERES_SELECT, $this->clause_env_criteres_select);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION)) $criteria->add(CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION, $this->date_prevue_notification);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_PREVUE_FIN_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::DATE_PREVUE_FIN_CONTRAT, $this->date_prevue_fin_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_PREVUE_MAX_FIN_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::DATE_PREVUE_MAX_FIN_CONTRAT, $this->date_prevue_max_fin_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_NOTIFICATION)) $criteria->add(CommonTContratTitulairePeer::DATE_NOTIFICATION, $this->date_notification);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_FIN_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::DATE_FIN_CONTRAT, $this->date_fin_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_MAX_FIN_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::DATE_MAX_FIN_CONTRAT, $this->date_max_fin_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_ATTRIBUTION)) $criteria->add(CommonTContratTitulairePeer::DATE_ATTRIBUTION, $this->date_attribution);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_CREATION)) $criteria->add(CommonTContratTitulairePeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_MODIFICATION)) $criteria->add(CommonTContratTitulairePeer::DATE_MODIFICATION, $this->date_modification);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ENVOI_INTERFACE)) $criteria->add(CommonTContratTitulairePeer::ENVOI_INTERFACE, $this->envoi_interface);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CONTRAT_CLASS_KEY)) $criteria->add(CommonTContratTitulairePeer::CONTRAT_CLASS_KEY, $this->contrat_class_key);
        if ($this->isColumnModified(CommonTContratTitulairePeer::PME_PMI)) $criteria->add(CommonTContratTitulairePeer::PME_PMI, $this->pme_pmi);
        if ($this->isColumnModified(CommonTContratTitulairePeer::REFERENCE_CONSULTATION)) $criteria->add(CommonTContratTitulairePeer::REFERENCE_CONSULTATION, $this->reference_consultation);
        if ($this->isColumnModified(CommonTContratTitulairePeer::HORS_PASSATION)) $criteria->add(CommonTContratTitulairePeer::HORS_PASSATION, $this->hors_passation);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_AGENT)) $criteria->add(CommonTContratTitulairePeer::ID_AGENT, $this->id_agent);
        if ($this->isColumnModified(CommonTContratTitulairePeer::NOM_AGENT)) $criteria->add(CommonTContratTitulairePeer::NOM_AGENT, $this->nom_agent);
        if ($this->isColumnModified(CommonTContratTitulairePeer::PRENOM_AGENT)) $criteria->add(CommonTContratTitulairePeer::PRENOM_AGENT, $this->prenom_agent);
        if ($this->isColumnModified(CommonTContratTitulairePeer::LIEU_EXECUTION)) $criteria->add(CommonTContratTitulairePeer::LIEU_EXECUTION, $this->lieu_execution);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CODE_CPV_1)) $criteria->add(CommonTContratTitulairePeer::CODE_CPV_1, $this->code_cpv_1);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CODE_CPV_2)) $criteria->add(CommonTContratTitulairePeer::CODE_CPV_2, $this->code_cpv_2);
        if ($this->isColumnModified(CommonTContratTitulairePeer::MARCHE_INSERTION)) $criteria->add(CommonTContratTitulairePeer::MARCHE_INSERTION, $this->marche_insertion);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CLAUSE_SPECIFICATION_TECHNIQUE)) $criteria->add(CommonTContratTitulairePeer::CLAUSE_SPECIFICATION_TECHNIQUE, $this->clause_specification_technique);
        if ($this->isColumnModified(CommonTContratTitulairePeer::PROCEDURE_PASSATION_PIVOT)) $criteria->add(CommonTContratTitulairePeer::PROCEDURE_PASSATION_PIVOT, $this->procedure_passation_pivot);
        if ($this->isColumnModified(CommonTContratTitulairePeer::NOM_LIEU_PRINCIPAL_EXECUTION)) $criteria->add(CommonTContratTitulairePeer::NOM_LIEU_PRINCIPAL_EXECUTION, $this->nom_lieu_principal_execution);
        if ($this->isColumnModified(CommonTContratTitulairePeer::CODE_LIEU_PRINCIPAL_EXECUTION)) $criteria->add(CommonTContratTitulairePeer::CODE_LIEU_PRINCIPAL_EXECUTION, $this->code_lieu_principal_execution);
        if ($this->isColumnModified(CommonTContratTitulairePeer::TYPE_CODE_LIEU_PRINCIPAL_EXECUTION)) $criteria->add(CommonTContratTitulairePeer::TYPE_CODE_LIEU_PRINCIPAL_EXECUTION, $this->type_code_lieu_principal_execution);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DUREE_INITIALE_CONTRAT)) $criteria->add(CommonTContratTitulairePeer::DUREE_INITIALE_CONTRAT, $this->duree_initiale_contrat);
        if ($this->isColumnModified(CommonTContratTitulairePeer::FORME_PRIX)) $criteria->add(CommonTContratTitulairePeer::FORME_PRIX, $this->forme_prix);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_PUBLICATION_INITIALE_DE)) $criteria->add(CommonTContratTitulairePeer::DATE_PUBLICATION_INITIALE_DE, $this->date_publication_initiale_de);
        if ($this->isColumnModified(CommonTContratTitulairePeer::NUM_ID_UNIQUE_MARCHE_PUBLIC)) $criteria->add(CommonTContratTitulairePeer::NUM_ID_UNIQUE_MARCHE_PUBLIC, $this->num_id_unique_marche_public);
        if ($this->isColumnModified(CommonTContratTitulairePeer::LIBELLE_TYPE_CONTRAT_PIVOT)) $criteria->add(CommonTContratTitulairePeer::LIBELLE_TYPE_CONTRAT_PIVOT, $this->libelle_type_contrat_pivot);
        if ($this->isColumnModified(CommonTContratTitulairePeer::SIRET_PA_ACCORD_CADRE)) $criteria->add(CommonTContratTitulairePeer::SIRET_PA_ACCORD_CADRE, $this->siret_pa_accord_cadre);
        if ($this->isColumnModified(CommonTContratTitulairePeer::AC_MARCHE_SUBSEQUENT)) $criteria->add(CommonTContratTitulairePeer::AC_MARCHE_SUBSEQUENT, $this->ac_marche_subsequent);
        if ($this->isColumnModified(CommonTContratTitulairePeer::LIBELLE_TYPE_PROCEDURE_MPE)) $criteria->add(CommonTContratTitulairePeer::LIBELLE_TYPE_PROCEDURE_MPE, $this->libelle_type_procedure_mpe);
        if ($this->isColumnModified(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_LOT)) $criteria->add(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_LOT, $this->nb_total_propositions_lot);
        if ($this->isColumnModified(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_DEMAT_LOT)) $criteria->add(CommonTContratTitulairePeer::NB_TOTAL_PROPOSITIONS_DEMAT_LOT, $this->nb_total_propositions_demat_lot);
        if ($this->isColumnModified(CommonTContratTitulairePeer::MARCHE_DEFENSE)) $criteria->add(CommonTContratTitulairePeer::MARCHE_DEFENSE, $this->marche_defense);
        if ($this->isColumnModified(CommonTContratTitulairePeer::SIRET)) $criteria->add(CommonTContratTitulairePeer::SIRET, $this->siret);
        if ($this->isColumnModified(CommonTContratTitulairePeer::NOM_ENTITE_ACHETEUR)) $criteria->add(CommonTContratTitulairePeer::NOM_ENTITE_ACHETEUR, $this->nom_entite_acheteur);
        if ($this->isColumnModified(CommonTContratTitulairePeer::STATUT_PUBLICATION_SN)) $criteria->add(CommonTContratTitulairePeer::STATUT_PUBLICATION_SN, $this->statut_publication_sn);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_PUBLICATION_SN)) $criteria->add(CommonTContratTitulairePeer::DATE_PUBLICATION_SN, $this->date_publication_sn);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ERREUR_SN)) $criteria->add(CommonTContratTitulairePeer::ERREUR_SN, $this->erreur_sn);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_MODIFICATION_SN)) $criteria->add(CommonTContratTitulairePeer::DATE_MODIFICATION_SN, $this->date_modification_sn);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT)) $criteria->add(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_PIVOT, $this->id_type_procedure_pivot);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT)) $criteria->add(CommonTContratTitulairePeer::ID_TYPE_PROCEDURE_CONCESSION_PIVOT, $this->id_type_procedure_concession_pivot);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT)) $criteria->add(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_PIVOT, $this->id_type_contrat_pivot);
        if ($this->isColumnModified(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT)) $criteria->add(CommonTContratTitulairePeer::ID_TYPE_CONTRAT_CONCESSION_PIVOT, $this->id_type_contrat_concession_pivot);
        if ($this->isColumnModified(CommonTContratTitulairePeer::MONTANT_SUBVENTION_PUBLIQUE)) $criteria->add(CommonTContratTitulairePeer::MONTANT_SUBVENTION_PUBLIQUE, $this->montant_subvention_publique);
        if ($this->isColumnModified(CommonTContratTitulairePeer::DATE_DEBUT_EXECUTION)) $criteria->add(CommonTContratTitulairePeer::DATE_DEBUT_EXECUTION, $this->date_debut_execution);
        if ($this->isColumnModified(CommonTContratTitulairePeer::UUID)) $criteria->add(CommonTContratTitulairePeer::UUID, $this->uuid);
        if ($this->isColumnModified(CommonTContratTitulairePeer::MARCHE_INNOVANT)) $criteria->add(CommonTContratTitulairePeer::MARCHE_INNOVANT, $this->marche_innovant);
        if ($this->isColumnModified(CommonTContratTitulairePeer::SERVICE_ID)) $criteria->add(CommonTContratTitulairePeer::SERVICE_ID, $this->service_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTContratTitulairePeer::DATABASE_NAME);
        $criteria->add(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $this->id_contrat_titulaire);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdContratTitulaire();
    }

    /**
     * Generic method to set the primary key (id_contrat_titulaire column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdContratTitulaire($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdContratTitulaire();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTContratTitulaire (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdTypeContrat($this->getIdTypeContrat());
        $copyObj->setIdContactContrat($this->getIdContactContrat());
        $copyObj->setNumeroContrat($this->getNumeroContrat());
        $copyObj->setLienAcSad($this->getLienAcSad());
        $copyObj->setIdContratMulti($this->getIdContratMulti());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setOldServiceId($this->getOldServiceId());
        $copyObj->setIdTitulaire($this->getIdTitulaire());
        $copyObj->setIdTitulaireEtab($this->getIdTitulaireEtab());
        $copyObj->setIdOffre($this->getIdOffre());
        $copyObj->setTypeDepotReponse($this->getTypeDepotReponse());
        $copyObj->setObjetContrat($this->getObjetContrat());
        $copyObj->setIntitule($this->getIntitule());
        $copyObj->setMontantContrat($this->getMontantContrat());
        $copyObj->setIdTrancheBudgetaire($this->getIdTrancheBudgetaire());
        $copyObj->setMontantMaxEstime($this->getMontantMaxEstime());
        $copyObj->setPublicationMontant($this->getPublicationMontant());
        $copyObj->setIdMotifNonPublicationMontant($this->getIdMotifNonPublicationMontant());
        $copyObj->setDescMotifNonPublicationMontant($this->getDescMotifNonPublicationMontant());
        $copyObj->setPublicationContrat($this->getPublicationContrat());
        $copyObj->setNumEj($this->getNumEj());
        $copyObj->setStatutej($this->getStatutej());
        $copyObj->setNumLongOeap($this->getNumLongOeap());
        $copyObj->setReferenceLibre($this->getReferenceLibre());
        $copyObj->setStatutContrat($this->getStatutContrat());
        $copyObj->setCategorie($this->getCategorie());
        $copyObj->setCcagApplicable($this->getCcagApplicable());
        $copyObj->setClauseSociale($this->getClauseSociale());
        $copyObj->setClauseSocialeConditionExecution($this->getClauseSocialeConditionExecution());
        $copyObj->setClauseSocialeInsertion($this->getClauseSocialeInsertion());
        $copyObj->setClauseSocialeAteliersProteges($this->getClauseSocialeAteliersProteges());
        $copyObj->setClauseSocialeSiae($this->getClauseSocialeSiae());
        $copyObj->setClauseSocialeEss($this->getClauseSocialeEss());
        $copyObj->setClauseEnvironnementale($this->getClauseEnvironnementale());
        $copyObj->setClauseEnvSpecsTechniques($this->getClauseEnvSpecsTechniques());
        $copyObj->setClauseEnvCondExecution($this->getClauseEnvCondExecution());
        $copyObj->setClauseEnvCriteresSelect($this->getClauseEnvCriteresSelect());
        $copyObj->setDatePrevueNotification($this->getDatePrevueNotification());
        $copyObj->setDatePrevueFinContrat($this->getDatePrevueFinContrat());
        $copyObj->setDatePrevueMaxFinContrat($this->getDatePrevueMaxFinContrat());
        $copyObj->setDateNotification($this->getDateNotification());
        $copyObj->setDateFinContrat($this->getDateFinContrat());
        $copyObj->setDateMaxFinContrat($this->getDateMaxFinContrat());
        $copyObj->setDateAttribution($this->getDateAttribution());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateModification($this->getDateModification());
        $copyObj->setEnvoiInterface($this->getEnvoiInterface());
        $copyObj->setContratClassKey($this->getContratClassKey());
        $copyObj->setPmePmi($this->getPmePmi());
        $copyObj->setReferenceConsultation($this->getReferenceConsultation());
        $copyObj->setHorsPassation($this->getHorsPassation());
        $copyObj->setIdAgent($this->getIdAgent());
        $copyObj->setNomAgent($this->getNomAgent());
        $copyObj->setPrenomAgent($this->getPrenomAgent());
        $copyObj->setLieuExecution($this->getLieuExecution());
        $copyObj->setCodeCpv1($this->getCodeCpv1());
        $copyObj->setCodeCpv2($this->getCodeCpv2());
        $copyObj->setMarcheInsertion($this->getMarcheInsertion());
        $copyObj->setClauseSpecificationTechnique($this->getClauseSpecificationTechnique());
        $copyObj->setProcedurePassationPivot($this->getProcedurePassationPivot());
        $copyObj->setNomLieuPrincipalExecution($this->getNomLieuPrincipalExecution());
        $copyObj->setCodeLieuPrincipalExecution($this->getCodeLieuPrincipalExecution());
        $copyObj->setTypeCodeLieuPrincipalExecution($this->getTypeCodeLieuPrincipalExecution());
        $copyObj->setDureeInitialeContrat($this->getDureeInitialeContrat());
        $copyObj->setFormePrix($this->getFormePrix());
        $copyObj->setDatePublicationInitialeDe($this->getDatePublicationInitialeDe());
        $copyObj->setNumIdUniqueMarchePublic($this->getNumIdUniqueMarchePublic());
        $copyObj->setLibelleTypeContratPivot($this->getLibelleTypeContratPivot());
        $copyObj->setSiretPaAccordCadre($this->getSiretPaAccordCadre());
        $copyObj->setAcMarcheSubsequent($this->getAcMarcheSubsequent());
        $copyObj->setLibelleTypeProcedureMpe($this->getLibelleTypeProcedureMpe());
        $copyObj->setNbTotalPropositionsLot($this->getNbTotalPropositionsLot());
        $copyObj->setNbTotalPropositionsDematLot($this->getNbTotalPropositionsDematLot());
        $copyObj->setMarcheDefense($this->getMarcheDefense());
        $copyObj->setSiret($this->getSiret());
        $copyObj->setNomEntiteAcheteur($this->getNomEntiteAcheteur());
        $copyObj->setStatutPublicationSn($this->getStatutPublicationSn());
        $copyObj->setDatePublicationSn($this->getDatePublicationSn());
        $copyObj->setErreurSn($this->getErreurSn());
        $copyObj->setDateModificationSn($this->getDateModificationSn());
        $copyObj->setIdTypeProcedurePivot($this->getIdTypeProcedurePivot());
        $copyObj->setIdTypeProcedureConcessionPivot($this->getIdTypeProcedureConcessionPivot());
        $copyObj->setIdTypeContratPivot($this->getIdTypeContratPivot());
        $copyObj->setIdTypeContratConcessionPivot($this->getIdTypeContratConcessionPivot());
        $copyObj->setMontantSubventionPublique($this->getMontantSubventionPublique());
        $copyObj->setDateDebutExecution($this->getDateDebutExecution());
        $copyObj->setUuid($this->getUuid());
        $copyObj->setMarcheInnovant($this->getMarcheInnovant());
        $copyObj->setServiceId($this->getServiceId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonMarches() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonMarche($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonDonneesAnnuellesConcessions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonDonneesAnnuellesConcession($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonEchangeDocs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchangeDoc($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonModificationContrats() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonModificationContrat($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTConsLotContrats() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTConsLotContrat($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTContratTitulairesRelatedByIdContratTitulaire() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTContratTitulaireRelatedByIdContratTitulaire($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTDonneesConsultations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTDonneesConsultation($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdContratTitulaire(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTContratTitulaire Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTContratTitulairePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTContratTitulairePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonTypeContratConcessionPivot object.
     *
     * @param   CommonTypeContratConcessionPivot $v
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTypeContratConcessionPivot(CommonTypeContratConcessionPivot $v = null)
    {
        if ($v === null) {
            $this->setIdTypeContratConcessionPivot(NULL);
        } else {
            $this->setIdTypeContratConcessionPivot($v->getId());
        }

        $this->aCommonTypeContratConcessionPivot = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTypeContratConcessionPivot object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTContratTitulaire($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTypeContratConcessionPivot object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTypeContratConcessionPivot The associated CommonTypeContratConcessionPivot object.
     * @throws PropelException
     */
    public function getCommonTypeContratConcessionPivot(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTypeContratConcessionPivot === null && ($this->id_type_contrat_concession_pivot !== null) && $doQuery) {
            $this->aCommonTypeContratConcessionPivot = CommonTypeContratConcessionPivotQuery::create()->findPk($this->id_type_contrat_concession_pivot, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTypeContratConcessionPivot->addCommonTContratTitulaires($this);
             */
        }

        return $this->aCommonTypeContratConcessionPivot;
    }

    /**
     * Declares an association between this object and a CommonTypeContratPivot object.
     *
     * @param   CommonTypeContratPivot $v
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTypeContratPivot(CommonTypeContratPivot $v = null)
    {
        if ($v === null) {
            $this->setIdTypeContratPivot(NULL);
        } else {
            $this->setIdTypeContratPivot($v->getId());
        }

        $this->aCommonTypeContratPivot = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTypeContratPivot object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTContratTitulaire($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTypeContratPivot object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTypeContratPivot The associated CommonTypeContratPivot object.
     * @throws PropelException
     */
    public function getCommonTypeContratPivot(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTypeContratPivot === null && ($this->id_type_contrat_pivot !== null) && $doQuery) {
            $this->aCommonTypeContratPivot = CommonTypeContratPivotQuery::create()->findPk($this->id_type_contrat_pivot, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTypeContratPivot->addCommonTContratTitulaires($this);
             */
        }

        return $this->aCommonTypeContratPivot;
    }

    /**
     * Declares an association between this object and a CommonTypeProcedureConcessionPivot object.
     *
     * @param   CommonTypeProcedureConcessionPivot $v
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTypeProcedureConcessionPivot(CommonTypeProcedureConcessionPivot $v = null)
    {
        if ($v === null) {
            $this->setIdTypeProcedureConcessionPivot(NULL);
        } else {
            $this->setIdTypeProcedureConcessionPivot($v->getId());
        }

        $this->aCommonTypeProcedureConcessionPivot = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTypeProcedureConcessionPivot object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTContratTitulaire($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTypeProcedureConcessionPivot object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTypeProcedureConcessionPivot The associated CommonTypeProcedureConcessionPivot object.
     * @throws PropelException
     */
    public function getCommonTypeProcedureConcessionPivot(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTypeProcedureConcessionPivot === null && ($this->id_type_procedure_concession_pivot !== null) && $doQuery) {
            $this->aCommonTypeProcedureConcessionPivot = CommonTypeProcedureConcessionPivotQuery::create()->findPk($this->id_type_procedure_concession_pivot, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTypeProcedureConcessionPivot->addCommonTContratTitulaires($this);
             */
        }

        return $this->aCommonTypeProcedureConcessionPivot;
    }

    /**
     * Declares an association between this object and a CommonTContactContrat object.
     *
     * @param   CommonTContactContrat $v
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTContactContrat(CommonTContactContrat $v = null)
    {
        if ($v === null) {
            $this->setIdContactContrat(NULL);
        } else {
            $this->setIdContactContrat($v->getIdContactContrat());
        }

        $this->aCommonTContactContrat = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTContactContrat object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTContratTitulaire($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTContactContrat object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTContactContrat The associated CommonTContactContrat object.
     * @throws PropelException
     */
    public function getCommonTContactContrat(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTContactContrat === null && ($this->id_contact_contrat !== null) && $doQuery) {
            $this->aCommonTContactContrat = CommonTContactContratQuery::create()->findPk($this->id_contact_contrat, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTContactContrat->addCommonTContratTitulaires($this);
             */
        }

        return $this->aCommonTContactContrat;
    }

    /**
     * Declares an association between this object and a CommonTContratTitulaire object.
     *
     * @param   CommonTContratTitulaire $v
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTContratTitulaireRelatedByIdContratMulti(CommonTContratTitulaire $v = null)
    {
        if ($v === null) {
            $this->setIdContratMulti(NULL);
        } else {
            $this->setIdContratMulti($v->getIdContratTitulaire());
        }

        $this->aCommonTContratTitulaireRelatedByIdContratMulti = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTContratTitulaire object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTContratTitulaireRelatedByIdContratTitulaire($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTContratTitulaire object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTContratTitulaire The associated CommonTContratTitulaire object.
     * @throws PropelException
     */
    public function getCommonTContratTitulaireRelatedByIdContratMulti(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTContratTitulaireRelatedByIdContratMulti === null && ($this->id_contrat_multi !== null) && $doQuery) {
            $this->aCommonTContratTitulaireRelatedByIdContratMulti = CommonTContratTitulaireQuery::create()->findPk($this->id_contrat_multi, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTContratTitulaireRelatedByIdContratMulti->addCommonTContratTitulairesRelatedByIdContratTitulaire($this);
             */
        }

        return $this->aCommonTContratTitulaireRelatedByIdContratMulti;
    }

    /**
     * Declares an association between this object and a CommonTTypeContrat object.
     *
     * @param   CommonTTypeContrat $v
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTTypeContrat(CommonTTypeContrat $v = null)
    {
        if ($v === null) {
            $this->setIdTypeContrat(NULL);
        } else {
            $this->setIdTypeContrat($v->getIdTypeContrat());
        }

        $this->aCommonTTypeContrat = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTTypeContrat object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTContratTitulaire($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTTypeContrat object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTTypeContrat The associated CommonTTypeContrat object.
     * @throws PropelException
     */
    public function getCommonTTypeContrat(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTTypeContrat === null && ($this->id_type_contrat !== null) && $doQuery) {
            $this->aCommonTTypeContrat = CommonTTypeContratQuery::create()->findPk($this->id_type_contrat, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTTypeContrat->addCommonTContratTitulaires($this);
             */
        }

        return $this->aCommonTTypeContrat;
    }

    /**
     * Declares an association between this object and a CommonTypeProcedure object.
     *
     * @param   CommonTypeProcedure $v
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTypeProcedure(CommonTypeProcedure $v = null)
    {
        if ($v === null) {
            $this->setIdTypeProcedurePivot(NULL);
        } else {
            $this->setIdTypeProcedurePivot($v->getIdTypeProcedure());
        }

        $this->aCommonTypeProcedure = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTypeProcedure object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTContratTitulaire($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTypeProcedure object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTypeProcedure The associated CommonTypeProcedure object.
     * @throws PropelException
     */
    public function getCommonTypeProcedure(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTypeProcedure === null && ($this->id_type_procedure_pivot !== null) && $doQuery) {
            $this->aCommonTypeProcedure = CommonTypeProcedureQuery::create()->findPk($this->id_type_procedure_pivot, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTypeProcedure->addCommonTContratTitulaires($this);
             */
        }

        return $this->aCommonTypeProcedure;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonMarche' == $relationName) {
            $this->initCommonMarches();
        }
        if ('CommonDonneesAnnuellesConcession' == $relationName) {
            $this->initCommonDonneesAnnuellesConcessions();
        }
        if ('CommonEchangeDoc' == $relationName) {
            $this->initCommonEchangeDocs();
        }
        if ('CommonModificationContrat' == $relationName) {
            $this->initCommonModificationContrats();
        }
        if ('CommonTConsLotContrat' == $relationName) {
            $this->initCommonTConsLotContrats();
        }
        if ('CommonTContratTitulaireRelatedByIdContratTitulaire' == $relationName) {
            $this->initCommonTContratTitulairesRelatedByIdContratTitulaire();
        }
        if ('CommonTDonneesConsultation' == $relationName) {
            $this->initCommonTDonneesConsultations();
        }
    }

    /**
     * Clears out the collCommonMarches collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @see        addCommonMarches()
     */
    public function clearCommonMarches()
    {
        $this->collCommonMarches = null; // important to set this to null since that means it is uninitialized
        $this->collCommonMarchesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonMarches collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonMarches($v = true)
    {
        $this->collCommonMarchesPartial = $v;
    }

    /**
     * Initializes the collCommonMarches collection.
     *
     * By default this just sets the collCommonMarches collection to an empty array (like clearcollCommonMarches());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonMarches($overrideExisting = true)
    {
        if (null !== $this->collCommonMarches && !$overrideExisting) {
            return;
        }
        $this->collCommonMarches = new PropelObjectCollection();
        $this->collCommonMarches->setModel('CommonMarche');
    }

    /**
     * Gets an array of CommonMarche objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTContratTitulaire is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonMarche[] List of CommonMarche objects
     * @throws PropelException
     */
    public function getCommonMarches($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonMarchesPartial && !$this->isNew();
        if (null === $this->collCommonMarches || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonMarches) {
                // return empty collection
                $this->initCommonMarches();
            } else {
                $collCommonMarches = CommonMarcheQuery::create(null, $criteria)
                    ->filterByCommonTContratTitulaire($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonMarchesPartial && count($collCommonMarches)) {
                      $this->initCommonMarches(false);

                      foreach ($collCommonMarches as $obj) {
                        if (false == $this->collCommonMarches->contains($obj)) {
                          $this->collCommonMarches->append($obj);
                        }
                      }

                      $this->collCommonMarchesPartial = true;
                    }

                    $collCommonMarches->getInternalIterator()->rewind();

                    return $collCommonMarches;
                }

                if ($partial && $this->collCommonMarches) {
                    foreach ($this->collCommonMarches as $obj) {
                        if ($obj->isNew()) {
                            $collCommonMarches[] = $obj;
                        }
                    }
                }

                $this->collCommonMarches = $collCommonMarches;
                $this->collCommonMarchesPartial = false;
            }
        }

        return $this->collCommonMarches;
    }

    /**
     * Sets a collection of CommonMarche objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonMarches A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCommonMarches(PropelCollection $commonMarches, PropelPDO $con = null)
    {
        $commonMarchesToDelete = $this->getCommonMarches(new Criteria(), $con)->diff($commonMarches);


        $this->commonMarchesScheduledForDeletion = $commonMarchesToDelete;

        foreach ($commonMarchesToDelete as $commonMarcheRemoved) {
            $commonMarcheRemoved->setCommonTContratTitulaire(null);
        }

        $this->collCommonMarches = null;
        foreach ($commonMarches as $commonMarche) {
            $this->addCommonMarche($commonMarche);
        }

        $this->collCommonMarches = $commonMarches;
        $this->collCommonMarchesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonMarche objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonMarche objects.
     * @throws PropelException
     */
    public function countCommonMarches(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonMarchesPartial && !$this->isNew();
        if (null === $this->collCommonMarches || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonMarches) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonMarches());
            }
            $query = CommonMarcheQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTContratTitulaire($this)
                ->count($con);
        }

        return count($this->collCommonMarches);
    }

    /**
     * Method called to associate a CommonMarche object to this object
     * through the CommonMarche foreign key attribute.
     *
     * @param   CommonMarche $l CommonMarche
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function addCommonMarche(CommonMarche $l)
    {
        if ($this->collCommonMarches === null) {
            $this->initCommonMarches();
            $this->collCommonMarchesPartial = true;
        }
        if (!in_array($l, $this->collCommonMarches->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonMarche($l);
        }

        return $this;
    }

    /**
     * @param	CommonMarche $commonMarche The commonMarche object to add.
     */
    protected function doAddCommonMarche($commonMarche)
    {
        $this->collCommonMarches[]= $commonMarche;
        $commonMarche->setCommonTContratTitulaire($this);
    }

    /**
     * @param	CommonMarche $commonMarche The commonMarche object to remove.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function removeCommonMarche($commonMarche)
    {
        if ($this->getCommonMarches()->contains($commonMarche)) {
            $this->collCommonMarches->remove($this->collCommonMarches->search($commonMarche));
            if (null === $this->commonMarchesScheduledForDeletion) {
                $this->commonMarchesScheduledForDeletion = clone $this->collCommonMarches;
                $this->commonMarchesScheduledForDeletion->clear();
            }
            $this->commonMarchesScheduledForDeletion[]= clone $commonMarche;
            $commonMarche->setCommonTContratTitulaire(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonMarches from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonMarche[] List of CommonMarche objects
     */
    public function getCommonMarchesJoinCommonCategorieConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonMarcheQuery::create(null, $criteria);
        $query->joinWith('CommonCategorieConsultation', $join_behavior);

        return $this->getCommonMarches($query, $con);
    }

    /**
     * Clears out the collCommonDonneesAnnuellesConcessions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @see        addCommonDonneesAnnuellesConcessions()
     */
    public function clearCommonDonneesAnnuellesConcessions()
    {
        $this->collCommonDonneesAnnuellesConcessions = null; // important to set this to null since that means it is uninitialized
        $this->collCommonDonneesAnnuellesConcessionsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonDonneesAnnuellesConcessions collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonDonneesAnnuellesConcessions($v = true)
    {
        $this->collCommonDonneesAnnuellesConcessionsPartial = $v;
    }

    /**
     * Initializes the collCommonDonneesAnnuellesConcessions collection.
     *
     * By default this just sets the collCommonDonneesAnnuellesConcessions collection to an empty array (like clearcollCommonDonneesAnnuellesConcessions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonDonneesAnnuellesConcessions($overrideExisting = true)
    {
        if (null !== $this->collCommonDonneesAnnuellesConcessions && !$overrideExisting) {
            return;
        }
        $this->collCommonDonneesAnnuellesConcessions = new PropelObjectCollection();
        $this->collCommonDonneesAnnuellesConcessions->setModel('CommonDonneesAnnuellesConcession');
    }

    /**
     * Gets an array of CommonDonneesAnnuellesConcession objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTContratTitulaire is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonDonneesAnnuellesConcession[] List of CommonDonneesAnnuellesConcession objects
     * @throws PropelException
     */
    public function getCommonDonneesAnnuellesConcessions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonDonneesAnnuellesConcessionsPartial && !$this->isNew();
        if (null === $this->collCommonDonneesAnnuellesConcessions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonDonneesAnnuellesConcessions) {
                // return empty collection
                $this->initCommonDonneesAnnuellesConcessions();
            } else {
                $collCommonDonneesAnnuellesConcessions = CommonDonneesAnnuellesConcessionQuery::create(null, $criteria)
                    ->filterByCommonTContratTitulaire($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonDonneesAnnuellesConcessionsPartial && count($collCommonDonneesAnnuellesConcessions)) {
                      $this->initCommonDonneesAnnuellesConcessions(false);

                      foreach ($collCommonDonneesAnnuellesConcessions as $obj) {
                        if (false == $this->collCommonDonneesAnnuellesConcessions->contains($obj)) {
                          $this->collCommonDonneesAnnuellesConcessions->append($obj);
                        }
                      }

                      $this->collCommonDonneesAnnuellesConcessionsPartial = true;
                    }

                    $collCommonDonneesAnnuellesConcessions->getInternalIterator()->rewind();

                    return $collCommonDonneesAnnuellesConcessions;
                }

                if ($partial && $this->collCommonDonneesAnnuellesConcessions) {
                    foreach ($this->collCommonDonneesAnnuellesConcessions as $obj) {
                        if ($obj->isNew()) {
                            $collCommonDonneesAnnuellesConcessions[] = $obj;
                        }
                    }
                }

                $this->collCommonDonneesAnnuellesConcessions = $collCommonDonneesAnnuellesConcessions;
                $this->collCommonDonneesAnnuellesConcessionsPartial = false;
            }
        }

        return $this->collCommonDonneesAnnuellesConcessions;
    }

    /**
     * Sets a collection of CommonDonneesAnnuellesConcession objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonDonneesAnnuellesConcessions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCommonDonneesAnnuellesConcessions(PropelCollection $commonDonneesAnnuellesConcessions, PropelPDO $con = null)
    {
        $commonDonneesAnnuellesConcessionsToDelete = $this->getCommonDonneesAnnuellesConcessions(new Criteria(), $con)->diff($commonDonneesAnnuellesConcessions);


        $this->commonDonneesAnnuellesConcessionsScheduledForDeletion = $commonDonneesAnnuellesConcessionsToDelete;

        foreach ($commonDonneesAnnuellesConcessionsToDelete as $commonDonneesAnnuellesConcessionRemoved) {
            $commonDonneesAnnuellesConcessionRemoved->setCommonTContratTitulaire(null);
        }

        $this->collCommonDonneesAnnuellesConcessions = null;
        foreach ($commonDonneesAnnuellesConcessions as $commonDonneesAnnuellesConcession) {
            $this->addCommonDonneesAnnuellesConcession($commonDonneesAnnuellesConcession);
        }

        $this->collCommonDonneesAnnuellesConcessions = $commonDonneesAnnuellesConcessions;
        $this->collCommonDonneesAnnuellesConcessionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonDonneesAnnuellesConcession objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonDonneesAnnuellesConcession objects.
     * @throws PropelException
     */
    public function countCommonDonneesAnnuellesConcessions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonDonneesAnnuellesConcessionsPartial && !$this->isNew();
        if (null === $this->collCommonDonneesAnnuellesConcessions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonDonneesAnnuellesConcessions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonDonneesAnnuellesConcessions());
            }
            $query = CommonDonneesAnnuellesConcessionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTContratTitulaire($this)
                ->count($con);
        }

        return count($this->collCommonDonneesAnnuellesConcessions);
    }

    /**
     * Method called to associate a CommonDonneesAnnuellesConcession object to this object
     * through the CommonDonneesAnnuellesConcession foreign key attribute.
     *
     * @param   CommonDonneesAnnuellesConcession $l CommonDonneesAnnuellesConcession
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function addCommonDonneesAnnuellesConcession(CommonDonneesAnnuellesConcession $l)
    {
        if ($this->collCommonDonneesAnnuellesConcessions === null) {
            $this->initCommonDonneesAnnuellesConcessions();
            $this->collCommonDonneesAnnuellesConcessionsPartial = true;
        }
        if (!in_array($l, $this->collCommonDonneesAnnuellesConcessions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonDonneesAnnuellesConcession($l);
        }

        return $this;
    }

    /**
     * @param	CommonDonneesAnnuellesConcession $commonDonneesAnnuellesConcession The commonDonneesAnnuellesConcession object to add.
     */
    protected function doAddCommonDonneesAnnuellesConcession($commonDonneesAnnuellesConcession)
    {
        $this->collCommonDonneesAnnuellesConcessions[]= $commonDonneesAnnuellesConcession;
        $commonDonneesAnnuellesConcession->setCommonTContratTitulaire($this);
    }

    /**
     * @param	CommonDonneesAnnuellesConcession $commonDonneesAnnuellesConcession The commonDonneesAnnuellesConcession object to remove.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function removeCommonDonneesAnnuellesConcession($commonDonneesAnnuellesConcession)
    {
        if ($this->getCommonDonneesAnnuellesConcessions()->contains($commonDonneesAnnuellesConcession)) {
            $this->collCommonDonneesAnnuellesConcessions->remove($this->collCommonDonneesAnnuellesConcessions->search($commonDonneesAnnuellesConcession));
            if (null === $this->commonDonneesAnnuellesConcessionsScheduledForDeletion) {
                $this->commonDonneesAnnuellesConcessionsScheduledForDeletion = clone $this->collCommonDonneesAnnuellesConcessions;
                $this->commonDonneesAnnuellesConcessionsScheduledForDeletion->clear();
            }
            $this->commonDonneesAnnuellesConcessionsScheduledForDeletion[]= clone $commonDonneesAnnuellesConcession;
            $commonDonneesAnnuellesConcession->setCommonTContratTitulaire(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonEchangeDocs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @see        addCommonEchangeDocs()
     */
    public function clearCommonEchangeDocs()
    {
        $this->collCommonEchangeDocs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangeDocsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchangeDocs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchangeDocs($v = true)
    {
        $this->collCommonEchangeDocsPartial = $v;
    }

    /**
     * Initializes the collCommonEchangeDocs collection.
     *
     * By default this just sets the collCommonEchangeDocs collection to an empty array (like clearcollCommonEchangeDocs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchangeDocs($overrideExisting = true)
    {
        if (null !== $this->collCommonEchangeDocs && !$overrideExisting) {
            return;
        }
        $this->collCommonEchangeDocs = new PropelObjectCollection();
        $this->collCommonEchangeDocs->setModel('CommonEchangeDoc');
    }

    /**
     * Gets an array of CommonEchangeDoc objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTContratTitulaire is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     * @throws PropelException
     */
    public function getCommonEchangeDocs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocsPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocs) {
                // return empty collection
                $this->initCommonEchangeDocs();
            } else {
                $collCommonEchangeDocs = CommonEchangeDocQuery::create(null, $criteria)
                    ->filterByCommonTContratTitulaire($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangeDocsPartial && count($collCommonEchangeDocs)) {
                      $this->initCommonEchangeDocs(false);

                      foreach ($collCommonEchangeDocs as $obj) {
                        if (false == $this->collCommonEchangeDocs->contains($obj)) {
                          $this->collCommonEchangeDocs->append($obj);
                        }
                      }

                      $this->collCommonEchangeDocsPartial = true;
                    }

                    $collCommonEchangeDocs->getInternalIterator()->rewind();

                    return $collCommonEchangeDocs;
                }

                if ($partial && $this->collCommonEchangeDocs) {
                    foreach ($this->collCommonEchangeDocs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchangeDocs[] = $obj;
                        }
                    }
                }

                $this->collCommonEchangeDocs = $collCommonEchangeDocs;
                $this->collCommonEchangeDocsPartial = false;
            }
        }

        return $this->collCommonEchangeDocs;
    }

    /**
     * Sets a collection of CommonEchangeDoc objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchangeDocs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCommonEchangeDocs(PropelCollection $commonEchangeDocs, PropelPDO $con = null)
    {
        $commonEchangeDocsToDelete = $this->getCommonEchangeDocs(new Criteria(), $con)->diff($commonEchangeDocs);


        $this->commonEchangeDocsScheduledForDeletion = $commonEchangeDocsToDelete;

        foreach ($commonEchangeDocsToDelete as $commonEchangeDocRemoved) {
            $commonEchangeDocRemoved->setCommonTContratTitulaire(null);
        }

        $this->collCommonEchangeDocs = null;
        foreach ($commonEchangeDocs as $commonEchangeDoc) {
            $this->addCommonEchangeDoc($commonEchangeDoc);
        }

        $this->collCommonEchangeDocs = $commonEchangeDocs;
        $this->collCommonEchangeDocsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchangeDoc objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchangeDoc objects.
     * @throws PropelException
     */
    public function countCommonEchangeDocs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocsPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchangeDocs());
            }
            $query = CommonEchangeDocQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTContratTitulaire($this)
                ->count($con);
        }

        return count($this->collCommonEchangeDocs);
    }

    /**
     * Method called to associate a CommonEchangeDoc object to this object
     * through the CommonEchangeDoc foreign key attribute.
     *
     * @param   CommonEchangeDoc $l CommonEchangeDoc
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function addCommonEchangeDoc(CommonEchangeDoc $l)
    {
        if ($this->collCommonEchangeDocs === null) {
            $this->initCommonEchangeDocs();
            $this->collCommonEchangeDocsPartial = true;
        }
        if (!in_array($l, $this->collCommonEchangeDocs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchangeDoc($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchangeDoc $commonEchangeDoc The commonEchangeDoc object to add.
     */
    protected function doAddCommonEchangeDoc($commonEchangeDoc)
    {
        $this->collCommonEchangeDocs[]= $commonEchangeDoc;
        $commonEchangeDoc->setCommonTContratTitulaire($this);
    }

    /**
     * @param	CommonEchangeDoc $commonEchangeDoc The commonEchangeDoc object to remove.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function removeCommonEchangeDoc($commonEchangeDoc)
    {
        if ($this->getCommonEchangeDocs()->contains($commonEchangeDoc)) {
            $this->collCommonEchangeDocs->remove($this->collCommonEchangeDocs->search($commonEchangeDoc));
            if (null === $this->commonEchangeDocsScheduledForDeletion) {
                $this->commonEchangeDocsScheduledForDeletion = clone $this->collCommonEchangeDocs;
                $this->commonEchangeDocsScheduledForDeletion->clear();
            }
            $this->commonEchangeDocsScheduledForDeletion[]= $commonEchangeDoc;
            $commonEchangeDoc->setCommonTContratTitulaire(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonEchangeDocs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     */
    public function getCommonEchangeDocsJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonEchangeDocs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonEchangeDocs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     */
    public function getCommonEchangeDocsJoinCommonEchangeDocApplicationClient($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocQuery::create(null, $criteria);
        $query->joinWith('CommonEchangeDocApplicationClient', $join_behavior);

        return $this->getCommonEchangeDocs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonEchangeDocs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     */
    public function getCommonEchangeDocsJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonEchangeDocs($query, $con);
    }

    /**
     * Clears out the collCommonModificationContrats collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @see        addCommonModificationContrats()
     */
    public function clearCommonModificationContrats()
    {
        $this->collCommonModificationContrats = null; // important to set this to null since that means it is uninitialized
        $this->collCommonModificationContratsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonModificationContrats collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonModificationContrats($v = true)
    {
        $this->collCommonModificationContratsPartial = $v;
    }

    /**
     * Initializes the collCommonModificationContrats collection.
     *
     * By default this just sets the collCommonModificationContrats collection to an empty array (like clearcollCommonModificationContrats());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonModificationContrats($overrideExisting = true)
    {
        if (null !== $this->collCommonModificationContrats && !$overrideExisting) {
            return;
        }
        $this->collCommonModificationContrats = new PropelObjectCollection();
        $this->collCommonModificationContrats->setModel('CommonModificationContrat');
    }

    /**
     * Gets an array of CommonModificationContrat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTContratTitulaire is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonModificationContrat[] List of CommonModificationContrat objects
     * @throws PropelException
     */
    public function getCommonModificationContrats($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonModificationContratsPartial && !$this->isNew();
        if (null === $this->collCommonModificationContrats || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonModificationContrats) {
                // return empty collection
                $this->initCommonModificationContrats();
            } else {
                $collCommonModificationContrats = CommonModificationContratQuery::create(null, $criteria)
                    ->filterByCommonTContratTitulaire($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonModificationContratsPartial && count($collCommonModificationContrats)) {
                      $this->initCommonModificationContrats(false);

                      foreach ($collCommonModificationContrats as $obj) {
                        if (false == $this->collCommonModificationContrats->contains($obj)) {
                          $this->collCommonModificationContrats->append($obj);
                        }
                      }

                      $this->collCommonModificationContratsPartial = true;
                    }

                    $collCommonModificationContrats->getInternalIterator()->rewind();

                    return $collCommonModificationContrats;
                }

                if ($partial && $this->collCommonModificationContrats) {
                    foreach ($this->collCommonModificationContrats as $obj) {
                        if ($obj->isNew()) {
                            $collCommonModificationContrats[] = $obj;
                        }
                    }
                }

                $this->collCommonModificationContrats = $collCommonModificationContrats;
                $this->collCommonModificationContratsPartial = false;
            }
        }

        return $this->collCommonModificationContrats;
    }

    /**
     * Sets a collection of CommonModificationContrat objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonModificationContrats A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCommonModificationContrats(PropelCollection $commonModificationContrats, PropelPDO $con = null)
    {
        $commonModificationContratsToDelete = $this->getCommonModificationContrats(new Criteria(), $con)->diff($commonModificationContrats);


        $this->commonModificationContratsScheduledForDeletion = $commonModificationContratsToDelete;

        foreach ($commonModificationContratsToDelete as $commonModificationContratRemoved) {
            $commonModificationContratRemoved->setCommonTContratTitulaire(null);
        }

        $this->collCommonModificationContrats = null;
        foreach ($commonModificationContrats as $commonModificationContrat) {
            $this->addCommonModificationContrat($commonModificationContrat);
        }

        $this->collCommonModificationContrats = $commonModificationContrats;
        $this->collCommonModificationContratsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonModificationContrat objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonModificationContrat objects.
     * @throws PropelException
     */
    public function countCommonModificationContrats(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonModificationContratsPartial && !$this->isNew();
        if (null === $this->collCommonModificationContrats || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonModificationContrats) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonModificationContrats());
            }
            $query = CommonModificationContratQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTContratTitulaire($this)
                ->count($con);
        }

        return count($this->collCommonModificationContrats);
    }

    /**
     * Method called to associate a CommonModificationContrat object to this object
     * through the CommonModificationContrat foreign key attribute.
     *
     * @param   CommonModificationContrat $l CommonModificationContrat
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function addCommonModificationContrat(CommonModificationContrat $l)
    {
        if ($this->collCommonModificationContrats === null) {
            $this->initCommonModificationContrats();
            $this->collCommonModificationContratsPartial = true;
        }
        if (!in_array($l, $this->collCommonModificationContrats->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonModificationContrat($l);
        }

        return $this;
    }

    /**
     * @param	CommonModificationContrat $commonModificationContrat The commonModificationContrat object to add.
     */
    protected function doAddCommonModificationContrat($commonModificationContrat)
    {
        $this->collCommonModificationContrats[]= $commonModificationContrat;
        $commonModificationContrat->setCommonTContratTitulaire($this);
    }

    /**
     * @param	CommonModificationContrat $commonModificationContrat The commonModificationContrat object to remove.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function removeCommonModificationContrat($commonModificationContrat)
    {
        if ($this->getCommonModificationContrats()->contains($commonModificationContrat)) {
            $this->collCommonModificationContrats->remove($this->collCommonModificationContrats->search($commonModificationContrat));
            if (null === $this->commonModificationContratsScheduledForDeletion) {
                $this->commonModificationContratsScheduledForDeletion = clone $this->collCommonModificationContrats;
                $this->commonModificationContratsScheduledForDeletion->clear();
            }
            $this->commonModificationContratsScheduledForDeletion[]= clone $commonModificationContrat;
            $commonModificationContrat->setCommonTContratTitulaire(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonModificationContrats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonModificationContrat[] List of CommonModificationContrat objects
     */
    public function getCommonModificationContratsJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonModificationContratQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonModificationContrats($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonModificationContrats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonModificationContrat[] List of CommonModificationContrat objects
     */
    public function getCommonModificationContratsJoinCommonTEtablissement($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonModificationContratQuery::create(null, $criteria);
        $query->joinWith('CommonTEtablissement', $join_behavior);

        return $this->getCommonModificationContrats($query, $con);
    }

    /**
     * Clears out the collCommonTConsLotContrats collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @see        addCommonTConsLotContrats()
     */
    public function clearCommonTConsLotContrats()
    {
        $this->collCommonTConsLotContrats = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTConsLotContratsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTConsLotContrats collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTConsLotContrats($v = true)
    {
        $this->collCommonTConsLotContratsPartial = $v;
    }

    /**
     * Initializes the collCommonTConsLotContrats collection.
     *
     * By default this just sets the collCommonTConsLotContrats collection to an empty array (like clearcollCommonTConsLotContrats());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTConsLotContrats($overrideExisting = true)
    {
        if (null !== $this->collCommonTConsLotContrats && !$overrideExisting) {
            return;
        }
        $this->collCommonTConsLotContrats = new PropelObjectCollection();
        $this->collCommonTConsLotContrats->setModel('CommonTConsLotContrat');
    }

    /**
     * Gets an array of CommonTConsLotContrat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTContratTitulaire is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTConsLotContrat[] List of CommonTConsLotContrat objects
     * @throws PropelException
     */
    public function getCommonTConsLotContrats($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTConsLotContratsPartial && !$this->isNew();
        if (null === $this->collCommonTConsLotContrats || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTConsLotContrats) {
                // return empty collection
                $this->initCommonTConsLotContrats();
            } else {
                $collCommonTConsLotContrats = CommonTConsLotContratQuery::create(null, $criteria)
                    ->filterByCommonTContratTitulaire($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTConsLotContratsPartial && count($collCommonTConsLotContrats)) {
                      $this->initCommonTConsLotContrats(false);

                      foreach ($collCommonTConsLotContrats as $obj) {
                        if (false == $this->collCommonTConsLotContrats->contains($obj)) {
                          $this->collCommonTConsLotContrats->append($obj);
                        }
                      }

                      $this->collCommonTConsLotContratsPartial = true;
                    }

                    $collCommonTConsLotContrats->getInternalIterator()->rewind();

                    return $collCommonTConsLotContrats;
                }

                if ($partial && $this->collCommonTConsLotContrats) {
                    foreach ($this->collCommonTConsLotContrats as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTConsLotContrats[] = $obj;
                        }
                    }
                }

                $this->collCommonTConsLotContrats = $collCommonTConsLotContrats;
                $this->collCommonTConsLotContratsPartial = false;
            }
        }

        return $this->collCommonTConsLotContrats;
    }

    /**
     * Sets a collection of CommonTConsLotContrat objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTConsLotContrats A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCommonTConsLotContrats(PropelCollection $commonTConsLotContrats, PropelPDO $con = null)
    {
        $commonTConsLotContratsToDelete = $this->getCommonTConsLotContrats(new Criteria(), $con)->diff($commonTConsLotContrats);


        $this->commonTConsLotContratsScheduledForDeletion = $commonTConsLotContratsToDelete;

        foreach ($commonTConsLotContratsToDelete as $commonTConsLotContratRemoved) {
            $commonTConsLotContratRemoved->setCommonTContratTitulaire(null);
        }

        $this->collCommonTConsLotContrats = null;
        foreach ($commonTConsLotContrats as $commonTConsLotContrat) {
            $this->addCommonTConsLotContrat($commonTConsLotContrat);
        }

        $this->collCommonTConsLotContrats = $commonTConsLotContrats;
        $this->collCommonTConsLotContratsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTConsLotContrat objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTConsLotContrat objects.
     * @throws PropelException
     */
    public function countCommonTConsLotContrats(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTConsLotContratsPartial && !$this->isNew();
        if (null === $this->collCommonTConsLotContrats || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTConsLotContrats) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTConsLotContrats());
            }
            $query = CommonTConsLotContratQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTContratTitulaire($this)
                ->count($con);
        }

        return count($this->collCommonTConsLotContrats);
    }

    /**
     * Method called to associate a CommonTConsLotContrat object to this object
     * through the CommonTConsLotContrat foreign key attribute.
     *
     * @param   CommonTConsLotContrat $l CommonTConsLotContrat
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function addCommonTConsLotContrat(CommonTConsLotContrat $l)
    {
        if ($this->collCommonTConsLotContrats === null) {
            $this->initCommonTConsLotContrats();
            $this->collCommonTConsLotContratsPartial = true;
        }
        if (!in_array($l, $this->collCommonTConsLotContrats->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTConsLotContrat($l);
        }

        return $this;
    }

    /**
     * @param	CommonTConsLotContrat $commonTConsLotContrat The commonTConsLotContrat object to add.
     */
    protected function doAddCommonTConsLotContrat($commonTConsLotContrat)
    {
        $this->collCommonTConsLotContrats[]= $commonTConsLotContrat;
        $commonTConsLotContrat->setCommonTContratTitulaire($this);
    }

    /**
     * @param	CommonTConsLotContrat $commonTConsLotContrat The commonTConsLotContrat object to remove.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function removeCommonTConsLotContrat($commonTConsLotContrat)
    {
        if ($this->getCommonTConsLotContrats()->contains($commonTConsLotContrat)) {
            $this->collCommonTConsLotContrats->remove($this->collCommonTConsLotContrats->search($commonTConsLotContrat));
            if (null === $this->commonTConsLotContratsScheduledForDeletion) {
                $this->commonTConsLotContratsScheduledForDeletion = clone $this->collCommonTConsLotContrats;
                $this->commonTConsLotContratsScheduledForDeletion->clear();
            }
            $this->commonTConsLotContratsScheduledForDeletion[]= clone $commonTConsLotContrat;
            $commonTConsLotContrat->setCommonTContratTitulaire(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTContratTitulairesRelatedByIdContratTitulaire collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @see        addCommonTContratTitulairesRelatedByIdContratTitulaire()
     */
    public function clearCommonTContratTitulairesRelatedByIdContratTitulaire()
    {
        $this->collCommonTContratTitulairesRelatedByIdContratTitulaire = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTContratTitulairesRelatedByIdContratTitulairePartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTContratTitulairesRelatedByIdContratTitulaire collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTContratTitulairesRelatedByIdContratTitulaire($v = true)
    {
        $this->collCommonTContratTitulairesRelatedByIdContratTitulairePartial = $v;
    }

    /**
     * Initializes the collCommonTContratTitulairesRelatedByIdContratTitulaire collection.
     *
     * By default this just sets the collCommonTContratTitulairesRelatedByIdContratTitulaire collection to an empty array (like clearcollCommonTContratTitulairesRelatedByIdContratTitulaire());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTContratTitulairesRelatedByIdContratTitulaire($overrideExisting = true)
    {
        if (null !== $this->collCommonTContratTitulairesRelatedByIdContratTitulaire && !$overrideExisting) {
            return;
        }
        $this->collCommonTContratTitulairesRelatedByIdContratTitulaire = new PropelObjectCollection();
        $this->collCommonTContratTitulairesRelatedByIdContratTitulaire->setModel('CommonTContratTitulaire');
    }

    /**
     * Gets an array of CommonTContratTitulaire objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTContratTitulaire is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     * @throws PropelException
     */
    public function getCommonTContratTitulairesRelatedByIdContratTitulaire($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTContratTitulairesRelatedByIdContratTitulairePartial && !$this->isNew();
        if (null === $this->collCommonTContratTitulairesRelatedByIdContratTitulaire || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTContratTitulairesRelatedByIdContratTitulaire) {
                // return empty collection
                $this->initCommonTContratTitulairesRelatedByIdContratTitulaire();
            } else {
                $collCommonTContratTitulairesRelatedByIdContratTitulaire = CommonTContratTitulaireQuery::create(null, $criteria)
                    ->filterByCommonTContratTitulaireRelatedByIdContratMulti($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTContratTitulairesRelatedByIdContratTitulairePartial && count($collCommonTContratTitulairesRelatedByIdContratTitulaire)) {
                      $this->initCommonTContratTitulairesRelatedByIdContratTitulaire(false);

                      foreach ($collCommonTContratTitulairesRelatedByIdContratTitulaire as $obj) {
                        if (false == $this->collCommonTContratTitulairesRelatedByIdContratTitulaire->contains($obj)) {
                          $this->collCommonTContratTitulairesRelatedByIdContratTitulaire->append($obj);
                        }
                      }

                      $this->collCommonTContratTitulairesRelatedByIdContratTitulairePartial = true;
                    }

                    $collCommonTContratTitulairesRelatedByIdContratTitulaire->getInternalIterator()->rewind();

                    return $collCommonTContratTitulairesRelatedByIdContratTitulaire;
                }

                if ($partial && $this->collCommonTContratTitulairesRelatedByIdContratTitulaire) {
                    foreach ($this->collCommonTContratTitulairesRelatedByIdContratTitulaire as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTContratTitulairesRelatedByIdContratTitulaire[] = $obj;
                        }
                    }
                }

                $this->collCommonTContratTitulairesRelatedByIdContratTitulaire = $collCommonTContratTitulairesRelatedByIdContratTitulaire;
                $this->collCommonTContratTitulairesRelatedByIdContratTitulairePartial = false;
            }
        }

        return $this->collCommonTContratTitulairesRelatedByIdContratTitulaire;
    }

    /**
     * Sets a collection of CommonTContratTitulaireRelatedByIdContratTitulaire objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTContratTitulairesRelatedByIdContratTitulaire A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCommonTContratTitulairesRelatedByIdContratTitulaire(PropelCollection $commonTContratTitulairesRelatedByIdContratTitulaire, PropelPDO $con = null)
    {
        $commonTContratTitulairesRelatedByIdContratTitulaireToDelete = $this->getCommonTContratTitulairesRelatedByIdContratTitulaire(new Criteria(), $con)->diff($commonTContratTitulairesRelatedByIdContratTitulaire);


        $this->commonTContratTitulairesRelatedByIdContratTitulaireScheduledForDeletion = $commonTContratTitulairesRelatedByIdContratTitulaireToDelete;

        foreach ($commonTContratTitulairesRelatedByIdContratTitulaireToDelete as $commonTContratTitulaireRelatedByIdContratTitulaireRemoved) {
            $commonTContratTitulaireRelatedByIdContratTitulaireRemoved->setCommonTContratTitulaireRelatedByIdContratMulti(null);
        }

        $this->collCommonTContratTitulairesRelatedByIdContratTitulaire = null;
        foreach ($commonTContratTitulairesRelatedByIdContratTitulaire as $commonTContratTitulaireRelatedByIdContratTitulaire) {
            $this->addCommonTContratTitulaireRelatedByIdContratTitulaire($commonTContratTitulaireRelatedByIdContratTitulaire);
        }

        $this->collCommonTContratTitulairesRelatedByIdContratTitulaire = $commonTContratTitulairesRelatedByIdContratTitulaire;
        $this->collCommonTContratTitulairesRelatedByIdContratTitulairePartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTContratTitulaire objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTContratTitulaire objects.
     * @throws PropelException
     */
    public function countCommonTContratTitulairesRelatedByIdContratTitulaire(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTContratTitulairesRelatedByIdContratTitulairePartial && !$this->isNew();
        if (null === $this->collCommonTContratTitulairesRelatedByIdContratTitulaire || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTContratTitulairesRelatedByIdContratTitulaire) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTContratTitulairesRelatedByIdContratTitulaire());
            }
            $query = CommonTContratTitulaireQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTContratTitulaireRelatedByIdContratMulti($this)
                ->count($con);
        }

        return count($this->collCommonTContratTitulairesRelatedByIdContratTitulaire);
    }

    /**
     * Method called to associate a BaseCommonTContratTitulaire object to this object
     * through the BaseCommonTContratTitulaire foreign key attribute.
     *
     * @param   BaseCommonTContratTitulaire $l BaseCommonTContratTitulaire
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function addCommonTContratTitulaireRelatedByIdContratTitulaire(BaseCommonTContratTitulaire $l)
    {
        if ($this->collCommonTContratTitulairesRelatedByIdContratTitulaire === null) {
            $this->initCommonTContratTitulairesRelatedByIdContratTitulaire();
            $this->collCommonTContratTitulairesRelatedByIdContratTitulairePartial = true;
        }
        if (!in_array($l, $this->collCommonTContratTitulairesRelatedByIdContratTitulaire->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTContratTitulaireRelatedByIdContratTitulaire($l);
        }

        return $this;
    }

    /**
     * @param	CommonTContratTitulaireRelatedByIdContratTitulaire $commonTContratTitulaireRelatedByIdContratTitulaire The commonTContratTitulaireRelatedByIdContratTitulaire object to add.
     */
    protected function doAddCommonTContratTitulaireRelatedByIdContratTitulaire($commonTContratTitulaireRelatedByIdContratTitulaire)
    {
        $this->collCommonTContratTitulairesRelatedByIdContratTitulaire[]= $commonTContratTitulaireRelatedByIdContratTitulaire;
        $commonTContratTitulaireRelatedByIdContratTitulaire->setCommonTContratTitulaireRelatedByIdContratMulti($this);
    }

    /**
     * @param	CommonTContratTitulaireRelatedByIdContratTitulaire $commonTContratTitulaireRelatedByIdContratTitulaire The commonTContratTitulaireRelatedByIdContratTitulaire object to remove.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function removeCommonTContratTitulaireRelatedByIdContratTitulaire($commonTContratTitulaireRelatedByIdContratTitulaire)
    {
        if ($this->getCommonTContratTitulairesRelatedByIdContratTitulaire()->contains($commonTContratTitulaireRelatedByIdContratTitulaire)) {
            $this->collCommonTContratTitulairesRelatedByIdContratTitulaire->remove($this->collCommonTContratTitulairesRelatedByIdContratTitulaire->search($commonTContratTitulaireRelatedByIdContratTitulaire));
            if (null === $this->commonTContratTitulairesRelatedByIdContratTitulaireScheduledForDeletion) {
                $this->commonTContratTitulairesRelatedByIdContratTitulaireScheduledForDeletion = clone $this->collCommonTContratTitulairesRelatedByIdContratTitulaire;
                $this->commonTContratTitulairesRelatedByIdContratTitulaireScheduledForDeletion->clear();
            }
            $this->commonTContratTitulairesRelatedByIdContratTitulaireScheduledForDeletion[]= $commonTContratTitulaireRelatedByIdContratTitulaire;
            $commonTContratTitulaireRelatedByIdContratTitulaire->setCommonTContratTitulaireRelatedByIdContratMulti(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonTContratTitulairesRelatedByIdContratTitulaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesRelatedByIdContratTitulaireJoinCommonTypeContratConcessionPivot($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeContratConcessionPivot', $join_behavior);

        return $this->getCommonTContratTitulairesRelatedByIdContratTitulaire($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonTContratTitulairesRelatedByIdContratTitulaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesRelatedByIdContratTitulaireJoinCommonTypeContratPivot($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeContratPivot', $join_behavior);

        return $this->getCommonTContratTitulairesRelatedByIdContratTitulaire($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonTContratTitulairesRelatedByIdContratTitulaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesRelatedByIdContratTitulaireJoinCommonTypeProcedureConcessionPivot($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeProcedureConcessionPivot', $join_behavior);

        return $this->getCommonTContratTitulairesRelatedByIdContratTitulaire($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonTContratTitulairesRelatedByIdContratTitulaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesRelatedByIdContratTitulaireJoinCommonTContactContrat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTContactContrat', $join_behavior);

        return $this->getCommonTContratTitulairesRelatedByIdContratTitulaire($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonTContratTitulairesRelatedByIdContratTitulaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesRelatedByIdContratTitulaireJoinCommonTTypeContrat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTTypeContrat', $join_behavior);

        return $this->getCommonTContratTitulairesRelatedByIdContratTitulaire($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContratTitulaire is new, it will return
     * an empty collection; or if this CommonTContratTitulaire has previously
     * been saved, it will retrieve related CommonTContratTitulairesRelatedByIdContratTitulaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContratTitulaire.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesRelatedByIdContratTitulaireJoinCommonTypeProcedure($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeProcedure', $join_behavior);

        return $this->getCommonTContratTitulairesRelatedByIdContratTitulaire($query, $con);
    }

    /**
     * Clears out the collCommonTDonneesConsultations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTContratTitulaire The current object (for fluent API support)
     * @see        addCommonTDonneesConsultations()
     */
    public function clearCommonTDonneesConsultations()
    {
        $this->collCommonTDonneesConsultations = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTDonneesConsultationsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTDonneesConsultations collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTDonneesConsultations($v = true)
    {
        $this->collCommonTDonneesConsultationsPartial = $v;
    }

    /**
     * Initializes the collCommonTDonneesConsultations collection.
     *
     * By default this just sets the collCommonTDonneesConsultations collection to an empty array (like clearcollCommonTDonneesConsultations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTDonneesConsultations($overrideExisting = true)
    {
        if (null !== $this->collCommonTDonneesConsultations && !$overrideExisting) {
            return;
        }
        $this->collCommonTDonneesConsultations = new PropelObjectCollection();
        $this->collCommonTDonneesConsultations->setModel('CommonTDonneesConsultation');
    }

    /**
     * Gets an array of CommonTDonneesConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTContratTitulaire is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTDonneesConsultation[] List of CommonTDonneesConsultation objects
     * @throws PropelException
     */
    public function getCommonTDonneesConsultations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTDonneesConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonTDonneesConsultations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTDonneesConsultations) {
                // return empty collection
                $this->initCommonTDonneesConsultations();
            } else {
                $collCommonTDonneesConsultations = CommonTDonneesConsultationQuery::create(null, $criteria)
                    ->filterByCommonTContratTitulaire($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTDonneesConsultationsPartial && count($collCommonTDonneesConsultations)) {
                      $this->initCommonTDonneesConsultations(false);

                      foreach ($collCommonTDonneesConsultations as $obj) {
                        if (false == $this->collCommonTDonneesConsultations->contains($obj)) {
                          $this->collCommonTDonneesConsultations->append($obj);
                        }
                      }

                      $this->collCommonTDonneesConsultationsPartial = true;
                    }

                    $collCommonTDonneesConsultations->getInternalIterator()->rewind();

                    return $collCommonTDonneesConsultations;
                }

                if ($partial && $this->collCommonTDonneesConsultations) {
                    foreach ($this->collCommonTDonneesConsultations as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTDonneesConsultations[] = $obj;
                        }
                    }
                }

                $this->collCommonTDonneesConsultations = $collCommonTDonneesConsultations;
                $this->collCommonTDonneesConsultationsPartial = false;
            }
        }

        return $this->collCommonTDonneesConsultations;
    }

    /**
     * Sets a collection of CommonTDonneesConsultation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTDonneesConsultations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function setCommonTDonneesConsultations(PropelCollection $commonTDonneesConsultations, PropelPDO $con = null)
    {
        $commonTDonneesConsultationsToDelete = $this->getCommonTDonneesConsultations(new Criteria(), $con)->diff($commonTDonneesConsultations);


        $this->commonTDonneesConsultationsScheduledForDeletion = $commonTDonneesConsultationsToDelete;

        foreach ($commonTDonneesConsultationsToDelete as $commonTDonneesConsultationRemoved) {
            $commonTDonneesConsultationRemoved->setCommonTContratTitulaire(null);
        }

        $this->collCommonTDonneesConsultations = null;
        foreach ($commonTDonneesConsultations as $commonTDonneesConsultation) {
            $this->addCommonTDonneesConsultation($commonTDonneesConsultation);
        }

        $this->collCommonTDonneesConsultations = $commonTDonneesConsultations;
        $this->collCommonTDonneesConsultationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTDonneesConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTDonneesConsultation objects.
     * @throws PropelException
     */
    public function countCommonTDonneesConsultations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTDonneesConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonTDonneesConsultations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTDonneesConsultations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTDonneesConsultations());
            }
            $query = CommonTDonneesConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTContratTitulaire($this)
                ->count($con);
        }

        return count($this->collCommonTDonneesConsultations);
    }

    /**
     * Method called to associate a CommonTDonneesConsultation object to this object
     * through the CommonTDonneesConsultation foreign key attribute.
     *
     * @param   CommonTDonneesConsultation $l CommonTDonneesConsultation
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function addCommonTDonneesConsultation(CommonTDonneesConsultation $l)
    {
        if ($this->collCommonTDonneesConsultations === null) {
            $this->initCommonTDonneesConsultations();
            $this->collCommonTDonneesConsultationsPartial = true;
        }
        if (!in_array($l, $this->collCommonTDonneesConsultations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTDonneesConsultation($l);
        }

        return $this;
    }

    /**
     * @param	CommonTDonneesConsultation $commonTDonneesConsultation The commonTDonneesConsultation object to add.
     */
    protected function doAddCommonTDonneesConsultation($commonTDonneesConsultation)
    {
        $this->collCommonTDonneesConsultations[]= $commonTDonneesConsultation;
        $commonTDonneesConsultation->setCommonTContratTitulaire($this);
    }

    /**
     * @param	CommonTDonneesConsultation $commonTDonneesConsultation The commonTDonneesConsultation object to remove.
     * @return CommonTContratTitulaire The current object (for fluent API support)
     */
    public function removeCommonTDonneesConsultation($commonTDonneesConsultation)
    {
        if ($this->getCommonTDonneesConsultations()->contains($commonTDonneesConsultation)) {
            $this->collCommonTDonneesConsultations->remove($this->collCommonTDonneesConsultations->search($commonTDonneesConsultation));
            if (null === $this->commonTDonneesConsultationsScheduledForDeletion) {
                $this->commonTDonneesConsultationsScheduledForDeletion = clone $this->collCommonTDonneesConsultations;
                $this->commonTDonneesConsultationsScheduledForDeletion->clear();
            }
            $this->commonTDonneesConsultationsScheduledForDeletion[]= $commonTDonneesConsultation;
            $commonTDonneesConsultation->setCommonTContratTitulaire(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_contrat_titulaire = null;
        $this->id_type_contrat = null;
        $this->id_contact_contrat = null;
        $this->numero_contrat = null;
        $this->lien_ac_sad = null;
        $this->id_contrat_multi = null;
        $this->organisme = null;
        $this->old_service_id = null;
        $this->id_titulaire = null;
        $this->id_titulaire_etab = null;
        $this->id_offre = null;
        $this->type_depot_reponse = null;
        $this->objet_contrat = null;
        $this->intitule = null;
        $this->montant_contrat = null;
        $this->id_tranche_budgetaire = null;
        $this->montant_max_estime = null;
        $this->publication_montant = null;
        $this->id_motif_non_publication_montant = null;
        $this->desc_motif_non_publication_montant = null;
        $this->publication_contrat = null;
        $this->num_ej = null;
        $this->statutej = null;
        $this->num_long_oeap = null;
        $this->reference_libre = null;
        $this->statut_contrat = null;
        $this->categorie = null;
        $this->ccag_applicable = null;
        $this->clause_sociale = null;
        $this->clause_sociale_condition_execution = null;
        $this->clause_sociale_insertion = null;
        $this->clause_sociale_ateliers_proteges = null;
        $this->clause_sociale_siae = null;
        $this->clause_sociale_ess = null;
        $this->clause_environnementale = null;
        $this->clause_env_specs_techniques = null;
        $this->clause_env_cond_execution = null;
        $this->clause_env_criteres_select = null;
        $this->date_prevue_notification = null;
        $this->date_prevue_fin_contrat = null;
        $this->date_prevue_max_fin_contrat = null;
        $this->date_notification = null;
        $this->date_fin_contrat = null;
        $this->date_max_fin_contrat = null;
        $this->date_attribution = null;
        $this->date_creation = null;
        $this->date_modification = null;
        $this->envoi_interface = null;
        $this->contrat_class_key = null;
        $this->pme_pmi = null;
        $this->reference_consultation = null;
        $this->hors_passation = null;
        $this->id_agent = null;
        $this->nom_agent = null;
        $this->prenom_agent = null;
        $this->lieu_execution = null;
        $this->code_cpv_1 = null;
        $this->code_cpv_2 = null;
        $this->marche_insertion = null;
        $this->clause_specification_technique = null;
        $this->procedure_passation_pivot = null;
        $this->nom_lieu_principal_execution = null;
        $this->code_lieu_principal_execution = null;
        $this->type_code_lieu_principal_execution = null;
        $this->duree_initiale_contrat = null;
        $this->forme_prix = null;
        $this->date_publication_initiale_de = null;
        $this->num_id_unique_marche_public = null;
        $this->libelle_type_contrat_pivot = null;
        $this->siret_pa_accord_cadre = null;
        $this->ac_marche_subsequent = null;
        $this->libelle_type_procedure_mpe = null;
        $this->nb_total_propositions_lot = null;
        $this->nb_total_propositions_demat_lot = null;
        $this->marche_defense = null;
        $this->siret = null;
        $this->nom_entite_acheteur = null;
        $this->statut_publication_sn = null;
        $this->date_publication_sn = null;
        $this->erreur_sn = null;
        $this->date_modification_sn = null;
        $this->id_type_procedure_pivot = null;
        $this->id_type_procedure_concession_pivot = null;
        $this->id_type_contrat_pivot = null;
        $this->id_type_contrat_concession_pivot = null;
        $this->montant_subvention_publique = null;
        $this->date_debut_execution = null;
        $this->uuid = null;
        $this->marche_innovant = null;
        $this->service_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonMarches) {
                foreach ($this->collCommonMarches as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonDonneesAnnuellesConcessions) {
                foreach ($this->collCommonDonneesAnnuellesConcessions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonEchangeDocs) {
                foreach ($this->collCommonEchangeDocs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonModificationContrats) {
                foreach ($this->collCommonModificationContrats as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTConsLotContrats) {
                foreach ($this->collCommonTConsLotContrats as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTContratTitulairesRelatedByIdContratTitulaire) {
                foreach ($this->collCommonTContratTitulairesRelatedByIdContratTitulaire as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTDonneesConsultations) {
                foreach ($this->collCommonTDonneesConsultations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonTypeContratConcessionPivot instanceof Persistent) {
              $this->aCommonTypeContratConcessionPivot->clearAllReferences($deep);
            }
            if ($this->aCommonTypeContratPivot instanceof Persistent) {
              $this->aCommonTypeContratPivot->clearAllReferences($deep);
            }
            if ($this->aCommonTypeProcedureConcessionPivot instanceof Persistent) {
              $this->aCommonTypeProcedureConcessionPivot->clearAllReferences($deep);
            }
            if ($this->aCommonTContactContrat instanceof Persistent) {
              $this->aCommonTContactContrat->clearAllReferences($deep);
            }
            if ($this->aCommonTContratTitulaireRelatedByIdContratMulti instanceof Persistent) {
              $this->aCommonTContratTitulaireRelatedByIdContratMulti->clearAllReferences($deep);
            }
            if ($this->aCommonTTypeContrat instanceof Persistent) {
              $this->aCommonTTypeContrat->clearAllReferences($deep);
            }
            if ($this->aCommonTypeProcedure instanceof Persistent) {
              $this->aCommonTypeProcedure->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonMarches instanceof PropelCollection) {
            $this->collCommonMarches->clearIterator();
        }
        $this->collCommonMarches = null;
        if ($this->collCommonDonneesAnnuellesConcessions instanceof PropelCollection) {
            $this->collCommonDonneesAnnuellesConcessions->clearIterator();
        }
        $this->collCommonDonneesAnnuellesConcessions = null;
        if ($this->collCommonEchangeDocs instanceof PropelCollection) {
            $this->collCommonEchangeDocs->clearIterator();
        }
        $this->collCommonEchangeDocs = null;
        if ($this->collCommonModificationContrats instanceof PropelCollection) {
            $this->collCommonModificationContrats->clearIterator();
        }
        $this->collCommonModificationContrats = null;
        if ($this->collCommonTConsLotContrats instanceof PropelCollection) {
            $this->collCommonTConsLotContrats->clearIterator();
        }
        $this->collCommonTConsLotContrats = null;
        if ($this->collCommonTContratTitulairesRelatedByIdContratTitulaire instanceof PropelCollection) {
            $this->collCommonTContratTitulairesRelatedByIdContratTitulaire->clearIterator();
        }
        $this->collCommonTContratTitulairesRelatedByIdContratTitulaire = null;
        if ($this->collCommonTDonneesConsultations instanceof PropelCollection) {
            $this->collCommonTDonneesConsultations->clearIterator();
        }
        $this->collCommonTDonneesConsultations = null;
        $this->aCommonTypeContratConcessionPivot = null;
        $this->aCommonTypeContratPivot = null;
        $this->aCommonTypeProcedureConcessionPivot = null;
        $this->aCommonTContactContrat = null;
        $this->aCommonTContratTitulaireRelatedByIdContratMulti = null;
        $this->aCommonTTypeContrat = null;
        $this->aCommonTypeProcedure = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTContratTitulairePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
