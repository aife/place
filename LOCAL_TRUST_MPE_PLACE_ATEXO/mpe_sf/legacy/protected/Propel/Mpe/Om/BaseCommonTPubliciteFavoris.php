<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTPubliciteFavoris;
use Application\Propel\Mpe\CommonTPubliciteFavorisPeer;
use Application\Propel\Mpe\CommonTPubliciteFavorisQuery;

/**
 * Base class that represents a row from the 't_publicite_favoris' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTPubliciteFavoris extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTPubliciteFavorisPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTPubliciteFavorisPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the organisme field.
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the old_service_id field.
     * @var        int
     */
    protected $old_service_id;

    /**
     * The value for the libelle field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $libelle;

    /**
     * The value for the regime_financier_cautionnement field.
     * @var        string
     */
    protected $regime_financier_cautionnement;

    /**
     * The value for the regime_financier_modalites_financement field.
     * @var        string
     */
    protected $regime_financier_modalites_financement;

    /**
     * The value for the acheteur_correspondant field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $acheteur_correspondant;

    /**
     * The value for the acheteur_nom_organisme field.
     * @var        string
     */
    protected $acheteur_nom_organisme;

    /**
     * The value for the acheteur_adresse field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $acheteur_adresse;

    /**
     * The value for the acheteur_cp field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $acheteur_cp;

    /**
     * The value for the acheteur_ville field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $acheteur_ville;

    /**
     * The value for the acheteur_url field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $acheteur_url;

    /**
     * The value for the facture_denomination field.
     * @var        string
     */
    protected $facture_denomination;

    /**
     * The value for the facture_adresse field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $facture_adresse;

    /**
     * The value for the facture_cp field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $facture_cp;

    /**
     * The value for the facture_ville field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $facture_ville;

    /**
     * The value for the instance_recours_organisme field.
     * @var        string
     */
    protected $instance_recours_organisme;

    /**
     * The value for the instance_recours_adresse field.
     * @var        string
     */
    protected $instance_recours_adresse;

    /**
     * The value for the instance_recours_cp field.
     * @var        string
     */
    protected $instance_recours_cp;

    /**
     * The value for the instance_recours_ville field.
     * @var        string
     */
    protected $instance_recours_ville;

    /**
     * The value for the instance_recours_url field.
     * @var        string
     */
    protected $instance_recours_url;

    /**
     * The value for the acheteur_telephone field.
     * @var        string
     */
    protected $acheteur_telephone;

    /**
     * The value for the acheteur_email field.
     * @var        string
     */
    protected $acheteur_email;

    /**
     * The value for the service_id field.
     * @var        string
     */
    protected $service_id;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->libelle = '';
        $this->acheteur_correspondant = '';
        $this->acheteur_adresse = '';
        $this->acheteur_cp = '';
        $this->acheteur_ville = '';
        $this->acheteur_url = '';
        $this->facture_adresse = '';
        $this->facture_cp = '';
        $this->facture_ville = '';
    }

    /**
     * Initializes internal state of BaseCommonTPubliciteFavoris object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [old_service_id] column value.
     *
     * @return int
     */
    public function getOldServiceId()
    {

        return $this->old_service_id;
    }

    /**
     * Get the [libelle] column value.
     *
     * @return string
     */
    public function getLibelle()
    {

        return $this->libelle;
    }

    /**
     * Get the [regime_financier_cautionnement] column value.
     *
     * @return string
     */
    public function getRegimeFinancierCautionnement()
    {

        return $this->regime_financier_cautionnement;
    }

    /**
     * Get the [regime_financier_modalites_financement] column value.
     *
     * @return string
     */
    public function getRegimeFinancierModalitesFinancement()
    {

        return $this->regime_financier_modalites_financement;
    }

    /**
     * Get the [acheteur_correspondant] column value.
     *
     * @return string
     */
    public function getAcheteurCorrespondant()
    {

        return $this->acheteur_correspondant;
    }

    /**
     * Get the [acheteur_nom_organisme] column value.
     *
     * @return string
     */
    public function getAcheteurNomOrganisme()
    {

        return $this->acheteur_nom_organisme;
    }

    /**
     * Get the [acheteur_adresse] column value.
     *
     * @return string
     */
    public function getAcheteurAdresse()
    {

        return $this->acheteur_adresse;
    }

    /**
     * Get the [acheteur_cp] column value.
     *
     * @return string
     */
    public function getAcheteurCp()
    {

        return $this->acheteur_cp;
    }

    /**
     * Get the [acheteur_ville] column value.
     *
     * @return string
     */
    public function getAcheteurVille()
    {

        return $this->acheteur_ville;
    }

    /**
     * Get the [acheteur_url] column value.
     *
     * @return string
     */
    public function getAcheteurUrl()
    {

        return $this->acheteur_url;
    }

    /**
     * Get the [facture_denomination] column value.
     *
     * @return string
     */
    public function getFactureDenomination()
    {

        return $this->facture_denomination;
    }

    /**
     * Get the [facture_adresse] column value.
     *
     * @return string
     */
    public function getFactureAdresse()
    {

        return $this->facture_adresse;
    }

    /**
     * Get the [facture_cp] column value.
     *
     * @return string
     */
    public function getFactureCp()
    {

        return $this->facture_cp;
    }

    /**
     * Get the [facture_ville] column value.
     *
     * @return string
     */
    public function getFactureVille()
    {

        return $this->facture_ville;
    }

    /**
     * Get the [instance_recours_organisme] column value.
     *
     * @return string
     */
    public function getInstanceRecoursOrganisme()
    {

        return $this->instance_recours_organisme;
    }

    /**
     * Get the [instance_recours_adresse] column value.
     *
     * @return string
     */
    public function getInstanceRecoursAdresse()
    {

        return $this->instance_recours_adresse;
    }

    /**
     * Get the [instance_recours_cp] column value.
     *
     * @return string
     */
    public function getInstanceRecoursCp()
    {

        return $this->instance_recours_cp;
    }

    /**
     * Get the [instance_recours_ville] column value.
     *
     * @return string
     */
    public function getInstanceRecoursVille()
    {

        return $this->instance_recours_ville;
    }

    /**
     * Get the [instance_recours_url] column value.
     *
     * @return string
     */
    public function getInstanceRecoursUrl()
    {

        return $this->instance_recours_url;
    }

    /**
     * Get the [acheteur_telephone] column value.
     *
     * @return string
     */
    public function getAcheteurTelephone()
    {

        return $this->acheteur_telephone;
    }

    /**
     * Get the [acheteur_email] column value.
     *
     * @return string
     */
    public function getAcheteurEmail()
    {

        return $this->acheteur_email;
    }

    /**
     * Get the [service_id] column value.
     *
     * @return string
     */
    public function getServiceId()
    {

        return $this->service_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [old_service_id] column.
     *
     * @param int $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setOldServiceId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_service_id !== $v) {
            $this->old_service_id = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::OLD_SERVICE_ID;
        }


        return $this;
    } // setOldServiceId()

    /**
     * Set the value of [libelle] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setLibelle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle !== $v) {
            $this->libelle = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::LIBELLE;
        }


        return $this;
    } // setLibelle()

    /**
     * Set the value of [regime_financier_cautionnement] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setRegimeFinancierCautionnement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->regime_financier_cautionnement !== $v) {
            $this->regime_financier_cautionnement = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::REGIME_FINANCIER_CAUTIONNEMENT;
        }


        return $this;
    } // setRegimeFinancierCautionnement()

    /**
     * Set the value of [regime_financier_modalites_financement] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setRegimeFinancierModalitesFinancement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->regime_financier_modalites_financement !== $v) {
            $this->regime_financier_modalites_financement = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::REGIME_FINANCIER_MODALITES_FINANCEMENT;
        }


        return $this;
    } // setRegimeFinancierModalitesFinancement()

    /**
     * Set the value of [acheteur_correspondant] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setAcheteurCorrespondant($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->acheteur_correspondant !== $v) {
            $this->acheteur_correspondant = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::ACHETEUR_CORRESPONDANT;
        }


        return $this;
    } // setAcheteurCorrespondant()

    /**
     * Set the value of [acheteur_nom_organisme] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setAcheteurNomOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->acheteur_nom_organisme !== $v) {
            $this->acheteur_nom_organisme = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::ACHETEUR_NOM_ORGANISME;
        }


        return $this;
    } // setAcheteurNomOrganisme()

    /**
     * Set the value of [acheteur_adresse] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setAcheteurAdresse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->acheteur_adresse !== $v) {
            $this->acheteur_adresse = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::ACHETEUR_ADRESSE;
        }


        return $this;
    } // setAcheteurAdresse()

    /**
     * Set the value of [acheteur_cp] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setAcheteurCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->acheteur_cp !== $v) {
            $this->acheteur_cp = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::ACHETEUR_CP;
        }


        return $this;
    } // setAcheteurCp()

    /**
     * Set the value of [acheteur_ville] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setAcheteurVille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->acheteur_ville !== $v) {
            $this->acheteur_ville = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::ACHETEUR_VILLE;
        }


        return $this;
    } // setAcheteurVille()

    /**
     * Set the value of [acheteur_url] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setAcheteurUrl($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->acheteur_url !== $v) {
            $this->acheteur_url = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::ACHETEUR_URL;
        }


        return $this;
    } // setAcheteurUrl()

    /**
     * Set the value of [facture_denomination] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setFactureDenomination($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->facture_denomination !== $v) {
            $this->facture_denomination = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::FACTURE_DENOMINATION;
        }


        return $this;
    } // setFactureDenomination()

    /**
     * Set the value of [facture_adresse] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setFactureAdresse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->facture_adresse !== $v) {
            $this->facture_adresse = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::FACTURE_ADRESSE;
        }


        return $this;
    } // setFactureAdresse()

    /**
     * Set the value of [facture_cp] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setFactureCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->facture_cp !== $v) {
            $this->facture_cp = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::FACTURE_CP;
        }


        return $this;
    } // setFactureCp()

    /**
     * Set the value of [facture_ville] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setFactureVille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->facture_ville !== $v) {
            $this->facture_ville = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::FACTURE_VILLE;
        }


        return $this;
    } // setFactureVille()

    /**
     * Set the value of [instance_recours_organisme] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setInstanceRecoursOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->instance_recours_organisme !== $v) {
            $this->instance_recours_organisme = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ORGANISME;
        }


        return $this;
    } // setInstanceRecoursOrganisme()

    /**
     * Set the value of [instance_recours_adresse] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setInstanceRecoursAdresse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->instance_recours_adresse !== $v) {
            $this->instance_recours_adresse = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ADRESSE;
        }


        return $this;
    } // setInstanceRecoursAdresse()

    /**
     * Set the value of [instance_recours_cp] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setInstanceRecoursCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->instance_recours_cp !== $v) {
            $this->instance_recours_cp = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_CP;
        }


        return $this;
    } // setInstanceRecoursCp()

    /**
     * Set the value of [instance_recours_ville] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setInstanceRecoursVille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->instance_recours_ville !== $v) {
            $this->instance_recours_ville = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_VILLE;
        }


        return $this;
    } // setInstanceRecoursVille()

    /**
     * Set the value of [instance_recours_url] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setInstanceRecoursUrl($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->instance_recours_url !== $v) {
            $this->instance_recours_url = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_URL;
        }


        return $this;
    } // setInstanceRecoursUrl()

    /**
     * Set the value of [acheteur_telephone] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setAcheteurTelephone($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->acheteur_telephone !== $v) {
            $this->acheteur_telephone = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::ACHETEUR_TELEPHONE;
        }


        return $this;
    } // setAcheteurTelephone()

    /**
     * Set the value of [acheteur_email] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setAcheteurEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->acheteur_email !== $v) {
            $this->acheteur_email = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::ACHETEUR_EMAIL;
        }


        return $this;
    } // setAcheteurEmail()

    /**
     * Set the value of [service_id] column.
     *
     * @param string $v new value
     * @return CommonTPubliciteFavoris The current object (for fluent API support)
     */
    public function setServiceId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->service_id !== $v) {
            $this->service_id = $v;
            $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::SERVICE_ID;
        }


        return $this;
    } // setServiceId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->libelle !== '') {
                return false;
            }

            if ($this->acheteur_correspondant !== '') {
                return false;
            }

            if ($this->acheteur_adresse !== '') {
                return false;
            }

            if ($this->acheteur_cp !== '') {
                return false;
            }

            if ($this->acheteur_ville !== '') {
                return false;
            }

            if ($this->acheteur_url !== '') {
                return false;
            }

            if ($this->facture_adresse !== '') {
                return false;
            }

            if ($this->facture_cp !== '') {
                return false;
            }

            if ($this->facture_ville !== '') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->organisme = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->old_service_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->libelle = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->regime_financier_cautionnement = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->regime_financier_modalites_financement = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->acheteur_correspondant = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->acheteur_nom_organisme = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->acheteur_adresse = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->acheteur_cp = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->acheteur_ville = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->acheteur_url = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->facture_denomination = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->facture_adresse = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->facture_cp = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->facture_ville = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->instance_recours_organisme = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->instance_recours_adresse = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->instance_recours_cp = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->instance_recours_ville = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->instance_recours_url = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->acheteur_telephone = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->acheteur_email = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->service_id = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 24; // 24 = CommonTPubliciteFavorisPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTPubliciteFavoris object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTPubliciteFavorisPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTPubliciteFavorisQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTPubliciteFavorisPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTPubliciteFavorisPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTPubliciteFavorisPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::OLD_SERVICE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`old_service_id`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::LIBELLE)) {
            $modifiedColumns[':p' . $index++]  = '`libelle`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::REGIME_FINANCIER_CAUTIONNEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`regime_financier_cautionnement`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::REGIME_FINANCIER_MODALITES_FINANCEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`regime_financier_modalites_financement`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_CORRESPONDANT)) {
            $modifiedColumns[':p' . $index++]  = '`acheteur_correspondant`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_NOM_ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`acheteur_nom_organisme`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`acheteur_adresse`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_CP)) {
            $modifiedColumns[':p' . $index++]  = '`acheteur_cp`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`acheteur_ville`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_URL)) {
            $modifiedColumns[':p' . $index++]  = '`acheteur_url`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::FACTURE_DENOMINATION)) {
            $modifiedColumns[':p' . $index++]  = '`facture_denomination`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::FACTURE_ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`facture_adresse`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::FACTURE_CP)) {
            $modifiedColumns[':p' . $index++]  = '`facture_cp`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::FACTURE_VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`facture_ville`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`instance_recours_organisme`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`instance_recours_adresse`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_CP)) {
            $modifiedColumns[':p' . $index++]  = '`instance_recours_cp`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`instance_recours_ville`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_URL)) {
            $modifiedColumns[':p' . $index++]  = '`instance_recours_url`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`acheteur_telephone`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`acheteur_email`';
        }
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::SERVICE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`service_id`';
        }

        $sql = sprintf(
            'INSERT INTO `t_publicite_favoris` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`old_service_id`':
                        $stmt->bindValue($identifier, $this->old_service_id, PDO::PARAM_INT);
                        break;
                    case '`libelle`':
                        $stmt->bindValue($identifier, $this->libelle, PDO::PARAM_STR);
                        break;
                    case '`regime_financier_cautionnement`':
                        $stmt->bindValue($identifier, $this->regime_financier_cautionnement, PDO::PARAM_STR);
                        break;
                    case '`regime_financier_modalites_financement`':
                        $stmt->bindValue($identifier, $this->regime_financier_modalites_financement, PDO::PARAM_STR);
                        break;
                    case '`acheteur_correspondant`':
                        $stmt->bindValue($identifier, $this->acheteur_correspondant, PDO::PARAM_STR);
                        break;
                    case '`acheteur_nom_organisme`':
                        $stmt->bindValue($identifier, $this->acheteur_nom_organisme, PDO::PARAM_STR);
                        break;
                    case '`acheteur_adresse`':
                        $stmt->bindValue($identifier, $this->acheteur_adresse, PDO::PARAM_STR);
                        break;
                    case '`acheteur_cp`':
                        $stmt->bindValue($identifier, $this->acheteur_cp, PDO::PARAM_STR);
                        break;
                    case '`acheteur_ville`':
                        $stmt->bindValue($identifier, $this->acheteur_ville, PDO::PARAM_STR);
                        break;
                    case '`acheteur_url`':
                        $stmt->bindValue($identifier, $this->acheteur_url, PDO::PARAM_STR);
                        break;
                    case '`facture_denomination`':
                        $stmt->bindValue($identifier, $this->facture_denomination, PDO::PARAM_STR);
                        break;
                    case '`facture_adresse`':
                        $stmt->bindValue($identifier, $this->facture_adresse, PDO::PARAM_STR);
                        break;
                    case '`facture_cp`':
                        $stmt->bindValue($identifier, $this->facture_cp, PDO::PARAM_STR);
                        break;
                    case '`facture_ville`':
                        $stmt->bindValue($identifier, $this->facture_ville, PDO::PARAM_STR);
                        break;
                    case '`instance_recours_organisme`':
                        $stmt->bindValue($identifier, $this->instance_recours_organisme, PDO::PARAM_STR);
                        break;
                    case '`instance_recours_adresse`':
                        $stmt->bindValue($identifier, $this->instance_recours_adresse, PDO::PARAM_STR);
                        break;
                    case '`instance_recours_cp`':
                        $stmt->bindValue($identifier, $this->instance_recours_cp, PDO::PARAM_STR);
                        break;
                    case '`instance_recours_ville`':
                        $stmt->bindValue($identifier, $this->instance_recours_ville, PDO::PARAM_STR);
                        break;
                    case '`instance_recours_url`':
                        $stmt->bindValue($identifier, $this->instance_recours_url, PDO::PARAM_STR);
                        break;
                    case '`acheteur_telephone`':
                        $stmt->bindValue($identifier, $this->acheteur_telephone, PDO::PARAM_STR);
                        break;
                    case '`acheteur_email`':
                        $stmt->bindValue($identifier, $this->acheteur_email, PDO::PARAM_STR);
                        break;
                    case '`service_id`':
                        $stmt->bindValue($identifier, $this->service_id, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonTPubliciteFavorisPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTPubliciteFavorisPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getOrganisme();
                break;
            case 2:
                return $this->getOldServiceId();
                break;
            case 3:
                return $this->getLibelle();
                break;
            case 4:
                return $this->getRegimeFinancierCautionnement();
                break;
            case 5:
                return $this->getRegimeFinancierModalitesFinancement();
                break;
            case 6:
                return $this->getAcheteurCorrespondant();
                break;
            case 7:
                return $this->getAcheteurNomOrganisme();
                break;
            case 8:
                return $this->getAcheteurAdresse();
                break;
            case 9:
                return $this->getAcheteurCp();
                break;
            case 10:
                return $this->getAcheteurVille();
                break;
            case 11:
                return $this->getAcheteurUrl();
                break;
            case 12:
                return $this->getFactureDenomination();
                break;
            case 13:
                return $this->getFactureAdresse();
                break;
            case 14:
                return $this->getFactureCp();
                break;
            case 15:
                return $this->getFactureVille();
                break;
            case 16:
                return $this->getInstanceRecoursOrganisme();
                break;
            case 17:
                return $this->getInstanceRecoursAdresse();
                break;
            case 18:
                return $this->getInstanceRecoursCp();
                break;
            case 19:
                return $this->getInstanceRecoursVille();
                break;
            case 20:
                return $this->getInstanceRecoursUrl();
                break;
            case 21:
                return $this->getAcheteurTelephone();
                break;
            case 22:
                return $this->getAcheteurEmail();
                break;
            case 23:
                return $this->getServiceId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['CommonTPubliciteFavoris'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTPubliciteFavoris'][$this->getPrimaryKey()] = true;
        $keys = CommonTPubliciteFavorisPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getOrganisme(),
            $keys[2] => $this->getOldServiceId(),
            $keys[3] => $this->getLibelle(),
            $keys[4] => $this->getRegimeFinancierCautionnement(),
            $keys[5] => $this->getRegimeFinancierModalitesFinancement(),
            $keys[6] => $this->getAcheteurCorrespondant(),
            $keys[7] => $this->getAcheteurNomOrganisme(),
            $keys[8] => $this->getAcheteurAdresse(),
            $keys[9] => $this->getAcheteurCp(),
            $keys[10] => $this->getAcheteurVille(),
            $keys[11] => $this->getAcheteurUrl(),
            $keys[12] => $this->getFactureDenomination(),
            $keys[13] => $this->getFactureAdresse(),
            $keys[14] => $this->getFactureCp(),
            $keys[15] => $this->getFactureVille(),
            $keys[16] => $this->getInstanceRecoursOrganisme(),
            $keys[17] => $this->getInstanceRecoursAdresse(),
            $keys[18] => $this->getInstanceRecoursCp(),
            $keys[19] => $this->getInstanceRecoursVille(),
            $keys[20] => $this->getInstanceRecoursUrl(),
            $keys[21] => $this->getAcheteurTelephone(),
            $keys[22] => $this->getAcheteurEmail(),
            $keys[23] => $this->getServiceId(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTPubliciteFavorisPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setOrganisme($value);
                break;
            case 2:
                $this->setOldServiceId($value);
                break;
            case 3:
                $this->setLibelle($value);
                break;
            case 4:
                $this->setRegimeFinancierCautionnement($value);
                break;
            case 5:
                $this->setRegimeFinancierModalitesFinancement($value);
                break;
            case 6:
                $this->setAcheteurCorrespondant($value);
                break;
            case 7:
                $this->setAcheteurNomOrganisme($value);
                break;
            case 8:
                $this->setAcheteurAdresse($value);
                break;
            case 9:
                $this->setAcheteurCp($value);
                break;
            case 10:
                $this->setAcheteurVille($value);
                break;
            case 11:
                $this->setAcheteurUrl($value);
                break;
            case 12:
                $this->setFactureDenomination($value);
                break;
            case 13:
                $this->setFactureAdresse($value);
                break;
            case 14:
                $this->setFactureCp($value);
                break;
            case 15:
                $this->setFactureVille($value);
                break;
            case 16:
                $this->setInstanceRecoursOrganisme($value);
                break;
            case 17:
                $this->setInstanceRecoursAdresse($value);
                break;
            case 18:
                $this->setInstanceRecoursCp($value);
                break;
            case 19:
                $this->setInstanceRecoursVille($value);
                break;
            case 20:
                $this->setInstanceRecoursUrl($value);
                break;
            case 21:
                $this->setAcheteurTelephone($value);
                break;
            case 22:
                $this->setAcheteurEmail($value);
                break;
            case 23:
                $this->setServiceId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTPubliciteFavorisPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setOrganisme($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setOldServiceId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setLibelle($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setRegimeFinancierCautionnement($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setRegimeFinancierModalitesFinancement($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setAcheteurCorrespondant($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setAcheteurNomOrganisme($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setAcheteurAdresse($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setAcheteurCp($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setAcheteurVille($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setAcheteurUrl($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setFactureDenomination($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setFactureAdresse($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setFactureCp($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setFactureVille($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setInstanceRecoursOrganisme($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setInstanceRecoursAdresse($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setInstanceRecoursCp($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setInstanceRecoursVille($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setInstanceRecoursUrl($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setAcheteurTelephone($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setAcheteurEmail($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setServiceId($arr[$keys[23]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTPubliciteFavorisPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ID)) $criteria->add(CommonTPubliciteFavorisPeer::ID, $this->id);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ORGANISME)) $criteria->add(CommonTPubliciteFavorisPeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::OLD_SERVICE_ID)) $criteria->add(CommonTPubliciteFavorisPeer::OLD_SERVICE_ID, $this->old_service_id);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::LIBELLE)) $criteria->add(CommonTPubliciteFavorisPeer::LIBELLE, $this->libelle);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::REGIME_FINANCIER_CAUTIONNEMENT)) $criteria->add(CommonTPubliciteFavorisPeer::REGIME_FINANCIER_CAUTIONNEMENT, $this->regime_financier_cautionnement);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::REGIME_FINANCIER_MODALITES_FINANCEMENT)) $criteria->add(CommonTPubliciteFavorisPeer::REGIME_FINANCIER_MODALITES_FINANCEMENT, $this->regime_financier_modalites_financement);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_CORRESPONDANT)) $criteria->add(CommonTPubliciteFavorisPeer::ACHETEUR_CORRESPONDANT, $this->acheteur_correspondant);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_NOM_ORGANISME)) $criteria->add(CommonTPubliciteFavorisPeer::ACHETEUR_NOM_ORGANISME, $this->acheteur_nom_organisme);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_ADRESSE)) $criteria->add(CommonTPubliciteFavorisPeer::ACHETEUR_ADRESSE, $this->acheteur_adresse);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_CP)) $criteria->add(CommonTPubliciteFavorisPeer::ACHETEUR_CP, $this->acheteur_cp);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_VILLE)) $criteria->add(CommonTPubliciteFavorisPeer::ACHETEUR_VILLE, $this->acheteur_ville);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_URL)) $criteria->add(CommonTPubliciteFavorisPeer::ACHETEUR_URL, $this->acheteur_url);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::FACTURE_DENOMINATION)) $criteria->add(CommonTPubliciteFavorisPeer::FACTURE_DENOMINATION, $this->facture_denomination);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::FACTURE_ADRESSE)) $criteria->add(CommonTPubliciteFavorisPeer::FACTURE_ADRESSE, $this->facture_adresse);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::FACTURE_CP)) $criteria->add(CommonTPubliciteFavorisPeer::FACTURE_CP, $this->facture_cp);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::FACTURE_VILLE)) $criteria->add(CommonTPubliciteFavorisPeer::FACTURE_VILLE, $this->facture_ville);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ORGANISME)) $criteria->add(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ORGANISME, $this->instance_recours_organisme);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ADRESSE)) $criteria->add(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ADRESSE, $this->instance_recours_adresse);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_CP)) $criteria->add(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_CP, $this->instance_recours_cp);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_VILLE)) $criteria->add(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_VILLE, $this->instance_recours_ville);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_URL)) $criteria->add(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_URL, $this->instance_recours_url);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_TELEPHONE)) $criteria->add(CommonTPubliciteFavorisPeer::ACHETEUR_TELEPHONE, $this->acheteur_telephone);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::ACHETEUR_EMAIL)) $criteria->add(CommonTPubliciteFavorisPeer::ACHETEUR_EMAIL, $this->acheteur_email);
        if ($this->isColumnModified(CommonTPubliciteFavorisPeer::SERVICE_ID)) $criteria->add(CommonTPubliciteFavorisPeer::SERVICE_ID, $this->service_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTPubliciteFavorisPeer::DATABASE_NAME);
        $criteria->add(CommonTPubliciteFavorisPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTPubliciteFavoris (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setOldServiceId($this->getOldServiceId());
        $copyObj->setLibelle($this->getLibelle());
        $copyObj->setRegimeFinancierCautionnement($this->getRegimeFinancierCautionnement());
        $copyObj->setRegimeFinancierModalitesFinancement($this->getRegimeFinancierModalitesFinancement());
        $copyObj->setAcheteurCorrespondant($this->getAcheteurCorrespondant());
        $copyObj->setAcheteurNomOrganisme($this->getAcheteurNomOrganisme());
        $copyObj->setAcheteurAdresse($this->getAcheteurAdresse());
        $copyObj->setAcheteurCp($this->getAcheteurCp());
        $copyObj->setAcheteurVille($this->getAcheteurVille());
        $copyObj->setAcheteurUrl($this->getAcheteurUrl());
        $copyObj->setFactureDenomination($this->getFactureDenomination());
        $copyObj->setFactureAdresse($this->getFactureAdresse());
        $copyObj->setFactureCp($this->getFactureCp());
        $copyObj->setFactureVille($this->getFactureVille());
        $copyObj->setInstanceRecoursOrganisme($this->getInstanceRecoursOrganisme());
        $copyObj->setInstanceRecoursAdresse($this->getInstanceRecoursAdresse());
        $copyObj->setInstanceRecoursCp($this->getInstanceRecoursCp());
        $copyObj->setInstanceRecoursVille($this->getInstanceRecoursVille());
        $copyObj->setInstanceRecoursUrl($this->getInstanceRecoursUrl());
        $copyObj->setAcheteurTelephone($this->getAcheteurTelephone());
        $copyObj->setAcheteurEmail($this->getAcheteurEmail());
        $copyObj->setServiceId($this->getServiceId());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTPubliciteFavoris Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTPubliciteFavorisPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTPubliciteFavorisPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->organisme = null;
        $this->old_service_id = null;
        $this->libelle = null;
        $this->regime_financier_cautionnement = null;
        $this->regime_financier_modalites_financement = null;
        $this->acheteur_correspondant = null;
        $this->acheteur_nom_organisme = null;
        $this->acheteur_adresse = null;
        $this->acheteur_cp = null;
        $this->acheteur_ville = null;
        $this->acheteur_url = null;
        $this->facture_denomination = null;
        $this->facture_adresse = null;
        $this->facture_cp = null;
        $this->facture_ville = null;
        $this->instance_recours_organisme = null;
        $this->instance_recours_adresse = null;
        $this->instance_recours_cp = null;
        $this->instance_recours_ville = null;
        $this->instance_recours_url = null;
        $this->acheteur_telephone = null;
        $this->acheteur_email = null;
        $this->service_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTPubliciteFavorisPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
