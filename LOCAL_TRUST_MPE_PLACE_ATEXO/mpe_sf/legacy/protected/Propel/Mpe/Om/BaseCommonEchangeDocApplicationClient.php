<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonEchangeDoc;
use Application\Propel\Mpe\CommonEchangeDocApplication;
use Application\Propel\Mpe\CommonEchangeDocApplicationClient;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientOrganisme;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientOrganismeQuery;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientPeer;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientQuery;
use Application\Propel\Mpe\CommonEchangeDocApplicationQuery;
use Application\Propel\Mpe\CommonEchangeDocQuery;

/**
 * Base class that represents a row from the 'echange_doc_application_client' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangeDocApplicationClient extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonEchangeDocApplicationClientPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonEchangeDocApplicationClientPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the echange_doc_application_id field.
     * @var        int
     */
    protected $echange_doc_application_id;

    /**
     * The value for the code field.
     * @var        string
     */
    protected $code;

    /**
     * The value for the libelle field.
     * @var        string
     */
    protected $libelle;

    /**
     * The value for the actif field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $actif;

    /**
     * The value for the cheminement_signature field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $cheminement_signature;

    /**
     * The value for the cheminement_ged field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $cheminement_ged;

    /**
     * The value for the cheminement_sae field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $cheminement_sae;

    /**
     * The value for the cheminement_tdt field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $cheminement_tdt;

    /**
     * The value for the classification_1 field.
     * @var        string
     */
    protected $classification_1;

    /**
     * The value for the classification_2 field.
     * @var        string
     */
    protected $classification_2;

    /**
     * The value for the classification_3 field.
     * @var        string
     */
    protected $classification_3;

    /**
     * The value for the classification_4 field.
     * @var        string
     */
    protected $classification_4;

    /**
     * The value for the classification_5 field.
     * @var        string
     */
    protected $classification_5;

    /**
     * The value for the multi_docs_principaux field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $multi_docs_principaux;

    /**
     * The value for the envoi_auto_archivage field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $envoi_auto_archivage;

    /**
     * @var        CommonEchangeDocApplication
     */
    protected $aCommonEchangeDocApplication;

    /**
     * @var        PropelObjectCollection|CommonEchangeDoc[] Collection to store aggregation of CommonEchangeDoc objects.
     */
    protected $collCommonEchangeDocs;
    protected $collCommonEchangeDocsPartial;

    /**
     * @var        PropelObjectCollection|CommonEchangeDocApplicationClientOrganisme[] Collection to store aggregation of CommonEchangeDocApplicationClientOrganisme objects.
     */
    protected $collCommonEchangeDocApplicationClientOrganismes;
    protected $collCommonEchangeDocApplicationClientOrganismesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangeDocsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangeDocApplicationClientOrganismesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->actif = true;
        $this->cheminement_signature = false;
        $this->cheminement_ged = false;
        $this->cheminement_sae = false;
        $this->cheminement_tdt = false;
        $this->multi_docs_principaux = false;
        $this->envoi_auto_archivage = false;
    }

    /**
     * Initializes internal state of BaseCommonEchangeDocApplicationClient object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [echange_doc_application_id] column value.
     *
     * @return int
     */
    public function getEchangeDocApplicationId()
    {

        return $this->echange_doc_application_id;
    }

    /**
     * Get the [code] column value.
     *
     * @return string
     */
    public function getCode()
    {

        return $this->code;
    }

    /**
     * Get the [libelle] column value.
     *
     * @return string
     */
    public function getLibelle()
    {

        return $this->libelle;
    }

    /**
     * Get the [actif] column value.
     *
     * @return boolean
     */
    public function getActif()
    {

        return $this->actif;
    }

    /**
     * Get the [cheminement_signature] column value.
     *
     * @return boolean
     */
    public function getCheminementSignature()
    {

        return $this->cheminement_signature;
    }

    /**
     * Get the [cheminement_ged] column value.
     *
     * @return boolean
     */
    public function getCheminementGed()
    {

        return $this->cheminement_ged;
    }

    /**
     * Get the [cheminement_sae] column value.
     *
     * @return boolean
     */
    public function getCheminementSae()
    {

        return $this->cheminement_sae;
    }

    /**
     * Get the [cheminement_tdt] column value.
     *
     * @return boolean
     */
    public function getCheminementTdt()
    {

        return $this->cheminement_tdt;
    }

    /**
     * Get the [classification_1] column value.
     *
     * @return string
     */
    public function getClassification1()
    {

        return $this->classification_1;
    }

    /**
     * Get the [classification_2] column value.
     *
     * @return string
     */
    public function getClassification2()
    {

        return $this->classification_2;
    }

    /**
     * Get the [classification_3] column value.
     *
     * @return string
     */
    public function getClassification3()
    {

        return $this->classification_3;
    }

    /**
     * Get the [classification_4] column value.
     *
     * @return string
     */
    public function getClassification4()
    {

        return $this->classification_4;
    }

    /**
     * Get the [classification_5] column value.
     *
     * @return string
     */
    public function getClassification5()
    {

        return $this->classification_5;
    }

    /**
     * Get the [multi_docs_principaux] column value.
     *
     * @return boolean
     */
    public function getMultiDocsPrincipaux()
    {

        return $this->multi_docs_principaux;
    }

    /**
     * Get the [envoi_auto_archivage] column value.
     *
     * @return boolean
     */
    public function getEnvoiAutoArchivage()
    {

        return $this->envoi_auto_archivage;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [echange_doc_application_id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setEchangeDocApplicationId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->echange_doc_application_id !== $v) {
            $this->echange_doc_application_id = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID;
        }

        if ($this->aCommonEchangeDocApplication !== null && $this->aCommonEchangeDocApplication->getId() !== $v) {
            $this->aCommonEchangeDocApplication = null;
        }


        return $this;
    } // setEchangeDocApplicationId()

    /**
     * Set the value of [code] column.
     *
     * @param string $v new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::CODE;
        }


        return $this;
    } // setCode()

    /**
     * Set the value of [libelle] column.
     *
     * @param string $v new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setLibelle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle !== $v) {
            $this->libelle = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::LIBELLE;
        }


        return $this;
    } // setLibelle()

    /**
     * Sets the value of the [actif] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setActif($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->actif !== $v) {
            $this->actif = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::ACTIF;
        }


        return $this;
    } // setActif()

    /**
     * Sets the value of the [cheminement_signature] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setCheminementSignature($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cheminement_signature !== $v) {
            $this->cheminement_signature = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SIGNATURE;
        }


        return $this;
    } // setCheminementSignature()

    /**
     * Sets the value of the [cheminement_ged] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setCheminementGed($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cheminement_ged !== $v) {
            $this->cheminement_ged = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::CHEMINEMENT_GED;
        }


        return $this;
    } // setCheminementGed()

    /**
     * Sets the value of the [cheminement_sae] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setCheminementSae($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cheminement_sae !== $v) {
            $this->cheminement_sae = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SAE;
        }


        return $this;
    } // setCheminementSae()

    /**
     * Sets the value of the [cheminement_tdt] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setCheminementTdt($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->cheminement_tdt !== $v) {
            $this->cheminement_tdt = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::CHEMINEMENT_TDT;
        }


        return $this;
    } // setCheminementTdt()

    /**
     * Set the value of [classification_1] column.
     *
     * @param string $v new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setClassification1($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->classification_1 !== $v) {
            $this->classification_1 = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::CLASSIFICATION_1;
        }


        return $this;
    } // setClassification1()

    /**
     * Set the value of [classification_2] column.
     *
     * @param string $v new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setClassification2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->classification_2 !== $v) {
            $this->classification_2 = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::CLASSIFICATION_2;
        }


        return $this;
    } // setClassification2()

    /**
     * Set the value of [classification_3] column.
     *
     * @param string $v new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setClassification3($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->classification_3 !== $v) {
            $this->classification_3 = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::CLASSIFICATION_3;
        }


        return $this;
    } // setClassification3()

    /**
     * Set the value of [classification_4] column.
     *
     * @param string $v new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setClassification4($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->classification_4 !== $v) {
            $this->classification_4 = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::CLASSIFICATION_4;
        }


        return $this;
    } // setClassification4()

    /**
     * Set the value of [classification_5] column.
     *
     * @param string $v new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setClassification5($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->classification_5 !== $v) {
            $this->classification_5 = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::CLASSIFICATION_5;
        }


        return $this;
    } // setClassification5()

    /**
     * Sets the value of the [multi_docs_principaux] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setMultiDocsPrincipaux($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->multi_docs_principaux !== $v) {
            $this->multi_docs_principaux = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::MULTI_DOCS_PRINCIPAUX;
        }


        return $this;
    } // setMultiDocsPrincipaux()

    /**
     * Sets the value of the [envoi_auto_archivage] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setEnvoiAutoArchivage($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->envoi_auto_archivage !== $v) {
            $this->envoi_auto_archivage = $v;
            $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::ENVOI_AUTO_ARCHIVAGE;
        }


        return $this;
    } // setEnvoiAutoArchivage()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->actif !== true) {
                return false;
            }

            if ($this->cheminement_signature !== false) {
                return false;
            }

            if ($this->cheminement_ged !== false) {
                return false;
            }

            if ($this->cheminement_sae !== false) {
                return false;
            }

            if ($this->cheminement_tdt !== false) {
                return false;
            }

            if ($this->multi_docs_principaux !== false) {
                return false;
            }

            if ($this->envoi_auto_archivage !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->echange_doc_application_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->code = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->libelle = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->actif = ($row[$startcol + 4] !== null) ? (boolean) $row[$startcol + 4] : null;
            $this->cheminement_signature = ($row[$startcol + 5] !== null) ? (boolean) $row[$startcol + 5] : null;
            $this->cheminement_ged = ($row[$startcol + 6] !== null) ? (boolean) $row[$startcol + 6] : null;
            $this->cheminement_sae = ($row[$startcol + 7] !== null) ? (boolean) $row[$startcol + 7] : null;
            $this->cheminement_tdt = ($row[$startcol + 8] !== null) ? (boolean) $row[$startcol + 8] : null;
            $this->classification_1 = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->classification_2 = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->classification_3 = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->classification_4 = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->classification_5 = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->multi_docs_principaux = ($row[$startcol + 14] !== null) ? (boolean) $row[$startcol + 14] : null;
            $this->envoi_auto_archivage = ($row[$startcol + 15] !== null) ? (boolean) $row[$startcol + 15] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 16; // 16 = CommonEchangeDocApplicationClientPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonEchangeDocApplicationClient object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonEchangeDocApplication !== null && $this->echange_doc_application_id !== $this->aCommonEchangeDocApplication->getId()) {
            $this->aCommonEchangeDocApplication = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonEchangeDocApplicationClientPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonEchangeDocApplication = null;
            $this->collCommonEchangeDocs = null;

            $this->collCommonEchangeDocApplicationClientOrganismes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonEchangeDocApplicationClientQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonEchangeDocApplicationClientPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonEchangeDocApplication !== null) {
                if ($this->aCommonEchangeDocApplication->isModified() || $this->aCommonEchangeDocApplication->isNew()) {
                    $affectedRows += $this->aCommonEchangeDocApplication->save($con);
                }
                $this->setCommonEchangeDocApplication($this->aCommonEchangeDocApplication);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonEchangeDocsScheduledForDeletion !== null) {
                if (!$this->commonEchangeDocsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangeDocsScheduledForDeletion as $commonEchangeDoc) {
                        // need to save related object because we set the relation to null
                        $commonEchangeDoc->save($con);
                    }
                    $this->commonEchangeDocsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchangeDocs !== null) {
                foreach ($this->collCommonEchangeDocs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion !== null) {
                if (!$this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion as $commonEchangeDocApplicationClientOrganisme) {
                        // need to save related object because we set the relation to null
                        $commonEchangeDocApplicationClientOrganisme->save($con);
                    }
                    $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchangeDocApplicationClientOrganismes !== null) {
                foreach ($this->collCommonEchangeDocApplicationClientOrganismes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonEchangeDocApplicationClientPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonEchangeDocApplicationClientPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`echange_doc_application_id`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CODE)) {
            $modifiedColumns[':p' . $index++]  = '`code`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::LIBELLE)) {
            $modifiedColumns[':p' . $index++]  = '`libelle`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::ACTIF)) {
            $modifiedColumns[':p' . $index++]  = '`actif`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SIGNATURE)) {
            $modifiedColumns[':p' . $index++]  = '`cheminement_signature`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_GED)) {
            $modifiedColumns[':p' . $index++]  = '`cheminement_ged`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SAE)) {
            $modifiedColumns[':p' . $index++]  = '`cheminement_sae`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_TDT)) {
            $modifiedColumns[':p' . $index++]  = '`cheminement_tdt`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_1)) {
            $modifiedColumns[':p' . $index++]  = '`classification_1`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_2)) {
            $modifiedColumns[':p' . $index++]  = '`classification_2`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_3)) {
            $modifiedColumns[':p' . $index++]  = '`classification_3`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_4)) {
            $modifiedColumns[':p' . $index++]  = '`classification_4`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_5)) {
            $modifiedColumns[':p' . $index++]  = '`classification_5`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::MULTI_DOCS_PRINCIPAUX)) {
            $modifiedColumns[':p' . $index++]  = '`multi_docs_principaux`';
        }
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::ENVOI_AUTO_ARCHIVAGE)) {
            $modifiedColumns[':p' . $index++]  = '`envoi_auto_archivage`';
        }

        $sql = sprintf(
            'INSERT INTO `echange_doc_application_client` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`echange_doc_application_id`':
                        $stmt->bindValue($identifier, $this->echange_doc_application_id, PDO::PARAM_INT);
                        break;
                    case '`code`':
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case '`libelle`':
                        $stmt->bindValue($identifier, $this->libelle, PDO::PARAM_STR);
                        break;
                    case '`actif`':
                        $stmt->bindValue($identifier, (int) $this->actif, PDO::PARAM_INT);
                        break;
                    case '`cheminement_signature`':
                        $stmt->bindValue($identifier, (int) $this->cheminement_signature, PDO::PARAM_INT);
                        break;
                    case '`cheminement_ged`':
                        $stmt->bindValue($identifier, (int) $this->cheminement_ged, PDO::PARAM_INT);
                        break;
                    case '`cheminement_sae`':
                        $stmt->bindValue($identifier, (int) $this->cheminement_sae, PDO::PARAM_INT);
                        break;
                    case '`cheminement_tdt`':
                        $stmt->bindValue($identifier, (int) $this->cheminement_tdt, PDO::PARAM_INT);
                        break;
                    case '`classification_1`':
                        $stmt->bindValue($identifier, $this->classification_1, PDO::PARAM_STR);
                        break;
                    case '`classification_2`':
                        $stmt->bindValue($identifier, $this->classification_2, PDO::PARAM_STR);
                        break;
                    case '`classification_3`':
                        $stmt->bindValue($identifier, $this->classification_3, PDO::PARAM_STR);
                        break;
                    case '`classification_4`':
                        $stmt->bindValue($identifier, $this->classification_4, PDO::PARAM_STR);
                        break;
                    case '`classification_5`':
                        $stmt->bindValue($identifier, $this->classification_5, PDO::PARAM_STR);
                        break;
                    case '`multi_docs_principaux`':
                        $stmt->bindValue($identifier, (int) $this->multi_docs_principaux, PDO::PARAM_INT);
                        break;
                    case '`envoi_auto_archivage`':
                        $stmt->bindValue($identifier, (int) $this->envoi_auto_archivage, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonEchangeDocApplication !== null) {
                if (!$this->aCommonEchangeDocApplication->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonEchangeDocApplication->getValidationFailures());
                }
            }


            if (($retval = CommonEchangeDocApplicationClientPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonEchangeDocs !== null) {
                    foreach ($this->collCommonEchangeDocs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonEchangeDocApplicationClientOrganismes !== null) {
                    foreach ($this->collCommonEchangeDocApplicationClientOrganismes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonEchangeDocApplicationClientPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getEchangeDocApplicationId();
                break;
            case 2:
                return $this->getCode();
                break;
            case 3:
                return $this->getLibelle();
                break;
            case 4:
                return $this->getActif();
                break;
            case 5:
                return $this->getCheminementSignature();
                break;
            case 6:
                return $this->getCheminementGed();
                break;
            case 7:
                return $this->getCheminementSae();
                break;
            case 8:
                return $this->getCheminementTdt();
                break;
            case 9:
                return $this->getClassification1();
                break;
            case 10:
                return $this->getClassification2();
                break;
            case 11:
                return $this->getClassification3();
                break;
            case 12:
                return $this->getClassification4();
                break;
            case 13:
                return $this->getClassification5();
                break;
            case 14:
                return $this->getMultiDocsPrincipaux();
                break;
            case 15:
                return $this->getEnvoiAutoArchivage();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonEchangeDocApplicationClient'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonEchangeDocApplicationClient'][$this->getPrimaryKey()] = true;
        $keys = CommonEchangeDocApplicationClientPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getEchangeDocApplicationId(),
            $keys[2] => $this->getCode(),
            $keys[3] => $this->getLibelle(),
            $keys[4] => $this->getActif(),
            $keys[5] => $this->getCheminementSignature(),
            $keys[6] => $this->getCheminementGed(),
            $keys[7] => $this->getCheminementSae(),
            $keys[8] => $this->getCheminementTdt(),
            $keys[9] => $this->getClassification1(),
            $keys[10] => $this->getClassification2(),
            $keys[11] => $this->getClassification3(),
            $keys[12] => $this->getClassification4(),
            $keys[13] => $this->getClassification5(),
            $keys[14] => $this->getMultiDocsPrincipaux(),
            $keys[15] => $this->getEnvoiAutoArchivage(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonEchangeDocApplication) {
                $result['CommonEchangeDocApplication'] = $this->aCommonEchangeDocApplication->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonEchangeDocs) {
                $result['CommonEchangeDocs'] = $this->collCommonEchangeDocs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonEchangeDocApplicationClientOrganismes) {
                $result['CommonEchangeDocApplicationClientOrganismes'] = $this->collCommonEchangeDocApplicationClientOrganismes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonEchangeDocApplicationClientPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setEchangeDocApplicationId($value);
                break;
            case 2:
                $this->setCode($value);
                break;
            case 3:
                $this->setLibelle($value);
                break;
            case 4:
                $this->setActif($value);
                break;
            case 5:
                $this->setCheminementSignature($value);
                break;
            case 6:
                $this->setCheminementGed($value);
                break;
            case 7:
                $this->setCheminementSae($value);
                break;
            case 8:
                $this->setCheminementTdt($value);
                break;
            case 9:
                $this->setClassification1($value);
                break;
            case 10:
                $this->setClassification2($value);
                break;
            case 11:
                $this->setClassification3($value);
                break;
            case 12:
                $this->setClassification4($value);
                break;
            case 13:
                $this->setClassification5($value);
                break;
            case 14:
                $this->setMultiDocsPrincipaux($value);
                break;
            case 15:
                $this->setEnvoiAutoArchivage($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonEchangeDocApplicationClientPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setEchangeDocApplicationId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCode($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setLibelle($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setActif($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setCheminementSignature($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCheminementGed($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setCheminementSae($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setCheminementTdt($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setClassification1($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setClassification2($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setClassification3($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setClassification4($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setClassification5($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setMultiDocsPrincipaux($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setEnvoiAutoArchivage($arr[$keys[15]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::ID)) $criteria->add(CommonEchangeDocApplicationClientPeer::ID, $this->id);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID)) $criteria->add(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID, $this->echange_doc_application_id);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CODE)) $criteria->add(CommonEchangeDocApplicationClientPeer::CODE, $this->code);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::LIBELLE)) $criteria->add(CommonEchangeDocApplicationClientPeer::LIBELLE, $this->libelle);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::ACTIF)) $criteria->add(CommonEchangeDocApplicationClientPeer::ACTIF, $this->actif);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SIGNATURE)) $criteria->add(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SIGNATURE, $this->cheminement_signature);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_GED)) $criteria->add(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_GED, $this->cheminement_ged);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SAE)) $criteria->add(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SAE, $this->cheminement_sae);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_TDT)) $criteria->add(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_TDT, $this->cheminement_tdt);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_1)) $criteria->add(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_1, $this->classification_1);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_2)) $criteria->add(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_2, $this->classification_2);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_3)) $criteria->add(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_3, $this->classification_3);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_4)) $criteria->add(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_4, $this->classification_4);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_5)) $criteria->add(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_5, $this->classification_5);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::MULTI_DOCS_PRINCIPAUX)) $criteria->add(CommonEchangeDocApplicationClientPeer::MULTI_DOCS_PRINCIPAUX, $this->multi_docs_principaux);
        if ($this->isColumnModified(CommonEchangeDocApplicationClientPeer::ENVOI_AUTO_ARCHIVAGE)) $criteria->add(CommonEchangeDocApplicationClientPeer::ENVOI_AUTO_ARCHIVAGE, $this->envoi_auto_archivage);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);
        $criteria->add(CommonEchangeDocApplicationClientPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonEchangeDocApplicationClient (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setEchangeDocApplicationId($this->getEchangeDocApplicationId());
        $copyObj->setCode($this->getCode());
        $copyObj->setLibelle($this->getLibelle());
        $copyObj->setActif($this->getActif());
        $copyObj->setCheminementSignature($this->getCheminementSignature());
        $copyObj->setCheminementGed($this->getCheminementGed());
        $copyObj->setCheminementSae($this->getCheminementSae());
        $copyObj->setCheminementTdt($this->getCheminementTdt());
        $copyObj->setClassification1($this->getClassification1());
        $copyObj->setClassification2($this->getClassification2());
        $copyObj->setClassification3($this->getClassification3());
        $copyObj->setClassification4($this->getClassification4());
        $copyObj->setClassification5($this->getClassification5());
        $copyObj->setMultiDocsPrincipaux($this->getMultiDocsPrincipaux());
        $copyObj->setEnvoiAutoArchivage($this->getEnvoiAutoArchivage());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonEchangeDocs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchangeDoc($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonEchangeDocApplicationClientOrganismes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchangeDocApplicationClientOrganisme($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonEchangeDocApplicationClient Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonEchangeDocApplicationClientPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonEchangeDocApplicationClientPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonEchangeDocApplication object.
     *
     * @param   CommonEchangeDocApplication $v
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonEchangeDocApplication(CommonEchangeDocApplication $v = null)
    {
        if ($v === null) {
            $this->setEchangeDocApplicationId(NULL);
        } else {
            $this->setEchangeDocApplicationId($v->getId());
        }

        $this->aCommonEchangeDocApplication = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonEchangeDocApplication object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonEchangeDocApplicationClient($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonEchangeDocApplication object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonEchangeDocApplication The associated CommonEchangeDocApplication object.
     * @throws PropelException
     */
    public function getCommonEchangeDocApplication(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonEchangeDocApplication === null && ($this->echange_doc_application_id !== null) && $doQuery) {
            $this->aCommonEchangeDocApplication = CommonEchangeDocApplicationQuery::create()->findPk($this->echange_doc_application_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonEchangeDocApplication->addCommonEchangeDocApplicationClients($this);
             */
        }

        return $this->aCommonEchangeDocApplication;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonEchangeDoc' == $relationName) {
            $this->initCommonEchangeDocs();
        }
        if ('CommonEchangeDocApplicationClientOrganisme' == $relationName) {
            $this->initCommonEchangeDocApplicationClientOrganismes();
        }
    }

    /**
     * Clears out the collCommonEchangeDocs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     * @see        addCommonEchangeDocs()
     */
    public function clearCommonEchangeDocs()
    {
        $this->collCommonEchangeDocs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangeDocsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchangeDocs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchangeDocs($v = true)
    {
        $this->collCommonEchangeDocsPartial = $v;
    }

    /**
     * Initializes the collCommonEchangeDocs collection.
     *
     * By default this just sets the collCommonEchangeDocs collection to an empty array (like clearcollCommonEchangeDocs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchangeDocs($overrideExisting = true)
    {
        if (null !== $this->collCommonEchangeDocs && !$overrideExisting) {
            return;
        }
        $this->collCommonEchangeDocs = new PropelObjectCollection();
        $this->collCommonEchangeDocs->setModel('CommonEchangeDoc');
    }

    /**
     * Gets an array of CommonEchangeDoc objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonEchangeDocApplicationClient is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     * @throws PropelException
     */
    public function getCommonEchangeDocs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocsPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocs) {
                // return empty collection
                $this->initCommonEchangeDocs();
            } else {
                $collCommonEchangeDocs = CommonEchangeDocQuery::create(null, $criteria)
                    ->filterByCommonEchangeDocApplicationClient($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangeDocsPartial && count($collCommonEchangeDocs)) {
                      $this->initCommonEchangeDocs(false);

                      foreach ($collCommonEchangeDocs as $obj) {
                        if (false == $this->collCommonEchangeDocs->contains($obj)) {
                          $this->collCommonEchangeDocs->append($obj);
                        }
                      }

                      $this->collCommonEchangeDocsPartial = true;
                    }

                    $collCommonEchangeDocs->getInternalIterator()->rewind();

                    return $collCommonEchangeDocs;
                }

                if ($partial && $this->collCommonEchangeDocs) {
                    foreach ($this->collCommonEchangeDocs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchangeDocs[] = $obj;
                        }
                    }
                }

                $this->collCommonEchangeDocs = $collCommonEchangeDocs;
                $this->collCommonEchangeDocsPartial = false;
            }
        }

        return $this->collCommonEchangeDocs;
    }

    /**
     * Sets a collection of CommonEchangeDoc objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchangeDocs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setCommonEchangeDocs(PropelCollection $commonEchangeDocs, PropelPDO $con = null)
    {
        $commonEchangeDocsToDelete = $this->getCommonEchangeDocs(new Criteria(), $con)->diff($commonEchangeDocs);


        $this->commonEchangeDocsScheduledForDeletion = $commonEchangeDocsToDelete;

        foreach ($commonEchangeDocsToDelete as $commonEchangeDocRemoved) {
            $commonEchangeDocRemoved->setCommonEchangeDocApplicationClient(null);
        }

        $this->collCommonEchangeDocs = null;
        foreach ($commonEchangeDocs as $commonEchangeDoc) {
            $this->addCommonEchangeDoc($commonEchangeDoc);
        }

        $this->collCommonEchangeDocs = $commonEchangeDocs;
        $this->collCommonEchangeDocsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchangeDoc objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchangeDoc objects.
     * @throws PropelException
     */
    public function countCommonEchangeDocs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocsPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchangeDocs());
            }
            $query = CommonEchangeDocQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonEchangeDocApplicationClient($this)
                ->count($con);
        }

        return count($this->collCommonEchangeDocs);
    }

    /**
     * Method called to associate a CommonEchangeDoc object to this object
     * through the CommonEchangeDoc foreign key attribute.
     *
     * @param   CommonEchangeDoc $l CommonEchangeDoc
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function addCommonEchangeDoc(CommonEchangeDoc $l)
    {
        if ($this->collCommonEchangeDocs === null) {
            $this->initCommonEchangeDocs();
            $this->collCommonEchangeDocsPartial = true;
        }
        if (!in_array($l, $this->collCommonEchangeDocs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchangeDoc($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchangeDoc $commonEchangeDoc The commonEchangeDoc object to add.
     */
    protected function doAddCommonEchangeDoc($commonEchangeDoc)
    {
        $this->collCommonEchangeDocs[]= $commonEchangeDoc;
        $commonEchangeDoc->setCommonEchangeDocApplicationClient($this);
    }

    /**
     * @param	CommonEchangeDoc $commonEchangeDoc The commonEchangeDoc object to remove.
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function removeCommonEchangeDoc($commonEchangeDoc)
    {
        if ($this->getCommonEchangeDocs()->contains($commonEchangeDoc)) {
            $this->collCommonEchangeDocs->remove($this->collCommonEchangeDocs->search($commonEchangeDoc));
            if (null === $this->commonEchangeDocsScheduledForDeletion) {
                $this->commonEchangeDocsScheduledForDeletion = clone $this->collCommonEchangeDocs;
                $this->commonEchangeDocsScheduledForDeletion->clear();
            }
            $this->commonEchangeDocsScheduledForDeletion[]= $commonEchangeDoc;
            $commonEchangeDoc->setCommonEchangeDocApplicationClient(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonEchangeDocApplicationClient is new, it will return
     * an empty collection; or if this CommonEchangeDocApplicationClient has previously
     * been saved, it will retrieve related CommonEchangeDocs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonEchangeDocApplicationClient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     */
    public function getCommonEchangeDocsJoinCommonTContratTitulaire($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocQuery::create(null, $criteria);
        $query->joinWith('CommonTContratTitulaire', $join_behavior);

        return $this->getCommonEchangeDocs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonEchangeDocApplicationClient is new, it will return
     * an empty collection; or if this CommonEchangeDocApplicationClient has previously
     * been saved, it will retrieve related CommonEchangeDocs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonEchangeDocApplicationClient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     */
    public function getCommonEchangeDocsJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonEchangeDocs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonEchangeDocApplicationClient is new, it will return
     * an empty collection; or if this CommonEchangeDocApplicationClient has previously
     * been saved, it will retrieve related CommonEchangeDocs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonEchangeDocApplicationClient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     */
    public function getCommonEchangeDocsJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonEchangeDocs($query, $con);
    }

    /**
     * Clears out the collCommonEchangeDocApplicationClientOrganismes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     * @see        addCommonEchangeDocApplicationClientOrganismes()
     */
    public function clearCommonEchangeDocApplicationClientOrganismes()
    {
        $this->collCommonEchangeDocApplicationClientOrganismes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangeDocApplicationClientOrganismesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchangeDocApplicationClientOrganismes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchangeDocApplicationClientOrganismes($v = true)
    {
        $this->collCommonEchangeDocApplicationClientOrganismesPartial = $v;
    }

    /**
     * Initializes the collCommonEchangeDocApplicationClientOrganismes collection.
     *
     * By default this just sets the collCommonEchangeDocApplicationClientOrganismes collection to an empty array (like clearcollCommonEchangeDocApplicationClientOrganismes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchangeDocApplicationClientOrganismes($overrideExisting = true)
    {
        if (null !== $this->collCommonEchangeDocApplicationClientOrganismes && !$overrideExisting) {
            return;
        }
        $this->collCommonEchangeDocApplicationClientOrganismes = new PropelObjectCollection();
        $this->collCommonEchangeDocApplicationClientOrganismes->setModel('CommonEchangeDocApplicationClientOrganisme');
    }

    /**
     * Gets an array of CommonEchangeDocApplicationClientOrganisme objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonEchangeDocApplicationClient is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchangeDocApplicationClientOrganisme[] List of CommonEchangeDocApplicationClientOrganisme objects
     * @throws PropelException
     */
    public function getCommonEchangeDocApplicationClientOrganismes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocApplicationClientOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocApplicationClientOrganismes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocApplicationClientOrganismes) {
                // return empty collection
                $this->initCommonEchangeDocApplicationClientOrganismes();
            } else {
                $collCommonEchangeDocApplicationClientOrganismes = CommonEchangeDocApplicationClientOrganismeQuery::create(null, $criteria)
                    ->filterByCommonEchangeDocApplicationClient($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangeDocApplicationClientOrganismesPartial && count($collCommonEchangeDocApplicationClientOrganismes)) {
                      $this->initCommonEchangeDocApplicationClientOrganismes(false);

                      foreach ($collCommonEchangeDocApplicationClientOrganismes as $obj) {
                        if (false == $this->collCommonEchangeDocApplicationClientOrganismes->contains($obj)) {
                          $this->collCommonEchangeDocApplicationClientOrganismes->append($obj);
                        }
                      }

                      $this->collCommonEchangeDocApplicationClientOrganismesPartial = true;
                    }

                    $collCommonEchangeDocApplicationClientOrganismes->getInternalIterator()->rewind();

                    return $collCommonEchangeDocApplicationClientOrganismes;
                }

                if ($partial && $this->collCommonEchangeDocApplicationClientOrganismes) {
                    foreach ($this->collCommonEchangeDocApplicationClientOrganismes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchangeDocApplicationClientOrganismes[] = $obj;
                        }
                    }
                }

                $this->collCommonEchangeDocApplicationClientOrganismes = $collCommonEchangeDocApplicationClientOrganismes;
                $this->collCommonEchangeDocApplicationClientOrganismesPartial = false;
            }
        }

        return $this->collCommonEchangeDocApplicationClientOrganismes;
    }

    /**
     * Sets a collection of CommonEchangeDocApplicationClientOrganisme objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchangeDocApplicationClientOrganismes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function setCommonEchangeDocApplicationClientOrganismes(PropelCollection $commonEchangeDocApplicationClientOrganismes, PropelPDO $con = null)
    {
        $commonEchangeDocApplicationClientOrganismesToDelete = $this->getCommonEchangeDocApplicationClientOrganismes(new Criteria(), $con)->diff($commonEchangeDocApplicationClientOrganismes);


        $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion = $commonEchangeDocApplicationClientOrganismesToDelete;

        foreach ($commonEchangeDocApplicationClientOrganismesToDelete as $commonEchangeDocApplicationClientOrganismeRemoved) {
            $commonEchangeDocApplicationClientOrganismeRemoved->setCommonEchangeDocApplicationClient(null);
        }

        $this->collCommonEchangeDocApplicationClientOrganismes = null;
        foreach ($commonEchangeDocApplicationClientOrganismes as $commonEchangeDocApplicationClientOrganisme) {
            $this->addCommonEchangeDocApplicationClientOrganisme($commonEchangeDocApplicationClientOrganisme);
        }

        $this->collCommonEchangeDocApplicationClientOrganismes = $commonEchangeDocApplicationClientOrganismes;
        $this->collCommonEchangeDocApplicationClientOrganismesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchangeDocApplicationClientOrganisme objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchangeDocApplicationClientOrganisme objects.
     * @throws PropelException
     */
    public function countCommonEchangeDocApplicationClientOrganismes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocApplicationClientOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocApplicationClientOrganismes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocApplicationClientOrganismes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchangeDocApplicationClientOrganismes());
            }
            $query = CommonEchangeDocApplicationClientOrganismeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonEchangeDocApplicationClient($this)
                ->count($con);
        }

        return count($this->collCommonEchangeDocApplicationClientOrganismes);
    }

    /**
     * Method called to associate a CommonEchangeDocApplicationClientOrganisme object to this object
     * through the CommonEchangeDocApplicationClientOrganisme foreign key attribute.
     *
     * @param   CommonEchangeDocApplicationClientOrganisme $l CommonEchangeDocApplicationClientOrganisme
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function addCommonEchangeDocApplicationClientOrganisme(CommonEchangeDocApplicationClientOrganisme $l)
    {
        if ($this->collCommonEchangeDocApplicationClientOrganismes === null) {
            $this->initCommonEchangeDocApplicationClientOrganismes();
            $this->collCommonEchangeDocApplicationClientOrganismesPartial = true;
        }
        if (!in_array($l, $this->collCommonEchangeDocApplicationClientOrganismes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchangeDocApplicationClientOrganisme($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchangeDocApplicationClientOrganisme $commonEchangeDocApplicationClientOrganisme The commonEchangeDocApplicationClientOrganisme object to add.
     */
    protected function doAddCommonEchangeDocApplicationClientOrganisme($commonEchangeDocApplicationClientOrganisme)
    {
        $this->collCommonEchangeDocApplicationClientOrganismes[]= $commonEchangeDocApplicationClientOrganisme;
        $commonEchangeDocApplicationClientOrganisme->setCommonEchangeDocApplicationClient($this);
    }

    /**
     * @param	CommonEchangeDocApplicationClientOrganisme $commonEchangeDocApplicationClientOrganisme The commonEchangeDocApplicationClientOrganisme object to remove.
     * @return CommonEchangeDocApplicationClient The current object (for fluent API support)
     */
    public function removeCommonEchangeDocApplicationClientOrganisme($commonEchangeDocApplicationClientOrganisme)
    {
        if ($this->getCommonEchangeDocApplicationClientOrganismes()->contains($commonEchangeDocApplicationClientOrganisme)) {
            $this->collCommonEchangeDocApplicationClientOrganismes->remove($this->collCommonEchangeDocApplicationClientOrganismes->search($commonEchangeDocApplicationClientOrganisme));
            if (null === $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion) {
                $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion = clone $this->collCommonEchangeDocApplicationClientOrganismes;
                $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion->clear();
            }
            $this->commonEchangeDocApplicationClientOrganismesScheduledForDeletion[]= $commonEchangeDocApplicationClientOrganisme;
            $commonEchangeDocApplicationClientOrganisme->setCommonEchangeDocApplicationClient(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonEchangeDocApplicationClient is new, it will return
     * an empty collection; or if this CommonEchangeDocApplicationClient has previously
     * been saved, it will retrieve related CommonEchangeDocApplicationClientOrganismes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonEchangeDocApplicationClient.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocApplicationClientOrganisme[] List of CommonEchangeDocApplicationClientOrganisme objects
     */
    public function getCommonEchangeDocApplicationClientOrganismesJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocApplicationClientOrganismeQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonEchangeDocApplicationClientOrganismes($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->echange_doc_application_id = null;
        $this->code = null;
        $this->libelle = null;
        $this->actif = null;
        $this->cheminement_signature = null;
        $this->cheminement_ged = null;
        $this->cheminement_sae = null;
        $this->cheminement_tdt = null;
        $this->classification_1 = null;
        $this->classification_2 = null;
        $this->classification_3 = null;
        $this->classification_4 = null;
        $this->classification_5 = null;
        $this->multi_docs_principaux = null;
        $this->envoi_auto_archivage = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonEchangeDocs) {
                foreach ($this->collCommonEchangeDocs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonEchangeDocApplicationClientOrganismes) {
                foreach ($this->collCommonEchangeDocApplicationClientOrganismes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonEchangeDocApplication instanceof Persistent) {
              $this->aCommonEchangeDocApplication->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonEchangeDocs instanceof PropelCollection) {
            $this->collCommonEchangeDocs->clearIterator();
        }
        $this->collCommonEchangeDocs = null;
        if ($this->collCommonEchangeDocApplicationClientOrganismes instanceof PropelCollection) {
            $this->collCommonEchangeDocApplicationClientOrganismes->clearIterator();
        }
        $this->collCommonEchangeDocApplicationClientOrganismes = null;
        $this->aCommonEchangeDocApplication = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonEchangeDocApplicationClientPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
