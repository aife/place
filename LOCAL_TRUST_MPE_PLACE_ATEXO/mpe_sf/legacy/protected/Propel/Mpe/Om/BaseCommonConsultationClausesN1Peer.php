<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonConsultationClausesN1;
use Application\Propel\Mpe\CommonConsultationClausesN1Peer;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN1Peer;
use Application\Propel\Mpe\CommonTContratTitulairePeer;
use Application\Propel\Mpe\Map\CommonConsultationClausesN1TableMap;

/**
 * Base static class for performing query and update operations on the 'consultation_clauses_n1' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationClausesN1Peer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'consultation_clauses_n1';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonConsultationClausesN1';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonConsultationClausesN1TableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 5;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 5;

    /** the column name for the id field */
    const ID = 'consultation_clauses_n1.id';

    /** the column name for the consultation_id field */
    const CONSULTATION_ID = 'consultation_clauses_n1.consultation_id';

    /** the column name for the lot_id field */
    const LOT_ID = 'consultation_clauses_n1.lot_id';

    /** the column name for the referentiel_clause_n1_id field */
    const REFERENTIEL_CLAUSE_N1_ID = 'consultation_clauses_n1.referentiel_clause_n1_id';

    /** the column name for the contrat_id field */
    const CONTRAT_ID = 'consultation_clauses_n1.contrat_id';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonConsultationClausesN1 objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonConsultationClausesN1[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonConsultationClausesN1Peer::$fieldNames[CommonConsultationClausesN1Peer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'ConsultationId', 'LotId', 'ReferentielClauseN1Id', 'ContratId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'consultationId', 'lotId', 'referentielClauseN1Id', 'contratId', ),
        BasePeer::TYPE_COLNAME => array (CommonConsultationClausesN1Peer::ID, CommonConsultationClausesN1Peer::CONSULTATION_ID, CommonConsultationClausesN1Peer::LOT_ID, CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, CommonConsultationClausesN1Peer::CONTRAT_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'CONSULTATION_ID', 'LOT_ID', 'REFERENTIEL_CLAUSE_N1_ID', 'CONTRAT_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'consultation_id', 'lot_id', 'referentiel_clause_n1_id', 'contrat_id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonConsultationClausesN1Peer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'ConsultationId' => 1, 'LotId' => 2, 'ReferentielClauseN1Id' => 3, 'ContratId' => 4, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'consultationId' => 1, 'lotId' => 2, 'referentielClauseN1Id' => 3, 'contratId' => 4, ),
        BasePeer::TYPE_COLNAME => array (CommonConsultationClausesN1Peer::ID => 0, CommonConsultationClausesN1Peer::CONSULTATION_ID => 1, CommonConsultationClausesN1Peer::LOT_ID => 2, CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID => 3, CommonConsultationClausesN1Peer::CONTRAT_ID => 4, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'CONSULTATION_ID' => 1, 'LOT_ID' => 2, 'REFERENTIEL_CLAUSE_N1_ID' => 3, 'CONTRAT_ID' => 4, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'consultation_id' => 1, 'lot_id' => 2, 'referentiel_clause_n1_id' => 3, 'contrat_id' => 4, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonConsultationClausesN1Peer::getFieldNames($toType);
        $key = isset(CommonConsultationClausesN1Peer::$fieldKeys[$fromType][$name]) ? CommonConsultationClausesN1Peer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonConsultationClausesN1Peer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonConsultationClausesN1Peer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonConsultationClausesN1Peer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonConsultationClausesN1Peer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonConsultationClausesN1Peer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonConsultationClausesN1Peer::ID);
            $criteria->addSelectColumn(CommonConsultationClausesN1Peer::CONSULTATION_ID);
            $criteria->addSelectColumn(CommonConsultationClausesN1Peer::LOT_ID);
            $criteria->addSelectColumn(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID);
            $criteria->addSelectColumn(CommonConsultationClausesN1Peer::CONTRAT_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.consultation_id');
            $criteria->addSelectColumn($alias . '.lot_id');
            $criteria->addSelectColumn($alias . '.referentiel_clause_n1_id');
            $criteria->addSelectColumn($alias . '.contrat_id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationClausesN1Peer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonConsultationClausesN1
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonConsultationClausesN1Peer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonConsultationClausesN1Peer::populateObjects(CommonConsultationClausesN1Peer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonConsultationClausesN1 $obj A CommonConsultationClausesN1 object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonConsultationClausesN1Peer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonConsultationClausesN1 object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonConsultationClausesN1) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonConsultationClausesN1 object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonConsultationClausesN1Peer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonConsultationClausesN1 Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonConsultationClausesN1Peer::$instances[$key])) {
                return CommonConsultationClausesN1Peer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonConsultationClausesN1Peer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonConsultationClausesN1Peer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to consultation_clauses_n1
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonConsultationClausesN1Peer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonConsultationClausesN1Peer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonConsultationClausesN1Peer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonConsultationClausesN1 object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonConsultationClausesN1Peer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonConsultationClausesN1Peer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonConsultationClausesN1Peer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTContratTitulaire table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTContratTitulaire(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationClausesN1Peer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONTRAT_ID, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonReferentielConsultationClausesN1 table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonReferentielConsultationClausesN1(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationClausesN1Peer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, CommonReferentielConsultationClausesN1Peer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationClausesN1Peer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonCategorieLot table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonCategorieLot(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationClausesN1Peer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationClausesN1Peer::LOT_ID, CommonCategorieLotPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonConsultationClausesN1 objects pre-filled with their CommonTContratTitulaire objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultationClausesN1 objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTContratTitulaire(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);
        }

        CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol = CommonConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;
        CommonTContratTitulairePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONTRAT_ID, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationClausesN1Peer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationClausesN1Peer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationClausesN1Peer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTContratTitulairePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol);
                    $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTContratTitulairePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to $obj2 (CommonTContratTitulaire)
                $obj2->addCommonConsultationClausesN1($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultationClausesN1 objects pre-filled with their CommonReferentielConsultationClausesN1 objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultationClausesN1 objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonReferentielConsultationClausesN1(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);
        }

        CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol = CommonConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;
        CommonReferentielConsultationClausesN1Peer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, CommonReferentielConsultationClausesN1Peer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationClausesN1Peer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationClausesN1Peer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationClausesN1Peer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonReferentielConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonReferentielConsultationClausesN1Peer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonReferentielConsultationClausesN1Peer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonReferentielConsultationClausesN1Peer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to $obj2 (CommonReferentielConsultationClausesN1)
                $obj2->addCommonConsultationClausesN1($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultationClausesN1 objects pre-filled with their CommonConsultation objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultationClausesN1 objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);
        }

        CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol = CommonConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;
        CommonConsultationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationClausesN1Peer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationClausesN1Peer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationClausesN1Peer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to $obj2 (CommonConsultation)
                $obj2->addCommonConsultationClausesN1($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultationClausesN1 objects pre-filled with their CommonCategorieLot objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultationClausesN1 objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonCategorieLot(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);
        }

        CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol = CommonConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;
        CommonCategorieLotPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationClausesN1Peer::LOT_ID, CommonCategorieLotPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationClausesN1Peer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationClausesN1Peer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationClausesN1Peer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonCategorieLotPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonCategorieLotPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonCategorieLotPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonCategorieLotPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to $obj2 (CommonCategorieLot)
                $obj2->addCommonConsultationClausesN1($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationClausesN1Peer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONTRAT_ID, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, CommonReferentielConsultationClausesN1Peer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::LOT_ID, CommonCategorieLotPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonConsultationClausesN1 objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultationClausesN1 objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);
        }

        CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonReferentielConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonCategorieLotPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonCategorieLotPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONTRAT_ID, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, CommonReferentielConsultationClausesN1Peer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::LOT_ID, CommonCategorieLotPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationClausesN1Peer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationClausesN1Peer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationClausesN1Peer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonTContratTitulaire rows

            $key2 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonTContratTitulairePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol2);
          $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTContratTitulairePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj2 (CommonTContratTitulaire)
                $obj2->addCommonConsultationClausesN1($obj1);
            } // if joined row not null

            // Add objects for joined CommonReferentielConsultationClausesN1 rows

            $key3 = CommonReferentielConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonReferentielConsultationClausesN1Peer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonReferentielConsultationClausesN1Peer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonReferentielConsultationClausesN1Peer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj3 (CommonReferentielConsultationClausesN1)
                $obj3->addCommonConsultationClausesN1($obj1);
            } // if joined row not null

            // Add objects for joined CommonConsultation rows

            $key4 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CommonConsultationPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonConsultationPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj4 (CommonConsultation)
                $obj4->addCommonConsultationClausesN1($obj1);
            } // if joined row not null

            // Add objects for joined CommonCategorieLot rows

            $key5 = CommonCategorieLotPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = CommonCategorieLotPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = CommonCategorieLotPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonCategorieLotPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj5 (CommonCategorieLot)
                $obj5->addCommonConsultationClausesN1($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTContratTitulaire table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTContratTitulaire(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationClausesN1Peer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, CommonReferentielConsultationClausesN1Peer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::LOT_ID, CommonCategorieLotPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonReferentielConsultationClausesN1 table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonReferentielConsultationClausesN1(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationClausesN1Peer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONTRAT_ID, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::LOT_ID, CommonCategorieLotPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationClausesN1Peer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONTRAT_ID, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, CommonReferentielConsultationClausesN1Peer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::LOT_ID, CommonCategorieLotPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonCategorieLot table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonCategorieLot(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationClausesN1Peer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONTRAT_ID, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, CommonReferentielConsultationClausesN1Peer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonConsultationClausesN1 objects pre-filled with all related objects except CommonTContratTitulaire.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultationClausesN1 objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTContratTitulaire(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);
        }

        CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;

        CommonReferentielConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonCategorieLotPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonCategorieLotPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, CommonReferentielConsultationClausesN1Peer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::LOT_ID, CommonCategorieLotPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationClausesN1Peer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationClausesN1Peer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationClausesN1Peer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielConsultationClausesN1 rows

                $key2 = CommonReferentielConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielConsultationClausesN1Peer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielConsultationClausesN1Peer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielConsultationClausesN1Peer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj2 (CommonReferentielConsultationClausesN1)
                $obj2->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key3 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonConsultationPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj3 (CommonConsultation)
                $obj3->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

                // Add objects for joined CommonCategorieLot rows

                $key4 = CommonCategorieLotPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonCategorieLotPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonCategorieLotPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonCategorieLotPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj4 (CommonCategorieLot)
                $obj4->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultationClausesN1 objects pre-filled with all related objects except CommonReferentielConsultationClausesN1.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultationClausesN1 objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonReferentielConsultationClausesN1(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);
        }

        CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonCategorieLotPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonCategorieLotPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONTRAT_ID, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::LOT_ID, CommonCategorieLotPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationClausesN1Peer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationClausesN1Peer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationClausesN1Peer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTContratTitulaire rows

                $key2 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTContratTitulairePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol2);
            $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTContratTitulairePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj2 (CommonTContratTitulaire)
                $obj2->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key3 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonConsultationPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj3 (CommonConsultation)
                $obj3->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

                // Add objects for joined CommonCategorieLot rows

                $key4 = CommonCategorieLotPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonCategorieLotPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonCategorieLotPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonCategorieLotPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj4 (CommonCategorieLot)
                $obj4->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultationClausesN1 objects pre-filled with all related objects except CommonConsultation.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultationClausesN1 objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);
        }

        CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonReferentielConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;

        CommonCategorieLotPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonCategorieLotPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONTRAT_ID, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, CommonReferentielConsultationClausesN1Peer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::LOT_ID, CommonCategorieLotPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationClausesN1Peer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationClausesN1Peer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationClausesN1Peer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTContratTitulaire rows

                $key2 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTContratTitulairePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol2);
            $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTContratTitulairePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj2 (CommonTContratTitulaire)
                $obj2->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

                // Add objects for joined CommonReferentielConsultationClausesN1 rows

                $key3 = CommonReferentielConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonReferentielConsultationClausesN1Peer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonReferentielConsultationClausesN1Peer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonReferentielConsultationClausesN1Peer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj3 (CommonReferentielConsultationClausesN1)
                $obj3->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

                // Add objects for joined CommonCategorieLot rows

                $key4 = CommonCategorieLotPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonCategorieLotPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonCategorieLotPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonCategorieLotPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj4 (CommonCategorieLot)
                $obj4->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultationClausesN1 objects pre-filled with all related objects except CommonCategorieLot.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultationClausesN1 objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonCategorieLot(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);
        }

        CommonConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielConsultationClausesN1Peer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonReferentielConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONTRAT_ID, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, CommonReferentielConsultationClausesN1Peer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationClausesN1Peer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationClausesN1Peer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationClausesN1Peer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationClausesN1Peer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTContratTitulaire rows

                $key2 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTContratTitulairePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol2);
            $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTContratTitulairePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj2 (CommonTContratTitulaire)
                $obj2->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

                // Add objects for joined CommonReferentielConsultationClausesN1 rows

                $key3 = CommonReferentielConsultationClausesN1Peer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonReferentielConsultationClausesN1Peer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonReferentielConsultationClausesN1Peer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonReferentielConsultationClausesN1Peer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj3 (CommonReferentielConsultationClausesN1)
                $obj3->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key4 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonConsultationPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonConsultationPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultationClausesN1) to the collection in $obj4 (CommonConsultation)
                $obj4->addCommonConsultationClausesN1($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonConsultationClausesN1Peer::DATABASE_NAME)->getTable(CommonConsultationClausesN1Peer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonConsultationClausesN1Peer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonConsultationClausesN1Peer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonConsultationClausesN1TableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonConsultationClausesN1Peer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonConsultationClausesN1 or Criteria object.
     *
     * @param      mixed $values Criteria or CommonConsultationClausesN1 object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonConsultationClausesN1 object
        }

        if ($criteria->containsKey(CommonConsultationClausesN1Peer::ID) && $criteria->keyContainsValue(CommonConsultationClausesN1Peer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonConsultationClausesN1Peer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonConsultationClausesN1 or Criteria object.
     *
     * @param      mixed $values Criteria or CommonConsultationClausesN1 object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonConsultationClausesN1Peer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonConsultationClausesN1Peer::ID);
            $value = $criteria->remove(CommonConsultationClausesN1Peer::ID);
            if ($value) {
                $selectCriteria->add(CommonConsultationClausesN1Peer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonConsultationClausesN1Peer::TABLE_NAME);
            }

        } else { // $values is CommonConsultationClausesN1 object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the consultation_clauses_n1 table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonConsultationClausesN1Peer::TABLE_NAME, $con, CommonConsultationClausesN1Peer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonConsultationClausesN1Peer::clearInstancePool();
            CommonConsultationClausesN1Peer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonConsultationClausesN1 or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonConsultationClausesN1 object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonConsultationClausesN1Peer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonConsultationClausesN1) { // it's a model object
            // invalidate the cache for this single object
            CommonConsultationClausesN1Peer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonConsultationClausesN1Peer::DATABASE_NAME);
            $criteria->add(CommonConsultationClausesN1Peer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonConsultationClausesN1Peer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationClausesN1Peer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonConsultationClausesN1Peer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonConsultationClausesN1 object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonConsultationClausesN1 $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonConsultationClausesN1Peer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonConsultationClausesN1Peer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonConsultationClausesN1Peer::DATABASE_NAME, CommonConsultationClausesN1Peer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonConsultationClausesN1
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonConsultationClausesN1Peer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonConsultationClausesN1Peer::DATABASE_NAME);
        $criteria->add(CommonConsultationClausesN1Peer::ID, $pk);

        $v = CommonConsultationClausesN1Peer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonConsultationClausesN1[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonConsultationClausesN1Peer::DATABASE_NAME);
            $criteria->add(CommonConsultationClausesN1Peer::ID, $pks, Criteria::IN);
            $objs = CommonConsultationClausesN1Peer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonConsultationClausesN1Peer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonConsultationClausesN1Peer::buildTableMap();

