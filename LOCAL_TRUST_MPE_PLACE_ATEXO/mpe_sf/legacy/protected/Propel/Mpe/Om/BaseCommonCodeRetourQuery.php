<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonCodeRetour;
use Application\Propel\Mpe\CommonCodeRetourPeer;
use Application\Propel\Mpe\CommonCodeRetourQuery;
use Application\Propel\Mpe\CommonEchangesInterfaces;

/**
 * Base class that represents a query for the 'code_retour' table.
 *
 *
 *
 * @method CommonCodeRetourQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method CommonCodeRetourQuery orderByDescription($order = Criteria::ASC) Order by the description column
 *
 * @method CommonCodeRetourQuery groupByCode() Group by the code column
 * @method CommonCodeRetourQuery groupByDescription() Group by the description column
 *
 * @method CommonCodeRetourQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonCodeRetourQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonCodeRetourQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonCodeRetourQuery leftJoinCommonEchangesInterfaces($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangesInterfaces relation
 * @method CommonCodeRetourQuery rightJoinCommonEchangesInterfaces($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangesInterfaces relation
 * @method CommonCodeRetourQuery innerJoinCommonEchangesInterfaces($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangesInterfaces relation
 *
 * @method CommonCodeRetour findOne(PropelPDO $con = null) Return the first CommonCodeRetour matching the query
 * @method CommonCodeRetour findOneOrCreate(PropelPDO $con = null) Return the first CommonCodeRetour matching the query, or a new CommonCodeRetour object populated from the query conditions when no match is found
 *
 * @method CommonCodeRetour findOneByDescription(string $description) Return the first CommonCodeRetour filtered by the description column
 *
 * @method array findByCode(int $code) Return CommonCodeRetour objects filtered by the code column
 * @method array findByDescription(string $description) Return CommonCodeRetour objects filtered by the description column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonCodeRetourQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonCodeRetourQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonCodeRetour', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonCodeRetourQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonCodeRetourQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonCodeRetourQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonCodeRetourQuery) {
            return $criteria;
        }
        $query = new CommonCodeRetourQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonCodeRetour|CommonCodeRetour[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonCodeRetourPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonCodeRetourPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonCodeRetour A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByCode($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonCodeRetour A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `code`, `description` FROM `code_retour` WHERE `code` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonCodeRetour();
            $obj->hydrate($row);
            CommonCodeRetourPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonCodeRetour|CommonCodeRetour[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonCodeRetour[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonCodeRetourQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonCodeRetourPeer::CODE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonCodeRetourQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonCodeRetourPeer::CODE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode(1234); // WHERE code = 1234
     * $query->filterByCode(array(12, 34)); // WHERE code IN (12, 34)
     * $query->filterByCode(array('min' => 12)); // WHERE code >= 12
     * $query->filterByCode(array('max' => 12)); // WHERE code <= 12
     * </code>
     *
     * @param     mixed $code The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonCodeRetourQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (is_array($code)) {
            $useMinMax = false;
            if (isset($code['min'])) {
                $this->addUsingAlias(CommonCodeRetourPeer::CODE, $code['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($code['max'])) {
                $this->addUsingAlias(CommonCodeRetourPeer::CODE, $code['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonCodeRetourPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonCodeRetourQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonCodeRetourPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query by a related CommonEchangesInterfaces object
     *
     * @param   CommonEchangesInterfaces|PropelObjectCollection $commonEchangesInterfaces  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonCodeRetourQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangesInterfaces($commonEchangesInterfaces, $comparison = null)
    {
        if ($commonEchangesInterfaces instanceof CommonEchangesInterfaces) {
            return $this
                ->addUsingAlias(CommonCodeRetourPeer::CODE, $commonEchangesInterfaces->getCodeRetour(), $comparison);
        } elseif ($commonEchangesInterfaces instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangesInterfacesQuery()
                ->filterByPrimaryKeys($commonEchangesInterfaces->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangesInterfaces() only accepts arguments of type CommonEchangesInterfaces or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangesInterfaces relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonCodeRetourQuery The current query, for fluid interface
     */
    public function joinCommonEchangesInterfaces($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangesInterfaces');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangesInterfaces');
        }

        return $this;
    }

    /**
     * Use the CommonEchangesInterfaces relation CommonEchangesInterfaces object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangesInterfacesQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangesInterfacesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonEchangesInterfaces($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangesInterfaces', '\Application\Propel\Mpe\CommonEchangesInterfacesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonCodeRetour $commonCodeRetour Object to remove from the list of results
     *
     * @return CommonCodeRetourQuery The current query, for fluid interface
     */
    public function prune($commonCodeRetour = null)
    {
        if ($commonCodeRetour) {
            $this->addUsingAlias(CommonCodeRetourPeer::CODE, $commonCodeRetour->getCode(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
