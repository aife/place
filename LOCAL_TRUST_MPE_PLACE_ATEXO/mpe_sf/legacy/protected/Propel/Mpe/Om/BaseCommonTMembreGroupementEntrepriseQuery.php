<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprise;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprisePeer;
use Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery;
use Application\Propel\Mpe\CommonTRoleJuridique;
use Application\Propel\Mpe\Entreprise;

/**
 * Base class that represents a query for the 't_membre_groupement_entreprise' table.
 *
 *
 *
 * @method CommonTMembreGroupementEntrepriseQuery orderByIdMembreGroupementEntreprise($order = Criteria::ASC) Order by the id_membre_groupement_entreprise column
 * @method CommonTMembreGroupementEntrepriseQuery orderByIdRoleJuridique($order = Criteria::ASC) Order by the id_role_juridique column
 * @method CommonTMembreGroupementEntrepriseQuery orderByIdGroupementEntreprise($order = Criteria::ASC) Order by the id_groupement_entreprise column
 * @method CommonTMembreGroupementEntrepriseQuery orderByIdEntreprise($order = Criteria::ASC) Order by the id_entreprise column
 * @method CommonTMembreGroupementEntrepriseQuery orderByIdEtablissement($order = Criteria::ASC) Order by the id_etablissement column
 * @method CommonTMembreGroupementEntrepriseQuery orderByIdMembreParent($order = Criteria::ASC) Order by the id_membre_parent column
 * @method CommonTMembreGroupementEntrepriseQuery orderByNumerosn($order = Criteria::ASC) Order by the numeroSN column
 * @method CommonTMembreGroupementEntrepriseQuery orderByEmail($order = Criteria::ASC) Order by the email column
 *
 * @method CommonTMembreGroupementEntrepriseQuery groupByIdMembreGroupementEntreprise() Group by the id_membre_groupement_entreprise column
 * @method CommonTMembreGroupementEntrepriseQuery groupByIdRoleJuridique() Group by the id_role_juridique column
 * @method CommonTMembreGroupementEntrepriseQuery groupByIdGroupementEntreprise() Group by the id_groupement_entreprise column
 * @method CommonTMembreGroupementEntrepriseQuery groupByIdEntreprise() Group by the id_entreprise column
 * @method CommonTMembreGroupementEntrepriseQuery groupByIdEtablissement() Group by the id_etablissement column
 * @method CommonTMembreGroupementEntrepriseQuery groupByIdMembreParent() Group by the id_membre_parent column
 * @method CommonTMembreGroupementEntrepriseQuery groupByNumerosn() Group by the numeroSN column
 * @method CommonTMembreGroupementEntrepriseQuery groupByEmail() Group by the email column
 *
 * @method CommonTMembreGroupementEntrepriseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTMembreGroupementEntrepriseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTMembreGroupementEntrepriseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTMembreGroupementEntrepriseQuery leftJoinEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entreprise relation
 * @method CommonTMembreGroupementEntrepriseQuery rightJoinEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entreprise relation
 * @method CommonTMembreGroupementEntrepriseQuery innerJoinEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the Entreprise relation
 *
 * @method CommonTMembreGroupementEntrepriseQuery leftJoinCommonTEtablissement($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTEtablissement relation
 * @method CommonTMembreGroupementEntrepriseQuery rightJoinCommonTEtablissement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTEtablissement relation
 * @method CommonTMembreGroupementEntrepriseQuery innerJoinCommonTEtablissement($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTEtablissement relation
 *
 * @method CommonTMembreGroupementEntrepriseQuery leftJoinCommonTGroupementEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTGroupementEntreprise relation
 * @method CommonTMembreGroupementEntrepriseQuery rightJoinCommonTGroupementEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTGroupementEntreprise relation
 * @method CommonTMembreGroupementEntrepriseQuery innerJoinCommonTGroupementEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTGroupementEntreprise relation
 *
 * @method CommonTMembreGroupementEntrepriseQuery leftJoinCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTMembreGroupementEntrepriseRelatedByIdMembreParent relation
 * @method CommonTMembreGroupementEntrepriseQuery rightJoinCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTMembreGroupementEntrepriseRelatedByIdMembreParent relation
 * @method CommonTMembreGroupementEntrepriseQuery innerJoinCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTMembreGroupementEntrepriseRelatedByIdMembreParent relation
 *
 * @method CommonTMembreGroupementEntrepriseQuery leftJoinCommonTRoleJuridique($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTRoleJuridique relation
 * @method CommonTMembreGroupementEntrepriseQuery rightJoinCommonTRoleJuridique($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTRoleJuridique relation
 * @method CommonTMembreGroupementEntrepriseQuery innerJoinCommonTRoleJuridique($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTRoleJuridique relation
 *
 * @method CommonTMembreGroupementEntrepriseQuery leftJoinCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise relation
 * @method CommonTMembreGroupementEntrepriseQuery rightJoinCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise relation
 * @method CommonTMembreGroupementEntrepriseQuery innerJoinCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise relation
 *
 * @method CommonTMembreGroupementEntreprise findOne(PropelPDO $con = null) Return the first CommonTMembreGroupementEntreprise matching the query
 * @method CommonTMembreGroupementEntreprise findOneOrCreate(PropelPDO $con = null) Return the first CommonTMembreGroupementEntreprise matching the query, or a new CommonTMembreGroupementEntreprise object populated from the query conditions when no match is found
 *
 * @method CommonTMembreGroupementEntreprise findOneByIdRoleJuridique(int $id_role_juridique) Return the first CommonTMembreGroupementEntreprise filtered by the id_role_juridique column
 * @method CommonTMembreGroupementEntreprise findOneByIdGroupementEntreprise(int $id_groupement_entreprise) Return the first CommonTMembreGroupementEntreprise filtered by the id_groupement_entreprise column
 * @method CommonTMembreGroupementEntreprise findOneByIdEntreprise(int $id_entreprise) Return the first CommonTMembreGroupementEntreprise filtered by the id_entreprise column
 * @method CommonTMembreGroupementEntreprise findOneByIdEtablissement(int $id_etablissement) Return the first CommonTMembreGroupementEntreprise filtered by the id_etablissement column
 * @method CommonTMembreGroupementEntreprise findOneByIdMembreParent(int $id_membre_parent) Return the first CommonTMembreGroupementEntreprise filtered by the id_membre_parent column
 * @method CommonTMembreGroupementEntreprise findOneByNumerosn(string $numeroSN) Return the first CommonTMembreGroupementEntreprise filtered by the numeroSN column
 * @method CommonTMembreGroupementEntreprise findOneByEmail(string $email) Return the first CommonTMembreGroupementEntreprise filtered by the email column
 *
 * @method array findByIdMembreGroupementEntreprise(int $id_membre_groupement_entreprise) Return CommonTMembreGroupementEntreprise objects filtered by the id_membre_groupement_entreprise column
 * @method array findByIdRoleJuridique(int $id_role_juridique) Return CommonTMembreGroupementEntreprise objects filtered by the id_role_juridique column
 * @method array findByIdGroupementEntreprise(int $id_groupement_entreprise) Return CommonTMembreGroupementEntreprise objects filtered by the id_groupement_entreprise column
 * @method array findByIdEntreprise(int $id_entreprise) Return CommonTMembreGroupementEntreprise objects filtered by the id_entreprise column
 * @method array findByIdEtablissement(int $id_etablissement) Return CommonTMembreGroupementEntreprise objects filtered by the id_etablissement column
 * @method array findByIdMembreParent(int $id_membre_parent) Return CommonTMembreGroupementEntreprise objects filtered by the id_membre_parent column
 * @method array findByNumerosn(string $numeroSN) Return CommonTMembreGroupementEntreprise objects filtered by the numeroSN column
 * @method array findByEmail(string $email) Return CommonTMembreGroupementEntreprise objects filtered by the email column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTMembreGroupementEntrepriseQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTMembreGroupementEntrepriseQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTMembreGroupementEntreprise', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTMembreGroupementEntrepriseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTMembreGroupementEntrepriseQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTMembreGroupementEntrepriseQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTMembreGroupementEntrepriseQuery) {
            return $criteria;
        }
        $query = new CommonTMembreGroupementEntrepriseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTMembreGroupementEntreprise|CommonTMembreGroupementEntreprise[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTMembreGroupementEntreprisePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTMembreGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTMembreGroupementEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdMembreGroupementEntreprise($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTMembreGroupementEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_membre_groupement_entreprise`, `id_role_juridique`, `id_groupement_entreprise`, `id_entreprise`, `id_etablissement`, `id_membre_parent`, `numeroSN`, `email` FROM `t_membre_groupement_entreprise` WHERE `id_membre_groupement_entreprise` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTMembreGroupementEntreprise();
            $obj->hydrate($row);
            CommonTMembreGroupementEntreprisePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTMembreGroupementEntreprise|CommonTMembreGroupementEntreprise[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_membre_groupement_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMembreGroupementEntreprise(1234); // WHERE id_membre_groupement_entreprise = 1234
     * $query->filterByIdMembreGroupementEntreprise(array(12, 34)); // WHERE id_membre_groupement_entreprise IN (12, 34)
     * $query->filterByIdMembreGroupementEntreprise(array('min' => 12)); // WHERE id_membre_groupement_entreprise >= 12
     * $query->filterByIdMembreGroupementEntreprise(array('max' => 12)); // WHERE id_membre_groupement_entreprise <= 12
     * </code>
     *
     * @param     mixed $idMembreGroupementEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdMembreGroupementEntreprise($idMembreGroupementEntreprise = null, $comparison = null)
    {
        if (is_array($idMembreGroupementEntreprise)) {
            $useMinMax = false;
            if (isset($idMembreGroupementEntreprise['min'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $idMembreGroupementEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMembreGroupementEntreprise['max'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $idMembreGroupementEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $idMembreGroupementEntreprise, $comparison);
    }

    /**
     * Filter the query on the id_role_juridique column
     *
     * Example usage:
     * <code>
     * $query->filterByIdRoleJuridique(1234); // WHERE id_role_juridique = 1234
     * $query->filterByIdRoleJuridique(array(12, 34)); // WHERE id_role_juridique IN (12, 34)
     * $query->filterByIdRoleJuridique(array('min' => 12)); // WHERE id_role_juridique >= 12
     * $query->filterByIdRoleJuridique(array('max' => 12)); // WHERE id_role_juridique <= 12
     * </code>
     *
     * @see       filterByCommonTRoleJuridique()
     *
     * @param     mixed $idRoleJuridique The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdRoleJuridique($idRoleJuridique = null, $comparison = null)
    {
        if (is_array($idRoleJuridique)) {
            $useMinMax = false;
            if (isset($idRoleJuridique['min'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, $idRoleJuridique['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idRoleJuridique['max'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, $idRoleJuridique['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, $idRoleJuridique, $comparison);
    }

    /**
     * Filter the query on the id_groupement_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdGroupementEntreprise(1234); // WHERE id_groupement_entreprise = 1234
     * $query->filterByIdGroupementEntreprise(array(12, 34)); // WHERE id_groupement_entreprise IN (12, 34)
     * $query->filterByIdGroupementEntreprise(array('min' => 12)); // WHERE id_groupement_entreprise >= 12
     * $query->filterByIdGroupementEntreprise(array('max' => 12)); // WHERE id_groupement_entreprise <= 12
     * </code>
     *
     * @see       filterByCommonTGroupementEntreprise()
     *
     * @param     mixed $idGroupementEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdGroupementEntreprise($idGroupementEntreprise = null, $comparison = null)
    {
        if (is_array($idGroupementEntreprise)) {
            $useMinMax = false;
            if (isset($idGroupementEntreprise['min'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $idGroupementEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idGroupementEntreprise['max'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $idGroupementEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $idGroupementEntreprise, $comparison);
    }

    /**
     * Filter the query on the id_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntreprise(1234); // WHERE id_entreprise = 1234
     * $query->filterByIdEntreprise(array(12, 34)); // WHERE id_entreprise IN (12, 34)
     * $query->filterByIdEntreprise(array('min' => 12)); // WHERE id_entreprise >= 12
     * $query->filterByIdEntreprise(array('max' => 12)); // WHERE id_entreprise <= 12
     * </code>
     *
     * @see       filterByEntreprise()
     *
     * @param     mixed $idEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdEntreprise($idEntreprise = null, $comparison = null)
    {
        if (is_array($idEntreprise)) {
            $useMinMax = false;
            if (isset($idEntreprise['min'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, $idEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntreprise['max'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, $idEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, $idEntreprise, $comparison);
    }

    /**
     * Filter the query on the id_etablissement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtablissement(1234); // WHERE id_etablissement = 1234
     * $query->filterByIdEtablissement(array(12, 34)); // WHERE id_etablissement IN (12, 34)
     * $query->filterByIdEtablissement(array('min' => 12)); // WHERE id_etablissement >= 12
     * $query->filterByIdEtablissement(array('max' => 12)); // WHERE id_etablissement <= 12
     * </code>
     *
     * @see       filterByCommonTEtablissement()
     *
     * @param     mixed $idEtablissement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdEtablissement($idEtablissement = null, $comparison = null)
    {
        if (is_array($idEtablissement)) {
            $useMinMax = false;
            if (isset($idEtablissement['min'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, $idEtablissement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtablissement['max'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, $idEtablissement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, $idEtablissement, $comparison);
    }

    /**
     * Filter the query on the id_membre_parent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdMembreParent(1234); // WHERE id_membre_parent = 1234
     * $query->filterByIdMembreParent(array(12, 34)); // WHERE id_membre_parent IN (12, 34)
     * $query->filterByIdMembreParent(array('min' => 12)); // WHERE id_membre_parent >= 12
     * $query->filterByIdMembreParent(array('max' => 12)); // WHERE id_membre_parent <= 12
     * </code>
     *
     * @see       filterByCommonTMembreGroupementEntrepriseRelatedByIdMembreParent()
     *
     * @param     mixed $idMembreParent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdMembreParent($idMembreParent = null, $comparison = null)
    {
        if (is_array($idMembreParent)) {
            $useMinMax = false;
            if (isset($idMembreParent['min'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT, $idMembreParent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idMembreParent['max'])) {
                $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT, $idMembreParent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT, $idMembreParent, $comparison);
    }

    /**
     * Filter the query on the numeroSN column
     *
     * Example usage:
     * <code>
     * $query->filterByNumerosn('fooValue');   // WHERE numeroSN = 'fooValue'
     * $query->filterByNumerosn('%fooValue%'); // WHERE numeroSN LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numerosn The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByNumerosn($numerosn = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numerosn)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numerosn)) {
                $numerosn = str_replace('*', '%', $numerosn);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::NUMEROSN, $numerosn, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query by a related Entreprise object
     *
     * @param   Entreprise|PropelObjectCollection $entreprise The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByEntreprise($entreprise, $comparison = null)
    {
        if ($entreprise instanceof Entreprise) {
            return $this
                ->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, $entreprise->getId(), $comparison);
        } elseif ($entreprise instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ENTREPRISE, $entreprise->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEntreprise() only accepts arguments of type Entreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function joinEntreprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entreprise');
        }

        return $this;
    }

    /**
     * Use the Entreprise relation Entreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\EntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useEntrepriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entreprise', '\Application\Propel\Mpe\EntrepriseQuery');
    }

    /**
     * Filter the query by a related CommonTEtablissement object
     *
     * @param   CommonTEtablissement|PropelObjectCollection $commonTEtablissement The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTEtablissement($commonTEtablissement, $comparison = null)
    {
        if ($commonTEtablissement instanceof CommonTEtablissement) {
            return $this
                ->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, $commonTEtablissement->getIdEtablissement(), $comparison);
        } elseif ($commonTEtablissement instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ETABLISSEMENT, $commonTEtablissement->toKeyValue('PrimaryKey', 'IdEtablissement'), $comparison);
        } else {
            throw new PropelException('filterByCommonTEtablissement() only accepts arguments of type CommonTEtablissement or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTEtablissement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTEtablissement($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTEtablissement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTEtablissement');
        }

        return $this;
    }

    /**
     * Use the CommonTEtablissement relation CommonTEtablissement object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTEtablissementQuery A secondary query class using the current class as primary query
     */
    public function useCommonTEtablissementQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTEtablissement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTEtablissement', '\Application\Propel\Mpe\CommonTEtablissementQuery');
    }

    /**
     * Filter the query by a related CommonTGroupementEntreprise object
     *
     * @param   CommonTGroupementEntreprise|PropelObjectCollection $commonTGroupementEntreprise The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTGroupementEntreprise($commonTGroupementEntreprise, $comparison = null)
    {
        if ($commonTGroupementEntreprise instanceof CommonTGroupementEntreprise) {
            return $this
                ->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $commonTGroupementEntreprise->getIdGroupementEntreprise(), $comparison);
        } elseif ($commonTGroupementEntreprise instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $commonTGroupementEntreprise->toKeyValue('PrimaryKey', 'IdGroupementEntreprise'), $comparison);
        } else {
            throw new PropelException('filterByCommonTGroupementEntreprise() only accepts arguments of type CommonTGroupementEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTGroupementEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTGroupementEntreprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTGroupementEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTGroupementEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonTGroupementEntreprise relation CommonTGroupementEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTGroupementEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonTGroupementEntrepriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTGroupementEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTGroupementEntreprise', '\Application\Propel\Mpe\CommonTGroupementEntrepriseQuery');
    }

    /**
     * Filter the query by a related CommonTMembreGroupementEntreprise object
     *
     * @param   CommonTMembreGroupementEntreprise|PropelObjectCollection $commonTMembreGroupementEntreprise The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($commonTMembreGroupementEntreprise, $comparison = null)
    {
        if ($commonTMembreGroupementEntreprise instanceof CommonTMembreGroupementEntreprise) {
            return $this
                ->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT, $commonTMembreGroupementEntreprise->getIdMembreGroupementEntreprise(), $comparison);
        } elseif ($commonTMembreGroupementEntreprise instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_PARENT, $commonTMembreGroupementEntreprise->toKeyValue('PrimaryKey', 'IdMembreGroupementEntreprise'), $comparison);
        } else {
            throw new PropelException('filterByCommonTMembreGroupementEntrepriseRelatedByIdMembreParent() only accepts arguments of type CommonTMembreGroupementEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTMembreGroupementEntrepriseRelatedByIdMembreParent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTMembreGroupementEntrepriseRelatedByIdMembreParent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTMembreGroupementEntrepriseRelatedByIdMembreParent');
        }

        return $this;
    }

    /**
     * Use the CommonTMembreGroupementEntrepriseRelatedByIdMembreParent relation CommonTMembreGroupementEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonTMembreGroupementEntrepriseRelatedByIdMembreParentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTMembreGroupementEntrepriseRelatedByIdMembreParent', '\Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery');
    }

    /**
     * Filter the query by a related CommonTRoleJuridique object
     *
     * @param   CommonTRoleJuridique|PropelObjectCollection $commonTRoleJuridique The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTRoleJuridique($commonTRoleJuridique, $comparison = null)
    {
        if ($commonTRoleJuridique instanceof CommonTRoleJuridique) {
            return $this
                ->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, $commonTRoleJuridique->getIdRoleJuridique(), $comparison);
        } elseif ($commonTRoleJuridique instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, $commonTRoleJuridique->toKeyValue('PrimaryKey', 'IdRoleJuridique'), $comparison);
        } else {
            throw new PropelException('filterByCommonTRoleJuridique() only accepts arguments of type CommonTRoleJuridique or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTRoleJuridique relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTRoleJuridique($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTRoleJuridique');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTRoleJuridique');
        }

        return $this;
    }

    /**
     * Use the CommonTRoleJuridique relation CommonTRoleJuridique object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTRoleJuridiqueQuery A secondary query class using the current class as primary query
     */
    public function useCommonTRoleJuridiqueQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTRoleJuridique($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTRoleJuridique', '\Application\Propel\Mpe\CommonTRoleJuridiqueQuery');
    }

    /**
     * Filter the query by a related CommonTMembreGroupementEntreprise object
     *
     * @param   CommonTMembreGroupementEntreprise|PropelObjectCollection $commonTMembreGroupementEntreprise  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($commonTMembreGroupementEntreprise, $comparison = null)
    {
        if ($commonTMembreGroupementEntreprise instanceof CommonTMembreGroupementEntreprise) {
            return $this
                ->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $commonTMembreGroupementEntreprise->getIdMembreParent(), $comparison);
        } elseif ($commonTMembreGroupementEntreprise instanceof PropelObjectCollection) {
            return $this
                ->useCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntrepriseQuery()
                ->filterByPrimaryKeys($commonTMembreGroupementEntreprise->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise() only accepts arguments of type CommonTMembreGroupementEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise relation CommonTMembreGroupementEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntrepriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise', '\Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTMembreGroupementEntreprise $commonTMembreGroupementEntreprise Object to remove from the list of results
     *
     * @return CommonTMembreGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function prune($commonTMembreGroupementEntreprise = null)
    {
        if ($commonTMembreGroupementEntreprise) {
            $this->addUsingAlias(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, $commonTMembreGroupementEntreprise->getIdMembreGroupementEntreprise(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
