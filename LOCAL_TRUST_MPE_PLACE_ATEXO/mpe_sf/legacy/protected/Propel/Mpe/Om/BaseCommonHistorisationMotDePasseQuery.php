<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonHistorisationMotDePasse;
use Application\Propel\Mpe\CommonHistorisationMotDePassePeer;
use Application\Propel\Mpe\CommonHistorisationMotDePasseQuery;
use Application\Propel\Mpe\CommonInscrit;

/**
 * Base class that represents a query for the 'historisation_mot_de_passe' table.
 *
 *
 *
 * @method CommonHistorisationMotDePasseQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonHistorisationMotDePasseQuery orderByAncienMotDePasse($order = Criteria::ASC) Order by the ancien_mot_de_passe column
 * @method CommonHistorisationMotDePasseQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonHistorisationMotDePasseQuery orderByOldIdInscrit($order = Criteria::ASC) Order by the old_id_inscrit column
 * @method CommonHistorisationMotDePasseQuery orderByIdInscrit($order = Criteria::ASC) Order by the id_inscrit column
 *
 * @method CommonHistorisationMotDePasseQuery groupById() Group by the id column
 * @method CommonHistorisationMotDePasseQuery groupByAncienMotDePasse() Group by the ancien_mot_de_passe column
 * @method CommonHistorisationMotDePasseQuery groupByDateModification() Group by the date_modification column
 * @method CommonHistorisationMotDePasseQuery groupByOldIdInscrit() Group by the old_id_inscrit column
 * @method CommonHistorisationMotDePasseQuery groupByIdInscrit() Group by the id_inscrit column
 *
 * @method CommonHistorisationMotDePasseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonHistorisationMotDePasseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonHistorisationMotDePasseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonHistorisationMotDePasseQuery leftJoinCommonInscrit($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonInscrit relation
 * @method CommonHistorisationMotDePasseQuery rightJoinCommonInscrit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonInscrit relation
 * @method CommonHistorisationMotDePasseQuery innerJoinCommonInscrit($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonInscrit relation
 *
 * @method CommonHistorisationMotDePasse findOne(PropelPDO $con = null) Return the first CommonHistorisationMotDePasse matching the query
 * @method CommonHistorisationMotDePasse findOneOrCreate(PropelPDO $con = null) Return the first CommonHistorisationMotDePasse matching the query, or a new CommonHistorisationMotDePasse object populated from the query conditions when no match is found
 *
 * @method CommonHistorisationMotDePasse findOneByAncienMotDePasse(string $ancien_mot_de_passe) Return the first CommonHistorisationMotDePasse filtered by the ancien_mot_de_passe column
 * @method CommonHistorisationMotDePasse findOneByDateModification(string $date_modification) Return the first CommonHistorisationMotDePasse filtered by the date_modification column
 * @method CommonHistorisationMotDePasse findOneByOldIdInscrit(int $old_id_inscrit) Return the first CommonHistorisationMotDePasse filtered by the old_id_inscrit column
 * @method CommonHistorisationMotDePasse findOneByIdInscrit(string $id_inscrit) Return the first CommonHistorisationMotDePasse filtered by the id_inscrit column
 *
 * @method array findById(int $id) Return CommonHistorisationMotDePasse objects filtered by the id column
 * @method array findByAncienMotDePasse(string $ancien_mot_de_passe) Return CommonHistorisationMotDePasse objects filtered by the ancien_mot_de_passe column
 * @method array findByDateModification(string $date_modification) Return CommonHistorisationMotDePasse objects filtered by the date_modification column
 * @method array findByOldIdInscrit(int $old_id_inscrit) Return CommonHistorisationMotDePasse objects filtered by the old_id_inscrit column
 * @method array findByIdInscrit(string $id_inscrit) Return CommonHistorisationMotDePasse objects filtered by the id_inscrit column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonHistorisationMotDePasseQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonHistorisationMotDePasseQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonHistorisationMotDePasse', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonHistorisationMotDePasseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonHistorisationMotDePasseQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonHistorisationMotDePasseQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonHistorisationMotDePasseQuery) {
            return $criteria;
        }
        $query = new CommonHistorisationMotDePasseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonHistorisationMotDePasse|CommonHistorisationMotDePasse[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonHistorisationMotDePassePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonHistorisationMotDePassePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonHistorisationMotDePasse A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonHistorisationMotDePasse A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `ancien_mot_de_passe`, `date_modification`, `old_id_inscrit`, `id_inscrit` FROM `historisation_mot_de_passe` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonHistorisationMotDePasse();
            $obj->hydrate($row);
            CommonHistorisationMotDePassePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonHistorisationMotDePasse|CommonHistorisationMotDePasse[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonHistorisationMotDePasse[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonHistorisationMotDePasseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonHistorisationMotDePassePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonHistorisationMotDePasseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonHistorisationMotDePassePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHistorisationMotDePasseQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonHistorisationMotDePassePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonHistorisationMotDePassePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHistorisationMotDePassePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the ancien_mot_de_passe column
     *
     * Example usage:
     * <code>
     * $query->filterByAncienMotDePasse('fooValue');   // WHERE ancien_mot_de_passe = 'fooValue'
     * $query->filterByAncienMotDePasse('%fooValue%'); // WHERE ancien_mot_de_passe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ancienMotDePasse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHistorisationMotDePasseQuery The current query, for fluid interface
     */
    public function filterByAncienMotDePasse($ancienMotDePasse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ancienMotDePasse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ancienMotDePasse)) {
                $ancienMotDePasse = str_replace('*', '%', $ancienMotDePasse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonHistorisationMotDePassePeer::ANCIEN_MOT_DE_PASSE, $ancienMotDePasse, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('2011-03-14'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification('now'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification(array('max' => 'yesterday')); // WHERE date_modification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHistorisationMotDePasseQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (is_array($dateModification)) {
            $useMinMax = false;
            if (isset($dateModification['min'])) {
                $this->addUsingAlias(CommonHistorisationMotDePassePeer::DATE_MODIFICATION, $dateModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModification['max'])) {
                $this->addUsingAlias(CommonHistorisationMotDePassePeer::DATE_MODIFICATION, $dateModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHistorisationMotDePassePeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the old_id_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdInscrit(1234); // WHERE old_id_inscrit = 1234
     * $query->filterByOldIdInscrit(array(12, 34)); // WHERE old_id_inscrit IN (12, 34)
     * $query->filterByOldIdInscrit(array('min' => 12)); // WHERE old_id_inscrit >= 12
     * $query->filterByOldIdInscrit(array('max' => 12)); // WHERE old_id_inscrit <= 12
     * </code>
     *
     * @param     mixed $oldIdInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHistorisationMotDePasseQuery The current query, for fluid interface
     */
    public function filterByOldIdInscrit($oldIdInscrit = null, $comparison = null)
    {
        if (is_array($oldIdInscrit)) {
            $useMinMax = false;
            if (isset($oldIdInscrit['min'])) {
                $this->addUsingAlias(CommonHistorisationMotDePassePeer::OLD_ID_INSCRIT, $oldIdInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdInscrit['max'])) {
                $this->addUsingAlias(CommonHistorisationMotDePassePeer::OLD_ID_INSCRIT, $oldIdInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHistorisationMotDePassePeer::OLD_ID_INSCRIT, $oldIdInscrit, $comparison);
    }

    /**
     * Filter the query on the id_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInscrit(1234); // WHERE id_inscrit = 1234
     * $query->filterByIdInscrit(array(12, 34)); // WHERE id_inscrit IN (12, 34)
     * $query->filterByIdInscrit(array('min' => 12)); // WHERE id_inscrit >= 12
     * $query->filterByIdInscrit(array('max' => 12)); // WHERE id_inscrit <= 12
     * </code>
     *
     * @see       filterByCommonInscrit()
     *
     * @param     mixed $idInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHistorisationMotDePasseQuery The current query, for fluid interface
     */
    public function filterByIdInscrit($idInscrit = null, $comparison = null)
    {
        if (is_array($idInscrit)) {
            $useMinMax = false;
            if (isset($idInscrit['min'])) {
                $this->addUsingAlias(CommonHistorisationMotDePassePeer::ID_INSCRIT, $idInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInscrit['max'])) {
                $this->addUsingAlias(CommonHistorisationMotDePassePeer::ID_INSCRIT, $idInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHistorisationMotDePassePeer::ID_INSCRIT, $idInscrit, $comparison);
    }

    /**
     * Filter the query by a related CommonInscrit object
     *
     * @param   CommonInscrit|PropelObjectCollection $commonInscrit The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonHistorisationMotDePasseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonInscrit($commonInscrit, $comparison = null)
    {
        if ($commonInscrit instanceof CommonInscrit) {
            return $this
                ->addUsingAlias(CommonHistorisationMotDePassePeer::ID_INSCRIT, $commonInscrit->getId(), $comparison);
        } elseif ($commonInscrit instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonHistorisationMotDePassePeer::ID_INSCRIT, $commonInscrit->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonInscrit() only accepts arguments of type CommonInscrit or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonInscrit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonHistorisationMotDePasseQuery The current query, for fluid interface
     */
    public function joinCommonInscrit($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonInscrit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonInscrit');
        }

        return $this;
    }

    /**
     * Use the CommonInscrit relation CommonInscrit object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonInscritQuery A secondary query class using the current class as primary query
     */
    public function useCommonInscritQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonInscrit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonInscrit', '\Application\Propel\Mpe\CommonInscritQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonHistorisationMotDePasse $commonHistorisationMotDePasse Object to remove from the list of results
     *
     * @return CommonHistorisationMotDePasseQuery The current query, for fluid interface
     */
    public function prune($commonHistorisationMotDePasse = null)
    {
        if ($commonHistorisationMotDePasse) {
            $this->addUsingAlias(CommonHistorisationMotDePassePeer::ID, $commonHistorisationMotDePasse->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
