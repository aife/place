<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationArchive;
use Application\Propel\Mpe\CommonConsultationArchiveArcade;
use Application\Propel\Mpe\CommonConsultationArchiveBloc;
use Application\Propel\Mpe\CommonConsultationArchivePeer;
use Application\Propel\Mpe\CommonConsultationArchiveQuery;

/**
 * Base class that represents a query for the 'consultation_archive' table.
 *
 *
 *
 * @method CommonConsultationArchiveQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonConsultationArchiveQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonConsultationArchiveQuery orderByConsultationRef($order = Criteria::ASC) Order by the consultation_ref column
 * @method CommonConsultationArchiveQuery orderByCheminFichier($order = Criteria::ASC) Order by the chemin_fichier column
 * @method CommonConsultationArchiveQuery orderByDateArchivage($order = Criteria::ASC) Order by the date_archivage column
 * @method CommonConsultationArchiveQuery orderByPoidsArchivage($order = Criteria::ASC) Order by the poids_archivage column
 * @method CommonConsultationArchiveQuery orderByAnneeCreationConsultation($order = Criteria::ASC) Order by the annee_creation_consultation column
 * @method CommonConsultationArchiveQuery orderByStatusGlobalTransmission($order = Criteria::ASC) Order by the status_global_transmission column
 * @method CommonConsultationArchiveQuery orderByStatusFragmentation($order = Criteria::ASC) Order by the status_fragmentation column
 * @method CommonConsultationArchiveQuery orderByNombreBloc($order = Criteria::ASC) Order by the nombre_bloc column
 * @method CommonConsultationArchiveQuery orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 *
 * @method CommonConsultationArchiveQuery groupById() Group by the id column
 * @method CommonConsultationArchiveQuery groupByOrganisme() Group by the organisme column
 * @method CommonConsultationArchiveQuery groupByConsultationRef() Group by the consultation_ref column
 * @method CommonConsultationArchiveQuery groupByCheminFichier() Group by the chemin_fichier column
 * @method CommonConsultationArchiveQuery groupByDateArchivage() Group by the date_archivage column
 * @method CommonConsultationArchiveQuery groupByPoidsArchivage() Group by the poids_archivage column
 * @method CommonConsultationArchiveQuery groupByAnneeCreationConsultation() Group by the annee_creation_consultation column
 * @method CommonConsultationArchiveQuery groupByStatusGlobalTransmission() Group by the status_global_transmission column
 * @method CommonConsultationArchiveQuery groupByStatusFragmentation() Group by the status_fragmentation column
 * @method CommonConsultationArchiveQuery groupByNombreBloc() Group by the nombre_bloc column
 * @method CommonConsultationArchiveQuery groupByConsultationId() Group by the consultation_id column
 *
 * @method CommonConsultationArchiveQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonConsultationArchiveQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonConsultationArchiveQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonConsultationArchiveQuery leftJoinCommonConsultationArchiveArcade($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultationArchiveArcade relation
 * @method CommonConsultationArchiveQuery rightJoinCommonConsultationArchiveArcade($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultationArchiveArcade relation
 * @method CommonConsultationArchiveQuery innerJoinCommonConsultationArchiveArcade($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultationArchiveArcade relation
 *
 * @method CommonConsultationArchiveQuery leftJoinCommonConsultationArchiveBloc($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultationArchiveBloc relation
 * @method CommonConsultationArchiveQuery rightJoinCommonConsultationArchiveBloc($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultationArchiveBloc relation
 * @method CommonConsultationArchiveQuery innerJoinCommonConsultationArchiveBloc($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultationArchiveBloc relation
 *
 * @method CommonConsultationArchive findOne(PropelPDO $con = null) Return the first CommonConsultationArchive matching the query
 * @method CommonConsultationArchive findOneOrCreate(PropelPDO $con = null) Return the first CommonConsultationArchive matching the query, or a new CommonConsultationArchive object populated from the query conditions when no match is found
 *
 * @method CommonConsultationArchive findOneByOrganisme(string $organisme) Return the first CommonConsultationArchive filtered by the organisme column
 * @method CommonConsultationArchive findOneByConsultationRef(int $consultation_ref) Return the first CommonConsultationArchive filtered by the consultation_ref column
 * @method CommonConsultationArchive findOneByCheminFichier(string $chemin_fichier) Return the first CommonConsultationArchive filtered by the chemin_fichier column
 * @method CommonConsultationArchive findOneByDateArchivage(string $date_archivage) Return the first CommonConsultationArchive filtered by the date_archivage column
 * @method CommonConsultationArchive findOneByPoidsArchivage(int $poids_archivage) Return the first CommonConsultationArchive filtered by the poids_archivage column
 * @method CommonConsultationArchive findOneByAnneeCreationConsultation(int $annee_creation_consultation) Return the first CommonConsultationArchive filtered by the annee_creation_consultation column
 * @method CommonConsultationArchive findOneByStatusGlobalTransmission(string $status_global_transmission) Return the first CommonConsultationArchive filtered by the status_global_transmission column
 * @method CommonConsultationArchive findOneByStatusFragmentation(boolean $status_fragmentation) Return the first CommonConsultationArchive filtered by the status_fragmentation column
 * @method CommonConsultationArchive findOneByNombreBloc(int $nombre_bloc) Return the first CommonConsultationArchive filtered by the nombre_bloc column
 * @method CommonConsultationArchive findOneByConsultationId(int $consultation_id) Return the first CommonConsultationArchive filtered by the consultation_id column
 *
 * @method array findById(int $id) Return CommonConsultationArchive objects filtered by the id column
 * @method array findByOrganisme(string $organisme) Return CommonConsultationArchive objects filtered by the organisme column
 * @method array findByConsultationRef(int $consultation_ref) Return CommonConsultationArchive objects filtered by the consultation_ref column
 * @method array findByCheminFichier(string $chemin_fichier) Return CommonConsultationArchive objects filtered by the chemin_fichier column
 * @method array findByDateArchivage(string $date_archivage) Return CommonConsultationArchive objects filtered by the date_archivage column
 * @method array findByPoidsArchivage(int $poids_archivage) Return CommonConsultationArchive objects filtered by the poids_archivage column
 * @method array findByAnneeCreationConsultation(int $annee_creation_consultation) Return CommonConsultationArchive objects filtered by the annee_creation_consultation column
 * @method array findByStatusGlobalTransmission(string $status_global_transmission) Return CommonConsultationArchive objects filtered by the status_global_transmission column
 * @method array findByStatusFragmentation(boolean $status_fragmentation) Return CommonConsultationArchive objects filtered by the status_fragmentation column
 * @method array findByNombreBloc(int $nombre_bloc) Return CommonConsultationArchive objects filtered by the nombre_bloc column
 * @method array findByConsultationId(int $consultation_id) Return CommonConsultationArchive objects filtered by the consultation_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationArchiveQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonConsultationArchiveQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonConsultationArchive', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonConsultationArchiveQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonConsultationArchiveQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonConsultationArchiveQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonConsultationArchiveQuery) {
            return $criteria;
        }
        $query = new CommonConsultationArchiveQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonConsultationArchive|CommonConsultationArchive[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonConsultationArchivePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationArchivePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationArchive A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationArchive A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `organisme`, `consultation_ref`, `chemin_fichier`, `date_archivage`, `poids_archivage`, `annee_creation_consultation`, `status_global_transmission`, `status_fragmentation`, `nombre_bloc`, `consultation_id` FROM `consultation_archive` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonConsultationArchive();
            $obj->hydrate($row);
            CommonConsultationArchivePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonConsultationArchive|CommonConsultationArchive[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonConsultationArchive[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonConsultationArchivePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonConsultationArchivePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchivePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchivePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the consultation_ref column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationRef(1234); // WHERE consultation_ref = 1234
     * $query->filterByConsultationRef(array(12, 34)); // WHERE consultation_ref IN (12, 34)
     * $query->filterByConsultationRef(array('min' => 12)); // WHERE consultation_ref >= 12
     * $query->filterByConsultationRef(array('max' => 12)); // WHERE consultation_ref <= 12
     * </code>
     *
     * @param     mixed $consultationRef The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByConsultationRef($consultationRef = null, $comparison = null)
    {
        if (is_array($consultationRef)) {
            $useMinMax = false;
            if (isset($consultationRef['min'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::CONSULTATION_REF, $consultationRef['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationRef['max'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::CONSULTATION_REF, $consultationRef['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchivePeer::CONSULTATION_REF, $consultationRef, $comparison);
    }

    /**
     * Filter the query on the chemin_fichier column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminFichier('fooValue');   // WHERE chemin_fichier = 'fooValue'
     * $query->filterByCheminFichier('%fooValue%'); // WHERE chemin_fichier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cheminFichier The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByCheminFichier($cheminFichier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cheminFichier)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cheminFichier)) {
                $cheminFichier = str_replace('*', '%', $cheminFichier);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchivePeer::CHEMIN_FICHIER, $cheminFichier, $comparison);
    }

    /**
     * Filter the query on the date_archivage column
     *
     * Example usage:
     * <code>
     * $query->filterByDateArchivage('2011-03-14'); // WHERE date_archivage = '2011-03-14'
     * $query->filterByDateArchivage('now'); // WHERE date_archivage = '2011-03-14'
     * $query->filterByDateArchivage(array('max' => 'yesterday')); // WHERE date_archivage > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateArchivage The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByDateArchivage($dateArchivage = null, $comparison = null)
    {
        if (is_array($dateArchivage)) {
            $useMinMax = false;
            if (isset($dateArchivage['min'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::DATE_ARCHIVAGE, $dateArchivage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateArchivage['max'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::DATE_ARCHIVAGE, $dateArchivage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchivePeer::DATE_ARCHIVAGE, $dateArchivage, $comparison);
    }

    /**
     * Filter the query on the poids_archivage column
     *
     * Example usage:
     * <code>
     * $query->filterByPoidsArchivage(1234); // WHERE poids_archivage = 1234
     * $query->filterByPoidsArchivage(array(12, 34)); // WHERE poids_archivage IN (12, 34)
     * $query->filterByPoidsArchivage(array('min' => 12)); // WHERE poids_archivage >= 12
     * $query->filterByPoidsArchivage(array('max' => 12)); // WHERE poids_archivage <= 12
     * </code>
     *
     * @param     mixed $poidsArchivage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByPoidsArchivage($poidsArchivage = null, $comparison = null)
    {
        if (is_array($poidsArchivage)) {
            $useMinMax = false;
            if (isset($poidsArchivage['min'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::POIDS_ARCHIVAGE, $poidsArchivage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($poidsArchivage['max'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::POIDS_ARCHIVAGE, $poidsArchivage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchivePeer::POIDS_ARCHIVAGE, $poidsArchivage, $comparison);
    }

    /**
     * Filter the query on the annee_creation_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByAnneeCreationConsultation(1234); // WHERE annee_creation_consultation = 1234
     * $query->filterByAnneeCreationConsultation(array(12, 34)); // WHERE annee_creation_consultation IN (12, 34)
     * $query->filterByAnneeCreationConsultation(array('min' => 12)); // WHERE annee_creation_consultation >= 12
     * $query->filterByAnneeCreationConsultation(array('max' => 12)); // WHERE annee_creation_consultation <= 12
     * </code>
     *
     * @param     mixed $anneeCreationConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByAnneeCreationConsultation($anneeCreationConsultation = null, $comparison = null)
    {
        if (is_array($anneeCreationConsultation)) {
            $useMinMax = false;
            if (isset($anneeCreationConsultation['min'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::ANNEE_CREATION_CONSULTATION, $anneeCreationConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($anneeCreationConsultation['max'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::ANNEE_CREATION_CONSULTATION, $anneeCreationConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchivePeer::ANNEE_CREATION_CONSULTATION, $anneeCreationConsultation, $comparison);
    }

    /**
     * Filter the query on the status_global_transmission column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusGlobalTransmission('fooValue');   // WHERE status_global_transmission = 'fooValue'
     * $query->filterByStatusGlobalTransmission('%fooValue%'); // WHERE status_global_transmission LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statusGlobalTransmission The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByStatusGlobalTransmission($statusGlobalTransmission = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statusGlobalTransmission)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $statusGlobalTransmission)) {
                $statusGlobalTransmission = str_replace('*', '%', $statusGlobalTransmission);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchivePeer::STATUS_GLOBAL_TRANSMISSION, $statusGlobalTransmission, $comparison);
    }

    /**
     * Filter the query on the status_fragmentation column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusFragmentation(true); // WHERE status_fragmentation = true
     * $query->filterByStatusFragmentation('yes'); // WHERE status_fragmentation = true
     * </code>
     *
     * @param     boolean|string $statusFragmentation The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByStatusFragmentation($statusFragmentation = null, $comparison = null)
    {
        if (is_string($statusFragmentation)) {
            $statusFragmentation = in_array(strtolower($statusFragmentation), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConsultationArchivePeer::STATUS_FRAGMENTATION, $statusFragmentation, $comparison);
    }

    /**
     * Filter the query on the nombre_bloc column
     *
     * Example usage:
     * <code>
     * $query->filterByNombreBloc(1234); // WHERE nombre_bloc = 1234
     * $query->filterByNombreBloc(array(12, 34)); // WHERE nombre_bloc IN (12, 34)
     * $query->filterByNombreBloc(array('min' => 12)); // WHERE nombre_bloc >= 12
     * $query->filterByNombreBloc(array('max' => 12)); // WHERE nombre_bloc <= 12
     * </code>
     *
     * @param     mixed $nombreBloc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByNombreBloc($nombreBloc = null, $comparison = null)
    {
        if (is_array($nombreBloc)) {
            $useMinMax = false;
            if (isset($nombreBloc['min'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::NOMBRE_BLOC, $nombreBloc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nombreBloc['max'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::NOMBRE_BLOC, $nombreBloc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchivePeer::NOMBRE_BLOC, $nombreBloc, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonConsultationArchivePeer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchivePeer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Filter the query by a related CommonConsultationArchiveArcade object
     *
     * @param   CommonConsultationArchiveArcade|PropelObjectCollection $commonConsultationArchiveArcade  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationArchiveQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultationArchiveArcade($commonConsultationArchiveArcade, $comparison = null)
    {
        if ($commonConsultationArchiveArcade instanceof CommonConsultationArchiveArcade) {
            return $this
                ->addUsingAlias(CommonConsultationArchivePeer::ID, $commonConsultationArchiveArcade->getConsultationArchiveId(), $comparison);
        } elseif ($commonConsultationArchiveArcade instanceof PropelObjectCollection) {
            return $this
                ->useCommonConsultationArchiveArcadeQuery()
                ->filterByPrimaryKeys($commonConsultationArchiveArcade->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonConsultationArchiveArcade() only accepts arguments of type CommonConsultationArchiveArcade or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultationArchiveArcade relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function joinCommonConsultationArchiveArcade($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultationArchiveArcade');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultationArchiveArcade');
        }

        return $this;
    }

    /**
     * Use the CommonConsultationArchiveArcade relation CommonConsultationArchiveArcade object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationArchiveArcadeQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationArchiveArcadeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonConsultationArchiveArcade($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultationArchiveArcade', '\Application\Propel\Mpe\CommonConsultationArchiveArcadeQuery');
    }

    /**
     * Filter the query by a related CommonConsultationArchiveBloc object
     *
     * @param   CommonConsultationArchiveBloc|PropelObjectCollection $commonConsultationArchiveBloc  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationArchiveQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultationArchiveBloc($commonConsultationArchiveBloc, $comparison = null)
    {
        if ($commonConsultationArchiveBloc instanceof CommonConsultationArchiveBloc) {
            return $this
                ->addUsingAlias(CommonConsultationArchivePeer::ID, $commonConsultationArchiveBloc->getConsultationArchiveId(), $comparison);
        } elseif ($commonConsultationArchiveBloc instanceof PropelObjectCollection) {
            return $this
                ->useCommonConsultationArchiveBlocQuery()
                ->filterByPrimaryKeys($commonConsultationArchiveBloc->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonConsultationArchiveBloc() only accepts arguments of type CommonConsultationArchiveBloc or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultationArchiveBloc relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function joinCommonConsultationArchiveBloc($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultationArchiveBloc');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultationArchiveBloc');
        }

        return $this;
    }

    /**
     * Use the CommonConsultationArchiveBloc relation CommonConsultationArchiveBloc object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationArchiveBlocQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationArchiveBlocQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonConsultationArchiveBloc($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultationArchiveBloc', '\Application\Propel\Mpe\CommonConsultationArchiveBlocQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonConsultationArchive $commonConsultationArchive Object to remove from the list of results
     *
     * @return CommonConsultationArchiveQuery The current query, for fluid interface
     */
    public function prune($commonConsultationArchive = null)
    {
        if ($commonConsultationArchive) {
            $this->addUsingAlias(CommonConsultationArchivePeer::ID, $commonConsultationArchive->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
