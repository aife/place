<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTListeLotsCandidature;
use Application\Propel\Mpe\CommonTListeLotsCandidaturePeer;
use Application\Propel\Mpe\CommonTListeLotsCandidatureQuery;

/**
 * Base class that represents a query for the 't_liste_lots_candidature' table.
 *
 *
 *
 * @method CommonTListeLotsCandidatureQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTListeLotsCandidatureQuery orderByRefConsultation($order = Criteria::ASC) Order by the ref_consultation column
 * @method CommonTListeLotsCandidatureQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTListeLotsCandidatureQuery orderByOldIdInscrit($order = Criteria::ASC) Order by the old_id_inscrit column
 * @method CommonTListeLotsCandidatureQuery orderByIdEntreprise($order = Criteria::ASC) Order by the id_entreprise column
 * @method CommonTListeLotsCandidatureQuery orderByIdEtablissement($order = Criteria::ASC) Order by the id_etablissement column
 * @method CommonTListeLotsCandidatureQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method CommonTListeLotsCandidatureQuery orderByIdCandidature($order = Criteria::ASC) Order by the id_candidature column
 * @method CommonTListeLotsCandidatureQuery orderByNumLot($order = Criteria::ASC) Order by the num_lot column
 * @method CommonTListeLotsCandidatureQuery orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 * @method CommonTListeLotsCandidatureQuery orderByIdInscrit($order = Criteria::ASC) Order by the id_inscrit column
 *
 * @method CommonTListeLotsCandidatureQuery groupById() Group by the id column
 * @method CommonTListeLotsCandidatureQuery groupByRefConsultation() Group by the ref_consultation column
 * @method CommonTListeLotsCandidatureQuery groupByOrganisme() Group by the organisme column
 * @method CommonTListeLotsCandidatureQuery groupByOldIdInscrit() Group by the old_id_inscrit column
 * @method CommonTListeLotsCandidatureQuery groupByIdEntreprise() Group by the id_entreprise column
 * @method CommonTListeLotsCandidatureQuery groupByIdEtablissement() Group by the id_etablissement column
 * @method CommonTListeLotsCandidatureQuery groupByStatus() Group by the status column
 * @method CommonTListeLotsCandidatureQuery groupByIdCandidature() Group by the id_candidature column
 * @method CommonTListeLotsCandidatureQuery groupByNumLot() Group by the num_lot column
 * @method CommonTListeLotsCandidatureQuery groupByConsultationId() Group by the consultation_id column
 * @method CommonTListeLotsCandidatureQuery groupByIdInscrit() Group by the id_inscrit column
 *
 * @method CommonTListeLotsCandidatureQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTListeLotsCandidatureQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTListeLotsCandidatureQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTListeLotsCandidatureQuery leftJoinCommonTCandidature($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCandidature relation
 * @method CommonTListeLotsCandidatureQuery rightJoinCommonTCandidature($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCandidature relation
 * @method CommonTListeLotsCandidatureQuery innerJoinCommonTCandidature($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCandidature relation
 *
 * @method CommonTListeLotsCandidatureQuery leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonTListeLotsCandidatureQuery rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonTListeLotsCandidatureQuery innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonTListeLotsCandidatureQuery leftJoinCommonInscrit($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonInscrit relation
 * @method CommonTListeLotsCandidatureQuery rightJoinCommonInscrit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonInscrit relation
 * @method CommonTListeLotsCandidatureQuery innerJoinCommonInscrit($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonInscrit relation
 *
 * @method CommonTListeLotsCandidatureQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonTListeLotsCandidatureQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonTListeLotsCandidatureQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonTListeLotsCandidature findOne(PropelPDO $con = null) Return the first CommonTListeLotsCandidature matching the query
 * @method CommonTListeLotsCandidature findOneOrCreate(PropelPDO $con = null) Return the first CommonTListeLotsCandidature matching the query, or a new CommonTListeLotsCandidature object populated from the query conditions when no match is found
 *
 * @method CommonTListeLotsCandidature findOneByRefConsultation(int $ref_consultation) Return the first CommonTListeLotsCandidature filtered by the ref_consultation column
 * @method CommonTListeLotsCandidature findOneByOrganisme(string $organisme) Return the first CommonTListeLotsCandidature filtered by the organisme column
 * @method CommonTListeLotsCandidature findOneByOldIdInscrit(int $old_id_inscrit) Return the first CommonTListeLotsCandidature filtered by the old_id_inscrit column
 * @method CommonTListeLotsCandidature findOneByIdEntreprise(int $id_entreprise) Return the first CommonTListeLotsCandidature filtered by the id_entreprise column
 * @method CommonTListeLotsCandidature findOneByIdEtablissement(int $id_etablissement) Return the first CommonTListeLotsCandidature filtered by the id_etablissement column
 * @method CommonTListeLotsCandidature findOneByStatus(int $status) Return the first CommonTListeLotsCandidature filtered by the status column
 * @method CommonTListeLotsCandidature findOneByIdCandidature(int $id_candidature) Return the first CommonTListeLotsCandidature filtered by the id_candidature column
 * @method CommonTListeLotsCandidature findOneByNumLot(int $num_lot) Return the first CommonTListeLotsCandidature filtered by the num_lot column
 * @method CommonTListeLotsCandidature findOneByConsultationId(int $consultation_id) Return the first CommonTListeLotsCandidature filtered by the consultation_id column
 * @method CommonTListeLotsCandidature findOneByIdInscrit(string $id_inscrit) Return the first CommonTListeLotsCandidature filtered by the id_inscrit column
 *
 * @method array findById(int $id) Return CommonTListeLotsCandidature objects filtered by the id column
 * @method array findByRefConsultation(int $ref_consultation) Return CommonTListeLotsCandidature objects filtered by the ref_consultation column
 * @method array findByOrganisme(string $organisme) Return CommonTListeLotsCandidature objects filtered by the organisme column
 * @method array findByOldIdInscrit(int $old_id_inscrit) Return CommonTListeLotsCandidature objects filtered by the old_id_inscrit column
 * @method array findByIdEntreprise(int $id_entreprise) Return CommonTListeLotsCandidature objects filtered by the id_entreprise column
 * @method array findByIdEtablissement(int $id_etablissement) Return CommonTListeLotsCandidature objects filtered by the id_etablissement column
 * @method array findByStatus(int $status) Return CommonTListeLotsCandidature objects filtered by the status column
 * @method array findByIdCandidature(int $id_candidature) Return CommonTListeLotsCandidature objects filtered by the id_candidature column
 * @method array findByNumLot(int $num_lot) Return CommonTListeLotsCandidature objects filtered by the num_lot column
 * @method array findByConsultationId(int $consultation_id) Return CommonTListeLotsCandidature objects filtered by the consultation_id column
 * @method array findByIdInscrit(string $id_inscrit) Return CommonTListeLotsCandidature objects filtered by the id_inscrit column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTListeLotsCandidatureQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTListeLotsCandidatureQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTListeLotsCandidature', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTListeLotsCandidatureQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTListeLotsCandidatureQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTListeLotsCandidatureQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTListeLotsCandidatureQuery) {
            return $criteria;
        }
        $query = new CommonTListeLotsCandidatureQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTListeLotsCandidature|CommonTListeLotsCandidature[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTListeLotsCandidaturePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTListeLotsCandidature A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTListeLotsCandidature A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `ref_consultation`, `organisme`, `old_id_inscrit`, `id_entreprise`, `id_etablissement`, `status`, `id_candidature`, `num_lot`, `consultation_id`, `id_inscrit` FROM `t_liste_lots_candidature` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTListeLotsCandidature();
            $obj->hydrate($row);
            CommonTListeLotsCandidaturePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTListeLotsCandidature|CommonTListeLotsCandidature[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTListeLotsCandidature[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the ref_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByRefConsultation(1234); // WHERE ref_consultation = 1234
     * $query->filterByRefConsultation(array(12, 34)); // WHERE ref_consultation IN (12, 34)
     * $query->filterByRefConsultation(array('min' => 12)); // WHERE ref_consultation >= 12
     * $query->filterByRefConsultation(array('max' => 12)); // WHERE ref_consultation <= 12
     * </code>
     *
     * @param     mixed $refConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByRefConsultation($refConsultation = null, $comparison = null)
    {
        if (is_array($refConsultation)) {
            $useMinMax = false;
            if (isset($refConsultation['min'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::REF_CONSULTATION, $refConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($refConsultation['max'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::REF_CONSULTATION, $refConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::REF_CONSULTATION, $refConsultation, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the old_id_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdInscrit(1234); // WHERE old_id_inscrit = 1234
     * $query->filterByOldIdInscrit(array(12, 34)); // WHERE old_id_inscrit IN (12, 34)
     * $query->filterByOldIdInscrit(array('min' => 12)); // WHERE old_id_inscrit >= 12
     * $query->filterByOldIdInscrit(array('max' => 12)); // WHERE old_id_inscrit <= 12
     * </code>
     *
     * @param     mixed $oldIdInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByOldIdInscrit($oldIdInscrit = null, $comparison = null)
    {
        if (is_array($oldIdInscrit)) {
            $useMinMax = false;
            if (isset($oldIdInscrit['min'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::OLD_ID_INSCRIT, $oldIdInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdInscrit['max'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::OLD_ID_INSCRIT, $oldIdInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::OLD_ID_INSCRIT, $oldIdInscrit, $comparison);
    }

    /**
     * Filter the query on the id_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntreprise(1234); // WHERE id_entreprise = 1234
     * $query->filterByIdEntreprise(array(12, 34)); // WHERE id_entreprise IN (12, 34)
     * $query->filterByIdEntreprise(array('min' => 12)); // WHERE id_entreprise >= 12
     * $query->filterByIdEntreprise(array('max' => 12)); // WHERE id_entreprise <= 12
     * </code>
     *
     * @param     mixed $idEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByIdEntreprise($idEntreprise = null, $comparison = null)
    {
        if (is_array($idEntreprise)) {
            $useMinMax = false;
            if (isset($idEntreprise['min'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_ENTREPRISE, $idEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntreprise['max'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_ENTREPRISE, $idEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_ENTREPRISE, $idEntreprise, $comparison);
    }

    /**
     * Filter the query on the id_etablissement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtablissement(1234); // WHERE id_etablissement = 1234
     * $query->filterByIdEtablissement(array(12, 34)); // WHERE id_etablissement IN (12, 34)
     * $query->filterByIdEtablissement(array('min' => 12)); // WHERE id_etablissement >= 12
     * $query->filterByIdEtablissement(array('max' => 12)); // WHERE id_etablissement <= 12
     * </code>
     *
     * @param     mixed $idEtablissement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByIdEtablissement($idEtablissement = null, $comparison = null)
    {
        if (is_array($idEtablissement)) {
            $useMinMax = false;
            if (isset($idEtablissement['min'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_ETABLISSEMENT, $idEtablissement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtablissement['max'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_ETABLISSEMENT, $idEtablissement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_ETABLISSEMENT, $idEtablissement, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status >= 12
     * $query->filterByStatus(array('max' => 12)); // WHERE status <= 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the id_candidature column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCandidature(1234); // WHERE id_candidature = 1234
     * $query->filterByIdCandidature(array(12, 34)); // WHERE id_candidature IN (12, 34)
     * $query->filterByIdCandidature(array('min' => 12)); // WHERE id_candidature >= 12
     * $query->filterByIdCandidature(array('max' => 12)); // WHERE id_candidature <= 12
     * </code>
     *
     * @see       filterByCommonTCandidature()
     *
     * @param     mixed $idCandidature The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByIdCandidature($idCandidature = null, $comparison = null)
    {
        if (is_array($idCandidature)) {
            $useMinMax = false;
            if (isset($idCandidature['min'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, $idCandidature['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCandidature['max'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, $idCandidature['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, $idCandidature, $comparison);
    }

    /**
     * Filter the query on the num_lot column
     *
     * Example usage:
     * <code>
     * $query->filterByNumLot(1234); // WHERE num_lot = 1234
     * $query->filterByNumLot(array(12, 34)); // WHERE num_lot IN (12, 34)
     * $query->filterByNumLot(array('min' => 12)); // WHERE num_lot >= 12
     * $query->filterByNumLot(array('max' => 12)); // WHERE num_lot <= 12
     * </code>
     *
     * @param     mixed $numLot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByNumLot($numLot = null, $comparison = null)
    {
        if (is_array($numLot)) {
            $useMinMax = false;
            if (isset($numLot['min'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::NUM_LOT, $numLot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numLot['max'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::NUM_LOT, $numLot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::NUM_LOT, $numLot, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultation()
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Filter the query on the id_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInscrit(1234); // WHERE id_inscrit = 1234
     * $query->filterByIdInscrit(array(12, 34)); // WHERE id_inscrit IN (12, 34)
     * $query->filterByIdInscrit(array('min' => 12)); // WHERE id_inscrit >= 12
     * $query->filterByIdInscrit(array('max' => 12)); // WHERE id_inscrit <= 12
     * </code>
     *
     * @see       filterByCommonInscrit()
     *
     * @param     mixed $idInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function filterByIdInscrit($idInscrit = null, $comparison = null)
    {
        if (is_array($idInscrit)) {
            $useMinMax = false;
            if (isset($idInscrit['min'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_INSCRIT, $idInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInscrit['max'])) {
                $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_INSCRIT, $idInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_INSCRIT, $idInscrit, $comparison);
    }

    /**
     * Filter the query by a related CommonTCandidature object
     *
     * @param   CommonTCandidature|PropelObjectCollection $commonTCandidature The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTListeLotsCandidatureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCandidature($commonTCandidature, $comparison = null)
    {
        if ($commonTCandidature instanceof CommonTCandidature) {
            return $this
                ->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, $commonTCandidature->getId(), $comparison);
        } elseif ($commonTCandidature instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, $commonTCandidature->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTCandidature() only accepts arguments of type CommonTCandidature or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCandidature relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function joinCommonTCandidature($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCandidature');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCandidature');
        }

        return $this;
    }

    /**
     * Use the CommonTCandidature relation CommonTCandidature object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCandidatureQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCandidatureQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTCandidature($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCandidature', '\Application\Propel\Mpe\CommonTCandidatureQuery');
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTListeLotsCandidatureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, $commonConsultation->getId(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, $commonConsultation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Filter the query by a related CommonInscrit object
     *
     * @param   CommonInscrit|PropelObjectCollection $commonInscrit The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTListeLotsCandidatureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonInscrit($commonInscrit, $comparison = null)
    {
        if ($commonInscrit instanceof CommonInscrit) {
            return $this
                ->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_INSCRIT, $commonInscrit->getId(), $comparison);
        } elseif ($commonInscrit instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTListeLotsCandidaturePeer::ID_INSCRIT, $commonInscrit->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonInscrit() only accepts arguments of type CommonInscrit or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonInscrit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function joinCommonInscrit($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonInscrit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonInscrit');
        }

        return $this;
    }

    /**
     * Use the CommonInscrit relation CommonInscrit object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonInscritQuery A secondary query class using the current class as primary query
     */
    public function useCommonInscritQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonInscrit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonInscrit', '\Application\Propel\Mpe\CommonInscritQuery');
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTListeLotsCandidatureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonTListeLotsCandidaturePeer::ORGANISME, $commonOrganisme->getAcronyme(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTListeLotsCandidaturePeer::ORGANISME, $commonOrganisme->toKeyValue('PrimaryKey', 'Acronyme'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTListeLotsCandidature $commonTListeLotsCandidature Object to remove from the list of results
     *
     * @return CommonTListeLotsCandidatureQuery The current query, for fluid interface
     */
    public function prune($commonTListeLotsCandidature = null)
    {
        if ($commonTListeLotsCandidature) {
            $this->addUsingAlias(CommonTListeLotsCandidaturePeer::ID, $commonTListeLotsCandidature->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
