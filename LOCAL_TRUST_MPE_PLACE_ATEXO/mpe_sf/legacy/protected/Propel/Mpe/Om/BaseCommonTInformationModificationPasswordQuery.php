<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTInformationModificationPassword;
use Application\Propel\Mpe\CommonTInformationModificationPasswordPeer;
use Application\Propel\Mpe\CommonTInformationModificationPasswordQuery;

/**
 * Base class that represents a query for the 't_information_modification_password' table.
 *
 *
 *
 * @method CommonTInformationModificationPasswordQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTInformationModificationPasswordQuery orderByIdUser($order = Criteria::ASC) Order by the id_user column
 * @method CommonTInformationModificationPasswordQuery orderByTypeUser($order = Criteria::ASC) Order by the type_user column
 * @method CommonTInformationModificationPasswordQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method CommonTInformationModificationPasswordQuery orderByDateDemandeModification($order = Criteria::ASC) Order by the date_demande_modification column
 * @method CommonTInformationModificationPasswordQuery orderByDateFinValidite($order = Criteria::ASC) Order by the date_fin_validite column
 * @method CommonTInformationModificationPasswordQuery orderByJeton($order = Criteria::ASC) Order by the jeton column
 * @method CommonTInformationModificationPasswordQuery orderByModificationFaite($order = Criteria::ASC) Order by the modification_faite column
 *
 * @method CommonTInformationModificationPasswordQuery groupById() Group by the id column
 * @method CommonTInformationModificationPasswordQuery groupByIdUser() Group by the id_user column
 * @method CommonTInformationModificationPasswordQuery groupByTypeUser() Group by the type_user column
 * @method CommonTInformationModificationPasswordQuery groupByEmail() Group by the email column
 * @method CommonTInformationModificationPasswordQuery groupByDateDemandeModification() Group by the date_demande_modification column
 * @method CommonTInformationModificationPasswordQuery groupByDateFinValidite() Group by the date_fin_validite column
 * @method CommonTInformationModificationPasswordQuery groupByJeton() Group by the jeton column
 * @method CommonTInformationModificationPasswordQuery groupByModificationFaite() Group by the modification_faite column
 *
 * @method CommonTInformationModificationPasswordQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTInformationModificationPasswordQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTInformationModificationPasswordQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTInformationModificationPassword findOne(PropelPDO $con = null) Return the first CommonTInformationModificationPassword matching the query
 * @method CommonTInformationModificationPassword findOneOrCreate(PropelPDO $con = null) Return the first CommonTInformationModificationPassword matching the query, or a new CommonTInformationModificationPassword object populated from the query conditions when no match is found
 *
 * @method CommonTInformationModificationPassword findOneByIdUser(int $id_user) Return the first CommonTInformationModificationPassword filtered by the id_user column
 * @method CommonTInformationModificationPassword findOneByTypeUser(string $type_user) Return the first CommonTInformationModificationPassword filtered by the type_user column
 * @method CommonTInformationModificationPassword findOneByEmail(string $email) Return the first CommonTInformationModificationPassword filtered by the email column
 * @method CommonTInformationModificationPassword findOneByDateDemandeModification(string $date_demande_modification) Return the first CommonTInformationModificationPassword filtered by the date_demande_modification column
 * @method CommonTInformationModificationPassword findOneByDateFinValidite(string $date_fin_validite) Return the first CommonTInformationModificationPassword filtered by the date_fin_validite column
 * @method CommonTInformationModificationPassword findOneByJeton(string $jeton) Return the first CommonTInformationModificationPassword filtered by the jeton column
 * @method CommonTInformationModificationPassword findOneByModificationFaite(string $modification_faite) Return the first CommonTInformationModificationPassword filtered by the modification_faite column
 *
 * @method array findById(int $id) Return CommonTInformationModificationPassword objects filtered by the id column
 * @method array findByIdUser(int $id_user) Return CommonTInformationModificationPassword objects filtered by the id_user column
 * @method array findByTypeUser(string $type_user) Return CommonTInformationModificationPassword objects filtered by the type_user column
 * @method array findByEmail(string $email) Return CommonTInformationModificationPassword objects filtered by the email column
 * @method array findByDateDemandeModification(string $date_demande_modification) Return CommonTInformationModificationPassword objects filtered by the date_demande_modification column
 * @method array findByDateFinValidite(string $date_fin_validite) Return CommonTInformationModificationPassword objects filtered by the date_fin_validite column
 * @method array findByJeton(string $jeton) Return CommonTInformationModificationPassword objects filtered by the jeton column
 * @method array findByModificationFaite(string $modification_faite) Return CommonTInformationModificationPassword objects filtered by the modification_faite column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTInformationModificationPasswordQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTInformationModificationPasswordQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTInformationModificationPassword', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTInformationModificationPasswordQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTInformationModificationPasswordQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTInformationModificationPasswordQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTInformationModificationPasswordQuery) {
            return $criteria;
        }
        $query = new CommonTInformationModificationPasswordQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTInformationModificationPassword|CommonTInformationModificationPassword[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTInformationModificationPasswordPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTInformationModificationPasswordPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTInformationModificationPassword A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTInformationModificationPassword A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_user`, `type_user`, `email`, `date_demande_modification`, `date_fin_validite`, `jeton`, `modification_faite` FROM `t_information_modification_password` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTInformationModificationPassword();
            $obj->hydrate($row);
            CommonTInformationModificationPasswordPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTInformationModificationPassword|CommonTInformationModificationPassword[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTInformationModificationPassword[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTInformationModificationPasswordQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTInformationModificationPasswordPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTInformationModificationPasswordQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTInformationModificationPasswordPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTInformationModificationPasswordQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTInformationModificationPasswordPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTInformationModificationPasswordPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTInformationModificationPasswordPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_user column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUser(1234); // WHERE id_user = 1234
     * $query->filterByIdUser(array(12, 34)); // WHERE id_user IN (12, 34)
     * $query->filterByIdUser(array('min' => 12)); // WHERE id_user >= 12
     * $query->filterByIdUser(array('max' => 12)); // WHERE id_user <= 12
     * </code>
     *
     * @param     mixed $idUser The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTInformationModificationPasswordQuery The current query, for fluid interface
     */
    public function filterByIdUser($idUser = null, $comparison = null)
    {
        if (is_array($idUser)) {
            $useMinMax = false;
            if (isset($idUser['min'])) {
                $this->addUsingAlias(CommonTInformationModificationPasswordPeer::ID_USER, $idUser['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUser['max'])) {
                $this->addUsingAlias(CommonTInformationModificationPasswordPeer::ID_USER, $idUser['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTInformationModificationPasswordPeer::ID_USER, $idUser, $comparison);
    }

    /**
     * Filter the query on the type_user column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeUser('fooValue');   // WHERE type_user = 'fooValue'
     * $query->filterByTypeUser('%fooValue%'); // WHERE type_user LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeUser The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTInformationModificationPasswordQuery The current query, for fluid interface
     */
    public function filterByTypeUser($typeUser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeUser)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeUser)) {
                $typeUser = str_replace('*', '%', $typeUser);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTInformationModificationPasswordPeer::TYPE_USER, $typeUser, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTInformationModificationPasswordQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTInformationModificationPasswordPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the date_demande_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateDemandeModification('fooValue');   // WHERE date_demande_modification = 'fooValue'
     * $query->filterByDateDemandeModification('%fooValue%'); // WHERE date_demande_modification LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dateDemandeModification The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTInformationModificationPasswordQuery The current query, for fluid interface
     */
    public function filterByDateDemandeModification($dateDemandeModification = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dateDemandeModification)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dateDemandeModification)) {
                $dateDemandeModification = str_replace('*', '%', $dateDemandeModification);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTInformationModificationPasswordPeer::DATE_DEMANDE_MODIFICATION, $dateDemandeModification, $comparison);
    }

    /**
     * Filter the query on the date_fin_validite column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFinValidite('fooValue');   // WHERE date_fin_validite = 'fooValue'
     * $query->filterByDateFinValidite('%fooValue%'); // WHERE date_fin_validite LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dateFinValidite The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTInformationModificationPasswordQuery The current query, for fluid interface
     */
    public function filterByDateFinValidite($dateFinValidite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dateFinValidite)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dateFinValidite)) {
                $dateFinValidite = str_replace('*', '%', $dateFinValidite);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTInformationModificationPasswordPeer::DATE_FIN_VALIDITE, $dateFinValidite, $comparison);
    }

    /**
     * Filter the query on the jeton column
     *
     * Example usage:
     * <code>
     * $query->filterByJeton('fooValue');   // WHERE jeton = 'fooValue'
     * $query->filterByJeton('%fooValue%'); // WHERE jeton LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jeton The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTInformationModificationPasswordQuery The current query, for fluid interface
     */
    public function filterByJeton($jeton = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jeton)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $jeton)) {
                $jeton = str_replace('*', '%', $jeton);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTInformationModificationPasswordPeer::JETON, $jeton, $comparison);
    }

    /**
     * Filter the query on the modification_faite column
     *
     * Example usage:
     * <code>
     * $query->filterByModificationFaite('fooValue');   // WHERE modification_faite = 'fooValue'
     * $query->filterByModificationFaite('%fooValue%'); // WHERE modification_faite LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modificationFaite The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTInformationModificationPasswordQuery The current query, for fluid interface
     */
    public function filterByModificationFaite($modificationFaite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modificationFaite)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modificationFaite)) {
                $modificationFaite = str_replace('*', '%', $modificationFaite);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTInformationModificationPasswordPeer::MODIFICATION_FAITE, $modificationFaite, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTInformationModificationPassword $commonTInformationModificationPassword Object to remove from the list of results
     *
     * @return CommonTInformationModificationPasswordQuery The current query, for fluid interface
     */
    public function prune($commonTInformationModificationPassword = null)
    {
        if ($commonTInformationModificationPassword) {
            $this->addUsingAlias(CommonTInformationModificationPasswordPeer::ID, $commonTInformationModificationPassword->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
