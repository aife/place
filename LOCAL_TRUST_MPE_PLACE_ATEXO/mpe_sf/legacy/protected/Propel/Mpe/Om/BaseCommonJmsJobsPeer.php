<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonJmsJobs;
use Application\Propel\Mpe\CommonJmsJobsPeer;
use Application\Propel\Mpe\Map\CommonJmsJobsTableMap;

/**
 * Base static class for performing query and update operations on the 'jms_jobs' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonJmsJobsPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'jms_jobs';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonJmsJobs';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonJmsJobsTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 22;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 22;

    /** the column name for the id field */
    const ID = 'jms_jobs.id';

    /** the column name for the state field */
    const STATE = 'jms_jobs.state';

    /** the column name for the queue field */
    const QUEUE = 'jms_jobs.queue';

    /** the column name for the priority field */
    const PRIORITY = 'jms_jobs.priority';

    /** the column name for the createdAt field */
    const CREATEDAT = 'jms_jobs.createdAt';

    /** the column name for the startedAt field */
    const STARTEDAT = 'jms_jobs.startedAt';

    /** the column name for the checkedAt field */
    const CHECKEDAT = 'jms_jobs.checkedAt';

    /** the column name for the workerName field */
    const WORKERNAME = 'jms_jobs.workerName';

    /** the column name for the executeAfter field */
    const EXECUTEAFTER = 'jms_jobs.executeAfter';

    /** the column name for the closedAt field */
    const CLOSEDAT = 'jms_jobs.closedAt';

    /** the column name for the command field */
    const COMMAND = 'jms_jobs.command';

    /** the column name for the args field */
    const ARGS = 'jms_jobs.args';

    /** the column name for the output field */
    const OUTPUT = 'jms_jobs.output';

    /** the column name for the errorOutput field */
    const ERROROUTPUT = 'jms_jobs.errorOutput';

    /** the column name for the exitCode field */
    const EXITCODE = 'jms_jobs.exitCode';

    /** the column name for the maxRuntime field */
    const MAXRUNTIME = 'jms_jobs.maxRuntime';

    /** the column name for the maxRetries field */
    const MAXRETRIES = 'jms_jobs.maxRetries';

    /** the column name for the stackTrace field */
    const STACKTRACE = 'jms_jobs.stackTrace';

    /** the column name for the runtime field */
    const RUNTIME = 'jms_jobs.runtime';

    /** the column name for the memoryUsage field */
    const MEMORYUSAGE = 'jms_jobs.memoryUsage';

    /** the column name for the memoryUsageReal field */
    const MEMORYUSAGEREAL = 'jms_jobs.memoryUsageReal';

    /** the column name for the originalJob_id field */
    const ORIGINALJOB_ID = 'jms_jobs.originalJob_id';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonJmsJobs objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonJmsJobs[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonJmsJobsPeer::$fieldNames[CommonJmsJobsPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'State', 'Queue', 'Priority', 'Createdat', 'Startedat', 'Checkedat', 'Workername', 'Executeafter', 'Closedat', 'Command', 'Args', 'Output', 'Erroroutput', 'Exitcode', 'Maxruntime', 'Maxretries', 'Stacktrace', 'Runtime', 'Memoryusage', 'Memoryusagereal', 'OriginaljobId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'state', 'queue', 'priority', 'createdat', 'startedat', 'checkedat', 'workername', 'executeafter', 'closedat', 'command', 'args', 'output', 'erroroutput', 'exitcode', 'maxruntime', 'maxretries', 'stacktrace', 'runtime', 'memoryusage', 'memoryusagereal', 'originaljobId', ),
        BasePeer::TYPE_COLNAME => array (CommonJmsJobsPeer::ID, CommonJmsJobsPeer::STATE, CommonJmsJobsPeer::QUEUE, CommonJmsJobsPeer::PRIORITY, CommonJmsJobsPeer::CREATEDAT, CommonJmsJobsPeer::STARTEDAT, CommonJmsJobsPeer::CHECKEDAT, CommonJmsJobsPeer::WORKERNAME, CommonJmsJobsPeer::EXECUTEAFTER, CommonJmsJobsPeer::CLOSEDAT, CommonJmsJobsPeer::COMMAND, CommonJmsJobsPeer::ARGS, CommonJmsJobsPeer::OUTPUT, CommonJmsJobsPeer::ERROROUTPUT, CommonJmsJobsPeer::EXITCODE, CommonJmsJobsPeer::MAXRUNTIME, CommonJmsJobsPeer::MAXRETRIES, CommonJmsJobsPeer::STACKTRACE, CommonJmsJobsPeer::RUNTIME, CommonJmsJobsPeer::MEMORYUSAGE, CommonJmsJobsPeer::MEMORYUSAGEREAL, CommonJmsJobsPeer::ORIGINALJOB_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'STATE', 'QUEUE', 'PRIORITY', 'CREATEDAT', 'STARTEDAT', 'CHECKEDAT', 'WORKERNAME', 'EXECUTEAFTER', 'CLOSEDAT', 'COMMAND', 'ARGS', 'OUTPUT', 'ERROROUTPUT', 'EXITCODE', 'MAXRUNTIME', 'MAXRETRIES', 'STACKTRACE', 'RUNTIME', 'MEMORYUSAGE', 'MEMORYUSAGEREAL', 'ORIGINALJOB_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'state', 'queue', 'priority', 'createdAt', 'startedAt', 'checkedAt', 'workerName', 'executeAfter', 'closedAt', 'command', 'args', 'output', 'errorOutput', 'exitCode', 'maxRuntime', 'maxRetries', 'stackTrace', 'runtime', 'memoryUsage', 'memoryUsageReal', 'originalJob_id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonJmsJobsPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'State' => 1, 'Queue' => 2, 'Priority' => 3, 'Createdat' => 4, 'Startedat' => 5, 'Checkedat' => 6, 'Workername' => 7, 'Executeafter' => 8, 'Closedat' => 9, 'Command' => 10, 'Args' => 11, 'Output' => 12, 'Erroroutput' => 13, 'Exitcode' => 14, 'Maxruntime' => 15, 'Maxretries' => 16, 'Stacktrace' => 17, 'Runtime' => 18, 'Memoryusage' => 19, 'Memoryusagereal' => 20, 'OriginaljobId' => 21, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'state' => 1, 'queue' => 2, 'priority' => 3, 'createdat' => 4, 'startedat' => 5, 'checkedat' => 6, 'workername' => 7, 'executeafter' => 8, 'closedat' => 9, 'command' => 10, 'args' => 11, 'output' => 12, 'erroroutput' => 13, 'exitcode' => 14, 'maxruntime' => 15, 'maxretries' => 16, 'stacktrace' => 17, 'runtime' => 18, 'memoryusage' => 19, 'memoryusagereal' => 20, 'originaljobId' => 21, ),
        BasePeer::TYPE_COLNAME => array (CommonJmsJobsPeer::ID => 0, CommonJmsJobsPeer::STATE => 1, CommonJmsJobsPeer::QUEUE => 2, CommonJmsJobsPeer::PRIORITY => 3, CommonJmsJobsPeer::CREATEDAT => 4, CommonJmsJobsPeer::STARTEDAT => 5, CommonJmsJobsPeer::CHECKEDAT => 6, CommonJmsJobsPeer::WORKERNAME => 7, CommonJmsJobsPeer::EXECUTEAFTER => 8, CommonJmsJobsPeer::CLOSEDAT => 9, CommonJmsJobsPeer::COMMAND => 10, CommonJmsJobsPeer::ARGS => 11, CommonJmsJobsPeer::OUTPUT => 12, CommonJmsJobsPeer::ERROROUTPUT => 13, CommonJmsJobsPeer::EXITCODE => 14, CommonJmsJobsPeer::MAXRUNTIME => 15, CommonJmsJobsPeer::MAXRETRIES => 16, CommonJmsJobsPeer::STACKTRACE => 17, CommonJmsJobsPeer::RUNTIME => 18, CommonJmsJobsPeer::MEMORYUSAGE => 19, CommonJmsJobsPeer::MEMORYUSAGEREAL => 20, CommonJmsJobsPeer::ORIGINALJOB_ID => 21, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'STATE' => 1, 'QUEUE' => 2, 'PRIORITY' => 3, 'CREATEDAT' => 4, 'STARTEDAT' => 5, 'CHECKEDAT' => 6, 'WORKERNAME' => 7, 'EXECUTEAFTER' => 8, 'CLOSEDAT' => 9, 'COMMAND' => 10, 'ARGS' => 11, 'OUTPUT' => 12, 'ERROROUTPUT' => 13, 'EXITCODE' => 14, 'MAXRUNTIME' => 15, 'MAXRETRIES' => 16, 'STACKTRACE' => 17, 'RUNTIME' => 18, 'MEMORYUSAGE' => 19, 'MEMORYUSAGEREAL' => 20, 'ORIGINALJOB_ID' => 21, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'state' => 1, 'queue' => 2, 'priority' => 3, 'createdAt' => 4, 'startedAt' => 5, 'checkedAt' => 6, 'workerName' => 7, 'executeAfter' => 8, 'closedAt' => 9, 'command' => 10, 'args' => 11, 'output' => 12, 'errorOutput' => 13, 'exitCode' => 14, 'maxRuntime' => 15, 'maxRetries' => 16, 'stackTrace' => 17, 'runtime' => 18, 'memoryUsage' => 19, 'memoryUsageReal' => 20, 'originalJob_id' => 21, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonJmsJobsPeer::getFieldNames($toType);
        $key = isset(CommonJmsJobsPeer::$fieldKeys[$fromType][$name]) ? CommonJmsJobsPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonJmsJobsPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonJmsJobsPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonJmsJobsPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonJmsJobsPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonJmsJobsPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonJmsJobsPeer::ID);
            $criteria->addSelectColumn(CommonJmsJobsPeer::STATE);
            $criteria->addSelectColumn(CommonJmsJobsPeer::QUEUE);
            $criteria->addSelectColumn(CommonJmsJobsPeer::PRIORITY);
            $criteria->addSelectColumn(CommonJmsJobsPeer::CREATEDAT);
            $criteria->addSelectColumn(CommonJmsJobsPeer::STARTEDAT);
            $criteria->addSelectColumn(CommonJmsJobsPeer::CHECKEDAT);
            $criteria->addSelectColumn(CommonJmsJobsPeer::WORKERNAME);
            $criteria->addSelectColumn(CommonJmsJobsPeer::EXECUTEAFTER);
            $criteria->addSelectColumn(CommonJmsJobsPeer::CLOSEDAT);
            $criteria->addSelectColumn(CommonJmsJobsPeer::COMMAND);
            $criteria->addSelectColumn(CommonJmsJobsPeer::ARGS);
            $criteria->addSelectColumn(CommonJmsJobsPeer::OUTPUT);
            $criteria->addSelectColumn(CommonJmsJobsPeer::ERROROUTPUT);
            $criteria->addSelectColumn(CommonJmsJobsPeer::EXITCODE);
            $criteria->addSelectColumn(CommonJmsJobsPeer::MAXRUNTIME);
            $criteria->addSelectColumn(CommonJmsJobsPeer::MAXRETRIES);
            $criteria->addSelectColumn(CommonJmsJobsPeer::STACKTRACE);
            $criteria->addSelectColumn(CommonJmsJobsPeer::RUNTIME);
            $criteria->addSelectColumn(CommonJmsJobsPeer::MEMORYUSAGE);
            $criteria->addSelectColumn(CommonJmsJobsPeer::MEMORYUSAGEREAL);
            $criteria->addSelectColumn(CommonJmsJobsPeer::ORIGINALJOB_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.state');
            $criteria->addSelectColumn($alias . '.queue');
            $criteria->addSelectColumn($alias . '.priority');
            $criteria->addSelectColumn($alias . '.createdAt');
            $criteria->addSelectColumn($alias . '.startedAt');
            $criteria->addSelectColumn($alias . '.checkedAt');
            $criteria->addSelectColumn($alias . '.workerName');
            $criteria->addSelectColumn($alias . '.executeAfter');
            $criteria->addSelectColumn($alias . '.closedAt');
            $criteria->addSelectColumn($alias . '.command');
            $criteria->addSelectColumn($alias . '.args');
            $criteria->addSelectColumn($alias . '.output');
            $criteria->addSelectColumn($alias . '.errorOutput');
            $criteria->addSelectColumn($alias . '.exitCode');
            $criteria->addSelectColumn($alias . '.maxRuntime');
            $criteria->addSelectColumn($alias . '.maxRetries');
            $criteria->addSelectColumn($alias . '.stackTrace');
            $criteria->addSelectColumn($alias . '.runtime');
            $criteria->addSelectColumn($alias . '.memoryUsage');
            $criteria->addSelectColumn($alias . '.memoryUsageReal');
            $criteria->addSelectColumn($alias . '.originalJob_id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonJmsJobsPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonJmsJobsPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonJmsJobsPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonJmsJobs
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonJmsJobsPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonJmsJobsPeer::populateObjects(CommonJmsJobsPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonJmsJobsPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonJmsJobsPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonJmsJobs $obj A CommonJmsJobs object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonJmsJobsPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonJmsJobs object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonJmsJobs) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonJmsJobs object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonJmsJobsPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonJmsJobs Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonJmsJobsPeer::$instances[$key])) {
                return CommonJmsJobsPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonJmsJobsPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonJmsJobsPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to jms_jobs
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (string) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonJmsJobsPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonJmsJobsPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonJmsJobsPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonJmsJobsPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonJmsJobs object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonJmsJobsPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonJmsJobsPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonJmsJobsPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonJmsJobsPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonJmsJobsPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonJmsJobsPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonJmsJobsPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonJmsJobsPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonJmsJobs objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonJmsJobs objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonJmsJobsPeer::DATABASE_NAME);
        }

        CommonJmsJobsPeer::addSelectColumns($criteria);
        $startcol2 = CommonJmsJobsPeer::NUM_HYDRATE_COLUMNS;

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonJmsJobsPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonJmsJobsPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonJmsJobsPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonJmsJobsPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonJmsJobsPeer::DATABASE_NAME)->getTable(CommonJmsJobsPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonJmsJobsPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonJmsJobsPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonJmsJobsTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonJmsJobsPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonJmsJobs or Criteria object.
     *
     * @param      mixed $values Criteria or CommonJmsJobs object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonJmsJobs object
        }

        if ($criteria->containsKey(CommonJmsJobsPeer::ID) && $criteria->keyContainsValue(CommonJmsJobsPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonJmsJobsPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonJmsJobsPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonJmsJobs or Criteria object.
     *
     * @param      mixed $values Criteria or CommonJmsJobs object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonJmsJobsPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonJmsJobsPeer::ID);
            $value = $criteria->remove(CommonJmsJobsPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonJmsJobsPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonJmsJobsPeer::TABLE_NAME);
            }

        } else { // $values is CommonJmsJobs object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonJmsJobsPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the jms_jobs table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonJmsJobsPeer::TABLE_NAME, $con, CommonJmsJobsPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonJmsJobsPeer::clearInstancePool();
            CommonJmsJobsPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonJmsJobs or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonJmsJobs object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonJmsJobsPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonJmsJobs) { // it's a model object
            // invalidate the cache for this single object
            CommonJmsJobsPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonJmsJobsPeer::DATABASE_NAME);
            $criteria->add(CommonJmsJobsPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonJmsJobsPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonJmsJobsPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonJmsJobsPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonJmsJobs object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonJmsJobs $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonJmsJobsPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonJmsJobsPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonJmsJobsPeer::DATABASE_NAME, CommonJmsJobsPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      string $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonJmsJobs
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonJmsJobsPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonJmsJobsPeer::DATABASE_NAME);
        $criteria->add(CommonJmsJobsPeer::ID, $pk);

        $v = CommonJmsJobsPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonJmsJobs[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonJmsJobsPeer::DATABASE_NAME);
            $criteria->add(CommonJmsJobsPeer::ID, $pks, Criteria::IN);
            $objs = CommonJmsJobsPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonJmsJobsPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonJmsJobsPeer::buildTableMap();

