<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonCodeRetourPeer;
use Application\Propel\Mpe\CommonEchangesInterfaces;
use Application\Propel\Mpe\CommonEchangesInterfacesPeer;
use Application\Propel\Mpe\Map\CommonEchangesInterfacesTableMap;

/**
 * Base static class for performing query and update operations on the 'echanges_interfaces' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangesInterfacesPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'echanges_interfaces';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonEchangesInterfaces';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonEchangesInterfacesTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 14;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 14;

    /** the column name for the id field */
    const ID = 'echanges_interfaces.id';

    /** the column name for the code_retour field */
    const CODE_RETOUR = 'echanges_interfaces.code_retour';

    /** the column name for the service field */
    const SERVICE = 'echanges_interfaces.service';

    /** the column name for the nom_batch field */
    const NOM_BATCH = 'echanges_interfaces.nom_batch';

    /** the column name for the variables_entree field */
    const VARIABLES_ENTREE = 'echanges_interfaces.variables_entree';

    /** the column name for the resultat field */
    const RESULTAT = 'echanges_interfaces.resultat';

    /** the column name for the type_flux field */
    const TYPE_FLUX = 'echanges_interfaces.type_flux';

    /** the column name for the poids field */
    const POIDS = 'echanges_interfaces.poids';

    /** the column name for the information_metier field */
    const INFORMATION_METIER = 'echanges_interfaces.information_metier';

    /** the column name for the nb_flux field */
    const NB_FLUX = 'echanges_interfaces.nb_flux';

    /** the column name for the debut_execution field */
    const DEBUT_EXECUTION = 'echanges_interfaces.debut_execution';

    /** the column name for the fin_execution field */
    const FIN_EXECUTION = 'echanges_interfaces.fin_execution';

    /** the column name for the id_echanges_interfaces field */
    const ID_ECHANGES_INTERFACES = 'echanges_interfaces.id_echanges_interfaces';

    /** the column name for the nom_interface field */
    const NOM_INTERFACE = 'echanges_interfaces.nom_interface';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonEchangesInterfaces objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonEchangesInterfaces[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonEchangesInterfacesPeer::$fieldNames[CommonEchangesInterfacesPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'CodeRetour', 'Service', 'NomBatch', 'VariablesEntree', 'Resultat', 'TypeFlux', 'Poids', 'InformationMetier', 'NbFlux', 'DebutExecution', 'FinExecution', 'IdEchangesInterfaces', 'NomInterface', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'codeRetour', 'service', 'nomBatch', 'variablesEntree', 'resultat', 'typeFlux', 'poids', 'informationMetier', 'nbFlux', 'debutExecution', 'finExecution', 'idEchangesInterfaces', 'nomInterface', ),
        BasePeer::TYPE_COLNAME => array (CommonEchangesInterfacesPeer::ID, CommonEchangesInterfacesPeer::CODE_RETOUR, CommonEchangesInterfacesPeer::SERVICE, CommonEchangesInterfacesPeer::NOM_BATCH, CommonEchangesInterfacesPeer::VARIABLES_ENTREE, CommonEchangesInterfacesPeer::RESULTAT, CommonEchangesInterfacesPeer::TYPE_FLUX, CommonEchangesInterfacesPeer::POIDS, CommonEchangesInterfacesPeer::INFORMATION_METIER, CommonEchangesInterfacesPeer::NB_FLUX, CommonEchangesInterfacesPeer::DEBUT_EXECUTION, CommonEchangesInterfacesPeer::FIN_EXECUTION, CommonEchangesInterfacesPeer::ID_ECHANGES_INTERFACES, CommonEchangesInterfacesPeer::NOM_INTERFACE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'CODE_RETOUR', 'SERVICE', 'NOM_BATCH', 'VARIABLES_ENTREE', 'RESULTAT', 'TYPE_FLUX', 'POIDS', 'INFORMATION_METIER', 'NB_FLUX', 'DEBUT_EXECUTION', 'FIN_EXECUTION', 'ID_ECHANGES_INTERFACES', 'NOM_INTERFACE', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'code_retour', 'service', 'nom_batch', 'variables_entree', 'resultat', 'type_flux', 'poids', 'information_metier', 'nb_flux', 'debut_execution', 'fin_execution', 'id_echanges_interfaces', 'nom_interface', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonEchangesInterfacesPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'CodeRetour' => 1, 'Service' => 2, 'NomBatch' => 3, 'VariablesEntree' => 4, 'Resultat' => 5, 'TypeFlux' => 6, 'Poids' => 7, 'InformationMetier' => 8, 'NbFlux' => 9, 'DebutExecution' => 10, 'FinExecution' => 11, 'IdEchangesInterfaces' => 12, 'NomInterface' => 13, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'codeRetour' => 1, 'service' => 2, 'nomBatch' => 3, 'variablesEntree' => 4, 'resultat' => 5, 'typeFlux' => 6, 'poids' => 7, 'informationMetier' => 8, 'nbFlux' => 9, 'debutExecution' => 10, 'finExecution' => 11, 'idEchangesInterfaces' => 12, 'nomInterface' => 13, ),
        BasePeer::TYPE_COLNAME => array (CommonEchangesInterfacesPeer::ID => 0, CommonEchangesInterfacesPeer::CODE_RETOUR => 1, CommonEchangesInterfacesPeer::SERVICE => 2, CommonEchangesInterfacesPeer::NOM_BATCH => 3, CommonEchangesInterfacesPeer::VARIABLES_ENTREE => 4, CommonEchangesInterfacesPeer::RESULTAT => 5, CommonEchangesInterfacesPeer::TYPE_FLUX => 6, CommonEchangesInterfacesPeer::POIDS => 7, CommonEchangesInterfacesPeer::INFORMATION_METIER => 8, CommonEchangesInterfacesPeer::NB_FLUX => 9, CommonEchangesInterfacesPeer::DEBUT_EXECUTION => 10, CommonEchangesInterfacesPeer::FIN_EXECUTION => 11, CommonEchangesInterfacesPeer::ID_ECHANGES_INTERFACES => 12, CommonEchangesInterfacesPeer::NOM_INTERFACE => 13, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'CODE_RETOUR' => 1, 'SERVICE' => 2, 'NOM_BATCH' => 3, 'VARIABLES_ENTREE' => 4, 'RESULTAT' => 5, 'TYPE_FLUX' => 6, 'POIDS' => 7, 'INFORMATION_METIER' => 8, 'NB_FLUX' => 9, 'DEBUT_EXECUTION' => 10, 'FIN_EXECUTION' => 11, 'ID_ECHANGES_INTERFACES' => 12, 'NOM_INTERFACE' => 13, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'code_retour' => 1, 'service' => 2, 'nom_batch' => 3, 'variables_entree' => 4, 'resultat' => 5, 'type_flux' => 6, 'poids' => 7, 'information_metier' => 8, 'nb_flux' => 9, 'debut_execution' => 10, 'fin_execution' => 11, 'id_echanges_interfaces' => 12, 'nom_interface' => 13, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonEchangesInterfacesPeer::getFieldNames($toType);
        $key = isset(CommonEchangesInterfacesPeer::$fieldKeys[$fromType][$name]) ? CommonEchangesInterfacesPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonEchangesInterfacesPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonEchangesInterfacesPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonEchangesInterfacesPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonEchangesInterfacesPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonEchangesInterfacesPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::ID);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::CODE_RETOUR);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::SERVICE);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::NOM_BATCH);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::VARIABLES_ENTREE);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::RESULTAT);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::TYPE_FLUX);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::POIDS);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::INFORMATION_METIER);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::NB_FLUX);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::DEBUT_EXECUTION);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::FIN_EXECUTION);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::ID_ECHANGES_INTERFACES);
            $criteria->addSelectColumn(CommonEchangesInterfacesPeer::NOM_INTERFACE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.code_retour');
            $criteria->addSelectColumn($alias . '.service');
            $criteria->addSelectColumn($alias . '.nom_batch');
            $criteria->addSelectColumn($alias . '.variables_entree');
            $criteria->addSelectColumn($alias . '.resultat');
            $criteria->addSelectColumn($alias . '.type_flux');
            $criteria->addSelectColumn($alias . '.poids');
            $criteria->addSelectColumn($alias . '.information_metier');
            $criteria->addSelectColumn($alias . '.nb_flux');
            $criteria->addSelectColumn($alias . '.debut_execution');
            $criteria->addSelectColumn($alias . '.fin_execution');
            $criteria->addSelectColumn($alias . '.id_echanges_interfaces');
            $criteria->addSelectColumn($alias . '.nom_interface');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangesInterfacesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangesInterfacesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonEchangesInterfaces
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonEchangesInterfacesPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonEchangesInterfacesPeer::populateObjects(CommonEchangesInterfacesPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonEchangesInterfacesPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonEchangesInterfaces $obj A CommonEchangesInterfaces object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonEchangesInterfacesPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonEchangesInterfaces object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonEchangesInterfaces) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonEchangesInterfaces object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonEchangesInterfacesPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonEchangesInterfaces Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonEchangesInterfacesPeer::$instances[$key])) {
                return CommonEchangesInterfacesPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonEchangesInterfacesPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonEchangesInterfacesPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to echanges_interfaces
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in CommonEchangesInterfacesPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonEchangesInterfacesPeer::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonEchangesInterfacesPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonEchangesInterfacesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonEchangesInterfacesPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonEchangesInterfacesPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonEchangesInterfaces object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonEchangesInterfacesPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonEchangesInterfacesPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonEchangesInterfacesPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonEchangesInterfacesPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonEchangesInterfacesPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonCodeRetour table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonCodeRetour(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangesInterfacesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangesInterfacesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangesInterfacesPeer::CODE_RETOUR, CommonCodeRetourPeer::CODE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonEchangesInterfaces objects pre-filled with their CommonCodeRetour objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangesInterfaces objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonCodeRetour(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);
        }

        CommonEchangesInterfacesPeer::addSelectColumns($criteria);
        $startcol = CommonEchangesInterfacesPeer::NUM_HYDRATE_COLUMNS;
        CommonCodeRetourPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonEchangesInterfacesPeer::CODE_RETOUR, CommonCodeRetourPeer::CODE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangesInterfacesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangesInterfacesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonEchangesInterfacesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangesInterfacesPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonCodeRetourPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonCodeRetourPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonCodeRetourPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonCodeRetourPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonEchangesInterfaces) to $obj2 (CommonCodeRetour)
                $obj2->addCommonEchangesInterfaces($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangesInterfacesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangesInterfacesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangesInterfacesPeer::CODE_RETOUR, CommonCodeRetourPeer::CODE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonEchangesInterfaces objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangesInterfaces objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);
        }

        CommonEchangesInterfacesPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangesInterfacesPeer::NUM_HYDRATE_COLUMNS;

        CommonCodeRetourPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonCodeRetourPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangesInterfacesPeer::CODE_RETOUR, CommonCodeRetourPeer::CODE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangesInterfacesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangesInterfacesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangesInterfacesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangesInterfacesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonCodeRetour rows

            $key2 = CommonCodeRetourPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonCodeRetourPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonCodeRetourPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonCodeRetourPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonEchangesInterfaces) to the collection in $obj2 (CommonCodeRetour)
                $obj2->addCommonEchangesInterfaces($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonCodeRetour table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonCodeRetour(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangesInterfacesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangesInterfacesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonEchangesInterfacesRelatedByIdEchangesInterfaces table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonEchangesInterfacesRelatedByIdEchangesInterfaces(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangesInterfacesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangesInterfacesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangesInterfacesPeer::CODE_RETOUR, CommonCodeRetourPeer::CODE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonEchangesInterfaces objects pre-filled with all related objects except CommonCodeRetour.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangesInterfaces objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonCodeRetour(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);
        }

        CommonEchangesInterfacesPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangesInterfacesPeer::NUM_HYDRATE_COLUMNS;


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangesInterfacesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangesInterfacesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangesInterfacesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangesInterfacesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonEchangesInterfaces objects pre-filled with all related objects except CommonEchangesInterfacesRelatedByIdEchangesInterfaces.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangesInterfaces objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonEchangesInterfacesRelatedByIdEchangesInterfaces(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);
        }

        CommonEchangesInterfacesPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangesInterfacesPeer::NUM_HYDRATE_COLUMNS;

        CommonCodeRetourPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonCodeRetourPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangesInterfacesPeer::CODE_RETOUR, CommonCodeRetourPeer::CODE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangesInterfacesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangesInterfacesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangesInterfacesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangesInterfacesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonCodeRetour rows

                $key2 = CommonCodeRetourPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonCodeRetourPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonCodeRetourPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonCodeRetourPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonEchangesInterfaces) to the collection in $obj2 (CommonCodeRetour)
                $obj2->addCommonEchangesInterfaces($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonEchangesInterfacesPeer::DATABASE_NAME)->getTable(CommonEchangesInterfacesPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonEchangesInterfacesPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonEchangesInterfacesPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonEchangesInterfacesTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonEchangesInterfacesPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonEchangesInterfaces or Criteria object.
     *
     * @param      mixed $values Criteria or CommonEchangesInterfaces object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonEchangesInterfaces object
        }

        if ($criteria->containsKey(CommonEchangesInterfacesPeer::ID) && $criteria->keyContainsValue(CommonEchangesInterfacesPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonEchangesInterfacesPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonEchangesInterfaces or Criteria object.
     *
     * @param      mixed $values Criteria or CommonEchangesInterfaces object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonEchangesInterfacesPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonEchangesInterfacesPeer::ID);
            $value = $criteria->remove(CommonEchangesInterfacesPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonEchangesInterfacesPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonEchangesInterfacesPeer::TABLE_NAME);
            }

        } else { // $values is CommonEchangesInterfaces object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the echanges_interfaces table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += CommonEchangesInterfacesPeer::doOnDeleteCascade(new Criteria(CommonEchangesInterfacesPeer::DATABASE_NAME), $con);
            $affectedRows += BasePeer::doDeleteAll(CommonEchangesInterfacesPeer::TABLE_NAME, $con, CommonEchangesInterfacesPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonEchangesInterfacesPeer::clearInstancePool();
            CommonEchangesInterfacesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonEchangesInterfaces or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonEchangesInterfaces object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonEchangesInterfaces) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonEchangesInterfacesPeer::DATABASE_NAME);
            $criteria->add(CommonEchangesInterfacesPeer::ID, (array) $values, Criteria::IN);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonEchangesInterfacesPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            // cloning the Criteria in case it's modified by doSelect() or doSelectStmt()
            $c = clone $criteria;
            $affectedRows += CommonEchangesInterfacesPeer::doOnDeleteCascade($c, $con);

            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            if ($values instanceof Criteria) {
                CommonEchangesInterfacesPeer::clearInstancePool();
            } elseif ($values instanceof CommonEchangesInterfaces) { // it's a model object
                CommonEchangesInterfacesPeer::removeInstanceFromPool($values);
            } else { // it's a primary key, or an array of pks
                foreach ((array) $values as $singleval) {
                    CommonEchangesInterfacesPeer::removeInstanceFromPool($singleval);
                }
            }

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonEchangesInterfacesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * This is a method for emulating ON DELETE CASCADE for DBs that don't support this
     * feature (like MySQL or SQLite).
     *
     * This method is not very speedy because it must perform a query first to get
     * the implicated records and then perform the deletes by calling those Peer classes.
     *
     * This method should be used within a transaction if possible.
     *
     * @param      Criteria $criteria
     * @param      PropelPDO $con
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    protected static function doOnDeleteCascade(Criteria $criteria, PropelPDO $con)
    {
        // initialize var to track total num of affected rows
        $affectedRows = 0;

        // first find the objects that are implicated by the $criteria
        $objects = CommonEchangesInterfacesPeer::doSelect($criteria, $con);
        foreach ($objects as $obj) {


            // delete related CommonEchangesInterfaces objects
            $criteria = new Criteria(CommonEchangesInterfacesPeer::DATABASE_NAME);

            $criteria->add(CommonEchangesInterfacesPeer::ID_ECHANGES_INTERFACES, $obj->getId());
            $affectedRows += CommonEchangesInterfacesPeer::doDelete($criteria, $con);
        }

        return $affectedRows;
    }

    /**
     * Validates all modified columns of given CommonEchangesInterfaces object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonEchangesInterfaces $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonEchangesInterfacesPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonEchangesInterfacesPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonEchangesInterfacesPeer::DATABASE_NAME, CommonEchangesInterfacesPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonEchangesInterfaces
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonEchangesInterfacesPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonEchangesInterfacesPeer::DATABASE_NAME);
        $criteria->add(CommonEchangesInterfacesPeer::ID, $pk);

        $v = CommonEchangesInterfacesPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonEchangesInterfaces[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangesInterfacesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonEchangesInterfacesPeer::DATABASE_NAME);
            $criteria->add(CommonEchangesInterfacesPeer::ID, $pks, Criteria::IN);
            $objs = CommonEchangesInterfacesPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonEchangesInterfacesPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonEchangesInterfacesPeer::buildTableMap();

