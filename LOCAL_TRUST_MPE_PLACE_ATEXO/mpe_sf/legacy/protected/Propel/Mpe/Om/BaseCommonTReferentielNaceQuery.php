<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTReferentielNace;
use Application\Propel\Mpe\CommonTReferentielNacePeer;
use Application\Propel\Mpe\CommonTReferentielNaceQuery;

/**
 * Base class that represents a query for the 't_referentiel_nace' table.
 *
 *
 *
 * @method CommonTReferentielNaceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTReferentielNaceQuery orderByCodeNace5($order = Criteria::ASC) Order by the code_nace_5 column
 * @method CommonTReferentielNaceQuery orderByLibelleActiviteDetaillee($order = Criteria::ASC) Order by the libelle_activite_detaillee column
 * @method CommonTReferentielNaceQuery orderByCodeNace2($order = Criteria::ASC) Order by the code_nace_2 column
 * @method CommonTReferentielNaceQuery orderByLibelleActiviteGeneral($order = Criteria::ASC) Order by the libelle_activite_general column
 *
 * @method CommonTReferentielNaceQuery groupById() Group by the id column
 * @method CommonTReferentielNaceQuery groupByCodeNace5() Group by the code_nace_5 column
 * @method CommonTReferentielNaceQuery groupByLibelleActiviteDetaillee() Group by the libelle_activite_detaillee column
 * @method CommonTReferentielNaceQuery groupByCodeNace2() Group by the code_nace_2 column
 * @method CommonTReferentielNaceQuery groupByLibelleActiviteGeneral() Group by the libelle_activite_general column
 *
 * @method CommonTReferentielNaceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTReferentielNaceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTReferentielNaceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTReferentielNace findOne(PropelPDO $con = null) Return the first CommonTReferentielNace matching the query
 * @method CommonTReferentielNace findOneOrCreate(PropelPDO $con = null) Return the first CommonTReferentielNace matching the query, or a new CommonTReferentielNace object populated from the query conditions when no match is found
 *
 * @method CommonTReferentielNace findOneByCodeNace5(string $code_nace_5) Return the first CommonTReferentielNace filtered by the code_nace_5 column
 * @method CommonTReferentielNace findOneByLibelleActiviteDetaillee(string $libelle_activite_detaillee) Return the first CommonTReferentielNace filtered by the libelle_activite_detaillee column
 * @method CommonTReferentielNace findOneByCodeNace2(string $code_nace_2) Return the first CommonTReferentielNace filtered by the code_nace_2 column
 * @method CommonTReferentielNace findOneByLibelleActiviteGeneral(string $libelle_activite_general) Return the first CommonTReferentielNace filtered by the libelle_activite_general column
 *
 * @method array findById(int $id) Return CommonTReferentielNace objects filtered by the id column
 * @method array findByCodeNace5(string $code_nace_5) Return CommonTReferentielNace objects filtered by the code_nace_5 column
 * @method array findByLibelleActiviteDetaillee(string $libelle_activite_detaillee) Return CommonTReferentielNace objects filtered by the libelle_activite_detaillee column
 * @method array findByCodeNace2(string $code_nace_2) Return CommonTReferentielNace objects filtered by the code_nace_2 column
 * @method array findByLibelleActiviteGeneral(string $libelle_activite_general) Return CommonTReferentielNace objects filtered by the libelle_activite_general column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTReferentielNaceQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTReferentielNaceQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTReferentielNace', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTReferentielNaceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTReferentielNaceQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTReferentielNaceQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTReferentielNaceQuery) {
            return $criteria;
        }
        $query = new CommonTReferentielNaceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTReferentielNace|CommonTReferentielNace[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTReferentielNacePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTReferentielNacePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTReferentielNace A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTReferentielNace A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `code_nace_5`, `libelle_activite_detaillee`, `code_nace_2`, `libelle_activite_general` FROM `t_referentiel_nace` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTReferentielNace();
            $obj->hydrate($row);
            CommonTReferentielNacePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTReferentielNace|CommonTReferentielNace[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTReferentielNace[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTReferentielNaceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTReferentielNacePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTReferentielNaceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTReferentielNacePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTReferentielNaceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTReferentielNacePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTReferentielNacePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTReferentielNacePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the code_nace_5 column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeNace5('fooValue');   // WHERE code_nace_5 = 'fooValue'
     * $query->filterByCodeNace5('%fooValue%'); // WHERE code_nace_5 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codeNace5 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTReferentielNaceQuery The current query, for fluid interface
     */
    public function filterByCodeNace5($codeNace5 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codeNace5)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codeNace5)) {
                $codeNace5 = str_replace('*', '%', $codeNace5);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTReferentielNacePeer::CODE_NACE_5, $codeNace5, $comparison);
    }

    /**
     * Filter the query on the libelle_activite_detaillee column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleActiviteDetaillee('fooValue');   // WHERE libelle_activite_detaillee = 'fooValue'
     * $query->filterByLibelleActiviteDetaillee('%fooValue%'); // WHERE libelle_activite_detaillee LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleActiviteDetaillee The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTReferentielNaceQuery The current query, for fluid interface
     */
    public function filterByLibelleActiviteDetaillee($libelleActiviteDetaillee = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleActiviteDetaillee)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleActiviteDetaillee)) {
                $libelleActiviteDetaillee = str_replace('*', '%', $libelleActiviteDetaillee);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTReferentielNacePeer::LIBELLE_ACTIVITE_DETAILLEE, $libelleActiviteDetaillee, $comparison);
    }

    /**
     * Filter the query on the code_nace_2 column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeNace2('fooValue');   // WHERE code_nace_2 = 'fooValue'
     * $query->filterByCodeNace2('%fooValue%'); // WHERE code_nace_2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codeNace2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTReferentielNaceQuery The current query, for fluid interface
     */
    public function filterByCodeNace2($codeNace2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codeNace2)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codeNace2)) {
                $codeNace2 = str_replace('*', '%', $codeNace2);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTReferentielNacePeer::CODE_NACE_2, $codeNace2, $comparison);
    }

    /**
     * Filter the query on the libelle_activite_general column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleActiviteGeneral('fooValue');   // WHERE libelle_activite_general = 'fooValue'
     * $query->filterByLibelleActiviteGeneral('%fooValue%'); // WHERE libelle_activite_general LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleActiviteGeneral The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTReferentielNaceQuery The current query, for fluid interface
     */
    public function filterByLibelleActiviteGeneral($libelleActiviteGeneral = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleActiviteGeneral)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleActiviteGeneral)) {
                $libelleActiviteGeneral = str_replace('*', '%', $libelleActiviteGeneral);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTReferentielNacePeer::LIBELLE_ACTIVITE_GENERAL, $libelleActiviteGeneral, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTReferentielNace $commonTReferentielNace Object to remove from the list of results
     *
     * @return CommonTReferentielNaceQuery The current query, for fluid interface
     */
    public function prune($commonTReferentielNace = null)
    {
        if ($commonTReferentielNace) {
            $this->addUsingAlias(CommonTReferentielNacePeer::ID, $commonTReferentielNace->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
