<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonHabilitationAgentWs;
use Application\Propel\Mpe\CommonHabilitationAgentWsPeer;
use Application\Propel\Mpe\CommonHabilitationAgentWsQuery;
use Application\Propel\Mpe\CommonWebService;

/**
 * Base class that represents a query for the 'habilitation_agent_ws' table.
 *
 *
 *
 * @method CommonHabilitationAgentWsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonHabilitationAgentWsQuery orderByAgentId($order = Criteria::ASC) Order by the agent_id column
 * @method CommonHabilitationAgentWsQuery orderByWebServiceId($order = Criteria::ASC) Order by the web_service_id column
 *
 * @method CommonHabilitationAgentWsQuery groupById() Group by the id column
 * @method CommonHabilitationAgentWsQuery groupByAgentId() Group by the agent_id column
 * @method CommonHabilitationAgentWsQuery groupByWebServiceId() Group by the web_service_id column
 *
 * @method CommonHabilitationAgentWsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonHabilitationAgentWsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonHabilitationAgentWsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonHabilitationAgentWsQuery leftJoinCommonAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAgent relation
 * @method CommonHabilitationAgentWsQuery rightJoinCommonAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAgent relation
 * @method CommonHabilitationAgentWsQuery innerJoinCommonAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAgent relation
 *
 * @method CommonHabilitationAgentWsQuery leftJoinCommonWebService($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonWebService relation
 * @method CommonHabilitationAgentWsQuery rightJoinCommonWebService($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonWebService relation
 * @method CommonHabilitationAgentWsQuery innerJoinCommonWebService($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonWebService relation
 *
 * @method CommonHabilitationAgentWs findOne(PropelPDO $con = null) Return the first CommonHabilitationAgentWs matching the query
 * @method CommonHabilitationAgentWs findOneOrCreate(PropelPDO $con = null) Return the first CommonHabilitationAgentWs matching the query, or a new CommonHabilitationAgentWs object populated from the query conditions when no match is found
 *
 * @method CommonHabilitationAgentWs findOneByAgentId(int $agent_id) Return the first CommonHabilitationAgentWs filtered by the agent_id column
 * @method CommonHabilitationAgentWs findOneByWebServiceId(int $web_service_id) Return the first CommonHabilitationAgentWs filtered by the web_service_id column
 *
 * @method array findById(int $id) Return CommonHabilitationAgentWs objects filtered by the id column
 * @method array findByAgentId(int $agent_id) Return CommonHabilitationAgentWs objects filtered by the agent_id column
 * @method array findByWebServiceId(int $web_service_id) Return CommonHabilitationAgentWs objects filtered by the web_service_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonHabilitationAgentWsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonHabilitationAgentWsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonHabilitationAgentWs', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonHabilitationAgentWsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonHabilitationAgentWsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonHabilitationAgentWsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonHabilitationAgentWsQuery) {
            return $criteria;
        }
        $query = new CommonHabilitationAgentWsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonHabilitationAgentWs|CommonHabilitationAgentWs[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonHabilitationAgentWsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonHabilitationAgentWsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonHabilitationAgentWs A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonHabilitationAgentWs A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `agent_id`, `web_service_id` FROM `habilitation_agent_ws` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonHabilitationAgentWs();
            $obj->hydrate($row);
            CommonHabilitationAgentWsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonHabilitationAgentWs|CommonHabilitationAgentWs[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonHabilitationAgentWs[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonHabilitationAgentWsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonHabilitationAgentWsPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonHabilitationAgentWsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonHabilitationAgentWsPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHabilitationAgentWsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonHabilitationAgentWsPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonHabilitationAgentWsPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHabilitationAgentWsPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the agent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAgentId(1234); // WHERE agent_id = 1234
     * $query->filterByAgentId(array(12, 34)); // WHERE agent_id IN (12, 34)
     * $query->filterByAgentId(array('min' => 12)); // WHERE agent_id >= 12
     * $query->filterByAgentId(array('max' => 12)); // WHERE agent_id <= 12
     * </code>
     *
     * @see       filterByCommonAgent()
     *
     * @param     mixed $agentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHabilitationAgentWsQuery The current query, for fluid interface
     */
    public function filterByAgentId($agentId = null, $comparison = null)
    {
        if (is_array($agentId)) {
            $useMinMax = false;
            if (isset($agentId['min'])) {
                $this->addUsingAlias(CommonHabilitationAgentWsPeer::AGENT_ID, $agentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($agentId['max'])) {
                $this->addUsingAlias(CommonHabilitationAgentWsPeer::AGENT_ID, $agentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHabilitationAgentWsPeer::AGENT_ID, $agentId, $comparison);
    }

    /**
     * Filter the query on the web_service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByWebServiceId(1234); // WHERE web_service_id = 1234
     * $query->filterByWebServiceId(array(12, 34)); // WHERE web_service_id IN (12, 34)
     * $query->filterByWebServiceId(array('min' => 12)); // WHERE web_service_id >= 12
     * $query->filterByWebServiceId(array('max' => 12)); // WHERE web_service_id <= 12
     * </code>
     *
     * @see       filterByCommonWebService()
     *
     * @param     mixed $webServiceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHabilitationAgentWsQuery The current query, for fluid interface
     */
    public function filterByWebServiceId($webServiceId = null, $comparison = null)
    {
        if (is_array($webServiceId)) {
            $useMinMax = false;
            if (isset($webServiceId['min'])) {
                $this->addUsingAlias(CommonHabilitationAgentWsPeer::WEB_SERVICE_ID, $webServiceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($webServiceId['max'])) {
                $this->addUsingAlias(CommonHabilitationAgentWsPeer::WEB_SERVICE_ID, $webServiceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHabilitationAgentWsPeer::WEB_SERVICE_ID, $webServiceId, $comparison);
    }

    /**
     * Filter the query by a related CommonAgent object
     *
     * @param   CommonAgent|PropelObjectCollection $commonAgent The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonHabilitationAgentWsQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAgent($commonAgent, $comparison = null)
    {
        if ($commonAgent instanceof CommonAgent) {
            return $this
                ->addUsingAlias(CommonHabilitationAgentWsPeer::AGENT_ID, $commonAgent->getId(), $comparison);
        } elseif ($commonAgent instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonHabilitationAgentWsPeer::AGENT_ID, $commonAgent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonAgent() only accepts arguments of type CommonAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonHabilitationAgentWsQuery The current query, for fluid interface
     */
    public function joinCommonAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAgent');
        }

        return $this;
    }

    /**
     * Use the CommonAgent relation CommonAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAgent', '\Application\Propel\Mpe\CommonAgentQuery');
    }

    /**
     * Filter the query by a related CommonWebService object
     *
     * @param   CommonWebService|PropelObjectCollection $commonWebService The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonHabilitationAgentWsQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonWebService($commonWebService, $comparison = null)
    {
        if ($commonWebService instanceof CommonWebService) {
            return $this
                ->addUsingAlias(CommonHabilitationAgentWsPeer::WEB_SERVICE_ID, $commonWebService->getId(), $comparison);
        } elseif ($commonWebService instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonHabilitationAgentWsPeer::WEB_SERVICE_ID, $commonWebService->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonWebService() only accepts arguments of type CommonWebService or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonWebService relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonHabilitationAgentWsQuery The current query, for fluid interface
     */
    public function joinCommonWebService($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonWebService');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonWebService');
        }

        return $this;
    }

    /**
     * Use the CommonWebService relation CommonWebService object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonWebServiceQuery A secondary query class using the current class as primary query
     */
    public function useCommonWebServiceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonWebService($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonWebService', '\Application\Propel\Mpe\CommonWebServiceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonHabilitationAgentWs $commonHabilitationAgentWs Object to remove from the list of results
     *
     * @return CommonHabilitationAgentWsQuery The current query, for fluid interface
     */
    public function prune($commonHabilitationAgentWs = null)
    {
        if ($commonHabilitationAgentWs) {
            $this->addUsingAlias(CommonHabilitationAgentWsPeer::ID, $commonHabilitationAgentWs->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
