<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationArchiveAtlas;
use Application\Propel\Mpe\CommonConsultationArchiveAtlasPeer;
use Application\Propel\Mpe\CommonConsultationArchiveAtlasQuery;

/**
 * Base class that represents a query for the 'consultation_archive_atlas' table.
 *
 *
 *
 * @method CommonConsultationArchiveAtlasQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonConsultationArchiveAtlasQuery orderByDocId($order = Criteria::ASC) Order by the doc_id column
 * @method CommonConsultationArchiveAtlasQuery orderByNumeroBloc($order = Criteria::ASC) Order by the numero_bloc column
 * @method CommonConsultationArchiveAtlasQuery orderByCompId($order = Criteria::ASC) Order by the comp_id column
 * @method CommonConsultationArchiveAtlasQuery orderByConsultationRef($order = Criteria::ASC) Order by the consultation_ref column
 * @method CommonConsultationArchiveAtlasQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonConsultationArchiveAtlasQuery orderByTaille($order = Criteria::ASC) Order by the taille column
 * @method CommonConsultationArchiveAtlasQuery orderByDateEnvoi($order = Criteria::ASC) Order by the date_envoi column
 *
 * @method CommonConsultationArchiveAtlasQuery groupById() Group by the id column
 * @method CommonConsultationArchiveAtlasQuery groupByDocId() Group by the doc_id column
 * @method CommonConsultationArchiveAtlasQuery groupByNumeroBloc() Group by the numero_bloc column
 * @method CommonConsultationArchiveAtlasQuery groupByCompId() Group by the comp_id column
 * @method CommonConsultationArchiveAtlasQuery groupByConsultationRef() Group by the consultation_ref column
 * @method CommonConsultationArchiveAtlasQuery groupByOrganisme() Group by the organisme column
 * @method CommonConsultationArchiveAtlasQuery groupByTaille() Group by the taille column
 * @method CommonConsultationArchiveAtlasQuery groupByDateEnvoi() Group by the date_envoi column
 *
 * @method CommonConsultationArchiveAtlasQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonConsultationArchiveAtlasQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonConsultationArchiveAtlasQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonConsultationArchiveAtlas findOne(PropelPDO $con = null) Return the first CommonConsultationArchiveAtlas matching the query
 * @method CommonConsultationArchiveAtlas findOneOrCreate(PropelPDO $con = null) Return the first CommonConsultationArchiveAtlas matching the query, or a new CommonConsultationArchiveAtlas object populated from the query conditions when no match is found
 *
 * @method CommonConsultationArchiveAtlas findOneByDocId(string $doc_id) Return the first CommonConsultationArchiveAtlas filtered by the doc_id column
 * @method CommonConsultationArchiveAtlas findOneByNumeroBloc(int $numero_bloc) Return the first CommonConsultationArchiveAtlas filtered by the numero_bloc column
 * @method CommonConsultationArchiveAtlas findOneByCompId(string $comp_id) Return the first CommonConsultationArchiveAtlas filtered by the comp_id column
 * @method CommonConsultationArchiveAtlas findOneByConsultationRef(int $consultation_ref) Return the first CommonConsultationArchiveAtlas filtered by the consultation_ref column
 * @method CommonConsultationArchiveAtlas findOneByOrganisme(string $organisme) Return the first CommonConsultationArchiveAtlas filtered by the organisme column
 * @method CommonConsultationArchiveAtlas findOneByTaille(int $taille) Return the first CommonConsultationArchiveAtlas filtered by the taille column
 * @method CommonConsultationArchiveAtlas findOneByDateEnvoi(string $date_envoi) Return the first CommonConsultationArchiveAtlas filtered by the date_envoi column
 *
 * @method array findById(int $id) Return CommonConsultationArchiveAtlas objects filtered by the id column
 * @method array findByDocId(string $doc_id) Return CommonConsultationArchiveAtlas objects filtered by the doc_id column
 * @method array findByNumeroBloc(int $numero_bloc) Return CommonConsultationArchiveAtlas objects filtered by the numero_bloc column
 * @method array findByCompId(string $comp_id) Return CommonConsultationArchiveAtlas objects filtered by the comp_id column
 * @method array findByConsultationRef(int $consultation_ref) Return CommonConsultationArchiveAtlas objects filtered by the consultation_ref column
 * @method array findByOrganisme(string $organisme) Return CommonConsultationArchiveAtlas objects filtered by the organisme column
 * @method array findByTaille(int $taille) Return CommonConsultationArchiveAtlas objects filtered by the taille column
 * @method array findByDateEnvoi(string $date_envoi) Return CommonConsultationArchiveAtlas objects filtered by the date_envoi column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationArchiveAtlasQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonConsultationArchiveAtlasQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonConsultationArchiveAtlas', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonConsultationArchiveAtlasQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonConsultationArchiveAtlasQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonConsultationArchiveAtlasQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonConsultationArchiveAtlasQuery) {
            return $criteria;
        }
        $query = new CommonConsultationArchiveAtlasQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonConsultationArchiveAtlas|CommonConsultationArchiveAtlas[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonConsultationArchiveAtlasPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationArchiveAtlasPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationArchiveAtlas A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationArchiveAtlas A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `doc_id`, `numero_bloc`, `comp_id`, `consultation_ref`, `organisme`, `taille`, `date_envoi` FROM `consultation_archive_atlas` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonConsultationArchiveAtlas();
            $obj->hydrate($row);
            CommonConsultationArchiveAtlasPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonConsultationArchiveAtlas|CommonConsultationArchiveAtlas[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonConsultationArchiveAtlas[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonConsultationArchiveAtlasQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonConsultationArchiveAtlasQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveAtlasQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the doc_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDocId('fooValue');   // WHERE doc_id = 'fooValue'
     * $query->filterByDocId('%fooValue%'); // WHERE doc_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $docId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveAtlasQuery The current query, for fluid interface
     */
    public function filterByDocId($docId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($docId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $docId)) {
                $docId = str_replace('*', '%', $docId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::DOC_ID, $docId, $comparison);
    }

    /**
     * Filter the query on the numero_bloc column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroBloc(1234); // WHERE numero_bloc = 1234
     * $query->filterByNumeroBloc(array(12, 34)); // WHERE numero_bloc IN (12, 34)
     * $query->filterByNumeroBloc(array('min' => 12)); // WHERE numero_bloc >= 12
     * $query->filterByNumeroBloc(array('max' => 12)); // WHERE numero_bloc <= 12
     * </code>
     *
     * @param     mixed $numeroBloc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveAtlasQuery The current query, for fluid interface
     */
    public function filterByNumeroBloc($numeroBloc = null, $comparison = null)
    {
        if (is_array($numeroBloc)) {
            $useMinMax = false;
            if (isset($numeroBloc['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::NUMERO_BLOC, $numeroBloc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numeroBloc['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::NUMERO_BLOC, $numeroBloc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::NUMERO_BLOC, $numeroBloc, $comparison);
    }

    /**
     * Filter the query on the comp_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompId('fooValue');   // WHERE comp_id = 'fooValue'
     * $query->filterByCompId('%fooValue%'); // WHERE comp_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $compId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveAtlasQuery The current query, for fluid interface
     */
    public function filterByCompId($compId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($compId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $compId)) {
                $compId = str_replace('*', '%', $compId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::COMP_ID, $compId, $comparison);
    }

    /**
     * Filter the query on the consultation_ref column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationRef(1234); // WHERE consultation_ref = 1234
     * $query->filterByConsultationRef(array(12, 34)); // WHERE consultation_ref IN (12, 34)
     * $query->filterByConsultationRef(array('min' => 12)); // WHERE consultation_ref >= 12
     * $query->filterByConsultationRef(array('max' => 12)); // WHERE consultation_ref <= 12
     * </code>
     *
     * @param     mixed $consultationRef The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveAtlasQuery The current query, for fluid interface
     */
    public function filterByConsultationRef($consultationRef = null, $comparison = null)
    {
        if (is_array($consultationRef)) {
            $useMinMax = false;
            if (isset($consultationRef['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::CONSULTATION_REF, $consultationRef['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationRef['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::CONSULTATION_REF, $consultationRef['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::CONSULTATION_REF, $consultationRef, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveAtlasQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the taille column
     *
     * Example usage:
     * <code>
     * $query->filterByTaille(1234); // WHERE taille = 1234
     * $query->filterByTaille(array(12, 34)); // WHERE taille IN (12, 34)
     * $query->filterByTaille(array('min' => 12)); // WHERE taille >= 12
     * $query->filterByTaille(array('max' => 12)); // WHERE taille <= 12
     * </code>
     *
     * @param     mixed $taille The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveAtlasQuery The current query, for fluid interface
     */
    public function filterByTaille($taille = null, $comparison = null)
    {
        if (is_array($taille)) {
            $useMinMax = false;
            if (isset($taille['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::TAILLE, $taille['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($taille['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::TAILLE, $taille['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::TAILLE, $taille, $comparison);
    }

    /**
     * Filter the query on the date_envoi column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnvoi('2011-03-14'); // WHERE date_envoi = '2011-03-14'
     * $query->filterByDateEnvoi('now'); // WHERE date_envoi = '2011-03-14'
     * $query->filterByDateEnvoi(array('max' => 'yesterday')); // WHERE date_envoi > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnvoi The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveAtlasQuery The current query, for fluid interface
     */
    public function filterByDateEnvoi($dateEnvoi = null, $comparison = null)
    {
        if (is_array($dateEnvoi)) {
            $useMinMax = false;
            if (isset($dateEnvoi['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::DATE_ENVOI, $dateEnvoi['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnvoi['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::DATE_ENVOI, $dateEnvoi['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::DATE_ENVOI, $dateEnvoi, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonConsultationArchiveAtlas $commonConsultationArchiveAtlas Object to remove from the list of results
     *
     * @return CommonConsultationArchiveAtlasQuery The current query, for fluid interface
     */
    public function prune($commonConsultationArchiveAtlas = null)
    {
        if ($commonConsultationArchiveAtlas) {
            $this->addUsingAlias(CommonConsultationArchiveAtlasPeer::ID, $commonConsultationArchiveAtlas->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
