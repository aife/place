<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTAnnonceConsultationQuery;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationQuery;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Propel\Mpe\CommonTSupportPublicationQuery;

/**
 * Base class that represents a row from the 't_support_annonce_consultation' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTSupportAnnonceConsultation extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTSupportAnnonceConsultationPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTSupportAnnonceConsultationPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the id_support field.
     * @var        int
     */
    protected $id_support;

    /**
     * The value for the id_annonce_cons field.
     * @var        int
     */
    protected $id_annonce_cons;

    /**
     * The value for the prenom_nom_agent_createur field.
     * @var        string
     */
    protected $prenom_nom_agent_createur;

    /**
     * The value for the id_agent field.
     * @var        int
     */
    protected $id_agent;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the statut field.
     * @var        string
     */
    protected $statut;

    /**
     * The value for the date_statut field.
     * @var        string
     */
    protected $date_statut;

    /**
     * The value for the numero_avis field.
     * @var        string
     */
    protected $numero_avis;

    /**
     * The value for the message_statut field.
     * @var        string
     */
    protected $message_statut;

    /**
     * The value for the lien_publication field.
     * @var        string
     */
    protected $lien_publication;

    /**
     * The value for the date_envoi_support field.
     * @var        string
     */
    protected $date_envoi_support;

    /**
     * The value for the date_publication_support field.
     * @var        string
     */
    protected $date_publication_support;

    /**
     * The value for the numero_avis_parent field.
     * @var        string
     */
    protected $numero_avis_parent;

    /**
     * The value for the id_offre field.
     * @var        int
     */
    protected $id_offre;

    /**
     * The value for the message_acheteur field.
     * @var        string
     */
    protected $message_acheteur;

    /**
     * The value for the departements_parution_annonce field.
     * @var        string
     */
    protected $departements_parution_annonce;

    /**
     * @var        CommonTAnnonceConsultation
     */
    protected $aCommonTAnnonceConsultation;

    /**
     * @var        CommonTSupportPublication
     */
    protected $aCommonTSupportPublication;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [id_support] column value.
     *
     * @return int
     */
    public function getIdSupport()
    {

        return $this->id_support;
    }

    /**
     * Get the [id_annonce_cons] column value.
     *
     * @return int
     */
    public function getIdAnnonceCons()
    {

        return $this->id_annonce_cons;
    }

    /**
     * Get the [prenom_nom_agent_createur] column value.
     *
     * @return string
     */
    public function getPrenomNomAgentCreateur()
    {

        return $this->prenom_nom_agent_createur;
    }

    /**
     * Get the [id_agent] column value.
     *
     * @return int
     */
    public function getIdAgent()
    {

        return $this->id_agent;
    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [statut] column value.
     *
     * @return string
     */
    public function getStatut()
    {

        return $this->statut;
    }

    /**
     * Get the [optionally formatted] temporal [date_statut] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateStatut($format = 'Y-m-d H:i:s')
    {
        if ($this->date_statut === null) {
            return null;
        }

        if ($this->date_statut === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_statut);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_statut, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [numero_avis] column value.
     *
     * @return string
     */
    public function getNumeroAvis()
    {

        return $this->numero_avis;
    }

    /**
     * Get the [message_statut] column value.
     *
     * @return string
     */
    public function getMessageStatut()
    {

        return $this->message_statut;
    }

    /**
     * Get the [lien_publication] column value.
     *
     * @return string
     */
    public function getLienPublication()
    {

        return $this->lien_publication;
    }

    /**
     * Get the [optionally formatted] temporal [date_envoi_support] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateEnvoiSupport($format = 'Y-m-d H:i:s')
    {
        if ($this->date_envoi_support === null) {
            return null;
        }

        if ($this->date_envoi_support === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_envoi_support);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_envoi_support, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_publication_support] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatePublicationSupport($format = 'Y-m-d H:i:s')
    {
        if ($this->date_publication_support === null) {
            return null;
        }

        if ($this->date_publication_support === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_publication_support);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_publication_support, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [numero_avis_parent] column value.
     *
     * @return string
     */
    public function getNumeroAvisParent()
    {

        return $this->numero_avis_parent;
    }

    /**
     * Get the [id_offre] column value.
     *
     * @return int
     */
    public function getIdOffre()
    {

        return $this->id_offre;
    }

    /**
     * Get the [message_acheteur] column value.
     *
     * @return string
     */
    public function getMessageAcheteur()
    {

        return $this->message_acheteur;
    }

    /**
     * Get the [departements_parution_annonce] column value.
     *
     * @return string
     */
    public function getDepartementsParutionAnnonce()
    {

        return $this->departements_parution_annonce;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [id_support] column.
     *
     * @param int $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setIdSupport($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_support !== $v) {
            $this->id_support = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::ID_SUPPORT;
        }

        if ($this->aCommonTSupportPublication !== null && $this->aCommonTSupportPublication->getId() !== $v) {
            $this->aCommonTSupportPublication = null;
        }


        return $this;
    } // setIdSupport()

    /**
     * Set the value of [id_annonce_cons] column.
     *
     * @param int $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setIdAnnonceCons($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_annonce_cons !== $v) {
            $this->id_annonce_cons = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS;
        }

        if ($this->aCommonTAnnonceConsultation !== null && $this->aCommonTAnnonceConsultation->getId() !== $v) {
            $this->aCommonTAnnonceConsultation = null;
        }


        return $this;
    } // setIdAnnonceCons()

    /**
     * Set the value of [prenom_nom_agent_createur] column.
     *
     * @param string $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setPrenomNomAgentCreateur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prenom_nom_agent_createur !== $v) {
            $this->prenom_nom_agent_createur = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::PRENOM_NOM_AGENT_CREATEUR;
        }


        return $this;
    } // setPrenomNomAgentCreateur()

    /**
     * Set the value of [id_agent] column.
     *
     * @param int $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setIdAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_agent !== $v) {
            $this->id_agent = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::ID_AGENT;
        }


        return $this;
    } // setIdAgent()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Set the value of [statut] column.
     *
     * @param string $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setStatut($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->statut !== $v) {
            $this->statut = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::STATUT;
        }


        return $this;
    } // setStatut()

    /**
     * Sets the value of [date_statut] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setDateStatut($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_statut !== null || $dt !== null) {
            $currentDateAsString = ($this->date_statut !== null && $tmpDt = new DateTime($this->date_statut)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_statut = $newDateAsString;
                $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::DATE_STATUT;
            }
        } // if either are not null


        return $this;
    } // setDateStatut()

    /**
     * Set the value of [numero_avis] column.
     *
     * @param string $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setNumeroAvis($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->numero_avis !== $v) {
            $this->numero_avis = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS;
        }


        return $this;
    } // setNumeroAvis()

    /**
     * Set the value of [message_statut] column.
     *
     * @param string $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setMessageStatut($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->message_statut !== $v) {
            $this->message_statut = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::MESSAGE_STATUT;
        }


        return $this;
    } // setMessageStatut()

    /**
     * Set the value of [lien_publication] column.
     *
     * @param string $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setLienPublication($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->lien_publication !== $v) {
            $this->lien_publication = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::LIEN_PUBLICATION;
        }


        return $this;
    } // setLienPublication()

    /**
     * Sets the value of [date_envoi_support] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setDateEnvoiSupport($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_envoi_support !== null || $dt !== null) {
            $currentDateAsString = ($this->date_envoi_support !== null && $tmpDt = new DateTime($this->date_envoi_support)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_envoi_support = $newDateAsString;
                $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::DATE_ENVOI_SUPPORT;
            }
        } // if either are not null


        return $this;
    } // setDateEnvoiSupport()

    /**
     * Sets the value of [date_publication_support] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setDatePublicationSupport($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_publication_support !== null || $dt !== null) {
            $currentDateAsString = ($this->date_publication_support !== null && $tmpDt = new DateTime($this->date_publication_support)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_publication_support = $newDateAsString;
                $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::DATE_PUBLICATION_SUPPORT;
            }
        } // if either are not null


        return $this;
    } // setDatePublicationSupport()

    /**
     * Set the value of [numero_avis_parent] column.
     *
     * @param string $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setNumeroAvisParent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->numero_avis_parent !== $v) {
            $this->numero_avis_parent = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS_PARENT;
        }


        return $this;
    } // setNumeroAvisParent()

    /**
     * Set the value of [id_offre] column.
     *
     * @param int $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setIdOffre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_offre !== $v) {
            $this->id_offre = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::ID_OFFRE;
        }


        return $this;
    } // setIdOffre()

    /**
     * Set the value of [message_acheteur] column.
     *
     * @param string $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setMessageAcheteur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->message_acheteur !== $v) {
            $this->message_acheteur = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::MESSAGE_ACHETEUR;
        }


        return $this;
    } // setMessageAcheteur()

    /**
     * Set the value of [departements_parution_annonce] column.
     *
     * @param string $v new value
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     */
    public function setDepartementsParutionAnnonce($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->departements_parution_annonce !== $v) {
            $this->departements_parution_annonce = $v;
            $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::DEPARTEMENTS_PARUTION_ANNONCE;
        }


        return $this;
    } // setDepartementsParutionAnnonce()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_support = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->id_annonce_cons = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->prenom_nom_agent_createur = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->id_agent = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->date_creation = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->statut = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->date_statut = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->numero_avis = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->message_statut = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->lien_publication = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->date_envoi_support = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->date_publication_support = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->numero_avis_parent = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->id_offre = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
            $this->message_acheteur = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->departements_parution_annonce = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 17; // 17 = CommonTSupportAnnonceConsultationPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTSupportAnnonceConsultation object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTSupportPublication !== null && $this->id_support !== $this->aCommonTSupportPublication->getId()) {
            $this->aCommonTSupportPublication = null;
        }
        if ($this->aCommonTAnnonceConsultation !== null && $this->id_annonce_cons !== $this->aCommonTAnnonceConsultation->getId()) {
            $this->aCommonTAnnonceConsultation = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTSupportAnnonceConsultationPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonTAnnonceConsultation = null;
            $this->aCommonTSupportPublication = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTSupportAnnonceConsultationQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTSupportAnnonceConsultationPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTAnnonceConsultation !== null) {
                if ($this->aCommonTAnnonceConsultation->isModified() || $this->aCommonTAnnonceConsultation->isNew()) {
                    $affectedRows += $this->aCommonTAnnonceConsultation->save($con);
                }
                $this->setCommonTAnnonceConsultation($this->aCommonTAnnonceConsultation);
            }

            if ($this->aCommonTSupportPublication !== null) {
                if ($this->aCommonTSupportPublication->isModified() || $this->aCommonTSupportPublication->isNew()) {
                    $affectedRows += $this->aCommonTSupportPublication->save($con);
                }
                $this->setCommonTSupportPublication($this->aCommonTSupportPublication);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTSupportAnnonceConsultationPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTSupportAnnonceConsultationPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT)) {
            $modifiedColumns[':p' . $index++]  = '`id_support`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS)) {
            $modifiedColumns[':p' . $index++]  = '`id_annonce_cons`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::PRENOM_NOM_AGENT_CREATEUR)) {
            $modifiedColumns[':p' . $index++]  = '`prenom_nom_agent_createur`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::ID_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_agent`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::STATUT)) {
            $modifiedColumns[':p' . $index++]  = '`statut`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::DATE_STATUT)) {
            $modifiedColumns[':p' . $index++]  = '`date_statut`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS)) {
            $modifiedColumns[':p' . $index++]  = '`numero_avis`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::MESSAGE_STATUT)) {
            $modifiedColumns[':p' . $index++]  = '`message_statut`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::LIEN_PUBLICATION)) {
            $modifiedColumns[':p' . $index++]  = '`lien_publication`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::DATE_ENVOI_SUPPORT)) {
            $modifiedColumns[':p' . $index++]  = '`date_envoi_support`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::DATE_PUBLICATION_SUPPORT)) {
            $modifiedColumns[':p' . $index++]  = '`date_publication_support`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS_PARENT)) {
            $modifiedColumns[':p' . $index++]  = '`numero_avis_parent`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::ID_OFFRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_offre`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::MESSAGE_ACHETEUR)) {
            $modifiedColumns[':p' . $index++]  = '`message_acheteur`';
        }
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::DEPARTEMENTS_PARUTION_ANNONCE)) {
            $modifiedColumns[':p' . $index++]  = '`departements_parution_annonce`';
        }

        $sql = sprintf(
            'INSERT INTO `t_support_annonce_consultation` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`id_support`':
                        $stmt->bindValue($identifier, $this->id_support, PDO::PARAM_INT);
                        break;
                    case '`id_annonce_cons`':
                        $stmt->bindValue($identifier, $this->id_annonce_cons, PDO::PARAM_INT);
                        break;
                    case '`prenom_nom_agent_createur`':
                        $stmt->bindValue($identifier, $this->prenom_nom_agent_createur, PDO::PARAM_STR);
                        break;
                    case '`id_agent`':
                        $stmt->bindValue($identifier, $this->id_agent, PDO::PARAM_INT);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`statut`':
                        $stmt->bindValue($identifier, $this->statut, PDO::PARAM_STR);
                        break;
                    case '`date_statut`':
                        $stmt->bindValue($identifier, $this->date_statut, PDO::PARAM_STR);
                        break;
                    case '`numero_avis`':
                        $stmt->bindValue($identifier, $this->numero_avis, PDO::PARAM_STR);
                        break;
                    case '`message_statut`':
                        $stmt->bindValue($identifier, $this->message_statut, PDO::PARAM_STR);
                        break;
                    case '`lien_publication`':
                        $stmt->bindValue($identifier, $this->lien_publication, PDO::PARAM_STR);
                        break;
                    case '`date_envoi_support`':
                        $stmt->bindValue($identifier, $this->date_envoi_support, PDO::PARAM_STR);
                        break;
                    case '`date_publication_support`':
                        $stmt->bindValue($identifier, $this->date_publication_support, PDO::PARAM_STR);
                        break;
                    case '`numero_avis_parent`':
                        $stmt->bindValue($identifier, $this->numero_avis_parent, PDO::PARAM_STR);
                        break;
                    case '`id_offre`':
                        $stmt->bindValue($identifier, $this->id_offre, PDO::PARAM_INT);
                        break;
                    case '`message_acheteur`':
                        $stmt->bindValue($identifier, $this->message_acheteur, PDO::PARAM_STR);
                        break;
                    case '`departements_parution_annonce`':
                        $stmt->bindValue($identifier, $this->departements_parution_annonce, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTAnnonceConsultation !== null) {
                if (!$this->aCommonTAnnonceConsultation->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTAnnonceConsultation->getValidationFailures());
                }
            }

            if ($this->aCommonTSupportPublication !== null) {
                if (!$this->aCommonTSupportPublication->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTSupportPublication->getValidationFailures());
                }
            }


            if (($retval = CommonTSupportAnnonceConsultationPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTSupportAnnonceConsultationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getIdSupport();
                break;
            case 2:
                return $this->getIdAnnonceCons();
                break;
            case 3:
                return $this->getPrenomNomAgentCreateur();
                break;
            case 4:
                return $this->getIdAgent();
                break;
            case 5:
                return $this->getDateCreation();
                break;
            case 6:
                return $this->getStatut();
                break;
            case 7:
                return $this->getDateStatut();
                break;
            case 8:
                return $this->getNumeroAvis();
                break;
            case 9:
                return $this->getMessageStatut();
                break;
            case 10:
                return $this->getLienPublication();
                break;
            case 11:
                return $this->getDateEnvoiSupport();
                break;
            case 12:
                return $this->getDatePublicationSupport();
                break;
            case 13:
                return $this->getNumeroAvisParent();
                break;
            case 14:
                return $this->getIdOffre();
                break;
            case 15:
                return $this->getMessageAcheteur();
                break;
            case 16:
                return $this->getDepartementsParutionAnnonce();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTSupportAnnonceConsultation'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTSupportAnnonceConsultation'][$this->getPrimaryKey()] = true;
        $keys = CommonTSupportAnnonceConsultationPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getIdSupport(),
            $keys[2] => $this->getIdAnnonceCons(),
            $keys[3] => $this->getPrenomNomAgentCreateur(),
            $keys[4] => $this->getIdAgent(),
            $keys[5] => $this->getDateCreation(),
            $keys[6] => $this->getStatut(),
            $keys[7] => $this->getDateStatut(),
            $keys[8] => $this->getNumeroAvis(),
            $keys[9] => $this->getMessageStatut(),
            $keys[10] => $this->getLienPublication(),
            $keys[11] => $this->getDateEnvoiSupport(),
            $keys[12] => $this->getDatePublicationSupport(),
            $keys[13] => $this->getNumeroAvisParent(),
            $keys[14] => $this->getIdOffre(),
            $keys[15] => $this->getMessageAcheteur(),
            $keys[16] => $this->getDepartementsParutionAnnonce(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonTAnnonceConsultation) {
                $result['CommonTAnnonceConsultation'] = $this->aCommonTAnnonceConsultation->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTSupportPublication) {
                $result['CommonTSupportPublication'] = $this->aCommonTSupportPublication->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTSupportAnnonceConsultationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setIdSupport($value);
                break;
            case 2:
                $this->setIdAnnonceCons($value);
                break;
            case 3:
                $this->setPrenomNomAgentCreateur($value);
                break;
            case 4:
                $this->setIdAgent($value);
                break;
            case 5:
                $this->setDateCreation($value);
                break;
            case 6:
                $this->setStatut($value);
                break;
            case 7:
                $this->setDateStatut($value);
                break;
            case 8:
                $this->setNumeroAvis($value);
                break;
            case 9:
                $this->setMessageStatut($value);
                break;
            case 10:
                $this->setLienPublication($value);
                break;
            case 11:
                $this->setDateEnvoiSupport($value);
                break;
            case 12:
                $this->setDatePublicationSupport($value);
                break;
            case 13:
                $this->setNumeroAvisParent($value);
                break;
            case 14:
                $this->setIdOffre($value);
                break;
            case 15:
                $this->setMessageAcheteur($value);
                break;
            case 16:
                $this->setDepartementsParutionAnnonce($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTSupportAnnonceConsultationPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdSupport($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdAnnonceCons($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setPrenomNomAgentCreateur($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIdAgent($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDateCreation($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setStatut($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setDateStatut($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setNumeroAvis($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setMessageStatut($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setLienPublication($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setDateEnvoiSupport($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setDatePublicationSupport($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setNumeroAvisParent($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setIdOffre($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setMessageAcheteur($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setDepartementsParutionAnnonce($arr[$keys[16]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::ID)) $criteria->add(CommonTSupportAnnonceConsultationPeer::ID, $this->id);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT)) $criteria->add(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, $this->id_support);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS)) $criteria->add(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, $this->id_annonce_cons);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::PRENOM_NOM_AGENT_CREATEUR)) $criteria->add(CommonTSupportAnnonceConsultationPeer::PRENOM_NOM_AGENT_CREATEUR, $this->prenom_nom_agent_createur);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::ID_AGENT)) $criteria->add(CommonTSupportAnnonceConsultationPeer::ID_AGENT, $this->id_agent);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::DATE_CREATION)) $criteria->add(CommonTSupportAnnonceConsultationPeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::STATUT)) $criteria->add(CommonTSupportAnnonceConsultationPeer::STATUT, $this->statut);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::DATE_STATUT)) $criteria->add(CommonTSupportAnnonceConsultationPeer::DATE_STATUT, $this->date_statut);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS)) $criteria->add(CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS, $this->numero_avis);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::MESSAGE_STATUT)) $criteria->add(CommonTSupportAnnonceConsultationPeer::MESSAGE_STATUT, $this->message_statut);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::LIEN_PUBLICATION)) $criteria->add(CommonTSupportAnnonceConsultationPeer::LIEN_PUBLICATION, $this->lien_publication);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::DATE_ENVOI_SUPPORT)) $criteria->add(CommonTSupportAnnonceConsultationPeer::DATE_ENVOI_SUPPORT, $this->date_envoi_support);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::DATE_PUBLICATION_SUPPORT)) $criteria->add(CommonTSupportAnnonceConsultationPeer::DATE_PUBLICATION_SUPPORT, $this->date_publication_support);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS_PARENT)) $criteria->add(CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS_PARENT, $this->numero_avis_parent);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::ID_OFFRE)) $criteria->add(CommonTSupportAnnonceConsultationPeer::ID_OFFRE, $this->id_offre);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::MESSAGE_ACHETEUR)) $criteria->add(CommonTSupportAnnonceConsultationPeer::MESSAGE_ACHETEUR, $this->message_acheteur);
        if ($this->isColumnModified(CommonTSupportAnnonceConsultationPeer::DEPARTEMENTS_PARUTION_ANNONCE)) $criteria->add(CommonTSupportAnnonceConsultationPeer::DEPARTEMENTS_PARUTION_ANNONCE, $this->departements_parution_annonce);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME);
        $criteria->add(CommonTSupportAnnonceConsultationPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTSupportAnnonceConsultation (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdSupport($this->getIdSupport());
        $copyObj->setIdAnnonceCons($this->getIdAnnonceCons());
        $copyObj->setPrenomNomAgentCreateur($this->getPrenomNomAgentCreateur());
        $copyObj->setIdAgent($this->getIdAgent());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setStatut($this->getStatut());
        $copyObj->setDateStatut($this->getDateStatut());
        $copyObj->setNumeroAvis($this->getNumeroAvis());
        $copyObj->setMessageStatut($this->getMessageStatut());
        $copyObj->setLienPublication($this->getLienPublication());
        $copyObj->setDateEnvoiSupport($this->getDateEnvoiSupport());
        $copyObj->setDatePublicationSupport($this->getDatePublicationSupport());
        $copyObj->setNumeroAvisParent($this->getNumeroAvisParent());
        $copyObj->setIdOffre($this->getIdOffre());
        $copyObj->setMessageAcheteur($this->getMessageAcheteur());
        $copyObj->setDepartementsParutionAnnonce($this->getDepartementsParutionAnnonce());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTSupportAnnonceConsultation Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTSupportAnnonceConsultationPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTSupportAnnonceConsultationPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonTAnnonceConsultation object.
     *
     * @param   CommonTAnnonceConsultation $v
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTAnnonceConsultation(CommonTAnnonceConsultation $v = null)
    {
        if ($v === null) {
            $this->setIdAnnonceCons(NULL);
        } else {
            $this->setIdAnnonceCons($v->getId());
        }

        $this->aCommonTAnnonceConsultation = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTAnnonceConsultation object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTSupportAnnonceConsultation($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTAnnonceConsultation object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTAnnonceConsultation The associated CommonTAnnonceConsultation object.
     * @throws PropelException
     */
    public function getCommonTAnnonceConsultation(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTAnnonceConsultation === null && ($this->id_annonce_cons !== null) && $doQuery) {
            $this->aCommonTAnnonceConsultation = CommonTAnnonceConsultationQuery::create()->findPk($this->id_annonce_cons, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTAnnonceConsultation->addCommonTSupportAnnonceConsultations($this);
             */
        }

        return $this->aCommonTAnnonceConsultation;
    }

    /**
     * Declares an association between this object and a CommonTSupportPublication object.
     *
     * @param   CommonTSupportPublication $v
     * @return CommonTSupportAnnonceConsultation The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTSupportPublication(CommonTSupportPublication $v = null)
    {
        if ($v === null) {
            $this->setIdSupport(NULL);
        } else {
            $this->setIdSupport($v->getId());
        }

        $this->aCommonTSupportPublication = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTSupportPublication object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTSupportAnnonceConsultation($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTSupportPublication object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTSupportPublication The associated CommonTSupportPublication object.
     * @throws PropelException
     */
    public function getCommonTSupportPublication(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTSupportPublication === null && ($this->id_support !== null) && $doQuery) {
            $this->aCommonTSupportPublication = CommonTSupportPublicationQuery::create()->findPk($this->id_support, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTSupportPublication->addCommonTSupportAnnonceConsultations($this);
             */
        }

        return $this->aCommonTSupportPublication;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->id_support = null;
        $this->id_annonce_cons = null;
        $this->prenom_nom_agent_createur = null;
        $this->id_agent = null;
        $this->date_creation = null;
        $this->statut = null;
        $this->date_statut = null;
        $this->numero_avis = null;
        $this->message_statut = null;
        $this->lien_publication = null;
        $this->date_envoi_support = null;
        $this->date_publication_support = null;
        $this->numero_avis_parent = null;
        $this->id_offre = null;
        $this->message_acheteur = null;
        $this->departements_parution_annonce = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aCommonTAnnonceConsultation instanceof Persistent) {
              $this->aCommonTAnnonceConsultation->clearAllReferences($deep);
            }
            if ($this->aCommonTSupportPublication instanceof Persistent) {
              $this->aCommonTSupportPublication->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aCommonTAnnonceConsultation = null;
        $this->aCommonTSupportPublication = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTSupportAnnonceConsultationPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
