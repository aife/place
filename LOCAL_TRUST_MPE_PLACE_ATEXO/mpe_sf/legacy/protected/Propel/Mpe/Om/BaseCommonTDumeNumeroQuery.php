<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Propel\Mpe\CommonTDumeNumero;
use Application\Propel\Mpe\CommonTDumeNumeroPeer;
use Application\Propel\Mpe\CommonTDumeNumeroQuery;

/**
 * Base class that represents a query for the 't_dume_numero' table.
 *
 *
 *
 * @method CommonTDumeNumeroQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTDumeNumeroQuery orderByNumeroDumeNational($order = Criteria::ASC) Order by the numero_dume_national column
 * @method CommonTDumeNumeroQuery orderByBlobId($order = Criteria::ASC) Order by the blob_id column
 * @method CommonTDumeNumeroQuery orderByListLot($order = Criteria::ASC) Order by the list_lot column
 * @method CommonTDumeNumeroQuery orderByIdDumeContexte($order = Criteria::ASC) Order by the id_dume_contexte column
 * @method CommonTDumeNumeroQuery orderByDateRecuperationPdf($order = Criteria::ASC) Order by the date_recuperation_pdf column
 * @method CommonTDumeNumeroQuery orderByBlobIdXml($order = Criteria::ASC) Order by the blob_id_xml column
 * @method CommonTDumeNumeroQuery orderByDateRecuperationXml($order = Criteria::ASC) Order by the date_recuperation_xml column
 *
 * @method CommonTDumeNumeroQuery groupById() Group by the id column
 * @method CommonTDumeNumeroQuery groupByNumeroDumeNational() Group by the numero_dume_national column
 * @method CommonTDumeNumeroQuery groupByBlobId() Group by the blob_id column
 * @method CommonTDumeNumeroQuery groupByListLot() Group by the list_lot column
 * @method CommonTDumeNumeroQuery groupByIdDumeContexte() Group by the id_dume_contexte column
 * @method CommonTDumeNumeroQuery groupByDateRecuperationPdf() Group by the date_recuperation_pdf column
 * @method CommonTDumeNumeroQuery groupByBlobIdXml() Group by the blob_id_xml column
 * @method CommonTDumeNumeroQuery groupByDateRecuperationXml() Group by the date_recuperation_xml column
 *
 * @method CommonTDumeNumeroQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTDumeNumeroQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTDumeNumeroQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTDumeNumeroQuery leftJoinCommonTDumeContexte($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTDumeContexte relation
 * @method CommonTDumeNumeroQuery rightJoinCommonTDumeContexte($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTDumeContexte relation
 * @method CommonTDumeNumeroQuery innerJoinCommonTDumeContexte($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTDumeContexte relation
 *
 * @method CommonTDumeNumero findOne(PropelPDO $con = null) Return the first CommonTDumeNumero matching the query
 * @method CommonTDumeNumero findOneOrCreate(PropelPDO $con = null) Return the first CommonTDumeNumero matching the query, or a new CommonTDumeNumero object populated from the query conditions when no match is found
 *
 * @method CommonTDumeNumero findOneByNumeroDumeNational(string $numero_dume_national) Return the first CommonTDumeNumero filtered by the numero_dume_national column
 * @method CommonTDumeNumero findOneByBlobId(int $blob_id) Return the first CommonTDumeNumero filtered by the blob_id column
 * @method CommonTDumeNumero findOneByListLot(string $list_lot) Return the first CommonTDumeNumero filtered by the list_lot column
 * @method CommonTDumeNumero findOneByIdDumeContexte(int $id_dume_contexte) Return the first CommonTDumeNumero filtered by the id_dume_contexte column
 * @method CommonTDumeNumero findOneByDateRecuperationPdf(string $date_recuperation_pdf) Return the first CommonTDumeNumero filtered by the date_recuperation_pdf column
 * @method CommonTDumeNumero findOneByBlobIdXml(int $blob_id_xml) Return the first CommonTDumeNumero filtered by the blob_id_xml column
 * @method CommonTDumeNumero findOneByDateRecuperationXml(string $date_recuperation_xml) Return the first CommonTDumeNumero filtered by the date_recuperation_xml column
 *
 * @method array findById(int $id) Return CommonTDumeNumero objects filtered by the id column
 * @method array findByNumeroDumeNational(string $numero_dume_national) Return CommonTDumeNumero objects filtered by the numero_dume_national column
 * @method array findByBlobId(int $blob_id) Return CommonTDumeNumero objects filtered by the blob_id column
 * @method array findByListLot(string $list_lot) Return CommonTDumeNumero objects filtered by the list_lot column
 * @method array findByIdDumeContexte(int $id_dume_contexte) Return CommonTDumeNumero objects filtered by the id_dume_contexte column
 * @method array findByDateRecuperationPdf(string $date_recuperation_pdf) Return CommonTDumeNumero objects filtered by the date_recuperation_pdf column
 * @method array findByBlobIdXml(int $blob_id_xml) Return CommonTDumeNumero objects filtered by the blob_id_xml column
 * @method array findByDateRecuperationXml(string $date_recuperation_xml) Return CommonTDumeNumero objects filtered by the date_recuperation_xml column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDumeNumeroQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTDumeNumeroQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTDumeNumero', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTDumeNumeroQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTDumeNumeroQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTDumeNumeroQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTDumeNumeroQuery) {
            return $criteria;
        }
        $query = new CommonTDumeNumeroQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTDumeNumero|CommonTDumeNumero[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTDumeNumeroPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTDumeNumeroPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDumeNumero A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDumeNumero A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `numero_dume_national`, `blob_id`, `list_lot`, `id_dume_contexte`, `date_recuperation_pdf`, `blob_id_xml`, `date_recuperation_xml` FROM `t_dume_numero` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTDumeNumero();
            $obj->hydrate($row);
            CommonTDumeNumeroPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTDumeNumero|CommonTDumeNumero[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTDumeNumero[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTDumeNumeroPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTDumeNumeroPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeNumeroPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the numero_dume_national column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroDumeNational('fooValue');   // WHERE numero_dume_national = 'fooValue'
     * $query->filterByNumeroDumeNational('%fooValue%'); // WHERE numero_dume_national LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numeroDumeNational The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function filterByNumeroDumeNational($numeroDumeNational = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numeroDumeNational)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numeroDumeNational)) {
                $numeroDumeNational = str_replace('*', '%', $numeroDumeNational);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDumeNumeroPeer::NUMERO_DUME_NATIONAL, $numeroDumeNational, $comparison);
    }

    /**
     * Filter the query on the blob_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBlobId(1234); // WHERE blob_id = 1234
     * $query->filterByBlobId(array(12, 34)); // WHERE blob_id IN (12, 34)
     * $query->filterByBlobId(array('min' => 12)); // WHERE blob_id >= 12
     * $query->filterByBlobId(array('max' => 12)); // WHERE blob_id <= 12
     * </code>
     *
     * @param     mixed $blobId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function filterByBlobId($blobId = null, $comparison = null)
    {
        if (is_array($blobId)) {
            $useMinMax = false;
            if (isset($blobId['min'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::BLOB_ID, $blobId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($blobId['max'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::BLOB_ID, $blobId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeNumeroPeer::BLOB_ID, $blobId, $comparison);
    }

    /**
     * Filter the query on the list_lot column
     *
     * Example usage:
     * <code>
     * $query->filterByListLot('fooValue');   // WHERE list_lot = 'fooValue'
     * $query->filterByListLot('%fooValue%'); // WHERE list_lot LIKE '%fooValue%'
     * </code>
     *
     * @param     string $listLot The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function filterByListLot($listLot = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($listLot)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $listLot)) {
                $listLot = str_replace('*', '%', $listLot);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDumeNumeroPeer::LIST_LOT, $listLot, $comparison);
    }

    /**
     * Filter the query on the id_dume_contexte column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDumeContexte(1234); // WHERE id_dume_contexte = 1234
     * $query->filterByIdDumeContexte(array(12, 34)); // WHERE id_dume_contexte IN (12, 34)
     * $query->filterByIdDumeContexte(array('min' => 12)); // WHERE id_dume_contexte >= 12
     * $query->filterByIdDumeContexte(array('max' => 12)); // WHERE id_dume_contexte <= 12
     * </code>
     *
     * @see       filterByCommonTDumeContexte()
     *
     * @param     mixed $idDumeContexte The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function filterByIdDumeContexte($idDumeContexte = null, $comparison = null)
    {
        if (is_array($idDumeContexte)) {
            $useMinMax = false;
            if (isset($idDumeContexte['min'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::ID_DUME_CONTEXTE, $idDumeContexte['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDumeContexte['max'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::ID_DUME_CONTEXTE, $idDumeContexte['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeNumeroPeer::ID_DUME_CONTEXTE, $idDumeContexte, $comparison);
    }

    /**
     * Filter the query on the date_recuperation_pdf column
     *
     * Example usage:
     * <code>
     * $query->filterByDateRecuperationPdf('2011-03-14'); // WHERE date_recuperation_pdf = '2011-03-14'
     * $query->filterByDateRecuperationPdf('now'); // WHERE date_recuperation_pdf = '2011-03-14'
     * $query->filterByDateRecuperationPdf(array('max' => 'yesterday')); // WHERE date_recuperation_pdf > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateRecuperationPdf The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function filterByDateRecuperationPdf($dateRecuperationPdf = null, $comparison = null)
    {
        if (is_array($dateRecuperationPdf)) {
            $useMinMax = false;
            if (isset($dateRecuperationPdf['min'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::DATE_RECUPERATION_PDF, $dateRecuperationPdf['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateRecuperationPdf['max'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::DATE_RECUPERATION_PDF, $dateRecuperationPdf['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeNumeroPeer::DATE_RECUPERATION_PDF, $dateRecuperationPdf, $comparison);
    }

    /**
     * Filter the query on the blob_id_xml column
     *
     * Example usage:
     * <code>
     * $query->filterByBlobIdXml(1234); // WHERE blob_id_xml = 1234
     * $query->filterByBlobIdXml(array(12, 34)); // WHERE blob_id_xml IN (12, 34)
     * $query->filterByBlobIdXml(array('min' => 12)); // WHERE blob_id_xml >= 12
     * $query->filterByBlobIdXml(array('max' => 12)); // WHERE blob_id_xml <= 12
     * </code>
     *
     * @param     mixed $blobIdXml The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function filterByBlobIdXml($blobIdXml = null, $comparison = null)
    {
        if (is_array($blobIdXml)) {
            $useMinMax = false;
            if (isset($blobIdXml['min'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::BLOB_ID_XML, $blobIdXml['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($blobIdXml['max'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::BLOB_ID_XML, $blobIdXml['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeNumeroPeer::BLOB_ID_XML, $blobIdXml, $comparison);
    }

    /**
     * Filter the query on the date_recuperation_xml column
     *
     * Example usage:
     * <code>
     * $query->filterByDateRecuperationXml('2011-03-14'); // WHERE date_recuperation_xml = '2011-03-14'
     * $query->filterByDateRecuperationXml('now'); // WHERE date_recuperation_xml = '2011-03-14'
     * $query->filterByDateRecuperationXml(array('max' => 'yesterday')); // WHERE date_recuperation_xml > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateRecuperationXml The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function filterByDateRecuperationXml($dateRecuperationXml = null, $comparison = null)
    {
        if (is_array($dateRecuperationXml)) {
            $useMinMax = false;
            if (isset($dateRecuperationXml['min'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::DATE_RECUPERATION_XML, $dateRecuperationXml['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateRecuperationXml['max'])) {
                $this->addUsingAlias(CommonTDumeNumeroPeer::DATE_RECUPERATION_XML, $dateRecuperationXml['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeNumeroPeer::DATE_RECUPERATION_XML, $dateRecuperationXml, $comparison);
    }

    /**
     * Filter the query by a related CommonTDumeContexte object
     *
     * @param   CommonTDumeContexte|PropelObjectCollection $commonTDumeContexte The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTDumeNumeroQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTDumeContexte($commonTDumeContexte, $comparison = null)
    {
        if ($commonTDumeContexte instanceof CommonTDumeContexte) {
            return $this
                ->addUsingAlias(CommonTDumeNumeroPeer::ID_DUME_CONTEXTE, $commonTDumeContexte->getId(), $comparison);
        } elseif ($commonTDumeContexte instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTDumeNumeroPeer::ID_DUME_CONTEXTE, $commonTDumeContexte->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTDumeContexte() only accepts arguments of type CommonTDumeContexte or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTDumeContexte relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function joinCommonTDumeContexte($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTDumeContexte');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTDumeContexte');
        }

        return $this;
    }

    /**
     * Use the CommonTDumeContexte relation CommonTDumeContexte object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTDumeContexteQuery A secondary query class using the current class as primary query
     */
    public function useCommonTDumeContexteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTDumeContexte($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTDumeContexte', '\Application\Propel\Mpe\CommonTDumeContexteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTDumeNumero $commonTDumeNumero Object to remove from the list of results
     *
     * @return CommonTDumeNumeroQuery The current query, for fluid interface
     */
    public function prune($commonTDumeNumero = null)
    {
        if ($commonTDumeNumero) {
            $this->addUsingAlias(CommonTDumeNumeroPeer::ID, $commonTDumeNumero->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
