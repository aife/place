<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTypeContratMpePivot;
use Application\Propel\Mpe\CommonTypeContratMpePivotPeer;
use Application\Propel\Mpe\CommonTypeContratMpePivotQuery;

/**
 * Base class that represents a query for the 'type_contrat_mpe_pivot' table.
 *
 *
 *
 * @method CommonTypeContratMpePivotQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTypeContratMpePivotQuery orderByIdTypeContratMpe($order = Criteria::ASC) Order by the id_type_contrat_mpe column
 * @method CommonTypeContratMpePivotQuery orderByIdTypeContratPivot($order = Criteria::ASC) Order by the id_type_contrat_pivot column
 *
 * @method CommonTypeContratMpePivotQuery groupById() Group by the id column
 * @method CommonTypeContratMpePivotQuery groupByIdTypeContratMpe() Group by the id_type_contrat_mpe column
 * @method CommonTypeContratMpePivotQuery groupByIdTypeContratPivot() Group by the id_type_contrat_pivot column
 *
 * @method CommonTypeContratMpePivotQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTypeContratMpePivotQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTypeContratMpePivotQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTypeContratMpePivot findOne(PropelPDO $con = null) Return the first CommonTypeContratMpePivot matching the query
 * @method CommonTypeContratMpePivot findOneOrCreate(PropelPDO $con = null) Return the first CommonTypeContratMpePivot matching the query, or a new CommonTypeContratMpePivot object populated from the query conditions when no match is found
 *
 * @method CommonTypeContratMpePivot findOneByIdTypeContratMpe(int $id_type_contrat_mpe) Return the first CommonTypeContratMpePivot filtered by the id_type_contrat_mpe column
 * @method CommonTypeContratMpePivot findOneByIdTypeContratPivot(int $id_type_contrat_pivot) Return the first CommonTypeContratMpePivot filtered by the id_type_contrat_pivot column
 *
 * @method array findById(int $id) Return CommonTypeContratMpePivot objects filtered by the id column
 * @method array findByIdTypeContratMpe(int $id_type_contrat_mpe) Return CommonTypeContratMpePivot objects filtered by the id_type_contrat_mpe column
 * @method array findByIdTypeContratPivot(int $id_type_contrat_pivot) Return CommonTypeContratMpePivot objects filtered by the id_type_contrat_pivot column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTypeContratMpePivotQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTypeContratMpePivotQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTypeContratMpePivot', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTypeContratMpePivotQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTypeContratMpePivotQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTypeContratMpePivotQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTypeContratMpePivotQuery) {
            return $criteria;
        }
        $query = new CommonTypeContratMpePivotQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTypeContratMpePivot|CommonTypeContratMpePivot[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTypeContratMpePivotPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTypeContratMpePivotPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTypeContratMpePivot A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTypeContratMpePivot A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_type_contrat_mpe`, `id_type_contrat_pivot` FROM `type_contrat_mpe_pivot` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTypeContratMpePivot();
            $obj->hydrate($row);
            CommonTypeContratMpePivotPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTypeContratMpePivot|CommonTypeContratMpePivot[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTypeContratMpePivot[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTypeContratMpePivotQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTypeContratMpePivotQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTypeContratMpePivotQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_type_contrat_mpe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeContratMpe(1234); // WHERE id_type_contrat_mpe = 1234
     * $query->filterByIdTypeContratMpe(array(12, 34)); // WHERE id_type_contrat_mpe IN (12, 34)
     * $query->filterByIdTypeContratMpe(array('min' => 12)); // WHERE id_type_contrat_mpe >= 12
     * $query->filterByIdTypeContratMpe(array('max' => 12)); // WHERE id_type_contrat_mpe <= 12
     * </code>
     *
     * @param     mixed $idTypeContratMpe The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTypeContratMpePivotQuery The current query, for fluid interface
     */
    public function filterByIdTypeContratMpe($idTypeContratMpe = null, $comparison = null)
    {
        if (is_array($idTypeContratMpe)) {
            $useMinMax = false;
            if (isset($idTypeContratMpe['min'])) {
                $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID_TYPE_CONTRAT_MPE, $idTypeContratMpe['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeContratMpe['max'])) {
                $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID_TYPE_CONTRAT_MPE, $idTypeContratMpe['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID_TYPE_CONTRAT_MPE, $idTypeContratMpe, $comparison);
    }

    /**
     * Filter the query on the id_type_contrat_pivot column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeContratPivot(1234); // WHERE id_type_contrat_pivot = 1234
     * $query->filterByIdTypeContratPivot(array(12, 34)); // WHERE id_type_contrat_pivot IN (12, 34)
     * $query->filterByIdTypeContratPivot(array('min' => 12)); // WHERE id_type_contrat_pivot >= 12
     * $query->filterByIdTypeContratPivot(array('max' => 12)); // WHERE id_type_contrat_pivot <= 12
     * </code>
     *
     * @param     mixed $idTypeContratPivot The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTypeContratMpePivotQuery The current query, for fluid interface
     */
    public function filterByIdTypeContratPivot($idTypeContratPivot = null, $comparison = null)
    {
        if (is_array($idTypeContratPivot)) {
            $useMinMax = false;
            if (isset($idTypeContratPivot['min'])) {
                $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID_TYPE_CONTRAT_PIVOT, $idTypeContratPivot['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeContratPivot['max'])) {
                $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID_TYPE_CONTRAT_PIVOT, $idTypeContratPivot['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID_TYPE_CONTRAT_PIVOT, $idTypeContratPivot, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTypeContratMpePivot $commonTypeContratMpePivot Object to remove from the list of results
     *
     * @return CommonTypeContratMpePivotQuery The current query, for fluid interface
     */
    public function prune($commonTypeContratMpePivot = null)
    {
        if ($commonTypeContratMpePivot) {
            $this->addUsingAlias(CommonTypeContratMpePivotPeer::ID, $commonTypeContratMpePivot->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
