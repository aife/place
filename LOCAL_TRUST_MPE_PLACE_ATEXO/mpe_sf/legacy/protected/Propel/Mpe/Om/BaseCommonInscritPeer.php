<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\CommonTmpSiretIncorrectPeer;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Propel\Mpe\Map\CommonInscritTableMap;

/**
 * Base static class for performing query and update operations on the 'Inscrit' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonInscritPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'Inscrit';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonInscrit';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonInscritTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 41;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 41;

    /** the column name for the old_id field */
    const OLD_ID = 'Inscrit.old_id';

    /** the column name for the entreprise_id field */
    const ENTREPRISE_ID = 'Inscrit.entreprise_id';

    /** the column name for the id_etablissement field */
    const ID_ETABLISSEMENT = 'Inscrit.id_etablissement';

    /** the column name for the login field */
    const LOGIN = 'Inscrit.login';

    /** the column name for the mdp field */
    const MDP = 'Inscrit.mdp';

    /** the column name for the num_cert field */
    const NUM_CERT = 'Inscrit.num_cert';

    /** the column name for the cert field */
    const CERT = 'Inscrit.cert';

    /** the column name for the civilite field */
    const CIVILITE = 'Inscrit.civilite';

    /** the column name for the nom field */
    const NOM = 'Inscrit.nom';

    /** the column name for the prenom field */
    const PRENOM = 'Inscrit.prenom';

    /** the column name for the adresse field */
    const ADRESSE = 'Inscrit.adresse';

    /** the column name for the codepostal field */
    const CODEPOSTAL = 'Inscrit.codepostal';

    /** the column name for the ville field */
    const VILLE = 'Inscrit.ville';

    /** the column name for the pays field */
    const PAYS = 'Inscrit.pays';

    /** the column name for the email field */
    const EMAIL = 'Inscrit.email';

    /** the column name for the telephone field */
    const TELEPHONE = 'Inscrit.telephone';

    /** the column name for the categorie field */
    const CATEGORIE = 'Inscrit.categorie';

    /** the column name for the motstitreresume field */
    const MOTSTITRERESUME = 'Inscrit.motstitreresume';

    /** the column name for the periode field */
    const PERIODE = 'Inscrit.periode';

    /** the column name for the siret field */
    const SIRET = 'Inscrit.siret';

    /** the column name for the fax field */
    const FAX = 'Inscrit.fax';

    /** the column name for the code_cpv field */
    const CODE_CPV = 'Inscrit.code_cpv';

    /** the column name for the id_langue field */
    const ID_LANGUE = 'Inscrit.id_langue';

    /** the column name for the profil field */
    const PROFIL = 'Inscrit.profil';

    /** the column name for the adresse2 field */
    const ADRESSE2 = 'Inscrit.adresse2';

    /** the column name for the bloque field */
    const BLOQUE = 'Inscrit.bloque';

    /** the column name for the id_initial field */
    const ID_INITIAL = 'Inscrit.id_initial';

    /** the column name for the inscrit_annuaire_defense field */
    const INSCRIT_ANNUAIRE_DEFENSE = 'Inscrit.inscrit_annuaire_defense';

    /** the column name for the date_creation field */
    const DATE_CREATION = 'Inscrit.date_creation';

    /** the column name for the date_modification field */
    const DATE_MODIFICATION = 'Inscrit.date_modification';

    /** the column name for the tentatives_mdp field */
    const TENTATIVES_MDP = 'Inscrit.tentatives_mdp';

    /** the column name for the uid field */
    const UID = 'Inscrit.uid';

    /** the column name for the type_hash field */
    const TYPE_HASH = 'Inscrit.type_hash';

    /** the column name for the id_externe field */
    const ID_EXTERNE = 'Inscrit.id_externe';

    /** the column name for the id field */
    const ID = 'Inscrit.id';

    /** the column name for the date_validation_rgpd field */
    const DATE_VALIDATION_RGPD = 'Inscrit.date_validation_rgpd';

    /** the column name for the rgpd_communication_place field */
    const RGPD_COMMUNICATION_PLACE = 'Inscrit.rgpd_communication_place';

    /** the column name for the rgpd_enquete field */
    const RGPD_ENQUETE = 'Inscrit.rgpd_enquete';

    /** the column name for the rgpd_communication field */
    const RGPD_COMMUNICATION = 'Inscrit.rgpd_communication';

    /** the column name for the deleted_at field */
    const DELETED_AT = 'Inscrit.deleted_at';

    /** the column name for the deleted field */
    const DELETED = 'Inscrit.deleted';

    /** The enumerated values for the bloque field */
    const BLOQUE_0 = '0';
    const BLOQUE_1 = '1';

    /** The enumerated values for the inscrit_annuaire_defense field */
    const INSCRIT_ANNUAIRE_DEFENSE_0 = '0';
    const INSCRIT_ANNUAIRE_DEFENSE_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonInscrit objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonInscrit[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonInscritPeer::$fieldNames[CommonInscritPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('OldId', 'EntrepriseId', 'IdEtablissement', 'Login', 'Mdp', 'NumCert', 'Cert', 'Civilite', 'Nom', 'Prenom', 'Adresse', 'Codepostal', 'Ville', 'Pays', 'Email', 'Telephone', 'Categorie', 'Motstitreresume', 'Periode', 'Siret', 'Fax', 'CodeCpv', 'IdLangue', 'Profil', 'Adresse2', 'Bloque', 'IdInitial', 'InscritAnnuaireDefense', 'DateCreation', 'DateModification', 'TentativesMdp', 'Uid', 'TypeHash', 'IdExterne', 'Id', 'DateValidationRgpd', 'RgpdCommunicationPlace', 'RgpdEnquete', 'RgpdCommunication', 'DeletedAt', 'Deleted', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('oldId', 'entrepriseId', 'idEtablissement', 'login', 'mdp', 'numCert', 'cert', 'civilite', 'nom', 'prenom', 'adresse', 'codepostal', 'ville', 'pays', 'email', 'telephone', 'categorie', 'motstitreresume', 'periode', 'siret', 'fax', 'codeCpv', 'idLangue', 'profil', 'adresse2', 'bloque', 'idInitial', 'inscritAnnuaireDefense', 'dateCreation', 'dateModification', 'tentativesMdp', 'uid', 'typeHash', 'idExterne', 'id', 'dateValidationRgpd', 'rgpdCommunicationPlace', 'rgpdEnquete', 'rgpdCommunication', 'deletedAt', 'deleted', ),
        BasePeer::TYPE_COLNAME => array (CommonInscritPeer::OLD_ID, CommonInscritPeer::ENTREPRISE_ID, CommonInscritPeer::ID_ETABLISSEMENT, CommonInscritPeer::LOGIN, CommonInscritPeer::MDP, CommonInscritPeer::NUM_CERT, CommonInscritPeer::CERT, CommonInscritPeer::CIVILITE, CommonInscritPeer::NOM, CommonInscritPeer::PRENOM, CommonInscritPeer::ADRESSE, CommonInscritPeer::CODEPOSTAL, CommonInscritPeer::VILLE, CommonInscritPeer::PAYS, CommonInscritPeer::EMAIL, CommonInscritPeer::TELEPHONE, CommonInscritPeer::CATEGORIE, CommonInscritPeer::MOTSTITRERESUME, CommonInscritPeer::PERIODE, CommonInscritPeer::SIRET, CommonInscritPeer::FAX, CommonInscritPeer::CODE_CPV, CommonInscritPeer::ID_LANGUE, CommonInscritPeer::PROFIL, CommonInscritPeer::ADRESSE2, CommonInscritPeer::BLOQUE, CommonInscritPeer::ID_INITIAL, CommonInscritPeer::INSCRIT_ANNUAIRE_DEFENSE, CommonInscritPeer::DATE_CREATION, CommonInscritPeer::DATE_MODIFICATION, CommonInscritPeer::TENTATIVES_MDP, CommonInscritPeer::UID, CommonInscritPeer::TYPE_HASH, CommonInscritPeer::ID_EXTERNE, CommonInscritPeer::ID, CommonInscritPeer::DATE_VALIDATION_RGPD, CommonInscritPeer::RGPD_COMMUNICATION_PLACE, CommonInscritPeer::RGPD_ENQUETE, CommonInscritPeer::RGPD_COMMUNICATION, CommonInscritPeer::DELETED_AT, CommonInscritPeer::DELETED, ),
        BasePeer::TYPE_RAW_COLNAME => array ('OLD_ID', 'ENTREPRISE_ID', 'ID_ETABLISSEMENT', 'LOGIN', 'MDP', 'NUM_CERT', 'CERT', 'CIVILITE', 'NOM', 'PRENOM', 'ADRESSE', 'CODEPOSTAL', 'VILLE', 'PAYS', 'EMAIL', 'TELEPHONE', 'CATEGORIE', 'MOTSTITRERESUME', 'PERIODE', 'SIRET', 'FAX', 'CODE_CPV', 'ID_LANGUE', 'PROFIL', 'ADRESSE2', 'BLOQUE', 'ID_INITIAL', 'INSCRIT_ANNUAIRE_DEFENSE', 'DATE_CREATION', 'DATE_MODIFICATION', 'TENTATIVES_MDP', 'UID', 'TYPE_HASH', 'ID_EXTERNE', 'ID', 'DATE_VALIDATION_RGPD', 'RGPD_COMMUNICATION_PLACE', 'RGPD_ENQUETE', 'RGPD_COMMUNICATION', 'DELETED_AT', 'DELETED', ),
        BasePeer::TYPE_FIELDNAME => array ('old_id', 'entreprise_id', 'id_etablissement', 'login', 'mdp', 'num_cert', 'cert', 'civilite', 'nom', 'prenom', 'adresse', 'codepostal', 'ville', 'pays', 'email', 'telephone', 'categorie', 'motstitreresume', 'periode', 'siret', 'fax', 'code_cpv', 'id_langue', 'profil', 'adresse2', 'bloque', 'id_initial', 'inscrit_annuaire_defense', 'date_creation', 'date_modification', 'tentatives_mdp', 'uid', 'type_hash', 'id_externe', 'id', 'date_validation_rgpd', 'rgpd_communication_place', 'rgpd_enquete', 'rgpd_communication', 'deleted_at', 'deleted', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonInscritPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('OldId' => 0, 'EntrepriseId' => 1, 'IdEtablissement' => 2, 'Login' => 3, 'Mdp' => 4, 'NumCert' => 5, 'Cert' => 6, 'Civilite' => 7, 'Nom' => 8, 'Prenom' => 9, 'Adresse' => 10, 'Codepostal' => 11, 'Ville' => 12, 'Pays' => 13, 'Email' => 14, 'Telephone' => 15, 'Categorie' => 16, 'Motstitreresume' => 17, 'Periode' => 18, 'Siret' => 19, 'Fax' => 20, 'CodeCpv' => 21, 'IdLangue' => 22, 'Profil' => 23, 'Adresse2' => 24, 'Bloque' => 25, 'IdInitial' => 26, 'InscritAnnuaireDefense' => 27, 'DateCreation' => 28, 'DateModification' => 29, 'TentativesMdp' => 30, 'Uid' => 31, 'TypeHash' => 32, 'IdExterne' => 33, 'Id' => 34, 'DateValidationRgpd' => 35, 'RgpdCommunicationPlace' => 36, 'RgpdEnquete' => 37, 'RgpdCommunication' => 38, 'DeletedAt' => 39, 'Deleted' => 40, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('oldId' => 0, 'entrepriseId' => 1, 'idEtablissement' => 2, 'login' => 3, 'mdp' => 4, 'numCert' => 5, 'cert' => 6, 'civilite' => 7, 'nom' => 8, 'prenom' => 9, 'adresse' => 10, 'codepostal' => 11, 'ville' => 12, 'pays' => 13, 'email' => 14, 'telephone' => 15, 'categorie' => 16, 'motstitreresume' => 17, 'periode' => 18, 'siret' => 19, 'fax' => 20, 'codeCpv' => 21, 'idLangue' => 22, 'profil' => 23, 'adresse2' => 24, 'bloque' => 25, 'idInitial' => 26, 'inscritAnnuaireDefense' => 27, 'dateCreation' => 28, 'dateModification' => 29, 'tentativesMdp' => 30, 'uid' => 31, 'typeHash' => 32, 'idExterne' => 33, 'id' => 34, 'dateValidationRgpd' => 35, 'rgpdCommunicationPlace' => 36, 'rgpdEnquete' => 37, 'rgpdCommunication' => 38, 'deletedAt' => 39, 'deleted' => 40, ),
        BasePeer::TYPE_COLNAME => array (CommonInscritPeer::OLD_ID => 0, CommonInscritPeer::ENTREPRISE_ID => 1, CommonInscritPeer::ID_ETABLISSEMENT => 2, CommonInscritPeer::LOGIN => 3, CommonInscritPeer::MDP => 4, CommonInscritPeer::NUM_CERT => 5, CommonInscritPeer::CERT => 6, CommonInscritPeer::CIVILITE => 7, CommonInscritPeer::NOM => 8, CommonInscritPeer::PRENOM => 9, CommonInscritPeer::ADRESSE => 10, CommonInscritPeer::CODEPOSTAL => 11, CommonInscritPeer::VILLE => 12, CommonInscritPeer::PAYS => 13, CommonInscritPeer::EMAIL => 14, CommonInscritPeer::TELEPHONE => 15, CommonInscritPeer::CATEGORIE => 16, CommonInscritPeer::MOTSTITRERESUME => 17, CommonInscritPeer::PERIODE => 18, CommonInscritPeer::SIRET => 19, CommonInscritPeer::FAX => 20, CommonInscritPeer::CODE_CPV => 21, CommonInscritPeer::ID_LANGUE => 22, CommonInscritPeer::PROFIL => 23, CommonInscritPeer::ADRESSE2 => 24, CommonInscritPeer::BLOQUE => 25, CommonInscritPeer::ID_INITIAL => 26, CommonInscritPeer::INSCRIT_ANNUAIRE_DEFENSE => 27, CommonInscritPeer::DATE_CREATION => 28, CommonInscritPeer::DATE_MODIFICATION => 29, CommonInscritPeer::TENTATIVES_MDP => 30, CommonInscritPeer::UID => 31, CommonInscritPeer::TYPE_HASH => 32, CommonInscritPeer::ID_EXTERNE => 33, CommonInscritPeer::ID => 34, CommonInscritPeer::DATE_VALIDATION_RGPD => 35, CommonInscritPeer::RGPD_COMMUNICATION_PLACE => 36, CommonInscritPeer::RGPD_ENQUETE => 37, CommonInscritPeer::RGPD_COMMUNICATION => 38, CommonInscritPeer::DELETED_AT => 39, CommonInscritPeer::DELETED => 40, ),
        BasePeer::TYPE_RAW_COLNAME => array ('OLD_ID' => 0, 'ENTREPRISE_ID' => 1, 'ID_ETABLISSEMENT' => 2, 'LOGIN' => 3, 'MDP' => 4, 'NUM_CERT' => 5, 'CERT' => 6, 'CIVILITE' => 7, 'NOM' => 8, 'PRENOM' => 9, 'ADRESSE' => 10, 'CODEPOSTAL' => 11, 'VILLE' => 12, 'PAYS' => 13, 'EMAIL' => 14, 'TELEPHONE' => 15, 'CATEGORIE' => 16, 'MOTSTITRERESUME' => 17, 'PERIODE' => 18, 'SIRET' => 19, 'FAX' => 20, 'CODE_CPV' => 21, 'ID_LANGUE' => 22, 'PROFIL' => 23, 'ADRESSE2' => 24, 'BLOQUE' => 25, 'ID_INITIAL' => 26, 'INSCRIT_ANNUAIRE_DEFENSE' => 27, 'DATE_CREATION' => 28, 'DATE_MODIFICATION' => 29, 'TENTATIVES_MDP' => 30, 'UID' => 31, 'TYPE_HASH' => 32, 'ID_EXTERNE' => 33, 'ID' => 34, 'DATE_VALIDATION_RGPD' => 35, 'RGPD_COMMUNICATION_PLACE' => 36, 'RGPD_ENQUETE' => 37, 'RGPD_COMMUNICATION' => 38, 'DELETED_AT' => 39, 'DELETED' => 40, ),
        BasePeer::TYPE_FIELDNAME => array ('old_id' => 0, 'entreprise_id' => 1, 'id_etablissement' => 2, 'login' => 3, 'mdp' => 4, 'num_cert' => 5, 'cert' => 6, 'civilite' => 7, 'nom' => 8, 'prenom' => 9, 'adresse' => 10, 'codepostal' => 11, 'ville' => 12, 'pays' => 13, 'email' => 14, 'telephone' => 15, 'categorie' => 16, 'motstitreresume' => 17, 'periode' => 18, 'siret' => 19, 'fax' => 20, 'code_cpv' => 21, 'id_langue' => 22, 'profil' => 23, 'adresse2' => 24, 'bloque' => 25, 'id_initial' => 26, 'inscrit_annuaire_defense' => 27, 'date_creation' => 28, 'date_modification' => 29, 'tentatives_mdp' => 30, 'uid' => 31, 'type_hash' => 32, 'id_externe' => 33, 'id' => 34, 'date_validation_rgpd' => 35, 'rgpd_communication_place' => 36, 'rgpd_enquete' => 37, 'rgpd_communication' => 38, 'deleted_at' => 39, 'deleted' => 40, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonInscritPeer::BLOQUE => array(
            CommonInscritPeer::BLOQUE_0,
            CommonInscritPeer::BLOQUE_1,
        ),
        CommonInscritPeer::INSCRIT_ANNUAIRE_DEFENSE => array(
            CommonInscritPeer::INSCRIT_ANNUAIRE_DEFENSE_0,
            CommonInscritPeer::INSCRIT_ANNUAIRE_DEFENSE_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonInscritPeer::getFieldNames($toType);
        $key = isset(CommonInscritPeer::$fieldKeys[$fromType][$name]) ? CommonInscritPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonInscritPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonInscritPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonInscritPeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonInscritPeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonInscritPeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonInscritPeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonInscritPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonInscritPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonInscritPeer::OLD_ID);
            $criteria->addSelectColumn(CommonInscritPeer::ENTREPRISE_ID);
            $criteria->addSelectColumn(CommonInscritPeer::ID_ETABLISSEMENT);
            $criteria->addSelectColumn(CommonInscritPeer::LOGIN);
            $criteria->addSelectColumn(CommonInscritPeer::MDP);
            $criteria->addSelectColumn(CommonInscritPeer::NUM_CERT);
            $criteria->addSelectColumn(CommonInscritPeer::CERT);
            $criteria->addSelectColumn(CommonInscritPeer::CIVILITE);
            $criteria->addSelectColumn(CommonInscritPeer::NOM);
            $criteria->addSelectColumn(CommonInscritPeer::PRENOM);
            $criteria->addSelectColumn(CommonInscritPeer::ADRESSE);
            $criteria->addSelectColumn(CommonInscritPeer::CODEPOSTAL);
            $criteria->addSelectColumn(CommonInscritPeer::VILLE);
            $criteria->addSelectColumn(CommonInscritPeer::PAYS);
            $criteria->addSelectColumn(CommonInscritPeer::EMAIL);
            $criteria->addSelectColumn(CommonInscritPeer::TELEPHONE);
            $criteria->addSelectColumn(CommonInscritPeer::CATEGORIE);
            $criteria->addSelectColumn(CommonInscritPeer::MOTSTITRERESUME);
            $criteria->addSelectColumn(CommonInscritPeer::PERIODE);
            $criteria->addSelectColumn(CommonInscritPeer::SIRET);
            $criteria->addSelectColumn(CommonInscritPeer::FAX);
            $criteria->addSelectColumn(CommonInscritPeer::CODE_CPV);
            $criteria->addSelectColumn(CommonInscritPeer::ID_LANGUE);
            $criteria->addSelectColumn(CommonInscritPeer::PROFIL);
            $criteria->addSelectColumn(CommonInscritPeer::ADRESSE2);
            $criteria->addSelectColumn(CommonInscritPeer::BLOQUE);
            $criteria->addSelectColumn(CommonInscritPeer::ID_INITIAL);
            $criteria->addSelectColumn(CommonInscritPeer::INSCRIT_ANNUAIRE_DEFENSE);
            $criteria->addSelectColumn(CommonInscritPeer::DATE_CREATION);
            $criteria->addSelectColumn(CommonInscritPeer::DATE_MODIFICATION);
            $criteria->addSelectColumn(CommonInscritPeer::TENTATIVES_MDP);
            $criteria->addSelectColumn(CommonInscritPeer::UID);
            $criteria->addSelectColumn(CommonInscritPeer::TYPE_HASH);
            $criteria->addSelectColumn(CommonInscritPeer::ID_EXTERNE);
            $criteria->addSelectColumn(CommonInscritPeer::ID);
            $criteria->addSelectColumn(CommonInscritPeer::DATE_VALIDATION_RGPD);
            $criteria->addSelectColumn(CommonInscritPeer::RGPD_COMMUNICATION_PLACE);
            $criteria->addSelectColumn(CommonInscritPeer::RGPD_ENQUETE);
            $criteria->addSelectColumn(CommonInscritPeer::RGPD_COMMUNICATION);
            $criteria->addSelectColumn(CommonInscritPeer::DELETED_AT);
            $criteria->addSelectColumn(CommonInscritPeer::DELETED);
        } else {
            $criteria->addSelectColumn($alias . '.old_id');
            $criteria->addSelectColumn($alias . '.entreprise_id');
            $criteria->addSelectColumn($alias . '.id_etablissement');
            $criteria->addSelectColumn($alias . '.login');
            $criteria->addSelectColumn($alias . '.mdp');
            $criteria->addSelectColumn($alias . '.num_cert');
            $criteria->addSelectColumn($alias . '.cert');
            $criteria->addSelectColumn($alias . '.civilite');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.prenom');
            $criteria->addSelectColumn($alias . '.adresse');
            $criteria->addSelectColumn($alias . '.codepostal');
            $criteria->addSelectColumn($alias . '.ville');
            $criteria->addSelectColumn($alias . '.pays');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.telephone');
            $criteria->addSelectColumn($alias . '.categorie');
            $criteria->addSelectColumn($alias . '.motstitreresume');
            $criteria->addSelectColumn($alias . '.periode');
            $criteria->addSelectColumn($alias . '.siret');
            $criteria->addSelectColumn($alias . '.fax');
            $criteria->addSelectColumn($alias . '.code_cpv');
            $criteria->addSelectColumn($alias . '.id_langue');
            $criteria->addSelectColumn($alias . '.profil');
            $criteria->addSelectColumn($alias . '.adresse2');
            $criteria->addSelectColumn($alias . '.bloque');
            $criteria->addSelectColumn($alias . '.id_initial');
            $criteria->addSelectColumn($alias . '.inscrit_annuaire_defense');
            $criteria->addSelectColumn($alias . '.date_creation');
            $criteria->addSelectColumn($alias . '.date_modification');
            $criteria->addSelectColumn($alias . '.tentatives_mdp');
            $criteria->addSelectColumn($alias . '.uid');
            $criteria->addSelectColumn($alias . '.type_hash');
            $criteria->addSelectColumn($alias . '.id_externe');
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.date_validation_rgpd');
            $criteria->addSelectColumn($alias . '.rgpd_communication_place');
            $criteria->addSelectColumn($alias . '.rgpd_enquete');
            $criteria->addSelectColumn($alias . '.rgpd_communication');
            $criteria->addSelectColumn($alias . '.deleted_at');
            $criteria->addSelectColumn($alias . '.deleted');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInscritPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInscritPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonInscritPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonInscrit
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonInscritPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonInscritPeer::populateObjects(CommonInscritPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonInscritPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonInscrit $obj A CommonInscrit object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonInscritPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonInscrit object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonInscrit) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonInscrit object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonInscritPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonInscrit Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonInscritPeer::$instances[$key])) {
                return CommonInscritPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonInscritPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonInscritPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to Inscrit
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in CommonTmpSiretIncorrectPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonTmpSiretIncorrectPeer::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol + 34] === null) {
            return null;
        }

        return (string) $row[$startcol + 34];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (string) $row[$startcol + 34];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonInscritPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonInscritPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonInscritPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonInscritPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonInscrit object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonInscritPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonInscritPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonInscritPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonInscritPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related Entreprise table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinEntreprise(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInscritPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInscritPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonInscritPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTEtablissement table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTEtablissement(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInscritPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInscritPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonInscritPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonInscrit objects pre-filled with their Entreprise objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonInscrit objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinEntreprise(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);
        }

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol = CommonInscritPeer::NUM_HYDRATE_COLUMNS;
        EntreprisePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonInscritPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonInscritPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonInscritPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonInscritPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonInscrit) to $obj2 (Entreprise)
                $obj2->addCommonInscrit($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonInscrit objects pre-filled with their CommonTEtablissement objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonInscrit objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTEtablissement(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);
        }

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol = CommonInscritPeer::NUM_HYDRATE_COLUMNS;
        CommonTEtablissementPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonInscritPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonInscritPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonInscritPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonInscritPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTEtablissementPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTEtablissementPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTEtablissementPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonInscrit) to $obj2 (CommonTEtablissement)
                $obj2->addCommonInscrit($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInscritPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInscritPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonInscritPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonInscritPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonInscrit objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonInscrit objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);
        }

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol2 = CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonInscritPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $criteria->addJoin(CommonInscritPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonInscritPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonInscritPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonInscritPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined Entreprise rows

            $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonInscrit) to the collection in $obj2 (Entreprise)
                $obj2->addCommonInscrit($obj1);
            } // if joined row not null

            // Add objects for joined CommonTEtablissement rows

            $key3 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonTEtablissementPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonTEtablissementPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTEtablissementPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonInscrit) to the collection in $obj3 (CommonTEtablissement)
                $obj3->addCommonInscrit($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related Entreprise table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptEntreprise(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInscritPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInscritPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonInscritPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTEtablissement table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTEtablissement(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInscritPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInscritPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonInscritPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonInscrit objects pre-filled with all related objects except Entreprise.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonInscrit objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptEntreprise(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);
        }

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol2 = CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonInscritPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonInscritPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonInscritPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonInscritPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTEtablissement rows

                $key2 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTEtablissementPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTEtablissementPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTEtablissementPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonInscrit) to the collection in $obj2 (CommonTEtablissement)
                $obj2->addCommonInscrit($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonInscrit objects pre-filled with all related objects except CommonTEtablissement.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonInscrit objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTEtablissement(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);
        }

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol2 = CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        EntreprisePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + EntreprisePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonInscritPeer::ENTREPRISE_ID, EntreprisePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonInscritPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonInscritPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonInscritPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined Entreprise rows

                $key2 = EntreprisePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = EntreprisePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = EntreprisePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    EntreprisePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonInscrit) to the collection in $obj2 (Entreprise)
                $obj2->addCommonInscrit($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonInscritPeer::DATABASE_NAME)->getTable(CommonInscritPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonInscritPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonInscritPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonInscritTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonInscritPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonInscrit or Criteria object.
     *
     * @param      mixed $values Criteria or CommonInscrit object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonInscrit object
        }

        if ($criteria->containsKey(CommonInscritPeer::ID) && $criteria->keyContainsValue(CommonInscritPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonInscritPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonInscrit or Criteria object.
     *
     * @param      mixed $values Criteria or CommonInscrit object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonInscritPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonInscritPeer::ID);
            $value = $criteria->remove(CommonInscritPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonInscritPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonInscritPeer::TABLE_NAME);
            }

        } else { // $values is CommonInscrit object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the Inscrit table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += CommonInscritPeer::doOnDeleteCascade(new Criteria(CommonInscritPeer::DATABASE_NAME), $con);
            $affectedRows += BasePeer::doDeleteAll(CommonInscritPeer::TABLE_NAME, $con, CommonInscritPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonInscritPeer::clearInstancePool();
            CommonInscritPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonInscrit or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonInscrit object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonInscrit) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonInscritPeer::DATABASE_NAME);
            $criteria->add(CommonInscritPeer::ID, (array) $values, Criteria::IN);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonInscritPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            // cloning the Criteria in case it's modified by doSelect() or doSelectStmt()
            $c = clone $criteria;
            $affectedRows += CommonInscritPeer::doOnDeleteCascade($c, $con);

            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            if ($values instanceof Criteria) {
                CommonInscritPeer::clearInstancePool();
            } elseif ($values instanceof CommonInscrit) { // it's a model object
                CommonInscritPeer::removeInstanceFromPool($values);
            } else { // it's a primary key, or an array of pks
                foreach ((array) $values as $singleval) {
                    CommonInscritPeer::removeInstanceFromPool($singleval);
                }
            }

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonInscritPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * This is a method for emulating ON DELETE CASCADE for DBs that don't support this
     * feature (like MySQL or SQLite).
     *
     * This method is not very speedy because it must perform a query first to get
     * the implicated records and then perform the deletes by calling those Peer classes.
     *
     * This method should be used within a transaction if possible.
     *
     * @param      Criteria $criteria
     * @param      PropelPDO $con
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    protected static function doOnDeleteCascade(Criteria $criteria, PropelPDO $con)
    {
        // initialize var to track total num of affected rows
        $affectedRows = 0;

        // first find the objects that are implicated by the $criteria
        $objects = CommonInscritPeer::doSelect($criteria, $con);
        foreach ($objects as $obj) {


            // delete related CommonTmpSiretIncorrect objects
            $criteria = new Criteria(CommonTmpSiretIncorrectPeer::DATABASE_NAME);

            $criteria->add(CommonTmpSiretIncorrectPeer::ID_INSCRIT, $obj->getId());
            $affectedRows += CommonTmpSiretIncorrectPeer::doDelete($criteria, $con);
        }

        return $affectedRows;
    }

    /**
     * Validates all modified columns of given CommonInscrit object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonInscrit $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonInscritPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonInscritPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonInscritPeer::DATABASE_NAME, CommonInscritPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      string $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonInscrit
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonInscritPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonInscritPeer::DATABASE_NAME);
        $criteria->add(CommonInscritPeer::ID, $pk);

        $v = CommonInscritPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonInscrit[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonInscritPeer::DATABASE_NAME);
            $criteria->add(CommonInscritPeer::ID, $pks, Criteria::IN);
            $objs = CommonInscritPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonInscritPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonInscritPeer::buildTableMap();

