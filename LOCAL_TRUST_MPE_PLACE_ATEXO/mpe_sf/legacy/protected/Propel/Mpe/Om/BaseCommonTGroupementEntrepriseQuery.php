<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Propel\Mpe\CommonTGroupementEntreprisePeer;
use Application\Propel\Mpe\CommonTGroupementEntrepriseQuery;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprise;
use Application\Propel\Mpe\CommonTTypeGroupementEntreprise;

/**
 * Base class that represents a query for the 't_groupement_entreprise' table.
 *
 *
 *
 * @method CommonTGroupementEntrepriseQuery orderByIdGroupementEntreprise($order = Criteria::ASC) Order by the id_groupement_entreprise column
 * @method CommonTGroupementEntrepriseQuery orderByIdTypeGroupement($order = Criteria::ASC) Order by the id_type_groupement column
 * @method CommonTGroupementEntrepriseQuery orderByIdOffre($order = Criteria::ASC) Order by the id_offre column
 * @method CommonTGroupementEntrepriseQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTGroupementEntrepriseQuery orderByIdCandidature($order = Criteria::ASC) Order by the id_candidature column
 *
 * @method CommonTGroupementEntrepriseQuery groupByIdGroupementEntreprise() Group by the id_groupement_entreprise column
 * @method CommonTGroupementEntrepriseQuery groupByIdTypeGroupement() Group by the id_type_groupement column
 * @method CommonTGroupementEntrepriseQuery groupByIdOffre() Group by the id_offre column
 * @method CommonTGroupementEntrepriseQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTGroupementEntrepriseQuery groupByIdCandidature() Group by the id_candidature column
 *
 * @method CommonTGroupementEntrepriseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTGroupementEntrepriseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTGroupementEntrepriseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTGroupementEntrepriseQuery leftJoinCommonOffres($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOffres relation
 * @method CommonTGroupementEntrepriseQuery rightJoinCommonOffres($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOffres relation
 * @method CommonTGroupementEntrepriseQuery innerJoinCommonOffres($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOffres relation
 *
 * @method CommonTGroupementEntrepriseQuery leftJoinCommonTTypeGroupementEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTTypeGroupementEntreprise relation
 * @method CommonTGroupementEntrepriseQuery rightJoinCommonTTypeGroupementEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTTypeGroupementEntreprise relation
 * @method CommonTGroupementEntrepriseQuery innerJoinCommonTTypeGroupementEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTTypeGroupementEntreprise relation
 *
 * @method CommonTGroupementEntrepriseQuery leftJoinCommonTCandidature($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCandidature relation
 * @method CommonTGroupementEntrepriseQuery rightJoinCommonTCandidature($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCandidature relation
 * @method CommonTGroupementEntrepriseQuery innerJoinCommonTCandidature($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCandidature relation
 *
 * @method CommonTGroupementEntrepriseQuery leftJoinCommonTMembreGroupementEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
 * @method CommonTGroupementEntrepriseQuery rightJoinCommonTMembreGroupementEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
 * @method CommonTGroupementEntrepriseQuery innerJoinCommonTMembreGroupementEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
 *
 * @method CommonTGroupementEntreprise findOne(PropelPDO $con = null) Return the first CommonTGroupementEntreprise matching the query
 * @method CommonTGroupementEntreprise findOneOrCreate(PropelPDO $con = null) Return the first CommonTGroupementEntreprise matching the query, or a new CommonTGroupementEntreprise object populated from the query conditions when no match is found
 *
 * @method CommonTGroupementEntreprise findOneByIdTypeGroupement(int $id_type_groupement) Return the first CommonTGroupementEntreprise filtered by the id_type_groupement column
 * @method CommonTGroupementEntreprise findOneByIdOffre(int $id_offre) Return the first CommonTGroupementEntreprise filtered by the id_offre column
 * @method CommonTGroupementEntreprise findOneByDateCreation(string $date_creation) Return the first CommonTGroupementEntreprise filtered by the date_creation column
 * @method CommonTGroupementEntreprise findOneByIdCandidature(int $id_candidature) Return the first CommonTGroupementEntreprise filtered by the id_candidature column
 *
 * @method array findByIdGroupementEntreprise(int $id_groupement_entreprise) Return CommonTGroupementEntreprise objects filtered by the id_groupement_entreprise column
 * @method array findByIdTypeGroupement(int $id_type_groupement) Return CommonTGroupementEntreprise objects filtered by the id_type_groupement column
 * @method array findByIdOffre(int $id_offre) Return CommonTGroupementEntreprise objects filtered by the id_offre column
 * @method array findByDateCreation(string $date_creation) Return CommonTGroupementEntreprise objects filtered by the date_creation column
 * @method array findByIdCandidature(int $id_candidature) Return CommonTGroupementEntreprise objects filtered by the id_candidature column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTGroupementEntrepriseQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTGroupementEntrepriseQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTGroupementEntreprise', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTGroupementEntrepriseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTGroupementEntrepriseQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTGroupementEntrepriseQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTGroupementEntrepriseQuery) {
            return $criteria;
        }
        $query = new CommonTGroupementEntrepriseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTGroupementEntreprise|CommonTGroupementEntreprise[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTGroupementEntreprisePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTGroupementEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdGroupementEntreprise($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTGroupementEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_groupement_entreprise`, `id_type_groupement`, `id_offre`, `date_creation`, `id_candidature` FROM `t_groupement_entreprise` WHERE `id_groupement_entreprise` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTGroupementEntreprise();
            $obj->hydrate($row);
            CommonTGroupementEntreprisePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTGroupementEntreprise|CommonTGroupementEntreprise[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTGroupementEntreprise[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_groupement_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdGroupementEntreprise(1234); // WHERE id_groupement_entreprise = 1234
     * $query->filterByIdGroupementEntreprise(array(12, 34)); // WHERE id_groupement_entreprise IN (12, 34)
     * $query->filterByIdGroupementEntreprise(array('min' => 12)); // WHERE id_groupement_entreprise >= 12
     * $query->filterByIdGroupementEntreprise(array('max' => 12)); // WHERE id_groupement_entreprise <= 12
     * </code>
     *
     * @param     mixed $idGroupementEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdGroupementEntreprise($idGroupementEntreprise = null, $comparison = null)
    {
        if (is_array($idGroupementEntreprise)) {
            $useMinMax = false;
            if (isset($idGroupementEntreprise['min'])) {
                $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $idGroupementEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idGroupementEntreprise['max'])) {
                $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $idGroupementEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $idGroupementEntreprise, $comparison);
    }

    /**
     * Filter the query on the id_type_groupement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeGroupement(1234); // WHERE id_type_groupement = 1234
     * $query->filterByIdTypeGroupement(array(12, 34)); // WHERE id_type_groupement IN (12, 34)
     * $query->filterByIdTypeGroupement(array('min' => 12)); // WHERE id_type_groupement >= 12
     * $query->filterByIdTypeGroupement(array('max' => 12)); // WHERE id_type_groupement <= 12
     * </code>
     *
     * @see       filterByCommonTTypeGroupementEntreprise()
     *
     * @param     mixed $idTypeGroupement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdTypeGroupement($idTypeGroupement = null, $comparison = null)
    {
        if (is_array($idTypeGroupement)) {
            $useMinMax = false;
            if (isset($idTypeGroupement['min'])) {
                $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $idTypeGroupement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeGroupement['max'])) {
                $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $idTypeGroupement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $idTypeGroupement, $comparison);
    }

    /**
     * Filter the query on the id_offre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdOffre(1234); // WHERE id_offre = 1234
     * $query->filterByIdOffre(array(12, 34)); // WHERE id_offre IN (12, 34)
     * $query->filterByIdOffre(array('min' => 12)); // WHERE id_offre >= 12
     * $query->filterByIdOffre(array('max' => 12)); // WHERE id_offre <= 12
     * </code>
     *
     * @see       filterByCommonOffres()
     *
     * @param     mixed $idOffre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdOffre($idOffre = null, $comparison = null)
    {
        if (is_array($idOffre)) {
            $useMinMax = false;
            if (isset($idOffre['min'])) {
                $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_OFFRE, $idOffre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idOffre['max'])) {
                $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_OFFRE, $idOffre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_OFFRE, $idOffre, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTGroupementEntreprisePeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTGroupementEntreprisePeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTGroupementEntreprisePeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the id_candidature column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCandidature(1234); // WHERE id_candidature = 1234
     * $query->filterByIdCandidature(array(12, 34)); // WHERE id_candidature IN (12, 34)
     * $query->filterByIdCandidature(array('min' => 12)); // WHERE id_candidature >= 12
     * $query->filterByIdCandidature(array('max' => 12)); // WHERE id_candidature <= 12
     * </code>
     *
     * @see       filterByCommonTCandidature()
     *
     * @param     mixed $idCandidature The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdCandidature($idCandidature = null, $comparison = null)
    {
        if (is_array($idCandidature)) {
            $useMinMax = false;
            if (isset($idCandidature['min'])) {
                $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_CANDIDATURE, $idCandidature['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCandidature['max'])) {
                $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_CANDIDATURE, $idCandidature['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_CANDIDATURE, $idCandidature, $comparison);
    }

    /**
     * Filter the query by a related CommonOffres object
     *
     * @param   CommonOffres|PropelObjectCollection $commonOffres The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTGroupementEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOffres($commonOffres, $comparison = null)
    {
        if ($commonOffres instanceof CommonOffres) {
            return $this
                ->addUsingAlias(CommonTGroupementEntreprisePeer::ID_OFFRE, $commonOffres->getId(), $comparison);
        } elseif ($commonOffres instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTGroupementEntreprisePeer::ID_OFFRE, $commonOffres->toKeyValue('Id', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonOffres() only accepts arguments of type CommonOffres or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOffres relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonOffres($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOffres');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOffres');
        }

        return $this;
    }

    /**
     * Use the CommonOffres relation CommonOffres object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOffresQuery A secondary query class using the current class as primary query
     */
    public function useCommonOffresQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonOffres($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOffres', '\Application\Propel\Mpe\CommonOffresQuery');
    }

    /**
     * Filter the query by a related CommonTTypeGroupementEntreprise object
     *
     * @param   CommonTTypeGroupementEntreprise|PropelObjectCollection $commonTTypeGroupementEntreprise The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTGroupementEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTTypeGroupementEntreprise($commonTTypeGroupementEntreprise, $comparison = null)
    {
        if ($commonTTypeGroupementEntreprise instanceof CommonTTypeGroupementEntreprise) {
            return $this
                ->addUsingAlias(CommonTGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $commonTTypeGroupementEntreprise->getIdTypeGroupement(), $comparison);
        } elseif ($commonTTypeGroupementEntreprise instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $commonTTypeGroupementEntreprise->toKeyValue('PrimaryKey', 'IdTypeGroupement'), $comparison);
        } else {
            throw new PropelException('filterByCommonTTypeGroupementEntreprise() only accepts arguments of type CommonTTypeGroupementEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTTypeGroupementEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTTypeGroupementEntreprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTTypeGroupementEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTTypeGroupementEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonTTypeGroupementEntreprise relation CommonTTypeGroupementEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTTypeGroupementEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonTTypeGroupementEntrepriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTTypeGroupementEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTTypeGroupementEntreprise', '\Application\Propel\Mpe\CommonTTypeGroupementEntrepriseQuery');
    }

    /**
     * Filter the query by a related CommonTCandidature object
     *
     * @param   CommonTCandidature|PropelObjectCollection $commonTCandidature The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTGroupementEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCandidature($commonTCandidature, $comparison = null)
    {
        if ($commonTCandidature instanceof CommonTCandidature) {
            return $this
                ->addUsingAlias(CommonTGroupementEntreprisePeer::ID_CANDIDATURE, $commonTCandidature->getId(), $comparison);
        } elseif ($commonTCandidature instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTGroupementEntreprisePeer::ID_CANDIDATURE, $commonTCandidature->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTCandidature() only accepts arguments of type CommonTCandidature or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCandidature relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTCandidature($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCandidature');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCandidature');
        }

        return $this;
    }

    /**
     * Use the CommonTCandidature relation CommonTCandidature object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCandidatureQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCandidatureQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTCandidature($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCandidature', '\Application\Propel\Mpe\CommonTCandidatureQuery');
    }

    /**
     * Filter the query by a related CommonTMembreGroupementEntreprise object
     *
     * @param   CommonTMembreGroupementEntreprise|PropelObjectCollection $commonTMembreGroupementEntreprise  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTGroupementEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTMembreGroupementEntreprise($commonTMembreGroupementEntreprise, $comparison = null)
    {
        if ($commonTMembreGroupementEntreprise instanceof CommonTMembreGroupementEntreprise) {
            return $this
                ->addUsingAlias(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $commonTMembreGroupementEntreprise->getIdGroupementEntreprise(), $comparison);
        } elseif ($commonTMembreGroupementEntreprise instanceof PropelObjectCollection) {
            return $this
                ->useCommonTMembreGroupementEntrepriseQuery()
                ->filterByPrimaryKeys($commonTMembreGroupementEntreprise->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTMembreGroupementEntreprise() only accepts arguments of type CommonTMembreGroupementEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTMembreGroupementEntreprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTMembreGroupementEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTMembreGroupementEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonTMembreGroupementEntreprise relation CommonTMembreGroupementEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonTMembreGroupementEntrepriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTMembreGroupementEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTMembreGroupementEntreprise', '\Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTGroupementEntreprise $commonTGroupementEntreprise Object to remove from the list of results
     *
     * @return CommonTGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function prune($commonTGroupementEntreprise = null)
    {
        if ($commonTGroupementEntreprise) {
            $this->addUsingAlias(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE, $commonTGroupementEntreprise->getIdGroupementEntreprise(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
