<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonModificationContrat;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprise;
use Application\Propel\Mpe\Entreprise;

/**
 * Base class that represents a query for the 't_etablissement' table.
 *
 *
 *
 * @method CommonTEtablissementQuery orderByIdEtablissement($order = Criteria::ASC) Order by the id_etablissement column
 * @method CommonTEtablissementQuery orderByIdEntreprise($order = Criteria::ASC) Order by the id_entreprise column
 * @method CommonTEtablissementQuery orderByCodeEtablissement($order = Criteria::ASC) Order by the code_etablissement column
 * @method CommonTEtablissementQuery orderByEstSiege($order = Criteria::ASC) Order by the est_siege column
 * @method CommonTEtablissementQuery orderByAdresse($order = Criteria::ASC) Order by the adresse column
 * @method CommonTEtablissementQuery orderByAdresse2($order = Criteria::ASC) Order by the adresse2 column
 * @method CommonTEtablissementQuery orderByCodePostal($order = Criteria::ASC) Order by the code_postal column
 * @method CommonTEtablissementQuery orderByVille($order = Criteria::ASC) Order by the ville column
 * @method CommonTEtablissementQuery orderByPays($order = Criteria::ASC) Order by the pays column
 * @method CommonTEtablissementQuery orderBySaisieManuelle($order = Criteria::ASC) Order by the saisie_manuelle column
 * @method CommonTEtablissementQuery orderByIdInitial($order = Criteria::ASC) Order by the id_initial column
 * @method CommonTEtablissementQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTEtablissementQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonTEtablissementQuery orderByDateSuppression($order = Criteria::ASC) Order by the date_suppression column
 * @method CommonTEtablissementQuery orderByStatutActif($order = Criteria::ASC) Order by the statut_actif column
 * @method CommonTEtablissementQuery orderByInscritAnnuaireDefense($order = Criteria::ASC) Order by the inscrit_annuaire_defense column
 * @method CommonTEtablissementQuery orderByLong($order = Criteria::ASC) Order by the long column
 * @method CommonTEtablissementQuery orderByLat($order = Criteria::ASC) Order by the lat column
 * @method CommonTEtablissementQuery orderByMajLongLat($order = Criteria::ASC) Order by the maj_long_lat column
 * @method CommonTEtablissementQuery orderByTvaIntracommunautaire($order = Criteria::ASC) Order by the tva_intracommunautaire column
 * @method CommonTEtablissementQuery orderByEtatAdministratif($order = Criteria::ASC) Order by the etat_administratif column
 * @method CommonTEtablissementQuery orderByDateFermeture($order = Criteria::ASC) Order by the date_fermeture column
 * @method CommonTEtablissementQuery orderByIdExterne($order = Criteria::ASC) Order by the id_externe column
 *
 * @method CommonTEtablissementQuery groupByIdEtablissement() Group by the id_etablissement column
 * @method CommonTEtablissementQuery groupByIdEntreprise() Group by the id_entreprise column
 * @method CommonTEtablissementQuery groupByCodeEtablissement() Group by the code_etablissement column
 * @method CommonTEtablissementQuery groupByEstSiege() Group by the est_siege column
 * @method CommonTEtablissementQuery groupByAdresse() Group by the adresse column
 * @method CommonTEtablissementQuery groupByAdresse2() Group by the adresse2 column
 * @method CommonTEtablissementQuery groupByCodePostal() Group by the code_postal column
 * @method CommonTEtablissementQuery groupByVille() Group by the ville column
 * @method CommonTEtablissementQuery groupByPays() Group by the pays column
 * @method CommonTEtablissementQuery groupBySaisieManuelle() Group by the saisie_manuelle column
 * @method CommonTEtablissementQuery groupByIdInitial() Group by the id_initial column
 * @method CommonTEtablissementQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTEtablissementQuery groupByDateModification() Group by the date_modification column
 * @method CommonTEtablissementQuery groupByDateSuppression() Group by the date_suppression column
 * @method CommonTEtablissementQuery groupByStatutActif() Group by the statut_actif column
 * @method CommonTEtablissementQuery groupByInscritAnnuaireDefense() Group by the inscrit_annuaire_defense column
 * @method CommonTEtablissementQuery groupByLong() Group by the long column
 * @method CommonTEtablissementQuery groupByLat() Group by the lat column
 * @method CommonTEtablissementQuery groupByMajLongLat() Group by the maj_long_lat column
 * @method CommonTEtablissementQuery groupByTvaIntracommunautaire() Group by the tva_intracommunautaire column
 * @method CommonTEtablissementQuery groupByEtatAdministratif() Group by the etat_administratif column
 * @method CommonTEtablissementQuery groupByDateFermeture() Group by the date_fermeture column
 * @method CommonTEtablissementQuery groupByIdExterne() Group by the id_externe column
 *
 * @method CommonTEtablissementQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTEtablissementQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTEtablissementQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTEtablissementQuery leftJoinEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entreprise relation
 * @method CommonTEtablissementQuery rightJoinEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entreprise relation
 * @method CommonTEtablissementQuery innerJoinEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the Entreprise relation
 *
 * @method CommonTEtablissementQuery leftJoinCommonInscritRelatedByIdEtablissement($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonInscritRelatedByIdEtablissement relation
 * @method CommonTEtablissementQuery rightJoinCommonInscritRelatedByIdEtablissement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonInscritRelatedByIdEtablissement relation
 * @method CommonTEtablissementQuery innerJoinCommonInscritRelatedByIdEtablissement($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonInscritRelatedByIdEtablissement relation
 *
 * @method CommonTEtablissementQuery leftJoinCommonModificationContrat($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonModificationContrat relation
 * @method CommonTEtablissementQuery rightJoinCommonModificationContrat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonModificationContrat relation
 * @method CommonTEtablissementQuery innerJoinCommonModificationContrat($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonModificationContrat relation
 *
 * @method CommonTEtablissementQuery leftJoinCommonTMembreGroupementEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
 * @method CommonTEtablissementQuery rightJoinCommonTMembreGroupementEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
 * @method CommonTEtablissementQuery innerJoinCommonTMembreGroupementEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
 *
 * @method CommonTEtablissement findOne(PropelPDO $con = null) Return the first CommonTEtablissement matching the query
 * @method CommonTEtablissement findOneOrCreate(PropelPDO $con = null) Return the first CommonTEtablissement matching the query, or a new CommonTEtablissement object populated from the query conditions when no match is found
 *
 * @method CommonTEtablissement findOneByIdEntreprise(int $id_entreprise) Return the first CommonTEtablissement filtered by the id_entreprise column
 * @method CommonTEtablissement findOneByCodeEtablissement(string $code_etablissement) Return the first CommonTEtablissement filtered by the code_etablissement column
 * @method CommonTEtablissement findOneByEstSiege(string $est_siege) Return the first CommonTEtablissement filtered by the est_siege column
 * @method CommonTEtablissement findOneByAdresse(string $adresse) Return the first CommonTEtablissement filtered by the adresse column
 * @method CommonTEtablissement findOneByAdresse2(string $adresse2) Return the first CommonTEtablissement filtered by the adresse2 column
 * @method CommonTEtablissement findOneByCodePostal(string $code_postal) Return the first CommonTEtablissement filtered by the code_postal column
 * @method CommonTEtablissement findOneByVille(string $ville) Return the first CommonTEtablissement filtered by the ville column
 * @method CommonTEtablissement findOneByPays(string $pays) Return the first CommonTEtablissement filtered by the pays column
 * @method CommonTEtablissement findOneBySaisieManuelle(string $saisie_manuelle) Return the first CommonTEtablissement filtered by the saisie_manuelle column
 * @method CommonTEtablissement findOneByIdInitial(int $id_initial) Return the first CommonTEtablissement filtered by the id_initial column
 * @method CommonTEtablissement findOneByDateCreation(string $date_creation) Return the first CommonTEtablissement filtered by the date_creation column
 * @method CommonTEtablissement findOneByDateModification(string $date_modification) Return the first CommonTEtablissement filtered by the date_modification column
 * @method CommonTEtablissement findOneByDateSuppression(string $date_suppression) Return the first CommonTEtablissement filtered by the date_suppression column
 * @method CommonTEtablissement findOneByStatutActif(string $statut_actif) Return the first CommonTEtablissement filtered by the statut_actif column
 * @method CommonTEtablissement findOneByInscritAnnuaireDefense(string $inscrit_annuaire_defense) Return the first CommonTEtablissement filtered by the inscrit_annuaire_defense column
 * @method CommonTEtablissement findOneByLong(double $long) Return the first CommonTEtablissement filtered by the long column
 * @method CommonTEtablissement findOneByLat(double $lat) Return the first CommonTEtablissement filtered by the lat column
 * @method CommonTEtablissement findOneByMajLongLat(string $maj_long_lat) Return the first CommonTEtablissement filtered by the maj_long_lat column
 * @method CommonTEtablissement findOneByTvaIntracommunautaire(string $tva_intracommunautaire) Return the first CommonTEtablissement filtered by the tva_intracommunautaire column
 * @method CommonTEtablissement findOneByEtatAdministratif(string $etat_administratif) Return the first CommonTEtablissement filtered by the etat_administratif column
 * @method CommonTEtablissement findOneByDateFermeture(string $date_fermeture) Return the first CommonTEtablissement filtered by the date_fermeture column
 * @method CommonTEtablissement findOneByIdExterne(string $id_externe) Return the first CommonTEtablissement filtered by the id_externe column
 *
 * @method array findByIdEtablissement(int $id_etablissement) Return CommonTEtablissement objects filtered by the id_etablissement column
 * @method array findByIdEntreprise(int $id_entreprise) Return CommonTEtablissement objects filtered by the id_entreprise column
 * @method array findByCodeEtablissement(string $code_etablissement) Return CommonTEtablissement objects filtered by the code_etablissement column
 * @method array findByEstSiege(string $est_siege) Return CommonTEtablissement objects filtered by the est_siege column
 * @method array findByAdresse(string $adresse) Return CommonTEtablissement objects filtered by the adresse column
 * @method array findByAdresse2(string $adresse2) Return CommonTEtablissement objects filtered by the adresse2 column
 * @method array findByCodePostal(string $code_postal) Return CommonTEtablissement objects filtered by the code_postal column
 * @method array findByVille(string $ville) Return CommonTEtablissement objects filtered by the ville column
 * @method array findByPays(string $pays) Return CommonTEtablissement objects filtered by the pays column
 * @method array findBySaisieManuelle(string $saisie_manuelle) Return CommonTEtablissement objects filtered by the saisie_manuelle column
 * @method array findByIdInitial(int $id_initial) Return CommonTEtablissement objects filtered by the id_initial column
 * @method array findByDateCreation(string $date_creation) Return CommonTEtablissement objects filtered by the date_creation column
 * @method array findByDateModification(string $date_modification) Return CommonTEtablissement objects filtered by the date_modification column
 * @method array findByDateSuppression(string $date_suppression) Return CommonTEtablissement objects filtered by the date_suppression column
 * @method array findByStatutActif(string $statut_actif) Return CommonTEtablissement objects filtered by the statut_actif column
 * @method array findByInscritAnnuaireDefense(string $inscrit_annuaire_defense) Return CommonTEtablissement objects filtered by the inscrit_annuaire_defense column
 * @method array findByLong(double $long) Return CommonTEtablissement objects filtered by the long column
 * @method array findByLat(double $lat) Return CommonTEtablissement objects filtered by the lat column
 * @method array findByMajLongLat(string $maj_long_lat) Return CommonTEtablissement objects filtered by the maj_long_lat column
 * @method array findByTvaIntracommunautaire(string $tva_intracommunautaire) Return CommonTEtablissement objects filtered by the tva_intracommunautaire column
 * @method array findByEtatAdministratif(string $etat_administratif) Return CommonTEtablissement objects filtered by the etat_administratif column
 * @method array findByDateFermeture(string $date_fermeture) Return CommonTEtablissement objects filtered by the date_fermeture column
 * @method array findByIdExterne(string $id_externe) Return CommonTEtablissement objects filtered by the id_externe column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTEtablissementQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTEtablissementQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTEtablissement', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTEtablissementQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTEtablissementQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTEtablissementQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTEtablissementQuery) {
            return $criteria;
        }
        $query = new CommonTEtablissementQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTEtablissement|CommonTEtablissement[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTEtablissementPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTEtablissement A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdEtablissement($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTEtablissement A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_etablissement`, `id_entreprise`, `code_etablissement`, `est_siege`, `adresse`, `adresse2`, `code_postal`, `ville`, `pays`, `saisie_manuelle`, `id_initial`, `date_creation`, `date_modification`, `date_suppression`, `statut_actif`, `inscrit_annuaire_defense`, `long`, `lat`, `maj_long_lat`, `tva_intracommunautaire`, `etat_administratif`, `date_fermeture`, `id_externe` FROM `t_etablissement` WHERE `id_etablissement` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTEtablissement();
            $obj->hydrate($row);
            CommonTEtablissementPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTEtablissement|CommonTEtablissement[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTEtablissement[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTEtablissementPeer::ID_ETABLISSEMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTEtablissementPeer::ID_ETABLISSEMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_etablissement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtablissement(1234); // WHERE id_etablissement = 1234
     * $query->filterByIdEtablissement(array(12, 34)); // WHERE id_etablissement IN (12, 34)
     * $query->filterByIdEtablissement(array('min' => 12)); // WHERE id_etablissement >= 12
     * $query->filterByIdEtablissement(array('max' => 12)); // WHERE id_etablissement <= 12
     * </code>
     *
     * @param     mixed $idEtablissement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByIdEtablissement($idEtablissement = null, $comparison = null)
    {
        if (is_array($idEtablissement)) {
            $useMinMax = false;
            if (isset($idEtablissement['min'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::ID_ETABLISSEMENT, $idEtablissement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtablissement['max'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::ID_ETABLISSEMENT, $idEtablissement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::ID_ETABLISSEMENT, $idEtablissement, $comparison);
    }

    /**
     * Filter the query on the id_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntreprise(1234); // WHERE id_entreprise = 1234
     * $query->filterByIdEntreprise(array(12, 34)); // WHERE id_entreprise IN (12, 34)
     * $query->filterByIdEntreprise(array('min' => 12)); // WHERE id_entreprise >= 12
     * $query->filterByIdEntreprise(array('max' => 12)); // WHERE id_entreprise <= 12
     * </code>
     *
     * @see       filterByEntreprise()
     *
     * @param     mixed $idEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByIdEntreprise($idEntreprise = null, $comparison = null)
    {
        if (is_array($idEntreprise)) {
            $useMinMax = false;
            if (isset($idEntreprise['min'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::ID_ENTREPRISE, $idEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntreprise['max'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::ID_ENTREPRISE, $idEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::ID_ENTREPRISE, $idEntreprise, $comparison);
    }

    /**
     * Filter the query on the code_etablissement column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeEtablissement('fooValue');   // WHERE code_etablissement = 'fooValue'
     * $query->filterByCodeEtablissement('%fooValue%'); // WHERE code_etablissement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codeEtablissement The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByCodeEtablissement($codeEtablissement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codeEtablissement)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codeEtablissement)) {
                $codeEtablissement = str_replace('*', '%', $codeEtablissement);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::CODE_ETABLISSEMENT, $codeEtablissement, $comparison);
    }

    /**
     * Filter the query on the est_siege column
     *
     * Example usage:
     * <code>
     * $query->filterByEstSiege('fooValue');   // WHERE est_siege = 'fooValue'
     * $query->filterByEstSiege('%fooValue%'); // WHERE est_siege LIKE '%fooValue%'
     * </code>
     *
     * @param     string $estSiege The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByEstSiege($estSiege = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($estSiege)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $estSiege)) {
                $estSiege = str_replace('*', '%', $estSiege);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::EST_SIEGE, $estSiege, $comparison);
    }

    /**
     * Filter the query on the adresse column
     *
     * Example usage:
     * <code>
     * $query->filterByAdresse('fooValue');   // WHERE adresse = 'fooValue'
     * $query->filterByAdresse('%fooValue%'); // WHERE adresse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adresse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByAdresse($adresse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adresse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $adresse)) {
                $adresse = str_replace('*', '%', $adresse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::ADRESSE, $adresse, $comparison);
    }

    /**
     * Filter the query on the adresse2 column
     *
     * Example usage:
     * <code>
     * $query->filterByAdresse2('fooValue');   // WHERE adresse2 = 'fooValue'
     * $query->filterByAdresse2('%fooValue%'); // WHERE adresse2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adresse2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByAdresse2($adresse2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adresse2)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $adresse2)) {
                $adresse2 = str_replace('*', '%', $adresse2);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::ADRESSE2, $adresse2, $comparison);
    }

    /**
     * Filter the query on the code_postal column
     *
     * Example usage:
     * <code>
     * $query->filterByCodePostal('fooValue');   // WHERE code_postal = 'fooValue'
     * $query->filterByCodePostal('%fooValue%'); // WHERE code_postal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codePostal The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByCodePostal($codePostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codePostal)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codePostal)) {
                $codePostal = str_replace('*', '%', $codePostal);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::CODE_POSTAL, $codePostal, $comparison);
    }

    /**
     * Filter the query on the ville column
     *
     * Example usage:
     * <code>
     * $query->filterByVille('fooValue');   // WHERE ville = 'fooValue'
     * $query->filterByVille('%fooValue%'); // WHERE ville LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ville The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByVille($ville = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ville)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ville)) {
                $ville = str_replace('*', '%', $ville);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::VILLE, $ville, $comparison);
    }

    /**
     * Filter the query on the pays column
     *
     * Example usage:
     * <code>
     * $query->filterByPays('fooValue');   // WHERE pays = 'fooValue'
     * $query->filterByPays('%fooValue%'); // WHERE pays LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pays The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByPays($pays = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pays)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pays)) {
                $pays = str_replace('*', '%', $pays);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::PAYS, $pays, $comparison);
    }

    /**
     * Filter the query on the saisie_manuelle column
     *
     * Example usage:
     * <code>
     * $query->filterBySaisieManuelle('fooValue');   // WHERE saisie_manuelle = 'fooValue'
     * $query->filterBySaisieManuelle('%fooValue%'); // WHERE saisie_manuelle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $saisieManuelle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterBySaisieManuelle($saisieManuelle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($saisieManuelle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $saisieManuelle)) {
                $saisieManuelle = str_replace('*', '%', $saisieManuelle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::SAISIE_MANUELLE, $saisieManuelle, $comparison);
    }

    /**
     * Filter the query on the id_initial column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInitial(1234); // WHERE id_initial = 1234
     * $query->filterByIdInitial(array(12, 34)); // WHERE id_initial IN (12, 34)
     * $query->filterByIdInitial(array('min' => 12)); // WHERE id_initial >= 12
     * $query->filterByIdInitial(array('max' => 12)); // WHERE id_initial <= 12
     * </code>
     *
     * @param     mixed $idInitial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByIdInitial($idInitial = null, $comparison = null)
    {
        if (is_array($idInitial)) {
            $useMinMax = false;
            if (isset($idInitial['min'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::ID_INITIAL, $idInitial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInitial['max'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::ID_INITIAL, $idInitial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::ID_INITIAL, $idInitial, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('2011-03-14'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification('now'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification(array('max' => 'yesterday')); // WHERE date_modification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (is_array($dateModification)) {
            $useMinMax = false;
            if (isset($dateModification['min'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::DATE_MODIFICATION, $dateModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModification['max'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::DATE_MODIFICATION, $dateModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the date_suppression column
     *
     * Example usage:
     * <code>
     * $query->filterByDateSuppression('2011-03-14'); // WHERE date_suppression = '2011-03-14'
     * $query->filterByDateSuppression('now'); // WHERE date_suppression = '2011-03-14'
     * $query->filterByDateSuppression(array('max' => 'yesterday')); // WHERE date_suppression > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateSuppression The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByDateSuppression($dateSuppression = null, $comparison = null)
    {
        if (is_array($dateSuppression)) {
            $useMinMax = false;
            if (isset($dateSuppression['min'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::DATE_SUPPRESSION, $dateSuppression['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateSuppression['max'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::DATE_SUPPRESSION, $dateSuppression['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::DATE_SUPPRESSION, $dateSuppression, $comparison);
    }

    /**
     * Filter the query on the statut_actif column
     *
     * Example usage:
     * <code>
     * $query->filterByStatutActif('fooValue');   // WHERE statut_actif = 'fooValue'
     * $query->filterByStatutActif('%fooValue%'); // WHERE statut_actif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statutActif The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByStatutActif($statutActif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statutActif)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $statutActif)) {
                $statutActif = str_replace('*', '%', $statutActif);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::STATUT_ACTIF, $statutActif, $comparison);
    }

    /**
     * Filter the query on the inscrit_annuaire_defense column
     *
     * Example usage:
     * <code>
     * $query->filterByInscritAnnuaireDefense('fooValue');   // WHERE inscrit_annuaire_defense = 'fooValue'
     * $query->filterByInscritAnnuaireDefense('%fooValue%'); // WHERE inscrit_annuaire_defense LIKE '%fooValue%'
     * </code>
     *
     * @param     string $inscritAnnuaireDefense The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByInscritAnnuaireDefense($inscritAnnuaireDefense = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($inscritAnnuaireDefense)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $inscritAnnuaireDefense)) {
                $inscritAnnuaireDefense = str_replace('*', '%', $inscritAnnuaireDefense);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE, $inscritAnnuaireDefense, $comparison);
    }

    /**
     * Filter the query on the long column
     *
     * Example usage:
     * <code>
     * $query->filterByLong(1234); // WHERE long = 1234
     * $query->filterByLong(array(12, 34)); // WHERE long IN (12, 34)
     * $query->filterByLong(array('min' => 12)); // WHERE long >= 12
     * $query->filterByLong(array('max' => 12)); // WHERE long <= 12
     * </code>
     *
     * @param     mixed $long The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByLong($long = null, $comparison = null)
    {
        if (is_array($long)) {
            $useMinMax = false;
            if (isset($long['min'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::LONG, $long['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($long['max'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::LONG, $long['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::LONG, $long, $comparison);
    }

    /**
     * Filter the query on the lat column
     *
     * Example usage:
     * <code>
     * $query->filterByLat(1234); // WHERE lat = 1234
     * $query->filterByLat(array(12, 34)); // WHERE lat IN (12, 34)
     * $query->filterByLat(array('min' => 12)); // WHERE lat >= 12
     * $query->filterByLat(array('max' => 12)); // WHERE lat <= 12
     * </code>
     *
     * @param     mixed $lat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByLat($lat = null, $comparison = null)
    {
        if (is_array($lat)) {
            $useMinMax = false;
            if (isset($lat['min'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::LAT, $lat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lat['max'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::LAT, $lat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::LAT, $lat, $comparison);
    }

    /**
     * Filter the query on the maj_long_lat column
     *
     * Example usage:
     * <code>
     * $query->filterByMajLongLat('2011-03-14'); // WHERE maj_long_lat = '2011-03-14'
     * $query->filterByMajLongLat('now'); // WHERE maj_long_lat = '2011-03-14'
     * $query->filterByMajLongLat(array('max' => 'yesterday')); // WHERE maj_long_lat > '2011-03-13'
     * </code>
     *
     * @param     mixed $majLongLat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByMajLongLat($majLongLat = null, $comparison = null)
    {
        if (is_array($majLongLat)) {
            $useMinMax = false;
            if (isset($majLongLat['min'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::MAJ_LONG_LAT, $majLongLat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($majLongLat['max'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::MAJ_LONG_LAT, $majLongLat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::MAJ_LONG_LAT, $majLongLat, $comparison);
    }

    /**
     * Filter the query on the tva_intracommunautaire column
     *
     * Example usage:
     * <code>
     * $query->filterByTvaIntracommunautaire('fooValue');   // WHERE tva_intracommunautaire = 'fooValue'
     * $query->filterByTvaIntracommunautaire('%fooValue%'); // WHERE tva_intracommunautaire LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tvaIntracommunautaire The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByTvaIntracommunautaire($tvaIntracommunautaire = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tvaIntracommunautaire)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tvaIntracommunautaire)) {
                $tvaIntracommunautaire = str_replace('*', '%', $tvaIntracommunautaire);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::TVA_INTRACOMMUNAUTAIRE, $tvaIntracommunautaire, $comparison);
    }

    /**
     * Filter the query on the etat_administratif column
     *
     * Example usage:
     * <code>
     * $query->filterByEtatAdministratif('fooValue');   // WHERE etat_administratif = 'fooValue'
     * $query->filterByEtatAdministratif('%fooValue%'); // WHERE etat_administratif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $etatAdministratif The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByEtatAdministratif($etatAdministratif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($etatAdministratif)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $etatAdministratif)) {
                $etatAdministratif = str_replace('*', '%', $etatAdministratif);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::ETAT_ADMINISTRATIF, $etatAdministratif, $comparison);
    }

    /**
     * Filter the query on the date_fermeture column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFermeture('2011-03-14'); // WHERE date_fermeture = '2011-03-14'
     * $query->filterByDateFermeture('now'); // WHERE date_fermeture = '2011-03-14'
     * $query->filterByDateFermeture(array('max' => 'yesterday')); // WHERE date_fermeture > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateFermeture The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByDateFermeture($dateFermeture = null, $comparison = null)
    {
        if (is_array($dateFermeture)) {
            $useMinMax = false;
            if (isset($dateFermeture['min'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::DATE_FERMETURE, $dateFermeture['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateFermeture['max'])) {
                $this->addUsingAlias(CommonTEtablissementPeer::DATE_FERMETURE, $dateFermeture['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::DATE_FERMETURE, $dateFermeture, $comparison);
    }

    /**
     * Filter the query on the id_externe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdExterne('fooValue');   // WHERE id_externe = 'fooValue'
     * $query->filterByIdExterne('%fooValue%'); // WHERE id_externe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idExterne The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function filterByIdExterne($idExterne = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idExterne)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idExterne)) {
                $idExterne = str_replace('*', '%', $idExterne);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEtablissementPeer::ID_EXTERNE, $idExterne, $comparison);
    }

    /**
     * Filter the query by a related Entreprise object
     *
     * @param   Entreprise|PropelObjectCollection $entreprise The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTEtablissementQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByEntreprise($entreprise, $comparison = null)
    {
        if ($entreprise instanceof Entreprise) {
            return $this
                ->addUsingAlias(CommonTEtablissementPeer::ID_ENTREPRISE, $entreprise->getId(), $comparison);
        } elseif ($entreprise instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTEtablissementPeer::ID_ENTREPRISE, $entreprise->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEntreprise() only accepts arguments of type Entreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function joinEntreprise($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entreprise');
        }

        return $this;
    }

    /**
     * Use the Entreprise relation Entreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\EntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useEntrepriseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entreprise', '\Application\Propel\Mpe\EntrepriseQuery');
    }




    /**
     * Filter the query by a related CommonInscrit object
     *
     * @param   CommonInscrit|PropelObjectCollection $commonInscrit  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTEtablissementQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonInscritRelatedByIdEtablissement($commonInscrit, $comparison = null)
    {
        if ($commonInscrit instanceof CommonInscrit) {
            return $this
                ->addUsingAlias(CommonTEtablissementPeer::ID_ETABLISSEMENT, $commonInscrit->getIdEtablissement(), $comparison);
        } elseif ($commonInscrit instanceof PropelObjectCollection) {
            return $this
                ->useCommonInscritRelatedByIdEtablissementQuery()
                ->filterByPrimaryKeys($commonInscrit->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonInscritRelatedByIdEtablissement() only accepts arguments of type CommonInscrit or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonInscritRelatedByIdEtablissement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function joinCommonInscritRelatedByIdEtablissement($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonInscritRelatedByIdEtablissement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonInscritRelatedByIdEtablissement');
        }

        return $this;
    }

    /**
     * Use the CommonInscritRelatedByIdEtablissement relation CommonInscrit object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonInscritQuery A secondary query class using the current class as primary query
     */
    public function useCommonInscritRelatedByIdEtablissementQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonInscritRelatedByIdEtablissement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonInscritRelatedByIdEtablissement', '\Application\Propel\Mpe\CommonInscritQuery');
    }

    /**
     * Filter the query by a related CommonModificationContrat object
     *
     * @param   CommonModificationContrat|PropelObjectCollection $commonModificationContrat  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTEtablissementQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonModificationContrat($commonModificationContrat, $comparison = null)
    {
        if ($commonModificationContrat instanceof CommonModificationContrat) {
            return $this
                ->addUsingAlias(CommonTEtablissementPeer::ID_ETABLISSEMENT, $commonModificationContrat->getIdEtablissement(), $comparison);
        } elseif ($commonModificationContrat instanceof PropelObjectCollection) {
            return $this
                ->useCommonModificationContratQuery()
                ->filterByPrimaryKeys($commonModificationContrat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonModificationContrat() only accepts arguments of type CommonModificationContrat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonModificationContrat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function joinCommonModificationContrat($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonModificationContrat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonModificationContrat');
        }

        return $this;
    }

    /**
     * Use the CommonModificationContrat relation CommonModificationContrat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonModificationContratQuery A secondary query class using the current class as primary query
     */
    public function useCommonModificationContratQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonModificationContrat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonModificationContrat', '\Application\Propel\Mpe\CommonModificationContratQuery');
    }

    /**
     * Filter the query by a related CommonTMembreGroupementEntreprise object
     *
     * @param   CommonTMembreGroupementEntreprise|PropelObjectCollection $commonTMembreGroupementEntreprise  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTEtablissementQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTMembreGroupementEntreprise($commonTMembreGroupementEntreprise, $comparison = null)
    {
        if ($commonTMembreGroupementEntreprise instanceof CommonTMembreGroupementEntreprise) {
            return $this
                ->addUsingAlias(CommonTEtablissementPeer::ID_ETABLISSEMENT, $commonTMembreGroupementEntreprise->getIdEtablissement(), $comparison);
        } elseif ($commonTMembreGroupementEntreprise instanceof PropelObjectCollection) {
            return $this
                ->useCommonTMembreGroupementEntrepriseQuery()
                ->filterByPrimaryKeys($commonTMembreGroupementEntreprise->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTMembreGroupementEntreprise() only accepts arguments of type CommonTMembreGroupementEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function joinCommonTMembreGroupementEntreprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTMembreGroupementEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTMembreGroupementEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonTMembreGroupementEntreprise relation CommonTMembreGroupementEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonTMembreGroupementEntrepriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTMembreGroupementEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTMembreGroupementEntreprise', '\Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTEtablissement $commonTEtablissement Object to remove from the list of results
     *
     * @return CommonTEtablissementQuery The current query, for fluid interface
     */
    public function prune($commonTEtablissement = null)
    {
        if ($commonTEtablissement) {
            $this->addUsingAlias(CommonTEtablissementPeer::ID_ETABLISSEMENT, $commonTEtablissement->getIdEtablissement(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
