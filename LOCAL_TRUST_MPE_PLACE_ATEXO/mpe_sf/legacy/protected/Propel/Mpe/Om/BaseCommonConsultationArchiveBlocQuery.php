<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationArchive;
use Application\Propel\Mpe\CommonConsultationArchiveBloc;
use Application\Propel\Mpe\CommonConsultationArchiveBlocPeer;
use Application\Propel\Mpe\CommonConsultationArchiveBlocQuery;

/**
 * Base class that represents a query for the 'consultation_archive_bloc' table.
 *
 *
 *
 * @method CommonConsultationArchiveBlocQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonConsultationArchiveBlocQuery orderByConsultationArchiveId($order = Criteria::ASC) Order by the consultation_archive_id column
 * @method CommonConsultationArchiveBlocQuery orderByDocId($order = Criteria::ASC) Order by the doc_id column
 * @method CommonConsultationArchiveBlocQuery orderByCheminFichier($order = Criteria::ASC) Order by the chemin_fichier column
 * @method CommonConsultationArchiveBlocQuery orderByNumeroBloc($order = Criteria::ASC) Order by the numero_bloc column
 * @method CommonConsultationArchiveBlocQuery orderByPoidsBloc($order = Criteria::ASC) Order by the poids_bloc column
 * @method CommonConsultationArchiveBlocQuery orderByDateEnvoiDebut($order = Criteria::ASC) Order by the date_envoi_debut column
 * @method CommonConsultationArchiveBlocQuery orderByDateEnvoiFin($order = Criteria::ASC) Order by the date_envoi_fin column
 * @method CommonConsultationArchiveBlocQuery orderByStatusTransmission($order = Criteria::ASC) Order by the status_transmission column
 * @method CommonConsultationArchiveBlocQuery orderByErreur($order = Criteria::ASC) Order by the erreur column
 * @method CommonConsultationArchiveBlocQuery orderByCompId($order = Criteria::ASC) Order by the comp_id column
 *
 * @method CommonConsultationArchiveBlocQuery groupById() Group by the id column
 * @method CommonConsultationArchiveBlocQuery groupByConsultationArchiveId() Group by the consultation_archive_id column
 * @method CommonConsultationArchiveBlocQuery groupByDocId() Group by the doc_id column
 * @method CommonConsultationArchiveBlocQuery groupByCheminFichier() Group by the chemin_fichier column
 * @method CommonConsultationArchiveBlocQuery groupByNumeroBloc() Group by the numero_bloc column
 * @method CommonConsultationArchiveBlocQuery groupByPoidsBloc() Group by the poids_bloc column
 * @method CommonConsultationArchiveBlocQuery groupByDateEnvoiDebut() Group by the date_envoi_debut column
 * @method CommonConsultationArchiveBlocQuery groupByDateEnvoiFin() Group by the date_envoi_fin column
 * @method CommonConsultationArchiveBlocQuery groupByStatusTransmission() Group by the status_transmission column
 * @method CommonConsultationArchiveBlocQuery groupByErreur() Group by the erreur column
 * @method CommonConsultationArchiveBlocQuery groupByCompId() Group by the comp_id column
 *
 * @method CommonConsultationArchiveBlocQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonConsultationArchiveBlocQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonConsultationArchiveBlocQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonConsultationArchiveBlocQuery leftJoinCommonConsultationArchive($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultationArchive relation
 * @method CommonConsultationArchiveBlocQuery rightJoinCommonConsultationArchive($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultationArchive relation
 * @method CommonConsultationArchiveBlocQuery innerJoinCommonConsultationArchive($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultationArchive relation
 *
 * @method CommonConsultationArchiveBloc findOne(PropelPDO $con = null) Return the first CommonConsultationArchiveBloc matching the query
 * @method CommonConsultationArchiveBloc findOneOrCreate(PropelPDO $con = null) Return the first CommonConsultationArchiveBloc matching the query, or a new CommonConsultationArchiveBloc object populated from the query conditions when no match is found
 *
 * @method CommonConsultationArchiveBloc findOneByConsultationArchiveId(int $consultation_archive_id) Return the first CommonConsultationArchiveBloc filtered by the consultation_archive_id column
 * @method CommonConsultationArchiveBloc findOneByDocId(string $doc_id) Return the first CommonConsultationArchiveBloc filtered by the doc_id column
 * @method CommonConsultationArchiveBloc findOneByCheminFichier(string $chemin_fichier) Return the first CommonConsultationArchiveBloc filtered by the chemin_fichier column
 * @method CommonConsultationArchiveBloc findOneByNumeroBloc(int $numero_bloc) Return the first CommonConsultationArchiveBloc filtered by the numero_bloc column
 * @method CommonConsultationArchiveBloc findOneByPoidsBloc(int $poids_bloc) Return the first CommonConsultationArchiveBloc filtered by the poids_bloc column
 * @method CommonConsultationArchiveBloc findOneByDateEnvoiDebut(string $date_envoi_debut) Return the first CommonConsultationArchiveBloc filtered by the date_envoi_debut column
 * @method CommonConsultationArchiveBloc findOneByDateEnvoiFin(string $date_envoi_fin) Return the first CommonConsultationArchiveBloc filtered by the date_envoi_fin column
 * @method CommonConsultationArchiveBloc findOneByStatusTransmission(boolean $status_transmission) Return the first CommonConsultationArchiveBloc filtered by the status_transmission column
 * @method CommonConsultationArchiveBloc findOneByErreur(string $erreur) Return the first CommonConsultationArchiveBloc filtered by the erreur column
 * @method CommonConsultationArchiveBloc findOneByCompId(string $comp_id) Return the first CommonConsultationArchiveBloc filtered by the comp_id column
 *
 * @method array findById(int $id) Return CommonConsultationArchiveBloc objects filtered by the id column
 * @method array findByConsultationArchiveId(int $consultation_archive_id) Return CommonConsultationArchiveBloc objects filtered by the consultation_archive_id column
 * @method array findByDocId(string $doc_id) Return CommonConsultationArchiveBloc objects filtered by the doc_id column
 * @method array findByCheminFichier(string $chemin_fichier) Return CommonConsultationArchiveBloc objects filtered by the chemin_fichier column
 * @method array findByNumeroBloc(int $numero_bloc) Return CommonConsultationArchiveBloc objects filtered by the numero_bloc column
 * @method array findByPoidsBloc(int $poids_bloc) Return CommonConsultationArchiveBloc objects filtered by the poids_bloc column
 * @method array findByDateEnvoiDebut(string $date_envoi_debut) Return CommonConsultationArchiveBloc objects filtered by the date_envoi_debut column
 * @method array findByDateEnvoiFin(string $date_envoi_fin) Return CommonConsultationArchiveBloc objects filtered by the date_envoi_fin column
 * @method array findByStatusTransmission(boolean $status_transmission) Return CommonConsultationArchiveBloc objects filtered by the status_transmission column
 * @method array findByErreur(string $erreur) Return CommonConsultationArchiveBloc objects filtered by the erreur column
 * @method array findByCompId(string $comp_id) Return CommonConsultationArchiveBloc objects filtered by the comp_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationArchiveBlocQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonConsultationArchiveBlocQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonConsultationArchiveBloc', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonConsultationArchiveBlocQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonConsultationArchiveBlocQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonConsultationArchiveBlocQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonConsultationArchiveBlocQuery) {
            return $criteria;
        }
        $query = new CommonConsultationArchiveBlocQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonConsultationArchiveBloc|CommonConsultationArchiveBloc[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonConsultationArchiveBlocPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationArchiveBlocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationArchiveBloc A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationArchiveBloc A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `consultation_archive_id`, `doc_id`, `chemin_fichier`, `numero_bloc`, `poids_bloc`, `date_envoi_debut`, `date_envoi_fin`, `status_transmission`, `erreur`, `comp_id` FROM `consultation_archive_bloc` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonConsultationArchiveBloc();
            $obj->hydrate($row);
            CommonConsultationArchiveBlocPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonConsultationArchiveBloc|CommonConsultationArchiveBloc[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonConsultationArchiveBloc[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the consultation_archive_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationArchiveId(1234); // WHERE consultation_archive_id = 1234
     * $query->filterByConsultationArchiveId(array(12, 34)); // WHERE consultation_archive_id IN (12, 34)
     * $query->filterByConsultationArchiveId(array('min' => 12)); // WHERE consultation_archive_id >= 12
     * $query->filterByConsultationArchiveId(array('max' => 12)); // WHERE consultation_archive_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultationArchive()
     *
     * @param     mixed $consultationArchiveId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByConsultationArchiveId($consultationArchiveId = null, $comparison = null)
    {
        if (is_array($consultationArchiveId)) {
            $useMinMax = false;
            if (isset($consultationArchiveId['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::CONSULTATION_ARCHIVE_ID, $consultationArchiveId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationArchiveId['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::CONSULTATION_ARCHIVE_ID, $consultationArchiveId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::CONSULTATION_ARCHIVE_ID, $consultationArchiveId, $comparison);
    }

    /**
     * Filter the query on the doc_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDocId('fooValue');   // WHERE doc_id = 'fooValue'
     * $query->filterByDocId('%fooValue%'); // WHERE doc_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $docId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByDocId($docId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($docId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $docId)) {
                $docId = str_replace('*', '%', $docId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::DOC_ID, $docId, $comparison);
    }

    /**
     * Filter the query on the chemin_fichier column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminFichier('fooValue');   // WHERE chemin_fichier = 'fooValue'
     * $query->filterByCheminFichier('%fooValue%'); // WHERE chemin_fichier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cheminFichier The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByCheminFichier($cheminFichier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cheminFichier)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cheminFichier)) {
                $cheminFichier = str_replace('*', '%', $cheminFichier);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::CHEMIN_FICHIER, $cheminFichier, $comparison);
    }

    /**
     * Filter the query on the numero_bloc column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroBloc(1234); // WHERE numero_bloc = 1234
     * $query->filterByNumeroBloc(array(12, 34)); // WHERE numero_bloc IN (12, 34)
     * $query->filterByNumeroBloc(array('min' => 12)); // WHERE numero_bloc >= 12
     * $query->filterByNumeroBloc(array('max' => 12)); // WHERE numero_bloc <= 12
     * </code>
     *
     * @param     mixed $numeroBloc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByNumeroBloc($numeroBloc = null, $comparison = null)
    {
        if (is_array($numeroBloc)) {
            $useMinMax = false;
            if (isset($numeroBloc['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::NUMERO_BLOC, $numeroBloc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numeroBloc['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::NUMERO_BLOC, $numeroBloc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::NUMERO_BLOC, $numeroBloc, $comparison);
    }

    /**
     * Filter the query on the poids_bloc column
     *
     * Example usage:
     * <code>
     * $query->filterByPoidsBloc(1234); // WHERE poids_bloc = 1234
     * $query->filterByPoidsBloc(array(12, 34)); // WHERE poids_bloc IN (12, 34)
     * $query->filterByPoidsBloc(array('min' => 12)); // WHERE poids_bloc >= 12
     * $query->filterByPoidsBloc(array('max' => 12)); // WHERE poids_bloc <= 12
     * </code>
     *
     * @param     mixed $poidsBloc The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByPoidsBloc($poidsBloc = null, $comparison = null)
    {
        if (is_array($poidsBloc)) {
            $useMinMax = false;
            if (isset($poidsBloc['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::POIDS_BLOC, $poidsBloc['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($poidsBloc['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::POIDS_BLOC, $poidsBloc['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::POIDS_BLOC, $poidsBloc, $comparison);
    }

    /**
     * Filter the query on the date_envoi_debut column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnvoiDebut('2011-03-14'); // WHERE date_envoi_debut = '2011-03-14'
     * $query->filterByDateEnvoiDebut('now'); // WHERE date_envoi_debut = '2011-03-14'
     * $query->filterByDateEnvoiDebut(array('max' => 'yesterday')); // WHERE date_envoi_debut > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnvoiDebut The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByDateEnvoiDebut($dateEnvoiDebut = null, $comparison = null)
    {
        if (is_array($dateEnvoiDebut)) {
            $useMinMax = false;
            if (isset($dateEnvoiDebut['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::DATE_ENVOI_DEBUT, $dateEnvoiDebut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnvoiDebut['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::DATE_ENVOI_DEBUT, $dateEnvoiDebut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::DATE_ENVOI_DEBUT, $dateEnvoiDebut, $comparison);
    }

    /**
     * Filter the query on the date_envoi_fin column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnvoiFin('2011-03-14'); // WHERE date_envoi_fin = '2011-03-14'
     * $query->filterByDateEnvoiFin('now'); // WHERE date_envoi_fin = '2011-03-14'
     * $query->filterByDateEnvoiFin(array('max' => 'yesterday')); // WHERE date_envoi_fin > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnvoiFin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByDateEnvoiFin($dateEnvoiFin = null, $comparison = null)
    {
        if (is_array($dateEnvoiFin)) {
            $useMinMax = false;
            if (isset($dateEnvoiFin['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::DATE_ENVOI_FIN, $dateEnvoiFin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnvoiFin['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveBlocPeer::DATE_ENVOI_FIN, $dateEnvoiFin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::DATE_ENVOI_FIN, $dateEnvoiFin, $comparison);
    }

    /**
     * Filter the query on the status_transmission column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusTransmission(true); // WHERE status_transmission = true
     * $query->filterByStatusTransmission('yes'); // WHERE status_transmission = true
     * </code>
     *
     * @param     boolean|string $statusTransmission The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByStatusTransmission($statusTransmission = null, $comparison = null)
    {
        if (is_string($statusTransmission)) {
            $statusTransmission = in_array(strtolower($statusTransmission), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::STATUS_TRANSMISSION, $statusTransmission, $comparison);
    }

    /**
     * Filter the query on the erreur column
     *
     * Example usage:
     * <code>
     * $query->filterByErreur('fooValue');   // WHERE erreur = 'fooValue'
     * $query->filterByErreur('%fooValue%'); // WHERE erreur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $erreur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByErreur($erreur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($erreur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $erreur)) {
                $erreur = str_replace('*', '%', $erreur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::ERREUR, $erreur, $comparison);
    }

    /**
     * Filter the query on the comp_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompId('fooValue');   // WHERE comp_id = 'fooValue'
     * $query->filterByCompId('%fooValue%'); // WHERE comp_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $compId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function filterByCompId($compId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($compId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $compId)) {
                $compId = str_replace('*', '%', $compId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveBlocPeer::COMP_ID, $compId, $comparison);
    }

    /**
     * Filter the query by a related CommonConsultationArchive object
     *
     * @param   CommonConsultationArchive|PropelObjectCollection $commonConsultationArchive The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationArchiveBlocQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultationArchive($commonConsultationArchive, $comparison = null)
    {
        if ($commonConsultationArchive instanceof CommonConsultationArchive) {
            return $this
                ->addUsingAlias(CommonConsultationArchiveBlocPeer::CONSULTATION_ARCHIVE_ID, $commonConsultationArchive->getId(), $comparison);
        } elseif ($commonConsultationArchive instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConsultationArchiveBlocPeer::CONSULTATION_ARCHIVE_ID, $commonConsultationArchive->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultationArchive() only accepts arguments of type CommonConsultationArchive or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultationArchive relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function joinCommonConsultationArchive($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultationArchive');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultationArchive');
        }

        return $this;
    }

    /**
     * Use the CommonConsultationArchive relation CommonConsultationArchive object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationArchiveQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationArchiveQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonConsultationArchive($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultationArchive', '\Application\Propel\Mpe\CommonConsultationArchiveQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonConsultationArchiveBloc $commonConsultationArchiveBloc Object to remove from the list of results
     *
     * @return CommonConsultationArchiveBlocQuery The current query, for fluid interface
     */
    public function prune($commonConsultationArchiveBloc = null)
    {
        if ($commonConsultationArchiveBloc) {
            $this->addUsingAlias(CommonConsultationArchiveBlocPeer::ID, $commonConsultationArchiveBloc->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
