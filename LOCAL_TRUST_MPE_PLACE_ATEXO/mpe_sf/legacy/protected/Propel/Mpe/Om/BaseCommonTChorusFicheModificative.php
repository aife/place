<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusEchangeQuery;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTChorusFicheModificativePeer;
use Application\Propel\Mpe\CommonTChorusFicheModificativePj;
use Application\Propel\Mpe\CommonTChorusFicheModificativePjQuery;
use Application\Propel\Mpe\CommonTChorusFicheModificativeQuery;

/**
 * Base class that represents a row from the 't_chorus_fiche_modificative' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTChorusFicheModificative extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTChorusFicheModificativePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTChorusFicheModificativePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_fiche_modificative field.
     * @var        int
     */
    protected $id_fiche_modificative;

    /**
     * The value for the id_echange field.
     * @var        int
     */
    protected $id_echange;

    /**
     * The value for the organisme field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the type_modification field.
     * @var        int
     */
    protected $type_modification;

    /**
     * The value for the date_prevue_notification field.
     * @var        string
     */
    protected $date_prevue_notification;

    /**
     * The value for the date_fin_marche field.
     * @var        string
     */
    protected $date_fin_marche;

    /**
     * The value for the date_fin_marche_modifie field.
     * @var        string
     */
    protected $date_fin_marche_modifie;

    /**
     * The value for the montant_marche field.
     * @var        double
     */
    protected $montant_marche;

    /**
     * The value for the montant_acte field.
     * @var        double
     */
    protected $montant_acte;

    /**
     * The value for the taux_tva field.
     * @var        string
     */
    protected $taux_tva;

    /**
     * The value for the nombre_fournisseur_cotraitant field.
     * @var        string
     */
    protected $nombre_fournisseur_cotraitant;

    /**
     * The value for the localites_fournisseurs field.
     * @var        string
     */
    protected $localites_fournisseurs;

    /**
     * The value for the siren_fournisseur field.
     * @var        string
     */
    protected $siren_fournisseur;

    /**
     * The value for the siret_fournisseur field.
     * @var        string
     */
    protected $siret_fournisseur;

    /**
     * The value for the nom_fournisseur field.
     * @var        string
     */
    protected $nom_fournisseur;

    /**
     * The value for the type_fournisseur field.
     * @var        string
     */
    protected $type_fournisseur;

    /**
     * The value for the visa_accf field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $visa_accf;

    /**
     * The value for the visa_prefet field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $visa_prefet;

    /**
     * The value for the remarque field.
     * @var        string
     */
    protected $remarque;

    /**
     * The value for the id_blob_piece_justificatives field.
     * @var        string
     */
    protected $id_blob_piece_justificatives;

    /**
     * The value for the id_blob_fiche_modificative field.
     * @var        string
     */
    protected $id_blob_fiche_modificative;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_modification field.
     * @var        string
     */
    protected $date_modification;

    /**
     * @var        CommonChorusEchange
     */
    protected $aCommonChorusEchange;

    /**
     * @var        PropelObjectCollection|CommonTChorusFicheModificativePj[] Collection to store aggregation of CommonTChorusFicheModificativePj objects.
     */
    protected $collCommonTChorusFicheModificativePjs;
    protected $collCommonTChorusFicheModificativePjsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTChorusFicheModificativePjsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->organisme = '';
        $this->visa_accf = '0';
        $this->visa_prefet = '0';
    }

    /**
     * Initializes internal state of BaseCommonTChorusFicheModificative object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_fiche_modificative] column value.
     *
     * @return int
     */
    public function getIdFicheModificative()
    {

        return $this->id_fiche_modificative;
    }

    /**
     * Get the [id_echange] column value.
     *
     * @return int
     */
    public function getIdEchange()
    {

        return $this->id_echange;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [type_modification] column value.
     *
     * @return int
     */
    public function getTypeModification()
    {

        return $this->type_modification;
    }

    /**
     * Get the [optionally formatted] temporal [date_prevue_notification] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDatePrevueNotification($format = '%d/%m/%Y')
    {
        if ($this->date_prevue_notification === null) {
            return null;
        }

        if ($this->date_prevue_notification === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_prevue_notification);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_prevue_notification, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_fin_marche] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateFinMarche($format = '%d/%m/%Y')
    {
        if ($this->date_fin_marche === null) {
            return null;
        }

        if ($this->date_fin_marche === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_fin_marche);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_fin_marche, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_fin_marche_modifie] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateFinMarcheModifie($format = '%d/%m/%Y')
    {
        if ($this->date_fin_marche_modifie === null) {
            return null;
        }

        if ($this->date_fin_marche_modifie === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_fin_marche_modifie);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_fin_marche_modifie, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [montant_marche] column value.
     *
     * @return double
     */
    public function getMontantMarche()
    {

        return $this->montant_marche;
    }

    /**
     * Get the [montant_acte] column value.
     *
     * @return double
     */
    public function getMontantActe()
    {

        return $this->montant_acte;
    }

    /**
     * Get the [taux_tva] column value.
     *
     * @return string
     */
    public function getTauxTva()
    {

        return $this->taux_tva;
    }

    /**
     * Get the [nombre_fournisseur_cotraitant] column value.
     *
     * @return string
     */
    public function getNombreFournisseurCotraitant()
    {

        return $this->nombre_fournisseur_cotraitant;
    }

    /**
     * Get the [localites_fournisseurs] column value.
     *
     * @return string
     */
    public function getLocalitesFournisseurs()
    {

        return $this->localites_fournisseurs;
    }

    /**
     * Get the [siren_fournisseur] column value.
     *
     * @return string
     */
    public function getSirenFournisseur()
    {

        return $this->siren_fournisseur;
    }

    /**
     * Get the [siret_fournisseur] column value.
     *
     * @return string
     */
    public function getSiretFournisseur()
    {

        return $this->siret_fournisseur;
    }

    /**
     * Get the [nom_fournisseur] column value.
     *
     * @return string
     */
    public function getNomFournisseur()
    {

        return $this->nom_fournisseur;
    }

    /**
     * Get the [type_fournisseur] column value.
     *
     * @return string
     */
    public function getTypeFournisseur()
    {

        return $this->type_fournisseur;
    }

    /**
     * Get the [visa_accf] column value.
     *
     * @return string
     */
    public function getVisaAccf()
    {

        return $this->visa_accf;
    }

    /**
     * Get the [visa_prefet] column value.
     *
     * @return string
     */
    public function getVisaPrefet()
    {

        return $this->visa_prefet;
    }

    /**
     * Get the [remarque] column value.
     *
     * @return string
     */
    public function getRemarque()
    {

        return $this->remarque;
    }

    /**
     * Get the [id_blob_piece_justificatives] column value.
     *
     * @return string
     */
    public function getIdBlobPieceJustificatives()
    {

        return $this->id_blob_piece_justificatives;
    }

    /**
     * Get the [id_blob_fiche_modificative] column value.
     *
     * @return string
     */
    public function getIdBlobFicheModificative()
    {

        return $this->id_blob_fiche_modificative;
    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_modification] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateModification($format = 'Y-m-d H:i:s')
    {
        if ($this->date_modification === null) {
            return null;
        }

        if ($this->date_modification === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_modification);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_modification, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [id_fiche_modificative] column.
     *
     * @param int $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setIdFicheModificative($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_fiche_modificative !== $v) {
            $this->id_fiche_modificative = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE;
        }


        return $this;
    } // setIdFicheModificative()

    /**
     * Set the value of [id_echange] column.
     *
     * @param int $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setIdEchange($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_echange !== $v) {
            $this->id_echange = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::ID_ECHANGE;
        }

        if ($this->aCommonChorusEchange !== null && $this->aCommonChorusEchange->getId() !== $v) {
            $this->aCommonChorusEchange = null;
        }


        return $this;
    } // setIdEchange()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::ORGANISME;
        }

        if ($this->aCommonChorusEchange !== null && $this->aCommonChorusEchange->getOrganisme() !== $v) {
            $this->aCommonChorusEchange = null;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [type_modification] column.
     *
     * @param int $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setTypeModification($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->type_modification !== $v) {
            $this->type_modification = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::TYPE_MODIFICATION;
        }


        return $this;
    } // setTypeModification()

    /**
     * Sets the value of [date_prevue_notification] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setDatePrevueNotification($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_prevue_notification !== null || $dt !== null) {
            $currentDateAsString = ($this->date_prevue_notification !== null && $tmpDt = new DateTime($this->date_prevue_notification)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_prevue_notification = $newDateAsString;
                $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::DATE_PREVUE_NOTIFICATION;
            }
        } // if either are not null


        return $this;
    } // setDatePrevueNotification()

    /**
     * Sets the value of [date_fin_marche] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setDateFinMarche($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_fin_marche !== null || $dt !== null) {
            $currentDateAsString = ($this->date_fin_marche !== null && $tmpDt = new DateTime($this->date_fin_marche)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_fin_marche = $newDateAsString;
                $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE;
            }
        } // if either are not null


        return $this;
    } // setDateFinMarche()

    /**
     * Sets the value of [date_fin_marche_modifie] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setDateFinMarcheModifie($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_fin_marche_modifie !== null || $dt !== null) {
            $currentDateAsString = ($this->date_fin_marche_modifie !== null && $tmpDt = new DateTime($this->date_fin_marche_modifie)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_fin_marche_modifie = $newDateAsString;
                $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE_MODIFIE;
            }
        } // if either are not null


        return $this;
    } // setDateFinMarcheModifie()

    /**
     * Set the value of [montant_marche] column.
     *
     * @param double $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setMontantMarche($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->montant_marche !== $v) {
            $this->montant_marche = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::MONTANT_MARCHE;
        }


        return $this;
    } // setMontantMarche()

    /**
     * Set the value of [montant_acte] column.
     *
     * @param double $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setMontantActe($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->montant_acte !== $v) {
            $this->montant_acte = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::MONTANT_ACTE;
        }


        return $this;
    } // setMontantActe()

    /**
     * Set the value of [taux_tva] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setTauxTva($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->taux_tva !== $v) {
            $this->taux_tva = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::TAUX_TVA;
        }


        return $this;
    } // setTauxTva()

    /**
     * Set the value of [nombre_fournisseur_cotraitant] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setNombreFournisseurCotraitant($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nombre_fournisseur_cotraitant !== $v) {
            $this->nombre_fournisseur_cotraitant = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::NOMBRE_FOURNISSEUR_COTRAITANT;
        }


        return $this;
    } // setNombreFournisseurCotraitant()

    /**
     * Set the value of [localites_fournisseurs] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setLocalitesFournisseurs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->localites_fournisseurs !== $v) {
            $this->localites_fournisseurs = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::LOCALITES_FOURNISSEURS;
        }


        return $this;
    } // setLocalitesFournisseurs()

    /**
     * Set the value of [siren_fournisseur] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setSirenFournisseur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->siren_fournisseur !== $v) {
            $this->siren_fournisseur = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::SIREN_FOURNISSEUR;
        }


        return $this;
    } // setSirenFournisseur()

    /**
     * Set the value of [siret_fournisseur] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setSiretFournisseur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->siret_fournisseur !== $v) {
            $this->siret_fournisseur = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::SIRET_FOURNISSEUR;
        }


        return $this;
    } // setSiretFournisseur()

    /**
     * Set the value of [nom_fournisseur] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setNomFournisseur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_fournisseur !== $v) {
            $this->nom_fournisseur = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::NOM_FOURNISSEUR;
        }


        return $this;
    } // setNomFournisseur()

    /**
     * Set the value of [type_fournisseur] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setTypeFournisseur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_fournisseur !== $v) {
            $this->type_fournisseur = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::TYPE_FOURNISSEUR;
        }


        return $this;
    } // setTypeFournisseur()

    /**
     * Set the value of [visa_accf] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setVisaAccf($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->visa_accf !== $v) {
            $this->visa_accf = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::VISA_ACCF;
        }


        return $this;
    } // setVisaAccf()

    /**
     * Set the value of [visa_prefet] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setVisaPrefet($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->visa_prefet !== $v) {
            $this->visa_prefet = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::VISA_PREFET;
        }


        return $this;
    } // setVisaPrefet()

    /**
     * Set the value of [remarque] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setRemarque($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->remarque !== $v) {
            $this->remarque = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::REMARQUE;
        }


        return $this;
    } // setRemarque()

    /**
     * Set the value of [id_blob_piece_justificatives] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setIdBlobPieceJustificatives($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_blob_piece_justificatives !== $v) {
            $this->id_blob_piece_justificatives = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::ID_BLOB_PIECE_JUSTIFICATIVES;
        }


        return $this;
    } // setIdBlobPieceJustificatives()

    /**
     * Set the value of [id_blob_fiche_modificative] column.
     *
     * @param string $v new value
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setIdBlobFicheModificative($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_blob_fiche_modificative !== $v) {
            $this->id_blob_fiche_modificative = $v;
            $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::ID_BLOB_FICHE_MODIFICATIVE;
        }


        return $this;
    } // setIdBlobFicheModificative()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Sets the value of [date_modification] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setDateModification($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_modification !== null || $dt !== null) {
            $currentDateAsString = ($this->date_modification !== null && $tmpDt = new DateTime($this->date_modification)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_modification = $newDateAsString;
                $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::DATE_MODIFICATION;
            }
        } // if either are not null


        return $this;
    } // setDateModification()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->organisme !== '') {
                return false;
            }

            if ($this->visa_accf !== '0') {
                return false;
            }

            if ($this->visa_prefet !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_fiche_modificative = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_echange = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->organisme = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->type_modification = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->date_prevue_notification = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->date_fin_marche = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->date_fin_marche_modifie = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->montant_marche = ($row[$startcol + 7] !== null) ? (double) $row[$startcol + 7] : null;
            $this->montant_acte = ($row[$startcol + 8] !== null) ? (double) $row[$startcol + 8] : null;
            $this->taux_tva = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->nombre_fournisseur_cotraitant = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->localites_fournisseurs = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->siren_fournisseur = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->siret_fournisseur = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->nom_fournisseur = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->type_fournisseur = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->visa_accf = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->visa_prefet = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->remarque = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->id_blob_piece_justificatives = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->id_blob_fiche_modificative = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->date_creation = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->date_modification = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 23; // 23 = CommonTChorusFicheModificativePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTChorusFicheModificative object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonChorusEchange !== null && $this->id_echange !== $this->aCommonChorusEchange->getId()) {
            $this->aCommonChorusEchange = null;
        }
        if ($this->aCommonChorusEchange !== null && $this->organisme !== $this->aCommonChorusEchange->getOrganisme()) {
            $this->aCommonChorusEchange = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTChorusFicheModificativePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonChorusEchange = null;
            $this->collCommonTChorusFicheModificativePjs = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTChorusFicheModificativeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTChorusFicheModificativePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonChorusEchange !== null) {
                if ($this->aCommonChorusEchange->isModified() || $this->aCommonChorusEchange->isNew()) {
                    $affectedRows += $this->aCommonChorusEchange->save($con);
                }
                $this->setCommonChorusEchange($this->aCommonChorusEchange);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonTChorusFicheModificativePjsScheduledForDeletion !== null) {
                if (!$this->commonTChorusFicheModificativePjsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTChorusFicheModificativePjQuery::create()
                        ->filterByPrimaryKeys($this->commonTChorusFicheModificativePjsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTChorusFicheModificativePjsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTChorusFicheModificativePjs !== null) {
                foreach ($this->collCommonTChorusFicheModificativePjs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE;
        if (null !== $this->id_fiche_modificative) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE)) {
            $modifiedColumns[':p' . $index++]  = '`id_fiche_modificative`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::ID_ECHANGE)) {
            $modifiedColumns[':p' . $index++]  = '`id_echange`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::TYPE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`type_modification`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::DATE_PREVUE_NOTIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_prevue_notification`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE)) {
            $modifiedColumns[':p' . $index++]  = '`date_fin_marche`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE_MODIFIE)) {
            $modifiedColumns[':p' . $index++]  = '`date_fin_marche_modifie`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::MONTANT_MARCHE)) {
            $modifiedColumns[':p' . $index++]  = '`montant_marche`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::MONTANT_ACTE)) {
            $modifiedColumns[':p' . $index++]  = '`montant_acte`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::TAUX_TVA)) {
            $modifiedColumns[':p' . $index++]  = '`taux_tva`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::NOMBRE_FOURNISSEUR_COTRAITANT)) {
            $modifiedColumns[':p' . $index++]  = '`nombre_fournisseur_cotraitant`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::LOCALITES_FOURNISSEURS)) {
            $modifiedColumns[':p' . $index++]  = '`localites_fournisseurs`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::SIREN_FOURNISSEUR)) {
            $modifiedColumns[':p' . $index++]  = '`siren_fournisseur`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::SIRET_FOURNISSEUR)) {
            $modifiedColumns[':p' . $index++]  = '`siret_fournisseur`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::NOM_FOURNISSEUR)) {
            $modifiedColumns[':p' . $index++]  = '`nom_fournisseur`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::TYPE_FOURNISSEUR)) {
            $modifiedColumns[':p' . $index++]  = '`type_fournisseur`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::VISA_ACCF)) {
            $modifiedColumns[':p' . $index++]  = '`visa_accf`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::VISA_PREFET)) {
            $modifiedColumns[':p' . $index++]  = '`visa_prefet`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::REMARQUE)) {
            $modifiedColumns[':p' . $index++]  = '`remarque`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::ID_BLOB_PIECE_JUSTIFICATIVES)) {
            $modifiedColumns[':p' . $index++]  = '`id_blob_piece_justificatives`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::ID_BLOB_FICHE_MODIFICATIVE)) {
            $modifiedColumns[':p' . $index++]  = '`id_blob_fiche_modificative`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::DATE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification`';
        }

        $sql = sprintf(
            'INSERT INTO `t_chorus_fiche_modificative` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_fiche_modificative`':
                        $stmt->bindValue($identifier, $this->id_fiche_modificative, PDO::PARAM_INT);
                        break;
                    case '`id_echange`':
                        $stmt->bindValue($identifier, $this->id_echange, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`type_modification`':
                        $stmt->bindValue($identifier, $this->type_modification, PDO::PARAM_INT);
                        break;
                    case '`date_prevue_notification`':
                        $stmt->bindValue($identifier, $this->date_prevue_notification, PDO::PARAM_STR);
                        break;
                    case '`date_fin_marche`':
                        $stmt->bindValue($identifier, $this->date_fin_marche, PDO::PARAM_STR);
                        break;
                    case '`date_fin_marche_modifie`':
                        $stmt->bindValue($identifier, $this->date_fin_marche_modifie, PDO::PARAM_STR);
                        break;
                    case '`montant_marche`':
                        $stmt->bindValue($identifier, $this->montant_marche, PDO::PARAM_STR);
                        break;
                    case '`montant_acte`':
                        $stmt->bindValue($identifier, $this->montant_acte, PDO::PARAM_STR);
                        break;
                    case '`taux_tva`':
                        $stmt->bindValue($identifier, $this->taux_tva, PDO::PARAM_STR);
                        break;
                    case '`nombre_fournisseur_cotraitant`':
                        $stmt->bindValue($identifier, $this->nombre_fournisseur_cotraitant, PDO::PARAM_STR);
                        break;
                    case '`localites_fournisseurs`':
                        $stmt->bindValue($identifier, $this->localites_fournisseurs, PDO::PARAM_STR);
                        break;
                    case '`siren_fournisseur`':
                        $stmt->bindValue($identifier, $this->siren_fournisseur, PDO::PARAM_STR);
                        break;
                    case '`siret_fournisseur`':
                        $stmt->bindValue($identifier, $this->siret_fournisseur, PDO::PARAM_STR);
                        break;
                    case '`nom_fournisseur`':
                        $stmt->bindValue($identifier, $this->nom_fournisseur, PDO::PARAM_STR);
                        break;
                    case '`type_fournisseur`':
                        $stmt->bindValue($identifier, $this->type_fournisseur, PDO::PARAM_STR);
                        break;
                    case '`visa_accf`':
                        $stmt->bindValue($identifier, $this->visa_accf, PDO::PARAM_STR);
                        break;
                    case '`visa_prefet`':
                        $stmt->bindValue($identifier, $this->visa_prefet, PDO::PARAM_STR);
                        break;
                    case '`remarque`':
                        $stmt->bindValue($identifier, $this->remarque, PDO::PARAM_STR);
                        break;
                    case '`id_blob_piece_justificatives`':
                        $stmt->bindValue($identifier, $this->id_blob_piece_justificatives, PDO::PARAM_STR);
                        break;
                    case '`id_blob_fiche_modificative`':
                        $stmt->bindValue($identifier, $this->id_blob_fiche_modificative, PDO::PARAM_STR);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_modification`':
                        $stmt->bindValue($identifier, $this->date_modification, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdFicheModificative($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonChorusEchange !== null) {
                if (!$this->aCommonChorusEchange->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonChorusEchange->getValidationFailures());
                }
            }


            if (($retval = CommonTChorusFicheModificativePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTChorusFicheModificativePjs !== null) {
                    foreach ($this->collCommonTChorusFicheModificativePjs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTChorusFicheModificativePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdFicheModificative();
                break;
            case 1:
                return $this->getIdEchange();
                break;
            case 2:
                return $this->getOrganisme();
                break;
            case 3:
                return $this->getTypeModification();
                break;
            case 4:
                return $this->getDatePrevueNotification();
                break;
            case 5:
                return $this->getDateFinMarche();
                break;
            case 6:
                return $this->getDateFinMarcheModifie();
                break;
            case 7:
                return $this->getMontantMarche();
                break;
            case 8:
                return $this->getMontantActe();
                break;
            case 9:
                return $this->getTauxTva();
                break;
            case 10:
                return $this->getNombreFournisseurCotraitant();
                break;
            case 11:
                return $this->getLocalitesFournisseurs();
                break;
            case 12:
                return $this->getSirenFournisseur();
                break;
            case 13:
                return $this->getSiretFournisseur();
                break;
            case 14:
                return $this->getNomFournisseur();
                break;
            case 15:
                return $this->getTypeFournisseur();
                break;
            case 16:
                return $this->getVisaAccf();
                break;
            case 17:
                return $this->getVisaPrefet();
                break;
            case 18:
                return $this->getRemarque();
                break;
            case 19:
                return $this->getIdBlobPieceJustificatives();
                break;
            case 20:
                return $this->getIdBlobFicheModificative();
                break;
            case 21:
                return $this->getDateCreation();
                break;
            case 22:
                return $this->getDateModification();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTChorusFicheModificative'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTChorusFicheModificative'][$this->getPrimaryKey()] = true;
        $keys = CommonTChorusFicheModificativePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdFicheModificative(),
            $keys[1] => $this->getIdEchange(),
            $keys[2] => $this->getOrganisme(),
            $keys[3] => $this->getTypeModification(),
            $keys[4] => $this->getDatePrevueNotification(),
            $keys[5] => $this->getDateFinMarche(),
            $keys[6] => $this->getDateFinMarcheModifie(),
            $keys[7] => $this->getMontantMarche(),
            $keys[8] => $this->getMontantActe(),
            $keys[9] => $this->getTauxTva(),
            $keys[10] => $this->getNombreFournisseurCotraitant(),
            $keys[11] => $this->getLocalitesFournisseurs(),
            $keys[12] => $this->getSirenFournisseur(),
            $keys[13] => $this->getSiretFournisseur(),
            $keys[14] => $this->getNomFournisseur(),
            $keys[15] => $this->getTypeFournisseur(),
            $keys[16] => $this->getVisaAccf(),
            $keys[17] => $this->getVisaPrefet(),
            $keys[18] => $this->getRemarque(),
            $keys[19] => $this->getIdBlobPieceJustificatives(),
            $keys[20] => $this->getIdBlobFicheModificative(),
            $keys[21] => $this->getDateCreation(),
            $keys[22] => $this->getDateModification(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonChorusEchange) {
                $result['CommonChorusEchange'] = $this->aCommonChorusEchange->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonTChorusFicheModificativePjs) {
                $result['CommonTChorusFicheModificativePjs'] = $this->collCommonTChorusFicheModificativePjs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTChorusFicheModificativePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdFicheModificative($value);
                break;
            case 1:
                $this->setIdEchange($value);
                break;
            case 2:
                $this->setOrganisme($value);
                break;
            case 3:
                $this->setTypeModification($value);
                break;
            case 4:
                $this->setDatePrevueNotification($value);
                break;
            case 5:
                $this->setDateFinMarche($value);
                break;
            case 6:
                $this->setDateFinMarcheModifie($value);
                break;
            case 7:
                $this->setMontantMarche($value);
                break;
            case 8:
                $this->setMontantActe($value);
                break;
            case 9:
                $this->setTauxTva($value);
                break;
            case 10:
                $this->setNombreFournisseurCotraitant($value);
                break;
            case 11:
                $this->setLocalitesFournisseurs($value);
                break;
            case 12:
                $this->setSirenFournisseur($value);
                break;
            case 13:
                $this->setSiretFournisseur($value);
                break;
            case 14:
                $this->setNomFournisseur($value);
                break;
            case 15:
                $this->setTypeFournisseur($value);
                break;
            case 16:
                $this->setVisaAccf($value);
                break;
            case 17:
                $this->setVisaPrefet($value);
                break;
            case 18:
                $this->setRemarque($value);
                break;
            case 19:
                $this->setIdBlobPieceJustificatives($value);
                break;
            case 20:
                $this->setIdBlobFicheModificative($value);
                break;
            case 21:
                $this->setDateCreation($value);
                break;
            case 22:
                $this->setDateModification($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTChorusFicheModificativePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdFicheModificative($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdEchange($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setOrganisme($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTypeModification($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDatePrevueNotification($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDateFinMarche($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setDateFinMarcheModifie($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setMontantMarche($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setMontantActe($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setTauxTva($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setNombreFournisseurCotraitant($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setLocalitesFournisseurs($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setSirenFournisseur($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setSiretFournisseur($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setNomFournisseur($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setTypeFournisseur($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setVisaAccf($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setVisaPrefet($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setRemarque($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setIdBlobPieceJustificatives($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setIdBlobFicheModificative($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setDateCreation($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setDateModification($arr[$keys[22]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTChorusFicheModificativePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE)) $criteria->add(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $this->id_fiche_modificative);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::ID_ECHANGE)) $criteria->add(CommonTChorusFicheModificativePeer::ID_ECHANGE, $this->id_echange);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::ORGANISME)) $criteria->add(CommonTChorusFicheModificativePeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::TYPE_MODIFICATION)) $criteria->add(CommonTChorusFicheModificativePeer::TYPE_MODIFICATION, $this->type_modification);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::DATE_PREVUE_NOTIFICATION)) $criteria->add(CommonTChorusFicheModificativePeer::DATE_PREVUE_NOTIFICATION, $this->date_prevue_notification);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE)) $criteria->add(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE, $this->date_fin_marche);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE_MODIFIE)) $criteria->add(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE_MODIFIE, $this->date_fin_marche_modifie);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::MONTANT_MARCHE)) $criteria->add(CommonTChorusFicheModificativePeer::MONTANT_MARCHE, $this->montant_marche);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::MONTANT_ACTE)) $criteria->add(CommonTChorusFicheModificativePeer::MONTANT_ACTE, $this->montant_acte);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::TAUX_TVA)) $criteria->add(CommonTChorusFicheModificativePeer::TAUX_TVA, $this->taux_tva);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::NOMBRE_FOURNISSEUR_COTRAITANT)) $criteria->add(CommonTChorusFicheModificativePeer::NOMBRE_FOURNISSEUR_COTRAITANT, $this->nombre_fournisseur_cotraitant);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::LOCALITES_FOURNISSEURS)) $criteria->add(CommonTChorusFicheModificativePeer::LOCALITES_FOURNISSEURS, $this->localites_fournisseurs);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::SIREN_FOURNISSEUR)) $criteria->add(CommonTChorusFicheModificativePeer::SIREN_FOURNISSEUR, $this->siren_fournisseur);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::SIRET_FOURNISSEUR)) $criteria->add(CommonTChorusFicheModificativePeer::SIRET_FOURNISSEUR, $this->siret_fournisseur);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::NOM_FOURNISSEUR)) $criteria->add(CommonTChorusFicheModificativePeer::NOM_FOURNISSEUR, $this->nom_fournisseur);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::TYPE_FOURNISSEUR)) $criteria->add(CommonTChorusFicheModificativePeer::TYPE_FOURNISSEUR, $this->type_fournisseur);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::VISA_ACCF)) $criteria->add(CommonTChorusFicheModificativePeer::VISA_ACCF, $this->visa_accf);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::VISA_PREFET)) $criteria->add(CommonTChorusFicheModificativePeer::VISA_PREFET, $this->visa_prefet);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::REMARQUE)) $criteria->add(CommonTChorusFicheModificativePeer::REMARQUE, $this->remarque);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::ID_BLOB_PIECE_JUSTIFICATIVES)) $criteria->add(CommonTChorusFicheModificativePeer::ID_BLOB_PIECE_JUSTIFICATIVES, $this->id_blob_piece_justificatives);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::ID_BLOB_FICHE_MODIFICATIVE)) $criteria->add(CommonTChorusFicheModificativePeer::ID_BLOB_FICHE_MODIFICATIVE, $this->id_blob_fiche_modificative);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::DATE_CREATION)) $criteria->add(CommonTChorusFicheModificativePeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonTChorusFicheModificativePeer::DATE_MODIFICATION)) $criteria->add(CommonTChorusFicheModificativePeer::DATE_MODIFICATION, $this->date_modification);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTChorusFicheModificativePeer::DATABASE_NAME);
        $criteria->add(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $this->id_fiche_modificative);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdFicheModificative();
    }

    /**
     * Generic method to set the primary key (id_fiche_modificative column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdFicheModificative($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdFicheModificative();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTChorusFicheModificative (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdEchange($this->getIdEchange());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setTypeModification($this->getTypeModification());
        $copyObj->setDatePrevueNotification($this->getDatePrevueNotification());
        $copyObj->setDateFinMarche($this->getDateFinMarche());
        $copyObj->setDateFinMarcheModifie($this->getDateFinMarcheModifie());
        $copyObj->setMontantMarche($this->getMontantMarche());
        $copyObj->setMontantActe($this->getMontantActe());
        $copyObj->setTauxTva($this->getTauxTva());
        $copyObj->setNombreFournisseurCotraitant($this->getNombreFournisseurCotraitant());
        $copyObj->setLocalitesFournisseurs($this->getLocalitesFournisseurs());
        $copyObj->setSirenFournisseur($this->getSirenFournisseur());
        $copyObj->setSiretFournisseur($this->getSiretFournisseur());
        $copyObj->setNomFournisseur($this->getNomFournisseur());
        $copyObj->setTypeFournisseur($this->getTypeFournisseur());
        $copyObj->setVisaAccf($this->getVisaAccf());
        $copyObj->setVisaPrefet($this->getVisaPrefet());
        $copyObj->setRemarque($this->getRemarque());
        $copyObj->setIdBlobPieceJustificatives($this->getIdBlobPieceJustificatives());
        $copyObj->setIdBlobFicheModificative($this->getIdBlobFicheModificative());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateModification($this->getDateModification());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTChorusFicheModificativePjs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTChorusFicheModificativePj($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdFicheModificative(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTChorusFicheModificative Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTChorusFicheModificativePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTChorusFicheModificativePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonChorusEchange object.
     *
     * @param   CommonChorusEchange $v
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonChorusEchange(CommonChorusEchange $v = null)
    {
        if ($v === null) {
            $this->setIdEchange(NULL);
        } else {
            $this->setIdEchange($v->getId());
        }

        if ($v === null) {
            $this->setOrganisme('');
        } else {
            $this->setOrganisme($v->getOrganisme());
        }

        $this->aCommonChorusEchange = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonChorusEchange object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTChorusFicheModificative($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonChorusEchange object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonChorusEchange The associated CommonChorusEchange object.
     * @throws PropelException
     */
    public function getCommonChorusEchange(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonChorusEchange === null && ($this->id_echange !== null && ($this->organisme !== "" && $this->organisme !== null)) && $doQuery) {
            $this->aCommonChorusEchange = CommonChorusEchangeQuery::create()->findPk(array($this->id_echange, $this->organisme), $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonChorusEchange->addCommonTChorusFicheModificatives($this);
             */
        }

        return $this->aCommonChorusEchange;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTChorusFicheModificativePj' == $relationName) {
            $this->initCommonTChorusFicheModificativePjs();
        }
    }

    /**
     * Clears out the collCommonTChorusFicheModificativePjs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     * @see        addCommonTChorusFicheModificativePjs()
     */
    public function clearCommonTChorusFicheModificativePjs()
    {
        $this->collCommonTChorusFicheModificativePjs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTChorusFicheModificativePjsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTChorusFicheModificativePjs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTChorusFicheModificativePjs($v = true)
    {
        $this->collCommonTChorusFicheModificativePjsPartial = $v;
    }

    /**
     * Initializes the collCommonTChorusFicheModificativePjs collection.
     *
     * By default this just sets the collCommonTChorusFicheModificativePjs collection to an empty array (like clearcollCommonTChorusFicheModificativePjs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTChorusFicheModificativePjs($overrideExisting = true)
    {
        if (null !== $this->collCommonTChorusFicheModificativePjs && !$overrideExisting) {
            return;
        }
        $this->collCommonTChorusFicheModificativePjs = new PropelObjectCollection();
        $this->collCommonTChorusFicheModificativePjs->setModel('CommonTChorusFicheModificativePj');
    }

    /**
     * Gets an array of CommonTChorusFicheModificativePj objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTChorusFicheModificative is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTChorusFicheModificativePj[] List of CommonTChorusFicheModificativePj objects
     * @throws PropelException
     */
    public function getCommonTChorusFicheModificativePjs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTChorusFicheModificativePjsPartial && !$this->isNew();
        if (null === $this->collCommonTChorusFicheModificativePjs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTChorusFicheModificativePjs) {
                // return empty collection
                $this->initCommonTChorusFicheModificativePjs();
            } else {
                $collCommonTChorusFicheModificativePjs = CommonTChorusFicheModificativePjQuery::create(null, $criteria)
                    ->filterByCommonTChorusFicheModificative($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTChorusFicheModificativePjsPartial && count($collCommonTChorusFicheModificativePjs)) {
                      $this->initCommonTChorusFicheModificativePjs(false);

                      foreach ($collCommonTChorusFicheModificativePjs as $obj) {
                        if (false == $this->collCommonTChorusFicheModificativePjs->contains($obj)) {
                          $this->collCommonTChorusFicheModificativePjs->append($obj);
                        }
                      }

                      $this->collCommonTChorusFicheModificativePjsPartial = true;
                    }

                    $collCommonTChorusFicheModificativePjs->getInternalIterator()->rewind();

                    return $collCommonTChorusFicheModificativePjs;
                }

                if ($partial && $this->collCommonTChorusFicheModificativePjs) {
                    foreach ($this->collCommonTChorusFicheModificativePjs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTChorusFicheModificativePjs[] = $obj;
                        }
                    }
                }

                $this->collCommonTChorusFicheModificativePjs = $collCommonTChorusFicheModificativePjs;
                $this->collCommonTChorusFicheModificativePjsPartial = false;
            }
        }

        return $this->collCommonTChorusFicheModificativePjs;
    }

    /**
     * Sets a collection of CommonTChorusFicheModificativePj objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTChorusFicheModificativePjs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function setCommonTChorusFicheModificativePjs(PropelCollection $commonTChorusFicheModificativePjs, PropelPDO $con = null)
    {
        $commonTChorusFicheModificativePjsToDelete = $this->getCommonTChorusFicheModificativePjs(new Criteria(), $con)->diff($commonTChorusFicheModificativePjs);


        $this->commonTChorusFicheModificativePjsScheduledForDeletion = $commonTChorusFicheModificativePjsToDelete;

        foreach ($commonTChorusFicheModificativePjsToDelete as $commonTChorusFicheModificativePjRemoved) {
            $commonTChorusFicheModificativePjRemoved->setCommonTChorusFicheModificative(null);
        }

        $this->collCommonTChorusFicheModificativePjs = null;
        foreach ($commonTChorusFicheModificativePjs as $commonTChorusFicheModificativePj) {
            $this->addCommonTChorusFicheModificativePj($commonTChorusFicheModificativePj);
        }

        $this->collCommonTChorusFicheModificativePjs = $commonTChorusFicheModificativePjs;
        $this->collCommonTChorusFicheModificativePjsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTChorusFicheModificativePj objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTChorusFicheModificativePj objects.
     * @throws PropelException
     */
    public function countCommonTChorusFicheModificativePjs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTChorusFicheModificativePjsPartial && !$this->isNew();
        if (null === $this->collCommonTChorusFicheModificativePjs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTChorusFicheModificativePjs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTChorusFicheModificativePjs());
            }
            $query = CommonTChorusFicheModificativePjQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTChorusFicheModificative($this)
                ->count($con);
        }

        return count($this->collCommonTChorusFicheModificativePjs);
    }

    /**
     * Method called to associate a CommonTChorusFicheModificativePj object to this object
     * through the CommonTChorusFicheModificativePj foreign key attribute.
     *
     * @param   CommonTChorusFicheModificativePj $l CommonTChorusFicheModificativePj
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function addCommonTChorusFicheModificativePj(CommonTChorusFicheModificativePj $l)
    {
        if ($this->collCommonTChorusFicheModificativePjs === null) {
            $this->initCommonTChorusFicheModificativePjs();
            $this->collCommonTChorusFicheModificativePjsPartial = true;
        }
        if (!in_array($l, $this->collCommonTChorusFicheModificativePjs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTChorusFicheModificativePj($l);
        }

        return $this;
    }

    /**
     * @param	CommonTChorusFicheModificativePj $commonTChorusFicheModificativePj The commonTChorusFicheModificativePj object to add.
     */
    protected function doAddCommonTChorusFicheModificativePj($commonTChorusFicheModificativePj)
    {
        $this->collCommonTChorusFicheModificativePjs[]= $commonTChorusFicheModificativePj;
        $commonTChorusFicheModificativePj->setCommonTChorusFicheModificative($this);
    }

    /**
     * @param	CommonTChorusFicheModificativePj $commonTChorusFicheModificativePj The commonTChorusFicheModificativePj object to remove.
     * @return CommonTChorusFicheModificative The current object (for fluent API support)
     */
    public function removeCommonTChorusFicheModificativePj($commonTChorusFicheModificativePj)
    {
        if ($this->getCommonTChorusFicheModificativePjs()->contains($commonTChorusFicheModificativePj)) {
            $this->collCommonTChorusFicheModificativePjs->remove($this->collCommonTChorusFicheModificativePjs->search($commonTChorusFicheModificativePj));
            if (null === $this->commonTChorusFicheModificativePjsScheduledForDeletion) {
                $this->commonTChorusFicheModificativePjsScheduledForDeletion = clone $this->collCommonTChorusFicheModificativePjs;
                $this->commonTChorusFicheModificativePjsScheduledForDeletion->clear();
            }
            $this->commonTChorusFicheModificativePjsScheduledForDeletion[]= clone $commonTChorusFicheModificativePj;
            $commonTChorusFicheModificativePj->setCommonTChorusFicheModificative(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_fiche_modificative = null;
        $this->id_echange = null;
        $this->organisme = null;
        $this->type_modification = null;
        $this->date_prevue_notification = null;
        $this->date_fin_marche = null;
        $this->date_fin_marche_modifie = null;
        $this->montant_marche = null;
        $this->montant_acte = null;
        $this->taux_tva = null;
        $this->nombre_fournisseur_cotraitant = null;
        $this->localites_fournisseurs = null;
        $this->siren_fournisseur = null;
        $this->siret_fournisseur = null;
        $this->nom_fournisseur = null;
        $this->type_fournisseur = null;
        $this->visa_accf = null;
        $this->visa_prefet = null;
        $this->remarque = null;
        $this->id_blob_piece_justificatives = null;
        $this->id_blob_fiche_modificative = null;
        $this->date_creation = null;
        $this->date_modification = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTChorusFicheModificativePjs) {
                foreach ($this->collCommonTChorusFicheModificativePjs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonChorusEchange instanceof Persistent) {
              $this->aCommonChorusEchange->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTChorusFicheModificativePjs instanceof PropelCollection) {
            $this->collCommonTChorusFicheModificativePjs->clearIterator();
        }
        $this->collCommonTChorusFicheModificativePjs = null;
        $this->aCommonChorusEchange = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTChorusFicheModificativePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
