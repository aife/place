<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonEchangeDocApplication;
use Application\Propel\Mpe\CommonEchangeDocApplicationClient;
use Application\Propel\Mpe\CommonEchangeDocApplicationPeer;
use Application\Propel\Mpe\CommonEchangeDocApplicationQuery;

/**
 * Base class that represents a query for the 'echange_doc_application' table.
 *
 *
 *
 * @method CommonEchangeDocApplicationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonEchangeDocApplicationQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method CommonEchangeDocApplicationQuery orderByLibelle($order = Criteria::ASC) Order by the libelle column
 * @method CommonEchangeDocApplicationQuery orderByFluxActes($order = Criteria::ASC) Order by the flux_actes column
 * @method CommonEchangeDocApplicationQuery orderByPrimoSignature($order = Criteria::ASC) Order by the primo_signature column
 * @method CommonEchangeDocApplicationQuery orderByEnvoiDic($order = Criteria::ASC) Order by the envoi_dic column
 *
 * @method CommonEchangeDocApplicationQuery groupById() Group by the id column
 * @method CommonEchangeDocApplicationQuery groupByCode() Group by the code column
 * @method CommonEchangeDocApplicationQuery groupByLibelle() Group by the libelle column
 * @method CommonEchangeDocApplicationQuery groupByFluxActes() Group by the flux_actes column
 * @method CommonEchangeDocApplicationQuery groupByPrimoSignature() Group by the primo_signature column
 * @method CommonEchangeDocApplicationQuery groupByEnvoiDic() Group by the envoi_dic column
 *
 * @method CommonEchangeDocApplicationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonEchangeDocApplicationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonEchangeDocApplicationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonEchangeDocApplicationQuery leftJoinCommonEchangeDocApplicationClient($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocApplicationClient relation
 * @method CommonEchangeDocApplicationQuery rightJoinCommonEchangeDocApplicationClient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocApplicationClient relation
 * @method CommonEchangeDocApplicationQuery innerJoinCommonEchangeDocApplicationClient($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocApplicationClient relation
 *
 * @method CommonEchangeDocApplication findOne(PropelPDO $con = null) Return the first CommonEchangeDocApplication matching the query
 * @method CommonEchangeDocApplication findOneOrCreate(PropelPDO $con = null) Return the first CommonEchangeDocApplication matching the query, or a new CommonEchangeDocApplication object populated from the query conditions when no match is found
 *
 * @method CommonEchangeDocApplication findOneByCode(string $code) Return the first CommonEchangeDocApplication filtered by the code column
 * @method CommonEchangeDocApplication findOneByLibelle(string $libelle) Return the first CommonEchangeDocApplication filtered by the libelle column
 * @method CommonEchangeDocApplication findOneByFluxActes(boolean $flux_actes) Return the first CommonEchangeDocApplication filtered by the flux_actes column
 * @method CommonEchangeDocApplication findOneByPrimoSignature(boolean $primo_signature) Return the first CommonEchangeDocApplication filtered by the primo_signature column
 * @method CommonEchangeDocApplication findOneByEnvoiDic(boolean $envoi_dic) Return the first CommonEchangeDocApplication filtered by the envoi_dic column
 *
 * @method array findById(int $id) Return CommonEchangeDocApplication objects filtered by the id column
 * @method array findByCode(string $code) Return CommonEchangeDocApplication objects filtered by the code column
 * @method array findByLibelle(string $libelle) Return CommonEchangeDocApplication objects filtered by the libelle column
 * @method array findByFluxActes(boolean $flux_actes) Return CommonEchangeDocApplication objects filtered by the flux_actes column
 * @method array findByPrimoSignature(boolean $primo_signature) Return CommonEchangeDocApplication objects filtered by the primo_signature column
 * @method array findByEnvoiDic(boolean $envoi_dic) Return CommonEchangeDocApplication objects filtered by the envoi_dic column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangeDocApplicationQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonEchangeDocApplicationQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonEchangeDocApplication', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonEchangeDocApplicationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonEchangeDocApplicationQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonEchangeDocApplicationQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonEchangeDocApplicationQuery) {
            return $criteria;
        }
        $query = new CommonEchangeDocApplicationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonEchangeDocApplication|CommonEchangeDocApplication[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonEchangeDocApplicationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangeDocApplication A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangeDocApplication A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `code`, `libelle`, `flux_actes`, `primo_signature`, `envoi_dic` FROM `echange_doc_application` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonEchangeDocApplication();
            $obj->hydrate($row);
            CommonEchangeDocApplicationPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonEchangeDocApplication|CommonEchangeDocApplication[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonEchangeDocApplication[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonEchangeDocApplicationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonEchangeDocApplicationPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonEchangeDocApplicationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonEchangeDocApplicationPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonEchangeDocApplicationPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonEchangeDocApplicationPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the libelle column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelle('fooValue');   // WHERE libelle = 'fooValue'
     * $query->filterByLibelle('%fooValue%'); // WHERE libelle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationQuery The current query, for fluid interface
     */
    public function filterByLibelle($libelle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelle)) {
                $libelle = str_replace('*', '%', $libelle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationPeer::LIBELLE, $libelle, $comparison);
    }

    /**
     * Filter the query on the flux_actes column
     *
     * Example usage:
     * <code>
     * $query->filterByFluxActes(true); // WHERE flux_actes = true
     * $query->filterByFluxActes('yes'); // WHERE flux_actes = true
     * </code>
     *
     * @param     boolean|string $fluxActes The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationQuery The current query, for fluid interface
     */
    public function filterByFluxActes($fluxActes = null, $comparison = null)
    {
        if (is_string($fluxActes)) {
            $fluxActes = in_array(strtolower($fluxActes), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationPeer::FLUX_ACTES, $fluxActes, $comparison);
    }

    /**
     * Filter the query on the primo_signature column
     *
     * Example usage:
     * <code>
     * $query->filterByPrimoSignature(true); // WHERE primo_signature = true
     * $query->filterByPrimoSignature('yes'); // WHERE primo_signature = true
     * </code>
     *
     * @param     boolean|string $primoSignature The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationQuery The current query, for fluid interface
     */
    public function filterByPrimoSignature($primoSignature = null, $comparison = null)
    {
        if (is_string($primoSignature)) {
            $primoSignature = in_array(strtolower($primoSignature), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationPeer::PRIMO_SIGNATURE, $primoSignature, $comparison);
    }

    /**
     * Filter the query on the envoi_dic column
     *
     * Example usage:
     * <code>
     * $query->filterByEnvoiDic(true); // WHERE envoi_dic = true
     * $query->filterByEnvoiDic('yes'); // WHERE envoi_dic = true
     * </code>
     *
     * @param     boolean|string $envoiDic The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationQuery The current query, for fluid interface
     */
    public function filterByEnvoiDic($envoiDic = null, $comparison = null)
    {
        if (is_string($envoiDic)) {
            $envoiDic = in_array(strtolower($envoiDic), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationPeer::ENVOI_DIC, $envoiDic, $comparison);
    }

    /**
     * Filter the query by a related CommonEchangeDocApplicationClient object
     *
     * @param   CommonEchangeDocApplicationClient|PropelObjectCollection $commonEchangeDocApplicationClient  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocApplicationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocApplicationClient($commonEchangeDocApplicationClient, $comparison = null)
    {
        if ($commonEchangeDocApplicationClient instanceof CommonEchangeDocApplicationClient) {
            return $this
                ->addUsingAlias(CommonEchangeDocApplicationPeer::ID, $commonEchangeDocApplicationClient->getEchangeDocApplicationId(), $comparison);
        } elseif ($commonEchangeDocApplicationClient instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeDocApplicationClientQuery()
                ->filterByPrimaryKeys($commonEchangeDocApplicationClient->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangeDocApplicationClient() only accepts arguments of type CommonEchangeDocApplicationClient or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocApplicationClient relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocApplicationQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocApplicationClient($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocApplicationClient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocApplicationClient');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocApplicationClient relation CommonEchangeDocApplicationClient object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocApplicationClientQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocApplicationClientQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocApplicationClient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocApplicationClient', '\Application\Propel\Mpe\CommonEchangeDocApplicationClientQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonEchangeDocApplication $commonEchangeDocApplication Object to remove from the list of results
     *
     * @return CommonEchangeDocApplicationQuery The current query, for fluid interface
     */
    public function prune($commonEchangeDocApplication = null)
    {
        if ($commonEchangeDocApplication) {
            $this->addUsingAlias(CommonEchangeDocApplicationPeer::ID, $commonEchangeDocApplication->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
