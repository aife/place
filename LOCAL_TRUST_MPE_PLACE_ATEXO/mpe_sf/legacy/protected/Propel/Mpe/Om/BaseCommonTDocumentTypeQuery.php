<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDocumentEntreprise;
use Application\Propel\Mpe\CommonTDocumentType;
use Application\Propel\Mpe\CommonTDocumentTypePeer;
use Application\Propel\Mpe\CommonTDocumentTypeQuery;

/**
 * Base class that represents a query for the 't_document_type' table.
 *
 *
 *
 * @method CommonTDocumentTypeQuery orderByIdTypeDocument($order = Criteria::ASC) Order by the id_type_document column
 * @method CommonTDocumentTypeQuery orderByNomTypeDocument($order = Criteria::ASC) Order by the nom_type_document column
 * @method CommonTDocumentTypeQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method CommonTDocumentTypeQuery orderByTypeDocEntrepriseEtablissement($order = Criteria::ASC) Order by the type_doc_entreprise_etablissement column
 * @method CommonTDocumentTypeQuery orderByUri($order = Criteria::ASC) Order by the uri column
 * @method CommonTDocumentTypeQuery orderByParamsUri($order = Criteria::ASC) Order by the params_uri column
 * @method CommonTDocumentTypeQuery orderByClassName($order = Criteria::ASC) Order by the class_name column
 * @method CommonTDocumentTypeQuery orderByNatureDocument($order = Criteria::ASC) Order by the nature_document column
 * @method CommonTDocumentTypeQuery orderByTypeRetourWs($order = Criteria::ASC) Order by the type_retour_ws column
 * @method CommonTDocumentTypeQuery orderBySynchroActif($order = Criteria::ASC) Order by the synchro_actif column
 * @method CommonTDocumentTypeQuery orderByMessageDesactivationSynchro($order = Criteria::ASC) Order by the message_desactivation_synchro column
 * @method CommonTDocumentTypeQuery orderByAfficherTypeDoc($order = Criteria::ASC) Order by the afficher_type_doc column
 *
 * @method CommonTDocumentTypeQuery groupByIdTypeDocument() Group by the id_type_document column
 * @method CommonTDocumentTypeQuery groupByNomTypeDocument() Group by the nom_type_document column
 * @method CommonTDocumentTypeQuery groupByCode() Group by the code column
 * @method CommonTDocumentTypeQuery groupByTypeDocEntrepriseEtablissement() Group by the type_doc_entreprise_etablissement column
 * @method CommonTDocumentTypeQuery groupByUri() Group by the uri column
 * @method CommonTDocumentTypeQuery groupByParamsUri() Group by the params_uri column
 * @method CommonTDocumentTypeQuery groupByClassName() Group by the class_name column
 * @method CommonTDocumentTypeQuery groupByNatureDocument() Group by the nature_document column
 * @method CommonTDocumentTypeQuery groupByTypeRetourWs() Group by the type_retour_ws column
 * @method CommonTDocumentTypeQuery groupBySynchroActif() Group by the synchro_actif column
 * @method CommonTDocumentTypeQuery groupByMessageDesactivationSynchro() Group by the message_desactivation_synchro column
 * @method CommonTDocumentTypeQuery groupByAfficherTypeDoc() Group by the afficher_type_doc column
 *
 * @method CommonTDocumentTypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTDocumentTypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTDocumentTypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTDocumentTypeQuery leftJoinCommonTDocumentEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTDocumentEntreprise relation
 * @method CommonTDocumentTypeQuery rightJoinCommonTDocumentEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTDocumentEntreprise relation
 * @method CommonTDocumentTypeQuery innerJoinCommonTDocumentEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTDocumentEntreprise relation
 *
 * @method CommonTDocumentType findOne(PropelPDO $con = null) Return the first CommonTDocumentType matching the query
 * @method CommonTDocumentType findOneOrCreate(PropelPDO $con = null) Return the first CommonTDocumentType matching the query, or a new CommonTDocumentType object populated from the query conditions when no match is found
 *
 * @method CommonTDocumentType findOneByNomTypeDocument(string $nom_type_document) Return the first CommonTDocumentType filtered by the nom_type_document column
 * @method CommonTDocumentType findOneByCode(string $code) Return the first CommonTDocumentType filtered by the code column
 * @method CommonTDocumentType findOneByTypeDocEntrepriseEtablissement(string $type_doc_entreprise_etablissement) Return the first CommonTDocumentType filtered by the type_doc_entreprise_etablissement column
 * @method CommonTDocumentType findOneByUri(string $uri) Return the first CommonTDocumentType filtered by the uri column
 * @method CommonTDocumentType findOneByParamsUri(string $params_uri) Return the first CommonTDocumentType filtered by the params_uri column
 * @method CommonTDocumentType findOneByClassName(string $class_name) Return the first CommonTDocumentType filtered by the class_name column
 * @method CommonTDocumentType findOneByNatureDocument(string $nature_document) Return the first CommonTDocumentType filtered by the nature_document column
 * @method CommonTDocumentType findOneByTypeRetourWs(string $type_retour_ws) Return the first CommonTDocumentType filtered by the type_retour_ws column
 * @method CommonTDocumentType findOneBySynchroActif(string $synchro_actif) Return the first CommonTDocumentType filtered by the synchro_actif column
 * @method CommonTDocumentType findOneByMessageDesactivationSynchro(string $message_desactivation_synchro) Return the first CommonTDocumentType filtered by the message_desactivation_synchro column
 * @method CommonTDocumentType findOneByAfficherTypeDoc(string $afficher_type_doc) Return the first CommonTDocumentType filtered by the afficher_type_doc column
 *
 * @method array findByIdTypeDocument(int $id_type_document) Return CommonTDocumentType objects filtered by the id_type_document column
 * @method array findByNomTypeDocument(string $nom_type_document) Return CommonTDocumentType objects filtered by the nom_type_document column
 * @method array findByCode(string $code) Return CommonTDocumentType objects filtered by the code column
 * @method array findByTypeDocEntrepriseEtablissement(string $type_doc_entreprise_etablissement) Return CommonTDocumentType objects filtered by the type_doc_entreprise_etablissement column
 * @method array findByUri(string $uri) Return CommonTDocumentType objects filtered by the uri column
 * @method array findByParamsUri(string $params_uri) Return CommonTDocumentType objects filtered by the params_uri column
 * @method array findByClassName(string $class_name) Return CommonTDocumentType objects filtered by the class_name column
 * @method array findByNatureDocument(string $nature_document) Return CommonTDocumentType objects filtered by the nature_document column
 * @method array findByTypeRetourWs(string $type_retour_ws) Return CommonTDocumentType objects filtered by the type_retour_ws column
 * @method array findBySynchroActif(string $synchro_actif) Return CommonTDocumentType objects filtered by the synchro_actif column
 * @method array findByMessageDesactivationSynchro(string $message_desactivation_synchro) Return CommonTDocumentType objects filtered by the message_desactivation_synchro column
 * @method array findByAfficherTypeDoc(string $afficher_type_doc) Return CommonTDocumentType objects filtered by the afficher_type_doc column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDocumentTypeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTDocumentTypeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTDocumentType', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTDocumentTypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTDocumentTypeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTDocumentTypeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTDocumentTypeQuery) {
            return $criteria;
        }
        $query = new CommonTDocumentTypeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTDocumentType|CommonTDocumentType[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTDocumentTypePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDocumentType A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdTypeDocument($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDocumentType A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_type_document`, `nom_type_document`, `code`, `type_doc_entreprise_etablissement`, `uri`, `params_uri`, `class_name`, `nature_document`, `type_retour_ws`, `synchro_actif`, `message_desactivation_synchro`, `afficher_type_doc` FROM `t_document_type` WHERE `id_type_document` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTDocumentType();
            $obj->hydrate($row);
            CommonTDocumentTypePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTDocumentType|CommonTDocumentType[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTDocumentType[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_type_document column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeDocument(1234); // WHERE id_type_document = 1234
     * $query->filterByIdTypeDocument(array(12, 34)); // WHERE id_type_document IN (12, 34)
     * $query->filterByIdTypeDocument(array('min' => 12)); // WHERE id_type_document >= 12
     * $query->filterByIdTypeDocument(array('max' => 12)); // WHERE id_type_document <= 12
     * </code>
     *
     * @param     mixed $idTypeDocument The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByIdTypeDocument($idTypeDocument = null, $comparison = null)
    {
        if (is_array($idTypeDocument)) {
            $useMinMax = false;
            if (isset($idTypeDocument['min'])) {
                $this->addUsingAlias(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $idTypeDocument['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeDocument['max'])) {
                $this->addUsingAlias(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $idTypeDocument['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $idTypeDocument, $comparison);
    }

    /**
     * Filter the query on the nom_type_document column
     *
     * Example usage:
     * <code>
     * $query->filterByNomTypeDocument('fooValue');   // WHERE nom_type_document = 'fooValue'
     * $query->filterByNomTypeDocument('%fooValue%'); // WHERE nom_type_document LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomTypeDocument The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByNomTypeDocument($nomTypeDocument = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomTypeDocument)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomTypeDocument)) {
                $nomTypeDocument = str_replace('*', '%', $nomTypeDocument);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::NOM_TYPE_DOCUMENT, $nomTypeDocument, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the type_doc_entreprise_etablissement column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeDocEntrepriseEtablissement('fooValue');   // WHERE type_doc_entreprise_etablissement = 'fooValue'
     * $query->filterByTypeDocEntrepriseEtablissement('%fooValue%'); // WHERE type_doc_entreprise_etablissement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeDocEntrepriseEtablissement The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByTypeDocEntrepriseEtablissement($typeDocEntrepriseEtablissement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeDocEntrepriseEtablissement)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeDocEntrepriseEtablissement)) {
                $typeDocEntrepriseEtablissement = str_replace('*', '%', $typeDocEntrepriseEtablissement);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::TYPE_DOC_ENTREPRISE_ETABLISSEMENT, $typeDocEntrepriseEtablissement, $comparison);
    }

    /**
     * Filter the query on the uri column
     *
     * Example usage:
     * <code>
     * $query->filterByUri('fooValue');   // WHERE uri = 'fooValue'
     * $query->filterByUri('%fooValue%'); // WHERE uri LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uri The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByUri($uri = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uri)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $uri)) {
                $uri = str_replace('*', '%', $uri);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::URI, $uri, $comparison);
    }

    /**
     * Filter the query on the params_uri column
     *
     * Example usage:
     * <code>
     * $query->filterByParamsUri('fooValue');   // WHERE params_uri = 'fooValue'
     * $query->filterByParamsUri('%fooValue%'); // WHERE params_uri LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paramsUri The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByParamsUri($paramsUri = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paramsUri)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $paramsUri)) {
                $paramsUri = str_replace('*', '%', $paramsUri);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::PARAMS_URI, $paramsUri, $comparison);
    }

    /**
     * Filter the query on the class_name column
     *
     * Example usage:
     * <code>
     * $query->filterByClassName('fooValue');   // WHERE class_name = 'fooValue'
     * $query->filterByClassName('%fooValue%'); // WHERE class_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $className The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByClassName($className = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($className)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $className)) {
                $className = str_replace('*', '%', $className);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::CLASS_NAME, $className, $comparison);
    }

    /**
     * Filter the query on the nature_document column
     *
     * Example usage:
     * <code>
     * $query->filterByNatureDocument('fooValue');   // WHERE nature_document = 'fooValue'
     * $query->filterByNatureDocument('%fooValue%'); // WHERE nature_document LIKE '%fooValue%'
     * </code>
     *
     * @param     string $natureDocument The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByNatureDocument($natureDocument = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($natureDocument)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $natureDocument)) {
                $natureDocument = str_replace('*', '%', $natureDocument);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::NATURE_DOCUMENT, $natureDocument, $comparison);
    }

    /**
     * Filter the query on the type_retour_ws column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeRetourWs('fooValue');   // WHERE type_retour_ws = 'fooValue'
     * $query->filterByTypeRetourWs('%fooValue%'); // WHERE type_retour_ws LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeRetourWs The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByTypeRetourWs($typeRetourWs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeRetourWs)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeRetourWs)) {
                $typeRetourWs = str_replace('*', '%', $typeRetourWs);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::TYPE_RETOUR_WS, $typeRetourWs, $comparison);
    }

    /**
     * Filter the query on the synchro_actif column
     *
     * Example usage:
     * <code>
     * $query->filterBySynchroActif('fooValue');   // WHERE synchro_actif = 'fooValue'
     * $query->filterBySynchroActif('%fooValue%'); // WHERE synchro_actif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $synchroActif The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterBySynchroActif($synchroActif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($synchroActif)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $synchroActif)) {
                $synchroActif = str_replace('*', '%', $synchroActif);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::SYNCHRO_ACTIF, $synchroActif, $comparison);
    }

    /**
     * Filter the query on the message_desactivation_synchro column
     *
     * Example usage:
     * <code>
     * $query->filterByMessageDesactivationSynchro('fooValue');   // WHERE message_desactivation_synchro = 'fooValue'
     * $query->filterByMessageDesactivationSynchro('%fooValue%'); // WHERE message_desactivation_synchro LIKE '%fooValue%'
     * </code>
     *
     * @param     string $messageDesactivationSynchro The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByMessageDesactivationSynchro($messageDesactivationSynchro = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($messageDesactivationSynchro)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $messageDesactivationSynchro)) {
                $messageDesactivationSynchro = str_replace('*', '%', $messageDesactivationSynchro);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::MESSAGE_DESACTIVATION_SYNCHRO, $messageDesactivationSynchro, $comparison);
    }

    /**
     * Filter the query on the afficher_type_doc column
     *
     * Example usage:
     * <code>
     * $query->filterByAfficherTypeDoc('fooValue');   // WHERE afficher_type_doc = 'fooValue'
     * $query->filterByAfficherTypeDoc('%fooValue%'); // WHERE afficher_type_doc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $afficherTypeDoc The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function filterByAfficherTypeDoc($afficherTypeDoc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($afficherTypeDoc)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $afficherTypeDoc)) {
                $afficherTypeDoc = str_replace('*', '%', $afficherTypeDoc);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDocumentTypePeer::AFFICHER_TYPE_DOC, $afficherTypeDoc, $comparison);
    }

    /**
     * Filter the query by a related CommonTDocumentEntreprise object
     *
     * @param   CommonTDocumentEntreprise|PropelObjectCollection $commonTDocumentEntreprise  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTDocumentTypeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTDocumentEntreprise($commonTDocumentEntreprise, $comparison = null)
    {
        if ($commonTDocumentEntreprise instanceof CommonTDocumentEntreprise) {
            return $this
                ->addUsingAlias(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $commonTDocumentEntreprise->getIdTypeDocument(), $comparison);
        } elseif ($commonTDocumentEntreprise instanceof PropelObjectCollection) {
            return $this
                ->useCommonTDocumentEntrepriseQuery()
                ->filterByPrimaryKeys($commonTDocumentEntreprise->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTDocumentEntreprise() only accepts arguments of type CommonTDocumentEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTDocumentEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function joinCommonTDocumentEntreprise($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTDocumentEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTDocumentEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonTDocumentEntreprise relation CommonTDocumentEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTDocumentEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonTDocumentEntrepriseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTDocumentEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTDocumentEntreprise', '\Application\Propel\Mpe\CommonTDocumentEntrepriseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTDocumentType $commonTDocumentType Object to remove from the list of results
     *
     * @return CommonTDocumentTypeQuery The current query, for fluid interface
     */
    public function prune($commonTDocumentType = null)
    {
        if ($commonTDocumentType) {
            $this->addUsingAlias(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $commonTDocumentType->getIdTypeDocument(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
