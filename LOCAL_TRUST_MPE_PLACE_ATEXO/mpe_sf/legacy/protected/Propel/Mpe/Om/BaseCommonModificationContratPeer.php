<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonModificationContrat;
use Application\Propel\Mpe\CommonModificationContratPeer;
use Application\Propel\Mpe\CommonTContratTitulairePeer;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\Map\CommonModificationContratTableMap;

/**
 * Base static class for performing query and update operations on the 'modification_contrat' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonModificationContratPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'modification_contrat';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonModificationContrat';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonModificationContratTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 15;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 15;

    /** the column name for the id field */
    const ID = 'modification_contrat.id';

    /** the column name for the id_contrat_titulaire field */
    const ID_CONTRAT_TITULAIRE = 'modification_contrat.id_contrat_titulaire';

    /** the column name for the num_ordre field */
    const NUM_ORDRE = 'modification_contrat.num_ordre';

    /** the column name for the date_creation field */
    const DATE_CREATION = 'modification_contrat.date_creation';

    /** the column name for the date_modification field */
    const DATE_MODIFICATION = 'modification_contrat.date_modification';

    /** the column name for the id_agent field */
    const ID_AGENT = 'modification_contrat.id_agent';

    /** the column name for the objet_modification field */
    const OBJET_MODIFICATION = 'modification_contrat.objet_modification';

    /** the column name for the date_signature field */
    const DATE_SIGNATURE = 'modification_contrat.date_signature';

    /** the column name for the montant field */
    const MONTANT = 'modification_contrat.montant';

    /** the column name for the id_etablissement field */
    const ID_ETABLISSEMENT = 'modification_contrat.id_etablissement';

    /** the column name for the duree_marche field */
    const DUREE_MARCHE = 'modification_contrat.duree_marche';

    /** the column name for the statut_publication_sn field */
    const STATUT_PUBLICATION_SN = 'modification_contrat.statut_publication_sn';

    /** the column name for the date_publication_sn field */
    const DATE_PUBLICATION_SN = 'modification_contrat.date_publication_sn';

    /** the column name for the erreur_sn field */
    const ERREUR_SN = 'modification_contrat.erreur_sn';

    /** the column name for the date_modification_sn field */
    const DATE_MODIFICATION_SN = 'modification_contrat.date_modification_sn';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonModificationContrat objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonModificationContrat[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonModificationContratPeer::$fieldNames[CommonModificationContratPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'IdContratTitulaire', 'NumOrdre', 'DateCreation', 'DateModification', 'IdAgent', 'ObjetModification', 'DateSignature', 'Montant', 'IdEtablissement', 'DureeMarche', 'StatutPublicationSn', 'DatePublicationSn', 'ErreurSn', 'DateModificationSn', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'idContratTitulaire', 'numOrdre', 'dateCreation', 'dateModification', 'idAgent', 'objetModification', 'dateSignature', 'montant', 'idEtablissement', 'dureeMarche', 'statutPublicationSn', 'datePublicationSn', 'erreurSn', 'dateModificationSn', ),
        BasePeer::TYPE_COLNAME => array (CommonModificationContratPeer::ID, CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, CommonModificationContratPeer::NUM_ORDRE, CommonModificationContratPeer::DATE_CREATION, CommonModificationContratPeer::DATE_MODIFICATION, CommonModificationContratPeer::ID_AGENT, CommonModificationContratPeer::OBJET_MODIFICATION, CommonModificationContratPeer::DATE_SIGNATURE, CommonModificationContratPeer::MONTANT, CommonModificationContratPeer::ID_ETABLISSEMENT, CommonModificationContratPeer::DUREE_MARCHE, CommonModificationContratPeer::STATUT_PUBLICATION_SN, CommonModificationContratPeer::DATE_PUBLICATION_SN, CommonModificationContratPeer::ERREUR_SN, CommonModificationContratPeer::DATE_MODIFICATION_SN, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ID_CONTRAT_TITULAIRE', 'NUM_ORDRE', 'DATE_CREATION', 'DATE_MODIFICATION', 'ID_AGENT', 'OBJET_MODIFICATION', 'DATE_SIGNATURE', 'MONTANT', 'ID_ETABLISSEMENT', 'DUREE_MARCHE', 'STATUT_PUBLICATION_SN', 'DATE_PUBLICATION_SN', 'ERREUR_SN', 'DATE_MODIFICATION_SN', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'id_contrat_titulaire', 'num_ordre', 'date_creation', 'date_modification', 'id_agent', 'objet_modification', 'date_signature', 'montant', 'id_etablissement', 'duree_marche', 'statut_publication_sn', 'date_publication_sn', 'erreur_sn', 'date_modification_sn', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonModificationContratPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IdContratTitulaire' => 1, 'NumOrdre' => 2, 'DateCreation' => 3, 'DateModification' => 4, 'IdAgent' => 5, 'ObjetModification' => 6, 'DateSignature' => 7, 'Montant' => 8, 'IdEtablissement' => 9, 'DureeMarche' => 10, 'StatutPublicationSn' => 11, 'DatePublicationSn' => 12, 'ErreurSn' => 13, 'DateModificationSn' => 14, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'idContratTitulaire' => 1, 'numOrdre' => 2, 'dateCreation' => 3, 'dateModification' => 4, 'idAgent' => 5, 'objetModification' => 6, 'dateSignature' => 7, 'montant' => 8, 'idEtablissement' => 9, 'dureeMarche' => 10, 'statutPublicationSn' => 11, 'datePublicationSn' => 12, 'erreurSn' => 13, 'dateModificationSn' => 14, ),
        BasePeer::TYPE_COLNAME => array (CommonModificationContratPeer::ID => 0, CommonModificationContratPeer::ID_CONTRAT_TITULAIRE => 1, CommonModificationContratPeer::NUM_ORDRE => 2, CommonModificationContratPeer::DATE_CREATION => 3, CommonModificationContratPeer::DATE_MODIFICATION => 4, CommonModificationContratPeer::ID_AGENT => 5, CommonModificationContratPeer::OBJET_MODIFICATION => 6, CommonModificationContratPeer::DATE_SIGNATURE => 7, CommonModificationContratPeer::MONTANT => 8, CommonModificationContratPeer::ID_ETABLISSEMENT => 9, CommonModificationContratPeer::DUREE_MARCHE => 10, CommonModificationContratPeer::STATUT_PUBLICATION_SN => 11, CommonModificationContratPeer::DATE_PUBLICATION_SN => 12, CommonModificationContratPeer::ERREUR_SN => 13, CommonModificationContratPeer::DATE_MODIFICATION_SN => 14, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ID_CONTRAT_TITULAIRE' => 1, 'NUM_ORDRE' => 2, 'DATE_CREATION' => 3, 'DATE_MODIFICATION' => 4, 'ID_AGENT' => 5, 'OBJET_MODIFICATION' => 6, 'DATE_SIGNATURE' => 7, 'MONTANT' => 8, 'ID_ETABLISSEMENT' => 9, 'DUREE_MARCHE' => 10, 'STATUT_PUBLICATION_SN' => 11, 'DATE_PUBLICATION_SN' => 12, 'ERREUR_SN' => 13, 'DATE_MODIFICATION_SN' => 14, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'id_contrat_titulaire' => 1, 'num_ordre' => 2, 'date_creation' => 3, 'date_modification' => 4, 'id_agent' => 5, 'objet_modification' => 6, 'date_signature' => 7, 'montant' => 8, 'id_etablissement' => 9, 'duree_marche' => 10, 'statut_publication_sn' => 11, 'date_publication_sn' => 12, 'erreur_sn' => 13, 'date_modification_sn' => 14, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonModificationContratPeer::getFieldNames($toType);
        $key = isset(CommonModificationContratPeer::$fieldKeys[$fromType][$name]) ? CommonModificationContratPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonModificationContratPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonModificationContratPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonModificationContratPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonModificationContratPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonModificationContratPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonModificationContratPeer::ID);
            $criteria->addSelectColumn(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE);
            $criteria->addSelectColumn(CommonModificationContratPeer::NUM_ORDRE);
            $criteria->addSelectColumn(CommonModificationContratPeer::DATE_CREATION);
            $criteria->addSelectColumn(CommonModificationContratPeer::DATE_MODIFICATION);
            $criteria->addSelectColumn(CommonModificationContratPeer::ID_AGENT);
            $criteria->addSelectColumn(CommonModificationContratPeer::OBJET_MODIFICATION);
            $criteria->addSelectColumn(CommonModificationContratPeer::DATE_SIGNATURE);
            $criteria->addSelectColumn(CommonModificationContratPeer::MONTANT);
            $criteria->addSelectColumn(CommonModificationContratPeer::ID_ETABLISSEMENT);
            $criteria->addSelectColumn(CommonModificationContratPeer::DUREE_MARCHE);
            $criteria->addSelectColumn(CommonModificationContratPeer::STATUT_PUBLICATION_SN);
            $criteria->addSelectColumn(CommonModificationContratPeer::DATE_PUBLICATION_SN);
            $criteria->addSelectColumn(CommonModificationContratPeer::ERREUR_SN);
            $criteria->addSelectColumn(CommonModificationContratPeer::DATE_MODIFICATION_SN);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.id_contrat_titulaire');
            $criteria->addSelectColumn($alias . '.num_ordre');
            $criteria->addSelectColumn($alias . '.date_creation');
            $criteria->addSelectColumn($alias . '.date_modification');
            $criteria->addSelectColumn($alias . '.id_agent');
            $criteria->addSelectColumn($alias . '.objet_modification');
            $criteria->addSelectColumn($alias . '.date_signature');
            $criteria->addSelectColumn($alias . '.montant');
            $criteria->addSelectColumn($alias . '.id_etablissement');
            $criteria->addSelectColumn($alias . '.duree_marche');
            $criteria->addSelectColumn($alias . '.statut_publication_sn');
            $criteria->addSelectColumn($alias . '.date_publication_sn');
            $criteria->addSelectColumn($alias . '.erreur_sn');
            $criteria->addSelectColumn($alias . '.date_modification_sn');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonModificationContratPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonModificationContratPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonModificationContrat
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonModificationContratPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonModificationContratPeer::populateObjects(CommonModificationContratPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonModificationContratPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonModificationContrat $obj A CommonModificationContrat object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonModificationContratPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonModificationContrat object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonModificationContrat) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonModificationContrat object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonModificationContratPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonModificationContrat Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonModificationContratPeer::$instances[$key])) {
                return CommonModificationContratPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonModificationContratPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonModificationContratPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to modification_contrat
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonModificationContratPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonModificationContratPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonModificationContratPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonModificationContratPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonModificationContrat object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonModificationContratPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonModificationContratPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonModificationContratPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonModificationContratPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonModificationContratPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonAgent table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonAgent(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonModificationContratPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonModificationContratPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonModificationContratPeer::ID_AGENT, CommonAgentPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTContratTitulaire table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTContratTitulaire(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonModificationContratPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonModificationContratPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTEtablissement table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTEtablissement(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonModificationContratPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonModificationContratPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonModificationContratPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonModificationContrat objects pre-filled with their CommonAgent objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonModificationContrat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonAgent(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);
        }

        CommonModificationContratPeer::addSelectColumns($criteria);
        $startcol = CommonModificationContratPeer::NUM_HYDRATE_COLUMNS;
        CommonAgentPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonModificationContratPeer::ID_AGENT, CommonAgentPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonModificationContratPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonModificationContratPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonModificationContratPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonModificationContratPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonAgentPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonAgentPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonAgentPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonModificationContrat) to $obj2 (CommonAgent)
                $obj2->addCommonModificationContrat($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonModificationContrat objects pre-filled with their CommonTContratTitulaire objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonModificationContrat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTContratTitulaire(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);
        }

        CommonModificationContratPeer::addSelectColumns($criteria);
        $startcol = CommonModificationContratPeer::NUM_HYDRATE_COLUMNS;
        CommonTContratTitulairePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonModificationContratPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonModificationContratPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonModificationContratPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonModificationContratPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTContratTitulairePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol);
                    $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTContratTitulairePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonModificationContrat) to $obj2 (CommonTContratTitulaire)
                $obj2->addCommonModificationContrat($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonModificationContrat objects pre-filled with their CommonTEtablissement objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonModificationContrat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTEtablissement(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);
        }

        CommonModificationContratPeer::addSelectColumns($criteria);
        $startcol = CommonModificationContratPeer::NUM_HYDRATE_COLUMNS;
        CommonTEtablissementPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonModificationContratPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonModificationContratPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonModificationContratPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonModificationContratPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonModificationContratPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTEtablissementPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTEtablissementPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTEtablissementPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonModificationContrat) to $obj2 (CommonTEtablissement)
                $obj2->addCommonModificationContrat($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonModificationContratPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonModificationContratPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonModificationContratPeer::ID_AGENT, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonModificationContratPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonModificationContrat objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonModificationContrat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);
        }

        CommonModificationContratPeer::addSelectColumns($criteria);
        $startcol2 = CommonModificationContratPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonModificationContratPeer::ID_AGENT, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonModificationContratPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonModificationContratPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonModificationContratPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonModificationContratPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonModificationContratPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonAgent rows

            $key2 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonAgentPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonAgentPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonAgentPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonModificationContrat) to the collection in $obj2 (CommonAgent)
                $obj2->addCommonModificationContrat($obj1);
            } // if joined row not null

            // Add objects for joined CommonTContratTitulaire rows

            $key3 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonTContratTitulairePeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol3);
          $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTContratTitulairePeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonModificationContrat) to the collection in $obj3 (CommonTContratTitulaire)
                $obj3->addCommonModificationContrat($obj1);
            } // if joined row not null

            // Add objects for joined CommonTEtablissement rows

            $key4 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CommonTEtablissementPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CommonTEtablissementPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTEtablissementPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CommonModificationContrat) to the collection in $obj4 (CommonTEtablissement)
                $obj4->addCommonModificationContrat($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonAgent table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonAgent(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonModificationContratPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonModificationContratPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonModificationContratPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTContratTitulaire table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTContratTitulaire(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonModificationContratPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonModificationContratPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonModificationContratPeer::ID_AGENT, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonModificationContratPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTEtablissement table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTEtablissement(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonModificationContratPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonModificationContratPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonModificationContratPeer::ID_AGENT, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonModificationContrat objects pre-filled with all related objects except CommonAgent.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonModificationContrat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonAgent(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);
        }

        CommonModificationContratPeer::addSelectColumns($criteria);
        $startcol2 = CommonModificationContratPeer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);

        $criteria->addJoin(CommonModificationContratPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonModificationContratPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonModificationContratPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonModificationContratPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonModificationContratPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTContratTitulaire rows

                $key2 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTContratTitulairePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol2);
            $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTContratTitulairePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonModificationContrat) to the collection in $obj2 (CommonTContratTitulaire)
                $obj2->addCommonModificationContrat($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTEtablissement rows

                $key3 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTEtablissementPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTEtablissementPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTEtablissementPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonModificationContrat) to the collection in $obj3 (CommonTEtablissement)
                $obj3->addCommonModificationContrat($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonModificationContrat objects pre-filled with all related objects except CommonTContratTitulaire.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonModificationContrat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTContratTitulaire(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);
        }

        CommonModificationContratPeer::addSelectColumns($criteria);
        $startcol2 = CommonModificationContratPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonTEtablissementPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonModificationContratPeer::ID_AGENT, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonModificationContratPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonModificationContratPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonModificationContratPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonModificationContratPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonModificationContratPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonAgent rows

                $key2 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonAgentPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonAgentPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonModificationContrat) to the collection in $obj2 (CommonAgent)
                $obj2->addCommonModificationContrat($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTEtablissement rows

                $key3 = CommonTEtablissementPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTEtablissementPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonTEtablissementPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTEtablissementPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonModificationContrat) to the collection in $obj3 (CommonTEtablissement)
                $obj3->addCommonModificationContrat($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonModificationContrat objects pre-filled with all related objects except CommonTEtablissement.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonModificationContrat objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTEtablissement(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);
        }

        CommonModificationContratPeer::addSelectColumns($criteria);
        $startcol2 = CommonModificationContratPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonTContratTitulairePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonTContratTitulairePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonModificationContratPeer::ID_AGENT, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonModificationContratPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonModificationContratPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonModificationContratPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonModificationContratPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonAgent rows

                $key2 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonAgentPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonAgentPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonModificationContrat) to the collection in $obj2 (CommonAgent)
                $obj2->addCommonModificationContrat($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTContratTitulaire rows

                $key3 = CommonTContratTitulairePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonTContratTitulairePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $omClass = CommonTContratTitulairePeer::getOMClass($row, $startcol3);
            $cls = substr('.'.$omClass, strrpos('.'.$omClass, '.') + 1);

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonTContratTitulairePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonModificationContrat) to the collection in $obj3 (CommonTContratTitulaire)
                $obj3->addCommonModificationContrat($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonModificationContratPeer::DATABASE_NAME)->getTable(CommonModificationContratPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonModificationContratPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonModificationContratPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonModificationContratTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonModificationContratPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonModificationContrat or Criteria object.
     *
     * @param      mixed $values Criteria or CommonModificationContrat object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonModificationContrat object
        }

        if ($criteria->containsKey(CommonModificationContratPeer::ID) && $criteria->keyContainsValue(CommonModificationContratPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonModificationContratPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonModificationContrat or Criteria object.
     *
     * @param      mixed $values Criteria or CommonModificationContrat object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonModificationContratPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonModificationContratPeer::ID);
            $value = $criteria->remove(CommonModificationContratPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonModificationContratPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonModificationContratPeer::TABLE_NAME);
            }

        } else { // $values is CommonModificationContrat object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the modification_contrat table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonModificationContratPeer::TABLE_NAME, $con, CommonModificationContratPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonModificationContratPeer::clearInstancePool();
            CommonModificationContratPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonModificationContrat or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonModificationContrat object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonModificationContratPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonModificationContrat) { // it's a model object
            // invalidate the cache for this single object
            CommonModificationContratPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonModificationContratPeer::DATABASE_NAME);
            $criteria->add(CommonModificationContratPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonModificationContratPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonModificationContratPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonModificationContratPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonModificationContrat object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonModificationContrat $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonModificationContratPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonModificationContratPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonModificationContratPeer::DATABASE_NAME, CommonModificationContratPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonModificationContrat
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonModificationContratPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonModificationContratPeer::DATABASE_NAME);
        $criteria->add(CommonModificationContratPeer::ID, $pk);

        $v = CommonModificationContratPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonModificationContrat[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonModificationContratPeer::DATABASE_NAME);
            $criteria->add(CommonModificationContratPeer::ID, $pks, Criteria::IN);
            $objs = CommonModificationContratPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonModificationContratPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonModificationContratPeer::buildTableMap();

