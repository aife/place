<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConfigurationMessagesTraduction;
use Application\Propel\Mpe\CommonConfigurationMessagesTraductionPeer;
use Application\Propel\Mpe\CommonConfigurationMessagesTraductionQuery;
use Application\Propel\Mpe\CommonLangue;

/**
 * Base class that represents a query for the 'configuration_messages_traduction' table.
 *
 *
 *
 * @method CommonConfigurationMessagesTraductionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonConfigurationMessagesTraductionQuery orderBySource($order = Criteria::ASC) Order by the source column
 * @method CommonConfigurationMessagesTraductionQuery orderByTarget($order = Criteria::ASC) Order by the target column
 * @method CommonConfigurationMessagesTraductionQuery orderByLangueId($order = Criteria::ASC) Order by the langue_id column
 *
 * @method CommonConfigurationMessagesTraductionQuery groupById() Group by the id column
 * @method CommonConfigurationMessagesTraductionQuery groupBySource() Group by the source column
 * @method CommonConfigurationMessagesTraductionQuery groupByTarget() Group by the target column
 * @method CommonConfigurationMessagesTraductionQuery groupByLangueId() Group by the langue_id column
 *
 * @method CommonConfigurationMessagesTraductionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonConfigurationMessagesTraductionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonConfigurationMessagesTraductionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonConfigurationMessagesTraductionQuery leftJoinCommonLangue($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonLangue relation
 * @method CommonConfigurationMessagesTraductionQuery rightJoinCommonLangue($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonLangue relation
 * @method CommonConfigurationMessagesTraductionQuery innerJoinCommonLangue($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonLangue relation
 *
 * @method CommonConfigurationMessagesTraduction findOne(PropelPDO $con = null) Return the first CommonConfigurationMessagesTraduction matching the query
 * @method CommonConfigurationMessagesTraduction findOneOrCreate(PropelPDO $con = null) Return the first CommonConfigurationMessagesTraduction matching the query, or a new CommonConfigurationMessagesTraduction object populated from the query conditions when no match is found
 *
 * @method CommonConfigurationMessagesTraduction findOneBySource(string $source) Return the first CommonConfigurationMessagesTraduction filtered by the source column
 * @method CommonConfigurationMessagesTraduction findOneByTarget(string $target) Return the first CommonConfigurationMessagesTraduction filtered by the target column
 * @method CommonConfigurationMessagesTraduction findOneByLangueId(int $langue_id) Return the first CommonConfigurationMessagesTraduction filtered by the langue_id column
 *
 * @method array findById(int $id) Return CommonConfigurationMessagesTraduction objects filtered by the id column
 * @method array findBySource(string $source) Return CommonConfigurationMessagesTraduction objects filtered by the source column
 * @method array findByTarget(string $target) Return CommonConfigurationMessagesTraduction objects filtered by the target column
 * @method array findByLangueId(int $langue_id) Return CommonConfigurationMessagesTraduction objects filtered by the langue_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConfigurationMessagesTraductionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonConfigurationMessagesTraductionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonConfigurationMessagesTraduction', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonConfigurationMessagesTraductionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonConfigurationMessagesTraductionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonConfigurationMessagesTraductionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonConfigurationMessagesTraductionQuery) {
            return $criteria;
        }
        $query = new CommonConfigurationMessagesTraductionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonConfigurationMessagesTraduction|CommonConfigurationMessagesTraduction[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonConfigurationMessagesTraductionPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationMessagesTraductionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConfigurationMessagesTraduction A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConfigurationMessagesTraduction A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `source`, `target`, `langue_id` FROM `configuration_messages_traduction` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonConfigurationMessagesTraduction();
            $obj->hydrate($row);
            CommonConfigurationMessagesTraductionPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonConfigurationMessagesTraduction|CommonConfigurationMessagesTraduction[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonConfigurationMessagesTraduction[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonConfigurationMessagesTraductionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonConfigurationMessagesTraductionPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonConfigurationMessagesTraductionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonConfigurationMessagesTraductionPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationMessagesTraductionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonConfigurationMessagesTraductionPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonConfigurationMessagesTraductionPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConfigurationMessagesTraductionPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the source column
     *
     * Example usage:
     * <code>
     * $query->filterBySource('fooValue');   // WHERE source = 'fooValue'
     * $query->filterBySource('%fooValue%'); // WHERE source LIKE '%fooValue%'
     * </code>
     *
     * @param     string $source The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationMessagesTraductionQuery The current query, for fluid interface
     */
    public function filterBySource($source = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($source)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $source)) {
                $source = str_replace('*', '%', $source);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationMessagesTraductionPeer::SOURCE, $source, $comparison);
    }

    /**
     * Filter the query on the target column
     *
     * Example usage:
     * <code>
     * $query->filterByTarget('fooValue');   // WHERE target = 'fooValue'
     * $query->filterByTarget('%fooValue%'); // WHERE target LIKE '%fooValue%'
     * </code>
     *
     * @param     string $target The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationMessagesTraductionQuery The current query, for fluid interface
     */
    public function filterByTarget($target = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($target)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $target)) {
                $target = str_replace('*', '%', $target);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationMessagesTraductionPeer::TARGET, $target, $comparison);
    }

    /**
     * Filter the query on the langue_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLangueId(1234); // WHERE langue_id = 1234
     * $query->filterByLangueId(array(12, 34)); // WHERE langue_id IN (12, 34)
     * $query->filterByLangueId(array('min' => 12)); // WHERE langue_id >= 12
     * $query->filterByLangueId(array('max' => 12)); // WHERE langue_id <= 12
     * </code>
     *
     * @see       filterByCommonLangue()
     *
     * @param     mixed $langueId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationMessagesTraductionQuery The current query, for fluid interface
     */
    public function filterByLangueId($langueId = null, $comparison = null)
    {
        if (is_array($langueId)) {
            $useMinMax = false;
            if (isset($langueId['min'])) {
                $this->addUsingAlias(CommonConfigurationMessagesTraductionPeer::LANGUE_ID, $langueId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($langueId['max'])) {
                $this->addUsingAlias(CommonConfigurationMessagesTraductionPeer::LANGUE_ID, $langueId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConfigurationMessagesTraductionPeer::LANGUE_ID, $langueId, $comparison);
    }

    /**
     * Filter the query by a related CommonLangue object
     *
     * @param   CommonLangue|PropelObjectCollection $commonLangue The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConfigurationMessagesTraductionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonLangue($commonLangue, $comparison = null)
    {
        if ($commonLangue instanceof CommonLangue) {
            return $this
                ->addUsingAlias(CommonConfigurationMessagesTraductionPeer::LANGUE_ID, $commonLangue->getIdLangue(), $comparison);
        } elseif ($commonLangue instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConfigurationMessagesTraductionPeer::LANGUE_ID, $commonLangue->toKeyValue('PrimaryKey', 'IdLangue'), $comparison);
        } else {
            throw new PropelException('filterByCommonLangue() only accepts arguments of type CommonLangue or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonLangue relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConfigurationMessagesTraductionQuery The current query, for fluid interface
     */
    public function joinCommonLangue($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonLangue');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonLangue');
        }

        return $this;
    }

    /**
     * Use the CommonLangue relation CommonLangue object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonLangueQuery A secondary query class using the current class as primary query
     */
    public function useCommonLangueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonLangue($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonLangue', '\Application\Propel\Mpe\CommonLangueQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonConfigurationMessagesTraduction $commonConfigurationMessagesTraduction Object to remove from the list of results
     *
     * @return CommonConfigurationMessagesTraductionQuery The current query, for fluid interface
     */
    public function prune($commonConfigurationMessagesTraduction = null)
    {
        if ($commonConfigurationMessagesTraduction) {
            $this->addUsingAlias(CommonConfigurationMessagesTraductionPeer::ID, $commonConfigurationMessagesTraduction->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
