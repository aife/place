<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTTypeProcedureDume;
use Application\Propel\Mpe\CommonTTypeProcedureDumePeer;
use Application\Propel\Mpe\CommonTTypeProcedureDumeQuery;

/**
 * Base class that represents a query for the 't_type_procedure_dume' table.
 *
 *
 *
 * @method CommonTTypeProcedureDumeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTTypeProcedureDumeQuery orderByLibelle($order = Criteria::ASC) Order by the libelle column
 * @method CommonTTypeProcedureDumeQuery orderByActive($order = Criteria::ASC) Order by the active column
 * @method CommonTTypeProcedureDumeQuery orderByCodeDume($order = Criteria::ASC) Order by the code_dume column
 *
 * @method CommonTTypeProcedureDumeQuery groupById() Group by the id column
 * @method CommonTTypeProcedureDumeQuery groupByLibelle() Group by the libelle column
 * @method CommonTTypeProcedureDumeQuery groupByActive() Group by the active column
 * @method CommonTTypeProcedureDumeQuery groupByCodeDume() Group by the code_dume column
 *
 * @method CommonTTypeProcedureDumeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTTypeProcedureDumeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTTypeProcedureDumeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTTypeProcedureDume findOne(PropelPDO $con = null) Return the first CommonTTypeProcedureDume matching the query
 * @method CommonTTypeProcedureDume findOneOrCreate(PropelPDO $con = null) Return the first CommonTTypeProcedureDume matching the query, or a new CommonTTypeProcedureDume object populated from the query conditions when no match is found
 *
 * @method CommonTTypeProcedureDume findOneByLibelle(string $libelle) Return the first CommonTTypeProcedureDume filtered by the libelle column
 * @method CommonTTypeProcedureDume findOneByActive(string $active) Return the first CommonTTypeProcedureDume filtered by the active column
 * @method CommonTTypeProcedureDume findOneByCodeDume(string $code_dume) Return the first CommonTTypeProcedureDume filtered by the code_dume column
 *
 * @method array findById(int $id) Return CommonTTypeProcedureDume objects filtered by the id column
 * @method array findByLibelle(string $libelle) Return CommonTTypeProcedureDume objects filtered by the libelle column
 * @method array findByActive(string $active) Return CommonTTypeProcedureDume objects filtered by the active column
 * @method array findByCodeDume(string $code_dume) Return CommonTTypeProcedureDume objects filtered by the code_dume column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTTypeProcedureDumeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTTypeProcedureDumeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTTypeProcedureDume', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTTypeProcedureDumeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTTypeProcedureDumeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTTypeProcedureDumeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTTypeProcedureDumeQuery) {
            return $criteria;
        }
        $query = new CommonTTypeProcedureDumeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTTypeProcedureDume|CommonTTypeProcedureDume[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTTypeProcedureDumePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeProcedureDumePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTypeProcedureDume A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTypeProcedureDume A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `libelle`, `active`, `code_dume` FROM `t_type_procedure_dume` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTTypeProcedureDume();
            $obj->hydrate($row);
            CommonTTypeProcedureDumePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTTypeProcedureDume|CommonTTypeProcedureDume[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTTypeProcedureDume[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTTypeProcedureDumeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTTypeProcedureDumePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTTypeProcedureDumeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTTypeProcedureDumePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeProcedureDumeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTTypeProcedureDumePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTTypeProcedureDumePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTypeProcedureDumePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the libelle column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelle('fooValue');   // WHERE libelle = 'fooValue'
     * $query->filterByLibelle('%fooValue%'); // WHERE libelle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeProcedureDumeQuery The current query, for fluid interface
     */
    public function filterByLibelle($libelle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelle)) {
                $libelle = str_replace('*', '%', $libelle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeProcedureDumePeer::LIBELLE, $libelle, $comparison);
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive('fooValue');   // WHERE active = 'fooValue'
     * $query->filterByActive('%fooValue%'); // WHERE active LIKE '%fooValue%'
     * </code>
     *
     * @param     string $active The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeProcedureDumeQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($active)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $active)) {
                $active = str_replace('*', '%', $active);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeProcedureDumePeer::ACTIVE, $active, $comparison);
    }

    /**
     * Filter the query on the code_dume column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeDume('fooValue');   // WHERE code_dume = 'fooValue'
     * $query->filterByCodeDume('%fooValue%'); // WHERE code_dume LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codeDume The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeProcedureDumeQuery The current query, for fluid interface
     */
    public function filterByCodeDume($codeDume = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codeDume)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codeDume)) {
                $codeDume = str_replace('*', '%', $codeDume);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeProcedureDumePeer::CODE_DUME, $codeDume, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTTypeProcedureDume $commonTTypeProcedureDume Object to remove from the list of results
     *
     * @return CommonTTypeProcedureDumeQuery The current query, for fluid interface
     */
    public function prune($commonTTypeProcedureDume = null)
    {
        if ($commonTTypeProcedureDume) {
            $this->addUsingAlias(CommonTTypeProcedureDumePeer::ID, $commonTTypeProcedureDume->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
