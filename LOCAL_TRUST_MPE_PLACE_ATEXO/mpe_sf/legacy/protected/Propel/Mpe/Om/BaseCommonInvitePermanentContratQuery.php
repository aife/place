<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonInvitePermanentContrat;
use Application\Propel\Mpe\CommonInvitePermanentContratPeer;
use Application\Propel\Mpe\CommonInvitePermanentContratQuery;

/**
 * Base class that represents a query for the 'invite_permanent_contrat' table.
 *
 *
 *
 * @method CommonInvitePermanentContratQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonInvitePermanentContratQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonInvitePermanentContratQuery orderByService($order = Criteria::ASC) Order by the service column
 * @method CommonInvitePermanentContratQuery orderByLot($order = Criteria::ASC) Order by the lot column
 * @method CommonInvitePermanentContratQuery orderByIdConsultation($order = Criteria::ASC) Order by the id_consultation column
 * @method CommonInvitePermanentContratQuery orderByIdContratTitulaire($order = Criteria::ASC) Order by the id_contrat_titulaire column
 * @method CommonInvitePermanentContratQuery orderByDateDecision($order = Criteria::ASC) Order by the date_decision column
 * @method CommonInvitePermanentContratQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method CommonInvitePermanentContratQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method CommonInvitePermanentContratQuery groupById() Group by the id column
 * @method CommonInvitePermanentContratQuery groupByOrganisme() Group by the organisme column
 * @method CommonInvitePermanentContratQuery groupByService() Group by the service column
 * @method CommonInvitePermanentContratQuery groupByLot() Group by the lot column
 * @method CommonInvitePermanentContratQuery groupByIdConsultation() Group by the id_consultation column
 * @method CommonInvitePermanentContratQuery groupByIdContratTitulaire() Group by the id_contrat_titulaire column
 * @method CommonInvitePermanentContratQuery groupByDateDecision() Group by the date_decision column
 * @method CommonInvitePermanentContratQuery groupByCreatedAt() Group by the created_at column
 * @method CommonInvitePermanentContratQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method CommonInvitePermanentContratQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonInvitePermanentContratQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonInvitePermanentContratQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonInvitePermanentContrat findOne(PropelPDO $con = null) Return the first CommonInvitePermanentContrat matching the query
 * @method CommonInvitePermanentContrat findOneOrCreate(PropelPDO $con = null) Return the first CommonInvitePermanentContrat matching the query, or a new CommonInvitePermanentContrat object populated from the query conditions when no match is found
 *
 * @method CommonInvitePermanentContrat findOneByOrganisme(string $organisme) Return the first CommonInvitePermanentContrat filtered by the organisme column
 * @method CommonInvitePermanentContrat findOneByService(int $service) Return the first CommonInvitePermanentContrat filtered by the service column
 * @method CommonInvitePermanentContrat findOneByLot(string $lot) Return the first CommonInvitePermanentContrat filtered by the lot column
 * @method CommonInvitePermanentContrat findOneByIdConsultation(int $id_consultation) Return the first CommonInvitePermanentContrat filtered by the id_consultation column
 * @method CommonInvitePermanentContrat findOneByIdContratTitulaire(int $id_contrat_titulaire) Return the first CommonInvitePermanentContrat filtered by the id_contrat_titulaire column
 * @method CommonInvitePermanentContrat findOneByDateDecision(string $date_decision) Return the first CommonInvitePermanentContrat filtered by the date_decision column
 * @method CommonInvitePermanentContrat findOneByCreatedAt(string $created_at) Return the first CommonInvitePermanentContrat filtered by the created_at column
 * @method CommonInvitePermanentContrat findOneByUpdatedAt(string $updated_at) Return the first CommonInvitePermanentContrat filtered by the updated_at column
 *
 * @method array findById(int $id) Return CommonInvitePermanentContrat objects filtered by the id column
 * @method array findByOrganisme(string $organisme) Return CommonInvitePermanentContrat objects filtered by the organisme column
 * @method array findByService(int $service) Return CommonInvitePermanentContrat objects filtered by the service column
 * @method array findByLot(string $lot) Return CommonInvitePermanentContrat objects filtered by the lot column
 * @method array findByIdConsultation(int $id_consultation) Return CommonInvitePermanentContrat objects filtered by the id_consultation column
 * @method array findByIdContratTitulaire(int $id_contrat_titulaire) Return CommonInvitePermanentContrat objects filtered by the id_contrat_titulaire column
 * @method array findByDateDecision(string $date_decision) Return CommonInvitePermanentContrat objects filtered by the date_decision column
 * @method array findByCreatedAt(string $created_at) Return CommonInvitePermanentContrat objects filtered by the created_at column
 * @method array findByUpdatedAt(string $updated_at) Return CommonInvitePermanentContrat objects filtered by the updated_at column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonInvitePermanentContratQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonInvitePermanentContratQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonInvitePermanentContrat', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonInvitePermanentContratQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonInvitePermanentContratQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonInvitePermanentContratQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonInvitePermanentContratQuery) {
            return $criteria;
        }
        $query = new CommonInvitePermanentContratQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonInvitePermanentContrat|CommonInvitePermanentContrat[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonInvitePermanentContratPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonInvitePermanentContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonInvitePermanentContrat A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonInvitePermanentContrat A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `organisme`, `service`, `lot`, `id_consultation`, `id_contrat_titulaire`, `date_decision`, `created_at`, `updated_at` FROM `invite_permanent_contrat` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonInvitePermanentContrat();
            $obj->hydrate($row);
            CommonInvitePermanentContratPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonInvitePermanentContrat|CommonInvitePermanentContrat[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonInvitePermanentContrat[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonInvitePermanentContratPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonInvitePermanentContratPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentContratPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentContratPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the service column
     *
     * Example usage:
     * <code>
     * $query->filterByService(1234); // WHERE service = 1234
     * $query->filterByService(array(12, 34)); // WHERE service IN (12, 34)
     * $query->filterByService(array('min' => 12)); // WHERE service >= 12
     * $query->filterByService(array('max' => 12)); // WHERE service <= 12
     * </code>
     *
     * @param     mixed $service The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function filterByService($service = null, $comparison = null)
    {
        if (is_array($service)) {
            $useMinMax = false;
            if (isset($service['min'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::SERVICE, $service['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($service['max'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::SERVICE, $service['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentContratPeer::SERVICE, $service, $comparison);
    }

    /**
     * Filter the query on the lot column
     *
     * Example usage:
     * <code>
     * $query->filterByLot('fooValue');   // WHERE lot = 'fooValue'
     * $query->filterByLot('%fooValue%'); // WHERE lot LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lot The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function filterByLot($lot = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lot)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lot)) {
                $lot = str_replace('*', '%', $lot);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentContratPeer::LOT, $lot, $comparison);
    }

    /**
     * Filter the query on the id_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdConsultation(1234); // WHERE id_consultation = 1234
     * $query->filterByIdConsultation(array(12, 34)); // WHERE id_consultation IN (12, 34)
     * $query->filterByIdConsultation(array('min' => 12)); // WHERE id_consultation >= 12
     * $query->filterByIdConsultation(array('max' => 12)); // WHERE id_consultation <= 12
     * </code>
     *
     * @param     mixed $idConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function filterByIdConsultation($idConsultation = null, $comparison = null)
    {
        if (is_array($idConsultation)) {
            $useMinMax = false;
            if (isset($idConsultation['min'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::ID_CONSULTATION, $idConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idConsultation['max'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::ID_CONSULTATION, $idConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentContratPeer::ID_CONSULTATION, $idConsultation, $comparison);
    }

    /**
     * Filter the query on the id_contrat_titulaire column
     *
     * Example usage:
     * <code>
     * $query->filterByIdContratTitulaire(1234); // WHERE id_contrat_titulaire = 1234
     * $query->filterByIdContratTitulaire(array(12, 34)); // WHERE id_contrat_titulaire IN (12, 34)
     * $query->filterByIdContratTitulaire(array('min' => 12)); // WHERE id_contrat_titulaire >= 12
     * $query->filterByIdContratTitulaire(array('max' => 12)); // WHERE id_contrat_titulaire <= 12
     * </code>
     *
     * @param     mixed $idContratTitulaire The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function filterByIdContratTitulaire($idContratTitulaire = null, $comparison = null)
    {
        if (is_array($idContratTitulaire)) {
            $useMinMax = false;
            if (isset($idContratTitulaire['min'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idContratTitulaire['max'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentContratPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire, $comparison);
    }

    /**
     * Filter the query on the date_decision column
     *
     * Example usage:
     * <code>
     * $query->filterByDateDecision('2011-03-14'); // WHERE date_decision = '2011-03-14'
     * $query->filterByDateDecision('now'); // WHERE date_decision = '2011-03-14'
     * $query->filterByDateDecision(array('max' => 'yesterday')); // WHERE date_decision > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateDecision The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function filterByDateDecision($dateDecision = null, $comparison = null)
    {
        if (is_array($dateDecision)) {
            $useMinMax = false;
            if (isset($dateDecision['min'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::DATE_DECISION, $dateDecision['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateDecision['max'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::DATE_DECISION, $dateDecision['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentContratPeer::DATE_DECISION, $dateDecision, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentContratPeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CommonInvitePermanentContratPeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentContratPeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonInvitePermanentContrat $commonInvitePermanentContrat Object to remove from the list of results
     *
     * @return CommonInvitePermanentContratQuery The current query, for fluid interface
     */
    public function prune($commonInvitePermanentContrat = null)
    {
        if ($commonInvitePermanentContrat) {
            $this->addUsingAlias(CommonInvitePermanentContratPeer::ID, $commonInvitePermanentContrat->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
