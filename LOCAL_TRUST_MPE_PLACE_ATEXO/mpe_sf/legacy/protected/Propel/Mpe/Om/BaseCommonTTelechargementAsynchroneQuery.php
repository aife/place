<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTTelechargementAsynchrone;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneFichier;
use Application\Propel\Mpe\CommonTTelechargementAsynchronePeer;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneQuery;

/**
 * Base class that represents a query for the 'T_Telechargement_Asynchrone' table.
 *
 *
 *
 * @method CommonTTelechargementAsynchroneQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTTelechargementAsynchroneQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 * @method CommonTTelechargementAsynchroneQuery orderByNomPrenomAgent($order = Criteria::ASC) Order by the nom_prenom_agent column
 * @method CommonTTelechargementAsynchroneQuery orderByEmailAgent($order = Criteria::ASC) Order by the email_agent column
 * @method CommonTTelechargementAsynchroneQuery orderByIdServiceAgent($order = Criteria::ASC) Order by the id_service_agent column
 * @method CommonTTelechargementAsynchroneQuery orderByOldIdServiceAgent($order = Criteria::ASC) Order by the old_id_service_agent column
 * @method CommonTTelechargementAsynchroneQuery orderByOrganismeAgent($order = Criteria::ASC) Order by the organisme_agent column
 * @method CommonTTelechargementAsynchroneQuery orderByNomFichierTelechargement($order = Criteria::ASC) Order by the nom_fichier_telechargement column
 * @method CommonTTelechargementAsynchroneQuery orderByTailleFichier($order = Criteria::ASC) Order by the taille_fichier column
 * @method CommonTTelechargementAsynchroneQuery orderByDateGeneration($order = Criteria::ASC) Order by the date_generation column
 * @method CommonTTelechargementAsynchroneQuery orderByIdBlobFichier($order = Criteria::ASC) Order by the id_blob_fichier column
 * @method CommonTTelechargementAsynchroneQuery orderByTagFichierGenere($order = Criteria::ASC) Order by the tag_fichier_genere column
 * @method CommonTTelechargementAsynchroneQuery orderByTagFichierSupprime($order = Criteria::ASC) Order by the tag_fichier_supprime column
 * @method CommonTTelechargementAsynchroneQuery orderByTypeTelechargement($order = Criteria::ASC) Order by the type_telechargement column
 *
 * @method CommonTTelechargementAsynchroneQuery groupById() Group by the id column
 * @method CommonTTelechargementAsynchroneQuery groupByIdAgent() Group by the id_agent column
 * @method CommonTTelechargementAsynchroneQuery groupByNomPrenomAgent() Group by the nom_prenom_agent column
 * @method CommonTTelechargementAsynchroneQuery groupByEmailAgent() Group by the email_agent column
 * @method CommonTTelechargementAsynchroneQuery groupByIdServiceAgent() Group by the id_service_agent column
 * @method CommonTTelechargementAsynchroneQuery groupByOldIdServiceAgent() Group by the old_id_service_agent column
 * @method CommonTTelechargementAsynchroneQuery groupByOrganismeAgent() Group by the organisme_agent column
 * @method CommonTTelechargementAsynchroneQuery groupByNomFichierTelechargement() Group by the nom_fichier_telechargement column
 * @method CommonTTelechargementAsynchroneQuery groupByTailleFichier() Group by the taille_fichier column
 * @method CommonTTelechargementAsynchroneQuery groupByDateGeneration() Group by the date_generation column
 * @method CommonTTelechargementAsynchroneQuery groupByIdBlobFichier() Group by the id_blob_fichier column
 * @method CommonTTelechargementAsynchroneQuery groupByTagFichierGenere() Group by the tag_fichier_genere column
 * @method CommonTTelechargementAsynchroneQuery groupByTagFichierSupprime() Group by the tag_fichier_supprime column
 * @method CommonTTelechargementAsynchroneQuery groupByTypeTelechargement() Group by the type_telechargement column
 *
 * @method CommonTTelechargementAsynchroneQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTTelechargementAsynchroneQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTTelechargementAsynchroneQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTTelechargementAsynchroneQuery leftJoinCommonService($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonService relation
 * @method CommonTTelechargementAsynchroneQuery rightJoinCommonService($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonService relation
 * @method CommonTTelechargementAsynchroneQuery innerJoinCommonService($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonService relation
 *
 * @method CommonTTelechargementAsynchroneQuery leftJoinCommonTTelechargementAsynchroneFichier($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTTelechargementAsynchroneFichier relation
 * @method CommonTTelechargementAsynchroneQuery rightJoinCommonTTelechargementAsynchroneFichier($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTTelechargementAsynchroneFichier relation
 * @method CommonTTelechargementAsynchroneQuery innerJoinCommonTTelechargementAsynchroneFichier($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTTelechargementAsynchroneFichier relation
 *
 * @method CommonTTelechargementAsynchrone findOne(PropelPDO $con = null) Return the first CommonTTelechargementAsynchrone matching the query
 * @method CommonTTelechargementAsynchrone findOneOrCreate(PropelPDO $con = null) Return the first CommonTTelechargementAsynchrone matching the query, or a new CommonTTelechargementAsynchrone object populated from the query conditions when no match is found
 *
 * @method CommonTTelechargementAsynchrone findOneByIdAgent(int $id_agent) Return the first CommonTTelechargementAsynchrone filtered by the id_agent column
 * @method CommonTTelechargementAsynchrone findOneByNomPrenomAgent(string $nom_prenom_agent) Return the first CommonTTelechargementAsynchrone filtered by the nom_prenom_agent column
 * @method CommonTTelechargementAsynchrone findOneByEmailAgent(string $email_agent) Return the first CommonTTelechargementAsynchrone filtered by the email_agent column
 * @method CommonTTelechargementAsynchrone findOneByIdServiceAgent(string $id_service_agent) Return the first CommonTTelechargementAsynchrone filtered by the id_service_agent column
 * @method CommonTTelechargementAsynchrone findOneByOldIdServiceAgent(int $old_id_service_agent) Return the first CommonTTelechargementAsynchrone filtered by the old_id_service_agent column
 * @method CommonTTelechargementAsynchrone findOneByOrganismeAgent(string $organisme_agent) Return the first CommonTTelechargementAsynchrone filtered by the organisme_agent column
 * @method CommonTTelechargementAsynchrone findOneByNomFichierTelechargement(string $nom_fichier_telechargement) Return the first CommonTTelechargementAsynchrone filtered by the nom_fichier_telechargement column
 * @method CommonTTelechargementAsynchrone findOneByTailleFichier(int $taille_fichier) Return the first CommonTTelechargementAsynchrone filtered by the taille_fichier column
 * @method CommonTTelechargementAsynchrone findOneByDateGeneration(string $date_generation) Return the first CommonTTelechargementAsynchrone filtered by the date_generation column
 * @method CommonTTelechargementAsynchrone findOneByIdBlobFichier(int $id_blob_fichier) Return the first CommonTTelechargementAsynchrone filtered by the id_blob_fichier column
 * @method CommonTTelechargementAsynchrone findOneByTagFichierGenere(string $tag_fichier_genere) Return the first CommonTTelechargementAsynchrone filtered by the tag_fichier_genere column
 * @method CommonTTelechargementAsynchrone findOneByTagFichierSupprime(string $tag_fichier_supprime) Return the first CommonTTelechargementAsynchrone filtered by the tag_fichier_supprime column
 * @method CommonTTelechargementAsynchrone findOneByTypeTelechargement(int $type_telechargement) Return the first CommonTTelechargementAsynchrone filtered by the type_telechargement column
 *
 * @method array findById(int $id) Return CommonTTelechargementAsynchrone objects filtered by the id column
 * @method array findByIdAgent(int $id_agent) Return CommonTTelechargementAsynchrone objects filtered by the id_agent column
 * @method array findByNomPrenomAgent(string $nom_prenom_agent) Return CommonTTelechargementAsynchrone objects filtered by the nom_prenom_agent column
 * @method array findByEmailAgent(string $email_agent) Return CommonTTelechargementAsynchrone objects filtered by the email_agent column
 * @method array findByIdServiceAgent(string $id_service_agent) Return CommonTTelechargementAsynchrone objects filtered by the id_service_agent column
 * @method array findByOldIdServiceAgent(int $old_id_service_agent) Return CommonTTelechargementAsynchrone objects filtered by the old_id_service_agent column
 * @method array findByOrganismeAgent(string $organisme_agent) Return CommonTTelechargementAsynchrone objects filtered by the organisme_agent column
 * @method array findByNomFichierTelechargement(string $nom_fichier_telechargement) Return CommonTTelechargementAsynchrone objects filtered by the nom_fichier_telechargement column
 * @method array findByTailleFichier(int $taille_fichier) Return CommonTTelechargementAsynchrone objects filtered by the taille_fichier column
 * @method array findByDateGeneration(string $date_generation) Return CommonTTelechargementAsynchrone objects filtered by the date_generation column
 * @method array findByIdBlobFichier(int $id_blob_fichier) Return CommonTTelechargementAsynchrone objects filtered by the id_blob_fichier column
 * @method array findByTagFichierGenere(string $tag_fichier_genere) Return CommonTTelechargementAsynchrone objects filtered by the tag_fichier_genere column
 * @method array findByTagFichierSupprime(string $tag_fichier_supprime) Return CommonTTelechargementAsynchrone objects filtered by the tag_fichier_supprime column
 * @method array findByTypeTelechargement(int $type_telechargement) Return CommonTTelechargementAsynchrone objects filtered by the type_telechargement column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTTelechargementAsynchroneQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTTelechargementAsynchroneQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTTelechargementAsynchrone', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTTelechargementAsynchroneQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTTelechargementAsynchroneQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTTelechargementAsynchroneQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTTelechargementAsynchroneQuery) {
            return $criteria;
        }
        $query = new CommonTTelechargementAsynchroneQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTTelechargementAsynchrone|CommonTTelechargementAsynchrone[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTTelechargementAsynchronePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTelechargementAsynchrone A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTelechargementAsynchrone A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_agent`, `nom_prenom_agent`, `email_agent`, `id_service_agent`, `old_id_service_agent`, `organisme_agent`, `nom_fichier_telechargement`, `taille_fichier`, `date_generation`, `id_blob_fichier`, `tag_fichier_genere`, `tag_fichier_supprime`, `type_telechargement` FROM `T_Telechargement_Asynchrone` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTTelechargementAsynchrone();
            $obj->hydrate($row);
            CommonTTelechargementAsynchronePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTTelechargementAsynchrone|CommonTTelechargementAsynchrone[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTTelechargementAsynchrone[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Filter the query on the nom_prenom_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByNomPrenomAgent('fooValue');   // WHERE nom_prenom_agent = 'fooValue'
     * $query->filterByNomPrenomAgent('%fooValue%'); // WHERE nom_prenom_agent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomPrenomAgent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByNomPrenomAgent($nomPrenomAgent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomPrenomAgent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomPrenomAgent)) {
                $nomPrenomAgent = str_replace('*', '%', $nomPrenomAgent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::NOM_PRENOM_AGENT, $nomPrenomAgent, $comparison);
    }

    /**
     * Filter the query on the email_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailAgent('fooValue');   // WHERE email_agent = 'fooValue'
     * $query->filterByEmailAgent('%fooValue%'); // WHERE email_agent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailAgent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByEmailAgent($emailAgent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailAgent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $emailAgent)) {
                $emailAgent = str_replace('*', '%', $emailAgent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::EMAIL_AGENT, $emailAgent, $comparison);
    }

    /**
     * Filter the query on the id_service_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdServiceAgent(1234); // WHERE id_service_agent = 1234
     * $query->filterByIdServiceAgent(array(12, 34)); // WHERE id_service_agent IN (12, 34)
     * $query->filterByIdServiceAgent(array('min' => 12)); // WHERE id_service_agent >= 12
     * $query->filterByIdServiceAgent(array('max' => 12)); // WHERE id_service_agent <= 12
     * </code>
     *
     * @see       filterByCommonService()
     *
     * @param     mixed $idServiceAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByIdServiceAgent($idServiceAgent = null, $comparison = null)
    {
        if (is_array($idServiceAgent)) {
            $useMinMax = false;
            if (isset($idServiceAgent['min'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, $idServiceAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idServiceAgent['max'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, $idServiceAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, $idServiceAgent, $comparison);
    }

    /**
     * Filter the query on the old_id_service_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdServiceAgent(1234); // WHERE old_id_service_agent = 1234
     * $query->filterByOldIdServiceAgent(array(12, 34)); // WHERE old_id_service_agent IN (12, 34)
     * $query->filterByOldIdServiceAgent(array('min' => 12)); // WHERE old_id_service_agent >= 12
     * $query->filterByOldIdServiceAgent(array('max' => 12)); // WHERE old_id_service_agent <= 12
     * </code>
     *
     * @param     mixed $oldIdServiceAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByOldIdServiceAgent($oldIdServiceAgent = null, $comparison = null)
    {
        if (is_array($oldIdServiceAgent)) {
            $useMinMax = false;
            if (isset($oldIdServiceAgent['min'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::OLD_ID_SERVICE_AGENT, $oldIdServiceAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdServiceAgent['max'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::OLD_ID_SERVICE_AGENT, $oldIdServiceAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::OLD_ID_SERVICE_AGENT, $oldIdServiceAgent, $comparison);
    }

    /**
     * Filter the query on the organisme_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganismeAgent('fooValue');   // WHERE organisme_agent = 'fooValue'
     * $query->filterByOrganismeAgent('%fooValue%'); // WHERE organisme_agent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organismeAgent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByOrganismeAgent($organismeAgent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organismeAgent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organismeAgent)) {
                $organismeAgent = str_replace('*', '%', $organismeAgent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ORGANISME_AGENT, $organismeAgent, $comparison);
    }

    /**
     * Filter the query on the nom_fichier_telechargement column
     *
     * Example usage:
     * <code>
     * $query->filterByNomFichierTelechargement('fooValue');   // WHERE nom_fichier_telechargement = 'fooValue'
     * $query->filterByNomFichierTelechargement('%fooValue%'); // WHERE nom_fichier_telechargement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomFichierTelechargement The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByNomFichierTelechargement($nomFichierTelechargement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomFichierTelechargement)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomFichierTelechargement)) {
                $nomFichierTelechargement = str_replace('*', '%', $nomFichierTelechargement);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::NOM_FICHIER_TELECHARGEMENT, $nomFichierTelechargement, $comparison);
    }

    /**
     * Filter the query on the taille_fichier column
     *
     * Example usage:
     * <code>
     * $query->filterByTailleFichier(1234); // WHERE taille_fichier = 1234
     * $query->filterByTailleFichier(array(12, 34)); // WHERE taille_fichier IN (12, 34)
     * $query->filterByTailleFichier(array('min' => 12)); // WHERE taille_fichier >= 12
     * $query->filterByTailleFichier(array('max' => 12)); // WHERE taille_fichier <= 12
     * </code>
     *
     * @param     mixed $tailleFichier The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByTailleFichier($tailleFichier = null, $comparison = null)
    {
        if (is_array($tailleFichier)) {
            $useMinMax = false;
            if (isset($tailleFichier['min'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::TAILLE_FICHIER, $tailleFichier['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tailleFichier['max'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::TAILLE_FICHIER, $tailleFichier['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::TAILLE_FICHIER, $tailleFichier, $comparison);
    }

    /**
     * Filter the query on the date_generation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateGeneration('2011-03-14'); // WHERE date_generation = '2011-03-14'
     * $query->filterByDateGeneration('now'); // WHERE date_generation = '2011-03-14'
     * $query->filterByDateGeneration(array('max' => 'yesterday')); // WHERE date_generation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateGeneration The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByDateGeneration($dateGeneration = null, $comparison = null)
    {
        if (is_array($dateGeneration)) {
            $useMinMax = false;
            if (isset($dateGeneration['min'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::DATE_GENERATION, $dateGeneration['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateGeneration['max'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::DATE_GENERATION, $dateGeneration['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::DATE_GENERATION, $dateGeneration, $comparison);
    }

    /**
     * Filter the query on the id_blob_fichier column
     *
     * Example usage:
     * <code>
     * $query->filterByIdBlobFichier(1234); // WHERE id_blob_fichier = 1234
     * $query->filterByIdBlobFichier(array(12, 34)); // WHERE id_blob_fichier IN (12, 34)
     * $query->filterByIdBlobFichier(array('min' => 12)); // WHERE id_blob_fichier >= 12
     * $query->filterByIdBlobFichier(array('max' => 12)); // WHERE id_blob_fichier <= 12
     * </code>
     *
     * @param     mixed $idBlobFichier The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByIdBlobFichier($idBlobFichier = null, $comparison = null)
    {
        if (is_array($idBlobFichier)) {
            $useMinMax = false;
            if (isset($idBlobFichier['min'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID_BLOB_FICHIER, $idBlobFichier['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idBlobFichier['max'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID_BLOB_FICHIER, $idBlobFichier['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID_BLOB_FICHIER, $idBlobFichier, $comparison);
    }

    /**
     * Filter the query on the tag_fichier_genere column
     *
     * Example usage:
     * <code>
     * $query->filterByTagFichierGenere('fooValue');   // WHERE tag_fichier_genere = 'fooValue'
     * $query->filterByTagFichierGenere('%fooValue%'); // WHERE tag_fichier_genere LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tagFichierGenere The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByTagFichierGenere($tagFichierGenere = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tagFichierGenere)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tagFichierGenere)) {
                $tagFichierGenere = str_replace('*', '%', $tagFichierGenere);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE, $tagFichierGenere, $comparison);
    }

    /**
     * Filter the query on the tag_fichier_supprime column
     *
     * Example usage:
     * <code>
     * $query->filterByTagFichierSupprime('fooValue');   // WHERE tag_fichier_supprime = 'fooValue'
     * $query->filterByTagFichierSupprime('%fooValue%'); // WHERE tag_fichier_supprime LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tagFichierSupprime The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByTagFichierSupprime($tagFichierSupprime = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tagFichierSupprime)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tagFichierSupprime)) {
                $tagFichierSupprime = str_replace('*', '%', $tagFichierSupprime);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME, $tagFichierSupprime, $comparison);
    }

    /**
     * Filter the query on the type_telechargement column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeTelechargement(1234); // WHERE type_telechargement = 1234
     * $query->filterByTypeTelechargement(array(12, 34)); // WHERE type_telechargement IN (12, 34)
     * $query->filterByTypeTelechargement(array('min' => 12)); // WHERE type_telechargement >= 12
     * $query->filterByTypeTelechargement(array('max' => 12)); // WHERE type_telechargement <= 12
     * </code>
     *
     * @param     mixed $typeTelechargement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function filterByTypeTelechargement($typeTelechargement = null, $comparison = null)
    {
        if (is_array($typeTelechargement)) {
            $useMinMax = false;
            if (isset($typeTelechargement['min'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT, $typeTelechargement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeTelechargement['max'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT, $typeTelechargement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT, $typeTelechargement, $comparison);
    }

    /**
     * Filter the query by a related CommonService object
     *
     * @param   CommonService|PropelObjectCollection $commonService The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonService($commonService, $comparison = null)
    {
        if ($commonService instanceof CommonService) {
            return $this
                ->addUsingAlias(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, $commonService->getId(), $comparison);
        } elseif ($commonService instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, $commonService->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonService() only accepts arguments of type CommonService or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonService relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function joinCommonService($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonService');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonService');
        }

        return $this;
    }

    /**
     * Use the CommonService relation CommonService object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonServiceQuery A secondary query class using the current class as primary query
     */
    public function useCommonServiceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonService($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonService', '\Application\Propel\Mpe\CommonServiceQuery');
    }

    /**
     * Filter the query by a related CommonTTelechargementAsynchroneFichier object
     *
     * @param   CommonTTelechargementAsynchroneFichier|PropelObjectCollection $commonTTelechargementAsynchroneFichier  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTTelechargementAsynchroneFichier($commonTTelechargementAsynchroneFichier, $comparison = null)
    {
        if ($commonTTelechargementAsynchroneFichier instanceof CommonTTelechargementAsynchroneFichier) {
            return $this
                ->addUsingAlias(CommonTTelechargementAsynchronePeer::ID, $commonTTelechargementAsynchroneFichier->getIdTelechargementAsynchrone(), $comparison);
        } elseif ($commonTTelechargementAsynchroneFichier instanceof PropelObjectCollection) {
            return $this
                ->useCommonTTelechargementAsynchroneFichierQuery()
                ->filterByPrimaryKeys($commonTTelechargementAsynchroneFichier->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTTelechargementAsynchroneFichier() only accepts arguments of type CommonTTelechargementAsynchroneFichier or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTTelechargementAsynchroneFichier relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function joinCommonTTelechargementAsynchroneFichier($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTTelechargementAsynchroneFichier');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTTelechargementAsynchroneFichier');
        }

        return $this;
    }

    /**
     * Use the CommonTTelechargementAsynchroneFichier relation CommonTTelechargementAsynchroneFichier object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTTelechargementAsynchroneFichierQuery A secondary query class using the current class as primary query
     */
    public function useCommonTTelechargementAsynchroneFichierQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTTelechargementAsynchroneFichier($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTTelechargementAsynchroneFichier', '\Application\Propel\Mpe\CommonTTelechargementAsynchroneFichierQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTTelechargementAsynchrone $commonTTelechargementAsynchrone Object to remove from the list of results
     *
     * @return CommonTTelechargementAsynchroneQuery The current query, for fluid interface
     */
    public function prune($commonTTelechargementAsynchrone = null)
    {
        if ($commonTTelechargementAsynchrone) {
            $this->addUsingAlias(CommonTTelechargementAsynchronePeer::ID, $commonTTelechargementAsynchrone->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
