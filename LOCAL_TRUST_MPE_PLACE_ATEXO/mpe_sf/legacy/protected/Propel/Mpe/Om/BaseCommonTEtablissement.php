<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritQuery;
use Application\Propel\Mpe\CommonModificationContrat;
use Application\Propel\Mpe\CommonModificationContratQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprise;
use Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntrepriseQuery;

/**
 * Base class that represents a row from the 't_etablissement' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTEtablissement extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTEtablissementPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTEtablissementPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_etablissement field.
     * @var        int
     */
    protected $id_etablissement;

    /**
     * The value for the id_entreprise field.
     * @var        int
     */
    protected $id_entreprise;

    /**
     * The value for the code_etablissement field.
     * @var        string
     */
    protected $code_etablissement;

    /**
     * The value for the est_siege field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $est_siege;

    /**
     * The value for the adresse field.
     * @var        string
     */
    protected $adresse;

    /**
     * The value for the adresse2 field.
     * @var        string
     */
    protected $adresse2;

    /**
     * The value for the code_postal field.
     * @var        string
     */
    protected $code_postal;

    /**
     * The value for the ville field.
     * @var        string
     */
    protected $ville;

    /**
     * The value for the pays field.
     * @var        string
     */
    protected $pays;

    /**
     * The value for the saisie_manuelle field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $saisie_manuelle;

    /**
     * The value for the id_initial field.
     * @var        int
     */
    protected $id_initial;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_modification field.
     * @var        string
     */
    protected $date_modification;

    /**
     * The value for the date_suppression field.
     * @var        string
     */
    protected $date_suppression;

    /**
     * The value for the statut_actif field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $statut_actif;

    /**
     * The value for the inscrit_annuaire_defense field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $inscrit_annuaire_defense;

    /**
     * The value for the long field.
     * @var        double
     */
    protected $long;

    /**
     * The value for the lat field.
     * @var        double
     */
    protected $lat;

    /**
     * The value for the maj_long_lat field.
     * @var        string
     */
    protected $maj_long_lat;

    /**
     * The value for the tva_intracommunautaire field.
     * @var        string
     */
    protected $tva_intracommunautaire;

    /**
     * The value for the etat_administratif field.
     * @var        string
     */
    protected $etat_administratif;

    /**
     * The value for the date_fermeture field.
     * @var        string
     */
    protected $date_fermeture;

    /**
     * The value for the id_externe field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $id_externe;

    /**
     * @var        Entreprise
     */
    protected $aEntreprise;

    /**
     * @var        PropelObjectCollection|CommonInscrit[] Collection to store aggregation of CommonInscrit objects.
     */
    protected $collCommonInscritsRelatedByIdEtablissement;
    protected $collCommonInscritsRelatedByIdEtablissementPartial;


    /**
     * @var        PropelObjectCollection|CommonModificationContrat[] Collection to store aggregation of CommonModificationContrat objects.
     */
    protected $collCommonModificationContrats;
    protected $collCommonModificationContratsPartial;

    /**
     * @var        PropelObjectCollection|CommonTMembreGroupementEntreprise[] Collection to store aggregation of CommonTMembreGroupementEntreprise objects.
     */
    protected $collCommonTMembreGroupementEntreprises;
    protected $collCommonTMembreGroupementEntreprisesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonInscritsRelatedByIdEtablissementScheduledForDeletion = null;


    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonModificationContratsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTMembreGroupementEntreprisesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->est_siege = '0';
        $this->saisie_manuelle = '0';
        $this->statut_actif = '1';
        $this->inscrit_annuaire_defense = '0';
        $this->id_externe = '0';
    }

    /**
     * Initializes internal state of BaseCommonTEtablissement object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_etablissement] column value.
     *
     * @return int
     */
    public function getIdEtablissement()
    {

        return $this->id_etablissement;
    }

    /**
     * Get the [id_entreprise] column value.
     *
     * @return int
     */
    public function getIdEntreprise()
    {

        return $this->id_entreprise;
    }

    /**
     * Get the [code_etablissement] column value.
     *
     * @return string
     */
    public function getCodeEtablissement()
    {

        return $this->code_etablissement;
    }

    /**
     * Get the [est_siege] column value.
     *
     * @return string
     */
    public function getEstSiege()
    {

        return $this->est_siege;
    }

    /**
     * Get the [adresse] column value.
     *
     * @return string
     */
    public function getAdresse()
    {

        return $this->adresse;
    }

    /**
     * Get the [adresse2] column value.
     *
     * @return string
     */
    public function getAdresse2()
    {

        return $this->adresse2;
    }

    /**
     * Get the [code_postal] column value.
     *
     * @return string
     */
    public function getCodePostal()
    {

        return $this->code_postal;
    }

    /**
     * Get the [ville] column value.
     *
     * @return string
     */
    public function getVille()
    {

        return $this->ville;
    }

    /**
     * Get the [pays] column value.
     *
     * @return string
     */
    public function getPays()
    {

        return $this->pays;
    }

    /**
     * Get the [saisie_manuelle] column value.
     *
     * @return string
     */
    public function getSaisieManuelle()
    {

        return $this->saisie_manuelle;
    }

    /**
     * Get the [id_initial] column value.
     *
     * @return int
     */
    public function getIdInitial()
    {

        return $this->id_initial;
    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_modification] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateModification($format = 'Y-m-d H:i:s')
    {
        if ($this->date_modification === null) {
            return null;
        }

        if ($this->date_modification === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_modification);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_modification, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_suppression] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateSuppression($format = 'Y-m-d H:i:s')
    {
        if ($this->date_suppression === null) {
            return null;
        }

        if ($this->date_suppression === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_suppression);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_suppression, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [statut_actif] column value.
     *
     * @return string
     */
    public function getStatutActif()
    {

        return $this->statut_actif;
    }

    /**
     * Get the [inscrit_annuaire_defense] column value.
     *
     * @return string
     */
    public function getInscritAnnuaireDefense()
    {

        return $this->inscrit_annuaire_defense;
    }

    /**
     * Get the [long] column value.
     *
     * @return double
     */
    public function getLong()
    {

        return $this->long;
    }

    /**
     * Get the [lat] column value.
     *
     * @return double
     */
    public function getLat()
    {

        return $this->lat;
    }

    /**
     * Get the [optionally formatted] temporal [maj_long_lat] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getMajLongLat($format = 'Y-m-d H:i:s')
    {
        if ($this->maj_long_lat === null) {
            return null;
        }

        if ($this->maj_long_lat === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->maj_long_lat);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->maj_long_lat, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [tva_intracommunautaire] column value.
     *
     * @return string
     */
    public function getTvaIntracommunautaire()
    {

        return $this->tva_intracommunautaire;
    }

    /**
     * Get the [etat_administratif] column value.
     *
     * @return string
     */
    public function getEtatAdministratif()
    {

        return $this->etat_administratif;
    }

    /**
     * Get the [optionally formatted] temporal [date_fermeture] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateFermeture($format = 'Y-m-d H:i:s')
    {
        if ($this->date_fermeture === null) {
            return null;
        }

        if ($this->date_fermeture === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_fermeture);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_fermeture, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [id_externe] column value.
     *
     * @return string
     */
    public function getIdExterne()
    {

        return $this->id_externe;
    }

    /**
     * Set the value of [id_etablissement] column.
     *
     * @param int $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setIdEtablissement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_etablissement !== $v) {
            $this->id_etablissement = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::ID_ETABLISSEMENT;
        }


        return $this;
    } // setIdEtablissement()

    /**
     * Set the value of [id_entreprise] column.
     *
     * @param int $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setIdEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_entreprise !== $v) {
            $this->id_entreprise = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::ID_ENTREPRISE;
        }

        if ($this->aEntreprise !== null && $this->aEntreprise->getId() !== $v) {
            $this->aEntreprise = null;
        }


        return $this;
    } // setIdEntreprise()

    /**
     * Set the value of [code_etablissement] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setCodeEtablissement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code_etablissement !== $v) {
            $this->code_etablissement = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::CODE_ETABLISSEMENT;
        }


        return $this;
    } // setCodeEtablissement()

    /**
     * Set the value of [est_siege] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setEstSiege($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->est_siege !== $v) {
            $this->est_siege = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::EST_SIEGE;
        }


        return $this;
    } // setEstSiege()

    /**
     * Set the value of [adresse] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setAdresse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse !== $v) {
            $this->adresse = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::ADRESSE;
        }


        return $this;
    } // setAdresse()

    /**
     * Set the value of [adresse2] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setAdresse2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2 !== $v) {
            $this->adresse2 = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::ADRESSE2;
        }


        return $this;
    } // setAdresse2()

    /**
     * Set the value of [code_postal] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setCodePostal($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code_postal !== $v) {
            $this->code_postal = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::CODE_POSTAL;
        }


        return $this;
    } // setCodePostal()

    /**
     * Set the value of [ville] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setVille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville !== $v) {
            $this->ville = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::VILLE;
        }


        return $this;
    } // setVille()

    /**
     * Set the value of [pays] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setPays($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays !== $v) {
            $this->pays = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::PAYS;
        }


        return $this;
    } // setPays()

    /**
     * Set the value of [saisie_manuelle] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setSaisieManuelle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->saisie_manuelle !== $v) {
            $this->saisie_manuelle = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::SAISIE_MANUELLE;
        }


        return $this;
    } // setSaisieManuelle()

    /**
     * Set the value of [id_initial] column.
     *
     * @param int $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setIdInitial($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_initial !== $v) {
            $this->id_initial = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::ID_INITIAL;
        }


        return $this;
    } // setIdInitial()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonTEtablissementPeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Sets the value of [date_modification] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setDateModification($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_modification !== null || $dt !== null) {
            $currentDateAsString = ($this->date_modification !== null && $tmpDt = new DateTime($this->date_modification)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_modification = $newDateAsString;
                $this->modifiedColumns[] = CommonTEtablissementPeer::DATE_MODIFICATION;
            }
        } // if either are not null


        return $this;
    } // setDateModification()

    /**
     * Sets the value of [date_suppression] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setDateSuppression($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_suppression !== null || $dt !== null) {
            $currentDateAsString = ($this->date_suppression !== null && $tmpDt = new DateTime($this->date_suppression)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_suppression = $newDateAsString;
                $this->modifiedColumns[] = CommonTEtablissementPeer::DATE_SUPPRESSION;
            }
        } // if either are not null


        return $this;
    } // setDateSuppression()

    /**
     * Set the value of [statut_actif] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setStatutActif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->statut_actif !== $v) {
            $this->statut_actif = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::STATUT_ACTIF;
        }


        return $this;
    } // setStatutActif()

    /**
     * Set the value of [inscrit_annuaire_defense] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setInscritAnnuaireDefense($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->inscrit_annuaire_defense !== $v) {
            $this->inscrit_annuaire_defense = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE;
        }


        return $this;
    } // setInscritAnnuaireDefense()

    /**
     * Set the value of [long] column.
     *
     * @param double $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setLong($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->long !== $v) {
            $this->long = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::LONG;
        }


        return $this;
    } // setLong()

    /**
     * Set the value of [lat] column.
     *
     * @param double $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setLat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (double) $v;
        }

        if ($this->lat !== $v) {
            $this->lat = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::LAT;
        }


        return $this;
    } // setLat()

    /**
     * Sets the value of [maj_long_lat] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setMajLongLat($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->maj_long_lat !== null || $dt !== null) {
            $currentDateAsString = ($this->maj_long_lat !== null && $tmpDt = new DateTime($this->maj_long_lat)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->maj_long_lat = $newDateAsString;
                $this->modifiedColumns[] = CommonTEtablissementPeer::MAJ_LONG_LAT;
            }
        } // if either are not null


        return $this;
    } // setMajLongLat()

    /**
     * Set the value of [tva_intracommunautaire] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setTvaIntracommunautaire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tva_intracommunautaire !== $v) {
            $this->tva_intracommunautaire = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::TVA_INTRACOMMUNAUTAIRE;
        }


        return $this;
    } // setTvaIntracommunautaire()

    /**
     * Set the value of [etat_administratif] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setEtatAdministratif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->etat_administratif !== $v) {
            $this->etat_administratif = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::ETAT_ADMINISTRATIF;
        }


        return $this;
    } // setEtatAdministratif()

    /**
     * Sets the value of [date_fermeture] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setDateFermeture($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_fermeture !== null || $dt !== null) {
            $currentDateAsString = ($this->date_fermeture !== null && $tmpDt = new DateTime($this->date_fermeture)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_fermeture = $newDateAsString;
                $this->modifiedColumns[] = CommonTEtablissementPeer::DATE_FERMETURE;
            }
        } // if either are not null


        return $this;
    } // setDateFermeture()

    /**
     * Set the value of [id_externe] column.
     *
     * @param string $v new value
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setIdExterne($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_externe !== $v) {
            $this->id_externe = $v;
            $this->modifiedColumns[] = CommonTEtablissementPeer::ID_EXTERNE;
        }


        return $this;
    } // setIdExterne()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->est_siege !== '0') {
                return false;
            }

            if ($this->saisie_manuelle !== '0') {
                return false;
            }

            if ($this->statut_actif !== '1') {
                return false;
            }

            if ($this->inscrit_annuaire_defense !== '0') {
                return false;
            }

            if ($this->id_externe !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_etablissement = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_entreprise = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->code_etablissement = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->est_siege = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->adresse = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->adresse2 = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->code_postal = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->ville = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->pays = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->saisie_manuelle = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->id_initial = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->date_creation = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->date_modification = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->date_suppression = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->statut_actif = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->inscrit_annuaire_defense = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->long = ($row[$startcol + 16] !== null) ? (double) $row[$startcol + 16] : null;
            $this->lat = ($row[$startcol + 17] !== null) ? (double) $row[$startcol + 17] : null;
            $this->maj_long_lat = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->tva_intracommunautaire = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->etat_administratif = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->date_fermeture = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->id_externe = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 23; // 23 = CommonTEtablissementPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTEtablissement object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aEntreprise !== null && $this->id_entreprise !== $this->aEntreprise->getId()) {
            $this->aEntreprise = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTEtablissementPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEntreprise = null;
            $this->collCommonInscritsRelatedByIdEtablissement = null;

            $this->collCommonInscritsRelatedByIdEtablissement = null;

            $this->collCommonModificationContrats = null;

            $this->collCommonTMembreGroupementEntreprises = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTEtablissementQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTEtablissementPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTEtablissementPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEntreprise !== null) {
                if ($this->aEntreprise->isModified() || $this->aEntreprise->isNew()) {
                    $affectedRows += $this->aEntreprise->save($con);
                }
                $this->setEntreprise($this->aEntreprise);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonInscritsRelatedByIdEtablissementScheduledForDeletion !== null) {
                if (!$this->commonInscritsRelatedByIdEtablissementScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonInscritsRelatedByIdEtablissementScheduledForDeletion as $commonInscritRelatedByIdEtablissement) {
                        // need to save related object because we set the relation to null
                        $commonInscritRelatedByIdEtablissement->save($con);
                    }
                    $this->commonInscritsRelatedByIdEtablissementScheduledForDeletion = null;
                }
            }

            if ($this->collCommonInscritsRelatedByIdEtablissement !== null) {
                foreach ($this->collCommonInscritsRelatedByIdEtablissement as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonInscritsRelatedByIdEtablissementScheduledForDeletion !== null) {
                if (!$this->commonInscritsRelatedByIdEtablissementScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonInscritsRelatedByIdEtablissementScheduledForDeletion as $commonInscritRelatedByIdEtablissement) {
                        // need to save related object because we set the relation to null
                        $commonInscritRelatedByIdEtablissement->save($con);
                    }
                    $this->commonInscritsRelatedByIdEtablissementScheduledForDeletion = null;
                }
            }

            if ($this->collCommonInscritsRelatedByIdEtablissement !== null) {
                foreach ($this->collCommonInscritsRelatedByIdEtablissement as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonModificationContratsScheduledForDeletion !== null) {
                if (!$this->commonModificationContratsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonModificationContratsScheduledForDeletion as $commonModificationContrat) {
                        // need to save related object because we set the relation to null
                        $commonModificationContrat->save($con);
                    }
                    $this->commonModificationContratsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonModificationContrats !== null) {
                foreach ($this->collCommonModificationContrats as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTMembreGroupementEntreprisesScheduledForDeletion !== null) {
                if (!$this->commonTMembreGroupementEntreprisesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTMembreGroupementEntreprisesScheduledForDeletion as $commonTMembreGroupementEntreprise) {
                        // need to save related object because we set the relation to null
                        $commonTMembreGroupementEntreprise->save($con);
                    }
                    $this->commonTMembreGroupementEntreprisesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTMembreGroupementEntreprises !== null) {
                foreach ($this->collCommonTMembreGroupementEntreprises as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTEtablissementPeer::ID_ETABLISSEMENT;
        if (null !== $this->id_etablissement) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTEtablissementPeer::ID_ETABLISSEMENT . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTEtablissementPeer::ID_ETABLISSEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_etablissement`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::ID_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entreprise`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::CODE_ETABLISSEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`code_etablissement`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::EST_SIEGE)) {
            $modifiedColumns[':p' . $index++]  = '`est_siege`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`adresse`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::ADRESSE2)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::CODE_POSTAL)) {
            $modifiedColumns[':p' . $index++]  = '`code_postal`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`ville`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::PAYS)) {
            $modifiedColumns[':p' . $index++]  = '`pays`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::SAISIE_MANUELLE)) {
            $modifiedColumns[':p' . $index++]  = '`saisie_manuelle`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::ID_INITIAL)) {
            $modifiedColumns[':p' . $index++]  = '`id_initial`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::DATE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::DATE_SUPPRESSION)) {
            $modifiedColumns[':p' . $index++]  = '`date_suppression`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::STATUT_ACTIF)) {
            $modifiedColumns[':p' . $index++]  = '`statut_actif`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE)) {
            $modifiedColumns[':p' . $index++]  = '`inscrit_annuaire_defense`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::LONG)) {
            $modifiedColumns[':p' . $index++]  = '`long`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::LAT)) {
            $modifiedColumns[':p' . $index++]  = '`lat`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::MAJ_LONG_LAT)) {
            $modifiedColumns[':p' . $index++]  = '`maj_long_lat`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::TVA_INTRACOMMUNAUTAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`tva_intracommunautaire`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::ETAT_ADMINISTRATIF)) {
            $modifiedColumns[':p' . $index++]  = '`etat_administratif`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::DATE_FERMETURE)) {
            $modifiedColumns[':p' . $index++]  = '`date_fermeture`';
        }
        if ($this->isColumnModified(CommonTEtablissementPeer::ID_EXTERNE)) {
            $modifiedColumns[':p' . $index++]  = '`id_externe`';
        }

        $sql = sprintf(
            'INSERT INTO `t_etablissement` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_etablissement`':
                        $stmt->bindValue($identifier, $this->id_etablissement, PDO::PARAM_INT);
                        break;
                    case '`id_entreprise`':
                        $stmt->bindValue($identifier, $this->id_entreprise, PDO::PARAM_INT);
                        break;
                    case '`code_etablissement`':
                        $stmt->bindValue($identifier, $this->code_etablissement, PDO::PARAM_STR);
                        break;
                    case '`est_siege`':
                        $stmt->bindValue($identifier, $this->est_siege, PDO::PARAM_STR);
                        break;
                    case '`adresse`':
                        $stmt->bindValue($identifier, $this->adresse, PDO::PARAM_STR);
                        break;
                    case '`adresse2`':
                        $stmt->bindValue($identifier, $this->adresse2, PDO::PARAM_STR);
                        break;
                    case '`code_postal`':
                        $stmt->bindValue($identifier, $this->code_postal, PDO::PARAM_STR);
                        break;
                    case '`ville`':
                        $stmt->bindValue($identifier, $this->ville, PDO::PARAM_STR);
                        break;
                    case '`pays`':
                        $stmt->bindValue($identifier, $this->pays, PDO::PARAM_STR);
                        break;
                    case '`saisie_manuelle`':
                        $stmt->bindValue($identifier, $this->saisie_manuelle, PDO::PARAM_STR);
                        break;
                    case '`id_initial`':
                        $stmt->bindValue($identifier, $this->id_initial, PDO::PARAM_INT);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_modification`':
                        $stmt->bindValue($identifier, $this->date_modification, PDO::PARAM_STR);
                        break;
                    case '`date_suppression`':
                        $stmt->bindValue($identifier, $this->date_suppression, PDO::PARAM_STR);
                        break;
                    case '`statut_actif`':
                        $stmt->bindValue($identifier, $this->statut_actif, PDO::PARAM_STR);
                        break;
                    case '`inscrit_annuaire_defense`':
                        $stmt->bindValue($identifier, $this->inscrit_annuaire_defense, PDO::PARAM_STR);
                        break;
                    case '`long`':
                        $stmt->bindValue($identifier, $this->long, PDO::PARAM_STR);
                        break;
                    case '`lat`':
                        $stmt->bindValue($identifier, $this->lat, PDO::PARAM_STR);
                        break;
                    case '`maj_long_lat`':
                        $stmt->bindValue($identifier, $this->maj_long_lat, PDO::PARAM_STR);
                        break;
                    case '`tva_intracommunautaire`':
                        $stmt->bindValue($identifier, $this->tva_intracommunautaire, PDO::PARAM_STR);
                        break;
                    case '`etat_administratif`':
                        $stmt->bindValue($identifier, $this->etat_administratif, PDO::PARAM_STR);
                        break;
                    case '`date_fermeture`':
                        $stmt->bindValue($identifier, $this->date_fermeture, PDO::PARAM_STR);
                        break;
                    case '`id_externe`':
                        $stmt->bindValue($identifier, $this->id_externe, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdEtablissement($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEntreprise !== null) {
                if (!$this->aEntreprise->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aEntreprise->getValidationFailures());
                }
            }


            if (($retval = CommonTEtablissementPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonInscritsRelatedByIdEtablissement !== null) {
                    foreach ($this->collCommonInscritsRelatedByIdEtablissement as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonInscritsRelatedByIdEtablissement !== null) {
                    foreach ($this->collCommonInscritsRelatedByIdEtablissement as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonModificationContrats !== null) {
                    foreach ($this->collCommonModificationContrats as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTMembreGroupementEntreprises !== null) {
                    foreach ($this->collCommonTMembreGroupementEntreprises as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTEtablissementPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdEtablissement();
                break;
            case 1:
                return $this->getIdEntreprise();
                break;
            case 2:
                return $this->getCodeEtablissement();
                break;
            case 3:
                return $this->getEstSiege();
                break;
            case 4:
                return $this->getAdresse();
                break;
            case 5:
                return $this->getAdresse2();
                break;
            case 6:
                return $this->getCodePostal();
                break;
            case 7:
                return $this->getVille();
                break;
            case 8:
                return $this->getPays();
                break;
            case 9:
                return $this->getSaisieManuelle();
                break;
            case 10:
                return $this->getIdInitial();
                break;
            case 11:
                return $this->getDateCreation();
                break;
            case 12:
                return $this->getDateModification();
                break;
            case 13:
                return $this->getDateSuppression();
                break;
            case 14:
                return $this->getStatutActif();
                break;
            case 15:
                return $this->getInscritAnnuaireDefense();
                break;
            case 16:
                return $this->getLong();
                break;
            case 17:
                return $this->getLat();
                break;
            case 18:
                return $this->getMajLongLat();
                break;
            case 19:
                return $this->getTvaIntracommunautaire();
                break;
            case 20:
                return $this->getEtatAdministratif();
                break;
            case 21:
                return $this->getDateFermeture();
                break;
            case 22:
                return $this->getIdExterne();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTEtablissement'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTEtablissement'][$this->getPrimaryKey()] = true;
        $keys = CommonTEtablissementPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdEtablissement(),
            $keys[1] => $this->getIdEntreprise(),
            $keys[2] => $this->getCodeEtablissement(),
            $keys[3] => $this->getEstSiege(),
            $keys[4] => $this->getAdresse(),
            $keys[5] => $this->getAdresse2(),
            $keys[6] => $this->getCodePostal(),
            $keys[7] => $this->getVille(),
            $keys[8] => $this->getPays(),
            $keys[9] => $this->getSaisieManuelle(),
            $keys[10] => $this->getIdInitial(),
            $keys[11] => $this->getDateCreation(),
            $keys[12] => $this->getDateModification(),
            $keys[13] => $this->getDateSuppression(),
            $keys[14] => $this->getStatutActif(),
            $keys[15] => $this->getInscritAnnuaireDefense(),
            $keys[16] => $this->getLong(),
            $keys[17] => $this->getLat(),
            $keys[18] => $this->getMajLongLat(),
            $keys[19] => $this->getTvaIntracommunautaire(),
            $keys[20] => $this->getEtatAdministratif(),
            $keys[21] => $this->getDateFermeture(),
            $keys[22] => $this->getIdExterne(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aEntreprise) {
                $result['Entreprise'] = $this->aEntreprise->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonInscritsRelatedByIdEtablissement) {
                $result['CommonInscritsRelatedByIdEtablissement'] = $this->collCommonInscritsRelatedByIdEtablissement->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonInscritsRelatedByIdEtablissement) {
                $result['CommonInscritsRelatedByIdEtablissement'] = $this->collCommonInscritsRelatedByIdEtablissement->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonModificationContrats) {
                $result['CommonModificationContrats'] = $this->collCommonModificationContrats->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTMembreGroupementEntreprises) {
                $result['CommonTMembreGroupementEntreprises'] = $this->collCommonTMembreGroupementEntreprises->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTEtablissementPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdEtablissement($value);
                break;
            case 1:
                $this->setIdEntreprise($value);
                break;
            case 2:
                $this->setCodeEtablissement($value);
                break;
            case 3:
                $this->setEstSiege($value);
                break;
            case 4:
                $this->setAdresse($value);
                break;
            case 5:
                $this->setAdresse2($value);
                break;
            case 6:
                $this->setCodePostal($value);
                break;
            case 7:
                $this->setVille($value);
                break;
            case 8:
                $this->setPays($value);
                break;
            case 9:
                $this->setSaisieManuelle($value);
                break;
            case 10:
                $this->setIdInitial($value);
                break;
            case 11:
                $this->setDateCreation($value);
                break;
            case 12:
                $this->setDateModification($value);
                break;
            case 13:
                $this->setDateSuppression($value);
                break;
            case 14:
                $this->setStatutActif($value);
                break;
            case 15:
                $this->setInscritAnnuaireDefense($value);
                break;
            case 16:
                $this->setLong($value);
                break;
            case 17:
                $this->setLat($value);
                break;
            case 18:
                $this->setMajLongLat($value);
                break;
            case 19:
                $this->setTvaIntracommunautaire($value);
                break;
            case 20:
                $this->setEtatAdministratif($value);
                break;
            case 21:
                $this->setDateFermeture($value);
                break;
            case 22:
                $this->setIdExterne($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTEtablissementPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdEtablissement($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdEntreprise($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCodeEtablissement($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setEstSiege($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setAdresse($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setAdresse2($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCodePostal($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setVille($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setPays($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setSaisieManuelle($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setIdInitial($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setDateCreation($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setDateModification($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setDateSuppression($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setStatutActif($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setInscritAnnuaireDefense($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setLong($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setLat($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setMajLongLat($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setTvaIntracommunautaire($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setEtatAdministratif($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setDateFermeture($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setIdExterne($arr[$keys[22]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTEtablissementPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTEtablissementPeer::ID_ETABLISSEMENT)) $criteria->add(CommonTEtablissementPeer::ID_ETABLISSEMENT, $this->id_etablissement);
        if ($this->isColumnModified(CommonTEtablissementPeer::ID_ENTREPRISE)) $criteria->add(CommonTEtablissementPeer::ID_ENTREPRISE, $this->id_entreprise);
        if ($this->isColumnModified(CommonTEtablissementPeer::CODE_ETABLISSEMENT)) $criteria->add(CommonTEtablissementPeer::CODE_ETABLISSEMENT, $this->code_etablissement);
        if ($this->isColumnModified(CommonTEtablissementPeer::EST_SIEGE)) $criteria->add(CommonTEtablissementPeer::EST_SIEGE, $this->est_siege);
        if ($this->isColumnModified(CommonTEtablissementPeer::ADRESSE)) $criteria->add(CommonTEtablissementPeer::ADRESSE, $this->adresse);
        if ($this->isColumnModified(CommonTEtablissementPeer::ADRESSE2)) $criteria->add(CommonTEtablissementPeer::ADRESSE2, $this->adresse2);
        if ($this->isColumnModified(CommonTEtablissementPeer::CODE_POSTAL)) $criteria->add(CommonTEtablissementPeer::CODE_POSTAL, $this->code_postal);
        if ($this->isColumnModified(CommonTEtablissementPeer::VILLE)) $criteria->add(CommonTEtablissementPeer::VILLE, $this->ville);
        if ($this->isColumnModified(CommonTEtablissementPeer::PAYS)) $criteria->add(CommonTEtablissementPeer::PAYS, $this->pays);
        if ($this->isColumnModified(CommonTEtablissementPeer::SAISIE_MANUELLE)) $criteria->add(CommonTEtablissementPeer::SAISIE_MANUELLE, $this->saisie_manuelle);
        if ($this->isColumnModified(CommonTEtablissementPeer::ID_INITIAL)) $criteria->add(CommonTEtablissementPeer::ID_INITIAL, $this->id_initial);
        if ($this->isColumnModified(CommonTEtablissementPeer::DATE_CREATION)) $criteria->add(CommonTEtablissementPeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonTEtablissementPeer::DATE_MODIFICATION)) $criteria->add(CommonTEtablissementPeer::DATE_MODIFICATION, $this->date_modification);
        if ($this->isColumnModified(CommonTEtablissementPeer::DATE_SUPPRESSION)) $criteria->add(CommonTEtablissementPeer::DATE_SUPPRESSION, $this->date_suppression);
        if ($this->isColumnModified(CommonTEtablissementPeer::STATUT_ACTIF)) $criteria->add(CommonTEtablissementPeer::STATUT_ACTIF, $this->statut_actif);
        if ($this->isColumnModified(CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE)) $criteria->add(CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE, $this->inscrit_annuaire_defense);
        if ($this->isColumnModified(CommonTEtablissementPeer::LONG)) $criteria->add(CommonTEtablissementPeer::LONG, $this->long);
        if ($this->isColumnModified(CommonTEtablissementPeer::LAT)) $criteria->add(CommonTEtablissementPeer::LAT, $this->lat);
        if ($this->isColumnModified(CommonTEtablissementPeer::MAJ_LONG_LAT)) $criteria->add(CommonTEtablissementPeer::MAJ_LONG_LAT, $this->maj_long_lat);
        if ($this->isColumnModified(CommonTEtablissementPeer::TVA_INTRACOMMUNAUTAIRE)) $criteria->add(CommonTEtablissementPeer::TVA_INTRACOMMUNAUTAIRE, $this->tva_intracommunautaire);
        if ($this->isColumnModified(CommonTEtablissementPeer::ETAT_ADMINISTRATIF)) $criteria->add(CommonTEtablissementPeer::ETAT_ADMINISTRATIF, $this->etat_administratif);
        if ($this->isColumnModified(CommonTEtablissementPeer::DATE_FERMETURE)) $criteria->add(CommonTEtablissementPeer::DATE_FERMETURE, $this->date_fermeture);
        if ($this->isColumnModified(CommonTEtablissementPeer::ID_EXTERNE)) $criteria->add(CommonTEtablissementPeer::ID_EXTERNE, $this->id_externe);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTEtablissementPeer::DATABASE_NAME);
        $criteria->add(CommonTEtablissementPeer::ID_ETABLISSEMENT, $this->id_etablissement);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdEtablissement();
    }

    /**
     * Generic method to set the primary key (id_etablissement column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdEtablissement($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdEtablissement();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTEtablissement (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdEntreprise($this->getIdEntreprise());
        $copyObj->setCodeEtablissement($this->getCodeEtablissement());
        $copyObj->setEstSiege($this->getEstSiege());
        $copyObj->setAdresse($this->getAdresse());
        $copyObj->setAdresse2($this->getAdresse2());
        $copyObj->setCodePostal($this->getCodePostal());
        $copyObj->setVille($this->getVille());
        $copyObj->setPays($this->getPays());
        $copyObj->setSaisieManuelle($this->getSaisieManuelle());
        $copyObj->setIdInitial($this->getIdInitial());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateModification($this->getDateModification());
        $copyObj->setDateSuppression($this->getDateSuppression());
        $copyObj->setStatutActif($this->getStatutActif());
        $copyObj->setInscritAnnuaireDefense($this->getInscritAnnuaireDefense());
        $copyObj->setLong($this->getLong());
        $copyObj->setLat($this->getLat());
        $copyObj->setMajLongLat($this->getMajLongLat());
        $copyObj->setTvaIntracommunautaire($this->getTvaIntracommunautaire());
        $copyObj->setEtatAdministratif($this->getEtatAdministratif());
        $copyObj->setDateFermeture($this->getDateFermeture());
        $copyObj->setIdExterne($this->getIdExterne());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonInscritsRelatedByIdEtablissement() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonInscritRelatedByIdEtablissement($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonInscritsRelatedByIdEtablissement() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonInscritRelatedByIdEtablissement($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonModificationContrats() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonModificationContrat($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTMembreGroupementEntreprises() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTMembreGroupementEntreprise($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdEtablissement(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTEtablissement Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTEtablissementPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTEtablissementPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a Entreprise object.
     *
     * @param   Entreprise $v
     * @return CommonTEtablissement The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntreprise(Entreprise $v = null)
    {
        if ($v === null) {
            $this->setIdEntreprise(NULL);
        } else {
            $this->setIdEntreprise($v->getId());
        }

        $this->aEntreprise = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Entreprise object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTEtablissement($this);
        }


        return $this;
    }


    /**
     * Get the associated Entreprise object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Entreprise The associated Entreprise object.
     * @throws PropelException
     */
    public function getEntreprise(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aEntreprise === null && ($this->id_entreprise !== null) && $doQuery) {
            $this->aEntreprise = EntrepriseQuery::create()->findPk($this->id_entreprise, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntreprise->addCommonTEtablissements($this);
             */
        }

        return $this->aEntreprise;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonInscritRelatedByIdEtablissement' == $relationName) {
            $this->initCommonInscritsRelatedByIdEtablissement();
        }
        if ('CommonInscritRelatedByIdEtablissement' == $relationName) {
            $this->initCommonInscritsRelatedByIdEtablissement();
        }
        if ('CommonModificationContrat' == $relationName) {
            $this->initCommonModificationContrats();
        }
        if ('CommonTMembreGroupementEntreprise' == $relationName) {
            $this->initCommonTMembreGroupementEntreprises();
        }
    }

    /**
     * Clears out the collCommonInscritsRelatedByIdEtablissement collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTEtablissement The current object (for fluent API support)
     * @see        addCommonInscritsRelatedByIdEtablissement()
     */
    public function clearCommonInscritsRelatedByIdEtablissement()
    {
        $this->collCommonInscritsRelatedByIdEtablissement = null; // important to set this to null since that means it is uninitialized
        $this->collCommonInscritsRelatedByIdEtablissementPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonInscritsRelatedByIdEtablissement collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonInscritsRelatedByIdEtablissement($v = true)
    {
        $this->collCommonInscritsRelatedByIdEtablissementPartial = $v;
    }

    /**
     * Initializes the collCommonInscritsRelatedByIdEtablissement collection.
     *
     * By default this just sets the collCommonInscritsRelatedByIdEtablissement collection to an empty array (like clearcollCommonInscritsRelatedByIdEtablissement());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonInscritsRelatedByIdEtablissement($overrideExisting = true)
    {
        if (null !== $this->collCommonInscritsRelatedByIdEtablissement && !$overrideExisting) {
            return;
        }
        $this->collCommonInscritsRelatedByIdEtablissement = new PropelObjectCollection();
        $this->collCommonInscritsRelatedByIdEtablissement->setModel('CommonInscrit');
    }

    /**
     * Gets an array of CommonInscrit objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTEtablissement is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonInscrit[] List of CommonInscrit objects
     * @throws PropelException
     */
    public function getCommonInscritsRelatedByIdEtablissement($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonInscritsRelatedByIdEtablissementPartial && !$this->isNew();
        if (null === $this->collCommonInscritsRelatedByIdEtablissement || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonInscritsRelatedByIdEtablissement) {
                // return empty collection
                $this->initCommonInscritsRelatedByIdEtablissement();
            } else {
                $collCommonInscritsRelatedByIdEtablissement = CommonInscritQuery::create(null, $criteria)
                    ->filterByCommonTEtablissementRelatedByIdEtablissement($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonInscritsRelatedByIdEtablissementPartial && count($collCommonInscritsRelatedByIdEtablissement)) {
                      $this->initCommonInscritsRelatedByIdEtablissement(false);

                      foreach ($collCommonInscritsRelatedByIdEtablissement as $obj) {
                        if (false == $this->collCommonInscritsRelatedByIdEtablissement->contains($obj)) {
                          $this->collCommonInscritsRelatedByIdEtablissement->append($obj);
                        }
                      }

                      $this->collCommonInscritsRelatedByIdEtablissementPartial = true;
                    }

                    $collCommonInscritsRelatedByIdEtablissement->getInternalIterator()->rewind();

                    return $collCommonInscritsRelatedByIdEtablissement;
                }

                if ($partial && $this->collCommonInscritsRelatedByIdEtablissement) {
                    foreach ($this->collCommonInscritsRelatedByIdEtablissement as $obj) {
                        if ($obj->isNew()) {
                            $collCommonInscritsRelatedByIdEtablissement[] = $obj;
                        }
                    }
                }

                $this->collCommonInscritsRelatedByIdEtablissement = $collCommonInscritsRelatedByIdEtablissement;
                $this->collCommonInscritsRelatedByIdEtablissementPartial = false;
            }
        }

        return $this->collCommonInscritsRelatedByIdEtablissement;
    }

    /**
     * Sets a collection of CommonInscritRelatedByIdEtablissement objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonInscritsRelatedByIdEtablissement A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setCommonInscritsRelatedByIdEtablissement(PropelCollection $commonInscritsRelatedByIdEtablissement, PropelPDO $con = null)
    {
        $commonInscritsRelatedByIdEtablissementToDelete = $this->getCommonInscritsRelatedByIdEtablissement(new Criteria(), $con)->diff($commonInscritsRelatedByIdEtablissement);


        $this->commonInscritsRelatedByIdEtablissementScheduledForDeletion = $commonInscritsRelatedByIdEtablissementToDelete;

        foreach ($commonInscritsRelatedByIdEtablissementToDelete as $commonInscritRelatedByIdEtablissementRemoved) {
            $commonInscritRelatedByIdEtablissementRemoved->setCommonTEtablissementRelatedByIdEtablissement(null);
        }

        $this->collCommonInscritsRelatedByIdEtablissement = null;
        foreach ($commonInscritsRelatedByIdEtablissement as $commonInscritRelatedByIdEtablissement) {
            $this->addCommonInscritRelatedByIdEtablissement($commonInscritRelatedByIdEtablissement);
        }

        $this->collCommonInscritsRelatedByIdEtablissement = $commonInscritsRelatedByIdEtablissement;
        $this->collCommonInscritsRelatedByIdEtablissementPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonInscrit objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonInscrit objects.
     * @throws PropelException
     */
    public function countCommonInscritsRelatedByIdEtablissement(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonInscritsRelatedByIdEtablissementPartial && !$this->isNew();
        if (null === $this->collCommonInscritsRelatedByIdEtablissement || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonInscritsRelatedByIdEtablissement) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonInscritsRelatedByIdEtablissement());
            }
            $query = CommonInscritQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTEtablissementRelatedByIdEtablissement($this)
                ->count($con);
        }

        return count($this->collCommonInscritsRelatedByIdEtablissement);
    }

    /**
     * Method called to associate a CommonInscrit object to this object
     * through the CommonInscrit foreign key attribute.
     *
     * @param   CommonInscrit $l CommonInscrit
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function addCommonInscritRelatedByIdEtablissement(CommonInscrit $l)
    {
        if ($this->collCommonInscritsRelatedByIdEtablissement === null) {
            $this->initCommonInscritsRelatedByIdEtablissement();
            $this->collCommonInscritsRelatedByIdEtablissementPartial = true;
        }
        if (!in_array($l, $this->collCommonInscritsRelatedByIdEtablissement->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonInscritRelatedByIdEtablissement($l);
        }

        return $this;
    }

    /**
     * @param	CommonInscritRelatedByIdEtablissement $commonInscritRelatedByIdEtablissement The commonInscritRelatedByIdEtablissement object to add.
     */
    protected function doAddCommonInscritRelatedByIdEtablissement($commonInscritRelatedByIdEtablissement)
    {
        $this->collCommonInscritsRelatedByIdEtablissement[]= $commonInscritRelatedByIdEtablissement;
        $commonInscritRelatedByIdEtablissement->setCommonTEtablissementRelatedByIdEtablissement($this);
    }

    /**
     * @param	CommonInscritRelatedByIdEtablissement $commonInscritRelatedByIdEtablissement The commonInscritRelatedByIdEtablissement object to remove.
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function removeCommonInscritRelatedByIdEtablissement($commonInscritRelatedByIdEtablissement)
    {
        if ($this->getCommonInscritsRelatedByIdEtablissement()->contains($commonInscritRelatedByIdEtablissement)) {
            $this->collCommonInscritsRelatedByIdEtablissement->remove($this->collCommonInscritsRelatedByIdEtablissement->search($commonInscritRelatedByIdEtablissement));
            if (null === $this->commonInscritsRelatedByIdEtablissementScheduledForDeletion) {
                $this->commonInscritsRelatedByIdEtablissementScheduledForDeletion = clone $this->collCommonInscritsRelatedByIdEtablissement;
                $this->commonInscritsRelatedByIdEtablissementScheduledForDeletion->clear();
            }
            $this->commonInscritsRelatedByIdEtablissementScheduledForDeletion[]= $commonInscritRelatedByIdEtablissement;
            $commonInscritRelatedByIdEtablissement->setCommonTEtablissementRelatedByIdEtablissement(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTEtablissement is new, it will return
     * an empty collection; or if this CommonTEtablissement has previously
     * been saved, it will retrieve related CommonInscritsRelatedByIdEtablissement from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTEtablissement.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonInscrit[] List of CommonInscrit objects
     */
    public function getCommonInscritsRelatedByIdEtablissementJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonInscritQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonInscritsRelatedByIdEtablissement($query, $con);
    }

    /**
     * Clears out the collCommonModificationContrats collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTEtablissement The current object (for fluent API support)
     * @see        addCommonModificationContrats()
     */
    public function clearCommonModificationContrats()
    {
        $this->collCommonModificationContrats = null; // important to set this to null since that means it is uninitialized
        $this->collCommonModificationContratsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonModificationContrats collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonModificationContrats($v = true)
    {
        $this->collCommonModificationContratsPartial = $v;
    }

    /**
     * Initializes the collCommonModificationContrats collection.
     *
     * By default this just sets the collCommonModificationContrats collection to an empty array (like clearcollCommonModificationContrats());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonModificationContrats($overrideExisting = true)
    {
        if (null !== $this->collCommonModificationContrats && !$overrideExisting) {
            return;
        }
        $this->collCommonModificationContrats = new PropelObjectCollection();
        $this->collCommonModificationContrats->setModel('CommonModificationContrat');
    }

    /**
     * Gets an array of CommonModificationContrat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTEtablissement is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonModificationContrat[] List of CommonModificationContrat objects
     * @throws PropelException
     */
    public function getCommonModificationContrats($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonModificationContratsPartial && !$this->isNew();
        if (null === $this->collCommonModificationContrats || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonModificationContrats) {
                // return empty collection
                $this->initCommonModificationContrats();
            } else {
                $collCommonModificationContrats = CommonModificationContratQuery::create(null, $criteria)
                    ->filterByCommonTEtablissement($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonModificationContratsPartial && count($collCommonModificationContrats)) {
                      $this->initCommonModificationContrats(false);

                      foreach ($collCommonModificationContrats as $obj) {
                        if (false == $this->collCommonModificationContrats->contains($obj)) {
                          $this->collCommonModificationContrats->append($obj);
                        }
                      }

                      $this->collCommonModificationContratsPartial = true;
                    }

                    $collCommonModificationContrats->getInternalIterator()->rewind();

                    return $collCommonModificationContrats;
                }

                if ($partial && $this->collCommonModificationContrats) {
                    foreach ($this->collCommonModificationContrats as $obj) {
                        if ($obj->isNew()) {
                            $collCommonModificationContrats[] = $obj;
                        }
                    }
                }

                $this->collCommonModificationContrats = $collCommonModificationContrats;
                $this->collCommonModificationContratsPartial = false;
            }
        }

        return $this->collCommonModificationContrats;
    }

    /**
     * Sets a collection of CommonModificationContrat objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonModificationContrats A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setCommonModificationContrats(PropelCollection $commonModificationContrats, PropelPDO $con = null)
    {
        $commonModificationContratsToDelete = $this->getCommonModificationContrats(new Criteria(), $con)->diff($commonModificationContrats);


        $this->commonModificationContratsScheduledForDeletion = $commonModificationContratsToDelete;

        foreach ($commonModificationContratsToDelete as $commonModificationContratRemoved) {
            $commonModificationContratRemoved->setCommonTEtablissement(null);
        }

        $this->collCommonModificationContrats = null;
        foreach ($commonModificationContrats as $commonModificationContrat) {
            $this->addCommonModificationContrat($commonModificationContrat);
        }

        $this->collCommonModificationContrats = $commonModificationContrats;
        $this->collCommonModificationContratsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonModificationContrat objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonModificationContrat objects.
     * @throws PropelException
     */
    public function countCommonModificationContrats(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonModificationContratsPartial && !$this->isNew();
        if (null === $this->collCommonModificationContrats || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonModificationContrats) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonModificationContrats());
            }
            $query = CommonModificationContratQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTEtablissement($this)
                ->count($con);
        }

        return count($this->collCommonModificationContrats);
    }

    /**
     * Method called to associate a CommonModificationContrat object to this object
     * through the CommonModificationContrat foreign key attribute.
     *
     * @param   CommonModificationContrat $l CommonModificationContrat
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function addCommonModificationContrat(CommonModificationContrat $l)
    {
        if ($this->collCommonModificationContrats === null) {
            $this->initCommonModificationContrats();
            $this->collCommonModificationContratsPartial = true;
        }
        if (!in_array($l, $this->collCommonModificationContrats->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonModificationContrat($l);
        }

        return $this;
    }

    /**
     * @param	CommonModificationContrat $commonModificationContrat The commonModificationContrat object to add.
     */
    protected function doAddCommonModificationContrat($commonModificationContrat)
    {
        $this->collCommonModificationContrats[]= $commonModificationContrat;
        $commonModificationContrat->setCommonTEtablissement($this);
    }

    /**
     * @param	CommonModificationContrat $commonModificationContrat The commonModificationContrat object to remove.
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function removeCommonModificationContrat($commonModificationContrat)
    {
        if ($this->getCommonModificationContrats()->contains($commonModificationContrat)) {
            $this->collCommonModificationContrats->remove($this->collCommonModificationContrats->search($commonModificationContrat));
            if (null === $this->commonModificationContratsScheduledForDeletion) {
                $this->commonModificationContratsScheduledForDeletion = clone $this->collCommonModificationContrats;
                $this->commonModificationContratsScheduledForDeletion->clear();
            }
            $this->commonModificationContratsScheduledForDeletion[]= $commonModificationContrat;
            $commonModificationContrat->setCommonTEtablissement(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTEtablissement is new, it will return
     * an empty collection; or if this CommonTEtablissement has previously
     * been saved, it will retrieve related CommonModificationContrats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTEtablissement.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonModificationContrat[] List of CommonModificationContrat objects
     */
    public function getCommonModificationContratsJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonModificationContratQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonModificationContrats($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTEtablissement is new, it will return
     * an empty collection; or if this CommonTEtablissement has previously
     * been saved, it will retrieve related CommonModificationContrats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTEtablissement.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonModificationContrat[] List of CommonModificationContrat objects
     */
    public function getCommonModificationContratsJoinCommonTContratTitulaire($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonModificationContratQuery::create(null, $criteria);
        $query->joinWith('CommonTContratTitulaire', $join_behavior);

        return $this->getCommonModificationContrats($query, $con);
    }

    /**
     * Clears out the collCommonTMembreGroupementEntreprises collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTEtablissement The current object (for fluent API support)
     * @see        addCommonTMembreGroupementEntreprises()
     */
    public function clearCommonTMembreGroupementEntreprises()
    {
        $this->collCommonTMembreGroupementEntreprises = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTMembreGroupementEntreprisesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTMembreGroupementEntreprises collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTMembreGroupementEntreprises($v = true)
    {
        $this->collCommonTMembreGroupementEntreprisesPartial = $v;
    }

    /**
     * Initializes the collCommonTMembreGroupementEntreprises collection.
     *
     * By default this just sets the collCommonTMembreGroupementEntreprises collection to an empty array (like clearcollCommonTMembreGroupementEntreprises());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTMembreGroupementEntreprises($overrideExisting = true)
    {
        if (null !== $this->collCommonTMembreGroupementEntreprises && !$overrideExisting) {
            return;
        }
        $this->collCommonTMembreGroupementEntreprises = new PropelObjectCollection();
        $this->collCommonTMembreGroupementEntreprises->setModel('CommonTMembreGroupementEntreprise');
    }

    /**
     * Gets an array of CommonTMembreGroupementEntreprise objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTEtablissement is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     * @throws PropelException
     */
    public function getCommonTMembreGroupementEntreprises($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTMembreGroupementEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTMembreGroupementEntreprises || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTMembreGroupementEntreprises) {
                // return empty collection
                $this->initCommonTMembreGroupementEntreprises();
            } else {
                $collCommonTMembreGroupementEntreprises = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria)
                    ->filterByCommonTEtablissement($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTMembreGroupementEntreprisesPartial && count($collCommonTMembreGroupementEntreprises)) {
                      $this->initCommonTMembreGroupementEntreprises(false);

                      foreach ($collCommonTMembreGroupementEntreprises as $obj) {
                        if (false == $this->collCommonTMembreGroupementEntreprises->contains($obj)) {
                          $this->collCommonTMembreGroupementEntreprises->append($obj);
                        }
                      }

                      $this->collCommonTMembreGroupementEntreprisesPartial = true;
                    }

                    $collCommonTMembreGroupementEntreprises->getInternalIterator()->rewind();

                    return $collCommonTMembreGroupementEntreprises;
                }

                if ($partial && $this->collCommonTMembreGroupementEntreprises) {
                    foreach ($this->collCommonTMembreGroupementEntreprises as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTMembreGroupementEntreprises[] = $obj;
                        }
                    }
                }

                $this->collCommonTMembreGroupementEntreprises = $collCommonTMembreGroupementEntreprises;
                $this->collCommonTMembreGroupementEntreprisesPartial = false;
            }
        }

        return $this->collCommonTMembreGroupementEntreprises;
    }

    /**
     * Sets a collection of CommonTMembreGroupementEntreprise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTMembreGroupementEntreprises A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function setCommonTMembreGroupementEntreprises(PropelCollection $commonTMembreGroupementEntreprises, PropelPDO $con = null)
    {
        $commonTMembreGroupementEntreprisesToDelete = $this->getCommonTMembreGroupementEntreprises(new Criteria(), $con)->diff($commonTMembreGroupementEntreprises);


        $this->commonTMembreGroupementEntreprisesScheduledForDeletion = $commonTMembreGroupementEntreprisesToDelete;

        foreach ($commonTMembreGroupementEntreprisesToDelete as $commonTMembreGroupementEntrepriseRemoved) {
            $commonTMembreGroupementEntrepriseRemoved->setCommonTEtablissement(null);
        }

        $this->collCommonTMembreGroupementEntreprises = null;
        foreach ($commonTMembreGroupementEntreprises as $commonTMembreGroupementEntreprise) {
            $this->addCommonTMembreGroupementEntreprise($commonTMembreGroupementEntreprise);
        }

        $this->collCommonTMembreGroupementEntreprises = $commonTMembreGroupementEntreprises;
        $this->collCommonTMembreGroupementEntreprisesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTMembreGroupementEntreprise objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTMembreGroupementEntreprise objects.
     * @throws PropelException
     */
    public function countCommonTMembreGroupementEntreprises(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTMembreGroupementEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTMembreGroupementEntreprises || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTMembreGroupementEntreprises) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTMembreGroupementEntreprises());
            }
            $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTEtablissement($this)
                ->count($con);
        }

        return count($this->collCommonTMembreGroupementEntreprises);
    }

    /**
     * Method called to associate a CommonTMembreGroupementEntreprise object to this object
     * through the CommonTMembreGroupementEntreprise foreign key attribute.
     *
     * @param   CommonTMembreGroupementEntreprise $l CommonTMembreGroupementEntreprise
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function addCommonTMembreGroupementEntreprise(CommonTMembreGroupementEntreprise $l)
    {
        if ($this->collCommonTMembreGroupementEntreprises === null) {
            $this->initCommonTMembreGroupementEntreprises();
            $this->collCommonTMembreGroupementEntreprisesPartial = true;
        }
        if (!in_array($l, $this->collCommonTMembreGroupementEntreprises->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTMembreGroupementEntreprise($l);
        }

        return $this;
    }

    /**
     * @param	CommonTMembreGroupementEntreprise $commonTMembreGroupementEntreprise The commonTMembreGroupementEntreprise object to add.
     */
    protected function doAddCommonTMembreGroupementEntreprise($commonTMembreGroupementEntreprise)
    {
        $this->collCommonTMembreGroupementEntreprises[]= $commonTMembreGroupementEntreprise;
        $commonTMembreGroupementEntreprise->setCommonTEtablissement($this);
    }

    /**
     * @param	CommonTMembreGroupementEntreprise $commonTMembreGroupementEntreprise The commonTMembreGroupementEntreprise object to remove.
     * @return CommonTEtablissement The current object (for fluent API support)
     */
    public function removeCommonTMembreGroupementEntreprise($commonTMembreGroupementEntreprise)
    {
        if ($this->getCommonTMembreGroupementEntreprises()->contains($commonTMembreGroupementEntreprise)) {
            $this->collCommonTMembreGroupementEntreprises->remove($this->collCommonTMembreGroupementEntreprises->search($commonTMembreGroupementEntreprise));
            if (null === $this->commonTMembreGroupementEntreprisesScheduledForDeletion) {
                $this->commonTMembreGroupementEntreprisesScheduledForDeletion = clone $this->collCommonTMembreGroupementEntreprises;
                $this->commonTMembreGroupementEntreprisesScheduledForDeletion->clear();
            }
            $this->commonTMembreGroupementEntreprisesScheduledForDeletion[]= $commonTMembreGroupementEntreprise;
            $commonTMembreGroupementEntreprise->setCommonTEtablissement(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTEtablissement is new, it will return
     * an empty collection; or if this CommonTEtablissement has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTEtablissement.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprises($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTEtablissement is new, it will return
     * an empty collection; or if this CommonTEtablissement has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTEtablissement.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesJoinCommonTGroupementEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTGroupementEntreprise', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprises($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTEtablissement is new, it will return
     * an empty collection; or if this CommonTEtablissement has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTEtablissement.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesJoinCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTMembreGroupementEntrepriseRelatedByIdMembreParent', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprises($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTEtablissement is new, it will return
     * an empty collection; or if this CommonTEtablissement has previously
     * been saved, it will retrieve related CommonTMembreGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTEtablissement.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTMembreGroupementEntreprise[] List of CommonTMembreGroupementEntreprise objects
     */
    public function getCommonTMembreGroupementEntreprisesJoinCommonTRoleJuridique($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTMembreGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTRoleJuridique', $join_behavior);

        return $this->getCommonTMembreGroupementEntreprises($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_etablissement = null;
        $this->id_entreprise = null;
        $this->code_etablissement = null;
        $this->est_siege = null;
        $this->adresse = null;
        $this->adresse2 = null;
        $this->code_postal = null;
        $this->ville = null;
        $this->pays = null;
        $this->saisie_manuelle = null;
        $this->id_initial = null;
        $this->date_creation = null;
        $this->date_modification = null;
        $this->date_suppression = null;
        $this->statut_actif = null;
        $this->inscrit_annuaire_defense = null;
        $this->long = null;
        $this->lat = null;
        $this->maj_long_lat = null;
        $this->tva_intracommunautaire = null;
        $this->etat_administratif = null;
        $this->date_fermeture = null;
        $this->id_externe = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonInscritsRelatedByIdEtablissement) {
                foreach ($this->collCommonInscritsRelatedByIdEtablissement as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonInscritsRelatedByIdEtablissement) {
                foreach ($this->collCommonInscritsRelatedByIdEtablissement as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonModificationContrats) {
                foreach ($this->collCommonModificationContrats as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTMembreGroupementEntreprises) {
                foreach ($this->collCommonTMembreGroupementEntreprises as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aEntreprise instanceof Persistent) {
              $this->aEntreprise->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonInscritsRelatedByIdEtablissement instanceof PropelCollection) {
            $this->collCommonInscritsRelatedByIdEtablissement->clearIterator();
        }
        $this->collCommonInscritsRelatedByIdEtablissement = null;
        if ($this->collCommonInscritsRelatedByIdEtablissement instanceof PropelCollection) {
            $this->collCommonInscritsRelatedByIdEtablissement->clearIterator();
        }
        $this->collCommonInscritsRelatedByIdEtablissement = null;
        if ($this->collCommonModificationContrats instanceof PropelCollection) {
            $this->collCommonModificationContrats->clearIterator();
        }
        $this->collCommonModificationContrats = null;
        if ($this->collCommonTMembreGroupementEntreprises instanceof PropelCollection) {
            $this->collCommonTMembreGroupementEntreprises->clearIterator();
        }
        $this->collCommonTMembreGroupementEntreprises = null;
        $this->aEntreprise = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTEtablissementPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
