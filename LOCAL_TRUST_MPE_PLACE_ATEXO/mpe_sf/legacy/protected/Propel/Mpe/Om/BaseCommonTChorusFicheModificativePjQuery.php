<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTChorusFicheModificativePj;
use Application\Propel\Mpe\CommonTChorusFicheModificativePjPeer;
use Application\Propel\Mpe\CommonTChorusFicheModificativePjQuery;

/**
 * Base class that represents a query for the 't_chorus_fiche_modificative_pj' table.
 *
 *
 *
 * @method CommonTChorusFicheModificativePjQuery orderByIdFicheModificativePj($order = Criteria::ASC) Order by the id_fiche_modificative_pj column
 * @method CommonTChorusFicheModificativePjQuery orderByIdFicheModificative($order = Criteria::ASC) Order by the id_fiche_modificative column
 * @method CommonTChorusFicheModificativePjQuery orderByNomFichier($order = Criteria::ASC) Order by the nom_fichier column
 * @method CommonTChorusFicheModificativePjQuery orderByFichier($order = Criteria::ASC) Order by the fichier column
 * @method CommonTChorusFicheModificativePjQuery orderByHorodatage($order = Criteria::ASC) Order by the horodatage column
 * @method CommonTChorusFicheModificativePjQuery orderByUntrusteddate($order = Criteria::ASC) Order by the untrusteddate column
 * @method CommonTChorusFicheModificativePjQuery orderByTaille($order = Criteria::ASC) Order by the taille column
 *
 * @method CommonTChorusFicheModificativePjQuery groupByIdFicheModificativePj() Group by the id_fiche_modificative_pj column
 * @method CommonTChorusFicheModificativePjQuery groupByIdFicheModificative() Group by the id_fiche_modificative column
 * @method CommonTChorusFicheModificativePjQuery groupByNomFichier() Group by the nom_fichier column
 * @method CommonTChorusFicheModificativePjQuery groupByFichier() Group by the fichier column
 * @method CommonTChorusFicheModificativePjQuery groupByHorodatage() Group by the horodatage column
 * @method CommonTChorusFicheModificativePjQuery groupByUntrusteddate() Group by the untrusteddate column
 * @method CommonTChorusFicheModificativePjQuery groupByTaille() Group by the taille column
 *
 * @method CommonTChorusFicheModificativePjQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTChorusFicheModificativePjQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTChorusFicheModificativePjQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTChorusFicheModificativePjQuery leftJoinCommonTChorusFicheModificative($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTChorusFicheModificative relation
 * @method CommonTChorusFicheModificativePjQuery rightJoinCommonTChorusFicheModificative($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTChorusFicheModificative relation
 * @method CommonTChorusFicheModificativePjQuery innerJoinCommonTChorusFicheModificative($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTChorusFicheModificative relation
 *
 * @method CommonTChorusFicheModificativePj findOne(PropelPDO $con = null) Return the first CommonTChorusFicheModificativePj matching the query
 * @method CommonTChorusFicheModificativePj findOneOrCreate(PropelPDO $con = null) Return the first CommonTChorusFicheModificativePj matching the query, or a new CommonTChorusFicheModificativePj object populated from the query conditions when no match is found
 *
 * @method CommonTChorusFicheModificativePj findOneByIdFicheModificative(int $id_fiche_modificative) Return the first CommonTChorusFicheModificativePj filtered by the id_fiche_modificative column
 * @method CommonTChorusFicheModificativePj findOneByNomFichier(string $nom_fichier) Return the first CommonTChorusFicheModificativePj filtered by the nom_fichier column
 * @method CommonTChorusFicheModificativePj findOneByFichier(string $fichier) Return the first CommonTChorusFicheModificativePj filtered by the fichier column
 * @method CommonTChorusFicheModificativePj findOneByHorodatage(resource $horodatage) Return the first CommonTChorusFicheModificativePj filtered by the horodatage column
 * @method CommonTChorusFicheModificativePj findOneByUntrusteddate(string $untrusteddate) Return the first CommonTChorusFicheModificativePj filtered by the untrusteddate column
 * @method CommonTChorusFicheModificativePj findOneByTaille(int $taille) Return the first CommonTChorusFicheModificativePj filtered by the taille column
 *
 * @method array findByIdFicheModificativePj(int $id_fiche_modificative_pj) Return CommonTChorusFicheModificativePj objects filtered by the id_fiche_modificative_pj column
 * @method array findByIdFicheModificative(int $id_fiche_modificative) Return CommonTChorusFicheModificativePj objects filtered by the id_fiche_modificative column
 * @method array findByNomFichier(string $nom_fichier) Return CommonTChorusFicheModificativePj objects filtered by the nom_fichier column
 * @method array findByFichier(string $fichier) Return CommonTChorusFicheModificativePj objects filtered by the fichier column
 * @method array findByHorodatage(resource $horodatage) Return CommonTChorusFicheModificativePj objects filtered by the horodatage column
 * @method array findByUntrusteddate(string $untrusteddate) Return CommonTChorusFicheModificativePj objects filtered by the untrusteddate column
 * @method array findByTaille(int $taille) Return CommonTChorusFicheModificativePj objects filtered by the taille column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTChorusFicheModificativePjQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTChorusFicheModificativePjQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTChorusFicheModificativePj', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTChorusFicheModificativePjQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTChorusFicheModificativePjQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTChorusFicheModificativePjQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTChorusFicheModificativePjQuery) {
            return $criteria;
        }
        $query = new CommonTChorusFicheModificativePjQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTChorusFicheModificativePj|CommonTChorusFicheModificativePj[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTChorusFicheModificativePjPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePjPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTChorusFicheModificativePj A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdFicheModificativePj($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTChorusFicheModificativePj A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_fiche_modificative_pj`, `id_fiche_modificative`, `nom_fichier`, `fichier`, `horodatage`, `untrusteddate`, `taille` FROM `t_chorus_fiche_modificative_pj` WHERE `id_fiche_modificative_pj` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTChorusFicheModificativePj();
            $obj->hydrate($row);
            CommonTChorusFicheModificativePjPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTChorusFicheModificativePj|CommonTChorusFicheModificativePj[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTChorusFicheModificativePj[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE_PJ, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE_PJ, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_fiche_modificative_pj column
     *
     * Example usage:
     * <code>
     * $query->filterByIdFicheModificativePj(1234); // WHERE id_fiche_modificative_pj = 1234
     * $query->filterByIdFicheModificativePj(array(12, 34)); // WHERE id_fiche_modificative_pj IN (12, 34)
     * $query->filterByIdFicheModificativePj(array('min' => 12)); // WHERE id_fiche_modificative_pj >= 12
     * $query->filterByIdFicheModificativePj(array('max' => 12)); // WHERE id_fiche_modificative_pj <= 12
     * </code>
     *
     * @param     mixed $idFicheModificativePj The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     */
    public function filterByIdFicheModificativePj($idFicheModificativePj = null, $comparison = null)
    {
        if (is_array($idFicheModificativePj)) {
            $useMinMax = false;
            if (isset($idFicheModificativePj['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE_PJ, $idFicheModificativePj['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idFicheModificativePj['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE_PJ, $idFicheModificativePj['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE_PJ, $idFicheModificativePj, $comparison);
    }

    /**
     * Filter the query on the id_fiche_modificative column
     *
     * Example usage:
     * <code>
     * $query->filterByIdFicheModificative(1234); // WHERE id_fiche_modificative = 1234
     * $query->filterByIdFicheModificative(array(12, 34)); // WHERE id_fiche_modificative IN (12, 34)
     * $query->filterByIdFicheModificative(array('min' => 12)); // WHERE id_fiche_modificative >= 12
     * $query->filterByIdFicheModificative(array('max' => 12)); // WHERE id_fiche_modificative <= 12
     * </code>
     *
     * @see       filterByCommonTChorusFicheModificative()
     *
     * @param     mixed $idFicheModificative The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     */
    public function filterByIdFicheModificative($idFicheModificative = null, $comparison = null)
    {
        if (is_array($idFicheModificative)) {
            $useMinMax = false;
            if (isset($idFicheModificative['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE, $idFicheModificative['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idFicheModificative['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE, $idFicheModificative['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE, $idFicheModificative, $comparison);
    }

    /**
     * Filter the query on the nom_fichier column
     *
     * Example usage:
     * <code>
     * $query->filterByNomFichier('fooValue');   // WHERE nom_fichier = 'fooValue'
     * $query->filterByNomFichier('%fooValue%'); // WHERE nom_fichier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomFichier The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     */
    public function filterByNomFichier($nomFichier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomFichier)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomFichier)) {
                $nomFichier = str_replace('*', '%', $nomFichier);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::NOM_FICHIER, $nomFichier, $comparison);
    }

    /**
     * Filter the query on the fichier column
     *
     * Example usage:
     * <code>
     * $query->filterByFichier('fooValue');   // WHERE fichier = 'fooValue'
     * $query->filterByFichier('%fooValue%'); // WHERE fichier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fichier The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     */
    public function filterByFichier($fichier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fichier)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fichier)) {
                $fichier = str_replace('*', '%', $fichier);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::FICHIER, $fichier, $comparison);
    }

    /**
     * Filter the query on the horodatage column
     *
     * @param     mixed $horodatage The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     */
    public function filterByHorodatage($horodatage = null, $comparison = null)
    {

        return $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::HORODATAGE, $horodatage, $comparison);
    }

    /**
     * Filter the query on the untrusteddate column
     *
     * Example usage:
     * <code>
     * $query->filterByUntrusteddate('fooValue');   // WHERE untrusteddate = 'fooValue'
     * $query->filterByUntrusteddate('%fooValue%'); // WHERE untrusteddate LIKE '%fooValue%'
     * </code>
     *
     * @param     string $untrusteddate The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     */
    public function filterByUntrusteddate($untrusteddate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($untrusteddate)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $untrusteddate)) {
                $untrusteddate = str_replace('*', '%', $untrusteddate);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::UNTRUSTEDDATE, $untrusteddate, $comparison);
    }

    /**
     * Filter the query on the taille column
     *
     * Example usage:
     * <code>
     * $query->filterByTaille(1234); // WHERE taille = 1234
     * $query->filterByTaille(array(12, 34)); // WHERE taille IN (12, 34)
     * $query->filterByTaille(array('min' => 12)); // WHERE taille >= 12
     * $query->filterByTaille(array('max' => 12)); // WHERE taille <= 12
     * </code>
     *
     * @param     mixed $taille The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     */
    public function filterByTaille($taille = null, $comparison = null)
    {
        if (is_array($taille)) {
            $useMinMax = false;
            if (isset($taille['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::TAILLE, $taille['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($taille['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::TAILLE, $taille['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::TAILLE, $taille, $comparison);
    }

    /**
     * Filter the query by a related CommonTChorusFicheModificative object
     *
     * @param   CommonTChorusFicheModificative|PropelObjectCollection $commonTChorusFicheModificative The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTChorusFicheModificative($commonTChorusFicheModificative, $comparison = null)
    {
        if ($commonTChorusFicheModificative instanceof CommonTChorusFicheModificative) {
            return $this
                ->addUsingAlias(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE, $commonTChorusFicheModificative->getIdFicheModificative(), $comparison);
        } elseif ($commonTChorusFicheModificative instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE, $commonTChorusFicheModificative->toKeyValue('PrimaryKey', 'IdFicheModificative'), $comparison);
        } else {
            throw new PropelException('filterByCommonTChorusFicheModificative() only accepts arguments of type CommonTChorusFicheModificative or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTChorusFicheModificative relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     */
    public function joinCommonTChorusFicheModificative($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTChorusFicheModificative');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTChorusFicheModificative');
        }

        return $this;
    }

    /**
     * Use the CommonTChorusFicheModificative relation CommonTChorusFicheModificative object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTChorusFicheModificativeQuery A secondary query class using the current class as primary query
     */
    public function useCommonTChorusFicheModificativeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTChorusFicheModificative($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTChorusFicheModificative', '\Application\Propel\Mpe\CommonTChorusFicheModificativeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTChorusFicheModificativePj $commonTChorusFicheModificativePj Object to remove from the list of results
     *
     * @return CommonTChorusFicheModificativePjQuery The current query, for fluid interface
     */
    public function prune($commonTChorusFicheModificativePj = null)
    {
        if ($commonTChorusFicheModificativePj) {
            $this->addUsingAlias(CommonTChorusFicheModificativePjPeer::ID_FICHE_MODIFICATIVE_PJ, $commonTChorusFicheModificativePj->getIdFicheModificativePj(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
