<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcession;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionPeer;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionQuery;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionTarif;
use Application\Propel\Mpe\CommonTContratTitulaire;

/**
 * Base class that represents a query for the 'donnees_annuelles_concession' table.
 *
 *
 *
 * @method CommonDonneesAnnuellesConcessionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonDonneesAnnuellesConcessionQuery orderByIdContrat($order = Criteria::ASC) Order by the id_contrat column
 * @method CommonDonneesAnnuellesConcessionQuery orderByValeurDepense($order = Criteria::ASC) Order by the valeur_depense column
 * @method CommonDonneesAnnuellesConcessionQuery orderByDateSaisie($order = Criteria::ASC) Order by the date_saisie column
 * @method CommonDonneesAnnuellesConcessionQuery orderByNumOrdre($order = Criteria::ASC) Order by the num_ordre column
 * @method CommonDonneesAnnuellesConcessionQuery orderBySuiviPublicationSn($order = Criteria::ASC) Order by the suivi_publication_sn column
 *
 * @method CommonDonneesAnnuellesConcessionQuery groupById() Group by the id column
 * @method CommonDonneesAnnuellesConcessionQuery groupByIdContrat() Group by the id_contrat column
 * @method CommonDonneesAnnuellesConcessionQuery groupByValeurDepense() Group by the valeur_depense column
 * @method CommonDonneesAnnuellesConcessionQuery groupByDateSaisie() Group by the date_saisie column
 * @method CommonDonneesAnnuellesConcessionQuery groupByNumOrdre() Group by the num_ordre column
 * @method CommonDonneesAnnuellesConcessionQuery groupBySuiviPublicationSn() Group by the suivi_publication_sn column
 *
 * @method CommonDonneesAnnuellesConcessionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonDonneesAnnuellesConcessionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonDonneesAnnuellesConcessionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonDonneesAnnuellesConcessionQuery leftJoinCommonTContratTitulaire($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonDonneesAnnuellesConcessionQuery rightJoinCommonTContratTitulaire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonDonneesAnnuellesConcessionQuery innerJoinCommonTContratTitulaire($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContratTitulaire relation
 *
 * @method CommonDonneesAnnuellesConcessionQuery leftJoinCommonDonneesAnnuellesConcessionTarif($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonDonneesAnnuellesConcessionTarif relation
 * @method CommonDonneesAnnuellesConcessionQuery rightJoinCommonDonneesAnnuellesConcessionTarif($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonDonneesAnnuellesConcessionTarif relation
 * @method CommonDonneesAnnuellesConcessionQuery innerJoinCommonDonneesAnnuellesConcessionTarif($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonDonneesAnnuellesConcessionTarif relation
 *
 * @method CommonDonneesAnnuellesConcession findOne(PropelPDO $con = null) Return the first CommonDonneesAnnuellesConcession matching the query
 * @method CommonDonneesAnnuellesConcession findOneOrCreate(PropelPDO $con = null) Return the first CommonDonneesAnnuellesConcession matching the query, or a new CommonDonneesAnnuellesConcession object populated from the query conditions when no match is found
 *
 * @method CommonDonneesAnnuellesConcession findOneByIdContrat(int $id_contrat) Return the first CommonDonneesAnnuellesConcession filtered by the id_contrat column
 * @method CommonDonneesAnnuellesConcession findOneByValeurDepense(double $valeur_depense) Return the first CommonDonneesAnnuellesConcession filtered by the valeur_depense column
 * @method CommonDonneesAnnuellesConcession findOneByDateSaisie(string $date_saisie) Return the first CommonDonneesAnnuellesConcession filtered by the date_saisie column
 * @method CommonDonneesAnnuellesConcession findOneByNumOrdre(int $num_ordre) Return the first CommonDonneesAnnuellesConcession filtered by the num_ordre column
 * @method CommonDonneesAnnuellesConcession findOneBySuiviPublicationSn(int $suivi_publication_sn) Return the first CommonDonneesAnnuellesConcession filtered by the suivi_publication_sn column
 *
 * @method array findById(int $id) Return CommonDonneesAnnuellesConcession objects filtered by the id column
 * @method array findByIdContrat(int $id_contrat) Return CommonDonneesAnnuellesConcession objects filtered by the id_contrat column
 * @method array findByValeurDepense(double $valeur_depense) Return CommonDonneesAnnuellesConcession objects filtered by the valeur_depense column
 * @method array findByDateSaisie(string $date_saisie) Return CommonDonneesAnnuellesConcession objects filtered by the date_saisie column
 * @method array findByNumOrdre(int $num_ordre) Return CommonDonneesAnnuellesConcession objects filtered by the num_ordre column
 * @method array findBySuiviPublicationSn(int $suivi_publication_sn) Return CommonDonneesAnnuellesConcession objects filtered by the suivi_publication_sn column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonDonneesAnnuellesConcessionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonDonneesAnnuellesConcessionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonDonneesAnnuellesConcession', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonDonneesAnnuellesConcessionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonDonneesAnnuellesConcessionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonDonneesAnnuellesConcessionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonDonneesAnnuellesConcessionQuery) {
            return $criteria;
        }
        $query = new CommonDonneesAnnuellesConcessionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonDonneesAnnuellesConcession|CommonDonneesAnnuellesConcession[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonDonneesAnnuellesConcessionPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonDonneesAnnuellesConcessionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonDonneesAnnuellesConcession A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonDonneesAnnuellesConcession A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_contrat`, `valeur_depense`, `date_saisie`, `num_ordre`, `suivi_publication_sn` FROM `donnees_annuelles_concession` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonDonneesAnnuellesConcession();
            $obj->hydrate($row);
            CommonDonneesAnnuellesConcessionPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonDonneesAnnuellesConcession|CommonDonneesAnnuellesConcession[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonDonneesAnnuellesConcession[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByIdContrat(1234); // WHERE id_contrat = 1234
     * $query->filterByIdContrat(array(12, 34)); // WHERE id_contrat IN (12, 34)
     * $query->filterByIdContrat(array('min' => 12)); // WHERE id_contrat >= 12
     * $query->filterByIdContrat(array('max' => 12)); // WHERE id_contrat <= 12
     * </code>
     *
     * @see       filterByCommonTContratTitulaire()
     *
     * @param     mixed $idContrat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     */
    public function filterByIdContrat($idContrat = null, $comparison = null)
    {
        if (is_array($idContrat)) {
            $useMinMax = false;
            if (isset($idContrat['min'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID_CONTRAT, $idContrat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idContrat['max'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID_CONTRAT, $idContrat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID_CONTRAT, $idContrat, $comparison);
    }

    /**
     * Filter the query on the valeur_depense column
     *
     * Example usage:
     * <code>
     * $query->filterByValeurDepense(1234); // WHERE valeur_depense = 1234
     * $query->filterByValeurDepense(array(12, 34)); // WHERE valeur_depense IN (12, 34)
     * $query->filterByValeurDepense(array('min' => 12)); // WHERE valeur_depense >= 12
     * $query->filterByValeurDepense(array('max' => 12)); // WHERE valeur_depense <= 12
     * </code>
     *
     * @param     mixed $valeurDepense The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     */
    public function filterByValeurDepense($valeurDepense = null, $comparison = null)
    {
        if (is_array($valeurDepense)) {
            $useMinMax = false;
            if (isset($valeurDepense['min'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::VALEUR_DEPENSE, $valeurDepense['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valeurDepense['max'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::VALEUR_DEPENSE, $valeurDepense['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::VALEUR_DEPENSE, $valeurDepense, $comparison);
    }

    /**
     * Filter the query on the date_saisie column
     *
     * Example usage:
     * <code>
     * $query->filterByDateSaisie('2011-03-14'); // WHERE date_saisie = '2011-03-14'
     * $query->filterByDateSaisie('now'); // WHERE date_saisie = '2011-03-14'
     * $query->filterByDateSaisie(array('max' => 'yesterday')); // WHERE date_saisie > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateSaisie The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     */
    public function filterByDateSaisie($dateSaisie = null, $comparison = null)
    {
        if (is_array($dateSaisie)) {
            $useMinMax = false;
            if (isset($dateSaisie['min'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::DATE_SAISIE, $dateSaisie['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateSaisie['max'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::DATE_SAISIE, $dateSaisie['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::DATE_SAISIE, $dateSaisie, $comparison);
    }

    /**
     * Filter the query on the num_ordre column
     *
     * Example usage:
     * <code>
     * $query->filterByNumOrdre(1234); // WHERE num_ordre = 1234
     * $query->filterByNumOrdre(array(12, 34)); // WHERE num_ordre IN (12, 34)
     * $query->filterByNumOrdre(array('min' => 12)); // WHERE num_ordre >= 12
     * $query->filterByNumOrdre(array('max' => 12)); // WHERE num_ordre <= 12
     * </code>
     *
     * @param     mixed $numOrdre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     */
    public function filterByNumOrdre($numOrdre = null, $comparison = null)
    {
        if (is_array($numOrdre)) {
            $useMinMax = false;
            if (isset($numOrdre['min'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::NUM_ORDRE, $numOrdre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numOrdre['max'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::NUM_ORDRE, $numOrdre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::NUM_ORDRE, $numOrdre, $comparison);
    }

    /**
     * Filter the query on the suivi_publication_sn column
     *
     * Example usage:
     * <code>
     * $query->filterBySuiviPublicationSn(1234); // WHERE suivi_publication_sn = 1234
     * $query->filterBySuiviPublicationSn(array(12, 34)); // WHERE suivi_publication_sn IN (12, 34)
     * $query->filterBySuiviPublicationSn(array('min' => 12)); // WHERE suivi_publication_sn >= 12
     * $query->filterBySuiviPublicationSn(array('max' => 12)); // WHERE suivi_publication_sn <= 12
     * </code>
     *
     * @param     mixed $suiviPublicationSn The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     */
    public function filterBySuiviPublicationSn($suiviPublicationSn = null, $comparison = null)
    {
        if (is_array($suiviPublicationSn)) {
            $useMinMax = false;
            if (isset($suiviPublicationSn['min'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::SUIVI_PUBLICATION_SN, $suiviPublicationSn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($suiviPublicationSn['max'])) {
                $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::SUIVI_PUBLICATION_SN, $suiviPublicationSn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::SUIVI_PUBLICATION_SN, $suiviPublicationSn, $comparison);
    }

    /**
     * Filter the query by a related CommonTContratTitulaire object
     *
     * @param   CommonTContratTitulaire|PropelObjectCollection $commonTContratTitulaire The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContratTitulaire($commonTContratTitulaire, $comparison = null)
    {
        if ($commonTContratTitulaire instanceof CommonTContratTitulaire) {
            return $this
                ->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID_CONTRAT, $commonTContratTitulaire->getIdContratTitulaire(), $comparison);
        } elseif ($commonTContratTitulaire instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID_CONTRAT, $commonTContratTitulaire->toKeyValue('PrimaryKey', 'IdContratTitulaire'), $comparison);
        } else {
            throw new PropelException('filterByCommonTContratTitulaire() only accepts arguments of type CommonTContratTitulaire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContratTitulaire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     */
    public function joinCommonTContratTitulaire($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContratTitulaire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContratTitulaire');
        }

        return $this;
    }

    /**
     * Use the CommonTContratTitulaire relation CommonTContratTitulaire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContratTitulaireQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContratTitulaireQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTContratTitulaire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContratTitulaire', '\Application\Propel\Mpe\CommonTContratTitulaireQuery');
    }

    /**
     * Filter the query by a related CommonDonneesAnnuellesConcessionTarif object
     *
     * @param   CommonDonneesAnnuellesConcessionTarif|PropelObjectCollection $commonDonneesAnnuellesConcessionTarif  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonDonneesAnnuellesConcessionTarif($commonDonneesAnnuellesConcessionTarif, $comparison = null)
    {
        if ($commonDonneesAnnuellesConcessionTarif instanceof CommonDonneesAnnuellesConcessionTarif) {
            return $this
                ->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID, $commonDonneesAnnuellesConcessionTarif->getIdDonneesAnnuelle(), $comparison);
        } elseif ($commonDonneesAnnuellesConcessionTarif instanceof PropelObjectCollection) {
            return $this
                ->useCommonDonneesAnnuellesConcessionTarifQuery()
                ->filterByPrimaryKeys($commonDonneesAnnuellesConcessionTarif->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonDonneesAnnuellesConcessionTarif() only accepts arguments of type CommonDonneesAnnuellesConcessionTarif or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonDonneesAnnuellesConcessionTarif relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     */
    public function joinCommonDonneesAnnuellesConcessionTarif($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonDonneesAnnuellesConcessionTarif');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonDonneesAnnuellesConcessionTarif');
        }

        return $this;
    }

    /**
     * Use the CommonDonneesAnnuellesConcessionTarif relation CommonDonneesAnnuellesConcessionTarif object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonDonneesAnnuellesConcessionTarifQuery A secondary query class using the current class as primary query
     */
    public function useCommonDonneesAnnuellesConcessionTarifQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonDonneesAnnuellesConcessionTarif($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonDonneesAnnuellesConcessionTarif', '\Application\Propel\Mpe\CommonDonneesAnnuellesConcessionTarifQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonDonneesAnnuellesConcession $commonDonneesAnnuellesConcession Object to remove from the list of results
     *
     * @return CommonDonneesAnnuellesConcessionQuery The current query, for fluid interface
     */
    public function prune($commonDonneesAnnuellesConcession = null)
    {
        if ($commonDonneesAnnuellesConcession) {
            $this->addUsingAlias(CommonDonneesAnnuellesConcessionPeer::ID, $commonDonneesAnnuellesConcession->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
