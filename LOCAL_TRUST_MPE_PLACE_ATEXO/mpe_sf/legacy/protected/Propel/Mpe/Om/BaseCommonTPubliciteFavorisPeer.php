<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTPubliciteFavoris;
use Application\Propel\Mpe\CommonTPubliciteFavorisPeer;
use Application\Propel\Mpe\Map\CommonTPubliciteFavorisTableMap;

/**
 * Base static class for performing query and update operations on the 't_publicite_favoris' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTPubliciteFavorisPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_publicite_favoris';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTPubliciteFavoris';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTPubliciteFavorisTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 24;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 24;

    /** the column name for the id field */
    const ID = 't_publicite_favoris.id';

    /** the column name for the organisme field */
    const ORGANISME = 't_publicite_favoris.organisme';

    /** the column name for the old_service_id field */
    const OLD_SERVICE_ID = 't_publicite_favoris.old_service_id';

    /** the column name for the libelle field */
    const LIBELLE = 't_publicite_favoris.libelle';

    /** the column name for the regime_financier_cautionnement field */
    const REGIME_FINANCIER_CAUTIONNEMENT = 't_publicite_favoris.regime_financier_cautionnement';

    /** the column name for the regime_financier_modalites_financement field */
    const REGIME_FINANCIER_MODALITES_FINANCEMENT = 't_publicite_favoris.regime_financier_modalites_financement';

    /** the column name for the acheteur_correspondant field */
    const ACHETEUR_CORRESPONDANT = 't_publicite_favoris.acheteur_correspondant';

    /** the column name for the acheteur_nom_organisme field */
    const ACHETEUR_NOM_ORGANISME = 't_publicite_favoris.acheteur_nom_organisme';

    /** the column name for the acheteur_adresse field */
    const ACHETEUR_ADRESSE = 't_publicite_favoris.acheteur_adresse';

    /** the column name for the acheteur_cp field */
    const ACHETEUR_CP = 't_publicite_favoris.acheteur_cp';

    /** the column name for the acheteur_ville field */
    const ACHETEUR_VILLE = 't_publicite_favoris.acheteur_ville';

    /** the column name for the acheteur_url field */
    const ACHETEUR_URL = 't_publicite_favoris.acheteur_url';

    /** the column name for the facture_denomination field */
    const FACTURE_DENOMINATION = 't_publicite_favoris.facture_denomination';

    /** the column name for the facture_adresse field */
    const FACTURE_ADRESSE = 't_publicite_favoris.facture_adresse';

    /** the column name for the facture_cp field */
    const FACTURE_CP = 't_publicite_favoris.facture_cp';

    /** the column name for the facture_ville field */
    const FACTURE_VILLE = 't_publicite_favoris.facture_ville';

    /** the column name for the instance_recours_organisme field */
    const INSTANCE_RECOURS_ORGANISME = 't_publicite_favoris.instance_recours_organisme';

    /** the column name for the instance_recours_adresse field */
    const INSTANCE_RECOURS_ADRESSE = 't_publicite_favoris.instance_recours_adresse';

    /** the column name for the instance_recours_cp field */
    const INSTANCE_RECOURS_CP = 't_publicite_favoris.instance_recours_cp';

    /** the column name for the instance_recours_ville field */
    const INSTANCE_RECOURS_VILLE = 't_publicite_favoris.instance_recours_ville';

    /** the column name for the instance_recours_url field */
    const INSTANCE_RECOURS_URL = 't_publicite_favoris.instance_recours_url';

    /** the column name for the acheteur_telephone field */
    const ACHETEUR_TELEPHONE = 't_publicite_favoris.acheteur_telephone';

    /** the column name for the acheteur_email field */
    const ACHETEUR_EMAIL = 't_publicite_favoris.acheteur_email';

    /** the column name for the service_id field */
    const SERVICE_ID = 't_publicite_favoris.service_id';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTPubliciteFavoris objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTPubliciteFavoris[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTPubliciteFavorisPeer::$fieldNames[CommonTPubliciteFavorisPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'Organisme', 'OldServiceId', 'Libelle', 'RegimeFinancierCautionnement', 'RegimeFinancierModalitesFinancement', 'AcheteurCorrespondant', 'AcheteurNomOrganisme', 'AcheteurAdresse', 'AcheteurCp', 'AcheteurVille', 'AcheteurUrl', 'FactureDenomination', 'FactureAdresse', 'FactureCp', 'FactureVille', 'InstanceRecoursOrganisme', 'InstanceRecoursAdresse', 'InstanceRecoursCp', 'InstanceRecoursVille', 'InstanceRecoursUrl', 'AcheteurTelephone', 'AcheteurEmail', 'ServiceId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'organisme', 'oldServiceId', 'libelle', 'regimeFinancierCautionnement', 'regimeFinancierModalitesFinancement', 'acheteurCorrespondant', 'acheteurNomOrganisme', 'acheteurAdresse', 'acheteurCp', 'acheteurVille', 'acheteurUrl', 'factureDenomination', 'factureAdresse', 'factureCp', 'factureVille', 'instanceRecoursOrganisme', 'instanceRecoursAdresse', 'instanceRecoursCp', 'instanceRecoursVille', 'instanceRecoursUrl', 'acheteurTelephone', 'acheteurEmail', 'serviceId', ),
        BasePeer::TYPE_COLNAME => array (CommonTPubliciteFavorisPeer::ID, CommonTPubliciteFavorisPeer::ORGANISME, CommonTPubliciteFavorisPeer::OLD_SERVICE_ID, CommonTPubliciteFavorisPeer::LIBELLE, CommonTPubliciteFavorisPeer::REGIME_FINANCIER_CAUTIONNEMENT, CommonTPubliciteFavorisPeer::REGIME_FINANCIER_MODALITES_FINANCEMENT, CommonTPubliciteFavorisPeer::ACHETEUR_CORRESPONDANT, CommonTPubliciteFavorisPeer::ACHETEUR_NOM_ORGANISME, CommonTPubliciteFavorisPeer::ACHETEUR_ADRESSE, CommonTPubliciteFavorisPeer::ACHETEUR_CP, CommonTPubliciteFavorisPeer::ACHETEUR_VILLE, CommonTPubliciteFavorisPeer::ACHETEUR_URL, CommonTPubliciteFavorisPeer::FACTURE_DENOMINATION, CommonTPubliciteFavorisPeer::FACTURE_ADRESSE, CommonTPubliciteFavorisPeer::FACTURE_CP, CommonTPubliciteFavorisPeer::FACTURE_VILLE, CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ORGANISME, CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ADRESSE, CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_CP, CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_VILLE, CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_URL, CommonTPubliciteFavorisPeer::ACHETEUR_TELEPHONE, CommonTPubliciteFavorisPeer::ACHETEUR_EMAIL, CommonTPubliciteFavorisPeer::SERVICE_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ORGANISME', 'OLD_SERVICE_ID', 'LIBELLE', 'REGIME_FINANCIER_CAUTIONNEMENT', 'REGIME_FINANCIER_MODALITES_FINANCEMENT', 'ACHETEUR_CORRESPONDANT', 'ACHETEUR_NOM_ORGANISME', 'ACHETEUR_ADRESSE', 'ACHETEUR_CP', 'ACHETEUR_VILLE', 'ACHETEUR_URL', 'FACTURE_DENOMINATION', 'FACTURE_ADRESSE', 'FACTURE_CP', 'FACTURE_VILLE', 'INSTANCE_RECOURS_ORGANISME', 'INSTANCE_RECOURS_ADRESSE', 'INSTANCE_RECOURS_CP', 'INSTANCE_RECOURS_VILLE', 'INSTANCE_RECOURS_URL', 'ACHETEUR_TELEPHONE', 'ACHETEUR_EMAIL', 'SERVICE_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'organisme', 'old_service_id', 'libelle', 'regime_financier_cautionnement', 'regime_financier_modalites_financement', 'acheteur_correspondant', 'acheteur_nom_organisme', 'acheteur_adresse', 'acheteur_cp', 'acheteur_ville', 'acheteur_url', 'facture_denomination', 'facture_adresse', 'facture_cp', 'facture_ville', 'instance_recours_organisme', 'instance_recours_adresse', 'instance_recours_cp', 'instance_recours_ville', 'instance_recours_url', 'acheteur_telephone', 'acheteur_email', 'service_id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTPubliciteFavorisPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Organisme' => 1, 'OldServiceId' => 2, 'Libelle' => 3, 'RegimeFinancierCautionnement' => 4, 'RegimeFinancierModalitesFinancement' => 5, 'AcheteurCorrespondant' => 6, 'AcheteurNomOrganisme' => 7, 'AcheteurAdresse' => 8, 'AcheteurCp' => 9, 'AcheteurVille' => 10, 'AcheteurUrl' => 11, 'FactureDenomination' => 12, 'FactureAdresse' => 13, 'FactureCp' => 14, 'FactureVille' => 15, 'InstanceRecoursOrganisme' => 16, 'InstanceRecoursAdresse' => 17, 'InstanceRecoursCp' => 18, 'InstanceRecoursVille' => 19, 'InstanceRecoursUrl' => 20, 'AcheteurTelephone' => 21, 'AcheteurEmail' => 22, 'ServiceId' => 23, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'organisme' => 1, 'oldServiceId' => 2, 'libelle' => 3, 'regimeFinancierCautionnement' => 4, 'regimeFinancierModalitesFinancement' => 5, 'acheteurCorrespondant' => 6, 'acheteurNomOrganisme' => 7, 'acheteurAdresse' => 8, 'acheteurCp' => 9, 'acheteurVille' => 10, 'acheteurUrl' => 11, 'factureDenomination' => 12, 'factureAdresse' => 13, 'factureCp' => 14, 'factureVille' => 15, 'instanceRecoursOrganisme' => 16, 'instanceRecoursAdresse' => 17, 'instanceRecoursCp' => 18, 'instanceRecoursVille' => 19, 'instanceRecoursUrl' => 20, 'acheteurTelephone' => 21, 'acheteurEmail' => 22, 'serviceId' => 23, ),
        BasePeer::TYPE_COLNAME => array (CommonTPubliciteFavorisPeer::ID => 0, CommonTPubliciteFavorisPeer::ORGANISME => 1, CommonTPubliciteFavorisPeer::OLD_SERVICE_ID => 2, CommonTPubliciteFavorisPeer::LIBELLE => 3, CommonTPubliciteFavorisPeer::REGIME_FINANCIER_CAUTIONNEMENT => 4, CommonTPubliciteFavorisPeer::REGIME_FINANCIER_MODALITES_FINANCEMENT => 5, CommonTPubliciteFavorisPeer::ACHETEUR_CORRESPONDANT => 6, CommonTPubliciteFavorisPeer::ACHETEUR_NOM_ORGANISME => 7, CommonTPubliciteFavorisPeer::ACHETEUR_ADRESSE => 8, CommonTPubliciteFavorisPeer::ACHETEUR_CP => 9, CommonTPubliciteFavorisPeer::ACHETEUR_VILLE => 10, CommonTPubliciteFavorisPeer::ACHETEUR_URL => 11, CommonTPubliciteFavorisPeer::FACTURE_DENOMINATION => 12, CommonTPubliciteFavorisPeer::FACTURE_ADRESSE => 13, CommonTPubliciteFavorisPeer::FACTURE_CP => 14, CommonTPubliciteFavorisPeer::FACTURE_VILLE => 15, CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ORGANISME => 16, CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ADRESSE => 17, CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_CP => 18, CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_VILLE => 19, CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_URL => 20, CommonTPubliciteFavorisPeer::ACHETEUR_TELEPHONE => 21, CommonTPubliciteFavorisPeer::ACHETEUR_EMAIL => 22, CommonTPubliciteFavorisPeer::SERVICE_ID => 23, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ORGANISME' => 1, 'OLD_SERVICE_ID' => 2, 'LIBELLE' => 3, 'REGIME_FINANCIER_CAUTIONNEMENT' => 4, 'REGIME_FINANCIER_MODALITES_FINANCEMENT' => 5, 'ACHETEUR_CORRESPONDANT' => 6, 'ACHETEUR_NOM_ORGANISME' => 7, 'ACHETEUR_ADRESSE' => 8, 'ACHETEUR_CP' => 9, 'ACHETEUR_VILLE' => 10, 'ACHETEUR_URL' => 11, 'FACTURE_DENOMINATION' => 12, 'FACTURE_ADRESSE' => 13, 'FACTURE_CP' => 14, 'FACTURE_VILLE' => 15, 'INSTANCE_RECOURS_ORGANISME' => 16, 'INSTANCE_RECOURS_ADRESSE' => 17, 'INSTANCE_RECOURS_CP' => 18, 'INSTANCE_RECOURS_VILLE' => 19, 'INSTANCE_RECOURS_URL' => 20, 'ACHETEUR_TELEPHONE' => 21, 'ACHETEUR_EMAIL' => 22, 'SERVICE_ID' => 23, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'organisme' => 1, 'old_service_id' => 2, 'libelle' => 3, 'regime_financier_cautionnement' => 4, 'regime_financier_modalites_financement' => 5, 'acheteur_correspondant' => 6, 'acheteur_nom_organisme' => 7, 'acheteur_adresse' => 8, 'acheteur_cp' => 9, 'acheteur_ville' => 10, 'acheteur_url' => 11, 'facture_denomination' => 12, 'facture_adresse' => 13, 'facture_cp' => 14, 'facture_ville' => 15, 'instance_recours_organisme' => 16, 'instance_recours_adresse' => 17, 'instance_recours_cp' => 18, 'instance_recours_ville' => 19, 'instance_recours_url' => 20, 'acheteur_telephone' => 21, 'acheteur_email' => 22, 'service_id' => 23, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTPubliciteFavorisPeer::getFieldNames($toType);
        $key = isset(CommonTPubliciteFavorisPeer::$fieldKeys[$fromType][$name]) ? CommonTPubliciteFavorisPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTPubliciteFavorisPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTPubliciteFavorisPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTPubliciteFavorisPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTPubliciteFavorisPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTPubliciteFavorisPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::ID);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::ORGANISME);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::OLD_SERVICE_ID);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::LIBELLE);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::REGIME_FINANCIER_CAUTIONNEMENT);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::REGIME_FINANCIER_MODALITES_FINANCEMENT);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::ACHETEUR_CORRESPONDANT);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::ACHETEUR_NOM_ORGANISME);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::ACHETEUR_ADRESSE);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::ACHETEUR_CP);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::ACHETEUR_VILLE);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::ACHETEUR_URL);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::FACTURE_DENOMINATION);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::FACTURE_ADRESSE);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::FACTURE_CP);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::FACTURE_VILLE);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ORGANISME);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_ADRESSE);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_CP);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_VILLE);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::INSTANCE_RECOURS_URL);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::ACHETEUR_TELEPHONE);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::ACHETEUR_EMAIL);
            $criteria->addSelectColumn(CommonTPubliciteFavorisPeer::SERVICE_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.old_service_id');
            $criteria->addSelectColumn($alias . '.libelle');
            $criteria->addSelectColumn($alias . '.regime_financier_cautionnement');
            $criteria->addSelectColumn($alias . '.regime_financier_modalites_financement');
            $criteria->addSelectColumn($alias . '.acheteur_correspondant');
            $criteria->addSelectColumn($alias . '.acheteur_nom_organisme');
            $criteria->addSelectColumn($alias . '.acheteur_adresse');
            $criteria->addSelectColumn($alias . '.acheteur_cp');
            $criteria->addSelectColumn($alias . '.acheteur_ville');
            $criteria->addSelectColumn($alias . '.acheteur_url');
            $criteria->addSelectColumn($alias . '.facture_denomination');
            $criteria->addSelectColumn($alias . '.facture_adresse');
            $criteria->addSelectColumn($alias . '.facture_cp');
            $criteria->addSelectColumn($alias . '.facture_ville');
            $criteria->addSelectColumn($alias . '.instance_recours_organisme');
            $criteria->addSelectColumn($alias . '.instance_recours_adresse');
            $criteria->addSelectColumn($alias . '.instance_recours_cp');
            $criteria->addSelectColumn($alias . '.instance_recours_ville');
            $criteria->addSelectColumn($alias . '.instance_recours_url');
            $criteria->addSelectColumn($alias . '.acheteur_telephone');
            $criteria->addSelectColumn($alias . '.acheteur_email');
            $criteria->addSelectColumn($alias . '.service_id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTPubliciteFavorisPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTPubliciteFavorisPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTPubliciteFavorisPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTPubliciteFavoris
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTPubliciteFavorisPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTPubliciteFavorisPeer::populateObjects(CommonTPubliciteFavorisPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTPubliciteFavorisPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTPubliciteFavorisPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTPubliciteFavoris $obj A CommonTPubliciteFavoris object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTPubliciteFavorisPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTPubliciteFavoris object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTPubliciteFavoris) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTPubliciteFavoris object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTPubliciteFavorisPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTPubliciteFavoris Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTPubliciteFavorisPeer::$instances[$key])) {
                return CommonTPubliciteFavorisPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTPubliciteFavorisPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTPubliciteFavorisPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_publicite_favoris
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTPubliciteFavorisPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTPubliciteFavorisPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTPubliciteFavorisPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTPubliciteFavorisPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTPubliciteFavoris object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTPubliciteFavorisPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTPubliciteFavorisPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTPubliciteFavorisPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTPubliciteFavorisPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTPubliciteFavorisPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTPubliciteFavorisPeer::DATABASE_NAME)->getTable(CommonTPubliciteFavorisPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTPubliciteFavorisPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTPubliciteFavorisPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTPubliciteFavorisTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTPubliciteFavorisPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTPubliciteFavoris or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTPubliciteFavoris object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTPubliciteFavoris object
        }

        if ($criteria->containsKey(CommonTPubliciteFavorisPeer::ID) && $criteria->keyContainsValue(CommonTPubliciteFavorisPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTPubliciteFavorisPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTPubliciteFavorisPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTPubliciteFavoris or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTPubliciteFavoris object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTPubliciteFavorisPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTPubliciteFavorisPeer::ID);
            $value = $criteria->remove(CommonTPubliciteFavorisPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTPubliciteFavorisPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTPubliciteFavorisPeer::TABLE_NAME);
            }

        } else { // $values is CommonTPubliciteFavoris object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTPubliciteFavorisPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_publicite_favoris table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTPubliciteFavorisPeer::TABLE_NAME, $con, CommonTPubliciteFavorisPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTPubliciteFavorisPeer::clearInstancePool();
            CommonTPubliciteFavorisPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTPubliciteFavoris or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTPubliciteFavoris object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTPubliciteFavorisPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTPubliciteFavoris) { // it's a model object
            // invalidate the cache for this single object
            CommonTPubliciteFavorisPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTPubliciteFavorisPeer::DATABASE_NAME);
            $criteria->add(CommonTPubliciteFavorisPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTPubliciteFavorisPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTPubliciteFavorisPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTPubliciteFavorisPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTPubliciteFavoris object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTPubliciteFavoris $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTPubliciteFavorisPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTPubliciteFavorisPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTPubliciteFavorisPeer::DATABASE_NAME, CommonTPubliciteFavorisPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTPubliciteFavoris
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTPubliciteFavorisPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTPubliciteFavorisPeer::DATABASE_NAME);
        $criteria->add(CommonTPubliciteFavorisPeer::ID, $pk);

        $v = CommonTPubliciteFavorisPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTPubliciteFavoris[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTPubliciteFavorisPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTPubliciteFavorisPeer::DATABASE_NAME);
            $criteria->add(CommonTPubliciteFavorisPeer::ID, $pks, Criteria::IN);
            $objs = CommonTPubliciteFavorisPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTPubliciteFavorisPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTPubliciteFavorisPeer::buildTableMap();

