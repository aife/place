<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprise;
use Application\Propel\Mpe\CommonTRoleJuridique;
use Application\Propel\Mpe\CommonTRoleJuridiquePeer;
use Application\Propel\Mpe\CommonTRoleJuridiqueQuery;

/**
 * Base class that represents a query for the 't_role_juridique' table.
 *
 *
 *
 * @method CommonTRoleJuridiqueQuery orderByIdRoleJuridique($order = Criteria::ASC) Order by the id_role_juridique column
 * @method CommonTRoleJuridiqueQuery orderByLibelleRoleJuridique($order = Criteria::ASC) Order by the libelle_role_juridique column
 *
 * @method CommonTRoleJuridiqueQuery groupByIdRoleJuridique() Group by the id_role_juridique column
 * @method CommonTRoleJuridiqueQuery groupByLibelleRoleJuridique() Group by the libelle_role_juridique column
 *
 * @method CommonTRoleJuridiqueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTRoleJuridiqueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTRoleJuridiqueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTRoleJuridiqueQuery leftJoinCommonTMembreGroupementEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
 * @method CommonTRoleJuridiqueQuery rightJoinCommonTMembreGroupementEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
 * @method CommonTRoleJuridiqueQuery innerJoinCommonTMembreGroupementEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
 *
 * @method CommonTRoleJuridique findOne(PropelPDO $con = null) Return the first CommonTRoleJuridique matching the query
 * @method CommonTRoleJuridique findOneOrCreate(PropelPDO $con = null) Return the first CommonTRoleJuridique matching the query, or a new CommonTRoleJuridique object populated from the query conditions when no match is found
 *
 * @method CommonTRoleJuridique findOneByLibelleRoleJuridique(string $libelle_role_juridique) Return the first CommonTRoleJuridique filtered by the libelle_role_juridique column
 *
 * @method array findByIdRoleJuridique(int $id_role_juridique) Return CommonTRoleJuridique objects filtered by the id_role_juridique column
 * @method array findByLibelleRoleJuridique(string $libelle_role_juridique) Return CommonTRoleJuridique objects filtered by the libelle_role_juridique column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTRoleJuridiqueQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTRoleJuridiqueQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTRoleJuridique', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTRoleJuridiqueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTRoleJuridiqueQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTRoleJuridiqueQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTRoleJuridiqueQuery) {
            return $criteria;
        }
        $query = new CommonTRoleJuridiqueQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTRoleJuridique|CommonTRoleJuridique[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTRoleJuridiquePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTRoleJuridiquePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTRoleJuridique A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdRoleJuridique($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTRoleJuridique A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_role_juridique`, `libelle_role_juridique` FROM `t_role_juridique` WHERE `id_role_juridique` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTRoleJuridique();
            $obj->hydrate($row);
            CommonTRoleJuridiquePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTRoleJuridique|CommonTRoleJuridique[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTRoleJuridique[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTRoleJuridiqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTRoleJuridiqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_role_juridique column
     *
     * Example usage:
     * <code>
     * $query->filterByIdRoleJuridique(1234); // WHERE id_role_juridique = 1234
     * $query->filterByIdRoleJuridique(array(12, 34)); // WHERE id_role_juridique IN (12, 34)
     * $query->filterByIdRoleJuridique(array('min' => 12)); // WHERE id_role_juridique >= 12
     * $query->filterByIdRoleJuridique(array('max' => 12)); // WHERE id_role_juridique <= 12
     * </code>
     *
     * @param     mixed $idRoleJuridique The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTRoleJuridiqueQuery The current query, for fluid interface
     */
    public function filterByIdRoleJuridique($idRoleJuridique = null, $comparison = null)
    {
        if (is_array($idRoleJuridique)) {
            $useMinMax = false;
            if (isset($idRoleJuridique['min'])) {
                $this->addUsingAlias(CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $idRoleJuridique['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idRoleJuridique['max'])) {
                $this->addUsingAlias(CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $idRoleJuridique['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $idRoleJuridique, $comparison);
    }

    /**
     * Filter the query on the libelle_role_juridique column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleRoleJuridique('fooValue');   // WHERE libelle_role_juridique = 'fooValue'
     * $query->filterByLibelleRoleJuridique('%fooValue%'); // WHERE libelle_role_juridique LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleRoleJuridique The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTRoleJuridiqueQuery The current query, for fluid interface
     */
    public function filterByLibelleRoleJuridique($libelleRoleJuridique = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleRoleJuridique)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleRoleJuridique)) {
                $libelleRoleJuridique = str_replace('*', '%', $libelleRoleJuridique);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTRoleJuridiquePeer::LIBELLE_ROLE_JURIDIQUE, $libelleRoleJuridique, $comparison);
    }

    /**
     * Filter the query by a related CommonTMembreGroupementEntreprise object
     *
     * @param   CommonTMembreGroupementEntreprise|PropelObjectCollection $commonTMembreGroupementEntreprise  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTRoleJuridiqueQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTMembreGroupementEntreprise($commonTMembreGroupementEntreprise, $comparison = null)
    {
        if ($commonTMembreGroupementEntreprise instanceof CommonTMembreGroupementEntreprise) {
            return $this
                ->addUsingAlias(CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $commonTMembreGroupementEntreprise->getIdRoleJuridique(), $comparison);
        } elseif ($commonTMembreGroupementEntreprise instanceof PropelObjectCollection) {
            return $this
                ->useCommonTMembreGroupementEntrepriseQuery()
                ->filterByPrimaryKeys($commonTMembreGroupementEntreprise->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTMembreGroupementEntreprise() only accepts arguments of type CommonTMembreGroupementEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTMembreGroupementEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTRoleJuridiqueQuery The current query, for fluid interface
     */
    public function joinCommonTMembreGroupementEntreprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTMembreGroupementEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTMembreGroupementEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonTMembreGroupementEntreprise relation CommonTMembreGroupementEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonTMembreGroupementEntrepriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTMembreGroupementEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTMembreGroupementEntreprise', '\Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTRoleJuridique $commonTRoleJuridique Object to remove from the list of results
     *
     * @return CommonTRoleJuridiqueQuery The current query, for fluid interface
     */
    public function prune($commonTRoleJuridique = null)
    {
        if ($commonTRoleJuridique) {
            $this->addUsingAlias(CommonTRoleJuridiquePeer::ID_ROLE_JURIDIQUE, $commonTRoleJuridique->getIdRoleJuridique(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
