<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAnnexeFinanciere;
use Application\Propel\Mpe\CommonAutrePieceConsultation;
use Application\Propel\Mpe\CommonBlobOrganismeFile;
use Application\Propel\Mpe\CommonBlobOrganismeFilePeer;
use Application\Propel\Mpe\CommonBlobOrganismeFileQuery;
use Application\Propel\Mpe\CommonDocumentExterne;
use Application\Propel\Mpe\CommonEchangeDocBlob;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonPieceGenereConsultation;

/**
 * Base class that represents a query for the 'blobOrganisme_file' table.
 *
 *
 *
 * @method CommonBlobOrganismeFileQuery orderByOldId($order = Criteria::ASC) Order by the old_id column
 * @method CommonBlobOrganismeFileQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonBlobOrganismeFileQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method CommonBlobOrganismeFileQuery orderByDeletionDatetime($order = Criteria::ASC) Order by the deletion_datetime column
 * @method CommonBlobOrganismeFileQuery orderByChemin($order = Criteria::ASC) Order by the chemin column
 * @method CommonBlobOrganismeFileQuery orderByDossier($order = Criteria::ASC) Order by the dossier column
 * @method CommonBlobOrganismeFileQuery orderByStatutSynchro($order = Criteria::ASC) Order by the statut_synchro column
 * @method CommonBlobOrganismeFileQuery orderByHash($order = Criteria::ASC) Order by the hash column
 * @method CommonBlobOrganismeFileQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonBlobOrganismeFileQuery orderByExtension($order = Criteria::ASC) Order by the extension column
 *
 * @method CommonBlobOrganismeFileQuery groupByOldId() Group by the old_id column
 * @method CommonBlobOrganismeFileQuery groupByOrganisme() Group by the organisme column
 * @method CommonBlobOrganismeFileQuery groupByName() Group by the name column
 * @method CommonBlobOrganismeFileQuery groupByDeletionDatetime() Group by the deletion_datetime column
 * @method CommonBlobOrganismeFileQuery groupByChemin() Group by the chemin column
 * @method CommonBlobOrganismeFileQuery groupByDossier() Group by the dossier column
 * @method CommonBlobOrganismeFileQuery groupByStatutSynchro() Group by the statut_synchro column
 * @method CommonBlobOrganismeFileQuery groupByHash() Group by the hash column
 * @method CommonBlobOrganismeFileQuery groupById() Group by the id column
 * @method CommonBlobOrganismeFileQuery groupByExtension() Group by the extension column
 *
 * @method CommonBlobOrganismeFileQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonBlobOrganismeFileQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonBlobOrganismeFileQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonBlobOrganismeFileQuery leftJoinCommonDocumentExterne($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonDocumentExterne relation
 * @method CommonBlobOrganismeFileQuery rightJoinCommonDocumentExterne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonDocumentExterne relation
 * @method CommonBlobOrganismeFileQuery innerJoinCommonDocumentExterne($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonDocumentExterne relation
 *
 * @method CommonBlobOrganismeFileQuery leftJoinCommonOffresRelatedByIdBlobHorodatageHash($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOffresRelatedByIdBlobHorodatageHash relation
 * @method CommonBlobOrganismeFileQuery rightJoinCommonOffresRelatedByIdBlobHorodatageHash($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOffresRelatedByIdBlobHorodatageHash relation
 * @method CommonBlobOrganismeFileQuery innerJoinCommonOffresRelatedByIdBlobHorodatageHash($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOffresRelatedByIdBlobHorodatageHash relation
 *
 * @method CommonBlobOrganismeFileQuery leftJoinCommonOffresRelatedByIdBlobXmlReponse($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOffresRelatedByIdBlobXmlReponse relation
 * @method CommonBlobOrganismeFileQuery rightJoinCommonOffresRelatedByIdBlobXmlReponse($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOffresRelatedByIdBlobXmlReponse relation
 * @method CommonBlobOrganismeFileQuery innerJoinCommonOffresRelatedByIdBlobXmlReponse($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOffresRelatedByIdBlobXmlReponse relation
 *
 * @method CommonBlobOrganismeFileQuery leftJoinCommonAnnexeFinanciere($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAnnexeFinanciere relation
 * @method CommonBlobOrganismeFileQuery rightJoinCommonAnnexeFinanciere($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAnnexeFinanciere relation
 * @method CommonBlobOrganismeFileQuery innerJoinCommonAnnexeFinanciere($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAnnexeFinanciere relation
 *
 * @method CommonBlobOrganismeFileQuery leftJoinCommonAutrePieceConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAutrePieceConsultation relation
 * @method CommonBlobOrganismeFileQuery rightJoinCommonAutrePieceConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAutrePieceConsultation relation
 * @method CommonBlobOrganismeFileQuery innerJoinCommonAutrePieceConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAutrePieceConsultation relation
 *
 * @method CommonBlobOrganismeFileQuery leftJoinCommonEchangeDocBlob($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocBlob relation
 * @method CommonBlobOrganismeFileQuery rightJoinCommonEchangeDocBlob($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocBlob relation
 * @method CommonBlobOrganismeFileQuery innerJoinCommonEchangeDocBlob($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocBlob relation
 *
 * @method CommonBlobOrganismeFileQuery leftJoinCommonPieceGenereConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonPieceGenereConsultation relation
 * @method CommonBlobOrganismeFileQuery rightJoinCommonPieceGenereConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonPieceGenereConsultation relation
 * @method CommonBlobOrganismeFileQuery innerJoinCommonPieceGenereConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonPieceGenereConsultation relation
 *
 * @method CommonBlobOrganismeFile findOne(PropelPDO $con = null) Return the first CommonBlobOrganismeFile matching the query
 * @method CommonBlobOrganismeFile findOneOrCreate(PropelPDO $con = null) Return the first CommonBlobOrganismeFile matching the query, or a new CommonBlobOrganismeFile object populated from the query conditions when no match is found
 *
 * @method CommonBlobOrganismeFile findOneByOldId(int $old_id) Return the first CommonBlobOrganismeFile filtered by the old_id column
 * @method CommonBlobOrganismeFile findOneByOrganisme(string $organisme) Return the first CommonBlobOrganismeFile filtered by the organisme column
 * @method CommonBlobOrganismeFile findOneByName(string $name) Return the first CommonBlobOrganismeFile filtered by the name column
 * @method CommonBlobOrganismeFile findOneByDeletionDatetime(string $deletion_datetime) Return the first CommonBlobOrganismeFile filtered by the deletion_datetime column
 * @method CommonBlobOrganismeFile findOneByChemin(string $chemin) Return the first CommonBlobOrganismeFile filtered by the chemin column
 * @method CommonBlobOrganismeFile findOneByDossier(string $dossier) Return the first CommonBlobOrganismeFile filtered by the dossier column
 * @method CommonBlobOrganismeFile findOneByStatutSynchro(int $statut_synchro) Return the first CommonBlobOrganismeFile filtered by the statut_synchro column
 * @method CommonBlobOrganismeFile findOneByHash(string $hash) Return the first CommonBlobOrganismeFile filtered by the hash column
 * @method CommonBlobOrganismeFile findOneByExtension(string $extension) Return the first CommonBlobOrganismeFile filtered by the extension column
 *
 * @method array findByOldId(int $old_id) Return CommonBlobOrganismeFile objects filtered by the old_id column
 * @method array findByOrganisme(string $organisme) Return CommonBlobOrganismeFile objects filtered by the organisme column
 * @method array findByName(string $name) Return CommonBlobOrganismeFile objects filtered by the name column
 * @method array findByDeletionDatetime(string $deletion_datetime) Return CommonBlobOrganismeFile objects filtered by the deletion_datetime column
 * @method array findByChemin(string $chemin) Return CommonBlobOrganismeFile objects filtered by the chemin column
 * @method array findByDossier(string $dossier) Return CommonBlobOrganismeFile objects filtered by the dossier column
 * @method array findByStatutSynchro(int $statut_synchro) Return CommonBlobOrganismeFile objects filtered by the statut_synchro column
 * @method array findByHash(string $hash) Return CommonBlobOrganismeFile objects filtered by the hash column
 * @method array findById(int $id) Return CommonBlobOrganismeFile objects filtered by the id column
 * @method array findByExtension(string $extension) Return CommonBlobOrganismeFile objects filtered by the extension column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonBlobOrganismeFileQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonBlobOrganismeFileQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonBlobOrganismeFile', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonBlobOrganismeFileQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonBlobOrganismeFileQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonBlobOrganismeFileQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonBlobOrganismeFileQuery) {
            return $criteria;
        }
        $query = new CommonBlobOrganismeFileQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonBlobOrganismeFile|CommonBlobOrganismeFile[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonBlobOrganismeFilePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonBlobOrganismeFilePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonBlobOrganismeFile A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonBlobOrganismeFile A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `old_id`, `organisme`, `name`, `deletion_datetime`, `chemin`, `dossier`, `statut_synchro`, `hash`, `id`, `extension` FROM `blobOrganisme_file` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonBlobOrganismeFile();
            $obj->hydrate($row);
            CommonBlobOrganismeFilePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonBlobOrganismeFile|CommonBlobOrganismeFile[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonBlobOrganismeFile[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the old_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldId(1234); // WHERE old_id = 1234
     * $query->filterByOldId(array(12, 34)); // WHERE old_id IN (12, 34)
     * $query->filterByOldId(array('min' => 12)); // WHERE old_id >= 12
     * $query->filterByOldId(array('max' => 12)); // WHERE old_id <= 12
     * </code>
     *
     * @param     mixed $oldId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterByOldId($oldId = null, $comparison = null)
    {
        if (is_array($oldId)) {
            $useMinMax = false;
            if (isset($oldId['min'])) {
                $this->addUsingAlias(CommonBlobOrganismeFilePeer::OLD_ID, $oldId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldId['max'])) {
                $this->addUsingAlias(CommonBlobOrganismeFilePeer::OLD_ID, $oldId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::OLD_ID, $oldId, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the deletion_datetime column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletionDatetime('2011-03-14'); // WHERE deletion_datetime = '2011-03-14'
     * $query->filterByDeletionDatetime('now'); // WHERE deletion_datetime = '2011-03-14'
     * $query->filterByDeletionDatetime(array('max' => 'yesterday')); // WHERE deletion_datetime > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletionDatetime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterByDeletionDatetime($deletionDatetime = null, $comparison = null)
    {
        if (is_array($deletionDatetime)) {
            $useMinMax = false;
            if (isset($deletionDatetime['min'])) {
                $this->addUsingAlias(CommonBlobOrganismeFilePeer::DELETION_DATETIME, $deletionDatetime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletionDatetime['max'])) {
                $this->addUsingAlias(CommonBlobOrganismeFilePeer::DELETION_DATETIME, $deletionDatetime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::DELETION_DATETIME, $deletionDatetime, $comparison);
    }

    /**
     * Filter the query on the chemin column
     *
     * Example usage:
     * <code>
     * $query->filterByChemin('fooValue');   // WHERE chemin = 'fooValue'
     * $query->filterByChemin('%fooValue%'); // WHERE chemin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chemin The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterByChemin($chemin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chemin)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $chemin)) {
                $chemin = str_replace('*', '%', $chemin);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::CHEMIN, $chemin, $comparison);
    }

    /**
     * Filter the query on the dossier column
     *
     * Example usage:
     * <code>
     * $query->filterByDossier('fooValue');   // WHERE dossier = 'fooValue'
     * $query->filterByDossier('%fooValue%'); // WHERE dossier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dossier The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterByDossier($dossier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dossier)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dossier)) {
                $dossier = str_replace('*', '%', $dossier);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::DOSSIER, $dossier, $comparison);
    }

    /**
     * Filter the query on the statut_synchro column
     *
     * Example usage:
     * <code>
     * $query->filterByStatutSynchro(1234); // WHERE statut_synchro = 1234
     * $query->filterByStatutSynchro(array(12, 34)); // WHERE statut_synchro IN (12, 34)
     * $query->filterByStatutSynchro(array('min' => 12)); // WHERE statut_synchro >= 12
     * $query->filterByStatutSynchro(array('max' => 12)); // WHERE statut_synchro <= 12
     * </code>
     *
     * @param     mixed $statutSynchro The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterByStatutSynchro($statutSynchro = null, $comparison = null)
    {
        if (is_array($statutSynchro)) {
            $useMinMax = false;
            if (isset($statutSynchro['min'])) {
                $this->addUsingAlias(CommonBlobOrganismeFilePeer::STATUT_SYNCHRO, $statutSynchro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statutSynchro['max'])) {
                $this->addUsingAlias(CommonBlobOrganismeFilePeer::STATUT_SYNCHRO, $statutSynchro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::STATUT_SYNCHRO, $statutSynchro, $comparison);
    }

    /**
     * Filter the query on the hash column
     *
     * Example usage:
     * <code>
     * $query->filterByHash('fooValue');   // WHERE hash = 'fooValue'
     * $query->filterByHash('%fooValue%'); // WHERE hash LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hash The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterByHash($hash = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hash)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $hash)) {
                $hash = str_replace('*', '%', $hash);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::HASH, $hash, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the extension column
     *
     * Example usage:
     * <code>
     * $query->filterByExtension('fooValue');   // WHERE extension = 'fooValue'
     * $query->filterByExtension('%fooValue%'); // WHERE extension LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extension The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function filterByExtension($extension = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extension)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $extension)) {
                $extension = str_replace('*', '%', $extension);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonBlobOrganismeFilePeer::EXTENSION, $extension, $comparison);
    }

    /**
     * Filter the query by a related CommonDocumentExterne object
     *
     * @param   CommonDocumentExterne|PropelObjectCollection $commonDocumentExterne  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonBlobOrganismeFileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonDocumentExterne($commonDocumentExterne, $comparison = null)
    {
        if ($commonDocumentExterne instanceof CommonDocumentExterne) {
            return $this
                ->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $commonDocumentExterne->getIdblob(), $comparison);
        } elseif ($commonDocumentExterne instanceof PropelObjectCollection) {
            return $this
                ->useCommonDocumentExterneQuery()
                ->filterByPrimaryKeys($commonDocumentExterne->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonDocumentExterne() only accepts arguments of type CommonDocumentExterne or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonDocumentExterne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function joinCommonDocumentExterne($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonDocumentExterne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonDocumentExterne');
        }

        return $this;
    }

    /**
     * Use the CommonDocumentExterne relation CommonDocumentExterne object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonDocumentExterneQuery A secondary query class using the current class as primary query
     */
    public function useCommonDocumentExterneQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonDocumentExterne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonDocumentExterne', '\Application\Propel\Mpe\CommonDocumentExterneQuery');
    }

    /**
     * Filter the query by a related CommonOffres object
     *
     * @param   CommonOffres|PropelObjectCollection $commonOffres  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonBlobOrganismeFileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOffresRelatedByIdBlobHorodatageHash($commonOffres, $comparison = null)
    {
        if ($commonOffres instanceof CommonOffres) {
            return $this
                ->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $commonOffres->getIdBlobHorodatageHash(), $comparison);
        } elseif ($commonOffres instanceof PropelObjectCollection) {
            return $this
                ->useCommonOffresRelatedByIdBlobHorodatageHashQuery()
                ->filterByPrimaryKeys($commonOffres->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonOffresRelatedByIdBlobHorodatageHash() only accepts arguments of type CommonOffres or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOffresRelatedByIdBlobHorodatageHash relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function joinCommonOffresRelatedByIdBlobHorodatageHash($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOffresRelatedByIdBlobHorodatageHash');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOffresRelatedByIdBlobHorodatageHash');
        }

        return $this;
    }

    /**
     * Use the CommonOffresRelatedByIdBlobHorodatageHash relation CommonOffres object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOffresQuery A secondary query class using the current class as primary query
     */
    public function useCommonOffresRelatedByIdBlobHorodatageHashQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonOffresRelatedByIdBlobHorodatageHash($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOffresRelatedByIdBlobHorodatageHash', '\Application\Propel\Mpe\CommonOffresQuery');
    }

    /**
     * Filter the query by a related CommonOffres object
     *
     * @param   CommonOffres|PropelObjectCollection $commonOffres  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonBlobOrganismeFileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOffresRelatedByIdBlobXmlReponse($commonOffres, $comparison = null)
    {
        if ($commonOffres instanceof CommonOffres) {
            return $this
                ->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $commonOffres->getIdBlobXmlReponse(), $comparison);
        } elseif ($commonOffres instanceof PropelObjectCollection) {
            return $this
                ->useCommonOffresRelatedByIdBlobXmlReponseQuery()
                ->filterByPrimaryKeys($commonOffres->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonOffresRelatedByIdBlobXmlReponse() only accepts arguments of type CommonOffres or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOffresRelatedByIdBlobXmlReponse relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function joinCommonOffresRelatedByIdBlobXmlReponse($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOffresRelatedByIdBlobXmlReponse');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOffresRelatedByIdBlobXmlReponse');
        }

        return $this;
    }

    /**
     * Use the CommonOffresRelatedByIdBlobXmlReponse relation CommonOffres object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOffresQuery A secondary query class using the current class as primary query
     */
    public function useCommonOffresRelatedByIdBlobXmlReponseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonOffresRelatedByIdBlobXmlReponse($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOffresRelatedByIdBlobXmlReponse', '\Application\Propel\Mpe\CommonOffresQuery');
    }

    /**
     * Filter the query by a related CommonAnnexeFinanciere object
     *
     * @param   CommonAnnexeFinanciere|PropelObjectCollection $commonAnnexeFinanciere  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonBlobOrganismeFileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAnnexeFinanciere($commonAnnexeFinanciere, $comparison = null)
    {
        if ($commonAnnexeFinanciere instanceof CommonAnnexeFinanciere) {
            return $this
                ->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $commonAnnexeFinanciere->getIdBlob(), $comparison);
        } elseif ($commonAnnexeFinanciere instanceof PropelObjectCollection) {
            return $this
                ->useCommonAnnexeFinanciereQuery()
                ->filterByPrimaryKeys($commonAnnexeFinanciere->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonAnnexeFinanciere() only accepts arguments of type CommonAnnexeFinanciere or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAnnexeFinanciere relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function joinCommonAnnexeFinanciere($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAnnexeFinanciere');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAnnexeFinanciere');
        }

        return $this;
    }

    /**
     * Use the CommonAnnexeFinanciere relation CommonAnnexeFinanciere object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAnnexeFinanciereQuery A secondary query class using the current class as primary query
     */
    public function useCommonAnnexeFinanciereQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonAnnexeFinanciere($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAnnexeFinanciere', '\Application\Propel\Mpe\CommonAnnexeFinanciereQuery');
    }

    /**
     * Filter the query by a related CommonAutrePieceConsultation object
     *
     * @param   CommonAutrePieceConsultation|PropelObjectCollection $commonAutrePieceConsultation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonBlobOrganismeFileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAutrePieceConsultation($commonAutrePieceConsultation, $comparison = null)
    {
        if ($commonAutrePieceConsultation instanceof CommonAutrePieceConsultation) {
            return $this
                ->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $commonAutrePieceConsultation->getBlobId(), $comparison);
        } elseif ($commonAutrePieceConsultation instanceof PropelObjectCollection) {
            return $this
                ->useCommonAutrePieceConsultationQuery()
                ->filterByPrimaryKeys($commonAutrePieceConsultation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonAutrePieceConsultation() only accepts arguments of type CommonAutrePieceConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAutrePieceConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function joinCommonAutrePieceConsultation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAutrePieceConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAutrePieceConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonAutrePieceConsultation relation CommonAutrePieceConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAutrePieceConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonAutrePieceConsultationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonAutrePieceConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAutrePieceConsultation', '\Application\Propel\Mpe\CommonAutrePieceConsultationQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDocBlob object
     *
     * @param   CommonEchangeDocBlob|PropelObjectCollection $commonEchangeDocBlob  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonBlobOrganismeFileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocBlob($commonEchangeDocBlob, $comparison = null)
    {
        if ($commonEchangeDocBlob instanceof CommonEchangeDocBlob) {
            return $this
                ->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $commonEchangeDocBlob->getBlobOrganismeId(), $comparison);
        } elseif ($commonEchangeDocBlob instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeDocBlobQuery()
                ->filterByPrimaryKeys($commonEchangeDocBlob->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangeDocBlob() only accepts arguments of type CommonEchangeDocBlob or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocBlob relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocBlob($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocBlob');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocBlob');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocBlob relation CommonEchangeDocBlob object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocBlobQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocBlobQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocBlob($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocBlob', '\Application\Propel\Mpe\CommonEchangeDocBlobQuery');
    }

    /**
     * Filter the query by a related CommonPieceGenereConsultation object
     *
     * @param   CommonPieceGenereConsultation|PropelObjectCollection $commonPieceGenereConsultation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonBlobOrganismeFileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonPieceGenereConsultation($commonPieceGenereConsultation, $comparison = null)
    {
        if ($commonPieceGenereConsultation instanceof CommonPieceGenereConsultation) {
            return $this
                ->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $commonPieceGenereConsultation->getBlobId(), $comparison);
        } elseif ($commonPieceGenereConsultation instanceof PropelObjectCollection) {
            return $this
                ->useCommonPieceGenereConsultationQuery()
                ->filterByPrimaryKeys($commonPieceGenereConsultation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonPieceGenereConsultation() only accepts arguments of type CommonPieceGenereConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonPieceGenereConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function joinCommonPieceGenereConsultation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonPieceGenereConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonPieceGenereConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonPieceGenereConsultation relation CommonPieceGenereConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonPieceGenereConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonPieceGenereConsultationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonPieceGenereConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonPieceGenereConsultation', '\Application\Propel\Mpe\CommonPieceGenereConsultationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonBlobOrganismeFile $commonBlobOrganismeFile Object to remove from the list of results
     *
     * @return CommonBlobOrganismeFileQuery The current query, for fluid interface
     */
    public function prune($commonBlobOrganismeFile = null)
    {
        if ($commonBlobOrganismeFile) {
            $this->addUsingAlias(CommonBlobOrganismeFilePeer::ID, $commonBlobOrganismeFile->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
