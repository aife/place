<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTTelechargementAsynchrone;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneFichier;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneFichierPeer;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneFichierQuery;

/**
 * Base class that represents a query for the 'T_Telechargement_Asynchrone_fichier' table.
 *
 *
 *
 * @method CommonTTelechargementAsynchroneFichierQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTTelechargementAsynchroneFichierQuery orderByIdTelechargementAsynchrone($order = Criteria::ASC) Order by the id_telechargement_asynchrone column
 * @method CommonTTelechargementAsynchroneFichierQuery orderByIdReferenceTelechargement($order = Criteria::ASC) Order by the id_reference_telechargement column
 *
 * @method CommonTTelechargementAsynchroneFichierQuery groupById() Group by the id column
 * @method CommonTTelechargementAsynchroneFichierQuery groupByIdTelechargementAsynchrone() Group by the id_telechargement_asynchrone column
 * @method CommonTTelechargementAsynchroneFichierQuery groupByIdReferenceTelechargement() Group by the id_reference_telechargement column
 *
 * @method CommonTTelechargementAsynchroneFichierQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTTelechargementAsynchroneFichierQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTTelechargementAsynchroneFichierQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTTelechargementAsynchroneFichierQuery leftJoinCommonTTelechargementAsynchrone($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTTelechargementAsynchrone relation
 * @method CommonTTelechargementAsynchroneFichierQuery rightJoinCommonTTelechargementAsynchrone($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTTelechargementAsynchrone relation
 * @method CommonTTelechargementAsynchroneFichierQuery innerJoinCommonTTelechargementAsynchrone($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTTelechargementAsynchrone relation
 *
 * @method CommonTTelechargementAsynchroneFichier findOne(PropelPDO $con = null) Return the first CommonTTelechargementAsynchroneFichier matching the query
 * @method CommonTTelechargementAsynchroneFichier findOneOrCreate(PropelPDO $con = null) Return the first CommonTTelechargementAsynchroneFichier matching the query, or a new CommonTTelechargementAsynchroneFichier object populated from the query conditions when no match is found
 *
 * @method CommonTTelechargementAsynchroneFichier findOneByIdTelechargementAsynchrone(int $id_telechargement_asynchrone) Return the first CommonTTelechargementAsynchroneFichier filtered by the id_telechargement_asynchrone column
 * @method CommonTTelechargementAsynchroneFichier findOneByIdReferenceTelechargement(int $id_reference_telechargement) Return the first CommonTTelechargementAsynchroneFichier filtered by the id_reference_telechargement column
 *
 * @method array findById(int $id) Return CommonTTelechargementAsynchroneFichier objects filtered by the id column
 * @method array findByIdTelechargementAsynchrone(int $id_telechargement_asynchrone) Return CommonTTelechargementAsynchroneFichier objects filtered by the id_telechargement_asynchrone column
 * @method array findByIdReferenceTelechargement(int $id_reference_telechargement) Return CommonTTelechargementAsynchroneFichier objects filtered by the id_reference_telechargement column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTTelechargementAsynchroneFichierQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTTelechargementAsynchroneFichierQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTTelechargementAsynchroneFichier', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTTelechargementAsynchroneFichierQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTTelechargementAsynchroneFichierQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTTelechargementAsynchroneFichierQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTTelechargementAsynchroneFichierQuery) {
            return $criteria;
        }
        $query = new CommonTTelechargementAsynchroneFichierQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTTelechargementAsynchroneFichier|CommonTTelechargementAsynchroneFichier[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTTelechargementAsynchroneFichierPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchroneFichierPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTelechargementAsynchroneFichier A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTelechargementAsynchroneFichier A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_telechargement_asynchrone`, `id_reference_telechargement` FROM `T_Telechargement_Asynchrone_fichier` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTTelechargementAsynchroneFichier();
            $obj->hydrate($row);
            CommonTTelechargementAsynchroneFichierPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTTelechargementAsynchroneFichier|CommonTTelechargementAsynchroneFichier[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTTelechargementAsynchroneFichier[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTTelechargementAsynchroneFichierQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTTelechargementAsynchroneFichierQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneFichierQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_telechargement_asynchrone column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTelechargementAsynchrone(1234); // WHERE id_telechargement_asynchrone = 1234
     * $query->filterByIdTelechargementAsynchrone(array(12, 34)); // WHERE id_telechargement_asynchrone IN (12, 34)
     * $query->filterByIdTelechargementAsynchrone(array('min' => 12)); // WHERE id_telechargement_asynchrone >= 12
     * $query->filterByIdTelechargementAsynchrone(array('max' => 12)); // WHERE id_telechargement_asynchrone <= 12
     * </code>
     *
     * @see       filterByCommonTTelechargementAsynchrone()
     *
     * @param     mixed $idTelechargementAsynchrone The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneFichierQuery The current query, for fluid interface
     */
    public function filterByIdTelechargementAsynchrone($idTelechargementAsynchrone = null, $comparison = null)
    {
        if (is_array($idTelechargementAsynchrone)) {
            $useMinMax = false;
            if (isset($idTelechargementAsynchrone['min'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID_TELECHARGEMENT_ASYNCHRONE, $idTelechargementAsynchrone['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTelechargementAsynchrone['max'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID_TELECHARGEMENT_ASYNCHRONE, $idTelechargementAsynchrone['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID_TELECHARGEMENT_ASYNCHRONE, $idTelechargementAsynchrone, $comparison);
    }

    /**
     * Filter the query on the id_reference_telechargement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdReferenceTelechargement(1234); // WHERE id_reference_telechargement = 1234
     * $query->filterByIdReferenceTelechargement(array(12, 34)); // WHERE id_reference_telechargement IN (12, 34)
     * $query->filterByIdReferenceTelechargement(array('min' => 12)); // WHERE id_reference_telechargement >= 12
     * $query->filterByIdReferenceTelechargement(array('max' => 12)); // WHERE id_reference_telechargement <= 12
     * </code>
     *
     * @param     mixed $idReferenceTelechargement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTelechargementAsynchroneFichierQuery The current query, for fluid interface
     */
    public function filterByIdReferenceTelechargement($idReferenceTelechargement = null, $comparison = null)
    {
        if (is_array($idReferenceTelechargement)) {
            $useMinMax = false;
            if (isset($idReferenceTelechargement['min'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID_REFERENCE_TELECHARGEMENT, $idReferenceTelechargement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idReferenceTelechargement['max'])) {
                $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID_REFERENCE_TELECHARGEMENT, $idReferenceTelechargement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID_REFERENCE_TELECHARGEMENT, $idReferenceTelechargement, $comparison);
    }

    /**
     * Filter the query by a related CommonTTelechargementAsynchrone object
     *
     * @param   CommonTTelechargementAsynchrone|PropelObjectCollection $commonTTelechargementAsynchrone The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTTelechargementAsynchroneFichierQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTTelechargementAsynchrone($commonTTelechargementAsynchrone, $comparison = null)
    {
        if ($commonTTelechargementAsynchrone instanceof CommonTTelechargementAsynchrone) {
            return $this
                ->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID_TELECHARGEMENT_ASYNCHRONE, $commonTTelechargementAsynchrone->getId(), $comparison);
        } elseif ($commonTTelechargementAsynchrone instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID_TELECHARGEMENT_ASYNCHRONE, $commonTTelechargementAsynchrone->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTTelechargementAsynchrone() only accepts arguments of type CommonTTelechargementAsynchrone or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTTelechargementAsynchrone relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTTelechargementAsynchroneFichierQuery The current query, for fluid interface
     */
    public function joinCommonTTelechargementAsynchrone($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTTelechargementAsynchrone');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTTelechargementAsynchrone');
        }

        return $this;
    }

    /**
     * Use the CommonTTelechargementAsynchrone relation CommonTTelechargementAsynchrone object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTTelechargementAsynchroneQuery A secondary query class using the current class as primary query
     */
    public function useCommonTTelechargementAsynchroneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTTelechargementAsynchrone($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTTelechargementAsynchrone', '\Application\Propel\Mpe\CommonTTelechargementAsynchroneQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTTelechargementAsynchroneFichier $commonTTelechargementAsynchroneFichier Object to remove from the list of results
     *
     * @return CommonTTelechargementAsynchroneFichierQuery The current query, for fluid interface
     */
    public function prune($commonTTelechargementAsynchroneFichier = null)
    {
        if ($commonTTelechargementAsynchroneFichier) {
            $this->addUsingAlias(CommonTTelechargementAsynchroneFichierPeer::ID, $commonTTelechargementAsynchroneFichier->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
