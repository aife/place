<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationQuery;
use Application\Propel\Mpe\CommonTSupportPublication;

/**
 * Base class that represents a query for the 't_support_annonce_consultation' table.
 *
 *
 *
 * @method CommonTSupportAnnonceConsultationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTSupportAnnonceConsultationQuery orderByIdSupport($order = Criteria::ASC) Order by the id_support column
 * @method CommonTSupportAnnonceConsultationQuery orderByIdAnnonceCons($order = Criteria::ASC) Order by the id_annonce_cons column
 * @method CommonTSupportAnnonceConsultationQuery orderByPrenomNomAgentCreateur($order = Criteria::ASC) Order by the prenom_nom_agent_createur column
 * @method CommonTSupportAnnonceConsultationQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 * @method CommonTSupportAnnonceConsultationQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTSupportAnnonceConsultationQuery orderByStatut($order = Criteria::ASC) Order by the statut column
 * @method CommonTSupportAnnonceConsultationQuery orderByDateStatut($order = Criteria::ASC) Order by the date_statut column
 * @method CommonTSupportAnnonceConsultationQuery orderByNumeroAvis($order = Criteria::ASC) Order by the numero_avis column
 * @method CommonTSupportAnnonceConsultationQuery orderByMessageStatut($order = Criteria::ASC) Order by the message_statut column
 * @method CommonTSupportAnnonceConsultationQuery orderByLienPublication($order = Criteria::ASC) Order by the lien_publication column
 * @method CommonTSupportAnnonceConsultationQuery orderByDateEnvoiSupport($order = Criteria::ASC) Order by the date_envoi_support column
 * @method CommonTSupportAnnonceConsultationQuery orderByDatePublicationSupport($order = Criteria::ASC) Order by the date_publication_support column
 * @method CommonTSupportAnnonceConsultationQuery orderByNumeroAvisParent($order = Criteria::ASC) Order by the numero_avis_parent column
 * @method CommonTSupportAnnonceConsultationQuery orderByIdOffre($order = Criteria::ASC) Order by the id_offre column
 * @method CommonTSupportAnnonceConsultationQuery orderByMessageAcheteur($order = Criteria::ASC) Order by the message_acheteur column
 * @method CommonTSupportAnnonceConsultationQuery orderByDepartementsParutionAnnonce($order = Criteria::ASC) Order by the departements_parution_annonce column
 *
 * @method CommonTSupportAnnonceConsultationQuery groupById() Group by the id column
 * @method CommonTSupportAnnonceConsultationQuery groupByIdSupport() Group by the id_support column
 * @method CommonTSupportAnnonceConsultationQuery groupByIdAnnonceCons() Group by the id_annonce_cons column
 * @method CommonTSupportAnnonceConsultationQuery groupByPrenomNomAgentCreateur() Group by the prenom_nom_agent_createur column
 * @method CommonTSupportAnnonceConsultationQuery groupByIdAgent() Group by the id_agent column
 * @method CommonTSupportAnnonceConsultationQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTSupportAnnonceConsultationQuery groupByStatut() Group by the statut column
 * @method CommonTSupportAnnonceConsultationQuery groupByDateStatut() Group by the date_statut column
 * @method CommonTSupportAnnonceConsultationQuery groupByNumeroAvis() Group by the numero_avis column
 * @method CommonTSupportAnnonceConsultationQuery groupByMessageStatut() Group by the message_statut column
 * @method CommonTSupportAnnonceConsultationQuery groupByLienPublication() Group by the lien_publication column
 * @method CommonTSupportAnnonceConsultationQuery groupByDateEnvoiSupport() Group by the date_envoi_support column
 * @method CommonTSupportAnnonceConsultationQuery groupByDatePublicationSupport() Group by the date_publication_support column
 * @method CommonTSupportAnnonceConsultationQuery groupByNumeroAvisParent() Group by the numero_avis_parent column
 * @method CommonTSupportAnnonceConsultationQuery groupByIdOffre() Group by the id_offre column
 * @method CommonTSupportAnnonceConsultationQuery groupByMessageAcheteur() Group by the message_acheteur column
 * @method CommonTSupportAnnonceConsultationQuery groupByDepartementsParutionAnnonce() Group by the departements_parution_annonce column
 *
 * @method CommonTSupportAnnonceConsultationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTSupportAnnonceConsultationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTSupportAnnonceConsultationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTSupportAnnonceConsultationQuery leftJoinCommonTAnnonceConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTAnnonceConsultation relation
 * @method CommonTSupportAnnonceConsultationQuery rightJoinCommonTAnnonceConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTAnnonceConsultation relation
 * @method CommonTSupportAnnonceConsultationQuery innerJoinCommonTAnnonceConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTAnnonceConsultation relation
 *
 * @method CommonTSupportAnnonceConsultationQuery leftJoinCommonTSupportPublication($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTSupportPublication relation
 * @method CommonTSupportAnnonceConsultationQuery rightJoinCommonTSupportPublication($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTSupportPublication relation
 * @method CommonTSupportAnnonceConsultationQuery innerJoinCommonTSupportPublication($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTSupportPublication relation
 *
 * @method CommonTSupportAnnonceConsultation findOne(PropelPDO $con = null) Return the first CommonTSupportAnnonceConsultation matching the query
 * @method CommonTSupportAnnonceConsultation findOneOrCreate(PropelPDO $con = null) Return the first CommonTSupportAnnonceConsultation matching the query, or a new CommonTSupportAnnonceConsultation object populated from the query conditions when no match is found
 *
 * @method CommonTSupportAnnonceConsultation findOneByIdSupport(int $id_support) Return the first CommonTSupportAnnonceConsultation filtered by the id_support column
 * @method CommonTSupportAnnonceConsultation findOneByIdAnnonceCons(int $id_annonce_cons) Return the first CommonTSupportAnnonceConsultation filtered by the id_annonce_cons column
 * @method CommonTSupportAnnonceConsultation findOneByPrenomNomAgentCreateur(string $prenom_nom_agent_createur) Return the first CommonTSupportAnnonceConsultation filtered by the prenom_nom_agent_createur column
 * @method CommonTSupportAnnonceConsultation findOneByIdAgent(int $id_agent) Return the first CommonTSupportAnnonceConsultation filtered by the id_agent column
 * @method CommonTSupportAnnonceConsultation findOneByDateCreation(string $date_creation) Return the first CommonTSupportAnnonceConsultation filtered by the date_creation column
 * @method CommonTSupportAnnonceConsultation findOneByStatut(string $statut) Return the first CommonTSupportAnnonceConsultation filtered by the statut column
 * @method CommonTSupportAnnonceConsultation findOneByDateStatut(string $date_statut) Return the first CommonTSupportAnnonceConsultation filtered by the date_statut column
 * @method CommonTSupportAnnonceConsultation findOneByNumeroAvis(string $numero_avis) Return the first CommonTSupportAnnonceConsultation filtered by the numero_avis column
 * @method CommonTSupportAnnonceConsultation findOneByMessageStatut(string $message_statut) Return the first CommonTSupportAnnonceConsultation filtered by the message_statut column
 * @method CommonTSupportAnnonceConsultation findOneByLienPublication(string $lien_publication) Return the first CommonTSupportAnnonceConsultation filtered by the lien_publication column
 * @method CommonTSupportAnnonceConsultation findOneByDateEnvoiSupport(string $date_envoi_support) Return the first CommonTSupportAnnonceConsultation filtered by the date_envoi_support column
 * @method CommonTSupportAnnonceConsultation findOneByDatePublicationSupport(string $date_publication_support) Return the first CommonTSupportAnnonceConsultation filtered by the date_publication_support column
 * @method CommonTSupportAnnonceConsultation findOneByNumeroAvisParent(string $numero_avis_parent) Return the first CommonTSupportAnnonceConsultation filtered by the numero_avis_parent column
 * @method CommonTSupportAnnonceConsultation findOneByIdOffre(int $id_offre) Return the first CommonTSupportAnnonceConsultation filtered by the id_offre column
 * @method CommonTSupportAnnonceConsultation findOneByMessageAcheteur(string $message_acheteur) Return the first CommonTSupportAnnonceConsultation filtered by the message_acheteur column
 * @method CommonTSupportAnnonceConsultation findOneByDepartementsParutionAnnonce(string $departements_parution_annonce) Return the first CommonTSupportAnnonceConsultation filtered by the departements_parution_annonce column
 *
 * @method array findById(int $id) Return CommonTSupportAnnonceConsultation objects filtered by the id column
 * @method array findByIdSupport(int $id_support) Return CommonTSupportAnnonceConsultation objects filtered by the id_support column
 * @method array findByIdAnnonceCons(int $id_annonce_cons) Return CommonTSupportAnnonceConsultation objects filtered by the id_annonce_cons column
 * @method array findByPrenomNomAgentCreateur(string $prenom_nom_agent_createur) Return CommonTSupportAnnonceConsultation objects filtered by the prenom_nom_agent_createur column
 * @method array findByIdAgent(int $id_agent) Return CommonTSupportAnnonceConsultation objects filtered by the id_agent column
 * @method array findByDateCreation(string $date_creation) Return CommonTSupportAnnonceConsultation objects filtered by the date_creation column
 * @method array findByStatut(string $statut) Return CommonTSupportAnnonceConsultation objects filtered by the statut column
 * @method array findByDateStatut(string $date_statut) Return CommonTSupportAnnonceConsultation objects filtered by the date_statut column
 * @method array findByNumeroAvis(string $numero_avis) Return CommonTSupportAnnonceConsultation objects filtered by the numero_avis column
 * @method array findByMessageStatut(string $message_statut) Return CommonTSupportAnnonceConsultation objects filtered by the message_statut column
 * @method array findByLienPublication(string $lien_publication) Return CommonTSupportAnnonceConsultation objects filtered by the lien_publication column
 * @method array findByDateEnvoiSupport(string $date_envoi_support) Return CommonTSupportAnnonceConsultation objects filtered by the date_envoi_support column
 * @method array findByDatePublicationSupport(string $date_publication_support) Return CommonTSupportAnnonceConsultation objects filtered by the date_publication_support column
 * @method array findByNumeroAvisParent(string $numero_avis_parent) Return CommonTSupportAnnonceConsultation objects filtered by the numero_avis_parent column
 * @method array findByIdOffre(int $id_offre) Return CommonTSupportAnnonceConsultation objects filtered by the id_offre column
 * @method array findByMessageAcheteur(string $message_acheteur) Return CommonTSupportAnnonceConsultation objects filtered by the message_acheteur column
 * @method array findByDepartementsParutionAnnonce(string $departements_parution_annonce) Return CommonTSupportAnnonceConsultation objects filtered by the departements_parution_annonce column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTSupportAnnonceConsultationQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTSupportAnnonceConsultationQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTSupportAnnonceConsultation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTSupportAnnonceConsultationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTSupportAnnonceConsultationQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTSupportAnnonceConsultationQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTSupportAnnonceConsultationQuery) {
            return $criteria;
        }
        $query = new CommonTSupportAnnonceConsultationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTSupportAnnonceConsultation|CommonTSupportAnnonceConsultation[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTSupportAnnonceConsultationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTSupportAnnonceConsultation A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTSupportAnnonceConsultation A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_support`, `id_annonce_cons`, `prenom_nom_agent_createur`, `id_agent`, `date_creation`, `statut`, `date_statut`, `numero_avis`, `message_statut`, `lien_publication`, `date_envoi_support`, `date_publication_support`, `numero_avis_parent`, `id_offre`, `message_acheteur`, `departements_parution_annonce` FROM `t_support_annonce_consultation` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTSupportAnnonceConsultation();
            $obj->hydrate($row);
            CommonTSupportAnnonceConsultationPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTSupportAnnonceConsultation|CommonTSupportAnnonceConsultation[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTSupportAnnonceConsultation[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_support column
     *
     * Example usage:
     * <code>
     * $query->filterByIdSupport(1234); // WHERE id_support = 1234
     * $query->filterByIdSupport(array(12, 34)); // WHERE id_support IN (12, 34)
     * $query->filterByIdSupport(array('min' => 12)); // WHERE id_support >= 12
     * $query->filterByIdSupport(array('max' => 12)); // WHERE id_support <= 12
     * </code>
     *
     * @see       filterByCommonTSupportPublication()
     *
     * @param     mixed $idSupport The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByIdSupport($idSupport = null, $comparison = null)
    {
        if (is_array($idSupport)) {
            $useMinMax = false;
            if (isset($idSupport['min'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, $idSupport['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idSupport['max'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, $idSupport['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, $idSupport, $comparison);
    }

    /**
     * Filter the query on the id_annonce_cons column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAnnonceCons(1234); // WHERE id_annonce_cons = 1234
     * $query->filterByIdAnnonceCons(array(12, 34)); // WHERE id_annonce_cons IN (12, 34)
     * $query->filterByIdAnnonceCons(array('min' => 12)); // WHERE id_annonce_cons >= 12
     * $query->filterByIdAnnonceCons(array('max' => 12)); // WHERE id_annonce_cons <= 12
     * </code>
     *
     * @see       filterByCommonTAnnonceConsultation()
     *
     * @param     mixed $idAnnonceCons The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByIdAnnonceCons($idAnnonceCons = null, $comparison = null)
    {
        if (is_array($idAnnonceCons)) {
            $useMinMax = false;
            if (isset($idAnnonceCons['min'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, $idAnnonceCons['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAnnonceCons['max'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, $idAnnonceCons['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, $idAnnonceCons, $comparison);
    }

    /**
     * Filter the query on the prenom_nom_agent_createur column
     *
     * Example usage:
     * <code>
     * $query->filterByPrenomNomAgentCreateur('fooValue');   // WHERE prenom_nom_agent_createur = 'fooValue'
     * $query->filterByPrenomNomAgentCreateur('%fooValue%'); // WHERE prenom_nom_agent_createur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prenomNomAgentCreateur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByPrenomNomAgentCreateur($prenomNomAgentCreateur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prenomNomAgentCreateur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prenomNomAgentCreateur)) {
                $prenomNomAgentCreateur = str_replace('*', '%', $prenomNomAgentCreateur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::PRENOM_NOM_AGENT_CREATEUR, $prenomNomAgentCreateur, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the statut column
     *
     * Example usage:
     * <code>
     * $query->filterByStatut('fooValue');   // WHERE statut = 'fooValue'
     * $query->filterByStatut('%fooValue%'); // WHERE statut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statut The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByStatut($statut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statut)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $statut)) {
                $statut = str_replace('*', '%', $statut);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::STATUT, $statut, $comparison);
    }

    /**
     * Filter the query on the date_statut column
     *
     * Example usage:
     * <code>
     * $query->filterByDateStatut('2011-03-14'); // WHERE date_statut = '2011-03-14'
     * $query->filterByDateStatut('now'); // WHERE date_statut = '2011-03-14'
     * $query->filterByDateStatut(array('max' => 'yesterday')); // WHERE date_statut > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateStatut The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByDateStatut($dateStatut = null, $comparison = null)
    {
        if (is_array($dateStatut)) {
            $useMinMax = false;
            if (isset($dateStatut['min'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_STATUT, $dateStatut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateStatut['max'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_STATUT, $dateStatut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_STATUT, $dateStatut, $comparison);
    }

    /**
     * Filter the query on the numero_avis column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroAvis('fooValue');   // WHERE numero_avis = 'fooValue'
     * $query->filterByNumeroAvis('%fooValue%'); // WHERE numero_avis LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numeroAvis The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByNumeroAvis($numeroAvis = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numeroAvis)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numeroAvis)) {
                $numeroAvis = str_replace('*', '%', $numeroAvis);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS, $numeroAvis, $comparison);
    }

    /**
     * Filter the query on the message_statut column
     *
     * Example usage:
     * <code>
     * $query->filterByMessageStatut('fooValue');   // WHERE message_statut = 'fooValue'
     * $query->filterByMessageStatut('%fooValue%'); // WHERE message_statut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $messageStatut The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByMessageStatut($messageStatut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($messageStatut)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $messageStatut)) {
                $messageStatut = str_replace('*', '%', $messageStatut);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::MESSAGE_STATUT, $messageStatut, $comparison);
    }

    /**
     * Filter the query on the lien_publication column
     *
     * Example usage:
     * <code>
     * $query->filterByLienPublication('fooValue');   // WHERE lien_publication = 'fooValue'
     * $query->filterByLienPublication('%fooValue%'); // WHERE lien_publication LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lienPublication The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByLienPublication($lienPublication = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lienPublication)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $lienPublication)) {
                $lienPublication = str_replace('*', '%', $lienPublication);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::LIEN_PUBLICATION, $lienPublication, $comparison);
    }

    /**
     * Filter the query on the date_envoi_support column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnvoiSupport('2011-03-14'); // WHERE date_envoi_support = '2011-03-14'
     * $query->filterByDateEnvoiSupport('now'); // WHERE date_envoi_support = '2011-03-14'
     * $query->filterByDateEnvoiSupport(array('max' => 'yesterday')); // WHERE date_envoi_support > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnvoiSupport The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByDateEnvoiSupport($dateEnvoiSupport = null, $comparison = null)
    {
        if (is_array($dateEnvoiSupport)) {
            $useMinMax = false;
            if (isset($dateEnvoiSupport['min'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_ENVOI_SUPPORT, $dateEnvoiSupport['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnvoiSupport['max'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_ENVOI_SUPPORT, $dateEnvoiSupport['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_ENVOI_SUPPORT, $dateEnvoiSupport, $comparison);
    }

    /**
     * Filter the query on the date_publication_support column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePublicationSupport('2011-03-14'); // WHERE date_publication_support = '2011-03-14'
     * $query->filterByDatePublicationSupport('now'); // WHERE date_publication_support = '2011-03-14'
     * $query->filterByDatePublicationSupport(array('max' => 'yesterday')); // WHERE date_publication_support > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePublicationSupport The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByDatePublicationSupport($datePublicationSupport = null, $comparison = null)
    {
        if (is_array($datePublicationSupport)) {
            $useMinMax = false;
            if (isset($datePublicationSupport['min'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_PUBLICATION_SUPPORT, $datePublicationSupport['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePublicationSupport['max'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_PUBLICATION_SUPPORT, $datePublicationSupport['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DATE_PUBLICATION_SUPPORT, $datePublicationSupport, $comparison);
    }

    /**
     * Filter the query on the numero_avis_parent column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroAvisParent('fooValue');   // WHERE numero_avis_parent = 'fooValue'
     * $query->filterByNumeroAvisParent('%fooValue%'); // WHERE numero_avis_parent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numeroAvisParent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByNumeroAvisParent($numeroAvisParent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numeroAvisParent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numeroAvisParent)) {
                $numeroAvisParent = str_replace('*', '%', $numeroAvisParent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::NUMERO_AVIS_PARENT, $numeroAvisParent, $comparison);
    }

    /**
     * Filter the query on the id_offre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdOffre(1234); // WHERE id_offre = 1234
     * $query->filterByIdOffre(array(12, 34)); // WHERE id_offre IN (12, 34)
     * $query->filterByIdOffre(array('min' => 12)); // WHERE id_offre >= 12
     * $query->filterByIdOffre(array('max' => 12)); // WHERE id_offre <= 12
     * </code>
     *
     * @param     mixed $idOffre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByIdOffre($idOffre = null, $comparison = null)
    {
        if (is_array($idOffre)) {
            $useMinMax = false;
            if (isset($idOffre['min'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_OFFRE, $idOffre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idOffre['max'])) {
                $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_OFFRE, $idOffre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_OFFRE, $idOffre, $comparison);
    }

    /**
     * Filter the query on the message_acheteur column
     *
     * Example usage:
     * <code>
     * $query->filterByMessageAcheteur('fooValue');   // WHERE message_acheteur = 'fooValue'
     * $query->filterByMessageAcheteur('%fooValue%'); // WHERE message_acheteur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $messageAcheteur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByMessageAcheteur($messageAcheteur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($messageAcheteur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $messageAcheteur)) {
                $messageAcheteur = str_replace('*', '%', $messageAcheteur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::MESSAGE_ACHETEUR, $messageAcheteur, $comparison);
    }

    /**
     * Filter the query on the departements_parution_annonce column
     *
     * Example usage:
     * <code>
     * $query->filterByDepartementsParutionAnnonce('fooValue');   // WHERE departements_parution_annonce = 'fooValue'
     * $query->filterByDepartementsParutionAnnonce('%fooValue%'); // WHERE departements_parution_annonce LIKE '%fooValue%'
     * </code>
     *
     * @param     string $departementsParutionAnnonce The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByDepartementsParutionAnnonce($departementsParutionAnnonce = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($departementsParutionAnnonce)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $departementsParutionAnnonce)) {
                $departementsParutionAnnonce = str_replace('*', '%', $departementsParutionAnnonce);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::DEPARTEMENTS_PARUTION_ANNONCE, $departementsParutionAnnonce, $comparison);
    }

    /**
     * Filter the query by a related CommonTAnnonceConsultation object
     *
     * @param   CommonTAnnonceConsultation|PropelObjectCollection $commonTAnnonceConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTAnnonceConsultation($commonTAnnonceConsultation, $comparison = null)
    {
        if ($commonTAnnonceConsultation instanceof CommonTAnnonceConsultation) {
            return $this
                ->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, $commonTAnnonceConsultation->getId(), $comparison);
        } elseif ($commonTAnnonceConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, $commonTAnnonceConsultation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTAnnonceConsultation() only accepts arguments of type CommonTAnnonceConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTAnnonceConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function joinCommonTAnnonceConsultation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTAnnonceConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTAnnonceConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonTAnnonceConsultation relation CommonTAnnonceConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTAnnonceConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonTAnnonceConsultationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTAnnonceConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTAnnonceConsultation', '\Application\Propel\Mpe\CommonTAnnonceConsultationQuery');
    }

    /**
     * Filter the query by a related CommonTSupportPublication object
     *
     * @param   CommonTSupportPublication|PropelObjectCollection $commonTSupportPublication The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTSupportPublication($commonTSupportPublication, $comparison = null)
    {
        if ($commonTSupportPublication instanceof CommonTSupportPublication) {
            return $this
                ->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, $commonTSupportPublication->getId(), $comparison);
        } elseif ($commonTSupportPublication instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID_SUPPORT, $commonTSupportPublication->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTSupportPublication() only accepts arguments of type CommonTSupportPublication or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTSupportPublication relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function joinCommonTSupportPublication($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTSupportPublication');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTSupportPublication');
        }

        return $this;
    }

    /**
     * Use the CommonTSupportPublication relation CommonTSupportPublication object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTSupportPublicationQuery A secondary query class using the current class as primary query
     */
    public function useCommonTSupportPublicationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTSupportPublication($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTSupportPublication', '\Application\Propel\Mpe\CommonTSupportPublicationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTSupportAnnonceConsultation $commonTSupportAnnonceConsultation Object to remove from the list of results
     *
     * @return CommonTSupportAnnonceConsultationQuery The current query, for fluid interface
     */
    public function prune($commonTSupportAnnonceConsultation = null)
    {
        if ($commonTSupportAnnonceConsultation) {
            $this->addUsingAlias(CommonTSupportAnnonceConsultationPeer::ID, $commonTSupportAnnonceConsultation->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
