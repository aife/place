<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTChorusFicheModificativePeer;
use Application\Propel\Mpe\CommonTChorusFicheModificativePj;
use Application\Propel\Mpe\CommonTChorusFicheModificativeQuery;

/**
 * Base class that represents a query for the 't_chorus_fiche_modificative' table.
 *
 *
 *
 * @method CommonTChorusFicheModificativeQuery orderByIdFicheModificative($order = Criteria::ASC) Order by the id_fiche_modificative column
 * @method CommonTChorusFicheModificativeQuery orderByIdEchange($order = Criteria::ASC) Order by the id_echange column
 * @method CommonTChorusFicheModificativeQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTChorusFicheModificativeQuery orderByTypeModification($order = Criteria::ASC) Order by the type_modification column
 * @method CommonTChorusFicheModificativeQuery orderByDatePrevueNotification($order = Criteria::ASC) Order by the date_prevue_notification column
 * @method CommonTChorusFicheModificativeQuery orderByDateFinMarche($order = Criteria::ASC) Order by the date_fin_marche column
 * @method CommonTChorusFicheModificativeQuery orderByDateFinMarcheModifie($order = Criteria::ASC) Order by the date_fin_marche_modifie column
 * @method CommonTChorusFicheModificativeQuery orderByMontantMarche($order = Criteria::ASC) Order by the montant_marche column
 * @method CommonTChorusFicheModificativeQuery orderByMontantActe($order = Criteria::ASC) Order by the montant_acte column
 * @method CommonTChorusFicheModificativeQuery orderByTauxTva($order = Criteria::ASC) Order by the taux_tva column
 * @method CommonTChorusFicheModificativeQuery orderByNombreFournisseurCotraitant($order = Criteria::ASC) Order by the nombre_fournisseur_cotraitant column
 * @method CommonTChorusFicheModificativeQuery orderByLocalitesFournisseurs($order = Criteria::ASC) Order by the localites_fournisseurs column
 * @method CommonTChorusFicheModificativeQuery orderBySirenFournisseur($order = Criteria::ASC) Order by the siren_fournisseur column
 * @method CommonTChorusFicheModificativeQuery orderBySiretFournisseur($order = Criteria::ASC) Order by the siret_fournisseur column
 * @method CommonTChorusFicheModificativeQuery orderByNomFournisseur($order = Criteria::ASC) Order by the nom_fournisseur column
 * @method CommonTChorusFicheModificativeQuery orderByTypeFournisseur($order = Criteria::ASC) Order by the type_fournisseur column
 * @method CommonTChorusFicheModificativeQuery orderByVisaAccf($order = Criteria::ASC) Order by the visa_accf column
 * @method CommonTChorusFicheModificativeQuery orderByVisaPrefet($order = Criteria::ASC) Order by the visa_prefet column
 * @method CommonTChorusFicheModificativeQuery orderByRemarque($order = Criteria::ASC) Order by the remarque column
 * @method CommonTChorusFicheModificativeQuery orderByIdBlobPieceJustificatives($order = Criteria::ASC) Order by the id_blob_piece_justificatives column
 * @method CommonTChorusFicheModificativeQuery orderByIdBlobFicheModificative($order = Criteria::ASC) Order by the id_blob_fiche_modificative column
 * @method CommonTChorusFicheModificativeQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTChorusFicheModificativeQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 *
 * @method CommonTChorusFicheModificativeQuery groupByIdFicheModificative() Group by the id_fiche_modificative column
 * @method CommonTChorusFicheModificativeQuery groupByIdEchange() Group by the id_echange column
 * @method CommonTChorusFicheModificativeQuery groupByOrganisme() Group by the organisme column
 * @method CommonTChorusFicheModificativeQuery groupByTypeModification() Group by the type_modification column
 * @method CommonTChorusFicheModificativeQuery groupByDatePrevueNotification() Group by the date_prevue_notification column
 * @method CommonTChorusFicheModificativeQuery groupByDateFinMarche() Group by the date_fin_marche column
 * @method CommonTChorusFicheModificativeQuery groupByDateFinMarcheModifie() Group by the date_fin_marche_modifie column
 * @method CommonTChorusFicheModificativeQuery groupByMontantMarche() Group by the montant_marche column
 * @method CommonTChorusFicheModificativeQuery groupByMontantActe() Group by the montant_acte column
 * @method CommonTChorusFicheModificativeQuery groupByTauxTva() Group by the taux_tva column
 * @method CommonTChorusFicheModificativeQuery groupByNombreFournisseurCotraitant() Group by the nombre_fournisseur_cotraitant column
 * @method CommonTChorusFicheModificativeQuery groupByLocalitesFournisseurs() Group by the localites_fournisseurs column
 * @method CommonTChorusFicheModificativeQuery groupBySirenFournisseur() Group by the siren_fournisseur column
 * @method CommonTChorusFicheModificativeQuery groupBySiretFournisseur() Group by the siret_fournisseur column
 * @method CommonTChorusFicheModificativeQuery groupByNomFournisseur() Group by the nom_fournisseur column
 * @method CommonTChorusFicheModificativeQuery groupByTypeFournisseur() Group by the type_fournisseur column
 * @method CommonTChorusFicheModificativeQuery groupByVisaAccf() Group by the visa_accf column
 * @method CommonTChorusFicheModificativeQuery groupByVisaPrefet() Group by the visa_prefet column
 * @method CommonTChorusFicheModificativeQuery groupByRemarque() Group by the remarque column
 * @method CommonTChorusFicheModificativeQuery groupByIdBlobPieceJustificatives() Group by the id_blob_piece_justificatives column
 * @method CommonTChorusFicheModificativeQuery groupByIdBlobFicheModificative() Group by the id_blob_fiche_modificative column
 * @method CommonTChorusFicheModificativeQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTChorusFicheModificativeQuery groupByDateModification() Group by the date_modification column
 *
 * @method CommonTChorusFicheModificativeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTChorusFicheModificativeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTChorusFicheModificativeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTChorusFicheModificativeQuery leftJoinCommonChorusEchange($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonChorusEchange relation
 * @method CommonTChorusFicheModificativeQuery rightJoinCommonChorusEchange($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonChorusEchange relation
 * @method CommonTChorusFicheModificativeQuery innerJoinCommonChorusEchange($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonChorusEchange relation
 *
 * @method CommonTChorusFicheModificativeQuery leftJoinCommonTChorusFicheModificativePj($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTChorusFicheModificativePj relation
 * @method CommonTChorusFicheModificativeQuery rightJoinCommonTChorusFicheModificativePj($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTChorusFicheModificativePj relation
 * @method CommonTChorusFicheModificativeQuery innerJoinCommonTChorusFicheModificativePj($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTChorusFicheModificativePj relation
 *
 * @method CommonTChorusFicheModificative findOne(PropelPDO $con = null) Return the first CommonTChorusFicheModificative matching the query
 * @method CommonTChorusFicheModificative findOneOrCreate(PropelPDO $con = null) Return the first CommonTChorusFicheModificative matching the query, or a new CommonTChorusFicheModificative object populated from the query conditions when no match is found
 *
 * @method CommonTChorusFicheModificative findOneByIdEchange(int $id_echange) Return the first CommonTChorusFicheModificative filtered by the id_echange column
 * @method CommonTChorusFicheModificative findOneByOrganisme(string $organisme) Return the first CommonTChorusFicheModificative filtered by the organisme column
 * @method CommonTChorusFicheModificative findOneByTypeModification(int $type_modification) Return the first CommonTChorusFicheModificative filtered by the type_modification column
 * @method CommonTChorusFicheModificative findOneByDatePrevueNotification(string $date_prevue_notification) Return the first CommonTChorusFicheModificative filtered by the date_prevue_notification column
 * @method CommonTChorusFicheModificative findOneByDateFinMarche(string $date_fin_marche) Return the first CommonTChorusFicheModificative filtered by the date_fin_marche column
 * @method CommonTChorusFicheModificative findOneByDateFinMarcheModifie(string $date_fin_marche_modifie) Return the first CommonTChorusFicheModificative filtered by the date_fin_marche_modifie column
 * @method CommonTChorusFicheModificative findOneByMontantMarche(double $montant_marche) Return the first CommonTChorusFicheModificative filtered by the montant_marche column
 * @method CommonTChorusFicheModificative findOneByMontantActe(double $montant_acte) Return the first CommonTChorusFicheModificative filtered by the montant_acte column
 * @method CommonTChorusFicheModificative findOneByTauxTva(string $taux_tva) Return the first CommonTChorusFicheModificative filtered by the taux_tva column
 * @method CommonTChorusFicheModificative findOneByNombreFournisseurCotraitant(string $nombre_fournisseur_cotraitant) Return the first CommonTChorusFicheModificative filtered by the nombre_fournisseur_cotraitant column
 * @method CommonTChorusFicheModificative findOneByLocalitesFournisseurs(string $localites_fournisseurs) Return the first CommonTChorusFicheModificative filtered by the localites_fournisseurs column
 * @method CommonTChorusFicheModificative findOneBySirenFournisseur(string $siren_fournisseur) Return the first CommonTChorusFicheModificative filtered by the siren_fournisseur column
 * @method CommonTChorusFicheModificative findOneBySiretFournisseur(string $siret_fournisseur) Return the first CommonTChorusFicheModificative filtered by the siret_fournisseur column
 * @method CommonTChorusFicheModificative findOneByNomFournisseur(string $nom_fournisseur) Return the first CommonTChorusFicheModificative filtered by the nom_fournisseur column
 * @method CommonTChorusFicheModificative findOneByTypeFournisseur(string $type_fournisseur) Return the first CommonTChorusFicheModificative filtered by the type_fournisseur column
 * @method CommonTChorusFicheModificative findOneByVisaAccf(string $visa_accf) Return the first CommonTChorusFicheModificative filtered by the visa_accf column
 * @method CommonTChorusFicheModificative findOneByVisaPrefet(string $visa_prefet) Return the first CommonTChorusFicheModificative filtered by the visa_prefet column
 * @method CommonTChorusFicheModificative findOneByRemarque(string $remarque) Return the first CommonTChorusFicheModificative filtered by the remarque column
 * @method CommonTChorusFicheModificative findOneByIdBlobPieceJustificatives(string $id_blob_piece_justificatives) Return the first CommonTChorusFicheModificative filtered by the id_blob_piece_justificatives column
 * @method CommonTChorusFicheModificative findOneByIdBlobFicheModificative(string $id_blob_fiche_modificative) Return the first CommonTChorusFicheModificative filtered by the id_blob_fiche_modificative column
 * @method CommonTChorusFicheModificative findOneByDateCreation(string $date_creation) Return the first CommonTChorusFicheModificative filtered by the date_creation column
 * @method CommonTChorusFicheModificative findOneByDateModification(string $date_modification) Return the first CommonTChorusFicheModificative filtered by the date_modification column
 *
 * @method array findByIdFicheModificative(int $id_fiche_modificative) Return CommonTChorusFicheModificative objects filtered by the id_fiche_modificative column
 * @method array findByIdEchange(int $id_echange) Return CommonTChorusFicheModificative objects filtered by the id_echange column
 * @method array findByOrganisme(string $organisme) Return CommonTChorusFicheModificative objects filtered by the organisme column
 * @method array findByTypeModification(int $type_modification) Return CommonTChorusFicheModificative objects filtered by the type_modification column
 * @method array findByDatePrevueNotification(string $date_prevue_notification) Return CommonTChorusFicheModificative objects filtered by the date_prevue_notification column
 * @method array findByDateFinMarche(string $date_fin_marche) Return CommonTChorusFicheModificative objects filtered by the date_fin_marche column
 * @method array findByDateFinMarcheModifie(string $date_fin_marche_modifie) Return CommonTChorusFicheModificative objects filtered by the date_fin_marche_modifie column
 * @method array findByMontantMarche(double $montant_marche) Return CommonTChorusFicheModificative objects filtered by the montant_marche column
 * @method array findByMontantActe(double $montant_acte) Return CommonTChorusFicheModificative objects filtered by the montant_acte column
 * @method array findByTauxTva(string $taux_tva) Return CommonTChorusFicheModificative objects filtered by the taux_tva column
 * @method array findByNombreFournisseurCotraitant(string $nombre_fournisseur_cotraitant) Return CommonTChorusFicheModificative objects filtered by the nombre_fournisseur_cotraitant column
 * @method array findByLocalitesFournisseurs(string $localites_fournisseurs) Return CommonTChorusFicheModificative objects filtered by the localites_fournisseurs column
 * @method array findBySirenFournisseur(string $siren_fournisseur) Return CommonTChorusFicheModificative objects filtered by the siren_fournisseur column
 * @method array findBySiretFournisseur(string $siret_fournisseur) Return CommonTChorusFicheModificative objects filtered by the siret_fournisseur column
 * @method array findByNomFournisseur(string $nom_fournisseur) Return CommonTChorusFicheModificative objects filtered by the nom_fournisseur column
 * @method array findByTypeFournisseur(string $type_fournisseur) Return CommonTChorusFicheModificative objects filtered by the type_fournisseur column
 * @method array findByVisaAccf(string $visa_accf) Return CommonTChorusFicheModificative objects filtered by the visa_accf column
 * @method array findByVisaPrefet(string $visa_prefet) Return CommonTChorusFicheModificative objects filtered by the visa_prefet column
 * @method array findByRemarque(string $remarque) Return CommonTChorusFicheModificative objects filtered by the remarque column
 * @method array findByIdBlobPieceJustificatives(string $id_blob_piece_justificatives) Return CommonTChorusFicheModificative objects filtered by the id_blob_piece_justificatives column
 * @method array findByIdBlobFicheModificative(string $id_blob_fiche_modificative) Return CommonTChorusFicheModificative objects filtered by the id_blob_fiche_modificative column
 * @method array findByDateCreation(string $date_creation) Return CommonTChorusFicheModificative objects filtered by the date_creation column
 * @method array findByDateModification(string $date_modification) Return CommonTChorusFicheModificative objects filtered by the date_modification column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTChorusFicheModificativeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTChorusFicheModificativeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTChorusFicheModificative', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTChorusFicheModificativeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTChorusFicheModificativeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTChorusFicheModificativeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTChorusFicheModificativeQuery) {
            return $criteria;
        }
        $query = new CommonTChorusFicheModificativeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTChorusFicheModificative|CommonTChorusFicheModificative[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTChorusFicheModificativePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusFicheModificativePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTChorusFicheModificative A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdFicheModificative($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTChorusFicheModificative A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_fiche_modificative`, `id_echange`, `organisme`, `type_modification`, `date_prevue_notification`, `date_fin_marche`, `date_fin_marche_modifie`, `montant_marche`, `montant_acte`, `taux_tva`, `nombre_fournisseur_cotraitant`, `localites_fournisseurs`, `siren_fournisseur`, `siret_fournisseur`, `nom_fournisseur`, `type_fournisseur`, `visa_accf`, `visa_prefet`, `remarque`, `id_blob_piece_justificatives`, `id_blob_fiche_modificative`, `date_creation`, `date_modification` FROM `t_chorus_fiche_modificative` WHERE `id_fiche_modificative` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTChorusFicheModificative();
            $obj->hydrate($row);
            CommonTChorusFicheModificativePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTChorusFicheModificative|CommonTChorusFicheModificative[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTChorusFicheModificative[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_fiche_modificative column
     *
     * Example usage:
     * <code>
     * $query->filterByIdFicheModificative(1234); // WHERE id_fiche_modificative = 1234
     * $query->filterByIdFicheModificative(array(12, 34)); // WHERE id_fiche_modificative IN (12, 34)
     * $query->filterByIdFicheModificative(array('min' => 12)); // WHERE id_fiche_modificative >= 12
     * $query->filterByIdFicheModificative(array('max' => 12)); // WHERE id_fiche_modificative <= 12
     * </code>
     *
     * @param     mixed $idFicheModificative The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByIdFicheModificative($idFicheModificative = null, $comparison = null)
    {
        if (is_array($idFicheModificative)) {
            $useMinMax = false;
            if (isset($idFicheModificative['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $idFicheModificative['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idFicheModificative['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $idFicheModificative['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $idFicheModificative, $comparison);
    }

    /**
     * Filter the query on the id_echange column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEchange(1234); // WHERE id_echange = 1234
     * $query->filterByIdEchange(array(12, 34)); // WHERE id_echange IN (12, 34)
     * $query->filterByIdEchange(array('min' => 12)); // WHERE id_echange >= 12
     * $query->filterByIdEchange(array('max' => 12)); // WHERE id_echange <= 12
     * </code>
     *
     * @see       filterByCommonChorusEchange()
     *
     * @param     mixed $idEchange The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByIdEchange($idEchange = null, $comparison = null)
    {
        if (is_array($idEchange)) {
            $useMinMax = false;
            if (isset($idEchange['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::ID_ECHANGE, $idEchange['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEchange['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::ID_ECHANGE, $idEchange['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::ID_ECHANGE, $idEchange, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the type_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeModification(1234); // WHERE type_modification = 1234
     * $query->filterByTypeModification(array(12, 34)); // WHERE type_modification IN (12, 34)
     * $query->filterByTypeModification(array('min' => 12)); // WHERE type_modification >= 12
     * $query->filterByTypeModification(array('max' => 12)); // WHERE type_modification <= 12
     * </code>
     *
     * @param     mixed $typeModification The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByTypeModification($typeModification = null, $comparison = null)
    {
        if (is_array($typeModification)) {
            $useMinMax = false;
            if (isset($typeModification['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::TYPE_MODIFICATION, $typeModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeModification['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::TYPE_MODIFICATION, $typeModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::TYPE_MODIFICATION, $typeModification, $comparison);
    }

    /**
     * Filter the query on the date_prevue_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePrevueNotification('2011-03-14'); // WHERE date_prevue_notification = '2011-03-14'
     * $query->filterByDatePrevueNotification('now'); // WHERE date_prevue_notification = '2011-03-14'
     * $query->filterByDatePrevueNotification(array('max' => 'yesterday')); // WHERE date_prevue_notification > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePrevueNotification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByDatePrevueNotification($datePrevueNotification = null, $comparison = null)
    {
        if (is_array($datePrevueNotification)) {
            $useMinMax = false;
            if (isset($datePrevueNotification['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_PREVUE_NOTIFICATION, $datePrevueNotification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePrevueNotification['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_PREVUE_NOTIFICATION, $datePrevueNotification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_PREVUE_NOTIFICATION, $datePrevueNotification, $comparison);
    }

    /**
     * Filter the query on the date_fin_marche column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFinMarche('2011-03-14'); // WHERE date_fin_marche = '2011-03-14'
     * $query->filterByDateFinMarche('now'); // WHERE date_fin_marche = '2011-03-14'
     * $query->filterByDateFinMarche(array('max' => 'yesterday')); // WHERE date_fin_marche > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateFinMarche The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByDateFinMarche($dateFinMarche = null, $comparison = null)
    {
        if (is_array($dateFinMarche)) {
            $useMinMax = false;
            if (isset($dateFinMarche['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE, $dateFinMarche['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateFinMarche['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE, $dateFinMarche['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE, $dateFinMarche, $comparison);
    }

    /**
     * Filter the query on the date_fin_marche_modifie column
     *
     * Example usage:
     * <code>
     * $query->filterByDateFinMarcheModifie('2011-03-14'); // WHERE date_fin_marche_modifie = '2011-03-14'
     * $query->filterByDateFinMarcheModifie('now'); // WHERE date_fin_marche_modifie = '2011-03-14'
     * $query->filterByDateFinMarcheModifie(array('max' => 'yesterday')); // WHERE date_fin_marche_modifie > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateFinMarcheModifie The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByDateFinMarcheModifie($dateFinMarcheModifie = null, $comparison = null)
    {
        if (is_array($dateFinMarcheModifie)) {
            $useMinMax = false;
            if (isset($dateFinMarcheModifie['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE_MODIFIE, $dateFinMarcheModifie['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateFinMarcheModifie['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE_MODIFIE, $dateFinMarcheModifie['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_FIN_MARCHE_MODIFIE, $dateFinMarcheModifie, $comparison);
    }

    /**
     * Filter the query on the montant_marche column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantMarche(1234); // WHERE montant_marche = 1234
     * $query->filterByMontantMarche(array(12, 34)); // WHERE montant_marche IN (12, 34)
     * $query->filterByMontantMarche(array('min' => 12)); // WHERE montant_marche >= 12
     * $query->filterByMontantMarche(array('max' => 12)); // WHERE montant_marche <= 12
     * </code>
     *
     * @param     mixed $montantMarche The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByMontantMarche($montantMarche = null, $comparison = null)
    {
        if (is_array($montantMarche)) {
            $useMinMax = false;
            if (isset($montantMarche['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::MONTANT_MARCHE, $montantMarche['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantMarche['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::MONTANT_MARCHE, $montantMarche['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::MONTANT_MARCHE, $montantMarche, $comparison);
    }

    /**
     * Filter the query on the montant_acte column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantActe(1234); // WHERE montant_acte = 1234
     * $query->filterByMontantActe(array(12, 34)); // WHERE montant_acte IN (12, 34)
     * $query->filterByMontantActe(array('min' => 12)); // WHERE montant_acte >= 12
     * $query->filterByMontantActe(array('max' => 12)); // WHERE montant_acte <= 12
     * </code>
     *
     * @param     mixed $montantActe The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByMontantActe($montantActe = null, $comparison = null)
    {
        if (is_array($montantActe)) {
            $useMinMax = false;
            if (isset($montantActe['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::MONTANT_ACTE, $montantActe['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantActe['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::MONTANT_ACTE, $montantActe['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::MONTANT_ACTE, $montantActe, $comparison);
    }

    /**
     * Filter the query on the taux_tva column
     *
     * Example usage:
     * <code>
     * $query->filterByTauxTva('fooValue');   // WHERE taux_tva = 'fooValue'
     * $query->filterByTauxTva('%fooValue%'); // WHERE taux_tva LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tauxTva The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByTauxTva($tauxTva = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tauxTva)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tauxTva)) {
                $tauxTva = str_replace('*', '%', $tauxTva);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::TAUX_TVA, $tauxTva, $comparison);
    }

    /**
     * Filter the query on the nombre_fournisseur_cotraitant column
     *
     * Example usage:
     * <code>
     * $query->filterByNombreFournisseurCotraitant('fooValue');   // WHERE nombre_fournisseur_cotraitant = 'fooValue'
     * $query->filterByNombreFournisseurCotraitant('%fooValue%'); // WHERE nombre_fournisseur_cotraitant LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombreFournisseurCotraitant The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByNombreFournisseurCotraitant($nombreFournisseurCotraitant = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombreFournisseurCotraitant)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombreFournisseurCotraitant)) {
                $nombreFournisseurCotraitant = str_replace('*', '%', $nombreFournisseurCotraitant);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::NOMBRE_FOURNISSEUR_COTRAITANT, $nombreFournisseurCotraitant, $comparison);
    }

    /**
     * Filter the query on the localites_fournisseurs column
     *
     * Example usage:
     * <code>
     * $query->filterByLocalitesFournisseurs('fooValue');   // WHERE localites_fournisseurs = 'fooValue'
     * $query->filterByLocalitesFournisseurs('%fooValue%'); // WHERE localites_fournisseurs LIKE '%fooValue%'
     * </code>
     *
     * @param     string $localitesFournisseurs The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByLocalitesFournisseurs($localitesFournisseurs = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($localitesFournisseurs)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $localitesFournisseurs)) {
                $localitesFournisseurs = str_replace('*', '%', $localitesFournisseurs);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::LOCALITES_FOURNISSEURS, $localitesFournisseurs, $comparison);
    }

    /**
     * Filter the query on the siren_fournisseur column
     *
     * Example usage:
     * <code>
     * $query->filterBySirenFournisseur('fooValue');   // WHERE siren_fournisseur = 'fooValue'
     * $query->filterBySirenFournisseur('%fooValue%'); // WHERE siren_fournisseur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sirenFournisseur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterBySirenFournisseur($sirenFournisseur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sirenFournisseur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sirenFournisseur)) {
                $sirenFournisseur = str_replace('*', '%', $sirenFournisseur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::SIREN_FOURNISSEUR, $sirenFournisseur, $comparison);
    }

    /**
     * Filter the query on the siret_fournisseur column
     *
     * Example usage:
     * <code>
     * $query->filterBySiretFournisseur('fooValue');   // WHERE siret_fournisseur = 'fooValue'
     * $query->filterBySiretFournisseur('%fooValue%'); // WHERE siret_fournisseur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siretFournisseur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterBySiretFournisseur($siretFournisseur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siretFournisseur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $siretFournisseur)) {
                $siretFournisseur = str_replace('*', '%', $siretFournisseur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::SIRET_FOURNISSEUR, $siretFournisseur, $comparison);
    }

    /**
     * Filter the query on the nom_fournisseur column
     *
     * Example usage:
     * <code>
     * $query->filterByNomFournisseur('fooValue');   // WHERE nom_fournisseur = 'fooValue'
     * $query->filterByNomFournisseur('%fooValue%'); // WHERE nom_fournisseur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomFournisseur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByNomFournisseur($nomFournisseur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomFournisseur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomFournisseur)) {
                $nomFournisseur = str_replace('*', '%', $nomFournisseur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::NOM_FOURNISSEUR, $nomFournisseur, $comparison);
    }

    /**
     * Filter the query on the type_fournisseur column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeFournisseur('fooValue');   // WHERE type_fournisseur = 'fooValue'
     * $query->filterByTypeFournisseur('%fooValue%'); // WHERE type_fournisseur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeFournisseur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByTypeFournisseur($typeFournisseur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeFournisseur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeFournisseur)) {
                $typeFournisseur = str_replace('*', '%', $typeFournisseur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::TYPE_FOURNISSEUR, $typeFournisseur, $comparison);
    }

    /**
     * Filter the query on the visa_accf column
     *
     * Example usage:
     * <code>
     * $query->filterByVisaAccf('fooValue');   // WHERE visa_accf = 'fooValue'
     * $query->filterByVisaAccf('%fooValue%'); // WHERE visa_accf LIKE '%fooValue%'
     * </code>
     *
     * @param     string $visaAccf The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByVisaAccf($visaAccf = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($visaAccf)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $visaAccf)) {
                $visaAccf = str_replace('*', '%', $visaAccf);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::VISA_ACCF, $visaAccf, $comparison);
    }

    /**
     * Filter the query on the visa_prefet column
     *
     * Example usage:
     * <code>
     * $query->filterByVisaPrefet('fooValue');   // WHERE visa_prefet = 'fooValue'
     * $query->filterByVisaPrefet('%fooValue%'); // WHERE visa_prefet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $visaPrefet The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByVisaPrefet($visaPrefet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($visaPrefet)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $visaPrefet)) {
                $visaPrefet = str_replace('*', '%', $visaPrefet);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::VISA_PREFET, $visaPrefet, $comparison);
    }

    /**
     * Filter the query on the remarque column
     *
     * Example usage:
     * <code>
     * $query->filterByRemarque('fooValue');   // WHERE remarque = 'fooValue'
     * $query->filterByRemarque('%fooValue%'); // WHERE remarque LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remarque The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByRemarque($remarque = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remarque)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $remarque)) {
                $remarque = str_replace('*', '%', $remarque);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::REMARQUE, $remarque, $comparison);
    }

    /**
     * Filter the query on the id_blob_piece_justificatives column
     *
     * Example usage:
     * <code>
     * $query->filterByIdBlobPieceJustificatives('fooValue');   // WHERE id_blob_piece_justificatives = 'fooValue'
     * $query->filterByIdBlobPieceJustificatives('%fooValue%'); // WHERE id_blob_piece_justificatives LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idBlobPieceJustificatives The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByIdBlobPieceJustificatives($idBlobPieceJustificatives = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idBlobPieceJustificatives)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idBlobPieceJustificatives)) {
                $idBlobPieceJustificatives = str_replace('*', '%', $idBlobPieceJustificatives);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::ID_BLOB_PIECE_JUSTIFICATIVES, $idBlobPieceJustificatives, $comparison);
    }

    /**
     * Filter the query on the id_blob_fiche_modificative column
     *
     * Example usage:
     * <code>
     * $query->filterByIdBlobFicheModificative('fooValue');   // WHERE id_blob_fiche_modificative = 'fooValue'
     * $query->filterByIdBlobFicheModificative('%fooValue%'); // WHERE id_blob_fiche_modificative LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idBlobFicheModificative The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByIdBlobFicheModificative($idBlobFicheModificative = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idBlobFicheModificative)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idBlobFicheModificative)) {
                $idBlobFicheModificative = str_replace('*', '%', $idBlobFicheModificative);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::ID_BLOB_FICHE_MODIFICATIVE, $idBlobFicheModificative, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('2011-03-14'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification('now'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification(array('max' => 'yesterday')); // WHERE date_modification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (is_array($dateModification)) {
            $useMinMax = false;
            if (isset($dateModification['min'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_MODIFICATION, $dateModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModification['max'])) {
                $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_MODIFICATION, $dateModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusFicheModificativePeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query by a related CommonChorusEchange object
     *
     * @param   CommonChorusEchange $commonChorusEchange The related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTChorusFicheModificativeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonChorusEchange($commonChorusEchange, $comparison = null)
    {
        if ($commonChorusEchange instanceof CommonChorusEchange) {
            return $this
                ->addUsingAlias(CommonTChorusFicheModificativePeer::ID_ECHANGE, $commonChorusEchange->getId(), $comparison)
                ->addUsingAlias(CommonTChorusFicheModificativePeer::ORGANISME, $commonChorusEchange->getOrganisme(), $comparison);
        } else {
            throw new PropelException('filterByCommonChorusEchange() only accepts arguments of type CommonChorusEchange');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonChorusEchange relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function joinCommonChorusEchange($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonChorusEchange');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonChorusEchange');
        }

        return $this;
    }

    /**
     * Use the CommonChorusEchange relation CommonChorusEchange object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonChorusEchangeQuery A secondary query class using the current class as primary query
     */
    public function useCommonChorusEchangeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonChorusEchange($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonChorusEchange', '\Application\Propel\Mpe\CommonChorusEchangeQuery');
    }

    /**
     * Filter the query by a related CommonTChorusFicheModificativePj object
     *
     * @param   CommonTChorusFicheModificativePj|PropelObjectCollection $commonTChorusFicheModificativePj  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTChorusFicheModificativeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTChorusFicheModificativePj($commonTChorusFicheModificativePj, $comparison = null)
    {
        if ($commonTChorusFicheModificativePj instanceof CommonTChorusFicheModificativePj) {
            return $this
                ->addUsingAlias(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $commonTChorusFicheModificativePj->getIdFicheModificative(), $comparison);
        } elseif ($commonTChorusFicheModificativePj instanceof PropelObjectCollection) {
            return $this
                ->useCommonTChorusFicheModificativePjQuery()
                ->filterByPrimaryKeys($commonTChorusFicheModificativePj->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTChorusFicheModificativePj() only accepts arguments of type CommonTChorusFicheModificativePj or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTChorusFicheModificativePj relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function joinCommonTChorusFicheModificativePj($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTChorusFicheModificativePj');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTChorusFicheModificativePj');
        }

        return $this;
    }

    /**
     * Use the CommonTChorusFicheModificativePj relation CommonTChorusFicheModificativePj object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTChorusFicheModificativePjQuery A secondary query class using the current class as primary query
     */
    public function useCommonTChorusFicheModificativePjQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTChorusFicheModificativePj($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTChorusFicheModificativePj', '\Application\Propel\Mpe\CommonTChorusFicheModificativePjQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTChorusFicheModificative $commonTChorusFicheModificative Object to remove from the list of results
     *
     * @return CommonTChorusFicheModificativeQuery The current query, for fluid interface
     */
    public function prune($commonTChorusFicheModificative = null)
    {
        if ($commonTChorusFicheModificative) {
            $this->addUsingAlias(CommonTChorusFicheModificativePeer::ID_FICHE_MODIFICATIVE, $commonTChorusFicheModificative->getIdFicheModificative(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
