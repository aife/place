<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonBlobOrganismeFile;
use Application\Propel\Mpe\CommonBlobOrganismeFileQuery;
use Application\Propel\Mpe\CommonEchangeDoc;
use Application\Propel\Mpe\CommonEchangeDocBlob;
use Application\Propel\Mpe\CommonEchangeDocBlobPeer;
use Application\Propel\Mpe\CommonEchangeDocBlobQuery;
use Application\Propel\Mpe\CommonEchangeDocQuery;
use Application\Propel\Mpe\CommonEchangeDocTypePieceStandard;
use Application\Propel\Mpe\CommonEchangeDocTypePieceStandardQuery;

/**
 * Base class that represents a row from the 'echange_doc_blob' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangeDocBlob extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonEchangeDocBlobPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonEchangeDocBlobPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the echange_doc_id field.
     * @var        int
     */
    protected $echange_doc_id;

    /**
     * The value for the blob_organisme_id field.
     * @var        int
     */
    protected $blob_organisme_id;

    /**
     * The value for the chemin field.
     * @var        string
     */
    protected $chemin;

    /**
     * The value for the poids field.
     * @var        int
     */
    protected $poids;

    /**
     * The value for the checksum field.
     * @var        string
     */
    protected $checksum;

    /**
     * The value for the echange_type_piece_actes_id field.
     * @var        int
     */
    protected $echange_type_piece_actes_id;

    /**
     * The value for the echange_type_piece_standard_id field.
     * @var        int
     */
    protected $echange_type_piece_standard_id;

    /**
     * The value for the categorie_piece field.
     * @var        int
     */
    protected $categorie_piece;

    /**
     * The value for the doc_blob_principal_id field.
     * @var        int
     */
    protected $doc_blob_principal_id;

    /**
     * @var        CommonEchangeDoc
     */
    protected $aCommonEchangeDoc;

    /**
     * @var        CommonBlobOrganismeFile
     */
    protected $aCommonBlobOrganismeFile;

    /**
     * @var        CommonEchangeDocBlob
     */
    protected $aCommonEchangeDocBlobRelatedByDocBlobPrincipalId;

    /**
     * @var        CommonEchangeDocTypePieceStandard
     */
    protected $aCommonEchangeDocTypePieceStandard;

    /**
     * @var        PropelObjectCollection|CommonEchangeDocBlob[] Collection to store aggregation of CommonEchangeDocBlob objects.
     */
    protected $collCommonEchangeDocBlobsRelatedById;
    protected $collCommonEchangeDocBlobsRelatedByIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangeDocBlobsRelatedByIdScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [echange_doc_id] column value.
     *
     * @return int
     */
    public function getEchangeDocId()
    {

        return $this->echange_doc_id;
    }

    /**
     * Get the [blob_organisme_id] column value.
     *
     * @return int
     */
    public function getBlobOrganismeId()
    {

        return $this->blob_organisme_id;
    }

    /**
     * Get the [chemin] column value.
     *
     * @return string
     */
    public function getChemin()
    {

        return $this->chemin;
    }

    /**
     * Get the [poids] column value.
     *
     * @return int
     */
    public function getPoids()
    {

        return $this->poids;
    }

    /**
     * Get the [checksum] column value.
     *
     * @return string
     */
    public function getChecksum()
    {

        return $this->checksum;
    }

    /**
     * Get the [echange_type_piece_actes_id] column value.
     *
     * @return int
     */
    public function getEchangeTypePieceActesId()
    {

        return $this->echange_type_piece_actes_id;
    }

    /**
     * Get the [echange_type_piece_standard_id] column value.
     *
     * @return int
     */
    public function getEchangeTypePieceStandardId()
    {

        return $this->echange_type_piece_standard_id;
    }

    /**
     * Get the [categorie_piece] column value.
     *
     * @return int
     */
    public function getCategoriePiece()
    {

        return $this->categorie_piece;
    }

    /**
     * Get the [doc_blob_principal_id] column value.
     *
     * @return int
     */
    public function getDocBlobPrincipalId()
    {

        return $this->doc_blob_principal_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonEchangeDocBlobPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [echange_doc_id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function setEchangeDocId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->echange_doc_id !== $v) {
            $this->echange_doc_id = $v;
            $this->modifiedColumns[] = CommonEchangeDocBlobPeer::ECHANGE_DOC_ID;
        }

        if ($this->aCommonEchangeDoc !== null && $this->aCommonEchangeDoc->getId() !== $v) {
            $this->aCommonEchangeDoc = null;
        }


        return $this;
    } // setEchangeDocId()

    /**
     * Set the value of [blob_organisme_id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function setBlobOrganismeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->blob_organisme_id !== $v) {
            $this->blob_organisme_id = $v;
            $this->modifiedColumns[] = CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID;
        }

        if ($this->aCommonBlobOrganismeFile !== null && $this->aCommonBlobOrganismeFile->getId() !== $v) {
            $this->aCommonBlobOrganismeFile = null;
        }


        return $this;
    } // setBlobOrganismeId()

    /**
     * Set the value of [chemin] column.
     *
     * @param string $v new value
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function setChemin($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin !== $v) {
            $this->chemin = $v;
            $this->modifiedColumns[] = CommonEchangeDocBlobPeer::CHEMIN;
        }


        return $this;
    } // setChemin()

    /**
     * Set the value of [poids] column.
     *
     * @param int $v new value
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function setPoids($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->poids !== $v) {
            $this->poids = $v;
            $this->modifiedColumns[] = CommonEchangeDocBlobPeer::POIDS;
        }


        return $this;
    } // setPoids()

    /**
     * Set the value of [checksum] column.
     *
     * @param string $v new value
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function setChecksum($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->checksum !== $v) {
            $this->checksum = $v;
            $this->modifiedColumns[] = CommonEchangeDocBlobPeer::CHECKSUM;
        }


        return $this;
    } // setChecksum()

    /**
     * Set the value of [echange_type_piece_actes_id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function setEchangeTypePieceActesId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->echange_type_piece_actes_id !== $v) {
            $this->echange_type_piece_actes_id = $v;
            $this->modifiedColumns[] = CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_ACTES_ID;
        }


        return $this;
    } // setEchangeTypePieceActesId()

    /**
     * Set the value of [echange_type_piece_standard_id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function setEchangeTypePieceStandardId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->echange_type_piece_standard_id !== $v) {
            $this->echange_type_piece_standard_id = $v;
            $this->modifiedColumns[] = CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID;
        }

        if ($this->aCommonEchangeDocTypePieceStandard !== null && $this->aCommonEchangeDocTypePieceStandard->getId() !== $v) {
            $this->aCommonEchangeDocTypePieceStandard = null;
        }


        return $this;
    } // setEchangeTypePieceStandardId()

    /**
     * Set the value of [categorie_piece] column.
     *
     * @param int $v new value
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function setCategoriePiece($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->categorie_piece !== $v) {
            $this->categorie_piece = $v;
            $this->modifiedColumns[] = CommonEchangeDocBlobPeer::CATEGORIE_PIECE;
        }


        return $this;
    } // setCategoriePiece()

    /**
     * Set the value of [doc_blob_principal_id] column.
     *
     * @param int $v new value
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function setDocBlobPrincipalId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->doc_blob_principal_id !== $v) {
            $this->doc_blob_principal_id = $v;
            $this->modifiedColumns[] = CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID;
        }

        if ($this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId !== null && $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId->getId() !== $v) {
            $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId = null;
        }


        return $this;
    } // setDocBlobPrincipalId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->echange_doc_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->blob_organisme_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->chemin = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->poids = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->checksum = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->echange_type_piece_actes_id = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->echange_type_piece_standard_id = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->categorie_piece = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->doc_blob_principal_id = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 10; // 10 = CommonEchangeDocBlobPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonEchangeDocBlob object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonEchangeDoc !== null && $this->echange_doc_id !== $this->aCommonEchangeDoc->getId()) {
            $this->aCommonEchangeDoc = null;
        }
        if ($this->aCommonBlobOrganismeFile !== null && $this->blob_organisme_id !== $this->aCommonBlobOrganismeFile->getId()) {
            $this->aCommonBlobOrganismeFile = null;
        }
        if ($this->aCommonEchangeDocTypePieceStandard !== null && $this->echange_type_piece_standard_id !== $this->aCommonEchangeDocTypePieceStandard->getId()) {
            $this->aCommonEchangeDocTypePieceStandard = null;
        }
        if ($this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId !== null && $this->doc_blob_principal_id !== $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId->getId()) {
            $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonEchangeDocBlobPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonEchangeDoc = null;
            $this->aCommonBlobOrganismeFile = null;
            $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId = null;
            $this->aCommonEchangeDocTypePieceStandard = null;
            $this->collCommonEchangeDocBlobsRelatedById = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonEchangeDocBlobQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonEchangeDocBlobPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonEchangeDoc !== null) {
                if ($this->aCommonEchangeDoc->isModified() || $this->aCommonEchangeDoc->isNew()) {
                    $affectedRows += $this->aCommonEchangeDoc->save($con);
                }
                $this->setCommonEchangeDoc($this->aCommonEchangeDoc);
            }

            if ($this->aCommonBlobOrganismeFile !== null) {
                if ($this->aCommonBlobOrganismeFile->isModified() || $this->aCommonBlobOrganismeFile->isNew()) {
                    $affectedRows += $this->aCommonBlobOrganismeFile->save($con);
                }
                $this->setCommonBlobOrganismeFile($this->aCommonBlobOrganismeFile);
            }

            if ($this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId !== null) {
                if ($this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId->isModified() || $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId->isNew()) {
                    $affectedRows += $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId->save($con);
                }
                $this->setCommonEchangeDocBlobRelatedByDocBlobPrincipalId($this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId);
            }

            if ($this->aCommonEchangeDocTypePieceStandard !== null) {
                if ($this->aCommonEchangeDocTypePieceStandard->isModified() || $this->aCommonEchangeDocTypePieceStandard->isNew()) {
                    $affectedRows += $this->aCommonEchangeDocTypePieceStandard->save($con);
                }
                $this->setCommonEchangeDocTypePieceStandard($this->aCommonEchangeDocTypePieceStandard);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonEchangeDocBlobsRelatedByIdScheduledForDeletion !== null) {
                if (!$this->commonEchangeDocBlobsRelatedByIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangeDocBlobsRelatedByIdScheduledForDeletion as $commonEchangeDocBlobRelatedById) {
                        // need to save related object because we set the relation to null
                        $commonEchangeDocBlobRelatedById->save($con);
                    }
                    $this->commonEchangeDocBlobsRelatedByIdScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchangeDocBlobsRelatedById !== null) {
                foreach ($this->collCommonEchangeDocBlobsRelatedById as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonEchangeDocBlobPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonEchangeDocBlobPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID)) {
            $modifiedColumns[':p' . $index++]  = '`echange_doc_id`';
        }
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID)) {
            $modifiedColumns[':p' . $index++]  = '`blob_organisme_id`';
        }
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::CHEMIN)) {
            $modifiedColumns[':p' . $index++]  = '`chemin`';
        }
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::POIDS)) {
            $modifiedColumns[':p' . $index++]  = '`poids`';
        }
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::CHECKSUM)) {
            $modifiedColumns[':p' . $index++]  = '`checksum`';
        }
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_ACTES_ID)) {
            $modifiedColumns[':p' . $index++]  = '`echange_type_piece_actes_id`';
        }
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID)) {
            $modifiedColumns[':p' . $index++]  = '`echange_type_piece_standard_id`';
        }
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::CATEGORIE_PIECE)) {
            $modifiedColumns[':p' . $index++]  = '`categorie_piece`';
        }
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID)) {
            $modifiedColumns[':p' . $index++]  = '`doc_blob_principal_id`';
        }

        $sql = sprintf(
            'INSERT INTO `echange_doc_blob` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`echange_doc_id`':
                        $stmt->bindValue($identifier, $this->echange_doc_id, PDO::PARAM_INT);
                        break;
                    case '`blob_organisme_id`':
                        $stmt->bindValue($identifier, $this->blob_organisme_id, PDO::PARAM_INT);
                        break;
                    case '`chemin`':
                        $stmt->bindValue($identifier, $this->chemin, PDO::PARAM_STR);
                        break;
                    case '`poids`':
                        $stmt->bindValue($identifier, $this->poids, PDO::PARAM_INT);
                        break;
                    case '`checksum`':
                        $stmt->bindValue($identifier, $this->checksum, PDO::PARAM_STR);
                        break;
                    case '`echange_type_piece_actes_id`':
                        $stmt->bindValue($identifier, $this->echange_type_piece_actes_id, PDO::PARAM_INT);
                        break;
                    case '`echange_type_piece_standard_id`':
                        $stmt->bindValue($identifier, $this->echange_type_piece_standard_id, PDO::PARAM_INT);
                        break;
                    case '`categorie_piece`':
                        $stmt->bindValue($identifier, $this->categorie_piece, PDO::PARAM_INT);
                        break;
                    case '`doc_blob_principal_id`':
                        $stmt->bindValue($identifier, $this->doc_blob_principal_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonEchangeDoc !== null) {
                if (!$this->aCommonEchangeDoc->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonEchangeDoc->getValidationFailures());
                }
            }

            if ($this->aCommonBlobOrganismeFile !== null) {
                if (!$this->aCommonBlobOrganismeFile->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonBlobOrganismeFile->getValidationFailures());
                }
            }

            if ($this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId !== null) {
                if (!$this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId->getValidationFailures());
                }
            }

            if ($this->aCommonEchangeDocTypePieceStandard !== null) {
                if (!$this->aCommonEchangeDocTypePieceStandard->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonEchangeDocTypePieceStandard->getValidationFailures());
                }
            }


            if (($retval = CommonEchangeDocBlobPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonEchangeDocBlobsRelatedById !== null) {
                    foreach ($this->collCommonEchangeDocBlobsRelatedById as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonEchangeDocBlobPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getEchangeDocId();
                break;
            case 2:
                return $this->getBlobOrganismeId();
                break;
            case 3:
                return $this->getChemin();
                break;
            case 4:
                return $this->getPoids();
                break;
            case 5:
                return $this->getChecksum();
                break;
            case 6:
                return $this->getEchangeTypePieceActesId();
                break;
            case 7:
                return $this->getEchangeTypePieceStandardId();
                break;
            case 8:
                return $this->getCategoriePiece();
                break;
            case 9:
                return $this->getDocBlobPrincipalId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonEchangeDocBlob'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonEchangeDocBlob'][$this->getPrimaryKey()] = true;
        $keys = CommonEchangeDocBlobPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getEchangeDocId(),
            $keys[2] => $this->getBlobOrganismeId(),
            $keys[3] => $this->getChemin(),
            $keys[4] => $this->getPoids(),
            $keys[5] => $this->getChecksum(),
            $keys[6] => $this->getEchangeTypePieceActesId(),
            $keys[7] => $this->getEchangeTypePieceStandardId(),
            $keys[8] => $this->getCategoriePiece(),
            $keys[9] => $this->getDocBlobPrincipalId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonEchangeDoc) {
                $result['CommonEchangeDoc'] = $this->aCommonEchangeDoc->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonBlobOrganismeFile) {
                $result['CommonBlobOrganismeFile'] = $this->aCommonBlobOrganismeFile->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId) {
                $result['CommonEchangeDocBlobRelatedByDocBlobPrincipalId'] = $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonEchangeDocTypePieceStandard) {
                $result['CommonEchangeDocTypePieceStandard'] = $this->aCommonEchangeDocTypePieceStandard->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonEchangeDocBlobsRelatedById) {
                $result['CommonEchangeDocBlobsRelatedById'] = $this->collCommonEchangeDocBlobsRelatedById->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonEchangeDocBlobPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setEchangeDocId($value);
                break;
            case 2:
                $this->setBlobOrganismeId($value);
                break;
            case 3:
                $this->setChemin($value);
                break;
            case 4:
                $this->setPoids($value);
                break;
            case 5:
                $this->setChecksum($value);
                break;
            case 6:
                $this->setEchangeTypePieceActesId($value);
                break;
            case 7:
                $this->setEchangeTypePieceStandardId($value);
                break;
            case 8:
                $this->setCategoriePiece($value);
                break;
            case 9:
                $this->setDocBlobPrincipalId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonEchangeDocBlobPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setEchangeDocId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setBlobOrganismeId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setChemin($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPoids($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setChecksum($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setEchangeTypePieceActesId($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setEchangeTypePieceStandardId($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setCategoriePiece($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setDocBlobPrincipalId($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonEchangeDocBlobPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonEchangeDocBlobPeer::ID)) $criteria->add(CommonEchangeDocBlobPeer::ID, $this->id);
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID)) $criteria->add(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, $this->echange_doc_id);
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID)) $criteria->add(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, $this->blob_organisme_id);
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::CHEMIN)) $criteria->add(CommonEchangeDocBlobPeer::CHEMIN, $this->chemin);
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::POIDS)) $criteria->add(CommonEchangeDocBlobPeer::POIDS, $this->poids);
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::CHECKSUM)) $criteria->add(CommonEchangeDocBlobPeer::CHECKSUM, $this->checksum);
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_ACTES_ID)) $criteria->add(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_ACTES_ID, $this->echange_type_piece_actes_id);
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID)) $criteria->add(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, $this->echange_type_piece_standard_id);
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::CATEGORIE_PIECE)) $criteria->add(CommonEchangeDocBlobPeer::CATEGORIE_PIECE, $this->categorie_piece);
        if ($this->isColumnModified(CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID)) $criteria->add(CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID, $this->doc_blob_principal_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonEchangeDocBlobPeer::DATABASE_NAME);
        $criteria->add(CommonEchangeDocBlobPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonEchangeDocBlob (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setEchangeDocId($this->getEchangeDocId());
        $copyObj->setBlobOrganismeId($this->getBlobOrganismeId());
        $copyObj->setChemin($this->getChemin());
        $copyObj->setPoids($this->getPoids());
        $copyObj->setChecksum($this->getChecksum());
        $copyObj->setEchangeTypePieceActesId($this->getEchangeTypePieceActesId());
        $copyObj->setEchangeTypePieceStandardId($this->getEchangeTypePieceStandardId());
        $copyObj->setCategoriePiece($this->getCategoriePiece());
        $copyObj->setDocBlobPrincipalId($this->getDocBlobPrincipalId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonEchangeDocBlobsRelatedById() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchangeDocBlobRelatedById($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonEchangeDocBlob Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonEchangeDocBlobPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonEchangeDocBlobPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonEchangeDoc object.
     *
     * @param   CommonEchangeDoc $v
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonEchangeDoc(CommonEchangeDoc $v = null)
    {
        if ($v === null) {
            $this->setEchangeDocId(NULL);
        } else {
            $this->setEchangeDocId($v->getId());
        }

        $this->aCommonEchangeDoc = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonEchangeDoc object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonEchangeDocBlob($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonEchangeDoc object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonEchangeDoc The associated CommonEchangeDoc object.
     * @throws PropelException
     */
    public function getCommonEchangeDoc(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonEchangeDoc === null && ($this->echange_doc_id !== null) && $doQuery) {
            $this->aCommonEchangeDoc = CommonEchangeDocQuery::create()->findPk($this->echange_doc_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonEchangeDoc->addCommonEchangeDocBlobs($this);
             */
        }

        return $this->aCommonEchangeDoc;
    }

    /**
     * Declares an association between this object and a CommonBlobOrganismeFile object.
     *
     * @param   CommonBlobOrganismeFile $v
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonBlobOrganismeFile(CommonBlobOrganismeFile $v = null)
    {
        if ($v === null) {
            $this->setBlobOrganismeId(NULL);
        } else {
            $this->setBlobOrganismeId($v->getId());
        }

        $this->aCommonBlobOrganismeFile = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonBlobOrganismeFile object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonEchangeDocBlob($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonBlobOrganismeFile object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonBlobOrganismeFile The associated CommonBlobOrganismeFile object.
     * @throws PropelException
     */
    public function getCommonBlobOrganismeFile(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonBlobOrganismeFile === null && ($this->blob_organisme_id !== null) && $doQuery) {
            $this->aCommonBlobOrganismeFile = CommonBlobOrganismeFileQuery::create()->findPk($this->blob_organisme_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonBlobOrganismeFile->addCommonEchangeDocBlobs($this);
             */
        }

        return $this->aCommonBlobOrganismeFile;
    }

    /**
     * Declares an association between this object and a CommonEchangeDocBlob object.
     *
     * @param   CommonEchangeDocBlob $v
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonEchangeDocBlobRelatedByDocBlobPrincipalId(CommonEchangeDocBlob $v = null)
    {
        if ($v === null) {
            $this->setDocBlobPrincipalId(NULL);
        } else {
            $this->setDocBlobPrincipalId($v->getId());
        }

        $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonEchangeDocBlob object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonEchangeDocBlobRelatedById($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonEchangeDocBlob object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonEchangeDocBlob The associated CommonEchangeDocBlob object.
     * @throws PropelException
     */
    public function getCommonEchangeDocBlobRelatedByDocBlobPrincipalId(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId === null && ($this->doc_blob_principal_id !== null) && $doQuery) {
            $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId = CommonEchangeDocBlobQuery::create()->findPk($this->doc_blob_principal_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId->addCommonEchangeDocBlobsRelatedById($this);
             */
        }

        return $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId;
    }

    /**
     * Declares an association between this object and a CommonEchangeDocTypePieceStandard object.
     *
     * @param   CommonEchangeDocTypePieceStandard $v
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonEchangeDocTypePieceStandard(CommonEchangeDocTypePieceStandard $v = null)
    {
        if ($v === null) {
            $this->setEchangeTypePieceStandardId(NULL);
        } else {
            $this->setEchangeTypePieceStandardId($v->getId());
        }

        $this->aCommonEchangeDocTypePieceStandard = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonEchangeDocTypePieceStandard object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonEchangeDocBlob($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonEchangeDocTypePieceStandard object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonEchangeDocTypePieceStandard The associated CommonEchangeDocTypePieceStandard object.
     * @throws PropelException
     */
    public function getCommonEchangeDocTypePieceStandard(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonEchangeDocTypePieceStandard === null && ($this->echange_type_piece_standard_id !== null) && $doQuery) {
            $this->aCommonEchangeDocTypePieceStandard = CommonEchangeDocTypePieceStandardQuery::create()->findPk($this->echange_type_piece_standard_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonEchangeDocTypePieceStandard->addCommonEchangeDocBlobs($this);
             */
        }

        return $this->aCommonEchangeDocTypePieceStandard;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonEchangeDocBlobRelatedById' == $relationName) {
            $this->initCommonEchangeDocBlobsRelatedById();
        }
    }

    /**
     * Clears out the collCommonEchangeDocBlobsRelatedById collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     * @see        addCommonEchangeDocBlobsRelatedById()
     */
    public function clearCommonEchangeDocBlobsRelatedById()
    {
        $this->collCommonEchangeDocBlobsRelatedById = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangeDocBlobsRelatedByIdPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchangeDocBlobsRelatedById collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchangeDocBlobsRelatedById($v = true)
    {
        $this->collCommonEchangeDocBlobsRelatedByIdPartial = $v;
    }

    /**
     * Initializes the collCommonEchangeDocBlobsRelatedById collection.
     *
     * By default this just sets the collCommonEchangeDocBlobsRelatedById collection to an empty array (like clearcollCommonEchangeDocBlobsRelatedById());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchangeDocBlobsRelatedById($overrideExisting = true)
    {
        if (null !== $this->collCommonEchangeDocBlobsRelatedById && !$overrideExisting) {
            return;
        }
        $this->collCommonEchangeDocBlobsRelatedById = new PropelObjectCollection();
        $this->collCommonEchangeDocBlobsRelatedById->setModel('CommonEchangeDocBlob');
    }

    /**
     * Gets an array of CommonEchangeDocBlob objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonEchangeDocBlob is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     * @throws PropelException
     */
    public function getCommonEchangeDocBlobsRelatedById($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocBlobsRelatedByIdPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocBlobsRelatedById || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocBlobsRelatedById) {
                // return empty collection
                $this->initCommonEchangeDocBlobsRelatedById();
            } else {
                $collCommonEchangeDocBlobsRelatedById = CommonEchangeDocBlobQuery::create(null, $criteria)
                    ->filterByCommonEchangeDocBlobRelatedByDocBlobPrincipalId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangeDocBlobsRelatedByIdPartial && count($collCommonEchangeDocBlobsRelatedById)) {
                      $this->initCommonEchangeDocBlobsRelatedById(false);

                      foreach ($collCommonEchangeDocBlobsRelatedById as $obj) {
                        if (false == $this->collCommonEchangeDocBlobsRelatedById->contains($obj)) {
                          $this->collCommonEchangeDocBlobsRelatedById->append($obj);
                        }
                      }

                      $this->collCommonEchangeDocBlobsRelatedByIdPartial = true;
                    }

                    $collCommonEchangeDocBlobsRelatedById->getInternalIterator()->rewind();

                    return $collCommonEchangeDocBlobsRelatedById;
                }

                if ($partial && $this->collCommonEchangeDocBlobsRelatedById) {
                    foreach ($this->collCommonEchangeDocBlobsRelatedById as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchangeDocBlobsRelatedById[] = $obj;
                        }
                    }
                }

                $this->collCommonEchangeDocBlobsRelatedById = $collCommonEchangeDocBlobsRelatedById;
                $this->collCommonEchangeDocBlobsRelatedByIdPartial = false;
            }
        }

        return $this->collCommonEchangeDocBlobsRelatedById;
    }

    /**
     * Sets a collection of CommonEchangeDocBlobRelatedById objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchangeDocBlobsRelatedById A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function setCommonEchangeDocBlobsRelatedById(PropelCollection $commonEchangeDocBlobsRelatedById, PropelPDO $con = null)
    {
        $commonEchangeDocBlobsRelatedByIdToDelete = $this->getCommonEchangeDocBlobsRelatedById(new Criteria(), $con)->diff($commonEchangeDocBlobsRelatedById);


        $this->commonEchangeDocBlobsRelatedByIdScheduledForDeletion = $commonEchangeDocBlobsRelatedByIdToDelete;

        foreach ($commonEchangeDocBlobsRelatedByIdToDelete as $commonEchangeDocBlobRelatedByIdRemoved) {
            $commonEchangeDocBlobRelatedByIdRemoved->setCommonEchangeDocBlobRelatedByDocBlobPrincipalId(null);
        }

        $this->collCommonEchangeDocBlobsRelatedById = null;
        foreach ($commonEchangeDocBlobsRelatedById as $commonEchangeDocBlobRelatedById) {
            $this->addCommonEchangeDocBlobRelatedById($commonEchangeDocBlobRelatedById);
        }

        $this->collCommonEchangeDocBlobsRelatedById = $commonEchangeDocBlobsRelatedById;
        $this->collCommonEchangeDocBlobsRelatedByIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchangeDocBlob objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchangeDocBlob objects.
     * @throws PropelException
     */
    public function countCommonEchangeDocBlobsRelatedById(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocBlobsRelatedByIdPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocBlobsRelatedById || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocBlobsRelatedById) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchangeDocBlobsRelatedById());
            }
            $query = CommonEchangeDocBlobQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonEchangeDocBlobRelatedByDocBlobPrincipalId($this)
                ->count($con);
        }

        return count($this->collCommonEchangeDocBlobsRelatedById);
    }

    /**
     * Method called to associate a CommonEchangeDocBlob object to this object
     * through the CommonEchangeDocBlob foreign key attribute.
     *
     * @param   CommonEchangeDocBlob $l CommonEchangeDocBlob
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function addCommonEchangeDocBlobRelatedById(CommonEchangeDocBlob $l)
    {
        if ($this->collCommonEchangeDocBlobsRelatedById === null) {
            $this->initCommonEchangeDocBlobsRelatedById();
            $this->collCommonEchangeDocBlobsRelatedByIdPartial = true;
        }
        if (!in_array($l, $this->collCommonEchangeDocBlobsRelatedById->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchangeDocBlobRelatedById($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchangeDocBlobRelatedById $commonEchangeDocBlobRelatedById The commonEchangeDocBlobRelatedById object to add.
     */
    protected function doAddCommonEchangeDocBlobRelatedById($commonEchangeDocBlobRelatedById)
    {
        $this->collCommonEchangeDocBlobsRelatedById[]= $commonEchangeDocBlobRelatedById;
        $commonEchangeDocBlobRelatedById->setCommonEchangeDocBlobRelatedByDocBlobPrincipalId($this);
    }

    /**
     * @param	CommonEchangeDocBlobRelatedById $commonEchangeDocBlobRelatedById The commonEchangeDocBlobRelatedById object to remove.
     * @return CommonEchangeDocBlob The current object (for fluent API support)
     */
    public function removeCommonEchangeDocBlobRelatedById($commonEchangeDocBlobRelatedById)
    {
        if ($this->getCommonEchangeDocBlobsRelatedById()->contains($commonEchangeDocBlobRelatedById)) {
            $this->collCommonEchangeDocBlobsRelatedById->remove($this->collCommonEchangeDocBlobsRelatedById->search($commonEchangeDocBlobRelatedById));
            if (null === $this->commonEchangeDocBlobsRelatedByIdScheduledForDeletion) {
                $this->commonEchangeDocBlobsRelatedByIdScheduledForDeletion = clone $this->collCommonEchangeDocBlobsRelatedById;
                $this->commonEchangeDocBlobsRelatedByIdScheduledForDeletion->clear();
            }
            $this->commonEchangeDocBlobsRelatedByIdScheduledForDeletion[]= $commonEchangeDocBlobRelatedById;
            $commonEchangeDocBlobRelatedById->setCommonEchangeDocBlobRelatedByDocBlobPrincipalId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonEchangeDocBlob is new, it will return
     * an empty collection; or if this CommonEchangeDocBlob has previously
     * been saved, it will retrieve related CommonEchangeDocBlobsRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonEchangeDocBlob.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     */
    public function getCommonEchangeDocBlobsRelatedByIdJoinCommonEchangeDoc($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocBlobQuery::create(null, $criteria);
        $query->joinWith('CommonEchangeDoc', $join_behavior);

        return $this->getCommonEchangeDocBlobsRelatedById($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonEchangeDocBlob is new, it will return
     * an empty collection; or if this CommonEchangeDocBlob has previously
     * been saved, it will retrieve related CommonEchangeDocBlobsRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonEchangeDocBlob.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     */
    public function getCommonEchangeDocBlobsRelatedByIdJoinCommonBlobOrganismeFile($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocBlobQuery::create(null, $criteria);
        $query->joinWith('CommonBlobOrganismeFile', $join_behavior);

        return $this->getCommonEchangeDocBlobsRelatedById($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonEchangeDocBlob is new, it will return
     * an empty collection; or if this CommonEchangeDocBlob has previously
     * been saved, it will retrieve related CommonEchangeDocBlobsRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonEchangeDocBlob.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     */
    public function getCommonEchangeDocBlobsRelatedByIdJoinCommonEchangeDocTypePieceStandard($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocBlobQuery::create(null, $criteria);
        $query->joinWith('CommonEchangeDocTypePieceStandard', $join_behavior);

        return $this->getCommonEchangeDocBlobsRelatedById($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->echange_doc_id = null;
        $this->blob_organisme_id = null;
        $this->chemin = null;
        $this->poids = null;
        $this->checksum = null;
        $this->echange_type_piece_actes_id = null;
        $this->echange_type_piece_standard_id = null;
        $this->categorie_piece = null;
        $this->doc_blob_principal_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonEchangeDocBlobsRelatedById) {
                foreach ($this->collCommonEchangeDocBlobsRelatedById as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonEchangeDoc instanceof Persistent) {
              $this->aCommonEchangeDoc->clearAllReferences($deep);
            }
            if ($this->aCommonBlobOrganismeFile instanceof Persistent) {
              $this->aCommonBlobOrganismeFile->clearAllReferences($deep);
            }
            if ($this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId instanceof Persistent) {
              $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId->clearAllReferences($deep);
            }
            if ($this->aCommonEchangeDocTypePieceStandard instanceof Persistent) {
              $this->aCommonEchangeDocTypePieceStandard->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonEchangeDocBlobsRelatedById instanceof PropelCollection) {
            $this->collCommonEchangeDocBlobsRelatedById->clearIterator();
        }
        $this->collCommonEchangeDocBlobsRelatedById = null;
        $this->aCommonEchangeDoc = null;
        $this->aCommonBlobOrganismeFile = null;
        $this->aCommonEchangeDocBlobRelatedByDocBlobPrincipalId = null;
        $this->aCommonEchangeDocTypePieceStandard = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonEchangeDocBlobPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
