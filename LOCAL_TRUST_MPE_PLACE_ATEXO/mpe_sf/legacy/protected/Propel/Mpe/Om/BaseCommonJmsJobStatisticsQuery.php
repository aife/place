<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonJmsJobStatistics;
use Application\Propel\Mpe\CommonJmsJobStatisticsPeer;
use Application\Propel\Mpe\CommonJmsJobStatisticsQuery;

/**
 * Base class that represents a query for the 'jms_job_statistics' table.
 *
 *
 *
 * @method CommonJmsJobStatisticsQuery orderByJobId($order = Criteria::ASC) Order by the job_id column
 * @method CommonJmsJobStatisticsQuery orderByCharacteristic($order = Criteria::ASC) Order by the characteristic column
 * @method CommonJmsJobStatisticsQuery orderByCreatedat($order = Criteria::ASC) Order by the createdAt column
 * @method CommonJmsJobStatisticsQuery orderByCharvalue($order = Criteria::ASC) Order by the charValue column
 *
 * @method CommonJmsJobStatisticsQuery groupByJobId() Group by the job_id column
 * @method CommonJmsJobStatisticsQuery groupByCharacteristic() Group by the characteristic column
 * @method CommonJmsJobStatisticsQuery groupByCreatedat() Group by the createdAt column
 * @method CommonJmsJobStatisticsQuery groupByCharvalue() Group by the charValue column
 *
 * @method CommonJmsJobStatisticsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonJmsJobStatisticsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonJmsJobStatisticsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonJmsJobStatistics findOne(PropelPDO $con = null) Return the first CommonJmsJobStatistics matching the query
 * @method CommonJmsJobStatistics findOneOrCreate(PropelPDO $con = null) Return the first CommonJmsJobStatistics matching the query, or a new CommonJmsJobStatistics object populated from the query conditions when no match is found
 *
 * @method CommonJmsJobStatistics findOneByJobId(string $job_id) Return the first CommonJmsJobStatistics filtered by the job_id column
 * @method CommonJmsJobStatistics findOneByCharacteristic(string $characteristic) Return the first CommonJmsJobStatistics filtered by the characteristic column
 * @method CommonJmsJobStatistics findOneByCreatedat(string $createdAt) Return the first CommonJmsJobStatistics filtered by the createdAt column
 * @method CommonJmsJobStatistics findOneByCharvalue(double $charValue) Return the first CommonJmsJobStatistics filtered by the charValue column
 *
 * @method array findByJobId(string $job_id) Return CommonJmsJobStatistics objects filtered by the job_id column
 * @method array findByCharacteristic(string $characteristic) Return CommonJmsJobStatistics objects filtered by the characteristic column
 * @method array findByCreatedat(string $createdAt) Return CommonJmsJobStatistics objects filtered by the createdAt column
 * @method array findByCharvalue(double $charValue) Return CommonJmsJobStatistics objects filtered by the charValue column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonJmsJobStatisticsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonJmsJobStatisticsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonJmsJobStatistics', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonJmsJobStatisticsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonJmsJobStatisticsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonJmsJobStatisticsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonJmsJobStatisticsQuery) {
            return $criteria;
        }
        $query = new CommonJmsJobStatisticsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34, 56), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$job_id, $characteristic, $createdAt]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonJmsJobStatistics|CommonJmsJobStatistics[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonJmsJobStatisticsPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1], (string) $key[2]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobStatisticsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonJmsJobStatistics A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `job_id`, `characteristic`, `createdAt`, `charValue` FROM `jms_job_statistics` WHERE `job_id` = :p0 AND `characteristic` = :p1 AND `createdAt` = :p2';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->bindValue(':p2', $key[2], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonJmsJobStatistics();
            $obj->hydrate($row);
            CommonJmsJobStatisticsPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1], (string) $key[2])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonJmsJobStatistics|CommonJmsJobStatistics[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonJmsJobStatistics[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonJmsJobStatisticsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(CommonJmsJobStatisticsPeer::JOB_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(CommonJmsJobStatisticsPeer::CHARACTERISTIC, $key[1], Criteria::EQUAL);
        $this->addUsingAlias(CommonJmsJobStatisticsPeer::CREATEDAT, $key[2], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonJmsJobStatisticsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(CommonJmsJobStatisticsPeer::JOB_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(CommonJmsJobStatisticsPeer::CHARACTERISTIC, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $cton2 = $this->getNewCriterion(CommonJmsJobStatisticsPeer::CREATEDAT, $key[2], Criteria::EQUAL);
            $cton0->addAnd($cton2);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the job_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJobId(1234); // WHERE job_id = 1234
     * $query->filterByJobId(array(12, 34)); // WHERE job_id IN (12, 34)
     * $query->filterByJobId(array('min' => 12)); // WHERE job_id >= 12
     * $query->filterByJobId(array('max' => 12)); // WHERE job_id <= 12
     * </code>
     *
     * @param     mixed $jobId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobStatisticsQuery The current query, for fluid interface
     */
    public function filterByJobId($jobId = null, $comparison = null)
    {
        if (is_array($jobId)) {
            $useMinMax = false;
            if (isset($jobId['min'])) {
                $this->addUsingAlias(CommonJmsJobStatisticsPeer::JOB_ID, $jobId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jobId['max'])) {
                $this->addUsingAlias(CommonJmsJobStatisticsPeer::JOB_ID, $jobId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobStatisticsPeer::JOB_ID, $jobId, $comparison);
    }

    /**
     * Filter the query on the characteristic column
     *
     * Example usage:
     * <code>
     * $query->filterByCharacteristic('fooValue');   // WHERE characteristic = 'fooValue'
     * $query->filterByCharacteristic('%fooValue%'); // WHERE characteristic LIKE '%fooValue%'
     * </code>
     *
     * @param     string $characteristic The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobStatisticsQuery The current query, for fluid interface
     */
    public function filterByCharacteristic($characteristic = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($characteristic)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $characteristic)) {
                $characteristic = str_replace('*', '%', $characteristic);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonJmsJobStatisticsPeer::CHARACTERISTIC, $characteristic, $comparison);
    }

    /**
     * Filter the query on the createdAt column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedat('2011-03-14'); // WHERE createdAt = '2011-03-14'
     * $query->filterByCreatedat('now'); // WHERE createdAt = '2011-03-14'
     * $query->filterByCreatedat(array('max' => 'yesterday')); // WHERE createdAt > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobStatisticsQuery The current query, for fluid interface
     */
    public function filterByCreatedat($createdat = null, $comparison = null)
    {
        if (is_array($createdat)) {
            $useMinMax = false;
            if (isset($createdat['min'])) {
                $this->addUsingAlias(CommonJmsJobStatisticsPeer::CREATEDAT, $createdat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdat['max'])) {
                $this->addUsingAlias(CommonJmsJobStatisticsPeer::CREATEDAT, $createdat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobStatisticsPeer::CREATEDAT, $createdat, $comparison);
    }

    /**
     * Filter the query on the charValue column
     *
     * Example usage:
     * <code>
     * $query->filterByCharvalue(1234); // WHERE charValue = 1234
     * $query->filterByCharvalue(array(12, 34)); // WHERE charValue IN (12, 34)
     * $query->filterByCharvalue(array('min' => 12)); // WHERE charValue >= 12
     * $query->filterByCharvalue(array('max' => 12)); // WHERE charValue <= 12
     * </code>
     *
     * @param     mixed $charvalue The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobStatisticsQuery The current query, for fluid interface
     */
    public function filterByCharvalue($charvalue = null, $comparison = null)
    {
        if (is_array($charvalue)) {
            $useMinMax = false;
            if (isset($charvalue['min'])) {
                $this->addUsingAlias(CommonJmsJobStatisticsPeer::CHARVALUE, $charvalue['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($charvalue['max'])) {
                $this->addUsingAlias(CommonJmsJobStatisticsPeer::CHARVALUE, $charvalue['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobStatisticsPeer::CHARVALUE, $charvalue, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonJmsJobStatistics $commonJmsJobStatistics Object to remove from the list of results
     *
     * @return CommonJmsJobStatisticsQuery The current query, for fluid interface
     */
    public function prune($commonJmsJobStatistics = null)
    {
        if ($commonJmsJobStatistics) {
            $this->addCond('pruneCond0', $this->getAliasedColName(CommonJmsJobStatisticsPeer::JOB_ID), $commonJmsJobStatistics->getJobId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(CommonJmsJobStatisticsPeer::CHARACTERISTIC), $commonJmsJobStatistics->getCharacteristic(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond2', $this->getAliasedColName(CommonJmsJobStatisticsPeer::CREATEDAT), $commonJmsJobStatistics->getCreatedat(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1', 'pruneCond2'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
