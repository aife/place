<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonEchangeDocApplicationClient;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientPeer;
use Application\Propel\Mpe\CommonEchangeDocApplicationPeer;
use Application\Propel\Mpe\Map\CommonEchangeDocApplicationClientTableMap;

/**
 * Base static class for performing query and update operations on the 'echange_doc_application_client' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangeDocApplicationClientPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'echange_doc_application_client';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonEchangeDocApplicationClient';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonEchangeDocApplicationClientTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 16;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 16;

    /** the column name for the id field */
    const ID = 'echange_doc_application_client.id';

    /** the column name for the echange_doc_application_id field */
    const ECHANGE_DOC_APPLICATION_ID = 'echange_doc_application_client.echange_doc_application_id';

    /** the column name for the code field */
    const CODE = 'echange_doc_application_client.code';

    /** the column name for the libelle field */
    const LIBELLE = 'echange_doc_application_client.libelle';

    /** the column name for the actif field */
    const ACTIF = 'echange_doc_application_client.actif';

    /** the column name for the cheminement_signature field */
    const CHEMINEMENT_SIGNATURE = 'echange_doc_application_client.cheminement_signature';

    /** the column name for the cheminement_ged field */
    const CHEMINEMENT_GED = 'echange_doc_application_client.cheminement_ged';

    /** the column name for the cheminement_sae field */
    const CHEMINEMENT_SAE = 'echange_doc_application_client.cheminement_sae';

    /** the column name for the cheminement_tdt field */
    const CHEMINEMENT_TDT = 'echange_doc_application_client.cheminement_tdt';

    /** the column name for the classification_1 field */
    const CLASSIFICATION_1 = 'echange_doc_application_client.classification_1';

    /** the column name for the classification_2 field */
    const CLASSIFICATION_2 = 'echange_doc_application_client.classification_2';

    /** the column name for the classification_3 field */
    const CLASSIFICATION_3 = 'echange_doc_application_client.classification_3';

    /** the column name for the classification_4 field */
    const CLASSIFICATION_4 = 'echange_doc_application_client.classification_4';

    /** the column name for the classification_5 field */
    const CLASSIFICATION_5 = 'echange_doc_application_client.classification_5';

    /** the column name for the multi_docs_principaux field */
    const MULTI_DOCS_PRINCIPAUX = 'echange_doc_application_client.multi_docs_principaux';

    /** the column name for the envoi_auto_archivage field */
    const ENVOI_AUTO_ARCHIVAGE = 'echange_doc_application_client.envoi_auto_archivage';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonEchangeDocApplicationClient objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonEchangeDocApplicationClient[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonEchangeDocApplicationClientPeer::$fieldNames[CommonEchangeDocApplicationClientPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'EchangeDocApplicationId', 'Code', 'Libelle', 'Actif', 'CheminementSignature', 'CheminementGed', 'CheminementSae', 'CheminementTdt', 'Classification1', 'Classification2', 'Classification3', 'Classification4', 'Classification5', 'MultiDocsPrincipaux', 'EnvoiAutoArchivage', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'echangeDocApplicationId', 'code', 'libelle', 'actif', 'cheminementSignature', 'cheminementGed', 'cheminementSae', 'cheminementTdt', 'classification1', 'classification2', 'classification3', 'classification4', 'classification5', 'multiDocsPrincipaux', 'envoiAutoArchivage', ),
        BasePeer::TYPE_COLNAME => array (CommonEchangeDocApplicationClientPeer::ID, CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID, CommonEchangeDocApplicationClientPeer::CODE, CommonEchangeDocApplicationClientPeer::LIBELLE, CommonEchangeDocApplicationClientPeer::ACTIF, CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SIGNATURE, CommonEchangeDocApplicationClientPeer::CHEMINEMENT_GED, CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SAE, CommonEchangeDocApplicationClientPeer::CHEMINEMENT_TDT, CommonEchangeDocApplicationClientPeer::CLASSIFICATION_1, CommonEchangeDocApplicationClientPeer::CLASSIFICATION_2, CommonEchangeDocApplicationClientPeer::CLASSIFICATION_3, CommonEchangeDocApplicationClientPeer::CLASSIFICATION_4, CommonEchangeDocApplicationClientPeer::CLASSIFICATION_5, CommonEchangeDocApplicationClientPeer::MULTI_DOCS_PRINCIPAUX, CommonEchangeDocApplicationClientPeer::ENVOI_AUTO_ARCHIVAGE, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ECHANGE_DOC_APPLICATION_ID', 'CODE', 'LIBELLE', 'ACTIF', 'CHEMINEMENT_SIGNATURE', 'CHEMINEMENT_GED', 'CHEMINEMENT_SAE', 'CHEMINEMENT_TDT', 'CLASSIFICATION_1', 'CLASSIFICATION_2', 'CLASSIFICATION_3', 'CLASSIFICATION_4', 'CLASSIFICATION_5', 'MULTI_DOCS_PRINCIPAUX', 'ENVOI_AUTO_ARCHIVAGE', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'echange_doc_application_id', 'code', 'libelle', 'actif', 'cheminement_signature', 'cheminement_ged', 'cheminement_sae', 'cheminement_tdt', 'classification_1', 'classification_2', 'classification_3', 'classification_4', 'classification_5', 'multi_docs_principaux', 'envoi_auto_archivage', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonEchangeDocApplicationClientPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'EchangeDocApplicationId' => 1, 'Code' => 2, 'Libelle' => 3, 'Actif' => 4, 'CheminementSignature' => 5, 'CheminementGed' => 6, 'CheminementSae' => 7, 'CheminementTdt' => 8, 'Classification1' => 9, 'Classification2' => 10, 'Classification3' => 11, 'Classification4' => 12, 'Classification5' => 13, 'MultiDocsPrincipaux' => 14, 'EnvoiAutoArchivage' => 15, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'echangeDocApplicationId' => 1, 'code' => 2, 'libelle' => 3, 'actif' => 4, 'cheminementSignature' => 5, 'cheminementGed' => 6, 'cheminementSae' => 7, 'cheminementTdt' => 8, 'classification1' => 9, 'classification2' => 10, 'classification3' => 11, 'classification4' => 12, 'classification5' => 13, 'multiDocsPrincipaux' => 14, 'envoiAutoArchivage' => 15, ),
        BasePeer::TYPE_COLNAME => array (CommonEchangeDocApplicationClientPeer::ID => 0, CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID => 1, CommonEchangeDocApplicationClientPeer::CODE => 2, CommonEchangeDocApplicationClientPeer::LIBELLE => 3, CommonEchangeDocApplicationClientPeer::ACTIF => 4, CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SIGNATURE => 5, CommonEchangeDocApplicationClientPeer::CHEMINEMENT_GED => 6, CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SAE => 7, CommonEchangeDocApplicationClientPeer::CHEMINEMENT_TDT => 8, CommonEchangeDocApplicationClientPeer::CLASSIFICATION_1 => 9, CommonEchangeDocApplicationClientPeer::CLASSIFICATION_2 => 10, CommonEchangeDocApplicationClientPeer::CLASSIFICATION_3 => 11, CommonEchangeDocApplicationClientPeer::CLASSIFICATION_4 => 12, CommonEchangeDocApplicationClientPeer::CLASSIFICATION_5 => 13, CommonEchangeDocApplicationClientPeer::MULTI_DOCS_PRINCIPAUX => 14, CommonEchangeDocApplicationClientPeer::ENVOI_AUTO_ARCHIVAGE => 15, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ECHANGE_DOC_APPLICATION_ID' => 1, 'CODE' => 2, 'LIBELLE' => 3, 'ACTIF' => 4, 'CHEMINEMENT_SIGNATURE' => 5, 'CHEMINEMENT_GED' => 6, 'CHEMINEMENT_SAE' => 7, 'CHEMINEMENT_TDT' => 8, 'CLASSIFICATION_1' => 9, 'CLASSIFICATION_2' => 10, 'CLASSIFICATION_3' => 11, 'CLASSIFICATION_4' => 12, 'CLASSIFICATION_5' => 13, 'MULTI_DOCS_PRINCIPAUX' => 14, 'ENVOI_AUTO_ARCHIVAGE' => 15, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'echange_doc_application_id' => 1, 'code' => 2, 'libelle' => 3, 'actif' => 4, 'cheminement_signature' => 5, 'cheminement_ged' => 6, 'cheminement_sae' => 7, 'cheminement_tdt' => 8, 'classification_1' => 9, 'classification_2' => 10, 'classification_3' => 11, 'classification_4' => 12, 'classification_5' => 13, 'multi_docs_principaux' => 14, 'envoi_auto_archivage' => 15, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonEchangeDocApplicationClientPeer::getFieldNames($toType);
        $key = isset(CommonEchangeDocApplicationClientPeer::$fieldKeys[$fromType][$name]) ? CommonEchangeDocApplicationClientPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonEchangeDocApplicationClientPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonEchangeDocApplicationClientPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonEchangeDocApplicationClientPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonEchangeDocApplicationClientPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonEchangeDocApplicationClientPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::ID);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::CODE);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::LIBELLE);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::ACTIF);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SIGNATURE);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_GED);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_SAE);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::CHEMINEMENT_TDT);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_1);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_2);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_3);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_4);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::CLASSIFICATION_5);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::MULTI_DOCS_PRINCIPAUX);
            $criteria->addSelectColumn(CommonEchangeDocApplicationClientPeer::ENVOI_AUTO_ARCHIVAGE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.echange_doc_application_id');
            $criteria->addSelectColumn($alias . '.code');
            $criteria->addSelectColumn($alias . '.libelle');
            $criteria->addSelectColumn($alias . '.actif');
            $criteria->addSelectColumn($alias . '.cheminement_signature');
            $criteria->addSelectColumn($alias . '.cheminement_ged');
            $criteria->addSelectColumn($alias . '.cheminement_sae');
            $criteria->addSelectColumn($alias . '.cheminement_tdt');
            $criteria->addSelectColumn($alias . '.classification_1');
            $criteria->addSelectColumn($alias . '.classification_2');
            $criteria->addSelectColumn($alias . '.classification_3');
            $criteria->addSelectColumn($alias . '.classification_4');
            $criteria->addSelectColumn($alias . '.classification_5');
            $criteria->addSelectColumn($alias . '.multi_docs_principaux');
            $criteria->addSelectColumn($alias . '.envoi_auto_archivage');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocApplicationClientPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonEchangeDocApplicationClientPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonEchangeDocApplicationClient
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonEchangeDocApplicationClientPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonEchangeDocApplicationClientPeer::populateObjects(CommonEchangeDocApplicationClientPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonEchangeDocApplicationClient $obj A CommonEchangeDocApplicationClient object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonEchangeDocApplicationClientPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonEchangeDocApplicationClient object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonEchangeDocApplicationClient) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonEchangeDocApplicationClient object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonEchangeDocApplicationClientPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonEchangeDocApplicationClient Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonEchangeDocApplicationClientPeer::$instances[$key])) {
                return CommonEchangeDocApplicationClientPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonEchangeDocApplicationClientPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonEchangeDocApplicationClientPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to echange_doc_application_client
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonEchangeDocApplicationClientPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonEchangeDocApplicationClientPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonEchangeDocApplicationClientPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonEchangeDocApplicationClientPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonEchangeDocApplicationClient object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonEchangeDocApplicationClientPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonEchangeDocApplicationClientPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonEchangeDocApplicationClientPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonEchangeDocApplicationClientPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonEchangeDocApplicationClientPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonEchangeDocApplication table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonEchangeDocApplication(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocApplicationClientPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID, CommonEchangeDocApplicationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonEchangeDocApplicationClient objects pre-filled with their CommonEchangeDocApplication objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDocApplicationClient objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonEchangeDocApplication(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);
        }

        CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);
        $startcol = CommonEchangeDocApplicationClientPeer::NUM_HYDRATE_COLUMNS;
        CommonEchangeDocApplicationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID, CommonEchangeDocApplicationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocApplicationClientPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocApplicationClientPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonEchangeDocApplicationClientPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocApplicationClientPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonEchangeDocApplicationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonEchangeDocApplicationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonEchangeDocApplicationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonEchangeDocApplicationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonEchangeDocApplicationClient) to $obj2 (CommonEchangeDocApplication)
                $obj2->addCommonEchangeDocApplicationClient($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonEchangeDocApplicationClientPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID, CommonEchangeDocApplicationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonEchangeDocApplicationClient objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonEchangeDocApplicationClient objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);
        }

        CommonEchangeDocApplicationClientPeer::addSelectColumns($criteria);
        $startcol2 = CommonEchangeDocApplicationClientPeer::NUM_HYDRATE_COLUMNS;

        CommonEchangeDocApplicationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonEchangeDocApplicationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonEchangeDocApplicationClientPeer::ECHANGE_DOC_APPLICATION_ID, CommonEchangeDocApplicationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonEchangeDocApplicationClientPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonEchangeDocApplicationClientPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonEchangeDocApplicationClientPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonEchangeDocApplicationClientPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonEchangeDocApplication rows

            $key2 = CommonEchangeDocApplicationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonEchangeDocApplicationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonEchangeDocApplicationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonEchangeDocApplicationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonEchangeDocApplicationClient) to the collection in $obj2 (CommonEchangeDocApplication)
                $obj2->addCommonEchangeDocApplicationClient($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonEchangeDocApplicationClientPeer::DATABASE_NAME)->getTable(CommonEchangeDocApplicationClientPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonEchangeDocApplicationClientPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonEchangeDocApplicationClientPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonEchangeDocApplicationClientTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonEchangeDocApplicationClientPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonEchangeDocApplicationClient or Criteria object.
     *
     * @param      mixed $values Criteria or CommonEchangeDocApplicationClient object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonEchangeDocApplicationClient object
        }

        if ($criteria->containsKey(CommonEchangeDocApplicationClientPeer::ID) && $criteria->keyContainsValue(CommonEchangeDocApplicationClientPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonEchangeDocApplicationClientPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonEchangeDocApplicationClient or Criteria object.
     *
     * @param      mixed $values Criteria or CommonEchangeDocApplicationClient object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonEchangeDocApplicationClientPeer::ID);
            $value = $criteria->remove(CommonEchangeDocApplicationClientPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonEchangeDocApplicationClientPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonEchangeDocApplicationClientPeer::TABLE_NAME);
            }

        } else { // $values is CommonEchangeDocApplicationClient object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the echange_doc_application_client table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonEchangeDocApplicationClientPeer::TABLE_NAME, $con, CommonEchangeDocApplicationClientPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonEchangeDocApplicationClientPeer::clearInstancePool();
            CommonEchangeDocApplicationClientPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonEchangeDocApplicationClient or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonEchangeDocApplicationClient object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonEchangeDocApplicationClientPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonEchangeDocApplicationClient) { // it's a model object
            // invalidate the cache for this single object
            CommonEchangeDocApplicationClientPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);
            $criteria->add(CommonEchangeDocApplicationClientPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonEchangeDocApplicationClientPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonEchangeDocApplicationClientPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonEchangeDocApplicationClient object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonEchangeDocApplicationClient $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonEchangeDocApplicationClientPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, CommonEchangeDocApplicationClientPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonEchangeDocApplicationClient
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonEchangeDocApplicationClientPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);
        $criteria->add(CommonEchangeDocApplicationClientPeer::ID, $pk);

        $v = CommonEchangeDocApplicationClientPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonEchangeDocApplicationClient[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonEchangeDocApplicationClientPeer::DATABASE_NAME);
            $criteria->add(CommonEchangeDocApplicationClientPeer::ID, $pks, Criteria::IN);
            $objs = CommonEchangeDocApplicationClientPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonEchangeDocApplicationClientPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonEchangeDocApplicationClientPeer::buildTableMap();

