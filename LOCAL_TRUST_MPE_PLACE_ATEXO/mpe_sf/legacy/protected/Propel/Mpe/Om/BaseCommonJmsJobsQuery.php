<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonJmsJobDependencies;
use Application\Propel\Mpe\CommonJmsJobRelatedEntities;
use Application\Propel\Mpe\CommonJmsJobs;
use Application\Propel\Mpe\CommonJmsJobsPeer;
use Application\Propel\Mpe\CommonJmsJobsQuery;

/**
 * Base class that represents a query for the 'jms_jobs' table.
 *
 *
 *
 * @method CommonJmsJobsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonJmsJobsQuery orderByState($order = Criteria::ASC) Order by the state column
 * @method CommonJmsJobsQuery orderByQueue($order = Criteria::ASC) Order by the queue column
 * @method CommonJmsJobsQuery orderByPriority($order = Criteria::ASC) Order by the priority column
 * @method CommonJmsJobsQuery orderByCreatedat($order = Criteria::ASC) Order by the createdAt column
 * @method CommonJmsJobsQuery orderByStartedat($order = Criteria::ASC) Order by the startedAt column
 * @method CommonJmsJobsQuery orderByCheckedat($order = Criteria::ASC) Order by the checkedAt column
 * @method CommonJmsJobsQuery orderByWorkername($order = Criteria::ASC) Order by the workerName column
 * @method CommonJmsJobsQuery orderByExecuteafter($order = Criteria::ASC) Order by the executeAfter column
 * @method CommonJmsJobsQuery orderByClosedat($order = Criteria::ASC) Order by the closedAt column
 * @method CommonJmsJobsQuery orderByCommand($order = Criteria::ASC) Order by the command column
 * @method CommonJmsJobsQuery orderByArgs($order = Criteria::ASC) Order by the args column
 * @method CommonJmsJobsQuery orderByOutput($order = Criteria::ASC) Order by the output column
 * @method CommonJmsJobsQuery orderByErroroutput($order = Criteria::ASC) Order by the errorOutput column
 * @method CommonJmsJobsQuery orderByExitcode($order = Criteria::ASC) Order by the exitCode column
 * @method CommonJmsJobsQuery orderByMaxruntime($order = Criteria::ASC) Order by the maxRuntime column
 * @method CommonJmsJobsQuery orderByMaxretries($order = Criteria::ASC) Order by the maxRetries column
 * @method CommonJmsJobsQuery orderByStacktrace($order = Criteria::ASC) Order by the stackTrace column
 * @method CommonJmsJobsQuery orderByRuntime($order = Criteria::ASC) Order by the runtime column
 * @method CommonJmsJobsQuery orderByMemoryusage($order = Criteria::ASC) Order by the memoryUsage column
 * @method CommonJmsJobsQuery orderByMemoryusagereal($order = Criteria::ASC) Order by the memoryUsageReal column
 * @method CommonJmsJobsQuery orderByOriginaljobId($order = Criteria::ASC) Order by the originalJob_id column
 *
 * @method CommonJmsJobsQuery groupById() Group by the id column
 * @method CommonJmsJobsQuery groupByState() Group by the state column
 * @method CommonJmsJobsQuery groupByQueue() Group by the queue column
 * @method CommonJmsJobsQuery groupByPriority() Group by the priority column
 * @method CommonJmsJobsQuery groupByCreatedat() Group by the createdAt column
 * @method CommonJmsJobsQuery groupByStartedat() Group by the startedAt column
 * @method CommonJmsJobsQuery groupByCheckedat() Group by the checkedAt column
 * @method CommonJmsJobsQuery groupByWorkername() Group by the workerName column
 * @method CommonJmsJobsQuery groupByExecuteafter() Group by the executeAfter column
 * @method CommonJmsJobsQuery groupByClosedat() Group by the closedAt column
 * @method CommonJmsJobsQuery groupByCommand() Group by the command column
 * @method CommonJmsJobsQuery groupByArgs() Group by the args column
 * @method CommonJmsJobsQuery groupByOutput() Group by the output column
 * @method CommonJmsJobsQuery groupByErroroutput() Group by the errorOutput column
 * @method CommonJmsJobsQuery groupByExitcode() Group by the exitCode column
 * @method CommonJmsJobsQuery groupByMaxruntime() Group by the maxRuntime column
 * @method CommonJmsJobsQuery groupByMaxretries() Group by the maxRetries column
 * @method CommonJmsJobsQuery groupByStacktrace() Group by the stackTrace column
 * @method CommonJmsJobsQuery groupByRuntime() Group by the runtime column
 * @method CommonJmsJobsQuery groupByMemoryusage() Group by the memoryUsage column
 * @method CommonJmsJobsQuery groupByMemoryusagereal() Group by the memoryUsageReal column
 * @method CommonJmsJobsQuery groupByOriginaljobId() Group by the originalJob_id column
 *
 * @method CommonJmsJobsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonJmsJobsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonJmsJobsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonJmsJobsQuery leftJoinCommonJmsJobsRelatedByOriginaljobId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonJmsJobsRelatedByOriginaljobId relation
 * @method CommonJmsJobsQuery rightJoinCommonJmsJobsRelatedByOriginaljobId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonJmsJobsRelatedByOriginaljobId relation
 * @method CommonJmsJobsQuery innerJoinCommonJmsJobsRelatedByOriginaljobId($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonJmsJobsRelatedByOriginaljobId relation
 *
 * @method CommonJmsJobsQuery leftJoinCommonJmsJobDependenciesRelatedByDestJobId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonJmsJobDependenciesRelatedByDestJobId relation
 * @method CommonJmsJobsQuery rightJoinCommonJmsJobDependenciesRelatedByDestJobId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonJmsJobDependenciesRelatedByDestJobId relation
 * @method CommonJmsJobsQuery innerJoinCommonJmsJobDependenciesRelatedByDestJobId($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonJmsJobDependenciesRelatedByDestJobId relation
 *
 * @method CommonJmsJobsQuery leftJoinCommonJmsJobDependenciesRelatedBySourceJobId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonJmsJobDependenciesRelatedBySourceJobId relation
 * @method CommonJmsJobsQuery rightJoinCommonJmsJobDependenciesRelatedBySourceJobId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonJmsJobDependenciesRelatedBySourceJobId relation
 * @method CommonJmsJobsQuery innerJoinCommonJmsJobDependenciesRelatedBySourceJobId($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonJmsJobDependenciesRelatedBySourceJobId relation
 *
 * @method CommonJmsJobsQuery leftJoinCommonJmsJobRelatedEntities($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonJmsJobRelatedEntities relation
 * @method CommonJmsJobsQuery rightJoinCommonJmsJobRelatedEntities($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonJmsJobRelatedEntities relation
 * @method CommonJmsJobsQuery innerJoinCommonJmsJobRelatedEntities($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonJmsJobRelatedEntities relation
 *
 * @method CommonJmsJobsQuery leftJoinCommonJmsJobsRelatedById($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonJmsJobsRelatedById relation
 * @method CommonJmsJobsQuery rightJoinCommonJmsJobsRelatedById($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonJmsJobsRelatedById relation
 * @method CommonJmsJobsQuery innerJoinCommonJmsJobsRelatedById($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonJmsJobsRelatedById relation
 *
 * @method CommonJmsJobs findOne(PropelPDO $con = null) Return the first CommonJmsJobs matching the query
 * @method CommonJmsJobs findOneOrCreate(PropelPDO $con = null) Return the first CommonJmsJobs matching the query, or a new CommonJmsJobs object populated from the query conditions when no match is found
 *
 * @method CommonJmsJobs findOneByState(string $state) Return the first CommonJmsJobs filtered by the state column
 * @method CommonJmsJobs findOneByQueue(string $queue) Return the first CommonJmsJobs filtered by the queue column
 * @method CommonJmsJobs findOneByPriority(int $priority) Return the first CommonJmsJobs filtered by the priority column
 * @method CommonJmsJobs findOneByCreatedat(string $createdAt) Return the first CommonJmsJobs filtered by the createdAt column
 * @method CommonJmsJobs findOneByStartedat(string $startedAt) Return the first CommonJmsJobs filtered by the startedAt column
 * @method CommonJmsJobs findOneByCheckedat(string $checkedAt) Return the first CommonJmsJobs filtered by the checkedAt column
 * @method CommonJmsJobs findOneByWorkername(string $workerName) Return the first CommonJmsJobs filtered by the workerName column
 * @method CommonJmsJobs findOneByExecuteafter(string $executeAfter) Return the first CommonJmsJobs filtered by the executeAfter column
 * @method CommonJmsJobs findOneByClosedat(string $closedAt) Return the first CommonJmsJobs filtered by the closedAt column
 * @method CommonJmsJobs findOneByCommand(string $command) Return the first CommonJmsJobs filtered by the command column
 * @method CommonJmsJobs findOneByArgs(string $args) Return the first CommonJmsJobs filtered by the args column
 * @method CommonJmsJobs findOneByOutput(string $output) Return the first CommonJmsJobs filtered by the output column
 * @method CommonJmsJobs findOneByErroroutput(string $errorOutput) Return the first CommonJmsJobs filtered by the errorOutput column
 * @method CommonJmsJobs findOneByExitcode(int $exitCode) Return the first CommonJmsJobs filtered by the exitCode column
 * @method CommonJmsJobs findOneByMaxruntime(int $maxRuntime) Return the first CommonJmsJobs filtered by the maxRuntime column
 * @method CommonJmsJobs findOneByMaxretries(int $maxRetries) Return the first CommonJmsJobs filtered by the maxRetries column
 * @method CommonJmsJobs findOneByStacktrace(resource $stackTrace) Return the first CommonJmsJobs filtered by the stackTrace column
 * @method CommonJmsJobs findOneByRuntime(int $runtime) Return the first CommonJmsJobs filtered by the runtime column
 * @method CommonJmsJobs findOneByMemoryusage(int $memoryUsage) Return the first CommonJmsJobs filtered by the memoryUsage column
 * @method CommonJmsJobs findOneByMemoryusagereal(int $memoryUsageReal) Return the first CommonJmsJobs filtered by the memoryUsageReal column
 * @method CommonJmsJobs findOneByOriginaljobId(string $originalJob_id) Return the first CommonJmsJobs filtered by the originalJob_id column
 *
 * @method array findById(string $id) Return CommonJmsJobs objects filtered by the id column
 * @method array findByState(string $state) Return CommonJmsJobs objects filtered by the state column
 * @method array findByQueue(string $queue) Return CommonJmsJobs objects filtered by the queue column
 * @method array findByPriority(int $priority) Return CommonJmsJobs objects filtered by the priority column
 * @method array findByCreatedat(string $createdAt) Return CommonJmsJobs objects filtered by the createdAt column
 * @method array findByStartedat(string $startedAt) Return CommonJmsJobs objects filtered by the startedAt column
 * @method array findByCheckedat(string $checkedAt) Return CommonJmsJobs objects filtered by the checkedAt column
 * @method array findByWorkername(string $workerName) Return CommonJmsJobs objects filtered by the workerName column
 * @method array findByExecuteafter(string $executeAfter) Return CommonJmsJobs objects filtered by the executeAfter column
 * @method array findByClosedat(string $closedAt) Return CommonJmsJobs objects filtered by the closedAt column
 * @method array findByCommand(string $command) Return CommonJmsJobs objects filtered by the command column
 * @method array findByArgs(string $args) Return CommonJmsJobs objects filtered by the args column
 * @method array findByOutput(string $output) Return CommonJmsJobs objects filtered by the output column
 * @method array findByErroroutput(string $errorOutput) Return CommonJmsJobs objects filtered by the errorOutput column
 * @method array findByExitcode(int $exitCode) Return CommonJmsJobs objects filtered by the exitCode column
 * @method array findByMaxruntime(int $maxRuntime) Return CommonJmsJobs objects filtered by the maxRuntime column
 * @method array findByMaxretries(int $maxRetries) Return CommonJmsJobs objects filtered by the maxRetries column
 * @method array findByStacktrace(resource $stackTrace) Return CommonJmsJobs objects filtered by the stackTrace column
 * @method array findByRuntime(int $runtime) Return CommonJmsJobs objects filtered by the runtime column
 * @method array findByMemoryusage(int $memoryUsage) Return CommonJmsJobs objects filtered by the memoryUsage column
 * @method array findByMemoryusagereal(int $memoryUsageReal) Return CommonJmsJobs objects filtered by the memoryUsageReal column
 * @method array findByOriginaljobId(string $originalJob_id) Return CommonJmsJobs objects filtered by the originalJob_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonJmsJobsQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonJmsJobsQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonJmsJobs', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonJmsJobsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonJmsJobsQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonJmsJobsQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonJmsJobsQuery) {
            return $criteria;
        }
        $query = new CommonJmsJobsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonJmsJobs|CommonJmsJobs[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonJmsJobsPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobsPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonJmsJobs A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonJmsJobs A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `state`, `queue`, `priority`, `createdAt`, `startedAt`, `checkedAt`, `workerName`, `executeAfter`, `closedAt`, `command`, `args`, `output`, `errorOutput`, `exitCode`, `maxRuntime`, `maxRetries`, `stackTrace`, `runtime`, `memoryUsage`, `memoryUsageReal`, `originalJob_id` FROM `jms_jobs` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonJmsJobs();
            $obj->hydrate($row);
            CommonJmsJobsPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonJmsJobs|CommonJmsJobs[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonJmsJobs[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonJmsJobsPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonJmsJobsPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState('fooValue');   // WHERE state = 'fooValue'
     * $query->filterByState('%fooValue%'); // WHERE state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $state The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($state)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $state)) {
                $state = str_replace('*', '%', $state);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::STATE, $state, $comparison);
    }

    /**
     * Filter the query on the queue column
     *
     * Example usage:
     * <code>
     * $query->filterByQueue('fooValue');   // WHERE queue = 'fooValue'
     * $query->filterByQueue('%fooValue%'); // WHERE queue LIKE '%fooValue%'
     * </code>
     *
     * @param     string $queue The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByQueue($queue = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($queue)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $queue)) {
                $queue = str_replace('*', '%', $queue);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::QUEUE, $queue, $comparison);
    }

    /**
     * Filter the query on the priority column
     *
     * Example usage:
     * <code>
     * $query->filterByPriority(1234); // WHERE priority = 1234
     * $query->filterByPriority(array(12, 34)); // WHERE priority IN (12, 34)
     * $query->filterByPriority(array('min' => 12)); // WHERE priority >= 12
     * $query->filterByPriority(array('max' => 12)); // WHERE priority <= 12
     * </code>
     *
     * @param     mixed $priority The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByPriority($priority = null, $comparison = null)
    {
        if (is_array($priority)) {
            $useMinMax = false;
            if (isset($priority['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::PRIORITY, $priority['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($priority['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::PRIORITY, $priority['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::PRIORITY, $priority, $comparison);
    }

    /**
     * Filter the query on the createdAt column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedat('2011-03-14'); // WHERE createdAt = '2011-03-14'
     * $query->filterByCreatedat('now'); // WHERE createdAt = '2011-03-14'
     * $query->filterByCreatedat(array('max' => 'yesterday')); // WHERE createdAt > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByCreatedat($createdat = null, $comparison = null)
    {
        if (is_array($createdat)) {
            $useMinMax = false;
            if (isset($createdat['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::CREATEDAT, $createdat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdat['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::CREATEDAT, $createdat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::CREATEDAT, $createdat, $comparison);
    }

    /**
     * Filter the query on the startedAt column
     *
     * Example usage:
     * <code>
     * $query->filterByStartedat('2011-03-14'); // WHERE startedAt = '2011-03-14'
     * $query->filterByStartedat('now'); // WHERE startedAt = '2011-03-14'
     * $query->filterByStartedat(array('max' => 'yesterday')); // WHERE startedAt > '2011-03-13'
     * </code>
     *
     * @param     mixed $startedat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByStartedat($startedat = null, $comparison = null)
    {
        if (is_array($startedat)) {
            $useMinMax = false;
            if (isset($startedat['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::STARTEDAT, $startedat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startedat['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::STARTEDAT, $startedat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::STARTEDAT, $startedat, $comparison);
    }

    /**
     * Filter the query on the checkedAt column
     *
     * Example usage:
     * <code>
     * $query->filterByCheckedat('2011-03-14'); // WHERE checkedAt = '2011-03-14'
     * $query->filterByCheckedat('now'); // WHERE checkedAt = '2011-03-14'
     * $query->filterByCheckedat(array('max' => 'yesterday')); // WHERE checkedAt > '2011-03-13'
     * </code>
     *
     * @param     mixed $checkedat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByCheckedat($checkedat = null, $comparison = null)
    {
        if (is_array($checkedat)) {
            $useMinMax = false;
            if (isset($checkedat['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::CHECKEDAT, $checkedat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($checkedat['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::CHECKEDAT, $checkedat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::CHECKEDAT, $checkedat, $comparison);
    }

    /**
     * Filter the query on the workerName column
     *
     * Example usage:
     * <code>
     * $query->filterByWorkername('fooValue');   // WHERE workerName = 'fooValue'
     * $query->filterByWorkername('%fooValue%'); // WHERE workerName LIKE '%fooValue%'
     * </code>
     *
     * @param     string $workername The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByWorkername($workername = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($workername)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $workername)) {
                $workername = str_replace('*', '%', $workername);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::WORKERNAME, $workername, $comparison);
    }

    /**
     * Filter the query on the executeAfter column
     *
     * Example usage:
     * <code>
     * $query->filterByExecuteafter('2011-03-14'); // WHERE executeAfter = '2011-03-14'
     * $query->filterByExecuteafter('now'); // WHERE executeAfter = '2011-03-14'
     * $query->filterByExecuteafter(array('max' => 'yesterday')); // WHERE executeAfter > '2011-03-13'
     * </code>
     *
     * @param     mixed $executeafter The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByExecuteafter($executeafter = null, $comparison = null)
    {
        if (is_array($executeafter)) {
            $useMinMax = false;
            if (isset($executeafter['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::EXECUTEAFTER, $executeafter['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($executeafter['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::EXECUTEAFTER, $executeafter['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::EXECUTEAFTER, $executeafter, $comparison);
    }

    /**
     * Filter the query on the closedAt column
     *
     * Example usage:
     * <code>
     * $query->filterByClosedat('2011-03-14'); // WHERE closedAt = '2011-03-14'
     * $query->filterByClosedat('now'); // WHERE closedAt = '2011-03-14'
     * $query->filterByClosedat(array('max' => 'yesterday')); // WHERE closedAt > '2011-03-13'
     * </code>
     *
     * @param     mixed $closedat The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByClosedat($closedat = null, $comparison = null)
    {
        if (is_array($closedat)) {
            $useMinMax = false;
            if (isset($closedat['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::CLOSEDAT, $closedat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($closedat['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::CLOSEDAT, $closedat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::CLOSEDAT, $closedat, $comparison);
    }

    /**
     * Filter the query on the command column
     *
     * Example usage:
     * <code>
     * $query->filterByCommand('fooValue');   // WHERE command = 'fooValue'
     * $query->filterByCommand('%fooValue%'); // WHERE command LIKE '%fooValue%'
     * </code>
     *
     * @param     string $command The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByCommand($command = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($command)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $command)) {
                $command = str_replace('*', '%', $command);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::COMMAND, $command, $comparison);
    }

    /**
     * Filter the query on the args column
     *
     * Example usage:
     * <code>
     * $query->filterByArgs('fooValue');   // WHERE args = 'fooValue'
     * $query->filterByArgs('%fooValue%'); // WHERE args LIKE '%fooValue%'
     * </code>
     *
     * @param     string $args The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByArgs($args = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($args)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $args)) {
                $args = str_replace('*', '%', $args);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::ARGS, $args, $comparison);
    }

    /**
     * Filter the query on the output column
     *
     * Example usage:
     * <code>
     * $query->filterByOutput('fooValue');   // WHERE output = 'fooValue'
     * $query->filterByOutput('%fooValue%'); // WHERE output LIKE '%fooValue%'
     * </code>
     *
     * @param     string $output The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByOutput($output = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($output)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $output)) {
                $output = str_replace('*', '%', $output);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::OUTPUT, $output, $comparison);
    }

    /**
     * Filter the query on the errorOutput column
     *
     * Example usage:
     * <code>
     * $query->filterByErroroutput('fooValue');   // WHERE errorOutput = 'fooValue'
     * $query->filterByErroroutput('%fooValue%'); // WHERE errorOutput LIKE '%fooValue%'
     * </code>
     *
     * @param     string $erroroutput The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByErroroutput($erroroutput = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($erroroutput)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $erroroutput)) {
                $erroroutput = str_replace('*', '%', $erroroutput);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::ERROROUTPUT, $erroroutput, $comparison);
    }

    /**
     * Filter the query on the exitCode column
     *
     * Example usage:
     * <code>
     * $query->filterByExitcode(1234); // WHERE exitCode = 1234
     * $query->filterByExitcode(array(12, 34)); // WHERE exitCode IN (12, 34)
     * $query->filterByExitcode(array('min' => 12)); // WHERE exitCode >= 12
     * $query->filterByExitcode(array('max' => 12)); // WHERE exitCode <= 12
     * </code>
     *
     * @param     mixed $exitcode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByExitcode($exitcode = null, $comparison = null)
    {
        if (is_array($exitcode)) {
            $useMinMax = false;
            if (isset($exitcode['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::EXITCODE, $exitcode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($exitcode['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::EXITCODE, $exitcode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::EXITCODE, $exitcode, $comparison);
    }

    /**
     * Filter the query on the maxRuntime column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxruntime(1234); // WHERE maxRuntime = 1234
     * $query->filterByMaxruntime(array(12, 34)); // WHERE maxRuntime IN (12, 34)
     * $query->filterByMaxruntime(array('min' => 12)); // WHERE maxRuntime >= 12
     * $query->filterByMaxruntime(array('max' => 12)); // WHERE maxRuntime <= 12
     * </code>
     *
     * @param     mixed $maxruntime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByMaxruntime($maxruntime = null, $comparison = null)
    {
        if (is_array($maxruntime)) {
            $useMinMax = false;
            if (isset($maxruntime['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::MAXRUNTIME, $maxruntime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxruntime['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::MAXRUNTIME, $maxruntime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::MAXRUNTIME, $maxruntime, $comparison);
    }

    /**
     * Filter the query on the maxRetries column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxretries(1234); // WHERE maxRetries = 1234
     * $query->filterByMaxretries(array(12, 34)); // WHERE maxRetries IN (12, 34)
     * $query->filterByMaxretries(array('min' => 12)); // WHERE maxRetries >= 12
     * $query->filterByMaxretries(array('max' => 12)); // WHERE maxRetries <= 12
     * </code>
     *
     * @param     mixed $maxretries The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByMaxretries($maxretries = null, $comparison = null)
    {
        if (is_array($maxretries)) {
            $useMinMax = false;
            if (isset($maxretries['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::MAXRETRIES, $maxretries['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxretries['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::MAXRETRIES, $maxretries['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::MAXRETRIES, $maxretries, $comparison);
    }

    /**
     * Filter the query on the stackTrace column
     *
     * @param     mixed $stacktrace The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByStacktrace($stacktrace = null, $comparison = null)
    {

        return $this->addUsingAlias(CommonJmsJobsPeer::STACKTRACE, $stacktrace, $comparison);
    }

    /**
     * Filter the query on the runtime column
     *
     * Example usage:
     * <code>
     * $query->filterByRuntime(1234); // WHERE runtime = 1234
     * $query->filterByRuntime(array(12, 34)); // WHERE runtime IN (12, 34)
     * $query->filterByRuntime(array('min' => 12)); // WHERE runtime >= 12
     * $query->filterByRuntime(array('max' => 12)); // WHERE runtime <= 12
     * </code>
     *
     * @param     mixed $runtime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByRuntime($runtime = null, $comparison = null)
    {
        if (is_array($runtime)) {
            $useMinMax = false;
            if (isset($runtime['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::RUNTIME, $runtime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($runtime['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::RUNTIME, $runtime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::RUNTIME, $runtime, $comparison);
    }

    /**
     * Filter the query on the memoryUsage column
     *
     * Example usage:
     * <code>
     * $query->filterByMemoryusage(1234); // WHERE memoryUsage = 1234
     * $query->filterByMemoryusage(array(12, 34)); // WHERE memoryUsage IN (12, 34)
     * $query->filterByMemoryusage(array('min' => 12)); // WHERE memoryUsage >= 12
     * $query->filterByMemoryusage(array('max' => 12)); // WHERE memoryUsage <= 12
     * </code>
     *
     * @param     mixed $memoryusage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByMemoryusage($memoryusage = null, $comparison = null)
    {
        if (is_array($memoryusage)) {
            $useMinMax = false;
            if (isset($memoryusage['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::MEMORYUSAGE, $memoryusage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($memoryusage['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::MEMORYUSAGE, $memoryusage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::MEMORYUSAGE, $memoryusage, $comparison);
    }

    /**
     * Filter the query on the memoryUsageReal column
     *
     * Example usage:
     * <code>
     * $query->filterByMemoryusagereal(1234); // WHERE memoryUsageReal = 1234
     * $query->filterByMemoryusagereal(array(12, 34)); // WHERE memoryUsageReal IN (12, 34)
     * $query->filterByMemoryusagereal(array('min' => 12)); // WHERE memoryUsageReal >= 12
     * $query->filterByMemoryusagereal(array('max' => 12)); // WHERE memoryUsageReal <= 12
     * </code>
     *
     * @param     mixed $memoryusagereal The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByMemoryusagereal($memoryusagereal = null, $comparison = null)
    {
        if (is_array($memoryusagereal)) {
            $useMinMax = false;
            if (isset($memoryusagereal['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::MEMORYUSAGEREAL, $memoryusagereal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($memoryusagereal['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::MEMORYUSAGEREAL, $memoryusagereal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::MEMORYUSAGEREAL, $memoryusagereal, $comparison);
    }

    /**
     * Filter the query on the originalJob_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginaljobId(1234); // WHERE originalJob_id = 1234
     * $query->filterByOriginaljobId(array(12, 34)); // WHERE originalJob_id IN (12, 34)
     * $query->filterByOriginaljobId(array('min' => 12)); // WHERE originalJob_id >= 12
     * $query->filterByOriginaljobId(array('max' => 12)); // WHERE originalJob_id <= 12
     * </code>
     *
     * @see       filterByCommonJmsJobsRelatedByOriginaljobId()
     *
     * @param     mixed $originaljobId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function filterByOriginaljobId($originaljobId = null, $comparison = null)
    {
        if (is_array($originaljobId)) {
            $useMinMax = false;
            if (isset($originaljobId['min'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::ORIGINALJOB_ID, $originaljobId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($originaljobId['max'])) {
                $this->addUsingAlias(CommonJmsJobsPeer::ORIGINALJOB_ID, $originaljobId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobsPeer::ORIGINALJOB_ID, $originaljobId, $comparison);
    }

    /**
     * Filter the query by a related CommonJmsJobs object
     *
     * @param   CommonJmsJobs|PropelObjectCollection $commonJmsJobs The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonJmsJobsQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonJmsJobsRelatedByOriginaljobId($commonJmsJobs, $comparison = null)
    {
        if ($commonJmsJobs instanceof CommonJmsJobs) {
            return $this
                ->addUsingAlias(CommonJmsJobsPeer::ORIGINALJOB_ID, $commonJmsJobs->getId(), $comparison);
        } elseif ($commonJmsJobs instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonJmsJobsPeer::ORIGINALJOB_ID, $commonJmsJobs->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonJmsJobsRelatedByOriginaljobId() only accepts arguments of type CommonJmsJobs or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonJmsJobsRelatedByOriginaljobId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function joinCommonJmsJobsRelatedByOriginaljobId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonJmsJobsRelatedByOriginaljobId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonJmsJobsRelatedByOriginaljobId');
        }

        return $this;
    }

    /**
     * Use the CommonJmsJobsRelatedByOriginaljobId relation CommonJmsJobs object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonJmsJobsQuery A secondary query class using the current class as primary query
     */
    public function useCommonJmsJobsRelatedByOriginaljobIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonJmsJobsRelatedByOriginaljobId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonJmsJobsRelatedByOriginaljobId', '\Application\Propel\Mpe\CommonJmsJobsQuery');
    }

    /**
     * Filter the query by a related CommonJmsJobDependencies object
     *
     * @param   CommonJmsJobDependencies|PropelObjectCollection $commonJmsJobDependencies  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonJmsJobsQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonJmsJobDependenciesRelatedByDestJobId($commonJmsJobDependencies, $comparison = null)
    {
        if ($commonJmsJobDependencies instanceof CommonJmsJobDependencies) {
            return $this
                ->addUsingAlias(CommonJmsJobsPeer::ID, $commonJmsJobDependencies->getDestJobId(), $comparison);
        } elseif ($commonJmsJobDependencies instanceof PropelObjectCollection) {
            return $this
                ->useCommonJmsJobDependenciesRelatedByDestJobIdQuery()
                ->filterByPrimaryKeys($commonJmsJobDependencies->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonJmsJobDependenciesRelatedByDestJobId() only accepts arguments of type CommonJmsJobDependencies or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonJmsJobDependenciesRelatedByDestJobId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function joinCommonJmsJobDependenciesRelatedByDestJobId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonJmsJobDependenciesRelatedByDestJobId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonJmsJobDependenciesRelatedByDestJobId');
        }

        return $this;
    }

    /**
     * Use the CommonJmsJobDependenciesRelatedByDestJobId relation CommonJmsJobDependencies object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonJmsJobDependenciesQuery A secondary query class using the current class as primary query
     */
    public function useCommonJmsJobDependenciesRelatedByDestJobIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonJmsJobDependenciesRelatedByDestJobId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonJmsJobDependenciesRelatedByDestJobId', '\Application\Propel\Mpe\CommonJmsJobDependenciesQuery');
    }

    /**
     * Filter the query by a related CommonJmsJobDependencies object
     *
     * @param   CommonJmsJobDependencies|PropelObjectCollection $commonJmsJobDependencies  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonJmsJobsQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonJmsJobDependenciesRelatedBySourceJobId($commonJmsJobDependencies, $comparison = null)
    {
        if ($commonJmsJobDependencies instanceof CommonJmsJobDependencies) {
            return $this
                ->addUsingAlias(CommonJmsJobsPeer::ID, $commonJmsJobDependencies->getSourceJobId(), $comparison);
        } elseif ($commonJmsJobDependencies instanceof PropelObjectCollection) {
            return $this
                ->useCommonJmsJobDependenciesRelatedBySourceJobIdQuery()
                ->filterByPrimaryKeys($commonJmsJobDependencies->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonJmsJobDependenciesRelatedBySourceJobId() only accepts arguments of type CommonJmsJobDependencies or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonJmsJobDependenciesRelatedBySourceJobId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function joinCommonJmsJobDependenciesRelatedBySourceJobId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonJmsJobDependenciesRelatedBySourceJobId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonJmsJobDependenciesRelatedBySourceJobId');
        }

        return $this;
    }

    /**
     * Use the CommonJmsJobDependenciesRelatedBySourceJobId relation CommonJmsJobDependencies object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonJmsJobDependenciesQuery A secondary query class using the current class as primary query
     */
    public function useCommonJmsJobDependenciesRelatedBySourceJobIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonJmsJobDependenciesRelatedBySourceJobId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonJmsJobDependenciesRelatedBySourceJobId', '\Application\Propel\Mpe\CommonJmsJobDependenciesQuery');
    }

    /**
     * Filter the query by a related CommonJmsJobRelatedEntities object
     *
     * @param   CommonJmsJobRelatedEntities|PropelObjectCollection $commonJmsJobRelatedEntities  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonJmsJobsQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonJmsJobRelatedEntities($commonJmsJobRelatedEntities, $comparison = null)
    {
        if ($commonJmsJobRelatedEntities instanceof CommonJmsJobRelatedEntities) {
            return $this
                ->addUsingAlias(CommonJmsJobsPeer::ID, $commonJmsJobRelatedEntities->getJobId(), $comparison);
        } elseif ($commonJmsJobRelatedEntities instanceof PropelObjectCollection) {
            return $this
                ->useCommonJmsJobRelatedEntitiesQuery()
                ->filterByPrimaryKeys($commonJmsJobRelatedEntities->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonJmsJobRelatedEntities() only accepts arguments of type CommonJmsJobRelatedEntities or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonJmsJobRelatedEntities relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function joinCommonJmsJobRelatedEntities($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonJmsJobRelatedEntities');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonJmsJobRelatedEntities');
        }

        return $this;
    }

    /**
     * Use the CommonJmsJobRelatedEntities relation CommonJmsJobRelatedEntities object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonJmsJobRelatedEntitiesQuery A secondary query class using the current class as primary query
     */
    public function useCommonJmsJobRelatedEntitiesQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonJmsJobRelatedEntities($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonJmsJobRelatedEntities', '\Application\Propel\Mpe\CommonJmsJobRelatedEntitiesQuery');
    }

    /**
     * Filter the query by a related CommonJmsJobs object
     *
     * @param   CommonJmsJobs|PropelObjectCollection $commonJmsJobs  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonJmsJobsQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonJmsJobsRelatedById($commonJmsJobs, $comparison = null)
    {
        if ($commonJmsJobs instanceof CommonJmsJobs) {
            return $this
                ->addUsingAlias(CommonJmsJobsPeer::ID, $commonJmsJobs->getOriginaljobId(), $comparison);
        } elseif ($commonJmsJobs instanceof PropelObjectCollection) {
            return $this
                ->useCommonJmsJobsRelatedByIdQuery()
                ->filterByPrimaryKeys($commonJmsJobs->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonJmsJobsRelatedById() only accepts arguments of type CommonJmsJobs or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonJmsJobsRelatedById relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function joinCommonJmsJobsRelatedById($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonJmsJobsRelatedById');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonJmsJobsRelatedById');
        }

        return $this;
    }

    /**
     * Use the CommonJmsJobsRelatedById relation CommonJmsJobs object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonJmsJobsQuery A secondary query class using the current class as primary query
     */
    public function useCommonJmsJobsRelatedByIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonJmsJobsRelatedById($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonJmsJobsRelatedById', '\Application\Propel\Mpe\CommonJmsJobsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonJmsJobs $commonJmsJobs Object to remove from the list of results
     *
     * @return CommonJmsJobsQuery The current query, for fluid interface
     */
    public function prune($commonJmsJobs = null)
    {
        if ($commonJmsJobs) {
            $this->addUsingAlias(CommonJmsJobsPeer::ID, $commonJmsJobs->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
