<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonTCAOIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourIntervenant;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourIntervenantPeer;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourIntervenantQuery;

/**
 * Base class that represents a query for the 't_CAO_Ordre_Du_Jour_Intervenant' table.
 *
 *
 *
 * @method CommonTCAOOrdreDuJourIntervenantQuery orderByIdOrdreDuJourIntervenant($order = Criteria::ASC) Order by the id_ordre_du_jour_intervenant column
 * @method CommonTCAOOrdreDuJourIntervenantQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTCAOOrdreDuJourIntervenantQuery orderByIdOrdreDePassage($order = Criteria::ASC) Order by the id_ordre_de_passage column
 * @method CommonTCAOOrdreDuJourIntervenantQuery orderByIdIntervenantExterne($order = Criteria::ASC) Order by the id_intervenant_externe column
 * @method CommonTCAOOrdreDuJourIntervenantQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 * @method CommonTCAOOrdreDuJourIntervenantQuery orderByIdRefValTypeVoix($order = Criteria::ASC) Order by the id_ref_val_type_voix column
 *
 * @method CommonTCAOOrdreDuJourIntervenantQuery groupByIdOrdreDuJourIntervenant() Group by the id_ordre_du_jour_intervenant column
 * @method CommonTCAOOrdreDuJourIntervenantQuery groupByOrganisme() Group by the organisme column
 * @method CommonTCAOOrdreDuJourIntervenantQuery groupByIdOrdreDePassage() Group by the id_ordre_de_passage column
 * @method CommonTCAOOrdreDuJourIntervenantQuery groupByIdIntervenantExterne() Group by the id_intervenant_externe column
 * @method CommonTCAOOrdreDuJourIntervenantQuery groupByIdAgent() Group by the id_agent column
 * @method CommonTCAOOrdreDuJourIntervenantQuery groupByIdRefValTypeVoix() Group by the id_ref_val_type_voix column
 *
 * @method CommonTCAOOrdreDuJourIntervenantQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTCAOOrdreDuJourIntervenantQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTCAOOrdreDuJourIntervenantQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTCAOOrdreDuJourIntervenantQuery leftJoinCommonAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAgent relation
 * @method CommonTCAOOrdreDuJourIntervenantQuery rightJoinCommonAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAgent relation
 * @method CommonTCAOOrdreDuJourIntervenantQuery innerJoinCommonAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAgent relation
 *
 * @method CommonTCAOOrdreDuJourIntervenantQuery leftJoinCommonTCAOIntervenantExterne($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCAOIntervenantExterne relation
 * @method CommonTCAOOrdreDuJourIntervenantQuery rightJoinCommonTCAOIntervenantExterne($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCAOIntervenantExterne relation
 * @method CommonTCAOOrdreDuJourIntervenantQuery innerJoinCommonTCAOIntervenantExterne($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCAOIntervenantExterne relation
 *
 * @method CommonTCAOOrdreDuJourIntervenantQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonTCAOOrdreDuJourIntervenantQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonTCAOOrdreDuJourIntervenantQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonTCAOOrdreDuJourIntervenant findOne(PropelPDO $con = null) Return the first CommonTCAOOrdreDuJourIntervenant matching the query
 * @method CommonTCAOOrdreDuJourIntervenant findOneOrCreate(PropelPDO $con = null) Return the first CommonTCAOOrdreDuJourIntervenant matching the query, or a new CommonTCAOOrdreDuJourIntervenant object populated from the query conditions when no match is found
 *
 * @method CommonTCAOOrdreDuJourIntervenant findOneByIdOrdreDuJourIntervenant(string $id_ordre_du_jour_intervenant) Return the first CommonTCAOOrdreDuJourIntervenant filtered by the id_ordre_du_jour_intervenant column
 * @method CommonTCAOOrdreDuJourIntervenant findOneByOrganisme(string $organisme) Return the first CommonTCAOOrdreDuJourIntervenant filtered by the organisme column
 * @method CommonTCAOOrdreDuJourIntervenant findOneByIdOrdreDePassage(string $id_ordre_de_passage) Return the first CommonTCAOOrdreDuJourIntervenant filtered by the id_ordre_de_passage column
 * @method CommonTCAOOrdreDuJourIntervenant findOneByIdIntervenantExterne(string $id_intervenant_externe) Return the first CommonTCAOOrdreDuJourIntervenant filtered by the id_intervenant_externe column
 * @method CommonTCAOOrdreDuJourIntervenant findOneByIdAgent(int $id_agent) Return the first CommonTCAOOrdreDuJourIntervenant filtered by the id_agent column
 * @method CommonTCAOOrdreDuJourIntervenant findOneByIdRefValTypeVoix(int $id_ref_val_type_voix) Return the first CommonTCAOOrdreDuJourIntervenant filtered by the id_ref_val_type_voix column
 *
 * @method array findByIdOrdreDuJourIntervenant(string $id_ordre_du_jour_intervenant) Return CommonTCAOOrdreDuJourIntervenant objects filtered by the id_ordre_du_jour_intervenant column
 * @method array findByOrganisme(string $organisme) Return CommonTCAOOrdreDuJourIntervenant objects filtered by the organisme column
 * @method array findByIdOrdreDePassage(string $id_ordre_de_passage) Return CommonTCAOOrdreDuJourIntervenant objects filtered by the id_ordre_de_passage column
 * @method array findByIdIntervenantExterne(string $id_intervenant_externe) Return CommonTCAOOrdreDuJourIntervenant objects filtered by the id_intervenant_externe column
 * @method array findByIdAgent(int $id_agent) Return CommonTCAOOrdreDuJourIntervenant objects filtered by the id_agent column
 * @method array findByIdRefValTypeVoix(int $id_ref_val_type_voix) Return CommonTCAOOrdreDuJourIntervenant objects filtered by the id_ref_val_type_voix column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTCAOOrdreDuJourIntervenantQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTCAOOrdreDuJourIntervenantQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTCAOOrdreDuJourIntervenant', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTCAOOrdreDuJourIntervenantQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTCAOOrdreDuJourIntervenantQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTCAOOrdreDuJourIntervenantQuery) {
            return $criteria;
        }
        $query = new CommonTCAOOrdreDuJourIntervenantQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$id_ordre_du_jour_intervenant, $organisme]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTCAOOrdreDuJourIntervenant|CommonTCAOOrdreDuJourIntervenant[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTCAOOrdreDuJourIntervenantPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTCAOOrdreDuJourIntervenantPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTCAOOrdreDuJourIntervenant A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_ordre_du_jour_intervenant`, `organisme`, `id_ordre_de_passage`, `id_intervenant_externe`, `id_agent`, `id_ref_val_type_voix` FROM `t_CAO_Ordre_Du_Jour_Intervenant` WHERE `id_ordre_du_jour_intervenant` = :p0 AND `organisme` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTCAOOrdreDuJourIntervenant();
            $obj->hydrate($row);
            CommonTCAOOrdreDuJourIntervenantPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTCAOOrdreDuJourIntervenant|CommonTCAOOrdreDuJourIntervenant[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTCAOOrdreDuJourIntervenant[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_ORDRE_DU_JOUR_INTERVENANT, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ORGANISME, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(CommonTCAOOrdreDuJourIntervenantPeer::ID_ORDRE_DU_JOUR_INTERVENANT, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(CommonTCAOOrdreDuJourIntervenantPeer::ORGANISME, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the id_ordre_du_jour_intervenant column
     *
     * Example usage:
     * <code>
     * $query->filterByIdOrdreDuJourIntervenant(1234); // WHERE id_ordre_du_jour_intervenant = 1234
     * $query->filterByIdOrdreDuJourIntervenant(array(12, 34)); // WHERE id_ordre_du_jour_intervenant IN (12, 34)
     * $query->filterByIdOrdreDuJourIntervenant(array('min' => 12)); // WHERE id_ordre_du_jour_intervenant >= 12
     * $query->filterByIdOrdreDuJourIntervenant(array('max' => 12)); // WHERE id_ordre_du_jour_intervenant <= 12
     * </code>
     *
     * @param     mixed $idOrdreDuJourIntervenant The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function filterByIdOrdreDuJourIntervenant($idOrdreDuJourIntervenant = null, $comparison = null)
    {
        if (is_array($idOrdreDuJourIntervenant)) {
            $useMinMax = false;
            if (isset($idOrdreDuJourIntervenant['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_ORDRE_DU_JOUR_INTERVENANT, $idOrdreDuJourIntervenant['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idOrdreDuJourIntervenant['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_ORDRE_DU_JOUR_INTERVENANT, $idOrdreDuJourIntervenant['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_ORDRE_DU_JOUR_INTERVENANT, $idOrdreDuJourIntervenant, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the id_ordre_de_passage column
     *
     * Example usage:
     * <code>
     * $query->filterByIdOrdreDePassage(1234); // WHERE id_ordre_de_passage = 1234
     * $query->filterByIdOrdreDePassage(array(12, 34)); // WHERE id_ordre_de_passage IN (12, 34)
     * $query->filterByIdOrdreDePassage(array('min' => 12)); // WHERE id_ordre_de_passage >= 12
     * $query->filterByIdOrdreDePassage(array('max' => 12)); // WHERE id_ordre_de_passage <= 12
     * </code>
     *
     * @param     mixed $idOrdreDePassage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function filterByIdOrdreDePassage($idOrdreDePassage = null, $comparison = null)
    {
        if (is_array($idOrdreDePassage)) {
            $useMinMax = false;
            if (isset($idOrdreDePassage['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_ORDRE_DE_PASSAGE, $idOrdreDePassage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idOrdreDePassage['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_ORDRE_DE_PASSAGE, $idOrdreDePassage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_ORDRE_DE_PASSAGE, $idOrdreDePassage, $comparison);
    }

    /**
     * Filter the query on the id_intervenant_externe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdIntervenantExterne(1234); // WHERE id_intervenant_externe = 1234
     * $query->filterByIdIntervenantExterne(array(12, 34)); // WHERE id_intervenant_externe IN (12, 34)
     * $query->filterByIdIntervenantExterne(array('min' => 12)); // WHERE id_intervenant_externe >= 12
     * $query->filterByIdIntervenantExterne(array('max' => 12)); // WHERE id_intervenant_externe <= 12
     * </code>
     *
     * @see       filterByCommonTCAOIntervenantExterne()
     *
     * @param     mixed $idIntervenantExterne The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function filterByIdIntervenantExterne($idIntervenantExterne = null, $comparison = null)
    {
        if (is_array($idIntervenantExterne)) {
            $useMinMax = false;
            if (isset($idIntervenantExterne['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_INTERVENANT_EXTERNE, $idIntervenantExterne['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idIntervenantExterne['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_INTERVENANT_EXTERNE, $idIntervenantExterne['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_INTERVENANT_EXTERNE, $idIntervenantExterne, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @see       filterByCommonAgent()
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Filter the query on the id_ref_val_type_voix column
     *
     * Example usage:
     * <code>
     * $query->filterByIdRefValTypeVoix(1234); // WHERE id_ref_val_type_voix = 1234
     * $query->filterByIdRefValTypeVoix(array(12, 34)); // WHERE id_ref_val_type_voix IN (12, 34)
     * $query->filterByIdRefValTypeVoix(array('min' => 12)); // WHERE id_ref_val_type_voix >= 12
     * $query->filterByIdRefValTypeVoix(array('max' => 12)); // WHERE id_ref_val_type_voix <= 12
     * </code>
     *
     * @param     mixed $idRefValTypeVoix The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function filterByIdRefValTypeVoix($idRefValTypeVoix = null, $comparison = null)
    {
        if (is_array($idRefValTypeVoix)) {
            $useMinMax = false;
            if (isset($idRefValTypeVoix['min'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_REF_VAL_TYPE_VOIX, $idRefValTypeVoix['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idRefValTypeVoix['max'])) {
                $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_REF_VAL_TYPE_VOIX, $idRefValTypeVoix['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_REF_VAL_TYPE_VOIX, $idRefValTypeVoix, $comparison);
    }

    /**
     * Filter the query by a related CommonAgent object
     *
     * @param   CommonAgent|PropelObjectCollection $commonAgent The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAgent($commonAgent, $comparison = null)
    {
        if ($commonAgent instanceof CommonAgent) {
            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_AGENT, $commonAgent->getId(), $comparison);
        } elseif ($commonAgent instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_AGENT, $commonAgent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonAgent() only accepts arguments of type CommonAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function joinCommonAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAgent');
        }

        return $this;
    }

    /**
     * Use the CommonAgent relation CommonAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAgent', '\Application\Propel\Mpe\CommonAgentQuery');
    }

    /**
     * Filter the query by a related CommonTCAOIntervenantExterne object
     *
     * @param   CommonTCAOIntervenantExterne|PropelObjectCollection $commonTCAOIntervenantExterne The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCAOIntervenantExterne($commonTCAOIntervenantExterne, $comparison = null)
    {
        if ($commonTCAOIntervenantExterne instanceof CommonTCAOIntervenantExterne) {
            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_INTERVENANT_EXTERNE, $commonTCAOIntervenantExterne->getIdIntervenantExterne(), $comparison);
        } elseif ($commonTCAOIntervenantExterne instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ID_INTERVENANT_EXTERNE, $commonTCAOIntervenantExterne->toKeyValue('IdIntervenantExterne', 'IdIntervenantExterne'), $comparison);
        } else {
            throw new PropelException('filterByCommonTCAOIntervenantExterne() only accepts arguments of type CommonTCAOIntervenantExterne or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCAOIntervenantExterne relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function joinCommonTCAOIntervenantExterne($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCAOIntervenantExterne');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCAOIntervenantExterne');
        }

        return $this;
    }

    /**
     * Use the CommonTCAOIntervenantExterne relation CommonTCAOIntervenantExterne object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCAOIntervenantExterneQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCAOIntervenantExterneQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTCAOIntervenantExterne($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCAOIntervenantExterne', '\Application\Propel\Mpe\CommonTCAOIntervenantExterneQuery');
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ORGANISME, $commonOrganisme->getAcronyme(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTCAOOrdreDuJourIntervenantPeer::ORGANISME, $commonOrganisme->toKeyValue('PrimaryKey', 'Acronyme'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTCAOOrdreDuJourIntervenant $commonTCAOOrdreDuJourIntervenant Object to remove from the list of results
     *
     * @return CommonTCAOOrdreDuJourIntervenantQuery The current query, for fluid interface
     */
    public function prune($commonTCAOOrdreDuJourIntervenant = null)
    {
        if ($commonTCAOOrdreDuJourIntervenant) {
            $this->addCond('pruneCond0', $this->getAliasedColName(CommonTCAOOrdreDuJourIntervenantPeer::ID_ORDRE_DU_JOUR_INTERVENANT), $commonTCAOOrdreDuJourIntervenant->getIdOrdreDuJourIntervenant(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(CommonTCAOOrdreDuJourIntervenantPeer::ORGANISME), $commonTCAOOrdreDuJourIntervenant->getOrganisme(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
