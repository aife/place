<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTNumeroReponse;
use Application\Propel\Mpe\CommonTNumeroReponsePeer;
use Application\Propel\Mpe\CommonTNumeroReponseQuery;

/**
 * Base class that represents a query for the 't_numero_reponse' table.
 *
 *
 *
 * @method CommonTNumeroReponseQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTNumeroReponseQuery orderByConsultationRef($order = Criteria::ASC) Order by the consultation_ref column
 * @method CommonTNumeroReponseQuery orderByNumeroReponse($order = Criteria::ASC) Order by the numero_reponse column
 * @method CommonTNumeroReponseQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTNumeroReponseQuery orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 *
 * @method CommonTNumeroReponseQuery groupById() Group by the id column
 * @method CommonTNumeroReponseQuery groupByConsultationRef() Group by the consultation_ref column
 * @method CommonTNumeroReponseQuery groupByNumeroReponse() Group by the numero_reponse column
 * @method CommonTNumeroReponseQuery groupByOrganisme() Group by the organisme column
 * @method CommonTNumeroReponseQuery groupByConsultationId() Group by the consultation_id column
 *
 * @method CommonTNumeroReponseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTNumeroReponseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTNumeroReponseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTNumeroReponse findOne(PropelPDO $con = null) Return the first CommonTNumeroReponse matching the query
 * @method CommonTNumeroReponse findOneOrCreate(PropelPDO $con = null) Return the first CommonTNumeroReponse matching the query, or a new CommonTNumeroReponse object populated from the query conditions when no match is found
 *
 * @method CommonTNumeroReponse findOneByConsultationRef(int $consultation_ref) Return the first CommonTNumeroReponse filtered by the consultation_ref column
 * @method CommonTNumeroReponse findOneByNumeroReponse(int $numero_reponse) Return the first CommonTNumeroReponse filtered by the numero_reponse column
 * @method CommonTNumeroReponse findOneByOrganisme(string $organisme) Return the first CommonTNumeroReponse filtered by the organisme column
 * @method CommonTNumeroReponse findOneByConsultationId(int $consultation_id) Return the first CommonTNumeroReponse filtered by the consultation_id column
 *
 * @method array findById(int $id) Return CommonTNumeroReponse objects filtered by the id column
 * @method array findByConsultationRef(int $consultation_ref) Return CommonTNumeroReponse objects filtered by the consultation_ref column
 * @method array findByNumeroReponse(int $numero_reponse) Return CommonTNumeroReponse objects filtered by the numero_reponse column
 * @method array findByOrganisme(string $organisme) Return CommonTNumeroReponse objects filtered by the organisme column
 * @method array findByConsultationId(int $consultation_id) Return CommonTNumeroReponse objects filtered by the consultation_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTNumeroReponseQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTNumeroReponseQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTNumeroReponse', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTNumeroReponseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTNumeroReponseQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTNumeroReponseQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTNumeroReponseQuery) {
            return $criteria;
        }
        $query = new CommonTNumeroReponseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTNumeroReponse|CommonTNumeroReponse[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTNumeroReponsePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTNumeroReponsePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTNumeroReponse A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTNumeroReponse A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `consultation_ref`, `numero_reponse`, `organisme`, `consultation_id` FROM `t_numero_reponse` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTNumeroReponse();
            $obj->hydrate($row);
            CommonTNumeroReponsePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTNumeroReponse|CommonTNumeroReponse[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTNumeroReponse[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTNumeroReponseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTNumeroReponsePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTNumeroReponseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTNumeroReponsePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumeroReponseQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTNumeroReponsePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTNumeroReponsePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNumeroReponsePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the consultation_ref column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationRef(1234); // WHERE consultation_ref = 1234
     * $query->filterByConsultationRef(array(12, 34)); // WHERE consultation_ref IN (12, 34)
     * $query->filterByConsultationRef(array('min' => 12)); // WHERE consultation_ref >= 12
     * $query->filterByConsultationRef(array('max' => 12)); // WHERE consultation_ref <= 12
     * </code>
     *
     * @param     mixed $consultationRef The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumeroReponseQuery The current query, for fluid interface
     */
    public function filterByConsultationRef($consultationRef = null, $comparison = null)
    {
        if (is_array($consultationRef)) {
            $useMinMax = false;
            if (isset($consultationRef['min'])) {
                $this->addUsingAlias(CommonTNumeroReponsePeer::CONSULTATION_REF, $consultationRef['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationRef['max'])) {
                $this->addUsingAlias(CommonTNumeroReponsePeer::CONSULTATION_REF, $consultationRef['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNumeroReponsePeer::CONSULTATION_REF, $consultationRef, $comparison);
    }

    /**
     * Filter the query on the numero_reponse column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroReponse(1234); // WHERE numero_reponse = 1234
     * $query->filterByNumeroReponse(array(12, 34)); // WHERE numero_reponse IN (12, 34)
     * $query->filterByNumeroReponse(array('min' => 12)); // WHERE numero_reponse >= 12
     * $query->filterByNumeroReponse(array('max' => 12)); // WHERE numero_reponse <= 12
     * </code>
     *
     * @param     mixed $numeroReponse The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumeroReponseQuery The current query, for fluid interface
     */
    public function filterByNumeroReponse($numeroReponse = null, $comparison = null)
    {
        if (is_array($numeroReponse)) {
            $useMinMax = false;
            if (isset($numeroReponse['min'])) {
                $this->addUsingAlias(CommonTNumeroReponsePeer::NUMERO_REPONSE, $numeroReponse['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numeroReponse['max'])) {
                $this->addUsingAlias(CommonTNumeroReponsePeer::NUMERO_REPONSE, $numeroReponse['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNumeroReponsePeer::NUMERO_REPONSE, $numeroReponse, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumeroReponseQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNumeroReponsePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumeroReponseQuery The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonTNumeroReponsePeer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonTNumeroReponsePeer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNumeroReponsePeer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTNumeroReponse $commonTNumeroReponse Object to remove from the list of results
     *
     * @return CommonTNumeroReponseQuery The current query, for fluid interface
     */
    public function prune($commonTNumeroReponse = null)
    {
        if ($commonTNumeroReponse) {
            $this->addUsingAlias(CommonTNumeroReponsePeer::ID, $commonTNumeroReponse->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
