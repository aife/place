<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTNotificationAgent;
use Application\Propel\Mpe\CommonTNotificationAgentPeer;
use Application\Propel\Mpe\CommonTNotificationAgentQuery;
use Application\Propel\Mpe\CommonTTypeNotificationAgent;

/**
 * Base class that represents a query for the 't_notification_agent' table.
 *
 *
 *
 * @method CommonTNotificationAgentQuery orderByIdNotification($order = Criteria::ASC) Order by the id_notification column
 * @method CommonTNotificationAgentQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 * @method CommonTNotificationAgentQuery orderByIdType($order = Criteria::ASC) Order by the id_type column
 * @method CommonTNotificationAgentQuery orderByServiceId($order = Criteria::ASC) Order by the service_id column
 * @method CommonTNotificationAgentQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTNotificationAgentQuery orderByReferenceObjet($order = Criteria::ASC) Order by the reference_objet column
 * @method CommonTNotificationAgentQuery orderByLibelleNotification($order = Criteria::ASC) Order by the libelle_notification column
 * @method CommonTNotificationAgentQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method CommonTNotificationAgentQuery orderByNotificationLue($order = Criteria::ASC) Order by the notification_lue column
 * @method CommonTNotificationAgentQuery orderByNotificationActif($order = Criteria::ASC) Order by the notification_actif column
 * @method CommonTNotificationAgentQuery orderByUrlNotification($order = Criteria::ASC) Order by the url_notification column
 * @method CommonTNotificationAgentQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTNotificationAgentQuery orderByDateLecture($order = Criteria::ASC) Order by the date_lecture column
 *
 * @method CommonTNotificationAgentQuery groupByIdNotification() Group by the id_notification column
 * @method CommonTNotificationAgentQuery groupByIdAgent() Group by the id_agent column
 * @method CommonTNotificationAgentQuery groupByIdType() Group by the id_type column
 * @method CommonTNotificationAgentQuery groupByServiceId() Group by the service_id column
 * @method CommonTNotificationAgentQuery groupByOrganisme() Group by the organisme column
 * @method CommonTNotificationAgentQuery groupByReferenceObjet() Group by the reference_objet column
 * @method CommonTNotificationAgentQuery groupByLibelleNotification() Group by the libelle_notification column
 * @method CommonTNotificationAgentQuery groupByDescription() Group by the description column
 * @method CommonTNotificationAgentQuery groupByNotificationLue() Group by the notification_lue column
 * @method CommonTNotificationAgentQuery groupByNotificationActif() Group by the notification_actif column
 * @method CommonTNotificationAgentQuery groupByUrlNotification() Group by the url_notification column
 * @method CommonTNotificationAgentQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTNotificationAgentQuery groupByDateLecture() Group by the date_lecture column
 *
 * @method CommonTNotificationAgentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTNotificationAgentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTNotificationAgentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTNotificationAgentQuery leftJoinCommonTTypeNotificationAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTTypeNotificationAgent relation
 * @method CommonTNotificationAgentQuery rightJoinCommonTTypeNotificationAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTTypeNotificationAgent relation
 * @method CommonTNotificationAgentQuery innerJoinCommonTTypeNotificationAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTTypeNotificationAgent relation
 *
 * @method CommonTNotificationAgent findOne(PropelPDO $con = null) Return the first CommonTNotificationAgent matching the query
 * @method CommonTNotificationAgent findOneOrCreate(PropelPDO $con = null) Return the first CommonTNotificationAgent matching the query, or a new CommonTNotificationAgent object populated from the query conditions when no match is found
 *
 * @method CommonTNotificationAgent findOneByIdAgent(int $id_agent) Return the first CommonTNotificationAgent filtered by the id_agent column
 * @method CommonTNotificationAgent findOneByIdType(int $id_type) Return the first CommonTNotificationAgent filtered by the id_type column
 * @method CommonTNotificationAgent findOneByServiceId(int $service_id) Return the first CommonTNotificationAgent filtered by the service_id column
 * @method CommonTNotificationAgent findOneByOrganisme(string $organisme) Return the first CommonTNotificationAgent filtered by the organisme column
 * @method CommonTNotificationAgent findOneByReferenceObjet(string $reference_objet) Return the first CommonTNotificationAgent filtered by the reference_objet column
 * @method CommonTNotificationAgent findOneByLibelleNotification(string $libelle_notification) Return the first CommonTNotificationAgent filtered by the libelle_notification column
 * @method CommonTNotificationAgent findOneByDescription(string $description) Return the first CommonTNotificationAgent filtered by the description column
 * @method CommonTNotificationAgent findOneByNotificationLue(string $notification_lue) Return the first CommonTNotificationAgent filtered by the notification_lue column
 * @method CommonTNotificationAgent findOneByNotificationActif(string $notification_actif) Return the first CommonTNotificationAgent filtered by the notification_actif column
 * @method CommonTNotificationAgent findOneByUrlNotification(string $url_notification) Return the first CommonTNotificationAgent filtered by the url_notification column
 * @method CommonTNotificationAgent findOneByDateCreation(string $date_creation) Return the first CommonTNotificationAgent filtered by the date_creation column
 * @method CommonTNotificationAgent findOneByDateLecture(string $date_lecture) Return the first CommonTNotificationAgent filtered by the date_lecture column
 *
 * @method array findByIdNotification(int $id_notification) Return CommonTNotificationAgent objects filtered by the id_notification column
 * @method array findByIdAgent(int $id_agent) Return CommonTNotificationAgent objects filtered by the id_agent column
 * @method array findByIdType(int $id_type) Return CommonTNotificationAgent objects filtered by the id_type column
 * @method array findByServiceId(int $service_id) Return CommonTNotificationAgent objects filtered by the service_id column
 * @method array findByOrganisme(string $organisme) Return CommonTNotificationAgent objects filtered by the organisme column
 * @method array findByReferenceObjet(string $reference_objet) Return CommonTNotificationAgent objects filtered by the reference_objet column
 * @method array findByLibelleNotification(string $libelle_notification) Return CommonTNotificationAgent objects filtered by the libelle_notification column
 * @method array findByDescription(string $description) Return CommonTNotificationAgent objects filtered by the description column
 * @method array findByNotificationLue(string $notification_lue) Return CommonTNotificationAgent objects filtered by the notification_lue column
 * @method array findByNotificationActif(string $notification_actif) Return CommonTNotificationAgent objects filtered by the notification_actif column
 * @method array findByUrlNotification(string $url_notification) Return CommonTNotificationAgent objects filtered by the url_notification column
 * @method array findByDateCreation(string $date_creation) Return CommonTNotificationAgent objects filtered by the date_creation column
 * @method array findByDateLecture(string $date_lecture) Return CommonTNotificationAgent objects filtered by the date_lecture column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTNotificationAgentQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTNotificationAgentQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTNotificationAgent', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTNotificationAgentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTNotificationAgentQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTNotificationAgentQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTNotificationAgentQuery) {
            return $criteria;
        }
        $query = new CommonTNotificationAgentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTNotificationAgent|CommonTNotificationAgent[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTNotificationAgentPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTNotificationAgentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTNotificationAgent A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdNotification($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTNotificationAgent A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_notification`, `id_agent`, `id_type`, `service_id`, `organisme`, `reference_objet`, `libelle_notification`, `description`, `notification_lue`, `notification_actif`, `url_notification`, `date_creation`, `date_lecture` FROM `t_notification_agent` WHERE `id_notification` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTNotificationAgent();
            $obj->hydrate($row);
            CommonTNotificationAgentPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTNotificationAgent|CommonTNotificationAgent[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTNotificationAgent[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTNotificationAgentPeer::ID_NOTIFICATION, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTNotificationAgentPeer::ID_NOTIFICATION, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByIdNotification(1234); // WHERE id_notification = 1234
     * $query->filterByIdNotification(array(12, 34)); // WHERE id_notification IN (12, 34)
     * $query->filterByIdNotification(array('min' => 12)); // WHERE id_notification >= 12
     * $query->filterByIdNotification(array('max' => 12)); // WHERE id_notification <= 12
     * </code>
     *
     * @param     mixed $idNotification The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByIdNotification($idNotification = null, $comparison = null)
    {
        if (is_array($idNotification)) {
            $useMinMax = false;
            if (isset($idNotification['min'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::ID_NOTIFICATION, $idNotification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idNotification['max'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::ID_NOTIFICATION, $idNotification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::ID_NOTIFICATION, $idNotification, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Filter the query on the id_type column
     *
     * Example usage:
     * <code>
     * $query->filterByIdType(1234); // WHERE id_type = 1234
     * $query->filterByIdType(array(12, 34)); // WHERE id_type IN (12, 34)
     * $query->filterByIdType(array('min' => 12)); // WHERE id_type >= 12
     * $query->filterByIdType(array('max' => 12)); // WHERE id_type <= 12
     * </code>
     *
     * @see       filterByCommonTTypeNotificationAgent()
     *
     * @param     mixed $idType The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByIdType($idType = null, $comparison = null)
    {
        if (is_array($idType)) {
            $useMinMax = false;
            if (isset($idType['min'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::ID_TYPE, $idType['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idType['max'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::ID_TYPE, $idType['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::ID_TYPE, $idType, $comparison);
    }

    /**
     * Filter the query on the service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceId(1234); // WHERE service_id = 1234
     * $query->filterByServiceId(array(12, 34)); // WHERE service_id IN (12, 34)
     * $query->filterByServiceId(array('min' => 12)); // WHERE service_id >= 12
     * $query->filterByServiceId(array('max' => 12)); // WHERE service_id <= 12
     * </code>
     *
     * @param     mixed $serviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByServiceId($serviceId = null, $comparison = null)
    {
        if (is_array($serviceId)) {
            $useMinMax = false;
            if (isset($serviceId['min'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::SERVICE_ID, $serviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceId['max'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::SERVICE_ID, $serviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::SERVICE_ID, $serviceId, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the reference_objet column
     *
     * Example usage:
     * <code>
     * $query->filterByReferenceObjet('fooValue');   // WHERE reference_objet = 'fooValue'
     * $query->filterByReferenceObjet('%fooValue%'); // WHERE reference_objet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $referenceObjet The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByReferenceObjet($referenceObjet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($referenceObjet)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $referenceObjet)) {
                $referenceObjet = str_replace('*', '%', $referenceObjet);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::REFERENCE_OBJET, $referenceObjet, $comparison);
    }

    /**
     * Filter the query on the libelle_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleNotification('fooValue');   // WHERE libelle_notification = 'fooValue'
     * $query->filterByLibelleNotification('%fooValue%'); // WHERE libelle_notification LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleNotification The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByLibelleNotification($libelleNotification = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleNotification)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleNotification)) {
                $libelleNotification = str_replace('*', '%', $libelleNotification);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::LIBELLE_NOTIFICATION, $libelleNotification, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the notification_lue column
     *
     * Example usage:
     * <code>
     * $query->filterByNotificationLue('fooValue');   // WHERE notification_lue = 'fooValue'
     * $query->filterByNotificationLue('%fooValue%'); // WHERE notification_lue LIKE '%fooValue%'
     * </code>
     *
     * @param     string $notificationLue The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByNotificationLue($notificationLue = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($notificationLue)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $notificationLue)) {
                $notificationLue = str_replace('*', '%', $notificationLue);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::NOTIFICATION_LUE, $notificationLue, $comparison);
    }

    /**
     * Filter the query on the notification_actif column
     *
     * Example usage:
     * <code>
     * $query->filterByNotificationActif('fooValue');   // WHERE notification_actif = 'fooValue'
     * $query->filterByNotificationActif('%fooValue%'); // WHERE notification_actif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $notificationActif The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByNotificationActif($notificationActif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($notificationActif)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $notificationActif)) {
                $notificationActif = str_replace('*', '%', $notificationActif);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::NOTIFICATION_ACTIF, $notificationActif, $comparison);
    }

    /**
     * Filter the query on the url_notification column
     *
     * Example usage:
     * <code>
     * $query->filterByUrlNotification('fooValue');   // WHERE url_notification = 'fooValue'
     * $query->filterByUrlNotification('%fooValue%'); // WHERE url_notification LIKE '%fooValue%'
     * </code>
     *
     * @param     string $urlNotification The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByUrlNotification($urlNotification = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($urlNotification)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $urlNotification)) {
                $urlNotification = str_replace('*', '%', $urlNotification);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::URL_NOTIFICATION, $urlNotification, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_lecture column
     *
     * Example usage:
     * <code>
     * $query->filterByDateLecture('2011-03-14'); // WHERE date_lecture = '2011-03-14'
     * $query->filterByDateLecture('now'); // WHERE date_lecture = '2011-03-14'
     * $query->filterByDateLecture(array('max' => 'yesterday')); // WHERE date_lecture > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateLecture The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function filterByDateLecture($dateLecture = null, $comparison = null)
    {
        if (is_array($dateLecture)) {
            $useMinMax = false;
            if (isset($dateLecture['min'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::DATE_LECTURE, $dateLecture['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateLecture['max'])) {
                $this->addUsingAlias(CommonTNotificationAgentPeer::DATE_LECTURE, $dateLecture['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNotificationAgentPeer::DATE_LECTURE, $dateLecture, $comparison);
    }

    /**
     * Filter the query by a related CommonTTypeNotificationAgent object
     *
     * @param   CommonTTypeNotificationAgent|PropelObjectCollection $commonTTypeNotificationAgent The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTNotificationAgentQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTTypeNotificationAgent($commonTTypeNotificationAgent, $comparison = null)
    {
        if ($commonTTypeNotificationAgent instanceof CommonTTypeNotificationAgent) {
            return $this
                ->addUsingAlias(CommonTNotificationAgentPeer::ID_TYPE, $commonTTypeNotificationAgent->getIdType(), $comparison);
        } elseif ($commonTTypeNotificationAgent instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTNotificationAgentPeer::ID_TYPE, $commonTTypeNotificationAgent->toKeyValue('PrimaryKey', 'IdType'), $comparison);
        } else {
            throw new PropelException('filterByCommonTTypeNotificationAgent() only accepts arguments of type CommonTTypeNotificationAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTTypeNotificationAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function joinCommonTTypeNotificationAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTTypeNotificationAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTTypeNotificationAgent');
        }

        return $this;
    }

    /**
     * Use the CommonTTypeNotificationAgent relation CommonTTypeNotificationAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTTypeNotificationAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonTTypeNotificationAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTTypeNotificationAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTTypeNotificationAgent', '\Application\Propel\Mpe\CommonTTypeNotificationAgentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTNotificationAgent $commonTNotificationAgent Object to remove from the list of results
     *
     * @return CommonTNotificationAgentQuery The current query, for fluid interface
     */
    public function prune($commonTNotificationAgent = null)
    {
        if ($commonTNotificationAgent) {
            $this->addUsingAlias(CommonTNotificationAgentPeer::ID_NOTIFICATION, $commonTNotificationAgent->getIdNotification(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
