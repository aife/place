<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAnnonceBoampPeer;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonInterfaceSuivi;
use Application\Propel\Mpe\CommonInterfaceSuiviPeer;
use Application\Propel\Mpe\Map\CommonInterfaceSuiviTableMap;

/**
 * Base static class for performing query and update operations on the 'interface_suivi' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonInterfaceSuiviPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'interface_suivi';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonInterfaceSuivi';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonInterfaceSuiviTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 13;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 13;

    /** the column name for the uuid field */
    const UUID = 'interface_suivi.uuid';

    /** the column name for the id_consultation field */
    const ID_CONSULTATION = 'interface_suivi.id_consultation';

    /** the column name for the flux field */
    const FLUX = 'interface_suivi.flux';

    /** the column name for the action field */
    const ACTION = 'interface_suivi.action';

    /** the column name for the statut field */
    const STATUT = 'interface_suivi.statut';

    /** the column name for the message field */
    const MESSAGE = 'interface_suivi.message';

    /** the column name for the id_objet_destination field */
    const ID_OBJET_DESTINATION = 'interface_suivi.id_objet_destination';

    /** the column name for the date_creation field */
    const DATE_CREATION = 'interface_suivi.date_creation';

    /** the column name for the date_modification field */
    const DATE_MODIFICATION = 'interface_suivi.date_modification';

    /** the column name for the id field */
    const ID = 'interface_suivi.id';

    /** the column name for the message_details field */
    const MESSAGE_DETAILS = 'interface_suivi.message_details';

    /** the column name for the etape field */
    const ETAPE = 'interface_suivi.etape';

    /** the column name for the annonce_id field */
    const ANNONCE_ID = 'interface_suivi.annonce_id';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonInterfaceSuivi objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonInterfaceSuivi[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonInterfaceSuiviPeer::$fieldNames[CommonInterfaceSuiviPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Uuid', 'IdConsultation', 'Flux', 'Action', 'Statut', 'Message', 'IdObjetDestination', 'DateCreation', 'DateModification', 'Id', 'MessageDetails', 'Etape', 'AnnonceId', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('uuid', 'idConsultation', 'flux', 'action', 'statut', 'message', 'idObjetDestination', 'dateCreation', 'dateModification', 'id', 'messageDetails', 'etape', 'annonceId', ),
        BasePeer::TYPE_COLNAME => array (CommonInterfaceSuiviPeer::UUID, CommonInterfaceSuiviPeer::ID_CONSULTATION, CommonInterfaceSuiviPeer::FLUX, CommonInterfaceSuiviPeer::ACTION, CommonInterfaceSuiviPeer::STATUT, CommonInterfaceSuiviPeer::MESSAGE, CommonInterfaceSuiviPeer::ID_OBJET_DESTINATION, CommonInterfaceSuiviPeer::DATE_CREATION, CommonInterfaceSuiviPeer::DATE_MODIFICATION, CommonInterfaceSuiviPeer::ID, CommonInterfaceSuiviPeer::MESSAGE_DETAILS, CommonInterfaceSuiviPeer::ETAPE, CommonInterfaceSuiviPeer::ANNONCE_ID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('UUID', 'ID_CONSULTATION', 'FLUX', 'ACTION', 'STATUT', 'MESSAGE', 'ID_OBJET_DESTINATION', 'DATE_CREATION', 'DATE_MODIFICATION', 'ID', 'MESSAGE_DETAILS', 'ETAPE', 'ANNONCE_ID', ),
        BasePeer::TYPE_FIELDNAME => array ('uuid', 'id_consultation', 'flux', 'action', 'statut', 'message', 'id_objet_destination', 'date_creation', 'date_modification', 'id', 'message_details', 'etape', 'annonce_id', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonInterfaceSuiviPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Uuid' => 0, 'IdConsultation' => 1, 'Flux' => 2, 'Action' => 3, 'Statut' => 4, 'Message' => 5, 'IdObjetDestination' => 6, 'DateCreation' => 7, 'DateModification' => 8, 'Id' => 9, 'MessageDetails' => 10, 'Etape' => 11, 'AnnonceId' => 12, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('uuid' => 0, 'idConsultation' => 1, 'flux' => 2, 'action' => 3, 'statut' => 4, 'message' => 5, 'idObjetDestination' => 6, 'dateCreation' => 7, 'dateModification' => 8, 'id' => 9, 'messageDetails' => 10, 'etape' => 11, 'annonceId' => 12, ),
        BasePeer::TYPE_COLNAME => array (CommonInterfaceSuiviPeer::UUID => 0, CommonInterfaceSuiviPeer::ID_CONSULTATION => 1, CommonInterfaceSuiviPeer::FLUX => 2, CommonInterfaceSuiviPeer::ACTION => 3, CommonInterfaceSuiviPeer::STATUT => 4, CommonInterfaceSuiviPeer::MESSAGE => 5, CommonInterfaceSuiviPeer::ID_OBJET_DESTINATION => 6, CommonInterfaceSuiviPeer::DATE_CREATION => 7, CommonInterfaceSuiviPeer::DATE_MODIFICATION => 8, CommonInterfaceSuiviPeer::ID => 9, CommonInterfaceSuiviPeer::MESSAGE_DETAILS => 10, CommonInterfaceSuiviPeer::ETAPE => 11, CommonInterfaceSuiviPeer::ANNONCE_ID => 12, ),
        BasePeer::TYPE_RAW_COLNAME => array ('UUID' => 0, 'ID_CONSULTATION' => 1, 'FLUX' => 2, 'ACTION' => 3, 'STATUT' => 4, 'MESSAGE' => 5, 'ID_OBJET_DESTINATION' => 6, 'DATE_CREATION' => 7, 'DATE_MODIFICATION' => 8, 'ID' => 9, 'MESSAGE_DETAILS' => 10, 'ETAPE' => 11, 'ANNONCE_ID' => 12, ),
        BasePeer::TYPE_FIELDNAME => array ('uuid' => 0, 'id_consultation' => 1, 'flux' => 2, 'action' => 3, 'statut' => 4, 'message' => 5, 'id_objet_destination' => 6, 'date_creation' => 7, 'date_modification' => 8, 'id' => 9, 'message_details' => 10, 'etape' => 11, 'annonce_id' => 12, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonInterfaceSuiviPeer::getFieldNames($toType);
        $key = isset(CommonInterfaceSuiviPeer::$fieldKeys[$fromType][$name]) ? CommonInterfaceSuiviPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonInterfaceSuiviPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonInterfaceSuiviPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonInterfaceSuiviPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonInterfaceSuiviPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonInterfaceSuiviPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::UUID);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::ID_CONSULTATION);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::FLUX);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::ACTION);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::STATUT);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::MESSAGE);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::ID_OBJET_DESTINATION);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::DATE_CREATION);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::DATE_MODIFICATION);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::ID);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::MESSAGE_DETAILS);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::ETAPE);
            $criteria->addSelectColumn(CommonInterfaceSuiviPeer::ANNONCE_ID);
        } else {
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.id_consultation');
            $criteria->addSelectColumn($alias . '.flux');
            $criteria->addSelectColumn($alias . '.action');
            $criteria->addSelectColumn($alias . '.statut');
            $criteria->addSelectColumn($alias . '.message');
            $criteria->addSelectColumn($alias . '.id_objet_destination');
            $criteria->addSelectColumn($alias . '.date_creation');
            $criteria->addSelectColumn($alias . '.date_modification');
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.message_details');
            $criteria->addSelectColumn($alias . '.etape');
            $criteria->addSelectColumn($alias . '.annonce_id');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInterfaceSuiviPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonInterfaceSuivi
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonInterfaceSuiviPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonInterfaceSuiviPeer::populateObjects(CommonInterfaceSuiviPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonInterfaceSuivi $obj A CommonInterfaceSuivi object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonInterfaceSuiviPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonInterfaceSuivi object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonInterfaceSuivi) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonInterfaceSuivi object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonInterfaceSuiviPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonInterfaceSuivi Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonInterfaceSuiviPeer::$instances[$key])) {
                return CommonInterfaceSuiviPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonInterfaceSuiviPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonInterfaceSuiviPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to interface_suivi
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol + 9] === null) {
            return null;
        }

        return (string) $row[$startcol + 9];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol + 9];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonInterfaceSuiviPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonInterfaceSuiviPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonInterfaceSuiviPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonInterfaceSuiviPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonInterfaceSuivi object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonInterfaceSuiviPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonInterfaceSuiviPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonInterfaceSuiviPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonInterfaceSuiviPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonInterfaceSuiviPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonAnnonceBoamp table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonAnnonceBoamp(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInterfaceSuiviPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonInterfaceSuiviPeer::ANNONCE_ID, CommonAnnonceBoampPeer::ID_BOAMP, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInterfaceSuiviPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonInterfaceSuiviPeer::ID_CONSULTATION, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonInterfaceSuivi objects pre-filled with their CommonAnnonceBoamp objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonInterfaceSuivi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonAnnonceBoamp(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);
        }

        CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        $startcol = CommonInterfaceSuiviPeer::NUM_HYDRATE_COLUMNS;
        CommonAnnonceBoampPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonInterfaceSuiviPeer::ANNONCE_ID, CommonAnnonceBoampPeer::ID_BOAMP, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonInterfaceSuiviPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonInterfaceSuiviPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonInterfaceSuiviPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonInterfaceSuiviPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonAnnonceBoampPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonAnnonceBoampPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonAnnonceBoampPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonAnnonceBoampPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonInterfaceSuivi) to $obj2 (CommonAnnonceBoamp)
                $obj2->addCommonInterfaceSuivi($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonInterfaceSuivi objects pre-filled with their CommonConsultation objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonInterfaceSuivi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);
        }

        CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        $startcol = CommonInterfaceSuiviPeer::NUM_HYDRATE_COLUMNS;
        CommonConsultationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonInterfaceSuiviPeer::ID_CONSULTATION, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonInterfaceSuiviPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonInterfaceSuiviPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonInterfaceSuiviPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonInterfaceSuiviPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonInterfaceSuivi) to $obj2 (CommonConsultation)
                $obj2->addCommonInterfaceSuivi($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInterfaceSuiviPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonInterfaceSuiviPeer::ANNONCE_ID, CommonAnnonceBoampPeer::ID_BOAMP, $join_behavior);

        $criteria->addJoin(CommonInterfaceSuiviPeer::ID_CONSULTATION, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonInterfaceSuivi objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonInterfaceSuivi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);
        }

        CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        $startcol2 = CommonInterfaceSuiviPeer::NUM_HYDRATE_COLUMNS;

        CommonAnnonceBoampPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonAnnonceBoampPeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonInterfaceSuiviPeer::ANNONCE_ID, CommonAnnonceBoampPeer::ID_BOAMP, $join_behavior);

        $criteria->addJoin(CommonInterfaceSuiviPeer::ID_CONSULTATION, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonInterfaceSuiviPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonInterfaceSuiviPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonInterfaceSuiviPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonInterfaceSuiviPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonAnnonceBoamp rows

            $key2 = CommonAnnonceBoampPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonAnnonceBoampPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonAnnonceBoampPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonAnnonceBoampPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonInterfaceSuivi) to the collection in $obj2 (CommonAnnonceBoamp)
                $obj2->addCommonInterfaceSuivi($obj1);
            } // if joined row not null

            // Add objects for joined CommonConsultation rows

            $key3 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonConsultationPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonInterfaceSuivi) to the collection in $obj3 (CommonConsultation)
                $obj3->addCommonInterfaceSuivi($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonAnnonceBoamp table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonAnnonceBoamp(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInterfaceSuiviPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonInterfaceSuiviPeer::ID_CONSULTATION, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonInterfaceSuiviPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonInterfaceSuiviPeer::ANNONCE_ID, CommonAnnonceBoampPeer::ID_BOAMP, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonInterfaceSuivi objects pre-filled with all related objects except CommonAnnonceBoamp.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonInterfaceSuivi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonAnnonceBoamp(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);
        }

        CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        $startcol2 = CommonInterfaceSuiviPeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonInterfaceSuiviPeer::ID_CONSULTATION, CommonConsultationPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonInterfaceSuiviPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonInterfaceSuiviPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonInterfaceSuiviPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonInterfaceSuiviPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonConsultation rows

                $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonInterfaceSuivi) to the collection in $obj2 (CommonConsultation)
                $obj2->addCommonInterfaceSuivi($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonInterfaceSuivi objects pre-filled with all related objects except CommonConsultation.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonInterfaceSuivi objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);
        }

        CommonInterfaceSuiviPeer::addSelectColumns($criteria);
        $startcol2 = CommonInterfaceSuiviPeer::NUM_HYDRATE_COLUMNS;

        CommonAnnonceBoampPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonAnnonceBoampPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonInterfaceSuiviPeer::ANNONCE_ID, CommonAnnonceBoampPeer::ID_BOAMP, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonInterfaceSuiviPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonInterfaceSuiviPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonInterfaceSuiviPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonInterfaceSuiviPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonAnnonceBoamp rows

                $key2 = CommonAnnonceBoampPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonAnnonceBoampPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonAnnonceBoampPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonAnnonceBoampPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonInterfaceSuivi) to the collection in $obj2 (CommonAnnonceBoamp)
                $obj2->addCommonInterfaceSuivi($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonInterfaceSuiviPeer::DATABASE_NAME)->getTable(CommonInterfaceSuiviPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonInterfaceSuiviPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonInterfaceSuiviPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonInterfaceSuiviTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonInterfaceSuiviPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonInterfaceSuivi or Criteria object.
     *
     * @param      mixed $values Criteria or CommonInterfaceSuivi object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonInterfaceSuivi object
        }

        if ($criteria->containsKey(CommonInterfaceSuiviPeer::ID) && $criteria->keyContainsValue(CommonInterfaceSuiviPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonInterfaceSuiviPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonInterfaceSuivi or Criteria object.
     *
     * @param      mixed $values Criteria or CommonInterfaceSuivi object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonInterfaceSuiviPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonInterfaceSuiviPeer::ID);
            $value = $criteria->remove(CommonInterfaceSuiviPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonInterfaceSuiviPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonInterfaceSuiviPeer::TABLE_NAME);
            }

        } else { // $values is CommonInterfaceSuivi object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the interface_suivi table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonInterfaceSuiviPeer::TABLE_NAME, $con, CommonInterfaceSuiviPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonInterfaceSuiviPeer::clearInstancePool();
            CommonInterfaceSuiviPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonInterfaceSuivi or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonInterfaceSuivi object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonInterfaceSuiviPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonInterfaceSuivi) { // it's a model object
            // invalidate the cache for this single object
            CommonInterfaceSuiviPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonInterfaceSuiviPeer::DATABASE_NAME);
            $criteria->add(CommonInterfaceSuiviPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonInterfaceSuiviPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonInterfaceSuiviPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonInterfaceSuiviPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonInterfaceSuivi object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonInterfaceSuivi $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonInterfaceSuiviPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonInterfaceSuiviPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonInterfaceSuiviPeer::DATABASE_NAME, CommonInterfaceSuiviPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonInterfaceSuivi
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonInterfaceSuiviPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonInterfaceSuiviPeer::DATABASE_NAME);
        $criteria->add(CommonInterfaceSuiviPeer::ID, $pk);

        $v = CommonInterfaceSuiviPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonInterfaceSuivi[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonInterfaceSuiviPeer::DATABASE_NAME);
            $criteria->add(CommonInterfaceSuiviPeer::ID, $pks, Criteria::IN);
            $objs = CommonInterfaceSuiviPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonInterfaceSuiviPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonInterfaceSuiviPeer::buildTableMap();

