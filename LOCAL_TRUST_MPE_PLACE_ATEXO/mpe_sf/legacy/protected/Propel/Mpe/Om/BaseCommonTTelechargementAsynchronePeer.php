<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonTTelechargementAsynchrone;
use Application\Propel\Mpe\CommonTTelechargementAsynchronePeer;
use Application\Propel\Mpe\Map\CommonTTelechargementAsynchroneTableMap;

/**
 * Base static class for performing query and update operations on the 'T_Telechargement_Asynchrone' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTTelechargementAsynchronePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'T_Telechargement_Asynchrone';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTTelechargementAsynchrone';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTTelechargementAsynchroneTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 14;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 14;

    /** the column name for the id field */
    const ID = 'T_Telechargement_Asynchrone.id';

    /** the column name for the id_agent field */
    const ID_AGENT = 'T_Telechargement_Asynchrone.id_agent';

    /** the column name for the nom_prenom_agent field */
    const NOM_PRENOM_AGENT = 'T_Telechargement_Asynchrone.nom_prenom_agent';

    /** the column name for the email_agent field */
    const EMAIL_AGENT = 'T_Telechargement_Asynchrone.email_agent';

    /** the column name for the id_service_agent field */
    const ID_SERVICE_AGENT = 'T_Telechargement_Asynchrone.id_service_agent';

    /** the column name for the old_id_service_agent field */
    const OLD_ID_SERVICE_AGENT = 'T_Telechargement_Asynchrone.old_id_service_agent';

    /** the column name for the organisme_agent field */
    const ORGANISME_AGENT = 'T_Telechargement_Asynchrone.organisme_agent';

    /** the column name for the nom_fichier_telechargement field */
    const NOM_FICHIER_TELECHARGEMENT = 'T_Telechargement_Asynchrone.nom_fichier_telechargement';

    /** the column name for the taille_fichier field */
    const TAILLE_FICHIER = 'T_Telechargement_Asynchrone.taille_fichier';

    /** the column name for the date_generation field */
    const DATE_GENERATION = 'T_Telechargement_Asynchrone.date_generation';

    /** the column name for the id_blob_fichier field */
    const ID_BLOB_FICHIER = 'T_Telechargement_Asynchrone.id_blob_fichier';

    /** the column name for the tag_fichier_genere field */
    const TAG_FICHIER_GENERE = 'T_Telechargement_Asynchrone.tag_fichier_genere';

    /** the column name for the tag_fichier_supprime field */
    const TAG_FICHIER_SUPPRIME = 'T_Telechargement_Asynchrone.tag_fichier_supprime';

    /** the column name for the type_telechargement field */
    const TYPE_TELECHARGEMENT = 'T_Telechargement_Asynchrone.type_telechargement';

    /** The enumerated values for the tag_fichier_genere field */
    const TAG_FICHIER_GENERE_0 = '0';
    const TAG_FICHIER_GENERE_1 = '1';

    /** The enumerated values for the tag_fichier_supprime field */
    const TAG_FICHIER_SUPPRIME_0 = '0';
    const TAG_FICHIER_SUPPRIME_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTTelechargementAsynchrone objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTTelechargementAsynchrone[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTTelechargementAsynchronePeer::$fieldNames[CommonTTelechargementAsynchronePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'IdAgent', 'NomPrenomAgent', 'EmailAgent', 'IdServiceAgent', 'OldIdServiceAgent', 'OrganismeAgent', 'NomFichierTelechargement', 'TailleFichier', 'DateGeneration', 'IdBlobFichier', 'TagFichierGenere', 'TagFichierSupprime', 'TypeTelechargement', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'idAgent', 'nomPrenomAgent', 'emailAgent', 'idServiceAgent', 'oldIdServiceAgent', 'organismeAgent', 'nomFichierTelechargement', 'tailleFichier', 'dateGeneration', 'idBlobFichier', 'tagFichierGenere', 'tagFichierSupprime', 'typeTelechargement', ),
        BasePeer::TYPE_COLNAME => array (CommonTTelechargementAsynchronePeer::ID, CommonTTelechargementAsynchronePeer::ID_AGENT, CommonTTelechargementAsynchronePeer::NOM_PRENOM_AGENT, CommonTTelechargementAsynchronePeer::EMAIL_AGENT, CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, CommonTTelechargementAsynchronePeer::OLD_ID_SERVICE_AGENT, CommonTTelechargementAsynchronePeer::ORGANISME_AGENT, CommonTTelechargementAsynchronePeer::NOM_FICHIER_TELECHARGEMENT, CommonTTelechargementAsynchronePeer::TAILLE_FICHIER, CommonTTelechargementAsynchronePeer::DATE_GENERATION, CommonTTelechargementAsynchronePeer::ID_BLOB_FICHIER, CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE, CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME, CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ID_AGENT', 'NOM_PRENOM_AGENT', 'EMAIL_AGENT', 'ID_SERVICE_AGENT', 'OLD_ID_SERVICE_AGENT', 'ORGANISME_AGENT', 'NOM_FICHIER_TELECHARGEMENT', 'TAILLE_FICHIER', 'DATE_GENERATION', 'ID_BLOB_FICHIER', 'TAG_FICHIER_GENERE', 'TAG_FICHIER_SUPPRIME', 'TYPE_TELECHARGEMENT', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'id_agent', 'nom_prenom_agent', 'email_agent', 'id_service_agent', 'old_id_service_agent', 'organisme_agent', 'nom_fichier_telechargement', 'taille_fichier', 'date_generation', 'id_blob_fichier', 'tag_fichier_genere', 'tag_fichier_supprime', 'type_telechargement', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTTelechargementAsynchronePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IdAgent' => 1, 'NomPrenomAgent' => 2, 'EmailAgent' => 3, 'IdServiceAgent' => 4, 'OldIdServiceAgent' => 5, 'OrganismeAgent' => 6, 'NomFichierTelechargement' => 7, 'TailleFichier' => 8, 'DateGeneration' => 9, 'IdBlobFichier' => 10, 'TagFichierGenere' => 11, 'TagFichierSupprime' => 12, 'TypeTelechargement' => 13, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'idAgent' => 1, 'nomPrenomAgent' => 2, 'emailAgent' => 3, 'idServiceAgent' => 4, 'oldIdServiceAgent' => 5, 'organismeAgent' => 6, 'nomFichierTelechargement' => 7, 'tailleFichier' => 8, 'dateGeneration' => 9, 'idBlobFichier' => 10, 'tagFichierGenere' => 11, 'tagFichierSupprime' => 12, 'typeTelechargement' => 13, ),
        BasePeer::TYPE_COLNAME => array (CommonTTelechargementAsynchronePeer::ID => 0, CommonTTelechargementAsynchronePeer::ID_AGENT => 1, CommonTTelechargementAsynchronePeer::NOM_PRENOM_AGENT => 2, CommonTTelechargementAsynchronePeer::EMAIL_AGENT => 3, CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT => 4, CommonTTelechargementAsynchronePeer::OLD_ID_SERVICE_AGENT => 5, CommonTTelechargementAsynchronePeer::ORGANISME_AGENT => 6, CommonTTelechargementAsynchronePeer::NOM_FICHIER_TELECHARGEMENT => 7, CommonTTelechargementAsynchronePeer::TAILLE_FICHIER => 8, CommonTTelechargementAsynchronePeer::DATE_GENERATION => 9, CommonTTelechargementAsynchronePeer::ID_BLOB_FICHIER => 10, CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE => 11, CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME => 12, CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT => 13, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ID_AGENT' => 1, 'NOM_PRENOM_AGENT' => 2, 'EMAIL_AGENT' => 3, 'ID_SERVICE_AGENT' => 4, 'OLD_ID_SERVICE_AGENT' => 5, 'ORGANISME_AGENT' => 6, 'NOM_FICHIER_TELECHARGEMENT' => 7, 'TAILLE_FICHIER' => 8, 'DATE_GENERATION' => 9, 'ID_BLOB_FICHIER' => 10, 'TAG_FICHIER_GENERE' => 11, 'TAG_FICHIER_SUPPRIME' => 12, 'TYPE_TELECHARGEMENT' => 13, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'id_agent' => 1, 'nom_prenom_agent' => 2, 'email_agent' => 3, 'id_service_agent' => 4, 'old_id_service_agent' => 5, 'organisme_agent' => 6, 'nom_fichier_telechargement' => 7, 'taille_fichier' => 8, 'date_generation' => 9, 'id_blob_fichier' => 10, 'tag_fichier_genere' => 11, 'tag_fichier_supprime' => 12, 'type_telechargement' => 13, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE => array(
            CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE_0,
            CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE_1,
        ),
        CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME => array(
            CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME_0,
            CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTTelechargementAsynchronePeer::getFieldNames($toType);
        $key = isset(CommonTTelechargementAsynchronePeer::$fieldKeys[$fromType][$name]) ? CommonTTelechargementAsynchronePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTTelechargementAsynchronePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTTelechargementAsynchronePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTTelechargementAsynchronePeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTTelechargementAsynchronePeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTTelechargementAsynchronePeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTTelechargementAsynchronePeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTTelechargementAsynchronePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTTelechargementAsynchronePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::ID);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::ID_AGENT);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::NOM_PRENOM_AGENT);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::EMAIL_AGENT);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::OLD_ID_SERVICE_AGENT);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::ORGANISME_AGENT);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::NOM_FICHIER_TELECHARGEMENT);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::TAILLE_FICHIER);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::DATE_GENERATION);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::ID_BLOB_FICHIER);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME);
            $criteria->addSelectColumn(CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.id_agent');
            $criteria->addSelectColumn($alias . '.nom_prenom_agent');
            $criteria->addSelectColumn($alias . '.email_agent');
            $criteria->addSelectColumn($alias . '.id_service_agent');
            $criteria->addSelectColumn($alias . '.old_id_service_agent');
            $criteria->addSelectColumn($alias . '.organisme_agent');
            $criteria->addSelectColumn($alias . '.nom_fichier_telechargement');
            $criteria->addSelectColumn($alias . '.taille_fichier');
            $criteria->addSelectColumn($alias . '.date_generation');
            $criteria->addSelectColumn($alias . '.id_blob_fichier');
            $criteria->addSelectColumn($alias . '.tag_fichier_genere');
            $criteria->addSelectColumn($alias . '.tag_fichier_supprime');
            $criteria->addSelectColumn($alias . '.type_telechargement');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTTelechargementAsynchronePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTTelechargementAsynchronePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTTelechargementAsynchronePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTTelechargementAsynchrone
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTTelechargementAsynchronePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTTelechargementAsynchronePeer::populateObjects(CommonTTelechargementAsynchronePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTTelechargementAsynchronePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTTelechargementAsynchronePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTTelechargementAsynchrone $obj A CommonTTelechargementAsynchrone object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTTelechargementAsynchronePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTTelechargementAsynchrone object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTTelechargementAsynchrone) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTTelechargementAsynchrone object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTTelechargementAsynchronePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTTelechargementAsynchrone Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTTelechargementAsynchronePeer::$instances[$key])) {
                return CommonTTelechargementAsynchronePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTTelechargementAsynchronePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTTelechargementAsynchronePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to T_Telechargement_Asynchrone
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTTelechargementAsynchronePeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTTelechargementAsynchronePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTTelechargementAsynchronePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTTelechargementAsynchronePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTTelechargementAsynchrone object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTTelechargementAsynchronePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTTelechargementAsynchronePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTTelechargementAsynchronePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTTelechargementAsynchronePeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTTelechargementAsynchronePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonService table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonService(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTTelechargementAsynchronePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTTelechargementAsynchronePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTTelechargementAsynchronePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTTelechargementAsynchrone objects pre-filled with their CommonService objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTTelechargementAsynchrone objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonService(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTTelechargementAsynchronePeer::DATABASE_NAME);
        }

        CommonTTelechargementAsynchronePeer::addSelectColumns($criteria);
        $startcol = CommonTTelechargementAsynchronePeer::NUM_HYDRATE_COLUMNS;
        CommonServicePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTTelechargementAsynchronePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTTelechargementAsynchronePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTTelechargementAsynchronePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTTelechargementAsynchronePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonServicePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonServicePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTTelechargementAsynchrone) to $obj2 (CommonService)
                $obj2->addCommonTTelechargementAsynchrone($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTTelechargementAsynchronePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTTelechargementAsynchronePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTTelechargementAsynchronePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTTelechargementAsynchrone objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTTelechargementAsynchrone objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTTelechargementAsynchronePeer::DATABASE_NAME);
        }

        CommonTTelechargementAsynchronePeer::addSelectColumns($criteria);
        $startcol2 = CommonTTelechargementAsynchronePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTTelechargementAsynchronePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTTelechargementAsynchronePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTTelechargementAsynchronePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTTelechargementAsynchronePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonService rows

            $key2 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonServicePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonServicePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTTelechargementAsynchrone) to the collection in $obj2 (CommonService)
                $obj2->addCommonTTelechargementAsynchrone($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTTelechargementAsynchronePeer::DATABASE_NAME)->getTable(CommonTTelechargementAsynchronePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTTelechargementAsynchronePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTTelechargementAsynchronePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTTelechargementAsynchroneTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTTelechargementAsynchronePeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTTelechargementAsynchrone or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTTelechargementAsynchrone object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTTelechargementAsynchrone object
        }

        if ($criteria->containsKey(CommonTTelechargementAsynchronePeer::ID) && $criteria->keyContainsValue(CommonTTelechargementAsynchronePeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTTelechargementAsynchronePeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTTelechargementAsynchronePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTTelechargementAsynchrone or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTTelechargementAsynchrone object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTTelechargementAsynchronePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTTelechargementAsynchronePeer::ID);
            $value = $criteria->remove(CommonTTelechargementAsynchronePeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTTelechargementAsynchronePeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTTelechargementAsynchronePeer::TABLE_NAME);
            }

        } else { // $values is CommonTTelechargementAsynchrone object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTTelechargementAsynchronePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the T_Telechargement_Asynchrone table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTTelechargementAsynchronePeer::TABLE_NAME, $con, CommonTTelechargementAsynchronePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTTelechargementAsynchronePeer::clearInstancePool();
            CommonTTelechargementAsynchronePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTTelechargementAsynchrone or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTTelechargementAsynchrone object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTTelechargementAsynchronePeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTTelechargementAsynchrone) { // it's a model object
            // invalidate the cache for this single object
            CommonTTelechargementAsynchronePeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTTelechargementAsynchronePeer::DATABASE_NAME);
            $criteria->add(CommonTTelechargementAsynchronePeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTTelechargementAsynchronePeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTTelechargementAsynchronePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTTelechargementAsynchronePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTTelechargementAsynchrone object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTTelechargementAsynchrone $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTTelechargementAsynchronePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTTelechargementAsynchronePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTTelechargementAsynchronePeer::DATABASE_NAME, CommonTTelechargementAsynchronePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTTelechargementAsynchrone
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTTelechargementAsynchronePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTTelechargementAsynchronePeer::DATABASE_NAME);
        $criteria->add(CommonTTelechargementAsynchronePeer::ID, $pk);

        $v = CommonTTelechargementAsynchronePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTTelechargementAsynchrone[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTTelechargementAsynchronePeer::DATABASE_NAME);
            $criteria->add(CommonTTelechargementAsynchronePeer::ID, $pks, Criteria::IN);
            $objs = CommonTTelechargementAsynchronePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTTelechargementAsynchronePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTTelechargementAsynchronePeer::buildTableMap();

