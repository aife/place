<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTDonneesConsultation;
use Application\Propel\Mpe\CommonTDonneesConsultationPeer;
use Application\Propel\Mpe\CommonTDonneesConsultationQuery;

/**
 * Base class that represents a query for the 't_donnees_consultation' table.
 *
 *
 *
 * @method CommonTDonneesConsultationQuery orderByIdDonneesConsultation($order = Criteria::ASC) Order by the id_donnees_consultation column
 * @method CommonTDonneesConsultationQuery orderByReferenceConsultation($order = Criteria::ASC) Order by the reference_consultation column
 * @method CommonTDonneesConsultationQuery orderByIdContratTitulaire($order = Criteria::ASC) Order by the id_contrat_titulaire column
 * @method CommonTDonneesConsultationQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTDonneesConsultationQuery orderByOldServiceId($order = Criteria::ASC) Order by the old_service_id column
 * @method CommonTDonneesConsultationQuery orderByIdTypeProcedure($order = Criteria::ASC) Order by the id_type_procedure column
 * @method CommonTDonneesConsultationQuery orderByLibelleTypeProcedure($order = Criteria::ASC) Order by the libelle_type_procedure column
 * @method CommonTDonneesConsultationQuery orderByNbreOffresRecues($order = Criteria::ASC) Order by the nbre_offres_recues column
 * @method CommonTDonneesConsultationQuery orderByNbreOffresDematerialisees($order = Criteria::ASC) Order by the nbre_offres_dematerialisees column
 * @method CommonTDonneesConsultationQuery orderBySignatureOffre($order = Criteria::ASC) Order by the signature_offre column
 * @method CommonTDonneesConsultationQuery orderByServiceId($order = Criteria::ASC) Order by the service_id column
 * @method CommonTDonneesConsultationQuery orderByUuidExterneExec($order = Criteria::ASC) Order by the uuid_externe_exec column
 *
 * @method CommonTDonneesConsultationQuery groupByIdDonneesConsultation() Group by the id_donnees_consultation column
 * @method CommonTDonneesConsultationQuery groupByReferenceConsultation() Group by the reference_consultation column
 * @method CommonTDonneesConsultationQuery groupByIdContratTitulaire() Group by the id_contrat_titulaire column
 * @method CommonTDonneesConsultationQuery groupByOrganisme() Group by the organisme column
 * @method CommonTDonneesConsultationQuery groupByOldServiceId() Group by the old_service_id column
 * @method CommonTDonneesConsultationQuery groupByIdTypeProcedure() Group by the id_type_procedure column
 * @method CommonTDonneesConsultationQuery groupByLibelleTypeProcedure() Group by the libelle_type_procedure column
 * @method CommonTDonneesConsultationQuery groupByNbreOffresRecues() Group by the nbre_offres_recues column
 * @method CommonTDonneesConsultationQuery groupByNbreOffresDematerialisees() Group by the nbre_offres_dematerialisees column
 * @method CommonTDonneesConsultationQuery groupBySignatureOffre() Group by the signature_offre column
 * @method CommonTDonneesConsultationQuery groupByServiceId() Group by the service_id column
 * @method CommonTDonneesConsultationQuery groupByUuidExterneExec() Group by the uuid_externe_exec column
 *
 * @method CommonTDonneesConsultationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTDonneesConsultationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTDonneesConsultationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTDonneesConsultationQuery leftJoinCommonTContratTitulaire($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonTDonneesConsultationQuery rightJoinCommonTContratTitulaire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonTDonneesConsultationQuery innerJoinCommonTContratTitulaire($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContratTitulaire relation
 *
 * @method CommonTDonneesConsultation findOne(PropelPDO $con = null) Return the first CommonTDonneesConsultation matching the query
 * @method CommonTDonneesConsultation findOneOrCreate(PropelPDO $con = null) Return the first CommonTDonneesConsultation matching the query, or a new CommonTDonneesConsultation object populated from the query conditions when no match is found
 *
 * @method CommonTDonneesConsultation findOneByReferenceConsultation(int $reference_consultation) Return the first CommonTDonneesConsultation filtered by the reference_consultation column
 * @method CommonTDonneesConsultation findOneByIdContratTitulaire(int $id_contrat_titulaire) Return the first CommonTDonneesConsultation filtered by the id_contrat_titulaire column
 * @method CommonTDonneesConsultation findOneByOrganisme(string $organisme) Return the first CommonTDonneesConsultation filtered by the organisme column
 * @method CommonTDonneesConsultation findOneByOldServiceId(int $old_service_id) Return the first CommonTDonneesConsultation filtered by the old_service_id column
 * @method CommonTDonneesConsultation findOneByIdTypeProcedure(int $id_type_procedure) Return the first CommonTDonneesConsultation filtered by the id_type_procedure column
 * @method CommonTDonneesConsultation findOneByLibelleTypeProcedure(string $libelle_type_procedure) Return the first CommonTDonneesConsultation filtered by the libelle_type_procedure column
 * @method CommonTDonneesConsultation findOneByNbreOffresRecues(int $nbre_offres_recues) Return the first CommonTDonneesConsultation filtered by the nbre_offres_recues column
 * @method CommonTDonneesConsultation findOneByNbreOffresDematerialisees(int $nbre_offres_dematerialisees) Return the first CommonTDonneesConsultation filtered by the nbre_offres_dematerialisees column
 * @method CommonTDonneesConsultation findOneBySignatureOffre(string $signature_offre) Return the first CommonTDonneesConsultation filtered by the signature_offre column
 * @method CommonTDonneesConsultation findOneByServiceId(string $service_id) Return the first CommonTDonneesConsultation filtered by the service_id column
 * @method CommonTDonneesConsultation findOneByUuidExterneExec(string $uuid_externe_exec) Return the first CommonTDonneesConsultation filtered by the uuid_externe_exec column
 *
 * @method array findByIdDonneesConsultation(int $id_donnees_consultation) Return CommonTDonneesConsultation objects filtered by the id_donnees_consultation column
 * @method array findByReferenceConsultation(int $reference_consultation) Return CommonTDonneesConsultation objects filtered by the reference_consultation column
 * @method array findByIdContratTitulaire(int $id_contrat_titulaire) Return CommonTDonneesConsultation objects filtered by the id_contrat_titulaire column
 * @method array findByOrganisme(string $organisme) Return CommonTDonneesConsultation objects filtered by the organisme column
 * @method array findByOldServiceId(int $old_service_id) Return CommonTDonneesConsultation objects filtered by the old_service_id column
 * @method array findByIdTypeProcedure(int $id_type_procedure) Return CommonTDonneesConsultation objects filtered by the id_type_procedure column
 * @method array findByLibelleTypeProcedure(string $libelle_type_procedure) Return CommonTDonneesConsultation objects filtered by the libelle_type_procedure column
 * @method array findByNbreOffresRecues(int $nbre_offres_recues) Return CommonTDonneesConsultation objects filtered by the nbre_offres_recues column
 * @method array findByNbreOffresDematerialisees(int $nbre_offres_dematerialisees) Return CommonTDonneesConsultation objects filtered by the nbre_offres_dematerialisees column
 * @method array findBySignatureOffre(string $signature_offre) Return CommonTDonneesConsultation objects filtered by the signature_offre column
 * @method array findByServiceId(string $service_id) Return CommonTDonneesConsultation objects filtered by the service_id column
 * @method array findByUuidExterneExec(string $uuid_externe_exec) Return CommonTDonneesConsultation objects filtered by the uuid_externe_exec column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDonneesConsultationQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTDonneesConsultationQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTDonneesConsultation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTDonneesConsultationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTDonneesConsultationQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTDonneesConsultationQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTDonneesConsultationQuery) {
            return $criteria;
        }
        $query = new CommonTDonneesConsultationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTDonneesConsultation|CommonTDonneesConsultation[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTDonneesConsultationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTDonneesConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDonneesConsultation A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdDonneesConsultation($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDonneesConsultation A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_donnees_consultation`, `reference_consultation`, `id_contrat_titulaire`, `organisme`, `old_service_id`, `id_type_procedure`, `libelle_type_procedure`, `nbre_offres_recues`, `nbre_offres_dematerialisees`, `signature_offre`, `service_id`, `uuid_externe_exec` FROM `t_donnees_consultation` WHERE `id_donnees_consultation` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTDonneesConsultation();
            $obj->hydrate($row);
            CommonTDonneesConsultationPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTDonneesConsultation|CommonTDonneesConsultation[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTDonneesConsultation[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_donnees_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDonneesConsultation(1234); // WHERE id_donnees_consultation = 1234
     * $query->filterByIdDonneesConsultation(array(12, 34)); // WHERE id_donnees_consultation IN (12, 34)
     * $query->filterByIdDonneesConsultation(array('min' => 12)); // WHERE id_donnees_consultation >= 12
     * $query->filterByIdDonneesConsultation(array('max' => 12)); // WHERE id_donnees_consultation <= 12
     * </code>
     *
     * @param     mixed $idDonneesConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByIdDonneesConsultation($idDonneesConsultation = null, $comparison = null)
    {
        if (is_array($idDonneesConsultation)) {
            $useMinMax = false;
            if (isset($idDonneesConsultation['min'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION, $idDonneesConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDonneesConsultation['max'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION, $idDonneesConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION, $idDonneesConsultation, $comparison);
    }

    /**
     * Filter the query on the reference_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByReferenceConsultation(1234); // WHERE reference_consultation = 1234
     * $query->filterByReferenceConsultation(array(12, 34)); // WHERE reference_consultation IN (12, 34)
     * $query->filterByReferenceConsultation(array('min' => 12)); // WHERE reference_consultation >= 12
     * $query->filterByReferenceConsultation(array('max' => 12)); // WHERE reference_consultation <= 12
     * </code>
     *
     * @param     mixed $referenceConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByReferenceConsultation($referenceConsultation = null, $comparison = null)
    {
        if (is_array($referenceConsultation)) {
            $useMinMax = false;
            if (isset($referenceConsultation['min'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::REFERENCE_CONSULTATION, $referenceConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($referenceConsultation['max'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::REFERENCE_CONSULTATION, $referenceConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::REFERENCE_CONSULTATION, $referenceConsultation, $comparison);
    }

    /**
     * Filter the query on the id_contrat_titulaire column
     *
     * Example usage:
     * <code>
     * $query->filterByIdContratTitulaire(1234); // WHERE id_contrat_titulaire = 1234
     * $query->filterByIdContratTitulaire(array(12, 34)); // WHERE id_contrat_titulaire IN (12, 34)
     * $query->filterByIdContratTitulaire(array('min' => 12)); // WHERE id_contrat_titulaire >= 12
     * $query->filterByIdContratTitulaire(array('max' => 12)); // WHERE id_contrat_titulaire <= 12
     * </code>
     *
     * @see       filterByCommonTContratTitulaire()
     *
     * @param     mixed $idContratTitulaire The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByIdContratTitulaire($idContratTitulaire = null, $comparison = null)
    {
        if (is_array($idContratTitulaire)) {
            $useMinMax = false;
            if (isset($idContratTitulaire['min'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idContratTitulaire['max'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the old_service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldServiceId(1234); // WHERE old_service_id = 1234
     * $query->filterByOldServiceId(array(12, 34)); // WHERE old_service_id IN (12, 34)
     * $query->filterByOldServiceId(array('min' => 12)); // WHERE old_service_id >= 12
     * $query->filterByOldServiceId(array('max' => 12)); // WHERE old_service_id <= 12
     * </code>
     *
     * @param     mixed $oldServiceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByOldServiceId($oldServiceId = null, $comparison = null)
    {
        if (is_array($oldServiceId)) {
            $useMinMax = false;
            if (isset($oldServiceId['min'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::OLD_SERVICE_ID, $oldServiceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldServiceId['max'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::OLD_SERVICE_ID, $oldServiceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::OLD_SERVICE_ID, $oldServiceId, $comparison);
    }

    /**
     * Filter the query on the id_type_procedure column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeProcedure(1234); // WHERE id_type_procedure = 1234
     * $query->filterByIdTypeProcedure(array(12, 34)); // WHERE id_type_procedure IN (12, 34)
     * $query->filterByIdTypeProcedure(array('min' => 12)); // WHERE id_type_procedure >= 12
     * $query->filterByIdTypeProcedure(array('max' => 12)); // WHERE id_type_procedure <= 12
     * </code>
     *
     * @param     mixed $idTypeProcedure The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByIdTypeProcedure($idTypeProcedure = null, $comparison = null)
    {
        if (is_array($idTypeProcedure)) {
            $useMinMax = false;
            if (isset($idTypeProcedure['min'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_TYPE_PROCEDURE, $idTypeProcedure['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeProcedure['max'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_TYPE_PROCEDURE, $idTypeProcedure['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_TYPE_PROCEDURE, $idTypeProcedure, $comparison);
    }

    /**
     * Filter the query on the libelle_type_procedure column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleTypeProcedure('fooValue');   // WHERE libelle_type_procedure = 'fooValue'
     * $query->filterByLibelleTypeProcedure('%fooValue%'); // WHERE libelle_type_procedure LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleTypeProcedure The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByLibelleTypeProcedure($libelleTypeProcedure = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleTypeProcedure)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleTypeProcedure)) {
                $libelleTypeProcedure = str_replace('*', '%', $libelleTypeProcedure);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::LIBELLE_TYPE_PROCEDURE, $libelleTypeProcedure, $comparison);
    }

    /**
     * Filter the query on the nbre_offres_recues column
     *
     * Example usage:
     * <code>
     * $query->filterByNbreOffresRecues(1234); // WHERE nbre_offres_recues = 1234
     * $query->filterByNbreOffresRecues(array(12, 34)); // WHERE nbre_offres_recues IN (12, 34)
     * $query->filterByNbreOffresRecues(array('min' => 12)); // WHERE nbre_offres_recues >= 12
     * $query->filterByNbreOffresRecues(array('max' => 12)); // WHERE nbre_offres_recues <= 12
     * </code>
     *
     * @param     mixed $nbreOffresRecues The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByNbreOffresRecues($nbreOffresRecues = null, $comparison = null)
    {
        if (is_array($nbreOffresRecues)) {
            $useMinMax = false;
            if (isset($nbreOffresRecues['min'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::NBRE_OFFRES_RECUES, $nbreOffresRecues['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbreOffresRecues['max'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::NBRE_OFFRES_RECUES, $nbreOffresRecues['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::NBRE_OFFRES_RECUES, $nbreOffresRecues, $comparison);
    }

    /**
     * Filter the query on the nbre_offres_dematerialisees column
     *
     * Example usage:
     * <code>
     * $query->filterByNbreOffresDematerialisees(1234); // WHERE nbre_offres_dematerialisees = 1234
     * $query->filterByNbreOffresDematerialisees(array(12, 34)); // WHERE nbre_offres_dematerialisees IN (12, 34)
     * $query->filterByNbreOffresDematerialisees(array('min' => 12)); // WHERE nbre_offres_dematerialisees >= 12
     * $query->filterByNbreOffresDematerialisees(array('max' => 12)); // WHERE nbre_offres_dematerialisees <= 12
     * </code>
     *
     * @param     mixed $nbreOffresDematerialisees The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByNbreOffresDematerialisees($nbreOffresDematerialisees = null, $comparison = null)
    {
        if (is_array($nbreOffresDematerialisees)) {
            $useMinMax = false;
            if (isset($nbreOffresDematerialisees['min'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::NBRE_OFFRES_DEMATERIALISEES, $nbreOffresDematerialisees['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbreOffresDematerialisees['max'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::NBRE_OFFRES_DEMATERIALISEES, $nbreOffresDematerialisees['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::NBRE_OFFRES_DEMATERIALISEES, $nbreOffresDematerialisees, $comparison);
    }

    /**
     * Filter the query on the signature_offre column
     *
     * Example usage:
     * <code>
     * $query->filterBySignatureOffre('fooValue');   // WHERE signature_offre = 'fooValue'
     * $query->filterBySignatureOffre('%fooValue%'); // WHERE signature_offre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $signatureOffre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterBySignatureOffre($signatureOffre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($signatureOffre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $signatureOffre)) {
                $signatureOffre = str_replace('*', '%', $signatureOffre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::SIGNATURE_OFFRE, $signatureOffre, $comparison);
    }

    /**
     * Filter the query on the service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceId(1234); // WHERE service_id = 1234
     * $query->filterByServiceId(array(12, 34)); // WHERE service_id IN (12, 34)
     * $query->filterByServiceId(array('min' => 12)); // WHERE service_id >= 12
     * $query->filterByServiceId(array('max' => 12)); // WHERE service_id <= 12
     * </code>
     *
     * @param     mixed $serviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByServiceId($serviceId = null, $comparison = null)
    {
        if (is_array($serviceId)) {
            $useMinMax = false;
            if (isset($serviceId['min'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::SERVICE_ID, $serviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceId['max'])) {
                $this->addUsingAlias(CommonTDonneesConsultationPeer::SERVICE_ID, $serviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::SERVICE_ID, $serviceId, $comparison);
    }

    /**
     * Filter the query on the uuid_externe_exec column
     *
     * Example usage:
     * <code>
     * $query->filterByUuidExterneExec('fooValue');   // WHERE uuid_externe_exec = 'fooValue'
     * $query->filterByUuidExterneExec('%fooValue%'); // WHERE uuid_externe_exec LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uuidExterneExec The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function filterByUuidExterneExec($uuidExterneExec = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuidExterneExec)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $uuidExterneExec)) {
                $uuidExterneExec = str_replace('*', '%', $uuidExterneExec);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDonneesConsultationPeer::UUID_EXTERNE_EXEC, $uuidExterneExec, $comparison);
    }

    /**
     * Filter the query by a related CommonTContratTitulaire object
     *
     * @param   CommonTContratTitulaire|PropelObjectCollection $commonTContratTitulaire The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTDonneesConsultationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContratTitulaire($commonTContratTitulaire, $comparison = null)
    {
        if ($commonTContratTitulaire instanceof CommonTContratTitulaire) {
            return $this
                ->addUsingAlias(CommonTDonneesConsultationPeer::ID_CONTRAT_TITULAIRE, $commonTContratTitulaire->getIdContratTitulaire(), $comparison);
        } elseif ($commonTContratTitulaire instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTDonneesConsultationPeer::ID_CONTRAT_TITULAIRE, $commonTContratTitulaire->toKeyValue('PrimaryKey', 'IdContratTitulaire'), $comparison);
        } else {
            throw new PropelException('filterByCommonTContratTitulaire() only accepts arguments of type CommonTContratTitulaire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContratTitulaire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function joinCommonTContratTitulaire($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContratTitulaire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContratTitulaire');
        }

        return $this;
    }

    /**
     * Use the CommonTContratTitulaire relation CommonTContratTitulaire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContratTitulaireQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContratTitulaireQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTContratTitulaire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContratTitulaire', '\Application\Propel\Mpe\CommonTContratTitulaireQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTDonneesConsultation $commonTDonneesConsultation Object to remove from the list of results
     *
     * @return CommonTDonneesConsultationQuery The current query, for fluid interface
     */
    public function prune($commonTDonneesConsultation = null)
    {
        if ($commonTDonneesConsultation) {
            $this->addUsingAlias(CommonTDonneesConsultationPeer::ID_DONNEES_CONSULTATION, $commonTDonneesConsultation->getIdDonneesConsultation(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
