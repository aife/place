<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonBlobFile;
use Application\Propel\Mpe\CommonBlobFilePeer;
use Application\Propel\Mpe\CommonBlobFileQuery;
use Application\Propel\Mpe\CommonDossierVolumineux;

/**
 * Base class that represents a query for the 'blob_file' table.
 *
 *
 *
 * @method CommonBlobFileQuery orderByOldId($order = Criteria::ASC) Order by the old_id column
 * @method CommonBlobFileQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method CommonBlobFileQuery orderByDeletionDatetime($order = Criteria::ASC) Order by the deletion_datetime column
 * @method CommonBlobFileQuery orderByChemin($order = Criteria::ASC) Order by the chemin column
 * @method CommonBlobFileQuery orderByDossier($order = Criteria::ASC) Order by the dossier column
 * @method CommonBlobFileQuery orderByStatutSynchro($order = Criteria::ASC) Order by the statut_synchro column
 * @method CommonBlobFileQuery orderByHash($order = Criteria::ASC) Order by the hash column
 * @method CommonBlobFileQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonBlobFileQuery orderByExtension($order = Criteria::ASC) Order by the extension column
 *
 * @method CommonBlobFileQuery groupByOldId() Group by the old_id column
 * @method CommonBlobFileQuery groupByName() Group by the name column
 * @method CommonBlobFileQuery groupByDeletionDatetime() Group by the deletion_datetime column
 * @method CommonBlobFileQuery groupByChemin() Group by the chemin column
 * @method CommonBlobFileQuery groupByDossier() Group by the dossier column
 * @method CommonBlobFileQuery groupByStatutSynchro() Group by the statut_synchro column
 * @method CommonBlobFileQuery groupByHash() Group by the hash column
 * @method CommonBlobFileQuery groupById() Group by the id column
 * @method CommonBlobFileQuery groupByExtension() Group by the extension column
 *
 * @method CommonBlobFileQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonBlobFileQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonBlobFileQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonBlobFileQuery leftJoinCommonDossierVolumineuxRelatedByIdBlobDescripteur($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonDossierVolumineuxRelatedByIdBlobDescripteur relation
 * @method CommonBlobFileQuery rightJoinCommonDossierVolumineuxRelatedByIdBlobDescripteur($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonDossierVolumineuxRelatedByIdBlobDescripteur relation
 * @method CommonBlobFileQuery innerJoinCommonDossierVolumineuxRelatedByIdBlobDescripteur($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonDossierVolumineuxRelatedByIdBlobDescripteur relation
 *
 * @method CommonBlobFileQuery leftJoinCommonDossierVolumineuxRelatedByIdBlobLogfile($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonDossierVolumineuxRelatedByIdBlobLogfile relation
 * @method CommonBlobFileQuery rightJoinCommonDossierVolumineuxRelatedByIdBlobLogfile($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonDossierVolumineuxRelatedByIdBlobLogfile relation
 * @method CommonBlobFileQuery innerJoinCommonDossierVolumineuxRelatedByIdBlobLogfile($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonDossierVolumineuxRelatedByIdBlobLogfile relation
 *
 * @method CommonBlobFile findOne(PropelPDO $con = null) Return the first CommonBlobFile matching the query
 * @method CommonBlobFile findOneOrCreate(PropelPDO $con = null) Return the first CommonBlobFile matching the query, or a new CommonBlobFile object populated from the query conditions when no match is found
 *
 * @method CommonBlobFile findOneByOldId(int $old_id) Return the first CommonBlobFile filtered by the old_id column
 * @method CommonBlobFile findOneByName(string $name) Return the first CommonBlobFile filtered by the name column
 * @method CommonBlobFile findOneByDeletionDatetime(string $deletion_datetime) Return the first CommonBlobFile filtered by the deletion_datetime column
 * @method CommonBlobFile findOneByChemin(string $chemin) Return the first CommonBlobFile filtered by the chemin column
 * @method CommonBlobFile findOneByDossier(string $dossier) Return the first CommonBlobFile filtered by the dossier column
 * @method CommonBlobFile findOneByStatutSynchro(int $statut_synchro) Return the first CommonBlobFile filtered by the statut_synchro column
 * @method CommonBlobFile findOneByHash(string $hash) Return the first CommonBlobFile filtered by the hash column
 * @method CommonBlobFile findOneByExtension(string $extension) Return the first CommonBlobFile filtered by the extension column
 *
 * @method array findByOldId(int $old_id) Return CommonBlobFile objects filtered by the old_id column
 * @method array findByName(string $name) Return CommonBlobFile objects filtered by the name column
 * @method array findByDeletionDatetime(string $deletion_datetime) Return CommonBlobFile objects filtered by the deletion_datetime column
 * @method array findByChemin(string $chemin) Return CommonBlobFile objects filtered by the chemin column
 * @method array findByDossier(string $dossier) Return CommonBlobFile objects filtered by the dossier column
 * @method array findByStatutSynchro(int $statut_synchro) Return CommonBlobFile objects filtered by the statut_synchro column
 * @method array findByHash(string $hash) Return CommonBlobFile objects filtered by the hash column
 * @method array findById(int $id) Return CommonBlobFile objects filtered by the id column
 * @method array findByExtension(string $extension) Return CommonBlobFile objects filtered by the extension column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonBlobFileQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonBlobFileQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonBlobFile', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonBlobFileQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonBlobFileQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonBlobFileQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonBlobFileQuery) {
            return $criteria;
        }
        $query = new CommonBlobFileQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonBlobFile|CommonBlobFile[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonBlobFilePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonBlobFilePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonBlobFile A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonBlobFile A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `old_id`, `name`, `deletion_datetime`, `chemin`, `dossier`, `statut_synchro`, `hash`, `id`, `extension` FROM `blob_file` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonBlobFile();
            $obj->hydrate($row);
            CommonBlobFilePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonBlobFile|CommonBlobFile[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonBlobFile[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonBlobFilePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonBlobFilePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the old_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldId(1234); // WHERE old_id = 1234
     * $query->filterByOldId(array(12, 34)); // WHERE old_id IN (12, 34)
     * $query->filterByOldId(array('min' => 12)); // WHERE old_id >= 12
     * $query->filterByOldId(array('max' => 12)); // WHERE old_id <= 12
     * </code>
     *
     * @param     mixed $oldId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function filterByOldId($oldId = null, $comparison = null)
    {
        if (is_array($oldId)) {
            $useMinMax = false;
            if (isset($oldId['min'])) {
                $this->addUsingAlias(CommonBlobFilePeer::OLD_ID, $oldId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldId['max'])) {
                $this->addUsingAlias(CommonBlobFilePeer::OLD_ID, $oldId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonBlobFilePeer::OLD_ID, $oldId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonBlobFilePeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the deletion_datetime column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletionDatetime('2011-03-14'); // WHERE deletion_datetime = '2011-03-14'
     * $query->filterByDeletionDatetime('now'); // WHERE deletion_datetime = '2011-03-14'
     * $query->filterByDeletionDatetime(array('max' => 'yesterday')); // WHERE deletion_datetime > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletionDatetime The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function filterByDeletionDatetime($deletionDatetime = null, $comparison = null)
    {
        if (is_array($deletionDatetime)) {
            $useMinMax = false;
            if (isset($deletionDatetime['min'])) {
                $this->addUsingAlias(CommonBlobFilePeer::DELETION_DATETIME, $deletionDatetime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletionDatetime['max'])) {
                $this->addUsingAlias(CommonBlobFilePeer::DELETION_DATETIME, $deletionDatetime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonBlobFilePeer::DELETION_DATETIME, $deletionDatetime, $comparison);
    }

    /**
     * Filter the query on the chemin column
     *
     * Example usage:
     * <code>
     * $query->filterByChemin('fooValue');   // WHERE chemin = 'fooValue'
     * $query->filterByChemin('%fooValue%'); // WHERE chemin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chemin The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function filterByChemin($chemin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chemin)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $chemin)) {
                $chemin = str_replace('*', '%', $chemin);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonBlobFilePeer::CHEMIN, $chemin, $comparison);
    }

    /**
     * Filter the query on the dossier column
     *
     * Example usage:
     * <code>
     * $query->filterByDossier('fooValue');   // WHERE dossier = 'fooValue'
     * $query->filterByDossier('%fooValue%'); // WHERE dossier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dossier The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function filterByDossier($dossier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dossier)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dossier)) {
                $dossier = str_replace('*', '%', $dossier);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonBlobFilePeer::DOSSIER, $dossier, $comparison);
    }

    /**
     * Filter the query on the statut_synchro column
     *
     * Example usage:
     * <code>
     * $query->filterByStatutSynchro(1234); // WHERE statut_synchro = 1234
     * $query->filterByStatutSynchro(array(12, 34)); // WHERE statut_synchro IN (12, 34)
     * $query->filterByStatutSynchro(array('min' => 12)); // WHERE statut_synchro >= 12
     * $query->filterByStatutSynchro(array('max' => 12)); // WHERE statut_synchro <= 12
     * </code>
     *
     * @param     mixed $statutSynchro The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function filterByStatutSynchro($statutSynchro = null, $comparison = null)
    {
        if (is_array($statutSynchro)) {
            $useMinMax = false;
            if (isset($statutSynchro['min'])) {
                $this->addUsingAlias(CommonBlobFilePeer::STATUT_SYNCHRO, $statutSynchro['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statutSynchro['max'])) {
                $this->addUsingAlias(CommonBlobFilePeer::STATUT_SYNCHRO, $statutSynchro['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonBlobFilePeer::STATUT_SYNCHRO, $statutSynchro, $comparison);
    }

    /**
     * Filter the query on the hash column
     *
     * Example usage:
     * <code>
     * $query->filterByHash('fooValue');   // WHERE hash = 'fooValue'
     * $query->filterByHash('%fooValue%'); // WHERE hash LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hash The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function filterByHash($hash = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hash)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $hash)) {
                $hash = str_replace('*', '%', $hash);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonBlobFilePeer::HASH, $hash, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonBlobFilePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonBlobFilePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonBlobFilePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the extension column
     *
     * Example usage:
     * <code>
     * $query->filterByExtension('fooValue');   // WHERE extension = 'fooValue'
     * $query->filterByExtension('%fooValue%'); // WHERE extension LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extension The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function filterByExtension($extension = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extension)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $extension)) {
                $extension = str_replace('*', '%', $extension);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonBlobFilePeer::EXTENSION, $extension, $comparison);
    }

    /**
     * Filter the query by a related CommonDossierVolumineux object
     *
     * @param   CommonDossierVolumineux|PropelObjectCollection $commonDossierVolumineux  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonBlobFileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonDossierVolumineuxRelatedByIdBlobDescripteur($commonDossierVolumineux, $comparison = null)
    {
        if ($commonDossierVolumineux instanceof CommonDossierVolumineux) {
            return $this
                ->addUsingAlias(CommonBlobFilePeer::ID, $commonDossierVolumineux->getIdBlobDescripteur(), $comparison);
        } elseif ($commonDossierVolumineux instanceof PropelObjectCollection) {
            return $this
                ->useCommonDossierVolumineuxRelatedByIdBlobDescripteurQuery()
                ->filterByPrimaryKeys($commonDossierVolumineux->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonDossierVolumineuxRelatedByIdBlobDescripteur() only accepts arguments of type CommonDossierVolumineux or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonDossierVolumineuxRelatedByIdBlobDescripteur relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function joinCommonDossierVolumineuxRelatedByIdBlobDescripteur($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonDossierVolumineuxRelatedByIdBlobDescripteur');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonDossierVolumineuxRelatedByIdBlobDescripteur');
        }

        return $this;
    }

    /**
     * Use the CommonDossierVolumineuxRelatedByIdBlobDescripteur relation CommonDossierVolumineux object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonDossierVolumineuxQuery A secondary query class using the current class as primary query
     */
    public function useCommonDossierVolumineuxRelatedByIdBlobDescripteurQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonDossierVolumineuxRelatedByIdBlobDescripteur($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonDossierVolumineuxRelatedByIdBlobDescripteur', '\Application\Propel\Mpe\CommonDossierVolumineuxQuery');
    }

    /**
     * Filter the query by a related CommonDossierVolumineux object
     *
     * @param   CommonDossierVolumineux|PropelObjectCollection $commonDossierVolumineux  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonBlobFileQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonDossierVolumineuxRelatedByIdBlobLogfile($commonDossierVolumineux, $comparison = null)
    {
        if ($commonDossierVolumineux instanceof CommonDossierVolumineux) {
            return $this
                ->addUsingAlias(CommonBlobFilePeer::ID, $commonDossierVolumineux->getIdBlobLogfile(), $comparison);
        } elseif ($commonDossierVolumineux instanceof PropelObjectCollection) {
            return $this
                ->useCommonDossierVolumineuxRelatedByIdBlobLogfileQuery()
                ->filterByPrimaryKeys($commonDossierVolumineux->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonDossierVolumineuxRelatedByIdBlobLogfile() only accepts arguments of type CommonDossierVolumineux or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonDossierVolumineuxRelatedByIdBlobLogfile relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function joinCommonDossierVolumineuxRelatedByIdBlobLogfile($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonDossierVolumineuxRelatedByIdBlobLogfile');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonDossierVolumineuxRelatedByIdBlobLogfile');
        }

        return $this;
    }

    /**
     * Use the CommonDossierVolumineuxRelatedByIdBlobLogfile relation CommonDossierVolumineux object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonDossierVolumineuxQuery A secondary query class using the current class as primary query
     */
    public function useCommonDossierVolumineuxRelatedByIdBlobLogfileQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonDossierVolumineuxRelatedByIdBlobLogfile($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonDossierVolumineuxRelatedByIdBlobLogfile', '\Application\Propel\Mpe\CommonDossierVolumineuxQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonBlobFile $commonBlobFile Object to remove from the list of results
     *
     * @return CommonBlobFileQuery The current query, for fluid interface
     */
    public function prune($commonBlobFile = null)
    {
        if ($commonBlobFile) {
            $this->addUsingAlias(CommonBlobFilePeer::ID, $commonBlobFile->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
