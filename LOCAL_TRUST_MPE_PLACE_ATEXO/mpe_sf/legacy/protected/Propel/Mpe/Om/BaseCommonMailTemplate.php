<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonMailTemplate;
use Application\Propel\Mpe\CommonMailTemplatePeer;
use Application\Propel\Mpe\CommonMailTemplateQuery;
use Application\Propel\Mpe\CommonMailType;
use Application\Propel\Mpe\CommonMailTypeQuery;

/**
 * Base class that represents a row from the 'mail_template' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonMailTemplate extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonMailTemplatePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonMailTemplatePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the mail_type_id field.
     * @var        int
     */
    protected $mail_type_id;

    /**
     * The value for the code field.
     * @var        string
     */
    protected $code;

    /**
     * The value for the objet field.
     * @var        string
     */
    protected $objet;

    /**
     * The value for the corps field.
     * @var        string
     */
    protected $corps;

    /**
     * The value for the ordre_affichage field.
     * @var        int
     */
    protected $ordre_affichage;

    /**
     * The value for the envoi_modalite field.
     * Note: this column has a database default value of: 'AVEC_AR'
     * @var        string
     */
    protected $envoi_modalite;

    /**
     * The value for the envoi_modalite_figee field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $envoi_modalite_figee;

    /**
     * The value for the reponse_attendue field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $reponse_attendue;

    /**
     * The value for the reponse_attendue_figee field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $reponse_attendue_figee;

    /**
     * @var        CommonMailType
     */
    protected $aCommonMailType;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->envoi_modalite = 'AVEC_AR';
        $this->envoi_modalite_figee = false;
        $this->reponse_attendue = false;
        $this->reponse_attendue_figee = false;
    }

    /**
     * Initializes internal state of BaseCommonMailTemplate object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [mail_type_id] column value.
     *
     * @return int
     */
    public function getMailTypeId()
    {

        return $this->mail_type_id;
    }

    /**
     * Get the [code] column value.
     *
     * @return string
     */
    public function getCode()
    {

        return $this->code;
    }

    /**
     * Get the [objet] column value.
     *
     * @return string
     */
    public function getObjet()
    {

        return $this->objet;
    }

    /**
     * Get the [corps] column value.
     *
     * @return string
     */
    public function getCorps()
    {

        return $this->corps;
    }

    /**
     * Get the [ordre_affichage] column value.
     *
     * @return int
     */
    public function getOrdreAffichage()
    {

        return $this->ordre_affichage;
    }

    /**
     * Get the [envoi_modalite] column value.
     *
     * @return string
     */
    public function getEnvoiModalite()
    {

        return $this->envoi_modalite;
    }

    /**
     * Get the [envoi_modalite_figee] column value.
     *
     * @return boolean
     */
    public function getEnvoiModaliteFigee()
    {

        return $this->envoi_modalite_figee;
    }

    /**
     * Get the [reponse_attendue] column value.
     *
     * @return boolean
     */
    public function getReponseAttendue()
    {

        return $this->reponse_attendue;
    }

    /**
     * Get the [reponse_attendue_figee] column value.
     *
     * @return boolean
     */
    public function getReponseAttendueFigee()
    {

        return $this->reponse_attendue_figee;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonMailTemplate The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonMailTemplatePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [mail_type_id] column.
     *
     * @param int $v new value
     * @return CommonMailTemplate The current object (for fluent API support)
     */
    public function setMailTypeId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->mail_type_id !== $v) {
            $this->mail_type_id = $v;
            $this->modifiedColumns[] = CommonMailTemplatePeer::MAIL_TYPE_ID;
        }

        if ($this->aCommonMailType !== null && $this->aCommonMailType->getId() !== $v) {
            $this->aCommonMailType = null;
        }


        return $this;
    } // setMailTypeId()

    /**
     * Set the value of [code] column.
     *
     * @param string $v new value
     * @return CommonMailTemplate The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[] = CommonMailTemplatePeer::CODE;
        }


        return $this;
    } // setCode()

    /**
     * Set the value of [objet] column.
     *
     * @param string $v new value
     * @return CommonMailTemplate The current object (for fluent API support)
     */
    public function setObjet($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->objet !== $v) {
            $this->objet = $v;
            $this->modifiedColumns[] = CommonMailTemplatePeer::OBJET;
        }


        return $this;
    } // setObjet()

    /**
     * Set the value of [corps] column.
     *
     * @param string $v new value
     * @return CommonMailTemplate The current object (for fluent API support)
     */
    public function setCorps($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->corps !== $v) {
            $this->corps = $v;
            $this->modifiedColumns[] = CommonMailTemplatePeer::CORPS;
        }


        return $this;
    } // setCorps()

    /**
     * Set the value of [ordre_affichage] column.
     *
     * @param int $v new value
     * @return CommonMailTemplate The current object (for fluent API support)
     */
    public function setOrdreAffichage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->ordre_affichage !== $v) {
            $this->ordre_affichage = $v;
            $this->modifiedColumns[] = CommonMailTemplatePeer::ORDRE_AFFICHAGE;
        }


        return $this;
    } // setOrdreAffichage()

    /**
     * Set the value of [envoi_modalite] column.
     *
     * @param string $v new value
     * @return CommonMailTemplate The current object (for fluent API support)
     */
    public function setEnvoiModalite($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->envoi_modalite !== $v) {
            $this->envoi_modalite = $v;
            $this->modifiedColumns[] = CommonMailTemplatePeer::ENVOI_MODALITE;
        }


        return $this;
    } // setEnvoiModalite()

    /**
     * Sets the value of the [envoi_modalite_figee] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonMailTemplate The current object (for fluent API support)
     */
    public function setEnvoiModaliteFigee($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->envoi_modalite_figee !== $v) {
            $this->envoi_modalite_figee = $v;
            $this->modifiedColumns[] = CommonMailTemplatePeer::ENVOI_MODALITE_FIGEE;
        }


        return $this;
    } // setEnvoiModaliteFigee()

    /**
     * Sets the value of the [reponse_attendue] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonMailTemplate The current object (for fluent API support)
     */
    public function setReponseAttendue($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->reponse_attendue !== $v) {
            $this->reponse_attendue = $v;
            $this->modifiedColumns[] = CommonMailTemplatePeer::REPONSE_ATTENDUE;
        }


        return $this;
    } // setReponseAttendue()

    /**
     * Sets the value of the [reponse_attendue_figee] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonMailTemplate The current object (for fluent API support)
     */
    public function setReponseAttendueFigee($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->reponse_attendue_figee !== $v) {
            $this->reponse_attendue_figee = $v;
            $this->modifiedColumns[] = CommonMailTemplatePeer::REPONSE_ATTENDUE_FIGEE;
        }


        return $this;
    } // setReponseAttendueFigee()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->envoi_modalite !== 'AVEC_AR') {
                return false;
            }

            if ($this->envoi_modalite_figee !== false) {
                return false;
            }

            if ($this->reponse_attendue !== false) {
                return false;
            }

            if ($this->reponse_attendue_figee !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->mail_type_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->code = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->objet = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->corps = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->ordre_affichage = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->envoi_modalite = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->envoi_modalite_figee = ($row[$startcol + 7] !== null) ? (boolean) $row[$startcol + 7] : null;
            $this->reponse_attendue = ($row[$startcol + 8] !== null) ? (boolean) $row[$startcol + 8] : null;
            $this->reponse_attendue_figee = ($row[$startcol + 9] !== null) ? (boolean) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 10; // 10 = CommonMailTemplatePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonMailTemplate object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonMailType !== null && $this->mail_type_id !== $this->aCommonMailType->getId()) {
            $this->aCommonMailType = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonMailTemplatePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonMailTemplatePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonMailType = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonMailTemplatePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonMailTemplateQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonMailTemplatePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonMailTemplatePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonMailType !== null) {
                if ($this->aCommonMailType->isModified() || $this->aCommonMailType->isNew()) {
                    $affectedRows += $this->aCommonMailType->save($con);
                }
                $this->setCommonMailType($this->aCommonMailType);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonMailTemplatePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonMailTemplatePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonMailTemplatePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonMailTemplatePeer::MAIL_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`mail_type_id`';
        }
        if ($this->isColumnModified(CommonMailTemplatePeer::CODE)) {
            $modifiedColumns[':p' . $index++]  = '`code`';
        }
        if ($this->isColumnModified(CommonMailTemplatePeer::OBJET)) {
            $modifiedColumns[':p' . $index++]  = '`objet`';
        }
        if ($this->isColumnModified(CommonMailTemplatePeer::CORPS)) {
            $modifiedColumns[':p' . $index++]  = '`corps`';
        }
        if ($this->isColumnModified(CommonMailTemplatePeer::ORDRE_AFFICHAGE)) {
            $modifiedColumns[':p' . $index++]  = '`ordre_affichage`';
        }
        if ($this->isColumnModified(CommonMailTemplatePeer::ENVOI_MODALITE)) {
            $modifiedColumns[':p' . $index++]  = '`envoi_modalite`';
        }
        if ($this->isColumnModified(CommonMailTemplatePeer::ENVOI_MODALITE_FIGEE)) {
            $modifiedColumns[':p' . $index++]  = '`envoi_modalite_figee`';
        }
        if ($this->isColumnModified(CommonMailTemplatePeer::REPONSE_ATTENDUE)) {
            $modifiedColumns[':p' . $index++]  = '`reponse_attendue`';
        }
        if ($this->isColumnModified(CommonMailTemplatePeer::REPONSE_ATTENDUE_FIGEE)) {
            $modifiedColumns[':p' . $index++]  = '`reponse_attendue_figee`';
        }

        $sql = sprintf(
            'INSERT INTO `mail_template` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`mail_type_id`':
                        $stmt->bindValue($identifier, $this->mail_type_id, PDO::PARAM_INT);
                        break;
                    case '`code`':
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case '`objet`':
                        $stmt->bindValue($identifier, $this->objet, PDO::PARAM_STR);
                        break;
                    case '`corps`':
                        $stmt->bindValue($identifier, $this->corps, PDO::PARAM_STR);
                        break;
                    case '`ordre_affichage`':
                        $stmt->bindValue($identifier, $this->ordre_affichage, PDO::PARAM_INT);
                        break;
                    case '`envoi_modalite`':
                        $stmt->bindValue($identifier, $this->envoi_modalite, PDO::PARAM_STR);
                        break;
                    case '`envoi_modalite_figee`':
                        $stmt->bindValue($identifier, (int) $this->envoi_modalite_figee, PDO::PARAM_INT);
                        break;
                    case '`reponse_attendue`':
                        $stmt->bindValue($identifier, (int) $this->reponse_attendue, PDO::PARAM_INT);
                        break;
                    case '`reponse_attendue_figee`':
                        $stmt->bindValue($identifier, (int) $this->reponse_attendue_figee, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonMailType !== null) {
                if (!$this->aCommonMailType->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonMailType->getValidationFailures());
                }
            }


            if (($retval = CommonMailTemplatePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonMailTemplatePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getMailTypeId();
                break;
            case 2:
                return $this->getCode();
                break;
            case 3:
                return $this->getObjet();
                break;
            case 4:
                return $this->getCorps();
                break;
            case 5:
                return $this->getOrdreAffichage();
                break;
            case 6:
                return $this->getEnvoiModalite();
                break;
            case 7:
                return $this->getEnvoiModaliteFigee();
                break;
            case 8:
                return $this->getReponseAttendue();
                break;
            case 9:
                return $this->getReponseAttendueFigee();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonMailTemplate'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonMailTemplate'][$this->getPrimaryKey()] = true;
        $keys = CommonMailTemplatePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getMailTypeId(),
            $keys[2] => $this->getCode(),
            $keys[3] => $this->getObjet(),
            $keys[4] => $this->getCorps(),
            $keys[5] => $this->getOrdreAffichage(),
            $keys[6] => $this->getEnvoiModalite(),
            $keys[7] => $this->getEnvoiModaliteFigee(),
            $keys[8] => $this->getReponseAttendue(),
            $keys[9] => $this->getReponseAttendueFigee(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonMailType) {
                $result['CommonMailType'] = $this->aCommonMailType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonMailTemplatePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setMailTypeId($value);
                break;
            case 2:
                $this->setCode($value);
                break;
            case 3:
                $this->setObjet($value);
                break;
            case 4:
                $this->setCorps($value);
                break;
            case 5:
                $this->setOrdreAffichage($value);
                break;
            case 6:
                $this->setEnvoiModalite($value);
                break;
            case 7:
                $this->setEnvoiModaliteFigee($value);
                break;
            case 8:
                $this->setReponseAttendue($value);
                break;
            case 9:
                $this->setReponseAttendueFigee($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonMailTemplatePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setMailTypeId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCode($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setObjet($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setCorps($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setOrdreAffichage($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setEnvoiModalite($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setEnvoiModaliteFigee($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setReponseAttendue($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setReponseAttendueFigee($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonMailTemplatePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonMailTemplatePeer::ID)) $criteria->add(CommonMailTemplatePeer::ID, $this->id);
        if ($this->isColumnModified(CommonMailTemplatePeer::MAIL_TYPE_ID)) $criteria->add(CommonMailTemplatePeer::MAIL_TYPE_ID, $this->mail_type_id);
        if ($this->isColumnModified(CommonMailTemplatePeer::CODE)) $criteria->add(CommonMailTemplatePeer::CODE, $this->code);
        if ($this->isColumnModified(CommonMailTemplatePeer::OBJET)) $criteria->add(CommonMailTemplatePeer::OBJET, $this->objet);
        if ($this->isColumnModified(CommonMailTemplatePeer::CORPS)) $criteria->add(CommonMailTemplatePeer::CORPS, $this->corps);
        if ($this->isColumnModified(CommonMailTemplatePeer::ORDRE_AFFICHAGE)) $criteria->add(CommonMailTemplatePeer::ORDRE_AFFICHAGE, $this->ordre_affichage);
        if ($this->isColumnModified(CommonMailTemplatePeer::ENVOI_MODALITE)) $criteria->add(CommonMailTemplatePeer::ENVOI_MODALITE, $this->envoi_modalite);
        if ($this->isColumnModified(CommonMailTemplatePeer::ENVOI_MODALITE_FIGEE)) $criteria->add(CommonMailTemplatePeer::ENVOI_MODALITE_FIGEE, $this->envoi_modalite_figee);
        if ($this->isColumnModified(CommonMailTemplatePeer::REPONSE_ATTENDUE)) $criteria->add(CommonMailTemplatePeer::REPONSE_ATTENDUE, $this->reponse_attendue);
        if ($this->isColumnModified(CommonMailTemplatePeer::REPONSE_ATTENDUE_FIGEE)) $criteria->add(CommonMailTemplatePeer::REPONSE_ATTENDUE_FIGEE, $this->reponse_attendue_figee);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonMailTemplatePeer::DATABASE_NAME);
        $criteria->add(CommonMailTemplatePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonMailTemplate (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setMailTypeId($this->getMailTypeId());
        $copyObj->setCode($this->getCode());
        $copyObj->setObjet($this->getObjet());
        $copyObj->setCorps($this->getCorps());
        $copyObj->setOrdreAffichage($this->getOrdreAffichage());
        $copyObj->setEnvoiModalite($this->getEnvoiModalite());
        $copyObj->setEnvoiModaliteFigee($this->getEnvoiModaliteFigee());
        $copyObj->setReponseAttendue($this->getReponseAttendue());
        $copyObj->setReponseAttendueFigee($this->getReponseAttendueFigee());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonMailTemplate Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonMailTemplatePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonMailTemplatePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonMailType object.
     *
     * @param   CommonMailType $v
     * @return CommonMailTemplate The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonMailType(CommonMailType $v = null)
    {
        if ($v === null) {
            $this->setMailTypeId(NULL);
        } else {
            $this->setMailTypeId($v->getId());
        }

        $this->aCommonMailType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonMailType object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonMailTemplate($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonMailType object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonMailType The associated CommonMailType object.
     * @throws PropelException
     */
    public function getCommonMailType(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonMailType === null && ($this->mail_type_id !== null) && $doQuery) {
            $this->aCommonMailType = CommonMailTypeQuery::create()->findPk($this->mail_type_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonMailType->addCommonMailTemplates($this);
             */
        }

        return $this->aCommonMailType;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->mail_type_id = null;
        $this->code = null;
        $this->objet = null;
        $this->corps = null;
        $this->ordre_affichage = null;
        $this->envoi_modalite = null;
        $this->envoi_modalite_figee = null;
        $this->reponse_attendue = null;
        $this->reponse_attendue_figee = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aCommonMailType instanceof Persistent) {
              $this->aCommonMailType->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aCommonMailType = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonMailTemplatePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
