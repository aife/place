<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTRechercheSauvegardee;
use Application\Propel\Mpe\CommonTRechercheSauvegardeePeer;
use Application\Propel\Mpe\CommonTRechercheSauvegardeeQuery;

/**
 * Base class that represents a query for the 'T_Recherche_Sauvegardee' table.
 *
 *
 *
 * @method CommonTRechercheSauvegardeeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTRechercheSauvegardeeQuery orderByIdInscrit($order = Criteria::ASC) Order by the id_inscrit column
 * @method CommonTRechercheSauvegardeeQuery orderByDenomination($order = Criteria::ASC) Order by the denomination column
 * @method CommonTRechercheSauvegardeeQuery orderByPeriodicite($order = Criteria::ASC) Order by the periodicite column
 * @method CommonTRechercheSauvegardeeQuery orderByXmlcriteria($order = Criteria::ASC) Order by the xmlCriteria column
 * @method CommonTRechercheSauvegardeeQuery orderByCategorie($order = Criteria::ASC) Order by the categorie column
 * @method CommonTRechercheSauvegardeeQuery orderByIdInitial($order = Criteria::ASC) Order by the id_initial column
 * @method CommonTRechercheSauvegardeeQuery orderByFormat($order = Criteria::ASC) Order by the format column
 *
 * @method CommonTRechercheSauvegardeeQuery groupById() Group by the id column
 * @method CommonTRechercheSauvegardeeQuery groupByIdInscrit() Group by the id_inscrit column
 * @method CommonTRechercheSauvegardeeQuery groupByDenomination() Group by the denomination column
 * @method CommonTRechercheSauvegardeeQuery groupByPeriodicite() Group by the periodicite column
 * @method CommonTRechercheSauvegardeeQuery groupByXmlcriteria() Group by the xmlCriteria column
 * @method CommonTRechercheSauvegardeeQuery groupByCategorie() Group by the categorie column
 * @method CommonTRechercheSauvegardeeQuery groupByIdInitial() Group by the id_initial column
 * @method CommonTRechercheSauvegardeeQuery groupByFormat() Group by the format column
 *
 * @method CommonTRechercheSauvegardeeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTRechercheSauvegardeeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTRechercheSauvegardeeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTRechercheSauvegardee findOne(PropelPDO $con = null) Return the first CommonTRechercheSauvegardee matching the query
 * @method CommonTRechercheSauvegardee findOneOrCreate(PropelPDO $con = null) Return the first CommonTRechercheSauvegardee matching the query, or a new CommonTRechercheSauvegardee object populated from the query conditions when no match is found
 *
 * @method CommonTRechercheSauvegardee findOneByIdInscrit(int $id_inscrit) Return the first CommonTRechercheSauvegardee filtered by the id_inscrit column
 * @method CommonTRechercheSauvegardee findOneByDenomination(string $denomination) Return the first CommonTRechercheSauvegardee filtered by the denomination column
 * @method CommonTRechercheSauvegardee findOneByPeriodicite(string $periodicite) Return the first CommonTRechercheSauvegardee filtered by the periodicite column
 * @method CommonTRechercheSauvegardee findOneByXmlcriteria(string $xmlCriteria) Return the first CommonTRechercheSauvegardee filtered by the xmlCriteria column
 * @method CommonTRechercheSauvegardee findOneByCategorie(string $categorie) Return the first CommonTRechercheSauvegardee filtered by the categorie column
 * @method CommonTRechercheSauvegardee findOneByIdInitial(int $id_initial) Return the first CommonTRechercheSauvegardee filtered by the id_initial column
 * @method CommonTRechercheSauvegardee findOneByFormat(string $format) Return the first CommonTRechercheSauvegardee filtered by the format column
 *
 * @method array findById(int $id) Return CommonTRechercheSauvegardee objects filtered by the id column
 * @method array findByIdInscrit(int $id_inscrit) Return CommonTRechercheSauvegardee objects filtered by the id_inscrit column
 * @method array findByDenomination(string $denomination) Return CommonTRechercheSauvegardee objects filtered by the denomination column
 * @method array findByPeriodicite(string $periodicite) Return CommonTRechercheSauvegardee objects filtered by the periodicite column
 * @method array findByXmlcriteria(string $xmlCriteria) Return CommonTRechercheSauvegardee objects filtered by the xmlCriteria column
 * @method array findByCategorie(string $categorie) Return CommonTRechercheSauvegardee objects filtered by the categorie column
 * @method array findByIdInitial(int $id_initial) Return CommonTRechercheSauvegardee objects filtered by the id_initial column
 * @method array findByFormat(string $format) Return CommonTRechercheSauvegardee objects filtered by the format column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTRechercheSauvegardeeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTRechercheSauvegardeeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTRechercheSauvegardee', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTRechercheSauvegardeeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTRechercheSauvegardeeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTRechercheSauvegardeeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTRechercheSauvegardeeQuery) {
            return $criteria;
        }
        $query = new CommonTRechercheSauvegardeeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTRechercheSauvegardee|CommonTRechercheSauvegardee[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTRechercheSauvegardeePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTRechercheSauvegardeePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTRechercheSauvegardee A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTRechercheSauvegardee A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_inscrit`, `denomination`, `periodicite`, `xmlCriteria`, `categorie`, `id_initial`, `format` FROM `T_Recherche_Sauvegardee` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTRechercheSauvegardee();
            $obj->hydrate($row);
            CommonTRechercheSauvegardeePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTRechercheSauvegardee|CommonTRechercheSauvegardee[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTRechercheSauvegardee[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTRechercheSauvegardeeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTRechercheSauvegardeeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTRechercheSauvegardeeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInscrit(1234); // WHERE id_inscrit = 1234
     * $query->filterByIdInscrit(array(12, 34)); // WHERE id_inscrit IN (12, 34)
     * $query->filterByIdInscrit(array('min' => 12)); // WHERE id_inscrit >= 12
     * $query->filterByIdInscrit(array('max' => 12)); // WHERE id_inscrit <= 12
     * </code>
     *
     * @param     mixed $idInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTRechercheSauvegardeeQuery The current query, for fluid interface
     */
    public function filterByIdInscrit($idInscrit = null, $comparison = null)
    {
        if (is_array($idInscrit)) {
            $useMinMax = false;
            if (isset($idInscrit['min'])) {
                $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID_INSCRIT, $idInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInscrit['max'])) {
                $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID_INSCRIT, $idInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID_INSCRIT, $idInscrit, $comparison);
    }

    /**
     * Filter the query on the denomination column
     *
     * Example usage:
     * <code>
     * $query->filterByDenomination('fooValue');   // WHERE denomination = 'fooValue'
     * $query->filterByDenomination('%fooValue%'); // WHERE denomination LIKE '%fooValue%'
     * </code>
     *
     * @param     string $denomination The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTRechercheSauvegardeeQuery The current query, for fluid interface
     */
    public function filterByDenomination($denomination = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($denomination)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $denomination)) {
                $denomination = str_replace('*', '%', $denomination);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTRechercheSauvegardeePeer::DENOMINATION, $denomination, $comparison);
    }

    /**
     * Filter the query on the periodicite column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriodicite('fooValue');   // WHERE periodicite = 'fooValue'
     * $query->filterByPeriodicite('%fooValue%'); // WHERE periodicite LIKE '%fooValue%'
     * </code>
     *
     * @param     string $periodicite The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTRechercheSauvegardeeQuery The current query, for fluid interface
     */
    public function filterByPeriodicite($periodicite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($periodicite)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $periodicite)) {
                $periodicite = str_replace('*', '%', $periodicite);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTRechercheSauvegardeePeer::PERIODICITE, $periodicite, $comparison);
    }

    /**
     * Filter the query on the xmlCriteria column
     *
     * Example usage:
     * <code>
     * $query->filterByXmlcriteria('fooValue');   // WHERE xmlCriteria = 'fooValue'
     * $query->filterByXmlcriteria('%fooValue%'); // WHERE xmlCriteria LIKE '%fooValue%'
     * </code>
     *
     * @param     string $xmlcriteria The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTRechercheSauvegardeeQuery The current query, for fluid interface
     */
    public function filterByXmlcriteria($xmlcriteria = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($xmlcriteria)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $xmlcriteria)) {
                $xmlcriteria = str_replace('*', '%', $xmlcriteria);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTRechercheSauvegardeePeer::XMLCRITERIA, $xmlcriteria, $comparison);
    }

    /**
     * Filter the query on the categorie column
     *
     * Example usage:
     * <code>
     * $query->filterByCategorie('fooValue');   // WHERE categorie = 'fooValue'
     * $query->filterByCategorie('%fooValue%'); // WHERE categorie LIKE '%fooValue%'
     * </code>
     *
     * @param     string $categorie The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTRechercheSauvegardeeQuery The current query, for fluid interface
     */
    public function filterByCategorie($categorie = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($categorie)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $categorie)) {
                $categorie = str_replace('*', '%', $categorie);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTRechercheSauvegardeePeer::CATEGORIE, $categorie, $comparison);
    }

    /**
     * Filter the query on the id_initial column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInitial(1234); // WHERE id_initial = 1234
     * $query->filterByIdInitial(array(12, 34)); // WHERE id_initial IN (12, 34)
     * $query->filterByIdInitial(array('min' => 12)); // WHERE id_initial >= 12
     * $query->filterByIdInitial(array('max' => 12)); // WHERE id_initial <= 12
     * </code>
     *
     * @param     mixed $idInitial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTRechercheSauvegardeeQuery The current query, for fluid interface
     */
    public function filterByIdInitial($idInitial = null, $comparison = null)
    {
        if (is_array($idInitial)) {
            $useMinMax = false;
            if (isset($idInitial['min'])) {
                $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID_INITIAL, $idInitial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInitial['max'])) {
                $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID_INITIAL, $idInitial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID_INITIAL, $idInitial, $comparison);
    }

    /**
     * Filter the query on the format column
     *
     * Example usage:
     * <code>
     * $query->filterByFormat('fooValue');   // WHERE format = 'fooValue'
     * $query->filterByFormat('%fooValue%'); // WHERE format LIKE '%fooValue%'
     * </code>
     *
     * @param     string $format The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTRechercheSauvegardeeQuery The current query, for fluid interface
     */
    public function filterByFormat($format = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($format)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $format)) {
                $format = str_replace('*', '%', $format);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTRechercheSauvegardeePeer::FORMAT, $format, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTRechercheSauvegardee $commonTRechercheSauvegardee Object to remove from the list of results
     *
     * @return CommonTRechercheSauvegardeeQuery The current query, for fluid interface
     */
    public function prune($commonTRechercheSauvegardee = null)
    {
        if ($commonTRechercheSauvegardee) {
            $this->addUsingAlias(CommonTRechercheSauvegardeePeer::ID, $commonTRechercheSauvegardee->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
