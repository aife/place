<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonModificationContrat;
use Application\Propel\Mpe\CommonModificationContratPeer;
use Application\Propel\Mpe\CommonModificationContratQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTEtablissement;

/**
 * Base class that represents a query for the 'modification_contrat' table.
 *
 *
 *
 * @method CommonModificationContratQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonModificationContratQuery orderByIdContratTitulaire($order = Criteria::ASC) Order by the id_contrat_titulaire column
 * @method CommonModificationContratQuery orderByNumOrdre($order = Criteria::ASC) Order by the num_ordre column
 * @method CommonModificationContratQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonModificationContratQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonModificationContratQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 * @method CommonModificationContratQuery orderByObjetModification($order = Criteria::ASC) Order by the objet_modification column
 * @method CommonModificationContratQuery orderByDateSignature($order = Criteria::ASC) Order by the date_signature column
 * @method CommonModificationContratQuery orderByMontant($order = Criteria::ASC) Order by the montant column
 * @method CommonModificationContratQuery orderByIdEtablissement($order = Criteria::ASC) Order by the id_etablissement column
 * @method CommonModificationContratQuery orderByDureeMarche($order = Criteria::ASC) Order by the duree_marche column
 * @method CommonModificationContratQuery orderByStatutPublicationSn($order = Criteria::ASC) Order by the statut_publication_sn column
 * @method CommonModificationContratQuery orderByDatePublicationSn($order = Criteria::ASC) Order by the date_publication_sn column
 * @method CommonModificationContratQuery orderByErreurSn($order = Criteria::ASC) Order by the erreur_sn column
 * @method CommonModificationContratQuery orderByDateModificationSn($order = Criteria::ASC) Order by the date_modification_sn column
 *
 * @method CommonModificationContratQuery groupById() Group by the id column
 * @method CommonModificationContratQuery groupByIdContratTitulaire() Group by the id_contrat_titulaire column
 * @method CommonModificationContratQuery groupByNumOrdre() Group by the num_ordre column
 * @method CommonModificationContratQuery groupByDateCreation() Group by the date_creation column
 * @method CommonModificationContratQuery groupByDateModification() Group by the date_modification column
 * @method CommonModificationContratQuery groupByIdAgent() Group by the id_agent column
 * @method CommonModificationContratQuery groupByObjetModification() Group by the objet_modification column
 * @method CommonModificationContratQuery groupByDateSignature() Group by the date_signature column
 * @method CommonModificationContratQuery groupByMontant() Group by the montant column
 * @method CommonModificationContratQuery groupByIdEtablissement() Group by the id_etablissement column
 * @method CommonModificationContratQuery groupByDureeMarche() Group by the duree_marche column
 * @method CommonModificationContratQuery groupByStatutPublicationSn() Group by the statut_publication_sn column
 * @method CommonModificationContratQuery groupByDatePublicationSn() Group by the date_publication_sn column
 * @method CommonModificationContratQuery groupByErreurSn() Group by the erreur_sn column
 * @method CommonModificationContratQuery groupByDateModificationSn() Group by the date_modification_sn column
 *
 * @method CommonModificationContratQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonModificationContratQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonModificationContratQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonModificationContratQuery leftJoinCommonAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAgent relation
 * @method CommonModificationContratQuery rightJoinCommonAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAgent relation
 * @method CommonModificationContratQuery innerJoinCommonAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAgent relation
 *
 * @method CommonModificationContratQuery leftJoinCommonTContratTitulaire($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonModificationContratQuery rightJoinCommonTContratTitulaire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonModificationContratQuery innerJoinCommonTContratTitulaire($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContratTitulaire relation
 *
 * @method CommonModificationContratQuery leftJoinCommonTEtablissement($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTEtablissement relation
 * @method CommonModificationContratQuery rightJoinCommonTEtablissement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTEtablissement relation
 * @method CommonModificationContratQuery innerJoinCommonTEtablissement($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTEtablissement relation
 *
 * @method CommonModificationContrat findOne(PropelPDO $con = null) Return the first CommonModificationContrat matching the query
 * @method CommonModificationContrat findOneOrCreate(PropelPDO $con = null) Return the first CommonModificationContrat matching the query, or a new CommonModificationContrat object populated from the query conditions when no match is found
 *
 * @method CommonModificationContrat findOneByIdContratTitulaire(int $id_contrat_titulaire) Return the first CommonModificationContrat filtered by the id_contrat_titulaire column
 * @method CommonModificationContrat findOneByNumOrdre(int $num_ordre) Return the first CommonModificationContrat filtered by the num_ordre column
 * @method CommonModificationContrat findOneByDateCreation(string $date_creation) Return the first CommonModificationContrat filtered by the date_creation column
 * @method CommonModificationContrat findOneByDateModification(string $date_modification) Return the first CommonModificationContrat filtered by the date_modification column
 * @method CommonModificationContrat findOneByIdAgent(int $id_agent) Return the first CommonModificationContrat filtered by the id_agent column
 * @method CommonModificationContrat findOneByObjetModification(string $objet_modification) Return the first CommonModificationContrat filtered by the objet_modification column
 * @method CommonModificationContrat findOneByDateSignature(string $date_signature) Return the first CommonModificationContrat filtered by the date_signature column
 * @method CommonModificationContrat findOneByMontant(string $montant) Return the first CommonModificationContrat filtered by the montant column
 * @method CommonModificationContrat findOneByIdEtablissement(int $id_etablissement) Return the first CommonModificationContrat filtered by the id_etablissement column
 * @method CommonModificationContrat findOneByDureeMarche(int $duree_marche) Return the first CommonModificationContrat filtered by the duree_marche column
 * @method CommonModificationContrat findOneByStatutPublicationSn(int $statut_publication_sn) Return the first CommonModificationContrat filtered by the statut_publication_sn column
 * @method CommonModificationContrat findOneByDatePublicationSn(string $date_publication_sn) Return the first CommonModificationContrat filtered by the date_publication_sn column
 * @method CommonModificationContrat findOneByErreurSn(string $erreur_sn) Return the first CommonModificationContrat filtered by the erreur_sn column
 * @method CommonModificationContrat findOneByDateModificationSn(string $date_modification_sn) Return the first CommonModificationContrat filtered by the date_modification_sn column
 *
 * @method array findById(int $id) Return CommonModificationContrat objects filtered by the id column
 * @method array findByIdContratTitulaire(int $id_contrat_titulaire) Return CommonModificationContrat objects filtered by the id_contrat_titulaire column
 * @method array findByNumOrdre(int $num_ordre) Return CommonModificationContrat objects filtered by the num_ordre column
 * @method array findByDateCreation(string $date_creation) Return CommonModificationContrat objects filtered by the date_creation column
 * @method array findByDateModification(string $date_modification) Return CommonModificationContrat objects filtered by the date_modification column
 * @method array findByIdAgent(int $id_agent) Return CommonModificationContrat objects filtered by the id_agent column
 * @method array findByObjetModification(string $objet_modification) Return CommonModificationContrat objects filtered by the objet_modification column
 * @method array findByDateSignature(string $date_signature) Return CommonModificationContrat objects filtered by the date_signature column
 * @method array findByMontant(string $montant) Return CommonModificationContrat objects filtered by the montant column
 * @method array findByIdEtablissement(int $id_etablissement) Return CommonModificationContrat objects filtered by the id_etablissement column
 * @method array findByDureeMarche(int $duree_marche) Return CommonModificationContrat objects filtered by the duree_marche column
 * @method array findByStatutPublicationSn(int $statut_publication_sn) Return CommonModificationContrat objects filtered by the statut_publication_sn column
 * @method array findByDatePublicationSn(string $date_publication_sn) Return CommonModificationContrat objects filtered by the date_publication_sn column
 * @method array findByErreurSn(string $erreur_sn) Return CommonModificationContrat objects filtered by the erreur_sn column
 * @method array findByDateModificationSn(string $date_modification_sn) Return CommonModificationContrat objects filtered by the date_modification_sn column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonModificationContratQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonModificationContratQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonModificationContrat', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonModificationContratQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonModificationContratQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonModificationContratQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonModificationContratQuery) {
            return $criteria;
        }
        $query = new CommonModificationContratQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonModificationContrat|CommonModificationContrat[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonModificationContratPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonModificationContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonModificationContrat A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonModificationContrat A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_contrat_titulaire`, `num_ordre`, `date_creation`, `date_modification`, `id_agent`, `objet_modification`, `date_signature`, `montant`, `id_etablissement`, `duree_marche`, `statut_publication_sn`, `date_publication_sn`, `erreur_sn`, `date_modification_sn` FROM `modification_contrat` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonModificationContrat();
            $obj->hydrate($row);
            CommonModificationContratPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonModificationContrat|CommonModificationContrat[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonModificationContrat[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonModificationContratPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonModificationContratPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_contrat_titulaire column
     *
     * Example usage:
     * <code>
     * $query->filterByIdContratTitulaire(1234); // WHERE id_contrat_titulaire = 1234
     * $query->filterByIdContratTitulaire(array(12, 34)); // WHERE id_contrat_titulaire IN (12, 34)
     * $query->filterByIdContratTitulaire(array('min' => 12)); // WHERE id_contrat_titulaire >= 12
     * $query->filterByIdContratTitulaire(array('max' => 12)); // WHERE id_contrat_titulaire <= 12
     * </code>
     *
     * @see       filterByCommonTContratTitulaire()
     *
     * @param     mixed $idContratTitulaire The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByIdContratTitulaire($idContratTitulaire = null, $comparison = null)
    {
        if (is_array($idContratTitulaire)) {
            $useMinMax = false;
            if (isset($idContratTitulaire['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idContratTitulaire['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire, $comparison);
    }

    /**
     * Filter the query on the num_ordre column
     *
     * Example usage:
     * <code>
     * $query->filterByNumOrdre(1234); // WHERE num_ordre = 1234
     * $query->filterByNumOrdre(array(12, 34)); // WHERE num_ordre IN (12, 34)
     * $query->filterByNumOrdre(array('min' => 12)); // WHERE num_ordre >= 12
     * $query->filterByNumOrdre(array('max' => 12)); // WHERE num_ordre <= 12
     * </code>
     *
     * @param     mixed $numOrdre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByNumOrdre($numOrdre = null, $comparison = null)
    {
        if (is_array($numOrdre)) {
            $useMinMax = false;
            if (isset($numOrdre['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::NUM_ORDRE, $numOrdre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numOrdre['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::NUM_ORDRE, $numOrdre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::NUM_ORDRE, $numOrdre, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('2011-03-14'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification('now'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification(array('max' => 'yesterday')); // WHERE date_modification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (is_array($dateModification)) {
            $useMinMax = false;
            if (isset($dateModification['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DATE_MODIFICATION, $dateModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModification['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DATE_MODIFICATION, $dateModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @see       filterByCommonAgent()
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Filter the query on the objet_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByObjetModification('fooValue');   // WHERE objet_modification = 'fooValue'
     * $query->filterByObjetModification('%fooValue%'); // WHERE objet_modification LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objetModification The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByObjetModification($objetModification = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objetModification)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $objetModification)) {
                $objetModification = str_replace('*', '%', $objetModification);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::OBJET_MODIFICATION, $objetModification, $comparison);
    }

    /**
     * Filter the query on the date_signature column
     *
     * Example usage:
     * <code>
     * $query->filterByDateSignature('2011-03-14'); // WHERE date_signature = '2011-03-14'
     * $query->filterByDateSignature('now'); // WHERE date_signature = '2011-03-14'
     * $query->filterByDateSignature(array('max' => 'yesterday')); // WHERE date_signature > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateSignature The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByDateSignature($dateSignature = null, $comparison = null)
    {
        if (is_array($dateSignature)) {
            $useMinMax = false;
            if (isset($dateSignature['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DATE_SIGNATURE, $dateSignature['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateSignature['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DATE_SIGNATURE, $dateSignature['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::DATE_SIGNATURE, $dateSignature, $comparison);
    }

    /**
     * Filter the query on the montant column
     *
     * Example usage:
     * <code>
     * $query->filterByMontant('fooValue');   // WHERE montant = 'fooValue'
     * $query->filterByMontant('%fooValue%'); // WHERE montant LIKE '%fooValue%'
     * </code>
     *
     * @param     string $montant The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByMontant($montant = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($montant)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $montant)) {
                $montant = str_replace('*', '%', $montant);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::MONTANT, $montant, $comparison);
    }

    /**
     * Filter the query on the id_etablissement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtablissement(1234); // WHERE id_etablissement = 1234
     * $query->filterByIdEtablissement(array(12, 34)); // WHERE id_etablissement IN (12, 34)
     * $query->filterByIdEtablissement(array('min' => 12)); // WHERE id_etablissement >= 12
     * $query->filterByIdEtablissement(array('max' => 12)); // WHERE id_etablissement <= 12
     * </code>
     *
     * @see       filterByCommonTEtablissement()
     *
     * @param     mixed $idEtablissement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByIdEtablissement($idEtablissement = null, $comparison = null)
    {
        if (is_array($idEtablissement)) {
            $useMinMax = false;
            if (isset($idEtablissement['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::ID_ETABLISSEMENT, $idEtablissement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtablissement['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::ID_ETABLISSEMENT, $idEtablissement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::ID_ETABLISSEMENT, $idEtablissement, $comparison);
    }

    /**
     * Filter the query on the duree_marche column
     *
     * Example usage:
     * <code>
     * $query->filterByDureeMarche(1234); // WHERE duree_marche = 1234
     * $query->filterByDureeMarche(array(12, 34)); // WHERE duree_marche IN (12, 34)
     * $query->filterByDureeMarche(array('min' => 12)); // WHERE duree_marche >= 12
     * $query->filterByDureeMarche(array('max' => 12)); // WHERE duree_marche <= 12
     * </code>
     *
     * @param     mixed $dureeMarche The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByDureeMarche($dureeMarche = null, $comparison = null)
    {
        if (is_array($dureeMarche)) {
            $useMinMax = false;
            if (isset($dureeMarche['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DUREE_MARCHE, $dureeMarche['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dureeMarche['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DUREE_MARCHE, $dureeMarche['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::DUREE_MARCHE, $dureeMarche, $comparison);
    }

    /**
     * Filter the query on the statut_publication_sn column
     *
     * Example usage:
     * <code>
     * $query->filterByStatutPublicationSn(1234); // WHERE statut_publication_sn = 1234
     * $query->filterByStatutPublicationSn(array(12, 34)); // WHERE statut_publication_sn IN (12, 34)
     * $query->filterByStatutPublicationSn(array('min' => 12)); // WHERE statut_publication_sn >= 12
     * $query->filterByStatutPublicationSn(array('max' => 12)); // WHERE statut_publication_sn <= 12
     * </code>
     *
     * @param     mixed $statutPublicationSn The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByStatutPublicationSn($statutPublicationSn = null, $comparison = null)
    {
        if (is_array($statutPublicationSn)) {
            $useMinMax = false;
            if (isset($statutPublicationSn['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::STATUT_PUBLICATION_SN, $statutPublicationSn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statutPublicationSn['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::STATUT_PUBLICATION_SN, $statutPublicationSn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::STATUT_PUBLICATION_SN, $statutPublicationSn, $comparison);
    }

    /**
     * Filter the query on the date_publication_sn column
     *
     * Example usage:
     * <code>
     * $query->filterByDatePublicationSn('2011-03-14'); // WHERE date_publication_sn = '2011-03-14'
     * $query->filterByDatePublicationSn('now'); // WHERE date_publication_sn = '2011-03-14'
     * $query->filterByDatePublicationSn(array('max' => 'yesterday')); // WHERE date_publication_sn > '2011-03-13'
     * </code>
     *
     * @param     mixed $datePublicationSn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByDatePublicationSn($datePublicationSn = null, $comparison = null)
    {
        if (is_array($datePublicationSn)) {
            $useMinMax = false;
            if (isset($datePublicationSn['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DATE_PUBLICATION_SN, $datePublicationSn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datePublicationSn['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DATE_PUBLICATION_SN, $datePublicationSn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::DATE_PUBLICATION_SN, $datePublicationSn, $comparison);
    }

    /**
     * Filter the query on the erreur_sn column
     *
     * Example usage:
     * <code>
     * $query->filterByErreurSn('fooValue');   // WHERE erreur_sn = 'fooValue'
     * $query->filterByErreurSn('%fooValue%'); // WHERE erreur_sn LIKE '%fooValue%'
     * </code>
     *
     * @param     string $erreurSn The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByErreurSn($erreurSn = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($erreurSn)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $erreurSn)) {
                $erreurSn = str_replace('*', '%', $erreurSn);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::ERREUR_SN, $erreurSn, $comparison);
    }

    /**
     * Filter the query on the date_modification_sn column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModificationSn('2011-03-14'); // WHERE date_modification_sn = '2011-03-14'
     * $query->filterByDateModificationSn('now'); // WHERE date_modification_sn = '2011-03-14'
     * $query->filterByDateModificationSn(array('max' => 'yesterday')); // WHERE date_modification_sn > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModificationSn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function filterByDateModificationSn($dateModificationSn = null, $comparison = null)
    {
        if (is_array($dateModificationSn)) {
            $useMinMax = false;
            if (isset($dateModificationSn['min'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DATE_MODIFICATION_SN, $dateModificationSn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModificationSn['max'])) {
                $this->addUsingAlias(CommonModificationContratPeer::DATE_MODIFICATION_SN, $dateModificationSn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonModificationContratPeer::DATE_MODIFICATION_SN, $dateModificationSn, $comparison);
    }

    /**
     * Filter the query by a related CommonAgent object
     *
     * @param   CommonAgent|PropelObjectCollection $commonAgent The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonModificationContratQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAgent($commonAgent, $comparison = null)
    {
        if ($commonAgent instanceof CommonAgent) {
            return $this
                ->addUsingAlias(CommonModificationContratPeer::ID_AGENT, $commonAgent->getId(), $comparison);
        } elseif ($commonAgent instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonModificationContratPeer::ID_AGENT, $commonAgent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonAgent() only accepts arguments of type CommonAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function joinCommonAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAgent');
        }

        return $this;
    }

    /**
     * Use the CommonAgent relation CommonAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAgent', '\Application\Propel\Mpe\CommonAgentQuery');
    }

    /**
     * Filter the query by a related CommonTContratTitulaire object
     *
     * @param   CommonTContratTitulaire|PropelObjectCollection $commonTContratTitulaire The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonModificationContratQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContratTitulaire($commonTContratTitulaire, $comparison = null)
    {
        if ($commonTContratTitulaire instanceof CommonTContratTitulaire) {
            return $this
                ->addUsingAlias(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, $commonTContratTitulaire->getIdContratTitulaire(), $comparison);
        } elseif ($commonTContratTitulaire instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonModificationContratPeer::ID_CONTRAT_TITULAIRE, $commonTContratTitulaire->toKeyValue('PrimaryKey', 'IdContratTitulaire'), $comparison);
        } else {
            throw new PropelException('filterByCommonTContratTitulaire() only accepts arguments of type CommonTContratTitulaire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContratTitulaire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function joinCommonTContratTitulaire($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContratTitulaire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContratTitulaire');
        }

        return $this;
    }

    /**
     * Use the CommonTContratTitulaire relation CommonTContratTitulaire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContratTitulaireQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContratTitulaireQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTContratTitulaire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContratTitulaire', '\Application\Propel\Mpe\CommonTContratTitulaireQuery');
    }

    /**
     * Filter the query by a related CommonTEtablissement object
     *
     * @param   CommonTEtablissement|PropelObjectCollection $commonTEtablissement The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonModificationContratQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTEtablissement($commonTEtablissement, $comparison = null)
    {
        if ($commonTEtablissement instanceof CommonTEtablissement) {
            return $this
                ->addUsingAlias(CommonModificationContratPeer::ID_ETABLISSEMENT, $commonTEtablissement->getIdEtablissement(), $comparison);
        } elseif ($commonTEtablissement instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonModificationContratPeer::ID_ETABLISSEMENT, $commonTEtablissement->toKeyValue('PrimaryKey', 'IdEtablissement'), $comparison);
        } else {
            throw new PropelException('filterByCommonTEtablissement() only accepts arguments of type CommonTEtablissement or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTEtablissement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function joinCommonTEtablissement($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTEtablissement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTEtablissement');
        }

        return $this;
    }

    /**
     * Use the CommonTEtablissement relation CommonTEtablissement object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTEtablissementQuery A secondary query class using the current class as primary query
     */
    public function useCommonTEtablissementQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTEtablissement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTEtablissement', '\Application\Propel\Mpe\CommonTEtablissementQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonModificationContrat $commonModificationContrat Object to remove from the list of results
     *
     * @return CommonModificationContratQuery The current query, for fluid interface
     */
    public function prune($commonModificationContrat = null)
    {
        if ($commonModificationContrat) {
            $this->addUsingAlias(CommonModificationContratPeer::ID, $commonModificationContrat->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
