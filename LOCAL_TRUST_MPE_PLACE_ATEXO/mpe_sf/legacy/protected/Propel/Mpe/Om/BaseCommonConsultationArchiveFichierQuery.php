<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationArchiveFichier;
use Application\Propel\Mpe\CommonConsultationArchiveFichierPeer;
use Application\Propel\Mpe\CommonConsultationArchiveFichierQuery;

/**
 * Base class that represents a query for the 'consultation_archive_fichier' table.
 *
 *
 *
 * @method CommonConsultationArchiveFichierQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonConsultationArchiveFichierQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonConsultationArchiveFichierQuery orderByConsultationRef($order = Criteria::ASC) Order by the consultation_ref column
 * @method CommonConsultationArchiveFichierQuery orderByCheminFichier($order = Criteria::ASC) Order by the chemin_fichier column
 * @method CommonConsultationArchiveFichierQuery orderByPoids($order = Criteria::ASC) Order by the poids column
 *
 * @method CommonConsultationArchiveFichierQuery groupById() Group by the id column
 * @method CommonConsultationArchiveFichierQuery groupByOrganisme() Group by the organisme column
 * @method CommonConsultationArchiveFichierQuery groupByConsultationRef() Group by the consultation_ref column
 * @method CommonConsultationArchiveFichierQuery groupByCheminFichier() Group by the chemin_fichier column
 * @method CommonConsultationArchiveFichierQuery groupByPoids() Group by the poids column
 *
 * @method CommonConsultationArchiveFichierQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonConsultationArchiveFichierQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonConsultationArchiveFichierQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonConsultationArchiveFichier findOne(PropelPDO $con = null) Return the first CommonConsultationArchiveFichier matching the query
 * @method CommonConsultationArchiveFichier findOneOrCreate(PropelPDO $con = null) Return the first CommonConsultationArchiveFichier matching the query, or a new CommonConsultationArchiveFichier object populated from the query conditions when no match is found
 *
 * @method CommonConsultationArchiveFichier findOneByOrganisme(string $organisme) Return the first CommonConsultationArchiveFichier filtered by the organisme column
 * @method CommonConsultationArchiveFichier findOneByConsultationRef(int $consultation_ref) Return the first CommonConsultationArchiveFichier filtered by the consultation_ref column
 * @method CommonConsultationArchiveFichier findOneByCheminFichier(string $chemin_fichier) Return the first CommonConsultationArchiveFichier filtered by the chemin_fichier column
 * @method CommonConsultationArchiveFichier findOneByPoids(int $poids) Return the first CommonConsultationArchiveFichier filtered by the poids column
 *
 * @method array findById(int $id) Return CommonConsultationArchiveFichier objects filtered by the id column
 * @method array findByOrganisme(string $organisme) Return CommonConsultationArchiveFichier objects filtered by the organisme column
 * @method array findByConsultationRef(int $consultation_ref) Return CommonConsultationArchiveFichier objects filtered by the consultation_ref column
 * @method array findByCheminFichier(string $chemin_fichier) Return CommonConsultationArchiveFichier objects filtered by the chemin_fichier column
 * @method array findByPoids(int $poids) Return CommonConsultationArchiveFichier objects filtered by the poids column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationArchiveFichierQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonConsultationArchiveFichierQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonConsultationArchiveFichier', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonConsultationArchiveFichierQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonConsultationArchiveFichierQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonConsultationArchiveFichierQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonConsultationArchiveFichierQuery) {
            return $criteria;
        }
        $query = new CommonConsultationArchiveFichierQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonConsultationArchiveFichier|CommonConsultationArchiveFichier[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonConsultationArchiveFichierPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationArchiveFichierPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationArchiveFichier A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationArchiveFichier A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `organisme`, `consultation_ref`, `chemin_fichier`, `poids` FROM `consultation_archive_fichier` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonConsultationArchiveFichier();
            $obj->hydrate($row);
            CommonConsultationArchiveFichierPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonConsultationArchiveFichier|CommonConsultationArchiveFichier[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonConsultationArchiveFichier[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonConsultationArchiveFichierQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonConsultationArchiveFichierPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonConsultationArchiveFichierQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonConsultationArchiveFichierPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveFichierQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveFichierPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveFichierPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveFichierPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveFichierQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveFichierPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the consultation_ref column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationRef(1234); // WHERE consultation_ref = 1234
     * $query->filterByConsultationRef(array(12, 34)); // WHERE consultation_ref IN (12, 34)
     * $query->filterByConsultationRef(array('min' => 12)); // WHERE consultation_ref >= 12
     * $query->filterByConsultationRef(array('max' => 12)); // WHERE consultation_ref <= 12
     * </code>
     *
     * @param     mixed $consultationRef The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveFichierQuery The current query, for fluid interface
     */
    public function filterByConsultationRef($consultationRef = null, $comparison = null)
    {
        if (is_array($consultationRef)) {
            $useMinMax = false;
            if (isset($consultationRef['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveFichierPeer::CONSULTATION_REF, $consultationRef['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationRef['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveFichierPeer::CONSULTATION_REF, $consultationRef['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveFichierPeer::CONSULTATION_REF, $consultationRef, $comparison);
    }

    /**
     * Filter the query on the chemin_fichier column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminFichier('fooValue');   // WHERE chemin_fichier = 'fooValue'
     * $query->filterByCheminFichier('%fooValue%'); // WHERE chemin_fichier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cheminFichier The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveFichierQuery The current query, for fluid interface
     */
    public function filterByCheminFichier($cheminFichier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cheminFichier)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cheminFichier)) {
                $cheminFichier = str_replace('*', '%', $cheminFichier);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveFichierPeer::CHEMIN_FICHIER, $cheminFichier, $comparison);
    }

    /**
     * Filter the query on the poids column
     *
     * Example usage:
     * <code>
     * $query->filterByPoids(1234); // WHERE poids = 1234
     * $query->filterByPoids(array(12, 34)); // WHERE poids IN (12, 34)
     * $query->filterByPoids(array('min' => 12)); // WHERE poids >= 12
     * $query->filterByPoids(array('max' => 12)); // WHERE poids <= 12
     * </code>
     *
     * @param     mixed $poids The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationArchiveFichierQuery The current query, for fluid interface
     */
    public function filterByPoids($poids = null, $comparison = null)
    {
        if (is_array($poids)) {
            $useMinMax = false;
            if (isset($poids['min'])) {
                $this->addUsingAlias(CommonConsultationArchiveFichierPeer::POIDS, $poids['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($poids['max'])) {
                $this->addUsingAlias(CommonConsultationArchiveFichierPeer::POIDS, $poids['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationArchiveFichierPeer::POIDS, $poids, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonConsultationArchiveFichier $commonConsultationArchiveFichier Object to remove from the list of results
     *
     * @return CommonConsultationArchiveFichierQuery The current query, for fluid interface
     */
    public function prune($commonConsultationArchiveFichier = null)
    {
        if ($commonConsultationArchiveFichier) {
            $this->addUsingAlias(CommonConsultationArchiveFichierPeer::ID, $commonConsultationArchiveFichier->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
