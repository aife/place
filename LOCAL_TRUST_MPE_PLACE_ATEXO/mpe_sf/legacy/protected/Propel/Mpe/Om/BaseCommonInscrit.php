<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAlerte;
use Application\Propel\Mpe\CommonAlerteQuery;
use Application\Propel\Mpe\CommonCertificatsEntreprises;
use Application\Propel\Mpe\CommonCertificatsEntreprisesQuery;
use Application\Propel\Mpe\CommonDossierVolumineux;
use Application\Propel\Mpe\CommonDossierVolumineuxQuery;
use Application\Propel\Mpe\CommonEchangeDestinataire;
use Application\Propel\Mpe\CommonEchangeDestinataireQuery;
use Application\Propel\Mpe\CommonHistorisationMotDePasse;
use Application\Propel\Mpe\CommonHistorisationMotDePasseQuery;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonInscritQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonPanierEntreprise;
use Application\Propel\Mpe\CommonPanierEntrepriseQuery;
use Application\Propel\Mpe\CommonQuestionDCE;
use Application\Propel\Mpe\CommonQuestionDCEQuery;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Propel\Mpe\CommonQuestionsDceQuery;
use Application\Propel\Mpe\CommonReponseInscritFormulaireConsultation;
use Application\Propel\Mpe\CommonReponseInscritFormulaireConsultationQuery;
use Application\Propel\Mpe\CommonSsoEntreprise;
use Application\Propel\Mpe\CommonSsoEntrepriseQuery;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Propel\Mpe\CommonTCandidatureMpsQuery;
use Application\Propel\Mpe\CommonTCandidatureQuery;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTContactContratQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\CommonTListeLotsCandidature;
use Application\Propel\Mpe\CommonTListeLotsCandidatureQuery;
use Application\Propel\Mpe\CommonTReponseElecFormulaire;
use Application\Propel\Mpe\CommonTReponseElecFormulaireQuery;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Propel\Mpe\CommonTelechargementQuery;
use Application\Propel\Mpe\CommonTmpSiretIncorrect;
use Application\Propel\Mpe\CommonTmpSiretIncorrectQuery;
use Application\Propel\Mpe\CommonTraceOperationsInscrit;
use Application\Propel\Mpe\CommonTraceOperationsInscritQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntrepriseQuery;

/**
 * Base class that represents a row from the 'Inscrit' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonInscrit extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonInscritPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonInscritPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the old_id field.
     * @var        int
     */
    protected $old_id;

    /**
     * The value for the entreprise_id field.
     * @var        int
     */
    protected $entreprise_id;

    /**
     * The value for the id_etablissement field.
     * @var        int
     */
    protected $id_etablissement;

    /**
     * The value for the login field.
     * @var        string
     */
    protected $login;

    /**
     * The value for the mdp field.
     * @var        string
     */
    protected $mdp;

    /**
     * The value for the num_cert field.
     * @var        string
     */
    protected $num_cert;

    /**
     * The value for the cert field.
     * @var        string
     */
    protected $cert;

    /**
     * The value for the civilite field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $civilite;

    /**
     * The value for the nom field.
     * @var        string
     */
    protected $nom;

    /**
     * The value for the prenom field.
     * @var        string
     */
    protected $prenom;

    /**
     * The value for the adresse field.
     * @var        string
     */
    protected $adresse;

    /**
     * The value for the codepostal field.
     * @var        string
     */
    protected $codepostal;

    /**
     * The value for the ville field.
     * @var        string
     */
    protected $ville;

    /**
     * The value for the pays field.
     * @var        string
     */
    protected $pays;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the telephone field.
     * @var        string
     */
    protected $telephone;

    /**
     * The value for the categorie field.
     * @var        string
     */
    protected $categorie;

    /**
     * The value for the motstitreresume field.
     * @var        string
     */
    protected $motstitreresume;

    /**
     * The value for the periode field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $periode;

    /**
     * The value for the siret field.
     * @var        string
     */
    protected $siret;

    /**
     * The value for the fax field.
     * @var        string
     */
    protected $fax;

    /**
     * The value for the code_cpv field.
     * @var        string
     */
    protected $code_cpv;

    /**
     * The value for the id_langue field.
     * @var        int
     */
    protected $id_langue;

    /**
     * The value for the profil field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $profil;

    /**
     * The value for the adresse2 field.
     * @var        string
     */
    protected $adresse2;

    /**
     * The value for the bloque field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $bloque;

    /**
     * The value for the id_initial field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $id_initial;

    /**
     * The value for the inscrit_annuaire_defense field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $inscrit_annuaire_defense;

    /**
     * The value for the date_creation field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_modification field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $date_modification;

    /**
     * The value for the tentatives_mdp field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $tentatives_mdp;

    /**
     * The value for the uid field.
     * @var        string
     */
    protected $uid;

    /**
     * The value for the type_hash field.
     * @var        string
     */
    protected $type_hash;

    /**
     * The value for the id_externe field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $id_externe;

    /**
     * The value for the id field.
     * @var        string
     */
    protected $id;

    /**
     * The value for the date_validation_rgpd field.
     * @var        string
     */
    protected $date_validation_rgpd;

    /**
     * The value for the rgpd_communication_place field.
     * @var        boolean
     */
    protected $rgpd_communication_place;

    /**
     * The value for the rgpd_enquete field.
     * @var        boolean
     */
    protected $rgpd_enquete;

    /**
     * The value for the rgpd_communication field.
     * @var        boolean
     */
    protected $rgpd_communication;

    /**
     * The value for the deleted_at field.
     * @var        string
     */
    protected $deleted_at;

    /**
     * The value for the deleted field.
     * @var        boolean
     */
    protected $deleted;

    /**
     * @var        CommonTEtablissement
     */
    protected $aCommonTEtablissementRelatedByIdEtablissement;

    /**
     * @var        Entreprise
     */
    protected $aEntreprise;


    /**
     * @var        PropelObjectCollection|CommonAlerte[] Collection to store aggregation of CommonAlerte objects.
     */
    protected $collCommonAlertes;
    protected $collCommonAlertesPartial;

    /**
     * @var        PropelObjectCollection|CommonCertificatsEntreprises[] Collection to store aggregation of CommonCertificatsEntreprises objects.
     */
    protected $collCommonCertificatsEntreprisess;
    protected $collCommonCertificatsEntreprisessPartial;

    /**
     * @var        PropelObjectCollection|CommonEchangeDestinataire[] Collection to store aggregation of CommonEchangeDestinataire objects.
     */
    protected $collCommonEchangeDestinataires;
    protected $collCommonEchangeDestinatairesPartial;

    /**
     * @var        PropelObjectCollection|CommonOffres[] Collection to store aggregation of CommonOffres objects.
     */
    protected $collCommonOffress;
    protected $collCommonOffressPartial;

    /**
     * @var        PropelObjectCollection|CommonPanierEntreprise[] Collection to store aggregation of CommonPanierEntreprise objects.
     */
    protected $collCommonPanierEntreprises;
    protected $collCommonPanierEntreprisesPartial;

    /**
     * @var        PropelObjectCollection|CommonQuestionDCE[] Collection to store aggregation of CommonQuestionDCE objects.
     */
    protected $collCommonQuestionDCEs;
    protected $collCommonQuestionDCEsPartial;

    /**
     * @var        PropelObjectCollection|CommonReponseInscritFormulaireConsultation[] Collection to store aggregation of CommonReponseInscritFormulaireConsultation objects.
     */
    protected $collCommonReponseInscritFormulaireConsultations;
    protected $collCommonReponseInscritFormulaireConsultationsPartial;

    /**
     * @var        PropelObjectCollection|CommonTelechargement[] Collection to store aggregation of CommonTelechargement objects.
     */
    protected $collCommonTelechargements;
    protected $collCommonTelechargementsPartial;

    /**
     * @var        PropelObjectCollection|CommonDossierVolumineux[] Collection to store aggregation of CommonDossierVolumineux objects.
     */
    protected $collCommonDossierVolumineuxs;
    protected $collCommonDossierVolumineuxsPartial;

    /**
     * @var        PropelObjectCollection|CommonHistorisationMotDePasse[] Collection to store aggregation of CommonHistorisationMotDePasse objects.
     */
    protected $collCommonHistorisationMotDePasses;
    protected $collCommonHistorisationMotDePassesPartial;

    /**
     * @var        PropelObjectCollection|CommonQuestionsDce[] Collection to store aggregation of CommonQuestionsDce objects.
     */
    protected $collCommonQuestionsDces;
    protected $collCommonQuestionsDcesPartial;

    /**
     * @var        PropelObjectCollection|CommonSsoEntreprise[] Collection to store aggregation of CommonSsoEntreprise objects.
     */
    protected $collCommonSsoEntreprises;
    protected $collCommonSsoEntreprisesPartial;

    /**
     * @var        PropelObjectCollection|CommonTCandidature[] Collection to store aggregation of CommonTCandidature objects.
     */
    protected $collCommonTCandidatures;
    protected $collCommonTCandidaturesPartial;

    /**
     * @var        PropelObjectCollection|CommonTCandidatureMps[] Collection to store aggregation of CommonTCandidatureMps objects.
     */
    protected $collCommonTCandidatureMpss;
    protected $collCommonTCandidatureMpssPartial;

    /**
     * @var        PropelObjectCollection|CommonTContactContrat[] Collection to store aggregation of CommonTContactContrat objects.
     */
    protected $collCommonTContactContrats;
    protected $collCommonTContactContratsPartial;

    /**
     * @var        PropelObjectCollection|CommonTListeLotsCandidature[] Collection to store aggregation of CommonTListeLotsCandidature objects.
     */
    protected $collCommonTListeLotsCandidatures;
    protected $collCommonTListeLotsCandidaturesPartial;

    /**
     * @var        PropelObjectCollection|CommonTReponseElecFormulaire[] Collection to store aggregation of CommonTReponseElecFormulaire objects.
     */
    protected $collCommonTReponseElecFormulaires;
    protected $collCommonTReponseElecFormulairesPartial;

    /**
     * @var        PropelObjectCollection|CommonTmpSiretIncorrect[] Collection to store aggregation of CommonTmpSiretIncorrect objects.
     */
    protected $collCommonTmpSiretIncorrects;
    protected $collCommonTmpSiretIncorrectsPartial;

    /**
     * @var        PropelObjectCollection|CommonTraceOperationsInscrit[] Collection to store aggregation of CommonTraceOperationsInscrit objects.
     */
    protected $collCommonTraceOperationsInscrits;
    protected $collCommonTraceOperationsInscritsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAlertesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonCertificatsEntreprisessScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangeDestinatairesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonOffressScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonPanierEntreprisesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonQuestionDCEsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonReponseInscritFormulaireConsultationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTelechargementsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonDossierVolumineuxsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonHistorisationMotDePassesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonQuestionsDcesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonSsoEntreprisesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCandidaturesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCandidatureMpssScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTContactContratsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTListeLotsCandidaturesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTReponseElecFormulairesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTmpSiretIncorrectsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTraceOperationsInscritsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->civilite = false;
        $this->periode = 0;
        $this->profil = 1;
        $this->bloque = '0';
        $this->id_initial = 0;
        $this->inscrit_annuaire_defense = '0';
        $this->date_creation = '';
        $this->date_modification = '';
        $this->tentatives_mdp = 0;
        $this->id_externe = '0';
    }

    /**
     * Initializes internal state of BaseCommonInscrit object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [old_id] column value.
     *
     * @return int
     */
    public function getOldId()
    {

        return $this->old_id;
    }

    /**
     * Get the [entreprise_id] column value.
     *
     * @return int
     */
    public function getEntrepriseId()
    {

        return $this->entreprise_id;
    }

    /**
     * Get the [id_etablissement] column value.
     *
     * @return int
     */
    public function getIdEtablissement()
    {

        return $this->id_etablissement;
    }

    /**
     * Get the [login] column value.
     *
     * @return string
     */
    public function getLogin()
    {

        return $this->login;
    }

    /**
     * Get the [mdp] column value.
     *
     * @return string
     */
    public function getMdp()
    {

        return $this->mdp;
    }

    /**
     * Get the [num_cert] column value.
     *
     * @return string
     */
    public function getNumCert()
    {

        return $this->num_cert;
    }

    /**
     * Get the [cert] column value.
     *
     * @return string
     */
    public function getCert()
    {

        return $this->cert;
    }

    /**
     * Get the [civilite] column value.
     *
     * @return boolean
     */
    public function getCivilite()
    {

        return $this->civilite;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {

        return $this->nom;
    }

    /**
     * Get the [prenom] column value.
     *
     * @return string
     */
    public function getPrenom()
    {

        return $this->prenom;
    }

    /**
     * Get the [adresse] column value.
     *
     * @return string
     */
    public function getAdresse()
    {

        return $this->adresse;
    }

    /**
     * Get the [codepostal] column value.
     *
     * @return string
     */
    public function getCodepostal()
    {

        return $this->codepostal;
    }

    /**
     * Get the [ville] column value.
     *
     * @return string
     */
    public function getVille()
    {

        return $this->ville;
    }

    /**
     * Get the [pays] column value.
     *
     * @return string
     */
    public function getPays()
    {

        return $this->pays;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [telephone] column value.
     *
     * @return string
     */
    public function getTelephone()
    {

        return $this->telephone;
    }

    /**
     * Get the [categorie] column value.
     *
     * @return string
     */
    public function getCategorie()
    {

        return $this->categorie;
    }

    /**
     * Get the [motstitreresume] column value.
     *
     * @return string
     */
    public function getMotstitreresume()
    {

        return $this->motstitreresume;
    }

    /**
     * Get the [periode] column value.
     *
     * @return int
     */
    public function getPeriode()
    {

        return $this->periode;
    }

    /**
     * Get the [siret] column value.
     *
     * @return string
     */
    public function getSiret()
    {

        return $this->siret;
    }

    /**
     * Get the [fax] column value.
     *
     * @return string
     */
    public function getFax()
    {

        return $this->fax;
    }

    /**
     * Get the [code_cpv] column value.
     *
     * @return string
     */
    public function getCodeCpv()
    {

        return $this->code_cpv;
    }

    /**
     * Get the [id_langue] column value.
     *
     * @return int
     */
    public function getIdLangue()
    {

        return $this->id_langue;
    }

    /**
     * Get the [profil] column value.
     *
     * @return int
     */
    public function getProfil()
    {

        return $this->profil;
    }

    /**
     * Get the [adresse2] column value.
     *
     * @return string
     */
    public function getAdresse2()
    {

        return $this->adresse2;
    }

    /**
     * Get the [bloque] column value.
     *
     * @return string
     */
    public function getBloque()
    {

        return $this->bloque;
    }

    /**
     * Get the [id_initial] column value.
     *
     * @return int
     */
    public function getIdInitial()
    {

        return $this->id_initial;
    }

    /**
     * Get the [inscrit_annuaire_defense] column value.
     *
     * @return string
     */
    public function getInscritAnnuaireDefense()
    {

        return $this->inscrit_annuaire_defense;
    }

    /**
     * Get the [date_creation] column value.
     *
     * @return string
     */
    public function getDateCreation()
    {

        return $this->date_creation;
    }

    /**
     * Get the [date_modification] column value.
     *
     * @return string
     */
    public function getDateModification()
    {

        return $this->date_modification;
    }

    /**
     * Get the [tentatives_mdp] column value.
     *
     * @return int
     */
    public function getTentativesMdp()
    {

        return $this->tentatives_mdp;
    }

    /**
     * Get the [uid] column value.
     *
     * @return string
     */
    public function getUid()
    {

        return $this->uid;
    }

    /**
     * Get the [type_hash] column value.
     *
     * @return string
     */
    public function getTypeHash()
    {

        return $this->type_hash;
    }

    /**
     * Get the [id_externe] column value.
     *
     * @return string
     */
    public function getIdExterne()
    {

        return $this->id_externe;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [optionally formatted] temporal [date_validation_rgpd] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateValidationRgpd($format = 'Y-m-d H:i:s')
    {
        if ($this->date_validation_rgpd === null) {
            return null;
        }

        if ($this->date_validation_rgpd === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_validation_rgpd);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_validation_rgpd, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [rgpd_communication_place] column value.
     *
     * @return boolean
     */
    public function getRgpdCommunicationPlace()
    {

        return $this->rgpd_communication_place;
    }

    /**
     * Get the [rgpd_enquete] column value.
     *
     * @return boolean
     */
    public function getRgpdEnquete()
    {

        return $this->rgpd_enquete;
    }

    /**
     * Get the [rgpd_communication] column value.
     *
     * @return boolean
     */
    public function getRgpdCommunication()
    {

        return $this->rgpd_communication;
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = 'Y-m-d H:i:s')
    {
        if ($this->deleted_at === null) {
            return null;
        }

        if ($this->deleted_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->deleted_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->deleted_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [deleted] column value.
     *
     * @return boolean
     */
    public function getDeleted()
    {

        return $this->deleted;
    }

    /**
     * Set the value of [old_id] column.
     *
     * @param int $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setOldId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_id !== $v) {
            $this->old_id = $v;
            $this->modifiedColumns[] = CommonInscritPeer::OLD_ID;
        }


        return $this;
    } // setOldId()

    /**
     * Set the value of [entreprise_id] column.
     *
     * @param int $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setEntrepriseId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->entreprise_id !== $v) {
            $this->entreprise_id = $v;
            $this->modifiedColumns[] = CommonInscritPeer::ENTREPRISE_ID;
        }

        if ($this->aEntreprise !== null && $this->aEntreprise->getId() !== $v) {
            $this->aEntreprise = null;
        }


        return $this;
    } // setEntrepriseId()

    /**
     * Set the value of [id_etablissement] column.
     *
     * @param int $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setIdEtablissement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_etablissement !== $v) {
            $this->id_etablissement = $v;
            $this->modifiedColumns[] = CommonInscritPeer::ID_ETABLISSEMENT;
        }

        if ($this->aCommonTEtablissementRelatedByIdEtablissement !== null && $this->aCommonTEtablissementRelatedByIdEtablissement->getIdEtablissement() !== $v) {
            $this->aCommonTEtablissementRelatedByIdEtablissement = null;
        }

        if ($this->aCommonTEtablissementRelatedByIdEtablissement !== null && $this->aCommonTEtablissementRelatedByIdEtablissement->getIdEtablissement() !== $v) {
            $this->aCommonTEtablissementRelatedByIdEtablissement = null;
        }


        return $this;
    } // setIdEtablissement()

    /**
     * Set the value of [login] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setLogin($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->login !== $v) {
            $this->login = $v;
            $this->modifiedColumns[] = CommonInscritPeer::LOGIN;
        }


        return $this;
    } // setLogin()

    /**
     * Set the value of [mdp] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setMdp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mdp !== $v) {
            $this->mdp = $v;
            $this->modifiedColumns[] = CommonInscritPeer::MDP;
        }


        return $this;
    } // setMdp()

    /**
     * Set the value of [num_cert] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setNumCert($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->num_cert !== $v) {
            $this->num_cert = $v;
            $this->modifiedColumns[] = CommonInscritPeer::NUM_CERT;
        }


        return $this;
    } // setNumCert()

    /**
     * Set the value of [cert] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCert($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->cert !== $v) {
            $this->cert = $v;
            $this->modifiedColumns[] = CommonInscritPeer::CERT;
        }


        return $this;
    } // setCert()

    /**
     * Sets the value of the [civilite] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCivilite($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->civilite !== $v) {
            $this->civilite = $v;
            $this->modifiedColumns[] = CommonInscritPeer::CIVILITE;
        }


        return $this;
    } // setCivilite()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[] = CommonInscritPeer::NOM;
        }


        return $this;
    } // setNom()

    /**
     * Set the value of [prenom] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setPrenom($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prenom !== $v) {
            $this->prenom = $v;
            $this->modifiedColumns[] = CommonInscritPeer::PRENOM;
        }


        return $this;
    } // setPrenom()

    /**
     * Set the value of [adresse] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setAdresse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse !== $v) {
            $this->adresse = $v;
            $this->modifiedColumns[] = CommonInscritPeer::ADRESSE;
        }


        return $this;
    } // setAdresse()

    /**
     * Set the value of [codepostal] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCodepostal($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->codepostal !== $v) {
            $this->codepostal = $v;
            $this->modifiedColumns[] = CommonInscritPeer::CODEPOSTAL;
        }


        return $this;
    } // setCodepostal()

    /**
     * Set the value of [ville] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setVille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville !== $v) {
            $this->ville = $v;
            $this->modifiedColumns[] = CommonInscritPeer::VILLE;
        }


        return $this;
    } // setVille()

    /**
     * Set the value of [pays] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setPays($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays !== $v) {
            $this->pays = $v;
            $this->modifiedColumns[] = CommonInscritPeer::PAYS;
        }


        return $this;
    } // setPays()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = CommonInscritPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [telephone] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setTelephone($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->telephone !== $v) {
            $this->telephone = $v;
            $this->modifiedColumns[] = CommonInscritPeer::TELEPHONE;
        }


        return $this;
    } // setTelephone()

    /**
     * Set the value of [categorie] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCategorie($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->categorie !== $v) {
            $this->categorie = $v;
            $this->modifiedColumns[] = CommonInscritPeer::CATEGORIE;
        }


        return $this;
    } // setCategorie()

    /**
     * Set the value of [motstitreresume] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setMotstitreresume($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->motstitreresume !== $v) {
            $this->motstitreresume = $v;
            $this->modifiedColumns[] = CommonInscritPeer::MOTSTITRERESUME;
        }


        return $this;
    } // setMotstitreresume()

    /**
     * Set the value of [periode] column.
     *
     * @param int $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setPeriode($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->periode !== $v) {
            $this->periode = $v;
            $this->modifiedColumns[] = CommonInscritPeer::PERIODE;
        }


        return $this;
    } // setPeriode()

    /**
     * Set the value of [siret] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setSiret($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->siret !== $v) {
            $this->siret = $v;
            $this->modifiedColumns[] = CommonInscritPeer::SIRET;
        }


        return $this;
    } // setSiret()

    /**
     * Set the value of [fax] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[] = CommonInscritPeer::FAX;
        }


        return $this;
    } // setFax()

    /**
     * Set the value of [code_cpv] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCodeCpv($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code_cpv !== $v) {
            $this->code_cpv = $v;
            $this->modifiedColumns[] = CommonInscritPeer::CODE_CPV;
        }


        return $this;
    } // setCodeCpv()

    /**
     * Set the value of [id_langue] column.
     *
     * @param int $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setIdLangue($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_langue !== $v) {
            $this->id_langue = $v;
            $this->modifiedColumns[] = CommonInscritPeer::ID_LANGUE;
        }


        return $this;
    } // setIdLangue()

    /**
     * Set the value of [profil] column.
     *
     * @param int $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setProfil($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->profil !== $v) {
            $this->profil = $v;
            $this->modifiedColumns[] = CommonInscritPeer::PROFIL;
        }


        return $this;
    } // setProfil()

    /**
     * Set the value of [adresse2] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setAdresse2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse2 !== $v) {
            $this->adresse2 = $v;
            $this->modifiedColumns[] = CommonInscritPeer::ADRESSE2;
        }


        return $this;
    } // setAdresse2()

    /**
     * Set the value of [bloque] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setBloque($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->bloque !== $v) {
            $this->bloque = $v;
            $this->modifiedColumns[] = CommonInscritPeer::BLOQUE;
        }


        return $this;
    } // setBloque()

    /**
     * Set the value of [id_initial] column.
     *
     * @param int $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setIdInitial($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_initial !== $v) {
            $this->id_initial = $v;
            $this->modifiedColumns[] = CommonInscritPeer::ID_INITIAL;
        }


        return $this;
    } // setIdInitial()

    /**
     * Set the value of [inscrit_annuaire_defense] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setInscritAnnuaireDefense($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->inscrit_annuaire_defense !== $v) {
            $this->inscrit_annuaire_defense = $v;
            $this->modifiedColumns[] = CommonInscritPeer::INSCRIT_ANNUAIRE_DEFENSE;
        }


        return $this;
    } // setInscritAnnuaireDefense()

    /**
     * Set the value of [date_creation] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_creation !== $v) {
            $this->date_creation = $v;
            $this->modifiedColumns[] = CommonInscritPeer::DATE_CREATION;
        }


        return $this;
    } // setDateCreation()

    /**
     * Set the value of [date_modification] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setDateModification($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_modification !== $v) {
            $this->date_modification = $v;
            $this->modifiedColumns[] = CommonInscritPeer::DATE_MODIFICATION;
        }


        return $this;
    } // setDateModification()

    /**
     * Set the value of [tentatives_mdp] column.
     *
     * @param int $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setTentativesMdp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->tentatives_mdp !== $v) {
            $this->tentatives_mdp = $v;
            $this->modifiedColumns[] = CommonInscritPeer::TENTATIVES_MDP;
        }


        return $this;
    } // setTentativesMdp()

    /**
     * Set the value of [uid] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setUid($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->uid !== $v) {
            $this->uid = $v;
            $this->modifiedColumns[] = CommonInscritPeer::UID;
        }


        return $this;
    } // setUid()

    /**
     * Set the value of [type_hash] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setTypeHash($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_hash !== $v) {
            $this->type_hash = $v;
            $this->modifiedColumns[] = CommonInscritPeer::TYPE_HASH;
        }


        return $this;
    } // setTypeHash()

    /**
     * Set the value of [id_externe] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setIdExterne($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_externe !== $v) {
            $this->id_externe = $v;
            $this->modifiedColumns[] = CommonInscritPeer::ID_EXTERNE;
        }


        return $this;
    } // setIdExterne()

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonInscritPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Sets the value of [date_validation_rgpd] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setDateValidationRgpd($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_validation_rgpd !== null || $dt !== null) {
            $currentDateAsString = ($this->date_validation_rgpd !== null && $tmpDt = new DateTime($this->date_validation_rgpd)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_validation_rgpd = $newDateAsString;
                $this->modifiedColumns[] = CommonInscritPeer::DATE_VALIDATION_RGPD;
            }
        } // if either are not null


        return $this;
    } // setDateValidationRgpd()

    /**
     * Sets the value of the [rgpd_communication_place] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setRgpdCommunicationPlace($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->rgpd_communication_place !== $v) {
            $this->rgpd_communication_place = $v;
            $this->modifiedColumns[] = CommonInscritPeer::RGPD_COMMUNICATION_PLACE;
        }


        return $this;
    } // setRgpdCommunicationPlace()

    /**
     * Sets the value of the [rgpd_enquete] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setRgpdEnquete($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->rgpd_enquete !== $v) {
            $this->rgpd_enquete = $v;
            $this->modifiedColumns[] = CommonInscritPeer::RGPD_ENQUETE;
        }


        return $this;
    } // setRgpdEnquete()

    /**
     * Sets the value of the [rgpd_communication] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setRgpdCommunication($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->rgpd_communication !== $v) {
            $this->rgpd_communication = $v;
            $this->modifiedColumns[] = CommonInscritPeer::RGPD_COMMUNICATION;
        }


        return $this;
    } // setRgpdCommunication()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            $currentDateAsString = ($this->deleted_at !== null && $tmpDt = new DateTime($this->deleted_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->deleted_at = $newDateAsString;
                $this->modifiedColumns[] = CommonInscritPeer::DELETED_AT;
            }
        } // if either are not null


        return $this;
    } // setDeletedAt()

    /**
     * Sets the value of the [deleted] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setDeleted($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->deleted !== $v) {
            $this->deleted = $v;
            $this->modifiedColumns[] = CommonInscritPeer::DELETED;
        }


        return $this;
    } // setDeleted()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->civilite !== false) {
                return false;
            }

            if ($this->periode !== 0) {
                return false;
            }

            if ($this->profil !== 1) {
                return false;
            }

            if ($this->bloque !== '0') {
                return false;
            }

            if ($this->id_initial !== 0) {
                return false;
            }

            if ($this->inscrit_annuaire_defense !== '0') {
                return false;
            }

            if ($this->date_creation !== '') {
                return false;
            }

            if ($this->date_modification !== '') {
                return false;
            }

            if ($this->tentatives_mdp !== 0) {
                return false;
            }

            if ($this->id_externe !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->old_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->entreprise_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->id_etablissement = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->login = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->mdp = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->num_cert = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->cert = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->civilite = ($row[$startcol + 7] !== null) ? (boolean) $row[$startcol + 7] : null;
            $this->nom = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->prenom = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->adresse = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->codepostal = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->ville = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->pays = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->email = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->telephone = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->categorie = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->motstitreresume = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->periode = ($row[$startcol + 18] !== null) ? (int) $row[$startcol + 18] : null;
            $this->siret = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->fax = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->code_cpv = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->id_langue = ($row[$startcol + 22] !== null) ? (int) $row[$startcol + 22] : null;
            $this->profil = ($row[$startcol + 23] !== null) ? (int) $row[$startcol + 23] : null;
            $this->adresse2 = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->bloque = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->id_initial = ($row[$startcol + 26] !== null) ? (int) $row[$startcol + 26] : null;
            $this->inscrit_annuaire_defense = ($row[$startcol + 27] !== null) ? (string) $row[$startcol + 27] : null;
            $this->date_creation = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->date_modification = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->tentatives_mdp = ($row[$startcol + 30] !== null) ? (int) $row[$startcol + 30] : null;
            $this->uid = ($row[$startcol + 31] !== null) ? (string) $row[$startcol + 31] : null;
            $this->type_hash = ($row[$startcol + 32] !== null) ? (string) $row[$startcol + 32] : null;
            $this->id_externe = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->id = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->date_validation_rgpd = ($row[$startcol + 35] !== null) ? (string) $row[$startcol + 35] : null;
            $this->rgpd_communication_place = ($row[$startcol + 36] !== null) ? (boolean) $row[$startcol + 36] : null;
            $this->rgpd_enquete = ($row[$startcol + 37] !== null) ? (boolean) $row[$startcol + 37] : null;
            $this->rgpd_communication = ($row[$startcol + 38] !== null) ? (boolean) $row[$startcol + 38] : null;
            $this->deleted_at = ($row[$startcol + 39] !== null) ? (string) $row[$startcol + 39] : null;
            $this->deleted = ($row[$startcol + 40] !== null) ? (boolean) $row[$startcol + 40] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 41; // 41 = CommonInscritPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonInscrit object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aEntreprise !== null && $this->entreprise_id !== $this->aEntreprise->getId()) {
            $this->aEntreprise = null;
        }
        if ($this->aCommonTEtablissementRelatedByIdEtablissement !== null && $this->id_etablissement !== $this->aCommonTEtablissementRelatedByIdEtablissement->getIdEtablissement()) {
            $this->aCommonTEtablissementRelatedByIdEtablissement = null;
        }
        if ($this->aCommonTEtablissementRelatedByIdEtablissement !== null && $this->id_etablissement !== $this->aCommonTEtablissementRelatedByIdEtablissement->getIdEtablissement()) {
            $this->aCommonTEtablissementRelatedByIdEtablissement = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonInscritPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonTEtablissementRelatedByIdEtablissement = null;
            $this->aEntreprise = null;
            $this->aCommonTEtablissementRelatedByIdEtablissement = null;
            $this->collCommonAlertes = null;

            $this->collCommonCertificatsEntreprisess = null;

            $this->collCommonEchangeDestinataires = null;

            $this->collCommonOffress = null;

            $this->collCommonPanierEntreprises = null;

            $this->collCommonQuestionDCEs = null;

            $this->collCommonReponseInscritFormulaireConsultations = null;

            $this->collCommonTelechargements = null;

            $this->collCommonDossierVolumineuxs = null;

            $this->collCommonHistorisationMotDePasses = null;

            $this->collCommonQuestionsDces = null;

            $this->collCommonSsoEntreprises = null;

            $this->collCommonTCandidatures = null;

            $this->collCommonTCandidatureMpss = null;

            $this->collCommonTContactContrats = null;

            $this->collCommonTListeLotsCandidatures = null;

            $this->collCommonTReponseElecFormulaires = null;

            $this->collCommonTmpSiretIncorrects = null;

            $this->collCommonTraceOperationsInscrits = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonInscritQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonInscritPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTEtablissementRelatedByIdEtablissement !== null) {
                if ($this->aCommonTEtablissementRelatedByIdEtablissement->isModified() || $this->aCommonTEtablissementRelatedByIdEtablissement->isNew()) {
                    $affectedRows += $this->aCommonTEtablissementRelatedByIdEtablissement->save($con);
                }
                $this->setCommonTEtablissementRelatedByIdEtablissement($this->aCommonTEtablissementRelatedByIdEtablissement);
            }

            if ($this->aEntreprise !== null) {
                if ($this->aEntreprise->isModified() || $this->aEntreprise->isNew()) {
                    $affectedRows += $this->aEntreprise->save($con);
                }
                $this->setEntreprise($this->aEntreprise);
            }

            if ($this->aCommonTEtablissementRelatedByIdEtablissement !== null) {
                if ($this->aCommonTEtablissementRelatedByIdEtablissement->isModified() || $this->aCommonTEtablissementRelatedByIdEtablissement->isNew()) {
                    $affectedRows += $this->aCommonTEtablissementRelatedByIdEtablissement->save($con);
                }
                $this->setCommonTEtablissementRelatedByIdEtablissement($this->aCommonTEtablissementRelatedByIdEtablissement);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonAlertesScheduledForDeletion !== null) {
                if (!$this->commonAlertesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonAlerteQuery::create()
                        ->filterByPrimaryKeys($this->commonAlertesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonAlertesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAlertes !== null) {
                foreach ($this->collCommonAlertes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonCertificatsEntreprisessScheduledForDeletion !== null) {
                if (!$this->commonCertificatsEntreprisessScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonCertificatsEntreprisesQuery::create()
                        ->filterByPrimaryKeys($this->commonCertificatsEntreprisessScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonCertificatsEntreprisessScheduledForDeletion = null;
                }
            }

            if ($this->collCommonCertificatsEntreprisess !== null) {
                foreach ($this->collCommonCertificatsEntreprisess as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonEchangeDestinatairesScheduledForDeletion !== null) {
                if (!$this->commonEchangeDestinatairesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonEchangeDestinataireQuery::create()
                        ->filterByPrimaryKeys($this->commonEchangeDestinatairesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonEchangeDestinatairesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchangeDestinataires !== null) {
                foreach ($this->collCommonEchangeDestinataires as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonOffressScheduledForDeletion !== null) {
                if (!$this->commonOffressScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonOffresQuery::create()
                        ->filterByPrimaryKeys($this->commonOffressScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonOffressScheduledForDeletion = null;
                }
            }

            if ($this->collCommonOffress !== null) {
                foreach ($this->collCommonOffress as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonPanierEntreprisesScheduledForDeletion !== null) {
                if (!$this->commonPanierEntreprisesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonPanierEntrepriseQuery::create()
                        ->filterByPrimaryKeys($this->commonPanierEntreprisesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonPanierEntreprisesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonPanierEntreprises !== null) {
                foreach ($this->collCommonPanierEntreprises as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonQuestionDCEsScheduledForDeletion !== null) {
                if (!$this->commonQuestionDCEsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonQuestionDCEQuery::create()
                        ->filterByPrimaryKeys($this->commonQuestionDCEsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonQuestionDCEsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonQuestionDCEs !== null) {
                foreach ($this->collCommonQuestionDCEs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonReponseInscritFormulaireConsultationsScheduledForDeletion !== null) {
                if (!$this->commonReponseInscritFormulaireConsultationsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonReponseInscritFormulaireConsultationQuery::create()
                        ->filterByPrimaryKeys($this->commonReponseInscritFormulaireConsultationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonReponseInscritFormulaireConsultationsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonReponseInscritFormulaireConsultations !== null) {
                foreach ($this->collCommonReponseInscritFormulaireConsultations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTelechargementsScheduledForDeletion !== null) {
                if (!$this->commonTelechargementsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTelechargementQuery::create()
                        ->filterByPrimaryKeys($this->commonTelechargementsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTelechargementsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTelechargements !== null) {
                foreach ($this->collCommonTelechargements as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonDossierVolumineuxsScheduledForDeletion !== null) {
                if (!$this->commonDossierVolumineuxsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonDossierVolumineuxQuery::create()
                        ->filterByPrimaryKeys($this->commonDossierVolumineuxsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonDossierVolumineuxsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonDossierVolumineuxs !== null) {
                foreach ($this->collCommonDossierVolumineuxs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonHistorisationMotDePassesScheduledForDeletion !== null) {
                if (!$this->commonHistorisationMotDePassesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonHistorisationMotDePasseQuery::create()
                        ->filterByPrimaryKeys($this->commonHistorisationMotDePassesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonHistorisationMotDePassesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonHistorisationMotDePasses !== null) {
                foreach ($this->collCommonHistorisationMotDePasses as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonQuestionsDcesScheduledForDeletion !== null) {
                if (!$this->commonQuestionsDcesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonQuestionsDceQuery::create()
                        ->filterByPrimaryKeys($this->commonQuestionsDcesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonQuestionsDcesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonQuestionsDces !== null) {
                foreach ($this->collCommonQuestionsDces as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonSsoEntreprisesScheduledForDeletion !== null) {
                if (!$this->commonSsoEntreprisesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonSsoEntrepriseQuery::create()
                        ->filterByPrimaryKeys($this->commonSsoEntreprisesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonSsoEntreprisesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonSsoEntreprises !== null) {
                foreach ($this->collCommonSsoEntreprises as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCandidaturesScheduledForDeletion !== null) {
                if (!$this->commonTCandidaturesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTCandidatureQuery::create()
                        ->filterByPrimaryKeys($this->commonTCandidaturesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTCandidaturesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCandidatures !== null) {
                foreach ($this->collCommonTCandidatures as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCandidatureMpssScheduledForDeletion !== null) {
                if (!$this->commonTCandidatureMpssScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTCandidatureMpsQuery::create()
                        ->filterByPrimaryKeys($this->commonTCandidatureMpssScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTCandidatureMpssScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCandidatureMpss !== null) {
                foreach ($this->collCommonTCandidatureMpss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTContactContratsScheduledForDeletion !== null) {
                if (!$this->commonTContactContratsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTContactContratQuery::create()
                        ->filterByPrimaryKeys($this->commonTContactContratsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTContactContratsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTContactContrats !== null) {
                foreach ($this->collCommonTContactContrats as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTListeLotsCandidaturesScheduledForDeletion !== null) {
                if (!$this->commonTListeLotsCandidaturesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTListeLotsCandidatureQuery::create()
                        ->filterByPrimaryKeys($this->commonTListeLotsCandidaturesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTListeLotsCandidaturesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTListeLotsCandidatures !== null) {
                foreach ($this->collCommonTListeLotsCandidatures as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTReponseElecFormulairesScheduledForDeletion !== null) {
                if (!$this->commonTReponseElecFormulairesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTReponseElecFormulaireQuery::create()
                        ->filterByPrimaryKeys($this->commonTReponseElecFormulairesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTReponseElecFormulairesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTReponseElecFormulaires !== null) {
                foreach ($this->collCommonTReponseElecFormulaires as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTmpSiretIncorrectsScheduledForDeletion !== null) {
                if (!$this->commonTmpSiretIncorrectsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTmpSiretIncorrectQuery::create()
                        ->filterByPrimaryKeys($this->commonTmpSiretIncorrectsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTmpSiretIncorrectsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTmpSiretIncorrects !== null) {
                foreach ($this->collCommonTmpSiretIncorrects as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTraceOperationsInscritsScheduledForDeletion !== null) {
                if (!$this->commonTraceOperationsInscritsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTraceOperationsInscritQuery::create()
                        ->filterByPrimaryKeys($this->commonTraceOperationsInscritsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTraceOperationsInscritsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTraceOperationsInscrits !== null) {
                foreach ($this->collCommonTraceOperationsInscrits as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonInscritPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonInscritPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonInscritPeer::OLD_ID)) {
            $modifiedColumns[':p' . $index++]  = '`old_id`';
        }
        if ($this->isColumnModified(CommonInscritPeer::ENTREPRISE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`entreprise_id`';
        }
        if ($this->isColumnModified(CommonInscritPeer::ID_ETABLISSEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_etablissement`';
        }
        if ($this->isColumnModified(CommonInscritPeer::LOGIN)) {
            $modifiedColumns[':p' . $index++]  = '`login`';
        }
        if ($this->isColumnModified(CommonInscritPeer::MDP)) {
            $modifiedColumns[':p' . $index++]  = '`mdp`';
        }
        if ($this->isColumnModified(CommonInscritPeer::NUM_CERT)) {
            $modifiedColumns[':p' . $index++]  = '`num_cert`';
        }
        if ($this->isColumnModified(CommonInscritPeer::CERT)) {
            $modifiedColumns[':p' . $index++]  = '`cert`';
        }
        if ($this->isColumnModified(CommonInscritPeer::CIVILITE)) {
            $modifiedColumns[':p' . $index++]  = '`civilite`';
        }
        if ($this->isColumnModified(CommonInscritPeer::NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(CommonInscritPeer::PRENOM)) {
            $modifiedColumns[':p' . $index++]  = '`prenom`';
        }
        if ($this->isColumnModified(CommonInscritPeer::ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`adresse`';
        }
        if ($this->isColumnModified(CommonInscritPeer::CODEPOSTAL)) {
            $modifiedColumns[':p' . $index++]  = '`codepostal`';
        }
        if ($this->isColumnModified(CommonInscritPeer::VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`ville`';
        }
        if ($this->isColumnModified(CommonInscritPeer::PAYS)) {
            $modifiedColumns[':p' . $index++]  = '`pays`';
        }
        if ($this->isColumnModified(CommonInscritPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(CommonInscritPeer::TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`telephone`';
        }
        if ($this->isColumnModified(CommonInscritPeer::CATEGORIE)) {
            $modifiedColumns[':p' . $index++]  = '`categorie`';
        }
        if ($this->isColumnModified(CommonInscritPeer::MOTSTITRERESUME)) {
            $modifiedColumns[':p' . $index++]  = '`motstitreresume`';
        }
        if ($this->isColumnModified(CommonInscritPeer::PERIODE)) {
            $modifiedColumns[':p' . $index++]  = '`periode`';
        }
        if ($this->isColumnModified(CommonInscritPeer::SIRET)) {
            $modifiedColumns[':p' . $index++]  = '`siret`';
        }
        if ($this->isColumnModified(CommonInscritPeer::FAX)) {
            $modifiedColumns[':p' . $index++]  = '`fax`';
        }
        if ($this->isColumnModified(CommonInscritPeer::CODE_CPV)) {
            $modifiedColumns[':p' . $index++]  = '`code_cpv`';
        }
        if ($this->isColumnModified(CommonInscritPeer::ID_LANGUE)) {
            $modifiedColumns[':p' . $index++]  = '`id_langue`';
        }
        if ($this->isColumnModified(CommonInscritPeer::PROFIL)) {
            $modifiedColumns[':p' . $index++]  = '`profil`';
        }
        if ($this->isColumnModified(CommonInscritPeer::ADRESSE2)) {
            $modifiedColumns[':p' . $index++]  = '`adresse2`';
        }
        if ($this->isColumnModified(CommonInscritPeer::BLOQUE)) {
            $modifiedColumns[':p' . $index++]  = '`bloque`';
        }
        if ($this->isColumnModified(CommonInscritPeer::ID_INITIAL)) {
            $modifiedColumns[':p' . $index++]  = '`id_initial`';
        }
        if ($this->isColumnModified(CommonInscritPeer::INSCRIT_ANNUAIRE_DEFENSE)) {
            $modifiedColumns[':p' . $index++]  = '`inscrit_annuaire_defense`';
        }
        if ($this->isColumnModified(CommonInscritPeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonInscritPeer::DATE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification`';
        }
        if ($this->isColumnModified(CommonInscritPeer::TENTATIVES_MDP)) {
            $modifiedColumns[':p' . $index++]  = '`tentatives_mdp`';
        }
        if ($this->isColumnModified(CommonInscritPeer::UID)) {
            $modifiedColumns[':p' . $index++]  = '`uid`';
        }
        if ($this->isColumnModified(CommonInscritPeer::TYPE_HASH)) {
            $modifiedColumns[':p' . $index++]  = '`type_hash`';
        }
        if ($this->isColumnModified(CommonInscritPeer::ID_EXTERNE)) {
            $modifiedColumns[':p' . $index++]  = '`id_externe`';
        }
        if ($this->isColumnModified(CommonInscritPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonInscritPeer::DATE_VALIDATION_RGPD)) {
            $modifiedColumns[':p' . $index++]  = '`date_validation_rgpd`';
        }
        if ($this->isColumnModified(CommonInscritPeer::RGPD_COMMUNICATION_PLACE)) {
            $modifiedColumns[':p' . $index++]  = '`rgpd_communication_place`';
        }
        if ($this->isColumnModified(CommonInscritPeer::RGPD_ENQUETE)) {
            $modifiedColumns[':p' . $index++]  = '`rgpd_enquete`';
        }
        if ($this->isColumnModified(CommonInscritPeer::RGPD_COMMUNICATION)) {
            $modifiedColumns[':p' . $index++]  = '`rgpd_communication`';
        }
        if ($this->isColumnModified(CommonInscritPeer::DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`deleted_at`';
        }
        if ($this->isColumnModified(CommonInscritPeer::DELETED)) {
            $modifiedColumns[':p' . $index++]  = '`deleted`';
        }

        $sql = sprintf(
            'INSERT INTO `Inscrit` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`old_id`':
                        $stmt->bindValue($identifier, $this->old_id, PDO::PARAM_INT);
                        break;
                    case '`entreprise_id`':
                        $stmt->bindValue($identifier, $this->entreprise_id, PDO::PARAM_INT);
                        break;
                    case '`id_etablissement`':
                        $stmt->bindValue($identifier, $this->id_etablissement, PDO::PARAM_INT);
                        break;
                    case '`login`':
                        $stmt->bindValue($identifier, $this->login, PDO::PARAM_STR);
                        break;
                    case '`mdp`':
                        $stmt->bindValue($identifier, $this->mdp, PDO::PARAM_STR);
                        break;
                    case '`num_cert`':
                        $stmt->bindValue($identifier, $this->num_cert, PDO::PARAM_STR);
                        break;
                    case '`cert`':
                        $stmt->bindValue($identifier, $this->cert, PDO::PARAM_STR);
                        break;
                    case '`civilite`':
                        $stmt->bindValue($identifier, (int) $this->civilite, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`prenom`':
                        $stmt->bindValue($identifier, $this->prenom, PDO::PARAM_STR);
                        break;
                    case '`adresse`':
                        $stmt->bindValue($identifier, $this->adresse, PDO::PARAM_STR);
                        break;
                    case '`codepostal`':
                        $stmt->bindValue($identifier, $this->codepostal, PDO::PARAM_STR);
                        break;
                    case '`ville`':
                        $stmt->bindValue($identifier, $this->ville, PDO::PARAM_STR);
                        break;
                    case '`pays`':
                        $stmt->bindValue($identifier, $this->pays, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`telephone`':
                        $stmt->bindValue($identifier, $this->telephone, PDO::PARAM_STR);
                        break;
                    case '`categorie`':
                        $stmt->bindValue($identifier, $this->categorie, PDO::PARAM_STR);
                        break;
                    case '`motstitreresume`':
                        $stmt->bindValue($identifier, $this->motstitreresume, PDO::PARAM_STR);
                        break;
                    case '`periode`':
                        $stmt->bindValue($identifier, $this->periode, PDO::PARAM_INT);
                        break;
                    case '`siret`':
                        $stmt->bindValue($identifier, $this->siret, PDO::PARAM_STR);
                        break;
                    case '`fax`':
                        $stmt->bindValue($identifier, $this->fax, PDO::PARAM_STR);
                        break;
                    case '`code_cpv`':
                        $stmt->bindValue($identifier, $this->code_cpv, PDO::PARAM_STR);
                        break;
                    case '`id_langue`':
                        $stmt->bindValue($identifier, $this->id_langue, PDO::PARAM_INT);
                        break;
                    case '`profil`':
                        $stmt->bindValue($identifier, $this->profil, PDO::PARAM_INT);
                        break;
                    case '`adresse2`':
                        $stmt->bindValue($identifier, $this->adresse2, PDO::PARAM_STR);
                        break;
                    case '`bloque`':
                        $stmt->bindValue($identifier, $this->bloque, PDO::PARAM_STR);
                        break;
                    case '`id_initial`':
                        $stmt->bindValue($identifier, $this->id_initial, PDO::PARAM_INT);
                        break;
                    case '`inscrit_annuaire_defense`':
                        $stmt->bindValue($identifier, $this->inscrit_annuaire_defense, PDO::PARAM_STR);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_modification`':
                        $stmt->bindValue($identifier, $this->date_modification, PDO::PARAM_STR);
                        break;
                    case '`tentatives_mdp`':
                        $stmt->bindValue($identifier, $this->tentatives_mdp, PDO::PARAM_INT);
                        break;
                    case '`uid`':
                        $stmt->bindValue($identifier, $this->uid, PDO::PARAM_STR);
                        break;
                    case '`type_hash`':
                        $stmt->bindValue($identifier, $this->type_hash, PDO::PARAM_STR);
                        break;
                    case '`id_externe`':
                        $stmt->bindValue($identifier, $this->id_externe, PDO::PARAM_STR);
                        break;
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_STR);
                        break;
                    case '`date_validation_rgpd`':
                        $stmt->bindValue($identifier, $this->date_validation_rgpd, PDO::PARAM_STR);
                        break;
                    case '`rgpd_communication_place`':
                        $stmt->bindValue($identifier, (int) $this->rgpd_communication_place, PDO::PARAM_INT);
                        break;
                    case '`rgpd_enquete`':
                        $stmt->bindValue($identifier, (int) $this->rgpd_enquete, PDO::PARAM_INT);
                        break;
                    case '`rgpd_communication`':
                        $stmt->bindValue($identifier, (int) $this->rgpd_communication, PDO::PARAM_INT);
                        break;
                    case '`deleted_at`':
                        $stmt->bindValue($identifier, $this->deleted_at, PDO::PARAM_STR);
                        break;
                    case '`deleted`':
                        $stmt->bindValue($identifier, (int) $this->deleted, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTEtablissementRelatedByIdEtablissement !== null) {
                if (!$this->aCommonTEtablissementRelatedByIdEtablissement->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTEtablissementRelatedByIdEtablissement->getValidationFailures());
                }
            }

            if ($this->aEntreprise !== null) {
                if (!$this->aEntreprise->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aEntreprise->getValidationFailures());
                }
            }

            if ($this->aCommonTEtablissementRelatedByIdEtablissement !== null) {
                if (!$this->aCommonTEtablissementRelatedByIdEtablissement->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTEtablissementRelatedByIdEtablissement->getValidationFailures());
                }
            }


            if (($retval = CommonInscritPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonAlertes !== null) {
                    foreach ($this->collCommonAlertes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonCertificatsEntreprisess !== null) {
                    foreach ($this->collCommonCertificatsEntreprisess as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonEchangeDestinataires !== null) {
                    foreach ($this->collCommonEchangeDestinataires as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonOffress !== null) {
                    foreach ($this->collCommonOffress as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonPanierEntreprises !== null) {
                    foreach ($this->collCommonPanierEntreprises as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonQuestionDCEs !== null) {
                    foreach ($this->collCommonQuestionDCEs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonReponseInscritFormulaireConsultations !== null) {
                    foreach ($this->collCommonReponseInscritFormulaireConsultations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTelechargements !== null) {
                    foreach ($this->collCommonTelechargements as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonDossierVolumineuxs !== null) {
                    foreach ($this->collCommonDossierVolumineuxs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonHistorisationMotDePasses !== null) {
                    foreach ($this->collCommonHistorisationMotDePasses as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonQuestionsDces !== null) {
                    foreach ($this->collCommonQuestionsDces as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonSsoEntreprises !== null) {
                    foreach ($this->collCommonSsoEntreprises as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCandidatures !== null) {
                    foreach ($this->collCommonTCandidatures as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCandidatureMpss !== null) {
                    foreach ($this->collCommonTCandidatureMpss as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTContactContrats !== null) {
                    foreach ($this->collCommonTContactContrats as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTListeLotsCandidatures !== null) {
                    foreach ($this->collCommonTListeLotsCandidatures as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTReponseElecFormulaires !== null) {
                    foreach ($this->collCommonTReponseElecFormulaires as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTmpSiretIncorrects !== null) {
                    foreach ($this->collCommonTmpSiretIncorrects as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTraceOperationsInscrits !== null) {
                    foreach ($this->collCommonTraceOperationsInscrits as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonInscritPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getOldId();
                break;
            case 1:
                return $this->getEntrepriseId();
                break;
            case 2:
                return $this->getIdEtablissement();
                break;
            case 3:
                return $this->getLogin();
                break;
            case 4:
                return $this->getMdp();
                break;
            case 5:
                return $this->getNumCert();
                break;
            case 6:
                return $this->getCert();
                break;
            case 7:
                return $this->getCivilite();
                break;
            case 8:
                return $this->getNom();
                break;
            case 9:
                return $this->getPrenom();
                break;
            case 10:
                return $this->getAdresse();
                break;
            case 11:
                return $this->getCodepostal();
                break;
            case 12:
                return $this->getVille();
                break;
            case 13:
                return $this->getPays();
                break;
            case 14:
                return $this->getEmail();
                break;
            case 15:
                return $this->getTelephone();
                break;
            case 16:
                return $this->getCategorie();
                break;
            case 17:
                return $this->getMotstitreresume();
                break;
            case 18:
                return $this->getPeriode();
                break;
            case 19:
                return $this->getSiret();
                break;
            case 20:
                return $this->getFax();
                break;
            case 21:
                return $this->getCodeCpv();
                break;
            case 22:
                return $this->getIdLangue();
                break;
            case 23:
                return $this->getProfil();
                break;
            case 24:
                return $this->getAdresse2();
                break;
            case 25:
                return $this->getBloque();
                break;
            case 26:
                return $this->getIdInitial();
                break;
            case 27:
                return $this->getInscritAnnuaireDefense();
                break;
            case 28:
                return $this->getDateCreation();
                break;
            case 29:
                return $this->getDateModification();
                break;
            case 30:
                return $this->getTentativesMdp();
                break;
            case 31:
                return $this->getUid();
                break;
            case 32:
                return $this->getTypeHash();
                break;
            case 33:
                return $this->getIdExterne();
                break;
            case 34:
                return $this->getId();
                break;
            case 35:
                return $this->getDateValidationRgpd();
                break;
            case 36:
                return $this->getRgpdCommunicationPlace();
                break;
            case 37:
                return $this->getRgpdEnquete();
                break;
            case 38:
                return $this->getRgpdCommunication();
                break;
            case 39:
                return $this->getDeletedAt();
                break;
            case 40:
                return $this->getDeleted();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonInscrit'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonInscrit'][$this->getPrimaryKey()] = true;
        $keys = CommonInscritPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getOldId(),
            $keys[1] => $this->getEntrepriseId(),
            $keys[2] => $this->getIdEtablissement(),
            $keys[3] => $this->getLogin(),
            $keys[4] => $this->getMdp(),
            $keys[5] => $this->getNumCert(),
            $keys[6] => $this->getCert(),
            $keys[7] => $this->getCivilite(),
            $keys[8] => $this->getNom(),
            $keys[9] => $this->getPrenom(),
            $keys[10] => $this->getAdresse(),
            $keys[11] => $this->getCodepostal(),
            $keys[12] => $this->getVille(),
            $keys[13] => $this->getPays(),
            $keys[14] => $this->getEmail(),
            $keys[15] => $this->getTelephone(),
            $keys[16] => $this->getCategorie(),
            $keys[17] => $this->getMotstitreresume(),
            $keys[18] => $this->getPeriode(),
            $keys[19] => $this->getSiret(),
            $keys[20] => $this->getFax(),
            $keys[21] => $this->getCodeCpv(),
            $keys[22] => $this->getIdLangue(),
            $keys[23] => $this->getProfil(),
            $keys[24] => $this->getAdresse2(),
            $keys[25] => $this->getBloque(),
            $keys[26] => $this->getIdInitial(),
            $keys[27] => $this->getInscritAnnuaireDefense(),
            $keys[28] => $this->getDateCreation(),
            $keys[29] => $this->getDateModification(),
            $keys[30] => $this->getTentativesMdp(),
            $keys[31] => $this->getUid(),
            $keys[32] => $this->getTypeHash(),
            $keys[33] => $this->getIdExterne(),
            $keys[34] => $this->getId(),
            $keys[35] => $this->getDateValidationRgpd(),
            $keys[36] => $this->getRgpdCommunicationPlace(),
            $keys[37] => $this->getRgpdEnquete(),
            $keys[38] => $this->getRgpdCommunication(),
            $keys[39] => $this->getDeletedAt(),
            $keys[40] => $this->getDeleted(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonTEtablissementRelatedByIdEtablissement) {
                $result['CommonTEtablissementRelatedByIdEtablissement'] = $this->aCommonTEtablissementRelatedByIdEtablissement->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aEntreprise) {
                $result['Entreprise'] = $this->aEntreprise->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTEtablissementRelatedByIdEtablissement) {
                $result['CommonTEtablissementRelatedByIdEtablissement'] = $this->aCommonTEtablissementRelatedByIdEtablissement->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonAlertes) {
                $result['CommonAlertes'] = $this->collCommonAlertes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonCertificatsEntreprisess) {
                $result['CommonCertificatsEntreprisess'] = $this->collCommonCertificatsEntreprisess->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonEchangeDestinataires) {
                $result['CommonEchangeDestinataires'] = $this->collCommonEchangeDestinataires->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonOffress) {
                $result['CommonOffress'] = $this->collCommonOffress->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonPanierEntreprises) {
                $result['CommonPanierEntreprises'] = $this->collCommonPanierEntreprises->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonQuestionDCEs) {
                $result['CommonQuestionDCEs'] = $this->collCommonQuestionDCEs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonReponseInscritFormulaireConsultations) {
                $result['CommonReponseInscritFormulaireConsultations'] = $this->collCommonReponseInscritFormulaireConsultations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTelechargements) {
                $result['CommonTelechargements'] = $this->collCommonTelechargements->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonDossierVolumineuxs) {
                $result['CommonDossierVolumineuxs'] = $this->collCommonDossierVolumineuxs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonHistorisationMotDePasses) {
                $result['CommonHistorisationMotDePasses'] = $this->collCommonHistorisationMotDePasses->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonQuestionsDces) {
                $result['CommonQuestionsDces'] = $this->collCommonQuestionsDces->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonSsoEntreprises) {
                $result['CommonSsoEntreprises'] = $this->collCommonSsoEntreprises->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCandidatures) {
                $result['CommonTCandidatures'] = $this->collCommonTCandidatures->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCandidatureMpss) {
                $result['CommonTCandidatureMpss'] = $this->collCommonTCandidatureMpss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTContactContrats) {
                $result['CommonTContactContrats'] = $this->collCommonTContactContrats->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTListeLotsCandidatures) {
                $result['CommonTListeLotsCandidatures'] = $this->collCommonTListeLotsCandidatures->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTReponseElecFormulaires) {
                $result['CommonTReponseElecFormulaires'] = $this->collCommonTReponseElecFormulaires->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTmpSiretIncorrects) {
                $result['CommonTmpSiretIncorrects'] = $this->collCommonTmpSiretIncorrects->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTraceOperationsInscrits) {
                $result['CommonTraceOperationsInscrits'] = $this->collCommonTraceOperationsInscrits->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonInscritPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setOldId($value);
                break;
            case 1:
                $this->setEntrepriseId($value);
                break;
            case 2:
                $this->setIdEtablissement($value);
                break;
            case 3:
                $this->setLogin($value);
                break;
            case 4:
                $this->setMdp($value);
                break;
            case 5:
                $this->setNumCert($value);
                break;
            case 6:
                $this->setCert($value);
                break;
            case 7:
                $this->setCivilite($value);
                break;
            case 8:
                $this->setNom($value);
                break;
            case 9:
                $this->setPrenom($value);
                break;
            case 10:
                $this->setAdresse($value);
                break;
            case 11:
                $this->setCodepostal($value);
                break;
            case 12:
                $this->setVille($value);
                break;
            case 13:
                $this->setPays($value);
                break;
            case 14:
                $this->setEmail($value);
                break;
            case 15:
                $this->setTelephone($value);
                break;
            case 16:
                $this->setCategorie($value);
                break;
            case 17:
                $this->setMotstitreresume($value);
                break;
            case 18:
                $this->setPeriode($value);
                break;
            case 19:
                $this->setSiret($value);
                break;
            case 20:
                $this->setFax($value);
                break;
            case 21:
                $this->setCodeCpv($value);
                break;
            case 22:
                $this->setIdLangue($value);
                break;
            case 23:
                $this->setProfil($value);
                break;
            case 24:
                $this->setAdresse2($value);
                break;
            case 25:
                $this->setBloque($value);
                break;
            case 26:
                $this->setIdInitial($value);
                break;
            case 27:
                $this->setInscritAnnuaireDefense($value);
                break;
            case 28:
                $this->setDateCreation($value);
                break;
            case 29:
                $this->setDateModification($value);
                break;
            case 30:
                $this->setTentativesMdp($value);
                break;
            case 31:
                $this->setUid($value);
                break;
            case 32:
                $this->setTypeHash($value);
                break;
            case 33:
                $this->setIdExterne($value);
                break;
            case 34:
                $this->setId($value);
                break;
            case 35:
                $this->setDateValidationRgpd($value);
                break;
            case 36:
                $this->setRgpdCommunicationPlace($value);
                break;
            case 37:
                $this->setRgpdEnquete($value);
                break;
            case 38:
                $this->setRgpdCommunication($value);
                break;
            case 39:
                $this->setDeletedAt($value);
                break;
            case 40:
                $this->setDeleted($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonInscritPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setOldId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setEntrepriseId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdEtablissement($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setLogin($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setMdp($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setNumCert($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCert($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setCivilite($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setNom($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setPrenom($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setAdresse($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setCodepostal($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setVille($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setPays($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setEmail($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setTelephone($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setCategorie($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setMotstitreresume($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setPeriode($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setSiret($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setFax($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setCodeCpv($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setIdLangue($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setProfil($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setAdresse2($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setBloque($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setIdInitial($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setInscritAnnuaireDefense($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setDateCreation($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setDateModification($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setTentativesMdp($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setUid($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setTypeHash($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setIdExterne($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setId($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setDateValidationRgpd($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setRgpdCommunicationPlace($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setRgpdEnquete($arr[$keys[37]]);
        if (array_key_exists($keys[38], $arr)) $this->setRgpdCommunication($arr[$keys[38]]);
        if (array_key_exists($keys[39], $arr)) $this->setDeletedAt($arr[$keys[39]]);
        if (array_key_exists($keys[40], $arr)) $this->setDeleted($arr[$keys[40]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonInscritPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonInscritPeer::OLD_ID)) $criteria->add(CommonInscritPeer::OLD_ID, $this->old_id);
        if ($this->isColumnModified(CommonInscritPeer::ENTREPRISE_ID)) $criteria->add(CommonInscritPeer::ENTREPRISE_ID, $this->entreprise_id);
        if ($this->isColumnModified(CommonInscritPeer::ID_ETABLISSEMENT)) $criteria->add(CommonInscritPeer::ID_ETABLISSEMENT, $this->id_etablissement);
        if ($this->isColumnModified(CommonInscritPeer::LOGIN)) $criteria->add(CommonInscritPeer::LOGIN, $this->login);
        if ($this->isColumnModified(CommonInscritPeer::MDP)) $criteria->add(CommonInscritPeer::MDP, $this->mdp);
        if ($this->isColumnModified(CommonInscritPeer::NUM_CERT)) $criteria->add(CommonInscritPeer::NUM_CERT, $this->num_cert);
        if ($this->isColumnModified(CommonInscritPeer::CERT)) $criteria->add(CommonInscritPeer::CERT, $this->cert);
        if ($this->isColumnModified(CommonInscritPeer::CIVILITE)) $criteria->add(CommonInscritPeer::CIVILITE, $this->civilite);
        if ($this->isColumnModified(CommonInscritPeer::NOM)) $criteria->add(CommonInscritPeer::NOM, $this->nom);
        if ($this->isColumnModified(CommonInscritPeer::PRENOM)) $criteria->add(CommonInscritPeer::PRENOM, $this->prenom);
        if ($this->isColumnModified(CommonInscritPeer::ADRESSE)) $criteria->add(CommonInscritPeer::ADRESSE, $this->adresse);
        if ($this->isColumnModified(CommonInscritPeer::CODEPOSTAL)) $criteria->add(CommonInscritPeer::CODEPOSTAL, $this->codepostal);
        if ($this->isColumnModified(CommonInscritPeer::VILLE)) $criteria->add(CommonInscritPeer::VILLE, $this->ville);
        if ($this->isColumnModified(CommonInscritPeer::PAYS)) $criteria->add(CommonInscritPeer::PAYS, $this->pays);
        if ($this->isColumnModified(CommonInscritPeer::EMAIL)) $criteria->add(CommonInscritPeer::EMAIL, $this->email);
        if ($this->isColumnModified(CommonInscritPeer::TELEPHONE)) $criteria->add(CommonInscritPeer::TELEPHONE, $this->telephone);
        if ($this->isColumnModified(CommonInscritPeer::CATEGORIE)) $criteria->add(CommonInscritPeer::CATEGORIE, $this->categorie);
        if ($this->isColumnModified(CommonInscritPeer::MOTSTITRERESUME)) $criteria->add(CommonInscritPeer::MOTSTITRERESUME, $this->motstitreresume);
        if ($this->isColumnModified(CommonInscritPeer::PERIODE)) $criteria->add(CommonInscritPeer::PERIODE, $this->periode);
        if ($this->isColumnModified(CommonInscritPeer::SIRET)) $criteria->add(CommonInscritPeer::SIRET, $this->siret);
        if ($this->isColumnModified(CommonInscritPeer::FAX)) $criteria->add(CommonInscritPeer::FAX, $this->fax);
        if ($this->isColumnModified(CommonInscritPeer::CODE_CPV)) $criteria->add(CommonInscritPeer::CODE_CPV, $this->code_cpv);
        if ($this->isColumnModified(CommonInscritPeer::ID_LANGUE)) $criteria->add(CommonInscritPeer::ID_LANGUE, $this->id_langue);
        if ($this->isColumnModified(CommonInscritPeer::PROFIL)) $criteria->add(CommonInscritPeer::PROFIL, $this->profil);
        if ($this->isColumnModified(CommonInscritPeer::ADRESSE2)) $criteria->add(CommonInscritPeer::ADRESSE2, $this->adresse2);
        if ($this->isColumnModified(CommonInscritPeer::BLOQUE)) $criteria->add(CommonInscritPeer::BLOQUE, $this->bloque);
        if ($this->isColumnModified(CommonInscritPeer::ID_INITIAL)) $criteria->add(CommonInscritPeer::ID_INITIAL, $this->id_initial);
        if ($this->isColumnModified(CommonInscritPeer::INSCRIT_ANNUAIRE_DEFENSE)) $criteria->add(CommonInscritPeer::INSCRIT_ANNUAIRE_DEFENSE, $this->inscrit_annuaire_defense);
        if ($this->isColumnModified(CommonInscritPeer::DATE_CREATION)) $criteria->add(CommonInscritPeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonInscritPeer::DATE_MODIFICATION)) $criteria->add(CommonInscritPeer::DATE_MODIFICATION, $this->date_modification);
        if ($this->isColumnModified(CommonInscritPeer::TENTATIVES_MDP)) $criteria->add(CommonInscritPeer::TENTATIVES_MDP, $this->tentatives_mdp);
        if ($this->isColumnModified(CommonInscritPeer::UID)) $criteria->add(CommonInscritPeer::UID, $this->uid);
        if ($this->isColumnModified(CommonInscritPeer::TYPE_HASH)) $criteria->add(CommonInscritPeer::TYPE_HASH, $this->type_hash);
        if ($this->isColumnModified(CommonInscritPeer::ID_EXTERNE)) $criteria->add(CommonInscritPeer::ID_EXTERNE, $this->id_externe);
        if ($this->isColumnModified(CommonInscritPeer::ID)) $criteria->add(CommonInscritPeer::ID, $this->id);
        if ($this->isColumnModified(CommonInscritPeer::DATE_VALIDATION_RGPD)) $criteria->add(CommonInscritPeer::DATE_VALIDATION_RGPD, $this->date_validation_rgpd);
        if ($this->isColumnModified(CommonInscritPeer::RGPD_COMMUNICATION_PLACE)) $criteria->add(CommonInscritPeer::RGPD_COMMUNICATION_PLACE, $this->rgpd_communication_place);
        if ($this->isColumnModified(CommonInscritPeer::RGPD_ENQUETE)) $criteria->add(CommonInscritPeer::RGPD_ENQUETE, $this->rgpd_enquete);
        if ($this->isColumnModified(CommonInscritPeer::RGPD_COMMUNICATION)) $criteria->add(CommonInscritPeer::RGPD_COMMUNICATION, $this->rgpd_communication);
        if ($this->isColumnModified(CommonInscritPeer::DELETED_AT)) $criteria->add(CommonInscritPeer::DELETED_AT, $this->deleted_at);
        if ($this->isColumnModified(CommonInscritPeer::DELETED)) $criteria->add(CommonInscritPeer::DELETED, $this->deleted);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonInscritPeer::DATABASE_NAME);
        $criteria->add(CommonInscritPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonInscrit (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOldId($this->getOldId());
        $copyObj->setEntrepriseId($this->getEntrepriseId());
        $copyObj->setIdEtablissement($this->getIdEtablissement());
        $copyObj->setLogin($this->getLogin());
        $copyObj->setMdp($this->getMdp());
        $copyObj->setNumCert($this->getNumCert());
        $copyObj->setCert($this->getCert());
        $copyObj->setCivilite($this->getCivilite());
        $copyObj->setNom($this->getNom());
        $copyObj->setPrenom($this->getPrenom());
        $copyObj->setAdresse($this->getAdresse());
        $copyObj->setCodepostal($this->getCodepostal());
        $copyObj->setVille($this->getVille());
        $copyObj->setPays($this->getPays());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setTelephone($this->getTelephone());
        $copyObj->setCategorie($this->getCategorie());
        $copyObj->setMotstitreresume($this->getMotstitreresume());
        $copyObj->setPeriode($this->getPeriode());
        $copyObj->setSiret($this->getSiret());
        $copyObj->setFax($this->getFax());
        $copyObj->setCodeCpv($this->getCodeCpv());
        $copyObj->setIdLangue($this->getIdLangue());
        $copyObj->setProfil($this->getProfil());
        $copyObj->setAdresse2($this->getAdresse2());
        $copyObj->setBloque($this->getBloque());
        $copyObj->setIdInitial($this->getIdInitial());
        $copyObj->setInscritAnnuaireDefense($this->getInscritAnnuaireDefense());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateModification($this->getDateModification());
        $copyObj->setTentativesMdp($this->getTentativesMdp());
        $copyObj->setUid($this->getUid());
        $copyObj->setTypeHash($this->getTypeHash());
        $copyObj->setIdExterne($this->getIdExterne());
        $copyObj->setDateValidationRgpd($this->getDateValidationRgpd());
        $copyObj->setRgpdCommunicationPlace($this->getRgpdCommunicationPlace());
        $copyObj->setRgpdEnquete($this->getRgpdEnquete());
        $copyObj->setRgpdCommunication($this->getRgpdCommunication());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setDeleted($this->getDeleted());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonAlertes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAlerte($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonCertificatsEntreprisess() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonCertificatsEntreprises($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonEchangeDestinataires() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchangeDestinataire($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonOffress() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonOffres($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonPanierEntreprises() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonPanierEntreprise($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonQuestionDCEs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonQuestionDCE($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonReponseInscritFormulaireConsultations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonReponseInscritFormulaireConsultation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTelechargements() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTelechargement($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonDossierVolumineuxs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonDossierVolumineux($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonHistorisationMotDePasses() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonHistorisationMotDePasse($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonQuestionsDces() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonQuestionsDce($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonSsoEntreprises() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonSsoEntreprise($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCandidatures() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCandidature($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCandidatureMpss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCandidatureMps($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTContactContrats() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTContactContrat($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTListeLotsCandidatures() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTListeLotsCandidature($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTReponseElecFormulaires() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTReponseElecFormulaire($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTmpSiretIncorrects() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTmpSiretIncorrect($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTraceOperationsInscrits() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTraceOperationsInscrit($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonInscrit Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonInscritPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonInscritPeer();
        }

        return self::$peer;
    }

    /**
     * Get the associated CommonTEtablissement object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTEtablissement The associated CommonTEtablissement object.
     * @throws PropelException
     */
    public function getCommonTEtablissement(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTEtablissementRelatedByIdEtablissement === null && ($this->id_etablissement !== null) && $doQuery) {
            $this->aCommonTEtablissementRelatedByIdEtablissement = CommonTEtablissementQuery::create()->findPk($this->id_etablissement, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTEtablissementRelatedByIdEtablissement->addCommonInscritsRelatedByIdEtablissement($this);
             */
        }

        return $this->aCommonTEtablissementRelatedByIdEtablissement;
    }

    /**
     * Declares an association between this object and a Entreprise object.
     *
     * @param   Entreprise $v
     * @return CommonInscrit The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntreprise(Entreprise $v = null)
    {
        if ($v === null) {
            $this->setEntrepriseId(NULL);
        } else {
            $this->setEntrepriseId($v->getId());
        }

        $this->aEntreprise = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Entreprise object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonInscrit($this);
        }


        return $this;
    }


    /**
     * Get the associated Entreprise object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Entreprise The associated Entreprise object.
     * @throws PropelException
     */
    public function getEntreprise(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aEntreprise === null && ($this->entreprise_id !== null) && $doQuery) {
            $this->aEntreprise = EntrepriseQuery::create()->findPk($this->entreprise_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntreprise->addCommonInscrits($this);
             */
        }

        return $this->aEntreprise;
    }

    /**
     * Declares an association between this object and a CommonTEtablissement object.
     *
     * @param   CommonTEtablissement $v
     * @return CommonInscrit The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTEtablissementRelatedByIdEtablissement(CommonTEtablissement $v = null)
    {
        if ($v === null) {
            $this->setIdEtablissement(NULL);
        } else {
            $this->setIdEtablissement($v->getIdEtablissement());
        }

        $this->aCommonTEtablissementRelatedByIdEtablissement = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTEtablissement object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonInscritRelatedByIdEtablissement($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTEtablissement object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTEtablissement The associated CommonTEtablissement object.
     * @throws PropelException
     */
    public function getCommonTEtablissementRelatedByIdEtablissement(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTEtablissementRelatedByIdEtablissement === null && ($this->id_etablissement !== null) && $doQuery) {
            $this->aCommonTEtablissementRelatedByIdEtablissement = CommonTEtablissementQuery::create()->findPk($this->id_etablissement, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTEtablissementRelatedByIdEtablissement->addCommonInscritsRelatedByIdEtablissement($this);
             */
        }

        return $this->aCommonTEtablissementRelatedByIdEtablissement;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonAlerte' == $relationName) {
            $this->initCommonAlertes();
        }
        if ('CommonCertificatsEntreprises' == $relationName) {
            $this->initCommonCertificatsEntreprisess();
        }
        if ('CommonEchangeDestinataire' == $relationName) {
            $this->initCommonEchangeDestinataires();
        }
        if ('CommonOffres' == $relationName) {
            $this->initCommonOffress();
        }
        if ('CommonPanierEntreprise' == $relationName) {
            $this->initCommonPanierEntreprises();
        }
        if ('CommonQuestionDCE' == $relationName) {
            $this->initCommonQuestionDCEs();
        }
        if ('CommonReponseInscritFormulaireConsultation' == $relationName) {
            $this->initCommonReponseInscritFormulaireConsultations();
        }
        if ('CommonTelechargement' == $relationName) {
            $this->initCommonTelechargements();
        }
        if ('CommonDossierVolumineux' == $relationName) {
            $this->initCommonDossierVolumineuxs();
        }
        if ('CommonHistorisationMotDePasse' == $relationName) {
            $this->initCommonHistorisationMotDePasses();
        }
        if ('CommonQuestionsDce' == $relationName) {
            $this->initCommonQuestionsDces();
        }
        if ('CommonSsoEntreprise' == $relationName) {
            $this->initCommonSsoEntreprises();
        }
        if ('CommonTCandidature' == $relationName) {
            $this->initCommonTCandidatures();
        }
        if ('CommonTCandidatureMps' == $relationName) {
            $this->initCommonTCandidatureMpss();
        }
        if ('CommonTContactContrat' == $relationName) {
            $this->initCommonTContactContrats();
        }
        if ('CommonTListeLotsCandidature' == $relationName) {
            $this->initCommonTListeLotsCandidatures();
        }
        if ('CommonTReponseElecFormulaire' == $relationName) {
            $this->initCommonTReponseElecFormulaires();
        }
        if ('CommonTmpSiretIncorrect' == $relationName) {
            $this->initCommonTmpSiretIncorrects();
        }
        if ('CommonTraceOperationsInscrit' == $relationName) {
            $this->initCommonTraceOperationsInscrits();
        }
    }

    /**
     * Clears out the collCommonAlertes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonAlertes()
     */
    public function clearCommonAlertes()
    {
        $this->collCommonAlertes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAlertesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAlertes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAlertes($v = true)
    {
        $this->collCommonAlertesPartial = $v;
    }

    /**
     * Initializes the collCommonAlertes collection.
     *
     * By default this just sets the collCommonAlertes collection to an empty array (like clearcollCommonAlertes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAlertes($overrideExisting = true)
    {
        if (null !== $this->collCommonAlertes && !$overrideExisting) {
            return;
        }
        $this->collCommonAlertes = new PropelObjectCollection();
        $this->collCommonAlertes->setModel('CommonAlerte');
    }

    /**
     * Gets an array of CommonAlerte objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAlerte[] List of CommonAlerte objects
     * @throws PropelException
     */
    public function getCommonAlertes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAlertesPartial && !$this->isNew();
        if (null === $this->collCommonAlertes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAlertes) {
                // return empty collection
                $this->initCommonAlertes();
            } else {
                $collCommonAlertes = CommonAlerteQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAlertesPartial && count($collCommonAlertes)) {
                      $this->initCommonAlertes(false);

                      foreach ($collCommonAlertes as $obj) {
                        if (false == $this->collCommonAlertes->contains($obj)) {
                          $this->collCommonAlertes->append($obj);
                        }
                      }

                      $this->collCommonAlertesPartial = true;
                    }

                    $collCommonAlertes->getInternalIterator()->rewind();

                    return $collCommonAlertes;
                }

                if ($partial && $this->collCommonAlertes) {
                    foreach ($this->collCommonAlertes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAlertes[] = $obj;
                        }
                    }
                }

                $this->collCommonAlertes = $collCommonAlertes;
                $this->collCommonAlertesPartial = false;
            }
        }

        return $this->collCommonAlertes;
    }

    /**
     * Sets a collection of CommonAlerte objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAlertes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonAlertes(PropelCollection $commonAlertes, PropelPDO $con = null)
    {
        $commonAlertesToDelete = $this->getCommonAlertes(new Criteria(), $con)->diff($commonAlertes);


        $this->commonAlertesScheduledForDeletion = $commonAlertesToDelete;

        foreach ($commonAlertesToDelete as $commonAlerteRemoved) {
            $commonAlerteRemoved->setCommonInscrit(null);
        }

        $this->collCommonAlertes = null;
        foreach ($commonAlertes as $commonAlerte) {
            $this->addCommonAlerte($commonAlerte);
        }

        $this->collCommonAlertes = $commonAlertes;
        $this->collCommonAlertesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAlerte objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAlerte objects.
     * @throws PropelException
     */
    public function countCommonAlertes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAlertesPartial && !$this->isNew();
        if (null === $this->collCommonAlertes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAlertes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAlertes());
            }
            $query = CommonAlerteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonAlertes);
    }

    /**
     * Method called to associate a CommonAlerte object to this object
     * through the CommonAlerte foreign key attribute.
     *
     * @param   CommonAlerte $l CommonAlerte
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonAlerte(CommonAlerte $l)
    {
        if ($this->collCommonAlertes === null) {
            $this->initCommonAlertes();
            $this->collCommonAlertesPartial = true;
        }
        if (!in_array($l, $this->collCommonAlertes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAlerte($l);
        }

        return $this;
    }

    /**
     * @param	CommonAlerte $commonAlerte The commonAlerte object to add.
     */
    protected function doAddCommonAlerte($commonAlerte)
    {
        $this->collCommonAlertes[]= $commonAlerte;
        $commonAlerte->setCommonInscrit($this);
    }

    /**
     * @param	CommonAlerte $commonAlerte The commonAlerte object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonAlerte($commonAlerte)
    {
        if ($this->getCommonAlertes()->contains($commonAlerte)) {
            $this->collCommonAlertes->remove($this->collCommonAlertes->search($commonAlerte));
            if (null === $this->commonAlertesScheduledForDeletion) {
                $this->commonAlertesScheduledForDeletion = clone $this->collCommonAlertes;
                $this->commonAlertesScheduledForDeletion->clear();
            }
            $this->commonAlertesScheduledForDeletion[]= $commonAlerte;
            $commonAlerte->setCommonInscrit(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonCertificatsEntreprisess collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonCertificatsEntreprisess()
     */
    public function clearCommonCertificatsEntreprisess()
    {
        $this->collCommonCertificatsEntreprisess = null; // important to set this to null since that means it is uninitialized
        $this->collCommonCertificatsEntreprisessPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonCertificatsEntreprisess collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonCertificatsEntreprisess($v = true)
    {
        $this->collCommonCertificatsEntreprisessPartial = $v;
    }

    /**
     * Initializes the collCommonCertificatsEntreprisess collection.
     *
     * By default this just sets the collCommonCertificatsEntreprisess collection to an empty array (like clearcollCommonCertificatsEntreprisess());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonCertificatsEntreprisess($overrideExisting = true)
    {
        if (null !== $this->collCommonCertificatsEntreprisess && !$overrideExisting) {
            return;
        }
        $this->collCommonCertificatsEntreprisess = new PropelObjectCollection();
        $this->collCommonCertificatsEntreprisess->setModel('CommonCertificatsEntreprises');
    }

    /**
     * Gets an array of CommonCertificatsEntreprises objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonCertificatsEntreprises[] List of CommonCertificatsEntreprises objects
     * @throws PropelException
     */
    public function getCommonCertificatsEntreprisess($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonCertificatsEntreprisessPartial && !$this->isNew();
        if (null === $this->collCommonCertificatsEntreprisess || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonCertificatsEntreprisess) {
                // return empty collection
                $this->initCommonCertificatsEntreprisess();
            } else {
                $collCommonCertificatsEntreprisess = CommonCertificatsEntreprisesQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonCertificatsEntreprisessPartial && count($collCommonCertificatsEntreprisess)) {
                      $this->initCommonCertificatsEntreprisess(false);

                      foreach ($collCommonCertificatsEntreprisess as $obj) {
                        if (false == $this->collCommonCertificatsEntreprisess->contains($obj)) {
                          $this->collCommonCertificatsEntreprisess->append($obj);
                        }
                      }

                      $this->collCommonCertificatsEntreprisessPartial = true;
                    }

                    $collCommonCertificatsEntreprisess->getInternalIterator()->rewind();

                    return $collCommonCertificatsEntreprisess;
                }

                if ($partial && $this->collCommonCertificatsEntreprisess) {
                    foreach ($this->collCommonCertificatsEntreprisess as $obj) {
                        if ($obj->isNew()) {
                            $collCommonCertificatsEntreprisess[] = $obj;
                        }
                    }
                }

                $this->collCommonCertificatsEntreprisess = $collCommonCertificatsEntreprisess;
                $this->collCommonCertificatsEntreprisessPartial = false;
            }
        }

        return $this->collCommonCertificatsEntreprisess;
    }

    /**
     * Sets a collection of CommonCertificatsEntreprises objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonCertificatsEntreprisess A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonCertificatsEntreprisess(PropelCollection $commonCertificatsEntreprisess, PropelPDO $con = null)
    {
        $commonCertificatsEntreprisessToDelete = $this->getCommonCertificatsEntreprisess(new Criteria(), $con)->diff($commonCertificatsEntreprisess);


        $this->commonCertificatsEntreprisessScheduledForDeletion = $commonCertificatsEntreprisessToDelete;

        foreach ($commonCertificatsEntreprisessToDelete as $commonCertificatsEntreprisesRemoved) {
            $commonCertificatsEntreprisesRemoved->setCommonInscrit(null);
        }

        $this->collCommonCertificatsEntreprisess = null;
        foreach ($commonCertificatsEntreprisess as $commonCertificatsEntreprises) {
            $this->addCommonCertificatsEntreprises($commonCertificatsEntreprises);
        }

        $this->collCommonCertificatsEntreprisess = $commonCertificatsEntreprisess;
        $this->collCommonCertificatsEntreprisessPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonCertificatsEntreprises objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonCertificatsEntreprises objects.
     * @throws PropelException
     */
    public function countCommonCertificatsEntreprisess(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonCertificatsEntreprisessPartial && !$this->isNew();
        if (null === $this->collCommonCertificatsEntreprisess || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonCertificatsEntreprisess) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonCertificatsEntreprisess());
            }
            $query = CommonCertificatsEntreprisesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonCertificatsEntreprisess);
    }

    /**
     * Method called to associate a CommonCertificatsEntreprises object to this object
     * through the CommonCertificatsEntreprises foreign key attribute.
     *
     * @param   CommonCertificatsEntreprises $l CommonCertificatsEntreprises
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonCertificatsEntreprises(CommonCertificatsEntreprises $l)
    {
        if ($this->collCommonCertificatsEntreprisess === null) {
            $this->initCommonCertificatsEntreprisess();
            $this->collCommonCertificatsEntreprisessPartial = true;
        }
        if (!in_array($l, $this->collCommonCertificatsEntreprisess->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonCertificatsEntreprises($l);
        }

        return $this;
    }

    /**
     * @param	CommonCertificatsEntreprises $commonCertificatsEntreprises The commonCertificatsEntreprises object to add.
     */
    protected function doAddCommonCertificatsEntreprises($commonCertificatsEntreprises)
    {
        $this->collCommonCertificatsEntreprisess[]= $commonCertificatsEntreprises;
        $commonCertificatsEntreprises->setCommonInscrit($this);
    }

    /**
     * @param	CommonCertificatsEntreprises $commonCertificatsEntreprises The commonCertificatsEntreprises object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonCertificatsEntreprises($commonCertificatsEntreprises)
    {
        if ($this->getCommonCertificatsEntreprisess()->contains($commonCertificatsEntreprises)) {
            $this->collCommonCertificatsEntreprisess->remove($this->collCommonCertificatsEntreprisess->search($commonCertificatsEntreprises));
            if (null === $this->commonCertificatsEntreprisessScheduledForDeletion) {
                $this->commonCertificatsEntreprisessScheduledForDeletion = clone $this->collCommonCertificatsEntreprisess;
                $this->commonCertificatsEntreprisessScheduledForDeletion->clear();
            }
            $this->commonCertificatsEntreprisessScheduledForDeletion[]= $commonCertificatsEntreprises;
            $commonCertificatsEntreprises->setCommonInscrit(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonEchangeDestinataires collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonEchangeDestinataires()
     */
    public function clearCommonEchangeDestinataires()
    {
        $this->collCommonEchangeDestinataires = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangeDestinatairesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchangeDestinataires collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchangeDestinataires($v = true)
    {
        $this->collCommonEchangeDestinatairesPartial = $v;
    }

    /**
     * Initializes the collCommonEchangeDestinataires collection.
     *
     * By default this just sets the collCommonEchangeDestinataires collection to an empty array (like clearcollCommonEchangeDestinataires());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchangeDestinataires($overrideExisting = true)
    {
        if (null !== $this->collCommonEchangeDestinataires && !$overrideExisting) {
            return;
        }
        $this->collCommonEchangeDestinataires = new PropelObjectCollection();
        $this->collCommonEchangeDestinataires->setModel('CommonEchangeDestinataire');
    }

    /**
     * Gets an array of CommonEchangeDestinataire objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchangeDestinataire[] List of CommonEchangeDestinataire objects
     * @throws PropelException
     */
    public function getCommonEchangeDestinataires($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDestinatairesPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDestinataires || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDestinataires) {
                // return empty collection
                $this->initCommonEchangeDestinataires();
            } else {
                $collCommonEchangeDestinataires = CommonEchangeDestinataireQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangeDestinatairesPartial && count($collCommonEchangeDestinataires)) {
                      $this->initCommonEchangeDestinataires(false);

                      foreach ($collCommonEchangeDestinataires as $obj) {
                        if (false == $this->collCommonEchangeDestinataires->contains($obj)) {
                          $this->collCommonEchangeDestinataires->append($obj);
                        }
                      }

                      $this->collCommonEchangeDestinatairesPartial = true;
                    }

                    $collCommonEchangeDestinataires->getInternalIterator()->rewind();

                    return $collCommonEchangeDestinataires;
                }

                if ($partial && $this->collCommonEchangeDestinataires) {
                    foreach ($this->collCommonEchangeDestinataires as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchangeDestinataires[] = $obj;
                        }
                    }
                }

                $this->collCommonEchangeDestinataires = $collCommonEchangeDestinataires;
                $this->collCommonEchangeDestinatairesPartial = false;
            }
        }

        return $this->collCommonEchangeDestinataires;
    }

    /**
     * Sets a collection of CommonEchangeDestinataire objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchangeDestinataires A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonEchangeDestinataires(PropelCollection $commonEchangeDestinataires, PropelPDO $con = null)
    {
        $commonEchangeDestinatairesToDelete = $this->getCommonEchangeDestinataires(new Criteria(), $con)->diff($commonEchangeDestinataires);


        $this->commonEchangeDestinatairesScheduledForDeletion = $commonEchangeDestinatairesToDelete;

        foreach ($commonEchangeDestinatairesToDelete as $commonEchangeDestinataireRemoved) {
            $commonEchangeDestinataireRemoved->setCommonInscrit(null);
        }

        $this->collCommonEchangeDestinataires = null;
        foreach ($commonEchangeDestinataires as $commonEchangeDestinataire) {
            $this->addCommonEchangeDestinataire($commonEchangeDestinataire);
        }

        $this->collCommonEchangeDestinataires = $commonEchangeDestinataires;
        $this->collCommonEchangeDestinatairesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchangeDestinataire objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchangeDestinataire objects.
     * @throws PropelException
     */
    public function countCommonEchangeDestinataires(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDestinatairesPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDestinataires || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDestinataires) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchangeDestinataires());
            }
            $query = CommonEchangeDestinataireQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonEchangeDestinataires);
    }

    /**
     * Method called to associate a CommonEchangeDestinataire object to this object
     * through the CommonEchangeDestinataire foreign key attribute.
     *
     * @param   CommonEchangeDestinataire $l CommonEchangeDestinataire
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonEchangeDestinataire(CommonEchangeDestinataire $l)
    {
        if ($this->collCommonEchangeDestinataires === null) {
            $this->initCommonEchangeDestinataires();
            $this->collCommonEchangeDestinatairesPartial = true;
        }
        if (!in_array($l, $this->collCommonEchangeDestinataires->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchangeDestinataire($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchangeDestinataire $commonEchangeDestinataire The commonEchangeDestinataire object to add.
     */
    protected function doAddCommonEchangeDestinataire($commonEchangeDestinataire)
    {
        $this->collCommonEchangeDestinataires[]= $commonEchangeDestinataire;
        $commonEchangeDestinataire->setCommonInscrit($this);
    }

    /**
     * @param	CommonEchangeDestinataire $commonEchangeDestinataire The commonEchangeDestinataire object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonEchangeDestinataire($commonEchangeDestinataire)
    {
        if ($this->getCommonEchangeDestinataires()->contains($commonEchangeDestinataire)) {
            $this->collCommonEchangeDestinataires->remove($this->collCommonEchangeDestinataires->search($commonEchangeDestinataire));
            if (null === $this->commonEchangeDestinatairesScheduledForDeletion) {
                $this->commonEchangeDestinatairesScheduledForDeletion = clone $this->collCommonEchangeDestinataires;
                $this->commonEchangeDestinatairesScheduledForDeletion->clear();
            }
            $this->commonEchangeDestinatairesScheduledForDeletion[]= $commonEchangeDestinataire;
            $commonEchangeDestinataire->setCommonInscrit(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonEchangeDestinataires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDestinataire[] List of CommonEchangeDestinataire objects
     */
    public function getCommonEchangeDestinatairesJoinCommonEchange($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDestinataireQuery::create(null, $criteria);
        $query->joinWith('CommonEchange', $join_behavior);

        return $this->getCommonEchangeDestinataires($query, $con);
    }

    /**
     * Clears out the collCommonOffress collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonOffress()
     */
    public function clearCommonOffress()
    {
        $this->collCommonOffress = null; // important to set this to null since that means it is uninitialized
        $this->collCommonOffressPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonOffress collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonOffress($v = true)
    {
        $this->collCommonOffressPartial = $v;
    }

    /**
     * Initializes the collCommonOffress collection.
     *
     * By default this just sets the collCommonOffress collection to an empty array (like clearcollCommonOffress());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonOffress($overrideExisting = true)
    {
        if (null !== $this->collCommonOffress && !$overrideExisting) {
            return;
        }
        $this->collCommonOffress = new PropelObjectCollection();
        $this->collCommonOffress->setModel('CommonOffres');
    }

    /**
     * Gets an array of CommonOffres objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     * @throws PropelException
     */
    public function getCommonOffress($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonOffressPartial && !$this->isNew();
        if (null === $this->collCommonOffress || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonOffress) {
                // return empty collection
                $this->initCommonOffress();
            } else {
                $collCommonOffress = CommonOffresQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonOffressPartial && count($collCommonOffress)) {
                      $this->initCommonOffress(false);

                      foreach ($collCommonOffress as $obj) {
                        if (false == $this->collCommonOffress->contains($obj)) {
                          $this->collCommonOffress->append($obj);
                        }
                      }

                      $this->collCommonOffressPartial = true;
                    }

                    $collCommonOffress->getInternalIterator()->rewind();

                    return $collCommonOffress;
                }

                if ($partial && $this->collCommonOffress) {
                    foreach ($this->collCommonOffress as $obj) {
                        if ($obj->isNew()) {
                            $collCommonOffress[] = $obj;
                        }
                    }
                }

                $this->collCommonOffress = $collCommonOffress;
                $this->collCommonOffressPartial = false;
            }
        }

        return $this->collCommonOffress;
    }

    /**
     * Sets a collection of CommonOffres objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonOffress A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonOffress(PropelCollection $commonOffress, PropelPDO $con = null)
    {
        $commonOffressToDelete = $this->getCommonOffress(new Criteria(), $con)->diff($commonOffress);


        $this->commonOffressScheduledForDeletion = $commonOffressToDelete;

        foreach ($commonOffressToDelete as $commonOffresRemoved) {
            $commonOffresRemoved->setCommonInscrit(null);
        }

        $this->collCommonOffress = null;
        foreach ($commonOffress as $commonOffres) {
            $this->addCommonOffres($commonOffres);
        }

        $this->collCommonOffress = $commonOffress;
        $this->collCommonOffressPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonOffres objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonOffres objects.
     * @throws PropelException
     */
    public function countCommonOffress(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonOffressPartial && !$this->isNew();
        if (null === $this->collCommonOffress || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonOffress) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonOffress());
            }
            $query = CommonOffresQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonOffress);
    }

    /**
     * Method called to associate a CommonOffres object to this object
     * through the CommonOffres foreign key attribute.
     *
     * @param   CommonOffres $l CommonOffres
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonOffres(CommonOffres $l)
    {
        if ($this->collCommonOffress === null) {
            $this->initCommonOffress();
            $this->collCommonOffressPartial = true;
        }
        if (!in_array($l, $this->collCommonOffress->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonOffres($l);
        }

        return $this;
    }

    /**
     * @param	CommonOffres $commonOffres The commonOffres object to add.
     */
    protected function doAddCommonOffres($commonOffres)
    {
        $this->collCommonOffress[]= $commonOffres;
        $commonOffres->setCommonInscrit($this);
    }

    /**
     * @param	CommonOffres $commonOffres The commonOffres object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonOffres($commonOffres)
    {
        if ($this->getCommonOffress()->contains($commonOffres)) {
            $this->collCommonOffress->remove($this->collCommonOffress->search($commonOffres));
            if (null === $this->commonOffressScheduledForDeletion) {
                $this->commonOffressScheduledForDeletion = clone $this->collCommonOffress;
                $this->commonOffressScheduledForDeletion->clear();
            }
            $this->commonOffressScheduledForDeletion[]= $commonOffres;
            $commonOffres->setCommonInscrit(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonOffress from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressJoinCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonBlobOrganismeFileRelatedByIdBlobHorodatageHash', $join_behavior);

        return $this->getCommonOffress($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonOffress from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonOffress($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonOffress from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonOffress($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonOffress from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonOffress($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonOffress from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressJoinCommonBlobOrganismeFileRelatedByIdBlobXmlReponse($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonBlobOrganismeFileRelatedByIdBlobXmlReponse', $join_behavior);

        return $this->getCommonOffress($query, $con);
    }

    /**
     * Clears out the collCommonPanierEntreprises collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonPanierEntreprises()
     */
    public function clearCommonPanierEntreprises()
    {
        $this->collCommonPanierEntreprises = null; // important to set this to null since that means it is uninitialized
        $this->collCommonPanierEntreprisesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonPanierEntreprises collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonPanierEntreprises($v = true)
    {
        $this->collCommonPanierEntreprisesPartial = $v;
    }

    /**
     * Initializes the collCommonPanierEntreprises collection.
     *
     * By default this just sets the collCommonPanierEntreprises collection to an empty array (like clearcollCommonPanierEntreprises());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonPanierEntreprises($overrideExisting = true)
    {
        if (null !== $this->collCommonPanierEntreprises && !$overrideExisting) {
            return;
        }
        $this->collCommonPanierEntreprises = new PropelObjectCollection();
        $this->collCommonPanierEntreprises->setModel('CommonPanierEntreprise');
    }

    /**
     * Gets an array of CommonPanierEntreprise objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonPanierEntreprise[] List of CommonPanierEntreprise objects
     * @throws PropelException
     */
    public function getCommonPanierEntreprises($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonPanierEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonPanierEntreprises || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonPanierEntreprises) {
                // return empty collection
                $this->initCommonPanierEntreprises();
            } else {
                $collCommonPanierEntreprises = CommonPanierEntrepriseQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonPanierEntreprisesPartial && count($collCommonPanierEntreprises)) {
                      $this->initCommonPanierEntreprises(false);

                      foreach ($collCommonPanierEntreprises as $obj) {
                        if (false == $this->collCommonPanierEntreprises->contains($obj)) {
                          $this->collCommonPanierEntreprises->append($obj);
                        }
                      }

                      $this->collCommonPanierEntreprisesPartial = true;
                    }

                    $collCommonPanierEntreprises->getInternalIterator()->rewind();

                    return $collCommonPanierEntreprises;
                }

                if ($partial && $this->collCommonPanierEntreprises) {
                    foreach ($this->collCommonPanierEntreprises as $obj) {
                        if ($obj->isNew()) {
                            $collCommonPanierEntreprises[] = $obj;
                        }
                    }
                }

                $this->collCommonPanierEntreprises = $collCommonPanierEntreprises;
                $this->collCommonPanierEntreprisesPartial = false;
            }
        }

        return $this->collCommonPanierEntreprises;
    }

    /**
     * Sets a collection of CommonPanierEntreprise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonPanierEntreprises A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonPanierEntreprises(PropelCollection $commonPanierEntreprises, PropelPDO $con = null)
    {
        $commonPanierEntreprisesToDelete = $this->getCommonPanierEntreprises(new Criteria(), $con)->diff($commonPanierEntreprises);


        $this->commonPanierEntreprisesScheduledForDeletion = $commonPanierEntreprisesToDelete;

        foreach ($commonPanierEntreprisesToDelete as $commonPanierEntrepriseRemoved) {
            $commonPanierEntrepriseRemoved->setCommonInscrit(null);
        }

        $this->collCommonPanierEntreprises = null;
        foreach ($commonPanierEntreprises as $commonPanierEntreprise) {
            $this->addCommonPanierEntreprise($commonPanierEntreprise);
        }

        $this->collCommonPanierEntreprises = $commonPanierEntreprises;
        $this->collCommonPanierEntreprisesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonPanierEntreprise objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonPanierEntreprise objects.
     * @throws PropelException
     */
    public function countCommonPanierEntreprises(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonPanierEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonPanierEntreprises || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonPanierEntreprises) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonPanierEntreprises());
            }
            $query = CommonPanierEntrepriseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonPanierEntreprises);
    }

    /**
     * Method called to associate a CommonPanierEntreprise object to this object
     * through the CommonPanierEntreprise foreign key attribute.
     *
     * @param   CommonPanierEntreprise $l CommonPanierEntreprise
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonPanierEntreprise(CommonPanierEntreprise $l)
    {
        if ($this->collCommonPanierEntreprises === null) {
            $this->initCommonPanierEntreprises();
            $this->collCommonPanierEntreprisesPartial = true;
        }
        if (!in_array($l, $this->collCommonPanierEntreprises->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonPanierEntreprise($l);
        }

        return $this;
    }

    /**
     * @param	CommonPanierEntreprise $commonPanierEntreprise The commonPanierEntreprise object to add.
     */
    protected function doAddCommonPanierEntreprise($commonPanierEntreprise)
    {
        $this->collCommonPanierEntreprises[]= $commonPanierEntreprise;
        $commonPanierEntreprise->setCommonInscrit($this);
    }

    /**
     * @param	CommonPanierEntreprise $commonPanierEntreprise The commonPanierEntreprise object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonPanierEntreprise($commonPanierEntreprise)
    {
        if ($this->getCommonPanierEntreprises()->contains($commonPanierEntreprise)) {
            $this->collCommonPanierEntreprises->remove($this->collCommonPanierEntreprises->search($commonPanierEntreprise));
            if (null === $this->commonPanierEntreprisesScheduledForDeletion) {
                $this->commonPanierEntreprisesScheduledForDeletion = clone $this->collCommonPanierEntreprises;
                $this->commonPanierEntreprisesScheduledForDeletion->clear();
            }
            $this->commonPanierEntreprisesScheduledForDeletion[]= $commonPanierEntreprise;
            $commonPanierEntreprise->setCommonInscrit(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonPanierEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonPanierEntreprise[] List of CommonPanierEntreprise objects
     */
    public function getCommonPanierEntreprisesJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonPanierEntrepriseQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonPanierEntreprises($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonPanierEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonPanierEntreprise[] List of CommonPanierEntreprise objects
     */
    public function getCommonPanierEntreprisesJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonPanierEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonPanierEntreprises($query, $con);
    }

    /**
     * Clears out the collCommonQuestionDCEs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonQuestionDCEs()
     */
    public function clearCommonQuestionDCEs()
    {
        $this->collCommonQuestionDCEs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonQuestionDCEsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonQuestionDCEs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonQuestionDCEs($v = true)
    {
        $this->collCommonQuestionDCEsPartial = $v;
    }

    /**
     * Initializes the collCommonQuestionDCEs collection.
     *
     * By default this just sets the collCommonQuestionDCEs collection to an empty array (like clearcollCommonQuestionDCEs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonQuestionDCEs($overrideExisting = true)
    {
        if (null !== $this->collCommonQuestionDCEs && !$overrideExisting) {
            return;
        }
        $this->collCommonQuestionDCEs = new PropelObjectCollection();
        $this->collCommonQuestionDCEs->setModel('CommonQuestionDCE');
    }

    /**
     * Gets an array of CommonQuestionDCE objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonQuestionDCE[] List of CommonQuestionDCE objects
     * @throws PropelException
     */
    public function getCommonQuestionDCEs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonQuestionDCEsPartial && !$this->isNew();
        if (null === $this->collCommonQuestionDCEs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonQuestionDCEs) {
                // return empty collection
                $this->initCommonQuestionDCEs();
            } else {
                $collCommonQuestionDCEs = CommonQuestionDCEQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonQuestionDCEsPartial && count($collCommonQuestionDCEs)) {
                      $this->initCommonQuestionDCEs(false);

                      foreach ($collCommonQuestionDCEs as $obj) {
                        if (false == $this->collCommonQuestionDCEs->contains($obj)) {
                          $this->collCommonQuestionDCEs->append($obj);
                        }
                      }

                      $this->collCommonQuestionDCEsPartial = true;
                    }

                    $collCommonQuestionDCEs->getInternalIterator()->rewind();

                    return $collCommonQuestionDCEs;
                }

                if ($partial && $this->collCommonQuestionDCEs) {
                    foreach ($this->collCommonQuestionDCEs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonQuestionDCEs[] = $obj;
                        }
                    }
                }

                $this->collCommonQuestionDCEs = $collCommonQuestionDCEs;
                $this->collCommonQuestionDCEsPartial = false;
            }
        }

        return $this->collCommonQuestionDCEs;
    }

    /**
     * Sets a collection of CommonQuestionDCE objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonQuestionDCEs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonQuestionDCEs(PropelCollection $commonQuestionDCEs, PropelPDO $con = null)
    {
        $commonQuestionDCEsToDelete = $this->getCommonQuestionDCEs(new Criteria(), $con)->diff($commonQuestionDCEs);


        $this->commonQuestionDCEsScheduledForDeletion = $commonQuestionDCEsToDelete;

        foreach ($commonQuestionDCEsToDelete as $commonQuestionDCERemoved) {
            $commonQuestionDCERemoved->setCommonInscrit(null);
        }

        $this->collCommonQuestionDCEs = null;
        foreach ($commonQuestionDCEs as $commonQuestionDCE) {
            $this->addCommonQuestionDCE($commonQuestionDCE);
        }

        $this->collCommonQuestionDCEs = $commonQuestionDCEs;
        $this->collCommonQuestionDCEsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonQuestionDCE objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonQuestionDCE objects.
     * @throws PropelException
     */
    public function countCommonQuestionDCEs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonQuestionDCEsPartial && !$this->isNew();
        if (null === $this->collCommonQuestionDCEs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonQuestionDCEs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonQuestionDCEs());
            }
            $query = CommonQuestionDCEQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonQuestionDCEs);
    }

    /**
     * Method called to associate a CommonQuestionDCE object to this object
     * through the CommonQuestionDCE foreign key attribute.
     *
     * @param   CommonQuestionDCE $l CommonQuestionDCE
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonQuestionDCE(CommonQuestionDCE $l)
    {
        if ($this->collCommonQuestionDCEs === null) {
            $this->initCommonQuestionDCEs();
            $this->collCommonQuestionDCEsPartial = true;
        }
        if (!in_array($l, $this->collCommonQuestionDCEs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonQuestionDCE($l);
        }

        return $this;
    }

    /**
     * @param	CommonQuestionDCE $commonQuestionDCE The commonQuestionDCE object to add.
     */
    protected function doAddCommonQuestionDCE($commonQuestionDCE)
    {
        $this->collCommonQuestionDCEs[]= $commonQuestionDCE;
        $commonQuestionDCE->setCommonInscrit($this);
    }

    /**
     * @param	CommonQuestionDCE $commonQuestionDCE The commonQuestionDCE object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonQuestionDCE($commonQuestionDCE)
    {
        if ($this->getCommonQuestionDCEs()->contains($commonQuestionDCE)) {
            $this->collCommonQuestionDCEs->remove($this->collCommonQuestionDCEs->search($commonQuestionDCE));
            if (null === $this->commonQuestionDCEsScheduledForDeletion) {
                $this->commonQuestionDCEsScheduledForDeletion = clone $this->collCommonQuestionDCEs;
                $this->commonQuestionDCEsScheduledForDeletion->clear();
            }
            $this->commonQuestionDCEsScheduledForDeletion[]= $commonQuestionDCE;
            $commonQuestionDCE->setCommonInscrit(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonQuestionDCEs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonQuestionDCE[] List of CommonQuestionDCE objects
     */
    public function getCommonQuestionDCEsJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonQuestionDCEQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonQuestionDCEs($query, $con);
    }

    /**
     * Clears out the collCommonReponseInscritFormulaireConsultations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonReponseInscritFormulaireConsultations()
     */
    public function clearCommonReponseInscritFormulaireConsultations()
    {
        $this->collCommonReponseInscritFormulaireConsultations = null; // important to set this to null since that means it is uninitialized
        $this->collCommonReponseInscritFormulaireConsultationsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonReponseInscritFormulaireConsultations collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonReponseInscritFormulaireConsultations($v = true)
    {
        $this->collCommonReponseInscritFormulaireConsultationsPartial = $v;
    }

    /**
     * Initializes the collCommonReponseInscritFormulaireConsultations collection.
     *
     * By default this just sets the collCommonReponseInscritFormulaireConsultations collection to an empty array (like clearcollCommonReponseInscritFormulaireConsultations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonReponseInscritFormulaireConsultations($overrideExisting = true)
    {
        if (null !== $this->collCommonReponseInscritFormulaireConsultations && !$overrideExisting) {
            return;
        }
        $this->collCommonReponseInscritFormulaireConsultations = new PropelObjectCollection();
        $this->collCommonReponseInscritFormulaireConsultations->setModel('CommonReponseInscritFormulaireConsultation');
    }

    /**
     * Gets an array of CommonReponseInscritFormulaireConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonReponseInscritFormulaireConsultation[] List of CommonReponseInscritFormulaireConsultation objects
     * @throws PropelException
     */
    public function getCommonReponseInscritFormulaireConsultations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonReponseInscritFormulaireConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonReponseInscritFormulaireConsultations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonReponseInscritFormulaireConsultations) {
                // return empty collection
                $this->initCommonReponseInscritFormulaireConsultations();
            } else {
                $collCommonReponseInscritFormulaireConsultations = CommonReponseInscritFormulaireConsultationQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonReponseInscritFormulaireConsultationsPartial && count($collCommonReponseInscritFormulaireConsultations)) {
                      $this->initCommonReponseInscritFormulaireConsultations(false);

                      foreach ($collCommonReponseInscritFormulaireConsultations as $obj) {
                        if (false == $this->collCommonReponseInscritFormulaireConsultations->contains($obj)) {
                          $this->collCommonReponseInscritFormulaireConsultations->append($obj);
                        }
                      }

                      $this->collCommonReponseInscritFormulaireConsultationsPartial = true;
                    }

                    $collCommonReponseInscritFormulaireConsultations->getInternalIterator()->rewind();

                    return $collCommonReponseInscritFormulaireConsultations;
                }

                if ($partial && $this->collCommonReponseInscritFormulaireConsultations) {
                    foreach ($this->collCommonReponseInscritFormulaireConsultations as $obj) {
                        if ($obj->isNew()) {
                            $collCommonReponseInscritFormulaireConsultations[] = $obj;
                        }
                    }
                }

                $this->collCommonReponseInscritFormulaireConsultations = $collCommonReponseInscritFormulaireConsultations;
                $this->collCommonReponseInscritFormulaireConsultationsPartial = false;
            }
        }

        return $this->collCommonReponseInscritFormulaireConsultations;
    }

    /**
     * Sets a collection of CommonReponseInscritFormulaireConsultation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonReponseInscritFormulaireConsultations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonReponseInscritFormulaireConsultations(PropelCollection $commonReponseInscritFormulaireConsultations, PropelPDO $con = null)
    {
        $commonReponseInscritFormulaireConsultationsToDelete = $this->getCommonReponseInscritFormulaireConsultations(new Criteria(), $con)->diff($commonReponseInscritFormulaireConsultations);


        $this->commonReponseInscritFormulaireConsultationsScheduledForDeletion = $commonReponseInscritFormulaireConsultationsToDelete;

        foreach ($commonReponseInscritFormulaireConsultationsToDelete as $commonReponseInscritFormulaireConsultationRemoved) {
            $commonReponseInscritFormulaireConsultationRemoved->setCommonInscrit(null);
        }

        $this->collCommonReponseInscritFormulaireConsultations = null;
        foreach ($commonReponseInscritFormulaireConsultations as $commonReponseInscritFormulaireConsultation) {
            $this->addCommonReponseInscritFormulaireConsultation($commonReponseInscritFormulaireConsultation);
        }

        $this->collCommonReponseInscritFormulaireConsultations = $commonReponseInscritFormulaireConsultations;
        $this->collCommonReponseInscritFormulaireConsultationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonReponseInscritFormulaireConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonReponseInscritFormulaireConsultation objects.
     * @throws PropelException
     */
    public function countCommonReponseInscritFormulaireConsultations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonReponseInscritFormulaireConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonReponseInscritFormulaireConsultations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonReponseInscritFormulaireConsultations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonReponseInscritFormulaireConsultations());
            }
            $query = CommonReponseInscritFormulaireConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonReponseInscritFormulaireConsultations);
    }

    /**
     * Method called to associate a CommonReponseInscritFormulaireConsultation object to this object
     * through the CommonReponseInscritFormulaireConsultation foreign key attribute.
     *
     * @param   CommonReponseInscritFormulaireConsultation $l CommonReponseInscritFormulaireConsultation
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonReponseInscritFormulaireConsultation(CommonReponseInscritFormulaireConsultation $l)
    {
        if ($this->collCommonReponseInscritFormulaireConsultations === null) {
            $this->initCommonReponseInscritFormulaireConsultations();
            $this->collCommonReponseInscritFormulaireConsultationsPartial = true;
        }
        if (!in_array($l, $this->collCommonReponseInscritFormulaireConsultations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonReponseInscritFormulaireConsultation($l);
        }

        return $this;
    }

    /**
     * @param	CommonReponseInscritFormulaireConsultation $commonReponseInscritFormulaireConsultation The commonReponseInscritFormulaireConsultation object to add.
     */
    protected function doAddCommonReponseInscritFormulaireConsultation($commonReponseInscritFormulaireConsultation)
    {
        $this->collCommonReponseInscritFormulaireConsultations[]= $commonReponseInscritFormulaireConsultation;
        $commonReponseInscritFormulaireConsultation->setCommonInscrit($this);
    }

    /**
     * @param	CommonReponseInscritFormulaireConsultation $commonReponseInscritFormulaireConsultation The commonReponseInscritFormulaireConsultation object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonReponseInscritFormulaireConsultation($commonReponseInscritFormulaireConsultation)
    {
        if ($this->getCommonReponseInscritFormulaireConsultations()->contains($commonReponseInscritFormulaireConsultation)) {
            $this->collCommonReponseInscritFormulaireConsultations->remove($this->collCommonReponseInscritFormulaireConsultations->search($commonReponseInscritFormulaireConsultation));
            if (null === $this->commonReponseInscritFormulaireConsultationsScheduledForDeletion) {
                $this->commonReponseInscritFormulaireConsultationsScheduledForDeletion = clone $this->collCommonReponseInscritFormulaireConsultations;
                $this->commonReponseInscritFormulaireConsultationsScheduledForDeletion->clear();
            }
            $this->commonReponseInscritFormulaireConsultationsScheduledForDeletion[]= $commonReponseInscritFormulaireConsultation;
            $commonReponseInscritFormulaireConsultation->setCommonInscrit(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTelechargements collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonTelechargements()
     */
    public function clearCommonTelechargements()
    {
        $this->collCommonTelechargements = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTelechargementsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTelechargements collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTelechargements($v = true)
    {
        $this->collCommonTelechargementsPartial = $v;
    }

    /**
     * Initializes the collCommonTelechargements collection.
     *
     * By default this just sets the collCommonTelechargements collection to an empty array (like clearcollCommonTelechargements());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTelechargements($overrideExisting = true)
    {
        if (null !== $this->collCommonTelechargements && !$overrideExisting) {
            return;
        }
        $this->collCommonTelechargements = new PropelObjectCollection();
        $this->collCommonTelechargements->setModel('CommonTelechargement');
    }

    /**
     * Gets an array of CommonTelechargement objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     * @throws PropelException
     */
    public function getCommonTelechargements($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTelechargementsPartial && !$this->isNew();
        if (null === $this->collCommonTelechargements || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTelechargements) {
                // return empty collection
                $this->initCommonTelechargements();
            } else {
                $collCommonTelechargements = CommonTelechargementQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTelechargementsPartial && count($collCommonTelechargements)) {
                      $this->initCommonTelechargements(false);

                      foreach ($collCommonTelechargements as $obj) {
                        if (false == $this->collCommonTelechargements->contains($obj)) {
                          $this->collCommonTelechargements->append($obj);
                        }
                      }

                      $this->collCommonTelechargementsPartial = true;
                    }

                    $collCommonTelechargements->getInternalIterator()->rewind();

                    return $collCommonTelechargements;
                }

                if ($partial && $this->collCommonTelechargements) {
                    foreach ($this->collCommonTelechargements as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTelechargements[] = $obj;
                        }
                    }
                }

                $this->collCommonTelechargements = $collCommonTelechargements;
                $this->collCommonTelechargementsPartial = false;
            }
        }

        return $this->collCommonTelechargements;
    }

    /**
     * Sets a collection of CommonTelechargement objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTelechargements A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonTelechargements(PropelCollection $commonTelechargements, PropelPDO $con = null)
    {
        $commonTelechargementsToDelete = $this->getCommonTelechargements(new Criteria(), $con)->diff($commonTelechargements);


        $this->commonTelechargementsScheduledForDeletion = $commonTelechargementsToDelete;

        foreach ($commonTelechargementsToDelete as $commonTelechargementRemoved) {
            $commonTelechargementRemoved->setCommonInscrit(null);
        }

        $this->collCommonTelechargements = null;
        foreach ($commonTelechargements as $commonTelechargement) {
            $this->addCommonTelechargement($commonTelechargement);
        }

        $this->collCommonTelechargements = $commonTelechargements;
        $this->collCommonTelechargementsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTelechargement objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTelechargement objects.
     * @throws PropelException
     */
    public function countCommonTelechargements(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTelechargementsPartial && !$this->isNew();
        if (null === $this->collCommonTelechargements || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTelechargements) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTelechargements());
            }
            $query = CommonTelechargementQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonTelechargements);
    }

    /**
     * Method called to associate a CommonTelechargement object to this object
     * through the CommonTelechargement foreign key attribute.
     *
     * @param   CommonTelechargement $l CommonTelechargement
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonTelechargement(CommonTelechargement $l)
    {
        if ($this->collCommonTelechargements === null) {
            $this->initCommonTelechargements();
            $this->collCommonTelechargementsPartial = true;
        }
        if (!in_array($l, $this->collCommonTelechargements->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTelechargement($l);
        }

        return $this;
    }

    /**
     * @param	CommonTelechargement $commonTelechargement The commonTelechargement object to add.
     */
    protected function doAddCommonTelechargement($commonTelechargement)
    {
        $this->collCommonTelechargements[]= $commonTelechargement;
        $commonTelechargement->setCommonInscrit($this);
    }

    /**
     * @param	CommonTelechargement $commonTelechargement The commonTelechargement object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonTelechargement($commonTelechargement)
    {
        if ($this->getCommonTelechargements()->contains($commonTelechargement)) {
            $this->collCommonTelechargements->remove($this->collCommonTelechargements->search($commonTelechargement));
            if (null === $this->commonTelechargementsScheduledForDeletion) {
                $this->commonTelechargementsScheduledForDeletion = clone $this->collCommonTelechargements;
                $this->commonTelechargementsScheduledForDeletion->clear();
            }
            $this->commonTelechargementsScheduledForDeletion[]= $commonTelechargement;
            $commonTelechargement->setCommonInscrit(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTelechargements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     */
    public function getCommonTelechargementsJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTelechargementQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTelechargements($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTelechargements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     */
    public function getCommonTelechargementsJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTelechargementQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonTelechargements($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTelechargements from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTelechargement[] List of CommonTelechargement objects
     */
    public function getCommonTelechargementsJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTelechargementQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonTelechargements($query, $con);
    }

    /**
     * Clears out the collCommonDossierVolumineuxs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonDossierVolumineuxs()
     */
    public function clearCommonDossierVolumineuxs()
    {
        $this->collCommonDossierVolumineuxs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonDossierVolumineuxsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonDossierVolumineuxs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonDossierVolumineuxs($v = true)
    {
        $this->collCommonDossierVolumineuxsPartial = $v;
    }

    /**
     * Initializes the collCommonDossierVolumineuxs collection.
     *
     * By default this just sets the collCommonDossierVolumineuxs collection to an empty array (like clearcollCommonDossierVolumineuxs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonDossierVolumineuxs($overrideExisting = true)
    {
        if (null !== $this->collCommonDossierVolumineuxs && !$overrideExisting) {
            return;
        }
        $this->collCommonDossierVolumineuxs = new PropelObjectCollection();
        $this->collCommonDossierVolumineuxs->setModel('CommonDossierVolumineux');
    }

    /**
     * Gets an array of CommonDossierVolumineux objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     * @throws PropelException
     */
    public function getCommonDossierVolumineuxs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonDossierVolumineuxsPartial && !$this->isNew();
        if (null === $this->collCommonDossierVolumineuxs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonDossierVolumineuxs) {
                // return empty collection
                $this->initCommonDossierVolumineuxs();
            } else {
                $collCommonDossierVolumineuxs = CommonDossierVolumineuxQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonDossierVolumineuxsPartial && count($collCommonDossierVolumineuxs)) {
                      $this->initCommonDossierVolumineuxs(false);

                      foreach ($collCommonDossierVolumineuxs as $obj) {
                        if (false == $this->collCommonDossierVolumineuxs->contains($obj)) {
                          $this->collCommonDossierVolumineuxs->append($obj);
                        }
                      }

                      $this->collCommonDossierVolumineuxsPartial = true;
                    }

                    $collCommonDossierVolumineuxs->getInternalIterator()->rewind();

                    return $collCommonDossierVolumineuxs;
                }

                if ($partial && $this->collCommonDossierVolumineuxs) {
                    foreach ($this->collCommonDossierVolumineuxs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonDossierVolumineuxs[] = $obj;
                        }
                    }
                }

                $this->collCommonDossierVolumineuxs = $collCommonDossierVolumineuxs;
                $this->collCommonDossierVolumineuxsPartial = false;
            }
        }

        return $this->collCommonDossierVolumineuxs;
    }

    /**
     * Sets a collection of CommonDossierVolumineux objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonDossierVolumineuxs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonDossierVolumineuxs(PropelCollection $commonDossierVolumineuxs, PropelPDO $con = null)
    {
        $commonDossierVolumineuxsToDelete = $this->getCommonDossierVolumineuxs(new Criteria(), $con)->diff($commonDossierVolumineuxs);


        $this->commonDossierVolumineuxsScheduledForDeletion = $commonDossierVolumineuxsToDelete;

        foreach ($commonDossierVolumineuxsToDelete as $commonDossierVolumineuxRemoved) {
            $commonDossierVolumineuxRemoved->setCommonInscrit(null);
        }

        $this->collCommonDossierVolumineuxs = null;
        foreach ($commonDossierVolumineuxs as $commonDossierVolumineux) {
            $this->addCommonDossierVolumineux($commonDossierVolumineux);
        }

        $this->collCommonDossierVolumineuxs = $commonDossierVolumineuxs;
        $this->collCommonDossierVolumineuxsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonDossierVolumineux objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonDossierVolumineux objects.
     * @throws PropelException
     */
    public function countCommonDossierVolumineuxs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonDossierVolumineuxsPartial && !$this->isNew();
        if (null === $this->collCommonDossierVolumineuxs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonDossierVolumineuxs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonDossierVolumineuxs());
            }
            $query = CommonDossierVolumineuxQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonDossierVolumineuxs);
    }

    /**
     * Method called to associate a CommonDossierVolumineux object to this object
     * through the CommonDossierVolumineux foreign key attribute.
     *
     * @param   CommonDossierVolumineux $l CommonDossierVolumineux
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonDossierVolumineux(CommonDossierVolumineux $l)
    {
        if ($this->collCommonDossierVolumineuxs === null) {
            $this->initCommonDossierVolumineuxs();
            $this->collCommonDossierVolumineuxsPartial = true;
        }
        if (!in_array($l, $this->collCommonDossierVolumineuxs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonDossierVolumineux($l);
        }

        return $this;
    }

    /**
     * @param	CommonDossierVolumineux $commonDossierVolumineux The commonDossierVolumineux object to add.
     */
    protected function doAddCommonDossierVolumineux($commonDossierVolumineux)
    {
        $this->collCommonDossierVolumineuxs[]= $commonDossierVolumineux;
        $commonDossierVolumineux->setCommonInscrit($this);
    }

    /**
     * @param	CommonDossierVolumineux $commonDossierVolumineux The commonDossierVolumineux object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonDossierVolumineux($commonDossierVolumineux)
    {
        if ($this->getCommonDossierVolumineuxs()->contains($commonDossierVolumineux)) {
            $this->collCommonDossierVolumineuxs->remove($this->collCommonDossierVolumineuxs->search($commonDossierVolumineux));
            if (null === $this->commonDossierVolumineuxsScheduledForDeletion) {
                $this->commonDossierVolumineuxsScheduledForDeletion = clone $this->collCommonDossierVolumineuxs;
                $this->commonDossierVolumineuxsScheduledForDeletion->clear();
            }
            $this->commonDossierVolumineuxsScheduledForDeletion[]= $commonDossierVolumineux;
            $commonDossierVolumineux->setCommonInscrit(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonDossierVolumineuxs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     */
    public function getCommonDossierVolumineuxsJoinCommonBlobFileRelatedByIdBlobDescripteur($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonDossierVolumineuxQuery::create(null, $criteria);
        $query->joinWith('CommonBlobFileRelatedByIdBlobDescripteur', $join_behavior);

        return $this->getCommonDossierVolumineuxs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonDossierVolumineuxs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     */
    public function getCommonDossierVolumineuxsJoinCommonBlobFileRelatedByIdBlobLogfile($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonDossierVolumineuxQuery::create(null, $criteria);
        $query->joinWith('CommonBlobFileRelatedByIdBlobLogfile', $join_behavior);

        return $this->getCommonDossierVolumineuxs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonDossierVolumineuxs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     */
    public function getCommonDossierVolumineuxsJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonDossierVolumineuxQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonDossierVolumineuxs($query, $con);
    }

    /**
     * Clears out the collCommonHistorisationMotDePasses collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonHistorisationMotDePasses()
     */
    public function clearCommonHistorisationMotDePasses()
    {
        $this->collCommonHistorisationMotDePasses = null; // important to set this to null since that means it is uninitialized
        $this->collCommonHistorisationMotDePassesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonHistorisationMotDePasses collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonHistorisationMotDePasses($v = true)
    {
        $this->collCommonHistorisationMotDePassesPartial = $v;
    }

    /**
     * Initializes the collCommonHistorisationMotDePasses collection.
     *
     * By default this just sets the collCommonHistorisationMotDePasses collection to an empty array (like clearcollCommonHistorisationMotDePasses());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonHistorisationMotDePasses($overrideExisting = true)
    {
        if (null !== $this->collCommonHistorisationMotDePasses && !$overrideExisting) {
            return;
        }
        $this->collCommonHistorisationMotDePasses = new PropelObjectCollection();
        $this->collCommonHistorisationMotDePasses->setModel('CommonHistorisationMotDePasse');
    }

    /**
     * Gets an array of CommonHistorisationMotDePasse objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonHistorisationMotDePasse[] List of CommonHistorisationMotDePasse objects
     * @throws PropelException
     */
    public function getCommonHistorisationMotDePasses($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonHistorisationMotDePassesPartial && !$this->isNew();
        if (null === $this->collCommonHistorisationMotDePasses || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonHistorisationMotDePasses) {
                // return empty collection
                $this->initCommonHistorisationMotDePasses();
            } else {
                $collCommonHistorisationMotDePasses = CommonHistorisationMotDePasseQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonHistorisationMotDePassesPartial && count($collCommonHistorisationMotDePasses)) {
                      $this->initCommonHistorisationMotDePasses(false);

                      foreach ($collCommonHistorisationMotDePasses as $obj) {
                        if (false == $this->collCommonHistorisationMotDePasses->contains($obj)) {
                          $this->collCommonHistorisationMotDePasses->append($obj);
                        }
                      }

                      $this->collCommonHistorisationMotDePassesPartial = true;
                    }

                    $collCommonHistorisationMotDePasses->getInternalIterator()->rewind();

                    return $collCommonHistorisationMotDePasses;
                }

                if ($partial && $this->collCommonHistorisationMotDePasses) {
                    foreach ($this->collCommonHistorisationMotDePasses as $obj) {
                        if ($obj->isNew()) {
                            $collCommonHistorisationMotDePasses[] = $obj;
                        }
                    }
                }

                $this->collCommonHistorisationMotDePasses = $collCommonHistorisationMotDePasses;
                $this->collCommonHistorisationMotDePassesPartial = false;
            }
        }

        return $this->collCommonHistorisationMotDePasses;
    }

    /**
     * Sets a collection of CommonHistorisationMotDePasse objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonHistorisationMotDePasses A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonHistorisationMotDePasses(PropelCollection $commonHistorisationMotDePasses, PropelPDO $con = null)
    {
        $commonHistorisationMotDePassesToDelete = $this->getCommonHistorisationMotDePasses(new Criteria(), $con)->diff($commonHistorisationMotDePasses);


        $this->commonHistorisationMotDePassesScheduledForDeletion = $commonHistorisationMotDePassesToDelete;

        foreach ($commonHistorisationMotDePassesToDelete as $commonHistorisationMotDePasseRemoved) {
            $commonHistorisationMotDePasseRemoved->setCommonInscrit(null);
        }

        $this->collCommonHistorisationMotDePasses = null;
        foreach ($commonHistorisationMotDePasses as $commonHistorisationMotDePasse) {
            $this->addCommonHistorisationMotDePasse($commonHistorisationMotDePasse);
        }

        $this->collCommonHistorisationMotDePasses = $commonHistorisationMotDePasses;
        $this->collCommonHistorisationMotDePassesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonHistorisationMotDePasse objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonHistorisationMotDePasse objects.
     * @throws PropelException
     */
    public function countCommonHistorisationMotDePasses(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonHistorisationMotDePassesPartial && !$this->isNew();
        if (null === $this->collCommonHistorisationMotDePasses || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonHistorisationMotDePasses) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonHistorisationMotDePasses());
            }
            $query = CommonHistorisationMotDePasseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonHistorisationMotDePasses);
    }

    /**
     * Method called to associate a CommonHistorisationMotDePasse object to this object
     * through the CommonHistorisationMotDePasse foreign key attribute.
     *
     * @param   CommonHistorisationMotDePasse $l CommonHistorisationMotDePasse
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonHistorisationMotDePasse(CommonHistorisationMotDePasse $l)
    {
        if ($this->collCommonHistorisationMotDePasses === null) {
            $this->initCommonHistorisationMotDePasses();
            $this->collCommonHistorisationMotDePassesPartial = true;
        }
        if (!in_array($l, $this->collCommonHistorisationMotDePasses->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonHistorisationMotDePasse($l);
        }

        return $this;
    }

    /**
     * @param	CommonHistorisationMotDePasse $commonHistorisationMotDePasse The commonHistorisationMotDePasse object to add.
     */
    protected function doAddCommonHistorisationMotDePasse($commonHistorisationMotDePasse)
    {
        $this->collCommonHistorisationMotDePasses[]= $commonHistorisationMotDePasse;
        $commonHistorisationMotDePasse->setCommonInscrit($this);
    }

    /**
     * @param	CommonHistorisationMotDePasse $commonHistorisationMotDePasse The commonHistorisationMotDePasse object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonHistorisationMotDePasse($commonHistorisationMotDePasse)
    {
        if ($this->getCommonHistorisationMotDePasses()->contains($commonHistorisationMotDePasse)) {
            $this->collCommonHistorisationMotDePasses->remove($this->collCommonHistorisationMotDePasses->search($commonHistorisationMotDePasse));
            if (null === $this->commonHistorisationMotDePassesScheduledForDeletion) {
                $this->commonHistorisationMotDePassesScheduledForDeletion = clone $this->collCommonHistorisationMotDePasses;
                $this->commonHistorisationMotDePassesScheduledForDeletion->clear();
            }
            $this->commonHistorisationMotDePassesScheduledForDeletion[]= $commonHistorisationMotDePasse;
            $commonHistorisationMotDePasse->setCommonInscrit(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonQuestionsDces collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonQuestionsDces()
     */
    public function clearCommonQuestionsDces()
    {
        $this->collCommonQuestionsDces = null; // important to set this to null since that means it is uninitialized
        $this->collCommonQuestionsDcesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonQuestionsDces collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonQuestionsDces($v = true)
    {
        $this->collCommonQuestionsDcesPartial = $v;
    }

    /**
     * Initializes the collCommonQuestionsDces collection.
     *
     * By default this just sets the collCommonQuestionsDces collection to an empty array (like clearcollCommonQuestionsDces());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonQuestionsDces($overrideExisting = true)
    {
        if (null !== $this->collCommonQuestionsDces && !$overrideExisting) {
            return;
        }
        $this->collCommonQuestionsDces = new PropelObjectCollection();
        $this->collCommonQuestionsDces->setModel('CommonQuestionsDce');
    }

    /**
     * Gets an array of CommonQuestionsDce objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonQuestionsDce[] List of CommonQuestionsDce objects
     * @throws PropelException
     */
    public function getCommonQuestionsDces($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonQuestionsDcesPartial && !$this->isNew();
        if (null === $this->collCommonQuestionsDces || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonQuestionsDces) {
                // return empty collection
                $this->initCommonQuestionsDces();
            } else {
                $collCommonQuestionsDces = CommonQuestionsDceQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonQuestionsDcesPartial && count($collCommonQuestionsDces)) {
                      $this->initCommonQuestionsDces(false);

                      foreach ($collCommonQuestionsDces as $obj) {
                        if (false == $this->collCommonQuestionsDces->contains($obj)) {
                          $this->collCommonQuestionsDces->append($obj);
                        }
                      }

                      $this->collCommonQuestionsDcesPartial = true;
                    }

                    $collCommonQuestionsDces->getInternalIterator()->rewind();

                    return $collCommonQuestionsDces;
                }

                if ($partial && $this->collCommonQuestionsDces) {
                    foreach ($this->collCommonQuestionsDces as $obj) {
                        if ($obj->isNew()) {
                            $collCommonQuestionsDces[] = $obj;
                        }
                    }
                }

                $this->collCommonQuestionsDces = $collCommonQuestionsDces;
                $this->collCommonQuestionsDcesPartial = false;
            }
        }

        return $this->collCommonQuestionsDces;
    }

    /**
     * Sets a collection of CommonQuestionsDce objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonQuestionsDces A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonQuestionsDces(PropelCollection $commonQuestionsDces, PropelPDO $con = null)
    {
        $commonQuestionsDcesToDelete = $this->getCommonQuestionsDces(new Criteria(), $con)->diff($commonQuestionsDces);


        $this->commonQuestionsDcesScheduledForDeletion = $commonQuestionsDcesToDelete;

        foreach ($commonQuestionsDcesToDelete as $commonQuestionsDceRemoved) {
            $commonQuestionsDceRemoved->setCommonInscrit(null);
        }

        $this->collCommonQuestionsDces = null;
        foreach ($commonQuestionsDces as $commonQuestionsDce) {
            $this->addCommonQuestionsDce($commonQuestionsDce);
        }

        $this->collCommonQuestionsDces = $commonQuestionsDces;
        $this->collCommonQuestionsDcesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonQuestionsDce objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonQuestionsDce objects.
     * @throws PropelException
     */
    public function countCommonQuestionsDces(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonQuestionsDcesPartial && !$this->isNew();
        if (null === $this->collCommonQuestionsDces || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonQuestionsDces) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonQuestionsDces());
            }
            $query = CommonQuestionsDceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonQuestionsDces);
    }

    /**
     * Method called to associate a CommonQuestionsDce object to this object
     * through the CommonQuestionsDce foreign key attribute.
     *
     * @param   CommonQuestionsDce $l CommonQuestionsDce
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonQuestionsDce(CommonQuestionsDce $l)
    {
        if ($this->collCommonQuestionsDces === null) {
            $this->initCommonQuestionsDces();
            $this->collCommonQuestionsDcesPartial = true;
        }
        if (!in_array($l, $this->collCommonQuestionsDces->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonQuestionsDce($l);
        }

        return $this;
    }

    /**
     * @param	CommonQuestionsDce $commonQuestionsDce The commonQuestionsDce object to add.
     */
    protected function doAddCommonQuestionsDce($commonQuestionsDce)
    {
        $this->collCommonQuestionsDces[]= $commonQuestionsDce;
        $commonQuestionsDce->setCommonInscrit($this);
    }

    /**
     * @param	CommonQuestionsDce $commonQuestionsDce The commonQuestionsDce object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonQuestionsDce($commonQuestionsDce)
    {
        if ($this->getCommonQuestionsDces()->contains($commonQuestionsDce)) {
            $this->collCommonQuestionsDces->remove($this->collCommonQuestionsDces->search($commonQuestionsDce));
            if (null === $this->commonQuestionsDcesScheduledForDeletion) {
                $this->commonQuestionsDcesScheduledForDeletion = clone $this->collCommonQuestionsDces;
                $this->commonQuestionsDcesScheduledForDeletion->clear();
            }
            $this->commonQuestionsDcesScheduledForDeletion[]= $commonQuestionsDce;
            $commonQuestionsDce->setCommonInscrit(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonQuestionsDces from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonQuestionsDce[] List of CommonQuestionsDce objects
     */
    public function getCommonQuestionsDcesJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonQuestionsDceQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonQuestionsDces($query, $con);
    }

    /**
     * Clears out the collCommonSsoEntreprises collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonSsoEntreprises()
     */
    public function clearCommonSsoEntreprises()
    {
        $this->collCommonSsoEntreprises = null; // important to set this to null since that means it is uninitialized
        $this->collCommonSsoEntreprisesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonSsoEntreprises collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonSsoEntreprises($v = true)
    {
        $this->collCommonSsoEntreprisesPartial = $v;
    }

    /**
     * Initializes the collCommonSsoEntreprises collection.
     *
     * By default this just sets the collCommonSsoEntreprises collection to an empty array (like clearcollCommonSsoEntreprises());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonSsoEntreprises($overrideExisting = true)
    {
        if (null !== $this->collCommonSsoEntreprises && !$overrideExisting) {
            return;
        }
        $this->collCommonSsoEntreprises = new PropelObjectCollection();
        $this->collCommonSsoEntreprises->setModel('CommonSsoEntreprise');
    }

    /**
     * Gets an array of CommonSsoEntreprise objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonSsoEntreprise[] List of CommonSsoEntreprise objects
     * @throws PropelException
     */
    public function getCommonSsoEntreprises($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonSsoEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonSsoEntreprises || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonSsoEntreprises) {
                // return empty collection
                $this->initCommonSsoEntreprises();
            } else {
                $collCommonSsoEntreprises = CommonSsoEntrepriseQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonSsoEntreprisesPartial && count($collCommonSsoEntreprises)) {
                      $this->initCommonSsoEntreprises(false);

                      foreach ($collCommonSsoEntreprises as $obj) {
                        if (false == $this->collCommonSsoEntreprises->contains($obj)) {
                          $this->collCommonSsoEntreprises->append($obj);
                        }
                      }

                      $this->collCommonSsoEntreprisesPartial = true;
                    }

                    $collCommonSsoEntreprises->getInternalIterator()->rewind();

                    return $collCommonSsoEntreprises;
                }

                if ($partial && $this->collCommonSsoEntreprises) {
                    foreach ($this->collCommonSsoEntreprises as $obj) {
                        if ($obj->isNew()) {
                            $collCommonSsoEntreprises[] = $obj;
                        }
                    }
                }

                $this->collCommonSsoEntreprises = $collCommonSsoEntreprises;
                $this->collCommonSsoEntreprisesPartial = false;
            }
        }

        return $this->collCommonSsoEntreprises;
    }

    /**
     * Sets a collection of CommonSsoEntreprise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonSsoEntreprises A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonSsoEntreprises(PropelCollection $commonSsoEntreprises, PropelPDO $con = null)
    {
        $commonSsoEntreprisesToDelete = $this->getCommonSsoEntreprises(new Criteria(), $con)->diff($commonSsoEntreprises);


        $this->commonSsoEntreprisesScheduledForDeletion = $commonSsoEntreprisesToDelete;

        foreach ($commonSsoEntreprisesToDelete as $commonSsoEntrepriseRemoved) {
            $commonSsoEntrepriseRemoved->setCommonInscrit(null);
        }

        $this->collCommonSsoEntreprises = null;
        foreach ($commonSsoEntreprises as $commonSsoEntreprise) {
            $this->addCommonSsoEntreprise($commonSsoEntreprise);
        }

        $this->collCommonSsoEntreprises = $commonSsoEntreprises;
        $this->collCommonSsoEntreprisesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonSsoEntreprise objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonSsoEntreprise objects.
     * @throws PropelException
     */
    public function countCommonSsoEntreprises(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonSsoEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonSsoEntreprises || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonSsoEntreprises) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonSsoEntreprises());
            }
            $query = CommonSsoEntrepriseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonSsoEntreprises);
    }

    /**
     * Method called to associate a CommonSsoEntreprise object to this object
     * through the CommonSsoEntreprise foreign key attribute.
     *
     * @param   CommonSsoEntreprise $l CommonSsoEntreprise
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonSsoEntreprise(CommonSsoEntreprise $l)
    {
        if ($this->collCommonSsoEntreprises === null) {
            $this->initCommonSsoEntreprises();
            $this->collCommonSsoEntreprisesPartial = true;
        }
        if (!in_array($l, $this->collCommonSsoEntreprises->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonSsoEntreprise($l);
        }

        return $this;
    }

    /**
     * @param	CommonSsoEntreprise $commonSsoEntreprise The commonSsoEntreprise object to add.
     */
    protected function doAddCommonSsoEntreprise($commonSsoEntreprise)
    {
        $this->collCommonSsoEntreprises[]= $commonSsoEntreprise;
        $commonSsoEntreprise->setCommonInscrit($this);
    }

    /**
     * @param	CommonSsoEntreprise $commonSsoEntreprise The commonSsoEntreprise object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonSsoEntreprise($commonSsoEntreprise)
    {
        if ($this->getCommonSsoEntreprises()->contains($commonSsoEntreprise)) {
            $this->collCommonSsoEntreprises->remove($this->collCommonSsoEntreprises->search($commonSsoEntreprise));
            if (null === $this->commonSsoEntreprisesScheduledForDeletion) {
                $this->commonSsoEntreprisesScheduledForDeletion = clone $this->collCommonSsoEntreprises;
                $this->commonSsoEntreprisesScheduledForDeletion->clear();
            }
            $this->commonSsoEntreprisesScheduledForDeletion[]= $commonSsoEntreprise;
            $commonSsoEntreprise->setCommonInscrit(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTCandidatures collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonTCandidatures()
     */
    public function clearCommonTCandidatures()
    {
        $this->collCommonTCandidatures = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCandidaturesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCandidatures collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCandidatures($v = true)
    {
        $this->collCommonTCandidaturesPartial = $v;
    }

    /**
     * Initializes the collCommonTCandidatures collection.
     *
     * By default this just sets the collCommonTCandidatures collection to an empty array (like clearcollCommonTCandidatures());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCandidatures($overrideExisting = true)
    {
        if (null !== $this->collCommonTCandidatures && !$overrideExisting) {
            return;
        }
        $this->collCommonTCandidatures = new PropelObjectCollection();
        $this->collCommonTCandidatures->setModel('CommonTCandidature');
    }

    /**
     * Gets an array of CommonTCandidature objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     * @throws PropelException
     */
    public function getCommonTCandidatures($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTCandidatures || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCandidatures) {
                // return empty collection
                $this->initCommonTCandidatures();
            } else {
                $collCommonTCandidatures = CommonTCandidatureQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCandidaturesPartial && count($collCommonTCandidatures)) {
                      $this->initCommonTCandidatures(false);

                      foreach ($collCommonTCandidatures as $obj) {
                        if (false == $this->collCommonTCandidatures->contains($obj)) {
                          $this->collCommonTCandidatures->append($obj);
                        }
                      }

                      $this->collCommonTCandidaturesPartial = true;
                    }

                    $collCommonTCandidatures->getInternalIterator()->rewind();

                    return $collCommonTCandidatures;
                }

                if ($partial && $this->collCommonTCandidatures) {
                    foreach ($this->collCommonTCandidatures as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCandidatures[] = $obj;
                        }
                    }
                }

                $this->collCommonTCandidatures = $collCommonTCandidatures;
                $this->collCommonTCandidaturesPartial = false;
            }
        }

        return $this->collCommonTCandidatures;
    }

    /**
     * Sets a collection of CommonTCandidature objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCandidatures A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonTCandidatures(PropelCollection $commonTCandidatures, PropelPDO $con = null)
    {
        $commonTCandidaturesToDelete = $this->getCommonTCandidatures(new Criteria(), $con)->diff($commonTCandidatures);


        $this->commonTCandidaturesScheduledForDeletion = $commonTCandidaturesToDelete;

        foreach ($commonTCandidaturesToDelete as $commonTCandidatureRemoved) {
            $commonTCandidatureRemoved->setCommonInscrit(null);
        }

        $this->collCommonTCandidatures = null;
        foreach ($commonTCandidatures as $commonTCandidature) {
            $this->addCommonTCandidature($commonTCandidature);
        }

        $this->collCommonTCandidatures = $commonTCandidatures;
        $this->collCommonTCandidaturesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCandidature objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCandidature objects.
     * @throws PropelException
     */
    public function countCommonTCandidatures(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTCandidatures || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCandidatures) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCandidatures());
            }
            $query = CommonTCandidatureQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonTCandidatures);
    }

    /**
     * Method called to associate a CommonTCandidature object to this object
     * through the CommonTCandidature foreign key attribute.
     *
     * @param   CommonTCandidature $l CommonTCandidature
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonTCandidature(CommonTCandidature $l)
    {
        if ($this->collCommonTCandidatures === null) {
            $this->initCommonTCandidatures();
            $this->collCommonTCandidaturesPartial = true;
        }
        if (!in_array($l, $this->collCommonTCandidatures->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCandidature($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCandidature $commonTCandidature The commonTCandidature object to add.
     */
    protected function doAddCommonTCandidature($commonTCandidature)
    {
        $this->collCommonTCandidatures[]= $commonTCandidature;
        $commonTCandidature->setCommonInscrit($this);
    }

    /**
     * @param	CommonTCandidature $commonTCandidature The commonTCandidature object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonTCandidature($commonTCandidature)
    {
        if ($this->getCommonTCandidatures()->contains($commonTCandidature)) {
            $this->collCommonTCandidatures->remove($this->collCommonTCandidatures->search($commonTCandidature));
            if (null === $this->commonTCandidaturesScheduledForDeletion) {
                $this->commonTCandidaturesScheduledForDeletion = clone $this->collCommonTCandidatures;
                $this->commonTCandidaturesScheduledForDeletion->clear();
            }
            $this->commonTCandidaturesScheduledForDeletion[]= $commonTCandidature;
            $commonTCandidature->setCommonInscrit(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonTDumeContexte($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonTDumeContexte', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonOffres($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonOffres', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCandidature[] List of CommonTCandidature objects
     */
    public function getCommonTCandidaturesJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonTCandidatures($query, $con);
    }

    /**
     * Clears out the collCommonTCandidatureMpss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonTCandidatureMpss()
     */
    public function clearCommonTCandidatureMpss()
    {
        $this->collCommonTCandidatureMpss = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCandidatureMpssPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCandidatureMpss collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCandidatureMpss($v = true)
    {
        $this->collCommonTCandidatureMpssPartial = $v;
    }

    /**
     * Initializes the collCommonTCandidatureMpss collection.
     *
     * By default this just sets the collCommonTCandidatureMpss collection to an empty array (like clearcollCommonTCandidatureMpss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCandidatureMpss($overrideExisting = true)
    {
        if (null !== $this->collCommonTCandidatureMpss && !$overrideExisting) {
            return;
        }
        $this->collCommonTCandidatureMpss = new PropelObjectCollection();
        $this->collCommonTCandidatureMpss->setModel('CommonTCandidatureMps');
    }

    /**
     * Gets an array of CommonTCandidatureMps objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCandidatureMps[] List of CommonTCandidatureMps objects
     * @throws PropelException
     */
    public function getCommonTCandidatureMpss($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCandidatureMpssPartial && !$this->isNew();
        if (null === $this->collCommonTCandidatureMpss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCandidatureMpss) {
                // return empty collection
                $this->initCommonTCandidatureMpss();
            } else {
                $collCommonTCandidatureMpss = CommonTCandidatureMpsQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCandidatureMpssPartial && count($collCommonTCandidatureMpss)) {
                      $this->initCommonTCandidatureMpss(false);

                      foreach ($collCommonTCandidatureMpss as $obj) {
                        if (false == $this->collCommonTCandidatureMpss->contains($obj)) {
                          $this->collCommonTCandidatureMpss->append($obj);
                        }
                      }

                      $this->collCommonTCandidatureMpssPartial = true;
                    }

                    $collCommonTCandidatureMpss->getInternalIterator()->rewind();

                    return $collCommonTCandidatureMpss;
                }

                if ($partial && $this->collCommonTCandidatureMpss) {
                    foreach ($this->collCommonTCandidatureMpss as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCandidatureMpss[] = $obj;
                        }
                    }
                }

                $this->collCommonTCandidatureMpss = $collCommonTCandidatureMpss;
                $this->collCommonTCandidatureMpssPartial = false;
            }
        }

        return $this->collCommonTCandidatureMpss;
    }

    /**
     * Sets a collection of CommonTCandidatureMps objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCandidatureMpss A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonTCandidatureMpss(PropelCollection $commonTCandidatureMpss, PropelPDO $con = null)
    {
        $commonTCandidatureMpssToDelete = $this->getCommonTCandidatureMpss(new Criteria(), $con)->diff($commonTCandidatureMpss);


        $this->commonTCandidatureMpssScheduledForDeletion = $commonTCandidatureMpssToDelete;

        foreach ($commonTCandidatureMpssToDelete as $commonTCandidatureMpsRemoved) {
            $commonTCandidatureMpsRemoved->setCommonInscrit(null);
        }

        $this->collCommonTCandidatureMpss = null;
        foreach ($commonTCandidatureMpss as $commonTCandidatureMps) {
            $this->addCommonTCandidatureMps($commonTCandidatureMps);
        }

        $this->collCommonTCandidatureMpss = $commonTCandidatureMpss;
        $this->collCommonTCandidatureMpssPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCandidatureMps objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCandidatureMps objects.
     * @throws PropelException
     */
    public function countCommonTCandidatureMpss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCandidatureMpssPartial && !$this->isNew();
        if (null === $this->collCommonTCandidatureMpss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCandidatureMpss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCandidatureMpss());
            }
            $query = CommonTCandidatureMpsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonTCandidatureMpss);
    }

    /**
     * Method called to associate a CommonTCandidatureMps object to this object
     * through the CommonTCandidatureMps foreign key attribute.
     *
     * @param   CommonTCandidatureMps $l CommonTCandidatureMps
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonTCandidatureMps(CommonTCandidatureMps $l)
    {
        if ($this->collCommonTCandidatureMpss === null) {
            $this->initCommonTCandidatureMpss();
            $this->collCommonTCandidatureMpssPartial = true;
        }
        if (!in_array($l, $this->collCommonTCandidatureMpss->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCandidatureMps($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCandidatureMps $commonTCandidatureMps The commonTCandidatureMps object to add.
     */
    protected function doAddCommonTCandidatureMps($commonTCandidatureMps)
    {
        $this->collCommonTCandidatureMpss[]= $commonTCandidatureMps;
        $commonTCandidatureMps->setCommonInscrit($this);
    }

    /**
     * @param	CommonTCandidatureMps $commonTCandidatureMps The commonTCandidatureMps object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonTCandidatureMps($commonTCandidatureMps)
    {
        if ($this->getCommonTCandidatureMpss()->contains($commonTCandidatureMps)) {
            $this->collCommonTCandidatureMpss->remove($this->collCommonTCandidatureMpss->search($commonTCandidatureMps));
            if (null === $this->commonTCandidatureMpssScheduledForDeletion) {
                $this->commonTCandidatureMpssScheduledForDeletion = clone $this->collCommonTCandidatureMpss;
                $this->commonTCandidatureMpssScheduledForDeletion->clear();
            }
            $this->commonTCandidatureMpssScheduledForDeletion[]= $commonTCandidatureMps;
            $commonTCandidatureMps->setCommonInscrit(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTContactContrats collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonTContactContrats()
     */
    public function clearCommonTContactContrats()
    {
        $this->collCommonTContactContrats = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTContactContratsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTContactContrats collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTContactContrats($v = true)
    {
        $this->collCommonTContactContratsPartial = $v;
    }

    /**
     * Initializes the collCommonTContactContrats collection.
     *
     * By default this just sets the collCommonTContactContrats collection to an empty array (like clearcollCommonTContactContrats());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTContactContrats($overrideExisting = true)
    {
        if (null !== $this->collCommonTContactContrats && !$overrideExisting) {
            return;
        }
        $this->collCommonTContactContrats = new PropelObjectCollection();
        $this->collCommonTContactContrats->setModel('CommonTContactContrat');
    }

    /**
     * Gets an array of CommonTContactContrat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTContactContrat[] List of CommonTContactContrat objects
     * @throws PropelException
     */
    public function getCommonTContactContrats($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTContactContratsPartial && !$this->isNew();
        if (null === $this->collCommonTContactContrats || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTContactContrats) {
                // return empty collection
                $this->initCommonTContactContrats();
            } else {
                $collCommonTContactContrats = CommonTContactContratQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTContactContratsPartial && count($collCommonTContactContrats)) {
                      $this->initCommonTContactContrats(false);

                      foreach ($collCommonTContactContrats as $obj) {
                        if (false == $this->collCommonTContactContrats->contains($obj)) {
                          $this->collCommonTContactContrats->append($obj);
                        }
                      }

                      $this->collCommonTContactContratsPartial = true;
                    }

                    $collCommonTContactContrats->getInternalIterator()->rewind();

                    return $collCommonTContactContrats;
                }

                if ($partial && $this->collCommonTContactContrats) {
                    foreach ($this->collCommonTContactContrats as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTContactContrats[] = $obj;
                        }
                    }
                }

                $this->collCommonTContactContrats = $collCommonTContactContrats;
                $this->collCommonTContactContratsPartial = false;
            }
        }

        return $this->collCommonTContactContrats;
    }

    /**
     * Sets a collection of CommonTContactContrat objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTContactContrats A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonTContactContrats(PropelCollection $commonTContactContrats, PropelPDO $con = null)
    {
        $commonTContactContratsToDelete = $this->getCommonTContactContrats(new Criteria(), $con)->diff($commonTContactContrats);


        $this->commonTContactContratsScheduledForDeletion = $commonTContactContratsToDelete;

        foreach ($commonTContactContratsToDelete as $commonTContactContratRemoved) {
            $commonTContactContratRemoved->setCommonInscrit(null);
        }

        $this->collCommonTContactContrats = null;
        foreach ($commonTContactContrats as $commonTContactContrat) {
            $this->addCommonTContactContrat($commonTContactContrat);
        }

        $this->collCommonTContactContrats = $commonTContactContrats;
        $this->collCommonTContactContratsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTContactContrat objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTContactContrat objects.
     * @throws PropelException
     */
    public function countCommonTContactContrats(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTContactContratsPartial && !$this->isNew();
        if (null === $this->collCommonTContactContrats || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTContactContrats) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTContactContrats());
            }
            $query = CommonTContactContratQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonTContactContrats);
    }

    /**
     * Method called to associate a CommonTContactContrat object to this object
     * through the CommonTContactContrat foreign key attribute.
     *
     * @param   CommonTContactContrat $l CommonTContactContrat
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonTContactContrat(CommonTContactContrat $l)
    {
        if ($this->collCommonTContactContrats === null) {
            $this->initCommonTContactContrats();
            $this->collCommonTContactContratsPartial = true;
        }
        if (!in_array($l, $this->collCommonTContactContrats->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTContactContrat($l);
        }

        return $this;
    }

    /**
     * @param	CommonTContactContrat $commonTContactContrat The commonTContactContrat object to add.
     */
    protected function doAddCommonTContactContrat($commonTContactContrat)
    {
        $this->collCommonTContactContrats[]= $commonTContactContrat;
        $commonTContactContrat->setCommonInscrit($this);
    }

    /**
     * @param	CommonTContactContrat $commonTContactContrat The commonTContactContrat object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonTContactContrat($commonTContactContrat)
    {
        if ($this->getCommonTContactContrats()->contains($commonTContactContrat)) {
            $this->collCommonTContactContrats->remove($this->collCommonTContactContrats->search($commonTContactContrat));
            if (null === $this->commonTContactContratsScheduledForDeletion) {
                $this->commonTContactContratsScheduledForDeletion = clone $this->collCommonTContactContrats;
                $this->commonTContactContratsScheduledForDeletion->clear();
            }
            $this->commonTContactContratsScheduledForDeletion[]= $commonTContactContrat;
            $commonTContactContrat->setCommonInscrit(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTListeLotsCandidatures collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonTListeLotsCandidatures()
     */
    public function clearCommonTListeLotsCandidatures()
    {
        $this->collCommonTListeLotsCandidatures = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTListeLotsCandidaturesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTListeLotsCandidatures collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTListeLotsCandidatures($v = true)
    {
        $this->collCommonTListeLotsCandidaturesPartial = $v;
    }

    /**
     * Initializes the collCommonTListeLotsCandidatures collection.
     *
     * By default this just sets the collCommonTListeLotsCandidatures collection to an empty array (like clearcollCommonTListeLotsCandidatures());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTListeLotsCandidatures($overrideExisting = true)
    {
        if (null !== $this->collCommonTListeLotsCandidatures && !$overrideExisting) {
            return;
        }
        $this->collCommonTListeLotsCandidatures = new PropelObjectCollection();
        $this->collCommonTListeLotsCandidatures->setModel('CommonTListeLotsCandidature');
    }

    /**
     * Gets an array of CommonTListeLotsCandidature objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     * @throws PropelException
     */
    public function getCommonTListeLotsCandidatures($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTListeLotsCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTListeLotsCandidatures || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTListeLotsCandidatures) {
                // return empty collection
                $this->initCommonTListeLotsCandidatures();
            } else {
                $collCommonTListeLotsCandidatures = CommonTListeLotsCandidatureQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTListeLotsCandidaturesPartial && count($collCommonTListeLotsCandidatures)) {
                      $this->initCommonTListeLotsCandidatures(false);

                      foreach ($collCommonTListeLotsCandidatures as $obj) {
                        if (false == $this->collCommonTListeLotsCandidatures->contains($obj)) {
                          $this->collCommonTListeLotsCandidatures->append($obj);
                        }
                      }

                      $this->collCommonTListeLotsCandidaturesPartial = true;
                    }

                    $collCommonTListeLotsCandidatures->getInternalIterator()->rewind();

                    return $collCommonTListeLotsCandidatures;
                }

                if ($partial && $this->collCommonTListeLotsCandidatures) {
                    foreach ($this->collCommonTListeLotsCandidatures as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTListeLotsCandidatures[] = $obj;
                        }
                    }
                }

                $this->collCommonTListeLotsCandidatures = $collCommonTListeLotsCandidatures;
                $this->collCommonTListeLotsCandidaturesPartial = false;
            }
        }

        return $this->collCommonTListeLotsCandidatures;
    }

    /**
     * Sets a collection of CommonTListeLotsCandidature objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTListeLotsCandidatures A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonTListeLotsCandidatures(PropelCollection $commonTListeLotsCandidatures, PropelPDO $con = null)
    {
        $commonTListeLotsCandidaturesToDelete = $this->getCommonTListeLotsCandidatures(new Criteria(), $con)->diff($commonTListeLotsCandidatures);


        $this->commonTListeLotsCandidaturesScheduledForDeletion = $commonTListeLotsCandidaturesToDelete;

        foreach ($commonTListeLotsCandidaturesToDelete as $commonTListeLotsCandidatureRemoved) {
            $commonTListeLotsCandidatureRemoved->setCommonInscrit(null);
        }

        $this->collCommonTListeLotsCandidatures = null;
        foreach ($commonTListeLotsCandidatures as $commonTListeLotsCandidature) {
            $this->addCommonTListeLotsCandidature($commonTListeLotsCandidature);
        }

        $this->collCommonTListeLotsCandidatures = $commonTListeLotsCandidatures;
        $this->collCommonTListeLotsCandidaturesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTListeLotsCandidature objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTListeLotsCandidature objects.
     * @throws PropelException
     */
    public function countCommonTListeLotsCandidatures(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTListeLotsCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTListeLotsCandidatures || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTListeLotsCandidatures) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTListeLotsCandidatures());
            }
            $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonTListeLotsCandidatures);
    }

    /**
     * Method called to associate a CommonTListeLotsCandidature object to this object
     * through the CommonTListeLotsCandidature foreign key attribute.
     *
     * @param   CommonTListeLotsCandidature $l CommonTListeLotsCandidature
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonTListeLotsCandidature(CommonTListeLotsCandidature $l)
    {
        if ($this->collCommonTListeLotsCandidatures === null) {
            $this->initCommonTListeLotsCandidatures();
            $this->collCommonTListeLotsCandidaturesPartial = true;
        }
        if (!in_array($l, $this->collCommonTListeLotsCandidatures->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTListeLotsCandidature($l);
        }

        return $this;
    }

    /**
     * @param	CommonTListeLotsCandidature $commonTListeLotsCandidature The commonTListeLotsCandidature object to add.
     */
    protected function doAddCommonTListeLotsCandidature($commonTListeLotsCandidature)
    {
        $this->collCommonTListeLotsCandidatures[]= $commonTListeLotsCandidature;
        $commonTListeLotsCandidature->setCommonInscrit($this);
    }

    /**
     * @param	CommonTListeLotsCandidature $commonTListeLotsCandidature The commonTListeLotsCandidature object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonTListeLotsCandidature($commonTListeLotsCandidature)
    {
        if ($this->getCommonTListeLotsCandidatures()->contains($commonTListeLotsCandidature)) {
            $this->collCommonTListeLotsCandidatures->remove($this->collCommonTListeLotsCandidatures->search($commonTListeLotsCandidature));
            if (null === $this->commonTListeLotsCandidaturesScheduledForDeletion) {
                $this->commonTListeLotsCandidaturesScheduledForDeletion = clone $this->collCommonTListeLotsCandidatures;
                $this->commonTListeLotsCandidaturesScheduledForDeletion->clear();
            }
            $this->commonTListeLotsCandidaturesScheduledForDeletion[]= $commonTListeLotsCandidature;
            $commonTListeLotsCandidature->setCommonInscrit(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTListeLotsCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     */
    public function getCommonTListeLotsCandidaturesJoinCommonTCandidature($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonTCandidature', $join_behavior);

        return $this->getCommonTListeLotsCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTListeLotsCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     */
    public function getCommonTListeLotsCandidaturesJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTListeLotsCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTListeLotsCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     */
    public function getCommonTListeLotsCandidaturesJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonTListeLotsCandidatures($query, $con);
    }

    /**
     * Clears out the collCommonTReponseElecFormulaires collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonTReponseElecFormulaires()
     */
    public function clearCommonTReponseElecFormulaires()
    {
        $this->collCommonTReponseElecFormulaires = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTReponseElecFormulairesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTReponseElecFormulaires collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTReponseElecFormulaires($v = true)
    {
        $this->collCommonTReponseElecFormulairesPartial = $v;
    }

    /**
     * Initializes the collCommonTReponseElecFormulaires collection.
     *
     * By default this just sets the collCommonTReponseElecFormulaires collection to an empty array (like clearcollCommonTReponseElecFormulaires());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTReponseElecFormulaires($overrideExisting = true)
    {
        if (null !== $this->collCommonTReponseElecFormulaires && !$overrideExisting) {
            return;
        }
        $this->collCommonTReponseElecFormulaires = new PropelObjectCollection();
        $this->collCommonTReponseElecFormulaires->setModel('CommonTReponseElecFormulaire');
    }

    /**
     * Gets an array of CommonTReponseElecFormulaire objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTReponseElecFormulaire[] List of CommonTReponseElecFormulaire objects
     * @throws PropelException
     */
    public function getCommonTReponseElecFormulaires($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTReponseElecFormulairesPartial && !$this->isNew();
        if (null === $this->collCommonTReponseElecFormulaires || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTReponseElecFormulaires) {
                // return empty collection
                $this->initCommonTReponseElecFormulaires();
            } else {
                $collCommonTReponseElecFormulaires = CommonTReponseElecFormulaireQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTReponseElecFormulairesPartial && count($collCommonTReponseElecFormulaires)) {
                      $this->initCommonTReponseElecFormulaires(false);

                      foreach ($collCommonTReponseElecFormulaires as $obj) {
                        if (false == $this->collCommonTReponseElecFormulaires->contains($obj)) {
                          $this->collCommonTReponseElecFormulaires->append($obj);
                        }
                      }

                      $this->collCommonTReponseElecFormulairesPartial = true;
                    }

                    $collCommonTReponseElecFormulaires->getInternalIterator()->rewind();

                    return $collCommonTReponseElecFormulaires;
                }

                if ($partial && $this->collCommonTReponseElecFormulaires) {
                    foreach ($this->collCommonTReponseElecFormulaires as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTReponseElecFormulaires[] = $obj;
                        }
                    }
                }

                $this->collCommonTReponseElecFormulaires = $collCommonTReponseElecFormulaires;
                $this->collCommonTReponseElecFormulairesPartial = false;
            }
        }

        return $this->collCommonTReponseElecFormulaires;
    }

    /**
     * Sets a collection of CommonTReponseElecFormulaire objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTReponseElecFormulaires A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonTReponseElecFormulaires(PropelCollection $commonTReponseElecFormulaires, PropelPDO $con = null)
    {
        $commonTReponseElecFormulairesToDelete = $this->getCommonTReponseElecFormulaires(new Criteria(), $con)->diff($commonTReponseElecFormulaires);


        $this->commonTReponseElecFormulairesScheduledForDeletion = $commonTReponseElecFormulairesToDelete;

        foreach ($commonTReponseElecFormulairesToDelete as $commonTReponseElecFormulaireRemoved) {
            $commonTReponseElecFormulaireRemoved->setCommonInscrit(null);
        }

        $this->collCommonTReponseElecFormulaires = null;
        foreach ($commonTReponseElecFormulaires as $commonTReponseElecFormulaire) {
            $this->addCommonTReponseElecFormulaire($commonTReponseElecFormulaire);
        }

        $this->collCommonTReponseElecFormulaires = $commonTReponseElecFormulaires;
        $this->collCommonTReponseElecFormulairesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTReponseElecFormulaire objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTReponseElecFormulaire objects.
     * @throws PropelException
     */
    public function countCommonTReponseElecFormulaires(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTReponseElecFormulairesPartial && !$this->isNew();
        if (null === $this->collCommonTReponseElecFormulaires || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTReponseElecFormulaires) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTReponseElecFormulaires());
            }
            $query = CommonTReponseElecFormulaireQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonTReponseElecFormulaires);
    }

    /**
     * Method called to associate a CommonTReponseElecFormulaire object to this object
     * through the CommonTReponseElecFormulaire foreign key attribute.
     *
     * @param   CommonTReponseElecFormulaire $l CommonTReponseElecFormulaire
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonTReponseElecFormulaire(CommonTReponseElecFormulaire $l)
    {
        if ($this->collCommonTReponseElecFormulaires === null) {
            $this->initCommonTReponseElecFormulaires();
            $this->collCommonTReponseElecFormulairesPartial = true;
        }
        if (!in_array($l, $this->collCommonTReponseElecFormulaires->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTReponseElecFormulaire($l);
        }

        return $this;
    }

    /**
     * @param	CommonTReponseElecFormulaire $commonTReponseElecFormulaire The commonTReponseElecFormulaire object to add.
     */
    protected function doAddCommonTReponseElecFormulaire($commonTReponseElecFormulaire)
    {
        $this->collCommonTReponseElecFormulaires[]= $commonTReponseElecFormulaire;
        $commonTReponseElecFormulaire->setCommonInscrit($this);
    }

    /**
     * @param	CommonTReponseElecFormulaire $commonTReponseElecFormulaire The commonTReponseElecFormulaire object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonTReponseElecFormulaire($commonTReponseElecFormulaire)
    {
        if ($this->getCommonTReponseElecFormulaires()->contains($commonTReponseElecFormulaire)) {
            $this->collCommonTReponseElecFormulaires->remove($this->collCommonTReponseElecFormulaires->search($commonTReponseElecFormulaire));
            if (null === $this->commonTReponseElecFormulairesScheduledForDeletion) {
                $this->commonTReponseElecFormulairesScheduledForDeletion = clone $this->collCommonTReponseElecFormulaires;
                $this->commonTReponseElecFormulairesScheduledForDeletion->clear();
            }
            $this->commonTReponseElecFormulairesScheduledForDeletion[]= $commonTReponseElecFormulaire;
            $commonTReponseElecFormulaire->setCommonInscrit(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTReponseElecFormulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTReponseElecFormulaire[] List of CommonTReponseElecFormulaire objects
     */
    public function getCommonTReponseElecFormulairesJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTReponseElecFormulaireQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonTReponseElecFormulaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonInscrit is new, it will return
     * an empty collection; or if this CommonInscrit has previously
     * been saved, it will retrieve related CommonTReponseElecFormulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonInscrit.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTReponseElecFormulaire[] List of CommonTReponseElecFormulaire objects
     */
    public function getCommonTReponseElecFormulairesJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTReponseElecFormulaireQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTReponseElecFormulaires($query, $con);
    }

    /**
     * Clears out the collCommonTmpSiretIncorrects collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonTmpSiretIncorrects()
     */
    public function clearCommonTmpSiretIncorrects()
    {
        $this->collCommonTmpSiretIncorrects = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTmpSiretIncorrectsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTmpSiretIncorrects collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTmpSiretIncorrects($v = true)
    {
        $this->collCommonTmpSiretIncorrectsPartial = $v;
    }

    /**
     * Initializes the collCommonTmpSiretIncorrects collection.
     *
     * By default this just sets the collCommonTmpSiretIncorrects collection to an empty array (like clearcollCommonTmpSiretIncorrects());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTmpSiretIncorrects($overrideExisting = true)
    {
        if (null !== $this->collCommonTmpSiretIncorrects && !$overrideExisting) {
            return;
        }
        $this->collCommonTmpSiretIncorrects = new PropelObjectCollection();
        $this->collCommonTmpSiretIncorrects->setModel('CommonTmpSiretIncorrect');
    }

    /**
     * Gets an array of CommonTmpSiretIncorrect objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTmpSiretIncorrect[] List of CommonTmpSiretIncorrect objects
     * @throws PropelException
     */
    public function getCommonTmpSiretIncorrects($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTmpSiretIncorrectsPartial && !$this->isNew();
        if (null === $this->collCommonTmpSiretIncorrects || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTmpSiretIncorrects) {
                // return empty collection
                $this->initCommonTmpSiretIncorrects();
            } else {
                $collCommonTmpSiretIncorrects = CommonTmpSiretIncorrectQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTmpSiretIncorrectsPartial && count($collCommonTmpSiretIncorrects)) {
                      $this->initCommonTmpSiretIncorrects(false);

                      foreach ($collCommonTmpSiretIncorrects as $obj) {
                        if (false == $this->collCommonTmpSiretIncorrects->contains($obj)) {
                          $this->collCommonTmpSiretIncorrects->append($obj);
                        }
                      }

                      $this->collCommonTmpSiretIncorrectsPartial = true;
                    }

                    $collCommonTmpSiretIncorrects->getInternalIterator()->rewind();

                    return $collCommonTmpSiretIncorrects;
                }

                if ($partial && $this->collCommonTmpSiretIncorrects) {
                    foreach ($this->collCommonTmpSiretIncorrects as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTmpSiretIncorrects[] = $obj;
                        }
                    }
                }

                $this->collCommonTmpSiretIncorrects = $collCommonTmpSiretIncorrects;
                $this->collCommonTmpSiretIncorrectsPartial = false;
            }
        }

        return $this->collCommonTmpSiretIncorrects;
    }

    /**
     * Sets a collection of CommonTmpSiretIncorrect objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTmpSiretIncorrects A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonTmpSiretIncorrects(PropelCollection $commonTmpSiretIncorrects, PropelPDO $con = null)
    {
        $commonTmpSiretIncorrectsToDelete = $this->getCommonTmpSiretIncorrects(new Criteria(), $con)->diff($commonTmpSiretIncorrects);


        $this->commonTmpSiretIncorrectsScheduledForDeletion = $commonTmpSiretIncorrectsToDelete;

        foreach ($commonTmpSiretIncorrectsToDelete as $commonTmpSiretIncorrectRemoved) {
            $commonTmpSiretIncorrectRemoved->setCommonInscrit(null);
        }

        $this->collCommonTmpSiretIncorrects = null;
        foreach ($commonTmpSiretIncorrects as $commonTmpSiretIncorrect) {
            $this->addCommonTmpSiretIncorrect($commonTmpSiretIncorrect);
        }

        $this->collCommonTmpSiretIncorrects = $commonTmpSiretIncorrects;
        $this->collCommonTmpSiretIncorrectsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTmpSiretIncorrect objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTmpSiretIncorrect objects.
     * @throws PropelException
     */
    public function countCommonTmpSiretIncorrects(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTmpSiretIncorrectsPartial && !$this->isNew();
        if (null === $this->collCommonTmpSiretIncorrects || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTmpSiretIncorrects) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTmpSiretIncorrects());
            }
            $query = CommonTmpSiretIncorrectQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonTmpSiretIncorrects);
    }

    /**
     * Method called to associate a CommonTmpSiretIncorrect object to this object
     * through the CommonTmpSiretIncorrect foreign key attribute.
     *
     * @param   CommonTmpSiretIncorrect $l CommonTmpSiretIncorrect
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonTmpSiretIncorrect(CommonTmpSiretIncorrect $l)
    {
        if ($this->collCommonTmpSiretIncorrects === null) {
            $this->initCommonTmpSiretIncorrects();
            $this->collCommonTmpSiretIncorrectsPartial = true;
        }
        if (!in_array($l, $this->collCommonTmpSiretIncorrects->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTmpSiretIncorrect($l);
        }

        return $this;
    }

    /**
     * @param	CommonTmpSiretIncorrect $commonTmpSiretIncorrect The commonTmpSiretIncorrect object to add.
     */
    protected function doAddCommonTmpSiretIncorrect($commonTmpSiretIncorrect)
    {
        $this->collCommonTmpSiretIncorrects[]= $commonTmpSiretIncorrect;
        $commonTmpSiretIncorrect->setCommonInscrit($this);
    }

    /**
     * @param	CommonTmpSiretIncorrect $commonTmpSiretIncorrect The commonTmpSiretIncorrect object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonTmpSiretIncorrect($commonTmpSiretIncorrect)
    {
        if ($this->getCommonTmpSiretIncorrects()->contains($commonTmpSiretIncorrect)) {
            $this->collCommonTmpSiretIncorrects->remove($this->collCommonTmpSiretIncorrects->search($commonTmpSiretIncorrect));
            if (null === $this->commonTmpSiretIncorrectsScheduledForDeletion) {
                $this->commonTmpSiretIncorrectsScheduledForDeletion = clone $this->collCommonTmpSiretIncorrects;
                $this->commonTmpSiretIncorrectsScheduledForDeletion->clear();
            }
            $this->commonTmpSiretIncorrectsScheduledForDeletion[]= $commonTmpSiretIncorrect;
            $commonTmpSiretIncorrect->setCommonInscrit(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTraceOperationsInscrits collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonInscrit The current object (for fluent API support)
     * @see        addCommonTraceOperationsInscrits()
     */
    public function clearCommonTraceOperationsInscrits()
    {
        $this->collCommonTraceOperationsInscrits = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTraceOperationsInscritsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTraceOperationsInscrits collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTraceOperationsInscrits($v = true)
    {
        $this->collCommonTraceOperationsInscritsPartial = $v;
    }

    /**
     * Initializes the collCommonTraceOperationsInscrits collection.
     *
     * By default this just sets the collCommonTraceOperationsInscrits collection to an empty array (like clearcollCommonTraceOperationsInscrits());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTraceOperationsInscrits($overrideExisting = true)
    {
        if (null !== $this->collCommonTraceOperationsInscrits && !$overrideExisting) {
            return;
        }
        $this->collCommonTraceOperationsInscrits = new PropelObjectCollection();
        $this->collCommonTraceOperationsInscrits->setModel('CommonTraceOperationsInscrit');
    }

    /**
     * Gets an array of CommonTraceOperationsInscrit objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonInscrit is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTraceOperationsInscrit[] List of CommonTraceOperationsInscrit objects
     * @throws PropelException
     */
    public function getCommonTraceOperationsInscrits($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTraceOperationsInscritsPartial && !$this->isNew();
        if (null === $this->collCommonTraceOperationsInscrits || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTraceOperationsInscrits) {
                // return empty collection
                $this->initCommonTraceOperationsInscrits();
            } else {
                $collCommonTraceOperationsInscrits = CommonTraceOperationsInscritQuery::create(null, $criteria)
                    ->filterByCommonInscrit($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTraceOperationsInscritsPartial && count($collCommonTraceOperationsInscrits)) {
                      $this->initCommonTraceOperationsInscrits(false);

                      foreach ($collCommonTraceOperationsInscrits as $obj) {
                        if (false == $this->collCommonTraceOperationsInscrits->contains($obj)) {
                          $this->collCommonTraceOperationsInscrits->append($obj);
                        }
                      }

                      $this->collCommonTraceOperationsInscritsPartial = true;
                    }

                    $collCommonTraceOperationsInscrits->getInternalIterator()->rewind();

                    return $collCommonTraceOperationsInscrits;
                }

                if ($partial && $this->collCommonTraceOperationsInscrits) {
                    foreach ($this->collCommonTraceOperationsInscrits as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTraceOperationsInscrits[] = $obj;
                        }
                    }
                }

                $this->collCommonTraceOperationsInscrits = $collCommonTraceOperationsInscrits;
                $this->collCommonTraceOperationsInscritsPartial = false;
            }
        }

        return $this->collCommonTraceOperationsInscrits;
    }

    /**
     * Sets a collection of CommonTraceOperationsInscrit objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTraceOperationsInscrits A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function setCommonTraceOperationsInscrits(PropelCollection $commonTraceOperationsInscrits, PropelPDO $con = null)
    {
        $commonTraceOperationsInscritsToDelete = $this->getCommonTraceOperationsInscrits(new Criteria(), $con)->diff($commonTraceOperationsInscrits);


        $this->commonTraceOperationsInscritsScheduledForDeletion = $commonTraceOperationsInscritsToDelete;

        foreach ($commonTraceOperationsInscritsToDelete as $commonTraceOperationsInscritRemoved) {
            $commonTraceOperationsInscritRemoved->setCommonInscrit(null);
        }

        $this->collCommonTraceOperationsInscrits = null;
        foreach ($commonTraceOperationsInscrits as $commonTraceOperationsInscrit) {
            $this->addCommonTraceOperationsInscrit($commonTraceOperationsInscrit);
        }

        $this->collCommonTraceOperationsInscrits = $commonTraceOperationsInscrits;
        $this->collCommonTraceOperationsInscritsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTraceOperationsInscrit objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTraceOperationsInscrit objects.
     * @throws PropelException
     */
    public function countCommonTraceOperationsInscrits(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTraceOperationsInscritsPartial && !$this->isNew();
        if (null === $this->collCommonTraceOperationsInscrits || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTraceOperationsInscrits) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTraceOperationsInscrits());
            }
            $query = CommonTraceOperationsInscritQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonInscrit($this)
                ->count($con);
        }

        return count($this->collCommonTraceOperationsInscrits);
    }

    /**
     * Method called to associate a CommonTraceOperationsInscrit object to this object
     * through the CommonTraceOperationsInscrit foreign key attribute.
     *
     * @param   CommonTraceOperationsInscrit $l CommonTraceOperationsInscrit
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function addCommonTraceOperationsInscrit(CommonTraceOperationsInscrit $l)
    {
        if ($this->collCommonTraceOperationsInscrits === null) {
            $this->initCommonTraceOperationsInscrits();
            $this->collCommonTraceOperationsInscritsPartial = true;
        }
        if (!in_array($l, $this->collCommonTraceOperationsInscrits->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTraceOperationsInscrit($l);
        }

        return $this;
    }

    /**
     * @param	CommonTraceOperationsInscrit $commonTraceOperationsInscrit The commonTraceOperationsInscrit object to add.
     */
    protected function doAddCommonTraceOperationsInscrit($commonTraceOperationsInscrit)
    {
        $this->collCommonTraceOperationsInscrits[]= $commonTraceOperationsInscrit;
        $commonTraceOperationsInscrit->setCommonInscrit($this);
    }

    /**
     * @param	CommonTraceOperationsInscrit $commonTraceOperationsInscrit The commonTraceOperationsInscrit object to remove.
     * @return CommonInscrit The current object (for fluent API support)
     */
    public function removeCommonTraceOperationsInscrit($commonTraceOperationsInscrit)
    {
        if ($this->getCommonTraceOperationsInscrits()->contains($commonTraceOperationsInscrit)) {
            $this->collCommonTraceOperationsInscrits->remove($this->collCommonTraceOperationsInscrits->search($commonTraceOperationsInscrit));
            if (null === $this->commonTraceOperationsInscritsScheduledForDeletion) {
                $this->commonTraceOperationsInscritsScheduledForDeletion = clone $this->collCommonTraceOperationsInscrits;
                $this->commonTraceOperationsInscritsScheduledForDeletion->clear();
            }
            $this->commonTraceOperationsInscritsScheduledForDeletion[]= $commonTraceOperationsInscrit;
            $commonTraceOperationsInscrit->setCommonInscrit(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->old_id = null;
        $this->entreprise_id = null;
        $this->id_etablissement = null;
        $this->login = null;
        $this->mdp = null;
        $this->num_cert = null;
        $this->cert = null;
        $this->civilite = null;
        $this->nom = null;
        $this->prenom = null;
        $this->adresse = null;
        $this->codepostal = null;
        $this->ville = null;
        $this->pays = null;
        $this->email = null;
        $this->telephone = null;
        $this->categorie = null;
        $this->motstitreresume = null;
        $this->periode = null;
        $this->siret = null;
        $this->fax = null;
        $this->code_cpv = null;
        $this->id_langue = null;
        $this->profil = null;
        $this->adresse2 = null;
        $this->bloque = null;
        $this->id_initial = null;
        $this->inscrit_annuaire_defense = null;
        $this->date_creation = null;
        $this->date_modification = null;
        $this->tentatives_mdp = null;
        $this->uid = null;
        $this->type_hash = null;
        $this->id_externe = null;
        $this->id = null;
        $this->date_validation_rgpd = null;
        $this->rgpd_communication_place = null;
        $this->rgpd_enquete = null;
        $this->rgpd_communication = null;
        $this->deleted_at = null;
        $this->deleted = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonAlertes) {
                foreach ($this->collCommonAlertes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonCertificatsEntreprisess) {
                foreach ($this->collCommonCertificatsEntreprisess as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonEchangeDestinataires) {
                foreach ($this->collCommonEchangeDestinataires as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonOffress) {
                foreach ($this->collCommonOffress as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonPanierEntreprises) {
                foreach ($this->collCommonPanierEntreprises as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonQuestionDCEs) {
                foreach ($this->collCommonQuestionDCEs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonReponseInscritFormulaireConsultations) {
                foreach ($this->collCommonReponseInscritFormulaireConsultations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTelechargements) {
                foreach ($this->collCommonTelechargements as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonDossierVolumineuxs) {
                foreach ($this->collCommonDossierVolumineuxs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonHistorisationMotDePasses) {
                foreach ($this->collCommonHistorisationMotDePasses as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonQuestionsDces) {
                foreach ($this->collCommonQuestionsDces as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonSsoEntreprises) {
                foreach ($this->collCommonSsoEntreprises as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCandidatures) {
                foreach ($this->collCommonTCandidatures as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCandidatureMpss) {
                foreach ($this->collCommonTCandidatureMpss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTContactContrats) {
                foreach ($this->collCommonTContactContrats as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTListeLotsCandidatures) {
                foreach ($this->collCommonTListeLotsCandidatures as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTReponseElecFormulaires) {
                foreach ($this->collCommonTReponseElecFormulaires as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTmpSiretIncorrects) {
                foreach ($this->collCommonTmpSiretIncorrects as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTraceOperationsInscrits) {
                foreach ($this->collCommonTraceOperationsInscrits as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonTEtablissementRelatedByIdEtablissement instanceof Persistent) {
              $this->aCommonTEtablissementRelatedByIdEtablissement->clearAllReferences($deep);
            }
            if ($this->aEntreprise instanceof Persistent) {
              $this->aEntreprise->clearAllReferences($deep);
            }
            if ($this->aCommonTEtablissementRelatedByIdEtablissement instanceof Persistent) {
              $this->aCommonTEtablissementRelatedByIdEtablissement->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonAlertes instanceof PropelCollection) {
            $this->collCommonAlertes->clearIterator();
        }
        $this->collCommonAlertes = null;
        if ($this->collCommonCertificatsEntreprisess instanceof PropelCollection) {
            $this->collCommonCertificatsEntreprisess->clearIterator();
        }
        $this->collCommonCertificatsEntreprisess = null;
        if ($this->collCommonEchangeDestinataires instanceof PropelCollection) {
            $this->collCommonEchangeDestinataires->clearIterator();
        }
        $this->collCommonEchangeDestinataires = null;
        if ($this->collCommonOffress instanceof PropelCollection) {
            $this->collCommonOffress->clearIterator();
        }
        $this->collCommonOffress = null;
        if ($this->collCommonPanierEntreprises instanceof PropelCollection) {
            $this->collCommonPanierEntreprises->clearIterator();
        }
        $this->collCommonPanierEntreprises = null;
        if ($this->collCommonQuestionDCEs instanceof PropelCollection) {
            $this->collCommonQuestionDCEs->clearIterator();
        }
        $this->collCommonQuestionDCEs = null;
        if ($this->collCommonReponseInscritFormulaireConsultations instanceof PropelCollection) {
            $this->collCommonReponseInscritFormulaireConsultations->clearIterator();
        }
        $this->collCommonReponseInscritFormulaireConsultations = null;
        if ($this->collCommonTelechargements instanceof PropelCollection) {
            $this->collCommonTelechargements->clearIterator();
        }
        $this->collCommonTelechargements = null;
        if ($this->collCommonDossierVolumineuxs instanceof PropelCollection) {
            $this->collCommonDossierVolumineuxs->clearIterator();
        }
        $this->collCommonDossierVolumineuxs = null;
        if ($this->collCommonHistorisationMotDePasses instanceof PropelCollection) {
            $this->collCommonHistorisationMotDePasses->clearIterator();
        }
        $this->collCommonHistorisationMotDePasses = null;
        if ($this->collCommonQuestionsDces instanceof PropelCollection) {
            $this->collCommonQuestionsDces->clearIterator();
        }
        $this->collCommonQuestionsDces = null;
        if ($this->collCommonSsoEntreprises instanceof PropelCollection) {
            $this->collCommonSsoEntreprises->clearIterator();
        }
        $this->collCommonSsoEntreprises = null;
        if ($this->collCommonTCandidatures instanceof PropelCollection) {
            $this->collCommonTCandidatures->clearIterator();
        }
        $this->collCommonTCandidatures = null;
        if ($this->collCommonTCandidatureMpss instanceof PropelCollection) {
            $this->collCommonTCandidatureMpss->clearIterator();
        }
        $this->collCommonTCandidatureMpss = null;
        if ($this->collCommonTContactContrats instanceof PropelCollection) {
            $this->collCommonTContactContrats->clearIterator();
        }
        $this->collCommonTContactContrats = null;
        if ($this->collCommonTListeLotsCandidatures instanceof PropelCollection) {
            $this->collCommonTListeLotsCandidatures->clearIterator();
        }
        $this->collCommonTListeLotsCandidatures = null;
        if ($this->collCommonTReponseElecFormulaires instanceof PropelCollection) {
            $this->collCommonTReponseElecFormulaires->clearIterator();
        }
        $this->collCommonTReponseElecFormulaires = null;
        if ($this->collCommonTmpSiretIncorrects instanceof PropelCollection) {
            $this->collCommonTmpSiretIncorrects->clearIterator();
        }
        $this->collCommonTmpSiretIncorrects = null;
        if ($this->collCommonTraceOperationsInscrits instanceof PropelCollection) {
            $this->collCommonTraceOperationsInscrits->clearIterator();
        }
        $this->collCommonTraceOperationsInscrits = null;
        $this->aCommonTEtablissementRelatedByIdEtablissement = null;
        $this->aEntreprise = null;
        $this->aCommonTEtablissementRelatedByIdEtablissement = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonInscritPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
