<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonConfigurationOrganismePeer;
use Application\Propel\Mpe\CommonConfigurationOrganismeQuery;
use Application\Propel\Mpe\CommonOrganisme;

/**
 * Base class that represents a query for the 'configuration_organisme' table.
 *
 *
 *
 * @method CommonConfigurationOrganismeQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonConfigurationOrganismeQuery orderByEncheres($order = Criteria::ASC) Order by the encheres column
 * @method CommonConfigurationOrganismeQuery orderByConsultationPjAutresPiecesTelechargeables($order = Criteria::ASC) Order by the consultation_pj_autres_pieces_telechargeables column
 * @method CommonConfigurationOrganismeQuery orderByNoActivex($order = Criteria::ASC) Order by the no_activex column
 * @method CommonConfigurationOrganismeQuery orderByGestionMapa($order = Criteria::ASC) Order by the gestion_mapa column
 * @method CommonConfigurationOrganismeQuery orderByArticle133UploadFichier($order = Criteria::ASC) Order by the article_133_upload_fichier column
 * @method CommonConfigurationOrganismeQuery orderByCentralePublication($order = Criteria::ASC) Order by the centrale_publication column
 * @method CommonConfigurationOrganismeQuery orderByOrganisationCentralisee($order = Criteria::ASC) Order by the organisation_centralisee column
 * @method CommonConfigurationOrganismeQuery orderByPresenceElu($order = Criteria::ASC) Order by the presence_elu column
 * @method CommonConfigurationOrganismeQuery orderByTraduireConsultation($order = Criteria::ASC) Order by the traduire_consultation column
 * @method CommonConfigurationOrganismeQuery orderBySuiviPassation($order = Criteria::ASC) Order by the suivi_passation column
 * @method CommonConfigurationOrganismeQuery orderByNumerotationRefCons($order = Criteria::ASC) Order by the numerotation_ref_cons column
 * @method CommonConfigurationOrganismeQuery orderByPmiLienPortailDefenseAgent($order = Criteria::ASC) Order by the pmi_lien_portail_defense_agent column
 * @method CommonConfigurationOrganismeQuery orderByInterfaceArchiveArcadePmi($order = Criteria::ASC) Order by the interface_archive_arcade_pmi column
 * @method CommonConfigurationOrganismeQuery orderByDesarchivageConsultation($order = Criteria::ASC) Order by the desarchivage_consultation column
 * @method CommonConfigurationOrganismeQuery orderByAlimentationAutomatiqueListeInvites($order = Criteria::ASC) Order by the alimentation_automatique_liste_invites column
 * @method CommonConfigurationOrganismeQuery orderByInterfaceChorusPmi($order = Criteria::ASC) Order by the interface_chorus_pmi column
 * @method CommonConfigurationOrganismeQuery orderByArchivageConsultationSurPf($order = Criteria::ASC) Order by the archivage_consultation_sur_pf column
 * @method CommonConfigurationOrganismeQuery orderByAutoriserModificationApresPhaseConsultation($order = Criteria::ASC) Order by the autoriser_modification_apres_phase_consultation column
 * @method CommonConfigurationOrganismeQuery orderByImporterEnveloppe($order = Criteria::ASC) Order by the importer_enveloppe column
 * @method CommonConfigurationOrganismeQuery orderByExportMarchesNotifies($order = Criteria::ASC) Order by the export_marches_notifies column
 * @method CommonConfigurationOrganismeQuery orderByAccesAgentsCfeBdFournisseur($order = Criteria::ASC) Order by the acces_agents_cfe_bd_fournisseur column
 * @method CommonConfigurationOrganismeQuery orderByAccesAgentsCfeOuvertureAnalyse($order = Criteria::ASC) Order by the acces_agents_cfe_ouverture_analyse column
 * @method CommonConfigurationOrganismeQuery orderByUtiliserParametrageEncheres($order = Criteria::ASC) Order by the utiliser_parametrage_encheres column
 * @method CommonConfigurationOrganismeQuery orderByVerifierCompteBoamp($order = Criteria::ASC) Order by the verifier_compte_boamp column
 * @method CommonConfigurationOrganismeQuery orderByGestionMandataire($order = Criteria::ASC) Order by the gestion_mandataire column
 * @method CommonConfigurationOrganismeQuery orderByFourEyes($order = Criteria::ASC) Order by the four_eyes column
 * @method CommonConfigurationOrganismeQuery orderByInterfaceModuleRsem($order = Criteria::ASC) Order by the interface_module_rsem column
 * @method CommonConfigurationOrganismeQuery orderByArchivageConsultationSaeExterneEnvoiArchive($order = Criteria::ASC) Order by the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE column
 * @method CommonConfigurationOrganismeQuery orderByArchivageConsultationSaeExterneTelechargementArchive($order = Criteria::ASC) Order by the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE column
 * @method CommonConfigurationOrganismeQuery orderByAgentVerificationCertificatPeppol($order = Criteria::ASC) Order by the agent_verification_certificat_peppol column
 * @method CommonConfigurationOrganismeQuery orderByFuseauHoraire($order = Criteria::ASC) Order by the fuseau_horaire column
 * @method CommonConfigurationOrganismeQuery orderByFicheWeka($order = Criteria::ASC) Order by the fiche_weka column
 * @method CommonConfigurationOrganismeQuery orderByMiseDispositionPiecesMarche($order = Criteria::ASC) Order by the mise_disposition_pieces_marche column
 * @method CommonConfigurationOrganismeQuery orderByBaseDce($order = Criteria::ASC) Order by the base_dce column
 * @method CommonConfigurationOrganismeQuery orderByAvisMembresCommision($order = Criteria::ASC) Order by the avis_membres_commision column
 * @method CommonConfigurationOrganismeQuery orderByDonneesRedac($order = Criteria::ASC) Order by the Donnees_Redac column
 * @method CommonConfigurationOrganismeQuery orderByPersonnaliserAffichageThemeEtIllustration($order = Criteria::ASC) Order by the Personnaliser_Affichage_Theme_Et_Illustration column
 * @method CommonConfigurationOrganismeQuery orderByTypeContrat($order = Criteria::ASC) Order by the type_contrat column
 * @method CommonConfigurationOrganismeQuery orderByEntiteAdjudicatrice($order = Criteria::ASC) Order by the entite_adjudicatrice column
 * @method CommonConfigurationOrganismeQuery orderByCalendrierDeLaConsultation($order = Criteria::ASC) Order by the calendrier_de_la_consultation column
 * @method CommonConfigurationOrganismeQuery orderByDonneesComplementaires($order = Criteria::ASC) Order by the donnees_complementaires column
 * @method CommonConfigurationOrganismeQuery orderByEspaceCollaboratif($order = Criteria::ASC) Order by the espace_collaboratif column
 * @method CommonConfigurationOrganismeQuery orderByHistoriqueNavigationInscrits($order = Criteria::ASC) Order by the historique_navigation_inscrits column
 * @method CommonConfigurationOrganismeQuery orderByIdentificationContrat($order = Criteria::ASC) Order by the Identification_contrat column
 * @method CommonConfigurationOrganismeQuery orderByExtractionAccordsCadres($order = Criteria::ASC) Order by the extraction_accords_cadres column
 * @method CommonConfigurationOrganismeQuery orderByGestionOperations($order = Criteria::ASC) Order by the gestion_operations column
 * @method CommonConfigurationOrganismeQuery orderByExtractionSiretAcheteur($order = Criteria::ASC) Order by the extraction_siret_acheteur column
 * @method CommonConfigurationOrganismeQuery orderByMarchePublicSimplifie($order = Criteria::ASC) Order by the marche_public_simplifie column
 * @method CommonConfigurationOrganismeQuery orderByRecherchesFavoritesAgent($order = Criteria::ASC) Order by the recherches_favorites_agent column
 * @method CommonConfigurationOrganismeQuery orderByProfilRma($order = Criteria::ASC) Order by the profil_rma column
 * @method CommonConfigurationOrganismeQuery orderByFiltreContratAcSad($order = Criteria::ASC) Order by the filtre_contrat_ac_sad column
 * @method CommonConfigurationOrganismeQuery orderByAffichageNomServicePere($order = Criteria::ASC) Order by the affichage_nom_service_pere column
 * @method CommonConfigurationOrganismeQuery orderByModeApplet($order = Criteria::ASC) Order by the mode_applet column
 * @method CommonConfigurationOrganismeQuery orderByMarcheDefense($order = Criteria::ASC) Order by the marche_defense column
 * @method CommonConfigurationOrganismeQuery orderByNumDonneesEssentiellesManuel($order = Criteria::ASC) Order by the num_donnees_essentielles_manuel column
 * @method CommonConfigurationOrganismeQuery orderByEspaceDocumentaire($order = Criteria::ASC) Order by the espace_documentaire column
 * @method CommonConfigurationOrganismeQuery orderByNumeroProjetAchat($order = Criteria::ASC) Order by the numero_projet_achat column
 * @method CommonConfigurationOrganismeQuery orderByModuleExec($order = Criteria::ASC) Order by the module_exec column
 * @method CommonConfigurationOrganismeQuery orderByEchangesDocuments($order = Criteria::ASC) Order by the echanges_documents column
 * @method CommonConfigurationOrganismeQuery orderByHeureLimiteDeRemiseDePlisParDefaut($order = Criteria::ASC) Order by the heure_limite_de_remise_de_plis_par_defaut column
 * @method CommonConfigurationOrganismeQuery orderBySaisieManuelleIdExterne($order = Criteria::ASC) Order by the saisie_manuelle_id_externe column
 * @method CommonConfigurationOrganismeQuery orderByModuleSourcing($order = Criteria::ASC) Order by the module_sourcing column
 * @method CommonConfigurationOrganismeQuery orderByModuleRecensementProgrammation($order = Criteria::ASC) Order by the module_recensement_programmation column
 * @method CommonConfigurationOrganismeQuery orderByModuleEnvol($order = Criteria::ASC) Order by the module_envol column
 * @method CommonConfigurationOrganismeQuery orderByModuleBiPremium($order = Criteria::ASC) Order by the module_bi_premium column
 * @method CommonConfigurationOrganismeQuery orderByAnalyseOffres($order = Criteria::ASC) Order by the analyse_offres column
 * @method CommonConfigurationOrganismeQuery orderByCao($order = Criteria::ASC) Order by the cao column
 * @method CommonConfigurationOrganismeQuery orderByModuleBi($order = Criteria::ASC) Order by the module_BI column
 * @method CommonConfigurationOrganismeQuery orderByCmsActif($order = Criteria::ASC) Order by the cms_actif column
 * @method CommonConfigurationOrganismeQuery orderByDceRestreint($order = Criteria::ASC) Order by the dce_restreint column
 * @method CommonConfigurationOrganismeQuery orderByActiverMonAssistantMarchesPublics($order = Criteria::ASC) Order by the activer_mon_assistant_marches_publics column
 * @method CommonConfigurationOrganismeQuery orderByGestionContratDansExec($order = Criteria::ASC) Order by the gestion_contrat_dans_exec column
 * @method CommonConfigurationOrganismeQuery orderByConsultationSimplifiee($order = Criteria::ASC) Order by the consultation_simplifiee column
 * @method CommonConfigurationOrganismeQuery orderByTypageJo2024($order = Criteria::ASC) Order by the typage_jo2024 column
 * @method CommonConfigurationOrganismeQuery orderByModuleTncp($order = Criteria::ASC) Order by the module_tncp column
 * @method CommonConfigurationOrganismeQuery orderByAccesModuleSpaser($order = Criteria::ASC) Order by the acces_module_spaser column
 * @method CommonConfigurationOrganismeQuery orderByPubTncp($order = Criteria::ASC) Order by the pub_tncp column
 * @method CommonConfigurationOrganismeQuery orderByPubMol($order = Criteria::ASC) Order by the pub_mol column
 * @method CommonConfigurationOrganismeQuery orderByPubJalFr($order = Criteria::ASC) Order by the pub_jal_fr column
 * @method CommonConfigurationOrganismeQuery orderByPubJalLux($order = Criteria::ASC) Order by the pub_jal_lux column
 * @method CommonConfigurationOrganismeQuery orderByPubJoue($order = Criteria::ASC) Order by the pub_joue column
 * @method CommonConfigurationOrganismeQuery orderByModuleEcoSip($order = Criteria::ASC) Order by the module_eco_sip column
 * @method CommonConfigurationOrganismeQuery orderByModuleMpePub($order = Criteria::ASC) Order by the module_mpe_pub column
 * @method CommonConfigurationOrganismeQuery orderByModuleAdministrationDocument($order = Criteria::ASC) Order by the module_administration_document column
 *
 * @method CommonConfigurationOrganismeQuery groupByOrganisme() Group by the organisme column
 * @method CommonConfigurationOrganismeQuery groupByEncheres() Group by the encheres column
 * @method CommonConfigurationOrganismeQuery groupByConsultationPjAutresPiecesTelechargeables() Group by the consultation_pj_autres_pieces_telechargeables column
 * @method CommonConfigurationOrganismeQuery groupByNoActivex() Group by the no_activex column
 * @method CommonConfigurationOrganismeQuery groupByGestionMapa() Group by the gestion_mapa column
 * @method CommonConfigurationOrganismeQuery groupByArticle133UploadFichier() Group by the article_133_upload_fichier column
 * @method CommonConfigurationOrganismeQuery groupByCentralePublication() Group by the centrale_publication column
 * @method CommonConfigurationOrganismeQuery groupByOrganisationCentralisee() Group by the organisation_centralisee column
 * @method CommonConfigurationOrganismeQuery groupByPresenceElu() Group by the presence_elu column
 * @method CommonConfigurationOrganismeQuery groupByTraduireConsultation() Group by the traduire_consultation column
 * @method CommonConfigurationOrganismeQuery groupBySuiviPassation() Group by the suivi_passation column
 * @method CommonConfigurationOrganismeQuery groupByNumerotationRefCons() Group by the numerotation_ref_cons column
 * @method CommonConfigurationOrganismeQuery groupByPmiLienPortailDefenseAgent() Group by the pmi_lien_portail_defense_agent column
 * @method CommonConfigurationOrganismeQuery groupByInterfaceArchiveArcadePmi() Group by the interface_archive_arcade_pmi column
 * @method CommonConfigurationOrganismeQuery groupByDesarchivageConsultation() Group by the desarchivage_consultation column
 * @method CommonConfigurationOrganismeQuery groupByAlimentationAutomatiqueListeInvites() Group by the alimentation_automatique_liste_invites column
 * @method CommonConfigurationOrganismeQuery groupByInterfaceChorusPmi() Group by the interface_chorus_pmi column
 * @method CommonConfigurationOrganismeQuery groupByArchivageConsultationSurPf() Group by the archivage_consultation_sur_pf column
 * @method CommonConfigurationOrganismeQuery groupByAutoriserModificationApresPhaseConsultation() Group by the autoriser_modification_apres_phase_consultation column
 * @method CommonConfigurationOrganismeQuery groupByImporterEnveloppe() Group by the importer_enveloppe column
 * @method CommonConfigurationOrganismeQuery groupByExportMarchesNotifies() Group by the export_marches_notifies column
 * @method CommonConfigurationOrganismeQuery groupByAccesAgentsCfeBdFournisseur() Group by the acces_agents_cfe_bd_fournisseur column
 * @method CommonConfigurationOrganismeQuery groupByAccesAgentsCfeOuvertureAnalyse() Group by the acces_agents_cfe_ouverture_analyse column
 * @method CommonConfigurationOrganismeQuery groupByUtiliserParametrageEncheres() Group by the utiliser_parametrage_encheres column
 * @method CommonConfigurationOrganismeQuery groupByVerifierCompteBoamp() Group by the verifier_compte_boamp column
 * @method CommonConfigurationOrganismeQuery groupByGestionMandataire() Group by the gestion_mandataire column
 * @method CommonConfigurationOrganismeQuery groupByFourEyes() Group by the four_eyes column
 * @method CommonConfigurationOrganismeQuery groupByInterfaceModuleRsem() Group by the interface_module_rsem column
 * @method CommonConfigurationOrganismeQuery groupByArchivageConsultationSaeExterneEnvoiArchive() Group by the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE column
 * @method CommonConfigurationOrganismeQuery groupByArchivageConsultationSaeExterneTelechargementArchive() Group by the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE column
 * @method CommonConfigurationOrganismeQuery groupByAgentVerificationCertificatPeppol() Group by the agent_verification_certificat_peppol column
 * @method CommonConfigurationOrganismeQuery groupByFuseauHoraire() Group by the fuseau_horaire column
 * @method CommonConfigurationOrganismeQuery groupByFicheWeka() Group by the fiche_weka column
 * @method CommonConfigurationOrganismeQuery groupByMiseDispositionPiecesMarche() Group by the mise_disposition_pieces_marche column
 * @method CommonConfigurationOrganismeQuery groupByBaseDce() Group by the base_dce column
 * @method CommonConfigurationOrganismeQuery groupByAvisMembresCommision() Group by the avis_membres_commision column
 * @method CommonConfigurationOrganismeQuery groupByDonneesRedac() Group by the Donnees_Redac column
 * @method CommonConfigurationOrganismeQuery groupByPersonnaliserAffichageThemeEtIllustration() Group by the Personnaliser_Affichage_Theme_Et_Illustration column
 * @method CommonConfigurationOrganismeQuery groupByTypeContrat() Group by the type_contrat column
 * @method CommonConfigurationOrganismeQuery groupByEntiteAdjudicatrice() Group by the entite_adjudicatrice column
 * @method CommonConfigurationOrganismeQuery groupByCalendrierDeLaConsultation() Group by the calendrier_de_la_consultation column
 * @method CommonConfigurationOrganismeQuery groupByDonneesComplementaires() Group by the donnees_complementaires column
 * @method CommonConfigurationOrganismeQuery groupByEspaceCollaboratif() Group by the espace_collaboratif column
 * @method CommonConfigurationOrganismeQuery groupByHistoriqueNavigationInscrits() Group by the historique_navigation_inscrits column
 * @method CommonConfigurationOrganismeQuery groupByIdentificationContrat() Group by the Identification_contrat column
 * @method CommonConfigurationOrganismeQuery groupByExtractionAccordsCadres() Group by the extraction_accords_cadres column
 * @method CommonConfigurationOrganismeQuery groupByGestionOperations() Group by the gestion_operations column
 * @method CommonConfigurationOrganismeQuery groupByExtractionSiretAcheteur() Group by the extraction_siret_acheteur column
 * @method CommonConfigurationOrganismeQuery groupByMarchePublicSimplifie() Group by the marche_public_simplifie column
 * @method CommonConfigurationOrganismeQuery groupByRecherchesFavoritesAgent() Group by the recherches_favorites_agent column
 * @method CommonConfigurationOrganismeQuery groupByProfilRma() Group by the profil_rma column
 * @method CommonConfigurationOrganismeQuery groupByFiltreContratAcSad() Group by the filtre_contrat_ac_sad column
 * @method CommonConfigurationOrganismeQuery groupByAffichageNomServicePere() Group by the affichage_nom_service_pere column
 * @method CommonConfigurationOrganismeQuery groupByModeApplet() Group by the mode_applet column
 * @method CommonConfigurationOrganismeQuery groupByMarcheDefense() Group by the marche_defense column
 * @method CommonConfigurationOrganismeQuery groupByNumDonneesEssentiellesManuel() Group by the num_donnees_essentielles_manuel column
 * @method CommonConfigurationOrganismeQuery groupByEspaceDocumentaire() Group by the espace_documentaire column
 * @method CommonConfigurationOrganismeQuery groupByNumeroProjetAchat() Group by the numero_projet_achat column
 * @method CommonConfigurationOrganismeQuery groupByModuleExec() Group by the module_exec column
 * @method CommonConfigurationOrganismeQuery groupByEchangesDocuments() Group by the echanges_documents column
 * @method CommonConfigurationOrganismeQuery groupByHeureLimiteDeRemiseDePlisParDefaut() Group by the heure_limite_de_remise_de_plis_par_defaut column
 * @method CommonConfigurationOrganismeQuery groupBySaisieManuelleIdExterne() Group by the saisie_manuelle_id_externe column
 * @method CommonConfigurationOrganismeQuery groupByModuleSourcing() Group by the module_sourcing column
 * @method CommonConfigurationOrganismeQuery groupByModuleRecensementProgrammation() Group by the module_recensement_programmation column
 * @method CommonConfigurationOrganismeQuery groupByModuleEnvol() Group by the module_envol column
 * @method CommonConfigurationOrganismeQuery groupByModuleBiPremium() Group by the module_bi_premium column
 * @method CommonConfigurationOrganismeQuery groupByAnalyseOffres() Group by the analyse_offres column
 * @method CommonConfigurationOrganismeQuery groupByCao() Group by the cao column
 * @method CommonConfigurationOrganismeQuery groupByModuleBi() Group by the module_BI column
 * @method CommonConfigurationOrganismeQuery groupByCmsActif() Group by the cms_actif column
 * @method CommonConfigurationOrganismeQuery groupByDceRestreint() Group by the dce_restreint column
 * @method CommonConfigurationOrganismeQuery groupByActiverMonAssistantMarchesPublics() Group by the activer_mon_assistant_marches_publics column
 * @method CommonConfigurationOrganismeQuery groupByGestionContratDansExec() Group by the gestion_contrat_dans_exec column
 * @method CommonConfigurationOrganismeQuery groupByConsultationSimplifiee() Group by the consultation_simplifiee column
 * @method CommonConfigurationOrganismeQuery groupByTypageJo2024() Group by the typage_jo2024 column
 * @method CommonConfigurationOrganismeQuery groupByModuleTncp() Group by the module_tncp column
 * @method CommonConfigurationOrganismeQuery groupByAccesModuleSpaser() Group by the acces_module_spaser column
 * @method CommonConfigurationOrganismeQuery groupByPubTncp() Group by the pub_tncp column
 * @method CommonConfigurationOrganismeQuery groupByPubMol() Group by the pub_mol column
 * @method CommonConfigurationOrganismeQuery groupByPubJalFr() Group by the pub_jal_fr column
 * @method CommonConfigurationOrganismeQuery groupByPubJalLux() Group by the pub_jal_lux column
 * @method CommonConfigurationOrganismeQuery groupByPubJoue() Group by the pub_joue column
 * @method CommonConfigurationOrganismeQuery groupByModuleEcoSip() Group by the module_eco_sip column
 * @method CommonConfigurationOrganismeQuery groupByModuleMpePub() Group by the module_mpe_pub column
 * @method CommonConfigurationOrganismeQuery groupByModuleAdministrationDocument() Group by the module_administration_document column
 *
 * @method CommonConfigurationOrganismeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonConfigurationOrganismeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonConfigurationOrganismeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonConfigurationOrganismeQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonConfigurationOrganismeQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonConfigurationOrganismeQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonConfigurationOrganisme findOne(PropelPDO $con = null) Return the first CommonConfigurationOrganisme matching the query
 * @method CommonConfigurationOrganisme findOneOrCreate(PropelPDO $con = null) Return the first CommonConfigurationOrganisme matching the query, or a new CommonConfigurationOrganisme object populated from the query conditions when no match is found
 *
 * @method CommonConfigurationOrganisme findOneByEncheres(string $encheres) Return the first CommonConfigurationOrganisme filtered by the encheres column
 * @method CommonConfigurationOrganisme findOneByConsultationPjAutresPiecesTelechargeables(string $consultation_pj_autres_pieces_telechargeables) Return the first CommonConfigurationOrganisme filtered by the consultation_pj_autres_pieces_telechargeables column
 * @method CommonConfigurationOrganisme findOneByNoActivex(string $no_activex) Return the first CommonConfigurationOrganisme filtered by the no_activex column
 * @method CommonConfigurationOrganisme findOneByGestionMapa(string $gestion_mapa) Return the first CommonConfigurationOrganisme filtered by the gestion_mapa column
 * @method CommonConfigurationOrganisme findOneByArticle133UploadFichier(string $article_133_upload_fichier) Return the first CommonConfigurationOrganisme filtered by the article_133_upload_fichier column
 * @method CommonConfigurationOrganisme findOneByCentralePublication(string $centrale_publication) Return the first CommonConfigurationOrganisme filtered by the centrale_publication column
 * @method CommonConfigurationOrganisme findOneByOrganisationCentralisee(string $organisation_centralisee) Return the first CommonConfigurationOrganisme filtered by the organisation_centralisee column
 * @method CommonConfigurationOrganisme findOneByPresenceElu(string $presence_elu) Return the first CommonConfigurationOrganisme filtered by the presence_elu column
 * @method CommonConfigurationOrganisme findOneByTraduireConsultation(string $traduire_consultation) Return the first CommonConfigurationOrganisme filtered by the traduire_consultation column
 * @method CommonConfigurationOrganisme findOneBySuiviPassation(string $suivi_passation) Return the first CommonConfigurationOrganisme filtered by the suivi_passation column
 * @method CommonConfigurationOrganisme findOneByNumerotationRefCons(string $numerotation_ref_cons) Return the first CommonConfigurationOrganisme filtered by the numerotation_ref_cons column
 * @method CommonConfigurationOrganisme findOneByPmiLienPortailDefenseAgent(string $pmi_lien_portail_defense_agent) Return the first CommonConfigurationOrganisme filtered by the pmi_lien_portail_defense_agent column
 * @method CommonConfigurationOrganisme findOneByInterfaceArchiveArcadePmi(string $interface_archive_arcade_pmi) Return the first CommonConfigurationOrganisme filtered by the interface_archive_arcade_pmi column
 * @method CommonConfigurationOrganisme findOneByDesarchivageConsultation(string $desarchivage_consultation) Return the first CommonConfigurationOrganisme filtered by the desarchivage_consultation column
 * @method CommonConfigurationOrganisme findOneByAlimentationAutomatiqueListeInvites(string $alimentation_automatique_liste_invites) Return the first CommonConfigurationOrganisme filtered by the alimentation_automatique_liste_invites column
 * @method CommonConfigurationOrganisme findOneByInterfaceChorusPmi(string $interface_chorus_pmi) Return the first CommonConfigurationOrganisme filtered by the interface_chorus_pmi column
 * @method CommonConfigurationOrganisme findOneByArchivageConsultationSurPf(string $archivage_consultation_sur_pf) Return the first CommonConfigurationOrganisme filtered by the archivage_consultation_sur_pf column
 * @method CommonConfigurationOrganisme findOneByAutoriserModificationApresPhaseConsultation(string $autoriser_modification_apres_phase_consultation) Return the first CommonConfigurationOrganisme filtered by the autoriser_modification_apres_phase_consultation column
 * @method CommonConfigurationOrganisme findOneByImporterEnveloppe(string $importer_enveloppe) Return the first CommonConfigurationOrganisme filtered by the importer_enveloppe column
 * @method CommonConfigurationOrganisme findOneByExportMarchesNotifies(string $export_marches_notifies) Return the first CommonConfigurationOrganisme filtered by the export_marches_notifies column
 * @method CommonConfigurationOrganisme findOneByAccesAgentsCfeBdFournisseur(string $acces_agents_cfe_bd_fournisseur) Return the first CommonConfigurationOrganisme filtered by the acces_agents_cfe_bd_fournisseur column
 * @method CommonConfigurationOrganisme findOneByAccesAgentsCfeOuvertureAnalyse(string $acces_agents_cfe_ouverture_analyse) Return the first CommonConfigurationOrganisme filtered by the acces_agents_cfe_ouverture_analyse column
 * @method CommonConfigurationOrganisme findOneByUtiliserParametrageEncheres(string $utiliser_parametrage_encheres) Return the first CommonConfigurationOrganisme filtered by the utiliser_parametrage_encheres column
 * @method CommonConfigurationOrganisme findOneByVerifierCompteBoamp(string $verifier_compte_boamp) Return the first CommonConfigurationOrganisme filtered by the verifier_compte_boamp column
 * @method CommonConfigurationOrganisme findOneByGestionMandataire(string $gestion_mandataire) Return the first CommonConfigurationOrganisme filtered by the gestion_mandataire column
 * @method CommonConfigurationOrganisme findOneByFourEyes(string $four_eyes) Return the first CommonConfigurationOrganisme filtered by the four_eyes column
 * @method CommonConfigurationOrganisme findOneByInterfaceModuleRsem(string $interface_module_rsem) Return the first CommonConfigurationOrganisme filtered by the interface_module_rsem column
 * @method CommonConfigurationOrganisme findOneByArchivageConsultationSaeExterneEnvoiArchive(string $ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE) Return the first CommonConfigurationOrganisme filtered by the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE column
 * @method CommonConfigurationOrganisme findOneByArchivageConsultationSaeExterneTelechargementArchive(string $ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE) Return the first CommonConfigurationOrganisme filtered by the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE column
 * @method CommonConfigurationOrganisme findOneByAgentVerificationCertificatPeppol(string $agent_verification_certificat_peppol) Return the first CommonConfigurationOrganisme filtered by the agent_verification_certificat_peppol column
 * @method CommonConfigurationOrganisme findOneByFuseauHoraire(string $fuseau_horaire) Return the first CommonConfigurationOrganisme filtered by the fuseau_horaire column
 * @method CommonConfigurationOrganisme findOneByFicheWeka(string $fiche_weka) Return the first CommonConfigurationOrganisme filtered by the fiche_weka column
 * @method CommonConfigurationOrganisme findOneByMiseDispositionPiecesMarche(string $mise_disposition_pieces_marche) Return the first CommonConfigurationOrganisme filtered by the mise_disposition_pieces_marche column
 * @method CommonConfigurationOrganisme findOneByBaseDce(string $base_dce) Return the first CommonConfigurationOrganisme filtered by the base_dce column
 * @method CommonConfigurationOrganisme findOneByAvisMembresCommision(string $avis_membres_commision) Return the first CommonConfigurationOrganisme filtered by the avis_membres_commision column
 * @method CommonConfigurationOrganisme findOneByDonneesRedac(string $Donnees_Redac) Return the first CommonConfigurationOrganisme filtered by the Donnees_Redac column
 * @method CommonConfigurationOrganisme findOneByPersonnaliserAffichageThemeEtIllustration(string $Personnaliser_Affichage_Theme_Et_Illustration) Return the first CommonConfigurationOrganisme filtered by the Personnaliser_Affichage_Theme_Et_Illustration column
 * @method CommonConfigurationOrganisme findOneByTypeContrat(string $type_contrat) Return the first CommonConfigurationOrganisme filtered by the type_contrat column
 * @method CommonConfigurationOrganisme findOneByEntiteAdjudicatrice(string $entite_adjudicatrice) Return the first CommonConfigurationOrganisme filtered by the entite_adjudicatrice column
 * @method CommonConfigurationOrganisme findOneByCalendrierDeLaConsultation(string $calendrier_de_la_consultation) Return the first CommonConfigurationOrganisme filtered by the calendrier_de_la_consultation column
 * @method CommonConfigurationOrganisme findOneByDonneesComplementaires(string $donnees_complementaires) Return the first CommonConfigurationOrganisme filtered by the donnees_complementaires column
 * @method CommonConfigurationOrganisme findOneByEspaceCollaboratif(string $espace_collaboratif) Return the first CommonConfigurationOrganisme filtered by the espace_collaboratif column
 * @method CommonConfigurationOrganisme findOneByHistoriqueNavigationInscrits(string $historique_navigation_inscrits) Return the first CommonConfigurationOrganisme filtered by the historique_navigation_inscrits column
 * @method CommonConfigurationOrganisme findOneByIdentificationContrat(string $Identification_contrat) Return the first CommonConfigurationOrganisme filtered by the Identification_contrat column
 * @method CommonConfigurationOrganisme findOneByExtractionAccordsCadres(string $extraction_accords_cadres) Return the first CommonConfigurationOrganisme filtered by the extraction_accords_cadres column
 * @method CommonConfigurationOrganisme findOneByGestionOperations(string $gestion_operations) Return the first CommonConfigurationOrganisme filtered by the gestion_operations column
 * @method CommonConfigurationOrganisme findOneByExtractionSiretAcheteur(string $extraction_siret_acheteur) Return the first CommonConfigurationOrganisme filtered by the extraction_siret_acheteur column
 * @method CommonConfigurationOrganisme findOneByMarchePublicSimplifie(string $marche_public_simplifie) Return the first CommonConfigurationOrganisme filtered by the marche_public_simplifie column
 * @method CommonConfigurationOrganisme findOneByRecherchesFavoritesAgent(string $recherches_favorites_agent) Return the first CommonConfigurationOrganisme filtered by the recherches_favorites_agent column
 * @method CommonConfigurationOrganisme findOneByProfilRma(string $profil_rma) Return the first CommonConfigurationOrganisme filtered by the profil_rma column
 * @method CommonConfigurationOrganisme findOneByFiltreContratAcSad(string $filtre_contrat_ac_sad) Return the first CommonConfigurationOrganisme filtered by the filtre_contrat_ac_sad column
 * @method CommonConfigurationOrganisme findOneByAffichageNomServicePere(string $affichage_nom_service_pere) Return the first CommonConfigurationOrganisme filtered by the affichage_nom_service_pere column
 * @method CommonConfigurationOrganisme findOneByModeApplet(string $mode_applet) Return the first CommonConfigurationOrganisme filtered by the mode_applet column
 * @method CommonConfigurationOrganisme findOneByMarcheDefense(string $marche_defense) Return the first CommonConfigurationOrganisme filtered by the marche_defense column
 * @method CommonConfigurationOrganisme findOneByNumDonneesEssentiellesManuel(string $num_donnees_essentielles_manuel) Return the first CommonConfigurationOrganisme filtered by the num_donnees_essentielles_manuel column
 * @method CommonConfigurationOrganisme findOneByEspaceDocumentaire(boolean $espace_documentaire) Return the first CommonConfigurationOrganisme filtered by the espace_documentaire column
 * @method CommonConfigurationOrganisme findOneByNumeroProjetAchat(string $numero_projet_achat) Return the first CommonConfigurationOrganisme filtered by the numero_projet_achat column
 * @method CommonConfigurationOrganisme findOneByModuleExec(string $module_exec) Return the first CommonConfigurationOrganisme filtered by the module_exec column
 * @method CommonConfigurationOrganisme findOneByEchangesDocuments(boolean $echanges_documents) Return the first CommonConfigurationOrganisme filtered by the echanges_documents column
 * @method CommonConfigurationOrganisme findOneByHeureLimiteDeRemiseDePlisParDefaut(string $heure_limite_de_remise_de_plis_par_defaut) Return the first CommonConfigurationOrganisme filtered by the heure_limite_de_remise_de_plis_par_defaut column
 * @method CommonConfigurationOrganisme findOneBySaisieManuelleIdExterne(boolean $saisie_manuelle_id_externe) Return the first CommonConfigurationOrganisme filtered by the saisie_manuelle_id_externe column
 * @method CommonConfigurationOrganisme findOneByModuleSourcing(boolean $module_sourcing) Return the first CommonConfigurationOrganisme filtered by the module_sourcing column
 * @method CommonConfigurationOrganisme findOneByModuleRecensementProgrammation(boolean $module_recensement_programmation) Return the first CommonConfigurationOrganisme filtered by the module_recensement_programmation column
 * @method CommonConfigurationOrganisme findOneByModuleEnvol(boolean $module_envol) Return the first CommonConfigurationOrganisme filtered by the module_envol column
 * @method CommonConfigurationOrganisme findOneByModuleBiPremium(boolean $module_bi_premium) Return the first CommonConfigurationOrganisme filtered by the module_bi_premium column
 * @method CommonConfigurationOrganisme findOneByAnalyseOffres(int $analyse_offres) Return the first CommonConfigurationOrganisme filtered by the analyse_offres column
 * @method CommonConfigurationOrganisme findOneByCao(int $cao) Return the first CommonConfigurationOrganisme filtered by the cao column
 * @method CommonConfigurationOrganisme findOneByModuleBi(boolean $module_BI) Return the first CommonConfigurationOrganisme filtered by the module_BI column
 * @method CommonConfigurationOrganisme findOneByCmsActif(boolean $cms_actif) Return the first CommonConfigurationOrganisme filtered by the cms_actif column
 * @method CommonConfigurationOrganisme findOneByDceRestreint(boolean $dce_restreint) Return the first CommonConfigurationOrganisme filtered by the dce_restreint column
 * @method CommonConfigurationOrganisme findOneByActiverMonAssistantMarchesPublics(boolean $activer_mon_assistant_marches_publics) Return the first CommonConfigurationOrganisme filtered by the activer_mon_assistant_marches_publics column
 * @method CommonConfigurationOrganisme findOneByGestionContratDansExec(int $gestion_contrat_dans_exec) Return the first CommonConfigurationOrganisme filtered by the gestion_contrat_dans_exec column
 * @method CommonConfigurationOrganisme findOneByConsultationSimplifiee(boolean $consultation_simplifiee) Return the first CommonConfigurationOrganisme filtered by the consultation_simplifiee column
 * @method CommonConfigurationOrganisme findOneByTypageJo2024(boolean $typage_jo2024) Return the first CommonConfigurationOrganisme filtered by the typage_jo2024 column
 * @method CommonConfigurationOrganisme findOneByModuleTncp(boolean $module_tncp) Return the first CommonConfigurationOrganisme filtered by the module_tncp column
 * @method CommonConfigurationOrganisme findOneByAccesModuleSpaser(boolean $acces_module_spaser) Return the first CommonConfigurationOrganisme filtered by the acces_module_spaser column
 * @method CommonConfigurationOrganisme findOneByPubTncp(boolean $pub_tncp) Return the first CommonConfigurationOrganisme filtered by the pub_tncp column
 * @method CommonConfigurationOrganisme findOneByPubMol(boolean $pub_mol) Return the first CommonConfigurationOrganisme filtered by the pub_mol column
 * @method CommonConfigurationOrganisme findOneByPubJalFr(boolean $pub_jal_fr) Return the first CommonConfigurationOrganisme filtered by the pub_jal_fr column
 * @method CommonConfigurationOrganisme findOneByPubJalLux(boolean $pub_jal_lux) Return the first CommonConfigurationOrganisme filtered by the pub_jal_lux column
 * @method CommonConfigurationOrganisme findOneByPubJoue(boolean $pub_joue) Return the first CommonConfigurationOrganisme filtered by the pub_joue column
 * @method CommonConfigurationOrganisme findOneByModuleEcoSip(boolean $module_eco_sip) Return the first CommonConfigurationOrganisme filtered by the module_eco_sip column
 * @method CommonConfigurationOrganisme findOneByModuleMpePub(boolean $module_mpe_pub) Return the first CommonConfigurationOrganisme filtered by the module_mpe_pub column
 * @method CommonConfigurationOrganisme findOneByModuleAdministrationDocument(boolean $module_administration_document) Return the first CommonConfigurationOrganisme filtered by the module_administration_document column
 *
 * @method array findByOrganisme(string $organisme) Return CommonConfigurationOrganisme objects filtered by the organisme column
 * @method array findByEncheres(string $encheres) Return CommonConfigurationOrganisme objects filtered by the encheres column
 * @method array findByConsultationPjAutresPiecesTelechargeables(string $consultation_pj_autres_pieces_telechargeables) Return CommonConfigurationOrganisme objects filtered by the consultation_pj_autres_pieces_telechargeables column
 * @method array findByNoActivex(string $no_activex) Return CommonConfigurationOrganisme objects filtered by the no_activex column
 * @method array findByGestionMapa(string $gestion_mapa) Return CommonConfigurationOrganisme objects filtered by the gestion_mapa column
 * @method array findByArticle133UploadFichier(string $article_133_upload_fichier) Return CommonConfigurationOrganisme objects filtered by the article_133_upload_fichier column
 * @method array findByCentralePublication(string $centrale_publication) Return CommonConfigurationOrganisme objects filtered by the centrale_publication column
 * @method array findByOrganisationCentralisee(string $organisation_centralisee) Return CommonConfigurationOrganisme objects filtered by the organisation_centralisee column
 * @method array findByPresenceElu(string $presence_elu) Return CommonConfigurationOrganisme objects filtered by the presence_elu column
 * @method array findByTraduireConsultation(string $traduire_consultation) Return CommonConfigurationOrganisme objects filtered by the traduire_consultation column
 * @method array findBySuiviPassation(string $suivi_passation) Return CommonConfigurationOrganisme objects filtered by the suivi_passation column
 * @method array findByNumerotationRefCons(string $numerotation_ref_cons) Return CommonConfigurationOrganisme objects filtered by the numerotation_ref_cons column
 * @method array findByPmiLienPortailDefenseAgent(string $pmi_lien_portail_defense_agent) Return CommonConfigurationOrganisme objects filtered by the pmi_lien_portail_defense_agent column
 * @method array findByInterfaceArchiveArcadePmi(string $interface_archive_arcade_pmi) Return CommonConfigurationOrganisme objects filtered by the interface_archive_arcade_pmi column
 * @method array findByDesarchivageConsultation(string $desarchivage_consultation) Return CommonConfigurationOrganisme objects filtered by the desarchivage_consultation column
 * @method array findByAlimentationAutomatiqueListeInvites(string $alimentation_automatique_liste_invites) Return CommonConfigurationOrganisme objects filtered by the alimentation_automatique_liste_invites column
 * @method array findByInterfaceChorusPmi(string $interface_chorus_pmi) Return CommonConfigurationOrganisme objects filtered by the interface_chorus_pmi column
 * @method array findByArchivageConsultationSurPf(string $archivage_consultation_sur_pf) Return CommonConfigurationOrganisme objects filtered by the archivage_consultation_sur_pf column
 * @method array findByAutoriserModificationApresPhaseConsultation(string $autoriser_modification_apres_phase_consultation) Return CommonConfigurationOrganisme objects filtered by the autoriser_modification_apres_phase_consultation column
 * @method array findByImporterEnveloppe(string $importer_enveloppe) Return CommonConfigurationOrganisme objects filtered by the importer_enveloppe column
 * @method array findByExportMarchesNotifies(string $export_marches_notifies) Return CommonConfigurationOrganisme objects filtered by the export_marches_notifies column
 * @method array findByAccesAgentsCfeBdFournisseur(string $acces_agents_cfe_bd_fournisseur) Return CommonConfigurationOrganisme objects filtered by the acces_agents_cfe_bd_fournisseur column
 * @method array findByAccesAgentsCfeOuvertureAnalyse(string $acces_agents_cfe_ouverture_analyse) Return CommonConfigurationOrganisme objects filtered by the acces_agents_cfe_ouverture_analyse column
 * @method array findByUtiliserParametrageEncheres(string $utiliser_parametrage_encheres) Return CommonConfigurationOrganisme objects filtered by the utiliser_parametrage_encheres column
 * @method array findByVerifierCompteBoamp(string $verifier_compte_boamp) Return CommonConfigurationOrganisme objects filtered by the verifier_compte_boamp column
 * @method array findByGestionMandataire(string $gestion_mandataire) Return CommonConfigurationOrganisme objects filtered by the gestion_mandataire column
 * @method array findByFourEyes(string $four_eyes) Return CommonConfigurationOrganisme objects filtered by the four_eyes column
 * @method array findByInterfaceModuleRsem(string $interface_module_rsem) Return CommonConfigurationOrganisme objects filtered by the interface_module_rsem column
 * @method array findByArchivageConsultationSaeExterneEnvoiArchive(string $ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE) Return CommonConfigurationOrganisme objects filtered by the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE column
 * @method array findByArchivageConsultationSaeExterneTelechargementArchive(string $ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE) Return CommonConfigurationOrganisme objects filtered by the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE column
 * @method array findByAgentVerificationCertificatPeppol(string $agent_verification_certificat_peppol) Return CommonConfigurationOrganisme objects filtered by the agent_verification_certificat_peppol column
 * @method array findByFuseauHoraire(string $fuseau_horaire) Return CommonConfigurationOrganisme objects filtered by the fuseau_horaire column
 * @method array findByFicheWeka(string $fiche_weka) Return CommonConfigurationOrganisme objects filtered by the fiche_weka column
 * @method array findByMiseDispositionPiecesMarche(string $mise_disposition_pieces_marche) Return CommonConfigurationOrganisme objects filtered by the mise_disposition_pieces_marche column
 * @method array findByBaseDce(string $base_dce) Return CommonConfigurationOrganisme objects filtered by the base_dce column
 * @method array findByAvisMembresCommision(string $avis_membres_commision) Return CommonConfigurationOrganisme objects filtered by the avis_membres_commision column
 * @method array findByDonneesRedac(string $Donnees_Redac) Return CommonConfigurationOrganisme objects filtered by the Donnees_Redac column
 * @method array findByPersonnaliserAffichageThemeEtIllustration(string $Personnaliser_Affichage_Theme_Et_Illustration) Return CommonConfigurationOrganisme objects filtered by the Personnaliser_Affichage_Theme_Et_Illustration column
 * @method array findByTypeContrat(string $type_contrat) Return CommonConfigurationOrganisme objects filtered by the type_contrat column
 * @method array findByEntiteAdjudicatrice(string $entite_adjudicatrice) Return CommonConfigurationOrganisme objects filtered by the entite_adjudicatrice column
 * @method array findByCalendrierDeLaConsultation(string $calendrier_de_la_consultation) Return CommonConfigurationOrganisme objects filtered by the calendrier_de_la_consultation column
 * @method array findByDonneesComplementaires(string $donnees_complementaires) Return CommonConfigurationOrganisme objects filtered by the donnees_complementaires column
 * @method array findByEspaceCollaboratif(string $espace_collaboratif) Return CommonConfigurationOrganisme objects filtered by the espace_collaboratif column
 * @method array findByHistoriqueNavigationInscrits(string $historique_navigation_inscrits) Return CommonConfigurationOrganisme objects filtered by the historique_navigation_inscrits column
 * @method array findByIdentificationContrat(string $Identification_contrat) Return CommonConfigurationOrganisme objects filtered by the Identification_contrat column
 * @method array findByExtractionAccordsCadres(string $extraction_accords_cadres) Return CommonConfigurationOrganisme objects filtered by the extraction_accords_cadres column
 * @method array findByGestionOperations(string $gestion_operations) Return CommonConfigurationOrganisme objects filtered by the gestion_operations column
 * @method array findByExtractionSiretAcheteur(string $extraction_siret_acheteur) Return CommonConfigurationOrganisme objects filtered by the extraction_siret_acheteur column
 * @method array findByMarchePublicSimplifie(string $marche_public_simplifie) Return CommonConfigurationOrganisme objects filtered by the marche_public_simplifie column
 * @method array findByRecherchesFavoritesAgent(string $recherches_favorites_agent) Return CommonConfigurationOrganisme objects filtered by the recherches_favorites_agent column
 * @method array findByProfilRma(string $profil_rma) Return CommonConfigurationOrganisme objects filtered by the profil_rma column
 * @method array findByFiltreContratAcSad(string $filtre_contrat_ac_sad) Return CommonConfigurationOrganisme objects filtered by the filtre_contrat_ac_sad column
 * @method array findByAffichageNomServicePere(string $affichage_nom_service_pere) Return CommonConfigurationOrganisme objects filtered by the affichage_nom_service_pere column
 * @method array findByModeApplet(string $mode_applet) Return CommonConfigurationOrganisme objects filtered by the mode_applet column
 * @method array findByMarcheDefense(string $marche_defense) Return CommonConfigurationOrganisme objects filtered by the marche_defense column
 * @method array findByNumDonneesEssentiellesManuel(string $num_donnees_essentielles_manuel) Return CommonConfigurationOrganisme objects filtered by the num_donnees_essentielles_manuel column
 * @method array findByEspaceDocumentaire(boolean $espace_documentaire) Return CommonConfigurationOrganisme objects filtered by the espace_documentaire column
 * @method array findByNumeroProjetAchat(string $numero_projet_achat) Return CommonConfigurationOrganisme objects filtered by the numero_projet_achat column
 * @method array findByModuleExec(string $module_exec) Return CommonConfigurationOrganisme objects filtered by the module_exec column
 * @method array findByEchangesDocuments(boolean $echanges_documents) Return CommonConfigurationOrganisme objects filtered by the echanges_documents column
 * @method array findByHeureLimiteDeRemiseDePlisParDefaut(string $heure_limite_de_remise_de_plis_par_defaut) Return CommonConfigurationOrganisme objects filtered by the heure_limite_de_remise_de_plis_par_defaut column
 * @method array findBySaisieManuelleIdExterne(boolean $saisie_manuelle_id_externe) Return CommonConfigurationOrganisme objects filtered by the saisie_manuelle_id_externe column
 * @method array findByModuleSourcing(boolean $module_sourcing) Return CommonConfigurationOrganisme objects filtered by the module_sourcing column
 * @method array findByModuleRecensementProgrammation(boolean $module_recensement_programmation) Return CommonConfigurationOrganisme objects filtered by the module_recensement_programmation column
 * @method array findByModuleEnvol(boolean $module_envol) Return CommonConfigurationOrganisme objects filtered by the module_envol column
 * @method array findByModuleBiPremium(boolean $module_bi_premium) Return CommonConfigurationOrganisme objects filtered by the module_bi_premium column
 * @method array findByAnalyseOffres(int $analyse_offres) Return CommonConfigurationOrganisme objects filtered by the analyse_offres column
 * @method array findByCao(int $cao) Return CommonConfigurationOrganisme objects filtered by the cao column
 * @method array findByModuleBi(boolean $module_BI) Return CommonConfigurationOrganisme objects filtered by the module_BI column
 * @method array findByCmsActif(boolean $cms_actif) Return CommonConfigurationOrganisme objects filtered by the cms_actif column
 * @method array findByDceRestreint(boolean $dce_restreint) Return CommonConfigurationOrganisme objects filtered by the dce_restreint column
 * @method array findByActiverMonAssistantMarchesPublics(boolean $activer_mon_assistant_marches_publics) Return CommonConfigurationOrganisme objects filtered by the activer_mon_assistant_marches_publics column
 * @method array findByGestionContratDansExec(int $gestion_contrat_dans_exec) Return CommonConfigurationOrganisme objects filtered by the gestion_contrat_dans_exec column
 * @method array findByConsultationSimplifiee(boolean $consultation_simplifiee) Return CommonConfigurationOrganisme objects filtered by the consultation_simplifiee column
 * @method array findByTypageJo2024(boolean $typage_jo2024) Return CommonConfigurationOrganisme objects filtered by the typage_jo2024 column
 * @method array findByModuleTncp(boolean $module_tncp) Return CommonConfigurationOrganisme objects filtered by the module_tncp column
 * @method array findByAccesModuleSpaser(boolean $acces_module_spaser) Return CommonConfigurationOrganisme objects filtered by the acces_module_spaser column
 * @method array findByPubTncp(boolean $pub_tncp) Return CommonConfigurationOrganisme objects filtered by the pub_tncp column
 * @method array findByPubMol(boolean $pub_mol) Return CommonConfigurationOrganisme objects filtered by the pub_mol column
 * @method array findByPubJalFr(boolean $pub_jal_fr) Return CommonConfigurationOrganisme objects filtered by the pub_jal_fr column
 * @method array findByPubJalLux(boolean $pub_jal_lux) Return CommonConfigurationOrganisme objects filtered by the pub_jal_lux column
 * @method array findByPubJoue(boolean $pub_joue) Return CommonConfigurationOrganisme objects filtered by the pub_joue column
 * @method array findByModuleEcoSip(boolean $module_eco_sip) Return CommonConfigurationOrganisme objects filtered by the module_eco_sip column
 * @method array findByModuleMpePub(boolean $module_mpe_pub) Return CommonConfigurationOrganisme objects filtered by the module_mpe_pub column
 * @method array findByModuleAdministrationDocument(boolean $module_administration_document) Return CommonConfigurationOrganisme objects filtered by the module_administration_document column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConfigurationOrganismeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonConfigurationOrganismeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonConfigurationOrganisme', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonConfigurationOrganismeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonConfigurationOrganismeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonConfigurationOrganismeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonConfigurationOrganismeQuery) {
            return $criteria;
        }
        $query = new CommonConfigurationOrganismeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonConfigurationOrganisme|CommonConfigurationOrganisme[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonConfigurationOrganismePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonConfigurationOrganismePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConfigurationOrganisme A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByOrganisme($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConfigurationOrganisme A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `organisme`, `encheres`, `consultation_pj_autres_pieces_telechargeables`, `no_activex`, `gestion_mapa`, `article_133_upload_fichier`, `centrale_publication`, `organisation_centralisee`, `presence_elu`, `traduire_consultation`, `suivi_passation`, `numerotation_ref_cons`, `pmi_lien_portail_defense_agent`, `interface_archive_arcade_pmi`, `desarchivage_consultation`, `alimentation_automatique_liste_invites`, `interface_chorus_pmi`, `archivage_consultation_sur_pf`, `autoriser_modification_apres_phase_consultation`, `importer_enveloppe`, `export_marches_notifies`, `acces_agents_cfe_bd_fournisseur`, `acces_agents_cfe_ouverture_analyse`, `utiliser_parametrage_encheres`, `verifier_compte_boamp`, `gestion_mandataire`, `four_eyes`, `interface_module_rsem`, `ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE`, `ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE`, `agent_verification_certificat_peppol`, `fuseau_horaire`, `fiche_weka`, `mise_disposition_pieces_marche`, `base_dce`, `avis_membres_commision`, `Donnees_Redac`, `Personnaliser_Affichage_Theme_Et_Illustration`, `type_contrat`, `entite_adjudicatrice`, `calendrier_de_la_consultation`, `donnees_complementaires`, `espace_collaboratif`, `historique_navigation_inscrits`, `Identification_contrat`, `extraction_accords_cadres`, `gestion_operations`, `extraction_siret_acheteur`, `marche_public_simplifie`, `recherches_favorites_agent`, `profil_rma`, `filtre_contrat_ac_sad`, `affichage_nom_service_pere`, `mode_applet`, `marche_defense`, `num_donnees_essentielles_manuel`, `espace_documentaire`, `numero_projet_achat`, `module_exec`, `echanges_documents`, `heure_limite_de_remise_de_plis_par_defaut`, `saisie_manuelle_id_externe`, `module_sourcing`, `module_recensement_programmation`, `module_envol`, `module_bi_premium`, `analyse_offres`, `cao`, `module_BI`, `cms_actif`, `dce_restreint`, `activer_mon_assistant_marches_publics`, `gestion_contrat_dans_exec`, `consultation_simplifiee`, `typage_jo2024`, `module_tncp`, `acces_module_spaser`, `pub_tncp`, `pub_mol`, `pub_jal_fr`, `pub_jal_lux`, `pub_joue`, `module_eco_sip`, `module_mpe_pub`, `module_administration_document` FROM `configuration_organisme` WHERE `organisme` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonConfigurationOrganisme();
            $obj->hydrate($row);
            CommonConfigurationOrganismePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonConfigurationOrganisme|CommonConfigurationOrganisme[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonConfigurationOrganisme[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ORGANISME, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ORGANISME, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the encheres column
     *
     * Example usage:
     * <code>
     * $query->filterByEncheres('fooValue');   // WHERE encheres = 'fooValue'
     * $query->filterByEncheres('%fooValue%'); // WHERE encheres LIKE '%fooValue%'
     * </code>
     *
     * @param     string $encheres The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByEncheres($encheres = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($encheres)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $encheres)) {
                $encheres = str_replace('*', '%', $encheres);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ENCHERES, $encheres, $comparison);
    }

    /**
     * Filter the query on the consultation_pj_autres_pieces_telechargeables column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationPjAutresPiecesTelechargeables('fooValue');   // WHERE consultation_pj_autres_pieces_telechargeables = 'fooValue'
     * $query->filterByConsultationPjAutresPiecesTelechargeables('%fooValue%'); // WHERE consultation_pj_autres_pieces_telechargeables LIKE '%fooValue%'
     * </code>
     *
     * @param     string $consultationPjAutresPiecesTelechargeables The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByConsultationPjAutresPiecesTelechargeables($consultationPjAutresPiecesTelechargeables = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($consultationPjAutresPiecesTelechargeables)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $consultationPjAutresPiecesTelechargeables)) {
                $consultationPjAutresPiecesTelechargeables = str_replace('*', '%', $consultationPjAutresPiecesTelechargeables);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::CONSULTATION_PJ_AUTRES_PIECES_TELECHARGEABLES, $consultationPjAutresPiecesTelechargeables, $comparison);
    }

    /**
     * Filter the query on the no_activex column
     *
     * Example usage:
     * <code>
     * $query->filterByNoActivex('fooValue');   // WHERE no_activex = 'fooValue'
     * $query->filterByNoActivex('%fooValue%'); // WHERE no_activex LIKE '%fooValue%'
     * </code>
     *
     * @param     string $noActivex The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByNoActivex($noActivex = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($noActivex)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $noActivex)) {
                $noActivex = str_replace('*', '%', $noActivex);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::NO_ACTIVEX, $noActivex, $comparison);
    }

    /**
     * Filter the query on the gestion_mapa column
     *
     * Example usage:
     * <code>
     * $query->filterByGestionMapa('fooValue');   // WHERE gestion_mapa = 'fooValue'
     * $query->filterByGestionMapa('%fooValue%'); // WHERE gestion_mapa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gestionMapa The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByGestionMapa($gestionMapa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gestionMapa)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gestionMapa)) {
                $gestionMapa = str_replace('*', '%', $gestionMapa);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::GESTION_MAPA, $gestionMapa, $comparison);
    }

    /**
     * Filter the query on the article_133_upload_fichier column
     *
     * Example usage:
     * <code>
     * $query->filterByArticle133UploadFichier('fooValue');   // WHERE article_133_upload_fichier = 'fooValue'
     * $query->filterByArticle133UploadFichier('%fooValue%'); // WHERE article_133_upload_fichier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $article133UploadFichier The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByArticle133UploadFichier($article133UploadFichier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($article133UploadFichier)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $article133UploadFichier)) {
                $article133UploadFichier = str_replace('*', '%', $article133UploadFichier);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ARTICLE_133_UPLOAD_FICHIER, $article133UploadFichier, $comparison);
    }

    /**
     * Filter the query on the centrale_publication column
     *
     * Example usage:
     * <code>
     * $query->filterByCentralePublication('fooValue');   // WHERE centrale_publication = 'fooValue'
     * $query->filterByCentralePublication('%fooValue%'); // WHERE centrale_publication LIKE '%fooValue%'
     * </code>
     *
     * @param     string $centralePublication The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByCentralePublication($centralePublication = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($centralePublication)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $centralePublication)) {
                $centralePublication = str_replace('*', '%', $centralePublication);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::CENTRALE_PUBLICATION, $centralePublication, $comparison);
    }

    /**
     * Filter the query on the organisation_centralisee column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisationCentralisee('fooValue');   // WHERE organisation_centralisee = 'fooValue'
     * $query->filterByOrganisationCentralisee('%fooValue%'); // WHERE organisation_centralisee LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisationCentralisee The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByOrganisationCentralisee($organisationCentralisee = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisationCentralisee)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisationCentralisee)) {
                $organisationCentralisee = str_replace('*', '%', $organisationCentralisee);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ORGANISATION_CENTRALISEE, $organisationCentralisee, $comparison);
    }

    /**
     * Filter the query on the presence_elu column
     *
     * Example usage:
     * <code>
     * $query->filterByPresenceElu('fooValue');   // WHERE presence_elu = 'fooValue'
     * $query->filterByPresenceElu('%fooValue%'); // WHERE presence_elu LIKE '%fooValue%'
     * </code>
     *
     * @param     string $presenceElu The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByPresenceElu($presenceElu = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($presenceElu)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $presenceElu)) {
                $presenceElu = str_replace('*', '%', $presenceElu);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::PRESENCE_ELU, $presenceElu, $comparison);
    }

    /**
     * Filter the query on the traduire_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByTraduireConsultation('fooValue');   // WHERE traduire_consultation = 'fooValue'
     * $query->filterByTraduireConsultation('%fooValue%'); // WHERE traduire_consultation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $traduireConsultation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByTraduireConsultation($traduireConsultation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($traduireConsultation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $traduireConsultation)) {
                $traduireConsultation = str_replace('*', '%', $traduireConsultation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::TRADUIRE_CONSULTATION, $traduireConsultation, $comparison);
    }

    /**
     * Filter the query on the suivi_passation column
     *
     * Example usage:
     * <code>
     * $query->filterBySuiviPassation('fooValue');   // WHERE suivi_passation = 'fooValue'
     * $query->filterBySuiviPassation('%fooValue%'); // WHERE suivi_passation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $suiviPassation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterBySuiviPassation($suiviPassation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($suiviPassation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $suiviPassation)) {
                $suiviPassation = str_replace('*', '%', $suiviPassation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::SUIVI_PASSATION, $suiviPassation, $comparison);
    }

    /**
     * Filter the query on the numerotation_ref_cons column
     *
     * Example usage:
     * <code>
     * $query->filterByNumerotationRefCons('fooValue');   // WHERE numerotation_ref_cons = 'fooValue'
     * $query->filterByNumerotationRefCons('%fooValue%'); // WHERE numerotation_ref_cons LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numerotationRefCons The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByNumerotationRefCons($numerotationRefCons = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numerotationRefCons)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numerotationRefCons)) {
                $numerotationRefCons = str_replace('*', '%', $numerotationRefCons);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::NUMEROTATION_REF_CONS, $numerotationRefCons, $comparison);
    }

    /**
     * Filter the query on the pmi_lien_portail_defense_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByPmiLienPortailDefenseAgent('fooValue');   // WHERE pmi_lien_portail_defense_agent = 'fooValue'
     * $query->filterByPmiLienPortailDefenseAgent('%fooValue%'); // WHERE pmi_lien_portail_defense_agent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pmiLienPortailDefenseAgent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByPmiLienPortailDefenseAgent($pmiLienPortailDefenseAgent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pmiLienPortailDefenseAgent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pmiLienPortailDefenseAgent)) {
                $pmiLienPortailDefenseAgent = str_replace('*', '%', $pmiLienPortailDefenseAgent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::PMI_LIEN_PORTAIL_DEFENSE_AGENT, $pmiLienPortailDefenseAgent, $comparison);
    }

    /**
     * Filter the query on the interface_archive_arcade_pmi column
     *
     * Example usage:
     * <code>
     * $query->filterByInterfaceArchiveArcadePmi('fooValue');   // WHERE interface_archive_arcade_pmi = 'fooValue'
     * $query->filterByInterfaceArchiveArcadePmi('%fooValue%'); // WHERE interface_archive_arcade_pmi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $interfaceArchiveArcadePmi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByInterfaceArchiveArcadePmi($interfaceArchiveArcadePmi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($interfaceArchiveArcadePmi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $interfaceArchiveArcadePmi)) {
                $interfaceArchiveArcadePmi = str_replace('*', '%', $interfaceArchiveArcadePmi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::INTERFACE_ARCHIVE_ARCADE_PMI, $interfaceArchiveArcadePmi, $comparison);
    }

    /**
     * Filter the query on the desarchivage_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByDesarchivageConsultation('fooValue');   // WHERE desarchivage_consultation = 'fooValue'
     * $query->filterByDesarchivageConsultation('%fooValue%'); // WHERE desarchivage_consultation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $desarchivageConsultation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByDesarchivageConsultation($desarchivageConsultation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($desarchivageConsultation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $desarchivageConsultation)) {
                $desarchivageConsultation = str_replace('*', '%', $desarchivageConsultation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::DESARCHIVAGE_CONSULTATION, $desarchivageConsultation, $comparison);
    }

    /**
     * Filter the query on the alimentation_automatique_liste_invites column
     *
     * Example usage:
     * <code>
     * $query->filterByAlimentationAutomatiqueListeInvites('fooValue');   // WHERE alimentation_automatique_liste_invites = 'fooValue'
     * $query->filterByAlimentationAutomatiqueListeInvites('%fooValue%'); // WHERE alimentation_automatique_liste_invites LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alimentationAutomatiqueListeInvites The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByAlimentationAutomatiqueListeInvites($alimentationAutomatiqueListeInvites = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alimentationAutomatiqueListeInvites)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alimentationAutomatiqueListeInvites)) {
                $alimentationAutomatiqueListeInvites = str_replace('*', '%', $alimentationAutomatiqueListeInvites);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ALIMENTATION_AUTOMATIQUE_LISTE_INVITES, $alimentationAutomatiqueListeInvites, $comparison);
    }

    /**
     * Filter the query on the interface_chorus_pmi column
     *
     * Example usage:
     * <code>
     * $query->filterByInterfaceChorusPmi('fooValue');   // WHERE interface_chorus_pmi = 'fooValue'
     * $query->filterByInterfaceChorusPmi('%fooValue%'); // WHERE interface_chorus_pmi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $interfaceChorusPmi The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByInterfaceChorusPmi($interfaceChorusPmi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($interfaceChorusPmi)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $interfaceChorusPmi)) {
                $interfaceChorusPmi = str_replace('*', '%', $interfaceChorusPmi);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::INTERFACE_CHORUS_PMI, $interfaceChorusPmi, $comparison);
    }

    /**
     * Filter the query on the archivage_consultation_sur_pf column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivageConsultationSurPf('fooValue');   // WHERE archivage_consultation_sur_pf = 'fooValue'
     * $query->filterByArchivageConsultationSurPf('%fooValue%'); // WHERE archivage_consultation_sur_pf LIKE '%fooValue%'
     * </code>
     *
     * @param     string $archivageConsultationSurPf The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByArchivageConsultationSurPf($archivageConsultationSurPf = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($archivageConsultationSurPf)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $archivageConsultationSurPf)) {
                $archivageConsultationSurPf = str_replace('*', '%', $archivageConsultationSurPf);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SUR_PF, $archivageConsultationSurPf, $comparison);
    }

    /**
     * Filter the query on the autoriser_modification_apres_phase_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByAutoriserModificationApresPhaseConsultation('fooValue');   // WHERE autoriser_modification_apres_phase_consultation = 'fooValue'
     * $query->filterByAutoriserModificationApresPhaseConsultation('%fooValue%'); // WHERE autoriser_modification_apres_phase_consultation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $autoriserModificationApresPhaseConsultation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByAutoriserModificationApresPhaseConsultation($autoriserModificationApresPhaseConsultation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($autoriserModificationApresPhaseConsultation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $autoriserModificationApresPhaseConsultation)) {
                $autoriserModificationApresPhaseConsultation = str_replace('*', '%', $autoriserModificationApresPhaseConsultation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::AUTORISER_MODIFICATION_APRES_PHASE_CONSULTATION, $autoriserModificationApresPhaseConsultation, $comparison);
    }

    /**
     * Filter the query on the importer_enveloppe column
     *
     * Example usage:
     * <code>
     * $query->filterByImporterEnveloppe('fooValue');   // WHERE importer_enveloppe = 'fooValue'
     * $query->filterByImporterEnveloppe('%fooValue%'); // WHERE importer_enveloppe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $importerEnveloppe The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByImporterEnveloppe($importerEnveloppe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($importerEnveloppe)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $importerEnveloppe)) {
                $importerEnveloppe = str_replace('*', '%', $importerEnveloppe);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::IMPORTER_ENVELOPPE, $importerEnveloppe, $comparison);
    }

    /**
     * Filter the query on the export_marches_notifies column
     *
     * Example usage:
     * <code>
     * $query->filterByExportMarchesNotifies('fooValue');   // WHERE export_marches_notifies = 'fooValue'
     * $query->filterByExportMarchesNotifies('%fooValue%'); // WHERE export_marches_notifies LIKE '%fooValue%'
     * </code>
     *
     * @param     string $exportMarchesNotifies The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByExportMarchesNotifies($exportMarchesNotifies = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($exportMarchesNotifies)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $exportMarchesNotifies)) {
                $exportMarchesNotifies = str_replace('*', '%', $exportMarchesNotifies);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::EXPORT_MARCHES_NOTIFIES, $exportMarchesNotifies, $comparison);
    }

    /**
     * Filter the query on the acces_agents_cfe_bd_fournisseur column
     *
     * Example usage:
     * <code>
     * $query->filterByAccesAgentsCfeBdFournisseur('fooValue');   // WHERE acces_agents_cfe_bd_fournisseur = 'fooValue'
     * $query->filterByAccesAgentsCfeBdFournisseur('%fooValue%'); // WHERE acces_agents_cfe_bd_fournisseur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accesAgentsCfeBdFournisseur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByAccesAgentsCfeBdFournisseur($accesAgentsCfeBdFournisseur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accesAgentsCfeBdFournisseur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $accesAgentsCfeBdFournisseur)) {
                $accesAgentsCfeBdFournisseur = str_replace('*', '%', $accesAgentsCfeBdFournisseur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_BD_FOURNISSEUR, $accesAgentsCfeBdFournisseur, $comparison);
    }

    /**
     * Filter the query on the acces_agents_cfe_ouverture_analyse column
     *
     * Example usage:
     * <code>
     * $query->filterByAccesAgentsCfeOuvertureAnalyse('fooValue');   // WHERE acces_agents_cfe_ouverture_analyse = 'fooValue'
     * $query->filterByAccesAgentsCfeOuvertureAnalyse('%fooValue%'); // WHERE acces_agents_cfe_ouverture_analyse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accesAgentsCfeOuvertureAnalyse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByAccesAgentsCfeOuvertureAnalyse($accesAgentsCfeOuvertureAnalyse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accesAgentsCfeOuvertureAnalyse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $accesAgentsCfeOuvertureAnalyse)) {
                $accesAgentsCfeOuvertureAnalyse = str_replace('*', '%', $accesAgentsCfeOuvertureAnalyse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ACCES_AGENTS_CFE_OUVERTURE_ANALYSE, $accesAgentsCfeOuvertureAnalyse, $comparison);
    }

    /**
     * Filter the query on the utiliser_parametrage_encheres column
     *
     * Example usage:
     * <code>
     * $query->filterByUtiliserParametrageEncheres('fooValue');   // WHERE utiliser_parametrage_encheres = 'fooValue'
     * $query->filterByUtiliserParametrageEncheres('%fooValue%'); // WHERE utiliser_parametrage_encheres LIKE '%fooValue%'
     * </code>
     *
     * @param     string $utiliserParametrageEncheres The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByUtiliserParametrageEncheres($utiliserParametrageEncheres = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($utiliserParametrageEncheres)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $utiliserParametrageEncheres)) {
                $utiliserParametrageEncheres = str_replace('*', '%', $utiliserParametrageEncheres);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::UTILISER_PARAMETRAGE_ENCHERES, $utiliserParametrageEncheres, $comparison);
    }

    /**
     * Filter the query on the verifier_compte_boamp column
     *
     * Example usage:
     * <code>
     * $query->filterByVerifierCompteBoamp('fooValue');   // WHERE verifier_compte_boamp = 'fooValue'
     * $query->filterByVerifierCompteBoamp('%fooValue%'); // WHERE verifier_compte_boamp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $verifierCompteBoamp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByVerifierCompteBoamp($verifierCompteBoamp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($verifierCompteBoamp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $verifierCompteBoamp)) {
                $verifierCompteBoamp = str_replace('*', '%', $verifierCompteBoamp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::VERIFIER_COMPTE_BOAMP, $verifierCompteBoamp, $comparison);
    }

    /**
     * Filter the query on the gestion_mandataire column
     *
     * Example usage:
     * <code>
     * $query->filterByGestionMandataire('fooValue');   // WHERE gestion_mandataire = 'fooValue'
     * $query->filterByGestionMandataire('%fooValue%'); // WHERE gestion_mandataire LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gestionMandataire The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByGestionMandataire($gestionMandataire = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gestionMandataire)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gestionMandataire)) {
                $gestionMandataire = str_replace('*', '%', $gestionMandataire);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::GESTION_MANDATAIRE, $gestionMandataire, $comparison);
    }

    /**
     * Filter the query on the four_eyes column
     *
     * Example usage:
     * <code>
     * $query->filterByFourEyes('fooValue');   // WHERE four_eyes = 'fooValue'
     * $query->filterByFourEyes('%fooValue%'); // WHERE four_eyes LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fourEyes The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByFourEyes($fourEyes = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fourEyes)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fourEyes)) {
                $fourEyes = str_replace('*', '%', $fourEyes);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::FOUR_EYES, $fourEyes, $comparison);
    }

    /**
     * Filter the query on the interface_module_rsem column
     *
     * Example usage:
     * <code>
     * $query->filterByInterfaceModuleRsem('fooValue');   // WHERE interface_module_rsem = 'fooValue'
     * $query->filterByInterfaceModuleRsem('%fooValue%'); // WHERE interface_module_rsem LIKE '%fooValue%'
     * </code>
     *
     * @param     string $interfaceModuleRsem The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByInterfaceModuleRsem($interfaceModuleRsem = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($interfaceModuleRsem)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $interfaceModuleRsem)) {
                $interfaceModuleRsem = str_replace('*', '%', $interfaceModuleRsem);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::INTERFACE_MODULE_RSEM, $interfaceModuleRsem, $comparison);
    }

    /**
     * Filter the query on the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivageConsultationSaeExterneEnvoiArchive('fooValue');   // WHERE ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE = 'fooValue'
     * $query->filterByArchivageConsultationSaeExterneEnvoiArchive('%fooValue%'); // WHERE ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $archivageConsultationSaeExterneEnvoiArchive The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByArchivageConsultationSaeExterneEnvoiArchive($archivageConsultationSaeExterneEnvoiArchive = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($archivageConsultationSaeExterneEnvoiArchive)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $archivageConsultationSaeExterneEnvoiArchive)) {
                $archivageConsultationSaeExterneEnvoiArchive = str_replace('*', '%', $archivageConsultationSaeExterneEnvoiArchive);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE, $archivageConsultationSaeExterneEnvoiArchive, $comparison);
    }

    /**
     * Filter the query on the ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE column
     *
     * Example usage:
     * <code>
     * $query->filterByArchivageConsultationSaeExterneTelechargementArchive('fooValue');   // WHERE ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE = 'fooValue'
     * $query->filterByArchivageConsultationSaeExterneTelechargementArchive('%fooValue%'); // WHERE ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE LIKE '%fooValue%'
     * </code>
     *
     * @param     string $archivageConsultationSaeExterneTelechargementArchive The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByArchivageConsultationSaeExterneTelechargementArchive($archivageConsultationSaeExterneTelechargementArchive = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($archivageConsultationSaeExterneTelechargementArchive)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $archivageConsultationSaeExterneTelechargementArchive)) {
                $archivageConsultationSaeExterneTelechargementArchive = str_replace('*', '%', $archivageConsultationSaeExterneTelechargementArchive);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE, $archivageConsultationSaeExterneTelechargementArchive, $comparison);
    }

    /**
     * Filter the query on the agent_verification_certificat_peppol column
     *
     * Example usage:
     * <code>
     * $query->filterByAgentVerificationCertificatPeppol('fooValue');   // WHERE agent_verification_certificat_peppol = 'fooValue'
     * $query->filterByAgentVerificationCertificatPeppol('%fooValue%'); // WHERE agent_verification_certificat_peppol LIKE '%fooValue%'
     * </code>
     *
     * @param     string $agentVerificationCertificatPeppol The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByAgentVerificationCertificatPeppol($agentVerificationCertificatPeppol = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($agentVerificationCertificatPeppol)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $agentVerificationCertificatPeppol)) {
                $agentVerificationCertificatPeppol = str_replace('*', '%', $agentVerificationCertificatPeppol);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::AGENT_VERIFICATION_CERTIFICAT_PEPPOL, $agentVerificationCertificatPeppol, $comparison);
    }

    /**
     * Filter the query on the fuseau_horaire column
     *
     * Example usage:
     * <code>
     * $query->filterByFuseauHoraire('fooValue');   // WHERE fuseau_horaire = 'fooValue'
     * $query->filterByFuseauHoraire('%fooValue%'); // WHERE fuseau_horaire LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fuseauHoraire The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByFuseauHoraire($fuseauHoraire = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fuseauHoraire)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fuseauHoraire)) {
                $fuseauHoraire = str_replace('*', '%', $fuseauHoraire);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::FUSEAU_HORAIRE, $fuseauHoraire, $comparison);
    }

    /**
     * Filter the query on the fiche_weka column
     *
     * Example usage:
     * <code>
     * $query->filterByFicheWeka('fooValue');   // WHERE fiche_weka = 'fooValue'
     * $query->filterByFicheWeka('%fooValue%'); // WHERE fiche_weka LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ficheWeka The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByFicheWeka($ficheWeka = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ficheWeka)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ficheWeka)) {
                $ficheWeka = str_replace('*', '%', $ficheWeka);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::FICHE_WEKA, $ficheWeka, $comparison);
    }

    /**
     * Filter the query on the mise_disposition_pieces_marche column
     *
     * Example usage:
     * <code>
     * $query->filterByMiseDispositionPiecesMarche('fooValue');   // WHERE mise_disposition_pieces_marche = 'fooValue'
     * $query->filterByMiseDispositionPiecesMarche('%fooValue%'); // WHERE mise_disposition_pieces_marche LIKE '%fooValue%'
     * </code>
     *
     * @param     string $miseDispositionPiecesMarche The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByMiseDispositionPiecesMarche($miseDispositionPiecesMarche = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($miseDispositionPiecesMarche)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $miseDispositionPiecesMarche)) {
                $miseDispositionPiecesMarche = str_replace('*', '%', $miseDispositionPiecesMarche);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MISE_DISPOSITION_PIECES_MARCHE, $miseDispositionPiecesMarche, $comparison);
    }

    /**
     * Filter the query on the base_dce column
     *
     * Example usage:
     * <code>
     * $query->filterByBaseDce('fooValue');   // WHERE base_dce = 'fooValue'
     * $query->filterByBaseDce('%fooValue%'); // WHERE base_dce LIKE '%fooValue%'
     * </code>
     *
     * @param     string $baseDce The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByBaseDce($baseDce = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($baseDce)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $baseDce)) {
                $baseDce = str_replace('*', '%', $baseDce);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::BASE_DCE, $baseDce, $comparison);
    }

    /**
     * Filter the query on the avis_membres_commision column
     *
     * Example usage:
     * <code>
     * $query->filterByAvisMembresCommision('fooValue');   // WHERE avis_membres_commision = 'fooValue'
     * $query->filterByAvisMembresCommision('%fooValue%'); // WHERE avis_membres_commision LIKE '%fooValue%'
     * </code>
     *
     * @param     string $avisMembresCommision The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByAvisMembresCommision($avisMembresCommision = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($avisMembresCommision)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $avisMembresCommision)) {
                $avisMembresCommision = str_replace('*', '%', $avisMembresCommision);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::AVIS_MEMBRES_COMMISION, $avisMembresCommision, $comparison);
    }

    /**
     * Filter the query on the Donnees_Redac column
     *
     * Example usage:
     * <code>
     * $query->filterByDonneesRedac('fooValue');   // WHERE Donnees_Redac = 'fooValue'
     * $query->filterByDonneesRedac('%fooValue%'); // WHERE Donnees_Redac LIKE '%fooValue%'
     * </code>
     *
     * @param     string $donneesRedac The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByDonneesRedac($donneesRedac = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($donneesRedac)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $donneesRedac)) {
                $donneesRedac = str_replace('*', '%', $donneesRedac);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::DONNEES_REDAC, $donneesRedac, $comparison);
    }

    /**
     * Filter the query on the Personnaliser_Affichage_Theme_Et_Illustration column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonnaliserAffichageThemeEtIllustration('fooValue');   // WHERE Personnaliser_Affichage_Theme_Et_Illustration = 'fooValue'
     * $query->filterByPersonnaliserAffichageThemeEtIllustration('%fooValue%'); // WHERE Personnaliser_Affichage_Theme_Et_Illustration LIKE '%fooValue%'
     * </code>
     *
     * @param     string $personnaliserAffichageThemeEtIllustration The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByPersonnaliserAffichageThemeEtIllustration($personnaliserAffichageThemeEtIllustration = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($personnaliserAffichageThemeEtIllustration)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $personnaliserAffichageThemeEtIllustration)) {
                $personnaliserAffichageThemeEtIllustration = str_replace('*', '%', $personnaliserAffichageThemeEtIllustration);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::PERSONNALISER_AFFICHAGE_THEME_ET_ILLUSTRATION, $personnaliserAffichageThemeEtIllustration, $comparison);
    }

    /**
     * Filter the query on the type_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeContrat('fooValue');   // WHERE type_contrat = 'fooValue'
     * $query->filterByTypeContrat('%fooValue%'); // WHERE type_contrat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeContrat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByTypeContrat($typeContrat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeContrat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeContrat)) {
                $typeContrat = str_replace('*', '%', $typeContrat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::TYPE_CONTRAT, $typeContrat, $comparison);
    }

    /**
     * Filter the query on the entite_adjudicatrice column
     *
     * Example usage:
     * <code>
     * $query->filterByEntiteAdjudicatrice('fooValue');   // WHERE entite_adjudicatrice = 'fooValue'
     * $query->filterByEntiteAdjudicatrice('%fooValue%'); // WHERE entite_adjudicatrice LIKE '%fooValue%'
     * </code>
     *
     * @param     string $entiteAdjudicatrice The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByEntiteAdjudicatrice($entiteAdjudicatrice = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($entiteAdjudicatrice)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $entiteAdjudicatrice)) {
                $entiteAdjudicatrice = str_replace('*', '%', $entiteAdjudicatrice);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ENTITE_ADJUDICATRICE, $entiteAdjudicatrice, $comparison);
    }

    /**
     * Filter the query on the calendrier_de_la_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByCalendrierDeLaConsultation('fooValue');   // WHERE calendrier_de_la_consultation = 'fooValue'
     * $query->filterByCalendrierDeLaConsultation('%fooValue%'); // WHERE calendrier_de_la_consultation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $calendrierDeLaConsultation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByCalendrierDeLaConsultation($calendrierDeLaConsultation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($calendrierDeLaConsultation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $calendrierDeLaConsultation)) {
                $calendrierDeLaConsultation = str_replace('*', '%', $calendrierDeLaConsultation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::CALENDRIER_DE_LA_CONSULTATION, $calendrierDeLaConsultation, $comparison);
    }

    /**
     * Filter the query on the donnees_complementaires column
     *
     * Example usage:
     * <code>
     * $query->filterByDonneesComplementaires('fooValue');   // WHERE donnees_complementaires = 'fooValue'
     * $query->filterByDonneesComplementaires('%fooValue%'); // WHERE donnees_complementaires LIKE '%fooValue%'
     * </code>
     *
     * @param     string $donneesComplementaires The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByDonneesComplementaires($donneesComplementaires = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($donneesComplementaires)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $donneesComplementaires)) {
                $donneesComplementaires = str_replace('*', '%', $donneesComplementaires);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::DONNEES_COMPLEMENTAIRES, $donneesComplementaires, $comparison);
    }

    /**
     * Filter the query on the espace_collaboratif column
     *
     * Example usage:
     * <code>
     * $query->filterByEspaceCollaboratif('fooValue');   // WHERE espace_collaboratif = 'fooValue'
     * $query->filterByEspaceCollaboratif('%fooValue%'); // WHERE espace_collaboratif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $espaceCollaboratif The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByEspaceCollaboratif($espaceCollaboratif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($espaceCollaboratif)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $espaceCollaboratif)) {
                $espaceCollaboratif = str_replace('*', '%', $espaceCollaboratif);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ESPACE_COLLABORATIF, $espaceCollaboratif, $comparison);
    }

    /**
     * Filter the query on the historique_navigation_inscrits column
     *
     * Example usage:
     * <code>
     * $query->filterByHistoriqueNavigationInscrits('fooValue');   // WHERE historique_navigation_inscrits = 'fooValue'
     * $query->filterByHistoriqueNavigationInscrits('%fooValue%'); // WHERE historique_navigation_inscrits LIKE '%fooValue%'
     * </code>
     *
     * @param     string $historiqueNavigationInscrits The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByHistoriqueNavigationInscrits($historiqueNavigationInscrits = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($historiqueNavigationInscrits)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $historiqueNavigationInscrits)) {
                $historiqueNavigationInscrits = str_replace('*', '%', $historiqueNavigationInscrits);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::HISTORIQUE_NAVIGATION_INSCRITS, $historiqueNavigationInscrits, $comparison);
    }

    /**
     * Filter the query on the Identification_contrat column
     *
     * Example usage:
     * <code>
     * $query->filterByIdentificationContrat('fooValue');   // WHERE Identification_contrat = 'fooValue'
     * $query->filterByIdentificationContrat('%fooValue%'); // WHERE Identification_contrat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $identificationContrat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByIdentificationContrat($identificationContrat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($identificationContrat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $identificationContrat)) {
                $identificationContrat = str_replace('*', '%', $identificationContrat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::IDENTIFICATION_CONTRAT, $identificationContrat, $comparison);
    }

    /**
     * Filter the query on the extraction_accords_cadres column
     *
     * Example usage:
     * <code>
     * $query->filterByExtractionAccordsCadres('fooValue');   // WHERE extraction_accords_cadres = 'fooValue'
     * $query->filterByExtractionAccordsCadres('%fooValue%'); // WHERE extraction_accords_cadres LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extractionAccordsCadres The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByExtractionAccordsCadres($extractionAccordsCadres = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extractionAccordsCadres)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $extractionAccordsCadres)) {
                $extractionAccordsCadres = str_replace('*', '%', $extractionAccordsCadres);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::EXTRACTION_ACCORDS_CADRES, $extractionAccordsCadres, $comparison);
    }

    /**
     * Filter the query on the gestion_operations column
     *
     * Example usage:
     * <code>
     * $query->filterByGestionOperations('fooValue');   // WHERE gestion_operations = 'fooValue'
     * $query->filterByGestionOperations('%fooValue%'); // WHERE gestion_operations LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gestionOperations The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByGestionOperations($gestionOperations = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gestionOperations)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $gestionOperations)) {
                $gestionOperations = str_replace('*', '%', $gestionOperations);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::GESTION_OPERATIONS, $gestionOperations, $comparison);
    }

    /**
     * Filter the query on the extraction_siret_acheteur column
     *
     * Example usage:
     * <code>
     * $query->filterByExtractionSiretAcheteur('fooValue');   // WHERE extraction_siret_acheteur = 'fooValue'
     * $query->filterByExtractionSiretAcheteur('%fooValue%'); // WHERE extraction_siret_acheteur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extractionSiretAcheteur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByExtractionSiretAcheteur($extractionSiretAcheteur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extractionSiretAcheteur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $extractionSiretAcheteur)) {
                $extractionSiretAcheteur = str_replace('*', '%', $extractionSiretAcheteur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::EXTRACTION_SIRET_ACHETEUR, $extractionSiretAcheteur, $comparison);
    }

    /**
     * Filter the query on the marche_public_simplifie column
     *
     * Example usage:
     * <code>
     * $query->filterByMarchePublicSimplifie('fooValue');   // WHERE marche_public_simplifie = 'fooValue'
     * $query->filterByMarchePublicSimplifie('%fooValue%'); // WHERE marche_public_simplifie LIKE '%fooValue%'
     * </code>
     *
     * @param     string $marchePublicSimplifie The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByMarchePublicSimplifie($marchePublicSimplifie = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($marchePublicSimplifie)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $marchePublicSimplifie)) {
                $marchePublicSimplifie = str_replace('*', '%', $marchePublicSimplifie);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MARCHE_PUBLIC_SIMPLIFIE, $marchePublicSimplifie, $comparison);
    }

    /**
     * Filter the query on the recherches_favorites_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByRecherchesFavoritesAgent('fooValue');   // WHERE recherches_favorites_agent = 'fooValue'
     * $query->filterByRecherchesFavoritesAgent('%fooValue%'); // WHERE recherches_favorites_agent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $recherchesFavoritesAgent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByRecherchesFavoritesAgent($recherchesFavoritesAgent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($recherchesFavoritesAgent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $recherchesFavoritesAgent)) {
                $recherchesFavoritesAgent = str_replace('*', '%', $recherchesFavoritesAgent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::RECHERCHES_FAVORITES_AGENT, $recherchesFavoritesAgent, $comparison);
    }

    /**
     * Filter the query on the profil_rma column
     *
     * Example usage:
     * <code>
     * $query->filterByProfilRma('fooValue');   // WHERE profil_rma = 'fooValue'
     * $query->filterByProfilRma('%fooValue%'); // WHERE profil_rma LIKE '%fooValue%'
     * </code>
     *
     * @param     string $profilRma The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByProfilRma($profilRma = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($profilRma)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $profilRma)) {
                $profilRma = str_replace('*', '%', $profilRma);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::PROFIL_RMA, $profilRma, $comparison);
    }

    /**
     * Filter the query on the filtre_contrat_ac_sad column
     *
     * Example usage:
     * <code>
     * $query->filterByFiltreContratAcSad('fooValue');   // WHERE filtre_contrat_ac_sad = 'fooValue'
     * $query->filterByFiltreContratAcSad('%fooValue%'); // WHERE filtre_contrat_ac_sad LIKE '%fooValue%'
     * </code>
     *
     * @param     string $filtreContratAcSad The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByFiltreContratAcSad($filtreContratAcSad = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($filtreContratAcSad)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $filtreContratAcSad)) {
                $filtreContratAcSad = str_replace('*', '%', $filtreContratAcSad);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::FILTRE_CONTRAT_AC_SAD, $filtreContratAcSad, $comparison);
    }

    /**
     * Filter the query on the affichage_nom_service_pere column
     *
     * Example usage:
     * <code>
     * $query->filterByAffichageNomServicePere('fooValue');   // WHERE affichage_nom_service_pere = 'fooValue'
     * $query->filterByAffichageNomServicePere('%fooValue%'); // WHERE affichage_nom_service_pere LIKE '%fooValue%'
     * </code>
     *
     * @param     string $affichageNomServicePere The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByAffichageNomServicePere($affichageNomServicePere = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($affichageNomServicePere)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $affichageNomServicePere)) {
                $affichageNomServicePere = str_replace('*', '%', $affichageNomServicePere);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::AFFICHAGE_NOM_SERVICE_PERE, $affichageNomServicePere, $comparison);
    }

    /**
     * Filter the query on the mode_applet column
     *
     * Example usage:
     * <code>
     * $query->filterByModeApplet('fooValue');   // WHERE mode_applet = 'fooValue'
     * $query->filterByModeApplet('%fooValue%'); // WHERE mode_applet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $modeApplet The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByModeApplet($modeApplet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($modeApplet)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $modeApplet)) {
                $modeApplet = str_replace('*', '%', $modeApplet);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MODE_APPLET, $modeApplet, $comparison);
    }

    /**
     * Filter the query on the marche_defense column
     *
     * Example usage:
     * <code>
     * $query->filterByMarcheDefense('fooValue');   // WHERE marche_defense = 'fooValue'
     * $query->filterByMarcheDefense('%fooValue%'); // WHERE marche_defense LIKE '%fooValue%'
     * </code>
     *
     * @param     string $marcheDefense The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByMarcheDefense($marcheDefense = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($marcheDefense)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $marcheDefense)) {
                $marcheDefense = str_replace('*', '%', $marcheDefense);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MARCHE_DEFENSE, $marcheDefense, $comparison);
    }

    /**
     * Filter the query on the num_donnees_essentielles_manuel column
     *
     * Example usage:
     * <code>
     * $query->filterByNumDonneesEssentiellesManuel('fooValue');   // WHERE num_donnees_essentielles_manuel = 'fooValue'
     * $query->filterByNumDonneesEssentiellesManuel('%fooValue%'); // WHERE num_donnees_essentielles_manuel LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numDonneesEssentiellesManuel The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByNumDonneesEssentiellesManuel($numDonneesEssentiellesManuel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numDonneesEssentiellesManuel)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numDonneesEssentiellesManuel)) {
                $numDonneesEssentiellesManuel = str_replace('*', '%', $numDonneesEssentiellesManuel);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::NUM_DONNEES_ESSENTIELLES_MANUEL, $numDonneesEssentiellesManuel, $comparison);
    }

    /**
     * Filter the query on the espace_documentaire column
     *
     * Example usage:
     * <code>
     * $query->filterByEspaceDocumentaire(true); // WHERE espace_documentaire = true
     * $query->filterByEspaceDocumentaire('yes'); // WHERE espace_documentaire = true
     * </code>
     *
     * @param     boolean|string $espaceDocumentaire The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByEspaceDocumentaire($espaceDocumentaire = null, $comparison = null)
    {
        if (is_string($espaceDocumentaire)) {
            $espaceDocumentaire = in_array(strtolower($espaceDocumentaire), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ESPACE_DOCUMENTAIRE, $espaceDocumentaire, $comparison);
    }

    /**
     * Filter the query on the numero_projet_achat column
     *
     * Example usage:
     * <code>
     * $query->filterByNumeroProjetAchat('fooValue');   // WHERE numero_projet_achat = 'fooValue'
     * $query->filterByNumeroProjetAchat('%fooValue%'); // WHERE numero_projet_achat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numeroProjetAchat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByNumeroProjetAchat($numeroProjetAchat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numeroProjetAchat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numeroProjetAchat)) {
                $numeroProjetAchat = str_replace('*', '%', $numeroProjetAchat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::NUMERO_PROJET_ACHAT, $numeroProjetAchat, $comparison);
    }

    /**
     * Filter the query on the module_exec column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleExec('fooValue');   // WHERE module_exec = 'fooValue'
     * $query->filterByModuleExec('%fooValue%'); // WHERE module_exec LIKE '%fooValue%'
     * </code>
     *
     * @param     string $moduleExec The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByModuleExec($moduleExec = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($moduleExec)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $moduleExec)) {
                $moduleExec = str_replace('*', '%', $moduleExec);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MODULE_EXEC, $moduleExec, $comparison);
    }

    /**
     * Filter the query on the echanges_documents column
     *
     * Example usage:
     * <code>
     * $query->filterByEchangesDocuments(true); // WHERE echanges_documents = true
     * $query->filterByEchangesDocuments('yes'); // WHERE echanges_documents = true
     * </code>
     *
     * @param     boolean|string $echangesDocuments The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByEchangesDocuments($echangesDocuments = null, $comparison = null)
    {
        if (is_string($echangesDocuments)) {
            $echangesDocuments = in_array(strtolower($echangesDocuments), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ECHANGES_DOCUMENTS, $echangesDocuments, $comparison);
    }

    /**
     * Filter the query on the heure_limite_de_remise_de_plis_par_defaut column
     *
     * Example usage:
     * <code>
     * $query->filterByHeureLimiteDeRemiseDePlisParDefaut('fooValue');   // WHERE heure_limite_de_remise_de_plis_par_defaut = 'fooValue'
     * $query->filterByHeureLimiteDeRemiseDePlisParDefaut('%fooValue%'); // WHERE heure_limite_de_remise_de_plis_par_defaut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $heureLimiteDeRemiseDePlisParDefaut The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByHeureLimiteDeRemiseDePlisParDefaut($heureLimiteDeRemiseDePlisParDefaut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($heureLimiteDeRemiseDePlisParDefaut)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $heureLimiteDeRemiseDePlisParDefaut)) {
                $heureLimiteDeRemiseDePlisParDefaut = str_replace('*', '%', $heureLimiteDeRemiseDePlisParDefaut);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::HEURE_LIMITE_DE_REMISE_DE_PLIS_PAR_DEFAUT, $heureLimiteDeRemiseDePlisParDefaut, $comparison);
    }

    /**
     * Filter the query on the saisie_manuelle_id_externe column
     *
     * Example usage:
     * <code>
     * $query->filterBySaisieManuelleIdExterne(true); // WHERE saisie_manuelle_id_externe = true
     * $query->filterBySaisieManuelleIdExterne('yes'); // WHERE saisie_manuelle_id_externe = true
     * </code>
     *
     * @param     boolean|string $saisieManuelleIdExterne The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterBySaisieManuelleIdExterne($saisieManuelleIdExterne = null, $comparison = null)
    {
        if (is_string($saisieManuelleIdExterne)) {
            $saisieManuelleIdExterne = in_array(strtolower($saisieManuelleIdExterne), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::SAISIE_MANUELLE_ID_EXTERNE, $saisieManuelleIdExterne, $comparison);
    }

    /**
     * Filter the query on the module_sourcing column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleSourcing(true); // WHERE module_sourcing = true
     * $query->filterByModuleSourcing('yes'); // WHERE module_sourcing = true
     * </code>
     *
     * @param     boolean|string $moduleSourcing The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByModuleSourcing($moduleSourcing = null, $comparison = null)
    {
        if (is_string($moduleSourcing)) {
            $moduleSourcing = in_array(strtolower($moduleSourcing), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MODULE_SOURCING, $moduleSourcing, $comparison);
    }

    /**
     * Filter the query on the module_recensement_programmation column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleRecensementProgrammation(true); // WHERE module_recensement_programmation = true
     * $query->filterByModuleRecensementProgrammation('yes'); // WHERE module_recensement_programmation = true
     * </code>
     *
     * @param     boolean|string $moduleRecensementProgrammation The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByModuleRecensementProgrammation($moduleRecensementProgrammation = null, $comparison = null)
    {
        if (is_string($moduleRecensementProgrammation)) {
            $moduleRecensementProgrammation = in_array(strtolower($moduleRecensementProgrammation), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MODULE_RECENSEMENT_PROGRAMMATION, $moduleRecensementProgrammation, $comparison);
    }

    /**
     * Filter the query on the module_envol column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleEnvol(true); // WHERE module_envol = true
     * $query->filterByModuleEnvol('yes'); // WHERE module_envol = true
     * </code>
     *
     * @param     boolean|string $moduleEnvol The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByModuleEnvol($moduleEnvol = null, $comparison = null)
    {
        if (is_string($moduleEnvol)) {
            $moduleEnvol = in_array(strtolower($moduleEnvol), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MODULE_ENVOL, $moduleEnvol, $comparison);
    }

    /**
     * Filter the query on the module_bi_premium column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleBiPremium(true); // WHERE module_bi_premium = true
     * $query->filterByModuleBiPremium('yes'); // WHERE module_bi_premium = true
     * </code>
     *
     * @param     boolean|string $moduleBiPremium The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByModuleBiPremium($moduleBiPremium = null, $comparison = null)
    {
        if (is_string($moduleBiPremium)) {
            $moduleBiPremium = in_array(strtolower($moduleBiPremium), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MODULE_BI_PREMIUM, $moduleBiPremium, $comparison);
    }

    /**
     * Filter the query on the analyse_offres column
     *
     * Example usage:
     * <code>
     * $query->filterByAnalyseOffres(1234); // WHERE analyse_offres = 1234
     * $query->filterByAnalyseOffres(array(12, 34)); // WHERE analyse_offres IN (12, 34)
     * $query->filterByAnalyseOffres(array('min' => 12)); // WHERE analyse_offres >= 12
     * $query->filterByAnalyseOffres(array('max' => 12)); // WHERE analyse_offres <= 12
     * </code>
     *
     * @param     mixed $analyseOffres The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByAnalyseOffres($analyseOffres = null, $comparison = null)
    {
        if (is_array($analyseOffres)) {
            $useMinMax = false;
            if (isset($analyseOffres['min'])) {
                $this->addUsingAlias(CommonConfigurationOrganismePeer::ANALYSE_OFFRES, $analyseOffres['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($analyseOffres['max'])) {
                $this->addUsingAlias(CommonConfigurationOrganismePeer::ANALYSE_OFFRES, $analyseOffres['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ANALYSE_OFFRES, $analyseOffres, $comparison);
    }

    /**
     * Filter the query on the cao column
     *
     * Example usage:
     * <code>
     * $query->filterByCao(1234); // WHERE cao = 1234
     * $query->filterByCao(array(12, 34)); // WHERE cao IN (12, 34)
     * $query->filterByCao(array('min' => 12)); // WHERE cao >= 12
     * $query->filterByCao(array('max' => 12)); // WHERE cao <= 12
     * </code>
     *
     * @param     mixed $cao The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByCao($cao = null, $comparison = null)
    {
        if (is_array($cao)) {
            $useMinMax = false;
            if (isset($cao['min'])) {
                $this->addUsingAlias(CommonConfigurationOrganismePeer::CAO, $cao['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cao['max'])) {
                $this->addUsingAlias(CommonConfigurationOrganismePeer::CAO, $cao['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::CAO, $cao, $comparison);
    }

    /**
     * Filter the query on the module_BI column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleBi(true); // WHERE module_BI = true
     * $query->filterByModuleBi('yes'); // WHERE module_BI = true
     * </code>
     *
     * @param     boolean|string $moduleBi The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByModuleBi($moduleBi = null, $comparison = null)
    {
        if (is_string($moduleBi)) {
            $moduleBi = in_array(strtolower($moduleBi), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MODULE_BI, $moduleBi, $comparison);
    }

    /**
     * Filter the query on the cms_actif column
     *
     * Example usage:
     * <code>
     * $query->filterByCmsActif(true); // WHERE cms_actif = true
     * $query->filterByCmsActif('yes'); // WHERE cms_actif = true
     * </code>
     *
     * @param     boolean|string $cmsActif The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByCmsActif($cmsActif = null, $comparison = null)
    {
        if (is_string($cmsActif)) {
            $cmsActif = in_array(strtolower($cmsActif), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::CMS_ACTIF, $cmsActif, $comparison);
    }

    /**
     * Filter the query on the dce_restreint column
     *
     * Example usage:
     * <code>
     * $query->filterByDceRestreint(true); // WHERE dce_restreint = true
     * $query->filterByDceRestreint('yes'); // WHERE dce_restreint = true
     * </code>
     *
     * @param     boolean|string $dceRestreint The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByDceRestreint($dceRestreint = null, $comparison = null)
    {
        if (is_string($dceRestreint)) {
            $dceRestreint = in_array(strtolower($dceRestreint), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::DCE_RESTREINT, $dceRestreint, $comparison);
    }

    /**
     * Filter the query on the activer_mon_assistant_marches_publics column
     *
     * Example usage:
     * <code>
     * $query->filterByActiverMonAssistantMarchesPublics(true); // WHERE activer_mon_assistant_marches_publics = true
     * $query->filterByActiverMonAssistantMarchesPublics('yes'); // WHERE activer_mon_assistant_marches_publics = true
     * </code>
     *
     * @param     boolean|string $activerMonAssistantMarchesPublics The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByActiverMonAssistantMarchesPublics($activerMonAssistantMarchesPublics = null, $comparison = null)
    {
        if (is_string($activerMonAssistantMarchesPublics)) {
            $activerMonAssistantMarchesPublics = in_array(strtolower($activerMonAssistantMarchesPublics), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ACTIVER_MON_ASSISTANT_MARCHES_PUBLICS, $activerMonAssistantMarchesPublics, $comparison);
    }

    /**
     * Filter the query on the gestion_contrat_dans_exec column
     *
     * Example usage:
     * <code>
     * $query->filterByGestionContratDansExec(1234); // WHERE gestion_contrat_dans_exec = 1234
     * $query->filterByGestionContratDansExec(array(12, 34)); // WHERE gestion_contrat_dans_exec IN (12, 34)
     * $query->filterByGestionContratDansExec(array('min' => 12)); // WHERE gestion_contrat_dans_exec >= 12
     * $query->filterByGestionContratDansExec(array('max' => 12)); // WHERE gestion_contrat_dans_exec <= 12
     * </code>
     *
     * @param     mixed $gestionContratDansExec The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByGestionContratDansExec($gestionContratDansExec = null, $comparison = null)
    {
        if (is_array($gestionContratDansExec)) {
            $useMinMax = false;
            if (isset($gestionContratDansExec['min'])) {
                $this->addUsingAlias(CommonConfigurationOrganismePeer::GESTION_CONTRAT_DANS_EXEC, $gestionContratDansExec['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($gestionContratDansExec['max'])) {
                $this->addUsingAlias(CommonConfigurationOrganismePeer::GESTION_CONTRAT_DANS_EXEC, $gestionContratDansExec['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::GESTION_CONTRAT_DANS_EXEC, $gestionContratDansExec, $comparison);
    }

    /**
     * Filter the query on the consultation_simplifiee column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationSimplifiee(true); // WHERE consultation_simplifiee = true
     * $query->filterByConsultationSimplifiee('yes'); // WHERE consultation_simplifiee = true
     * </code>
     *
     * @param     boolean|string $consultationSimplifiee The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByConsultationSimplifiee($consultationSimplifiee = null, $comparison = null)
    {
        if (is_string($consultationSimplifiee)) {
            $consultationSimplifiee = in_array(strtolower($consultationSimplifiee), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::CONSULTATION_SIMPLIFIEE, $consultationSimplifiee, $comparison);
    }

    /**
     * Filter the query on the typage_jo2024 column
     *
     * Example usage:
     * <code>
     * $query->filterByTypageJo2024(true); // WHERE typage_jo2024 = true
     * $query->filterByTypageJo2024('yes'); // WHERE typage_jo2024 = true
     * </code>
     *
     * @param     boolean|string $typageJo2024 The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByTypageJo2024($typageJo2024 = null, $comparison = null)
    {
        if (is_string($typageJo2024)) {
            $typageJo2024 = in_array(strtolower($typageJo2024), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::TYPAGE_JO2024, $typageJo2024, $comparison);
    }

    /**
     * Filter the query on the module_tncp column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleTncp(true); // WHERE module_tncp = true
     * $query->filterByModuleTncp('yes'); // WHERE module_tncp = true
     * </code>
     *
     * @param     boolean|string $moduleTncp The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByModuleTncp($moduleTncp = null, $comparison = null)
    {
        if (is_string($moduleTncp)) {
            $moduleTncp = in_array(strtolower($moduleTncp), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MODULE_TNCP, $moduleTncp, $comparison);
    }

    /**
     * Filter the query on the acces_module_spaser column
     *
     * Example usage:
     * <code>
     * $query->filterByAccesModuleSpaser(true); // WHERE acces_module_spaser = true
     * $query->filterByAccesModuleSpaser('yes'); // WHERE acces_module_spaser = true
     * </code>
     *
     * @param     boolean|string $accesModuleSpaser The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByAccesModuleSpaser($accesModuleSpaser = null, $comparison = null)
    {
        if (is_string($accesModuleSpaser)) {
            $accesModuleSpaser = in_array(strtolower($accesModuleSpaser), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::ACCES_MODULE_SPASER, $accesModuleSpaser, $comparison);
    }

    /**
     * Filter the query on the pub_tncp column
     *
     * Example usage:
     * <code>
     * $query->filterByPubTncp(true); // WHERE pub_tncp = true
     * $query->filterByPubTncp('yes'); // WHERE pub_tncp = true
     * </code>
     *
     * @param     boolean|string $pubTncp The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByPubTncp($pubTncp = null, $comparison = null)
    {
        if (is_string($pubTncp)) {
            $pubTncp = in_array(strtolower($pubTncp), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::PUB_TNCP, $pubTncp, $comparison);
    }

    /**
     * Filter the query on the pub_mol column
     *
     * Example usage:
     * <code>
     * $query->filterByPubMol(true); // WHERE pub_mol = true
     * $query->filterByPubMol('yes'); // WHERE pub_mol = true
     * </code>
     *
     * @param     boolean|string $pubMol The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByPubMol($pubMol = null, $comparison = null)
    {
        if (is_string($pubMol)) {
            $pubMol = in_array(strtolower($pubMol), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::PUB_MOL, $pubMol, $comparison);
    }

    /**
     * Filter the query on the pub_jal_fr column
     *
     * Example usage:
     * <code>
     * $query->filterByPubJalFr(true); // WHERE pub_jal_fr = true
     * $query->filterByPubJalFr('yes'); // WHERE pub_jal_fr = true
     * </code>
     *
     * @param     boolean|string $pubJalFr The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByPubJalFr($pubJalFr = null, $comparison = null)
    {
        if (is_string($pubJalFr)) {
            $pubJalFr = in_array(strtolower($pubJalFr), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::PUB_JAL_FR, $pubJalFr, $comparison);
    }

    /**
     * Filter the query on the pub_jal_lux column
     *
     * Example usage:
     * <code>
     * $query->filterByPubJalLux(true); // WHERE pub_jal_lux = true
     * $query->filterByPubJalLux('yes'); // WHERE pub_jal_lux = true
     * </code>
     *
     * @param     boolean|string $pubJalLux The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByPubJalLux($pubJalLux = null, $comparison = null)
    {
        if (is_string($pubJalLux)) {
            $pubJalLux = in_array(strtolower($pubJalLux), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::PUB_JAL_LUX, $pubJalLux, $comparison);
    }

    /**
     * Filter the query on the pub_joue column
     *
     * Example usage:
     * <code>
     * $query->filterByPubJoue(true); // WHERE pub_joue = true
     * $query->filterByPubJoue('yes'); // WHERE pub_joue = true
     * </code>
     *
     * @param     boolean|string $pubJoue The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByPubJoue($pubJoue = null, $comparison = null)
    {
        if (is_string($pubJoue)) {
            $pubJoue = in_array(strtolower($pubJoue), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::PUB_JOUE, $pubJoue, $comparison);
    }

    /**
     * Filter the query on the module_eco_sip column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleEcoSip(true); // WHERE module_eco_sip = true
     * $query->filterByModuleEcoSip('yes'); // WHERE module_eco_sip = true
     * </code>
     *
     * @param     boolean|string $moduleEcoSip The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByModuleEcoSip($moduleEcoSip = null, $comparison = null)
    {
        if (is_string($moduleEcoSip)) {
            $moduleEcoSip = in_array(strtolower($moduleEcoSip), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MODULE_ECO_SIP, $moduleEcoSip, $comparison);
    }

    /**
     * Filter the query on the module_mpe_pub column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleMpePub(true); // WHERE module_mpe_pub = true
     * $query->filterByModuleMpePub('yes'); // WHERE module_mpe_pub = true
     * </code>
     *
     * @param     boolean|string $moduleMpePub The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByModuleMpePub($moduleMpePub = null, $comparison = null)
    {
        if (is_string($moduleMpePub)) {
            $moduleMpePub = in_array(strtolower($moduleMpePub), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MODULE_MPE_PUB, $moduleMpePub, $comparison);
    }

    /**
     * Filter the query on the module_administration_document column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleAdministrationDocument(true); // WHERE module_administration_document = true
     * $query->filterByModuleAdministrationDocument('yes'); // WHERE module_administration_document = true
     * </code>
     *
     * @param     boolean|string $moduleAdministrationDocument The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function filterByModuleAdministrationDocument($moduleAdministrationDocument = null, $comparison = null)
    {
        if (is_string($moduleAdministrationDocument)) {
            $moduleAdministrationDocument = in_array(strtolower($moduleAdministrationDocument), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonConfigurationOrganismePeer::MODULE_ADMINISTRATION_DOCUMENT, $moduleAdministrationDocument, $comparison);
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConfigurationOrganismeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonConfigurationOrganismePeer::ORGANISME, $commonOrganisme->getAcronyme(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConfigurationOrganismePeer::ORGANISME, $commonOrganisme->toKeyValue('PrimaryKey', 'Acronyme'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonConfigurationOrganisme $commonConfigurationOrganisme Object to remove from the list of results
     *
     * @return CommonConfigurationOrganismeQuery The current query, for fluid interface
     */
    public function prune($commonConfigurationOrganisme = null)
    {
        if ($commonConfigurationOrganisme) {
            $this->addUsingAlias(CommonConfigurationOrganismePeer::ORGANISME, $commonConfigurationOrganisme->getOrganisme(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
