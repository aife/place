<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonArchiveArcade;
use Application\Propel\Mpe\CommonArchiveArcadePeer;
use Application\Propel\Mpe\CommonArchiveArcadeQuery;
use Application\Propel\Mpe\CommonConsultationArchiveArcade;
use Application\Propel\Mpe\CommonConsultationArchiveArcadeQuery;

/**
 * Base class that represents a row from the 'archive_arcade' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonArchiveArcade extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonArchiveArcadePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonArchiveArcadePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the annee field.
     * @var        int
     */
    protected $annee;

    /**
     * The value for the num_semaine field.
     * @var        int
     */
    protected $num_semaine;

    /**
     * The value for the poids_archive field.
     * @var        int
     */
    protected $poids_archive;

    /**
     * The value for the chemin_fichier field.
     * @var        string
     */
    protected $chemin_fichier;

    /**
     * The value for the date_envoi_debut field.
     * @var        string
     */
    protected $date_envoi_debut;

    /**
     * The value for the date_envoi_fin field.
     * @var        string
     */
    protected $date_envoi_fin;

    /**
     * The value for the status_transmission field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $status_transmission;

    /**
     * The value for the erreur field.
     * @var        string
     */
    protected $erreur;

    /**
     * @var        PropelObjectCollection|CommonConsultationArchiveArcade[] Collection to store aggregation of CommonConsultationArchiveArcade objects.
     */
    protected $collCommonConsultationArchiveArcades;
    protected $collCommonConsultationArchiveArcadesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationArchiveArcadesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->status_transmission = false;
    }

    /**
     * Initializes internal state of BaseCommonArchiveArcade object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [annee] column value.
     *
     * @return int
     */
    public function getAnnee()
    {

        return $this->annee;
    }

    /**
     * Get the [num_semaine] column value.
     *
     * @return int
     */
    public function getNumSemaine()
    {

        return $this->num_semaine;
    }

    /**
     * Get the [poids_archive] column value.
     *
     * @return int
     */
    public function getPoidsArchive()
    {

        return $this->poids_archive;
    }

    /**
     * Get the [chemin_fichier] column value.
     *
     * @return string
     */
    public function getCheminFichier()
    {

        return $this->chemin_fichier;
    }

    /**
     * Get the [optionally formatted] temporal [date_envoi_debut] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateEnvoiDebut($format = 'Y-m-d H:i:s')
    {
        if ($this->date_envoi_debut === null) {
            return null;
        }

        if ($this->date_envoi_debut === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_envoi_debut);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_envoi_debut, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_envoi_fin] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateEnvoiFin($format = 'Y-m-d H:i:s')
    {
        if ($this->date_envoi_fin === null) {
            return null;
        }

        if ($this->date_envoi_fin === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_envoi_fin);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_envoi_fin, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [status_transmission] column value.
     *
     * @return boolean
     */
    public function getStatusTransmission()
    {

        return $this->status_transmission;
    }

    /**
     * Get the [erreur] column value.
     *
     * @return string
     */
    public function getErreur()
    {

        return $this->erreur;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonArchiveArcadePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [annee] column.
     *
     * @param int $v new value
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function setAnnee($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->annee !== $v) {
            $this->annee = $v;
            $this->modifiedColumns[] = CommonArchiveArcadePeer::ANNEE;
        }


        return $this;
    } // setAnnee()

    /**
     * Set the value of [num_semaine] column.
     *
     * @param int $v new value
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function setNumSemaine($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->num_semaine !== $v) {
            $this->num_semaine = $v;
            $this->modifiedColumns[] = CommonArchiveArcadePeer::NUM_SEMAINE;
        }


        return $this;
    } // setNumSemaine()

    /**
     * Set the value of [poids_archive] column.
     *
     * @param int $v new value
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function setPoidsArchive($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->poids_archive !== $v) {
            $this->poids_archive = $v;
            $this->modifiedColumns[] = CommonArchiveArcadePeer::POIDS_ARCHIVE;
        }


        return $this;
    } // setPoidsArchive()

    /**
     * Set the value of [chemin_fichier] column.
     *
     * @param string $v new value
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function setCheminFichier($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_fichier !== $v) {
            $this->chemin_fichier = $v;
            $this->modifiedColumns[] = CommonArchiveArcadePeer::CHEMIN_FICHIER;
        }


        return $this;
    } // setCheminFichier()

    /**
     * Sets the value of [date_envoi_debut] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function setDateEnvoiDebut($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_envoi_debut !== null || $dt !== null) {
            $currentDateAsString = ($this->date_envoi_debut !== null && $tmpDt = new DateTime($this->date_envoi_debut)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_envoi_debut = $newDateAsString;
                $this->modifiedColumns[] = CommonArchiveArcadePeer::DATE_ENVOI_DEBUT;
            }
        } // if either are not null


        return $this;
    } // setDateEnvoiDebut()

    /**
     * Sets the value of [date_envoi_fin] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function setDateEnvoiFin($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_envoi_fin !== null || $dt !== null) {
            $currentDateAsString = ($this->date_envoi_fin !== null && $tmpDt = new DateTime($this->date_envoi_fin)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_envoi_fin = $newDateAsString;
                $this->modifiedColumns[] = CommonArchiveArcadePeer::DATE_ENVOI_FIN;
            }
        } // if either are not null


        return $this;
    } // setDateEnvoiFin()

    /**
     * Sets the value of the [status_transmission] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function setStatusTransmission($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->status_transmission !== $v) {
            $this->status_transmission = $v;
            $this->modifiedColumns[] = CommonArchiveArcadePeer::STATUS_TRANSMISSION;
        }


        return $this;
    } // setStatusTransmission()

    /**
     * Set the value of [erreur] column.
     *
     * @param string $v new value
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function setErreur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->erreur !== $v) {
            $this->erreur = $v;
            $this->modifiedColumns[] = CommonArchiveArcadePeer::ERREUR;
        }


        return $this;
    } // setErreur()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->status_transmission !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->annee = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->num_semaine = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->poids_archive = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->chemin_fichier = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->date_envoi_debut = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->date_envoi_fin = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->status_transmission = ($row[$startcol + 7] !== null) ? (boolean) $row[$startcol + 7] : null;
            $this->erreur = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 9; // 9 = CommonArchiveArcadePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonArchiveArcade object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonArchiveArcadePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonArchiveArcadePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCommonConsultationArchiveArcades = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonArchiveArcadePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonArchiveArcadeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonArchiveArcadePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonArchiveArcadePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonConsultationArchiveArcadesScheduledForDeletion !== null) {
                if (!$this->commonConsultationArchiveArcadesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationArchiveArcadesScheduledForDeletion as $commonConsultationArchiveArcade) {
                        // need to save related object because we set the relation to null
                        $commonConsultationArchiveArcade->save($con);
                    }
                    $this->commonConsultationArchiveArcadesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultationArchiveArcades !== null) {
                foreach ($this->collCommonConsultationArchiveArcades as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonArchiveArcadePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonArchiveArcadePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonArchiveArcadePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonArchiveArcadePeer::ANNEE)) {
            $modifiedColumns[':p' . $index++]  = '`annee`';
        }
        if ($this->isColumnModified(CommonArchiveArcadePeer::NUM_SEMAINE)) {
            $modifiedColumns[':p' . $index++]  = '`num_semaine`';
        }
        if ($this->isColumnModified(CommonArchiveArcadePeer::POIDS_ARCHIVE)) {
            $modifiedColumns[':p' . $index++]  = '`poids_archive`';
        }
        if ($this->isColumnModified(CommonArchiveArcadePeer::CHEMIN_FICHIER)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_fichier`';
        }
        if ($this->isColumnModified(CommonArchiveArcadePeer::DATE_ENVOI_DEBUT)) {
            $modifiedColumns[':p' . $index++]  = '`date_envoi_debut`';
        }
        if ($this->isColumnModified(CommonArchiveArcadePeer::DATE_ENVOI_FIN)) {
            $modifiedColumns[':p' . $index++]  = '`date_envoi_fin`';
        }
        if ($this->isColumnModified(CommonArchiveArcadePeer::STATUS_TRANSMISSION)) {
            $modifiedColumns[':p' . $index++]  = '`status_transmission`';
        }
        if ($this->isColumnModified(CommonArchiveArcadePeer::ERREUR)) {
            $modifiedColumns[':p' . $index++]  = '`erreur`';
        }

        $sql = sprintf(
            'INSERT INTO `archive_arcade` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`annee`':
                        $stmt->bindValue($identifier, $this->annee, PDO::PARAM_INT);
                        break;
                    case '`num_semaine`':
                        $stmt->bindValue($identifier, $this->num_semaine, PDO::PARAM_INT);
                        break;
                    case '`poids_archive`':
                        $stmt->bindValue($identifier, $this->poids_archive, PDO::PARAM_INT);
                        break;
                    case '`chemin_fichier`':
                        $stmt->bindValue($identifier, $this->chemin_fichier, PDO::PARAM_STR);
                        break;
                    case '`date_envoi_debut`':
                        $stmt->bindValue($identifier, $this->date_envoi_debut, PDO::PARAM_STR);
                        break;
                    case '`date_envoi_fin`':
                        $stmt->bindValue($identifier, $this->date_envoi_fin, PDO::PARAM_STR);
                        break;
                    case '`status_transmission`':
                        $stmt->bindValue($identifier, (int) $this->status_transmission, PDO::PARAM_INT);
                        break;
                    case '`erreur`':
                        $stmt->bindValue($identifier, $this->erreur, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonArchiveArcadePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonConsultationArchiveArcades !== null) {
                    foreach ($this->collCommonConsultationArchiveArcades as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonArchiveArcadePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getAnnee();
                break;
            case 2:
                return $this->getNumSemaine();
                break;
            case 3:
                return $this->getPoidsArchive();
                break;
            case 4:
                return $this->getCheminFichier();
                break;
            case 5:
                return $this->getDateEnvoiDebut();
                break;
            case 6:
                return $this->getDateEnvoiFin();
                break;
            case 7:
                return $this->getStatusTransmission();
                break;
            case 8:
                return $this->getErreur();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonArchiveArcade'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonArchiveArcade'][$this->getPrimaryKey()] = true;
        $keys = CommonArchiveArcadePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getAnnee(),
            $keys[2] => $this->getNumSemaine(),
            $keys[3] => $this->getPoidsArchive(),
            $keys[4] => $this->getCheminFichier(),
            $keys[5] => $this->getDateEnvoiDebut(),
            $keys[6] => $this->getDateEnvoiFin(),
            $keys[7] => $this->getStatusTransmission(),
            $keys[8] => $this->getErreur(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collCommonConsultationArchiveArcades) {
                $result['CommonConsultationArchiveArcades'] = $this->collCommonConsultationArchiveArcades->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonArchiveArcadePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setAnnee($value);
                break;
            case 2:
                $this->setNumSemaine($value);
                break;
            case 3:
                $this->setPoidsArchive($value);
                break;
            case 4:
                $this->setCheminFichier($value);
                break;
            case 5:
                $this->setDateEnvoiDebut($value);
                break;
            case 6:
                $this->setDateEnvoiFin($value);
                break;
            case 7:
                $this->setStatusTransmission($value);
                break;
            case 8:
                $this->setErreur($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonArchiveArcadePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setAnnee($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNumSemaine($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setPoidsArchive($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setCheminFichier($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDateEnvoiDebut($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setDateEnvoiFin($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setStatusTransmission($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setErreur($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonArchiveArcadePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonArchiveArcadePeer::ID)) $criteria->add(CommonArchiveArcadePeer::ID, $this->id);
        if ($this->isColumnModified(CommonArchiveArcadePeer::ANNEE)) $criteria->add(CommonArchiveArcadePeer::ANNEE, $this->annee);
        if ($this->isColumnModified(CommonArchiveArcadePeer::NUM_SEMAINE)) $criteria->add(CommonArchiveArcadePeer::NUM_SEMAINE, $this->num_semaine);
        if ($this->isColumnModified(CommonArchiveArcadePeer::POIDS_ARCHIVE)) $criteria->add(CommonArchiveArcadePeer::POIDS_ARCHIVE, $this->poids_archive);
        if ($this->isColumnModified(CommonArchiveArcadePeer::CHEMIN_FICHIER)) $criteria->add(CommonArchiveArcadePeer::CHEMIN_FICHIER, $this->chemin_fichier);
        if ($this->isColumnModified(CommonArchiveArcadePeer::DATE_ENVOI_DEBUT)) $criteria->add(CommonArchiveArcadePeer::DATE_ENVOI_DEBUT, $this->date_envoi_debut);
        if ($this->isColumnModified(CommonArchiveArcadePeer::DATE_ENVOI_FIN)) $criteria->add(CommonArchiveArcadePeer::DATE_ENVOI_FIN, $this->date_envoi_fin);
        if ($this->isColumnModified(CommonArchiveArcadePeer::STATUS_TRANSMISSION)) $criteria->add(CommonArchiveArcadePeer::STATUS_TRANSMISSION, $this->status_transmission);
        if ($this->isColumnModified(CommonArchiveArcadePeer::ERREUR)) $criteria->add(CommonArchiveArcadePeer::ERREUR, $this->erreur);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonArchiveArcadePeer::DATABASE_NAME);
        $criteria->add(CommonArchiveArcadePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonArchiveArcade (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setAnnee($this->getAnnee());
        $copyObj->setNumSemaine($this->getNumSemaine());
        $copyObj->setPoidsArchive($this->getPoidsArchive());
        $copyObj->setCheminFichier($this->getCheminFichier());
        $copyObj->setDateEnvoiDebut($this->getDateEnvoiDebut());
        $copyObj->setDateEnvoiFin($this->getDateEnvoiFin());
        $copyObj->setStatusTransmission($this->getStatusTransmission());
        $copyObj->setErreur($this->getErreur());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonConsultationArchiveArcades() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultationArchiveArcade($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonArchiveArcade Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonArchiveArcadePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonArchiveArcadePeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonConsultationArchiveArcade' == $relationName) {
            $this->initCommonConsultationArchiveArcades();
        }
    }

    /**
     * Clears out the collCommonConsultationArchiveArcades collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonArchiveArcade The current object (for fluent API support)
     * @see        addCommonConsultationArchiveArcades()
     */
    public function clearCommonConsultationArchiveArcades()
    {
        $this->collCommonConsultationArchiveArcades = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationArchiveArcadesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultationArchiveArcades collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultationArchiveArcades($v = true)
    {
        $this->collCommonConsultationArchiveArcadesPartial = $v;
    }

    /**
     * Initializes the collCommonConsultationArchiveArcades collection.
     *
     * By default this just sets the collCommonConsultationArchiveArcades collection to an empty array (like clearcollCommonConsultationArchiveArcades());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultationArchiveArcades($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultationArchiveArcades && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultationArchiveArcades = new PropelObjectCollection();
        $this->collCommonConsultationArchiveArcades->setModel('CommonConsultationArchiveArcade');
    }

    /**
     * Gets an array of CommonConsultationArchiveArcade objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonArchiveArcade is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultationArchiveArcade[] List of CommonConsultationArchiveArcade objects
     * @throws PropelException
     */
    public function getCommonConsultationArchiveArcades($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationArchiveArcadesPartial && !$this->isNew();
        if (null === $this->collCommonConsultationArchiveArcades || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationArchiveArcades) {
                // return empty collection
                $this->initCommonConsultationArchiveArcades();
            } else {
                $collCommonConsultationArchiveArcades = CommonConsultationArchiveArcadeQuery::create(null, $criteria)
                    ->filterByCommonArchiveArcade($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationArchiveArcadesPartial && count($collCommonConsultationArchiveArcades)) {
                      $this->initCommonConsultationArchiveArcades(false);

                      foreach ($collCommonConsultationArchiveArcades as $obj) {
                        if (false == $this->collCommonConsultationArchiveArcades->contains($obj)) {
                          $this->collCommonConsultationArchiveArcades->append($obj);
                        }
                      }

                      $this->collCommonConsultationArchiveArcadesPartial = true;
                    }

                    $collCommonConsultationArchiveArcades->getInternalIterator()->rewind();

                    return $collCommonConsultationArchiveArcades;
                }

                if ($partial && $this->collCommonConsultationArchiveArcades) {
                    foreach ($this->collCommonConsultationArchiveArcades as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultationArchiveArcades[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultationArchiveArcades = $collCommonConsultationArchiveArcades;
                $this->collCommonConsultationArchiveArcadesPartial = false;
            }
        }

        return $this->collCommonConsultationArchiveArcades;
    }

    /**
     * Sets a collection of CommonConsultationArchiveArcade objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultationArchiveArcades A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function setCommonConsultationArchiveArcades(PropelCollection $commonConsultationArchiveArcades, PropelPDO $con = null)
    {
        $commonConsultationArchiveArcadesToDelete = $this->getCommonConsultationArchiveArcades(new Criteria(), $con)->diff($commonConsultationArchiveArcades);


        $this->commonConsultationArchiveArcadesScheduledForDeletion = $commonConsultationArchiveArcadesToDelete;

        foreach ($commonConsultationArchiveArcadesToDelete as $commonConsultationArchiveArcadeRemoved) {
            $commonConsultationArchiveArcadeRemoved->setCommonArchiveArcade(null);
        }

        $this->collCommonConsultationArchiveArcades = null;
        foreach ($commonConsultationArchiveArcades as $commonConsultationArchiveArcade) {
            $this->addCommonConsultationArchiveArcade($commonConsultationArchiveArcade);
        }

        $this->collCommonConsultationArchiveArcades = $commonConsultationArchiveArcades;
        $this->collCommonConsultationArchiveArcadesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultationArchiveArcade objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultationArchiveArcade objects.
     * @throws PropelException
     */
    public function countCommonConsultationArchiveArcades(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationArchiveArcadesPartial && !$this->isNew();
        if (null === $this->collCommonConsultationArchiveArcades || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationArchiveArcades) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultationArchiveArcades());
            }
            $query = CommonConsultationArchiveArcadeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonArchiveArcade($this)
                ->count($con);
        }

        return count($this->collCommonConsultationArchiveArcades);
    }

    /**
     * Method called to associate a CommonConsultationArchiveArcade object to this object
     * through the CommonConsultationArchiveArcade foreign key attribute.
     *
     * @param   CommonConsultationArchiveArcade $l CommonConsultationArchiveArcade
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function addCommonConsultationArchiveArcade(CommonConsultationArchiveArcade $l)
    {
        if ($this->collCommonConsultationArchiveArcades === null) {
            $this->initCommonConsultationArchiveArcades();
            $this->collCommonConsultationArchiveArcadesPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultationArchiveArcades->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultationArchiveArcade($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultationArchiveArcade $commonConsultationArchiveArcade The commonConsultationArchiveArcade object to add.
     */
    protected function doAddCommonConsultationArchiveArcade($commonConsultationArchiveArcade)
    {
        $this->collCommonConsultationArchiveArcades[]= $commonConsultationArchiveArcade;
        $commonConsultationArchiveArcade->setCommonArchiveArcade($this);
    }

    /**
     * @param	CommonConsultationArchiveArcade $commonConsultationArchiveArcade The commonConsultationArchiveArcade object to remove.
     * @return CommonArchiveArcade The current object (for fluent API support)
     */
    public function removeCommonConsultationArchiveArcade($commonConsultationArchiveArcade)
    {
        if ($this->getCommonConsultationArchiveArcades()->contains($commonConsultationArchiveArcade)) {
            $this->collCommonConsultationArchiveArcades->remove($this->collCommonConsultationArchiveArcades->search($commonConsultationArchiveArcade));
            if (null === $this->commonConsultationArchiveArcadesScheduledForDeletion) {
                $this->commonConsultationArchiveArcadesScheduledForDeletion = clone $this->collCommonConsultationArchiveArcades;
                $this->commonConsultationArchiveArcadesScheduledForDeletion->clear();
            }
            $this->commonConsultationArchiveArcadesScheduledForDeletion[]= $commonConsultationArchiveArcade;
            $commonConsultationArchiveArcade->setCommonArchiveArcade(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonArchiveArcade is new, it will return
     * an empty collection; or if this CommonArchiveArcade has previously
     * been saved, it will retrieve related CommonConsultationArchiveArcades from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonArchiveArcade.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultationArchiveArcade[] List of CommonConsultationArchiveArcade objects
     */
    public function getCommonConsultationArchiveArcadesJoinCommonConsultationArchive($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationArchiveArcadeQuery::create(null, $criteria);
        $query->joinWith('CommonConsultationArchive', $join_behavior);

        return $this->getCommonConsultationArchiveArcades($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->annee = null;
        $this->num_semaine = null;
        $this->poids_archive = null;
        $this->chemin_fichier = null;
        $this->date_envoi_debut = null;
        $this->date_envoi_fin = null;
        $this->status_transmission = null;
        $this->erreur = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonConsultationArchiveArcades) {
                foreach ($this->collCommonConsultationArchiveArcades as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonConsultationArchiveArcades instanceof PropelCollection) {
            $this->collCommonConsultationArchiveArcades->clearIterator();
        }
        $this->collCommonConsultationArchiveArcades = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonArchiveArcadePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
