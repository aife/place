<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Propel\Mpe\CommonTDumeContextePeer;
use Application\Propel\Mpe\CommonTDumeContexteQuery;
use Application\Propel\Mpe\CommonTDumeNumero;

/**
 * Base class that represents a query for the 't_dume_contexte' table.
 *
 *
 *
 * @method CommonTDumeContexteQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTDumeContexteQuery orderByRefConsultation($order = Criteria::ASC) Order by the ref_consultation column
 * @method CommonTDumeContexteQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTDumeContexteQuery orderByContexteLtDumeId($order = Criteria::ASC) Order by the contexte_lt_dume_id column
 * @method CommonTDumeContexteQuery orderByTypeDume($order = Criteria::ASC) Order by the type_dume column
 * @method CommonTDumeContexteQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method CommonTDumeContexteQuery orderByIsStandard($order = Criteria::ASC) Order by the is_standard column
 * @method CommonTDumeContexteQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTDumeContexteQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonTDumeContexteQuery orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 *
 * @method CommonTDumeContexteQuery groupById() Group by the id column
 * @method CommonTDumeContexteQuery groupByRefConsultation() Group by the ref_consultation column
 * @method CommonTDumeContexteQuery groupByOrganisme() Group by the organisme column
 * @method CommonTDumeContexteQuery groupByContexteLtDumeId() Group by the contexte_lt_dume_id column
 * @method CommonTDumeContexteQuery groupByTypeDume() Group by the type_dume column
 * @method CommonTDumeContexteQuery groupByStatus() Group by the status column
 * @method CommonTDumeContexteQuery groupByIsStandard() Group by the is_standard column
 * @method CommonTDumeContexteQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTDumeContexteQuery groupByDateModification() Group by the date_modification column
 * @method CommonTDumeContexteQuery groupByConsultationId() Group by the consultation_id column
 *
 * @method CommonTDumeContexteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTDumeContexteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTDumeContexteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTDumeContexteQuery leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonTDumeContexteQuery rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonTDumeContexteQuery innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonTDumeContexteQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonTDumeContexteQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonTDumeContexteQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonTDumeContexteQuery leftJoinCommonTCandidature($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCandidature relation
 * @method CommonTDumeContexteQuery rightJoinCommonTCandidature($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCandidature relation
 * @method CommonTDumeContexteQuery innerJoinCommonTCandidature($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCandidature relation
 *
 * @method CommonTDumeContexteQuery leftJoinCommonTDumeNumero($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTDumeNumero relation
 * @method CommonTDumeContexteQuery rightJoinCommonTDumeNumero($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTDumeNumero relation
 * @method CommonTDumeContexteQuery innerJoinCommonTDumeNumero($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTDumeNumero relation
 *
 * @method CommonTDumeContexte findOne(PropelPDO $con = null) Return the first CommonTDumeContexte matching the query
 * @method CommonTDumeContexte findOneOrCreate(PropelPDO $con = null) Return the first CommonTDumeContexte matching the query, or a new CommonTDumeContexte object populated from the query conditions when no match is found
 *
 * @method CommonTDumeContexte findOneByRefConsultation(int $ref_consultation) Return the first CommonTDumeContexte filtered by the ref_consultation column
 * @method CommonTDumeContexte findOneByOrganisme(string $organisme) Return the first CommonTDumeContexte filtered by the organisme column
 * @method CommonTDumeContexte findOneByContexteLtDumeId(string $contexte_lt_dume_id) Return the first CommonTDumeContexte filtered by the contexte_lt_dume_id column
 * @method CommonTDumeContexte findOneByTypeDume(string $type_dume) Return the first CommonTDumeContexte filtered by the type_dume column
 * @method CommonTDumeContexte findOneByStatus(int $status) Return the first CommonTDumeContexte filtered by the status column
 * @method CommonTDumeContexte findOneByIsStandard(boolean $is_standard) Return the first CommonTDumeContexte filtered by the is_standard column
 * @method CommonTDumeContexte findOneByDateCreation(string $date_creation) Return the first CommonTDumeContexte filtered by the date_creation column
 * @method CommonTDumeContexte findOneByDateModification(string $date_modification) Return the first CommonTDumeContexte filtered by the date_modification column
 * @method CommonTDumeContexte findOneByConsultationId(int $consultation_id) Return the first CommonTDumeContexte filtered by the consultation_id column
 *
 * @method array findById(int $id) Return CommonTDumeContexte objects filtered by the id column
 * @method array findByRefConsultation(int $ref_consultation) Return CommonTDumeContexte objects filtered by the ref_consultation column
 * @method array findByOrganisme(string $organisme) Return CommonTDumeContexte objects filtered by the organisme column
 * @method array findByContexteLtDumeId(string $contexte_lt_dume_id) Return CommonTDumeContexte objects filtered by the contexte_lt_dume_id column
 * @method array findByTypeDume(string $type_dume) Return CommonTDumeContexte objects filtered by the type_dume column
 * @method array findByStatus(int $status) Return CommonTDumeContexte objects filtered by the status column
 * @method array findByIsStandard(boolean $is_standard) Return CommonTDumeContexte objects filtered by the is_standard column
 * @method array findByDateCreation(string $date_creation) Return CommonTDumeContexte objects filtered by the date_creation column
 * @method array findByDateModification(string $date_modification) Return CommonTDumeContexte objects filtered by the date_modification column
 * @method array findByConsultationId(int $consultation_id) Return CommonTDumeContexte objects filtered by the consultation_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDumeContexteQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTDumeContexteQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTDumeContexte', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTDumeContexteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTDumeContexteQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTDumeContexteQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTDumeContexteQuery) {
            return $criteria;
        }
        $query = new CommonTDumeContexteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTDumeContexte|CommonTDumeContexte[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTDumeContextePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTDumeContextePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDumeContexte A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTDumeContexte A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `ref_consultation`, `organisme`, `contexte_lt_dume_id`, `type_dume`, `status`, `is_standard`, `date_creation`, `date_modification`, `consultation_id` FROM `t_dume_contexte` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTDumeContexte();
            $obj->hydrate($row);
            CommonTDumeContextePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTDumeContexte|CommonTDumeContexte[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTDumeContexte[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTDumeContextePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTDumeContextePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeContextePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the ref_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByRefConsultation(1234); // WHERE ref_consultation = 1234
     * $query->filterByRefConsultation(array(12, 34)); // WHERE ref_consultation IN (12, 34)
     * $query->filterByRefConsultation(array('min' => 12)); // WHERE ref_consultation >= 12
     * $query->filterByRefConsultation(array('max' => 12)); // WHERE ref_consultation <= 12
     * </code>
     *
     * @param     mixed $refConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterByRefConsultation($refConsultation = null, $comparison = null)
    {
        if (is_array($refConsultation)) {
            $useMinMax = false;
            if (isset($refConsultation['min'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::REF_CONSULTATION, $refConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($refConsultation['max'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::REF_CONSULTATION, $refConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeContextePeer::REF_CONSULTATION, $refConsultation, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDumeContextePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the contexte_lt_dume_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContexteLtDumeId('fooValue');   // WHERE contexte_lt_dume_id = 'fooValue'
     * $query->filterByContexteLtDumeId('%fooValue%'); // WHERE contexte_lt_dume_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contexteLtDumeId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterByContexteLtDumeId($contexteLtDumeId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contexteLtDumeId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $contexteLtDumeId)) {
                $contexteLtDumeId = str_replace('*', '%', $contexteLtDumeId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDumeContextePeer::CONTEXTE_LT_DUME_ID, $contexteLtDumeId, $comparison);
    }

    /**
     * Filter the query on the type_dume column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeDume('fooValue');   // WHERE type_dume = 'fooValue'
     * $query->filterByTypeDume('%fooValue%'); // WHERE type_dume LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeDume The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterByTypeDume($typeDume = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeDume)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeDume)) {
                $typeDume = str_replace('*', '%', $typeDume);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTDumeContextePeer::TYPE_DUME, $typeDume, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus(1234); // WHERE status = 1234
     * $query->filterByStatus(array(12, 34)); // WHERE status IN (12, 34)
     * $query->filterByStatus(array('min' => 12)); // WHERE status >= 12
     * $query->filterByStatus(array('max' => 12)); // WHERE status <= 12
     * </code>
     *
     * @param     mixed $status The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (is_array($status)) {
            $useMinMax = false;
            if (isset($status['min'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::STATUS, $status['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($status['max'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::STATUS, $status['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeContextePeer::STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the is_standard column
     *
     * Example usage:
     * <code>
     * $query->filterByIsStandard(true); // WHERE is_standard = true
     * $query->filterByIsStandard('yes'); // WHERE is_standard = true
     * </code>
     *
     * @param     boolean|string $isStandard The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterByIsStandard($isStandard = null, $comparison = null)
    {
        if (is_string($isStandard)) {
            $isStandard = in_array(strtolower($isStandard), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonTDumeContextePeer::IS_STANDARD, $isStandard, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeContextePeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('2011-03-14'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification('now'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification(array('max' => 'yesterday')); // WHERE date_modification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (is_array($dateModification)) {
            $useMinMax = false;
            if (isset($dateModification['min'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::DATE_MODIFICATION, $dateModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModification['max'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::DATE_MODIFICATION, $dateModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeContextePeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultation()
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonTDumeContextePeer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTDumeContextePeer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTDumeContexteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonTDumeContextePeer::CONSULTATION_ID, $commonConsultation->getId(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTDumeContextePeer::CONSULTATION_ID, $commonConsultation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTDumeContexteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonTDumeContextePeer::ORGANISME, $commonOrganisme->getAcronyme(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTDumeContextePeer::ORGANISME, $commonOrganisme->toKeyValue('PrimaryKey', 'Acronyme'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Filter the query by a related CommonTCandidature object
     *
     * @param   CommonTCandidature|PropelObjectCollection $commonTCandidature  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTDumeContexteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCandidature($commonTCandidature, $comparison = null)
    {
        if ($commonTCandidature instanceof CommonTCandidature) {
            return $this
                ->addUsingAlias(CommonTDumeContextePeer::ID, $commonTCandidature->getIdDumeContexte(), $comparison);
        } elseif ($commonTCandidature instanceof PropelObjectCollection) {
            return $this
                ->useCommonTCandidatureQuery()
                ->filterByPrimaryKeys($commonTCandidature->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTCandidature() only accepts arguments of type CommonTCandidature or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCandidature relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function joinCommonTCandidature($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCandidature');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCandidature');
        }

        return $this;
    }

    /**
     * Use the CommonTCandidature relation CommonTCandidature object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCandidatureQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCandidatureQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTCandidature($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCandidature', '\Application\Propel\Mpe\CommonTCandidatureQuery');
    }

    /**
     * Filter the query by a related CommonTDumeNumero object
     *
     * @param   CommonTDumeNumero|PropelObjectCollection $commonTDumeNumero  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTDumeContexteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTDumeNumero($commonTDumeNumero, $comparison = null)
    {
        if ($commonTDumeNumero instanceof CommonTDumeNumero) {
            return $this
                ->addUsingAlias(CommonTDumeContextePeer::ID, $commonTDumeNumero->getIdDumeContexte(), $comparison);
        } elseif ($commonTDumeNumero instanceof PropelObjectCollection) {
            return $this
                ->useCommonTDumeNumeroQuery()
                ->filterByPrimaryKeys($commonTDumeNumero->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTDumeNumero() only accepts arguments of type CommonTDumeNumero or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTDumeNumero relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function joinCommonTDumeNumero($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTDumeNumero');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTDumeNumero');
        }

        return $this;
    }

    /**
     * Use the CommonTDumeNumero relation CommonTDumeNumero object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTDumeNumeroQuery A secondary query class using the current class as primary query
     */
    public function useCommonTDumeNumeroQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTDumeNumero($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTDumeNumero', '\Application\Propel\Mpe\CommonTDumeNumeroQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTDumeContexte $commonTDumeContexte Object to remove from the list of results
     *
     * @return CommonTDumeContexteQuery The current query, for fluid interface
     */
    public function prune($commonTDumeContexte = null)
    {
        if ($commonTDumeContexte) {
            $this->addUsingAlias(CommonTDumeContextePeer::ID, $commonTDumeContexte->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
