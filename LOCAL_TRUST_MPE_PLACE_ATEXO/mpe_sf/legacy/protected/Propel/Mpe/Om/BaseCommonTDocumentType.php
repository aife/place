<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDocumentEntreprise;
use Application\Propel\Mpe\CommonTDocumentEntrepriseQuery;
use Application\Propel\Mpe\CommonTDocumentType;
use Application\Propel\Mpe\CommonTDocumentTypePeer;
use Application\Propel\Mpe\CommonTDocumentTypeQuery;

/**
 * Base class that represents a row from the 't_document_type' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDocumentType extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTDocumentTypePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTDocumentTypePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_type_document field.
     * @var        int
     */
    protected $id_type_document;

    /**
     * The value for the nom_type_document field.
     * @var        string
     */
    protected $nom_type_document;

    /**
     * The value for the code field.
     * @var        string
     */
    protected $code;

    /**
     * The value for the type_doc_entreprise_etablissement field.
     * @var        string
     */
    protected $type_doc_entreprise_etablissement;

    /**
     * The value for the uri field.
     * @var        string
     */
    protected $uri;

    /**
     * The value for the params_uri field.
     * @var        string
     */
    protected $params_uri;

    /**
     * The value for the class_name field.
     * @var        string
     */
    protected $class_name;

    /**
     * The value for the nature_document field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $nature_document;

    /**
     * The value for the type_retour_ws field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $type_retour_ws;

    /**
     * The value for the synchro_actif field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $synchro_actif;

    /**
     * The value for the message_desactivation_synchro field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $message_desactivation_synchro;

    /**
     * The value for the afficher_type_doc field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $afficher_type_doc;

    /**
     * @var        PropelObjectCollection|CommonTDocumentEntreprise[] Collection to store aggregation of CommonTDocumentEntreprise objects.
     */
    protected $collCommonTDocumentEntreprises;
    protected $collCommonTDocumentEntreprisesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTDocumentEntreprisesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->nature_document = '1';
        $this->type_retour_ws = '1';
        $this->synchro_actif = '1';
        $this->message_desactivation_synchro = '';
        $this->afficher_type_doc = '1';
    }

    /**
     * Initializes internal state of BaseCommonTDocumentType object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_type_document] column value.
     *
     * @return int
     */
    public function getIdTypeDocument()
    {

        return $this->id_type_document;
    }

    /**
     * Get the [nom_type_document] column value.
     *
     * @return string
     */
    public function getNomTypeDocument()
    {

        return $this->nom_type_document;
    }

    /**
     * Get the [code] column value.
     *
     * @return string
     */
    public function getCode()
    {

        return $this->code;
    }

    /**
     * Get the [type_doc_entreprise_etablissement] column value.
     *
     * @return string
     */
    public function getTypeDocEntrepriseEtablissement()
    {

        return $this->type_doc_entreprise_etablissement;
    }

    /**
     * Get the [uri] column value.
     *
     * @return string
     */
    public function getUri()
    {

        return $this->uri;
    }

    /**
     * Get the [params_uri] column value.
     *
     * @return string
     */
    public function getParamsUri()
    {

        return $this->params_uri;
    }

    /**
     * Get the [class_name] column value.
     *
     * @return string
     */
    public function getClassName()
    {

        return $this->class_name;
    }

    /**
     * Get the [nature_document] column value.
     *
     * @return string
     */
    public function getNatureDocument()
    {

        return $this->nature_document;
    }

    /**
     * Get the [type_retour_ws] column value.
     *
     * @return string
     */
    public function getTypeRetourWs()
    {

        return $this->type_retour_ws;
    }

    /**
     * Get the [synchro_actif] column value.
     *
     * @return string
     */
    public function getSynchroActif()
    {

        return $this->synchro_actif;
    }

    /**
     * Get the [message_desactivation_synchro] column value.
     *
     * @return string
     */
    public function getMessageDesactivationSynchro()
    {

        return $this->message_desactivation_synchro;
    }

    /**
     * Get the [afficher_type_doc] column value.
     *
     * @return string
     */
    public function getAfficherTypeDoc()
    {

        return $this->afficher_type_doc;
    }

    /**
     * Set the value of [id_type_document] column.
     *
     * @param int $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setIdTypeDocument($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type_document !== $v) {
            $this->id_type_document = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::ID_TYPE_DOCUMENT;
        }


        return $this;
    } // setIdTypeDocument()

    /**
     * Set the value of [nom_type_document] column.
     *
     * @param string $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setNomTypeDocument($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_type_document !== $v) {
            $this->nom_type_document = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::NOM_TYPE_DOCUMENT;
        }


        return $this;
    } // setNomTypeDocument()

    /**
     * Set the value of [code] column.
     *
     * @param string $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::CODE;
        }


        return $this;
    } // setCode()

    /**
     * Set the value of [type_doc_entreprise_etablissement] column.
     *
     * @param string $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setTypeDocEntrepriseEtablissement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_doc_entreprise_etablissement !== $v) {
            $this->type_doc_entreprise_etablissement = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::TYPE_DOC_ENTREPRISE_ETABLISSEMENT;
        }


        return $this;
    } // setTypeDocEntrepriseEtablissement()

    /**
     * Set the value of [uri] column.
     *
     * @param string $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setUri($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->uri !== $v) {
            $this->uri = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::URI;
        }


        return $this;
    } // setUri()

    /**
     * Set the value of [params_uri] column.
     *
     * @param string $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setParamsUri($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->params_uri !== $v) {
            $this->params_uri = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::PARAMS_URI;
        }


        return $this;
    } // setParamsUri()

    /**
     * Set the value of [class_name] column.
     *
     * @param string $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setClassName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->class_name !== $v) {
            $this->class_name = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::CLASS_NAME;
        }


        return $this;
    } // setClassName()

    /**
     * Set the value of [nature_document] column.
     *
     * @param string $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setNatureDocument($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nature_document !== $v) {
            $this->nature_document = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::NATURE_DOCUMENT;
        }


        return $this;
    } // setNatureDocument()

    /**
     * Set the value of [type_retour_ws] column.
     *
     * @param string $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setTypeRetourWs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_retour_ws !== $v) {
            $this->type_retour_ws = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::TYPE_RETOUR_WS;
        }


        return $this;
    } // setTypeRetourWs()

    /**
     * Set the value of [synchro_actif] column.
     *
     * @param string $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setSynchroActif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->synchro_actif !== $v) {
            $this->synchro_actif = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::SYNCHRO_ACTIF;
        }


        return $this;
    } // setSynchroActif()

    /**
     * Set the value of [message_desactivation_synchro] column.
     *
     * @param string $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setMessageDesactivationSynchro($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->message_desactivation_synchro !== $v) {
            $this->message_desactivation_synchro = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::MESSAGE_DESACTIVATION_SYNCHRO;
        }


        return $this;
    } // setMessageDesactivationSynchro()

    /**
     * Set the value of [afficher_type_doc] column.
     *
     * @param string $v new value
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setAfficherTypeDoc($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->afficher_type_doc !== $v) {
            $this->afficher_type_doc = $v;
            $this->modifiedColumns[] = CommonTDocumentTypePeer::AFFICHER_TYPE_DOC;
        }


        return $this;
    } // setAfficherTypeDoc()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->nature_document !== '1') {
                return false;
            }

            if ($this->type_retour_ws !== '1') {
                return false;
            }

            if ($this->synchro_actif !== '1') {
                return false;
            }

            if ($this->message_desactivation_synchro !== '') {
                return false;
            }

            if ($this->afficher_type_doc !== '1') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_type_document = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nom_type_document = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->code = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->type_doc_entreprise_etablissement = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->uri = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->params_uri = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->class_name = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->nature_document = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->type_retour_ws = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->synchro_actif = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->message_desactivation_synchro = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->afficher_type_doc = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 12; // 12 = CommonTDocumentTypePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTDocumentType object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTDocumentTypePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCommonTDocumentEntreprises = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTDocumentTypeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTDocumentTypePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonTDocumentEntreprisesScheduledForDeletion !== null) {
                if (!$this->commonTDocumentEntreprisesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTDocumentEntreprisesScheduledForDeletion as $commonTDocumentEntreprise) {
                        // need to save related object because we set the relation to null
                        $commonTDocumentEntreprise->save($con);
                    }
                    $this->commonTDocumentEntreprisesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTDocumentEntreprises !== null) {
                foreach ($this->collCommonTDocumentEntreprises as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTDocumentTypePeer::ID_TYPE_DOCUMENT;
        if (null !== $this->id_type_document) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTDocumentTypePeer::ID_TYPE_DOCUMENT . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_type_document`';
        }
        if ($this->isColumnModified(CommonTDocumentTypePeer::NOM_TYPE_DOCUMENT)) {
            $modifiedColumns[':p' . $index++]  = '`nom_type_document`';
        }
        if ($this->isColumnModified(CommonTDocumentTypePeer::CODE)) {
            $modifiedColumns[':p' . $index++]  = '`code`';
        }
        if ($this->isColumnModified(CommonTDocumentTypePeer::TYPE_DOC_ENTREPRISE_ETABLISSEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`type_doc_entreprise_etablissement`';
        }
        if ($this->isColumnModified(CommonTDocumentTypePeer::URI)) {
            $modifiedColumns[':p' . $index++]  = '`uri`';
        }
        if ($this->isColumnModified(CommonTDocumentTypePeer::PARAMS_URI)) {
            $modifiedColumns[':p' . $index++]  = '`params_uri`';
        }
        if ($this->isColumnModified(CommonTDocumentTypePeer::CLASS_NAME)) {
            $modifiedColumns[':p' . $index++]  = '`class_name`';
        }
        if ($this->isColumnModified(CommonTDocumentTypePeer::NATURE_DOCUMENT)) {
            $modifiedColumns[':p' . $index++]  = '`nature_document`';
        }
        if ($this->isColumnModified(CommonTDocumentTypePeer::TYPE_RETOUR_WS)) {
            $modifiedColumns[':p' . $index++]  = '`type_retour_ws`';
        }
        if ($this->isColumnModified(CommonTDocumentTypePeer::SYNCHRO_ACTIF)) {
            $modifiedColumns[':p' . $index++]  = '`synchro_actif`';
        }
        if ($this->isColumnModified(CommonTDocumentTypePeer::MESSAGE_DESACTIVATION_SYNCHRO)) {
            $modifiedColumns[':p' . $index++]  = '`message_desactivation_synchro`';
        }
        if ($this->isColumnModified(CommonTDocumentTypePeer::AFFICHER_TYPE_DOC)) {
            $modifiedColumns[':p' . $index++]  = '`afficher_type_doc`';
        }

        $sql = sprintf(
            'INSERT INTO `t_document_type` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_type_document`':
                        $stmt->bindValue($identifier, $this->id_type_document, PDO::PARAM_INT);
                        break;
                    case '`nom_type_document`':
                        $stmt->bindValue($identifier, $this->nom_type_document, PDO::PARAM_STR);
                        break;
                    case '`code`':
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case '`type_doc_entreprise_etablissement`':
                        $stmt->bindValue($identifier, $this->type_doc_entreprise_etablissement, PDO::PARAM_STR);
                        break;
                    case '`uri`':
                        $stmt->bindValue($identifier, $this->uri, PDO::PARAM_STR);
                        break;
                    case '`params_uri`':
                        $stmt->bindValue($identifier, $this->params_uri, PDO::PARAM_STR);
                        break;
                    case '`class_name`':
                        $stmt->bindValue($identifier, $this->class_name, PDO::PARAM_STR);
                        break;
                    case '`nature_document`':
                        $stmt->bindValue($identifier, $this->nature_document, PDO::PARAM_STR);
                        break;
                    case '`type_retour_ws`':
                        $stmt->bindValue($identifier, $this->type_retour_ws, PDO::PARAM_STR);
                        break;
                    case '`synchro_actif`':
                        $stmt->bindValue($identifier, $this->synchro_actif, PDO::PARAM_STR);
                        break;
                    case '`message_desactivation_synchro`':
                        $stmt->bindValue($identifier, $this->message_desactivation_synchro, PDO::PARAM_STR);
                        break;
                    case '`afficher_type_doc`':
                        $stmt->bindValue($identifier, $this->afficher_type_doc, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdTypeDocument($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonTDocumentTypePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTDocumentEntreprises !== null) {
                    foreach ($this->collCommonTDocumentEntreprises as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDocumentTypePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdTypeDocument();
                break;
            case 1:
                return $this->getNomTypeDocument();
                break;
            case 2:
                return $this->getCode();
                break;
            case 3:
                return $this->getTypeDocEntrepriseEtablissement();
                break;
            case 4:
                return $this->getUri();
                break;
            case 5:
                return $this->getParamsUri();
                break;
            case 6:
                return $this->getClassName();
                break;
            case 7:
                return $this->getNatureDocument();
                break;
            case 8:
                return $this->getTypeRetourWs();
                break;
            case 9:
                return $this->getSynchroActif();
                break;
            case 10:
                return $this->getMessageDesactivationSynchro();
                break;
            case 11:
                return $this->getAfficherTypeDoc();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTDocumentType'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTDocumentType'][$this->getPrimaryKey()] = true;
        $keys = CommonTDocumentTypePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdTypeDocument(),
            $keys[1] => $this->getNomTypeDocument(),
            $keys[2] => $this->getCode(),
            $keys[3] => $this->getTypeDocEntrepriseEtablissement(),
            $keys[4] => $this->getUri(),
            $keys[5] => $this->getParamsUri(),
            $keys[6] => $this->getClassName(),
            $keys[7] => $this->getNatureDocument(),
            $keys[8] => $this->getTypeRetourWs(),
            $keys[9] => $this->getSynchroActif(),
            $keys[10] => $this->getMessageDesactivationSynchro(),
            $keys[11] => $this->getAfficherTypeDoc(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collCommonTDocumentEntreprises) {
                $result['CommonTDocumentEntreprises'] = $this->collCommonTDocumentEntreprises->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTDocumentTypePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdTypeDocument($value);
                break;
            case 1:
                $this->setNomTypeDocument($value);
                break;
            case 2:
                $this->setCode($value);
                break;
            case 3:
                $this->setTypeDocEntrepriseEtablissement($value);
                break;
            case 4:
                $this->setUri($value);
                break;
            case 5:
                $this->setParamsUri($value);
                break;
            case 6:
                $this->setClassName($value);
                break;
            case 7:
                $this->setNatureDocument($value);
                break;
            case 8:
                $this->setTypeRetourWs($value);
                break;
            case 9:
                $this->setSynchroActif($value);
                break;
            case 10:
                $this->setMessageDesactivationSynchro($value);
                break;
            case 11:
                $this->setAfficherTypeDoc($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTDocumentTypePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdTypeDocument($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNomTypeDocument($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setCode($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTypeDocEntrepriseEtablissement($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setUri($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setParamsUri($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setClassName($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setNatureDocument($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setTypeRetourWs($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setSynchroActif($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setMessageDesactivationSynchro($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setAfficherTypeDoc($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTDocumentTypePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT)) $criteria->add(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $this->id_type_document);
        if ($this->isColumnModified(CommonTDocumentTypePeer::NOM_TYPE_DOCUMENT)) $criteria->add(CommonTDocumentTypePeer::NOM_TYPE_DOCUMENT, $this->nom_type_document);
        if ($this->isColumnModified(CommonTDocumentTypePeer::CODE)) $criteria->add(CommonTDocumentTypePeer::CODE, $this->code);
        if ($this->isColumnModified(CommonTDocumentTypePeer::TYPE_DOC_ENTREPRISE_ETABLISSEMENT)) $criteria->add(CommonTDocumentTypePeer::TYPE_DOC_ENTREPRISE_ETABLISSEMENT, $this->type_doc_entreprise_etablissement);
        if ($this->isColumnModified(CommonTDocumentTypePeer::URI)) $criteria->add(CommonTDocumentTypePeer::URI, $this->uri);
        if ($this->isColumnModified(CommonTDocumentTypePeer::PARAMS_URI)) $criteria->add(CommonTDocumentTypePeer::PARAMS_URI, $this->params_uri);
        if ($this->isColumnModified(CommonTDocumentTypePeer::CLASS_NAME)) $criteria->add(CommonTDocumentTypePeer::CLASS_NAME, $this->class_name);
        if ($this->isColumnModified(CommonTDocumentTypePeer::NATURE_DOCUMENT)) $criteria->add(CommonTDocumentTypePeer::NATURE_DOCUMENT, $this->nature_document);
        if ($this->isColumnModified(CommonTDocumentTypePeer::TYPE_RETOUR_WS)) $criteria->add(CommonTDocumentTypePeer::TYPE_RETOUR_WS, $this->type_retour_ws);
        if ($this->isColumnModified(CommonTDocumentTypePeer::SYNCHRO_ACTIF)) $criteria->add(CommonTDocumentTypePeer::SYNCHRO_ACTIF, $this->synchro_actif);
        if ($this->isColumnModified(CommonTDocumentTypePeer::MESSAGE_DESACTIVATION_SYNCHRO)) $criteria->add(CommonTDocumentTypePeer::MESSAGE_DESACTIVATION_SYNCHRO, $this->message_desactivation_synchro);
        if ($this->isColumnModified(CommonTDocumentTypePeer::AFFICHER_TYPE_DOC)) $criteria->add(CommonTDocumentTypePeer::AFFICHER_TYPE_DOC, $this->afficher_type_doc);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTDocumentTypePeer::DATABASE_NAME);
        $criteria->add(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $this->id_type_document);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdTypeDocument();
    }

    /**
     * Generic method to set the primary key (id_type_document column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdTypeDocument($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdTypeDocument();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTDocumentType (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNomTypeDocument($this->getNomTypeDocument());
        $copyObj->setCode($this->getCode());
        $copyObj->setTypeDocEntrepriseEtablissement($this->getTypeDocEntrepriseEtablissement());
        $copyObj->setUri($this->getUri());
        $copyObj->setParamsUri($this->getParamsUri());
        $copyObj->setClassName($this->getClassName());
        $copyObj->setNatureDocument($this->getNatureDocument());
        $copyObj->setTypeRetourWs($this->getTypeRetourWs());
        $copyObj->setSynchroActif($this->getSynchroActif());
        $copyObj->setMessageDesactivationSynchro($this->getMessageDesactivationSynchro());
        $copyObj->setAfficherTypeDoc($this->getAfficherTypeDoc());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTDocumentEntreprises() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTDocumentEntreprise($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdTypeDocument(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTDocumentType Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTDocumentTypePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTDocumentTypePeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTDocumentEntreprise' == $relationName) {
            $this->initCommonTDocumentEntreprises();
        }
    }

    /**
     * Clears out the collCommonTDocumentEntreprises collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTDocumentType The current object (for fluent API support)
     * @see        addCommonTDocumentEntreprises()
     */
    public function clearCommonTDocumentEntreprises()
    {
        $this->collCommonTDocumentEntreprises = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTDocumentEntreprisesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTDocumentEntreprises collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTDocumentEntreprises($v = true)
    {
        $this->collCommonTDocumentEntreprisesPartial = $v;
    }

    /**
     * Initializes the collCommonTDocumentEntreprises collection.
     *
     * By default this just sets the collCommonTDocumentEntreprises collection to an empty array (like clearcollCommonTDocumentEntreprises());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTDocumentEntreprises($overrideExisting = true)
    {
        if (null !== $this->collCommonTDocumentEntreprises && !$overrideExisting) {
            return;
        }
        $this->collCommonTDocumentEntreprises = new PropelObjectCollection();
        $this->collCommonTDocumentEntreprises->setModel('CommonTDocumentEntreprise');
    }

    /**
     * Gets an array of CommonTDocumentEntreprise objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTDocumentType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTDocumentEntreprise[] List of CommonTDocumentEntreprise objects
     * @throws PropelException
     */
    public function getCommonTDocumentEntreprises($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTDocumentEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTDocumentEntreprises || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTDocumentEntreprises) {
                // return empty collection
                $this->initCommonTDocumentEntreprises();
            } else {
                $collCommonTDocumentEntreprises = CommonTDocumentEntrepriseQuery::create(null, $criteria)
                    ->filterByCommonTDocumentType($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTDocumentEntreprisesPartial && count($collCommonTDocumentEntreprises)) {
                      $this->initCommonTDocumentEntreprises(false);

                      foreach ($collCommonTDocumentEntreprises as $obj) {
                        if (false == $this->collCommonTDocumentEntreprises->contains($obj)) {
                          $this->collCommonTDocumentEntreprises->append($obj);
                        }
                      }

                      $this->collCommonTDocumentEntreprisesPartial = true;
                    }

                    $collCommonTDocumentEntreprises->getInternalIterator()->rewind();

                    return $collCommonTDocumentEntreprises;
                }

                if ($partial && $this->collCommonTDocumentEntreprises) {
                    foreach ($this->collCommonTDocumentEntreprises as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTDocumentEntreprises[] = $obj;
                        }
                    }
                }

                $this->collCommonTDocumentEntreprises = $collCommonTDocumentEntreprises;
                $this->collCommonTDocumentEntreprisesPartial = false;
            }
        }

        return $this->collCommonTDocumentEntreprises;
    }

    /**
     * Sets a collection of CommonTDocumentEntreprise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTDocumentEntreprises A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function setCommonTDocumentEntreprises(PropelCollection $commonTDocumentEntreprises, PropelPDO $con = null)
    {
        $commonTDocumentEntreprisesToDelete = $this->getCommonTDocumentEntreprises(new Criteria(), $con)->diff($commonTDocumentEntreprises);


        $this->commonTDocumentEntreprisesScheduledForDeletion = $commonTDocumentEntreprisesToDelete;

        foreach ($commonTDocumentEntreprisesToDelete as $commonTDocumentEntrepriseRemoved) {
            $commonTDocumentEntrepriseRemoved->setCommonTDocumentType(null);
        }

        $this->collCommonTDocumentEntreprises = null;
        foreach ($commonTDocumentEntreprises as $commonTDocumentEntreprise) {
            $this->addCommonTDocumentEntreprise($commonTDocumentEntreprise);
        }

        $this->collCommonTDocumentEntreprises = $commonTDocumentEntreprises;
        $this->collCommonTDocumentEntreprisesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTDocumentEntreprise objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTDocumentEntreprise objects.
     * @throws PropelException
     */
    public function countCommonTDocumentEntreprises(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTDocumentEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTDocumentEntreprises || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTDocumentEntreprises) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTDocumentEntreprises());
            }
            $query = CommonTDocumentEntrepriseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTDocumentType($this)
                ->count($con);
        }

        return count($this->collCommonTDocumentEntreprises);
    }

    /**
     * Method called to associate a CommonTDocumentEntreprise object to this object
     * through the CommonTDocumentEntreprise foreign key attribute.
     *
     * @param   CommonTDocumentEntreprise $l CommonTDocumentEntreprise
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function addCommonTDocumentEntreprise(CommonTDocumentEntreprise $l)
    {
        if ($this->collCommonTDocumentEntreprises === null) {
            $this->initCommonTDocumentEntreprises();
            $this->collCommonTDocumentEntreprisesPartial = true;
        }
        if (!in_array($l, $this->collCommonTDocumentEntreprises->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTDocumentEntreprise($l);
        }

        return $this;
    }

    /**
     * @param	CommonTDocumentEntreprise $commonTDocumentEntreprise The commonTDocumentEntreprise object to add.
     */
    protected function doAddCommonTDocumentEntreprise($commonTDocumentEntreprise)
    {
        $this->collCommonTDocumentEntreprises[]= $commonTDocumentEntreprise;
        $commonTDocumentEntreprise->setCommonTDocumentType($this);
    }

    /**
     * @param	CommonTDocumentEntreprise $commonTDocumentEntreprise The commonTDocumentEntreprise object to remove.
     * @return CommonTDocumentType The current object (for fluent API support)
     */
    public function removeCommonTDocumentEntreprise($commonTDocumentEntreprise)
    {
        if ($this->getCommonTDocumentEntreprises()->contains($commonTDocumentEntreprise)) {
            $this->collCommonTDocumentEntreprises->remove($this->collCommonTDocumentEntreprises->search($commonTDocumentEntreprise));
            if (null === $this->commonTDocumentEntreprisesScheduledForDeletion) {
                $this->commonTDocumentEntreprisesScheduledForDeletion = clone $this->collCommonTDocumentEntreprises;
                $this->commonTDocumentEntreprisesScheduledForDeletion->clear();
            }
            $this->commonTDocumentEntreprisesScheduledForDeletion[]= clone $commonTDocumentEntreprise;
            $commonTDocumentEntreprise->setCommonTDocumentType(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTDocumentType is new, it will return
     * an empty collection; or if this CommonTDocumentType has previously
     * been saved, it will retrieve related CommonTDocumentEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTDocumentType.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTDocumentEntreprise[] List of CommonTDocumentEntreprise objects
     */
    public function getCommonTDocumentEntreprisesJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTDocumentEntrepriseQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonTDocumentEntreprises($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_type_document = null;
        $this->nom_type_document = null;
        $this->code = null;
        $this->type_doc_entreprise_etablissement = null;
        $this->uri = null;
        $this->params_uri = null;
        $this->class_name = null;
        $this->nature_document = null;
        $this->type_retour_ws = null;
        $this->synchro_actif = null;
        $this->message_desactivation_synchro = null;
        $this->afficher_type_doc = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTDocumentEntreprises) {
                foreach ($this->collCommonTDocumentEntreprises as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTDocumentEntreprises instanceof PropelCollection) {
            $this->collCommonTDocumentEntreprises->clearIterator();
        }
        $this->collCommonTDocumentEntreprises = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTDocumentTypePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
