<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEchangeDoc;
use Application\Propel\Mpe\CommonEchangeDocApplicationClient;
use Application\Propel\Mpe\CommonEchangeDocBlob;
use Application\Propel\Mpe\CommonEchangeDocHistorique;
use Application\Propel\Mpe\CommonEchangeDocPeer;
use Application\Propel\Mpe\CommonEchangeDocQuery;
use Application\Propel\Mpe\CommonReferentielSousTypeParapheur;
use Application\Propel\Mpe\CommonTContratTitulaire;

/**
 * Base class that represents a query for the 'echange_doc' table.
 *
 *
 *
 * @method CommonEchangeDocQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonEchangeDocQuery orderByEchangeDocApplicationClientId($order = Criteria::ASC) Order by the echange_doc_application_client_id column
 * @method CommonEchangeDocQuery orderByObjet($order = Criteria::ASC) Order by the objet column
 * @method CommonEchangeDocQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method CommonEchangeDocQuery orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 * @method CommonEchangeDocQuery orderByAgentId($order = Criteria::ASC) Order by the agent_id column
 * @method CommonEchangeDocQuery orderByStatut($order = Criteria::ASC) Order by the statut column
 * @method CommonEchangeDocQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method CommonEchangeDocQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 * @method CommonEchangeDocQuery orderByCheminementSignature($order = Criteria::ASC) Order by the cheminement_signature column
 * @method CommonEchangeDocQuery orderByCheminementGed($order = Criteria::ASC) Order by the cheminement_ged column
 * @method CommonEchangeDocQuery orderByCheminementSae($order = Criteria::ASC) Order by the cheminement_sae column
 * @method CommonEchangeDocQuery orderByCheminementTdt($order = Criteria::ASC) Order by the cheminement_tdt column
 * @method CommonEchangeDocQuery orderByIdContratTitulaire($order = Criteria::ASC) Order by the id_contrat_titulaire column
 * @method CommonEchangeDocQuery orderByReferentielSousTypeParapheurId($order = Criteria::ASC) Order by the referentiel_sous_type_parapheur_id column
 *
 * @method CommonEchangeDocQuery groupById() Group by the id column
 * @method CommonEchangeDocQuery groupByEchangeDocApplicationClientId() Group by the echange_doc_application_client_id column
 * @method CommonEchangeDocQuery groupByObjet() Group by the objet column
 * @method CommonEchangeDocQuery groupByDescription() Group by the description column
 * @method CommonEchangeDocQuery groupByConsultationId() Group by the consultation_id column
 * @method CommonEchangeDocQuery groupByAgentId() Group by the agent_id column
 * @method CommonEchangeDocQuery groupByStatut() Group by the statut column
 * @method CommonEchangeDocQuery groupByCreatedAt() Group by the created_at column
 * @method CommonEchangeDocQuery groupByUpdatedAt() Group by the updated_at column
 * @method CommonEchangeDocQuery groupByCheminementSignature() Group by the cheminement_signature column
 * @method CommonEchangeDocQuery groupByCheminementGed() Group by the cheminement_ged column
 * @method CommonEchangeDocQuery groupByCheminementSae() Group by the cheminement_sae column
 * @method CommonEchangeDocQuery groupByCheminementTdt() Group by the cheminement_tdt column
 * @method CommonEchangeDocQuery groupByIdContratTitulaire() Group by the id_contrat_titulaire column
 * @method CommonEchangeDocQuery groupByReferentielSousTypeParapheurId() Group by the referentiel_sous_type_parapheur_id column
 *
 * @method CommonEchangeDocQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonEchangeDocQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonEchangeDocQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonEchangeDocQuery leftJoinCommonReferentielSousTypeParapheur($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonReferentielSousTypeParapheur relation
 * @method CommonEchangeDocQuery rightJoinCommonReferentielSousTypeParapheur($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonReferentielSousTypeParapheur relation
 * @method CommonEchangeDocQuery innerJoinCommonReferentielSousTypeParapheur($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonReferentielSousTypeParapheur relation
 *
 * @method CommonEchangeDocQuery leftJoinCommonTContratTitulaire($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonEchangeDocQuery rightJoinCommonTContratTitulaire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonEchangeDocQuery innerJoinCommonTContratTitulaire($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContratTitulaire relation
 *
 * @method CommonEchangeDocQuery leftJoinCommonAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAgent relation
 * @method CommonEchangeDocQuery rightJoinCommonAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAgent relation
 * @method CommonEchangeDocQuery innerJoinCommonAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAgent relation
 *
 * @method CommonEchangeDocQuery leftJoinCommonEchangeDocApplicationClient($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocApplicationClient relation
 * @method CommonEchangeDocQuery rightJoinCommonEchangeDocApplicationClient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocApplicationClient relation
 * @method CommonEchangeDocQuery innerJoinCommonEchangeDocApplicationClient($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocApplicationClient relation
 *
 * @method CommonEchangeDocQuery leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonEchangeDocQuery rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonEchangeDocQuery innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonEchangeDocQuery leftJoinCommonEchangeDocBlob($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocBlob relation
 * @method CommonEchangeDocQuery rightJoinCommonEchangeDocBlob($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocBlob relation
 * @method CommonEchangeDocQuery innerJoinCommonEchangeDocBlob($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocBlob relation
 *
 * @method CommonEchangeDocQuery leftJoinCommonEchangeDocHistorique($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocHistorique relation
 * @method CommonEchangeDocQuery rightJoinCommonEchangeDocHistorique($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocHistorique relation
 * @method CommonEchangeDocQuery innerJoinCommonEchangeDocHistorique($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocHistorique relation
 *
 * @method CommonEchangeDoc findOne(PropelPDO $con = null) Return the first CommonEchangeDoc matching the query
 * @method CommonEchangeDoc findOneOrCreate(PropelPDO $con = null) Return the first CommonEchangeDoc matching the query, or a new CommonEchangeDoc object populated from the query conditions when no match is found
 *
 * @method CommonEchangeDoc findOneByEchangeDocApplicationClientId(int $echange_doc_application_client_id) Return the first CommonEchangeDoc filtered by the echange_doc_application_client_id column
 * @method CommonEchangeDoc findOneByObjet(string $objet) Return the first CommonEchangeDoc filtered by the objet column
 * @method CommonEchangeDoc findOneByDescription(string $description) Return the first CommonEchangeDoc filtered by the description column
 * @method CommonEchangeDoc findOneByConsultationId(int $consultation_id) Return the first CommonEchangeDoc filtered by the consultation_id column
 * @method CommonEchangeDoc findOneByAgentId(int $agent_id) Return the first CommonEchangeDoc filtered by the agent_id column
 * @method CommonEchangeDoc findOneByStatut(string $statut) Return the first CommonEchangeDoc filtered by the statut column
 * @method CommonEchangeDoc findOneByCreatedAt(string $created_at) Return the first CommonEchangeDoc filtered by the created_at column
 * @method CommonEchangeDoc findOneByUpdatedAt(string $updated_at) Return the first CommonEchangeDoc filtered by the updated_at column
 * @method CommonEchangeDoc findOneByCheminementSignature(boolean $cheminement_signature) Return the first CommonEchangeDoc filtered by the cheminement_signature column
 * @method CommonEchangeDoc findOneByCheminementGed(boolean $cheminement_ged) Return the first CommonEchangeDoc filtered by the cheminement_ged column
 * @method CommonEchangeDoc findOneByCheminementSae(boolean $cheminement_sae) Return the first CommonEchangeDoc filtered by the cheminement_sae column
 * @method CommonEchangeDoc findOneByCheminementTdt(boolean $cheminement_tdt) Return the first CommonEchangeDoc filtered by the cheminement_tdt column
 * @method CommonEchangeDoc findOneByIdContratTitulaire(int $id_contrat_titulaire) Return the first CommonEchangeDoc filtered by the id_contrat_titulaire column
 * @method CommonEchangeDoc findOneByReferentielSousTypeParapheurId(int $referentiel_sous_type_parapheur_id) Return the first CommonEchangeDoc filtered by the referentiel_sous_type_parapheur_id column
 *
 * @method array findById(int $id) Return CommonEchangeDoc objects filtered by the id column
 * @method array findByEchangeDocApplicationClientId(int $echange_doc_application_client_id) Return CommonEchangeDoc objects filtered by the echange_doc_application_client_id column
 * @method array findByObjet(string $objet) Return CommonEchangeDoc objects filtered by the objet column
 * @method array findByDescription(string $description) Return CommonEchangeDoc objects filtered by the description column
 * @method array findByConsultationId(int $consultation_id) Return CommonEchangeDoc objects filtered by the consultation_id column
 * @method array findByAgentId(int $agent_id) Return CommonEchangeDoc objects filtered by the agent_id column
 * @method array findByStatut(string $statut) Return CommonEchangeDoc objects filtered by the statut column
 * @method array findByCreatedAt(string $created_at) Return CommonEchangeDoc objects filtered by the created_at column
 * @method array findByUpdatedAt(string $updated_at) Return CommonEchangeDoc objects filtered by the updated_at column
 * @method array findByCheminementSignature(boolean $cheminement_signature) Return CommonEchangeDoc objects filtered by the cheminement_signature column
 * @method array findByCheminementGed(boolean $cheminement_ged) Return CommonEchangeDoc objects filtered by the cheminement_ged column
 * @method array findByCheminementSae(boolean $cheminement_sae) Return CommonEchangeDoc objects filtered by the cheminement_sae column
 * @method array findByCheminementTdt(boolean $cheminement_tdt) Return CommonEchangeDoc objects filtered by the cheminement_tdt column
 * @method array findByIdContratTitulaire(int $id_contrat_titulaire) Return CommonEchangeDoc objects filtered by the id_contrat_titulaire column
 * @method array findByReferentielSousTypeParapheurId(int $referentiel_sous_type_parapheur_id) Return CommonEchangeDoc objects filtered by the referentiel_sous_type_parapheur_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangeDocQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonEchangeDocQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonEchangeDoc', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonEchangeDocQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonEchangeDocQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonEchangeDocQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonEchangeDocQuery) {
            return $criteria;
        }
        $query = new CommonEchangeDocQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonEchangeDoc|CommonEchangeDoc[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonEchangeDocPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangeDoc A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangeDoc A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `echange_doc_application_client_id`, `objet`, `description`, `consultation_id`, `agent_id`, `statut`, `created_at`, `updated_at`, `cheminement_signature`, `cheminement_ged`, `cheminement_sae`, `cheminement_tdt`, `id_contrat_titulaire`, `referentiel_sous_type_parapheur_id` FROM `echange_doc` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonEchangeDoc();
            $obj->hydrate($row);
            CommonEchangeDocPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonEchangeDoc|CommonEchangeDoc[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonEchangeDoc[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonEchangeDocPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonEchangeDocPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the echange_doc_application_client_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEchangeDocApplicationClientId(1234); // WHERE echange_doc_application_client_id = 1234
     * $query->filterByEchangeDocApplicationClientId(array(12, 34)); // WHERE echange_doc_application_client_id IN (12, 34)
     * $query->filterByEchangeDocApplicationClientId(array('min' => 12)); // WHERE echange_doc_application_client_id >= 12
     * $query->filterByEchangeDocApplicationClientId(array('max' => 12)); // WHERE echange_doc_application_client_id <= 12
     * </code>
     *
     * @see       filterByCommonEchangeDocApplicationClient()
     *
     * @param     mixed $echangeDocApplicationClientId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByEchangeDocApplicationClientId($echangeDocApplicationClientId = null, $comparison = null)
    {
        if (is_array($echangeDocApplicationClientId)) {
            $useMinMax = false;
            if (isset($echangeDocApplicationClientId['min'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, $echangeDocApplicationClientId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($echangeDocApplicationClientId['max'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, $echangeDocApplicationClientId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, $echangeDocApplicationClientId, $comparison);
    }

    /**
     * Filter the query on the objet column
     *
     * Example usage:
     * <code>
     * $query->filterByObjet('fooValue');   // WHERE objet = 'fooValue'
     * $query->filterByObjet('%fooValue%'); // WHERE objet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $objet The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByObjet($objet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($objet)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $objet)) {
                $objet = str_replace('*', '%', $objet);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::OBJET, $objet, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultation()
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Filter the query on the agent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAgentId(1234); // WHERE agent_id = 1234
     * $query->filterByAgentId(array(12, 34)); // WHERE agent_id IN (12, 34)
     * $query->filterByAgentId(array('min' => 12)); // WHERE agent_id >= 12
     * $query->filterByAgentId(array('max' => 12)); // WHERE agent_id <= 12
     * </code>
     *
     * @see       filterByCommonAgent()
     *
     * @param     mixed $agentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByAgentId($agentId = null, $comparison = null)
    {
        if (is_array($agentId)) {
            $useMinMax = false;
            if (isset($agentId['min'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::AGENT_ID, $agentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($agentId['max'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::AGENT_ID, $agentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::AGENT_ID, $agentId, $comparison);
    }

    /**
     * Filter the query on the statut column
     *
     * Example usage:
     * <code>
     * $query->filterByStatut('fooValue');   // WHERE statut = 'fooValue'
     * $query->filterByStatut('%fooValue%'); // WHERE statut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statut The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByStatut($statut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statut)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $statut)) {
                $statut = str_replace('*', '%', $statut);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::STATUT, $statut, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query on the cheminement_signature column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminementSignature(true); // WHERE cheminement_signature = true
     * $query->filterByCheminementSignature('yes'); // WHERE cheminement_signature = true
     * </code>
     *
     * @param     boolean|string $cheminementSignature The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByCheminementSignature($cheminementSignature = null, $comparison = null)
    {
        if (is_string($cheminementSignature)) {
            $cheminementSignature = in_array(strtolower($cheminementSignature), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::CHEMINEMENT_SIGNATURE, $cheminementSignature, $comparison);
    }

    /**
     * Filter the query on the cheminement_ged column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminementGed(true); // WHERE cheminement_ged = true
     * $query->filterByCheminementGed('yes'); // WHERE cheminement_ged = true
     * </code>
     *
     * @param     boolean|string $cheminementGed The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByCheminementGed($cheminementGed = null, $comparison = null)
    {
        if (is_string($cheminementGed)) {
            $cheminementGed = in_array(strtolower($cheminementGed), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::CHEMINEMENT_GED, $cheminementGed, $comparison);
    }

    /**
     * Filter the query on the cheminement_sae column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminementSae(true); // WHERE cheminement_sae = true
     * $query->filterByCheminementSae('yes'); // WHERE cheminement_sae = true
     * </code>
     *
     * @param     boolean|string $cheminementSae The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByCheminementSae($cheminementSae = null, $comparison = null)
    {
        if (is_string($cheminementSae)) {
            $cheminementSae = in_array(strtolower($cheminementSae), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::CHEMINEMENT_SAE, $cheminementSae, $comparison);
    }

    /**
     * Filter the query on the cheminement_tdt column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminementTdt(true); // WHERE cheminement_tdt = true
     * $query->filterByCheminementTdt('yes'); // WHERE cheminement_tdt = true
     * </code>
     *
     * @param     boolean|string $cheminementTdt The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByCheminementTdt($cheminementTdt = null, $comparison = null)
    {
        if (is_string($cheminementTdt)) {
            $cheminementTdt = in_array(strtolower($cheminementTdt), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::CHEMINEMENT_TDT, $cheminementTdt, $comparison);
    }

    /**
     * Filter the query on the id_contrat_titulaire column
     *
     * Example usage:
     * <code>
     * $query->filterByIdContratTitulaire(1234); // WHERE id_contrat_titulaire = 1234
     * $query->filterByIdContratTitulaire(array(12, 34)); // WHERE id_contrat_titulaire IN (12, 34)
     * $query->filterByIdContratTitulaire(array('min' => 12)); // WHERE id_contrat_titulaire >= 12
     * $query->filterByIdContratTitulaire(array('max' => 12)); // WHERE id_contrat_titulaire <= 12
     * </code>
     *
     * @see       filterByCommonTContratTitulaire()
     *
     * @param     mixed $idContratTitulaire The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByIdContratTitulaire($idContratTitulaire = null, $comparison = null)
    {
        if (is_array($idContratTitulaire)) {
            $useMinMax = false;
            if (isset($idContratTitulaire['min'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idContratTitulaire['max'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, $idContratTitulaire, $comparison);
    }

    /**
     * Filter the query on the referentiel_sous_type_parapheur_id column
     *
     * Example usage:
     * <code>
     * $query->filterByReferentielSousTypeParapheurId(1234); // WHERE referentiel_sous_type_parapheur_id = 1234
     * $query->filterByReferentielSousTypeParapheurId(array(12, 34)); // WHERE referentiel_sous_type_parapheur_id IN (12, 34)
     * $query->filterByReferentielSousTypeParapheurId(array('min' => 12)); // WHERE referentiel_sous_type_parapheur_id >= 12
     * $query->filterByReferentielSousTypeParapheurId(array('max' => 12)); // WHERE referentiel_sous_type_parapheur_id <= 12
     * </code>
     *
     * @see       filterByCommonReferentielSousTypeParapheur()
     *
     * @param     mixed $referentielSousTypeParapheurId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function filterByReferentielSousTypeParapheurId($referentielSousTypeParapheurId = null, $comparison = null)
    {
        if (is_array($referentielSousTypeParapheurId)) {
            $useMinMax = false;
            if (isset($referentielSousTypeParapheurId['min'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, $referentielSousTypeParapheurId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($referentielSousTypeParapheurId['max'])) {
                $this->addUsingAlias(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, $referentielSousTypeParapheurId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, $referentielSousTypeParapheurId, $comparison);
    }

    /**
     * Filter the query by a related CommonReferentielSousTypeParapheur object
     *
     * @param   CommonReferentielSousTypeParapheur|PropelObjectCollection $commonReferentielSousTypeParapheur The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonReferentielSousTypeParapheur($commonReferentielSousTypeParapheur, $comparison = null)
    {
        if ($commonReferentielSousTypeParapheur instanceof CommonReferentielSousTypeParapheur) {
            return $this
                ->addUsingAlias(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, $commonReferentielSousTypeParapheur->getId(), $comparison);
        } elseif ($commonReferentielSousTypeParapheur instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocPeer::REFERENTIEL_SOUS_TYPE_PARAPHEUR_ID, $commonReferentielSousTypeParapheur->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonReferentielSousTypeParapheur() only accepts arguments of type CommonReferentielSousTypeParapheur or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonReferentielSousTypeParapheur relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function joinCommonReferentielSousTypeParapheur($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonReferentielSousTypeParapheur');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonReferentielSousTypeParapheur');
        }

        return $this;
    }

    /**
     * Use the CommonReferentielSousTypeParapheur relation CommonReferentielSousTypeParapheur object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonReferentielSousTypeParapheurQuery A secondary query class using the current class as primary query
     */
    public function useCommonReferentielSousTypeParapheurQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonReferentielSousTypeParapheur($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonReferentielSousTypeParapheur', '\Application\Propel\Mpe\CommonReferentielSousTypeParapheurQuery');
    }

    /**
     * Filter the query by a related CommonTContratTitulaire object
     *
     * @param   CommonTContratTitulaire|PropelObjectCollection $commonTContratTitulaire The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContratTitulaire($commonTContratTitulaire, $comparison = null)
    {
        if ($commonTContratTitulaire instanceof CommonTContratTitulaire) {
            return $this
                ->addUsingAlias(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, $commonTContratTitulaire->getIdContratTitulaire(), $comparison);
        } elseif ($commonTContratTitulaire instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocPeer::ID_CONTRAT_TITULAIRE, $commonTContratTitulaire->toKeyValue('PrimaryKey', 'IdContratTitulaire'), $comparison);
        } else {
            throw new PropelException('filterByCommonTContratTitulaire() only accepts arguments of type CommonTContratTitulaire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContratTitulaire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function joinCommonTContratTitulaire($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContratTitulaire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContratTitulaire');
        }

        return $this;
    }

    /**
     * Use the CommonTContratTitulaire relation CommonTContratTitulaire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContratTitulaireQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContratTitulaireQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTContratTitulaire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContratTitulaire', '\Application\Propel\Mpe\CommonTContratTitulaireQuery');
    }

    /**
     * Filter the query by a related CommonAgent object
     *
     * @param   CommonAgent|PropelObjectCollection $commonAgent The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAgent($commonAgent, $comparison = null)
    {
        if ($commonAgent instanceof CommonAgent) {
            return $this
                ->addUsingAlias(CommonEchangeDocPeer::AGENT_ID, $commonAgent->getId(), $comparison);
        } elseif ($commonAgent instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocPeer::AGENT_ID, $commonAgent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonAgent() only accepts arguments of type CommonAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function joinCommonAgent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAgent');
        }

        return $this;
    }

    /**
     * Use the CommonAgent relation CommonAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonAgentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAgent', '\Application\Propel\Mpe\CommonAgentQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDocApplicationClient object
     *
     * @param   CommonEchangeDocApplicationClient|PropelObjectCollection $commonEchangeDocApplicationClient The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocApplicationClient($commonEchangeDocApplicationClient, $comparison = null)
    {
        if ($commonEchangeDocApplicationClient instanceof CommonEchangeDocApplicationClient) {
            return $this
                ->addUsingAlias(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, $commonEchangeDocApplicationClient->getId(), $comparison);
        } elseif ($commonEchangeDocApplicationClient instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocPeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, $commonEchangeDocApplicationClient->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonEchangeDocApplicationClient() only accepts arguments of type CommonEchangeDocApplicationClient or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocApplicationClient relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocApplicationClient($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocApplicationClient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocApplicationClient');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocApplicationClient relation CommonEchangeDocApplicationClient object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocApplicationClientQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocApplicationClientQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocApplicationClient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocApplicationClient', '\Application\Propel\Mpe\CommonEchangeDocApplicationClientQuery');
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonEchangeDocPeer::CONSULTATION_ID, $commonConsultation->getId(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocPeer::CONSULTATION_ID, $commonConsultation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDocBlob object
     *
     * @param   CommonEchangeDocBlob|PropelObjectCollection $commonEchangeDocBlob  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocBlob($commonEchangeDocBlob, $comparison = null)
    {
        if ($commonEchangeDocBlob instanceof CommonEchangeDocBlob) {
            return $this
                ->addUsingAlias(CommonEchangeDocPeer::ID, $commonEchangeDocBlob->getEchangeDocId(), $comparison);
        } elseif ($commonEchangeDocBlob instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeDocBlobQuery()
                ->filterByPrimaryKeys($commonEchangeDocBlob->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangeDocBlob() only accepts arguments of type CommonEchangeDocBlob or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocBlob relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocBlob($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocBlob');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocBlob');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocBlob relation CommonEchangeDocBlob object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocBlobQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocBlobQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocBlob($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocBlob', '\Application\Propel\Mpe\CommonEchangeDocBlobQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDocHistorique object
     *
     * @param   CommonEchangeDocHistorique|PropelObjectCollection $commonEchangeDocHistorique  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocHistorique($commonEchangeDocHistorique, $comparison = null)
    {
        if ($commonEchangeDocHistorique instanceof CommonEchangeDocHistorique) {
            return $this
                ->addUsingAlias(CommonEchangeDocPeer::ID, $commonEchangeDocHistorique->getEchangeDocId(), $comparison);
        } elseif ($commonEchangeDocHistorique instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeDocHistoriqueQuery()
                ->filterByPrimaryKeys($commonEchangeDocHistorique->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangeDocHistorique() only accepts arguments of type CommonEchangeDocHistorique or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocHistorique relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocHistorique($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocHistorique');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocHistorique');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocHistorique relation CommonEchangeDocHistorique object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocHistoriqueQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocHistoriqueQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocHistorique($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocHistorique', '\Application\Propel\Mpe\CommonEchangeDocHistoriqueQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonEchangeDoc $commonEchangeDoc Object to remove from the list of results
     *
     * @return CommonEchangeDocQuery The current query, for fluid interface
     */
    public function prune($commonEchangeDoc = null)
    {
        if ($commonEchangeDoc) {
            $this->addUsingAlias(CommonEchangeDocPeer::ID, $commonEchangeDoc->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
