<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAffiliationService;
use Application\Propel\Mpe\CommonAffiliationServicePeer;
use Application\Propel\Mpe\CommonAffiliationServiceQuery;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;

/**
 * Base class that represents a query for the 'AffiliationService' table.
 *
 *
 *
 * @method CommonAffiliationServiceQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonAffiliationServiceQuery orderByOldIdPole($order = Criteria::ASC) Order by the old_id_pole column
 * @method CommonAffiliationServiceQuery orderByOldIdService($order = Criteria::ASC) Order by the old_id_service column
 * @method CommonAffiliationServiceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonAffiliationServiceQuery orderByServiceParentId($order = Criteria::ASC) Order by the service_parent_id column
 * @method CommonAffiliationServiceQuery orderByServiceId($order = Criteria::ASC) Order by the service_id column
 *
 * @method CommonAffiliationServiceQuery groupByOrganisme() Group by the organisme column
 * @method CommonAffiliationServiceQuery groupByOldIdPole() Group by the old_id_pole column
 * @method CommonAffiliationServiceQuery groupByOldIdService() Group by the old_id_service column
 * @method CommonAffiliationServiceQuery groupById() Group by the id column
 * @method CommonAffiliationServiceQuery groupByServiceParentId() Group by the service_parent_id column
 * @method CommonAffiliationServiceQuery groupByServiceId() Group by the service_id column
 *
 * @method CommonAffiliationServiceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonAffiliationServiceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonAffiliationServiceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonAffiliationServiceQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonAffiliationServiceQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonAffiliationServiceQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonAffiliationServiceQuery leftJoinCommonServiceRelatedByServiceId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonServiceRelatedByServiceId relation
 * @method CommonAffiliationServiceQuery rightJoinCommonServiceRelatedByServiceId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonServiceRelatedByServiceId relation
 * @method CommonAffiliationServiceQuery innerJoinCommonServiceRelatedByServiceId($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonServiceRelatedByServiceId relation
 *
 * @method CommonAffiliationServiceQuery leftJoinCommonServiceRelatedByServiceParentId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonServiceRelatedByServiceParentId relation
 * @method CommonAffiliationServiceQuery rightJoinCommonServiceRelatedByServiceParentId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonServiceRelatedByServiceParentId relation
 * @method CommonAffiliationServiceQuery innerJoinCommonServiceRelatedByServiceParentId($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonServiceRelatedByServiceParentId relation
 *
 * @method CommonAffiliationService findOne(PropelPDO $con = null) Return the first CommonAffiliationService matching the query
 * @method CommonAffiliationService findOneOrCreate(PropelPDO $con = null) Return the first CommonAffiliationService matching the query, or a new CommonAffiliationService object populated from the query conditions when no match is found
 *
 * @method CommonAffiliationService findOneByOrganisme(string $organisme) Return the first CommonAffiliationService filtered by the organisme column
 * @method CommonAffiliationService findOneByOldIdPole(int $old_id_pole) Return the first CommonAffiliationService filtered by the old_id_pole column
 * @method CommonAffiliationService findOneByOldIdService(int $old_id_service) Return the first CommonAffiliationService filtered by the old_id_service column
 * @method CommonAffiliationService findOneByServiceParentId(string $service_parent_id) Return the first CommonAffiliationService filtered by the service_parent_id column
 * @method CommonAffiliationService findOneByServiceId(string $service_id) Return the first CommonAffiliationService filtered by the service_id column
 *
 * @method array findByOrganisme(string $organisme) Return CommonAffiliationService objects filtered by the organisme column
 * @method array findByOldIdPole(int $old_id_pole) Return CommonAffiliationService objects filtered by the old_id_pole column
 * @method array findByOldIdService(int $old_id_service) Return CommonAffiliationService objects filtered by the old_id_service column
 * @method array findById(string $id) Return CommonAffiliationService objects filtered by the id column
 * @method array findByServiceParentId(string $service_parent_id) Return CommonAffiliationService objects filtered by the service_parent_id column
 * @method array findByServiceId(string $service_id) Return CommonAffiliationService objects filtered by the service_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonAffiliationServiceQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonAffiliationServiceQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonAffiliationService', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonAffiliationServiceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonAffiliationServiceQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonAffiliationServiceQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonAffiliationServiceQuery) {
            return $criteria;
        }
        $query = new CommonAffiliationServiceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonAffiliationService|CommonAffiliationService[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonAffiliationServicePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonAffiliationServicePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonAffiliationService A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonAffiliationService A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `organisme`, `old_id_pole`, `old_id_service`, `id`, `service_parent_id`, `service_id` FROM `AffiliationService` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonAffiliationService();
            $obj->hydrate($row);
            CommonAffiliationServicePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonAffiliationService|CommonAffiliationService[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonAffiliationService[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonAffiliationServicePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonAffiliationServicePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonAffiliationServicePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the old_id_pole column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdPole(1234); // WHERE old_id_pole = 1234
     * $query->filterByOldIdPole(array(12, 34)); // WHERE old_id_pole IN (12, 34)
     * $query->filterByOldIdPole(array('min' => 12)); // WHERE old_id_pole >= 12
     * $query->filterByOldIdPole(array('max' => 12)); // WHERE old_id_pole <= 12
     * </code>
     *
     * @param     mixed $oldIdPole The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function filterByOldIdPole($oldIdPole = null, $comparison = null)
    {
        if (is_array($oldIdPole)) {
            $useMinMax = false;
            if (isset($oldIdPole['min'])) {
                $this->addUsingAlias(CommonAffiliationServicePeer::OLD_ID_POLE, $oldIdPole['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdPole['max'])) {
                $this->addUsingAlias(CommonAffiliationServicePeer::OLD_ID_POLE, $oldIdPole['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAffiliationServicePeer::OLD_ID_POLE, $oldIdPole, $comparison);
    }

    /**
     * Filter the query on the old_id_service column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdService(1234); // WHERE old_id_service = 1234
     * $query->filterByOldIdService(array(12, 34)); // WHERE old_id_service IN (12, 34)
     * $query->filterByOldIdService(array('min' => 12)); // WHERE old_id_service >= 12
     * $query->filterByOldIdService(array('max' => 12)); // WHERE old_id_service <= 12
     * </code>
     *
     * @param     mixed $oldIdService The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function filterByOldIdService($oldIdService = null, $comparison = null)
    {
        if (is_array($oldIdService)) {
            $useMinMax = false;
            if (isset($oldIdService['min'])) {
                $this->addUsingAlias(CommonAffiliationServicePeer::OLD_ID_SERVICE, $oldIdService['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdService['max'])) {
                $this->addUsingAlias(CommonAffiliationServicePeer::OLD_ID_SERVICE, $oldIdService['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAffiliationServicePeer::OLD_ID_SERVICE, $oldIdService, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonAffiliationServicePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonAffiliationServicePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAffiliationServicePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the service_parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceParentId(1234); // WHERE service_parent_id = 1234
     * $query->filterByServiceParentId(array(12, 34)); // WHERE service_parent_id IN (12, 34)
     * $query->filterByServiceParentId(array('min' => 12)); // WHERE service_parent_id >= 12
     * $query->filterByServiceParentId(array('max' => 12)); // WHERE service_parent_id <= 12
     * </code>
     *
     * @see       filterByCommonServiceRelatedByServiceParentId()
     *
     * @param     mixed $serviceParentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function filterByServiceParentId($serviceParentId = null, $comparison = null)
    {
        if (is_array($serviceParentId)) {
            $useMinMax = false;
            if (isset($serviceParentId['min'])) {
                $this->addUsingAlias(CommonAffiliationServicePeer::SERVICE_PARENT_ID, $serviceParentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceParentId['max'])) {
                $this->addUsingAlias(CommonAffiliationServicePeer::SERVICE_PARENT_ID, $serviceParentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAffiliationServicePeer::SERVICE_PARENT_ID, $serviceParentId, $comparison);
    }

    /**
     * Filter the query on the service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceId(1234); // WHERE service_id = 1234
     * $query->filterByServiceId(array(12, 34)); // WHERE service_id IN (12, 34)
     * $query->filterByServiceId(array('min' => 12)); // WHERE service_id >= 12
     * $query->filterByServiceId(array('max' => 12)); // WHERE service_id <= 12
     * </code>
     *
     * @see       filterByCommonServiceRelatedByServiceId()
     *
     * @param     mixed $serviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function filterByServiceId($serviceId = null, $comparison = null)
    {
        if (is_array($serviceId)) {
            $useMinMax = false;
            if (isset($serviceId['min'])) {
                $this->addUsingAlias(CommonAffiliationServicePeer::SERVICE_ID, $serviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceId['max'])) {
                $this->addUsingAlias(CommonAffiliationServicePeer::SERVICE_ID, $serviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonAffiliationServicePeer::SERVICE_ID, $serviceId, $comparison);
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAffiliationServiceQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonAffiliationServicePeer::ORGANISME, $commonOrganisme->getAcronyme(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonAffiliationServicePeer::ORGANISME, $commonOrganisme->toKeyValue('PrimaryKey', 'Acronyme'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Filter the query by a related CommonService object
     *
     * @param   CommonService|PropelObjectCollection $commonService The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAffiliationServiceQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonServiceRelatedByServiceId($commonService, $comparison = null)
    {
        if ($commonService instanceof CommonService) {
            return $this
                ->addUsingAlias(CommonAffiliationServicePeer::SERVICE_ID, $commonService->getId(), $comparison);
        } elseif ($commonService instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonAffiliationServicePeer::SERVICE_ID, $commonService->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonServiceRelatedByServiceId() only accepts arguments of type CommonService or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonServiceRelatedByServiceId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function joinCommonServiceRelatedByServiceId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonServiceRelatedByServiceId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonServiceRelatedByServiceId');
        }

        return $this;
    }

    /**
     * Use the CommonServiceRelatedByServiceId relation CommonService object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonServiceQuery A secondary query class using the current class as primary query
     */
    public function useCommonServiceRelatedByServiceIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonServiceRelatedByServiceId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonServiceRelatedByServiceId', '\Application\Propel\Mpe\CommonServiceQuery');
    }

    /**
     * Filter the query by a related CommonService object
     *
     * @param   CommonService|PropelObjectCollection $commonService The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonAffiliationServiceQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonServiceRelatedByServiceParentId($commonService, $comparison = null)
    {
        if ($commonService instanceof CommonService) {
            return $this
                ->addUsingAlias(CommonAffiliationServicePeer::SERVICE_PARENT_ID, $commonService->getId(), $comparison);
        } elseif ($commonService instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonAffiliationServicePeer::SERVICE_PARENT_ID, $commonService->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonServiceRelatedByServiceParentId() only accepts arguments of type CommonService or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonServiceRelatedByServiceParentId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function joinCommonServiceRelatedByServiceParentId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonServiceRelatedByServiceParentId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonServiceRelatedByServiceParentId');
        }

        return $this;
    }

    /**
     * Use the CommonServiceRelatedByServiceParentId relation CommonService object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonServiceQuery A secondary query class using the current class as primary query
     */
    public function useCommonServiceRelatedByServiceParentIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonServiceRelatedByServiceParentId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonServiceRelatedByServiceParentId', '\Application\Propel\Mpe\CommonServiceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonAffiliationService $commonAffiliationService Object to remove from the list of results
     *
     * @return CommonAffiliationServiceQuery The current query, for fluid interface
     */
    public function prune($commonAffiliationService = null)
    {
        if ($commonAffiliationService) {
            $this->addUsingAlias(CommonAffiliationServicePeer::ID, $commonAffiliationService->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
