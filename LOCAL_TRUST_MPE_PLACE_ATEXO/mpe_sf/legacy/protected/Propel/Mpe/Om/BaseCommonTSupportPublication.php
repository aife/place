<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTOffreSupportPublicite;
use Application\Propel\Mpe\CommonTOffreSupportPubliciteQuery;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationQuery;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Propel\Mpe\CommonTSupportPublicationPeer;
use Application\Propel\Mpe\CommonTSupportPublicationQuery;

/**
 * Base class that represents a row from the 't_support_publication' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTSupportPublication extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTSupportPublicationPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTSupportPublicationPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the image_logo field.
     * @var        string
     */
    protected $image_logo;

    /**
     * The value for the nom field.
     * @var        string
     */
    protected $nom;

    /**
     * The value for the visible field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $visible;

    /**
     * The value for the ordre field.
     * @var        int
     */
    protected $ordre;

    /**
     * The value for the default_value field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $default_value;

    /**
     * The value for the code field.
     * @var        string
     */
    protected $code;

    /**
     * The value for the actif field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $actif;

    /**
     * The value for the url field.
     * @var        string
     */
    protected $url;

    /**
     * The value for the groupe field.
     * @var        string
     */
    protected $groupe;

    /**
     * The value for the tag_debut_fin_groupe field.
     * @var        string
     */
    protected $tag_debut_fin_groupe;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * The value for the nbre_total_groupe field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $nbre_total_groupe;

    /**
     * The value for the lieux_execution field.
     * @var        string
     */
    protected $lieux_execution;

    /**
     * The value for the detail_info field.
     * @var        string
     */
    protected $detail_info;

    /**
     * The value for the type_info field.
     * @var        string
     */
    protected $type_info;

    /**
     * The value for the affichage_infos field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $affichage_infos;

    /**
     * The value for the affichage_message_support field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $affichage_message_support;

    /**
     * The value for the selection_departements_parution field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $selection_departements_parution;

    /**
     * The value for the departements_parution field.
     * @var        string
     */
    protected $departements_parution;

    /**
     * @var        PropelObjectCollection|CommonTOffreSupportPublicite[] Collection to store aggregation of CommonTOffreSupportPublicite objects.
     */
    protected $collCommonTOffreSupportPublicites;
    protected $collCommonTOffreSupportPublicitesPartial;

    /**
     * @var        PropelObjectCollection|CommonTSupportAnnonceConsultation[] Collection to store aggregation of CommonTSupportAnnonceConsultation objects.
     */
    protected $collCommonTSupportAnnonceConsultations;
    protected $collCommonTSupportAnnonceConsultationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTOffreSupportPublicitesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTSupportAnnonceConsultationsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->visible = '1';
        $this->default_value = '0';
        $this->actif = '0';
        $this->nbre_total_groupe = 1;
        $this->affichage_infos = '0';
        $this->affichage_message_support = '0';
        $this->selection_departements_parution = '0';
    }

    /**
     * Initializes internal state of BaseCommonTSupportPublication object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [image_logo] column value.
     *
     * @return string
     */
    public function getImageLogo()
    {

        return $this->image_logo;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {

        return $this->nom;
    }

    /**
     * Get the [visible] column value.
     *
     * @return string
     */
    public function getVisible()
    {

        return $this->visible;
    }

    /**
     * Get the [ordre] column value.
     *
     * @return int
     */
    public function getOrdre()
    {

        return $this->ordre;
    }

    /**
     * Get the [default_value] column value.
     *
     * @return string
     */
    public function getDefaultValue()
    {

        return $this->default_value;
    }

    /**
     * Get the [code] column value.
     *
     * @return string
     */
    public function getCode()
    {

        return $this->code;
    }

    /**
     * Get the [actif] column value.
     *
     * @return string
     */
    public function getActif()
    {

        return $this->actif;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {

        return $this->url;
    }

    /**
     * Get the [groupe] column value.
     *
     * @return string
     */
    public function getGroupe()
    {

        return $this->groupe;
    }

    /**
     * Get the [tag_debut_fin_groupe] column value.
     *
     * @return string
     */
    public function getTagDebutFinGroupe()
    {

        return $this->tag_debut_fin_groupe;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [nbre_total_groupe] column value.
     *
     * @return int
     */
    public function getNbreTotalGroupe()
    {

        return $this->nbre_total_groupe;
    }

    /**
     * Get the [lieux_execution] column value.
     *
     * @return string
     */
    public function getLieuxExecution()
    {

        return $this->lieux_execution;
    }

    /**
     * Get the [detail_info] column value.
     *
     * @return string
     */
    public function getDetailInfo()
    {

        return $this->detail_info;
    }

    /**
     * Get the [type_info] column value.
     *
     * @return string
     */
    public function getTypeInfo()
    {

        return $this->type_info;
    }

    /**
     * Get the [affichage_infos] column value.
     *
     * @return string
     */
    public function getAffichageInfos()
    {

        return $this->affichage_infos;
    }

    /**
     * Get the [affichage_message_support] column value.
     *
     * @return string
     */
    public function getAffichageMessageSupport()
    {

        return $this->affichage_message_support;
    }

    /**
     * Get the [selection_departements_parution] column value.
     *
     * @return string
     */
    public function getSelectionDepartementsParution()
    {

        return $this->selection_departements_parution;
    }

    /**
     * Get the [departements_parution] column value.
     *
     * @return string
     */
    public function getDepartementsParution()
    {

        return $this->departements_parution;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [image_logo] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setImageLogo($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->image_logo !== $v) {
            $this->image_logo = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::IMAGE_LOGO;
        }


        return $this;
    } // setImageLogo()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::NOM;
        }


        return $this;
    } // setNom()

    /**
     * Set the value of [visible] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setVisible($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->visible !== $v) {
            $this->visible = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::VISIBLE;
        }


        return $this;
    } // setVisible()

    /**
     * Set the value of [ordre] column.
     *
     * @param int $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setOrdre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->ordre !== $v) {
            $this->ordre = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::ORDRE;
        }


        return $this;
    } // setOrdre()

    /**
     * Set the value of [default_value] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setDefaultValue($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->default_value !== $v) {
            $this->default_value = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::DEFAULT_VALUE;
        }


        return $this;
    } // setDefaultValue()

    /**
     * Set the value of [code] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::CODE;
        }


        return $this;
    } // setCode()

    /**
     * Set the value of [actif] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setActif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->actif !== $v) {
            $this->actif = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::ACTIF;
        }


        return $this;
    } // setActif()

    /**
     * Set the value of [url] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::URL;
        }


        return $this;
    } // setUrl()

    /**
     * Set the value of [groupe] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setGroupe($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->groupe !== $v) {
            $this->groupe = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::GROUPE;
        }


        return $this;
    } // setGroupe()

    /**
     * Set the value of [tag_debut_fin_groupe] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setTagDebutFinGroupe($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tag_debut_fin_groupe !== $v) {
            $this->tag_debut_fin_groupe = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::TAG_DEBUT_FIN_GROUPE;
        }


        return $this;
    } // setTagDebutFinGroupe()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Set the value of [nbre_total_groupe] column.
     *
     * @param int $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setNbreTotalGroupe($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->nbre_total_groupe !== $v) {
            $this->nbre_total_groupe = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::NBRE_TOTAL_GROUPE;
        }


        return $this;
    } // setNbreTotalGroupe()

    /**
     * Set the value of [lieux_execution] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setLieuxExecution($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->lieux_execution !== $v) {
            $this->lieux_execution = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::LIEUX_EXECUTION;
        }


        return $this;
    } // setLieuxExecution()

    /**
     * Set the value of [detail_info] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setDetailInfo($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->detail_info !== $v) {
            $this->detail_info = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::DETAIL_INFO;
        }


        return $this;
    } // setDetailInfo()

    /**
     * Set the value of [type_info] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setTypeInfo($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_info !== $v) {
            $this->type_info = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::TYPE_INFO;
        }


        return $this;
    } // setTypeInfo()

    /**
     * Set the value of [affichage_infos] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setAffichageInfos($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->affichage_infos !== $v) {
            $this->affichage_infos = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::AFFICHAGE_INFOS;
        }


        return $this;
    } // setAffichageInfos()

    /**
     * Set the value of [affichage_message_support] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setAffichageMessageSupport($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->affichage_message_support !== $v) {
            $this->affichage_message_support = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::AFFICHAGE_MESSAGE_SUPPORT;
        }


        return $this;
    } // setAffichageMessageSupport()

    /**
     * Set the value of [selection_departements_parution] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setSelectionDepartementsParution($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->selection_departements_parution !== $v) {
            $this->selection_departements_parution = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::SELECTION_DEPARTEMENTS_PARUTION;
        }


        return $this;
    } // setSelectionDepartementsParution()

    /**
     * Set the value of [departements_parution] column.
     *
     * @param string $v new value
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setDepartementsParution($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->departements_parution !== $v) {
            $this->departements_parution = $v;
            $this->modifiedColumns[] = CommonTSupportPublicationPeer::DEPARTEMENTS_PARUTION;
        }


        return $this;
    } // setDepartementsParution()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->visible !== '1') {
                return false;
            }

            if ($this->default_value !== '0') {
                return false;
            }

            if ($this->actif !== '0') {
                return false;
            }

            if ($this->nbre_total_groupe !== 1) {
                return false;
            }

            if ($this->affichage_infos !== '0') {
                return false;
            }

            if ($this->affichage_message_support !== '0') {
                return false;
            }

            if ($this->selection_departements_parution !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->image_logo = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->nom = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->visible = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->ordre = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->default_value = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->code = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->actif = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->url = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->groupe = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->tag_debut_fin_groupe = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->description = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->nbre_total_groupe = ($row[$startcol + 12] !== null) ? (int) $row[$startcol + 12] : null;
            $this->lieux_execution = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->detail_info = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->type_info = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->affichage_infos = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->affichage_message_support = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->selection_departements_parution = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->departements_parution = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 20; // 20 = CommonTSupportPublicationPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTSupportPublication object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTSupportPublicationPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCommonTOffreSupportPublicites = null;

            $this->collCommonTSupportAnnonceConsultations = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTSupportPublicationQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTSupportPublicationPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonTOffreSupportPublicitesScheduledForDeletion !== null) {
                if (!$this->commonTOffreSupportPublicitesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTOffreSupportPubliciteQuery::create()
                        ->filterByPrimaryKeys($this->commonTOffreSupportPublicitesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTOffreSupportPublicitesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTOffreSupportPublicites !== null) {
                foreach ($this->collCommonTOffreSupportPublicites as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTSupportAnnonceConsultationsScheduledForDeletion !== null) {
                if (!$this->commonTSupportAnnonceConsultationsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTSupportAnnonceConsultationQuery::create()
                        ->filterByPrimaryKeys($this->commonTSupportAnnonceConsultationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTSupportAnnonceConsultationsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTSupportAnnonceConsultations !== null) {
                foreach ($this->collCommonTSupportAnnonceConsultations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTSupportPublicationPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTSupportPublicationPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTSupportPublicationPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::IMAGE_LOGO)) {
            $modifiedColumns[':p' . $index++]  = '`image_logo`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::VISIBLE)) {
            $modifiedColumns[':p' . $index++]  = '`visible`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::ORDRE)) {
            $modifiedColumns[':p' . $index++]  = '`ordre`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::DEFAULT_VALUE)) {
            $modifiedColumns[':p' . $index++]  = '`default_value`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::CODE)) {
            $modifiedColumns[':p' . $index++]  = '`code`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::ACTIF)) {
            $modifiedColumns[':p' . $index++]  = '`actif`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::URL)) {
            $modifiedColumns[':p' . $index++]  = '`url`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::GROUPE)) {
            $modifiedColumns[':p' . $index++]  = '`groupe`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::TAG_DEBUT_FIN_GROUPE)) {
            $modifiedColumns[':p' . $index++]  = '`tag_debut_fin_groupe`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`description`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::NBRE_TOTAL_GROUPE)) {
            $modifiedColumns[':p' . $index++]  = '`nbre_total_groupe`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::LIEUX_EXECUTION)) {
            $modifiedColumns[':p' . $index++]  = '`lieux_execution`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::DETAIL_INFO)) {
            $modifiedColumns[':p' . $index++]  = '`detail_info`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::TYPE_INFO)) {
            $modifiedColumns[':p' . $index++]  = '`type_info`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::AFFICHAGE_INFOS)) {
            $modifiedColumns[':p' . $index++]  = '`affichage_infos`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::AFFICHAGE_MESSAGE_SUPPORT)) {
            $modifiedColumns[':p' . $index++]  = '`affichage_message_support`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::SELECTION_DEPARTEMENTS_PARUTION)) {
            $modifiedColumns[':p' . $index++]  = '`selection_departements_parution`';
        }
        if ($this->isColumnModified(CommonTSupportPublicationPeer::DEPARTEMENTS_PARUTION)) {
            $modifiedColumns[':p' . $index++]  = '`departements_parution`';
        }

        $sql = sprintf(
            'INSERT INTO `t_support_publication` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`image_logo`':
                        $stmt->bindValue($identifier, $this->image_logo, PDO::PARAM_STR);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`visible`':
                        $stmt->bindValue($identifier, $this->visible, PDO::PARAM_STR);
                        break;
                    case '`ordre`':
                        $stmt->bindValue($identifier, $this->ordre, PDO::PARAM_INT);
                        break;
                    case '`default_value`':
                        $stmt->bindValue($identifier, $this->default_value, PDO::PARAM_STR);
                        break;
                    case '`code`':
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case '`actif`':
                        $stmt->bindValue($identifier, $this->actif, PDO::PARAM_STR);
                        break;
                    case '`url`':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case '`groupe`':
                        $stmt->bindValue($identifier, $this->groupe, PDO::PARAM_STR);
                        break;
                    case '`tag_debut_fin_groupe`':
                        $stmt->bindValue($identifier, $this->tag_debut_fin_groupe, PDO::PARAM_STR);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`nbre_total_groupe`':
                        $stmt->bindValue($identifier, $this->nbre_total_groupe, PDO::PARAM_INT);
                        break;
                    case '`lieux_execution`':
                        $stmt->bindValue($identifier, $this->lieux_execution, PDO::PARAM_STR);
                        break;
                    case '`detail_info`':
                        $stmt->bindValue($identifier, $this->detail_info, PDO::PARAM_STR);
                        break;
                    case '`type_info`':
                        $stmt->bindValue($identifier, $this->type_info, PDO::PARAM_STR);
                        break;
                    case '`affichage_infos`':
                        $stmt->bindValue($identifier, $this->affichage_infos, PDO::PARAM_STR);
                        break;
                    case '`affichage_message_support`':
                        $stmt->bindValue($identifier, $this->affichage_message_support, PDO::PARAM_STR);
                        break;
                    case '`selection_departements_parution`':
                        $stmt->bindValue($identifier, $this->selection_departements_parution, PDO::PARAM_STR);
                        break;
                    case '`departements_parution`':
                        $stmt->bindValue($identifier, $this->departements_parution, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonTSupportPublicationPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTOffreSupportPublicites !== null) {
                    foreach ($this->collCommonTOffreSupportPublicites as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTSupportAnnonceConsultations !== null) {
                    foreach ($this->collCommonTSupportAnnonceConsultations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTSupportPublicationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getImageLogo();
                break;
            case 2:
                return $this->getNom();
                break;
            case 3:
                return $this->getVisible();
                break;
            case 4:
                return $this->getOrdre();
                break;
            case 5:
                return $this->getDefaultValue();
                break;
            case 6:
                return $this->getCode();
                break;
            case 7:
                return $this->getActif();
                break;
            case 8:
                return $this->getUrl();
                break;
            case 9:
                return $this->getGroupe();
                break;
            case 10:
                return $this->getTagDebutFinGroupe();
                break;
            case 11:
                return $this->getDescription();
                break;
            case 12:
                return $this->getNbreTotalGroupe();
                break;
            case 13:
                return $this->getLieuxExecution();
                break;
            case 14:
                return $this->getDetailInfo();
                break;
            case 15:
                return $this->getTypeInfo();
                break;
            case 16:
                return $this->getAffichageInfos();
                break;
            case 17:
                return $this->getAffichageMessageSupport();
                break;
            case 18:
                return $this->getSelectionDepartementsParution();
                break;
            case 19:
                return $this->getDepartementsParution();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTSupportPublication'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTSupportPublication'][$this->getPrimaryKey()] = true;
        $keys = CommonTSupportPublicationPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getImageLogo(),
            $keys[2] => $this->getNom(),
            $keys[3] => $this->getVisible(),
            $keys[4] => $this->getOrdre(),
            $keys[5] => $this->getDefaultValue(),
            $keys[6] => $this->getCode(),
            $keys[7] => $this->getActif(),
            $keys[8] => $this->getUrl(),
            $keys[9] => $this->getGroupe(),
            $keys[10] => $this->getTagDebutFinGroupe(),
            $keys[11] => $this->getDescription(),
            $keys[12] => $this->getNbreTotalGroupe(),
            $keys[13] => $this->getLieuxExecution(),
            $keys[14] => $this->getDetailInfo(),
            $keys[15] => $this->getTypeInfo(),
            $keys[16] => $this->getAffichageInfos(),
            $keys[17] => $this->getAffichageMessageSupport(),
            $keys[18] => $this->getSelectionDepartementsParution(),
            $keys[19] => $this->getDepartementsParution(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collCommonTOffreSupportPublicites) {
                $result['CommonTOffreSupportPublicites'] = $this->collCommonTOffreSupportPublicites->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTSupportAnnonceConsultations) {
                $result['CommonTSupportAnnonceConsultations'] = $this->collCommonTSupportAnnonceConsultations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTSupportPublicationPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setImageLogo($value);
                break;
            case 2:
                $this->setNom($value);
                break;
            case 3:
                $this->setVisible($value);
                break;
            case 4:
                $this->setOrdre($value);
                break;
            case 5:
                $this->setDefaultValue($value);
                break;
            case 6:
                $this->setCode($value);
                break;
            case 7:
                $this->setActif($value);
                break;
            case 8:
                $this->setUrl($value);
                break;
            case 9:
                $this->setGroupe($value);
                break;
            case 10:
                $this->setTagDebutFinGroupe($value);
                break;
            case 11:
                $this->setDescription($value);
                break;
            case 12:
                $this->setNbreTotalGroupe($value);
                break;
            case 13:
                $this->setLieuxExecution($value);
                break;
            case 14:
                $this->setDetailInfo($value);
                break;
            case 15:
                $this->setTypeInfo($value);
                break;
            case 16:
                $this->setAffichageInfos($value);
                break;
            case 17:
                $this->setAffichageMessageSupport($value);
                break;
            case 18:
                $this->setSelectionDepartementsParution($value);
                break;
            case 19:
                $this->setDepartementsParution($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTSupportPublicationPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setImageLogo($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNom($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setVisible($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setOrdre($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDefaultValue($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCode($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setActif($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setUrl($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setGroupe($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setTagDebutFinGroupe($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setDescription($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setNbreTotalGroupe($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setLieuxExecution($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setDetailInfo($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setTypeInfo($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setAffichageInfos($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setAffichageMessageSupport($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setSelectionDepartementsParution($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setDepartementsParution($arr[$keys[19]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTSupportPublicationPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTSupportPublicationPeer::ID)) $criteria->add(CommonTSupportPublicationPeer::ID, $this->id);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::IMAGE_LOGO)) $criteria->add(CommonTSupportPublicationPeer::IMAGE_LOGO, $this->image_logo);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::NOM)) $criteria->add(CommonTSupportPublicationPeer::NOM, $this->nom);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::VISIBLE)) $criteria->add(CommonTSupportPublicationPeer::VISIBLE, $this->visible);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::ORDRE)) $criteria->add(CommonTSupportPublicationPeer::ORDRE, $this->ordre);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::DEFAULT_VALUE)) $criteria->add(CommonTSupportPublicationPeer::DEFAULT_VALUE, $this->default_value);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::CODE)) $criteria->add(CommonTSupportPublicationPeer::CODE, $this->code);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::ACTIF)) $criteria->add(CommonTSupportPublicationPeer::ACTIF, $this->actif);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::URL)) $criteria->add(CommonTSupportPublicationPeer::URL, $this->url);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::GROUPE)) $criteria->add(CommonTSupportPublicationPeer::GROUPE, $this->groupe);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::TAG_DEBUT_FIN_GROUPE)) $criteria->add(CommonTSupportPublicationPeer::TAG_DEBUT_FIN_GROUPE, $this->tag_debut_fin_groupe);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::DESCRIPTION)) $criteria->add(CommonTSupportPublicationPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::NBRE_TOTAL_GROUPE)) $criteria->add(CommonTSupportPublicationPeer::NBRE_TOTAL_GROUPE, $this->nbre_total_groupe);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::LIEUX_EXECUTION)) $criteria->add(CommonTSupportPublicationPeer::LIEUX_EXECUTION, $this->lieux_execution);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::DETAIL_INFO)) $criteria->add(CommonTSupportPublicationPeer::DETAIL_INFO, $this->detail_info);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::TYPE_INFO)) $criteria->add(CommonTSupportPublicationPeer::TYPE_INFO, $this->type_info);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::AFFICHAGE_INFOS)) $criteria->add(CommonTSupportPublicationPeer::AFFICHAGE_INFOS, $this->affichage_infos);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::AFFICHAGE_MESSAGE_SUPPORT)) $criteria->add(CommonTSupportPublicationPeer::AFFICHAGE_MESSAGE_SUPPORT, $this->affichage_message_support);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::SELECTION_DEPARTEMENTS_PARUTION)) $criteria->add(CommonTSupportPublicationPeer::SELECTION_DEPARTEMENTS_PARUTION, $this->selection_departements_parution);
        if ($this->isColumnModified(CommonTSupportPublicationPeer::DEPARTEMENTS_PARUTION)) $criteria->add(CommonTSupportPublicationPeer::DEPARTEMENTS_PARUTION, $this->departements_parution);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTSupportPublicationPeer::DATABASE_NAME);
        $criteria->add(CommonTSupportPublicationPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTSupportPublication (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setImageLogo($this->getImageLogo());
        $copyObj->setNom($this->getNom());
        $copyObj->setVisible($this->getVisible());
        $copyObj->setOrdre($this->getOrdre());
        $copyObj->setDefaultValue($this->getDefaultValue());
        $copyObj->setCode($this->getCode());
        $copyObj->setActif($this->getActif());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setGroupe($this->getGroupe());
        $copyObj->setTagDebutFinGroupe($this->getTagDebutFinGroupe());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setNbreTotalGroupe($this->getNbreTotalGroupe());
        $copyObj->setLieuxExecution($this->getLieuxExecution());
        $copyObj->setDetailInfo($this->getDetailInfo());
        $copyObj->setTypeInfo($this->getTypeInfo());
        $copyObj->setAffichageInfos($this->getAffichageInfos());
        $copyObj->setAffichageMessageSupport($this->getAffichageMessageSupport());
        $copyObj->setSelectionDepartementsParution($this->getSelectionDepartementsParution());
        $copyObj->setDepartementsParution($this->getDepartementsParution());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTOffreSupportPublicites() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTOffreSupportPublicite($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTSupportAnnonceConsultations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTSupportAnnonceConsultation($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTSupportPublication Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTSupportPublicationPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTSupportPublicationPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTOffreSupportPublicite' == $relationName) {
            $this->initCommonTOffreSupportPublicites();
        }
        if ('CommonTSupportAnnonceConsultation' == $relationName) {
            $this->initCommonTSupportAnnonceConsultations();
        }
    }

    /**
     * Clears out the collCommonTOffreSupportPublicites collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTSupportPublication The current object (for fluent API support)
     * @see        addCommonTOffreSupportPublicites()
     */
    public function clearCommonTOffreSupportPublicites()
    {
        $this->collCommonTOffreSupportPublicites = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTOffreSupportPublicitesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTOffreSupportPublicites collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTOffreSupportPublicites($v = true)
    {
        $this->collCommonTOffreSupportPublicitesPartial = $v;
    }

    /**
     * Initializes the collCommonTOffreSupportPublicites collection.
     *
     * By default this just sets the collCommonTOffreSupportPublicites collection to an empty array (like clearcollCommonTOffreSupportPublicites());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTOffreSupportPublicites($overrideExisting = true)
    {
        if (null !== $this->collCommonTOffreSupportPublicites && !$overrideExisting) {
            return;
        }
        $this->collCommonTOffreSupportPublicites = new PropelObjectCollection();
        $this->collCommonTOffreSupportPublicites->setModel('CommonTOffreSupportPublicite');
    }

    /**
     * Gets an array of CommonTOffreSupportPublicite objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTSupportPublication is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTOffreSupportPublicite[] List of CommonTOffreSupportPublicite objects
     * @throws PropelException
     */
    public function getCommonTOffreSupportPublicites($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTOffreSupportPublicitesPartial && !$this->isNew();
        if (null === $this->collCommonTOffreSupportPublicites || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTOffreSupportPublicites) {
                // return empty collection
                $this->initCommonTOffreSupportPublicites();
            } else {
                $collCommonTOffreSupportPublicites = CommonTOffreSupportPubliciteQuery::create(null, $criteria)
                    ->filterByCommonTSupportPublication($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTOffreSupportPublicitesPartial && count($collCommonTOffreSupportPublicites)) {
                      $this->initCommonTOffreSupportPublicites(false);

                      foreach ($collCommonTOffreSupportPublicites as $obj) {
                        if (false == $this->collCommonTOffreSupportPublicites->contains($obj)) {
                          $this->collCommonTOffreSupportPublicites->append($obj);
                        }
                      }

                      $this->collCommonTOffreSupportPublicitesPartial = true;
                    }

                    $collCommonTOffreSupportPublicites->getInternalIterator()->rewind();

                    return $collCommonTOffreSupportPublicites;
                }

                if ($partial && $this->collCommonTOffreSupportPublicites) {
                    foreach ($this->collCommonTOffreSupportPublicites as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTOffreSupportPublicites[] = $obj;
                        }
                    }
                }

                $this->collCommonTOffreSupportPublicites = $collCommonTOffreSupportPublicites;
                $this->collCommonTOffreSupportPublicitesPartial = false;
            }
        }

        return $this->collCommonTOffreSupportPublicites;
    }

    /**
     * Sets a collection of CommonTOffreSupportPublicite objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTOffreSupportPublicites A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setCommonTOffreSupportPublicites(PropelCollection $commonTOffreSupportPublicites, PropelPDO $con = null)
    {
        $commonTOffreSupportPublicitesToDelete = $this->getCommonTOffreSupportPublicites(new Criteria(), $con)->diff($commonTOffreSupportPublicites);


        $this->commonTOffreSupportPublicitesScheduledForDeletion = $commonTOffreSupportPublicitesToDelete;

        foreach ($commonTOffreSupportPublicitesToDelete as $commonTOffreSupportPubliciteRemoved) {
            $commonTOffreSupportPubliciteRemoved->setCommonTSupportPublication(null);
        }

        $this->collCommonTOffreSupportPublicites = null;
        foreach ($commonTOffreSupportPublicites as $commonTOffreSupportPublicite) {
            $this->addCommonTOffreSupportPublicite($commonTOffreSupportPublicite);
        }

        $this->collCommonTOffreSupportPublicites = $commonTOffreSupportPublicites;
        $this->collCommonTOffreSupportPublicitesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTOffreSupportPublicite objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTOffreSupportPublicite objects.
     * @throws PropelException
     */
    public function countCommonTOffreSupportPublicites(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTOffreSupportPublicitesPartial && !$this->isNew();
        if (null === $this->collCommonTOffreSupportPublicites || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTOffreSupportPublicites) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTOffreSupportPublicites());
            }
            $query = CommonTOffreSupportPubliciteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTSupportPublication($this)
                ->count($con);
        }

        return count($this->collCommonTOffreSupportPublicites);
    }

    /**
     * Method called to associate a CommonTOffreSupportPublicite object to this object
     * through the CommonTOffreSupportPublicite foreign key attribute.
     *
     * @param   CommonTOffreSupportPublicite $l CommonTOffreSupportPublicite
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function addCommonTOffreSupportPublicite(CommonTOffreSupportPublicite $l)
    {
        if ($this->collCommonTOffreSupportPublicites === null) {
            $this->initCommonTOffreSupportPublicites();
            $this->collCommonTOffreSupportPublicitesPartial = true;
        }
        if (!in_array($l, $this->collCommonTOffreSupportPublicites->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTOffreSupportPublicite($l);
        }

        return $this;
    }

    /**
     * @param	CommonTOffreSupportPublicite $commonTOffreSupportPublicite The commonTOffreSupportPublicite object to add.
     */
    protected function doAddCommonTOffreSupportPublicite($commonTOffreSupportPublicite)
    {
        $this->collCommonTOffreSupportPublicites[]= $commonTOffreSupportPublicite;
        $commonTOffreSupportPublicite->setCommonTSupportPublication($this);
    }

    /**
     * @param	CommonTOffreSupportPublicite $commonTOffreSupportPublicite The commonTOffreSupportPublicite object to remove.
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function removeCommonTOffreSupportPublicite($commonTOffreSupportPublicite)
    {
        if ($this->getCommonTOffreSupportPublicites()->contains($commonTOffreSupportPublicite)) {
            $this->collCommonTOffreSupportPublicites->remove($this->collCommonTOffreSupportPublicites->search($commonTOffreSupportPublicite));
            if (null === $this->commonTOffreSupportPublicitesScheduledForDeletion) {
                $this->commonTOffreSupportPublicitesScheduledForDeletion = clone $this->collCommonTOffreSupportPublicites;
                $this->commonTOffreSupportPublicitesScheduledForDeletion->clear();
            }
            $this->commonTOffreSupportPublicitesScheduledForDeletion[]= clone $commonTOffreSupportPublicite;
            $commonTOffreSupportPublicite->setCommonTSupportPublication(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTSupportAnnonceConsultations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTSupportPublication The current object (for fluent API support)
     * @see        addCommonTSupportAnnonceConsultations()
     */
    public function clearCommonTSupportAnnonceConsultations()
    {
        $this->collCommonTSupportAnnonceConsultations = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTSupportAnnonceConsultationsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTSupportAnnonceConsultations collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTSupportAnnonceConsultations($v = true)
    {
        $this->collCommonTSupportAnnonceConsultationsPartial = $v;
    }

    /**
     * Initializes the collCommonTSupportAnnonceConsultations collection.
     *
     * By default this just sets the collCommonTSupportAnnonceConsultations collection to an empty array (like clearcollCommonTSupportAnnonceConsultations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTSupportAnnonceConsultations($overrideExisting = true)
    {
        if (null !== $this->collCommonTSupportAnnonceConsultations && !$overrideExisting) {
            return;
        }
        $this->collCommonTSupportAnnonceConsultations = new PropelObjectCollection();
        $this->collCommonTSupportAnnonceConsultations->setModel('CommonTSupportAnnonceConsultation');
    }

    /**
     * Gets an array of CommonTSupportAnnonceConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTSupportPublication is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTSupportAnnonceConsultation[] List of CommonTSupportAnnonceConsultation objects
     * @throws PropelException
     */
    public function getCommonTSupportAnnonceConsultations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTSupportAnnonceConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonTSupportAnnonceConsultations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTSupportAnnonceConsultations) {
                // return empty collection
                $this->initCommonTSupportAnnonceConsultations();
            } else {
                $collCommonTSupportAnnonceConsultations = CommonTSupportAnnonceConsultationQuery::create(null, $criteria)
                    ->filterByCommonTSupportPublication($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTSupportAnnonceConsultationsPartial && count($collCommonTSupportAnnonceConsultations)) {
                      $this->initCommonTSupportAnnonceConsultations(false);

                      foreach ($collCommonTSupportAnnonceConsultations as $obj) {
                        if (false == $this->collCommonTSupportAnnonceConsultations->contains($obj)) {
                          $this->collCommonTSupportAnnonceConsultations->append($obj);
                        }
                      }

                      $this->collCommonTSupportAnnonceConsultationsPartial = true;
                    }

                    $collCommonTSupportAnnonceConsultations->getInternalIterator()->rewind();

                    return $collCommonTSupportAnnonceConsultations;
                }

                if ($partial && $this->collCommonTSupportAnnonceConsultations) {
                    foreach ($this->collCommonTSupportAnnonceConsultations as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTSupportAnnonceConsultations[] = $obj;
                        }
                    }
                }

                $this->collCommonTSupportAnnonceConsultations = $collCommonTSupportAnnonceConsultations;
                $this->collCommonTSupportAnnonceConsultationsPartial = false;
            }
        }

        return $this->collCommonTSupportAnnonceConsultations;
    }

    /**
     * Sets a collection of CommonTSupportAnnonceConsultation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTSupportAnnonceConsultations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function setCommonTSupportAnnonceConsultations(PropelCollection $commonTSupportAnnonceConsultations, PropelPDO $con = null)
    {
        $commonTSupportAnnonceConsultationsToDelete = $this->getCommonTSupportAnnonceConsultations(new Criteria(), $con)->diff($commonTSupportAnnonceConsultations);


        $this->commonTSupportAnnonceConsultationsScheduledForDeletion = $commonTSupportAnnonceConsultationsToDelete;

        foreach ($commonTSupportAnnonceConsultationsToDelete as $commonTSupportAnnonceConsultationRemoved) {
            $commonTSupportAnnonceConsultationRemoved->setCommonTSupportPublication(null);
        }

        $this->collCommonTSupportAnnonceConsultations = null;
        foreach ($commonTSupportAnnonceConsultations as $commonTSupportAnnonceConsultation) {
            $this->addCommonTSupportAnnonceConsultation($commonTSupportAnnonceConsultation);
        }

        $this->collCommonTSupportAnnonceConsultations = $commonTSupportAnnonceConsultations;
        $this->collCommonTSupportAnnonceConsultationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTSupportAnnonceConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTSupportAnnonceConsultation objects.
     * @throws PropelException
     */
    public function countCommonTSupportAnnonceConsultations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTSupportAnnonceConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonTSupportAnnonceConsultations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTSupportAnnonceConsultations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTSupportAnnonceConsultations());
            }
            $query = CommonTSupportAnnonceConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTSupportPublication($this)
                ->count($con);
        }

        return count($this->collCommonTSupportAnnonceConsultations);
    }

    /**
     * Method called to associate a CommonTSupportAnnonceConsultation object to this object
     * through the CommonTSupportAnnonceConsultation foreign key attribute.
     *
     * @param   CommonTSupportAnnonceConsultation $l CommonTSupportAnnonceConsultation
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function addCommonTSupportAnnonceConsultation(CommonTSupportAnnonceConsultation $l)
    {
        if ($this->collCommonTSupportAnnonceConsultations === null) {
            $this->initCommonTSupportAnnonceConsultations();
            $this->collCommonTSupportAnnonceConsultationsPartial = true;
        }
        if (!in_array($l, $this->collCommonTSupportAnnonceConsultations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTSupportAnnonceConsultation($l);
        }

        return $this;
    }

    /**
     * @param	CommonTSupportAnnonceConsultation $commonTSupportAnnonceConsultation The commonTSupportAnnonceConsultation object to add.
     */
    protected function doAddCommonTSupportAnnonceConsultation($commonTSupportAnnonceConsultation)
    {
        $this->collCommonTSupportAnnonceConsultations[]= $commonTSupportAnnonceConsultation;
        $commonTSupportAnnonceConsultation->setCommonTSupportPublication($this);
    }

    /**
     * @param	CommonTSupportAnnonceConsultation $commonTSupportAnnonceConsultation The commonTSupportAnnonceConsultation object to remove.
     * @return CommonTSupportPublication The current object (for fluent API support)
     */
    public function removeCommonTSupportAnnonceConsultation($commonTSupportAnnonceConsultation)
    {
        if ($this->getCommonTSupportAnnonceConsultations()->contains($commonTSupportAnnonceConsultation)) {
            $this->collCommonTSupportAnnonceConsultations->remove($this->collCommonTSupportAnnonceConsultations->search($commonTSupportAnnonceConsultation));
            if (null === $this->commonTSupportAnnonceConsultationsScheduledForDeletion) {
                $this->commonTSupportAnnonceConsultationsScheduledForDeletion = clone $this->collCommonTSupportAnnonceConsultations;
                $this->commonTSupportAnnonceConsultationsScheduledForDeletion->clear();
            }
            $this->commonTSupportAnnonceConsultationsScheduledForDeletion[]= clone $commonTSupportAnnonceConsultation;
            $commonTSupportAnnonceConsultation->setCommonTSupportPublication(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTSupportPublication is new, it will return
     * an empty collection; or if this CommonTSupportPublication has previously
     * been saved, it will retrieve related CommonTSupportAnnonceConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTSupportPublication.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTSupportAnnonceConsultation[] List of CommonTSupportAnnonceConsultation objects
     */
    public function getCommonTSupportAnnonceConsultationsJoinCommonTAnnonceConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTSupportAnnonceConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonTAnnonceConsultation', $join_behavior);

        return $this->getCommonTSupportAnnonceConsultations($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->image_logo = null;
        $this->nom = null;
        $this->visible = null;
        $this->ordre = null;
        $this->default_value = null;
        $this->code = null;
        $this->actif = null;
        $this->url = null;
        $this->groupe = null;
        $this->tag_debut_fin_groupe = null;
        $this->description = null;
        $this->nbre_total_groupe = null;
        $this->lieux_execution = null;
        $this->detail_info = null;
        $this->type_info = null;
        $this->affichage_infos = null;
        $this->affichage_message_support = null;
        $this->selection_departements_parution = null;
        $this->departements_parution = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTOffreSupportPublicites) {
                foreach ($this->collCommonTOffreSupportPublicites as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTSupportAnnonceConsultations) {
                foreach ($this->collCommonTSupportAnnonceConsultations as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTOffreSupportPublicites instanceof PropelCollection) {
            $this->collCommonTOffreSupportPublicites->clearIterator();
        }
        $this->collCommonTOffreSupportPublicites = null;
        if ($this->collCommonTSupportAnnonceConsultations instanceof PropelCollection) {
            $this->collCommonTSupportAnnonceConsultations->clearIterator();
        }
        $this->collCommonTSupportAnnonceConsultations = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTSupportPublicationPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
