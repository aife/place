<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTAttestationsOffres;
use Application\Propel\Mpe\CommonTAttestationsOffresPeer;
use Application\Propel\Mpe\CommonTAttestationsOffresQuery;

/**
 * Base class that represents a query for the 't_attestations_offres' table.
 *
 *
 *
 * @method CommonTAttestationsOffresQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTAttestationsOffresQuery orderByIdOffre($order = Criteria::ASC) Order by the id_offre column
 * @method CommonTAttestationsOffresQuery orderByIdDocumentEntreprise($order = Criteria::ASC) Order by the id_document_entreprise column
 * @method CommonTAttestationsOffresQuery orderByIdDocumentVersion($order = Criteria::ASC) Order by the id_document_version column
 * @method CommonTAttestationsOffresQuery orderByIdTypeDocument($order = Criteria::ASC) Order by the id_type_document column
 *
 * @method CommonTAttestationsOffresQuery groupById() Group by the id column
 * @method CommonTAttestationsOffresQuery groupByIdOffre() Group by the id_offre column
 * @method CommonTAttestationsOffresQuery groupByIdDocumentEntreprise() Group by the id_document_entreprise column
 * @method CommonTAttestationsOffresQuery groupByIdDocumentVersion() Group by the id_document_version column
 * @method CommonTAttestationsOffresQuery groupByIdTypeDocument() Group by the id_type_document column
 *
 * @method CommonTAttestationsOffresQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTAttestationsOffresQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTAttestationsOffresQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTAttestationsOffres findOne(PropelPDO $con = null) Return the first CommonTAttestationsOffres matching the query
 * @method CommonTAttestationsOffres findOneOrCreate(PropelPDO $con = null) Return the first CommonTAttestationsOffres matching the query, or a new CommonTAttestationsOffres object populated from the query conditions when no match is found
 *
 * @method CommonTAttestationsOffres findOneByIdOffre(int $id_offre) Return the first CommonTAttestationsOffres filtered by the id_offre column
 * @method CommonTAttestationsOffres findOneByIdDocumentEntreprise(int $id_document_entreprise) Return the first CommonTAttestationsOffres filtered by the id_document_entreprise column
 * @method CommonTAttestationsOffres findOneByIdDocumentVersion(int $id_document_version) Return the first CommonTAttestationsOffres filtered by the id_document_version column
 * @method CommonTAttestationsOffres findOneByIdTypeDocument(int $id_type_document) Return the first CommonTAttestationsOffres filtered by the id_type_document column
 *
 * @method array findById(int $id) Return CommonTAttestationsOffres objects filtered by the id column
 * @method array findByIdOffre(int $id_offre) Return CommonTAttestationsOffres objects filtered by the id_offre column
 * @method array findByIdDocumentEntreprise(int $id_document_entreprise) Return CommonTAttestationsOffres objects filtered by the id_document_entreprise column
 * @method array findByIdDocumentVersion(int $id_document_version) Return CommonTAttestationsOffres objects filtered by the id_document_version column
 * @method array findByIdTypeDocument(int $id_type_document) Return CommonTAttestationsOffres objects filtered by the id_type_document column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTAttestationsOffresQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTAttestationsOffresQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTAttestationsOffres', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTAttestationsOffresQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTAttestationsOffresQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTAttestationsOffresQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTAttestationsOffresQuery) {
            return $criteria;
        }
        $query = new CommonTAttestationsOffresQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTAttestationsOffres|CommonTAttestationsOffres[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTAttestationsOffresPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTAttestationsOffresPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTAttestationsOffres A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTAttestationsOffres A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_offre`, `id_document_entreprise`, `id_document_version`, `id_type_document` FROM `t_attestations_offres` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTAttestationsOffres();
            $obj->hydrate($row);
            CommonTAttestationsOffresPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTAttestationsOffres|CommonTAttestationsOffres[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTAttestationsOffres[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTAttestationsOffresQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTAttestationsOffresPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTAttestationsOffresQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTAttestationsOffresPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAttestationsOffresQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTAttestationsOffresPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTAttestationsOffresPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAttestationsOffresPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_offre column
     *
     * Example usage:
     * <code>
     * $query->filterByIdOffre(1234); // WHERE id_offre = 1234
     * $query->filterByIdOffre(array(12, 34)); // WHERE id_offre IN (12, 34)
     * $query->filterByIdOffre(array('min' => 12)); // WHERE id_offre >= 12
     * $query->filterByIdOffre(array('max' => 12)); // WHERE id_offre <= 12
     * </code>
     *
     * @param     mixed $idOffre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAttestationsOffresQuery The current query, for fluid interface
     */
    public function filterByIdOffre($idOffre = null, $comparison = null)
    {
        if (is_array($idOffre)) {
            $useMinMax = false;
            if (isset($idOffre['min'])) {
                $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_OFFRE, $idOffre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idOffre['max'])) {
                $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_OFFRE, $idOffre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_OFFRE, $idOffre, $comparison);
    }

    /**
     * Filter the query on the id_document_entreprise column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDocumentEntreprise(1234); // WHERE id_document_entreprise = 1234
     * $query->filterByIdDocumentEntreprise(array(12, 34)); // WHERE id_document_entreprise IN (12, 34)
     * $query->filterByIdDocumentEntreprise(array('min' => 12)); // WHERE id_document_entreprise >= 12
     * $query->filterByIdDocumentEntreprise(array('max' => 12)); // WHERE id_document_entreprise <= 12
     * </code>
     *
     * @param     mixed $idDocumentEntreprise The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAttestationsOffresQuery The current query, for fluid interface
     */
    public function filterByIdDocumentEntreprise($idDocumentEntreprise = null, $comparison = null)
    {
        if (is_array($idDocumentEntreprise)) {
            $useMinMax = false;
            if (isset($idDocumentEntreprise['min'])) {
                $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_DOCUMENT_ENTREPRISE, $idDocumentEntreprise['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDocumentEntreprise['max'])) {
                $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_DOCUMENT_ENTREPRISE, $idDocumentEntreprise['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_DOCUMENT_ENTREPRISE, $idDocumentEntreprise, $comparison);
    }

    /**
     * Filter the query on the id_document_version column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDocumentVersion(1234); // WHERE id_document_version = 1234
     * $query->filterByIdDocumentVersion(array(12, 34)); // WHERE id_document_version IN (12, 34)
     * $query->filterByIdDocumentVersion(array('min' => 12)); // WHERE id_document_version >= 12
     * $query->filterByIdDocumentVersion(array('max' => 12)); // WHERE id_document_version <= 12
     * </code>
     *
     * @param     mixed $idDocumentVersion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAttestationsOffresQuery The current query, for fluid interface
     */
    public function filterByIdDocumentVersion($idDocumentVersion = null, $comparison = null)
    {
        if (is_array($idDocumentVersion)) {
            $useMinMax = false;
            if (isset($idDocumentVersion['min'])) {
                $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_DOCUMENT_VERSION, $idDocumentVersion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDocumentVersion['max'])) {
                $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_DOCUMENT_VERSION, $idDocumentVersion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_DOCUMENT_VERSION, $idDocumentVersion, $comparison);
    }

    /**
     * Filter the query on the id_type_document column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeDocument(1234); // WHERE id_type_document = 1234
     * $query->filterByIdTypeDocument(array(12, 34)); // WHERE id_type_document IN (12, 34)
     * $query->filterByIdTypeDocument(array('min' => 12)); // WHERE id_type_document >= 12
     * $query->filterByIdTypeDocument(array('max' => 12)); // WHERE id_type_document <= 12
     * </code>
     *
     * @param     mixed $idTypeDocument The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAttestationsOffresQuery The current query, for fluid interface
     */
    public function filterByIdTypeDocument($idTypeDocument = null, $comparison = null)
    {
        if (is_array($idTypeDocument)) {
            $useMinMax = false;
            if (isset($idTypeDocument['min'])) {
                $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_TYPE_DOCUMENT, $idTypeDocument['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeDocument['max'])) {
                $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_TYPE_DOCUMENT, $idTypeDocument['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAttestationsOffresPeer::ID_TYPE_DOCUMENT, $idTypeDocument, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTAttestationsOffres $commonTAttestationsOffres Object to remove from the list of results
     *
     * @return CommonTAttestationsOffresQuery The current query, for fluid interface
     */
    public function prune($commonTAttestationsOffres = null)
    {
        if ($commonTAttestationsOffres) {
            $this->addUsingAlias(CommonTAttestationsOffresPeer::ID, $commonTAttestationsOffres->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
