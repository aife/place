<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationArchive;
use Application\Propel\Mpe\CommonConsultationArchiveBloc;
use Application\Propel\Mpe\CommonConsultationArchiveBlocPeer;
use Application\Propel\Mpe\CommonConsultationArchiveBlocQuery;
use Application\Propel\Mpe\CommonConsultationArchiveQuery;

/**
 * Base class that represents a row from the 'consultation_archive_bloc' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationArchiveBloc extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonConsultationArchiveBlocPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonConsultationArchiveBlocPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the consultation_archive_id field.
     * @var        int
     */
    protected $consultation_archive_id;

    /**
     * The value for the doc_id field.
     * @var        string
     */
    protected $doc_id;

    /**
     * The value for the chemin_fichier field.
     * @var        string
     */
    protected $chemin_fichier;

    /**
     * The value for the numero_bloc field.
     * @var        int
     */
    protected $numero_bloc;

    /**
     * The value for the poids_bloc field.
     * @var        int
     */
    protected $poids_bloc;

    /**
     * The value for the date_envoi_debut field.
     * @var        string
     */
    protected $date_envoi_debut;

    /**
     * The value for the date_envoi_fin field.
     * @var        string
     */
    protected $date_envoi_fin;

    /**
     * The value for the status_transmission field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $status_transmission;

    /**
     * The value for the erreur field.
     * @var        string
     */
    protected $erreur;

    /**
     * The value for the comp_id field.
     * @var        string
     */
    protected $comp_id;

    /**
     * @var        CommonConsultationArchive
     */
    protected $aCommonConsultationArchive;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->status_transmission = false;
    }

    /**
     * Initializes internal state of BaseCommonConsultationArchiveBloc object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [consultation_archive_id] column value.
     *
     * @return int
     */
    public function getConsultationArchiveId()
    {

        return $this->consultation_archive_id;
    }

    /**
     * Get the [doc_id] column value.
     *
     * @return string
     */
    public function getDocId()
    {

        return $this->doc_id;
    }

    /**
     * Get the [chemin_fichier] column value.
     *
     * @return string
     */
    public function getCheminFichier()
    {

        return $this->chemin_fichier;
    }

    /**
     * Get the [numero_bloc] column value.
     *
     * @return int
     */
    public function getNumeroBloc()
    {

        return $this->numero_bloc;
    }

    /**
     * Get the [poids_bloc] column value.
     *
     * @return int
     */
    public function getPoidsBloc()
    {

        return $this->poids_bloc;
    }

    /**
     * Get the [optionally formatted] temporal [date_envoi_debut] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateEnvoiDebut($format = 'Y-m-d H:i:s')
    {
        if ($this->date_envoi_debut === null) {
            return null;
        }

        if ($this->date_envoi_debut === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_envoi_debut);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_envoi_debut, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_envoi_fin] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateEnvoiFin($format = 'Y-m-d H:i:s')
    {
        if ($this->date_envoi_fin === null) {
            return null;
        }

        if ($this->date_envoi_fin === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_envoi_fin);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_envoi_fin, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [status_transmission] column value.
     *
     * @return boolean
     */
    public function getStatusTransmission()
    {

        return $this->status_transmission;
    }

    /**
     * Get the [erreur] column value.
     *
     * @return string
     */
    public function getErreur()
    {

        return $this->erreur;
    }

    /**
     * Get the [comp_id] column value.
     *
     * @return string
     */
    public function getCompId()
    {

        return $this->comp_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [consultation_archive_id] column.
     *
     * @param int $v new value
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     */
    public function setConsultationArchiveId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->consultation_archive_id !== $v) {
            $this->consultation_archive_id = $v;
            $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::CONSULTATION_ARCHIVE_ID;
        }

        if ($this->aCommonConsultationArchive !== null && $this->aCommonConsultationArchive->getId() !== $v) {
            $this->aCommonConsultationArchive = null;
        }


        return $this;
    } // setConsultationArchiveId()

    /**
     * Set the value of [doc_id] column.
     *
     * @param string $v new value
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     */
    public function setDocId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->doc_id !== $v) {
            $this->doc_id = $v;
            $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::DOC_ID;
        }


        return $this;
    } // setDocId()

    /**
     * Set the value of [chemin_fichier] column.
     *
     * @param string $v new value
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     */
    public function setCheminFichier($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_fichier !== $v) {
            $this->chemin_fichier = $v;
            $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::CHEMIN_FICHIER;
        }


        return $this;
    } // setCheminFichier()

    /**
     * Set the value of [numero_bloc] column.
     *
     * @param int $v new value
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     */
    public function setNumeroBloc($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->numero_bloc !== $v) {
            $this->numero_bloc = $v;
            $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::NUMERO_BLOC;
        }


        return $this;
    } // setNumeroBloc()

    /**
     * Set the value of [poids_bloc] column.
     *
     * @param int $v new value
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     */
    public function setPoidsBloc($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->poids_bloc !== $v) {
            $this->poids_bloc = $v;
            $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::POIDS_BLOC;
        }


        return $this;
    } // setPoidsBloc()

    /**
     * Sets the value of [date_envoi_debut] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     */
    public function setDateEnvoiDebut($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_envoi_debut !== null || $dt !== null) {
            $currentDateAsString = ($this->date_envoi_debut !== null && $tmpDt = new DateTime($this->date_envoi_debut)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_envoi_debut = $newDateAsString;
                $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::DATE_ENVOI_DEBUT;
            }
        } // if either are not null


        return $this;
    } // setDateEnvoiDebut()

    /**
     * Sets the value of [date_envoi_fin] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     */
    public function setDateEnvoiFin($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_envoi_fin !== null || $dt !== null) {
            $currentDateAsString = ($this->date_envoi_fin !== null && $tmpDt = new DateTime($this->date_envoi_fin)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_envoi_fin = $newDateAsString;
                $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::DATE_ENVOI_FIN;
            }
        } // if either are not null


        return $this;
    } // setDateEnvoiFin()

    /**
     * Sets the value of the [status_transmission] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     */
    public function setStatusTransmission($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->status_transmission !== $v) {
            $this->status_transmission = $v;
            $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::STATUS_TRANSMISSION;
        }


        return $this;
    } // setStatusTransmission()

    /**
     * Set the value of [erreur] column.
     *
     * @param string $v new value
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     */
    public function setErreur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->erreur !== $v) {
            $this->erreur = $v;
            $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::ERREUR;
        }


        return $this;
    } // setErreur()

    /**
     * Set the value of [comp_id] column.
     *
     * @param string $v new value
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     */
    public function setCompId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->comp_id !== $v) {
            $this->comp_id = $v;
            $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::COMP_ID;
        }


        return $this;
    } // setCompId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->status_transmission !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->consultation_archive_id = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->doc_id = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->chemin_fichier = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->numero_bloc = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->poids_bloc = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->date_envoi_debut = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->date_envoi_fin = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->status_transmission = ($row[$startcol + 8] !== null) ? (boolean) $row[$startcol + 8] : null;
            $this->erreur = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->comp_id = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 11; // 11 = CommonConsultationArchiveBlocPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonConsultationArchiveBloc object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonConsultationArchive !== null && $this->consultation_archive_id !== $this->aCommonConsultationArchive->getId()) {
            $this->aCommonConsultationArchive = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationArchiveBlocPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonConsultationArchiveBlocPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonConsultationArchive = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationArchiveBlocPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonConsultationArchiveBlocQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationArchiveBlocPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonConsultationArchiveBlocPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonConsultationArchive !== null) {
                if ($this->aCommonConsultationArchive->isModified() || $this->aCommonConsultationArchive->isNew()) {
                    $affectedRows += $this->aCommonConsultationArchive->save($con);
                }
                $this->setCommonConsultationArchive($this->aCommonConsultationArchive);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonConsultationArchiveBlocPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonConsultationArchiveBlocPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::CONSULTATION_ARCHIVE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`consultation_archive_id`';
        }
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::DOC_ID)) {
            $modifiedColumns[':p' . $index++]  = '`doc_id`';
        }
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::CHEMIN_FICHIER)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_fichier`';
        }
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::NUMERO_BLOC)) {
            $modifiedColumns[':p' . $index++]  = '`numero_bloc`';
        }
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::POIDS_BLOC)) {
            $modifiedColumns[':p' . $index++]  = '`poids_bloc`';
        }
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::DATE_ENVOI_DEBUT)) {
            $modifiedColumns[':p' . $index++]  = '`date_envoi_debut`';
        }
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::DATE_ENVOI_FIN)) {
            $modifiedColumns[':p' . $index++]  = '`date_envoi_fin`';
        }
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::STATUS_TRANSMISSION)) {
            $modifiedColumns[':p' . $index++]  = '`status_transmission`';
        }
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::ERREUR)) {
            $modifiedColumns[':p' . $index++]  = '`erreur`';
        }
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::COMP_ID)) {
            $modifiedColumns[':p' . $index++]  = '`comp_id`';
        }

        $sql = sprintf(
            'INSERT INTO `consultation_archive_bloc` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`consultation_archive_id`':
                        $stmt->bindValue($identifier, $this->consultation_archive_id, PDO::PARAM_INT);
                        break;
                    case '`doc_id`':
                        $stmt->bindValue($identifier, $this->doc_id, PDO::PARAM_STR);
                        break;
                    case '`chemin_fichier`':
                        $stmt->bindValue($identifier, $this->chemin_fichier, PDO::PARAM_STR);
                        break;
                    case '`numero_bloc`':
                        $stmt->bindValue($identifier, $this->numero_bloc, PDO::PARAM_INT);
                        break;
                    case '`poids_bloc`':
                        $stmt->bindValue($identifier, $this->poids_bloc, PDO::PARAM_INT);
                        break;
                    case '`date_envoi_debut`':
                        $stmt->bindValue($identifier, $this->date_envoi_debut, PDO::PARAM_STR);
                        break;
                    case '`date_envoi_fin`':
                        $stmt->bindValue($identifier, $this->date_envoi_fin, PDO::PARAM_STR);
                        break;
                    case '`status_transmission`':
                        $stmt->bindValue($identifier, (int) $this->status_transmission, PDO::PARAM_INT);
                        break;
                    case '`erreur`':
                        $stmt->bindValue($identifier, $this->erreur, PDO::PARAM_STR);
                        break;
                    case '`comp_id`':
                        $stmt->bindValue($identifier, $this->comp_id, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonConsultationArchive !== null) {
                if (!$this->aCommonConsultationArchive->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonConsultationArchive->getValidationFailures());
                }
            }


            if (($retval = CommonConsultationArchiveBlocPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonConsultationArchiveBlocPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getConsultationArchiveId();
                break;
            case 2:
                return $this->getDocId();
                break;
            case 3:
                return $this->getCheminFichier();
                break;
            case 4:
                return $this->getNumeroBloc();
                break;
            case 5:
                return $this->getPoidsBloc();
                break;
            case 6:
                return $this->getDateEnvoiDebut();
                break;
            case 7:
                return $this->getDateEnvoiFin();
                break;
            case 8:
                return $this->getStatusTransmission();
                break;
            case 9:
                return $this->getErreur();
                break;
            case 10:
                return $this->getCompId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonConsultationArchiveBloc'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonConsultationArchiveBloc'][$this->getPrimaryKey()] = true;
        $keys = CommonConsultationArchiveBlocPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getConsultationArchiveId(),
            $keys[2] => $this->getDocId(),
            $keys[3] => $this->getCheminFichier(),
            $keys[4] => $this->getNumeroBloc(),
            $keys[5] => $this->getPoidsBloc(),
            $keys[6] => $this->getDateEnvoiDebut(),
            $keys[7] => $this->getDateEnvoiFin(),
            $keys[8] => $this->getStatusTransmission(),
            $keys[9] => $this->getErreur(),
            $keys[10] => $this->getCompId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonConsultationArchive) {
                $result['CommonConsultationArchive'] = $this->aCommonConsultationArchive->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonConsultationArchiveBlocPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setConsultationArchiveId($value);
                break;
            case 2:
                $this->setDocId($value);
                break;
            case 3:
                $this->setCheminFichier($value);
                break;
            case 4:
                $this->setNumeroBloc($value);
                break;
            case 5:
                $this->setPoidsBloc($value);
                break;
            case 6:
                $this->setDateEnvoiDebut($value);
                break;
            case 7:
                $this->setDateEnvoiFin($value);
                break;
            case 8:
                $this->setStatusTransmission($value);
                break;
            case 9:
                $this->setErreur($value);
                break;
            case 10:
                $this->setCompId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonConsultationArchiveBlocPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setConsultationArchiveId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setDocId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCheminFichier($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNumeroBloc($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPoidsBloc($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setDateEnvoiDebut($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setDateEnvoiFin($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setStatusTransmission($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setErreur($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setCompId($arr[$keys[10]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonConsultationArchiveBlocPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::ID)) $criteria->add(CommonConsultationArchiveBlocPeer::ID, $this->id);
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::CONSULTATION_ARCHIVE_ID)) $criteria->add(CommonConsultationArchiveBlocPeer::CONSULTATION_ARCHIVE_ID, $this->consultation_archive_id);
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::DOC_ID)) $criteria->add(CommonConsultationArchiveBlocPeer::DOC_ID, $this->doc_id);
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::CHEMIN_FICHIER)) $criteria->add(CommonConsultationArchiveBlocPeer::CHEMIN_FICHIER, $this->chemin_fichier);
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::NUMERO_BLOC)) $criteria->add(CommonConsultationArchiveBlocPeer::NUMERO_BLOC, $this->numero_bloc);
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::POIDS_BLOC)) $criteria->add(CommonConsultationArchiveBlocPeer::POIDS_BLOC, $this->poids_bloc);
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::DATE_ENVOI_DEBUT)) $criteria->add(CommonConsultationArchiveBlocPeer::DATE_ENVOI_DEBUT, $this->date_envoi_debut);
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::DATE_ENVOI_FIN)) $criteria->add(CommonConsultationArchiveBlocPeer::DATE_ENVOI_FIN, $this->date_envoi_fin);
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::STATUS_TRANSMISSION)) $criteria->add(CommonConsultationArchiveBlocPeer::STATUS_TRANSMISSION, $this->status_transmission);
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::ERREUR)) $criteria->add(CommonConsultationArchiveBlocPeer::ERREUR, $this->erreur);
        if ($this->isColumnModified(CommonConsultationArchiveBlocPeer::COMP_ID)) $criteria->add(CommonConsultationArchiveBlocPeer::COMP_ID, $this->comp_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonConsultationArchiveBlocPeer::DATABASE_NAME);
        $criteria->add(CommonConsultationArchiveBlocPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonConsultationArchiveBloc (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setConsultationArchiveId($this->getConsultationArchiveId());
        $copyObj->setDocId($this->getDocId());
        $copyObj->setCheminFichier($this->getCheminFichier());
        $copyObj->setNumeroBloc($this->getNumeroBloc());
        $copyObj->setPoidsBloc($this->getPoidsBloc());
        $copyObj->setDateEnvoiDebut($this->getDateEnvoiDebut());
        $copyObj->setDateEnvoiFin($this->getDateEnvoiFin());
        $copyObj->setStatusTransmission($this->getStatusTransmission());
        $copyObj->setErreur($this->getErreur());
        $copyObj->setCompId($this->getCompId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonConsultationArchiveBloc Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonConsultationArchiveBlocPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonConsultationArchiveBlocPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonConsultationArchive object.
     *
     * @param   CommonConsultationArchive $v
     * @return CommonConsultationArchiveBloc The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonConsultationArchive(CommonConsultationArchive $v = null)
    {
        if ($v === null) {
            $this->setConsultationArchiveId(NULL);
        } else {
            $this->setConsultationArchiveId($v->getId());
        }

        $this->aCommonConsultationArchive = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonConsultationArchive object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonConsultationArchiveBloc($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonConsultationArchive object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonConsultationArchive The associated CommonConsultationArchive object.
     * @throws PropelException
     */
    public function getCommonConsultationArchive(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonConsultationArchive === null && ($this->consultation_archive_id !== null) && $doQuery) {
            $this->aCommonConsultationArchive = CommonConsultationArchiveQuery::create()->findPk($this->consultation_archive_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonConsultationArchive->addCommonConsultationArchiveBlocs($this);
             */
        }

        return $this->aCommonConsultationArchive;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->consultation_archive_id = null;
        $this->doc_id = null;
        $this->chemin_fichier = null;
        $this->numero_bloc = null;
        $this->poids_bloc = null;
        $this->date_envoi_debut = null;
        $this->date_envoi_fin = null;
        $this->status_transmission = null;
        $this->erreur = null;
        $this->comp_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aCommonConsultationArchive instanceof Persistent) {
              $this->aCommonConsultationArchive->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aCommonConsultationArchive = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonConsultationArchiveBlocPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
