<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTMessageAccueil;
use Application\Propel\Mpe\CommonTMessageAccueilPeer;
use Application\Propel\Mpe\CommonTMessageAccueilQuery;

/**
 * Base class that represents a query for the 't_message_accueil' table.
 *
 *
 *
 * @method CommonTMessageAccueilQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTMessageAccueilQuery orderByTypeMessage($order = Criteria::ASC) Order by the type_message column
 * @method CommonTMessageAccueilQuery orderByContenu($order = Criteria::ASC) Order by the contenu column
 * @method CommonTMessageAccueilQuery orderByDestinataire($order = Criteria::ASC) Order by the destinataire column
 * @method CommonTMessageAccueilQuery orderByAuthentifier($order = Criteria::ASC) Order by the authentifier column
 * @method CommonTMessageAccueilQuery orderByConfig($order = Criteria::ASC) Order by the config column
 *
 * @method CommonTMessageAccueilQuery groupById() Group by the id column
 * @method CommonTMessageAccueilQuery groupByTypeMessage() Group by the type_message column
 * @method CommonTMessageAccueilQuery groupByContenu() Group by the contenu column
 * @method CommonTMessageAccueilQuery groupByDestinataire() Group by the destinataire column
 * @method CommonTMessageAccueilQuery groupByAuthentifier() Group by the authentifier column
 * @method CommonTMessageAccueilQuery groupByConfig() Group by the config column
 *
 * @method CommonTMessageAccueilQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTMessageAccueilQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTMessageAccueilQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTMessageAccueil findOne(PropelPDO $con = null) Return the first CommonTMessageAccueil matching the query
 * @method CommonTMessageAccueil findOneOrCreate(PropelPDO $con = null) Return the first CommonTMessageAccueil matching the query, or a new CommonTMessageAccueil object populated from the query conditions when no match is found
 *
 * @method CommonTMessageAccueil findOneByTypeMessage(string $type_message) Return the first CommonTMessageAccueil filtered by the type_message column
 * @method CommonTMessageAccueil findOneByContenu(string $contenu) Return the first CommonTMessageAccueil filtered by the contenu column
 * @method CommonTMessageAccueil findOneByDestinataire(string $destinataire) Return the first CommonTMessageAccueil filtered by the destinataire column
 * @method CommonTMessageAccueil findOneByAuthentifier(int $authentifier) Return the first CommonTMessageAccueil filtered by the authentifier column
 * @method CommonTMessageAccueil findOneByConfig(string $config) Return the first CommonTMessageAccueil filtered by the config column
 *
 * @method array findById(int $id) Return CommonTMessageAccueil objects filtered by the id column
 * @method array findByTypeMessage(string $type_message) Return CommonTMessageAccueil objects filtered by the type_message column
 * @method array findByContenu(string $contenu) Return CommonTMessageAccueil objects filtered by the contenu column
 * @method array findByDestinataire(string $destinataire) Return CommonTMessageAccueil objects filtered by the destinataire column
 * @method array findByAuthentifier(int $authentifier) Return CommonTMessageAccueil objects filtered by the authentifier column
 * @method array findByConfig(string $config) Return CommonTMessageAccueil objects filtered by the config column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTMessageAccueilQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTMessageAccueilQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTMessageAccueil', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTMessageAccueilQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTMessageAccueilQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTMessageAccueilQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTMessageAccueilQuery) {
            return $criteria;
        }
        $query = new CommonTMessageAccueilQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTMessageAccueil|CommonTMessageAccueil[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTMessageAccueilPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTMessageAccueilPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTMessageAccueil A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTMessageAccueil A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `type_message`, `contenu`, `destinataire`, `authentifier`, `config` FROM `t_message_accueil` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTMessageAccueil();
            $obj->hydrate($row);
            CommonTMessageAccueilPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTMessageAccueil|CommonTMessageAccueil[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTMessageAccueil[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTMessageAccueilQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTMessageAccueilPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTMessageAccueilQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTMessageAccueilPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMessageAccueilQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTMessageAccueilPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTMessageAccueilPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMessageAccueilPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the type_message column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeMessage('fooValue');   // WHERE type_message = 'fooValue'
     * $query->filterByTypeMessage('%fooValue%'); // WHERE type_message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeMessage The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMessageAccueilQuery The current query, for fluid interface
     */
    public function filterByTypeMessage($typeMessage = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeMessage)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeMessage)) {
                $typeMessage = str_replace('*', '%', $typeMessage);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMessageAccueilPeer::TYPE_MESSAGE, $typeMessage, $comparison);
    }

    /**
     * Filter the query on the contenu column
     *
     * Example usage:
     * <code>
     * $query->filterByContenu('fooValue');   // WHERE contenu = 'fooValue'
     * $query->filterByContenu('%fooValue%'); // WHERE contenu LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contenu The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMessageAccueilQuery The current query, for fluid interface
     */
    public function filterByContenu($contenu = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contenu)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $contenu)) {
                $contenu = str_replace('*', '%', $contenu);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMessageAccueilPeer::CONTENU, $contenu, $comparison);
    }

    /**
     * Filter the query on the destinataire column
     *
     * Example usage:
     * <code>
     * $query->filterByDestinataire('fooValue');   // WHERE destinataire = 'fooValue'
     * $query->filterByDestinataire('%fooValue%'); // WHERE destinataire LIKE '%fooValue%'
     * </code>
     *
     * @param     string $destinataire The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMessageAccueilQuery The current query, for fluid interface
     */
    public function filterByDestinataire($destinataire = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($destinataire)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $destinataire)) {
                $destinataire = str_replace('*', '%', $destinataire);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMessageAccueilPeer::DESTINATAIRE, $destinataire, $comparison);
    }

    /**
     * Filter the query on the authentifier column
     *
     * Example usage:
     * <code>
     * $query->filterByAuthentifier(1234); // WHERE authentifier = 1234
     * $query->filterByAuthentifier(array(12, 34)); // WHERE authentifier IN (12, 34)
     * $query->filterByAuthentifier(array('min' => 12)); // WHERE authentifier >= 12
     * $query->filterByAuthentifier(array('max' => 12)); // WHERE authentifier <= 12
     * </code>
     *
     * @param     mixed $authentifier The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMessageAccueilQuery The current query, for fluid interface
     */
    public function filterByAuthentifier($authentifier = null, $comparison = null)
    {
        if (is_array($authentifier)) {
            $useMinMax = false;
            if (isset($authentifier['min'])) {
                $this->addUsingAlias(CommonTMessageAccueilPeer::AUTHENTIFIER, $authentifier['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($authentifier['max'])) {
                $this->addUsingAlias(CommonTMessageAccueilPeer::AUTHENTIFIER, $authentifier['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTMessageAccueilPeer::AUTHENTIFIER, $authentifier, $comparison);
    }

    /**
     * Filter the query on the config column
     *
     * Example usage:
     * <code>
     * $query->filterByConfig('fooValue');   // WHERE config = 'fooValue'
     * $query->filterByConfig('%fooValue%'); // WHERE config LIKE '%fooValue%'
     * </code>
     *
     * @param     string $config The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTMessageAccueilQuery The current query, for fluid interface
     */
    public function filterByConfig($config = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($config)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $config)) {
                $config = str_replace('*', '%', $config);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTMessageAccueilPeer::CONFIG, $config, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTMessageAccueil $commonTMessageAccueil Object to remove from the list of results
     *
     * @return CommonTMessageAccueilQuery The current query, for fluid interface
     */
    public function prune($commonTMessageAccueil = null)
    {
        if ($commonTMessageAccueil) {
            $this->addUsingAlias(CommonTMessageAccueilPeer::ID, $commonTMessageAccueil->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
