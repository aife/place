<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDocumentType;
use Application\Propel\Mpe\CommonTDocumentTypePeer;
use Application\Propel\Mpe\Map\CommonTDocumentTypeTableMap;

/**
 * Base static class for performing query and update operations on the 't_document_type' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTDocumentTypePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_document_type';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTDocumentType';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTDocumentTypeTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 12;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 12;

    /** the column name for the id_type_document field */
    const ID_TYPE_DOCUMENT = 't_document_type.id_type_document';

    /** the column name for the nom_type_document field */
    const NOM_TYPE_DOCUMENT = 't_document_type.nom_type_document';

    /** the column name for the code field */
    const CODE = 't_document_type.code';

    /** the column name for the type_doc_entreprise_etablissement field */
    const TYPE_DOC_ENTREPRISE_ETABLISSEMENT = 't_document_type.type_doc_entreprise_etablissement';

    /** the column name for the uri field */
    const URI = 't_document_type.uri';

    /** the column name for the params_uri field */
    const PARAMS_URI = 't_document_type.params_uri';

    /** the column name for the class_name field */
    const CLASS_NAME = 't_document_type.class_name';

    /** the column name for the nature_document field */
    const NATURE_DOCUMENT = 't_document_type.nature_document';

    /** the column name for the type_retour_ws field */
    const TYPE_RETOUR_WS = 't_document_type.type_retour_ws';

    /** the column name for the synchro_actif field */
    const SYNCHRO_ACTIF = 't_document_type.synchro_actif';

    /** the column name for the message_desactivation_synchro field */
    const MESSAGE_DESACTIVATION_SYNCHRO = 't_document_type.message_desactivation_synchro';

    /** the column name for the afficher_type_doc field */
    const AFFICHER_TYPE_DOC = 't_document_type.afficher_type_doc';

    /** The enumerated values for the type_doc_entreprise_etablissement field */
    const TYPE_DOC_ENTREPRISE_ETABLISSEMENT_1 = '1';
    const TYPE_DOC_ENTREPRISE_ETABLISSEMENT_2 = '2';

    /** The enumerated values for the nature_document field */
    const NATURE_DOCUMENT_1 = '1';
    const NATURE_DOCUMENT_2 = '2';

    /** The enumerated values for the synchro_actif field */
    const SYNCHRO_ACTIF_0 = '0';
    const SYNCHRO_ACTIF_1 = '1';

    /** The enumerated values for the afficher_type_doc field */
    const AFFICHER_TYPE_DOC_0 = '0';
    const AFFICHER_TYPE_DOC_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTDocumentType objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTDocumentType[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTDocumentTypePeer::$fieldNames[CommonTDocumentTypePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('IdTypeDocument', 'NomTypeDocument', 'Code', 'TypeDocEntrepriseEtablissement', 'Uri', 'ParamsUri', 'ClassName', 'NatureDocument', 'TypeRetourWs', 'SynchroActif', 'MessageDesactivationSynchro', 'AfficherTypeDoc', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idTypeDocument', 'nomTypeDocument', 'code', 'typeDocEntrepriseEtablissement', 'uri', 'paramsUri', 'className', 'natureDocument', 'typeRetourWs', 'synchroActif', 'messageDesactivationSynchro', 'afficherTypeDoc', ),
        BasePeer::TYPE_COLNAME => array (CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, CommonTDocumentTypePeer::NOM_TYPE_DOCUMENT, CommonTDocumentTypePeer::CODE, CommonTDocumentTypePeer::TYPE_DOC_ENTREPRISE_ETABLISSEMENT, CommonTDocumentTypePeer::URI, CommonTDocumentTypePeer::PARAMS_URI, CommonTDocumentTypePeer::CLASS_NAME, CommonTDocumentTypePeer::NATURE_DOCUMENT, CommonTDocumentTypePeer::TYPE_RETOUR_WS, CommonTDocumentTypePeer::SYNCHRO_ACTIF, CommonTDocumentTypePeer::MESSAGE_DESACTIVATION_SYNCHRO, CommonTDocumentTypePeer::AFFICHER_TYPE_DOC, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_TYPE_DOCUMENT', 'NOM_TYPE_DOCUMENT', 'CODE', 'TYPE_DOC_ENTREPRISE_ETABLISSEMENT', 'URI', 'PARAMS_URI', 'CLASS_NAME', 'NATURE_DOCUMENT', 'TYPE_RETOUR_WS', 'SYNCHRO_ACTIF', 'MESSAGE_DESACTIVATION_SYNCHRO', 'AFFICHER_TYPE_DOC', ),
        BasePeer::TYPE_FIELDNAME => array ('id_type_document', 'nom_type_document', 'code', 'type_doc_entreprise_etablissement', 'uri', 'params_uri', 'class_name', 'nature_document', 'type_retour_ws', 'synchro_actif', 'message_desactivation_synchro', 'afficher_type_doc', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTDocumentTypePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('IdTypeDocument' => 0, 'NomTypeDocument' => 1, 'Code' => 2, 'TypeDocEntrepriseEtablissement' => 3, 'Uri' => 4, 'ParamsUri' => 5, 'ClassName' => 6, 'NatureDocument' => 7, 'TypeRetourWs' => 8, 'SynchroActif' => 9, 'MessageDesactivationSynchro' => 10, 'AfficherTypeDoc' => 11, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('idTypeDocument' => 0, 'nomTypeDocument' => 1, 'code' => 2, 'typeDocEntrepriseEtablissement' => 3, 'uri' => 4, 'paramsUri' => 5, 'className' => 6, 'natureDocument' => 7, 'typeRetourWs' => 8, 'synchroActif' => 9, 'messageDesactivationSynchro' => 10, 'afficherTypeDoc' => 11, ),
        BasePeer::TYPE_COLNAME => array (CommonTDocumentTypePeer::ID_TYPE_DOCUMENT => 0, CommonTDocumentTypePeer::NOM_TYPE_DOCUMENT => 1, CommonTDocumentTypePeer::CODE => 2, CommonTDocumentTypePeer::TYPE_DOC_ENTREPRISE_ETABLISSEMENT => 3, CommonTDocumentTypePeer::URI => 4, CommonTDocumentTypePeer::PARAMS_URI => 5, CommonTDocumentTypePeer::CLASS_NAME => 6, CommonTDocumentTypePeer::NATURE_DOCUMENT => 7, CommonTDocumentTypePeer::TYPE_RETOUR_WS => 8, CommonTDocumentTypePeer::SYNCHRO_ACTIF => 9, CommonTDocumentTypePeer::MESSAGE_DESACTIVATION_SYNCHRO => 10, CommonTDocumentTypePeer::AFFICHER_TYPE_DOC => 11, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID_TYPE_DOCUMENT' => 0, 'NOM_TYPE_DOCUMENT' => 1, 'CODE' => 2, 'TYPE_DOC_ENTREPRISE_ETABLISSEMENT' => 3, 'URI' => 4, 'PARAMS_URI' => 5, 'CLASS_NAME' => 6, 'NATURE_DOCUMENT' => 7, 'TYPE_RETOUR_WS' => 8, 'SYNCHRO_ACTIF' => 9, 'MESSAGE_DESACTIVATION_SYNCHRO' => 10, 'AFFICHER_TYPE_DOC' => 11, ),
        BasePeer::TYPE_FIELDNAME => array ('id_type_document' => 0, 'nom_type_document' => 1, 'code' => 2, 'type_doc_entreprise_etablissement' => 3, 'uri' => 4, 'params_uri' => 5, 'class_name' => 6, 'nature_document' => 7, 'type_retour_ws' => 8, 'synchro_actif' => 9, 'message_desactivation_synchro' => 10, 'afficher_type_doc' => 11, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTDocumentTypePeer::TYPE_DOC_ENTREPRISE_ETABLISSEMENT => array(
            CommonTDocumentTypePeer::TYPE_DOC_ENTREPRISE_ETABLISSEMENT_1,
            CommonTDocumentTypePeer::TYPE_DOC_ENTREPRISE_ETABLISSEMENT_2,
        ),
        CommonTDocumentTypePeer::NATURE_DOCUMENT => array(
            CommonTDocumentTypePeer::NATURE_DOCUMENT_1,
            CommonTDocumentTypePeer::NATURE_DOCUMENT_2,
        ),
        CommonTDocumentTypePeer::SYNCHRO_ACTIF => array(
            CommonTDocumentTypePeer::SYNCHRO_ACTIF_0,
            CommonTDocumentTypePeer::SYNCHRO_ACTIF_1,
        ),
        CommonTDocumentTypePeer::AFFICHER_TYPE_DOC => array(
            CommonTDocumentTypePeer::AFFICHER_TYPE_DOC_0,
            CommonTDocumentTypePeer::AFFICHER_TYPE_DOC_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTDocumentTypePeer::getFieldNames($toType);
        $key = isset(CommonTDocumentTypePeer::$fieldKeys[$fromType][$name]) ? CommonTDocumentTypePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTDocumentTypePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTDocumentTypePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTDocumentTypePeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTDocumentTypePeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTDocumentTypePeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTDocumentTypePeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTDocumentTypePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTDocumentTypePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT);
            $criteria->addSelectColumn(CommonTDocumentTypePeer::NOM_TYPE_DOCUMENT);
            $criteria->addSelectColumn(CommonTDocumentTypePeer::CODE);
            $criteria->addSelectColumn(CommonTDocumentTypePeer::TYPE_DOC_ENTREPRISE_ETABLISSEMENT);
            $criteria->addSelectColumn(CommonTDocumentTypePeer::URI);
            $criteria->addSelectColumn(CommonTDocumentTypePeer::PARAMS_URI);
            $criteria->addSelectColumn(CommonTDocumentTypePeer::CLASS_NAME);
            $criteria->addSelectColumn(CommonTDocumentTypePeer::NATURE_DOCUMENT);
            $criteria->addSelectColumn(CommonTDocumentTypePeer::TYPE_RETOUR_WS);
            $criteria->addSelectColumn(CommonTDocumentTypePeer::SYNCHRO_ACTIF);
            $criteria->addSelectColumn(CommonTDocumentTypePeer::MESSAGE_DESACTIVATION_SYNCHRO);
            $criteria->addSelectColumn(CommonTDocumentTypePeer::AFFICHER_TYPE_DOC);
        } else {
            $criteria->addSelectColumn($alias . '.id_type_document');
            $criteria->addSelectColumn($alias . '.nom_type_document');
            $criteria->addSelectColumn($alias . '.code');
            $criteria->addSelectColumn($alias . '.type_doc_entreprise_etablissement');
            $criteria->addSelectColumn($alias . '.uri');
            $criteria->addSelectColumn($alias . '.params_uri');
            $criteria->addSelectColumn($alias . '.class_name');
            $criteria->addSelectColumn($alias . '.nature_document');
            $criteria->addSelectColumn($alias . '.type_retour_ws');
            $criteria->addSelectColumn($alias . '.synchro_actif');
            $criteria->addSelectColumn($alias . '.message_desactivation_synchro');
            $criteria->addSelectColumn($alias . '.afficher_type_doc');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTDocumentTypePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTDocumentTypePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTDocumentTypePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTDocumentType
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTDocumentTypePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTDocumentTypePeer::populateObjects(CommonTDocumentTypePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTDocumentTypePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTDocumentTypePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTDocumentType $obj A CommonTDocumentType object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getIdTypeDocument();
            } // if key === null
            CommonTDocumentTypePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTDocumentType object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTDocumentType) {
                $key = (string) $value->getIdTypeDocument();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTDocumentType object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTDocumentTypePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTDocumentType Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTDocumentTypePeer::$instances[$key])) {
                return CommonTDocumentTypePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTDocumentTypePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTDocumentTypePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_document_type
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTDocumentTypePeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTDocumentTypePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTDocumentTypePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTDocumentTypePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTDocumentType object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTDocumentTypePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTDocumentTypePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTDocumentTypePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTDocumentTypePeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTDocumentTypePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTDocumentTypePeer::DATABASE_NAME)->getTable(CommonTDocumentTypePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTDocumentTypePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTDocumentTypePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTDocumentTypeTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTDocumentTypePeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTDocumentType or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTDocumentType object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTDocumentType object
        }

        if ($criteria->containsKey(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT) && $criteria->keyContainsValue(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTDocumentTypePeer::ID_TYPE_DOCUMENT.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTDocumentTypePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTDocumentType or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTDocumentType object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTDocumentTypePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT);
            $value = $criteria->remove(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT);
            if ($value) {
                $selectCriteria->add(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTDocumentTypePeer::TABLE_NAME);
            }

        } else { // $values is CommonTDocumentType object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTDocumentTypePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_document_type table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTDocumentTypePeer::TABLE_NAME, $con, CommonTDocumentTypePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTDocumentTypePeer::clearInstancePool();
            CommonTDocumentTypePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTDocumentType or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTDocumentType object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTDocumentTypePeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTDocumentType) { // it's a model object
            // invalidate the cache for this single object
            CommonTDocumentTypePeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTDocumentTypePeer::DATABASE_NAME);
            $criteria->add(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTDocumentTypePeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTDocumentTypePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTDocumentTypePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTDocumentType object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTDocumentType $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTDocumentTypePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTDocumentTypePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTDocumentTypePeer::DATABASE_NAME, CommonTDocumentTypePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTDocumentType
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTDocumentTypePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTDocumentTypePeer::DATABASE_NAME);
        $criteria->add(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $pk);

        $v = CommonTDocumentTypePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTDocumentType[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTDocumentTypePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTDocumentTypePeer::DATABASE_NAME);
            $criteria->add(CommonTDocumentTypePeer::ID_TYPE_DOCUMENT, $pks, Criteria::IN);
            $objs = CommonTDocumentTypePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTDocumentTypePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTDocumentTypePeer::buildTableMap();

