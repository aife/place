<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTChorusCcag;
use Application\Propel\Mpe\CommonTChorusCcagPeer;
use Application\Propel\Mpe\CommonTChorusCcagQuery;

/**
 * Base class that represents a query for the 't_chorus_ccag' table.
 *
 *
 *
 * @method CommonTChorusCcagQuery orderByIdCcag($order = Criteria::ASC) Order by the id_ccag column
 * @method CommonTChorusCcagQuery orderByReferenceCcagChorus($order = Criteria::ASC) Order by the reference_ccag_chorus column
 * @method CommonTChorusCcagQuery orderByLibelleCcag($order = Criteria::ASC) Order by the libelle_ccag column
 * @method CommonTChorusCcagQuery orderByReferenceCcagOrme($order = Criteria::ASC) Order by the reference_ccag_orme column
 *
 * @method CommonTChorusCcagQuery groupByIdCcag() Group by the id_ccag column
 * @method CommonTChorusCcagQuery groupByReferenceCcagChorus() Group by the reference_ccag_chorus column
 * @method CommonTChorusCcagQuery groupByLibelleCcag() Group by the libelle_ccag column
 * @method CommonTChorusCcagQuery groupByReferenceCcagOrme() Group by the reference_ccag_orme column
 *
 * @method CommonTChorusCcagQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTChorusCcagQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTChorusCcagQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTChorusCcag findOne(PropelPDO $con = null) Return the first CommonTChorusCcag matching the query
 * @method CommonTChorusCcag findOneOrCreate(PropelPDO $con = null) Return the first CommonTChorusCcag matching the query, or a new CommonTChorusCcag object populated from the query conditions when no match is found
 *
 * @method CommonTChorusCcag findOneByReferenceCcagChorus(string $reference_ccag_chorus) Return the first CommonTChorusCcag filtered by the reference_ccag_chorus column
 * @method CommonTChorusCcag findOneByLibelleCcag(string $libelle_ccag) Return the first CommonTChorusCcag filtered by the libelle_ccag column
 * @method CommonTChorusCcag findOneByReferenceCcagOrme(int $reference_ccag_orme) Return the first CommonTChorusCcag filtered by the reference_ccag_orme column
 *
 * @method array findByIdCcag(int $id_ccag) Return CommonTChorusCcag objects filtered by the id_ccag column
 * @method array findByReferenceCcagChorus(string $reference_ccag_chorus) Return CommonTChorusCcag objects filtered by the reference_ccag_chorus column
 * @method array findByLibelleCcag(string $libelle_ccag) Return CommonTChorusCcag objects filtered by the libelle_ccag column
 * @method array findByReferenceCcagOrme(int $reference_ccag_orme) Return CommonTChorusCcag objects filtered by the reference_ccag_orme column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTChorusCcagQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTChorusCcagQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTChorusCcag', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTChorusCcagQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTChorusCcagQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTChorusCcagQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTChorusCcagQuery) {
            return $criteria;
        }
        $query = new CommonTChorusCcagQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTChorusCcag|CommonTChorusCcag[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTChorusCcagPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTChorusCcagPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTChorusCcag A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdCcag($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTChorusCcag A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_ccag`, `reference_ccag_chorus`, `libelle_ccag`, `reference_ccag_orme` FROM `t_chorus_ccag` WHERE `id_ccag` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTChorusCcag();
            $obj->hydrate($row);
            CommonTChorusCcagPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTChorusCcag|CommonTChorusCcag[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTChorusCcag[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTChorusCcagQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTChorusCcagPeer::ID_CCAG, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTChorusCcagQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTChorusCcagPeer::ID_CCAG, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_ccag column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCcag(1234); // WHERE id_ccag = 1234
     * $query->filterByIdCcag(array(12, 34)); // WHERE id_ccag IN (12, 34)
     * $query->filterByIdCcag(array('min' => 12)); // WHERE id_ccag >= 12
     * $query->filterByIdCcag(array('max' => 12)); // WHERE id_ccag <= 12
     * </code>
     *
     * @param     mixed $idCcag The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusCcagQuery The current query, for fluid interface
     */
    public function filterByIdCcag($idCcag = null, $comparison = null)
    {
        if (is_array($idCcag)) {
            $useMinMax = false;
            if (isset($idCcag['min'])) {
                $this->addUsingAlias(CommonTChorusCcagPeer::ID_CCAG, $idCcag['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCcag['max'])) {
                $this->addUsingAlias(CommonTChorusCcagPeer::ID_CCAG, $idCcag['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusCcagPeer::ID_CCAG, $idCcag, $comparison);
    }

    /**
     * Filter the query on the reference_ccag_chorus column
     *
     * Example usage:
     * <code>
     * $query->filterByReferenceCcagChorus('fooValue');   // WHERE reference_ccag_chorus = 'fooValue'
     * $query->filterByReferenceCcagChorus('%fooValue%'); // WHERE reference_ccag_chorus LIKE '%fooValue%'
     * </code>
     *
     * @param     string $referenceCcagChorus The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusCcagQuery The current query, for fluid interface
     */
    public function filterByReferenceCcagChorus($referenceCcagChorus = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($referenceCcagChorus)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $referenceCcagChorus)) {
                $referenceCcagChorus = str_replace('*', '%', $referenceCcagChorus);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusCcagPeer::REFERENCE_CCAG_CHORUS, $referenceCcagChorus, $comparison);
    }

    /**
     * Filter the query on the libelle_ccag column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleCcag('fooValue');   // WHERE libelle_ccag = 'fooValue'
     * $query->filterByLibelleCcag('%fooValue%'); // WHERE libelle_ccag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleCcag The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusCcagQuery The current query, for fluid interface
     */
    public function filterByLibelleCcag($libelleCcag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleCcag)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleCcag)) {
                $libelleCcag = str_replace('*', '%', $libelleCcag);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTChorusCcagPeer::LIBELLE_CCAG, $libelleCcag, $comparison);
    }

    /**
     * Filter the query on the reference_ccag_orme column
     *
     * Example usage:
     * <code>
     * $query->filterByReferenceCcagOrme(1234); // WHERE reference_ccag_orme = 1234
     * $query->filterByReferenceCcagOrme(array(12, 34)); // WHERE reference_ccag_orme IN (12, 34)
     * $query->filterByReferenceCcagOrme(array('min' => 12)); // WHERE reference_ccag_orme >= 12
     * $query->filterByReferenceCcagOrme(array('max' => 12)); // WHERE reference_ccag_orme <= 12
     * </code>
     *
     * @param     mixed $referenceCcagOrme The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTChorusCcagQuery The current query, for fluid interface
     */
    public function filterByReferenceCcagOrme($referenceCcagOrme = null, $comparison = null)
    {
        if (is_array($referenceCcagOrme)) {
            $useMinMax = false;
            if (isset($referenceCcagOrme['min'])) {
                $this->addUsingAlias(CommonTChorusCcagPeer::REFERENCE_CCAG_ORME, $referenceCcagOrme['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($referenceCcagOrme['max'])) {
                $this->addUsingAlias(CommonTChorusCcagPeer::REFERENCE_CCAG_ORME, $referenceCcagOrme['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTChorusCcagPeer::REFERENCE_CCAG_ORME, $referenceCcagOrme, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTChorusCcag $commonTChorusCcag Object to remove from the list of results
     *
     * @return CommonTChorusCcagQuery The current query, for fluid interface
     */
    public function prune($commonTChorusCcag = null)
    {
        if ($commonTChorusCcag) {
            $this->addUsingAlias(CommonTChorusCcagPeer::ID_CCAG, $commonTChorusCcag->getIdCcag(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
