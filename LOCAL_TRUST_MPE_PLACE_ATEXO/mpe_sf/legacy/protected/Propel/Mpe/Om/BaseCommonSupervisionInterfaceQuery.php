<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonSupervisionInterface;
use Application\Propel\Mpe\CommonSupervisionInterfacePeer;
use Application\Propel\Mpe\CommonSupervisionInterfaceQuery;

/**
 * Base class that represents a query for the 'supervision_interface' table.
 *
 *
 *
 * @method CommonSupervisionInterfaceQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonSupervisionInterfaceQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 * @method CommonSupervisionInterfaceQuery orderByWebserviceBatch($order = Criteria::ASC) Order by the webservice_batch column
 * @method CommonSupervisionInterfaceQuery orderByService($order = Criteria::ASC) Order by the service column
 * @method CommonSupervisionInterfaceQuery orderByNomInterface($order = Criteria::ASC) Order by the nom_interface column
 * @method CommonSupervisionInterfaceQuery orderByTotal($order = Criteria::ASC) Order by the total column
 * @method CommonSupervisionInterfaceQuery orderByTotalOk($order = Criteria::ASC) Order by the total_ok column
 * @method CommonSupervisionInterfaceQuery orderByPoids($order = Criteria::ASC) Order by the poids column
 * @method CommonSupervisionInterfaceQuery orderByPoidsOk($order = Criteria::ASC) Order by the poids_ok column
 *
 * @method CommonSupervisionInterfaceQuery groupById() Group by the id column
 * @method CommonSupervisionInterfaceQuery groupByCreatedDate() Group by the created_date column
 * @method CommonSupervisionInterfaceQuery groupByWebserviceBatch() Group by the webservice_batch column
 * @method CommonSupervisionInterfaceQuery groupByService() Group by the service column
 * @method CommonSupervisionInterfaceQuery groupByNomInterface() Group by the nom_interface column
 * @method CommonSupervisionInterfaceQuery groupByTotal() Group by the total column
 * @method CommonSupervisionInterfaceQuery groupByTotalOk() Group by the total_ok column
 * @method CommonSupervisionInterfaceQuery groupByPoids() Group by the poids column
 * @method CommonSupervisionInterfaceQuery groupByPoidsOk() Group by the poids_ok column
 *
 * @method CommonSupervisionInterfaceQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonSupervisionInterfaceQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonSupervisionInterfaceQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonSupervisionInterface findOne(PropelPDO $con = null) Return the first CommonSupervisionInterface matching the query
 * @method CommonSupervisionInterface findOneOrCreate(PropelPDO $con = null) Return the first CommonSupervisionInterface matching the query, or a new CommonSupervisionInterface object populated from the query conditions when no match is found
 *
 * @method CommonSupervisionInterface findOneByCreatedDate(string $created_date) Return the first CommonSupervisionInterface filtered by the created_date column
 * @method CommonSupervisionInterface findOneByWebserviceBatch(string $webservice_batch) Return the first CommonSupervisionInterface filtered by the webservice_batch column
 * @method CommonSupervisionInterface findOneByService(string $service) Return the first CommonSupervisionInterface filtered by the service column
 * @method CommonSupervisionInterface findOneByNomInterface(string $nom_interface) Return the first CommonSupervisionInterface filtered by the nom_interface column
 * @method CommonSupervisionInterface findOneByTotal(int $total) Return the first CommonSupervisionInterface filtered by the total column
 * @method CommonSupervisionInterface findOneByTotalOk(int $total_ok) Return the first CommonSupervisionInterface filtered by the total_ok column
 * @method CommonSupervisionInterface findOneByPoids(string $poids) Return the first CommonSupervisionInterface filtered by the poids column
 * @method CommonSupervisionInterface findOneByPoidsOk(string $poids_ok) Return the first CommonSupervisionInterface filtered by the poids_ok column
 *
 * @method array findById(int $id) Return CommonSupervisionInterface objects filtered by the id column
 * @method array findByCreatedDate(string $created_date) Return CommonSupervisionInterface objects filtered by the created_date column
 * @method array findByWebserviceBatch(string $webservice_batch) Return CommonSupervisionInterface objects filtered by the webservice_batch column
 * @method array findByService(string $service) Return CommonSupervisionInterface objects filtered by the service column
 * @method array findByNomInterface(string $nom_interface) Return CommonSupervisionInterface objects filtered by the nom_interface column
 * @method array findByTotal(int $total) Return CommonSupervisionInterface objects filtered by the total column
 * @method array findByTotalOk(int $total_ok) Return CommonSupervisionInterface objects filtered by the total_ok column
 * @method array findByPoids(string $poids) Return CommonSupervisionInterface objects filtered by the poids column
 * @method array findByPoidsOk(string $poids_ok) Return CommonSupervisionInterface objects filtered by the poids_ok column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonSupervisionInterfaceQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonSupervisionInterfaceQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonSupervisionInterface', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonSupervisionInterfaceQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonSupervisionInterfaceQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonSupervisionInterfaceQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonSupervisionInterfaceQuery) {
            return $criteria;
        }
        $query = new CommonSupervisionInterfaceQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonSupervisionInterface|CommonSupervisionInterface[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonSupervisionInterfacePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonSupervisionInterfacePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonSupervisionInterface A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonSupervisionInterface A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `created_date`, `webservice_batch`, `service`, `nom_interface`, `total`, `total_ok`, `poids`, `poids_ok` FROM `supervision_interface` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonSupervisionInterface();
            $obj->hydrate($row);
            CommonSupervisionInterfacePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonSupervisionInterface|CommonSupervisionInterface[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonSupervisionInterface[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonSupervisionInterfacePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonSupervisionInterfacePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonSupervisionInterfacePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonSupervisionInterfacePeer::CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query on the webservice_batch column
     *
     * Example usage:
     * <code>
     * $query->filterByWebserviceBatch('fooValue');   // WHERE webservice_batch = 'fooValue'
     * $query->filterByWebserviceBatch('%fooValue%'); // WHERE webservice_batch LIKE '%fooValue%'
     * </code>
     *
     * @param     string $webserviceBatch The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function filterByWebserviceBatch($webserviceBatch = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($webserviceBatch)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $webserviceBatch)) {
                $webserviceBatch = str_replace('*', '%', $webserviceBatch);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonSupervisionInterfacePeer::WEBSERVICE_BATCH, $webserviceBatch, $comparison);
    }

    /**
     * Filter the query on the service column
     *
     * Example usage:
     * <code>
     * $query->filterByService('fooValue');   // WHERE service = 'fooValue'
     * $query->filterByService('%fooValue%'); // WHERE service LIKE '%fooValue%'
     * </code>
     *
     * @param     string $service The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function filterByService($service = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($service)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $service)) {
                $service = str_replace('*', '%', $service);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonSupervisionInterfacePeer::SERVICE, $service, $comparison);
    }

    /**
     * Filter the query on the nom_interface column
     *
     * Example usage:
     * <code>
     * $query->filterByNomInterface('fooValue');   // WHERE nom_interface = 'fooValue'
     * $query->filterByNomInterface('%fooValue%'); // WHERE nom_interface LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomInterface The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function filterByNomInterface($nomInterface = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomInterface)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomInterface)) {
                $nomInterface = str_replace('*', '%', $nomInterface);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonSupervisionInterfacePeer::NOM_INTERFACE, $nomInterface, $comparison);
    }

    /**
     * Filter the query on the total column
     *
     * Example usage:
     * <code>
     * $query->filterByTotal(1234); // WHERE total = 1234
     * $query->filterByTotal(array(12, 34)); // WHERE total IN (12, 34)
     * $query->filterByTotal(array('min' => 12)); // WHERE total >= 12
     * $query->filterByTotal(array('max' => 12)); // WHERE total <= 12
     * </code>
     *
     * @param     mixed $total The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function filterByTotal($total = null, $comparison = null)
    {
        if (is_array($total)) {
            $useMinMax = false;
            if (isset($total['min'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::TOTAL, $total['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($total['max'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::TOTAL, $total['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonSupervisionInterfacePeer::TOTAL, $total, $comparison);
    }

    /**
     * Filter the query on the total_ok column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalOk(1234); // WHERE total_ok = 1234
     * $query->filterByTotalOk(array(12, 34)); // WHERE total_ok IN (12, 34)
     * $query->filterByTotalOk(array('min' => 12)); // WHERE total_ok >= 12
     * $query->filterByTotalOk(array('max' => 12)); // WHERE total_ok <= 12
     * </code>
     *
     * @param     mixed $totalOk The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function filterByTotalOk($totalOk = null, $comparison = null)
    {
        if (is_array($totalOk)) {
            $useMinMax = false;
            if (isset($totalOk['min'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::TOTAL_OK, $totalOk['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalOk['max'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::TOTAL_OK, $totalOk['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonSupervisionInterfacePeer::TOTAL_OK, $totalOk, $comparison);
    }

    /**
     * Filter the query on the poids column
     *
     * Example usage:
     * <code>
     * $query->filterByPoids(1234); // WHERE poids = 1234
     * $query->filterByPoids(array(12, 34)); // WHERE poids IN (12, 34)
     * $query->filterByPoids(array('min' => 12)); // WHERE poids >= 12
     * $query->filterByPoids(array('max' => 12)); // WHERE poids <= 12
     * </code>
     *
     * @param     mixed $poids The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function filterByPoids($poids = null, $comparison = null)
    {
        if (is_array($poids)) {
            $useMinMax = false;
            if (isset($poids['min'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::POIDS, $poids['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($poids['max'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::POIDS, $poids['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonSupervisionInterfacePeer::POIDS, $poids, $comparison);
    }

    /**
     * Filter the query on the poids_ok column
     *
     * Example usage:
     * <code>
     * $query->filterByPoidsOk(1234); // WHERE poids_ok = 1234
     * $query->filterByPoidsOk(array(12, 34)); // WHERE poids_ok IN (12, 34)
     * $query->filterByPoidsOk(array('min' => 12)); // WHERE poids_ok >= 12
     * $query->filterByPoidsOk(array('max' => 12)); // WHERE poids_ok <= 12
     * </code>
     *
     * @param     mixed $poidsOk The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function filterByPoidsOk($poidsOk = null, $comparison = null)
    {
        if (is_array($poidsOk)) {
            $useMinMax = false;
            if (isset($poidsOk['min'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::POIDS_OK, $poidsOk['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($poidsOk['max'])) {
                $this->addUsingAlias(CommonSupervisionInterfacePeer::POIDS_OK, $poidsOk['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonSupervisionInterfacePeer::POIDS_OK, $poidsOk, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonSupervisionInterface $commonSupervisionInterface Object to remove from the list of results
     *
     * @return CommonSupervisionInterfaceQuery The current query, for fluid interface
     */
    public function prune($commonSupervisionInterface = null)
    {
        if ($commonSupervisionInterface) {
            $this->addUsingAlias(CommonSupervisionInterfacePeer::ID, $commonSupervisionInterface->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
