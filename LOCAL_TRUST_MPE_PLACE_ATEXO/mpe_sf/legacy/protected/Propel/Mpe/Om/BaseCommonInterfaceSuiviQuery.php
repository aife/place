<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAnnonceBoamp;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonInterfaceSuivi;
use Application\Propel\Mpe\CommonInterfaceSuiviPeer;
use Application\Propel\Mpe\CommonInterfaceSuiviQuery;

/**
 * Base class that represents a query for the 'interface_suivi' table.
 *
 *
 *
 * @method CommonInterfaceSuiviQuery orderByUuid($order = Criteria::ASC) Order by the uuid column
 * @method CommonInterfaceSuiviQuery orderByIdConsultation($order = Criteria::ASC) Order by the id_consultation column
 * @method CommonInterfaceSuiviQuery orderByFlux($order = Criteria::ASC) Order by the flux column
 * @method CommonInterfaceSuiviQuery orderByAction($order = Criteria::ASC) Order by the action column
 * @method CommonInterfaceSuiviQuery orderByStatut($order = Criteria::ASC) Order by the statut column
 * @method CommonInterfaceSuiviQuery orderByMessage($order = Criteria::ASC) Order by the message column
 * @method CommonInterfaceSuiviQuery orderByIdObjetDestination($order = Criteria::ASC) Order by the id_objet_destination column
 * @method CommonInterfaceSuiviQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonInterfaceSuiviQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonInterfaceSuiviQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonInterfaceSuiviQuery orderByMessageDetails($order = Criteria::ASC) Order by the message_details column
 * @method CommonInterfaceSuiviQuery orderByEtape($order = Criteria::ASC) Order by the etape column
 * @method CommonInterfaceSuiviQuery orderByAnnonceId($order = Criteria::ASC) Order by the annonce_id column
 *
 * @method CommonInterfaceSuiviQuery groupByUuid() Group by the uuid column
 * @method CommonInterfaceSuiviQuery groupByIdConsultation() Group by the id_consultation column
 * @method CommonInterfaceSuiviQuery groupByFlux() Group by the flux column
 * @method CommonInterfaceSuiviQuery groupByAction() Group by the action column
 * @method CommonInterfaceSuiviQuery groupByStatut() Group by the statut column
 * @method CommonInterfaceSuiviQuery groupByMessage() Group by the message column
 * @method CommonInterfaceSuiviQuery groupByIdObjetDestination() Group by the id_objet_destination column
 * @method CommonInterfaceSuiviQuery groupByDateCreation() Group by the date_creation column
 * @method CommonInterfaceSuiviQuery groupByDateModification() Group by the date_modification column
 * @method CommonInterfaceSuiviQuery groupById() Group by the id column
 * @method CommonInterfaceSuiviQuery groupByMessageDetails() Group by the message_details column
 * @method CommonInterfaceSuiviQuery groupByEtape() Group by the etape column
 * @method CommonInterfaceSuiviQuery groupByAnnonceId() Group by the annonce_id column
 *
 * @method CommonInterfaceSuiviQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonInterfaceSuiviQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonInterfaceSuiviQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonInterfaceSuiviQuery leftJoinCommonAnnonceBoamp($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAnnonceBoamp relation
 * @method CommonInterfaceSuiviQuery rightJoinCommonAnnonceBoamp($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAnnonceBoamp relation
 * @method CommonInterfaceSuiviQuery innerJoinCommonAnnonceBoamp($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAnnonceBoamp relation
 *
 * @method CommonInterfaceSuiviQuery leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonInterfaceSuiviQuery rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonInterfaceSuiviQuery innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonInterfaceSuivi findOne(PropelPDO $con = null) Return the first CommonInterfaceSuivi matching the query
 * @method CommonInterfaceSuivi findOneOrCreate(PropelPDO $con = null) Return the first CommonInterfaceSuivi matching the query, or a new CommonInterfaceSuivi object populated from the query conditions when no match is found
 *
 * @method CommonInterfaceSuivi findOneByUuid(string $uuid) Return the first CommonInterfaceSuivi filtered by the uuid column
 * @method CommonInterfaceSuivi findOneByIdConsultation(int $id_consultation) Return the first CommonInterfaceSuivi filtered by the id_consultation column
 * @method CommonInterfaceSuivi findOneByFlux(string $flux) Return the first CommonInterfaceSuivi filtered by the flux column
 * @method CommonInterfaceSuivi findOneByAction(string $action) Return the first CommonInterfaceSuivi filtered by the action column
 * @method CommonInterfaceSuivi findOneByStatut(int $statut) Return the first CommonInterfaceSuivi filtered by the statut column
 * @method CommonInterfaceSuivi findOneByMessage(string $message) Return the first CommonInterfaceSuivi filtered by the message column
 * @method CommonInterfaceSuivi findOneByIdObjetDestination(string $id_objet_destination) Return the first CommonInterfaceSuivi filtered by the id_objet_destination column
 * @method CommonInterfaceSuivi findOneByDateCreation(string $date_creation) Return the first CommonInterfaceSuivi filtered by the date_creation column
 * @method CommonInterfaceSuivi findOneByDateModification(string $date_modification) Return the first CommonInterfaceSuivi filtered by the date_modification column
 * @method CommonInterfaceSuivi findOneByMessageDetails(string $message_details) Return the first CommonInterfaceSuivi filtered by the message_details column
 * @method CommonInterfaceSuivi findOneByEtape(string $etape) Return the first CommonInterfaceSuivi filtered by the etape column
 * @method CommonInterfaceSuivi findOneByAnnonceId(int $annonce_id) Return the first CommonInterfaceSuivi filtered by the annonce_id column
 *
 * @method array findByUuid(string $uuid) Return CommonInterfaceSuivi objects filtered by the uuid column
 * @method array findByIdConsultation(int $id_consultation) Return CommonInterfaceSuivi objects filtered by the id_consultation column
 * @method array findByFlux(string $flux) Return CommonInterfaceSuivi objects filtered by the flux column
 * @method array findByAction(string $action) Return CommonInterfaceSuivi objects filtered by the action column
 * @method array findByStatut(int $statut) Return CommonInterfaceSuivi objects filtered by the statut column
 * @method array findByMessage(string $message) Return CommonInterfaceSuivi objects filtered by the message column
 * @method array findByIdObjetDestination(string $id_objet_destination) Return CommonInterfaceSuivi objects filtered by the id_objet_destination column
 * @method array findByDateCreation(string $date_creation) Return CommonInterfaceSuivi objects filtered by the date_creation column
 * @method array findByDateModification(string $date_modification) Return CommonInterfaceSuivi objects filtered by the date_modification column
 * @method array findById(int $id) Return CommonInterfaceSuivi objects filtered by the id column
 * @method array findByMessageDetails(string $message_details) Return CommonInterfaceSuivi objects filtered by the message_details column
 * @method array findByEtape(string $etape) Return CommonInterfaceSuivi objects filtered by the etape column
 * @method array findByAnnonceId(int $annonce_id) Return CommonInterfaceSuivi objects filtered by the annonce_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonInterfaceSuiviQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonInterfaceSuiviQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonInterfaceSuivi', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonInterfaceSuiviQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonInterfaceSuiviQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonInterfaceSuiviQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonInterfaceSuiviQuery) {
            return $criteria;
        }
        $query = new CommonInterfaceSuiviQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonInterfaceSuivi|CommonInterfaceSuivi[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonInterfaceSuiviPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonInterfaceSuiviPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonInterfaceSuivi A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonInterfaceSuivi A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `uuid`, `id_consultation`, `flux`, `action`, `statut`, `message`, `id_objet_destination`, `date_creation`, `date_modification`, `id`, `message_details`, `etape`, `annonce_id` FROM `interface_suivi` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonInterfaceSuivi();
            $obj->hydrate($row);
            CommonInterfaceSuiviPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonInterfaceSuivi|CommonInterfaceSuivi[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonInterfaceSuivi[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the uuid column
     *
     * Example usage:
     * <code>
     * $query->filterByUuid('fooValue');   // WHERE uuid = 'fooValue'
     * $query->filterByUuid('%fooValue%'); // WHERE uuid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uuid The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByUuid($uuid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuid)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $uuid)) {
                $uuid = str_replace('*', '%', $uuid);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::UUID, $uuid, $comparison);
    }

    /**
     * Filter the query on the id_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByIdConsultation(1234); // WHERE id_consultation = 1234
     * $query->filterByIdConsultation(array(12, 34)); // WHERE id_consultation IN (12, 34)
     * $query->filterByIdConsultation(array('min' => 12)); // WHERE id_consultation >= 12
     * $query->filterByIdConsultation(array('max' => 12)); // WHERE id_consultation <= 12
     * </code>
     *
     * @see       filterByCommonConsultation()
     *
     * @param     mixed $idConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByIdConsultation($idConsultation = null, $comparison = null)
    {
        if (is_array($idConsultation)) {
            $useMinMax = false;
            if (isset($idConsultation['min'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::ID_CONSULTATION, $idConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idConsultation['max'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::ID_CONSULTATION, $idConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::ID_CONSULTATION, $idConsultation, $comparison);
    }

    /**
     * Filter the query on the flux column
     *
     * Example usage:
     * <code>
     * $query->filterByFlux('fooValue');   // WHERE flux = 'fooValue'
     * $query->filterByFlux('%fooValue%'); // WHERE flux LIKE '%fooValue%'
     * </code>
     *
     * @param     string $flux The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByFlux($flux = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($flux)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $flux)) {
                $flux = str_replace('*', '%', $flux);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::FLUX, $flux, $comparison);
    }

    /**
     * Filter the query on the action column
     *
     * Example usage:
     * <code>
     * $query->filterByAction('fooValue');   // WHERE action = 'fooValue'
     * $query->filterByAction('%fooValue%'); // WHERE action LIKE '%fooValue%'
     * </code>
     *
     * @param     string $action The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByAction($action = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($action)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $action)) {
                $action = str_replace('*', '%', $action);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::ACTION, $action, $comparison);
    }

    /**
     * Filter the query on the statut column
     *
     * Example usage:
     * <code>
     * $query->filterByStatut(1234); // WHERE statut = 1234
     * $query->filterByStatut(array(12, 34)); // WHERE statut IN (12, 34)
     * $query->filterByStatut(array('min' => 12)); // WHERE statut >= 12
     * $query->filterByStatut(array('max' => 12)); // WHERE statut <= 12
     * </code>
     *
     * @param     mixed $statut The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByStatut($statut = null, $comparison = null)
    {
        if (is_array($statut)) {
            $useMinMax = false;
            if (isset($statut['min'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::STATUT, $statut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statut['max'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::STATUT, $statut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::STATUT, $statut, $comparison);
    }

    /**
     * Filter the query on the message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE message = 'fooValue'
     * $query->filterByMessage('%fooValue%'); // WHERE message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $message)) {
                $message = str_replace('*', '%', $message);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the id_objet_destination column
     *
     * Example usage:
     * <code>
     * $query->filterByIdObjetDestination('fooValue');   // WHERE id_objet_destination = 'fooValue'
     * $query->filterByIdObjetDestination('%fooValue%'); // WHERE id_objet_destination LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idObjetDestination The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByIdObjetDestination($idObjetDestination = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idObjetDestination)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idObjetDestination)) {
                $idObjetDestination = str_replace('*', '%', $idObjetDestination);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::ID_OBJET_DESTINATION, $idObjetDestination, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('2011-03-14'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification('now'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification(array('max' => 'yesterday')); // WHERE date_modification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (is_array($dateModification)) {
            $useMinMax = false;
            if (isset($dateModification['min'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::DATE_MODIFICATION, $dateModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModification['max'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::DATE_MODIFICATION, $dateModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the message_details column
     *
     * Example usage:
     * <code>
     * $query->filterByMessageDetails('fooValue');   // WHERE message_details = 'fooValue'
     * $query->filterByMessageDetails('%fooValue%'); // WHERE message_details LIKE '%fooValue%'
     * </code>
     *
     * @param     string $messageDetails The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByMessageDetails($messageDetails = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($messageDetails)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $messageDetails)) {
                $messageDetails = str_replace('*', '%', $messageDetails);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::MESSAGE_DETAILS, $messageDetails, $comparison);
    }

    /**
     * Filter the query on the etape column
     *
     * Example usage:
     * <code>
     * $query->filterByEtape('fooValue');   // WHERE etape = 'fooValue'
     * $query->filterByEtape('%fooValue%'); // WHERE etape LIKE '%fooValue%'
     * </code>
     *
     * @param     string $etape The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByEtape($etape = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($etape)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $etape)) {
                $etape = str_replace('*', '%', $etape);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::ETAPE, $etape, $comparison);
    }

    /**
     * Filter the query on the annonce_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAnnonceId(1234); // WHERE annonce_id = 1234
     * $query->filterByAnnonceId(array(12, 34)); // WHERE annonce_id IN (12, 34)
     * $query->filterByAnnonceId(array('min' => 12)); // WHERE annonce_id >= 12
     * $query->filterByAnnonceId(array('max' => 12)); // WHERE annonce_id <= 12
     * </code>
     *
     * @see       filterByCommonAnnonceBoamp()
     *
     * @param     mixed $annonceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function filterByAnnonceId($annonceId = null, $comparison = null)
    {
        if (is_array($annonceId)) {
            $useMinMax = false;
            if (isset($annonceId['min'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::ANNONCE_ID, $annonceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($annonceId['max'])) {
                $this->addUsingAlias(CommonInterfaceSuiviPeer::ANNONCE_ID, $annonceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInterfaceSuiviPeer::ANNONCE_ID, $annonceId, $comparison);
    }

    /**
     * Filter the query by a related CommonAnnonceBoamp object
     *
     * @param   CommonAnnonceBoamp|PropelObjectCollection $commonAnnonceBoamp The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInterfaceSuiviQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAnnonceBoamp($commonAnnonceBoamp, $comparison = null)
    {
        if ($commonAnnonceBoamp instanceof CommonAnnonceBoamp) {
            return $this
                ->addUsingAlias(CommonInterfaceSuiviPeer::ANNONCE_ID, $commonAnnonceBoamp->getIdBoamp(), $comparison);
        } elseif ($commonAnnonceBoamp instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonInterfaceSuiviPeer::ANNONCE_ID, $commonAnnonceBoamp->toKeyValue('IdBoamp', 'IdBoamp'), $comparison);
        } else {
            throw new PropelException('filterByCommonAnnonceBoamp() only accepts arguments of type CommonAnnonceBoamp or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAnnonceBoamp relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function joinCommonAnnonceBoamp($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAnnonceBoamp');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAnnonceBoamp');
        }

        return $this;
    }

    /**
     * Use the CommonAnnonceBoamp relation CommonAnnonceBoamp object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAnnonceBoampQuery A secondary query class using the current class as primary query
     */
    public function useCommonAnnonceBoampQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonAnnonceBoamp($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAnnonceBoamp', '\Application\Propel\Mpe\CommonAnnonceBoampQuery');
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInterfaceSuiviQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonInterfaceSuiviPeer::ID_CONSULTATION, $commonConsultation->getId(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonInterfaceSuiviPeer::ID_CONSULTATION, $commonConsultation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonInterfaceSuivi $commonInterfaceSuivi Object to remove from the list of results
     *
     * @return CommonInterfaceSuiviQuery The current query, for fluid interface
     */
    public function prune($commonInterfaceSuivi = null)
    {
        if ($commonInterfaceSuivi) {
            $this->addUsingAlias(CommonInterfaceSuiviPeer::ID, $commonInterfaceSuivi->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
