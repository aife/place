<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTConsultationComptePub;
use Application\Propel\Mpe\CommonTConsultationComptePubPeer;
use Application\Propel\Mpe\CommonTConsultationComptePubQuery;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTDonneeComplementaireQuery;

/**
 * Base class that represents a row from the 't_consultation_compte_pub' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTConsultationComptePub extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTConsultationComptePubPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTConsultationComptePubPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the id_donnees_complementaires field.
     * @var        int
     */
    protected $id_donnees_complementaires;

    /**
     * The value for the organisme field.
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the id_compte_pub field.
     * @var        int
     */
    protected $id_compte_pub;

    /**
     * The value for the boamp_login field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $boamp_login;

    /**
     * The value for the boamp_password field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $boamp_password;

    /**
     * The value for the boamp_mail field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $boamp_mail;

    /**
     * The value for the boamp_target field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $boamp_target;

    /**
     * The value for the denomination field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $denomination;

    /**
     * The value for the prm field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $prm;

    /**
     * The value for the adresse field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse;

    /**
     * The value for the cp field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $cp;

    /**
     * The value for the ville field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville;

    /**
     * The value for the url field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $url;

    /**
     * The value for the facture_denomination field.
     * @var        string
     */
    protected $facture_denomination;

    /**
     * The value for the facture_adresse field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $facture_adresse;

    /**
     * The value for the facture_cp field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $facture_cp;

    /**
     * The value for the facture_ville field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $facture_ville;

    /**
     * The value for the instance_recours_organisme field.
     * @var        string
     */
    protected $instance_recours_organisme;

    /**
     * The value for the instance_recours_adresse field.
     * @var        string
     */
    protected $instance_recours_adresse;

    /**
     * The value for the instance_recours_cp field.
     * @var        string
     */
    protected $instance_recours_cp;

    /**
     * The value for the instance_recours_ville field.
     * @var        string
     */
    protected $instance_recours_ville;

    /**
     * The value for the instance_recours_url field.
     * @var        string
     */
    protected $instance_recours_url;

    /**
     * The value for the telephone field.
     * @var        string
     */
    protected $telephone;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * @var        CommonTDonneeComplementaire
     */
    protected $aCommonTDonneeComplementaire;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->boamp_login = '';
        $this->boamp_password = '';
        $this->boamp_mail = '';
        $this->boamp_target = '0';
        $this->denomination = '';
        $this->prm = '';
        $this->adresse = '';
        $this->cp = '';
        $this->ville = '';
        $this->url = '';
        $this->facture_adresse = '';
        $this->facture_cp = '';
        $this->facture_ville = '';
    }

    /**
     * Initializes internal state of BaseCommonTConsultationComptePub object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [id_donnees_complementaires] column value.
     *
     * @return int
     */
    public function getIdDonneesComplementaires()
    {

        return $this->id_donnees_complementaires;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [id_compte_pub] column value.
     *
     * @return int
     */
    public function getIdComptePub()
    {

        return $this->id_compte_pub;
    }

    /**
     * Get the [boamp_login] column value.
     *
     * @return string
     */
    public function getBoampLogin()
    {

        return $this->boamp_login;
    }

    /**
     * Get the [boamp_password] column value.
     *
     * @return string
     */
    public function getBoampPassword()
    {

        return $this->boamp_password;
    }

    /**
     * Get the [boamp_mail] column value.
     *
     * @return string
     */
    public function getBoampMail()
    {

        return $this->boamp_mail;
    }

    /**
     * Get the [boamp_target] column value.
     *
     * @return string
     */
    public function getBoampTarget()
    {

        return $this->boamp_target;
    }

    /**
     * Get the [denomination] column value.
     *
     * @return string
     */
    public function getDenomination()
    {

        return $this->denomination;
    }

    /**
     * Get the [prm] column value.
     *
     * @return string
     */
    public function getPrm()
    {

        return $this->prm;
    }

    /**
     * Get the [adresse] column value.
     *
     * @return string
     */
    public function getAdresse()
    {

        return $this->adresse;
    }

    /**
     * Get the [cp] column value.
     *
     * @return string
     */
    public function getCp()
    {

        return $this->cp;
    }

    /**
     * Get the [ville] column value.
     *
     * @return string
     */
    public function getVille()
    {

        return $this->ville;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {

        return $this->url;
    }

    /**
     * Get the [facture_denomination] column value.
     *
     * @return string
     */
    public function getFactureDenomination()
    {

        return $this->facture_denomination;
    }

    /**
     * Get the [facture_adresse] column value.
     *
     * @return string
     */
    public function getFactureAdresse()
    {

        return $this->facture_adresse;
    }

    /**
     * Get the [facture_cp] column value.
     *
     * @return string
     */
    public function getFactureCp()
    {

        return $this->facture_cp;
    }

    /**
     * Get the [facture_ville] column value.
     *
     * @return string
     */
    public function getFactureVille()
    {

        return $this->facture_ville;
    }

    /**
     * Get the [instance_recours_organisme] column value.
     *
     * @return string
     */
    public function getInstanceRecoursOrganisme()
    {

        return $this->instance_recours_organisme;
    }

    /**
     * Get the [instance_recours_adresse] column value.
     *
     * @return string
     */
    public function getInstanceRecoursAdresse()
    {

        return $this->instance_recours_adresse;
    }

    /**
     * Get the [instance_recours_cp] column value.
     *
     * @return string
     */
    public function getInstanceRecoursCp()
    {

        return $this->instance_recours_cp;
    }

    /**
     * Get the [instance_recours_ville] column value.
     *
     * @return string
     */
    public function getInstanceRecoursVille()
    {

        return $this->instance_recours_ville;
    }

    /**
     * Get the [instance_recours_url] column value.
     *
     * @return string
     */
    public function getInstanceRecoursUrl()
    {

        return $this->instance_recours_url;
    }

    /**
     * Get the [telephone] column value.
     *
     * @return string
     */
    public function getTelephone()
    {

        return $this->telephone;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [id_donnees_complementaires] column.
     *
     * @param int $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setIdDonneesComplementaires($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_donnees_complementaires !== $v) {
            $this->id_donnees_complementaires = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::ID_DONNEES_COMPLEMENTAIRES;
        }

        if ($this->aCommonTDonneeComplementaire !== null && $this->aCommonTDonneeComplementaire->getIdDonneeComplementaire() !== $v) {
            $this->aCommonTDonneeComplementaire = null;
        }


        return $this;
    } // setIdDonneesComplementaires()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [id_compte_pub] column.
     *
     * @param int $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setIdComptePub($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_compte_pub !== $v) {
            $this->id_compte_pub = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::ID_COMPTE_PUB;
        }


        return $this;
    } // setIdComptePub()

    /**
     * Set the value of [boamp_login] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setBoampLogin($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->boamp_login !== $v) {
            $this->boamp_login = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::BOAMP_LOGIN;
        }


        return $this;
    } // setBoampLogin()

    /**
     * Set the value of [boamp_password] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setBoampPassword($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->boamp_password !== $v) {
            $this->boamp_password = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::BOAMP_PASSWORD;
        }


        return $this;
    } // setBoampPassword()

    /**
     * Set the value of [boamp_mail] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setBoampMail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->boamp_mail !== $v) {
            $this->boamp_mail = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::BOAMP_MAIL;
        }


        return $this;
    } // setBoampMail()

    /**
     * Set the value of [boamp_target] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setBoampTarget($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->boamp_target !== $v) {
            $this->boamp_target = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::BOAMP_TARGET;
        }


        return $this;
    } // setBoampTarget()

    /**
     * Set the value of [denomination] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setDenomination($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->denomination !== $v) {
            $this->denomination = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::DENOMINATION;
        }


        return $this;
    } // setDenomination()

    /**
     * Set the value of [prm] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setPrm($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prm !== $v) {
            $this->prm = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::PRM;
        }


        return $this;
    } // setPrm()

    /**
     * Set the value of [adresse] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setAdresse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse !== $v) {
            $this->adresse = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::ADRESSE;
        }


        return $this;
    } // setAdresse()

    /**
     * Set the value of [cp] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->cp !== $v) {
            $this->cp = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::CP;
        }


        return $this;
    } // setCp()

    /**
     * Set the value of [ville] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setVille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville !== $v) {
            $this->ville = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::VILLE;
        }


        return $this;
    } // setVille()

    /**
     * Set the value of [url] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::URL;
        }


        return $this;
    } // setUrl()

    /**
     * Set the value of [facture_denomination] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setFactureDenomination($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->facture_denomination !== $v) {
            $this->facture_denomination = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::FACTURE_DENOMINATION;
        }


        return $this;
    } // setFactureDenomination()

    /**
     * Set the value of [facture_adresse] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setFactureAdresse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->facture_adresse !== $v) {
            $this->facture_adresse = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::FACTURE_ADRESSE;
        }


        return $this;
    } // setFactureAdresse()

    /**
     * Set the value of [facture_cp] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setFactureCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->facture_cp !== $v) {
            $this->facture_cp = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::FACTURE_CP;
        }


        return $this;
    } // setFactureCp()

    /**
     * Set the value of [facture_ville] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setFactureVille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->facture_ville !== $v) {
            $this->facture_ville = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::FACTURE_VILLE;
        }


        return $this;
    } // setFactureVille()

    /**
     * Set the value of [instance_recours_organisme] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setInstanceRecoursOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->instance_recours_organisme !== $v) {
            $this->instance_recours_organisme = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ORGANISME;
        }


        return $this;
    } // setInstanceRecoursOrganisme()

    /**
     * Set the value of [instance_recours_adresse] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setInstanceRecoursAdresse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->instance_recours_adresse !== $v) {
            $this->instance_recours_adresse = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ADRESSE;
        }


        return $this;
    } // setInstanceRecoursAdresse()

    /**
     * Set the value of [instance_recours_cp] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setInstanceRecoursCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->instance_recours_cp !== $v) {
            $this->instance_recours_cp = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::INSTANCE_RECOURS_CP;
        }


        return $this;
    } // setInstanceRecoursCp()

    /**
     * Set the value of [instance_recours_ville] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setInstanceRecoursVille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->instance_recours_ville !== $v) {
            $this->instance_recours_ville = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::INSTANCE_RECOURS_VILLE;
        }


        return $this;
    } // setInstanceRecoursVille()

    /**
     * Set the value of [instance_recours_url] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setInstanceRecoursUrl($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->instance_recours_url !== $v) {
            $this->instance_recours_url = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::INSTANCE_RECOURS_URL;
        }


        return $this;
    } // setInstanceRecoursUrl()

    /**
     * Set the value of [telephone] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setTelephone($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->telephone !== $v) {
            $this->telephone = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::TELEPHONE;
        }


        return $this;
    } // setTelephone()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = CommonTConsultationComptePubPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->boamp_login !== '') {
                return false;
            }

            if ($this->boamp_password !== '') {
                return false;
            }

            if ($this->boamp_mail !== '') {
                return false;
            }

            if ($this->boamp_target !== '0') {
                return false;
            }

            if ($this->denomination !== '') {
                return false;
            }

            if ($this->prm !== '') {
                return false;
            }

            if ($this->adresse !== '') {
                return false;
            }

            if ($this->cp !== '') {
                return false;
            }

            if ($this->ville !== '') {
                return false;
            }

            if ($this->url !== '') {
                return false;
            }

            if ($this->facture_adresse !== '') {
                return false;
            }

            if ($this->facture_cp !== '') {
                return false;
            }

            if ($this->facture_ville !== '') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_donnees_complementaires = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->organisme = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->id_compte_pub = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->boamp_login = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->boamp_password = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->boamp_mail = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->boamp_target = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->denomination = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->prm = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->adresse = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->cp = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->ville = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->url = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->facture_denomination = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->facture_adresse = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->facture_cp = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->facture_ville = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->instance_recours_organisme = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->instance_recours_adresse = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->instance_recours_cp = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->instance_recours_ville = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->instance_recours_url = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->telephone = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->email = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 25; // 25 = CommonTConsultationComptePubPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTConsultationComptePub object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTDonneeComplementaire !== null && $this->id_donnees_complementaires !== $this->aCommonTDonneeComplementaire->getIdDonneeComplementaire()) {
            $this->aCommonTDonneeComplementaire = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTConsultationComptePubPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonTDonneeComplementaire = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTConsultationComptePubQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTConsultationComptePubPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTConsultationComptePubPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTDonneeComplementaire !== null) {
                if ($this->aCommonTDonneeComplementaire->isModified() || $this->aCommonTDonneeComplementaire->isNew()) {
                    $affectedRows += $this->aCommonTDonneeComplementaire->save($con);
                }
                $this->setCommonTDonneeComplementaire($this->aCommonTDonneeComplementaire);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTConsultationComptePubPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTConsultationComptePubPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::ID_DONNEES_COMPLEMENTAIRES)) {
            $modifiedColumns[':p' . $index++]  = '`id_donnees_complementaires`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::ID_COMPTE_PUB)) {
            $modifiedColumns[':p' . $index++]  = '`id_compte_pub`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::BOAMP_LOGIN)) {
            $modifiedColumns[':p' . $index++]  = '`boamp_login`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::BOAMP_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = '`boamp_password`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::BOAMP_MAIL)) {
            $modifiedColumns[':p' . $index++]  = '`boamp_mail`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::BOAMP_TARGET)) {
            $modifiedColumns[':p' . $index++]  = '`boamp_target`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::DENOMINATION)) {
            $modifiedColumns[':p' . $index++]  = '`denomination`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::PRM)) {
            $modifiedColumns[':p' . $index++]  = '`prm`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`adresse`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::CP)) {
            $modifiedColumns[':p' . $index++]  = '`cp`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`ville`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::URL)) {
            $modifiedColumns[':p' . $index++]  = '`url`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::FACTURE_DENOMINATION)) {
            $modifiedColumns[':p' . $index++]  = '`facture_denomination`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::FACTURE_ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`facture_adresse`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::FACTURE_CP)) {
            $modifiedColumns[':p' . $index++]  = '`facture_cp`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::FACTURE_VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`facture_ville`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`instance_recours_organisme`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`instance_recours_adresse`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_CP)) {
            $modifiedColumns[':p' . $index++]  = '`instance_recours_cp`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`instance_recours_ville`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_URL)) {
            $modifiedColumns[':p' . $index++]  = '`instance_recours_url`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`telephone`';
        }
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }

        $sql = sprintf(
            'INSERT INTO `t_consultation_compte_pub` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`id_donnees_complementaires`':
                        $stmt->bindValue($identifier, $this->id_donnees_complementaires, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`id_compte_pub`':
                        $stmt->bindValue($identifier, $this->id_compte_pub, PDO::PARAM_INT);
                        break;
                    case '`boamp_login`':
                        $stmt->bindValue($identifier, $this->boamp_login, PDO::PARAM_STR);
                        break;
                    case '`boamp_password`':
                        $stmt->bindValue($identifier, $this->boamp_password, PDO::PARAM_STR);
                        break;
                    case '`boamp_mail`':
                        $stmt->bindValue($identifier, $this->boamp_mail, PDO::PARAM_STR);
                        break;
                    case '`boamp_target`':
                        $stmt->bindValue($identifier, $this->boamp_target, PDO::PARAM_STR);
                        break;
                    case '`denomination`':
                        $stmt->bindValue($identifier, $this->denomination, PDO::PARAM_STR);
                        break;
                    case '`prm`':
                        $stmt->bindValue($identifier, $this->prm, PDO::PARAM_STR);
                        break;
                    case '`adresse`':
                        $stmt->bindValue($identifier, $this->adresse, PDO::PARAM_STR);
                        break;
                    case '`cp`':
                        $stmt->bindValue($identifier, $this->cp, PDO::PARAM_STR);
                        break;
                    case '`ville`':
                        $stmt->bindValue($identifier, $this->ville, PDO::PARAM_STR);
                        break;
                    case '`url`':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case '`facture_denomination`':
                        $stmt->bindValue($identifier, $this->facture_denomination, PDO::PARAM_STR);
                        break;
                    case '`facture_adresse`':
                        $stmt->bindValue($identifier, $this->facture_adresse, PDO::PARAM_STR);
                        break;
                    case '`facture_cp`':
                        $stmt->bindValue($identifier, $this->facture_cp, PDO::PARAM_STR);
                        break;
                    case '`facture_ville`':
                        $stmt->bindValue($identifier, $this->facture_ville, PDO::PARAM_STR);
                        break;
                    case '`instance_recours_organisme`':
                        $stmt->bindValue($identifier, $this->instance_recours_organisme, PDO::PARAM_STR);
                        break;
                    case '`instance_recours_adresse`':
                        $stmt->bindValue($identifier, $this->instance_recours_adresse, PDO::PARAM_STR);
                        break;
                    case '`instance_recours_cp`':
                        $stmt->bindValue($identifier, $this->instance_recours_cp, PDO::PARAM_STR);
                        break;
                    case '`instance_recours_ville`':
                        $stmt->bindValue($identifier, $this->instance_recours_ville, PDO::PARAM_STR);
                        break;
                    case '`instance_recours_url`':
                        $stmt->bindValue($identifier, $this->instance_recours_url, PDO::PARAM_STR);
                        break;
                    case '`telephone`':
                        $stmt->bindValue($identifier, $this->telephone, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTDonneeComplementaire !== null) {
                if (!$this->aCommonTDonneeComplementaire->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTDonneeComplementaire->getValidationFailures());
                }
            }


            if (($retval = CommonTConsultationComptePubPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTConsultationComptePubPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getIdDonneesComplementaires();
                break;
            case 2:
                return $this->getOrganisme();
                break;
            case 3:
                return $this->getIdComptePub();
                break;
            case 4:
                return $this->getBoampLogin();
                break;
            case 5:
                return $this->getBoampPassword();
                break;
            case 6:
                return $this->getBoampMail();
                break;
            case 7:
                return $this->getBoampTarget();
                break;
            case 8:
                return $this->getDenomination();
                break;
            case 9:
                return $this->getPrm();
                break;
            case 10:
                return $this->getAdresse();
                break;
            case 11:
                return $this->getCp();
                break;
            case 12:
                return $this->getVille();
                break;
            case 13:
                return $this->getUrl();
                break;
            case 14:
                return $this->getFactureDenomination();
                break;
            case 15:
                return $this->getFactureAdresse();
                break;
            case 16:
                return $this->getFactureCp();
                break;
            case 17:
                return $this->getFactureVille();
                break;
            case 18:
                return $this->getInstanceRecoursOrganisme();
                break;
            case 19:
                return $this->getInstanceRecoursAdresse();
                break;
            case 20:
                return $this->getInstanceRecoursCp();
                break;
            case 21:
                return $this->getInstanceRecoursVille();
                break;
            case 22:
                return $this->getInstanceRecoursUrl();
                break;
            case 23:
                return $this->getTelephone();
                break;
            case 24:
                return $this->getEmail();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTConsultationComptePub'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTConsultationComptePub'][$this->getPrimaryKey()] = true;
        $keys = CommonTConsultationComptePubPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getIdDonneesComplementaires(),
            $keys[2] => $this->getOrganisme(),
            $keys[3] => $this->getIdComptePub(),
            $keys[4] => $this->getBoampLogin(),
            $keys[5] => $this->getBoampPassword(),
            $keys[6] => $this->getBoampMail(),
            $keys[7] => $this->getBoampTarget(),
            $keys[8] => $this->getDenomination(),
            $keys[9] => $this->getPrm(),
            $keys[10] => $this->getAdresse(),
            $keys[11] => $this->getCp(),
            $keys[12] => $this->getVille(),
            $keys[13] => $this->getUrl(),
            $keys[14] => $this->getFactureDenomination(),
            $keys[15] => $this->getFactureAdresse(),
            $keys[16] => $this->getFactureCp(),
            $keys[17] => $this->getFactureVille(),
            $keys[18] => $this->getInstanceRecoursOrganisme(),
            $keys[19] => $this->getInstanceRecoursAdresse(),
            $keys[20] => $this->getInstanceRecoursCp(),
            $keys[21] => $this->getInstanceRecoursVille(),
            $keys[22] => $this->getInstanceRecoursUrl(),
            $keys[23] => $this->getTelephone(),
            $keys[24] => $this->getEmail(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonTDonneeComplementaire) {
                $result['CommonTDonneeComplementaire'] = $this->aCommonTDonneeComplementaire->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTConsultationComptePubPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setIdDonneesComplementaires($value);
                break;
            case 2:
                $this->setOrganisme($value);
                break;
            case 3:
                $this->setIdComptePub($value);
                break;
            case 4:
                $this->setBoampLogin($value);
                break;
            case 5:
                $this->setBoampPassword($value);
                break;
            case 6:
                $this->setBoampMail($value);
                break;
            case 7:
                $this->setBoampTarget($value);
                break;
            case 8:
                $this->setDenomination($value);
                break;
            case 9:
                $this->setPrm($value);
                break;
            case 10:
                $this->setAdresse($value);
                break;
            case 11:
                $this->setCp($value);
                break;
            case 12:
                $this->setVille($value);
                break;
            case 13:
                $this->setUrl($value);
                break;
            case 14:
                $this->setFactureDenomination($value);
                break;
            case 15:
                $this->setFactureAdresse($value);
                break;
            case 16:
                $this->setFactureCp($value);
                break;
            case 17:
                $this->setFactureVille($value);
                break;
            case 18:
                $this->setInstanceRecoursOrganisme($value);
                break;
            case 19:
                $this->setInstanceRecoursAdresse($value);
                break;
            case 20:
                $this->setInstanceRecoursCp($value);
                break;
            case 21:
                $this->setInstanceRecoursVille($value);
                break;
            case 22:
                $this->setInstanceRecoursUrl($value);
                break;
            case 23:
                $this->setTelephone($value);
                break;
            case 24:
                $this->setEmail($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTConsultationComptePubPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdDonneesComplementaires($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setOrganisme($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setIdComptePub($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setBoampLogin($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setBoampPassword($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setBoampMail($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setBoampTarget($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setDenomination($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setPrm($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setAdresse($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setCp($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setVille($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setUrl($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setFactureDenomination($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setFactureAdresse($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setFactureCp($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setFactureVille($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setInstanceRecoursOrganisme($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setInstanceRecoursAdresse($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setInstanceRecoursCp($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setInstanceRecoursVille($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setInstanceRecoursUrl($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setTelephone($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setEmail($arr[$keys[24]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTConsultationComptePubPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTConsultationComptePubPeer::ID)) $criteria->add(CommonTConsultationComptePubPeer::ID, $this->id);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::ID_DONNEES_COMPLEMENTAIRES)) $criteria->add(CommonTConsultationComptePubPeer::ID_DONNEES_COMPLEMENTAIRES, $this->id_donnees_complementaires);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::ORGANISME)) $criteria->add(CommonTConsultationComptePubPeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::ID_COMPTE_PUB)) $criteria->add(CommonTConsultationComptePubPeer::ID_COMPTE_PUB, $this->id_compte_pub);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::BOAMP_LOGIN)) $criteria->add(CommonTConsultationComptePubPeer::BOAMP_LOGIN, $this->boamp_login);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::BOAMP_PASSWORD)) $criteria->add(CommonTConsultationComptePubPeer::BOAMP_PASSWORD, $this->boamp_password);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::BOAMP_MAIL)) $criteria->add(CommonTConsultationComptePubPeer::BOAMP_MAIL, $this->boamp_mail);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::BOAMP_TARGET)) $criteria->add(CommonTConsultationComptePubPeer::BOAMP_TARGET, $this->boamp_target);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::DENOMINATION)) $criteria->add(CommonTConsultationComptePubPeer::DENOMINATION, $this->denomination);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::PRM)) $criteria->add(CommonTConsultationComptePubPeer::PRM, $this->prm);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::ADRESSE)) $criteria->add(CommonTConsultationComptePubPeer::ADRESSE, $this->adresse);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::CP)) $criteria->add(CommonTConsultationComptePubPeer::CP, $this->cp);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::VILLE)) $criteria->add(CommonTConsultationComptePubPeer::VILLE, $this->ville);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::URL)) $criteria->add(CommonTConsultationComptePubPeer::URL, $this->url);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::FACTURE_DENOMINATION)) $criteria->add(CommonTConsultationComptePubPeer::FACTURE_DENOMINATION, $this->facture_denomination);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::FACTURE_ADRESSE)) $criteria->add(CommonTConsultationComptePubPeer::FACTURE_ADRESSE, $this->facture_adresse);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::FACTURE_CP)) $criteria->add(CommonTConsultationComptePubPeer::FACTURE_CP, $this->facture_cp);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::FACTURE_VILLE)) $criteria->add(CommonTConsultationComptePubPeer::FACTURE_VILLE, $this->facture_ville);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ORGANISME)) $criteria->add(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ORGANISME, $this->instance_recours_organisme);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ADRESSE)) $criteria->add(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_ADRESSE, $this->instance_recours_adresse);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_CP)) $criteria->add(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_CP, $this->instance_recours_cp);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_VILLE)) $criteria->add(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_VILLE, $this->instance_recours_ville);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_URL)) $criteria->add(CommonTConsultationComptePubPeer::INSTANCE_RECOURS_URL, $this->instance_recours_url);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::TELEPHONE)) $criteria->add(CommonTConsultationComptePubPeer::TELEPHONE, $this->telephone);
        if ($this->isColumnModified(CommonTConsultationComptePubPeer::EMAIL)) $criteria->add(CommonTConsultationComptePubPeer::EMAIL, $this->email);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTConsultationComptePubPeer::DATABASE_NAME);
        $criteria->add(CommonTConsultationComptePubPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTConsultationComptePub (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdDonneesComplementaires($this->getIdDonneesComplementaires());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setIdComptePub($this->getIdComptePub());
        $copyObj->setBoampLogin($this->getBoampLogin());
        $copyObj->setBoampPassword($this->getBoampPassword());
        $copyObj->setBoampMail($this->getBoampMail());
        $copyObj->setBoampTarget($this->getBoampTarget());
        $copyObj->setDenomination($this->getDenomination());
        $copyObj->setPrm($this->getPrm());
        $copyObj->setAdresse($this->getAdresse());
        $copyObj->setCp($this->getCp());
        $copyObj->setVille($this->getVille());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setFactureDenomination($this->getFactureDenomination());
        $copyObj->setFactureAdresse($this->getFactureAdresse());
        $copyObj->setFactureCp($this->getFactureCp());
        $copyObj->setFactureVille($this->getFactureVille());
        $copyObj->setInstanceRecoursOrganisme($this->getInstanceRecoursOrganisme());
        $copyObj->setInstanceRecoursAdresse($this->getInstanceRecoursAdresse());
        $copyObj->setInstanceRecoursCp($this->getInstanceRecoursCp());
        $copyObj->setInstanceRecoursVille($this->getInstanceRecoursVille());
        $copyObj->setInstanceRecoursUrl($this->getInstanceRecoursUrl());
        $copyObj->setTelephone($this->getTelephone());
        $copyObj->setEmail($this->getEmail());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTConsultationComptePub Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTConsultationComptePubPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTConsultationComptePubPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonTDonneeComplementaire object.
     *
     * @param   CommonTDonneeComplementaire $v
     * @return CommonTConsultationComptePub The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTDonneeComplementaire(CommonTDonneeComplementaire $v = null)
    {
        if ($v === null) {
            $this->setIdDonneesComplementaires(NULL);
        } else {
            $this->setIdDonneesComplementaires($v->getIdDonneeComplementaire());
        }

        $this->aCommonTDonneeComplementaire = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTDonneeComplementaire object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTConsultationComptePub($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTDonneeComplementaire object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTDonneeComplementaire The associated CommonTDonneeComplementaire object.
     * @throws PropelException
     */
    public function getCommonTDonneeComplementaire(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTDonneeComplementaire === null && ($this->id_donnees_complementaires !== null) && $doQuery) {
            $this->aCommonTDonneeComplementaire = CommonTDonneeComplementaireQuery::create()->findPk($this->id_donnees_complementaires, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTDonneeComplementaire->addCommonTConsultationComptePubs($this);
             */
        }

        return $this->aCommonTDonneeComplementaire;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->id_donnees_complementaires = null;
        $this->organisme = null;
        $this->id_compte_pub = null;
        $this->boamp_login = null;
        $this->boamp_password = null;
        $this->boamp_mail = null;
        $this->boamp_target = null;
        $this->denomination = null;
        $this->prm = null;
        $this->adresse = null;
        $this->cp = null;
        $this->ville = null;
        $this->url = null;
        $this->facture_denomination = null;
        $this->facture_adresse = null;
        $this->facture_cp = null;
        $this->facture_ville = null;
        $this->instance_recours_organisme = null;
        $this->instance_recours_adresse = null;
        $this->instance_recours_cp = null;
        $this->instance_recours_ville = null;
        $this->instance_recours_url = null;
        $this->telephone = null;
        $this->email = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aCommonTDonneeComplementaire instanceof Persistent) {
              $this->aCommonTDonneeComplementaire->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aCommonTDonneeComplementaire = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTConsultationComptePubPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
