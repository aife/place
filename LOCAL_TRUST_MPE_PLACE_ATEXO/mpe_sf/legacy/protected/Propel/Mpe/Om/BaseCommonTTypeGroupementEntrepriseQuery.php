<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Propel\Mpe\CommonTTypeGroupementEntreprise;
use Application\Propel\Mpe\CommonTTypeGroupementEntreprisePeer;
use Application\Propel\Mpe\CommonTTypeGroupementEntrepriseQuery;

/**
 * Base class that represents a query for the 't_type_groupement_entreprise' table.
 *
 *
 *
 * @method CommonTTypeGroupementEntrepriseQuery orderByIdTypeGroupement($order = Criteria::ASC) Order by the id_type_groupement column
 * @method CommonTTypeGroupementEntrepriseQuery orderByLibelleTypeGroupement($order = Criteria::ASC) Order by the libelle_type_groupement column
 *
 * @method CommonTTypeGroupementEntrepriseQuery groupByIdTypeGroupement() Group by the id_type_groupement column
 * @method CommonTTypeGroupementEntrepriseQuery groupByLibelleTypeGroupement() Group by the libelle_type_groupement column
 *
 * @method CommonTTypeGroupementEntrepriseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTTypeGroupementEntrepriseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTTypeGroupementEntrepriseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTTypeGroupementEntrepriseQuery leftJoinCommonTGroupementEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTGroupementEntreprise relation
 * @method CommonTTypeGroupementEntrepriseQuery rightJoinCommonTGroupementEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTGroupementEntreprise relation
 * @method CommonTTypeGroupementEntrepriseQuery innerJoinCommonTGroupementEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTGroupementEntreprise relation
 *
 * @method CommonTTypeGroupementEntreprise findOne(PropelPDO $con = null) Return the first CommonTTypeGroupementEntreprise matching the query
 * @method CommonTTypeGroupementEntreprise findOneOrCreate(PropelPDO $con = null) Return the first CommonTTypeGroupementEntreprise matching the query, or a new CommonTTypeGroupementEntreprise object populated from the query conditions when no match is found
 *
 * @method CommonTTypeGroupementEntreprise findOneByLibelleTypeGroupement(string $libelle_type_groupement) Return the first CommonTTypeGroupementEntreprise filtered by the libelle_type_groupement column
 *
 * @method array findByIdTypeGroupement(int $id_type_groupement) Return CommonTTypeGroupementEntreprise objects filtered by the id_type_groupement column
 * @method array findByLibelleTypeGroupement(string $libelle_type_groupement) Return CommonTTypeGroupementEntreprise objects filtered by the libelle_type_groupement column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTTypeGroupementEntrepriseQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTTypeGroupementEntrepriseQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTTypeGroupementEntreprise', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTTypeGroupementEntrepriseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTTypeGroupementEntrepriseQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTTypeGroupementEntrepriseQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTTypeGroupementEntrepriseQuery) {
            return $criteria;
        }
        $query = new CommonTTypeGroupementEntrepriseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTTypeGroupementEntreprise|CommonTTypeGroupementEntreprise[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTTypeGroupementEntreprisePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeGroupementEntreprisePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTypeGroupementEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdTypeGroupement($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTypeGroupementEntreprise A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_type_groupement`, `libelle_type_groupement` FROM `t_type_groupement_entreprise` WHERE `id_type_groupement` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTTypeGroupementEntreprise();
            $obj->hydrate($row);
            CommonTTypeGroupementEntreprisePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTTypeGroupementEntreprise|CommonTTypeGroupementEntreprise[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTTypeGroupementEntreprise[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTTypeGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTTypeGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTTypeGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTTypeGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_type_groupement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeGroupement(1234); // WHERE id_type_groupement = 1234
     * $query->filterByIdTypeGroupement(array(12, 34)); // WHERE id_type_groupement IN (12, 34)
     * $query->filterByIdTypeGroupement(array('min' => 12)); // WHERE id_type_groupement >= 12
     * $query->filterByIdTypeGroupement(array('max' => 12)); // WHERE id_type_groupement <= 12
     * </code>
     *
     * @param     mixed $idTypeGroupement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByIdTypeGroupement($idTypeGroupement = null, $comparison = null)
    {
        if (is_array($idTypeGroupement)) {
            $useMinMax = false;
            if (isset($idTypeGroupement['min'])) {
                $this->addUsingAlias(CommonTTypeGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $idTypeGroupement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeGroupement['max'])) {
                $this->addUsingAlias(CommonTTypeGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $idTypeGroupement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTypeGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $idTypeGroupement, $comparison);
    }

    /**
     * Filter the query on the libelle_type_groupement column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleTypeGroupement('fooValue');   // WHERE libelle_type_groupement = 'fooValue'
     * $query->filterByLibelleTypeGroupement('%fooValue%'); // WHERE libelle_type_groupement LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleTypeGroupement The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function filterByLibelleTypeGroupement($libelleTypeGroupement = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleTypeGroupement)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleTypeGroupement)) {
                $libelleTypeGroupement = str_replace('*', '%', $libelleTypeGroupement);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeGroupementEntreprisePeer::LIBELLE_TYPE_GROUPEMENT, $libelleTypeGroupement, $comparison);
    }

    /**
     * Filter the query by a related CommonTGroupementEntreprise object
     *
     * @param   CommonTGroupementEntreprise|PropelObjectCollection $commonTGroupementEntreprise  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTTypeGroupementEntrepriseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTGroupementEntreprise($commonTGroupementEntreprise, $comparison = null)
    {
        if ($commonTGroupementEntreprise instanceof CommonTGroupementEntreprise) {
            return $this
                ->addUsingAlias(CommonTTypeGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $commonTGroupementEntreprise->getIdTypeGroupement(), $comparison);
        } elseif ($commonTGroupementEntreprise instanceof PropelObjectCollection) {
            return $this
                ->useCommonTGroupementEntrepriseQuery()
                ->filterByPrimaryKeys($commonTGroupementEntreprise->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTGroupementEntreprise() only accepts arguments of type CommonTGroupementEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTGroupementEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTTypeGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function joinCommonTGroupementEntreprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTGroupementEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTGroupementEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonTGroupementEntreprise relation CommonTGroupementEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTGroupementEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonTGroupementEntrepriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTGroupementEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTGroupementEntreprise', '\Application\Propel\Mpe\CommonTGroupementEntrepriseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTTypeGroupementEntreprise $commonTTypeGroupementEntreprise Object to remove from the list of results
     *
     * @return CommonTTypeGroupementEntrepriseQuery The current query, for fluid interface
     */
    public function prune($commonTTypeGroupementEntreprise = null)
    {
        if ($commonTTypeGroupementEntreprise) {
            $this->addUsingAlias(CommonTTypeGroupementEntreprisePeer::ID_TYPE_GROUPEMENT, $commonTTypeGroupementEntreprise->getIdTypeGroupement(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
