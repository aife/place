<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonTCandidaturePeer;
use Application\Propel\Mpe\CommonTListeLotsCandidature;
use Application\Propel\Mpe\CommonTListeLotsCandidaturePeer;
use Application\Propel\Mpe\Map\CommonTListeLotsCandidatureTableMap;

/**
 * Base static class for performing query and update operations on the 't_liste_lots_candidature' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTListeLotsCandidaturePeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 't_liste_lots_candidature';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTListeLotsCandidature';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTListeLotsCandidatureTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 11;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 11;

    /** the column name for the id field */
    const ID = 't_liste_lots_candidature.id';

    /** the column name for the ref_consultation field */
    const REF_CONSULTATION = 't_liste_lots_candidature.ref_consultation';

    /** the column name for the organisme field */
    const ORGANISME = 't_liste_lots_candidature.organisme';

    /** the column name for the old_id_inscrit field */
    const OLD_ID_INSCRIT = 't_liste_lots_candidature.old_id_inscrit';

    /** the column name for the id_entreprise field */
    const ID_ENTREPRISE = 't_liste_lots_candidature.id_entreprise';

    /** the column name for the id_etablissement field */
    const ID_ETABLISSEMENT = 't_liste_lots_candidature.id_etablissement';

    /** the column name for the status field */
    const STATUS = 't_liste_lots_candidature.status';

    /** the column name for the id_candidature field */
    const ID_CANDIDATURE = 't_liste_lots_candidature.id_candidature';

    /** the column name for the num_lot field */
    const NUM_LOT = 't_liste_lots_candidature.num_lot';

    /** the column name for the consultation_id field */
    const CONSULTATION_ID = 't_liste_lots_candidature.consultation_id';

    /** the column name for the id_inscrit field */
    const ID_INSCRIT = 't_liste_lots_candidature.id_inscrit';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTListeLotsCandidature objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTListeLotsCandidature[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTListeLotsCandidaturePeer::$fieldNames[CommonTListeLotsCandidaturePeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'RefConsultation', 'Organisme', 'OldIdInscrit', 'IdEntreprise', 'IdEtablissement', 'Status', 'IdCandidature', 'NumLot', 'ConsultationId', 'IdInscrit', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'refConsultation', 'organisme', 'oldIdInscrit', 'idEntreprise', 'idEtablissement', 'status', 'idCandidature', 'numLot', 'consultationId', 'idInscrit', ),
        BasePeer::TYPE_COLNAME => array (CommonTListeLotsCandidaturePeer::ID, CommonTListeLotsCandidaturePeer::REF_CONSULTATION, CommonTListeLotsCandidaturePeer::ORGANISME, CommonTListeLotsCandidaturePeer::OLD_ID_INSCRIT, CommonTListeLotsCandidaturePeer::ID_ENTREPRISE, CommonTListeLotsCandidaturePeer::ID_ETABLISSEMENT, CommonTListeLotsCandidaturePeer::STATUS, CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, CommonTListeLotsCandidaturePeer::NUM_LOT, CommonTListeLotsCandidaturePeer::CONSULTATION_ID, CommonTListeLotsCandidaturePeer::ID_INSCRIT, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'REF_CONSULTATION', 'ORGANISME', 'OLD_ID_INSCRIT', 'ID_ENTREPRISE', 'ID_ETABLISSEMENT', 'STATUS', 'ID_CANDIDATURE', 'NUM_LOT', 'CONSULTATION_ID', 'ID_INSCRIT', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'ref_consultation', 'organisme', 'old_id_inscrit', 'id_entreprise', 'id_etablissement', 'status', 'id_candidature', 'num_lot', 'consultation_id', 'id_inscrit', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTListeLotsCandidaturePeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'RefConsultation' => 1, 'Organisme' => 2, 'OldIdInscrit' => 3, 'IdEntreprise' => 4, 'IdEtablissement' => 5, 'Status' => 6, 'IdCandidature' => 7, 'NumLot' => 8, 'ConsultationId' => 9, 'IdInscrit' => 10, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'refConsultation' => 1, 'organisme' => 2, 'oldIdInscrit' => 3, 'idEntreprise' => 4, 'idEtablissement' => 5, 'status' => 6, 'idCandidature' => 7, 'numLot' => 8, 'consultationId' => 9, 'idInscrit' => 10, ),
        BasePeer::TYPE_COLNAME => array (CommonTListeLotsCandidaturePeer::ID => 0, CommonTListeLotsCandidaturePeer::REF_CONSULTATION => 1, CommonTListeLotsCandidaturePeer::ORGANISME => 2, CommonTListeLotsCandidaturePeer::OLD_ID_INSCRIT => 3, CommonTListeLotsCandidaturePeer::ID_ENTREPRISE => 4, CommonTListeLotsCandidaturePeer::ID_ETABLISSEMENT => 5, CommonTListeLotsCandidaturePeer::STATUS => 6, CommonTListeLotsCandidaturePeer::ID_CANDIDATURE => 7, CommonTListeLotsCandidaturePeer::NUM_LOT => 8, CommonTListeLotsCandidaturePeer::CONSULTATION_ID => 9, CommonTListeLotsCandidaturePeer::ID_INSCRIT => 10, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'REF_CONSULTATION' => 1, 'ORGANISME' => 2, 'OLD_ID_INSCRIT' => 3, 'ID_ENTREPRISE' => 4, 'ID_ETABLISSEMENT' => 5, 'STATUS' => 6, 'ID_CANDIDATURE' => 7, 'NUM_LOT' => 8, 'CONSULTATION_ID' => 9, 'ID_INSCRIT' => 10, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'ref_consultation' => 1, 'organisme' => 2, 'old_id_inscrit' => 3, 'id_entreprise' => 4, 'id_etablissement' => 5, 'status' => 6, 'id_candidature' => 7, 'num_lot' => 8, 'consultation_id' => 9, 'id_inscrit' => 10, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTListeLotsCandidaturePeer::getFieldNames($toType);
        $key = isset(CommonTListeLotsCandidaturePeer::$fieldKeys[$fromType][$name]) ? CommonTListeLotsCandidaturePeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTListeLotsCandidaturePeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTListeLotsCandidaturePeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTListeLotsCandidaturePeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTListeLotsCandidaturePeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTListeLotsCandidaturePeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTListeLotsCandidaturePeer::ID);
            $criteria->addSelectColumn(CommonTListeLotsCandidaturePeer::REF_CONSULTATION);
            $criteria->addSelectColumn(CommonTListeLotsCandidaturePeer::ORGANISME);
            $criteria->addSelectColumn(CommonTListeLotsCandidaturePeer::OLD_ID_INSCRIT);
            $criteria->addSelectColumn(CommonTListeLotsCandidaturePeer::ID_ENTREPRISE);
            $criteria->addSelectColumn(CommonTListeLotsCandidaturePeer::ID_ETABLISSEMENT);
            $criteria->addSelectColumn(CommonTListeLotsCandidaturePeer::STATUS);
            $criteria->addSelectColumn(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE);
            $criteria->addSelectColumn(CommonTListeLotsCandidaturePeer::NUM_LOT);
            $criteria->addSelectColumn(CommonTListeLotsCandidaturePeer::CONSULTATION_ID);
            $criteria->addSelectColumn(CommonTListeLotsCandidaturePeer::ID_INSCRIT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.ref_consultation');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.old_id_inscrit');
            $criteria->addSelectColumn($alias . '.id_entreprise');
            $criteria->addSelectColumn($alias . '.id_etablissement');
            $criteria->addSelectColumn($alias . '.status');
            $criteria->addSelectColumn($alias . '.id_candidature');
            $criteria->addSelectColumn($alias . '.num_lot');
            $criteria->addSelectColumn($alias . '.consultation_id');
            $criteria->addSelectColumn($alias . '.id_inscrit');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTListeLotsCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTListeLotsCandidature
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTListeLotsCandidaturePeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTListeLotsCandidaturePeer::populateObjects(CommonTListeLotsCandidaturePeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTListeLotsCandidature $obj A CommonTListeLotsCandidature object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTListeLotsCandidaturePeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTListeLotsCandidature object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTListeLotsCandidature) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTListeLotsCandidature object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTListeLotsCandidaturePeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTListeLotsCandidature Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTListeLotsCandidaturePeer::$instances[$key])) {
                return CommonTListeLotsCandidaturePeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTListeLotsCandidaturePeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTListeLotsCandidaturePeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to t_liste_lots_candidature
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTListeLotsCandidaturePeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTListeLotsCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTListeLotsCandidaturePeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTListeLotsCandidaturePeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTListeLotsCandidature object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTListeLotsCandidaturePeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTListeLotsCandidaturePeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTListeLotsCandidaturePeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTListeLotsCandidaturePeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTListeLotsCandidaturePeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTCandidature table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTCandidature(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTListeLotsCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, CommonTCandidaturePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTListeLotsCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonInscrit table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonInscrit(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTListeLotsCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOrganisme table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonOrganisme(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTListeLotsCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTListeLotsCandidature objects pre-filled with their CommonTCandidature objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTListeLotsCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTCandidature(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
        }

        CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        $startcol = CommonTListeLotsCandidaturePeer::NUM_HYDRATE_COLUMNS;
        CommonTCandidaturePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, CommonTCandidaturePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTListeLotsCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTListeLotsCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTListeLotsCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTListeLotsCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTCandidaturePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTCandidaturePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTCandidaturePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to $obj2 (CommonTCandidature)
                $obj2->addCommonTListeLotsCandidature($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTListeLotsCandidature objects pre-filled with their CommonConsultation objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTListeLotsCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
        }

        CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        $startcol = CommonTListeLotsCandidaturePeer::NUM_HYDRATE_COLUMNS;
        CommonConsultationPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTListeLotsCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTListeLotsCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTListeLotsCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTListeLotsCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to $obj2 (CommonConsultation)
                $obj2->addCommonTListeLotsCandidature($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTListeLotsCandidature objects pre-filled with their CommonInscrit objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTListeLotsCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonInscrit(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
        }

        CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        $startcol = CommonTListeLotsCandidaturePeer::NUM_HYDRATE_COLUMNS;
        CommonInscritPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTListeLotsCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTListeLotsCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTListeLotsCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTListeLotsCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonInscritPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonInscritPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonInscritPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to $obj2 (CommonInscrit)
                $obj2->addCommonTListeLotsCandidature($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTListeLotsCandidature objects pre-filled with their CommonOrganisme objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTListeLotsCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonOrganisme(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
        }

        CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        $startcol = CommonTListeLotsCandidaturePeer::NUM_HYDRATE_COLUMNS;
        CommonOrganismePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTListeLotsCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTListeLotsCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTListeLotsCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTListeLotsCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonOrganismePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonOrganismePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonOrganismePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to $obj2 (CommonOrganisme)
                $obj2->addCommonTListeLotsCandidature($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTListeLotsCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, CommonTCandidaturePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTListeLotsCandidature objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTListeLotsCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
        }

        CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        $startcol2 = CommonTListeLotsCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, CommonTCandidaturePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTListeLotsCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTListeLotsCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTListeLotsCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTListeLotsCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonTCandidature rows

            $key2 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonTCandidaturePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTCandidaturePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTCandidaturePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj2 (CommonTCandidature)
                $obj2->addCommonTListeLotsCandidature($obj1);
            } // if joined row not null

            // Add objects for joined CommonConsultation rows

            $key3 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonConsultationPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj3 (CommonConsultation)
                $obj3->addCommonTListeLotsCandidature($obj1);
            } // if joined row not null

            // Add objects for joined CommonInscrit rows

            $key4 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CommonInscritPeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CommonInscritPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonInscritPeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj4 (CommonInscrit)
                $obj4->addCommonTListeLotsCandidature($obj1);
            } // if joined row not null

            // Add objects for joined CommonOrganisme rows

            $key5 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = CommonOrganismePeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = CommonOrganismePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOrganismePeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj5 (CommonOrganisme)
                $obj5->addCommonTListeLotsCandidature($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTCandidature table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTCandidature(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTListeLotsCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonConsultation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTListeLotsCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, CommonTCandidaturePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonInscrit table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonInscrit(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTListeLotsCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, CommonTCandidaturePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOrganisme table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonOrganisme(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTListeLotsCandidaturePeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, CommonTCandidaturePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTListeLotsCandidature objects pre-filled with all related objects except CommonTCandidature.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTListeLotsCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTCandidature(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
        }

        CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        $startcol2 = CommonTListeLotsCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTListeLotsCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTListeLotsCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTListeLotsCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTListeLotsCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonConsultation rows

                $key2 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonConsultationPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonConsultationPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj2 (CommonConsultation)
                $obj2->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key3 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonInscritPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonInscritPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj3 (CommonInscrit)
                $obj3->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key4 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonOrganismePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonOrganismePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj4 (CommonOrganisme)
                $obj4->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTListeLotsCandidature objects pre-filled with all related objects except CommonConsultation.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTListeLotsCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonConsultation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
        }

        CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        $startcol2 = CommonTListeLotsCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, CommonTCandidaturePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTListeLotsCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTListeLotsCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTListeLotsCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTListeLotsCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTCandidature rows

                $key2 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTCandidaturePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTCandidaturePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTCandidaturePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj2 (CommonTCandidature)
                $obj2->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key3 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonInscritPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonInscritPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj3 (CommonInscrit)
                $obj3->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key4 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonOrganismePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonOrganismePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj4 (CommonOrganisme)
                $obj4->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTListeLotsCandidature objects pre-filled with all related objects except CommonInscrit.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTListeLotsCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonInscrit(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
        }

        CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        $startcol2 = CommonTListeLotsCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, CommonTCandidaturePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTListeLotsCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTListeLotsCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTListeLotsCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTListeLotsCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTCandidature rows

                $key2 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTCandidaturePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTCandidaturePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTCandidaturePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj2 (CommonTCandidature)
                $obj2->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key3 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonConsultationPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj3 (CommonConsultation)
                $obj3->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key4 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonOrganismePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonOrganismePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj4 (CommonOrganisme)
                $obj4->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonTListeLotsCandidature objects pre-filled with all related objects except CommonOrganisme.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTListeLotsCandidature objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonOrganisme(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
        }

        CommonTListeLotsCandidaturePeer::addSelectColumns($criteria);
        $startcol2 = CommonTListeLotsCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonTCandidaturePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS;

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonInscritPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonInscritPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_CANDIDATURE, CommonTCandidaturePeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::CONSULTATION_ID, CommonConsultationPeer::ID, $join_behavior);

        $criteria->addJoin(CommonTListeLotsCandidaturePeer::ID_INSCRIT, CommonInscritPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTListeLotsCandidaturePeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTListeLotsCandidaturePeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTListeLotsCandidaturePeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTListeLotsCandidaturePeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonTCandidature rows

                $key2 = CommonTCandidaturePeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonTCandidaturePeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonTCandidaturePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonTCandidaturePeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj2 (CommonTCandidature)
                $obj2->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonConsultation rows

                $key3 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonConsultationPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonConsultationPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonConsultationPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj3 (CommonConsultation)
                $obj3->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

                // Add objects for joined CommonInscrit rows

                $key4 = CommonInscritPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonInscritPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonInscritPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonInscritPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonTListeLotsCandidature) to the collection in $obj4 (CommonInscrit)
                $obj4->addCommonTListeLotsCandidature($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTListeLotsCandidaturePeer::DATABASE_NAME)->getTable(CommonTListeLotsCandidaturePeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTListeLotsCandidaturePeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTListeLotsCandidaturePeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTListeLotsCandidatureTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTListeLotsCandidaturePeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTListeLotsCandidature or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTListeLotsCandidature object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTListeLotsCandidature object
        }

        if ($criteria->containsKey(CommonTListeLotsCandidaturePeer::ID) && $criteria->keyContainsValue(CommonTListeLotsCandidaturePeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTListeLotsCandidaturePeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTListeLotsCandidature or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTListeLotsCandidature object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTListeLotsCandidaturePeer::ID);
            $value = $criteria->remove(CommonTListeLotsCandidaturePeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTListeLotsCandidaturePeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTListeLotsCandidaturePeer::TABLE_NAME);
            }

        } else { // $values is CommonTListeLotsCandidature object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the t_liste_lots_candidature table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTListeLotsCandidaturePeer::TABLE_NAME, $con, CommonTListeLotsCandidaturePeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTListeLotsCandidaturePeer::clearInstancePool();
            CommonTListeLotsCandidaturePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTListeLotsCandidature or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTListeLotsCandidature object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTListeLotsCandidaturePeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTListeLotsCandidature) { // it's a model object
            // invalidate the cache for this single object
            CommonTListeLotsCandidaturePeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
            $criteria->add(CommonTListeLotsCandidaturePeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTListeLotsCandidaturePeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTListeLotsCandidaturePeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTListeLotsCandidaturePeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTListeLotsCandidature object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTListeLotsCandidature $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTListeLotsCandidaturePeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTListeLotsCandidaturePeer::DATABASE_NAME, CommonTListeLotsCandidaturePeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTListeLotsCandidature
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTListeLotsCandidaturePeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
        $criteria->add(CommonTListeLotsCandidaturePeer::ID, $pk);

        $v = CommonTListeLotsCandidaturePeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTListeLotsCandidature[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTListeLotsCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTListeLotsCandidaturePeer::DATABASE_NAME);
            $criteria->add(CommonTListeLotsCandidaturePeer::ID, $pks, Criteria::IN);
            $objs = CommonTListeLotsCandidaturePeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTListeLotsCandidaturePeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTListeLotsCandidaturePeer::buildTableMap();

