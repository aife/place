<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonDossierVolumineux;
use Application\Propel\Mpe\CommonDossierVolumineuxQuery;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonEnveloppeQuery;

/**
 * Base class that represents a row from the 'Enveloppe' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEnveloppe extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonEnveloppePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonEnveloppePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_enveloppe_electro field.
     * @var        int
     */
    protected $id_enveloppe_electro;

    /**
     * The value for the organisme field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the offre_id field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $offre_id;

    /**
     * The value for the champs_optionnels field.
     * @var        resource
     */
    protected $champs_optionnels;

    /**
     * The value for the fichier field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $fichier;

    /**
     * The value for the supprime field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $supprime;

    /**
     * The value for the cryptage field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $cryptage;

    /**
     * The value for the nom_fichier field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $nom_fichier;

    /**
     * The value for the hash field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $hash;

    /**
     * The value for the type_env field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $type_env;

    /**
     * The value for the sous_pli field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $sous_pli;

    /**
     * The value for the attribue field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $attribue;

    /**
     * The value for the dateheure_ouverture field.
     * Note: this column has a database default value of: '0000-00-00 00:00:00'
     * @var        string
     */
    protected $dateheure_ouverture;

    /**
     * The value for the agent_id_ouverture field.
     * @var        int
     */
    protected $agent_id_ouverture;

    /**
     * The value for the agent_id_ouverture2 field.
     * @var        int
     */
    protected $agent_id_ouverture2;

    /**
     * The value for the donnees_ouverture field.
     * @var        resource
     */
    protected $donnees_ouverture;

    /**
     * The value for the horodatage_donnees_ouverture field.
     * @var        resource
     */
    protected $horodatage_donnees_ouverture;

    /**
     * The value for the statut_enveloppe field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $statut_enveloppe;

    /**
     * The value for the agent_telechargement field.
     * @var        int
     */
    protected $agent_telechargement;

    /**
     * The value for the date_telechargement field.
     * @var        string
     */
    protected $date_telechargement;

    /**
     * The value for the repertoire_telechargement field.
     * @var        string
     */
    protected $repertoire_telechargement;

    /**
     * The value for the nom_agent_ouverture field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $nom_agent_ouverture;

    /**
     * The value for the dateheure_ouverture_agent2 field.
     * Note: this column has a database default value of: '0000-00-00 00:00:00'
     * @var        string
     */
    protected $dateheure_ouverture_agent2;

    /**
     * The value for the enveloppe_fictive field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $enveloppe_fictive;

    /**
     * The value for the integrite_fichier field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $integrite_fichier;

    /**
     * The value for the verification_signature field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $verification_signature;

    /**
     * The value for the uid_response field.
     * @var        string
     */
    protected $uid_response;

    /**
     * The value for the resultat_verification_hash_files field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $resultat_verification_hash_files;

    /**
     * The value for the id_dossier_volumineux field.
     * @var        int
     */
    protected $id_dossier_volumineux;

    /**
     * The value for the date_debut_dechiffrement field.
     * @var        string
     */
    protected $date_debut_dechiffrement;

    /**
     * @var        CommonDossierVolumineux
     */
    protected $aCommonDossierVolumineux;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->organisme = '';
        $this->offre_id = 0;
        $this->fichier = 0;
        $this->supprime = '0';
        $this->cryptage = '1';
        $this->nom_fichier = '';
        $this->hash = '';
        $this->type_env = 0;
        $this->sous_pli = 0;
        $this->attribue = '0';
        $this->dateheure_ouverture = '0000-00-00 00:00:00';
        $this->statut_enveloppe = 1;
        $this->nom_agent_ouverture = '';
        $this->dateheure_ouverture_agent2 = '0000-00-00 00:00:00';
        $this->enveloppe_fictive = '0';
        $this->integrite_fichier = 0;
        $this->verification_signature = '';
        $this->resultat_verification_hash_files = '1';
    }

    /**
     * Initializes internal state of BaseCommonEnveloppe object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_enveloppe_electro] column value.
     *
     * @return int
     */
    public function getIdEnveloppeElectro()
    {

        return $this->id_enveloppe_electro;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [offre_id] column value.
     *
     * @return int
     */
    public function getOffreId()
    {

        return $this->offre_id;
    }

    /**
     * Get the [champs_optionnels] column value.
     *
     * @return resource
     */
    public function getChampsOptionnels()
    {

        return $this->champs_optionnels;
    }

    /**
     * Get the [fichier] column value.
     *
     * @return int
     */
    public function getFichier()
    {

        return $this->fichier;
    }

    /**
     * Get the [supprime] column value.
     *
     * @return string
     */
    public function getSupprime()
    {

        return $this->supprime;
    }

    /**
     * Get the [cryptage] column value.
     *
     * @return string
     */
    public function getCryptage()
    {

        return $this->cryptage;
    }

    /**
     * Get the [nom_fichier] column value.
     *
     * @return string
     */
    public function getNomFichier()
    {

        return $this->nom_fichier;
    }

    /**
     * Get the [hash] column value.
     *
     * @return string
     */
    public function getHash()
    {

        return $this->hash;
    }

    /**
     * Get the [type_env] column value.
     *
     * @return int
     */
    public function getTypeEnv()
    {

        return $this->type_env;
    }

    /**
     * Get the [sous_pli] column value.
     *
     * @return int
     */
    public function getSousPli()
    {

        return $this->sous_pli;
    }

    /**
     * Get the [attribue] column value.
     *
     * @return string
     */
    public function getAttribue()
    {

        return $this->attribue;
    }

    /**
     * Get the [dateheure_ouverture] column value.
     *
     * @return string
     */
    public function getDateheureOuverture()
    {

        return $this->dateheure_ouverture;
    }

    /**
     * Get the [agent_id_ouverture] column value.
     *
     * @return int
     */
    public function getAgentIdOuverture()
    {

        return $this->agent_id_ouverture;
    }

    /**
     * Get the [agent_id_ouverture2] column value.
     *
     * @return int
     */
    public function getAgentIdOuverture2()
    {

        return $this->agent_id_ouverture2;
    }

    /**
     * Get the [donnees_ouverture] column value.
     *
     * @return resource
     */
    public function getDonneesOuverture()
    {

        return $this->donnees_ouverture;
    }

    /**
     * Get the [horodatage_donnees_ouverture] column value.
     *
     * @return resource
     */
    public function getHorodatageDonneesOuverture()
    {

        return $this->horodatage_donnees_ouverture;
    }

    /**
     * Get the [statut_enveloppe] column value.
     *
     * @return int
     */
    public function getStatutEnveloppe()
    {

        return $this->statut_enveloppe;
    }

    /**
     * Get the [agent_telechargement] column value.
     *
     * @return int
     */
    public function getAgentTelechargement()
    {

        return $this->agent_telechargement;
    }

    /**
     * Get the [date_telechargement] column value.
     *
     * @return string
     */
    public function getDateTelechargement()
    {

        return $this->date_telechargement;
    }

    /**
     * Get the [repertoire_telechargement] column value.
     *
     * @return string
     */
    public function getRepertoireTelechargement()
    {

        return $this->repertoire_telechargement;
    }

    /**
     * Get the [nom_agent_ouverture] column value.
     *
     * @return string
     */
    public function getNomAgentOuverture()
    {

        return $this->nom_agent_ouverture;
    }

    /**
     * Get the [dateheure_ouverture_agent2] column value.
     *
     * @return string
     */
    public function getDateheureOuvertureAgent2()
    {

        return $this->dateheure_ouverture_agent2;
    }

    /**
     * Get the [enveloppe_fictive] column value.
     *
     * @return string
     */
    public function getEnveloppeFictive()
    {

        return $this->enveloppe_fictive;
    }

    /**
     * Get the [integrite_fichier] column value.
     *
     * @return int
     */
    public function getIntegriteFichier()
    {

        return $this->integrite_fichier;
    }

    /**
     * Get the [verification_signature] column value.
     *
     * @return string
     */
    public function getVerificationSignature()
    {

        return $this->verification_signature;
    }

    /**
     * Get the [uid_response] column value.
     *
     * @return string
     */
    public function getUidResponse()
    {

        return $this->uid_response;
    }

    /**
     * Get the [resultat_verification_hash_files] column value.
     *
     * @return string
     */
    public function getResultatVerificationHashFiles()
    {

        return $this->resultat_verification_hash_files;
    }

    /**
     * Get the [id_dossier_volumineux] column value.
     *
     * @return int
     */
    public function getIdDossierVolumineux()
    {

        return $this->id_dossier_volumineux;
    }

    /**
     * Get the [optionally formatted] temporal [date_debut_dechiffrement] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateDebutDechiffrement($format = 'Y-m-d H:i:s')
    {
        if ($this->date_debut_dechiffrement === null) {
            return null;
        }

        if ($this->date_debut_dechiffrement === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_debut_dechiffrement);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_debut_dechiffrement, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [id_enveloppe_electro] column.
     *
     * @param int $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setIdEnveloppeElectro($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_enveloppe_electro !== $v) {
            $this->id_enveloppe_electro = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::ID_ENVELOPPE_ELECTRO;
        }


        return $this;
    } // setIdEnveloppeElectro()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [offre_id] column.
     *
     * @param int $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setOffreId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->offre_id !== $v) {
            $this->offre_id = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::OFFRE_ID;
        }


        return $this;
    } // setOffreId()

    /**
     * Set the value of [champs_optionnels] column.
     *
     * @param resource $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setChampsOptionnels($v)
    {
        // Because BLOB columns are streams in PDO we have to assume that they are
        // always modified when a new value is passed in.  For example, the contents
        // of the stream itself may have changed externally.
        if (!is_resource($v) && $v !== null) {
            $this->champs_optionnels = fopen('php://memory', 'r+');
            fwrite($this->champs_optionnels, $v);
            rewind($this->champs_optionnels);
        } else { // it's already a stream
            $this->champs_optionnels = $v;
        }
        $this->modifiedColumns[] = CommonEnveloppePeer::CHAMPS_OPTIONNELS;


        return $this;
    } // setChampsOptionnels()

    /**
     * Set the value of [fichier] column.
     *
     * @param int $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setFichier($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->fichier !== $v) {
            $this->fichier = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::FICHIER;
        }


        return $this;
    } // setFichier()

    /**
     * Set the value of [supprime] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setSupprime($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->supprime !== $v) {
            $this->supprime = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::SUPPRIME;
        }


        return $this;
    } // setSupprime()

    /**
     * Set the value of [cryptage] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setCryptage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->cryptage !== $v) {
            $this->cryptage = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::CRYPTAGE;
        }


        return $this;
    } // setCryptage()

    /**
     * Set the value of [nom_fichier] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setNomFichier($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_fichier !== $v) {
            $this->nom_fichier = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::NOM_FICHIER;
        }


        return $this;
    } // setNomFichier()

    /**
     * Set the value of [hash] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setHash($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->hash !== $v) {
            $this->hash = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::HASH;
        }


        return $this;
    } // setHash()

    /**
     * Set the value of [type_env] column.
     *
     * @param int $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setTypeEnv($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->type_env !== $v) {
            $this->type_env = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::TYPE_ENV;
        }


        return $this;
    } // setTypeEnv()

    /**
     * Set the value of [sous_pli] column.
     *
     * @param int $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setSousPli($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->sous_pli !== $v) {
            $this->sous_pli = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::SOUS_PLI;
        }


        return $this;
    } // setSousPli()

    /**
     * Set the value of [attribue] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setAttribue($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->attribue !== $v) {
            $this->attribue = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::ATTRIBUE;
        }


        return $this;
    } // setAttribue()

    /**
     * Set the value of [dateheure_ouverture] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setDateheureOuverture($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->dateheure_ouverture !== $v) {
            $this->dateheure_ouverture = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::DATEHEURE_OUVERTURE;
        }


        return $this;
    } // setDateheureOuverture()

    /**
     * Set the value of [agent_id_ouverture] column.
     *
     * @param int $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setAgentIdOuverture($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->agent_id_ouverture !== $v) {
            $this->agent_id_ouverture = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::AGENT_ID_OUVERTURE;
        }


        return $this;
    } // setAgentIdOuverture()

    /**
     * Set the value of [agent_id_ouverture2] column.
     *
     * @param int $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setAgentIdOuverture2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->agent_id_ouverture2 !== $v) {
            $this->agent_id_ouverture2 = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::AGENT_ID_OUVERTURE2;
        }


        return $this;
    } // setAgentIdOuverture2()

    /**
     * Set the value of [donnees_ouverture] column.
     *
     * @param resource $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setDonneesOuverture($v)
    {
        // Because BLOB columns are streams in PDO we have to assume that they are
        // always modified when a new value is passed in.  For example, the contents
        // of the stream itself may have changed externally.
        if (!is_resource($v) && $v !== null) {
            $this->donnees_ouverture = fopen('php://memory', 'r+');
            fwrite($this->donnees_ouverture, $v);
            rewind($this->donnees_ouverture);
        } else { // it's already a stream
            $this->donnees_ouverture = $v;
        }
        $this->modifiedColumns[] = CommonEnveloppePeer::DONNEES_OUVERTURE;


        return $this;
    } // setDonneesOuverture()

    /**
     * Set the value of [horodatage_donnees_ouverture] column.
     *
     * @param resource $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setHorodatageDonneesOuverture($v)
    {
        // Because BLOB columns are streams in PDO we have to assume that they are
        // always modified when a new value is passed in.  For example, the contents
        // of the stream itself may have changed externally.
        if (!is_resource($v) && $v !== null) {
            $this->horodatage_donnees_ouverture = fopen('php://memory', 'r+');
            fwrite($this->horodatage_donnees_ouverture, $v);
            rewind($this->horodatage_donnees_ouverture);
        } else { // it's already a stream
            $this->horodatage_donnees_ouverture = $v;
        }
        $this->modifiedColumns[] = CommonEnveloppePeer::HORODATAGE_DONNEES_OUVERTURE;


        return $this;
    } // setHorodatageDonneesOuverture()

    /**
     * Set the value of [statut_enveloppe] column.
     *
     * @param int $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setStatutEnveloppe($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->statut_enveloppe !== $v) {
            $this->statut_enveloppe = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::STATUT_ENVELOPPE;
        }


        return $this;
    } // setStatutEnveloppe()

    /**
     * Set the value of [agent_telechargement] column.
     *
     * @param int $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setAgentTelechargement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->agent_telechargement !== $v) {
            $this->agent_telechargement = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::AGENT_TELECHARGEMENT;
        }


        return $this;
    } // setAgentTelechargement()

    /**
     * Set the value of [date_telechargement] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setDateTelechargement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_telechargement !== $v) {
            $this->date_telechargement = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::DATE_TELECHARGEMENT;
        }


        return $this;
    } // setDateTelechargement()

    /**
     * Set the value of [repertoire_telechargement] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setRepertoireTelechargement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->repertoire_telechargement !== $v) {
            $this->repertoire_telechargement = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::REPERTOIRE_TELECHARGEMENT;
        }


        return $this;
    } // setRepertoireTelechargement()

    /**
     * Set the value of [nom_agent_ouverture] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setNomAgentOuverture($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_agent_ouverture !== $v) {
            $this->nom_agent_ouverture = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::NOM_AGENT_OUVERTURE;
        }


        return $this;
    } // setNomAgentOuverture()

    /**
     * Set the value of [dateheure_ouverture_agent2] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setDateheureOuvertureAgent2($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->dateheure_ouverture_agent2 !== $v) {
            $this->dateheure_ouverture_agent2 = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::DATEHEURE_OUVERTURE_AGENT2;
        }


        return $this;
    } // setDateheureOuvertureAgent2()

    /**
     * Set the value of [enveloppe_fictive] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setEnveloppeFictive($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->enveloppe_fictive !== $v) {
            $this->enveloppe_fictive = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::ENVELOPPE_FICTIVE;
        }


        return $this;
    } // setEnveloppeFictive()

    /**
     * Set the value of [integrite_fichier] column.
     *
     * @param int $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setIntegriteFichier($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->integrite_fichier !== $v) {
            $this->integrite_fichier = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::INTEGRITE_FICHIER;
        }


        return $this;
    } // setIntegriteFichier()

    /**
     * Set the value of [verification_signature] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setVerificationSignature($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->verification_signature !== $v) {
            $this->verification_signature = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::VERIFICATION_SIGNATURE;
        }


        return $this;
    } // setVerificationSignature()

    /**
     * Set the value of [uid_response] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setUidResponse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->uid_response !== $v) {
            $this->uid_response = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::UID_RESPONSE;
        }


        return $this;
    } // setUidResponse()

    /**
     * Set the value of [resultat_verification_hash_files] column.
     *
     * @param string $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setResultatVerificationHashFiles($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->resultat_verification_hash_files !== $v) {
            $this->resultat_verification_hash_files = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::RESULTAT_VERIFICATION_HASH_FILES;
        }


        return $this;
    } // setResultatVerificationHashFiles()

    /**
     * Set the value of [id_dossier_volumineux] column.
     *
     * @param int $v new value
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setIdDossierVolumineux($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_dossier_volumineux !== $v) {
            $this->id_dossier_volumineux = $v;
            $this->modifiedColumns[] = CommonEnveloppePeer::ID_DOSSIER_VOLUMINEUX;
        }

        if ($this->aCommonDossierVolumineux !== null && $this->aCommonDossierVolumineux->getId() !== $v) {
            $this->aCommonDossierVolumineux = null;
        }


        return $this;
    } // setIdDossierVolumineux()

    /**
     * Sets the value of [date_debut_dechiffrement] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonEnveloppe The current object (for fluent API support)
     */
    public function setDateDebutDechiffrement($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_debut_dechiffrement !== null || $dt !== null) {
            $currentDateAsString = ($this->date_debut_dechiffrement !== null && $tmpDt = new DateTime($this->date_debut_dechiffrement)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_debut_dechiffrement = $newDateAsString;
                $this->modifiedColumns[] = CommonEnveloppePeer::DATE_DEBUT_DECHIFFREMENT;
            }
        } // if either are not null


        return $this;
    } // setDateDebutDechiffrement()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->organisme !== '') {
                return false;
            }

            if ($this->offre_id !== 0) {
                return false;
            }

            if ($this->fichier !== 0) {
                return false;
            }

            if ($this->supprime !== '0') {
                return false;
            }

            if ($this->cryptage !== '1') {
                return false;
            }

            if ($this->nom_fichier !== '') {
                return false;
            }

            if ($this->hash !== '') {
                return false;
            }

            if ($this->type_env !== 0) {
                return false;
            }

            if ($this->sous_pli !== 0) {
                return false;
            }

            if ($this->attribue !== '0') {
                return false;
            }

            if ($this->dateheure_ouverture !== '0000-00-00 00:00:00') {
                return false;
            }

            if ($this->statut_enveloppe !== 1) {
                return false;
            }

            if ($this->nom_agent_ouverture !== '') {
                return false;
            }

            if ($this->dateheure_ouverture_agent2 !== '0000-00-00 00:00:00') {
                return false;
            }

            if ($this->enveloppe_fictive !== '0') {
                return false;
            }

            if ($this->integrite_fichier !== 0) {
                return false;
            }

            if ($this->verification_signature !== '') {
                return false;
            }

            if ($this->resultat_verification_hash_files !== '1') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_enveloppe_electro = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->organisme = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->offre_id = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            if ($row[$startcol + 3] !== null) {
                $this->champs_optionnels = fopen('php://memory', 'r+');
                fwrite($this->champs_optionnels, $row[$startcol + 3]);
                rewind($this->champs_optionnels);
            } else {
                $this->champs_optionnels = null;
            }
            $this->fichier = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->supprime = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->cryptage = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->nom_fichier = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->hash = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->type_env = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->sous_pli = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->attribue = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->dateheure_ouverture = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->agent_id_ouverture = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
            $this->agent_id_ouverture2 = ($row[$startcol + 14] !== null) ? (int) $row[$startcol + 14] : null;
            if ($row[$startcol + 15] !== null) {
                $this->donnees_ouverture = fopen('php://memory', 'r+');
                fwrite($this->donnees_ouverture, $row[$startcol + 15]);
                rewind($this->donnees_ouverture);
            } else {
                $this->donnees_ouverture = null;
            }
            if ($row[$startcol + 16] !== null) {
                $this->horodatage_donnees_ouverture = fopen('php://memory', 'r+');
                fwrite($this->horodatage_donnees_ouverture, $row[$startcol + 16]);
                rewind($this->horodatage_donnees_ouverture);
            } else {
                $this->horodatage_donnees_ouverture = null;
            }
            $this->statut_enveloppe = ($row[$startcol + 17] !== null) ? (int) $row[$startcol + 17] : null;
            $this->agent_telechargement = ($row[$startcol + 18] !== null) ? (int) $row[$startcol + 18] : null;
            $this->date_telechargement = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->repertoire_telechargement = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->nom_agent_ouverture = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->dateheure_ouverture_agent2 = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->enveloppe_fictive = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->integrite_fichier = ($row[$startcol + 24] !== null) ? (int) $row[$startcol + 24] : null;
            $this->verification_signature = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->uid_response = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->resultat_verification_hash_files = ($row[$startcol + 27] !== null) ? (string) $row[$startcol + 27] : null;
            $this->id_dossier_volumineux = ($row[$startcol + 28] !== null) ? (int) $row[$startcol + 28] : null;
            $this->date_debut_dechiffrement = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 30; // 30 = CommonEnveloppePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonEnveloppe object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonDossierVolumineux !== null && $this->id_dossier_volumineux !== $this->aCommonDossierVolumineux->getId()) {
            $this->aCommonDossierVolumineux = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEnveloppePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonEnveloppePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonDossierVolumineux = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEnveloppePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonEnveloppeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonEnveloppePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonEnveloppePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonDossierVolumineux !== null) {
                if ($this->aCommonDossierVolumineux->isModified() || $this->aCommonDossierVolumineux->isNew()) {
                    $affectedRows += $this->aCommonDossierVolumineux->save($con);
                }
                $this->setCommonDossierVolumineux($this->aCommonDossierVolumineux);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                // Rewind the champs_optionnels LOB column, since PDO does not rewind after inserting value.
                if ($this->champs_optionnels !== null && is_resource($this->champs_optionnels)) {
                    rewind($this->champs_optionnels);
                }

                // Rewind the donnees_ouverture LOB column, since PDO does not rewind after inserting value.
                if ($this->donnees_ouverture !== null && is_resource($this->donnees_ouverture)) {
                    rewind($this->donnees_ouverture);
                }

                // Rewind the horodatage_donnees_ouverture LOB column, since PDO does not rewind after inserting value.
                if ($this->horodatage_donnees_ouverture !== null && is_resource($this->horodatage_donnees_ouverture)) {
                    rewind($this->horodatage_donnees_ouverture);
                }

                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonEnveloppePeer::ID_ENVELOPPE_ELECTRO;
        if (null !== $this->id_enveloppe_electro) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonEnveloppePeer::ID_ENVELOPPE_ELECTRO . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonEnveloppePeer::ID_ENVELOPPE_ELECTRO)) {
            $modifiedColumns[':p' . $index++]  = '`id_enveloppe_electro`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::OFFRE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`offre_id`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::CHAMPS_OPTIONNELS)) {
            $modifiedColumns[':p' . $index++]  = '`champs_optionnels`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::FICHIER)) {
            $modifiedColumns[':p' . $index++]  = '`fichier`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::SUPPRIME)) {
            $modifiedColumns[':p' . $index++]  = '`supprime`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::CRYPTAGE)) {
            $modifiedColumns[':p' . $index++]  = '`cryptage`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::NOM_FICHIER)) {
            $modifiedColumns[':p' . $index++]  = '`nom_fichier`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::HASH)) {
            $modifiedColumns[':p' . $index++]  = '`hash`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::TYPE_ENV)) {
            $modifiedColumns[':p' . $index++]  = '`type_env`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::SOUS_PLI)) {
            $modifiedColumns[':p' . $index++]  = '`sous_pli`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::ATTRIBUE)) {
            $modifiedColumns[':p' . $index++]  = '`attribue`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::DATEHEURE_OUVERTURE)) {
            $modifiedColumns[':p' . $index++]  = '`dateheure_ouverture`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::AGENT_ID_OUVERTURE)) {
            $modifiedColumns[':p' . $index++]  = '`agent_id_ouverture`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::AGENT_ID_OUVERTURE2)) {
            $modifiedColumns[':p' . $index++]  = '`agent_id_ouverture2`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::DONNEES_OUVERTURE)) {
            $modifiedColumns[':p' . $index++]  = '`donnees_ouverture`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::HORODATAGE_DONNEES_OUVERTURE)) {
            $modifiedColumns[':p' . $index++]  = '`horodatage_donnees_ouverture`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::STATUT_ENVELOPPE)) {
            $modifiedColumns[':p' . $index++]  = '`statut_enveloppe`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::AGENT_TELECHARGEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`agent_telechargement`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::DATE_TELECHARGEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`date_telechargement`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::REPERTOIRE_TELECHARGEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`repertoire_telechargement`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::NOM_AGENT_OUVERTURE)) {
            $modifiedColumns[':p' . $index++]  = '`nom_agent_ouverture`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::DATEHEURE_OUVERTURE_AGENT2)) {
            $modifiedColumns[':p' . $index++]  = '`dateheure_ouverture_agent2`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::ENVELOPPE_FICTIVE)) {
            $modifiedColumns[':p' . $index++]  = '`enveloppe_fictive`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::INTEGRITE_FICHIER)) {
            $modifiedColumns[':p' . $index++]  = '`integrite_fichier`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::VERIFICATION_SIGNATURE)) {
            $modifiedColumns[':p' . $index++]  = '`verification_signature`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::UID_RESPONSE)) {
            $modifiedColumns[':p' . $index++]  = '`uid_response`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::RESULTAT_VERIFICATION_HASH_FILES)) {
            $modifiedColumns[':p' . $index++]  = '`resultat_verification_hash_files`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::ID_DOSSIER_VOLUMINEUX)) {
            $modifiedColumns[':p' . $index++]  = '`id_dossier_volumineux`';
        }
        if ($this->isColumnModified(CommonEnveloppePeer::DATE_DEBUT_DECHIFFREMENT)) {
            $modifiedColumns[':p' . $index++]  = '`date_debut_dechiffrement`';
        }

        $sql = sprintf(
            'INSERT INTO `Enveloppe` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_enveloppe_electro`':
                        $stmt->bindValue($identifier, $this->id_enveloppe_electro, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`offre_id`':
                        $stmt->bindValue($identifier, $this->offre_id, PDO::PARAM_INT);
                        break;
                    case '`champs_optionnels`':
                        if (is_resource($this->champs_optionnels)) {
                            rewind($this->champs_optionnels);
                        }
                        $stmt->bindValue($identifier, $this->champs_optionnels, PDO::PARAM_LOB);
                        break;
                    case '`fichier`':
                        $stmt->bindValue($identifier, $this->fichier, PDO::PARAM_INT);
                        break;
                    case '`supprime`':
                        $stmt->bindValue($identifier, $this->supprime, PDO::PARAM_STR);
                        break;
                    case '`cryptage`':
                        $stmt->bindValue($identifier, $this->cryptage, PDO::PARAM_STR);
                        break;
                    case '`nom_fichier`':
                        $stmt->bindValue($identifier, $this->nom_fichier, PDO::PARAM_STR);
                        break;
                    case '`hash`':
                        $stmt->bindValue($identifier, $this->hash, PDO::PARAM_STR);
                        break;
                    case '`type_env`':
                        $stmt->bindValue($identifier, $this->type_env, PDO::PARAM_INT);
                        break;
                    case '`sous_pli`':
                        $stmt->bindValue($identifier, $this->sous_pli, PDO::PARAM_INT);
                        break;
                    case '`attribue`':
                        $stmt->bindValue($identifier, $this->attribue, PDO::PARAM_STR);
                        break;
                    case '`dateheure_ouverture`':
                        $stmt->bindValue($identifier, $this->dateheure_ouverture, PDO::PARAM_STR);
                        break;
                    case '`agent_id_ouverture`':
                        $stmt->bindValue($identifier, $this->agent_id_ouverture, PDO::PARAM_INT);
                        break;
                    case '`agent_id_ouverture2`':
                        $stmt->bindValue($identifier, $this->agent_id_ouverture2, PDO::PARAM_INT);
                        break;
                    case '`donnees_ouverture`':
                        if (is_resource($this->donnees_ouverture)) {
                            rewind($this->donnees_ouverture);
                        }
                        $stmt->bindValue($identifier, $this->donnees_ouverture, PDO::PARAM_LOB);
                        break;
                    case '`horodatage_donnees_ouverture`':
                        if (is_resource($this->horodatage_donnees_ouverture)) {
                            rewind($this->horodatage_donnees_ouverture);
                        }
                        $stmt->bindValue($identifier, $this->horodatage_donnees_ouverture, PDO::PARAM_LOB);
                        break;
                    case '`statut_enveloppe`':
                        $stmt->bindValue($identifier, $this->statut_enveloppe, PDO::PARAM_INT);
                        break;
                    case '`agent_telechargement`':
                        $stmt->bindValue($identifier, $this->agent_telechargement, PDO::PARAM_INT);
                        break;
                    case '`date_telechargement`':
                        $stmt->bindValue($identifier, $this->date_telechargement, PDO::PARAM_STR);
                        break;
                    case '`repertoire_telechargement`':
                        $stmt->bindValue($identifier, $this->repertoire_telechargement, PDO::PARAM_STR);
                        break;
                    case '`nom_agent_ouverture`':
                        $stmt->bindValue($identifier, $this->nom_agent_ouverture, PDO::PARAM_STR);
                        break;
                    case '`dateheure_ouverture_agent2`':
                        $stmt->bindValue($identifier, $this->dateheure_ouverture_agent2, PDO::PARAM_STR);
                        break;
                    case '`enveloppe_fictive`':
                        $stmt->bindValue($identifier, $this->enveloppe_fictive, PDO::PARAM_STR);
                        break;
                    case '`integrite_fichier`':
                        $stmt->bindValue($identifier, $this->integrite_fichier, PDO::PARAM_INT);
                        break;
                    case '`verification_signature`':
                        $stmt->bindValue($identifier, $this->verification_signature, PDO::PARAM_STR);
                        break;
                    case '`uid_response`':
                        $stmt->bindValue($identifier, $this->uid_response, PDO::PARAM_STR);
                        break;
                    case '`resultat_verification_hash_files`':
                        $stmt->bindValue($identifier, $this->resultat_verification_hash_files, PDO::PARAM_STR);
                        break;
                    case '`id_dossier_volumineux`':
                        $stmt->bindValue($identifier, $this->id_dossier_volumineux, PDO::PARAM_INT);
                        break;
                    case '`date_debut_dechiffrement`':
                        $stmt->bindValue($identifier, $this->date_debut_dechiffrement, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdEnveloppeElectro($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonDossierVolumineux !== null) {
                if (!$this->aCommonDossierVolumineux->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonDossierVolumineux->getValidationFailures());
                }
            }


            if (($retval = CommonEnveloppePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonEnveloppePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdEnveloppeElectro();
                break;
            case 1:
                return $this->getOrganisme();
                break;
            case 2:
                return $this->getOffreId();
                break;
            case 3:
                return $this->getChampsOptionnels();
                break;
            case 4:
                return $this->getFichier();
                break;
            case 5:
                return $this->getSupprime();
                break;
            case 6:
                return $this->getCryptage();
                break;
            case 7:
                return $this->getNomFichier();
                break;
            case 8:
                return $this->getHash();
                break;
            case 9:
                return $this->getTypeEnv();
                break;
            case 10:
                return $this->getSousPli();
                break;
            case 11:
                return $this->getAttribue();
                break;
            case 12:
                return $this->getDateheureOuverture();
                break;
            case 13:
                return $this->getAgentIdOuverture();
                break;
            case 14:
                return $this->getAgentIdOuverture2();
                break;
            case 15:
                return $this->getDonneesOuverture();
                break;
            case 16:
                return $this->getHorodatageDonneesOuverture();
                break;
            case 17:
                return $this->getStatutEnveloppe();
                break;
            case 18:
                return $this->getAgentTelechargement();
                break;
            case 19:
                return $this->getDateTelechargement();
                break;
            case 20:
                return $this->getRepertoireTelechargement();
                break;
            case 21:
                return $this->getNomAgentOuverture();
                break;
            case 22:
                return $this->getDateheureOuvertureAgent2();
                break;
            case 23:
                return $this->getEnveloppeFictive();
                break;
            case 24:
                return $this->getIntegriteFichier();
                break;
            case 25:
                return $this->getVerificationSignature();
                break;
            case 26:
                return $this->getUidResponse();
                break;
            case 27:
                return $this->getResultatVerificationHashFiles();
                break;
            case 28:
                return $this->getIdDossierVolumineux();
                break;
            case 29:
                return $this->getDateDebutDechiffrement();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonEnveloppe'][serialize($this->getPrimaryKey())])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonEnveloppe'][serialize($this->getPrimaryKey())] = true;
        $keys = CommonEnveloppePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdEnveloppeElectro(),
            $keys[1] => $this->getOrganisme(),
            $keys[2] => $this->getOffreId(),
            $keys[3] => $this->getChampsOptionnels(),
            $keys[4] => $this->getFichier(),
            $keys[5] => $this->getSupprime(),
            $keys[6] => $this->getCryptage(),
            $keys[7] => $this->getNomFichier(),
            $keys[8] => $this->getHash(),
            $keys[9] => $this->getTypeEnv(),
            $keys[10] => $this->getSousPli(),
            $keys[11] => $this->getAttribue(),
            $keys[12] => $this->getDateheureOuverture(),
            $keys[13] => $this->getAgentIdOuverture(),
            $keys[14] => $this->getAgentIdOuverture2(),
            $keys[15] => $this->getDonneesOuverture(),
            $keys[16] => $this->getHorodatageDonneesOuverture(),
            $keys[17] => $this->getStatutEnveloppe(),
            $keys[18] => $this->getAgentTelechargement(),
            $keys[19] => $this->getDateTelechargement(),
            $keys[20] => $this->getRepertoireTelechargement(),
            $keys[21] => $this->getNomAgentOuverture(),
            $keys[22] => $this->getDateheureOuvertureAgent2(),
            $keys[23] => $this->getEnveloppeFictive(),
            $keys[24] => $this->getIntegriteFichier(),
            $keys[25] => $this->getVerificationSignature(),
            $keys[26] => $this->getUidResponse(),
            $keys[27] => $this->getResultatVerificationHashFiles(),
            $keys[28] => $this->getIdDossierVolumineux(),
            $keys[29] => $this->getDateDebutDechiffrement(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonDossierVolumineux) {
                $result['CommonDossierVolumineux'] = $this->aCommonDossierVolumineux->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonEnveloppePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdEnveloppeElectro($value);
                break;
            case 1:
                $this->setOrganisme($value);
                break;
            case 2:
                $this->setOffreId($value);
                break;
            case 3:
                $this->setChampsOptionnels($value);
                break;
            case 4:
                $this->setFichier($value);
                break;
            case 5:
                $this->setSupprime($value);
                break;
            case 6:
                $this->setCryptage($value);
                break;
            case 7:
                $this->setNomFichier($value);
                break;
            case 8:
                $this->setHash($value);
                break;
            case 9:
                $this->setTypeEnv($value);
                break;
            case 10:
                $this->setSousPli($value);
                break;
            case 11:
                $this->setAttribue($value);
                break;
            case 12:
                $this->setDateheureOuverture($value);
                break;
            case 13:
                $this->setAgentIdOuverture($value);
                break;
            case 14:
                $this->setAgentIdOuverture2($value);
                break;
            case 15:
                $this->setDonneesOuverture($value);
                break;
            case 16:
                $this->setHorodatageDonneesOuverture($value);
                break;
            case 17:
                $this->setStatutEnveloppe($value);
                break;
            case 18:
                $this->setAgentTelechargement($value);
                break;
            case 19:
                $this->setDateTelechargement($value);
                break;
            case 20:
                $this->setRepertoireTelechargement($value);
                break;
            case 21:
                $this->setNomAgentOuverture($value);
                break;
            case 22:
                $this->setDateheureOuvertureAgent2($value);
                break;
            case 23:
                $this->setEnveloppeFictive($value);
                break;
            case 24:
                $this->setIntegriteFichier($value);
                break;
            case 25:
                $this->setVerificationSignature($value);
                break;
            case 26:
                $this->setUidResponse($value);
                break;
            case 27:
                $this->setResultatVerificationHashFiles($value);
                break;
            case 28:
                $this->setIdDossierVolumineux($value);
                break;
            case 29:
                $this->setDateDebutDechiffrement($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonEnveloppePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdEnveloppeElectro($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setOrganisme($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setOffreId($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setChampsOptionnels($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setFichier($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setSupprime($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setCryptage($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setNomFichier($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setHash($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setTypeEnv($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setSousPli($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setAttribue($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setDateheureOuverture($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setAgentIdOuverture($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setAgentIdOuverture2($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setDonneesOuverture($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setHorodatageDonneesOuverture($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setStatutEnveloppe($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setAgentTelechargement($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setDateTelechargement($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setRepertoireTelechargement($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setNomAgentOuverture($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setDateheureOuvertureAgent2($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setEnveloppeFictive($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setIntegriteFichier($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setVerificationSignature($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setUidResponse($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setResultatVerificationHashFiles($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setIdDossierVolumineux($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setDateDebutDechiffrement($arr[$keys[29]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonEnveloppePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonEnveloppePeer::ID_ENVELOPPE_ELECTRO)) $criteria->add(CommonEnveloppePeer::ID_ENVELOPPE_ELECTRO, $this->id_enveloppe_electro);
        if ($this->isColumnModified(CommonEnveloppePeer::ORGANISME)) $criteria->add(CommonEnveloppePeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonEnveloppePeer::OFFRE_ID)) $criteria->add(CommonEnveloppePeer::OFFRE_ID, $this->offre_id);
        if ($this->isColumnModified(CommonEnveloppePeer::CHAMPS_OPTIONNELS)) $criteria->add(CommonEnveloppePeer::CHAMPS_OPTIONNELS, $this->champs_optionnels);
        if ($this->isColumnModified(CommonEnveloppePeer::FICHIER)) $criteria->add(CommonEnveloppePeer::FICHIER, $this->fichier);
        if ($this->isColumnModified(CommonEnveloppePeer::SUPPRIME)) $criteria->add(CommonEnveloppePeer::SUPPRIME, $this->supprime);
        if ($this->isColumnModified(CommonEnveloppePeer::CRYPTAGE)) $criteria->add(CommonEnveloppePeer::CRYPTAGE, $this->cryptage);
        if ($this->isColumnModified(CommonEnveloppePeer::NOM_FICHIER)) $criteria->add(CommonEnveloppePeer::NOM_FICHIER, $this->nom_fichier);
        if ($this->isColumnModified(CommonEnveloppePeer::HASH)) $criteria->add(CommonEnveloppePeer::HASH, $this->hash);
        if ($this->isColumnModified(CommonEnveloppePeer::TYPE_ENV)) $criteria->add(CommonEnveloppePeer::TYPE_ENV, $this->type_env);
        if ($this->isColumnModified(CommonEnveloppePeer::SOUS_PLI)) $criteria->add(CommonEnveloppePeer::SOUS_PLI, $this->sous_pli);
        if ($this->isColumnModified(CommonEnveloppePeer::ATTRIBUE)) $criteria->add(CommonEnveloppePeer::ATTRIBUE, $this->attribue);
        if ($this->isColumnModified(CommonEnveloppePeer::DATEHEURE_OUVERTURE)) $criteria->add(CommonEnveloppePeer::DATEHEURE_OUVERTURE, $this->dateheure_ouverture);
        if ($this->isColumnModified(CommonEnveloppePeer::AGENT_ID_OUVERTURE)) $criteria->add(CommonEnveloppePeer::AGENT_ID_OUVERTURE, $this->agent_id_ouverture);
        if ($this->isColumnModified(CommonEnveloppePeer::AGENT_ID_OUVERTURE2)) $criteria->add(CommonEnveloppePeer::AGENT_ID_OUVERTURE2, $this->agent_id_ouverture2);
        if ($this->isColumnModified(CommonEnveloppePeer::DONNEES_OUVERTURE)) $criteria->add(CommonEnveloppePeer::DONNEES_OUVERTURE, $this->donnees_ouverture);
        if ($this->isColumnModified(CommonEnveloppePeer::HORODATAGE_DONNEES_OUVERTURE)) $criteria->add(CommonEnveloppePeer::HORODATAGE_DONNEES_OUVERTURE, $this->horodatage_donnees_ouverture);
        if ($this->isColumnModified(CommonEnveloppePeer::STATUT_ENVELOPPE)) $criteria->add(CommonEnveloppePeer::STATUT_ENVELOPPE, $this->statut_enveloppe);
        if ($this->isColumnModified(CommonEnveloppePeer::AGENT_TELECHARGEMENT)) $criteria->add(CommonEnveloppePeer::AGENT_TELECHARGEMENT, $this->agent_telechargement);
        if ($this->isColumnModified(CommonEnveloppePeer::DATE_TELECHARGEMENT)) $criteria->add(CommonEnveloppePeer::DATE_TELECHARGEMENT, $this->date_telechargement);
        if ($this->isColumnModified(CommonEnveloppePeer::REPERTOIRE_TELECHARGEMENT)) $criteria->add(CommonEnveloppePeer::REPERTOIRE_TELECHARGEMENT, $this->repertoire_telechargement);
        if ($this->isColumnModified(CommonEnveloppePeer::NOM_AGENT_OUVERTURE)) $criteria->add(CommonEnveloppePeer::NOM_AGENT_OUVERTURE, $this->nom_agent_ouverture);
        if ($this->isColumnModified(CommonEnveloppePeer::DATEHEURE_OUVERTURE_AGENT2)) $criteria->add(CommonEnveloppePeer::DATEHEURE_OUVERTURE_AGENT2, $this->dateheure_ouverture_agent2);
        if ($this->isColumnModified(CommonEnveloppePeer::ENVELOPPE_FICTIVE)) $criteria->add(CommonEnveloppePeer::ENVELOPPE_FICTIVE, $this->enveloppe_fictive);
        if ($this->isColumnModified(CommonEnveloppePeer::INTEGRITE_FICHIER)) $criteria->add(CommonEnveloppePeer::INTEGRITE_FICHIER, $this->integrite_fichier);
        if ($this->isColumnModified(CommonEnveloppePeer::VERIFICATION_SIGNATURE)) $criteria->add(CommonEnveloppePeer::VERIFICATION_SIGNATURE, $this->verification_signature);
        if ($this->isColumnModified(CommonEnveloppePeer::UID_RESPONSE)) $criteria->add(CommonEnveloppePeer::UID_RESPONSE, $this->uid_response);
        if ($this->isColumnModified(CommonEnveloppePeer::RESULTAT_VERIFICATION_HASH_FILES)) $criteria->add(CommonEnveloppePeer::RESULTAT_VERIFICATION_HASH_FILES, $this->resultat_verification_hash_files);
        if ($this->isColumnModified(CommonEnveloppePeer::ID_DOSSIER_VOLUMINEUX)) $criteria->add(CommonEnveloppePeer::ID_DOSSIER_VOLUMINEUX, $this->id_dossier_volumineux);
        if ($this->isColumnModified(CommonEnveloppePeer::DATE_DEBUT_DECHIFFREMENT)) $criteria->add(CommonEnveloppePeer::DATE_DEBUT_DECHIFFREMENT, $this->date_debut_dechiffrement);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonEnveloppePeer::DATABASE_NAME);
        $criteria->add(CommonEnveloppePeer::ID_ENVELOPPE_ELECTRO, $this->id_enveloppe_electro);
        $criteria->add(CommonEnveloppePeer::ORGANISME, $this->organisme);

        return $criteria;
    }

    /**
     * Returns the composite primary key for this object.
     * The array elements will be in same order as specified in XML.
     * @return array
     */
    public function getPrimaryKey()
    {
        $pks = array();
        $pks[0] = $this->getIdEnveloppeElectro();
        $pks[1] = $this->getOrganisme();

        return $pks;
    }

    /**
     * Set the [composite] primary key.
     *
     * @param array $keys The elements of the composite key (order must match the order in XML file).
     * @return void
     */
    public function setPrimaryKey($keys)
    {
        $this->setIdEnveloppeElectro($keys[0]);
        $this->setOrganisme($keys[1]);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return (null === $this->getIdEnveloppeElectro()) && (null === $this->getOrganisme());
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonEnveloppe (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setOffreId($this->getOffreId());
        $copyObj->setChampsOptionnels($this->getChampsOptionnels());
        $copyObj->setFichier($this->getFichier());
        $copyObj->setSupprime($this->getSupprime());
        $copyObj->setCryptage($this->getCryptage());
        $copyObj->setNomFichier($this->getNomFichier());
        $copyObj->setHash($this->getHash());
        $copyObj->setTypeEnv($this->getTypeEnv());
        $copyObj->setSousPli($this->getSousPli());
        $copyObj->setAttribue($this->getAttribue());
        $copyObj->setDateheureOuverture($this->getDateheureOuverture());
        $copyObj->setAgentIdOuverture($this->getAgentIdOuverture());
        $copyObj->setAgentIdOuverture2($this->getAgentIdOuverture2());
        $copyObj->setDonneesOuverture($this->getDonneesOuverture());
        $copyObj->setHorodatageDonneesOuverture($this->getHorodatageDonneesOuverture());
        $copyObj->setStatutEnveloppe($this->getStatutEnveloppe());
        $copyObj->setAgentTelechargement($this->getAgentTelechargement());
        $copyObj->setDateTelechargement($this->getDateTelechargement());
        $copyObj->setRepertoireTelechargement($this->getRepertoireTelechargement());
        $copyObj->setNomAgentOuverture($this->getNomAgentOuverture());
        $copyObj->setDateheureOuvertureAgent2($this->getDateheureOuvertureAgent2());
        $copyObj->setEnveloppeFictive($this->getEnveloppeFictive());
        $copyObj->setIntegriteFichier($this->getIntegriteFichier());
        $copyObj->setVerificationSignature($this->getVerificationSignature());
        $copyObj->setUidResponse($this->getUidResponse());
        $copyObj->setResultatVerificationHashFiles($this->getResultatVerificationHashFiles());
        $copyObj->setIdDossierVolumineux($this->getIdDossierVolumineux());
        $copyObj->setDateDebutDechiffrement($this->getDateDebutDechiffrement());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdEnveloppeElectro(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonEnveloppe Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonEnveloppePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonEnveloppePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonDossierVolumineux object.
     *
     * @param   CommonDossierVolumineux $v
     * @return CommonEnveloppe The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonDossierVolumineux(CommonDossierVolumineux $v = null)
    {
        if ($v === null) {
            $this->setIdDossierVolumineux(NULL);
        } else {
            $this->setIdDossierVolumineux($v->getId());
        }

        $this->aCommonDossierVolumineux = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonDossierVolumineux object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonEnveloppe($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonDossierVolumineux object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonDossierVolumineux The associated CommonDossierVolumineux object.
     * @throws PropelException
     */
    public function getCommonDossierVolumineux(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonDossierVolumineux === null && ($this->id_dossier_volumineux !== null) && $doQuery) {
            $this->aCommonDossierVolumineux = CommonDossierVolumineuxQuery::create()->findPk($this->id_dossier_volumineux, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonDossierVolumineux->addCommonEnveloppes($this);
             */
        }

        return $this->aCommonDossierVolumineux;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_enveloppe_electro = null;
        $this->organisme = null;
        $this->offre_id = null;
        $this->champs_optionnels = null;
        $this->fichier = null;
        $this->supprime = null;
        $this->cryptage = null;
        $this->nom_fichier = null;
        $this->hash = null;
        $this->type_env = null;
        $this->sous_pli = null;
        $this->attribue = null;
        $this->dateheure_ouverture = null;
        $this->agent_id_ouverture = null;
        $this->agent_id_ouverture2 = null;
        $this->donnees_ouverture = null;
        $this->horodatage_donnees_ouverture = null;
        $this->statut_enveloppe = null;
        $this->agent_telechargement = null;
        $this->date_telechargement = null;
        $this->repertoire_telechargement = null;
        $this->nom_agent_ouverture = null;
        $this->dateheure_ouverture_agent2 = null;
        $this->enveloppe_fictive = null;
        $this->integrite_fichier = null;
        $this->verification_signature = null;
        $this->uid_response = null;
        $this->resultat_verification_hash_files = null;
        $this->id_dossier_volumineux = null;
        $this->date_debut_dechiffrement = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aCommonDossierVolumineux instanceof Persistent) {
              $this->aCommonDossierVolumineux->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aCommonDossierVolumineux = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonEnveloppePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
