<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonSupervisionInterface;
use Application\Propel\Mpe\CommonSupervisionInterfacePeer;
use Application\Propel\Mpe\CommonSupervisionInterfaceQuery;

/**
 * Base class that represents a row from the 'supervision_interface' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonSupervisionInterface extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonSupervisionInterfacePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonSupervisionInterfacePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the created_date field.
     * @var        string
     */
    protected $created_date;

    /**
     * The value for the webservice_batch field.
     * @var        string
     */
    protected $webservice_batch;

    /**
     * The value for the service field.
     * @var        string
     */
    protected $service;

    /**
     * The value for the nom_interface field.
     * @var        string
     */
    protected $nom_interface;

    /**
     * The value for the total field.
     * @var        int
     */
    protected $total;

    /**
     * The value for the total_ok field.
     * @var        int
     */
    protected $total_ok;

    /**
     * The value for the poids field.
     * @var        string
     */
    protected $poids;

    /**
     * The value for the poids_ok field.
     * @var        string
     */
    protected $poids_ok;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [optionally formatted] temporal [created_date] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedDate($format = '%d/%m/%Y')
    {
        if ($this->created_date === null) {
            return null;
        }

        if ($this->created_date === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created_date);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_date, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [webservice_batch] column value.
     *
     * @return string
     */
    public function getWebserviceBatch()
    {

        return $this->webservice_batch;
    }

    /**
     * Get the [service] column value.
     *
     * @return string
     */
    public function getService()
    {

        return $this->service;
    }

    /**
     * Get the [nom_interface] column value.
     *
     * @return string
     */
    public function getNomInterface()
    {

        return $this->nom_interface;
    }

    /**
     * Get the [total] column value.
     *
     * @return int
     */
    public function getTotal()
    {

        return $this->total;
    }

    /**
     * Get the [total_ok] column value.
     *
     * @return int
     */
    public function getTotalOk()
    {

        return $this->total_ok;
    }

    /**
     * Get the [poids] column value.
     *
     * @return string
     */
    public function getPoids()
    {

        return $this->poids;
    }

    /**
     * Get the [poids_ok] column value.
     *
     * @return string
     */
    public function getPoidsOk()
    {

        return $this->poids_ok;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonSupervisionInterface The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonSupervisionInterfacePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Sets the value of [created_date] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonSupervisionInterface The current object (for fluent API support)
     */
    public function setCreatedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_date !== null || $dt !== null) {
            $currentDateAsString = ($this->created_date !== null && $tmpDt = new DateTime($this->created_date)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created_date = $newDateAsString;
                $this->modifiedColumns[] = CommonSupervisionInterfacePeer::CREATED_DATE;
            }
        } // if either are not null


        return $this;
    } // setCreatedDate()

    /**
     * Set the value of [webservice_batch] column.
     *
     * @param string $v new value
     * @return CommonSupervisionInterface The current object (for fluent API support)
     */
    public function setWebserviceBatch($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->webservice_batch !== $v) {
            $this->webservice_batch = $v;
            $this->modifiedColumns[] = CommonSupervisionInterfacePeer::WEBSERVICE_BATCH;
        }


        return $this;
    } // setWebserviceBatch()

    /**
     * Set the value of [service] column.
     *
     * @param string $v new value
     * @return CommonSupervisionInterface The current object (for fluent API support)
     */
    public function setService($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->service !== $v) {
            $this->service = $v;
            $this->modifiedColumns[] = CommonSupervisionInterfacePeer::SERVICE;
        }


        return $this;
    } // setService()

    /**
     * Set the value of [nom_interface] column.
     *
     * @param string $v new value
     * @return CommonSupervisionInterface The current object (for fluent API support)
     */
    public function setNomInterface($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_interface !== $v) {
            $this->nom_interface = $v;
            $this->modifiedColumns[] = CommonSupervisionInterfacePeer::NOM_INTERFACE;
        }


        return $this;
    } // setNomInterface()

    /**
     * Set the value of [total] column.
     *
     * @param int $v new value
     * @return CommonSupervisionInterface The current object (for fluent API support)
     */
    public function setTotal($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->total !== $v) {
            $this->total = $v;
            $this->modifiedColumns[] = CommonSupervisionInterfacePeer::TOTAL;
        }


        return $this;
    } // setTotal()

    /**
     * Set the value of [total_ok] column.
     *
     * @param int $v new value
     * @return CommonSupervisionInterface The current object (for fluent API support)
     */
    public function setTotalOk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->total_ok !== $v) {
            $this->total_ok = $v;
            $this->modifiedColumns[] = CommonSupervisionInterfacePeer::TOTAL_OK;
        }


        return $this;
    } // setTotalOk()

    /**
     * Set the value of [poids] column.
     *
     * @param string $v new value
     * @return CommonSupervisionInterface The current object (for fluent API support)
     */
    public function setPoids($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->poids !== $v) {
            $this->poids = $v;
            $this->modifiedColumns[] = CommonSupervisionInterfacePeer::POIDS;
        }


        return $this;
    } // setPoids()

    /**
     * Set the value of [poids_ok] column.
     *
     * @param string $v new value
     * @return CommonSupervisionInterface The current object (for fluent API support)
     */
    public function setPoidsOk($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->poids_ok !== $v) {
            $this->poids_ok = $v;
            $this->modifiedColumns[] = CommonSupervisionInterfacePeer::POIDS_OK;
        }


        return $this;
    } // setPoidsOk()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->created_date = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->webservice_batch = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->service = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->nom_interface = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->total = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->total_ok = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->poids = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->poids_ok = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 9; // 9 = CommonSupervisionInterfacePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonSupervisionInterface object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonSupervisionInterfacePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonSupervisionInterfacePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonSupervisionInterfacePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonSupervisionInterfaceQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonSupervisionInterfacePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonSupervisionInterfacePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonSupervisionInterfacePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonSupervisionInterfacePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::CREATED_DATE)) {
            $modifiedColumns[':p' . $index++]  = '`created_date`';
        }
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::WEBSERVICE_BATCH)) {
            $modifiedColumns[':p' . $index++]  = '`webservice_batch`';
        }
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::SERVICE)) {
            $modifiedColumns[':p' . $index++]  = '`service`';
        }
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::NOM_INTERFACE)) {
            $modifiedColumns[':p' . $index++]  = '`nom_interface`';
        }
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::TOTAL)) {
            $modifiedColumns[':p' . $index++]  = '`total`';
        }
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::TOTAL_OK)) {
            $modifiedColumns[':p' . $index++]  = '`total_ok`';
        }
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::POIDS)) {
            $modifiedColumns[':p' . $index++]  = '`poids`';
        }
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::POIDS_OK)) {
            $modifiedColumns[':p' . $index++]  = '`poids_ok`';
        }

        $sql = sprintf(
            'INSERT INTO `supervision_interface` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`created_date`':
                        $stmt->bindValue($identifier, $this->created_date, PDO::PARAM_STR);
                        break;
                    case '`webservice_batch`':
                        $stmt->bindValue($identifier, $this->webservice_batch, PDO::PARAM_STR);
                        break;
                    case '`service`':
                        $stmt->bindValue($identifier, $this->service, PDO::PARAM_STR);
                        break;
                    case '`nom_interface`':
                        $stmt->bindValue($identifier, $this->nom_interface, PDO::PARAM_STR);
                        break;
                    case '`total`':
                        $stmt->bindValue($identifier, $this->total, PDO::PARAM_INT);
                        break;
                    case '`total_ok`':
                        $stmt->bindValue($identifier, $this->total_ok, PDO::PARAM_INT);
                        break;
                    case '`poids`':
                        $stmt->bindValue($identifier, $this->poids, PDO::PARAM_STR);
                        break;
                    case '`poids_ok`':
                        $stmt->bindValue($identifier, $this->poids_ok, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonSupervisionInterfacePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonSupervisionInterfacePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getCreatedDate();
                break;
            case 2:
                return $this->getWebserviceBatch();
                break;
            case 3:
                return $this->getService();
                break;
            case 4:
                return $this->getNomInterface();
                break;
            case 5:
                return $this->getTotal();
                break;
            case 6:
                return $this->getTotalOk();
                break;
            case 7:
                return $this->getPoids();
                break;
            case 8:
                return $this->getPoidsOk();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['CommonSupervisionInterface'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonSupervisionInterface'][$this->getPrimaryKey()] = true;
        $keys = CommonSupervisionInterfacePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getCreatedDate(),
            $keys[2] => $this->getWebserviceBatch(),
            $keys[3] => $this->getService(),
            $keys[4] => $this->getNomInterface(),
            $keys[5] => $this->getTotal(),
            $keys[6] => $this->getTotalOk(),
            $keys[7] => $this->getPoids(),
            $keys[8] => $this->getPoidsOk(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonSupervisionInterfacePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setCreatedDate($value);
                break;
            case 2:
                $this->setWebserviceBatch($value);
                break;
            case 3:
                $this->setService($value);
                break;
            case 4:
                $this->setNomInterface($value);
                break;
            case 5:
                $this->setTotal($value);
                break;
            case 6:
                $this->setTotalOk($value);
                break;
            case 7:
                $this->setPoids($value);
                break;
            case 8:
                $this->setPoidsOk($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonSupervisionInterfacePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setCreatedDate($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setWebserviceBatch($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setService($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNomInterface($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setTotal($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setTotalOk($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setPoids($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setPoidsOk($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonSupervisionInterfacePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonSupervisionInterfacePeer::ID)) $criteria->add(CommonSupervisionInterfacePeer::ID, $this->id);
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::CREATED_DATE)) $criteria->add(CommonSupervisionInterfacePeer::CREATED_DATE, $this->created_date);
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::WEBSERVICE_BATCH)) $criteria->add(CommonSupervisionInterfacePeer::WEBSERVICE_BATCH, $this->webservice_batch);
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::SERVICE)) $criteria->add(CommonSupervisionInterfacePeer::SERVICE, $this->service);
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::NOM_INTERFACE)) $criteria->add(CommonSupervisionInterfacePeer::NOM_INTERFACE, $this->nom_interface);
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::TOTAL)) $criteria->add(CommonSupervisionInterfacePeer::TOTAL, $this->total);
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::TOTAL_OK)) $criteria->add(CommonSupervisionInterfacePeer::TOTAL_OK, $this->total_ok);
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::POIDS)) $criteria->add(CommonSupervisionInterfacePeer::POIDS, $this->poids);
        if ($this->isColumnModified(CommonSupervisionInterfacePeer::POIDS_OK)) $criteria->add(CommonSupervisionInterfacePeer::POIDS_OK, $this->poids_ok);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonSupervisionInterfacePeer::DATABASE_NAME);
        $criteria->add(CommonSupervisionInterfacePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonSupervisionInterface (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setCreatedDate($this->getCreatedDate());
        $copyObj->setWebserviceBatch($this->getWebserviceBatch());
        $copyObj->setService($this->getService());
        $copyObj->setNomInterface($this->getNomInterface());
        $copyObj->setTotal($this->getTotal());
        $copyObj->setTotalOk($this->getTotalOk());
        $copyObj->setPoids($this->getPoids());
        $copyObj->setPoidsOk($this->getPoidsOk());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonSupervisionInterface Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonSupervisionInterfacePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonSupervisionInterfacePeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->created_date = null;
        $this->webservice_batch = null;
        $this->service = null;
        $this->nom_interface = null;
        $this->total = null;
        $this->total_ok = null;
        $this->poids = null;
        $this->poids_ok = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonSupervisionInterfacePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
