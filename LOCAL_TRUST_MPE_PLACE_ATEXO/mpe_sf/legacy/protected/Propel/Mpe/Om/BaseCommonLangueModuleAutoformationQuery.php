<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonLangue;
use Application\Propel\Mpe\CommonLangueModuleAutoformation;
use Application\Propel\Mpe\CommonLangueModuleAutoformationPeer;
use Application\Propel\Mpe\CommonLangueModuleAutoformationQuery;
use Application\Propel\Mpe\CommonModuleAutoformation;

/**
 * Base class that represents a query for the 'langue_module_autoformation' table.
 *
 *
 *
 * @method CommonLangueModuleAutoformationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonLangueModuleAutoformationQuery orderByLangueId($order = Criteria::ASC) Order by the langue_id column
 * @method CommonLangueModuleAutoformationQuery orderByModuleAutoformationId($order = Criteria::ASC) Order by the module_autoformation_id column
 * @method CommonLangueModuleAutoformationQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method CommonLangueModuleAutoformationQuery orderByDescription($order = Criteria::ASC) Order by the description column
 *
 * @method CommonLangueModuleAutoformationQuery groupById() Group by the id column
 * @method CommonLangueModuleAutoformationQuery groupByLangueId() Group by the langue_id column
 * @method CommonLangueModuleAutoformationQuery groupByModuleAutoformationId() Group by the module_autoformation_id column
 * @method CommonLangueModuleAutoformationQuery groupByNom() Group by the nom column
 * @method CommonLangueModuleAutoformationQuery groupByDescription() Group by the description column
 *
 * @method CommonLangueModuleAutoformationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonLangueModuleAutoformationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonLangueModuleAutoformationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonLangueModuleAutoformationQuery leftJoinCommonLangue($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonLangue relation
 * @method CommonLangueModuleAutoformationQuery rightJoinCommonLangue($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonLangue relation
 * @method CommonLangueModuleAutoformationQuery innerJoinCommonLangue($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonLangue relation
 *
 * @method CommonLangueModuleAutoformationQuery leftJoinCommonModuleAutoformation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonModuleAutoformation relation
 * @method CommonLangueModuleAutoformationQuery rightJoinCommonModuleAutoformation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonModuleAutoformation relation
 * @method CommonLangueModuleAutoformationQuery innerJoinCommonModuleAutoformation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonModuleAutoformation relation
 *
 * @method CommonLangueModuleAutoformation findOne(PropelPDO $con = null) Return the first CommonLangueModuleAutoformation matching the query
 * @method CommonLangueModuleAutoformation findOneOrCreate(PropelPDO $con = null) Return the first CommonLangueModuleAutoformation matching the query, or a new CommonLangueModuleAutoformation object populated from the query conditions when no match is found
 *
 * @method CommonLangueModuleAutoformation findOneByLangueId(int $langue_id) Return the first CommonLangueModuleAutoformation filtered by the langue_id column
 * @method CommonLangueModuleAutoformation findOneByModuleAutoformationId(int $module_autoformation_id) Return the first CommonLangueModuleAutoformation filtered by the module_autoformation_id column
 * @method CommonLangueModuleAutoformation findOneByNom(string $nom) Return the first CommonLangueModuleAutoformation filtered by the nom column
 * @method CommonLangueModuleAutoformation findOneByDescription(string $description) Return the first CommonLangueModuleAutoformation filtered by the description column
 *
 * @method array findById(int $id) Return CommonLangueModuleAutoformation objects filtered by the id column
 * @method array findByLangueId(int $langue_id) Return CommonLangueModuleAutoformation objects filtered by the langue_id column
 * @method array findByModuleAutoformationId(int $module_autoformation_id) Return CommonLangueModuleAutoformation objects filtered by the module_autoformation_id column
 * @method array findByNom(string $nom) Return CommonLangueModuleAutoformation objects filtered by the nom column
 * @method array findByDescription(string $description) Return CommonLangueModuleAutoformation objects filtered by the description column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonLangueModuleAutoformationQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonLangueModuleAutoformationQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonLangueModuleAutoformation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonLangueModuleAutoformationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonLangueModuleAutoformationQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonLangueModuleAutoformationQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonLangueModuleAutoformationQuery) {
            return $criteria;
        }
        $query = new CommonLangueModuleAutoformationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonLangueModuleAutoformation|CommonLangueModuleAutoformation[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonLangueModuleAutoformationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonLangueModuleAutoformationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonLangueModuleAutoformation A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonLangueModuleAutoformation A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `langue_id`, `module_autoformation_id`, `nom`, `description` FROM `langue_module_autoformation` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonLangueModuleAutoformation();
            $obj->hydrate($row);
            CommonLangueModuleAutoformationPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonLangueModuleAutoformation|CommonLangueModuleAutoformation[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonLangueModuleAutoformation[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonLangueModuleAutoformationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonLangueModuleAutoformationPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonLangueModuleAutoformationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonLangueModuleAutoformationPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonLangueModuleAutoformationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonLangueModuleAutoformationPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonLangueModuleAutoformationPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonLangueModuleAutoformationPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the langue_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLangueId(1234); // WHERE langue_id = 1234
     * $query->filterByLangueId(array(12, 34)); // WHERE langue_id IN (12, 34)
     * $query->filterByLangueId(array('min' => 12)); // WHERE langue_id >= 12
     * $query->filterByLangueId(array('max' => 12)); // WHERE langue_id <= 12
     * </code>
     *
     * @see       filterByCommonLangue()
     *
     * @param     mixed $langueId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonLangueModuleAutoformationQuery The current query, for fluid interface
     */
    public function filterByLangueId($langueId = null, $comparison = null)
    {
        if (is_array($langueId)) {
            $useMinMax = false;
            if (isset($langueId['min'])) {
                $this->addUsingAlias(CommonLangueModuleAutoformationPeer::LANGUE_ID, $langueId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($langueId['max'])) {
                $this->addUsingAlias(CommonLangueModuleAutoformationPeer::LANGUE_ID, $langueId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonLangueModuleAutoformationPeer::LANGUE_ID, $langueId, $comparison);
    }

    /**
     * Filter the query on the module_autoformation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleAutoformationId(1234); // WHERE module_autoformation_id = 1234
     * $query->filterByModuleAutoformationId(array(12, 34)); // WHERE module_autoformation_id IN (12, 34)
     * $query->filterByModuleAutoformationId(array('min' => 12)); // WHERE module_autoformation_id >= 12
     * $query->filterByModuleAutoformationId(array('max' => 12)); // WHERE module_autoformation_id <= 12
     * </code>
     *
     * @see       filterByCommonModuleAutoformation()
     *
     * @param     mixed $moduleAutoformationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonLangueModuleAutoformationQuery The current query, for fluid interface
     */
    public function filterByModuleAutoformationId($moduleAutoformationId = null, $comparison = null)
    {
        if (is_array($moduleAutoformationId)) {
            $useMinMax = false;
            if (isset($moduleAutoformationId['min'])) {
                $this->addUsingAlias(CommonLangueModuleAutoformationPeer::MODULE_AUTOFORMATION_ID, $moduleAutoformationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($moduleAutoformationId['max'])) {
                $this->addUsingAlias(CommonLangueModuleAutoformationPeer::MODULE_AUTOFORMATION_ID, $moduleAutoformationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonLangueModuleAutoformationPeer::MODULE_AUTOFORMATION_ID, $moduleAutoformationId, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%'); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonLangueModuleAutoformationQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nom)) {
                $nom = str_replace('*', '%', $nom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonLangueModuleAutoformationPeer::NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonLangueModuleAutoformationQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $description)) {
                $description = str_replace('*', '%', $description);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonLangueModuleAutoformationPeer::DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query by a related CommonLangue object
     *
     * @param   CommonLangue|PropelObjectCollection $commonLangue The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonLangueModuleAutoformationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonLangue($commonLangue, $comparison = null)
    {
        if ($commonLangue instanceof CommonLangue) {
            return $this
                ->addUsingAlias(CommonLangueModuleAutoformationPeer::LANGUE_ID, $commonLangue->getIdLangue(), $comparison);
        } elseif ($commonLangue instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonLangueModuleAutoformationPeer::LANGUE_ID, $commonLangue->toKeyValue('PrimaryKey', 'IdLangue'), $comparison);
        } else {
            throw new PropelException('filterByCommonLangue() only accepts arguments of type CommonLangue or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonLangue relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonLangueModuleAutoformationQuery The current query, for fluid interface
     */
    public function joinCommonLangue($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonLangue');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonLangue');
        }

        return $this;
    }

    /**
     * Use the CommonLangue relation CommonLangue object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonLangueQuery A secondary query class using the current class as primary query
     */
    public function useCommonLangueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonLangue($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonLangue', '\Application\Propel\Mpe\CommonLangueQuery');
    }

    /**
     * Filter the query by a related CommonModuleAutoformation object
     *
     * @param   CommonModuleAutoformation|PropelObjectCollection $commonModuleAutoformation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonLangueModuleAutoformationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonModuleAutoformation($commonModuleAutoformation, $comparison = null)
    {
        if ($commonModuleAutoformation instanceof CommonModuleAutoformation) {
            return $this
                ->addUsingAlias(CommonLangueModuleAutoformationPeer::MODULE_AUTOFORMATION_ID, $commonModuleAutoformation->getId(), $comparison);
        } elseif ($commonModuleAutoformation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonLangueModuleAutoformationPeer::MODULE_AUTOFORMATION_ID, $commonModuleAutoformation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonModuleAutoformation() only accepts arguments of type CommonModuleAutoformation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonModuleAutoformation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonLangueModuleAutoformationQuery The current query, for fluid interface
     */
    public function joinCommonModuleAutoformation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonModuleAutoformation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonModuleAutoformation');
        }

        return $this;
    }

    /**
     * Use the CommonModuleAutoformation relation CommonModuleAutoformation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonModuleAutoformationQuery A secondary query class using the current class as primary query
     */
    public function useCommonModuleAutoformationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonModuleAutoformation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonModuleAutoformation', '\Application\Propel\Mpe\CommonModuleAutoformationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonLangueModuleAutoformation $commonLangueModuleAutoformation Object to remove from the list of results
     *
     * @return CommonLangueModuleAutoformationQuery The current query, for fluid interface
     */
    public function prune($commonLangueModuleAutoformation = null)
    {
        if ($commonLangueModuleAutoformation) {
            $this->addUsingAlias(CommonLangueModuleAutoformationPeer::ID, $commonLangueModuleAutoformation->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
