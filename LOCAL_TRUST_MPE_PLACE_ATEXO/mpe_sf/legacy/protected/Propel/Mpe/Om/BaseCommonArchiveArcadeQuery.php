<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonArchiveArcade;
use Application\Propel\Mpe\CommonArchiveArcadePeer;
use Application\Propel\Mpe\CommonArchiveArcadeQuery;
use Application\Propel\Mpe\CommonConsultationArchiveArcade;

/**
 * Base class that represents a query for the 'archive_arcade' table.
 *
 *
 *
 * @method CommonArchiveArcadeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonArchiveArcadeQuery orderByAnnee($order = Criteria::ASC) Order by the annee column
 * @method CommonArchiveArcadeQuery orderByNumSemaine($order = Criteria::ASC) Order by the num_semaine column
 * @method CommonArchiveArcadeQuery orderByPoidsArchive($order = Criteria::ASC) Order by the poids_archive column
 * @method CommonArchiveArcadeQuery orderByCheminFichier($order = Criteria::ASC) Order by the chemin_fichier column
 * @method CommonArchiveArcadeQuery orderByDateEnvoiDebut($order = Criteria::ASC) Order by the date_envoi_debut column
 * @method CommonArchiveArcadeQuery orderByDateEnvoiFin($order = Criteria::ASC) Order by the date_envoi_fin column
 * @method CommonArchiveArcadeQuery orderByStatusTransmission($order = Criteria::ASC) Order by the status_transmission column
 * @method CommonArchiveArcadeQuery orderByErreur($order = Criteria::ASC) Order by the erreur column
 *
 * @method CommonArchiveArcadeQuery groupById() Group by the id column
 * @method CommonArchiveArcadeQuery groupByAnnee() Group by the annee column
 * @method CommonArchiveArcadeQuery groupByNumSemaine() Group by the num_semaine column
 * @method CommonArchiveArcadeQuery groupByPoidsArchive() Group by the poids_archive column
 * @method CommonArchiveArcadeQuery groupByCheminFichier() Group by the chemin_fichier column
 * @method CommonArchiveArcadeQuery groupByDateEnvoiDebut() Group by the date_envoi_debut column
 * @method CommonArchiveArcadeQuery groupByDateEnvoiFin() Group by the date_envoi_fin column
 * @method CommonArchiveArcadeQuery groupByStatusTransmission() Group by the status_transmission column
 * @method CommonArchiveArcadeQuery groupByErreur() Group by the erreur column
 *
 * @method CommonArchiveArcadeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonArchiveArcadeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonArchiveArcadeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonArchiveArcadeQuery leftJoinCommonConsultationArchiveArcade($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultationArchiveArcade relation
 * @method CommonArchiveArcadeQuery rightJoinCommonConsultationArchiveArcade($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultationArchiveArcade relation
 * @method CommonArchiveArcadeQuery innerJoinCommonConsultationArchiveArcade($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultationArchiveArcade relation
 *
 * @method CommonArchiveArcade findOne(PropelPDO $con = null) Return the first CommonArchiveArcade matching the query
 * @method CommonArchiveArcade findOneOrCreate(PropelPDO $con = null) Return the first CommonArchiveArcade matching the query, or a new CommonArchiveArcade object populated from the query conditions when no match is found
 *
 * @method CommonArchiveArcade findOneByAnnee(int $annee) Return the first CommonArchiveArcade filtered by the annee column
 * @method CommonArchiveArcade findOneByNumSemaine(int $num_semaine) Return the first CommonArchiveArcade filtered by the num_semaine column
 * @method CommonArchiveArcade findOneByPoidsArchive(int $poids_archive) Return the first CommonArchiveArcade filtered by the poids_archive column
 * @method CommonArchiveArcade findOneByCheminFichier(string $chemin_fichier) Return the first CommonArchiveArcade filtered by the chemin_fichier column
 * @method CommonArchiveArcade findOneByDateEnvoiDebut(string $date_envoi_debut) Return the first CommonArchiveArcade filtered by the date_envoi_debut column
 * @method CommonArchiveArcade findOneByDateEnvoiFin(string $date_envoi_fin) Return the first CommonArchiveArcade filtered by the date_envoi_fin column
 * @method CommonArchiveArcade findOneByStatusTransmission(boolean $status_transmission) Return the first CommonArchiveArcade filtered by the status_transmission column
 * @method CommonArchiveArcade findOneByErreur(string $erreur) Return the first CommonArchiveArcade filtered by the erreur column
 *
 * @method array findById(int $id) Return CommonArchiveArcade objects filtered by the id column
 * @method array findByAnnee(int $annee) Return CommonArchiveArcade objects filtered by the annee column
 * @method array findByNumSemaine(int $num_semaine) Return CommonArchiveArcade objects filtered by the num_semaine column
 * @method array findByPoidsArchive(int $poids_archive) Return CommonArchiveArcade objects filtered by the poids_archive column
 * @method array findByCheminFichier(string $chemin_fichier) Return CommonArchiveArcade objects filtered by the chemin_fichier column
 * @method array findByDateEnvoiDebut(string $date_envoi_debut) Return CommonArchiveArcade objects filtered by the date_envoi_debut column
 * @method array findByDateEnvoiFin(string $date_envoi_fin) Return CommonArchiveArcade objects filtered by the date_envoi_fin column
 * @method array findByStatusTransmission(boolean $status_transmission) Return CommonArchiveArcade objects filtered by the status_transmission column
 * @method array findByErreur(string $erreur) Return CommonArchiveArcade objects filtered by the erreur column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonArchiveArcadeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonArchiveArcadeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonArchiveArcade', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonArchiveArcadeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonArchiveArcadeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonArchiveArcadeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonArchiveArcadeQuery) {
            return $criteria;
        }
        $query = new CommonArchiveArcadeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonArchiveArcade|CommonArchiveArcade[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonArchiveArcadePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonArchiveArcadePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonArchiveArcade A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonArchiveArcade A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `annee`, `num_semaine`, `poids_archive`, `chemin_fichier`, `date_envoi_debut`, `date_envoi_fin`, `status_transmission`, `erreur` FROM `archive_arcade` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonArchiveArcade();
            $obj->hydrate($row);
            CommonArchiveArcadePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonArchiveArcade|CommonArchiveArcade[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonArchiveArcade[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonArchiveArcadePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonArchiveArcadePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonArchiveArcadePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the annee column
     *
     * Example usage:
     * <code>
     * $query->filterByAnnee(1234); // WHERE annee = 1234
     * $query->filterByAnnee(array(12, 34)); // WHERE annee IN (12, 34)
     * $query->filterByAnnee(array('min' => 12)); // WHERE annee >= 12
     * $query->filterByAnnee(array('max' => 12)); // WHERE annee <= 12
     * </code>
     *
     * @param     mixed $annee The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByAnnee($annee = null, $comparison = null)
    {
        if (is_array($annee)) {
            $useMinMax = false;
            if (isset($annee['min'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::ANNEE, $annee['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($annee['max'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::ANNEE, $annee['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonArchiveArcadePeer::ANNEE, $annee, $comparison);
    }

    /**
     * Filter the query on the num_semaine column
     *
     * Example usage:
     * <code>
     * $query->filterByNumSemaine(1234); // WHERE num_semaine = 1234
     * $query->filterByNumSemaine(array(12, 34)); // WHERE num_semaine IN (12, 34)
     * $query->filterByNumSemaine(array('min' => 12)); // WHERE num_semaine >= 12
     * $query->filterByNumSemaine(array('max' => 12)); // WHERE num_semaine <= 12
     * </code>
     *
     * @param     mixed $numSemaine The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByNumSemaine($numSemaine = null, $comparison = null)
    {
        if (is_array($numSemaine)) {
            $useMinMax = false;
            if (isset($numSemaine['min'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::NUM_SEMAINE, $numSemaine['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numSemaine['max'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::NUM_SEMAINE, $numSemaine['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonArchiveArcadePeer::NUM_SEMAINE, $numSemaine, $comparison);
    }

    /**
     * Filter the query on the poids_archive column
     *
     * Example usage:
     * <code>
     * $query->filterByPoidsArchive(1234); // WHERE poids_archive = 1234
     * $query->filterByPoidsArchive(array(12, 34)); // WHERE poids_archive IN (12, 34)
     * $query->filterByPoidsArchive(array('min' => 12)); // WHERE poids_archive >= 12
     * $query->filterByPoidsArchive(array('max' => 12)); // WHERE poids_archive <= 12
     * </code>
     *
     * @param     mixed $poidsArchive The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByPoidsArchive($poidsArchive = null, $comparison = null)
    {
        if (is_array($poidsArchive)) {
            $useMinMax = false;
            if (isset($poidsArchive['min'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::POIDS_ARCHIVE, $poidsArchive['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($poidsArchive['max'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::POIDS_ARCHIVE, $poidsArchive['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonArchiveArcadePeer::POIDS_ARCHIVE, $poidsArchive, $comparison);
    }

    /**
     * Filter the query on the chemin_fichier column
     *
     * Example usage:
     * <code>
     * $query->filterByCheminFichier('fooValue');   // WHERE chemin_fichier = 'fooValue'
     * $query->filterByCheminFichier('%fooValue%'); // WHERE chemin_fichier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cheminFichier The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByCheminFichier($cheminFichier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cheminFichier)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cheminFichier)) {
                $cheminFichier = str_replace('*', '%', $cheminFichier);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonArchiveArcadePeer::CHEMIN_FICHIER, $cheminFichier, $comparison);
    }

    /**
     * Filter the query on the date_envoi_debut column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnvoiDebut('2011-03-14'); // WHERE date_envoi_debut = '2011-03-14'
     * $query->filterByDateEnvoiDebut('now'); // WHERE date_envoi_debut = '2011-03-14'
     * $query->filterByDateEnvoiDebut(array('max' => 'yesterday')); // WHERE date_envoi_debut > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnvoiDebut The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByDateEnvoiDebut($dateEnvoiDebut = null, $comparison = null)
    {
        if (is_array($dateEnvoiDebut)) {
            $useMinMax = false;
            if (isset($dateEnvoiDebut['min'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::DATE_ENVOI_DEBUT, $dateEnvoiDebut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnvoiDebut['max'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::DATE_ENVOI_DEBUT, $dateEnvoiDebut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonArchiveArcadePeer::DATE_ENVOI_DEBUT, $dateEnvoiDebut, $comparison);
    }

    /**
     * Filter the query on the date_envoi_fin column
     *
     * Example usage:
     * <code>
     * $query->filterByDateEnvoiFin('2011-03-14'); // WHERE date_envoi_fin = '2011-03-14'
     * $query->filterByDateEnvoiFin('now'); // WHERE date_envoi_fin = '2011-03-14'
     * $query->filterByDateEnvoiFin(array('max' => 'yesterday')); // WHERE date_envoi_fin > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateEnvoiFin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByDateEnvoiFin($dateEnvoiFin = null, $comparison = null)
    {
        if (is_array($dateEnvoiFin)) {
            $useMinMax = false;
            if (isset($dateEnvoiFin['min'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::DATE_ENVOI_FIN, $dateEnvoiFin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateEnvoiFin['max'])) {
                $this->addUsingAlias(CommonArchiveArcadePeer::DATE_ENVOI_FIN, $dateEnvoiFin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonArchiveArcadePeer::DATE_ENVOI_FIN, $dateEnvoiFin, $comparison);
    }

    /**
     * Filter the query on the status_transmission column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusTransmission(true); // WHERE status_transmission = true
     * $query->filterByStatusTransmission('yes'); // WHERE status_transmission = true
     * </code>
     *
     * @param     boolean|string $statusTransmission The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByStatusTransmission($statusTransmission = null, $comparison = null)
    {
        if (is_string($statusTransmission)) {
            $statusTransmission = in_array(strtolower($statusTransmission), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonArchiveArcadePeer::STATUS_TRANSMISSION, $statusTransmission, $comparison);
    }

    /**
     * Filter the query on the erreur column
     *
     * Example usage:
     * <code>
     * $query->filterByErreur('fooValue');   // WHERE erreur = 'fooValue'
     * $query->filterByErreur('%fooValue%'); // WHERE erreur LIKE '%fooValue%'
     * </code>
     *
     * @param     string $erreur The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function filterByErreur($erreur = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($erreur)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $erreur)) {
                $erreur = str_replace('*', '%', $erreur);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonArchiveArcadePeer::ERREUR, $erreur, $comparison);
    }

    /**
     * Filter the query by a related CommonConsultationArchiveArcade object
     *
     * @param   CommonConsultationArchiveArcade|PropelObjectCollection $commonConsultationArchiveArcade  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonArchiveArcadeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultationArchiveArcade($commonConsultationArchiveArcade, $comparison = null)
    {
        if ($commonConsultationArchiveArcade instanceof CommonConsultationArchiveArcade) {
            return $this
                ->addUsingAlias(CommonArchiveArcadePeer::ID, $commonConsultationArchiveArcade->getArchiveArcadeId(), $comparison);
        } elseif ($commonConsultationArchiveArcade instanceof PropelObjectCollection) {
            return $this
                ->useCommonConsultationArchiveArcadeQuery()
                ->filterByPrimaryKeys($commonConsultationArchiveArcade->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonConsultationArchiveArcade() only accepts arguments of type CommonConsultationArchiveArcade or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultationArchiveArcade relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function joinCommonConsultationArchiveArcade($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultationArchiveArcade');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultationArchiveArcade');
        }

        return $this;
    }

    /**
     * Use the CommonConsultationArchiveArcade relation CommonConsultationArchiveArcade object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationArchiveArcadeQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationArchiveArcadeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultationArchiveArcade($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultationArchiveArcade', '\Application\Propel\Mpe\CommonConsultationArchiveArcadeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonArchiveArcade $commonArchiveArcade Object to remove from the list of results
     *
     * @return CommonArchiveArcadeQuery The current query, for fluid interface
     */
    public function prune($commonArchiveArcade = null)
    {
        if ($commonArchiveArcade) {
            $this->addUsingAlias(CommonArchiveArcadePeer::ID, $commonArchiveArcade->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
