<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTNumerotationAutomatique;
use Application\Propel\Mpe\CommonTNumerotationAutomatiquePeer;
use Application\Propel\Mpe\CommonTNumerotationAutomatiqueQuery;

/**
 * Base class that represents a query for the 't_numerotation_automatique' table.
 *
 *
 *
 * @method CommonTNumerotationAutomatiqueQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTNumerotationAutomatiqueQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTNumerotationAutomatiqueQuery orderByOldServiceId($order = Criteria::ASC) Order by the old_service_id column
 * @method CommonTNumerotationAutomatiqueQuery orderByAnnee($order = Criteria::ASC) Order by the annee column
 * @method CommonTNumerotationAutomatiqueQuery orderByContratMulti($order = Criteria::ASC) Order by the contrat_multi column
 * @method CommonTNumerotationAutomatiqueQuery orderByContratSaDynamique($order = Criteria::ASC) Order by the contrat_SA_dynamique column
 * @method CommonTNumerotationAutomatiqueQuery orderByContratTitulaire($order = Criteria::ASC) Order by the contrat_titulaire column
 * @method CommonTNumerotationAutomatiqueQuery orderByServiceId($order = Criteria::ASC) Order by the service_id column
 *
 * @method CommonTNumerotationAutomatiqueQuery groupById() Group by the id column
 * @method CommonTNumerotationAutomatiqueQuery groupByOrganisme() Group by the organisme column
 * @method CommonTNumerotationAutomatiqueQuery groupByOldServiceId() Group by the old_service_id column
 * @method CommonTNumerotationAutomatiqueQuery groupByAnnee() Group by the annee column
 * @method CommonTNumerotationAutomatiqueQuery groupByContratMulti() Group by the contrat_multi column
 * @method CommonTNumerotationAutomatiqueQuery groupByContratSaDynamique() Group by the contrat_SA_dynamique column
 * @method CommonTNumerotationAutomatiqueQuery groupByContratTitulaire() Group by the contrat_titulaire column
 * @method CommonTNumerotationAutomatiqueQuery groupByServiceId() Group by the service_id column
 *
 * @method CommonTNumerotationAutomatiqueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTNumerotationAutomatiqueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTNumerotationAutomatiqueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTNumerotationAutomatique findOne(PropelPDO $con = null) Return the first CommonTNumerotationAutomatique matching the query
 * @method CommonTNumerotationAutomatique findOneOrCreate(PropelPDO $con = null) Return the first CommonTNumerotationAutomatique matching the query, or a new CommonTNumerotationAutomatique object populated from the query conditions when no match is found
 *
 * @method CommonTNumerotationAutomatique findOneByOrganisme(string $organisme) Return the first CommonTNumerotationAutomatique filtered by the organisme column
 * @method CommonTNumerotationAutomatique findOneByOldServiceId(int $old_service_id) Return the first CommonTNumerotationAutomatique filtered by the old_service_id column
 * @method CommonTNumerotationAutomatique findOneByAnnee(string $annee) Return the first CommonTNumerotationAutomatique filtered by the annee column
 * @method CommonTNumerotationAutomatique findOneByContratMulti(string $contrat_multi) Return the first CommonTNumerotationAutomatique filtered by the contrat_multi column
 * @method CommonTNumerotationAutomatique findOneByContratSaDynamique(string $contrat_SA_dynamique) Return the first CommonTNumerotationAutomatique filtered by the contrat_SA_dynamique column
 * @method CommonTNumerotationAutomatique findOneByContratTitulaire(string $contrat_titulaire) Return the first CommonTNumerotationAutomatique filtered by the contrat_titulaire column
 * @method CommonTNumerotationAutomatique findOneByServiceId(string $service_id) Return the first CommonTNumerotationAutomatique filtered by the service_id column
 *
 * @method array findById(int $id) Return CommonTNumerotationAutomatique objects filtered by the id column
 * @method array findByOrganisme(string $organisme) Return CommonTNumerotationAutomatique objects filtered by the organisme column
 * @method array findByOldServiceId(int $old_service_id) Return CommonTNumerotationAutomatique objects filtered by the old_service_id column
 * @method array findByAnnee(string $annee) Return CommonTNumerotationAutomatique objects filtered by the annee column
 * @method array findByContratMulti(string $contrat_multi) Return CommonTNumerotationAutomatique objects filtered by the contrat_multi column
 * @method array findByContratSaDynamique(string $contrat_SA_dynamique) Return CommonTNumerotationAutomatique objects filtered by the contrat_SA_dynamique column
 * @method array findByContratTitulaire(string $contrat_titulaire) Return CommonTNumerotationAutomatique objects filtered by the contrat_titulaire column
 * @method array findByServiceId(string $service_id) Return CommonTNumerotationAutomatique objects filtered by the service_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTNumerotationAutomatiqueQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTNumerotationAutomatiqueQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTNumerotationAutomatique', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTNumerotationAutomatiqueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTNumerotationAutomatiqueQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTNumerotationAutomatiqueQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTNumerotationAutomatiqueQuery) {
            return $criteria;
        }
        $query = new CommonTNumerotationAutomatiqueQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTNumerotationAutomatique|CommonTNumerotationAutomatique[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTNumerotationAutomatiquePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTNumerotationAutomatiquePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTNumerotationAutomatique A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTNumerotationAutomatique A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `organisme`, `old_service_id`, `annee`, `contrat_multi`, `contrat_SA_dynamique`, `contrat_titulaire`, `service_id` FROM `t_numerotation_automatique` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTNumerotationAutomatique();
            $obj->hydrate($row);
            CommonTNumerotationAutomatiquePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTNumerotationAutomatique|CommonTNumerotationAutomatique[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTNumerotationAutomatique[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTNumerotationAutomatiqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTNumerotationAutomatiqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumerotationAutomatiqueQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumerotationAutomatiqueQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the old_service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldServiceId(1234); // WHERE old_service_id = 1234
     * $query->filterByOldServiceId(array(12, 34)); // WHERE old_service_id IN (12, 34)
     * $query->filterByOldServiceId(array('min' => 12)); // WHERE old_service_id >= 12
     * $query->filterByOldServiceId(array('max' => 12)); // WHERE old_service_id <= 12
     * </code>
     *
     * @param     mixed $oldServiceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumerotationAutomatiqueQuery The current query, for fluid interface
     */
    public function filterByOldServiceId($oldServiceId = null, $comparison = null)
    {
        if (is_array($oldServiceId)) {
            $useMinMax = false;
            if (isset($oldServiceId['min'])) {
                $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::OLD_SERVICE_ID, $oldServiceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldServiceId['max'])) {
                $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::OLD_SERVICE_ID, $oldServiceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::OLD_SERVICE_ID, $oldServiceId, $comparison);
    }

    /**
     * Filter the query on the annee column
     *
     * Example usage:
     * <code>
     * $query->filterByAnnee('fooValue');   // WHERE annee = 'fooValue'
     * $query->filterByAnnee('%fooValue%'); // WHERE annee LIKE '%fooValue%'
     * </code>
     *
     * @param     string $annee The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumerotationAutomatiqueQuery The current query, for fluid interface
     */
    public function filterByAnnee($annee = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($annee)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $annee)) {
                $annee = str_replace('*', '%', $annee);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::ANNEE, $annee, $comparison);
    }

    /**
     * Filter the query on the contrat_multi column
     *
     * Example usage:
     * <code>
     * $query->filterByContratMulti('fooValue');   // WHERE contrat_multi = 'fooValue'
     * $query->filterByContratMulti('%fooValue%'); // WHERE contrat_multi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contratMulti The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumerotationAutomatiqueQuery The current query, for fluid interface
     */
    public function filterByContratMulti($contratMulti = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contratMulti)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $contratMulti)) {
                $contratMulti = str_replace('*', '%', $contratMulti);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::CONTRAT_MULTI, $contratMulti, $comparison);
    }

    /**
     * Filter the query on the contrat_SA_dynamique column
     *
     * Example usage:
     * <code>
     * $query->filterByContratSaDynamique('fooValue');   // WHERE contrat_SA_dynamique = 'fooValue'
     * $query->filterByContratSaDynamique('%fooValue%'); // WHERE contrat_SA_dynamique LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contratSaDynamique The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumerotationAutomatiqueQuery The current query, for fluid interface
     */
    public function filterByContratSaDynamique($contratSaDynamique = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contratSaDynamique)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $contratSaDynamique)) {
                $contratSaDynamique = str_replace('*', '%', $contratSaDynamique);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::CONTRAT_SA_DYNAMIQUE, $contratSaDynamique, $comparison);
    }

    /**
     * Filter the query on the contrat_titulaire column
     *
     * Example usage:
     * <code>
     * $query->filterByContratTitulaire('fooValue');   // WHERE contrat_titulaire = 'fooValue'
     * $query->filterByContratTitulaire('%fooValue%'); // WHERE contrat_titulaire LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contratTitulaire The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumerotationAutomatiqueQuery The current query, for fluid interface
     */
    public function filterByContratTitulaire($contratTitulaire = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contratTitulaire)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $contratTitulaire)) {
                $contratTitulaire = str_replace('*', '%', $contratTitulaire);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::CONTRAT_TITULAIRE, $contratTitulaire, $comparison);
    }

    /**
     * Filter the query on the service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceId(1234); // WHERE service_id = 1234
     * $query->filterByServiceId(array(12, 34)); // WHERE service_id IN (12, 34)
     * $query->filterByServiceId(array('min' => 12)); // WHERE service_id >= 12
     * $query->filterByServiceId(array('max' => 12)); // WHERE service_id <= 12
     * </code>
     *
     * @param     mixed $serviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTNumerotationAutomatiqueQuery The current query, for fluid interface
     */
    public function filterByServiceId($serviceId = null, $comparison = null)
    {
        if (is_array($serviceId)) {
            $useMinMax = false;
            if (isset($serviceId['min'])) {
                $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::SERVICE_ID, $serviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceId['max'])) {
                $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::SERVICE_ID, $serviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::SERVICE_ID, $serviceId, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTNumerotationAutomatique $commonTNumerotationAutomatique Object to remove from the list of results
     *
     * @return CommonTNumerotationAutomatiqueQuery The current query, for fluid interface
     */
    public function prune($commonTNumerotationAutomatique = null)
    {
        if ($commonTNumerotationAutomatique) {
            $this->addUsingAlias(CommonTNumerotationAutomatiquePeer::ID, $commonTNumerotationAutomatique->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
