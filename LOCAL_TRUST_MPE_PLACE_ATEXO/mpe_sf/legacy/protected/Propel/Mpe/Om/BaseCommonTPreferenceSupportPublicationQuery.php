<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTPreferenceSupportPublication;
use Application\Propel\Mpe\CommonTPreferenceSupportPublicationPeer;
use Application\Propel\Mpe\CommonTPreferenceSupportPublicationQuery;

/**
 * Base class that represents a query for the 't_preference_support_publication' table.
 *
 *
 *
 * @method CommonTPreferenceSupportPublicationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTPreferenceSupportPublicationQuery orderByIdSupport($order = Criteria::ASC) Order by the id_support column
 * @method CommonTPreferenceSupportPublicationQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 * @method CommonTPreferenceSupportPublicationQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTPreferenceSupportPublicationQuery orderByActive($order = Criteria::ASC) Order by the active column
 *
 * @method CommonTPreferenceSupportPublicationQuery groupById() Group by the id column
 * @method CommonTPreferenceSupportPublicationQuery groupByIdSupport() Group by the id_support column
 * @method CommonTPreferenceSupportPublicationQuery groupByIdAgent() Group by the id_agent column
 * @method CommonTPreferenceSupportPublicationQuery groupByOrganisme() Group by the organisme column
 * @method CommonTPreferenceSupportPublicationQuery groupByActive() Group by the active column
 *
 * @method CommonTPreferenceSupportPublicationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTPreferenceSupportPublicationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTPreferenceSupportPublicationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTPreferenceSupportPublication findOne(PropelPDO $con = null) Return the first CommonTPreferenceSupportPublication matching the query
 * @method CommonTPreferenceSupportPublication findOneOrCreate(PropelPDO $con = null) Return the first CommonTPreferenceSupportPublication matching the query, or a new CommonTPreferenceSupportPublication object populated from the query conditions when no match is found
 *
 * @method CommonTPreferenceSupportPublication findOneByIdSupport(int $id_support) Return the first CommonTPreferenceSupportPublication filtered by the id_support column
 * @method CommonTPreferenceSupportPublication findOneByIdAgent(int $id_agent) Return the first CommonTPreferenceSupportPublication filtered by the id_agent column
 * @method CommonTPreferenceSupportPublication findOneByOrganisme(string $organisme) Return the first CommonTPreferenceSupportPublication filtered by the organisme column
 * @method CommonTPreferenceSupportPublication findOneByActive(string $active) Return the first CommonTPreferenceSupportPublication filtered by the active column
 *
 * @method array findById(int $id) Return CommonTPreferenceSupportPublication objects filtered by the id column
 * @method array findByIdSupport(int $id_support) Return CommonTPreferenceSupportPublication objects filtered by the id_support column
 * @method array findByIdAgent(int $id_agent) Return CommonTPreferenceSupportPublication objects filtered by the id_agent column
 * @method array findByOrganisme(string $organisme) Return CommonTPreferenceSupportPublication objects filtered by the organisme column
 * @method array findByActive(string $active) Return CommonTPreferenceSupportPublication objects filtered by the active column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTPreferenceSupportPublicationQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTPreferenceSupportPublicationQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTPreferenceSupportPublication', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTPreferenceSupportPublicationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTPreferenceSupportPublicationQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTPreferenceSupportPublicationQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTPreferenceSupportPublicationQuery) {
            return $criteria;
        }
        $query = new CommonTPreferenceSupportPublicationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTPreferenceSupportPublication|CommonTPreferenceSupportPublication[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTPreferenceSupportPublicationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTPreferenceSupportPublicationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTPreferenceSupportPublication A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTPreferenceSupportPublication A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `id_support`, `id_agent`, `organisme`, `active` FROM `t_preference_support_publication` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTPreferenceSupportPublication();
            $obj->hydrate($row);
            CommonTPreferenceSupportPublicationPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTPreferenceSupportPublication|CommonTPreferenceSupportPublication[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTPreferenceSupportPublication[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTPreferenceSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTPreferenceSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPreferenceSupportPublicationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the id_support column
     *
     * Example usage:
     * <code>
     * $query->filterByIdSupport(1234); // WHERE id_support = 1234
     * $query->filterByIdSupport(array(12, 34)); // WHERE id_support IN (12, 34)
     * $query->filterByIdSupport(array('min' => 12)); // WHERE id_support >= 12
     * $query->filterByIdSupport(array('max' => 12)); // WHERE id_support <= 12
     * </code>
     *
     * @param     mixed $idSupport The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPreferenceSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByIdSupport($idSupport = null, $comparison = null)
    {
        if (is_array($idSupport)) {
            $useMinMax = false;
            if (isset($idSupport['min'])) {
                $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID_SUPPORT, $idSupport['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idSupport['max'])) {
                $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID_SUPPORT, $idSupport['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID_SUPPORT, $idSupport, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPreferenceSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPreferenceSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the active column
     *
     * Example usage:
     * <code>
     * $query->filterByActive('fooValue');   // WHERE active = 'fooValue'
     * $query->filterByActive('%fooValue%'); // WHERE active LIKE '%fooValue%'
     * </code>
     *
     * @param     string $active The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTPreferenceSupportPublicationQuery The current query, for fluid interface
     */
    public function filterByActive($active = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($active)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $active)) {
                $active = str_replace('*', '%', $active);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ACTIVE, $active, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTPreferenceSupportPublication $commonTPreferenceSupportPublication Object to remove from the list of results
     *
     * @return CommonTPreferenceSupportPublicationQuery The current query, for fluid interface
     */
    public function prune($commonTPreferenceSupportPublication = null)
    {
        if ($commonTPreferenceSupportPublication) {
            $this->addUsingAlias(CommonTPreferenceSupportPublicationPeer::ID, $commonTPreferenceSupportPublication->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
