<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonJmsJobDependencies;
use Application\Propel\Mpe\CommonJmsJobDependenciesPeer;
use Application\Propel\Mpe\CommonJmsJobDependenciesQuery;
use Application\Propel\Mpe\CommonJmsJobs;

/**
 * Base class that represents a query for the 'jms_job_dependencies' table.
 *
 *
 *
 * @method CommonJmsJobDependenciesQuery orderBySourceJobId($order = Criteria::ASC) Order by the source_job_id column
 * @method CommonJmsJobDependenciesQuery orderByDestJobId($order = Criteria::ASC) Order by the dest_job_id column
 *
 * @method CommonJmsJobDependenciesQuery groupBySourceJobId() Group by the source_job_id column
 * @method CommonJmsJobDependenciesQuery groupByDestJobId() Group by the dest_job_id column
 *
 * @method CommonJmsJobDependenciesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonJmsJobDependenciesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonJmsJobDependenciesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonJmsJobDependenciesQuery leftJoinCommonJmsJobsRelatedByDestJobId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonJmsJobsRelatedByDestJobId relation
 * @method CommonJmsJobDependenciesQuery rightJoinCommonJmsJobsRelatedByDestJobId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonJmsJobsRelatedByDestJobId relation
 * @method CommonJmsJobDependenciesQuery innerJoinCommonJmsJobsRelatedByDestJobId($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonJmsJobsRelatedByDestJobId relation
 *
 * @method CommonJmsJobDependenciesQuery leftJoinCommonJmsJobsRelatedBySourceJobId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonJmsJobsRelatedBySourceJobId relation
 * @method CommonJmsJobDependenciesQuery rightJoinCommonJmsJobsRelatedBySourceJobId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonJmsJobsRelatedBySourceJobId relation
 * @method CommonJmsJobDependenciesQuery innerJoinCommonJmsJobsRelatedBySourceJobId($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonJmsJobsRelatedBySourceJobId relation
 *
 * @method CommonJmsJobDependencies findOne(PropelPDO $con = null) Return the first CommonJmsJobDependencies matching the query
 * @method CommonJmsJobDependencies findOneOrCreate(PropelPDO $con = null) Return the first CommonJmsJobDependencies matching the query, or a new CommonJmsJobDependencies object populated from the query conditions when no match is found
 *
 * @method CommonJmsJobDependencies findOneBySourceJobId(string $source_job_id) Return the first CommonJmsJobDependencies filtered by the source_job_id column
 * @method CommonJmsJobDependencies findOneByDestJobId(string $dest_job_id) Return the first CommonJmsJobDependencies filtered by the dest_job_id column
 *
 * @method array findBySourceJobId(string $source_job_id) Return CommonJmsJobDependencies objects filtered by the source_job_id column
 * @method array findByDestJobId(string $dest_job_id) Return CommonJmsJobDependencies objects filtered by the dest_job_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonJmsJobDependenciesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonJmsJobDependenciesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonJmsJobDependencies', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonJmsJobDependenciesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonJmsJobDependenciesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonJmsJobDependenciesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonJmsJobDependenciesQuery) {
            return $criteria;
        }
        $query = new CommonJmsJobDependenciesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$source_job_id, $dest_job_id]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonJmsJobDependencies|CommonJmsJobDependencies[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonJmsJobDependenciesPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonJmsJobDependenciesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonJmsJobDependencies A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `source_job_id`, `dest_job_id` FROM `jms_job_dependencies` WHERE `source_job_id` = :p0 AND `dest_job_id` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_STR);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonJmsJobDependencies();
            $obj->hydrate($row);
            CommonJmsJobDependenciesPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonJmsJobDependencies|CommonJmsJobDependencies[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonJmsJobDependencies[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonJmsJobDependenciesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(CommonJmsJobDependenciesPeer::SOURCE_JOB_ID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(CommonJmsJobDependenciesPeer::DEST_JOB_ID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonJmsJobDependenciesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(CommonJmsJobDependenciesPeer::SOURCE_JOB_ID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(CommonJmsJobDependenciesPeer::DEST_JOB_ID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the source_job_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySourceJobId(1234); // WHERE source_job_id = 1234
     * $query->filterBySourceJobId(array(12, 34)); // WHERE source_job_id IN (12, 34)
     * $query->filterBySourceJobId(array('min' => 12)); // WHERE source_job_id >= 12
     * $query->filterBySourceJobId(array('max' => 12)); // WHERE source_job_id <= 12
     * </code>
     *
     * @see       filterByCommonJmsJobsRelatedBySourceJobId()
     *
     * @param     mixed $sourceJobId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobDependenciesQuery The current query, for fluid interface
     */
    public function filterBySourceJobId($sourceJobId = null, $comparison = null)
    {
        if (is_array($sourceJobId)) {
            $useMinMax = false;
            if (isset($sourceJobId['min'])) {
                $this->addUsingAlias(CommonJmsJobDependenciesPeer::SOURCE_JOB_ID, $sourceJobId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sourceJobId['max'])) {
                $this->addUsingAlias(CommonJmsJobDependenciesPeer::SOURCE_JOB_ID, $sourceJobId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobDependenciesPeer::SOURCE_JOB_ID, $sourceJobId, $comparison);
    }

    /**
     * Filter the query on the dest_job_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDestJobId(1234); // WHERE dest_job_id = 1234
     * $query->filterByDestJobId(array(12, 34)); // WHERE dest_job_id IN (12, 34)
     * $query->filterByDestJobId(array('min' => 12)); // WHERE dest_job_id >= 12
     * $query->filterByDestJobId(array('max' => 12)); // WHERE dest_job_id <= 12
     * </code>
     *
     * @see       filterByCommonJmsJobsRelatedByDestJobId()
     *
     * @param     mixed $destJobId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonJmsJobDependenciesQuery The current query, for fluid interface
     */
    public function filterByDestJobId($destJobId = null, $comparison = null)
    {
        if (is_array($destJobId)) {
            $useMinMax = false;
            if (isset($destJobId['min'])) {
                $this->addUsingAlias(CommonJmsJobDependenciesPeer::DEST_JOB_ID, $destJobId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($destJobId['max'])) {
                $this->addUsingAlias(CommonJmsJobDependenciesPeer::DEST_JOB_ID, $destJobId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonJmsJobDependenciesPeer::DEST_JOB_ID, $destJobId, $comparison);
    }

    /**
     * Filter the query by a related CommonJmsJobs object
     *
     * @param   CommonJmsJobs|PropelObjectCollection $commonJmsJobs The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonJmsJobDependenciesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonJmsJobsRelatedByDestJobId($commonJmsJobs, $comparison = null)
    {
        if ($commonJmsJobs instanceof CommonJmsJobs) {
            return $this
                ->addUsingAlias(CommonJmsJobDependenciesPeer::DEST_JOB_ID, $commonJmsJobs->getId(), $comparison);
        } elseif ($commonJmsJobs instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonJmsJobDependenciesPeer::DEST_JOB_ID, $commonJmsJobs->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonJmsJobsRelatedByDestJobId() only accepts arguments of type CommonJmsJobs or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonJmsJobsRelatedByDestJobId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonJmsJobDependenciesQuery The current query, for fluid interface
     */
    public function joinCommonJmsJobsRelatedByDestJobId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonJmsJobsRelatedByDestJobId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonJmsJobsRelatedByDestJobId');
        }

        return $this;
    }

    /**
     * Use the CommonJmsJobsRelatedByDestJobId relation CommonJmsJobs object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonJmsJobsQuery A secondary query class using the current class as primary query
     */
    public function useCommonJmsJobsRelatedByDestJobIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonJmsJobsRelatedByDestJobId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonJmsJobsRelatedByDestJobId', '\Application\Propel\Mpe\CommonJmsJobsQuery');
    }

    /**
     * Filter the query by a related CommonJmsJobs object
     *
     * @param   CommonJmsJobs|PropelObjectCollection $commonJmsJobs The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonJmsJobDependenciesQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonJmsJobsRelatedBySourceJobId($commonJmsJobs, $comparison = null)
    {
        if ($commonJmsJobs instanceof CommonJmsJobs) {
            return $this
                ->addUsingAlias(CommonJmsJobDependenciesPeer::SOURCE_JOB_ID, $commonJmsJobs->getId(), $comparison);
        } elseif ($commonJmsJobs instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonJmsJobDependenciesPeer::SOURCE_JOB_ID, $commonJmsJobs->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonJmsJobsRelatedBySourceJobId() only accepts arguments of type CommonJmsJobs or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonJmsJobsRelatedBySourceJobId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonJmsJobDependenciesQuery The current query, for fluid interface
     */
    public function joinCommonJmsJobsRelatedBySourceJobId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonJmsJobsRelatedBySourceJobId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonJmsJobsRelatedBySourceJobId');
        }

        return $this;
    }

    /**
     * Use the CommonJmsJobsRelatedBySourceJobId relation CommonJmsJobs object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonJmsJobsQuery A secondary query class using the current class as primary query
     */
    public function useCommonJmsJobsRelatedBySourceJobIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonJmsJobsRelatedBySourceJobId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonJmsJobsRelatedBySourceJobId', '\Application\Propel\Mpe\CommonJmsJobsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonJmsJobDependencies $commonJmsJobDependencies Object to remove from the list of results
     *
     * @return CommonJmsJobDependenciesQuery The current query, for fluid interface
     */
    public function prune($commonJmsJobDependencies = null)
    {
        if ($commonJmsJobDependencies) {
            $this->addCond('pruneCond0', $this->getAliasedColName(CommonJmsJobDependenciesPeer::SOURCE_JOB_ID), $commonJmsJobDependencies->getSourceJobId(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(CommonJmsJobDependenciesPeer::DEST_JOB_ID), $commonJmsJobDependencies->getDestJobId(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
