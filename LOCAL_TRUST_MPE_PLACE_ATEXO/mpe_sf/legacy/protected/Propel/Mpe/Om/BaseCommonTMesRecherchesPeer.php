<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonPlateformeVirtuellePeer;
use Application\Propel\Mpe\CommonTMesRecherches;
use Application\Propel\Mpe\CommonTMesRecherchesPeer;
use Application\Propel\Mpe\Map\CommonTMesRecherchesTableMap;

/**
 * Base static class for performing query and update operations on the 'T_MesRecherches' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTMesRecherchesPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'T_MesRecherches';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonTMesRecherches';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonTMesRecherchesTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 17;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 17;

    /** the column name for the id field */
    const ID = 'T_MesRecherches.id';

    /** the column name for the id_createur field */
    const ID_CREATEUR = 'T_MesRecherches.id_createur';

    /** the column name for the type_createur field */
    const TYPE_CREATEUR = 'T_MesRecherches.type_createur';

    /** the column name for the denomination field */
    const DENOMINATION = 'T_MesRecherches.denomination';

    /** the column name for the periodicite field */
    const PERIODICITE = 'T_MesRecherches.periodicite';

    /** the column name for the xmlCriteria field */
    const XMLCRITERIA = 'T_MesRecherches.xmlCriteria';

    /** the column name for the categorie field */
    const CATEGORIE = 'T_MesRecherches.categorie';

    /** the column name for the id_initial field */
    const ID_INITIAL = 'T_MesRecherches.id_initial';

    /** the column name for the format field */
    const FORMAT = 'T_MesRecherches.format';

    /** the column name for the date_creation field */
    const DATE_CREATION = 'T_MesRecherches.date_creation';

    /** the column name for the date_modification field */
    const DATE_MODIFICATION = 'T_MesRecherches.date_modification';

    /** the column name for the recherche field */
    const RECHERCHE = 'T_MesRecherches.recherche';

    /** the column name for the alerte field */
    const ALERTE = 'T_MesRecherches.alerte';

    /** the column name for the type_avis field */
    const TYPE_AVIS = 'T_MesRecherches.type_avis';

    /** the column name for the plateforme_virtuelle_id field */
    const PLATEFORME_VIRTUELLE_ID = 'T_MesRecherches.plateforme_virtuelle_id';

    /** the column name for the criteria field */
    const CRITERIA = 'T_MesRecherches.criteria';

    /** the column name for the hashed_criteria field */
    const HASHED_CRITERIA = 'T_MesRecherches.hashed_criteria';

    /** The enumerated values for the recherche field */
    const RECHERCHE_0 = '0';
    const RECHERCHE_1 = '1';

    /** The enumerated values for the alerte field */
    const ALERTE_0 = '0';
    const ALERTE_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonTMesRecherches objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonTMesRecherches[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonTMesRecherchesPeer::$fieldNames[CommonTMesRecherchesPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'IdCreateur', 'TypeCreateur', 'Denomination', 'Periodicite', 'Xmlcriteria', 'Categorie', 'IdInitial', 'Format', 'DateCreation', 'DateModification', 'Recherche', 'Alerte', 'TypeAvis', 'PlateformeVirtuelleId', 'Criteria', 'HashedCriteria', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'idCreateur', 'typeCreateur', 'denomination', 'periodicite', 'xmlcriteria', 'categorie', 'idInitial', 'format', 'dateCreation', 'dateModification', 'recherche', 'alerte', 'typeAvis', 'plateformeVirtuelleId', 'criteria', 'hashedCriteria', ),
        BasePeer::TYPE_COLNAME => array (CommonTMesRecherchesPeer::ID, CommonTMesRecherchesPeer::ID_CREATEUR, CommonTMesRecherchesPeer::TYPE_CREATEUR, CommonTMesRecherchesPeer::DENOMINATION, CommonTMesRecherchesPeer::PERIODICITE, CommonTMesRecherchesPeer::XMLCRITERIA, CommonTMesRecherchesPeer::CATEGORIE, CommonTMesRecherchesPeer::ID_INITIAL, CommonTMesRecherchesPeer::FORMAT, CommonTMesRecherchesPeer::DATE_CREATION, CommonTMesRecherchesPeer::DATE_MODIFICATION, CommonTMesRecherchesPeer::RECHERCHE, CommonTMesRecherchesPeer::ALERTE, CommonTMesRecherchesPeer::TYPE_AVIS, CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID, CommonTMesRecherchesPeer::CRITERIA, CommonTMesRecherchesPeer::HASHED_CRITERIA, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'ID_CREATEUR', 'TYPE_CREATEUR', 'DENOMINATION', 'PERIODICITE', 'XMLCRITERIA', 'CATEGORIE', 'ID_INITIAL', 'FORMAT', 'DATE_CREATION', 'DATE_MODIFICATION', 'RECHERCHE', 'ALERTE', 'TYPE_AVIS', 'PLATEFORME_VIRTUELLE_ID', 'CRITERIA', 'HASHED_CRITERIA', ),
        BasePeer::TYPE_FIELDNAME => array ('id', 'id_createur', 'type_createur', 'denomination', 'periodicite', 'xmlCriteria', 'categorie', 'id_initial', 'format', 'date_creation', 'date_modification', 'recherche', 'alerte', 'type_avis', 'plateforme_virtuelle_id', 'criteria', 'hashed_criteria', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonTMesRecherchesPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'IdCreateur' => 1, 'TypeCreateur' => 2, 'Denomination' => 3, 'Periodicite' => 4, 'Xmlcriteria' => 5, 'Categorie' => 6, 'IdInitial' => 7, 'Format' => 8, 'DateCreation' => 9, 'DateModification' => 10, 'Recherche' => 11, 'Alerte' => 12, 'TypeAvis' => 13, 'PlateformeVirtuelleId' => 14, 'Criteria' => 15, 'HashedCriteria' => 16, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'idCreateur' => 1, 'typeCreateur' => 2, 'denomination' => 3, 'periodicite' => 4, 'xmlcriteria' => 5, 'categorie' => 6, 'idInitial' => 7, 'format' => 8, 'dateCreation' => 9, 'dateModification' => 10, 'recherche' => 11, 'alerte' => 12, 'typeAvis' => 13, 'plateformeVirtuelleId' => 14, 'criteria' => 15, 'hashedCriteria' => 16, ),
        BasePeer::TYPE_COLNAME => array (CommonTMesRecherchesPeer::ID => 0, CommonTMesRecherchesPeer::ID_CREATEUR => 1, CommonTMesRecherchesPeer::TYPE_CREATEUR => 2, CommonTMesRecherchesPeer::DENOMINATION => 3, CommonTMesRecherchesPeer::PERIODICITE => 4, CommonTMesRecherchesPeer::XMLCRITERIA => 5, CommonTMesRecherchesPeer::CATEGORIE => 6, CommonTMesRecherchesPeer::ID_INITIAL => 7, CommonTMesRecherchesPeer::FORMAT => 8, CommonTMesRecherchesPeer::DATE_CREATION => 9, CommonTMesRecherchesPeer::DATE_MODIFICATION => 10, CommonTMesRecherchesPeer::RECHERCHE => 11, CommonTMesRecherchesPeer::ALERTE => 12, CommonTMesRecherchesPeer::TYPE_AVIS => 13, CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID => 14, CommonTMesRecherchesPeer::CRITERIA => 15, CommonTMesRecherchesPeer::HASHED_CRITERIA => 16, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'ID_CREATEUR' => 1, 'TYPE_CREATEUR' => 2, 'DENOMINATION' => 3, 'PERIODICITE' => 4, 'XMLCRITERIA' => 5, 'CATEGORIE' => 6, 'ID_INITIAL' => 7, 'FORMAT' => 8, 'DATE_CREATION' => 9, 'DATE_MODIFICATION' => 10, 'RECHERCHE' => 11, 'ALERTE' => 12, 'TYPE_AVIS' => 13, 'PLATEFORME_VIRTUELLE_ID' => 14, 'CRITERIA' => 15, 'HASHED_CRITERIA' => 16, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'id_createur' => 1, 'type_createur' => 2, 'denomination' => 3, 'periodicite' => 4, 'xmlCriteria' => 5, 'categorie' => 6, 'id_initial' => 7, 'format' => 8, 'date_creation' => 9, 'date_modification' => 10, 'recherche' => 11, 'alerte' => 12, 'type_avis' => 13, 'plateforme_virtuelle_id' => 14, 'criteria' => 15, 'hashed_criteria' => 16, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonTMesRecherchesPeer::RECHERCHE => array(
            CommonTMesRecherchesPeer::RECHERCHE_0,
            CommonTMesRecherchesPeer::RECHERCHE_1,
        ),
        CommonTMesRecherchesPeer::ALERTE => array(
            CommonTMesRecherchesPeer::ALERTE_0,
            CommonTMesRecherchesPeer::ALERTE_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonTMesRecherchesPeer::getFieldNames($toType);
        $key = isset(CommonTMesRecherchesPeer::$fieldKeys[$fromType][$name]) ? CommonTMesRecherchesPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonTMesRecherchesPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonTMesRecherchesPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonTMesRecherchesPeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonTMesRecherchesPeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonTMesRecherchesPeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonTMesRecherchesPeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonTMesRecherchesPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonTMesRecherchesPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::ID);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::ID_CREATEUR);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::TYPE_CREATEUR);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::DENOMINATION);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::PERIODICITE);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::XMLCRITERIA);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::CATEGORIE);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::ID_INITIAL);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::FORMAT);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::DATE_CREATION);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::DATE_MODIFICATION);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::RECHERCHE);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::ALERTE);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::TYPE_AVIS);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::CRITERIA);
            $criteria->addSelectColumn(CommonTMesRecherchesPeer::HASHED_CRITERIA);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.id_createur');
            $criteria->addSelectColumn($alias . '.type_createur');
            $criteria->addSelectColumn($alias . '.denomination');
            $criteria->addSelectColumn($alias . '.periodicite');
            $criteria->addSelectColumn($alias . '.xmlCriteria');
            $criteria->addSelectColumn($alias . '.categorie');
            $criteria->addSelectColumn($alias . '.id_initial');
            $criteria->addSelectColumn($alias . '.format');
            $criteria->addSelectColumn($alias . '.date_creation');
            $criteria->addSelectColumn($alias . '.date_modification');
            $criteria->addSelectColumn($alias . '.recherche');
            $criteria->addSelectColumn($alias . '.alerte');
            $criteria->addSelectColumn($alias . '.type_avis');
            $criteria->addSelectColumn($alias . '.plateforme_virtuelle_id');
            $criteria->addSelectColumn($alias . '.criteria');
            $criteria->addSelectColumn($alias . '.hashed_criteria');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMesRecherchesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMesRecherchesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonTMesRecherchesPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonTMesRecherches
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonTMesRecherchesPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonTMesRecherchesPeer::populateObjects(CommonTMesRecherchesPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonTMesRecherchesPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTMesRecherchesPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonTMesRecherches $obj A CommonTMesRecherches object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonTMesRecherchesPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonTMesRecherches object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonTMesRecherches) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonTMesRecherches object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonTMesRecherchesPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonTMesRecherches Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonTMesRecherchesPeer::$instances[$key])) {
                return CommonTMesRecherchesPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonTMesRecherchesPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonTMesRecherchesPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to T_MesRecherches
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonTMesRecherchesPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonTMesRecherchesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonTMesRecherchesPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonTMesRecherchesPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonTMesRecherches object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonTMesRecherchesPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonTMesRecherchesPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonTMesRecherchesPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonTMesRecherchesPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonTMesRecherchesPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonPlateformeVirtuelle table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonPlateformeVirtuelle(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMesRecherchesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMesRecherchesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTMesRecherchesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonTMesRecherches objects pre-filled with their CommonPlateformeVirtuelle objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMesRecherches objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonPlateformeVirtuelle(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMesRecherchesPeer::DATABASE_NAME);
        }

        CommonTMesRecherchesPeer::addSelectColumns($criteria);
        $startcol = CommonTMesRecherchesPeer::NUM_HYDRATE_COLUMNS;
        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMesRecherchesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMesRecherchesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonTMesRecherchesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMesRecherchesPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonTMesRecherches) to $obj2 (CommonPlateformeVirtuelle)
                $obj2->addCommonTMesRecherches($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonTMesRecherchesPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonTMesRecherchesPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonTMesRecherchesPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonTMesRecherches objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonTMesRecherches objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonTMesRecherchesPeer::DATABASE_NAME);
        }

        CommonTMesRecherchesPeer::addSelectColumns($criteria);
        $startcol2 = CommonTMesRecherchesPeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonTMesRecherchesPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonTMesRecherchesPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonTMesRecherchesPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonTMesRecherchesPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonTMesRecherchesPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonPlateformeVirtuelle rows

            $key2 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonTMesRecherches) to the collection in $obj2 (CommonPlateformeVirtuelle)
                $obj2->addCommonTMesRecherches($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonTMesRecherchesPeer::DATABASE_NAME)->getTable(CommonTMesRecherchesPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonTMesRecherchesPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonTMesRecherchesPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonTMesRecherchesTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonTMesRecherchesPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonTMesRecherches or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTMesRecherches object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonTMesRecherches object
        }

        if ($criteria->containsKey(CommonTMesRecherchesPeer::ID) && $criteria->keyContainsValue(CommonTMesRecherchesPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonTMesRecherchesPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonTMesRecherchesPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonTMesRecherches or Criteria object.
     *
     * @param      mixed $values Criteria or CommonTMesRecherches object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonTMesRecherchesPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonTMesRecherchesPeer::ID);
            $value = $criteria->remove(CommonTMesRecherchesPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonTMesRecherchesPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonTMesRecherchesPeer::TABLE_NAME);
            }

        } else { // $values is CommonTMesRecherches object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonTMesRecherchesPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the T_MesRecherches table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(CommonTMesRecherchesPeer::TABLE_NAME, $con, CommonTMesRecherchesPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonTMesRecherchesPeer::clearInstancePool();
            CommonTMesRecherchesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonTMesRecherches or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonTMesRecherches object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            CommonTMesRecherchesPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonTMesRecherches) { // it's a model object
            // invalidate the cache for this single object
            CommonTMesRecherchesPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonTMesRecherchesPeer::DATABASE_NAME);
            $criteria->add(CommonTMesRecherchesPeer::ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                CommonTMesRecherchesPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(CommonTMesRecherchesPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonTMesRecherchesPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given CommonTMesRecherches object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonTMesRecherches $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonTMesRecherchesPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonTMesRecherchesPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonTMesRecherchesPeer::DATABASE_NAME, CommonTMesRecherchesPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonTMesRecherches
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonTMesRecherchesPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonTMesRecherchesPeer::DATABASE_NAME);
        $criteria->add(CommonTMesRecherchesPeer::ID, $pk);

        $v = CommonTMesRecherchesPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonTMesRecherches[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonTMesRecherchesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonTMesRecherchesPeer::DATABASE_NAME);
            $criteria->add(CommonTMesRecherchesPeer::ID, $pks, Criteria::IN);
            $objs = CommonTMesRecherchesPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonTMesRecherchesPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonTMesRecherchesPeer::buildTableMap();

