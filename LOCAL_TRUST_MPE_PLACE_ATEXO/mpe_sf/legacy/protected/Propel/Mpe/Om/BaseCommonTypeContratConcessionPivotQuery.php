<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTypeContratConcessionPivot;
use Application\Propel\Mpe\CommonTypeContratConcessionPivotPeer;
use Application\Propel\Mpe\CommonTypeContratConcessionPivotQuery;

/**
 * Base class that represents a query for the 'type_contrat_concession_pivot' table.
 *
 *
 *
 * @method CommonTypeContratConcessionPivotQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTypeContratConcessionPivotQuery orderByLibelle($order = Criteria::ASC) Order by the libelle column
 *
 * @method CommonTypeContratConcessionPivotQuery groupById() Group by the id column
 * @method CommonTypeContratConcessionPivotQuery groupByLibelle() Group by the libelle column
 *
 * @method CommonTypeContratConcessionPivotQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTypeContratConcessionPivotQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTypeContratConcessionPivotQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTypeContratConcessionPivotQuery leftJoinCommonTContratTitulaire($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonTypeContratConcessionPivotQuery rightJoinCommonTContratTitulaire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonTypeContratConcessionPivotQuery innerJoinCommonTContratTitulaire($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContratTitulaire relation
 *
 * @method CommonTypeContratConcessionPivot findOne(PropelPDO $con = null) Return the first CommonTypeContratConcessionPivot matching the query
 * @method CommonTypeContratConcessionPivot findOneOrCreate(PropelPDO $con = null) Return the first CommonTypeContratConcessionPivot matching the query, or a new CommonTypeContratConcessionPivot object populated from the query conditions when no match is found
 *
 * @method CommonTypeContratConcessionPivot findOneByLibelle(string $libelle) Return the first CommonTypeContratConcessionPivot filtered by the libelle column
 *
 * @method array findById(int $id) Return CommonTypeContratConcessionPivot objects filtered by the id column
 * @method array findByLibelle(string $libelle) Return CommonTypeContratConcessionPivot objects filtered by the libelle column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTypeContratConcessionPivotQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTypeContratConcessionPivotQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTypeContratConcessionPivot', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTypeContratConcessionPivotQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTypeContratConcessionPivotQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTypeContratConcessionPivotQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTypeContratConcessionPivotQuery) {
            return $criteria;
        }
        $query = new CommonTypeContratConcessionPivotQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTypeContratConcessionPivot|CommonTypeContratConcessionPivot[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTypeContratConcessionPivotPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTypeContratConcessionPivotPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTypeContratConcessionPivot A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTypeContratConcessionPivot A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `libelle` FROM `type_contrat_concession_pivot` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTypeContratConcessionPivot();
            $obj->hydrate($row);
            CommonTypeContratConcessionPivotPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTypeContratConcessionPivot|CommonTypeContratConcessionPivot[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTypeContratConcessionPivot[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTypeContratConcessionPivotQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTypeContratConcessionPivotPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTypeContratConcessionPivotQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTypeContratConcessionPivotPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTypeContratConcessionPivotQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTypeContratConcessionPivotPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTypeContratConcessionPivotPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTypeContratConcessionPivotPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the libelle column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelle('fooValue');   // WHERE libelle = 'fooValue'
     * $query->filterByLibelle('%fooValue%'); // WHERE libelle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTypeContratConcessionPivotQuery The current query, for fluid interface
     */
    public function filterByLibelle($libelle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelle)) {
                $libelle = str_replace('*', '%', $libelle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTypeContratConcessionPivotPeer::LIBELLE, $libelle, $comparison);
    }

    /**
     * Filter the query by a related CommonTContratTitulaire object
     *
     * @param   CommonTContratTitulaire|PropelObjectCollection $commonTContratTitulaire  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTypeContratConcessionPivotQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContratTitulaire($commonTContratTitulaire, $comparison = null)
    {
        if ($commonTContratTitulaire instanceof CommonTContratTitulaire) {
            return $this
                ->addUsingAlias(CommonTypeContratConcessionPivotPeer::ID, $commonTContratTitulaire->getIdTypeContratConcessionPivot(), $comparison);
        } elseif ($commonTContratTitulaire instanceof PropelObjectCollection) {
            return $this
                ->useCommonTContratTitulaireQuery()
                ->filterByPrimaryKeys($commonTContratTitulaire->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTContratTitulaire() only accepts arguments of type CommonTContratTitulaire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContratTitulaire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTypeContratConcessionPivotQuery The current query, for fluid interface
     */
    public function joinCommonTContratTitulaire($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContratTitulaire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContratTitulaire');
        }

        return $this;
    }

    /**
     * Use the CommonTContratTitulaire relation CommonTContratTitulaire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContratTitulaireQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContratTitulaireQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTContratTitulaire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContratTitulaire', '\Application\Propel\Mpe\CommonTContratTitulaireQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTypeContratConcessionPivot $commonTypeContratConcessionPivot Object to remove from the list of results
     *
     * @return CommonTypeContratConcessionPivotQuery The current query, for fluid interface
     */
    public function prune($commonTypeContratConcessionPivot = null)
    {
        if ($commonTypeContratConcessionPivot) {
            $this->addUsingAlias(CommonTypeContratConcessionPivotPeer::ID, $commonTypeContratConcessionPivot->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
