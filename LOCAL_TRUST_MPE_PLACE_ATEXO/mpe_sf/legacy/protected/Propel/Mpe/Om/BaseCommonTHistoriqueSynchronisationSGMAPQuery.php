<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTHistoriqueSynchronisationSGMAP;
use Application\Propel\Mpe\CommonTHistoriqueSynchronisationSGMAPPeer;
use Application\Propel\Mpe\CommonTHistoriqueSynchronisationSGMAPQuery;

/**
 * Base class that represents a query for the 't_historique_synchronisation_SGMAP' table.
 *
 *
 *
 * @method CommonTHistoriqueSynchronisationSGMAPQuery orderByIdHistorique($order = Criteria::ASC) Order by the id_historique column
 * @method CommonTHistoriqueSynchronisationSGMAPQuery orderByIdObjet($order = Criteria::ASC) Order by the id_objet column
 * @method CommonTHistoriqueSynchronisationSGMAPQuery orderByTypeObjet($order = Criteria::ASC) Order by the type_objet column
 * @method CommonTHistoriqueSynchronisationSGMAPQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method CommonTHistoriqueSynchronisationSGMAPQuery orderByJeton($order = Criteria::ASC) Order by the jeton column
 * @method CommonTHistoriqueSynchronisationSGMAPQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 *
 * @method CommonTHistoriqueSynchronisationSGMAPQuery groupByIdHistorique() Group by the id_historique column
 * @method CommonTHistoriqueSynchronisationSGMAPQuery groupByIdObjet() Group by the id_objet column
 * @method CommonTHistoriqueSynchronisationSGMAPQuery groupByTypeObjet() Group by the type_objet column
 * @method CommonTHistoriqueSynchronisationSGMAPQuery groupByCode() Group by the code column
 * @method CommonTHistoriqueSynchronisationSGMAPQuery groupByJeton() Group by the jeton column
 * @method CommonTHistoriqueSynchronisationSGMAPQuery groupByDateCreation() Group by the date_creation column
 *
 * @method CommonTHistoriqueSynchronisationSGMAPQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTHistoriqueSynchronisationSGMAPQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTHistoriqueSynchronisationSGMAPQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTHistoriqueSynchronisationSGMAP findOne(PropelPDO $con = null) Return the first CommonTHistoriqueSynchronisationSGMAP matching the query
 * @method CommonTHistoriqueSynchronisationSGMAP findOneOrCreate(PropelPDO $con = null) Return the first CommonTHistoriqueSynchronisationSGMAP matching the query, or a new CommonTHistoriqueSynchronisationSGMAP object populated from the query conditions when no match is found
 *
 * @method CommonTHistoriqueSynchronisationSGMAP findOneByIdObjet(int $id_objet) Return the first CommonTHistoriqueSynchronisationSGMAP filtered by the id_objet column
 * @method CommonTHistoriqueSynchronisationSGMAP findOneByTypeObjet(string $type_objet) Return the first CommonTHistoriqueSynchronisationSGMAP filtered by the type_objet column
 * @method CommonTHistoriqueSynchronisationSGMAP findOneByCode(string $code) Return the first CommonTHistoriqueSynchronisationSGMAP filtered by the code column
 * @method CommonTHistoriqueSynchronisationSGMAP findOneByJeton(string $jeton) Return the first CommonTHistoriqueSynchronisationSGMAP filtered by the jeton column
 * @method CommonTHistoriqueSynchronisationSGMAP findOneByDateCreation(string $date_creation) Return the first CommonTHistoriqueSynchronisationSGMAP filtered by the date_creation column
 *
 * @method array findByIdHistorique(int $id_historique) Return CommonTHistoriqueSynchronisationSGMAP objects filtered by the id_historique column
 * @method array findByIdObjet(int $id_objet) Return CommonTHistoriqueSynchronisationSGMAP objects filtered by the id_objet column
 * @method array findByTypeObjet(string $type_objet) Return CommonTHistoriqueSynchronisationSGMAP objects filtered by the type_objet column
 * @method array findByCode(string $code) Return CommonTHistoriqueSynchronisationSGMAP objects filtered by the code column
 * @method array findByJeton(string $jeton) Return CommonTHistoriqueSynchronisationSGMAP objects filtered by the jeton column
 * @method array findByDateCreation(string $date_creation) Return CommonTHistoriqueSynchronisationSGMAP objects filtered by the date_creation column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTHistoriqueSynchronisationSGMAPQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTHistoriqueSynchronisationSGMAPQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTHistoriqueSynchronisationSGMAP', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTHistoriqueSynchronisationSGMAPQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTHistoriqueSynchronisationSGMAPQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTHistoriqueSynchronisationSGMAPQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTHistoriqueSynchronisationSGMAPQuery) {
            return $criteria;
        }
        $query = new CommonTHistoriqueSynchronisationSGMAPQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTHistoriqueSynchronisationSGMAP|CommonTHistoriqueSynchronisationSGMAP[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTHistoriqueSynchronisationSGMAPPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTHistoriqueSynchronisationSGMAPPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTHistoriqueSynchronisationSGMAP A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdHistorique($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTHistoriqueSynchronisationSGMAP A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_historique`, `id_objet`, `type_objet`, `code`, `jeton`, `date_creation` FROM `t_historique_synchronisation_SGMAP` WHERE `id_historique` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTHistoriqueSynchronisationSGMAP();
            $obj->hydrate($row);
            CommonTHistoriqueSynchronisationSGMAPPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTHistoriqueSynchronisationSGMAP|CommonTHistoriqueSynchronisationSGMAP[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTHistoriqueSynchronisationSGMAP[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTHistoriqueSynchronisationSGMAPQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::ID_HISTORIQUE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTHistoriqueSynchronisationSGMAPQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::ID_HISTORIQUE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_historique column
     *
     * Example usage:
     * <code>
     * $query->filterByIdHistorique(1234); // WHERE id_historique = 1234
     * $query->filterByIdHistorique(array(12, 34)); // WHERE id_historique IN (12, 34)
     * $query->filterByIdHistorique(array('min' => 12)); // WHERE id_historique >= 12
     * $query->filterByIdHistorique(array('max' => 12)); // WHERE id_historique <= 12
     * </code>
     *
     * @param     mixed $idHistorique The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTHistoriqueSynchronisationSGMAPQuery The current query, for fluid interface
     */
    public function filterByIdHistorique($idHistorique = null, $comparison = null)
    {
        if (is_array($idHistorique)) {
            $useMinMax = false;
            if (isset($idHistorique['min'])) {
                $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::ID_HISTORIQUE, $idHistorique['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idHistorique['max'])) {
                $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::ID_HISTORIQUE, $idHistorique['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::ID_HISTORIQUE, $idHistorique, $comparison);
    }

    /**
     * Filter the query on the id_objet column
     *
     * Example usage:
     * <code>
     * $query->filterByIdObjet(1234); // WHERE id_objet = 1234
     * $query->filterByIdObjet(array(12, 34)); // WHERE id_objet IN (12, 34)
     * $query->filterByIdObjet(array('min' => 12)); // WHERE id_objet >= 12
     * $query->filterByIdObjet(array('max' => 12)); // WHERE id_objet <= 12
     * </code>
     *
     * @param     mixed $idObjet The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTHistoriqueSynchronisationSGMAPQuery The current query, for fluid interface
     */
    public function filterByIdObjet($idObjet = null, $comparison = null)
    {
        if (is_array($idObjet)) {
            $useMinMax = false;
            if (isset($idObjet['min'])) {
                $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::ID_OBJET, $idObjet['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idObjet['max'])) {
                $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::ID_OBJET, $idObjet['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::ID_OBJET, $idObjet, $comparison);
    }

    /**
     * Filter the query on the type_objet column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeObjet('fooValue');   // WHERE type_objet = 'fooValue'
     * $query->filterByTypeObjet('%fooValue%'); // WHERE type_objet LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeObjet The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTHistoriqueSynchronisationSGMAPQuery The current query, for fluid interface
     */
    public function filterByTypeObjet($typeObjet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeObjet)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeObjet)) {
                $typeObjet = str_replace('*', '%', $typeObjet);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::TYPE_OBJET, $typeObjet, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTHistoriqueSynchronisationSGMAPQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the jeton column
     *
     * Example usage:
     * <code>
     * $query->filterByJeton('fooValue');   // WHERE jeton = 'fooValue'
     * $query->filterByJeton('%fooValue%'); // WHERE jeton LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jeton The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTHistoriqueSynchronisationSGMAPQuery The current query, for fluid interface
     */
    public function filterByJeton($jeton = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jeton)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $jeton)) {
                $jeton = str_replace('*', '%', $jeton);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::JETON, $jeton, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('fooValue');   // WHERE date_creation = 'fooValue'
     * $query->filterByDateCreation('%fooValue%'); // WHERE date_creation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dateCreation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTHistoriqueSynchronisationSGMAPQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dateCreation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dateCreation)) {
                $dateCreation = str_replace('*', '%', $dateCreation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTHistoriqueSynchronisationSGMAP $commonTHistoriqueSynchronisationSGMAP Object to remove from the list of results
     *
     * @return CommonTHistoriqueSynchronisationSGMAPQuery The current query, for fluid interface
     */
    public function prune($commonTHistoriqueSynchronisationSGMAP = null)
    {
        if ($commonTHistoriqueSynchronisationSGMAP) {
            $this->addUsingAlias(CommonTHistoriqueSynchronisationSGMAPPeer::ID_HISTORIQUE, $commonTHistoriqueSynchronisationSGMAP->getIdHistorique(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
