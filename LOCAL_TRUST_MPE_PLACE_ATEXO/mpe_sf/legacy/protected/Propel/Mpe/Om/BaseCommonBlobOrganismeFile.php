<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAnnexeFinanciere;
use Application\Propel\Mpe\CommonAnnexeFinanciereQuery;
use Application\Propel\Mpe\CommonAutrePieceConsultation;
use Application\Propel\Mpe\CommonAutrePieceConsultationQuery;
use Application\Propel\Mpe\CommonBlobOrganismeFile;
use Application\Propel\Mpe\CommonBlobOrganismeFilePeer;
use Application\Propel\Mpe\CommonBlobOrganismeFileQuery;
use Application\Propel\Mpe\CommonDocumentExterne;
use Application\Propel\Mpe\CommonDocumentExterneQuery;
use Application\Propel\Mpe\CommonEchangeDocBlob;
use Application\Propel\Mpe\CommonEchangeDocBlobQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonPieceGenereConsultation;
use Application\Propel\Mpe\CommonPieceGenereConsultationQuery;

/**
 * Base class that represents a row from the 'blobOrganisme_file' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonBlobOrganismeFile extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonBlobOrganismeFilePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonBlobOrganismeFilePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the old_id field.
     * @var        int
     */
    protected $old_id;

    /**
     * The value for the organisme field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the deletion_datetime field.
     * @var        string
     */
    protected $deletion_datetime;

    /**
     * The value for the chemin field.
     * @var        string
     */
    protected $chemin;

    /**
     * The value for the dossier field.
     * @var        string
     */
    protected $dossier;

    /**
     * The value for the statut_synchro field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $statut_synchro;

    /**
     * The value for the hash field.
     * Note: this column has a database default value of: 'ND'
     * @var        string
     */
    protected $hash;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the extension field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $extension;

    /**
     * @var        PropelObjectCollection|CommonDocumentExterne[] Collection to store aggregation of CommonDocumentExterne objects.
     */
    protected $collCommonDocumentExternes;
    protected $collCommonDocumentExternesPartial;

    /**
     * @var        PropelObjectCollection|CommonOffres[] Collection to store aggregation of CommonOffres objects.
     */
    protected $collCommonOffressRelatedByIdBlobHorodatageHash;
    protected $collCommonOffressRelatedByIdBlobHorodatageHashPartial;

    /**
     * @var        PropelObjectCollection|CommonOffres[] Collection to store aggregation of CommonOffres objects.
     */
    protected $collCommonOffressRelatedByIdBlobXmlReponse;
    protected $collCommonOffressRelatedByIdBlobXmlReponsePartial;

    /**
     * @var        PropelObjectCollection|CommonAnnexeFinanciere[] Collection to store aggregation of CommonAnnexeFinanciere objects.
     */
    protected $collCommonAnnexeFinancieres;
    protected $collCommonAnnexeFinancieresPartial;

    /**
     * @var        PropelObjectCollection|CommonAutrePieceConsultation[] Collection to store aggregation of CommonAutrePieceConsultation objects.
     */
    protected $collCommonAutrePieceConsultations;
    protected $collCommonAutrePieceConsultationsPartial;

    /**
     * @var        PropelObjectCollection|CommonEchangeDocBlob[] Collection to store aggregation of CommonEchangeDocBlob objects.
     */
    protected $collCommonEchangeDocBlobs;
    protected $collCommonEchangeDocBlobsPartial;

    /**
     * @var        PropelObjectCollection|CommonPieceGenereConsultation[] Collection to store aggregation of CommonPieceGenereConsultation objects.
     */
    protected $collCommonPieceGenereConsultations;
    protected $collCommonPieceGenereConsultationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonDocumentExternesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonOffressRelatedByIdBlobHorodatageHashScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonOffressRelatedByIdBlobXmlReponseScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAnnexeFinancieresScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAutrePieceConsultationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangeDocBlobsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonPieceGenereConsultationsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->organisme = '';
        $this->statut_synchro = 0;
        $this->hash = 'ND';
        $this->extension = '';
    }

    /**
     * Initializes internal state of BaseCommonBlobOrganismeFile object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [old_id] column value.
     *
     * @return int
     */
    public function getOldId()
    {

        return $this->old_id;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [optionally formatted] temporal [deletion_datetime] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletionDatetime($format = 'Y-m-d H:i:s')
    {
        if ($this->deletion_datetime === null) {
            return null;
        }

        if ($this->deletion_datetime === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->deletion_datetime);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->deletion_datetime, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [chemin] column value.
     *
     * @return string
     */
    public function getChemin()
    {

        return $this->chemin;
    }

    /**
     * Get the [dossier] column value.
     *
     * @return string
     */
    public function getDossier()
    {

        return $this->dossier;
    }

    /**
     * Get the [statut_synchro] column value.
     *
     * @return int
     */
    public function getStatutSynchro()
    {

        return $this->statut_synchro;
    }

    /**
     * Get the [hash] column value.
     *
     * @return string
     */
    public function getHash()
    {

        return $this->hash;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [extension] column value.
     *
     * @return string
     */
    public function getExtension()
    {

        return $this->extension;
    }

    /**
     * Set the value of [old_id] column.
     *
     * @param int $v new value
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setOldId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_id !== $v) {
            $this->old_id = $v;
            $this->modifiedColumns[] = CommonBlobOrganismeFilePeer::OLD_ID;
        }


        return $this;
    } // setOldId()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonBlobOrganismeFilePeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [name] column.
     *
     * @param string $v new value
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = CommonBlobOrganismeFilePeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Sets the value of [deletion_datetime] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setDeletionDatetime($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deletion_datetime !== null || $dt !== null) {
            $currentDateAsString = ($this->deletion_datetime !== null && $tmpDt = new DateTime($this->deletion_datetime)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->deletion_datetime = $newDateAsString;
                $this->modifiedColumns[] = CommonBlobOrganismeFilePeer::DELETION_DATETIME;
            }
        } // if either are not null


        return $this;
    } // setDeletionDatetime()

    /**
     * Set the value of [chemin] column.
     *
     * @param string $v new value
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setChemin($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin !== $v) {
            $this->chemin = $v;
            $this->modifiedColumns[] = CommonBlobOrganismeFilePeer::CHEMIN;
        }


        return $this;
    } // setChemin()

    /**
     * Set the value of [dossier] column.
     *
     * @param string $v new value
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setDossier($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->dossier !== $v) {
            $this->dossier = $v;
            $this->modifiedColumns[] = CommonBlobOrganismeFilePeer::DOSSIER;
        }


        return $this;
    } // setDossier()

    /**
     * Set the value of [statut_synchro] column.
     *
     * @param int $v new value
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setStatutSynchro($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->statut_synchro !== $v) {
            $this->statut_synchro = $v;
            $this->modifiedColumns[] = CommonBlobOrganismeFilePeer::STATUT_SYNCHRO;
        }


        return $this;
    } // setStatutSynchro()

    /**
     * Set the value of [hash] column.
     *
     * @param string $v new value
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setHash($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->hash !== $v) {
            $this->hash = $v;
            $this->modifiedColumns[] = CommonBlobOrganismeFilePeer::HASH;
        }


        return $this;
    } // setHash()

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonBlobOrganismeFilePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [extension] column.
     *
     * @param string $v new value
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setExtension($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->extension !== $v) {
            $this->extension = $v;
            $this->modifiedColumns[] = CommonBlobOrganismeFilePeer::EXTENSION;
        }


        return $this;
    } // setExtension()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->organisme !== '') {
                return false;
            }

            if ($this->statut_synchro !== 0) {
                return false;
            }

            if ($this->hash !== 'ND') {
                return false;
            }

            if ($this->extension !== '') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->old_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->organisme = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->name = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->deletion_datetime = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->chemin = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->dossier = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->statut_synchro = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->hash = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->id = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->extension = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 10; // 10 = CommonBlobOrganismeFilePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonBlobOrganismeFile object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonBlobOrganismeFilePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonBlobOrganismeFilePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCommonDocumentExternes = null;

            $this->collCommonOffressRelatedByIdBlobHorodatageHash = null;

            $this->collCommonOffressRelatedByIdBlobXmlReponse = null;

            $this->collCommonAnnexeFinancieres = null;

            $this->collCommonAutrePieceConsultations = null;

            $this->collCommonEchangeDocBlobs = null;

            $this->collCommonPieceGenereConsultations = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonBlobOrganismeFilePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonBlobOrganismeFileQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonBlobOrganismeFilePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonBlobOrganismeFilePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonDocumentExternesScheduledForDeletion !== null) {
                if (!$this->commonDocumentExternesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonDocumentExterneQuery::create()
                        ->filterByPrimaryKeys($this->commonDocumentExternesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonDocumentExternesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonDocumentExternes !== null) {
                foreach ($this->collCommonDocumentExternes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonOffressRelatedByIdBlobHorodatageHashScheduledForDeletion !== null) {
                if (!$this->commonOffressRelatedByIdBlobHorodatageHashScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonOffressRelatedByIdBlobHorodatageHashScheduledForDeletion as $commonOffresRelatedByIdBlobHorodatageHash) {
                        // need to save related object because we set the relation to null
                        $commonOffresRelatedByIdBlobHorodatageHash->save($con);
                    }
                    $this->commonOffressRelatedByIdBlobHorodatageHashScheduledForDeletion = null;
                }
            }

            if ($this->collCommonOffressRelatedByIdBlobHorodatageHash !== null) {
                foreach ($this->collCommonOffressRelatedByIdBlobHorodatageHash as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonOffressRelatedByIdBlobXmlReponseScheduledForDeletion !== null) {
                if (!$this->commonOffressRelatedByIdBlobXmlReponseScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonOffressRelatedByIdBlobXmlReponseScheduledForDeletion as $commonOffresRelatedByIdBlobXmlReponse) {
                        // need to save related object because we set the relation to null
                        $commonOffresRelatedByIdBlobXmlReponse->save($con);
                    }
                    $this->commonOffressRelatedByIdBlobXmlReponseScheduledForDeletion = null;
                }
            }

            if ($this->collCommonOffressRelatedByIdBlobXmlReponse !== null) {
                foreach ($this->collCommonOffressRelatedByIdBlobXmlReponse as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonAnnexeFinancieresScheduledForDeletion !== null) {
                if (!$this->commonAnnexeFinancieresScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonAnnexeFinancieresScheduledForDeletion as $commonAnnexeFinanciere) {
                        // need to save related object because we set the relation to null
                        $commonAnnexeFinanciere->save($con);
                    }
                    $this->commonAnnexeFinancieresScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAnnexeFinancieres !== null) {
                foreach ($this->collCommonAnnexeFinancieres as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonAutrePieceConsultationsScheduledForDeletion !== null) {
                if (!$this->commonAutrePieceConsultationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonAutrePieceConsultationsScheduledForDeletion as $commonAutrePieceConsultation) {
                        // need to save related object because we set the relation to null
                        $commonAutrePieceConsultation->save($con);
                    }
                    $this->commonAutrePieceConsultationsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAutrePieceConsultations !== null) {
                foreach ($this->collCommonAutrePieceConsultations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonEchangeDocBlobsScheduledForDeletion !== null) {
                if (!$this->commonEchangeDocBlobsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangeDocBlobsScheduledForDeletion as $commonEchangeDocBlob) {
                        // need to save related object because we set the relation to null
                        $commonEchangeDocBlob->save($con);
                    }
                    $this->commonEchangeDocBlobsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchangeDocBlobs !== null) {
                foreach ($this->collCommonEchangeDocBlobs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonPieceGenereConsultationsScheduledForDeletion !== null) {
                if (!$this->commonPieceGenereConsultationsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonPieceGenereConsultationQuery::create()
                        ->filterByPrimaryKeys($this->commonPieceGenereConsultationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonPieceGenereConsultationsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonPieceGenereConsultations !== null) {
                foreach ($this->collCommonPieceGenereConsultations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonBlobOrganismeFilePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonBlobOrganismeFilePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::OLD_ID)) {
            $modifiedColumns[':p' . $index++]  = '`old_id`';
        }
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`name`';
        }
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::DELETION_DATETIME)) {
            $modifiedColumns[':p' . $index++]  = '`deletion_datetime`';
        }
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::CHEMIN)) {
            $modifiedColumns[':p' . $index++]  = '`chemin`';
        }
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::DOSSIER)) {
            $modifiedColumns[':p' . $index++]  = '`dossier`';
        }
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::STATUT_SYNCHRO)) {
            $modifiedColumns[':p' . $index++]  = '`statut_synchro`';
        }
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::HASH)) {
            $modifiedColumns[':p' . $index++]  = '`hash`';
        }
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::EXTENSION)) {
            $modifiedColumns[':p' . $index++]  = '`extension`';
        }

        $sql = sprintf(
            'INSERT INTO `blobOrganisme_file` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`old_id`':
                        $stmt->bindValue($identifier, $this->old_id, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`name`':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`deletion_datetime`':
                        $stmt->bindValue($identifier, $this->deletion_datetime, PDO::PARAM_STR);
                        break;
                    case '`chemin`':
                        $stmt->bindValue($identifier, $this->chemin, PDO::PARAM_STR);
                        break;
                    case '`dossier`':
                        $stmt->bindValue($identifier, $this->dossier, PDO::PARAM_STR);
                        break;
                    case '`statut_synchro`':
                        $stmt->bindValue($identifier, $this->statut_synchro, PDO::PARAM_INT);
                        break;
                    case '`hash`':
                        $stmt->bindValue($identifier, $this->hash, PDO::PARAM_STR);
                        break;
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`extension`':
                        $stmt->bindValue($identifier, $this->extension, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonBlobOrganismeFilePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonDocumentExternes !== null) {
                    foreach ($this->collCommonDocumentExternes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonOffressRelatedByIdBlobHorodatageHash !== null) {
                    foreach ($this->collCommonOffressRelatedByIdBlobHorodatageHash as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonOffressRelatedByIdBlobXmlReponse !== null) {
                    foreach ($this->collCommonOffressRelatedByIdBlobXmlReponse as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonAnnexeFinancieres !== null) {
                    foreach ($this->collCommonAnnexeFinancieres as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonAutrePieceConsultations !== null) {
                    foreach ($this->collCommonAutrePieceConsultations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonEchangeDocBlobs !== null) {
                    foreach ($this->collCommonEchangeDocBlobs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonPieceGenereConsultations !== null) {
                    foreach ($this->collCommonPieceGenereConsultations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonBlobOrganismeFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getOldId();
                break;
            case 1:
                return $this->getOrganisme();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getDeletionDatetime();
                break;
            case 4:
                return $this->getChemin();
                break;
            case 5:
                return $this->getDossier();
                break;
            case 6:
                return $this->getStatutSynchro();
                break;
            case 7:
                return $this->getHash();
                break;
            case 8:
                return $this->getId();
                break;
            case 9:
                return $this->getExtension();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonBlobOrganismeFile'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonBlobOrganismeFile'][$this->getPrimaryKey()] = true;
        $keys = CommonBlobOrganismeFilePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getOldId(),
            $keys[1] => $this->getOrganisme(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getDeletionDatetime(),
            $keys[4] => $this->getChemin(),
            $keys[5] => $this->getDossier(),
            $keys[6] => $this->getStatutSynchro(),
            $keys[7] => $this->getHash(),
            $keys[8] => $this->getId(),
            $keys[9] => $this->getExtension(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collCommonDocumentExternes) {
                $result['CommonDocumentExternes'] = $this->collCommonDocumentExternes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonOffressRelatedByIdBlobHorodatageHash) {
                $result['CommonOffressRelatedByIdBlobHorodatageHash'] = $this->collCommonOffressRelatedByIdBlobHorodatageHash->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonOffressRelatedByIdBlobXmlReponse) {
                $result['CommonOffressRelatedByIdBlobXmlReponse'] = $this->collCommonOffressRelatedByIdBlobXmlReponse->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonAnnexeFinancieres) {
                $result['CommonAnnexeFinancieres'] = $this->collCommonAnnexeFinancieres->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonAutrePieceConsultations) {
                $result['CommonAutrePieceConsultations'] = $this->collCommonAutrePieceConsultations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonEchangeDocBlobs) {
                $result['CommonEchangeDocBlobs'] = $this->collCommonEchangeDocBlobs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonPieceGenereConsultations) {
                $result['CommonPieceGenereConsultations'] = $this->collCommonPieceGenereConsultations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonBlobOrganismeFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setOldId($value);
                break;
            case 1:
                $this->setOrganisme($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setDeletionDatetime($value);
                break;
            case 4:
                $this->setChemin($value);
                break;
            case 5:
                $this->setDossier($value);
                break;
            case 6:
                $this->setStatutSynchro($value);
                break;
            case 7:
                $this->setHash($value);
                break;
            case 8:
                $this->setId($value);
                break;
            case 9:
                $this->setExtension($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonBlobOrganismeFilePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setOldId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setOrganisme($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setName($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setDeletionDatetime($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setChemin($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDossier($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setStatutSynchro($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setHash($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setId($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setExtension($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonBlobOrganismeFilePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::OLD_ID)) $criteria->add(CommonBlobOrganismeFilePeer::OLD_ID, $this->old_id);
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::ORGANISME)) $criteria->add(CommonBlobOrganismeFilePeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::NAME)) $criteria->add(CommonBlobOrganismeFilePeer::NAME, $this->name);
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::DELETION_DATETIME)) $criteria->add(CommonBlobOrganismeFilePeer::DELETION_DATETIME, $this->deletion_datetime);
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::CHEMIN)) $criteria->add(CommonBlobOrganismeFilePeer::CHEMIN, $this->chemin);
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::DOSSIER)) $criteria->add(CommonBlobOrganismeFilePeer::DOSSIER, $this->dossier);
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::STATUT_SYNCHRO)) $criteria->add(CommonBlobOrganismeFilePeer::STATUT_SYNCHRO, $this->statut_synchro);
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::HASH)) $criteria->add(CommonBlobOrganismeFilePeer::HASH, $this->hash);
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::ID)) $criteria->add(CommonBlobOrganismeFilePeer::ID, $this->id);
        if ($this->isColumnModified(CommonBlobOrganismeFilePeer::EXTENSION)) $criteria->add(CommonBlobOrganismeFilePeer::EXTENSION, $this->extension);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonBlobOrganismeFilePeer::DATABASE_NAME);
        $criteria->add(CommonBlobOrganismeFilePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonBlobOrganismeFile (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOldId($this->getOldId());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setName($this->getName());
        $copyObj->setDeletionDatetime($this->getDeletionDatetime());
        $copyObj->setChemin($this->getChemin());
        $copyObj->setDossier($this->getDossier());
        $copyObj->setStatutSynchro($this->getStatutSynchro());
        $copyObj->setHash($this->getHash());
        $copyObj->setExtension($this->getExtension());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonDocumentExternes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonDocumentExterne($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonOffressRelatedByIdBlobHorodatageHash() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonOffresRelatedByIdBlobHorodatageHash($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonOffressRelatedByIdBlobXmlReponse() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonOffresRelatedByIdBlobXmlReponse($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonAnnexeFinancieres() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAnnexeFinanciere($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonAutrePieceConsultations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAutrePieceConsultation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonEchangeDocBlobs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchangeDocBlob($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonPieceGenereConsultations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonPieceGenereConsultation($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonBlobOrganismeFile Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonBlobOrganismeFilePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonBlobOrganismeFilePeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonDocumentExterne' == $relationName) {
            $this->initCommonDocumentExternes();
        }
        if ('CommonOffresRelatedByIdBlobHorodatageHash' == $relationName) {
            $this->initCommonOffressRelatedByIdBlobHorodatageHash();
        }
        if ('CommonOffresRelatedByIdBlobXmlReponse' == $relationName) {
            $this->initCommonOffressRelatedByIdBlobXmlReponse();
        }
        if ('CommonAnnexeFinanciere' == $relationName) {
            $this->initCommonAnnexeFinancieres();
        }
        if ('CommonAutrePieceConsultation' == $relationName) {
            $this->initCommonAutrePieceConsultations();
        }
        if ('CommonEchangeDocBlob' == $relationName) {
            $this->initCommonEchangeDocBlobs();
        }
        if ('CommonPieceGenereConsultation' == $relationName) {
            $this->initCommonPieceGenereConsultations();
        }
    }

    /**
     * Clears out the collCommonDocumentExternes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     * @see        addCommonDocumentExternes()
     */
    public function clearCommonDocumentExternes()
    {
        $this->collCommonDocumentExternes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonDocumentExternesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonDocumentExternes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonDocumentExternes($v = true)
    {
        $this->collCommonDocumentExternesPartial = $v;
    }

    /**
     * Initializes the collCommonDocumentExternes collection.
     *
     * By default this just sets the collCommonDocumentExternes collection to an empty array (like clearcollCommonDocumentExternes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonDocumentExternes($overrideExisting = true)
    {
        if (null !== $this->collCommonDocumentExternes && !$overrideExisting) {
            return;
        }
        $this->collCommonDocumentExternes = new PropelObjectCollection();
        $this->collCommonDocumentExternes->setModel('CommonDocumentExterne');
    }

    /**
     * Gets an array of CommonDocumentExterne objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonBlobOrganismeFile is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonDocumentExterne[] List of CommonDocumentExterne objects
     * @throws PropelException
     */
    public function getCommonDocumentExternes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonDocumentExternesPartial && !$this->isNew();
        if (null === $this->collCommonDocumentExternes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonDocumentExternes) {
                // return empty collection
                $this->initCommonDocumentExternes();
            } else {
                $collCommonDocumentExternes = CommonDocumentExterneQuery::create(null, $criteria)
                    ->filterByCommonBlobOrganismeFile($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonDocumentExternesPartial && count($collCommonDocumentExternes)) {
                      $this->initCommonDocumentExternes(false);

                      foreach ($collCommonDocumentExternes as $obj) {
                        if (false == $this->collCommonDocumentExternes->contains($obj)) {
                          $this->collCommonDocumentExternes->append($obj);
                        }
                      }

                      $this->collCommonDocumentExternesPartial = true;
                    }

                    $collCommonDocumentExternes->getInternalIterator()->rewind();

                    return $collCommonDocumentExternes;
                }

                if ($partial && $this->collCommonDocumentExternes) {
                    foreach ($this->collCommonDocumentExternes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonDocumentExternes[] = $obj;
                        }
                    }
                }

                $this->collCommonDocumentExternes = $collCommonDocumentExternes;
                $this->collCommonDocumentExternesPartial = false;
            }
        }

        return $this->collCommonDocumentExternes;
    }

    /**
     * Sets a collection of CommonDocumentExterne objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonDocumentExternes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setCommonDocumentExternes(PropelCollection $commonDocumentExternes, PropelPDO $con = null)
    {
        $commonDocumentExternesToDelete = $this->getCommonDocumentExternes(new Criteria(), $con)->diff($commonDocumentExternes);


        $this->commonDocumentExternesScheduledForDeletion = $commonDocumentExternesToDelete;

        foreach ($commonDocumentExternesToDelete as $commonDocumentExterneRemoved) {
            $commonDocumentExterneRemoved->setCommonBlobOrganismeFile(null);
        }

        $this->collCommonDocumentExternes = null;
        foreach ($commonDocumentExternes as $commonDocumentExterne) {
            $this->addCommonDocumentExterne($commonDocumentExterne);
        }

        $this->collCommonDocumentExternes = $commonDocumentExternes;
        $this->collCommonDocumentExternesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonDocumentExterne objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonDocumentExterne objects.
     * @throws PropelException
     */
    public function countCommonDocumentExternes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonDocumentExternesPartial && !$this->isNew();
        if (null === $this->collCommonDocumentExternes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonDocumentExternes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonDocumentExternes());
            }
            $query = CommonDocumentExterneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonBlobOrganismeFile($this)
                ->count($con);
        }

        return count($this->collCommonDocumentExternes);
    }

    /**
     * Method called to associate a CommonDocumentExterne object to this object
     * through the CommonDocumentExterne foreign key attribute.
     *
     * @param   CommonDocumentExterne $l CommonDocumentExterne
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function addCommonDocumentExterne(CommonDocumentExterne $l)
    {
        if ($this->collCommonDocumentExternes === null) {
            $this->initCommonDocumentExternes();
            $this->collCommonDocumentExternesPartial = true;
        }
        if (!in_array($l, $this->collCommonDocumentExternes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonDocumentExterne($l);
        }

        return $this;
    }

    /**
     * @param	CommonDocumentExterne $commonDocumentExterne The commonDocumentExterne object to add.
     */
    protected function doAddCommonDocumentExterne($commonDocumentExterne)
    {
        $this->collCommonDocumentExternes[]= $commonDocumentExterne;
        $commonDocumentExterne->setCommonBlobOrganismeFile($this);
    }

    /**
     * @param	CommonDocumentExterne $commonDocumentExterne The commonDocumentExterne object to remove.
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function removeCommonDocumentExterne($commonDocumentExterne)
    {
        if ($this->getCommonDocumentExternes()->contains($commonDocumentExterne)) {
            $this->collCommonDocumentExternes->remove($this->collCommonDocumentExternes->search($commonDocumentExterne));
            if (null === $this->commonDocumentExternesScheduledForDeletion) {
                $this->commonDocumentExternesScheduledForDeletion = clone $this->collCommonDocumentExternes;
                $this->commonDocumentExternesScheduledForDeletion->clear();
            }
            $this->commonDocumentExternesScheduledForDeletion[]= $commonDocumentExterne;
            $commonDocumentExterne->setCommonBlobOrganismeFile(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonDocumentExternes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonDocumentExterne[] List of CommonDocumentExterne objects
     */
    public function getCommonDocumentExternesJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonDocumentExterneQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonDocumentExternes($query, $con);
    }

    /**
     * Clears out the collCommonOffressRelatedByIdBlobHorodatageHash collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     * @see        addCommonOffressRelatedByIdBlobHorodatageHash()
     */
    public function clearCommonOffressRelatedByIdBlobHorodatageHash()
    {
        $this->collCommonOffressRelatedByIdBlobHorodatageHash = null; // important to set this to null since that means it is uninitialized
        $this->collCommonOffressRelatedByIdBlobHorodatageHashPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonOffressRelatedByIdBlobHorodatageHash collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonOffressRelatedByIdBlobHorodatageHash($v = true)
    {
        $this->collCommonOffressRelatedByIdBlobHorodatageHashPartial = $v;
    }

    /**
     * Initializes the collCommonOffressRelatedByIdBlobHorodatageHash collection.
     *
     * By default this just sets the collCommonOffressRelatedByIdBlobHorodatageHash collection to an empty array (like clearcollCommonOffressRelatedByIdBlobHorodatageHash());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonOffressRelatedByIdBlobHorodatageHash($overrideExisting = true)
    {
        if (null !== $this->collCommonOffressRelatedByIdBlobHorodatageHash && !$overrideExisting) {
            return;
        }
        $this->collCommonOffressRelatedByIdBlobHorodatageHash = new PropelObjectCollection();
        $this->collCommonOffressRelatedByIdBlobHorodatageHash->setModel('CommonOffres');
    }

    /**
     * Gets an array of CommonOffres objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonBlobOrganismeFile is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     * @throws PropelException
     */
    public function getCommonOffressRelatedByIdBlobHorodatageHash($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonOffressRelatedByIdBlobHorodatageHashPartial && !$this->isNew();
        if (null === $this->collCommonOffressRelatedByIdBlobHorodatageHash || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonOffressRelatedByIdBlobHorodatageHash) {
                // return empty collection
                $this->initCommonOffressRelatedByIdBlobHorodatageHash();
            } else {
                $collCommonOffressRelatedByIdBlobHorodatageHash = CommonOffresQuery::create(null, $criteria)
                    ->filterByCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonOffressRelatedByIdBlobHorodatageHashPartial && count($collCommonOffressRelatedByIdBlobHorodatageHash)) {
                      $this->initCommonOffressRelatedByIdBlobHorodatageHash(false);

                      foreach ($collCommonOffressRelatedByIdBlobHorodatageHash as $obj) {
                        if (false == $this->collCommonOffressRelatedByIdBlobHorodatageHash->contains($obj)) {
                          $this->collCommonOffressRelatedByIdBlobHorodatageHash->append($obj);
                        }
                      }

                      $this->collCommonOffressRelatedByIdBlobHorodatageHashPartial = true;
                    }

                    $collCommonOffressRelatedByIdBlobHorodatageHash->getInternalIterator()->rewind();

                    return $collCommonOffressRelatedByIdBlobHorodatageHash;
                }

                if ($partial && $this->collCommonOffressRelatedByIdBlobHorodatageHash) {
                    foreach ($this->collCommonOffressRelatedByIdBlobHorodatageHash as $obj) {
                        if ($obj->isNew()) {
                            $collCommonOffressRelatedByIdBlobHorodatageHash[] = $obj;
                        }
                    }
                }

                $this->collCommonOffressRelatedByIdBlobHorodatageHash = $collCommonOffressRelatedByIdBlobHorodatageHash;
                $this->collCommonOffressRelatedByIdBlobHorodatageHashPartial = false;
            }
        }

        return $this->collCommonOffressRelatedByIdBlobHorodatageHash;
    }

    /**
     * Sets a collection of CommonOffresRelatedByIdBlobHorodatageHash objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonOffressRelatedByIdBlobHorodatageHash A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setCommonOffressRelatedByIdBlobHorodatageHash(PropelCollection $commonOffressRelatedByIdBlobHorodatageHash, PropelPDO $con = null)
    {
        $commonOffressRelatedByIdBlobHorodatageHashToDelete = $this->getCommonOffressRelatedByIdBlobHorodatageHash(new Criteria(), $con)->diff($commonOffressRelatedByIdBlobHorodatageHash);


        $this->commonOffressRelatedByIdBlobHorodatageHashScheduledForDeletion = $commonOffressRelatedByIdBlobHorodatageHashToDelete;

        foreach ($commonOffressRelatedByIdBlobHorodatageHashToDelete as $commonOffresRelatedByIdBlobHorodatageHashRemoved) {
            $commonOffresRelatedByIdBlobHorodatageHashRemoved->setCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash(null);
        }

        $this->collCommonOffressRelatedByIdBlobHorodatageHash = null;
        foreach ($commonOffressRelatedByIdBlobHorodatageHash as $commonOffresRelatedByIdBlobHorodatageHash) {
            $this->addCommonOffresRelatedByIdBlobHorodatageHash($commonOffresRelatedByIdBlobHorodatageHash);
        }

        $this->collCommonOffressRelatedByIdBlobHorodatageHash = $commonOffressRelatedByIdBlobHorodatageHash;
        $this->collCommonOffressRelatedByIdBlobHorodatageHashPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonOffres objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonOffres objects.
     * @throws PropelException
     */
    public function countCommonOffressRelatedByIdBlobHorodatageHash(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonOffressRelatedByIdBlobHorodatageHashPartial && !$this->isNew();
        if (null === $this->collCommonOffressRelatedByIdBlobHorodatageHash || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonOffressRelatedByIdBlobHorodatageHash) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonOffressRelatedByIdBlobHorodatageHash());
            }
            $query = CommonOffresQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash($this)
                ->count($con);
        }

        return count($this->collCommonOffressRelatedByIdBlobHorodatageHash);
    }

    /**
     * Method called to associate a CommonOffres object to this object
     * through the CommonOffres foreign key attribute.
     *
     * @param   CommonOffres $l CommonOffres
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function addCommonOffresRelatedByIdBlobHorodatageHash(CommonOffres $l)
    {
        if ($this->collCommonOffressRelatedByIdBlobHorodatageHash === null) {
            $this->initCommonOffressRelatedByIdBlobHorodatageHash();
            $this->collCommonOffressRelatedByIdBlobHorodatageHashPartial = true;
        }
        if (!in_array($l, $this->collCommonOffressRelatedByIdBlobHorodatageHash->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonOffresRelatedByIdBlobHorodatageHash($l);
        }

        return $this;
    }

    /**
     * @param	CommonOffresRelatedByIdBlobHorodatageHash $commonOffresRelatedByIdBlobHorodatageHash The commonOffresRelatedByIdBlobHorodatageHash object to add.
     */
    protected function doAddCommonOffresRelatedByIdBlobHorodatageHash($commonOffresRelatedByIdBlobHorodatageHash)
    {
        $this->collCommonOffressRelatedByIdBlobHorodatageHash[]= $commonOffresRelatedByIdBlobHorodatageHash;
        $commonOffresRelatedByIdBlobHorodatageHash->setCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash($this);
    }

    /**
     * @param	CommonOffresRelatedByIdBlobHorodatageHash $commonOffresRelatedByIdBlobHorodatageHash The commonOffresRelatedByIdBlobHorodatageHash object to remove.
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function removeCommonOffresRelatedByIdBlobHorodatageHash($commonOffresRelatedByIdBlobHorodatageHash)
    {
        if ($this->getCommonOffressRelatedByIdBlobHorodatageHash()->contains($commonOffresRelatedByIdBlobHorodatageHash)) {
            $this->collCommonOffressRelatedByIdBlobHorodatageHash->remove($this->collCommonOffressRelatedByIdBlobHorodatageHash->search($commonOffresRelatedByIdBlobHorodatageHash));
            if (null === $this->commonOffressRelatedByIdBlobHorodatageHashScheduledForDeletion) {
                $this->commonOffressRelatedByIdBlobHorodatageHashScheduledForDeletion = clone $this->collCommonOffressRelatedByIdBlobHorodatageHash;
                $this->commonOffressRelatedByIdBlobHorodatageHashScheduledForDeletion->clear();
            }
            $this->commonOffressRelatedByIdBlobHorodatageHashScheduledForDeletion[]= $commonOffresRelatedByIdBlobHorodatageHash;
            $commonOffresRelatedByIdBlobHorodatageHash->setCommonBlobOrganismeFileRelatedByIdBlobHorodatageHash(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonOffressRelatedByIdBlobHorodatageHash from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressRelatedByIdBlobHorodatageHashJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonOffressRelatedByIdBlobHorodatageHash($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonOffressRelatedByIdBlobHorodatageHash from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressRelatedByIdBlobHorodatageHashJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonOffressRelatedByIdBlobHorodatageHash($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonOffressRelatedByIdBlobHorodatageHash from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressRelatedByIdBlobHorodatageHashJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonOffressRelatedByIdBlobHorodatageHash($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonOffressRelatedByIdBlobHorodatageHash from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressRelatedByIdBlobHorodatageHashJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonOffressRelatedByIdBlobHorodatageHash($query, $con);
    }

    /**
     * Clears out the collCommonOffressRelatedByIdBlobXmlReponse collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     * @see        addCommonOffressRelatedByIdBlobXmlReponse()
     */
    public function clearCommonOffressRelatedByIdBlobXmlReponse()
    {
        $this->collCommonOffressRelatedByIdBlobXmlReponse = null; // important to set this to null since that means it is uninitialized
        $this->collCommonOffressRelatedByIdBlobXmlReponsePartial = null;

        return $this;
    }

    /**
     * reset is the collCommonOffressRelatedByIdBlobXmlReponse collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonOffressRelatedByIdBlobXmlReponse($v = true)
    {
        $this->collCommonOffressRelatedByIdBlobXmlReponsePartial = $v;
    }

    /**
     * Initializes the collCommonOffressRelatedByIdBlobXmlReponse collection.
     *
     * By default this just sets the collCommonOffressRelatedByIdBlobXmlReponse collection to an empty array (like clearcollCommonOffressRelatedByIdBlobXmlReponse());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonOffressRelatedByIdBlobXmlReponse($overrideExisting = true)
    {
        if (null !== $this->collCommonOffressRelatedByIdBlobXmlReponse && !$overrideExisting) {
            return;
        }
        $this->collCommonOffressRelatedByIdBlobXmlReponse = new PropelObjectCollection();
        $this->collCommonOffressRelatedByIdBlobXmlReponse->setModel('CommonOffres');
    }

    /**
     * Gets an array of CommonOffres objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonBlobOrganismeFile is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     * @throws PropelException
     */
    public function getCommonOffressRelatedByIdBlobXmlReponse($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonOffressRelatedByIdBlobXmlReponsePartial && !$this->isNew();
        if (null === $this->collCommonOffressRelatedByIdBlobXmlReponse || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonOffressRelatedByIdBlobXmlReponse) {
                // return empty collection
                $this->initCommonOffressRelatedByIdBlobXmlReponse();
            } else {
                $collCommonOffressRelatedByIdBlobXmlReponse = CommonOffresQuery::create(null, $criteria)
                    ->filterByCommonBlobOrganismeFileRelatedByIdBlobXmlReponse($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonOffressRelatedByIdBlobXmlReponsePartial && count($collCommonOffressRelatedByIdBlobXmlReponse)) {
                      $this->initCommonOffressRelatedByIdBlobXmlReponse(false);

                      foreach ($collCommonOffressRelatedByIdBlobXmlReponse as $obj) {
                        if (false == $this->collCommonOffressRelatedByIdBlobXmlReponse->contains($obj)) {
                          $this->collCommonOffressRelatedByIdBlobXmlReponse->append($obj);
                        }
                      }

                      $this->collCommonOffressRelatedByIdBlobXmlReponsePartial = true;
                    }

                    $collCommonOffressRelatedByIdBlobXmlReponse->getInternalIterator()->rewind();

                    return $collCommonOffressRelatedByIdBlobXmlReponse;
                }

                if ($partial && $this->collCommonOffressRelatedByIdBlobXmlReponse) {
                    foreach ($this->collCommonOffressRelatedByIdBlobXmlReponse as $obj) {
                        if ($obj->isNew()) {
                            $collCommonOffressRelatedByIdBlobXmlReponse[] = $obj;
                        }
                    }
                }

                $this->collCommonOffressRelatedByIdBlobXmlReponse = $collCommonOffressRelatedByIdBlobXmlReponse;
                $this->collCommonOffressRelatedByIdBlobXmlReponsePartial = false;
            }
        }

        return $this->collCommonOffressRelatedByIdBlobXmlReponse;
    }

    /**
     * Sets a collection of CommonOffresRelatedByIdBlobXmlReponse objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonOffressRelatedByIdBlobXmlReponse A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setCommonOffressRelatedByIdBlobXmlReponse(PropelCollection $commonOffressRelatedByIdBlobXmlReponse, PropelPDO $con = null)
    {
        $commonOffressRelatedByIdBlobXmlReponseToDelete = $this->getCommonOffressRelatedByIdBlobXmlReponse(new Criteria(), $con)->diff($commonOffressRelatedByIdBlobXmlReponse);


        $this->commonOffressRelatedByIdBlobXmlReponseScheduledForDeletion = $commonOffressRelatedByIdBlobXmlReponseToDelete;

        foreach ($commonOffressRelatedByIdBlobXmlReponseToDelete as $commonOffresRelatedByIdBlobXmlReponseRemoved) {
            $commonOffresRelatedByIdBlobXmlReponseRemoved->setCommonBlobOrganismeFileRelatedByIdBlobXmlReponse(null);
        }

        $this->collCommonOffressRelatedByIdBlobXmlReponse = null;
        foreach ($commonOffressRelatedByIdBlobXmlReponse as $commonOffresRelatedByIdBlobXmlReponse) {
            $this->addCommonOffresRelatedByIdBlobXmlReponse($commonOffresRelatedByIdBlobXmlReponse);
        }

        $this->collCommonOffressRelatedByIdBlobXmlReponse = $commonOffressRelatedByIdBlobXmlReponse;
        $this->collCommonOffressRelatedByIdBlobXmlReponsePartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonOffres objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonOffres objects.
     * @throws PropelException
     */
    public function countCommonOffressRelatedByIdBlobXmlReponse(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonOffressRelatedByIdBlobXmlReponsePartial && !$this->isNew();
        if (null === $this->collCommonOffressRelatedByIdBlobXmlReponse || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonOffressRelatedByIdBlobXmlReponse) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonOffressRelatedByIdBlobXmlReponse());
            }
            $query = CommonOffresQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonBlobOrganismeFileRelatedByIdBlobXmlReponse($this)
                ->count($con);
        }

        return count($this->collCommonOffressRelatedByIdBlobXmlReponse);
    }

    /**
     * Method called to associate a CommonOffres object to this object
     * through the CommonOffres foreign key attribute.
     *
     * @param   CommonOffres $l CommonOffres
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function addCommonOffresRelatedByIdBlobXmlReponse(CommonOffres $l)
    {
        if ($this->collCommonOffressRelatedByIdBlobXmlReponse === null) {
            $this->initCommonOffressRelatedByIdBlobXmlReponse();
            $this->collCommonOffressRelatedByIdBlobXmlReponsePartial = true;
        }
        if (!in_array($l, $this->collCommonOffressRelatedByIdBlobXmlReponse->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonOffresRelatedByIdBlobXmlReponse($l);
        }

        return $this;
    }

    /**
     * @param	CommonOffresRelatedByIdBlobXmlReponse $commonOffresRelatedByIdBlobXmlReponse The commonOffresRelatedByIdBlobXmlReponse object to add.
     */
    protected function doAddCommonOffresRelatedByIdBlobXmlReponse($commonOffresRelatedByIdBlobXmlReponse)
    {
        $this->collCommonOffressRelatedByIdBlobXmlReponse[]= $commonOffresRelatedByIdBlobXmlReponse;
        $commonOffresRelatedByIdBlobXmlReponse->setCommonBlobOrganismeFileRelatedByIdBlobXmlReponse($this);
    }

    /**
     * @param	CommonOffresRelatedByIdBlobXmlReponse $commonOffresRelatedByIdBlobXmlReponse The commonOffresRelatedByIdBlobXmlReponse object to remove.
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function removeCommonOffresRelatedByIdBlobXmlReponse($commonOffresRelatedByIdBlobXmlReponse)
    {
        if ($this->getCommonOffressRelatedByIdBlobXmlReponse()->contains($commonOffresRelatedByIdBlobXmlReponse)) {
            $this->collCommonOffressRelatedByIdBlobXmlReponse->remove($this->collCommonOffressRelatedByIdBlobXmlReponse->search($commonOffresRelatedByIdBlobXmlReponse));
            if (null === $this->commonOffressRelatedByIdBlobXmlReponseScheduledForDeletion) {
                $this->commonOffressRelatedByIdBlobXmlReponseScheduledForDeletion = clone $this->collCommonOffressRelatedByIdBlobXmlReponse;
                $this->commonOffressRelatedByIdBlobXmlReponseScheduledForDeletion->clear();
            }
            $this->commonOffressRelatedByIdBlobXmlReponseScheduledForDeletion[]= $commonOffresRelatedByIdBlobXmlReponse;
            $commonOffresRelatedByIdBlobXmlReponse->setCommonBlobOrganismeFileRelatedByIdBlobXmlReponse(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonOffressRelatedByIdBlobXmlReponse from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressRelatedByIdBlobXmlReponseJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonOffressRelatedByIdBlobXmlReponse($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonOffressRelatedByIdBlobXmlReponse from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressRelatedByIdBlobXmlReponseJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonOffressRelatedByIdBlobXmlReponse($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonOffressRelatedByIdBlobXmlReponse from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressRelatedByIdBlobXmlReponseJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonOffressRelatedByIdBlobXmlReponse($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonOffressRelatedByIdBlobXmlReponse from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonOffres[] List of CommonOffres objects
     */
    public function getCommonOffressRelatedByIdBlobXmlReponseJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonOffresQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonOffressRelatedByIdBlobXmlReponse($query, $con);
    }

    /**
     * Clears out the collCommonAnnexeFinancieres collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     * @see        addCommonAnnexeFinancieres()
     */
    public function clearCommonAnnexeFinancieres()
    {
        $this->collCommonAnnexeFinancieres = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAnnexeFinancieresPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAnnexeFinancieres collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAnnexeFinancieres($v = true)
    {
        $this->collCommonAnnexeFinancieresPartial = $v;
    }

    /**
     * Initializes the collCommonAnnexeFinancieres collection.
     *
     * By default this just sets the collCommonAnnexeFinancieres collection to an empty array (like clearcollCommonAnnexeFinancieres());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAnnexeFinancieres($overrideExisting = true)
    {
        if (null !== $this->collCommonAnnexeFinancieres && !$overrideExisting) {
            return;
        }
        $this->collCommonAnnexeFinancieres = new PropelObjectCollection();
        $this->collCommonAnnexeFinancieres->setModel('CommonAnnexeFinanciere');
    }

    /**
     * Gets an array of CommonAnnexeFinanciere objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonBlobOrganismeFile is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAnnexeFinanciere[] List of CommonAnnexeFinanciere objects
     * @throws PropelException
     */
    public function getCommonAnnexeFinancieres($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAnnexeFinancieresPartial && !$this->isNew();
        if (null === $this->collCommonAnnexeFinancieres || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAnnexeFinancieres) {
                // return empty collection
                $this->initCommonAnnexeFinancieres();
            } else {
                $collCommonAnnexeFinancieres = CommonAnnexeFinanciereQuery::create(null, $criteria)
                    ->filterByCommonBlobOrganismeFile($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAnnexeFinancieresPartial && count($collCommonAnnexeFinancieres)) {
                      $this->initCommonAnnexeFinancieres(false);

                      foreach ($collCommonAnnexeFinancieres as $obj) {
                        if (false == $this->collCommonAnnexeFinancieres->contains($obj)) {
                          $this->collCommonAnnexeFinancieres->append($obj);
                        }
                      }

                      $this->collCommonAnnexeFinancieresPartial = true;
                    }

                    $collCommonAnnexeFinancieres->getInternalIterator()->rewind();

                    return $collCommonAnnexeFinancieres;
                }

                if ($partial && $this->collCommonAnnexeFinancieres) {
                    foreach ($this->collCommonAnnexeFinancieres as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAnnexeFinancieres[] = $obj;
                        }
                    }
                }

                $this->collCommonAnnexeFinancieres = $collCommonAnnexeFinancieres;
                $this->collCommonAnnexeFinancieresPartial = false;
            }
        }

        return $this->collCommonAnnexeFinancieres;
    }

    /**
     * Sets a collection of CommonAnnexeFinanciere objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAnnexeFinancieres A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setCommonAnnexeFinancieres(PropelCollection $commonAnnexeFinancieres, PropelPDO $con = null)
    {
        $commonAnnexeFinancieresToDelete = $this->getCommonAnnexeFinancieres(new Criteria(), $con)->diff($commonAnnexeFinancieres);


        $this->commonAnnexeFinancieresScheduledForDeletion = $commonAnnexeFinancieresToDelete;

        foreach ($commonAnnexeFinancieresToDelete as $commonAnnexeFinanciereRemoved) {
            $commonAnnexeFinanciereRemoved->setCommonBlobOrganismeFile(null);
        }

        $this->collCommonAnnexeFinancieres = null;
        foreach ($commonAnnexeFinancieres as $commonAnnexeFinanciere) {
            $this->addCommonAnnexeFinanciere($commonAnnexeFinanciere);
        }

        $this->collCommonAnnexeFinancieres = $commonAnnexeFinancieres;
        $this->collCommonAnnexeFinancieresPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAnnexeFinanciere objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAnnexeFinanciere objects.
     * @throws PropelException
     */
    public function countCommonAnnexeFinancieres(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAnnexeFinancieresPartial && !$this->isNew();
        if (null === $this->collCommonAnnexeFinancieres || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAnnexeFinancieres) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAnnexeFinancieres());
            }
            $query = CommonAnnexeFinanciereQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonBlobOrganismeFile($this)
                ->count($con);
        }

        return count($this->collCommonAnnexeFinancieres);
    }

    /**
     * Method called to associate a CommonAnnexeFinanciere object to this object
     * through the CommonAnnexeFinanciere foreign key attribute.
     *
     * @param   CommonAnnexeFinanciere $l CommonAnnexeFinanciere
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function addCommonAnnexeFinanciere(CommonAnnexeFinanciere $l)
    {
        if ($this->collCommonAnnexeFinancieres === null) {
            $this->initCommonAnnexeFinancieres();
            $this->collCommonAnnexeFinancieresPartial = true;
        }
        if (!in_array($l, $this->collCommonAnnexeFinancieres->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAnnexeFinanciere($l);
        }

        return $this;
    }

    /**
     * @param	CommonAnnexeFinanciere $commonAnnexeFinanciere The commonAnnexeFinanciere object to add.
     */
    protected function doAddCommonAnnexeFinanciere($commonAnnexeFinanciere)
    {
        $this->collCommonAnnexeFinancieres[]= $commonAnnexeFinanciere;
        $commonAnnexeFinanciere->setCommonBlobOrganismeFile($this);
    }

    /**
     * @param	CommonAnnexeFinanciere $commonAnnexeFinanciere The commonAnnexeFinanciere object to remove.
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function removeCommonAnnexeFinanciere($commonAnnexeFinanciere)
    {
        if ($this->getCommonAnnexeFinancieres()->contains($commonAnnexeFinanciere)) {
            $this->collCommonAnnexeFinancieres->remove($this->collCommonAnnexeFinancieres->search($commonAnnexeFinanciere));
            if (null === $this->commonAnnexeFinancieresScheduledForDeletion) {
                $this->commonAnnexeFinancieresScheduledForDeletion = clone $this->collCommonAnnexeFinancieres;
                $this->commonAnnexeFinancieresScheduledForDeletion->clear();
            }
            $this->commonAnnexeFinancieresScheduledForDeletion[]= $commonAnnexeFinanciere;
            $commonAnnexeFinanciere->setCommonBlobOrganismeFile(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonAnnexeFinancieres from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAnnexeFinanciere[] List of CommonAnnexeFinanciere objects
     */
    public function getCommonAnnexeFinancieresJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAnnexeFinanciereQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonAnnexeFinancieres($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonAnnexeFinancieres from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAnnexeFinanciere[] List of CommonAnnexeFinanciere objects
     */
    public function getCommonAnnexeFinancieresJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAnnexeFinanciereQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonAnnexeFinancieres($query, $con);
    }

    /**
     * Clears out the collCommonAutrePieceConsultations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     * @see        addCommonAutrePieceConsultations()
     */
    public function clearCommonAutrePieceConsultations()
    {
        $this->collCommonAutrePieceConsultations = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAutrePieceConsultationsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAutrePieceConsultations collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAutrePieceConsultations($v = true)
    {
        $this->collCommonAutrePieceConsultationsPartial = $v;
    }

    /**
     * Initializes the collCommonAutrePieceConsultations collection.
     *
     * By default this just sets the collCommonAutrePieceConsultations collection to an empty array (like clearcollCommonAutrePieceConsultations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAutrePieceConsultations($overrideExisting = true)
    {
        if (null !== $this->collCommonAutrePieceConsultations && !$overrideExisting) {
            return;
        }
        $this->collCommonAutrePieceConsultations = new PropelObjectCollection();
        $this->collCommonAutrePieceConsultations->setModel('CommonAutrePieceConsultation');
    }

    /**
     * Gets an array of CommonAutrePieceConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonBlobOrganismeFile is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAutrePieceConsultation[] List of CommonAutrePieceConsultation objects
     * @throws PropelException
     */
    public function getCommonAutrePieceConsultations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAutrePieceConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonAutrePieceConsultations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAutrePieceConsultations) {
                // return empty collection
                $this->initCommonAutrePieceConsultations();
            } else {
                $collCommonAutrePieceConsultations = CommonAutrePieceConsultationQuery::create(null, $criteria)
                    ->filterByCommonBlobOrganismeFile($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAutrePieceConsultationsPartial && count($collCommonAutrePieceConsultations)) {
                      $this->initCommonAutrePieceConsultations(false);

                      foreach ($collCommonAutrePieceConsultations as $obj) {
                        if (false == $this->collCommonAutrePieceConsultations->contains($obj)) {
                          $this->collCommonAutrePieceConsultations->append($obj);
                        }
                      }

                      $this->collCommonAutrePieceConsultationsPartial = true;
                    }

                    $collCommonAutrePieceConsultations->getInternalIterator()->rewind();

                    return $collCommonAutrePieceConsultations;
                }

                if ($partial && $this->collCommonAutrePieceConsultations) {
                    foreach ($this->collCommonAutrePieceConsultations as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAutrePieceConsultations[] = $obj;
                        }
                    }
                }

                $this->collCommonAutrePieceConsultations = $collCommonAutrePieceConsultations;
                $this->collCommonAutrePieceConsultationsPartial = false;
            }
        }

        return $this->collCommonAutrePieceConsultations;
    }

    /**
     * Sets a collection of CommonAutrePieceConsultation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAutrePieceConsultations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setCommonAutrePieceConsultations(PropelCollection $commonAutrePieceConsultations, PropelPDO $con = null)
    {
        $commonAutrePieceConsultationsToDelete = $this->getCommonAutrePieceConsultations(new Criteria(), $con)->diff($commonAutrePieceConsultations);


        $this->commonAutrePieceConsultationsScheduledForDeletion = $commonAutrePieceConsultationsToDelete;

        foreach ($commonAutrePieceConsultationsToDelete as $commonAutrePieceConsultationRemoved) {
            $commonAutrePieceConsultationRemoved->setCommonBlobOrganismeFile(null);
        }

        $this->collCommonAutrePieceConsultations = null;
        foreach ($commonAutrePieceConsultations as $commonAutrePieceConsultation) {
            $this->addCommonAutrePieceConsultation($commonAutrePieceConsultation);
        }

        $this->collCommonAutrePieceConsultations = $commonAutrePieceConsultations;
        $this->collCommonAutrePieceConsultationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAutrePieceConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAutrePieceConsultation objects.
     * @throws PropelException
     */
    public function countCommonAutrePieceConsultations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAutrePieceConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonAutrePieceConsultations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAutrePieceConsultations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAutrePieceConsultations());
            }
            $query = CommonAutrePieceConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonBlobOrganismeFile($this)
                ->count($con);
        }

        return count($this->collCommonAutrePieceConsultations);
    }

    /**
     * Method called to associate a CommonAutrePieceConsultation object to this object
     * through the CommonAutrePieceConsultation foreign key attribute.
     *
     * @param   CommonAutrePieceConsultation $l CommonAutrePieceConsultation
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function addCommonAutrePieceConsultation(CommonAutrePieceConsultation $l)
    {
        if ($this->collCommonAutrePieceConsultations === null) {
            $this->initCommonAutrePieceConsultations();
            $this->collCommonAutrePieceConsultationsPartial = true;
        }
        if (!in_array($l, $this->collCommonAutrePieceConsultations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAutrePieceConsultation($l);
        }

        return $this;
    }

    /**
     * @param	CommonAutrePieceConsultation $commonAutrePieceConsultation The commonAutrePieceConsultation object to add.
     */
    protected function doAddCommonAutrePieceConsultation($commonAutrePieceConsultation)
    {
        $this->collCommonAutrePieceConsultations[]= $commonAutrePieceConsultation;
        $commonAutrePieceConsultation->setCommonBlobOrganismeFile($this);
    }

    /**
     * @param	CommonAutrePieceConsultation $commonAutrePieceConsultation The commonAutrePieceConsultation object to remove.
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function removeCommonAutrePieceConsultation($commonAutrePieceConsultation)
    {
        if ($this->getCommonAutrePieceConsultations()->contains($commonAutrePieceConsultation)) {
            $this->collCommonAutrePieceConsultations->remove($this->collCommonAutrePieceConsultations->search($commonAutrePieceConsultation));
            if (null === $this->commonAutrePieceConsultationsScheduledForDeletion) {
                $this->commonAutrePieceConsultationsScheduledForDeletion = clone $this->collCommonAutrePieceConsultations;
                $this->commonAutrePieceConsultationsScheduledForDeletion->clear();
            }
            $this->commonAutrePieceConsultationsScheduledForDeletion[]= clone $commonAutrePieceConsultation;
            $commonAutrePieceConsultation->setCommonBlobOrganismeFile(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonAutrePieceConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAutrePieceConsultation[] List of CommonAutrePieceConsultation objects
     */
    public function getCommonAutrePieceConsultationsJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAutrePieceConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonAutrePieceConsultations($query, $con);
    }

    /**
     * Clears out the collCommonEchangeDocBlobs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     * @see        addCommonEchangeDocBlobs()
     */
    public function clearCommonEchangeDocBlobs()
    {
        $this->collCommonEchangeDocBlobs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangeDocBlobsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchangeDocBlobs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchangeDocBlobs($v = true)
    {
        $this->collCommonEchangeDocBlobsPartial = $v;
    }

    /**
     * Initializes the collCommonEchangeDocBlobs collection.
     *
     * By default this just sets the collCommonEchangeDocBlobs collection to an empty array (like clearcollCommonEchangeDocBlobs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchangeDocBlobs($overrideExisting = true)
    {
        if (null !== $this->collCommonEchangeDocBlobs && !$overrideExisting) {
            return;
        }
        $this->collCommonEchangeDocBlobs = new PropelObjectCollection();
        $this->collCommonEchangeDocBlobs->setModel('CommonEchangeDocBlob');
    }

    /**
     * Gets an array of CommonEchangeDocBlob objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonBlobOrganismeFile is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     * @throws PropelException
     */
    public function getCommonEchangeDocBlobs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocBlobsPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocBlobs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocBlobs) {
                // return empty collection
                $this->initCommonEchangeDocBlobs();
            } else {
                $collCommonEchangeDocBlobs = CommonEchangeDocBlobQuery::create(null, $criteria)
                    ->filterByCommonBlobOrganismeFile($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangeDocBlobsPartial && count($collCommonEchangeDocBlobs)) {
                      $this->initCommonEchangeDocBlobs(false);

                      foreach ($collCommonEchangeDocBlobs as $obj) {
                        if (false == $this->collCommonEchangeDocBlobs->contains($obj)) {
                          $this->collCommonEchangeDocBlobs->append($obj);
                        }
                      }

                      $this->collCommonEchangeDocBlobsPartial = true;
                    }

                    $collCommonEchangeDocBlobs->getInternalIterator()->rewind();

                    return $collCommonEchangeDocBlobs;
                }

                if ($partial && $this->collCommonEchangeDocBlobs) {
                    foreach ($this->collCommonEchangeDocBlobs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchangeDocBlobs[] = $obj;
                        }
                    }
                }

                $this->collCommonEchangeDocBlobs = $collCommonEchangeDocBlobs;
                $this->collCommonEchangeDocBlobsPartial = false;
            }
        }

        return $this->collCommonEchangeDocBlobs;
    }

    /**
     * Sets a collection of CommonEchangeDocBlob objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchangeDocBlobs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setCommonEchangeDocBlobs(PropelCollection $commonEchangeDocBlobs, PropelPDO $con = null)
    {
        $commonEchangeDocBlobsToDelete = $this->getCommonEchangeDocBlobs(new Criteria(), $con)->diff($commonEchangeDocBlobs);


        $this->commonEchangeDocBlobsScheduledForDeletion = $commonEchangeDocBlobsToDelete;

        foreach ($commonEchangeDocBlobsToDelete as $commonEchangeDocBlobRemoved) {
            $commonEchangeDocBlobRemoved->setCommonBlobOrganismeFile(null);
        }

        $this->collCommonEchangeDocBlobs = null;
        foreach ($commonEchangeDocBlobs as $commonEchangeDocBlob) {
            $this->addCommonEchangeDocBlob($commonEchangeDocBlob);
        }

        $this->collCommonEchangeDocBlobs = $commonEchangeDocBlobs;
        $this->collCommonEchangeDocBlobsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchangeDocBlob objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchangeDocBlob objects.
     * @throws PropelException
     */
    public function countCommonEchangeDocBlobs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocBlobsPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocBlobs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocBlobs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchangeDocBlobs());
            }
            $query = CommonEchangeDocBlobQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonBlobOrganismeFile($this)
                ->count($con);
        }

        return count($this->collCommonEchangeDocBlobs);
    }

    /**
     * Method called to associate a CommonEchangeDocBlob object to this object
     * through the CommonEchangeDocBlob foreign key attribute.
     *
     * @param   CommonEchangeDocBlob $l CommonEchangeDocBlob
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function addCommonEchangeDocBlob(CommonEchangeDocBlob $l)
    {
        if ($this->collCommonEchangeDocBlobs === null) {
            $this->initCommonEchangeDocBlobs();
            $this->collCommonEchangeDocBlobsPartial = true;
        }
        if (!in_array($l, $this->collCommonEchangeDocBlobs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchangeDocBlob($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchangeDocBlob $commonEchangeDocBlob The commonEchangeDocBlob object to add.
     */
    protected function doAddCommonEchangeDocBlob($commonEchangeDocBlob)
    {
        $this->collCommonEchangeDocBlobs[]= $commonEchangeDocBlob;
        $commonEchangeDocBlob->setCommonBlobOrganismeFile($this);
    }

    /**
     * @param	CommonEchangeDocBlob $commonEchangeDocBlob The commonEchangeDocBlob object to remove.
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function removeCommonEchangeDocBlob($commonEchangeDocBlob)
    {
        if ($this->getCommonEchangeDocBlobs()->contains($commonEchangeDocBlob)) {
            $this->collCommonEchangeDocBlobs->remove($this->collCommonEchangeDocBlobs->search($commonEchangeDocBlob));
            if (null === $this->commonEchangeDocBlobsScheduledForDeletion) {
                $this->commonEchangeDocBlobsScheduledForDeletion = clone $this->collCommonEchangeDocBlobs;
                $this->commonEchangeDocBlobsScheduledForDeletion->clear();
            }
            $this->commonEchangeDocBlobsScheduledForDeletion[]= $commonEchangeDocBlob;
            $commonEchangeDocBlob->setCommonBlobOrganismeFile(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonEchangeDocBlobs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     */
    public function getCommonEchangeDocBlobsJoinCommonEchangeDoc($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocBlobQuery::create(null, $criteria);
        $query->joinWith('CommonEchangeDoc', $join_behavior);

        return $this->getCommonEchangeDocBlobs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonEchangeDocBlobs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     */
    public function getCommonEchangeDocBlobsJoinCommonEchangeDocBlobRelatedByDocBlobPrincipalId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocBlobQuery::create(null, $criteria);
        $query->joinWith('CommonEchangeDocBlobRelatedByDocBlobPrincipalId', $join_behavior);

        return $this->getCommonEchangeDocBlobs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonEchangeDocBlobs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocBlob[] List of CommonEchangeDocBlob objects
     */
    public function getCommonEchangeDocBlobsJoinCommonEchangeDocTypePieceStandard($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocBlobQuery::create(null, $criteria);
        $query->joinWith('CommonEchangeDocTypePieceStandard', $join_behavior);

        return $this->getCommonEchangeDocBlobs($query, $con);
    }

    /**
     * Clears out the collCommonPieceGenereConsultations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     * @see        addCommonPieceGenereConsultations()
     */
    public function clearCommonPieceGenereConsultations()
    {
        $this->collCommonPieceGenereConsultations = null; // important to set this to null since that means it is uninitialized
        $this->collCommonPieceGenereConsultationsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonPieceGenereConsultations collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonPieceGenereConsultations($v = true)
    {
        $this->collCommonPieceGenereConsultationsPartial = $v;
    }

    /**
     * Initializes the collCommonPieceGenereConsultations collection.
     *
     * By default this just sets the collCommonPieceGenereConsultations collection to an empty array (like clearcollCommonPieceGenereConsultations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonPieceGenereConsultations($overrideExisting = true)
    {
        if (null !== $this->collCommonPieceGenereConsultations && !$overrideExisting) {
            return;
        }
        $this->collCommonPieceGenereConsultations = new PropelObjectCollection();
        $this->collCommonPieceGenereConsultations->setModel('CommonPieceGenereConsultation');
    }

    /**
     * Gets an array of CommonPieceGenereConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonBlobOrganismeFile is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonPieceGenereConsultation[] List of CommonPieceGenereConsultation objects
     * @throws PropelException
     */
    public function getCommonPieceGenereConsultations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonPieceGenereConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonPieceGenereConsultations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonPieceGenereConsultations) {
                // return empty collection
                $this->initCommonPieceGenereConsultations();
            } else {
                $collCommonPieceGenereConsultations = CommonPieceGenereConsultationQuery::create(null, $criteria)
                    ->filterByCommonBlobOrganismeFile($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonPieceGenereConsultationsPartial && count($collCommonPieceGenereConsultations)) {
                      $this->initCommonPieceGenereConsultations(false);

                      foreach ($collCommonPieceGenereConsultations as $obj) {
                        if (false == $this->collCommonPieceGenereConsultations->contains($obj)) {
                          $this->collCommonPieceGenereConsultations->append($obj);
                        }
                      }

                      $this->collCommonPieceGenereConsultationsPartial = true;
                    }

                    $collCommonPieceGenereConsultations->getInternalIterator()->rewind();

                    return $collCommonPieceGenereConsultations;
                }

                if ($partial && $this->collCommonPieceGenereConsultations) {
                    foreach ($this->collCommonPieceGenereConsultations as $obj) {
                        if ($obj->isNew()) {
                            $collCommonPieceGenereConsultations[] = $obj;
                        }
                    }
                }

                $this->collCommonPieceGenereConsultations = $collCommonPieceGenereConsultations;
                $this->collCommonPieceGenereConsultationsPartial = false;
            }
        }

        return $this->collCommonPieceGenereConsultations;
    }

    /**
     * Sets a collection of CommonPieceGenereConsultation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonPieceGenereConsultations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function setCommonPieceGenereConsultations(PropelCollection $commonPieceGenereConsultations, PropelPDO $con = null)
    {
        $commonPieceGenereConsultationsToDelete = $this->getCommonPieceGenereConsultations(new Criteria(), $con)->diff($commonPieceGenereConsultations);


        $this->commonPieceGenereConsultationsScheduledForDeletion = $commonPieceGenereConsultationsToDelete;

        foreach ($commonPieceGenereConsultationsToDelete as $commonPieceGenereConsultationRemoved) {
            $commonPieceGenereConsultationRemoved->setCommonBlobOrganismeFile(null);
        }

        $this->collCommonPieceGenereConsultations = null;
        foreach ($commonPieceGenereConsultations as $commonPieceGenereConsultation) {
            $this->addCommonPieceGenereConsultation($commonPieceGenereConsultation);
        }

        $this->collCommonPieceGenereConsultations = $commonPieceGenereConsultations;
        $this->collCommonPieceGenereConsultationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonPieceGenereConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonPieceGenereConsultation objects.
     * @throws PropelException
     */
    public function countCommonPieceGenereConsultations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonPieceGenereConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonPieceGenereConsultations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonPieceGenereConsultations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonPieceGenereConsultations());
            }
            $query = CommonPieceGenereConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonBlobOrganismeFile($this)
                ->count($con);
        }

        return count($this->collCommonPieceGenereConsultations);
    }

    /**
     * Method called to associate a CommonPieceGenereConsultation object to this object
     * through the CommonPieceGenereConsultation foreign key attribute.
     *
     * @param   CommonPieceGenereConsultation $l CommonPieceGenereConsultation
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function addCommonPieceGenereConsultation(CommonPieceGenereConsultation $l)
    {
        if ($this->collCommonPieceGenereConsultations === null) {
            $this->initCommonPieceGenereConsultations();
            $this->collCommonPieceGenereConsultationsPartial = true;
        }
        if (!in_array($l, $this->collCommonPieceGenereConsultations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonPieceGenereConsultation($l);
        }

        return $this;
    }

    /**
     * @param	CommonPieceGenereConsultation $commonPieceGenereConsultation The commonPieceGenereConsultation object to add.
     */
    protected function doAddCommonPieceGenereConsultation($commonPieceGenereConsultation)
    {
        $this->collCommonPieceGenereConsultations[]= $commonPieceGenereConsultation;
        $commonPieceGenereConsultation->setCommonBlobOrganismeFile($this);
    }

    /**
     * @param	CommonPieceGenereConsultation $commonPieceGenereConsultation The commonPieceGenereConsultation object to remove.
     * @return CommonBlobOrganismeFile The current object (for fluent API support)
     */
    public function removeCommonPieceGenereConsultation($commonPieceGenereConsultation)
    {
        if ($this->getCommonPieceGenereConsultations()->contains($commonPieceGenereConsultation)) {
            $this->collCommonPieceGenereConsultations->remove($this->collCommonPieceGenereConsultations->search($commonPieceGenereConsultation));
            if (null === $this->commonPieceGenereConsultationsScheduledForDeletion) {
                $this->commonPieceGenereConsultationsScheduledForDeletion = clone $this->collCommonPieceGenereConsultations;
                $this->commonPieceGenereConsultationsScheduledForDeletion->clear();
            }
            $this->commonPieceGenereConsultationsScheduledForDeletion[]= clone $commonPieceGenereConsultation;
            $commonPieceGenereConsultation->setCommonBlobOrganismeFile(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonPieceGenereConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonPieceGenereConsultation[] List of CommonPieceGenereConsultation objects
     */
    public function getCommonPieceGenereConsultationsJoinCommonCategorieLot($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonPieceGenereConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonCategorieLot', $join_behavior);

        return $this->getCommonPieceGenereConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonPieceGenereConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonPieceGenereConsultation[] List of CommonPieceGenereConsultation objects
     */
    public function getCommonPieceGenereConsultationsJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonPieceGenereConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonPieceGenereConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobOrganismeFile is new, it will return
     * an empty collection; or if this CommonBlobOrganismeFile has previously
     * been saved, it will retrieve related CommonPieceGenereConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobOrganismeFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonPieceGenereConsultation[] List of CommonPieceGenereConsultation objects
     */
    public function getCommonPieceGenereConsultationsJoinEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonPieceGenereConsultationQuery::create(null, $criteria);
        $query->joinWith('Entreprise', $join_behavior);

        return $this->getCommonPieceGenereConsultations($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->old_id = null;
        $this->organisme = null;
        $this->name = null;
        $this->deletion_datetime = null;
        $this->chemin = null;
        $this->dossier = null;
        $this->statut_synchro = null;
        $this->hash = null;
        $this->id = null;
        $this->extension = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonDocumentExternes) {
                foreach ($this->collCommonDocumentExternes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonOffressRelatedByIdBlobHorodatageHash) {
                foreach ($this->collCommonOffressRelatedByIdBlobHorodatageHash as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonOffressRelatedByIdBlobXmlReponse) {
                foreach ($this->collCommonOffressRelatedByIdBlobXmlReponse as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonAnnexeFinancieres) {
                foreach ($this->collCommonAnnexeFinancieres as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonAutrePieceConsultations) {
                foreach ($this->collCommonAutrePieceConsultations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonEchangeDocBlobs) {
                foreach ($this->collCommonEchangeDocBlobs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonPieceGenereConsultations) {
                foreach ($this->collCommonPieceGenereConsultations as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonDocumentExternes instanceof PropelCollection) {
            $this->collCommonDocumentExternes->clearIterator();
        }
        $this->collCommonDocumentExternes = null;
        if ($this->collCommonOffressRelatedByIdBlobHorodatageHash instanceof PropelCollection) {
            $this->collCommonOffressRelatedByIdBlobHorodatageHash->clearIterator();
        }
        $this->collCommonOffressRelatedByIdBlobHorodatageHash = null;
        if ($this->collCommonOffressRelatedByIdBlobXmlReponse instanceof PropelCollection) {
            $this->collCommonOffressRelatedByIdBlobXmlReponse->clearIterator();
        }
        $this->collCommonOffressRelatedByIdBlobXmlReponse = null;
        if ($this->collCommonAnnexeFinancieres instanceof PropelCollection) {
            $this->collCommonAnnexeFinancieres->clearIterator();
        }
        $this->collCommonAnnexeFinancieres = null;
        if ($this->collCommonAutrePieceConsultations instanceof PropelCollection) {
            $this->collCommonAutrePieceConsultations->clearIterator();
        }
        $this->collCommonAutrePieceConsultations = null;
        if ($this->collCommonEchangeDocBlobs instanceof PropelCollection) {
            $this->collCommonEchangeDocBlobs->clearIterator();
        }
        $this->collCommonEchangeDocBlobs = null;
        if ($this->collCommonPieceGenereConsultations instanceof PropelCollection) {
            $this->collCommonPieceGenereConsultations->clearIterator();
        }
        $this->collCommonPieceGenereConsultations = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonBlobOrganismeFilePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
