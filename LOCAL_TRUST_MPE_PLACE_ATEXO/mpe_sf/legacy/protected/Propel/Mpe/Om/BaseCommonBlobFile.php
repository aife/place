<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonBlobFile;
use Application\Propel\Mpe\CommonBlobFilePeer;
use Application\Propel\Mpe\CommonBlobFileQuery;
use Application\Propel\Mpe\CommonDossierVolumineux;
use Application\Propel\Mpe\CommonDossierVolumineuxQuery;

/**
 * Base class that represents a row from the 'blob_file' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonBlobFile extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonBlobFilePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonBlobFilePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the old_id field.
     * @var        int
     */
    protected $old_id;

    /**
     * The value for the name field.
     * @var        string
     */
    protected $name;

    /**
     * The value for the deletion_datetime field.
     * @var        string
     */
    protected $deletion_datetime;

    /**
     * The value for the chemin field.
     * @var        string
     */
    protected $chemin;

    /**
     * The value for the dossier field.
     * @var        string
     */
    protected $dossier;

    /**
     * The value for the statut_synchro field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $statut_synchro;

    /**
     * The value for the hash field.
     * Note: this column has a database default value of: 'ND'
     * @var        string
     */
    protected $hash;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the extension field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $extension;

    /**
     * @var        PropelObjectCollection|CommonDossierVolumineux[] Collection to store aggregation of CommonDossierVolumineux objects.
     */
    protected $collCommonDossierVolumineuxsRelatedByIdBlobDescripteur;
    protected $collCommonDossierVolumineuxsRelatedByIdBlobDescripteurPartial;

    /**
     * @var        PropelObjectCollection|CommonDossierVolumineux[] Collection to store aggregation of CommonDossierVolumineux objects.
     */
    protected $collCommonDossierVolumineuxsRelatedByIdBlobLogfile;
    protected $collCommonDossierVolumineuxsRelatedByIdBlobLogfilePartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonDossierVolumineuxsRelatedByIdBlobDescripteurScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonDossierVolumineuxsRelatedByIdBlobLogfileScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->statut_synchro = 0;
        $this->hash = 'ND';
        $this->extension = '';
    }

    /**
     * Initializes internal state of BaseCommonBlobFile object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [old_id] column value.
     *
     * @return int
     */
    public function getOldId()
    {

        return $this->old_id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * Get the [optionally formatted] temporal [deletion_datetime] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletionDatetime($format = 'Y-m-d H:i:s')
    {
        if ($this->deletion_datetime === null) {
            return null;
        }

        if ($this->deletion_datetime === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->deletion_datetime);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->deletion_datetime, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [chemin] column value.
     *
     * @return string
     */
    public function getChemin()
    {

        return $this->chemin;
    }

    /**
     * Get the [dossier] column value.
     *
     * @return string
     */
    public function getDossier()
    {

        return $this->dossier;
    }

    /**
     * Get the [statut_synchro] column value.
     *
     * @return int
     */
    public function getStatutSynchro()
    {

        return $this->statut_synchro;
    }

    /**
     * Get the [hash] column value.
     *
     * @return string
     */
    public function getHash()
    {

        return $this->hash;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [extension] column value.
     *
     * @return string
     */
    public function getExtension()
    {

        return $this->extension;
    }

    /**
     * Set the value of [old_id] column.
     *
     * @param int $v new value
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function setOldId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_id !== $v) {
            $this->old_id = $v;
            $this->modifiedColumns[] = CommonBlobFilePeer::OLD_ID;
        }


        return $this;
    } // setOldId()

    /**
     * Set the value of [name] column.
     *
     * @param string $v new value
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[] = CommonBlobFilePeer::NAME;
        }


        return $this;
    } // setName()

    /**
     * Sets the value of [deletion_datetime] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function setDeletionDatetime($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deletion_datetime !== null || $dt !== null) {
            $currentDateAsString = ($this->deletion_datetime !== null && $tmpDt = new DateTime($this->deletion_datetime)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->deletion_datetime = $newDateAsString;
                $this->modifiedColumns[] = CommonBlobFilePeer::DELETION_DATETIME;
            }
        } // if either are not null


        return $this;
    } // setDeletionDatetime()

    /**
     * Set the value of [chemin] column.
     *
     * @param string $v new value
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function setChemin($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin !== $v) {
            $this->chemin = $v;
            $this->modifiedColumns[] = CommonBlobFilePeer::CHEMIN;
        }


        return $this;
    } // setChemin()

    /**
     * Set the value of [dossier] column.
     *
     * @param string $v new value
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function setDossier($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->dossier !== $v) {
            $this->dossier = $v;
            $this->modifiedColumns[] = CommonBlobFilePeer::DOSSIER;
        }


        return $this;
    } // setDossier()

    /**
     * Set the value of [statut_synchro] column.
     *
     * @param int $v new value
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function setStatutSynchro($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->statut_synchro !== $v) {
            $this->statut_synchro = $v;
            $this->modifiedColumns[] = CommonBlobFilePeer::STATUT_SYNCHRO;
        }


        return $this;
    } // setStatutSynchro()

    /**
     * Set the value of [hash] column.
     *
     * @param string $v new value
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function setHash($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->hash !== $v) {
            $this->hash = $v;
            $this->modifiedColumns[] = CommonBlobFilePeer::HASH;
        }


        return $this;
    } // setHash()

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonBlobFilePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [extension] column.
     *
     * @param string $v new value
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function setExtension($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->extension !== $v) {
            $this->extension = $v;
            $this->modifiedColumns[] = CommonBlobFilePeer::EXTENSION;
        }


        return $this;
    } // setExtension()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->statut_synchro !== 0) {
                return false;
            }

            if ($this->hash !== 'ND') {
                return false;
            }

            if ($this->extension !== '') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->old_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->name = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->deletion_datetime = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->chemin = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->dossier = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->statut_synchro = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->hash = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->id = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->extension = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 9; // 9 = CommonBlobFilePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonBlobFile object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonBlobFilePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonBlobFilePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur = null;

            $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonBlobFilePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonBlobFileQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonBlobFilePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonBlobFilePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonDossierVolumineuxsRelatedByIdBlobDescripteurScheduledForDeletion !== null) {
                if (!$this->commonDossierVolumineuxsRelatedByIdBlobDescripteurScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonDossierVolumineuxsRelatedByIdBlobDescripteurScheduledForDeletion as $commonDossierVolumineuxRelatedByIdBlobDescripteur) {
                        // need to save related object because we set the relation to null
                        $commonDossierVolumineuxRelatedByIdBlobDescripteur->save($con);
                    }
                    $this->commonDossierVolumineuxsRelatedByIdBlobDescripteurScheduledForDeletion = null;
                }
            }

            if ($this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur !== null) {
                foreach ($this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonDossierVolumineuxsRelatedByIdBlobLogfileScheduledForDeletion !== null) {
                if (!$this->commonDossierVolumineuxsRelatedByIdBlobLogfileScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonDossierVolumineuxsRelatedByIdBlobLogfileScheduledForDeletion as $commonDossierVolumineuxRelatedByIdBlobLogfile) {
                        // need to save related object because we set the relation to null
                        $commonDossierVolumineuxRelatedByIdBlobLogfile->save($con);
                    }
                    $this->commonDossierVolumineuxsRelatedByIdBlobLogfileScheduledForDeletion = null;
                }
            }

            if ($this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile !== null) {
                foreach ($this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonBlobFilePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonBlobFilePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonBlobFilePeer::OLD_ID)) {
            $modifiedColumns[':p' . $index++]  = '`old_id`';
        }
        if ($this->isColumnModified(CommonBlobFilePeer::NAME)) {
            $modifiedColumns[':p' . $index++]  = '`name`';
        }
        if ($this->isColumnModified(CommonBlobFilePeer::DELETION_DATETIME)) {
            $modifiedColumns[':p' . $index++]  = '`deletion_datetime`';
        }
        if ($this->isColumnModified(CommonBlobFilePeer::CHEMIN)) {
            $modifiedColumns[':p' . $index++]  = '`chemin`';
        }
        if ($this->isColumnModified(CommonBlobFilePeer::DOSSIER)) {
            $modifiedColumns[':p' . $index++]  = '`dossier`';
        }
        if ($this->isColumnModified(CommonBlobFilePeer::STATUT_SYNCHRO)) {
            $modifiedColumns[':p' . $index++]  = '`statut_synchro`';
        }
        if ($this->isColumnModified(CommonBlobFilePeer::HASH)) {
            $modifiedColumns[':p' . $index++]  = '`hash`';
        }
        if ($this->isColumnModified(CommonBlobFilePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonBlobFilePeer::EXTENSION)) {
            $modifiedColumns[':p' . $index++]  = '`extension`';
        }

        $sql = sprintf(
            'INSERT INTO `blob_file` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`old_id`':
                        $stmt->bindValue($identifier, $this->old_id, PDO::PARAM_INT);
                        break;
                    case '`name`':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case '`deletion_datetime`':
                        $stmt->bindValue($identifier, $this->deletion_datetime, PDO::PARAM_STR);
                        break;
                    case '`chemin`':
                        $stmt->bindValue($identifier, $this->chemin, PDO::PARAM_STR);
                        break;
                    case '`dossier`':
                        $stmt->bindValue($identifier, $this->dossier, PDO::PARAM_STR);
                        break;
                    case '`statut_synchro`':
                        $stmt->bindValue($identifier, $this->statut_synchro, PDO::PARAM_INT);
                        break;
                    case '`hash`':
                        $stmt->bindValue($identifier, $this->hash, PDO::PARAM_STR);
                        break;
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`extension`':
                        $stmt->bindValue($identifier, $this->extension, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonBlobFilePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur !== null) {
                    foreach ($this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile !== null) {
                    foreach ($this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonBlobFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getOldId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getDeletionDatetime();
                break;
            case 3:
                return $this->getChemin();
                break;
            case 4:
                return $this->getDossier();
                break;
            case 5:
                return $this->getStatutSynchro();
                break;
            case 6:
                return $this->getHash();
                break;
            case 7:
                return $this->getId();
                break;
            case 8:
                return $this->getExtension();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonBlobFile'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonBlobFile'][$this->getPrimaryKey()] = true;
        $keys = CommonBlobFilePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getOldId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getDeletionDatetime(),
            $keys[3] => $this->getChemin(),
            $keys[4] => $this->getDossier(),
            $keys[5] => $this->getStatutSynchro(),
            $keys[6] => $this->getHash(),
            $keys[7] => $this->getId(),
            $keys[8] => $this->getExtension(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur) {
                $result['CommonDossierVolumineuxsRelatedByIdBlobDescripteur'] = $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile) {
                $result['CommonDossierVolumineuxsRelatedByIdBlobLogfile'] = $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonBlobFilePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setOldId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setDeletionDatetime($value);
                break;
            case 3:
                $this->setChemin($value);
                break;
            case 4:
                $this->setDossier($value);
                break;
            case 5:
                $this->setStatutSynchro($value);
                break;
            case 6:
                $this->setHash($value);
                break;
            case 7:
                $this->setId($value);
                break;
            case 8:
                $this->setExtension($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonBlobFilePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setOldId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setName($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setDeletionDatetime($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setChemin($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDossier($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setStatutSynchro($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setHash($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setId($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setExtension($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonBlobFilePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonBlobFilePeer::OLD_ID)) $criteria->add(CommonBlobFilePeer::OLD_ID, $this->old_id);
        if ($this->isColumnModified(CommonBlobFilePeer::NAME)) $criteria->add(CommonBlobFilePeer::NAME, $this->name);
        if ($this->isColumnModified(CommonBlobFilePeer::DELETION_DATETIME)) $criteria->add(CommonBlobFilePeer::DELETION_DATETIME, $this->deletion_datetime);
        if ($this->isColumnModified(CommonBlobFilePeer::CHEMIN)) $criteria->add(CommonBlobFilePeer::CHEMIN, $this->chemin);
        if ($this->isColumnModified(CommonBlobFilePeer::DOSSIER)) $criteria->add(CommonBlobFilePeer::DOSSIER, $this->dossier);
        if ($this->isColumnModified(CommonBlobFilePeer::STATUT_SYNCHRO)) $criteria->add(CommonBlobFilePeer::STATUT_SYNCHRO, $this->statut_synchro);
        if ($this->isColumnModified(CommonBlobFilePeer::HASH)) $criteria->add(CommonBlobFilePeer::HASH, $this->hash);
        if ($this->isColumnModified(CommonBlobFilePeer::ID)) $criteria->add(CommonBlobFilePeer::ID, $this->id);
        if ($this->isColumnModified(CommonBlobFilePeer::EXTENSION)) $criteria->add(CommonBlobFilePeer::EXTENSION, $this->extension);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonBlobFilePeer::DATABASE_NAME);
        $criteria->add(CommonBlobFilePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonBlobFile (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOldId($this->getOldId());
        $copyObj->setName($this->getName());
        $copyObj->setDeletionDatetime($this->getDeletionDatetime());
        $copyObj->setChemin($this->getChemin());
        $copyObj->setDossier($this->getDossier());
        $copyObj->setStatutSynchro($this->getStatutSynchro());
        $copyObj->setHash($this->getHash());
        $copyObj->setExtension($this->getExtension());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonDossierVolumineuxsRelatedByIdBlobDescripteur() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonDossierVolumineuxRelatedByIdBlobDescripteur($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonDossierVolumineuxsRelatedByIdBlobLogfile() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonDossierVolumineuxRelatedByIdBlobLogfile($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonBlobFile Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonBlobFilePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonBlobFilePeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonDossierVolumineuxRelatedByIdBlobDescripteur' == $relationName) {
            $this->initCommonDossierVolumineuxsRelatedByIdBlobDescripteur();
        }
        if ('CommonDossierVolumineuxRelatedByIdBlobLogfile' == $relationName) {
            $this->initCommonDossierVolumineuxsRelatedByIdBlobLogfile();
        }
    }

    /**
     * Clears out the collCommonDossierVolumineuxsRelatedByIdBlobDescripteur collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonBlobFile The current object (for fluent API support)
     * @see        addCommonDossierVolumineuxsRelatedByIdBlobDescripteur()
     */
    public function clearCommonDossierVolumineuxsRelatedByIdBlobDescripteur()
    {
        $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur = null; // important to set this to null since that means it is uninitialized
        $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteurPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonDossierVolumineuxsRelatedByIdBlobDescripteur collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonDossierVolumineuxsRelatedByIdBlobDescripteur($v = true)
    {
        $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteurPartial = $v;
    }

    /**
     * Initializes the collCommonDossierVolumineuxsRelatedByIdBlobDescripteur collection.
     *
     * By default this just sets the collCommonDossierVolumineuxsRelatedByIdBlobDescripteur collection to an empty array (like clearcollCommonDossierVolumineuxsRelatedByIdBlobDescripteur());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonDossierVolumineuxsRelatedByIdBlobDescripteur($overrideExisting = true)
    {
        if (null !== $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur && !$overrideExisting) {
            return;
        }
        $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur = new PropelObjectCollection();
        $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur->setModel('CommonDossierVolumineux');
    }

    /**
     * Gets an array of CommonDossierVolumineux objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonBlobFile is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     * @throws PropelException
     */
    public function getCommonDossierVolumineuxsRelatedByIdBlobDescripteur($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteurPartial && !$this->isNew();
        if (null === $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur) {
                // return empty collection
                $this->initCommonDossierVolumineuxsRelatedByIdBlobDescripteur();
            } else {
                $collCommonDossierVolumineuxsRelatedByIdBlobDescripteur = CommonDossierVolumineuxQuery::create(null, $criteria)
                    ->filterByCommonBlobFileRelatedByIdBlobDescripteur($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteurPartial && count($collCommonDossierVolumineuxsRelatedByIdBlobDescripteur)) {
                      $this->initCommonDossierVolumineuxsRelatedByIdBlobDescripteur(false);

                      foreach ($collCommonDossierVolumineuxsRelatedByIdBlobDescripteur as $obj) {
                        if (false == $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur->contains($obj)) {
                          $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur->append($obj);
                        }
                      }

                      $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteurPartial = true;
                    }

                    $collCommonDossierVolumineuxsRelatedByIdBlobDescripteur->getInternalIterator()->rewind();

                    return $collCommonDossierVolumineuxsRelatedByIdBlobDescripteur;
                }

                if ($partial && $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur) {
                    foreach ($this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur as $obj) {
                        if ($obj->isNew()) {
                            $collCommonDossierVolumineuxsRelatedByIdBlobDescripteur[] = $obj;
                        }
                    }
                }

                $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur = $collCommonDossierVolumineuxsRelatedByIdBlobDescripteur;
                $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteurPartial = false;
            }
        }

        return $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur;
    }

    /**
     * Sets a collection of CommonDossierVolumineuxRelatedByIdBlobDescripteur objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonDossierVolumineuxsRelatedByIdBlobDescripteur A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function setCommonDossierVolumineuxsRelatedByIdBlobDescripteur(PropelCollection $commonDossierVolumineuxsRelatedByIdBlobDescripteur, PropelPDO $con = null)
    {
        $commonDossierVolumineuxsRelatedByIdBlobDescripteurToDelete = $this->getCommonDossierVolumineuxsRelatedByIdBlobDescripteur(new Criteria(), $con)->diff($commonDossierVolumineuxsRelatedByIdBlobDescripteur);


        $this->commonDossierVolumineuxsRelatedByIdBlobDescripteurScheduledForDeletion = $commonDossierVolumineuxsRelatedByIdBlobDescripteurToDelete;

        foreach ($commonDossierVolumineuxsRelatedByIdBlobDescripteurToDelete as $commonDossierVolumineuxRelatedByIdBlobDescripteurRemoved) {
            $commonDossierVolumineuxRelatedByIdBlobDescripteurRemoved->setCommonBlobFileRelatedByIdBlobDescripteur(null);
        }

        $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur = null;
        foreach ($commonDossierVolumineuxsRelatedByIdBlobDescripteur as $commonDossierVolumineuxRelatedByIdBlobDescripteur) {
            $this->addCommonDossierVolumineuxRelatedByIdBlobDescripteur($commonDossierVolumineuxRelatedByIdBlobDescripteur);
        }

        $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur = $commonDossierVolumineuxsRelatedByIdBlobDescripteur;
        $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteurPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonDossierVolumineux objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonDossierVolumineux objects.
     * @throws PropelException
     */
    public function countCommonDossierVolumineuxsRelatedByIdBlobDescripteur(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteurPartial && !$this->isNew();
        if (null === $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonDossierVolumineuxsRelatedByIdBlobDescripteur());
            }
            $query = CommonDossierVolumineuxQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonBlobFileRelatedByIdBlobDescripteur($this)
                ->count($con);
        }

        return count($this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur);
    }

    /**
     * Method called to associate a CommonDossierVolumineux object to this object
     * through the CommonDossierVolumineux foreign key attribute.
     *
     * @param   CommonDossierVolumineux $l CommonDossierVolumineux
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function addCommonDossierVolumineuxRelatedByIdBlobDescripteur(CommonDossierVolumineux $l)
    {
        if ($this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur === null) {
            $this->initCommonDossierVolumineuxsRelatedByIdBlobDescripteur();
            $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteurPartial = true;
        }
        if (!in_array($l, $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonDossierVolumineuxRelatedByIdBlobDescripteur($l);
        }

        return $this;
    }

    /**
     * @param	CommonDossierVolumineuxRelatedByIdBlobDescripteur $commonDossierVolumineuxRelatedByIdBlobDescripteur The commonDossierVolumineuxRelatedByIdBlobDescripteur object to add.
     */
    protected function doAddCommonDossierVolumineuxRelatedByIdBlobDescripteur($commonDossierVolumineuxRelatedByIdBlobDescripteur)
    {
        $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur[]= $commonDossierVolumineuxRelatedByIdBlobDescripteur;
        $commonDossierVolumineuxRelatedByIdBlobDescripteur->setCommonBlobFileRelatedByIdBlobDescripteur($this);
    }

    /**
     * @param	CommonDossierVolumineuxRelatedByIdBlobDescripteur $commonDossierVolumineuxRelatedByIdBlobDescripteur The commonDossierVolumineuxRelatedByIdBlobDescripteur object to remove.
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function removeCommonDossierVolumineuxRelatedByIdBlobDescripteur($commonDossierVolumineuxRelatedByIdBlobDescripteur)
    {
        if ($this->getCommonDossierVolumineuxsRelatedByIdBlobDescripteur()->contains($commonDossierVolumineuxRelatedByIdBlobDescripteur)) {
            $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur->remove($this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur->search($commonDossierVolumineuxRelatedByIdBlobDescripteur));
            if (null === $this->commonDossierVolumineuxsRelatedByIdBlobDescripteurScheduledForDeletion) {
                $this->commonDossierVolumineuxsRelatedByIdBlobDescripteurScheduledForDeletion = clone $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur;
                $this->commonDossierVolumineuxsRelatedByIdBlobDescripteurScheduledForDeletion->clear();
            }
            $this->commonDossierVolumineuxsRelatedByIdBlobDescripteurScheduledForDeletion[]= $commonDossierVolumineuxRelatedByIdBlobDescripteur;
            $commonDossierVolumineuxRelatedByIdBlobDescripteur->setCommonBlobFileRelatedByIdBlobDescripteur(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobFile is new, it will return
     * an empty collection; or if this CommonBlobFile has previously
     * been saved, it will retrieve related CommonDossierVolumineuxsRelatedByIdBlobDescripteur from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     */
    public function getCommonDossierVolumineuxsRelatedByIdBlobDescripteurJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonDossierVolumineuxQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonDossierVolumineuxsRelatedByIdBlobDescripteur($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobFile is new, it will return
     * an empty collection; or if this CommonBlobFile has previously
     * been saved, it will retrieve related CommonDossierVolumineuxsRelatedByIdBlobDescripteur from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     */
    public function getCommonDossierVolumineuxsRelatedByIdBlobDescripteurJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonDossierVolumineuxQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonDossierVolumineuxsRelatedByIdBlobDescripteur($query, $con);
    }

    /**
     * Clears out the collCommonDossierVolumineuxsRelatedByIdBlobLogfile collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonBlobFile The current object (for fluent API support)
     * @see        addCommonDossierVolumineuxsRelatedByIdBlobLogfile()
     */
    public function clearCommonDossierVolumineuxsRelatedByIdBlobLogfile()
    {
        $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile = null; // important to set this to null since that means it is uninitialized
        $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfilePartial = null;

        return $this;
    }

    /**
     * reset is the collCommonDossierVolumineuxsRelatedByIdBlobLogfile collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonDossierVolumineuxsRelatedByIdBlobLogfile($v = true)
    {
        $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfilePartial = $v;
    }

    /**
     * Initializes the collCommonDossierVolumineuxsRelatedByIdBlobLogfile collection.
     *
     * By default this just sets the collCommonDossierVolumineuxsRelatedByIdBlobLogfile collection to an empty array (like clearcollCommonDossierVolumineuxsRelatedByIdBlobLogfile());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonDossierVolumineuxsRelatedByIdBlobLogfile($overrideExisting = true)
    {
        if (null !== $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile && !$overrideExisting) {
            return;
        }
        $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile = new PropelObjectCollection();
        $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile->setModel('CommonDossierVolumineux');
    }

    /**
     * Gets an array of CommonDossierVolumineux objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonBlobFile is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     * @throws PropelException
     */
    public function getCommonDossierVolumineuxsRelatedByIdBlobLogfile($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfilePartial && !$this->isNew();
        if (null === $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile) {
                // return empty collection
                $this->initCommonDossierVolumineuxsRelatedByIdBlobLogfile();
            } else {
                $collCommonDossierVolumineuxsRelatedByIdBlobLogfile = CommonDossierVolumineuxQuery::create(null, $criteria)
                    ->filterByCommonBlobFileRelatedByIdBlobLogfile($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfilePartial && count($collCommonDossierVolumineuxsRelatedByIdBlobLogfile)) {
                      $this->initCommonDossierVolumineuxsRelatedByIdBlobLogfile(false);

                      foreach ($collCommonDossierVolumineuxsRelatedByIdBlobLogfile as $obj) {
                        if (false == $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile->contains($obj)) {
                          $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile->append($obj);
                        }
                      }

                      $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfilePartial = true;
                    }

                    $collCommonDossierVolumineuxsRelatedByIdBlobLogfile->getInternalIterator()->rewind();

                    return $collCommonDossierVolumineuxsRelatedByIdBlobLogfile;
                }

                if ($partial && $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile) {
                    foreach ($this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile as $obj) {
                        if ($obj->isNew()) {
                            $collCommonDossierVolumineuxsRelatedByIdBlobLogfile[] = $obj;
                        }
                    }
                }

                $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile = $collCommonDossierVolumineuxsRelatedByIdBlobLogfile;
                $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfilePartial = false;
            }
        }

        return $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile;
    }

    /**
     * Sets a collection of CommonDossierVolumineuxRelatedByIdBlobLogfile objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonDossierVolumineuxsRelatedByIdBlobLogfile A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function setCommonDossierVolumineuxsRelatedByIdBlobLogfile(PropelCollection $commonDossierVolumineuxsRelatedByIdBlobLogfile, PropelPDO $con = null)
    {
        $commonDossierVolumineuxsRelatedByIdBlobLogfileToDelete = $this->getCommonDossierVolumineuxsRelatedByIdBlobLogfile(new Criteria(), $con)->diff($commonDossierVolumineuxsRelatedByIdBlobLogfile);


        $this->commonDossierVolumineuxsRelatedByIdBlobLogfileScheduledForDeletion = $commonDossierVolumineuxsRelatedByIdBlobLogfileToDelete;

        foreach ($commonDossierVolumineuxsRelatedByIdBlobLogfileToDelete as $commonDossierVolumineuxRelatedByIdBlobLogfileRemoved) {
            $commonDossierVolumineuxRelatedByIdBlobLogfileRemoved->setCommonBlobFileRelatedByIdBlobLogfile(null);
        }

        $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile = null;
        foreach ($commonDossierVolumineuxsRelatedByIdBlobLogfile as $commonDossierVolumineuxRelatedByIdBlobLogfile) {
            $this->addCommonDossierVolumineuxRelatedByIdBlobLogfile($commonDossierVolumineuxRelatedByIdBlobLogfile);
        }

        $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile = $commonDossierVolumineuxsRelatedByIdBlobLogfile;
        $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfilePartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonDossierVolumineux objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonDossierVolumineux objects.
     * @throws PropelException
     */
    public function countCommonDossierVolumineuxsRelatedByIdBlobLogfile(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfilePartial && !$this->isNew();
        if (null === $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonDossierVolumineuxsRelatedByIdBlobLogfile());
            }
            $query = CommonDossierVolumineuxQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonBlobFileRelatedByIdBlobLogfile($this)
                ->count($con);
        }

        return count($this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile);
    }

    /**
     * Method called to associate a CommonDossierVolumineux object to this object
     * through the CommonDossierVolumineux foreign key attribute.
     *
     * @param   CommonDossierVolumineux $l CommonDossierVolumineux
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function addCommonDossierVolumineuxRelatedByIdBlobLogfile(CommonDossierVolumineux $l)
    {
        if ($this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile === null) {
            $this->initCommonDossierVolumineuxsRelatedByIdBlobLogfile();
            $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfilePartial = true;
        }
        if (!in_array($l, $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonDossierVolumineuxRelatedByIdBlobLogfile($l);
        }

        return $this;
    }

    /**
     * @param	CommonDossierVolumineuxRelatedByIdBlobLogfile $commonDossierVolumineuxRelatedByIdBlobLogfile The commonDossierVolumineuxRelatedByIdBlobLogfile object to add.
     */
    protected function doAddCommonDossierVolumineuxRelatedByIdBlobLogfile($commonDossierVolumineuxRelatedByIdBlobLogfile)
    {
        $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile[]= $commonDossierVolumineuxRelatedByIdBlobLogfile;
        $commonDossierVolumineuxRelatedByIdBlobLogfile->setCommonBlobFileRelatedByIdBlobLogfile($this);
    }

    /**
     * @param	CommonDossierVolumineuxRelatedByIdBlobLogfile $commonDossierVolumineuxRelatedByIdBlobLogfile The commonDossierVolumineuxRelatedByIdBlobLogfile object to remove.
     * @return CommonBlobFile The current object (for fluent API support)
     */
    public function removeCommonDossierVolumineuxRelatedByIdBlobLogfile($commonDossierVolumineuxRelatedByIdBlobLogfile)
    {
        if ($this->getCommonDossierVolumineuxsRelatedByIdBlobLogfile()->contains($commonDossierVolumineuxRelatedByIdBlobLogfile)) {
            $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile->remove($this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile->search($commonDossierVolumineuxRelatedByIdBlobLogfile));
            if (null === $this->commonDossierVolumineuxsRelatedByIdBlobLogfileScheduledForDeletion) {
                $this->commonDossierVolumineuxsRelatedByIdBlobLogfileScheduledForDeletion = clone $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile;
                $this->commonDossierVolumineuxsRelatedByIdBlobLogfileScheduledForDeletion->clear();
            }
            $this->commonDossierVolumineuxsRelatedByIdBlobLogfileScheduledForDeletion[]= $commonDossierVolumineuxRelatedByIdBlobLogfile;
            $commonDossierVolumineuxRelatedByIdBlobLogfile->setCommonBlobFileRelatedByIdBlobLogfile(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobFile is new, it will return
     * an empty collection; or if this CommonBlobFile has previously
     * been saved, it will retrieve related CommonDossierVolumineuxsRelatedByIdBlobLogfile from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     */
    public function getCommonDossierVolumineuxsRelatedByIdBlobLogfileJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonDossierVolumineuxQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonDossierVolumineuxsRelatedByIdBlobLogfile($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonBlobFile is new, it will return
     * an empty collection; or if this CommonBlobFile has previously
     * been saved, it will retrieve related CommonDossierVolumineuxsRelatedByIdBlobLogfile from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonBlobFile.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     */
    public function getCommonDossierVolumineuxsRelatedByIdBlobLogfileJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonDossierVolumineuxQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonDossierVolumineuxsRelatedByIdBlobLogfile($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->old_id = null;
        $this->name = null;
        $this->deletion_datetime = null;
        $this->chemin = null;
        $this->dossier = null;
        $this->statut_synchro = null;
        $this->hash = null;
        $this->id = null;
        $this->extension = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur) {
                foreach ($this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile) {
                foreach ($this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur instanceof PropelCollection) {
            $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur->clearIterator();
        }
        $this->collCommonDossierVolumineuxsRelatedByIdBlobDescripteur = null;
        if ($this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile instanceof PropelCollection) {
            $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile->clearIterator();
        }
        $this->collCommonDossierVolumineuxsRelatedByIdBlobLogfile = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonBlobFilePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
