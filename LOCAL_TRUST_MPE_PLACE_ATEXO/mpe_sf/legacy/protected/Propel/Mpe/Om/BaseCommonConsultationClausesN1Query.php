<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationClausesN1;
use Application\Propel\Mpe\CommonConsultationClausesN1Peer;
use Application\Propel\Mpe\CommonConsultationClausesN1Query;
use Application\Propel\Mpe\CommonConsultationClausesN2;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN1;
use Application\Propel\Mpe\CommonTContratTitulaire;

/**
 * Base class that represents a query for the 'consultation_clauses_n1' table.
 *
 *
 *
 * @method CommonConsultationClausesN1Query orderById($order = Criteria::ASC) Order by the id column
 * @method CommonConsultationClausesN1Query orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 * @method CommonConsultationClausesN1Query orderByLotId($order = Criteria::ASC) Order by the lot_id column
 * @method CommonConsultationClausesN1Query orderByReferentielClauseN1Id($order = Criteria::ASC) Order by the referentiel_clause_n1_id column
 * @method CommonConsultationClausesN1Query orderByContratId($order = Criteria::ASC) Order by the contrat_id column
 *
 * @method CommonConsultationClausesN1Query groupById() Group by the id column
 * @method CommonConsultationClausesN1Query groupByConsultationId() Group by the consultation_id column
 * @method CommonConsultationClausesN1Query groupByLotId() Group by the lot_id column
 * @method CommonConsultationClausesN1Query groupByReferentielClauseN1Id() Group by the referentiel_clause_n1_id column
 * @method CommonConsultationClausesN1Query groupByContratId() Group by the contrat_id column
 *
 * @method CommonConsultationClausesN1Query leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonConsultationClausesN1Query rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonConsultationClausesN1Query innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonConsultationClausesN1Query leftJoinCommonTContratTitulaire($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonConsultationClausesN1Query rightJoinCommonTContratTitulaire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContratTitulaire relation
 * @method CommonConsultationClausesN1Query innerJoinCommonTContratTitulaire($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContratTitulaire relation
 *
 * @method CommonConsultationClausesN1Query leftJoinCommonReferentielConsultationClausesN1($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonReferentielConsultationClausesN1 relation
 * @method CommonConsultationClausesN1Query rightJoinCommonReferentielConsultationClausesN1($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonReferentielConsultationClausesN1 relation
 * @method CommonConsultationClausesN1Query innerJoinCommonReferentielConsultationClausesN1($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonReferentielConsultationClausesN1 relation
 *
 * @method CommonConsultationClausesN1Query leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonConsultationClausesN1Query rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonConsultationClausesN1Query innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonConsultationClausesN1Query leftJoinCommonCategorieLot($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonCategorieLot relation
 * @method CommonConsultationClausesN1Query rightJoinCommonCategorieLot($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonCategorieLot relation
 * @method CommonConsultationClausesN1Query innerJoinCommonCategorieLot($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonCategorieLot relation
 *
 * @method CommonConsultationClausesN1Query leftJoinCommonConsultationClausesN2($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultationClausesN2 relation
 * @method CommonConsultationClausesN1Query rightJoinCommonConsultationClausesN2($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultationClausesN2 relation
 * @method CommonConsultationClausesN1Query innerJoinCommonConsultationClausesN2($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultationClausesN2 relation
 *
 * @method CommonConsultationClausesN1 findOne(PropelPDO $con = null) Return the first CommonConsultationClausesN1 matching the query
 * @method CommonConsultationClausesN1 findOneOrCreate(PropelPDO $con = null) Return the first CommonConsultationClausesN1 matching the query, or a new CommonConsultationClausesN1 object populated from the query conditions when no match is found
 *
 * @method CommonConsultationClausesN1 findOneByConsultationId(int $consultation_id) Return the first CommonConsultationClausesN1 filtered by the consultation_id column
 * @method CommonConsultationClausesN1 findOneByLotId(int $lot_id) Return the first CommonConsultationClausesN1 filtered by the lot_id column
 * @method CommonConsultationClausesN1 findOneByReferentielClauseN1Id(int $referentiel_clause_n1_id) Return the first CommonConsultationClausesN1 filtered by the referentiel_clause_n1_id column
 * @method CommonConsultationClausesN1 findOneByContratId(int $contrat_id) Return the first CommonConsultationClausesN1 filtered by the contrat_id column
 *
 * @method array findById(int $id) Return CommonConsultationClausesN1 objects filtered by the id column
 * @method array findByConsultationId(int $consultation_id) Return CommonConsultationClausesN1 objects filtered by the consultation_id column
 * @method array findByLotId(int $lot_id) Return CommonConsultationClausesN1 objects filtered by the lot_id column
 * @method array findByReferentielClauseN1Id(int $referentiel_clause_n1_id) Return CommonConsultationClausesN1 objects filtered by the referentiel_clause_n1_id column
 * @method array findByContratId(int $contrat_id) Return CommonConsultationClausesN1 objects filtered by the contrat_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationClausesN1Query extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonConsultationClausesN1Query object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonConsultationClausesN1', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonConsultationClausesN1Query object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonConsultationClausesN1Query|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonConsultationClausesN1Query
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonConsultationClausesN1Query) {
            return $criteria;
        }
        $query = new CommonConsultationClausesN1Query();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonConsultationClausesN1|CommonConsultationClausesN1[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonConsultationClausesN1Peer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationClausesN1 A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonConsultationClausesN1 A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `consultation_id`, `lot_id`, `referentiel_clause_n1_id`, `contrat_id` FROM `consultation_clauses_n1` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonConsultationClausesN1();
            $obj->hydrate($row);
            CommonConsultationClausesN1Peer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonConsultationClausesN1|CommonConsultationClausesN1[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonConsultationClausesN1[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonConsultationClausesN1Peer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonConsultationClausesN1Peer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonConsultationClausesN1Peer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonConsultationClausesN1Peer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationClausesN1Peer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultation()
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonConsultationClausesN1Peer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonConsultationClausesN1Peer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationClausesN1Peer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Filter the query on the lot_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLotId(1234); // WHERE lot_id = 1234
     * $query->filterByLotId(array(12, 34)); // WHERE lot_id IN (12, 34)
     * $query->filterByLotId(array('min' => 12)); // WHERE lot_id >= 12
     * $query->filterByLotId(array('max' => 12)); // WHERE lot_id <= 12
     * </code>
     *
     * @see       filterByCommonCategorieLot()
     *
     * @param     mixed $lotId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function filterByLotId($lotId = null, $comparison = null)
    {
        if (is_array($lotId)) {
            $useMinMax = false;
            if (isset($lotId['min'])) {
                $this->addUsingAlias(CommonConsultationClausesN1Peer::LOT_ID, $lotId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lotId['max'])) {
                $this->addUsingAlias(CommonConsultationClausesN1Peer::LOT_ID, $lotId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationClausesN1Peer::LOT_ID, $lotId, $comparison);
    }

    /**
     * Filter the query on the referentiel_clause_n1_id column
     *
     * Example usage:
     * <code>
     * $query->filterByReferentielClauseN1Id(1234); // WHERE referentiel_clause_n1_id = 1234
     * $query->filterByReferentielClauseN1Id(array(12, 34)); // WHERE referentiel_clause_n1_id IN (12, 34)
     * $query->filterByReferentielClauseN1Id(array('min' => 12)); // WHERE referentiel_clause_n1_id >= 12
     * $query->filterByReferentielClauseN1Id(array('max' => 12)); // WHERE referentiel_clause_n1_id <= 12
     * </code>
     *
     * @see       filterByCommonReferentielConsultationClausesN1()
     *
     * @param     mixed $referentielClauseN1Id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function filterByReferentielClauseN1Id($referentielClauseN1Id = null, $comparison = null)
    {
        if (is_array($referentielClauseN1Id)) {
            $useMinMax = false;
            if (isset($referentielClauseN1Id['min'])) {
                $this->addUsingAlias(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, $referentielClauseN1Id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($referentielClauseN1Id['max'])) {
                $this->addUsingAlias(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, $referentielClauseN1Id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, $referentielClauseN1Id, $comparison);
    }

    /**
     * Filter the query on the contrat_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContratId(1234); // WHERE contrat_id = 1234
     * $query->filterByContratId(array(12, 34)); // WHERE contrat_id IN (12, 34)
     * $query->filterByContratId(array('min' => 12)); // WHERE contrat_id >= 12
     * $query->filterByContratId(array('max' => 12)); // WHERE contrat_id <= 12
     * </code>
     *
     * @see       filterByCommonTContratTitulaire()
     *
     * @param     mixed $contratId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function filterByContratId($contratId = null, $comparison = null)
    {
        if (is_array($contratId)) {
            $useMinMax = false;
            if (isset($contratId['min'])) {
                $this->addUsingAlias(CommonConsultationClausesN1Peer::CONTRAT_ID, $contratId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($contratId['max'])) {
                $this->addUsingAlias(CommonConsultationClausesN1Peer::CONTRAT_ID, $contratId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonConsultationClausesN1Peer::CONTRAT_ID, $contratId, $comparison);
    }

    /**
     * Filter the query by a related CommonTContratTitulaire object
     *
     * @param   CommonTContratTitulaire|PropelObjectCollection $commonTContratTitulaire The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationClausesN1Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContratTitulaire($commonTContratTitulaire, $comparison = null)
    {
        if ($commonTContratTitulaire instanceof CommonTContratTitulaire) {
            return $this
                ->addUsingAlias(CommonConsultationClausesN1Peer::CONTRAT_ID, $commonTContratTitulaire->getIdContratTitulaire(), $comparison);
        } elseif ($commonTContratTitulaire instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConsultationClausesN1Peer::CONTRAT_ID, $commonTContratTitulaire->toKeyValue('PrimaryKey', 'IdContratTitulaire'), $comparison);
        } else {
            throw new PropelException('filterByCommonTContratTitulaire() only accepts arguments of type CommonTContratTitulaire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContratTitulaire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function joinCommonTContratTitulaire($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContratTitulaire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContratTitulaire');
        }

        return $this;
    }

    /**
     * Use the CommonTContratTitulaire relation CommonTContratTitulaire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContratTitulaireQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContratTitulaireQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTContratTitulaire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContratTitulaire', '\Application\Propel\Mpe\CommonTContratTitulaireQuery');
    }

    /**
     * Filter the query by a related CommonReferentielConsultationClausesN1 object
     *
     * @param   CommonReferentielConsultationClausesN1|PropelObjectCollection $commonReferentielConsultationClausesN1 The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationClausesN1Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonReferentielConsultationClausesN1($commonReferentielConsultationClausesN1, $comparison = null)
    {
        if ($commonReferentielConsultationClausesN1 instanceof CommonReferentielConsultationClausesN1) {
            return $this
                ->addUsingAlias(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, $commonReferentielConsultationClausesN1->getId(), $comparison);
        } elseif ($commonReferentielConsultationClausesN1 instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConsultationClausesN1Peer::REFERENTIEL_CLAUSE_N1_ID, $commonReferentielConsultationClausesN1->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonReferentielConsultationClausesN1() only accepts arguments of type CommonReferentielConsultationClausesN1 or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonReferentielConsultationClausesN1 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function joinCommonReferentielConsultationClausesN1($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonReferentielConsultationClausesN1');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonReferentielConsultationClausesN1');
        }

        return $this;
    }

    /**
     * Use the CommonReferentielConsultationClausesN1 relation CommonReferentielConsultationClausesN1 object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonReferentielConsultationClausesN1Query A secondary query class using the current class as primary query
     */
    public function useCommonReferentielConsultationClausesN1Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonReferentielConsultationClausesN1($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonReferentielConsultationClausesN1', '\Application\Propel\Mpe\CommonReferentielConsultationClausesN1Query');
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationClausesN1Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonConsultationClausesN1Peer::CONSULTATION_ID, $commonConsultation->getId(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConsultationClausesN1Peer::CONSULTATION_ID, $commonConsultation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Filter the query by a related CommonCategorieLot object
     *
     * @param   CommonCategorieLot|PropelObjectCollection $commonCategorieLot The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationClausesN1Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonCategorieLot($commonCategorieLot, $comparison = null)
    {
        if ($commonCategorieLot instanceof CommonCategorieLot) {
            return $this
                ->addUsingAlias(CommonConsultationClausesN1Peer::LOT_ID, $commonCategorieLot->getId(), $comparison);
        } elseif ($commonCategorieLot instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonConsultationClausesN1Peer::LOT_ID, $commonCategorieLot->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonCategorieLot() only accepts arguments of type CommonCategorieLot or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonCategorieLot relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function joinCommonCategorieLot($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonCategorieLot');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonCategorieLot');
        }

        return $this;
    }

    /**
     * Use the CommonCategorieLot relation CommonCategorieLot object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonCategorieLotQuery A secondary query class using the current class as primary query
     */
    public function useCommonCategorieLotQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonCategorieLot($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonCategorieLot', '\Application\Propel\Mpe\CommonCategorieLotQuery');
    }

    /**
     * Filter the query by a related CommonConsultationClausesN2 object
     *
     * @param   CommonConsultationClausesN2|PropelObjectCollection $commonConsultationClausesN2  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonConsultationClausesN1Query The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultationClausesN2($commonConsultationClausesN2, $comparison = null)
    {
        if ($commonConsultationClausesN2 instanceof CommonConsultationClausesN2) {
            return $this
                ->addUsingAlias(CommonConsultationClausesN1Peer::ID, $commonConsultationClausesN2->getClauseN1Id(), $comparison);
        } elseif ($commonConsultationClausesN2 instanceof PropelObjectCollection) {
            return $this
                ->useCommonConsultationClausesN2Query()
                ->filterByPrimaryKeys($commonConsultationClausesN2->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonConsultationClausesN2() only accepts arguments of type CommonConsultationClausesN2 or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultationClausesN2 relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function joinCommonConsultationClausesN2($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultationClausesN2');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultationClausesN2');
        }

        return $this;
    }

    /**
     * Use the CommonConsultationClausesN2 relation CommonConsultationClausesN2 object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationClausesN2Query A secondary query class using the current class as primary query
     */
    public function useCommonConsultationClausesN2Query($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonConsultationClausesN2($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultationClausesN2', '\Application\Propel\Mpe\CommonConsultationClausesN2Query');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonConsultationClausesN1 $commonConsultationClausesN1 Object to remove from the list of results
     *
     * @return CommonConsultationClausesN1Query The current query, for fluid interface
     */
    public function prune($commonConsultationClausesN1 = null)
    {
        if ($commonConsultationClausesN1) {
            $this->addUsingAlias(CommonConsultationClausesN1Peer::ID, $commonConsultationClausesN1->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
