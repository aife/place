<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \PDO;
use \PDOStatement;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonConsultationTagsPeer;
use Application\Propel\Mpe\CommonDossierVolumineuxPeer;
use Application\Propel\Mpe\CommonInterfaceSuiviPeer;
use Application\Propel\Mpe\CommonOperationsPeer;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonPlateformeVirtuellePeer;
use Application\Propel\Mpe\CommonReferentielAchatPeer;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonTTypeContratPeer;
use Application\Propel\Mpe\Map\CommonConsultationTableMap;

/**
 * Base static class for performing query and update operations on the 'consultation' table.
 *
 *
 *
 * @package propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'mpe';

    /** the table name for this class */
    const TABLE_NAME = 'consultation';

    /** the related Propel class for this table */
    const OM_CLASS = 'Application\\Propel\\Mpe\\CommonConsultation';

    /** the related TableMap class for this table */
    const TM_CLASS = 'CommonConsultationTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 297;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 297;

    /** the column name for the id field */
    const ID = 'consultation.id';

    /** the column name for the reference field */
    const REFERENCE = 'consultation.reference';

    /** the column name for the code_externe field */
    const CODE_EXTERNE = 'consultation.code_externe';

    /** the column name for the organisme field */
    const ORGANISME = 'consultation.organisme';

    /** the column name for the reference_utilisateur field */
    const REFERENCE_UTILISATEUR = 'consultation.reference_utilisateur';

    /** the column name for the categorie field */
    const CATEGORIE = 'consultation.categorie';

    /** the column name for the titre field */
    const TITRE = 'consultation.titre';

    /** the column name for the resume field */
    const RESUME = 'consultation.resume';

    /** the column name for the datedebut field */
    const DATEDEBUT = 'consultation.datedebut';

    /** the column name for the datefin field */
    const DATEFIN = 'consultation.datefin';

    /** the column name for the datevalidation field */
    const DATEVALIDATION = 'consultation.datevalidation';

    /** the column name for the type_procedure field */
    const TYPE_PROCEDURE = 'consultation.type_procedure';

    /** the column name for the code_procedure field */
    const CODE_PROCEDURE = 'consultation.code_procedure';

    /** the column name for the reponse_electronique field */
    const REPONSE_ELECTRONIQUE = 'consultation.reponse_electronique';

    /** the column name for the num_procedure field */
    const NUM_PROCEDURE = 'consultation.num_procedure';

    /** the column name for the id_type_procedure field */
    const ID_TYPE_PROCEDURE = 'consultation.id_type_procedure';

    /** the column name for the id_type_avis field */
    const ID_TYPE_AVIS = 'consultation.id_type_avis';

    /** the column name for the lieu_execution field */
    const LIEU_EXECUTION = 'consultation.lieu_execution';

    /** the column name for the type_mise_en_ligne field */
    const TYPE_MISE_EN_LIGNE = 'consultation.type_mise_en_ligne';

    /** the column name for the datemiseenligne field */
    const DATEMISEENLIGNE = 'consultation.datemiseenligne';

    /** the column name for the is_tiers_avis field */
    const IS_TIERS_AVIS = 'consultation.is_tiers_avis';

    /** the column name for the url field */
    const URL = 'consultation.url';

    /** the column name for the datefin_sad field */
    const DATEFIN_SAD = 'consultation.datefin_sad';

    /** the column name for the is_sys_acq_dyn field */
    const IS_SYS_ACQ_DYN = 'consultation.is_sys_acq_dyn';

    /** the column name for the reference_consultation_init field */
    const REFERENCE_CONSULTATION_INIT = 'consultation.reference_consultation_init';

    /** the column name for the signature_offre field */
    const SIGNATURE_OFFRE = 'consultation.signature_offre';

    /** the column name for the id_type_validation field */
    const ID_TYPE_VALIDATION = 'consultation.id_type_validation';

    /** the column name for the etat_approbation field */
    const ETAT_APPROBATION = 'consultation.etat_approbation';

    /** the column name for the etat_validation field */
    const ETAT_VALIDATION = 'consultation.etat_validation';

    /** the column name for the champ_supp_invisible field */
    const CHAMP_SUPP_INVISIBLE = 'consultation.champ_supp_invisible';

    /** the column name for the code_cpv_1 field */
    const CODE_CPV_1 = 'consultation.code_cpv_1';

    /** the column name for the code_cpv_2 field */
    const CODE_CPV_2 = 'consultation.code_cpv_2';

    /** the column name for the publication_europe field */
    const PUBLICATION_EUROPE = 'consultation.publication_europe';

    /** the column name for the etat_publication field */
    const ETAT_PUBLICATION = 'consultation.etat_publication';

    /** the column name for the poursuivre_affichage field */
    const POURSUIVRE_AFFICHAGE = 'consultation.poursuivre_affichage';

    /** the column name for the poursuivre_affichage_unite field */
    const POURSUIVRE_AFFICHAGE_UNITE = 'consultation.poursuivre_affichage_unite';

    /** the column name for the nbr_telechargement_dce field */
    const NBR_TELECHARGEMENT_DCE = 'consultation.nbr_telechargement_dce';

    /** the column name for the old_service_id field */
    const OLD_SERVICE_ID = 'consultation.old_service_id';

    /** the column name for the old_service_associe_id field */
    const OLD_SERVICE_ASSOCIE_ID = 'consultation.old_service_associe_id';

    /** the column name for the detail_consultation field */
    const DETAIL_CONSULTATION = 'consultation.detail_consultation';

    /** the column name for the date_fin_affichage field */
    const DATE_FIN_AFFICHAGE = 'consultation.date_fin_affichage';

    /** the column name for the depouillable_phase_consultation field */
    const DEPOUILLABLE_PHASE_CONSULTATION = 'consultation.depouillable_phase_consultation';

    /** the column name for the consultation_transverse field */
    const CONSULTATION_TRANSVERSE = 'consultation.consultation_transverse';

    /** the column name for the consultation_achat_publique field */
    const CONSULTATION_ACHAT_PUBLIQUE = 'consultation.consultation_achat_publique';

    /** the column name for the url_consultation_achat_publique field */
    const URL_CONSULTATION_ACHAT_PUBLIQUE = 'consultation.url_consultation_achat_publique';

    /** the column name for the partial_dce_download field */
    const PARTIAL_DCE_DOWNLOAD = 'consultation.partial_dce_download';

    /** the column name for the tirage_plan field */
    const TIRAGE_PLAN = 'consultation.tirage_plan';

    /** the column name for the tireur_plan field */
    const TIREUR_PLAN = 'consultation.tireur_plan';

    /** the column name for the date_mise_en_ligne_calcule field */
    const DATE_MISE_EN_LIGNE_CALCULE = 'consultation.date_mise_en_ligne_calcule';

    /** the column name for the accessibilite_en field */
    const ACCESSIBILITE_EN = 'consultation.accessibilite_en';

    /** the column name for the accessibilite_es field */
    const ACCESSIBILITE_ES = 'consultation.accessibilite_es';

    /** the column name for the nbr_reponse field */
    const NBR_REPONSE = 'consultation.nbr_reponse';

    /** the column name for the id_type_procedure_org field */
    const ID_TYPE_PROCEDURE_ORG = 'consultation.id_type_procedure_org';

    /** the column name for the organisme_consultation_init field */
    const ORGANISME_CONSULTATION_INIT = 'consultation.organisme_consultation_init';

    /** the column name for the tirage_descriptif field */
    const TIRAGE_DESCRIPTIF = 'consultation.tirage_descriptif';

    /** the column name for the date_validation_intermediaire field */
    const DATE_VALIDATION_INTERMEDIAIRE = 'consultation.date_validation_intermediaire';

    /** the column name for the accessibilite_fr field */
    const ACCESSIBILITE_FR = 'consultation.accessibilite_fr';

    /** the column name for the id_tr_accessibilite field */
    const ID_TR_ACCESSIBILITE = 'consultation.id_tr_accessibilite';

    /** the column name for the accessibilite_cz field */
    const ACCESSIBILITE_CZ = 'consultation.accessibilite_cz';

    /** the column name for the accessibilite_du field */
    const ACCESSIBILITE_DU = 'consultation.accessibilite_du';

    /** the column name for the accessibilite_su field */
    const ACCESSIBILITE_SU = 'consultation.accessibilite_su';

    /** the column name for the accessibilite_ar field */
    const ACCESSIBILITE_AR = 'consultation.accessibilite_ar';

    /** the column name for the alloti field */
    const ALLOTI = 'consultation.alloti';

    /** the column name for the numero_phase field */
    const NUMERO_PHASE = 'consultation.numero_phase';

    /** the column name for the consultation_externe field */
    const CONSULTATION_EXTERNE = 'consultation.consultation_externe';

    /** the column name for the url_consultation_externe field */
    const URL_CONSULTATION_EXTERNE = 'consultation.url_consultation_externe';

    /** the column name for the org_denomination field */
    const ORG_DENOMINATION = 'consultation.org_denomination';

    /** the column name for the domaines_activites field */
    const DOMAINES_ACTIVITES = 'consultation.domaines_activites';

    /** the column name for the id_affaire field */
    const ID_AFFAIRE = 'consultation.id_affaire';

    /** the column name for the adresse_retrais_dossiers field */
    const ADRESSE_RETRAIS_DOSSIERS = 'consultation.adresse_retrais_dossiers';

    /** the column name for the caution_provisoire field */
    const CAUTION_PROVISOIRE = 'consultation.caution_provisoire';

    /** the column name for the adresse_depot_offres field */
    const ADRESSE_DEPOT_OFFRES = 'consultation.adresse_depot_offres';

    /** the column name for the lieu_ouverture_plis field */
    const LIEU_OUVERTURE_PLIS = 'consultation.lieu_ouverture_plis';

    /** the column name for the prix_aquisition_plans field */
    const PRIX_AQUISITION_PLANS = 'consultation.prix_aquisition_plans';

    /** the column name for the qualification field */
    const QUALIFICATION = 'consultation.qualification';

    /** the column name for the agrements field */
    const AGREMENTS = 'consultation.agrements';

    /** the column name for the add_echantillion field */
    const ADD_ECHANTILLION = 'consultation.add_echantillion';

    /** the column name for the date_limite_echantillion field */
    const DATE_LIMITE_ECHANTILLION = 'consultation.date_limite_echantillion';

    /** the column name for the add_reunion field */
    const ADD_REUNION = 'consultation.add_reunion';

    /** the column name for the date_reunion field */
    const DATE_REUNION = 'consultation.date_reunion';

    /** the column name for the variantes field */
    const VARIANTES = 'consultation.variantes';

    /** the column name for the adresse_depot_offres_ar field */
    const ADRESSE_DEPOT_OFFRES_AR = 'consultation.adresse_depot_offres_ar';

    /** the column name for the lieu_ouverture_plis_ar field */
    const LIEU_OUVERTURE_PLIS_AR = 'consultation.lieu_ouverture_plis_ar';

    /** the column name for the adresse_retrais_dossiers_ar field */
    const ADRESSE_RETRAIS_DOSSIERS_AR = 'consultation.adresse_retrais_dossiers_ar';

    /** the column name for the pieces_dossier_admin field */
    const PIECES_DOSSIER_ADMIN = 'consultation.pieces_dossier_admin';

    /** the column name for the pieces_dossier_admin_fr field */
    const PIECES_DOSSIER_ADMIN_FR = 'consultation.pieces_dossier_admin_fr';

    /** the column name for the pieces_dossier_admin_en field */
    const PIECES_DOSSIER_ADMIN_EN = 'consultation.pieces_dossier_admin_en';

    /** the column name for the pieces_dossier_admin_es field */
    const PIECES_DOSSIER_ADMIN_ES = 'consultation.pieces_dossier_admin_es';

    /** the column name for the pieces_dossier_admin_su field */
    const PIECES_DOSSIER_ADMIN_SU = 'consultation.pieces_dossier_admin_su';

    /** the column name for the pieces_dossier_admin_du field */
    const PIECES_DOSSIER_ADMIN_DU = 'consultation.pieces_dossier_admin_du';

    /** the column name for the pieces_dossier_admin_cz field */
    const PIECES_DOSSIER_ADMIN_CZ = 'consultation.pieces_dossier_admin_cz';

    /** the column name for the pieces_dossier_admin_ar field */
    const PIECES_DOSSIER_ADMIN_AR = 'consultation.pieces_dossier_admin_ar';

    /** the column name for the pieces_dossier_tech field */
    const PIECES_DOSSIER_TECH = 'consultation.pieces_dossier_tech';

    /** the column name for the pieces_dossier_tech_fr field */
    const PIECES_DOSSIER_TECH_FR = 'consultation.pieces_dossier_tech_fr';

    /** the column name for the pieces_dossier_tech_en field */
    const PIECES_DOSSIER_TECH_EN = 'consultation.pieces_dossier_tech_en';

    /** the column name for the pieces_dossier_tech_es field */
    const PIECES_DOSSIER_TECH_ES = 'consultation.pieces_dossier_tech_es';

    /** the column name for the pieces_dossier_tech_su field */
    const PIECES_DOSSIER_TECH_SU = 'consultation.pieces_dossier_tech_su';

    /** the column name for the pieces_dossier_tech_du field */
    const PIECES_DOSSIER_TECH_DU = 'consultation.pieces_dossier_tech_du';

    /** the column name for the pieces_dossier_tech_cz field */
    const PIECES_DOSSIER_TECH_CZ = 'consultation.pieces_dossier_tech_cz';

    /** the column name for the pieces_dossier_tech_ar field */
    const PIECES_DOSSIER_TECH_AR = 'consultation.pieces_dossier_tech_ar';

    /** the column name for the pieces_dossier_additif field */
    const PIECES_DOSSIER_ADDITIF = 'consultation.pieces_dossier_additif';

    /** the column name for the pieces_dossier_additif_fr field */
    const PIECES_DOSSIER_ADDITIF_FR = 'consultation.pieces_dossier_additif_fr';

    /** the column name for the pieces_dossier_additif_en field */
    const PIECES_DOSSIER_ADDITIF_EN = 'consultation.pieces_dossier_additif_en';

    /** the column name for the pieces_dossier_additif_es field */
    const PIECES_DOSSIER_ADDITIF_ES = 'consultation.pieces_dossier_additif_es';

    /** the column name for the pieces_dossier_additif_su field */
    const PIECES_DOSSIER_ADDITIF_SU = 'consultation.pieces_dossier_additif_su';

    /** the column name for the pieces_dossier_additif_du field */
    const PIECES_DOSSIER_ADDITIF_DU = 'consultation.pieces_dossier_additif_du';

    /** the column name for the pieces_dossier_additif_cz field */
    const PIECES_DOSSIER_ADDITIF_CZ = 'consultation.pieces_dossier_additif_cz';

    /** the column name for the pieces_dossier_additif_ar field */
    const PIECES_DOSSIER_ADDITIF_AR = 'consultation.pieces_dossier_additif_ar';

    /** the column name for the id_rpa field */
    const ID_RPA = 'consultation.id_rpa';

    /** the column name for the detail_consultation_fr field */
    const DETAIL_CONSULTATION_FR = 'consultation.detail_consultation_fr';

    /** the column name for the detail_consultation_en field */
    const DETAIL_CONSULTATION_EN = 'consultation.detail_consultation_en';

    /** the column name for the detail_consultation_es field */
    const DETAIL_CONSULTATION_ES = 'consultation.detail_consultation_es';

    /** the column name for the detail_consultation_su field */
    const DETAIL_CONSULTATION_SU = 'consultation.detail_consultation_su';

    /** the column name for the detail_consultation_du field */
    const DETAIL_CONSULTATION_DU = 'consultation.detail_consultation_du';

    /** the column name for the detail_consultation_cz field */
    const DETAIL_CONSULTATION_CZ = 'consultation.detail_consultation_cz';

    /** the column name for the detail_consultation_ar field */
    const DETAIL_CONSULTATION_AR = 'consultation.detail_consultation_ar';

    /** the column name for the echantillon field */
    const ECHANTILLON = 'consultation.echantillon';

    /** the column name for the reunion field */
    const REUNION = 'consultation.reunion';

    /** the column name for the visites_lieux field */
    const VISITES_LIEUX = 'consultation.visites_lieux';

    /** the column name for the variante_calcule field */
    const VARIANTE_CALCULE = 'consultation.variante_calcule';

    /** the column name for the adresse_retrais_dossiers_fr field */
    const ADRESSE_RETRAIS_DOSSIERS_FR = 'consultation.adresse_retrais_dossiers_fr';

    /** the column name for the adresse_retrais_dossiers_en field */
    const ADRESSE_RETRAIS_DOSSIERS_EN = 'consultation.adresse_retrais_dossiers_en';

    /** the column name for the adresse_retrais_dossiers_es field */
    const ADRESSE_RETRAIS_DOSSIERS_ES = 'consultation.adresse_retrais_dossiers_es';

    /** the column name for the adresse_retrais_dossiers_su field */
    const ADRESSE_RETRAIS_DOSSIERS_SU = 'consultation.adresse_retrais_dossiers_su';

    /** the column name for the adresse_retrais_dossiers_du field */
    const ADRESSE_RETRAIS_DOSSIERS_DU = 'consultation.adresse_retrais_dossiers_du';

    /** the column name for the adresse_retrais_dossiers_cz field */
    const ADRESSE_RETRAIS_DOSSIERS_CZ = 'consultation.adresse_retrais_dossiers_cz';

    /** the column name for the adresse_depot_offres_fr field */
    const ADRESSE_DEPOT_OFFRES_FR = 'consultation.adresse_depot_offres_fr';

    /** the column name for the adresse_depot_offres_en field */
    const ADRESSE_DEPOT_OFFRES_EN = 'consultation.adresse_depot_offres_en';

    /** the column name for the adresse_depot_offres_es field */
    const ADRESSE_DEPOT_OFFRES_ES = 'consultation.adresse_depot_offres_es';

    /** the column name for the adresse_depot_offres_su field */
    const ADRESSE_DEPOT_OFFRES_SU = 'consultation.adresse_depot_offres_su';

    /** the column name for the adresse_depot_offres_du field */
    const ADRESSE_DEPOT_OFFRES_DU = 'consultation.adresse_depot_offres_du';

    /** the column name for the adresse_depot_offres_cz field */
    const ADRESSE_DEPOT_OFFRES_CZ = 'consultation.adresse_depot_offres_cz';

    /** the column name for the lieu_ouverture_plis_fr field */
    const LIEU_OUVERTURE_PLIS_FR = 'consultation.lieu_ouverture_plis_fr';

    /** the column name for the lieu_ouverture_plis_en field */
    const LIEU_OUVERTURE_PLIS_EN = 'consultation.lieu_ouverture_plis_en';

    /** the column name for the lieu_ouverture_plis_es field */
    const LIEU_OUVERTURE_PLIS_ES = 'consultation.lieu_ouverture_plis_es';

    /** the column name for the lieu_ouverture_plis_su field */
    const LIEU_OUVERTURE_PLIS_SU = 'consultation.lieu_ouverture_plis_su';

    /** the column name for the lieu_ouverture_plis_du field */
    const LIEU_OUVERTURE_PLIS_DU = 'consultation.lieu_ouverture_plis_du';

    /** the column name for the lieu_ouverture_plis_cz field */
    const LIEU_OUVERTURE_PLIS_CZ = 'consultation.lieu_ouverture_plis_cz';

    /** the column name for the add_echantillion_fr field */
    const ADD_ECHANTILLION_FR = 'consultation.add_echantillion_fr';

    /** the column name for the add_echantillion_en field */
    const ADD_ECHANTILLION_EN = 'consultation.add_echantillion_en';

    /** the column name for the add_echantillion_es field */
    const ADD_ECHANTILLION_ES = 'consultation.add_echantillion_es';

    /** the column name for the add_echantillion_su field */
    const ADD_ECHANTILLION_SU = 'consultation.add_echantillion_su';

    /** the column name for the add_echantillion_du field */
    const ADD_ECHANTILLION_DU = 'consultation.add_echantillion_du';

    /** the column name for the add_echantillion_cz field */
    const ADD_ECHANTILLION_CZ = 'consultation.add_echantillion_cz';

    /** the column name for the add_echantillion_ar field */
    const ADD_ECHANTILLION_AR = 'consultation.add_echantillion_ar';

    /** the column name for the add_reunion_fr field */
    const ADD_REUNION_FR = 'consultation.add_reunion_fr';

    /** the column name for the add_reunion_en field */
    const ADD_REUNION_EN = 'consultation.add_reunion_en';

    /** the column name for the add_reunion_es field */
    const ADD_REUNION_ES = 'consultation.add_reunion_es';

    /** the column name for the add_reunion_su field */
    const ADD_REUNION_SU = 'consultation.add_reunion_su';

    /** the column name for the add_reunion_du field */
    const ADD_REUNION_DU = 'consultation.add_reunion_du';

    /** the column name for the add_reunion_cz field */
    const ADD_REUNION_CZ = 'consultation.add_reunion_cz';

    /** the column name for the add_reunion_ar field */
    const ADD_REUNION_AR = 'consultation.add_reunion_ar';

    /** the column name for the mode_passation field */
    const MODE_PASSATION = 'consultation.mode_passation';

    /** the column name for the consultation_annulee field */
    const CONSULTATION_ANNULEE = 'consultation.consultation_annulee';

    /** the column name for the accessibilite_it field */
    const ACCESSIBILITE_IT = 'consultation.accessibilite_it';

    /** the column name for the adresse_depot_offres_it field */
    const ADRESSE_DEPOT_OFFRES_IT = 'consultation.adresse_depot_offres_it';

    /** the column name for the lieu_ouverture_plis_it field */
    const LIEU_OUVERTURE_PLIS_IT = 'consultation.lieu_ouverture_plis_it';

    /** the column name for the adresse_retrais_dossiers_it field */
    const ADRESSE_RETRAIS_DOSSIERS_IT = 'consultation.adresse_retrais_dossiers_it';

    /** the column name for the pieces_dossier_admin_it field */
    const PIECES_DOSSIER_ADMIN_IT = 'consultation.pieces_dossier_admin_it';

    /** the column name for the pieces_dossier_tech_it field */
    const PIECES_DOSSIER_TECH_IT = 'consultation.pieces_dossier_tech_it';

    /** the column name for the pieces_dossier_additif_it field */
    const PIECES_DOSSIER_ADDITIF_IT = 'consultation.pieces_dossier_additif_it';

    /** the column name for the detail_consultation_it field */
    const DETAIL_CONSULTATION_IT = 'consultation.detail_consultation_it';

    /** the column name for the add_echantillion_it field */
    const ADD_ECHANTILLION_IT = 'consultation.add_echantillion_it';

    /** the column name for the add_reunion_it field */
    const ADD_REUNION_IT = 'consultation.add_reunion_it';

    /** the column name for the codes_nuts field */
    const CODES_NUTS = 'consultation.codes_nuts';

    /** the column name for the date_decision field */
    const DATE_DECISION = 'consultation.date_decision';

    /** the column name for the intitule field */
    const INTITULE = 'consultation.intitule';

    /** the column name for the id_tr_intitule field */
    const ID_TR_INTITULE = 'consultation.id_tr_intitule';

    /** the column name for the objet field */
    const OBJET = 'consultation.objet';

    /** the column name for the id_tr_objet field */
    const ID_TR_OBJET = 'consultation.id_tr_objet';

    /** the column name for the type_acces field */
    const TYPE_ACCES = 'consultation.type_acces';

    /** the column name for the autoriser_reponse_electronique field */
    const AUTORISER_REPONSE_ELECTRONIQUE = 'consultation.autoriser_reponse_electronique';

    /** the column name for the regle_mise_en_ligne field */
    const REGLE_MISE_EN_LIGNE = 'consultation.regle_mise_en_ligne';

    /** the column name for the id_regle_validation field */
    const ID_REGLE_VALIDATION = 'consultation.id_regle_validation';

    /** the column name for the intitule_fr field */
    const INTITULE_FR = 'consultation.intitule_fr';

    /** the column name for the intitule_en field */
    const INTITULE_EN = 'consultation.intitule_en';

    /** the column name for the intitule_es field */
    const INTITULE_ES = 'consultation.intitule_es';

    /** the column name for the intitule_su field */
    const INTITULE_SU = 'consultation.intitule_su';

    /** the column name for the intitule_du field */
    const INTITULE_DU = 'consultation.intitule_du';

    /** the column name for the intitule_cz field */
    const INTITULE_CZ = 'consultation.intitule_cz';

    /** the column name for the intitule_ar field */
    const INTITULE_AR = 'consultation.intitule_ar';

    /** the column name for the intitule_it field */
    const INTITULE_IT = 'consultation.intitule_it';

    /** the column name for the objet_fr field */
    const OBJET_FR = 'consultation.objet_fr';

    /** the column name for the objet_en field */
    const OBJET_EN = 'consultation.objet_en';

    /** the column name for the objet_es field */
    const OBJET_ES = 'consultation.objet_es';

    /** the column name for the objet_su field */
    const OBJET_SU = 'consultation.objet_su';

    /** the column name for the objet_du field */
    const OBJET_DU = 'consultation.objet_du';

    /** the column name for the objet_cz field */
    const OBJET_CZ = 'consultation.objet_cz';

    /** the column name for the objet_ar field */
    const OBJET_AR = 'consultation.objet_ar';

    /** the column name for the objet_it field */
    const OBJET_IT = 'consultation.objet_it';

    /** the column name for the clause_sociale field */
    const CLAUSE_SOCIALE = 'consultation.clause_sociale';

    /** the column name for the clause_environnementale field */
    const CLAUSE_ENVIRONNEMENTALE = 'consultation.clause_environnementale';

    /** the column name for the reponse_obligatoire field */
    const REPONSE_OBLIGATOIRE = 'consultation.reponse_obligatoire';

    /** the column name for the type_envoi field */
    const TYPE_ENVOI = 'consultation.type_envoi';

    /** the column name for the chiffrement_offre field */
    const CHIFFREMENT_OFFRE = 'consultation.chiffrement_offre';

    /** the column name for the env_candidature field */
    const ENV_CANDIDATURE = 'consultation.env_candidature';

    /** the column name for the env_offre field */
    const ENV_OFFRE = 'consultation.env_offre';

    /** the column name for the env_anonymat field */
    const ENV_ANONYMAT = 'consultation.env_anonymat';

    /** the column name for the id_etat_consultation field */
    const ID_ETAT_CONSULTATION = 'consultation.id_etat_consultation';

    /** the column name for the reference_connecteur field */
    const REFERENCE_CONNECTEUR = 'consultation.reference_connecteur';

    /** the column name for the cons_statut field */
    const CONS_STATUT = 'consultation.cons_statut';

    /** the column name for the id_approbateur field */
    const ID_APPROBATEUR = 'consultation.id_approbateur';

    /** the column name for the id_valideur field */
    const ID_VALIDEUR = 'consultation.id_valideur';

    /** the column name for the old_service_validation field */
    const OLD_SERVICE_VALIDATION = 'consultation.old_service_validation';

    /** the column name for the id_createur field */
    const ID_CREATEUR = 'consultation.id_createur';

    /** the column name for the nom_createur field */
    const NOM_CREATEUR = 'consultation.nom_createur';

    /** the column name for the prenom_createur field */
    const PRENOM_CREATEUR = 'consultation.prenom_createur';

    /** the column name for the signature_acte_engagement field */
    const SIGNATURE_ACTE_ENGAGEMENT = 'consultation.signature_acte_engagement';

    /** the column name for the archiveMetaDescription field */
    const ARCHIVEMETADESCRIPTION = 'consultation.archiveMetaDescription';

    /** the column name for the archiveMetaMotsClef field */
    const ARCHIVEMETAMOTSCLEF = 'consultation.archiveMetaMotsClef';

    /** the column name for the archiveIdBlobZip field */
    const ARCHIVEIDBLOBZIP = 'consultation.archiveIdBlobZip';

    /** the column name for the decision_partielle field */
    const DECISION_PARTIELLE = 'consultation.decision_partielle';

    /** the column name for the type_decision_a_renseigner field */
    const TYPE_DECISION_A_RENSEIGNER = 'consultation.type_decision_a_renseigner';

    /** the column name for the type_decision_attribution_marche field */
    const TYPE_DECISION_ATTRIBUTION_MARCHE = 'consultation.type_decision_attribution_marche';

    /** the column name for the type_decision_declaration_sans_suite field */
    const TYPE_DECISION_DECLARATION_SANS_SUITE = 'consultation.type_decision_declaration_sans_suite';

    /** the column name for the type_decision_declaration_infructueux field */
    const TYPE_DECISION_DECLARATION_INFRUCTUEUX = 'consultation.type_decision_declaration_infructueux';

    /** the column name for the type_decision_selection_entreprise field */
    const TYPE_DECISION_SELECTION_ENTREPRISE = 'consultation.type_decision_selection_entreprise';

    /** the column name for the type_decision_attribution_accord_cadre field */
    const TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE = 'consultation.type_decision_attribution_accord_cadre';

    /** the column name for the type_decision_admission_sad field */
    const TYPE_DECISION_ADMISSION_SAD = 'consultation.type_decision_admission_sad';

    /** the column name for the type_decision_autre field */
    const TYPE_DECISION_AUTRE = 'consultation.type_decision_autre';

    /** the column name for the id_archiveur field */
    const ID_ARCHIVEUR = 'consultation.id_archiveur';

    /** the column name for the prenom_nom_agent_telechargement_plis field */
    const PRENOM_NOM_AGENT_TELECHARGEMENT_PLIS = 'consultation.prenom_nom_agent_telechargement_plis';

    /** the column name for the id_agent_telechargement_plis field */
    const ID_AGENT_TELECHARGEMENT_PLIS = 'consultation.id_agent_telechargement_plis';

    /** the column name for the path_telechargement_plis field */
    const PATH_TELECHARGEMENT_PLIS = 'consultation.path_telechargement_plis';

    /** the column name for the date_telechargement_plis field */
    const DATE_TELECHARGEMENT_PLIS = 'consultation.date_telechargement_plis';

    /** the column name for the old_service_validation_intermediaire field */
    const OLD_SERVICE_VALIDATION_INTERMEDIAIRE = 'consultation.old_service_validation_intermediaire';

    /** the column name for the env_offre_technique field */
    const ENV_OFFRE_TECHNIQUE = 'consultation.env_offre_technique';

    /** the column name for the ref_org_partenaire field */
    const REF_ORG_PARTENAIRE = 'consultation.ref_org_partenaire';

    /** the column name for the date_archivage field */
    const DATE_ARCHIVAGE = 'consultation.date_archivage';

    /** the column name for the date_decision_annulation field */
    const DATE_DECISION_ANNULATION = 'consultation.date_decision_annulation';

    /** the column name for the commentaire_annulation field */
    const COMMENTAIRE_ANNULATION = 'consultation.commentaire_annulation';

    /** the column name for the compte_boamp_associe field */
    const COMPTE_BOAMP_ASSOCIE = 'consultation.compte_boamp_associe';

    /** the column name for the date_mise_en_ligne_souhaitee field */
    const DATE_MISE_EN_LIGNE_SOUHAITEE = 'consultation.date_mise_en_ligne_souhaitee';

    /** the column name for the autoriser_publicite field */
    const AUTORISER_PUBLICITE = 'consultation.autoriser_publicite';

    /** the column name for the dossier_additif field */
    const DOSSIER_ADDITIF = 'consultation.dossier_additif';

    /** the column name for the type_marche field */
    const TYPE_MARCHE = 'consultation.type_marche';

    /** the column name for the type_prestation field */
    const TYPE_PRESTATION = 'consultation.type_prestation';

    /** the column name for the date_modification field */
    const DATE_MODIFICATION = 'consultation.date_modification';

    /** the column name for the delai_partiel field */
    const DELAI_PARTIEL = 'consultation.delai_partiel';

    /** the column name for the dateFinLocale field */
    const DATEFINLOCALE = 'consultation.dateFinLocale';

    /** the column name for the lieuResidence field */
    const LIEURESIDENCE = 'consultation.lieuResidence';

    /** the column name for the alerte field */
    const ALERTE = 'consultation.alerte';

    /** the column name for the doublon field */
    const DOUBLON = 'consultation.doublon';

    /** the column name for the denomination_adapte field */
    const DENOMINATION_ADAPTE = 'consultation.denomination_adapte';

    /** the column name for the url_consultation_avis_pub field */
    const URL_CONSULTATION_AVIS_PUB = 'consultation.url_consultation_avis_pub';

    /** the column name for the doublon_de field */
    const DOUBLON_DE = 'consultation.doublon_de';

    /** the column name for the entite_adjudicatrice field */
    const ENTITE_ADJUDICATRICE = 'consultation.entite_adjudicatrice';

    /** the column name for the code_operation field */
    const CODE_OPERATION = 'consultation.code_operation';

    /** the column name for the clause_sociale_condition_execution field */
    const CLAUSE_SOCIALE_CONDITION_EXECUTION = 'consultation.clause_sociale_condition_execution';

    /** the column name for the clause_sociale_insertion field */
    const CLAUSE_SOCIALE_INSERTION = 'consultation.clause_sociale_insertion';

    /** the column name for the clause_sociale_ateliers_proteges field */
    const CLAUSE_SOCIALE_ATELIERS_PROTEGES = 'consultation.clause_sociale_ateliers_proteges';

    /** the column name for the clause_sociale_siae field */
    const CLAUSE_SOCIALE_SIAE = 'consultation.clause_sociale_siae';

    /** the column name for the clause_sociale_ess field */
    const CLAUSE_SOCIALE_ESS = 'consultation.clause_sociale_ess';

    /** the column name for the clause_env_specs_techniques field */
    const CLAUSE_ENV_SPECS_TECHNIQUES = 'consultation.clause_env_specs_techniques';

    /** the column name for the clause_env_cond_execution field */
    const CLAUSE_ENV_COND_EXECUTION = 'consultation.clause_env_cond_execution';

    /** the column name for the clause_env_criteres_select field */
    const CLAUSE_ENV_CRITERES_SELECT = 'consultation.clause_env_criteres_select';

    /** the column name for the id_donnee_complementaire field */
    const ID_DONNEE_COMPLEMENTAIRE = 'consultation.id_donnee_complementaire';

    /** the column name for the donnee_complementaire_obligatoire field */
    const DONNEE_COMPLEMENTAIRE_OBLIGATOIRE = 'consultation.donnee_complementaire_obligatoire';

    /** the column name for the mode_ouverture_reponse field */
    const MODE_OUVERTURE_REPONSE = 'consultation.mode_ouverture_reponse';

    /** the column name for the id_fichier_annulation field */
    const ID_FICHIER_ANNULATION = 'consultation.id_fichier_annulation';

    /** the column name for the idOperation field */
    const IDOPERATION = 'consultation.idOperation';

    /** the column name for the etat_en_attente_validation field */
    const ETAT_EN_ATTENTE_VALIDATION = 'consultation.etat_en_attente_validation';

    /** the column name for the infos_blocs_atlas field */
    const INFOS_BLOCS_ATLAS = 'consultation.infos_blocs_atlas';

    /** the column name for the marche_public_simplifie field */
    const MARCHE_PUBLIC_SIMPLIFIE = 'consultation.marche_public_simplifie';

    /** the column name for the DATE_FIN_UNIX field */
    const DATE_FIN_UNIX = 'consultation.DATE_FIN_UNIX';

    /** the column name for the numero_AC field */
    const NUMERO_AC = 'consultation.numero_AC';

    /** the column name for the id_contrat field */
    const ID_CONTRAT = 'consultation.id_contrat';

    /** the column name for the pin_api_sgmap_mps field */
    const PIN_API_SGMAP_MPS = 'consultation.pin_api_sgmap_mps';

    /** the column name for the donnee_publicite_obligatoire field */
    const DONNEE_PUBLICITE_OBLIGATOIRE = 'consultation.donnee_publicite_obligatoire';

    /** the column name for the dume_demande field */
    const DUME_DEMANDE = 'consultation.dume_demande';

    /** the column name for the type_procedure_dume field */
    const TYPE_PROCEDURE_DUME = 'consultation.type_procedure_dume';

    /** the column name for the marche_insertion field */
    const MARCHE_INSERTION = 'consultation.marche_insertion';

    /** the column name for the clause_specification_technique field */
    const CLAUSE_SPECIFICATION_TECHNIQUE = 'consultation.clause_specification_technique';

    /** the column name for the type_formulaire_dume field */
    const TYPE_FORMULAIRE_DUME = 'consultation.type_formulaire_dume';

    /** the column name for the source_externe field */
    const SOURCE_EXTERNE = 'consultation.source_externe';

    /** the column name for the id_source_externe field */
    const ID_SOURCE_EXTERNE = 'consultation.id_source_externe';

    /** the column name for the id_dossier_volumineux field */
    const ID_DOSSIER_VOLUMINEUX = 'consultation.id_dossier_volumineux';

    /** the column name for the attestation_consultation field */
    const ATTESTATION_CONSULTATION = 'consultation.attestation_consultation';

    /** the column name for the version_messagerie field */
    const VERSION_MESSAGERIE = 'consultation.version_messagerie';

    /** the column name for the plateforme_virtuelle_id field */
    const PLATEFORME_VIRTUELLE_ID = 'consultation.plateforme_virtuelle_id';

    /** the column name for the is_envoi_publicite_validation field */
    const IS_ENVOI_PUBLICITE_VALIDATION = 'consultation.is_envoi_publicite_validation';

    /** the column name for the annexe_financiere field */
    const ANNEXE_FINANCIERE = 'consultation.annexe_financiere';

    /** the column name for the envol_activation field */
    const ENVOL_ACTIVATION = 'consultation.envol_activation';

    /** the column name for the service_id field */
    const SERVICE_ID = 'consultation.service_id';

    /** the column name for the service_associe_id field */
    const SERVICE_ASSOCIE_ID = 'consultation.service_associe_id';

    /** the column name for the groupement field */
    const GROUPEMENT = 'consultation.groupement';

    /** the column name for the service_validation field */
    const SERVICE_VALIDATION = 'consultation.service_validation';

    /** the column name for the autre_technique_achat field */
    const AUTRE_TECHNIQUE_ACHAT = 'consultation.autre_technique_achat';

    /** the column name for the procedure_ouverte field */
    const PROCEDURE_OUVERTE = 'consultation.procedure_ouverte';

    /** the column name for the service_validation_intermediaire field */
    const SERVICE_VALIDATION_INTERMEDIAIRE = 'consultation.service_validation_intermediaire';

    /** the column name for the cms_actif field */
    const CMS_ACTIF = 'consultation.cms_actif';

    /** the column name for the controle_taille_depot field */
    const CONTROLE_TAILLE_DEPOT = 'consultation.controle_taille_depot';

    /** the column name for the code_dce_restreint field */
    const CODE_DCE_RESTREINT = 'consultation.code_dce_restreint';

    /** the column name for the numero_projet_achat field */
    const NUMERO_PROJET_ACHAT = 'consultation.numero_projet_achat';

    /** the column name for the referentiel_achat_id field */
    const REFERENTIEL_ACHAT_ID = 'consultation.referentiel_achat_id';

    /** the column name for the agent_technique_createur field */
    const AGENT_TECHNIQUE_CREATEUR = 'consultation.agent_technique_createur';

    /** the column name for the uuid field */
    const UUID = 'consultation.uuid';

    /** the column name for the contrat_exec_uuid field */
    const CONTRAT_EXEC_UUID = 'consultation.contrat_exec_uuid';

    /** The enumerated values for the etat_approbation field */
    const ETAT_APPROBATION_0 = '0';
    const ETAT_APPROBATION_1 = '1';

    /** The enumerated values for the etat_validation field */
    const ETAT_VALIDATION_0 = '0';
    const ETAT_VALIDATION_1 = '1';

    /** The enumerated values for the publication_europe field */
    const PUBLICATION_EUROPE_0 = '0';
    const PUBLICATION_EUROPE_1 = '1';

    /** The enumerated values for the poursuivre_affichage_unite field */
    const POURSUIVRE_AFFICHAGE_UNITE_MINUTE = 'MINUTE';
    const POURSUIVRE_AFFICHAGE_UNITE_HOUR = 'HOUR';
    const POURSUIVRE_AFFICHAGE_UNITE_DAY = 'DAY';
    const POURSUIVRE_AFFICHAGE_UNITE_MONTH = 'MONTH';
    const POURSUIVRE_AFFICHAGE_UNITE_YEAR = 'YEAR';

    /** The enumerated values for the depouillable_phase_consultation field */
    const DEPOUILLABLE_PHASE_CONSULTATION_0 = '0';
    const DEPOUILLABLE_PHASE_CONSULTATION_1 = '1';

    /** The enumerated values for the consultation_transverse field */
    const CONSULTATION_TRANSVERSE_0 = '0';
    const CONSULTATION_TRANSVERSE_1 = '1';

    /** The enumerated values for the consultation_achat_publique field */
    const CONSULTATION_ACHAT_PUBLIQUE_0 = '0';
    const CONSULTATION_ACHAT_PUBLIQUE_1 = '1';

    /** The enumerated values for the accessibilite_fr field */
    const ACCESSIBILITE_FR_0 = '0';
    const ACCESSIBILITE_FR_1 = '1';

    /** The enumerated values for the accessibilite_cz field */
    const ACCESSIBILITE_CZ_0 = '0';
    const ACCESSIBILITE_CZ_1 = '1';

    /** The enumerated values for the accessibilite_du field */
    const ACCESSIBILITE_DU_0 = '0';
    const ACCESSIBILITE_DU_1 = '1';

    /** The enumerated values for the accessibilite_su field */
    const ACCESSIBILITE_SU_0 = '0';
    const ACCESSIBILITE_SU_1 = '1';

    /** The enumerated values for the accessibilite_ar field */
    const ACCESSIBILITE_AR_0 = '0';
    const ACCESSIBILITE_AR_1 = '1';

    /** The enumerated values for the alloti field */
    const ALLOTI_0 = '0';
    const ALLOTI_1 = '1';

    /** The enumerated values for the consultation_externe field */
    const CONSULTATION_EXTERNE_0 = '0';
    const CONSULTATION_EXTERNE_1 = '1';

    /** The enumerated values for the echantillon field */
    const ECHANTILLON_0 = '0';
    const ECHANTILLON_1 = '1';

    /** The enumerated values for the reunion field */
    const REUNION_0 = '0';
    const REUNION_1 = '1';

    /** The enumerated values for the visites_lieux field */
    const VISITES_LIEUX_0 = '0';
    const VISITES_LIEUX_1 = '1';

    /** The enumerated values for the variante_calcule field */
    const VARIANTE_CALCULE_0 = '0';
    const VARIANTE_CALCULE_1 = '1';

    /** The enumerated values for the consultation_annulee field */
    const CONSULTATION_ANNULEE_0 = '0';
    const CONSULTATION_ANNULEE_1 = '1';

    /** The enumerated values for the accessibilite_it field */
    const ACCESSIBILITE_IT_0 = '0';
    const ACCESSIBILITE_IT_1 = '1';

    /** The enumerated values for the clause_sociale field */
    const CLAUSE_SOCIALE_0 = '0';
    const CLAUSE_SOCIALE_1 = '1';
    const CLAUSE_SOCIALE_2 = '2';

    /** The enumerated values for the clause_environnementale field */
    const CLAUSE_ENVIRONNEMENTALE_0 = '0';
    const CLAUSE_ENVIRONNEMENTALE_1 = '1';
    const CLAUSE_ENVIRONNEMENTALE_2 = '2';

    /** The enumerated values for the signature_acte_engagement field */
    const SIGNATURE_ACTE_ENGAGEMENT_0 = '0';
    const SIGNATURE_ACTE_ENGAGEMENT_1 = '1';

    /** The enumerated values for the decision_partielle field */
    const DECISION_PARTIELLE_0 = '0';
    const DECISION_PARTIELLE_1 = '1';

    /** The enumerated values for the type_decision_a_renseigner field */
    const TYPE_DECISION_A_RENSEIGNER_0 = '0';
    const TYPE_DECISION_A_RENSEIGNER_1 = '1';

    /** The enumerated values for the type_decision_attribution_marche field */
    const TYPE_DECISION_ATTRIBUTION_MARCHE_0 = '0';
    const TYPE_DECISION_ATTRIBUTION_MARCHE_1 = '1';

    /** The enumerated values for the type_decision_declaration_sans_suite field */
    const TYPE_DECISION_DECLARATION_SANS_SUITE_0 = '0';
    const TYPE_DECISION_DECLARATION_SANS_SUITE_1 = '1';

    /** The enumerated values for the type_decision_declaration_infructueux field */
    const TYPE_DECISION_DECLARATION_INFRUCTUEUX_0 = '0';
    const TYPE_DECISION_DECLARATION_INFRUCTUEUX_1 = '1';

    /** The enumerated values for the type_decision_selection_entreprise field */
    const TYPE_DECISION_SELECTION_ENTREPRISE_0 = '0';
    const TYPE_DECISION_SELECTION_ENTREPRISE_1 = '1';

    /** The enumerated values for the type_decision_attribution_accord_cadre field */
    const TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE_0 = '0';
    const TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE_1 = '1';

    /** The enumerated values for the type_decision_admission_sad field */
    const TYPE_DECISION_ADMISSION_SAD_0 = '0';
    const TYPE_DECISION_ADMISSION_SAD_1 = '1';

    /** The enumerated values for the type_decision_autre field */
    const TYPE_DECISION_AUTRE_0 = '0';
    const TYPE_DECISION_AUTRE_1 = '1';

    /** The enumerated values for the dossier_additif field */
    const DOSSIER_ADDITIF_0 = '0';
    const DOSSIER_ADDITIF_1 = '1';

    /** The enumerated values for the delai_partiel field */
    const DELAI_PARTIEL_0 = '0';
    const DELAI_PARTIEL_1 = '1';

    /** The enumerated values for the alerte field */
    const ALERTE_0 = '0';
    const ALERTE_1 = '1';

    /** The enumerated values for the doublon field */
    const DOUBLON_0 = '0';
    const DOUBLON_1 = '1';

    /** The enumerated values for the entite_adjudicatrice field */
    const ENTITE_ADJUDICATRICE_0 = '0';
    const ENTITE_ADJUDICATRICE_1 = '1';

    /** The enumerated values for the donnee_complementaire_obligatoire field */
    const DONNEE_COMPLEMENTAIRE_OBLIGATOIRE_0 = '0';
    const DONNEE_COMPLEMENTAIRE_OBLIGATOIRE_1 = '1';

    /** The enumerated values for the mode_ouverture_reponse field */
    const MODE_OUVERTURE_REPONSE_0 = '0';
    const MODE_OUVERTURE_REPONSE_1 = '1';

    /** The enumerated values for the etat_en_attente_validation field */
    const ETAT_EN_ATTENTE_VALIDATION_0 = '0';
    const ETAT_EN_ATTENTE_VALIDATION_1 = '1';

    /** The enumerated values for the marche_public_simplifie field */
    const MARCHE_PUBLIC_SIMPLIFIE_0 = '0';
    const MARCHE_PUBLIC_SIMPLIFIE_1 = '1';

    /** The enumerated values for the donnee_publicite_obligatoire field */
    const DONNEE_PUBLICITE_OBLIGATOIRE_0 = '0';
    const DONNEE_PUBLICITE_OBLIGATOIRE_1 = '1';

    /** The enumerated values for the dume_demande field */
    const DUME_DEMANDE_0 = '0';
    const DUME_DEMANDE_1 = '1';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identity map to hold any loaded instances of CommonConsultation objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array CommonConsultation[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. CommonConsultationPeer::$fieldNames[CommonConsultationPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('Id', 'Reference', 'CodeExterne', 'Organisme', 'ReferenceUtilisateur', 'Categorie', 'Titre', 'Resume', 'Datedebut', 'Datefin', 'Datevalidation', 'TypeProcedure', 'CodeProcedure', 'ReponseElectronique', 'NumProcedure', 'IdTypeProcedure', 'IdTypeAvis', 'LieuExecution', 'TypeMiseEnLigne', 'Datemiseenligne', 'IsTiersAvis', 'Url', 'DatefinSad', 'IsSysAcqDyn', 'ReferenceConsultationInit', 'SignatureOffre', 'IdTypeValidation', 'EtatApprobation', 'EtatValidation', 'ChampSuppInvisible', 'CodeCpv1', 'CodeCpv2', 'PublicationEurope', 'EtatPublication', 'PoursuivreAffichage', 'PoursuivreAffichageUnite', 'NbrTelechargementDce', 'OldServiceId', 'OldServiceAssocieId', 'DetailConsultation', 'DateFinAffichage', 'DepouillablePhaseConsultation', 'ConsultationTransverse', 'ConsultationAchatPublique', 'UrlConsultationAchatPublique', 'PartialDceDownload', 'TiragePlan', 'TireurPlan', 'DateMiseEnLigneCalcule', 'AccessibiliteEn', 'AccessibiliteEs', 'NbrReponse', 'IdTypeProcedureOrg', 'OrganismeConsultationInit', 'TirageDescriptif', 'DateValidationIntermediaire', 'AccessibiliteFr', 'IdTrAccessibilite', 'AccessibiliteCz', 'AccessibiliteDu', 'AccessibiliteSu', 'AccessibiliteAr', 'Alloti', 'NumeroPhase', 'ConsultationExterne', 'UrlConsultationExterne', 'OrgDenomination', 'DomainesActivites', 'IdAffaire', 'AdresseRetraisDossiers', 'CautionProvisoire', 'AdresseDepotOffres', 'LieuOuverturePlis', 'PrixAquisitionPlans', 'Qualification', 'Agrements', 'AddEchantillion', 'DateLimiteEchantillion', 'AddReunion', 'DateReunion', 'Variantes', 'AdresseDepotOffresAr', 'LieuOuverturePlisAr', 'AdresseRetraisDossiersAr', 'PiecesDossierAdmin', 'PiecesDossierAdminFr', 'PiecesDossierAdminEn', 'PiecesDossierAdminEs', 'PiecesDossierAdminSu', 'PiecesDossierAdminDu', 'PiecesDossierAdminCz', 'PiecesDossierAdminAr', 'PiecesDossierTech', 'PiecesDossierTechFr', 'PiecesDossierTechEn', 'PiecesDossierTechEs', 'PiecesDossierTechSu', 'PiecesDossierTechDu', 'PiecesDossierTechCz', 'PiecesDossierTechAr', 'PiecesDossierAdditif', 'PiecesDossierAdditifFr', 'PiecesDossierAdditifEn', 'PiecesDossierAdditifEs', 'PiecesDossierAdditifSu', 'PiecesDossierAdditifDu', 'PiecesDossierAdditifCz', 'PiecesDossierAdditifAr', 'IdRpa', 'DetailConsultationFr', 'DetailConsultationEn', 'DetailConsultationEs', 'DetailConsultationSu', 'DetailConsultationDu', 'DetailConsultationCz', 'DetailConsultationAr', 'Echantillon', 'Reunion', 'VisitesLieux', 'VarianteCalcule', 'AdresseRetraisDossiersFr', 'AdresseRetraisDossiersEn', 'AdresseRetraisDossiersEs', 'AdresseRetraisDossiersSu', 'AdresseRetraisDossiersDu', 'AdresseRetraisDossiersCz', 'AdresseDepotOffresFr', 'AdresseDepotOffresEn', 'AdresseDepotOffresEs', 'AdresseDepotOffresSu', 'AdresseDepotOffresDu', 'AdresseDepotOffresCz', 'LieuOuverturePlisFr', 'LieuOuverturePlisEn', 'LieuOuverturePlisEs', 'LieuOuverturePlisSu', 'LieuOuverturePlisDu', 'LieuOuverturePlisCz', 'AddEchantillionFr', 'AddEchantillionEn', 'AddEchantillionEs', 'AddEchantillionSu', 'AddEchantillionDu', 'AddEchantillionCz', 'AddEchantillionAr', 'AddReunionFr', 'AddReunionEn', 'AddReunionEs', 'AddReunionSu', 'AddReunionDu', 'AddReunionCz', 'AddReunionAr', 'ModePassation', 'ConsultationAnnulee', 'AccessibiliteIt', 'AdresseDepotOffresIt', 'LieuOuverturePlisIt', 'AdresseRetraisDossiersIt', 'PiecesDossierAdminIt', 'PiecesDossierTechIt', 'PiecesDossierAdditifIt', 'DetailConsultationIt', 'AddEchantillionIt', 'AddReunionIt', 'CodesNuts', 'DateDecision', 'Intitule', 'IdTrIntitule', 'Objet', 'IdTrObjet', 'TypeAcces', 'AutoriserReponseElectronique', 'RegleMiseEnLigne', 'IdRegleValidation', 'IntituleFr', 'IntituleEn', 'IntituleEs', 'IntituleSu', 'IntituleDu', 'IntituleCz', 'IntituleAr', 'IntituleIt', 'ObjetFr', 'ObjetEn', 'ObjetEs', 'ObjetSu', 'ObjetDu', 'ObjetCz', 'ObjetAr', 'ObjetIt', 'ClauseSociale', 'ClauseEnvironnementale', 'ReponseObligatoire', 'TypeEnvoi', 'ChiffrementOffre', 'EnvCandidature', 'EnvOffre', 'EnvAnonymat', 'IdEtatConsultation', 'ReferenceConnecteur', 'ConsStatut', 'IdApprobateur', 'IdValideur', 'OldServiceValidation', 'IdCreateur', 'NomCreateur', 'PrenomCreateur', 'SignatureActeEngagement', 'Archivemetadescription', 'Archivemetamotsclef', 'Archiveidblobzip', 'DecisionPartielle', 'TypeDecisionARenseigner', 'TypeDecisionAttributionMarche', 'TypeDecisionDeclarationSansSuite', 'TypeDecisionDeclarationInfructueux', 'TypeDecisionSelectionEntreprise', 'TypeDecisionAttributionAccordCadre', 'TypeDecisionAdmissionSad', 'TypeDecisionAutre', 'IdArchiveur', 'PrenomNomAgentTelechargementPlis', 'IdAgentTelechargementPlis', 'PathTelechargementPlis', 'DateTelechargementPlis', 'OldServiceValidationIntermediaire', 'EnvOffreTechnique', 'RefOrgPartenaire', 'DateArchivage', 'DateDecisionAnnulation', 'CommentaireAnnulation', 'CompteBoampAssocie', 'DateMiseEnLigneSouhaitee', 'AutoriserPublicite', 'DossierAdditif', 'TypeMarche', 'TypePrestation', 'DateModification', 'DelaiPartiel', 'Datefinlocale', 'Lieuresidence', 'Alerte', 'Doublon', 'DenominationAdapte', 'UrlConsultationAvisPub', 'DoublonDe', 'EntiteAdjudicatrice', 'CodeOperation', 'ClauseSocialeConditionExecution', 'ClauseSocialeInsertion', 'ClauseSocialeAteliersProteges', 'ClauseSocialeSiae', 'ClauseSocialeEss', 'ClauseEnvSpecsTechniques', 'ClauseEnvCondExecution', 'ClauseEnvCriteresSelect', 'IdDonneeComplementaire', 'DonneeComplementaireObligatoire', 'ModeOuvertureReponse', 'IdFichierAnnulation', 'Idoperation', 'EtatEnAttenteValidation', 'InfosBlocsAtlas', 'MarchePublicSimplifie', 'DateFinUnix', 'NumeroAc', 'IdContrat', 'PinApiSgmapMps', 'DonneePubliciteObligatoire', 'DumeDemande', 'TypeProcedureDume', 'MarcheInsertion', 'ClauseSpecificationTechnique', 'TypeFormulaireDume', 'SourceExterne', 'IdSourceExterne', 'IdDossierVolumineux', 'AttestationConsultation', 'VersionMessagerie', 'PlateformeVirtuelleId', 'IsEnvoiPubliciteValidation', 'AnnexeFinanciere', 'EnvolActivation', 'ServiceId', 'ServiceAssocieId', 'Groupement', 'ServiceValidation', 'AutreTechniqueAchat', 'ProcedureOuverte', 'ServiceValidationIntermediaire', 'CmsActif', 'ControleTailleDepot', 'CodeDceRestreint', 'NumeroProjetAchat', 'ReferentielAchatId', 'AgentTechniqueCreateur', 'Uuid', 'ContratExecUuid', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id', 'reference', 'codeExterne', 'organisme', 'referenceUtilisateur', 'categorie', 'titre', 'resume', 'datedebut', 'datefin', 'datevalidation', 'typeProcedure', 'codeProcedure', 'reponseElectronique', 'numProcedure', 'idTypeProcedure', 'idTypeAvis', 'lieuExecution', 'typeMiseEnLigne', 'datemiseenligne', 'isTiersAvis', 'url', 'datefinSad', 'isSysAcqDyn', 'referenceConsultationInit', 'signatureOffre', 'idTypeValidation', 'etatApprobation', 'etatValidation', 'champSuppInvisible', 'codeCpv1', 'codeCpv2', 'publicationEurope', 'etatPublication', 'poursuivreAffichage', 'poursuivreAffichageUnite', 'nbrTelechargementDce', 'oldServiceId', 'oldServiceAssocieId', 'detailConsultation', 'dateFinAffichage', 'depouillablePhaseConsultation', 'consultationTransverse', 'consultationAchatPublique', 'urlConsultationAchatPublique', 'partialDceDownload', 'tiragePlan', 'tireurPlan', 'dateMiseEnLigneCalcule', 'accessibiliteEn', 'accessibiliteEs', 'nbrReponse', 'idTypeProcedureOrg', 'organismeConsultationInit', 'tirageDescriptif', 'dateValidationIntermediaire', 'accessibiliteFr', 'idTrAccessibilite', 'accessibiliteCz', 'accessibiliteDu', 'accessibiliteSu', 'accessibiliteAr', 'alloti', 'numeroPhase', 'consultationExterne', 'urlConsultationExterne', 'orgDenomination', 'domainesActivites', 'idAffaire', 'adresseRetraisDossiers', 'cautionProvisoire', 'adresseDepotOffres', 'lieuOuverturePlis', 'prixAquisitionPlans', 'qualification', 'agrements', 'addEchantillion', 'dateLimiteEchantillion', 'addReunion', 'dateReunion', 'variantes', 'adresseDepotOffresAr', 'lieuOuverturePlisAr', 'adresseRetraisDossiersAr', 'piecesDossierAdmin', 'piecesDossierAdminFr', 'piecesDossierAdminEn', 'piecesDossierAdminEs', 'piecesDossierAdminSu', 'piecesDossierAdminDu', 'piecesDossierAdminCz', 'piecesDossierAdminAr', 'piecesDossierTech', 'piecesDossierTechFr', 'piecesDossierTechEn', 'piecesDossierTechEs', 'piecesDossierTechSu', 'piecesDossierTechDu', 'piecesDossierTechCz', 'piecesDossierTechAr', 'piecesDossierAdditif', 'piecesDossierAdditifFr', 'piecesDossierAdditifEn', 'piecesDossierAdditifEs', 'piecesDossierAdditifSu', 'piecesDossierAdditifDu', 'piecesDossierAdditifCz', 'piecesDossierAdditifAr', 'idRpa', 'detailConsultationFr', 'detailConsultationEn', 'detailConsultationEs', 'detailConsultationSu', 'detailConsultationDu', 'detailConsultationCz', 'detailConsultationAr', 'echantillon', 'reunion', 'visitesLieux', 'varianteCalcule', 'adresseRetraisDossiersFr', 'adresseRetraisDossiersEn', 'adresseRetraisDossiersEs', 'adresseRetraisDossiersSu', 'adresseRetraisDossiersDu', 'adresseRetraisDossiersCz', 'adresseDepotOffresFr', 'adresseDepotOffresEn', 'adresseDepotOffresEs', 'adresseDepotOffresSu', 'adresseDepotOffresDu', 'adresseDepotOffresCz', 'lieuOuverturePlisFr', 'lieuOuverturePlisEn', 'lieuOuverturePlisEs', 'lieuOuverturePlisSu', 'lieuOuverturePlisDu', 'lieuOuverturePlisCz', 'addEchantillionFr', 'addEchantillionEn', 'addEchantillionEs', 'addEchantillionSu', 'addEchantillionDu', 'addEchantillionCz', 'addEchantillionAr', 'addReunionFr', 'addReunionEn', 'addReunionEs', 'addReunionSu', 'addReunionDu', 'addReunionCz', 'addReunionAr', 'modePassation', 'consultationAnnulee', 'accessibiliteIt', 'adresseDepotOffresIt', 'lieuOuverturePlisIt', 'adresseRetraisDossiersIt', 'piecesDossierAdminIt', 'piecesDossierTechIt', 'piecesDossierAdditifIt', 'detailConsultationIt', 'addEchantillionIt', 'addReunionIt', 'codesNuts', 'dateDecision', 'intitule', 'idTrIntitule', 'objet', 'idTrObjet', 'typeAcces', 'autoriserReponseElectronique', 'regleMiseEnLigne', 'idRegleValidation', 'intituleFr', 'intituleEn', 'intituleEs', 'intituleSu', 'intituleDu', 'intituleCz', 'intituleAr', 'intituleIt', 'objetFr', 'objetEn', 'objetEs', 'objetSu', 'objetDu', 'objetCz', 'objetAr', 'objetIt', 'clauseSociale', 'clauseEnvironnementale', 'reponseObligatoire', 'typeEnvoi', 'chiffrementOffre', 'envCandidature', 'envOffre', 'envAnonymat', 'idEtatConsultation', 'referenceConnecteur', 'consStatut', 'idApprobateur', 'idValideur', 'oldServiceValidation', 'idCreateur', 'nomCreateur', 'prenomCreateur', 'signatureActeEngagement', 'archivemetadescription', 'archivemetamotsclef', 'archiveidblobzip', 'decisionPartielle', 'typeDecisionARenseigner', 'typeDecisionAttributionMarche', 'typeDecisionDeclarationSansSuite', 'typeDecisionDeclarationInfructueux', 'typeDecisionSelectionEntreprise', 'typeDecisionAttributionAccordCadre', 'typeDecisionAdmissionSad', 'typeDecisionAutre', 'idArchiveur', 'prenomNomAgentTelechargementPlis', 'idAgentTelechargementPlis', 'pathTelechargementPlis', 'dateTelechargementPlis', 'oldServiceValidationIntermediaire', 'envOffreTechnique', 'refOrgPartenaire', 'dateArchivage', 'dateDecisionAnnulation', 'commentaireAnnulation', 'compteBoampAssocie', 'dateMiseEnLigneSouhaitee', 'autoriserPublicite', 'dossierAdditif', 'typeMarche', 'typePrestation', 'dateModification', 'delaiPartiel', 'datefinlocale', 'lieuresidence', 'alerte', 'doublon', 'denominationAdapte', 'urlConsultationAvisPub', 'doublonDe', 'entiteAdjudicatrice', 'codeOperation', 'clauseSocialeConditionExecution', 'clauseSocialeInsertion', 'clauseSocialeAteliersProteges', 'clauseSocialeSiae', 'clauseSocialeEss', 'clauseEnvSpecsTechniques', 'clauseEnvCondExecution', 'clauseEnvCriteresSelect', 'idDonneeComplementaire', 'donneeComplementaireObligatoire', 'modeOuvertureReponse', 'idFichierAnnulation', 'idoperation', 'etatEnAttenteValidation', 'infosBlocsAtlas', 'marchePublicSimplifie', 'dateFinUnix', 'numeroAc', 'idContrat', 'pinApiSgmapMps', 'donneePubliciteObligatoire', 'dumeDemande', 'typeProcedureDume', 'marcheInsertion', 'clauseSpecificationTechnique', 'typeFormulaireDume', 'sourceExterne', 'idSourceExterne', 'idDossierVolumineux', 'attestationConsultation', 'versionMessagerie', 'plateformeVirtuelleId', 'isEnvoiPubliciteValidation', 'annexeFinanciere', 'envolActivation', 'serviceId', 'serviceAssocieId', 'groupement', 'serviceValidation', 'autreTechniqueAchat', 'procedureOuverte', 'serviceValidationIntermediaire', 'cmsActif', 'controleTailleDepot', 'codeDceRestreint', 'numeroProjetAchat', 'referentielAchatId', 'agentTechniqueCreateur', 'uuid', 'contratExecUuid', ),
        BasePeer::TYPE_COLNAME => array (CommonConsultationPeer::ID, CommonConsultationPeer::REFERENCE, CommonConsultationPeer::CODE_EXTERNE, CommonConsultationPeer::ORGANISME, CommonConsultationPeer::REFERENCE_UTILISATEUR, CommonConsultationPeer::CATEGORIE, CommonConsultationPeer::TITRE, CommonConsultationPeer::RESUME, CommonConsultationPeer::DATEDEBUT, CommonConsultationPeer::DATEFIN, CommonConsultationPeer::DATEVALIDATION, CommonConsultationPeer::TYPE_PROCEDURE, CommonConsultationPeer::CODE_PROCEDURE, CommonConsultationPeer::REPONSE_ELECTRONIQUE, CommonConsultationPeer::NUM_PROCEDURE, CommonConsultationPeer::ID_TYPE_PROCEDURE, CommonConsultationPeer::ID_TYPE_AVIS, CommonConsultationPeer::LIEU_EXECUTION, CommonConsultationPeer::TYPE_MISE_EN_LIGNE, CommonConsultationPeer::DATEMISEENLIGNE, CommonConsultationPeer::IS_TIERS_AVIS, CommonConsultationPeer::URL, CommonConsultationPeer::DATEFIN_SAD, CommonConsultationPeer::IS_SYS_ACQ_DYN, CommonConsultationPeer::REFERENCE_CONSULTATION_INIT, CommonConsultationPeer::SIGNATURE_OFFRE, CommonConsultationPeer::ID_TYPE_VALIDATION, CommonConsultationPeer::ETAT_APPROBATION, CommonConsultationPeer::ETAT_VALIDATION, CommonConsultationPeer::CHAMP_SUPP_INVISIBLE, CommonConsultationPeer::CODE_CPV_1, CommonConsultationPeer::CODE_CPV_2, CommonConsultationPeer::PUBLICATION_EUROPE, CommonConsultationPeer::ETAT_PUBLICATION, CommonConsultationPeer::POURSUIVRE_AFFICHAGE, CommonConsultationPeer::POURSUIVRE_AFFICHAGE_UNITE, CommonConsultationPeer::NBR_TELECHARGEMENT_DCE, CommonConsultationPeer::OLD_SERVICE_ID, CommonConsultationPeer::OLD_SERVICE_ASSOCIE_ID, CommonConsultationPeer::DETAIL_CONSULTATION, CommonConsultationPeer::DATE_FIN_AFFICHAGE, CommonConsultationPeer::DEPOUILLABLE_PHASE_CONSULTATION, CommonConsultationPeer::CONSULTATION_TRANSVERSE, CommonConsultationPeer::CONSULTATION_ACHAT_PUBLIQUE, CommonConsultationPeer::URL_CONSULTATION_ACHAT_PUBLIQUE, CommonConsultationPeer::PARTIAL_DCE_DOWNLOAD, CommonConsultationPeer::TIRAGE_PLAN, CommonConsultationPeer::TIREUR_PLAN, CommonConsultationPeer::DATE_MISE_EN_LIGNE_CALCULE, CommonConsultationPeer::ACCESSIBILITE_EN, CommonConsultationPeer::ACCESSIBILITE_ES, CommonConsultationPeer::NBR_REPONSE, CommonConsultationPeer::ID_TYPE_PROCEDURE_ORG, CommonConsultationPeer::ORGANISME_CONSULTATION_INIT, CommonConsultationPeer::TIRAGE_DESCRIPTIF, CommonConsultationPeer::DATE_VALIDATION_INTERMEDIAIRE, CommonConsultationPeer::ACCESSIBILITE_FR, CommonConsultationPeer::ID_TR_ACCESSIBILITE, CommonConsultationPeer::ACCESSIBILITE_CZ, CommonConsultationPeer::ACCESSIBILITE_DU, CommonConsultationPeer::ACCESSIBILITE_SU, CommonConsultationPeer::ACCESSIBILITE_AR, CommonConsultationPeer::ALLOTI, CommonConsultationPeer::NUMERO_PHASE, CommonConsultationPeer::CONSULTATION_EXTERNE, CommonConsultationPeer::URL_CONSULTATION_EXTERNE, CommonConsultationPeer::ORG_DENOMINATION, CommonConsultationPeer::DOMAINES_ACTIVITES, CommonConsultationPeer::ID_AFFAIRE, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS, CommonConsultationPeer::CAUTION_PROVISOIRE, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES, CommonConsultationPeer::LIEU_OUVERTURE_PLIS, CommonConsultationPeer::PRIX_AQUISITION_PLANS, CommonConsultationPeer::QUALIFICATION, CommonConsultationPeer::AGREMENTS, CommonConsultationPeer::ADD_ECHANTILLION, CommonConsultationPeer::DATE_LIMITE_ECHANTILLION, CommonConsultationPeer::ADD_REUNION, CommonConsultationPeer::DATE_REUNION, CommonConsultationPeer::VARIANTES, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_AR, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_AR, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_AR, CommonConsultationPeer::PIECES_DOSSIER_ADMIN, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_FR, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_EN, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_ES, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_SU, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_DU, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_CZ, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_AR, CommonConsultationPeer::PIECES_DOSSIER_TECH, CommonConsultationPeer::PIECES_DOSSIER_TECH_FR, CommonConsultationPeer::PIECES_DOSSIER_TECH_EN, CommonConsultationPeer::PIECES_DOSSIER_TECH_ES, CommonConsultationPeer::PIECES_DOSSIER_TECH_SU, CommonConsultationPeer::PIECES_DOSSIER_TECH_DU, CommonConsultationPeer::PIECES_DOSSIER_TECH_CZ, CommonConsultationPeer::PIECES_DOSSIER_TECH_AR, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_FR, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_EN, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_ES, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_SU, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_DU, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_CZ, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_AR, CommonConsultationPeer::ID_RPA, CommonConsultationPeer::DETAIL_CONSULTATION_FR, CommonConsultationPeer::DETAIL_CONSULTATION_EN, CommonConsultationPeer::DETAIL_CONSULTATION_ES, CommonConsultationPeer::DETAIL_CONSULTATION_SU, CommonConsultationPeer::DETAIL_CONSULTATION_DU, CommonConsultationPeer::DETAIL_CONSULTATION_CZ, CommonConsultationPeer::DETAIL_CONSULTATION_AR, CommonConsultationPeer::ECHANTILLON, CommonConsultationPeer::REUNION, CommonConsultationPeer::VISITES_LIEUX, CommonConsultationPeer::VARIANTE_CALCULE, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_FR, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_EN, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_ES, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_SU, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_DU, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_CZ, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_FR, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_EN, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_ES, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_SU, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_DU, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_CZ, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_FR, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_EN, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_ES, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_SU, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_DU, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_CZ, CommonConsultationPeer::ADD_ECHANTILLION_FR, CommonConsultationPeer::ADD_ECHANTILLION_EN, CommonConsultationPeer::ADD_ECHANTILLION_ES, CommonConsultationPeer::ADD_ECHANTILLION_SU, CommonConsultationPeer::ADD_ECHANTILLION_DU, CommonConsultationPeer::ADD_ECHANTILLION_CZ, CommonConsultationPeer::ADD_ECHANTILLION_AR, CommonConsultationPeer::ADD_REUNION_FR, CommonConsultationPeer::ADD_REUNION_EN, CommonConsultationPeer::ADD_REUNION_ES, CommonConsultationPeer::ADD_REUNION_SU, CommonConsultationPeer::ADD_REUNION_DU, CommonConsultationPeer::ADD_REUNION_CZ, CommonConsultationPeer::ADD_REUNION_AR, CommonConsultationPeer::MODE_PASSATION, CommonConsultationPeer::CONSULTATION_ANNULEE, CommonConsultationPeer::ACCESSIBILITE_IT, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_IT, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_IT, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_IT, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_IT, CommonConsultationPeer::PIECES_DOSSIER_TECH_IT, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_IT, CommonConsultationPeer::DETAIL_CONSULTATION_IT, CommonConsultationPeer::ADD_ECHANTILLION_IT, CommonConsultationPeer::ADD_REUNION_IT, CommonConsultationPeer::CODES_NUTS, CommonConsultationPeer::DATE_DECISION, CommonConsultationPeer::INTITULE, CommonConsultationPeer::ID_TR_INTITULE, CommonConsultationPeer::OBJET, CommonConsultationPeer::ID_TR_OBJET, CommonConsultationPeer::TYPE_ACCES, CommonConsultationPeer::AUTORISER_REPONSE_ELECTRONIQUE, CommonConsultationPeer::REGLE_MISE_EN_LIGNE, CommonConsultationPeer::ID_REGLE_VALIDATION, CommonConsultationPeer::INTITULE_FR, CommonConsultationPeer::INTITULE_EN, CommonConsultationPeer::INTITULE_ES, CommonConsultationPeer::INTITULE_SU, CommonConsultationPeer::INTITULE_DU, CommonConsultationPeer::INTITULE_CZ, CommonConsultationPeer::INTITULE_AR, CommonConsultationPeer::INTITULE_IT, CommonConsultationPeer::OBJET_FR, CommonConsultationPeer::OBJET_EN, CommonConsultationPeer::OBJET_ES, CommonConsultationPeer::OBJET_SU, CommonConsultationPeer::OBJET_DU, CommonConsultationPeer::OBJET_CZ, CommonConsultationPeer::OBJET_AR, CommonConsultationPeer::OBJET_IT, CommonConsultationPeer::CLAUSE_SOCIALE, CommonConsultationPeer::CLAUSE_ENVIRONNEMENTALE, CommonConsultationPeer::REPONSE_OBLIGATOIRE, CommonConsultationPeer::TYPE_ENVOI, CommonConsultationPeer::CHIFFREMENT_OFFRE, CommonConsultationPeer::ENV_CANDIDATURE, CommonConsultationPeer::ENV_OFFRE, CommonConsultationPeer::ENV_ANONYMAT, CommonConsultationPeer::ID_ETAT_CONSULTATION, CommonConsultationPeer::REFERENCE_CONNECTEUR, CommonConsultationPeer::CONS_STATUT, CommonConsultationPeer::ID_APPROBATEUR, CommonConsultationPeer::ID_VALIDEUR, CommonConsultationPeer::OLD_SERVICE_VALIDATION, CommonConsultationPeer::ID_CREATEUR, CommonConsultationPeer::NOM_CREATEUR, CommonConsultationPeer::PRENOM_CREATEUR, CommonConsultationPeer::SIGNATURE_ACTE_ENGAGEMENT, CommonConsultationPeer::ARCHIVEMETADESCRIPTION, CommonConsultationPeer::ARCHIVEMETAMOTSCLEF, CommonConsultationPeer::ARCHIVEIDBLOBZIP, CommonConsultationPeer::DECISION_PARTIELLE, CommonConsultationPeer::TYPE_DECISION_A_RENSEIGNER, CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_MARCHE, CommonConsultationPeer::TYPE_DECISION_DECLARATION_SANS_SUITE, CommonConsultationPeer::TYPE_DECISION_DECLARATION_INFRUCTUEUX, CommonConsultationPeer::TYPE_DECISION_SELECTION_ENTREPRISE, CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE, CommonConsultationPeer::TYPE_DECISION_ADMISSION_SAD, CommonConsultationPeer::TYPE_DECISION_AUTRE, CommonConsultationPeer::ID_ARCHIVEUR, CommonConsultationPeer::PRENOM_NOM_AGENT_TELECHARGEMENT_PLIS, CommonConsultationPeer::ID_AGENT_TELECHARGEMENT_PLIS, CommonConsultationPeer::PATH_TELECHARGEMENT_PLIS, CommonConsultationPeer::DATE_TELECHARGEMENT_PLIS, CommonConsultationPeer::OLD_SERVICE_VALIDATION_INTERMEDIAIRE, CommonConsultationPeer::ENV_OFFRE_TECHNIQUE, CommonConsultationPeer::REF_ORG_PARTENAIRE, CommonConsultationPeer::DATE_ARCHIVAGE, CommonConsultationPeer::DATE_DECISION_ANNULATION, CommonConsultationPeer::COMMENTAIRE_ANNULATION, CommonConsultationPeer::COMPTE_BOAMP_ASSOCIE, CommonConsultationPeer::DATE_MISE_EN_LIGNE_SOUHAITEE, CommonConsultationPeer::AUTORISER_PUBLICITE, CommonConsultationPeer::DOSSIER_ADDITIF, CommonConsultationPeer::TYPE_MARCHE, CommonConsultationPeer::TYPE_PRESTATION, CommonConsultationPeer::DATE_MODIFICATION, CommonConsultationPeer::DELAI_PARTIEL, CommonConsultationPeer::DATEFINLOCALE, CommonConsultationPeer::LIEURESIDENCE, CommonConsultationPeer::ALERTE, CommonConsultationPeer::DOUBLON, CommonConsultationPeer::DENOMINATION_ADAPTE, CommonConsultationPeer::URL_CONSULTATION_AVIS_PUB, CommonConsultationPeer::DOUBLON_DE, CommonConsultationPeer::ENTITE_ADJUDICATRICE, CommonConsultationPeer::CODE_OPERATION, CommonConsultationPeer::CLAUSE_SOCIALE_CONDITION_EXECUTION, CommonConsultationPeer::CLAUSE_SOCIALE_INSERTION, CommonConsultationPeer::CLAUSE_SOCIALE_ATELIERS_PROTEGES, CommonConsultationPeer::CLAUSE_SOCIALE_SIAE, CommonConsultationPeer::CLAUSE_SOCIALE_ESS, CommonConsultationPeer::CLAUSE_ENV_SPECS_TECHNIQUES, CommonConsultationPeer::CLAUSE_ENV_COND_EXECUTION, CommonConsultationPeer::CLAUSE_ENV_CRITERES_SELECT, CommonConsultationPeer::ID_DONNEE_COMPLEMENTAIRE, CommonConsultationPeer::DONNEE_COMPLEMENTAIRE_OBLIGATOIRE, CommonConsultationPeer::MODE_OUVERTURE_REPONSE, CommonConsultationPeer::ID_FICHIER_ANNULATION, CommonConsultationPeer::IDOPERATION, CommonConsultationPeer::ETAT_EN_ATTENTE_VALIDATION, CommonConsultationPeer::INFOS_BLOCS_ATLAS, CommonConsultationPeer::MARCHE_PUBLIC_SIMPLIFIE, CommonConsultationPeer::DATE_FIN_UNIX, CommonConsultationPeer::NUMERO_AC, CommonConsultationPeer::ID_CONTRAT, CommonConsultationPeer::PIN_API_SGMAP_MPS, CommonConsultationPeer::DONNEE_PUBLICITE_OBLIGATOIRE, CommonConsultationPeer::DUME_DEMANDE, CommonConsultationPeer::TYPE_PROCEDURE_DUME, CommonConsultationPeer::MARCHE_INSERTION, CommonConsultationPeer::CLAUSE_SPECIFICATION_TECHNIQUE, CommonConsultationPeer::TYPE_FORMULAIRE_DUME, CommonConsultationPeer::SOURCE_EXTERNE, CommonConsultationPeer::ID_SOURCE_EXTERNE, CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonConsultationPeer::ATTESTATION_CONSULTATION, CommonConsultationPeer::VERSION_MESSAGERIE, CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonConsultationPeer::IS_ENVOI_PUBLICITE_VALIDATION, CommonConsultationPeer::ANNEXE_FINANCIERE, CommonConsultationPeer::ENVOL_ACTIVATION, CommonConsultationPeer::SERVICE_ID, CommonConsultationPeer::SERVICE_ASSOCIE_ID, CommonConsultationPeer::GROUPEMENT, CommonConsultationPeer::SERVICE_VALIDATION, CommonConsultationPeer::AUTRE_TECHNIQUE_ACHAT, CommonConsultationPeer::PROCEDURE_OUVERTE, CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonConsultationPeer::CMS_ACTIF, CommonConsultationPeer::CONTROLE_TAILLE_DEPOT, CommonConsultationPeer::CODE_DCE_RESTREINT, CommonConsultationPeer::NUMERO_PROJET_ACHAT, CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonConsultationPeer::UUID, CommonConsultationPeer::CONTRAT_EXEC_UUID, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID', 'REFERENCE', 'CODE_EXTERNE', 'ORGANISME', 'REFERENCE_UTILISATEUR', 'CATEGORIE', 'TITRE', 'RESUME', 'DATEDEBUT', 'DATEFIN', 'DATEVALIDATION', 'TYPE_PROCEDURE', 'CODE_PROCEDURE', 'REPONSE_ELECTRONIQUE', 'NUM_PROCEDURE', 'ID_TYPE_PROCEDURE', 'ID_TYPE_AVIS', 'LIEU_EXECUTION', 'TYPE_MISE_EN_LIGNE', 'DATEMISEENLIGNE', 'IS_TIERS_AVIS', 'URL', 'DATEFIN_SAD', 'IS_SYS_ACQ_DYN', 'REFERENCE_CONSULTATION_INIT', 'SIGNATURE_OFFRE', 'ID_TYPE_VALIDATION', 'ETAT_APPROBATION', 'ETAT_VALIDATION', 'CHAMP_SUPP_INVISIBLE', 'CODE_CPV_1', 'CODE_CPV_2', 'PUBLICATION_EUROPE', 'ETAT_PUBLICATION', 'POURSUIVRE_AFFICHAGE', 'POURSUIVRE_AFFICHAGE_UNITE', 'NBR_TELECHARGEMENT_DCE', 'OLD_SERVICE_ID', 'OLD_SERVICE_ASSOCIE_ID', 'DETAIL_CONSULTATION', 'DATE_FIN_AFFICHAGE', 'DEPOUILLABLE_PHASE_CONSULTATION', 'CONSULTATION_TRANSVERSE', 'CONSULTATION_ACHAT_PUBLIQUE', 'URL_CONSULTATION_ACHAT_PUBLIQUE', 'PARTIAL_DCE_DOWNLOAD', 'TIRAGE_PLAN', 'TIREUR_PLAN', 'DATE_MISE_EN_LIGNE_CALCULE', 'ACCESSIBILITE_EN', 'ACCESSIBILITE_ES', 'NBR_REPONSE', 'ID_TYPE_PROCEDURE_ORG', 'ORGANISME_CONSULTATION_INIT', 'TIRAGE_DESCRIPTIF', 'DATE_VALIDATION_INTERMEDIAIRE', 'ACCESSIBILITE_FR', 'ID_TR_ACCESSIBILITE', 'ACCESSIBILITE_CZ', 'ACCESSIBILITE_DU', 'ACCESSIBILITE_SU', 'ACCESSIBILITE_AR', 'ALLOTI', 'NUMERO_PHASE', 'CONSULTATION_EXTERNE', 'URL_CONSULTATION_EXTERNE', 'ORG_DENOMINATION', 'DOMAINES_ACTIVITES', 'ID_AFFAIRE', 'ADRESSE_RETRAIS_DOSSIERS', 'CAUTION_PROVISOIRE', 'ADRESSE_DEPOT_OFFRES', 'LIEU_OUVERTURE_PLIS', 'PRIX_AQUISITION_PLANS', 'QUALIFICATION', 'AGREMENTS', 'ADD_ECHANTILLION', 'DATE_LIMITE_ECHANTILLION', 'ADD_REUNION', 'DATE_REUNION', 'VARIANTES', 'ADRESSE_DEPOT_OFFRES_AR', 'LIEU_OUVERTURE_PLIS_AR', 'ADRESSE_RETRAIS_DOSSIERS_AR', 'PIECES_DOSSIER_ADMIN', 'PIECES_DOSSIER_ADMIN_FR', 'PIECES_DOSSIER_ADMIN_EN', 'PIECES_DOSSIER_ADMIN_ES', 'PIECES_DOSSIER_ADMIN_SU', 'PIECES_DOSSIER_ADMIN_DU', 'PIECES_DOSSIER_ADMIN_CZ', 'PIECES_DOSSIER_ADMIN_AR', 'PIECES_DOSSIER_TECH', 'PIECES_DOSSIER_TECH_FR', 'PIECES_DOSSIER_TECH_EN', 'PIECES_DOSSIER_TECH_ES', 'PIECES_DOSSIER_TECH_SU', 'PIECES_DOSSIER_TECH_DU', 'PIECES_DOSSIER_TECH_CZ', 'PIECES_DOSSIER_TECH_AR', 'PIECES_DOSSIER_ADDITIF', 'PIECES_DOSSIER_ADDITIF_FR', 'PIECES_DOSSIER_ADDITIF_EN', 'PIECES_DOSSIER_ADDITIF_ES', 'PIECES_DOSSIER_ADDITIF_SU', 'PIECES_DOSSIER_ADDITIF_DU', 'PIECES_DOSSIER_ADDITIF_CZ', 'PIECES_DOSSIER_ADDITIF_AR', 'ID_RPA', 'DETAIL_CONSULTATION_FR', 'DETAIL_CONSULTATION_EN', 'DETAIL_CONSULTATION_ES', 'DETAIL_CONSULTATION_SU', 'DETAIL_CONSULTATION_DU', 'DETAIL_CONSULTATION_CZ', 'DETAIL_CONSULTATION_AR', 'ECHANTILLON', 'REUNION', 'VISITES_LIEUX', 'VARIANTE_CALCULE', 'ADRESSE_RETRAIS_DOSSIERS_FR', 'ADRESSE_RETRAIS_DOSSIERS_EN', 'ADRESSE_RETRAIS_DOSSIERS_ES', 'ADRESSE_RETRAIS_DOSSIERS_SU', 'ADRESSE_RETRAIS_DOSSIERS_DU', 'ADRESSE_RETRAIS_DOSSIERS_CZ', 'ADRESSE_DEPOT_OFFRES_FR', 'ADRESSE_DEPOT_OFFRES_EN', 'ADRESSE_DEPOT_OFFRES_ES', 'ADRESSE_DEPOT_OFFRES_SU', 'ADRESSE_DEPOT_OFFRES_DU', 'ADRESSE_DEPOT_OFFRES_CZ', 'LIEU_OUVERTURE_PLIS_FR', 'LIEU_OUVERTURE_PLIS_EN', 'LIEU_OUVERTURE_PLIS_ES', 'LIEU_OUVERTURE_PLIS_SU', 'LIEU_OUVERTURE_PLIS_DU', 'LIEU_OUVERTURE_PLIS_CZ', 'ADD_ECHANTILLION_FR', 'ADD_ECHANTILLION_EN', 'ADD_ECHANTILLION_ES', 'ADD_ECHANTILLION_SU', 'ADD_ECHANTILLION_DU', 'ADD_ECHANTILLION_CZ', 'ADD_ECHANTILLION_AR', 'ADD_REUNION_FR', 'ADD_REUNION_EN', 'ADD_REUNION_ES', 'ADD_REUNION_SU', 'ADD_REUNION_DU', 'ADD_REUNION_CZ', 'ADD_REUNION_AR', 'MODE_PASSATION', 'CONSULTATION_ANNULEE', 'ACCESSIBILITE_IT', 'ADRESSE_DEPOT_OFFRES_IT', 'LIEU_OUVERTURE_PLIS_IT', 'ADRESSE_RETRAIS_DOSSIERS_IT', 'PIECES_DOSSIER_ADMIN_IT', 'PIECES_DOSSIER_TECH_IT', 'PIECES_DOSSIER_ADDITIF_IT', 'DETAIL_CONSULTATION_IT', 'ADD_ECHANTILLION_IT', 'ADD_REUNION_IT', 'CODES_NUTS', 'DATE_DECISION', 'INTITULE', 'ID_TR_INTITULE', 'OBJET', 'ID_TR_OBJET', 'TYPE_ACCES', 'AUTORISER_REPONSE_ELECTRONIQUE', 'REGLE_MISE_EN_LIGNE', 'ID_REGLE_VALIDATION', 'INTITULE_FR', 'INTITULE_EN', 'INTITULE_ES', 'INTITULE_SU', 'INTITULE_DU', 'INTITULE_CZ', 'INTITULE_AR', 'INTITULE_IT', 'OBJET_FR', 'OBJET_EN', 'OBJET_ES', 'OBJET_SU', 'OBJET_DU', 'OBJET_CZ', 'OBJET_AR', 'OBJET_IT', 'CLAUSE_SOCIALE', 'CLAUSE_ENVIRONNEMENTALE', 'REPONSE_OBLIGATOIRE', 'TYPE_ENVOI', 'CHIFFREMENT_OFFRE', 'ENV_CANDIDATURE', 'ENV_OFFRE', 'ENV_ANONYMAT', 'ID_ETAT_CONSULTATION', 'REFERENCE_CONNECTEUR', 'CONS_STATUT', 'ID_APPROBATEUR', 'ID_VALIDEUR', 'OLD_SERVICE_VALIDATION', 'ID_CREATEUR', 'NOM_CREATEUR', 'PRENOM_CREATEUR', 'SIGNATURE_ACTE_ENGAGEMENT', 'ARCHIVEMETADESCRIPTION', 'ARCHIVEMETAMOTSCLEF', 'ARCHIVEIDBLOBZIP', 'DECISION_PARTIELLE', 'TYPE_DECISION_A_RENSEIGNER', 'TYPE_DECISION_ATTRIBUTION_MARCHE', 'TYPE_DECISION_DECLARATION_SANS_SUITE', 'TYPE_DECISION_DECLARATION_INFRUCTUEUX', 'TYPE_DECISION_SELECTION_ENTREPRISE', 'TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE', 'TYPE_DECISION_ADMISSION_SAD', 'TYPE_DECISION_AUTRE', 'ID_ARCHIVEUR', 'PRENOM_NOM_AGENT_TELECHARGEMENT_PLIS', 'ID_AGENT_TELECHARGEMENT_PLIS', 'PATH_TELECHARGEMENT_PLIS', 'DATE_TELECHARGEMENT_PLIS', 'OLD_SERVICE_VALIDATION_INTERMEDIAIRE', 'ENV_OFFRE_TECHNIQUE', 'REF_ORG_PARTENAIRE', 'DATE_ARCHIVAGE', 'DATE_DECISION_ANNULATION', 'COMMENTAIRE_ANNULATION', 'COMPTE_BOAMP_ASSOCIE', 'DATE_MISE_EN_LIGNE_SOUHAITEE', 'AUTORISER_PUBLICITE', 'DOSSIER_ADDITIF', 'TYPE_MARCHE', 'TYPE_PRESTATION', 'DATE_MODIFICATION', 'DELAI_PARTIEL', 'DATEFINLOCALE', 'LIEURESIDENCE', 'ALERTE', 'DOUBLON', 'DENOMINATION_ADAPTE', 'URL_CONSULTATION_AVIS_PUB', 'DOUBLON_DE', 'ENTITE_ADJUDICATRICE', 'CODE_OPERATION', 'CLAUSE_SOCIALE_CONDITION_EXECUTION', 'CLAUSE_SOCIALE_INSERTION', 'CLAUSE_SOCIALE_ATELIERS_PROTEGES', 'CLAUSE_SOCIALE_SIAE', 'CLAUSE_SOCIALE_ESS', 'CLAUSE_ENV_SPECS_TECHNIQUES', 'CLAUSE_ENV_COND_EXECUTION', 'CLAUSE_ENV_CRITERES_SELECT', 'ID_DONNEE_COMPLEMENTAIRE', 'DONNEE_COMPLEMENTAIRE_OBLIGATOIRE', 'MODE_OUVERTURE_REPONSE', 'ID_FICHIER_ANNULATION', 'IDOPERATION', 'ETAT_EN_ATTENTE_VALIDATION', 'INFOS_BLOCS_ATLAS', 'MARCHE_PUBLIC_SIMPLIFIE', 'DATE_FIN_UNIX', 'NUMERO_AC', 'ID_CONTRAT', 'PIN_API_SGMAP_MPS', 'DONNEE_PUBLICITE_OBLIGATOIRE', 'DUME_DEMANDE', 'TYPE_PROCEDURE_DUME', 'MARCHE_INSERTION', 'CLAUSE_SPECIFICATION_TECHNIQUE', 'TYPE_FORMULAIRE_DUME', 'SOURCE_EXTERNE', 'ID_SOURCE_EXTERNE', 'ID_DOSSIER_VOLUMINEUX', 'ATTESTATION_CONSULTATION', 'VERSION_MESSAGERIE', 'PLATEFORME_VIRTUELLE_ID', 'IS_ENVOI_PUBLICITE_VALIDATION', 'ANNEXE_FINANCIERE', 'ENVOL_ACTIVATION', 'SERVICE_ID', 'SERVICE_ASSOCIE_ID', 'GROUPEMENT', 'SERVICE_VALIDATION', 'AUTRE_TECHNIQUE_ACHAT', 'PROCEDURE_OUVERTE', 'SERVICE_VALIDATION_INTERMEDIAIRE', 'CMS_ACTIF', 'CONTROLE_TAILLE_DEPOT', 'CODE_DCE_RESTREINT', 'NUMERO_PROJET_ACHAT', 'REFERENTIEL_ACHAT_ID', 'AGENT_TECHNIQUE_CREATEUR', 'UUID', 'CONTRAT_EXEC_UUID'),
        BasePeer::TYPE_FIELDNAME => array ('id', 'reference', 'code_externe', 'organisme', 'reference_utilisateur', 'categorie', 'titre', 'resume', 'datedebut', 'datefin', 'datevalidation', 'type_procedure', 'code_procedure', 'reponse_electronique', 'num_procedure', 'id_type_procedure', 'id_type_avis', 'lieu_execution', 'type_mise_en_ligne', 'datemiseenligne', 'is_tiers_avis', 'url', 'datefin_sad', 'is_sys_acq_dyn', 'reference_consultation_init', 'signature_offre', 'id_type_validation', 'etat_approbation', 'etat_validation', 'champ_supp_invisible', 'code_cpv_1', 'code_cpv_2', 'publication_europe', 'etat_publication', 'poursuivre_affichage', 'poursuivre_affichage_unite', 'nbr_telechargement_dce', 'old_service_id', 'old_service_associe_id', 'detail_consultation', 'date_fin_affichage', 'depouillable_phase_consultation', 'consultation_transverse', 'consultation_achat_publique', 'url_consultation_achat_publique', 'partial_dce_download', 'tirage_plan', 'tireur_plan', 'date_mise_en_ligne_calcule', 'accessibilite_en', 'accessibilite_es', 'nbr_reponse', 'id_type_procedure_org', 'organisme_consultation_init', 'tirage_descriptif', 'date_validation_intermediaire', 'accessibilite_fr', 'id_tr_accessibilite', 'accessibilite_cz', 'accessibilite_du', 'accessibilite_su', 'accessibilite_ar', 'alloti', 'numero_phase', 'consultation_externe', 'url_consultation_externe', 'org_denomination', 'domaines_activites', 'id_affaire', 'adresse_retrais_dossiers', 'caution_provisoire', 'adresse_depot_offres', 'lieu_ouverture_plis', 'prix_aquisition_plans', 'qualification', 'agrements', 'add_echantillion', 'date_limite_echantillion', 'add_reunion', 'date_reunion', 'variantes', 'adresse_depot_offres_ar', 'lieu_ouverture_plis_ar', 'adresse_retrais_dossiers_ar', 'pieces_dossier_admin', 'pieces_dossier_admin_fr', 'pieces_dossier_admin_en', 'pieces_dossier_admin_es', 'pieces_dossier_admin_su', 'pieces_dossier_admin_du', 'pieces_dossier_admin_cz', 'pieces_dossier_admin_ar', 'pieces_dossier_tech', 'pieces_dossier_tech_fr', 'pieces_dossier_tech_en', 'pieces_dossier_tech_es', 'pieces_dossier_tech_su', 'pieces_dossier_tech_du', 'pieces_dossier_tech_cz', 'pieces_dossier_tech_ar', 'pieces_dossier_additif', 'pieces_dossier_additif_fr', 'pieces_dossier_additif_en', 'pieces_dossier_additif_es', 'pieces_dossier_additif_su', 'pieces_dossier_additif_du', 'pieces_dossier_additif_cz', 'pieces_dossier_additif_ar', 'id_rpa', 'detail_consultation_fr', 'detail_consultation_en', 'detail_consultation_es', 'detail_consultation_su', 'detail_consultation_du', 'detail_consultation_cz', 'detail_consultation_ar', 'echantillon', 'reunion', 'visites_lieux', 'variante_calcule', 'adresse_retrais_dossiers_fr', 'adresse_retrais_dossiers_en', 'adresse_retrais_dossiers_es', 'adresse_retrais_dossiers_su', 'adresse_retrais_dossiers_du', 'adresse_retrais_dossiers_cz', 'adresse_depot_offres_fr', 'adresse_depot_offres_en', 'adresse_depot_offres_es', 'adresse_depot_offres_su', 'adresse_depot_offres_du', 'adresse_depot_offres_cz', 'lieu_ouverture_plis_fr', 'lieu_ouverture_plis_en', 'lieu_ouverture_plis_es', 'lieu_ouverture_plis_su', 'lieu_ouverture_plis_du', 'lieu_ouverture_plis_cz', 'add_echantillion_fr', 'add_echantillion_en', 'add_echantillion_es', 'add_echantillion_su', 'add_echantillion_du', 'add_echantillion_cz', 'add_echantillion_ar', 'add_reunion_fr', 'add_reunion_en', 'add_reunion_es', 'add_reunion_su', 'add_reunion_du', 'add_reunion_cz', 'add_reunion_ar', 'mode_passation', 'consultation_annulee', 'accessibilite_it', 'adresse_depot_offres_it', 'lieu_ouverture_plis_it', 'adresse_retrais_dossiers_it', 'pieces_dossier_admin_it', 'pieces_dossier_tech_it', 'pieces_dossier_additif_it', 'detail_consultation_it', 'add_echantillion_it', 'add_reunion_it', 'codes_nuts', 'date_decision', 'intitule', 'id_tr_intitule', 'objet', 'id_tr_objet', 'type_acces', 'autoriser_reponse_electronique', 'regle_mise_en_ligne', 'id_regle_validation', 'intitule_fr', 'intitule_en', 'intitule_es', 'intitule_su', 'intitule_du', 'intitule_cz', 'intitule_ar', 'intitule_it', 'objet_fr', 'objet_en', 'objet_es', 'objet_su', 'objet_du', 'objet_cz', 'objet_ar', 'objet_it', 'clause_sociale', 'clause_environnementale', 'reponse_obligatoire', 'type_envoi', 'chiffrement_offre', 'env_candidature', 'env_offre', 'env_anonymat', 'id_etat_consultation', 'reference_connecteur', 'cons_statut', 'id_approbateur', 'id_valideur', 'old_service_validation', 'id_createur', 'nom_createur', 'prenom_createur', 'signature_acte_engagement', 'archiveMetaDescription', 'archiveMetaMotsClef', 'archiveIdBlobZip', 'decision_partielle', 'type_decision_a_renseigner', 'type_decision_attribution_marche', 'type_decision_declaration_sans_suite', 'type_decision_declaration_infructueux', 'type_decision_selection_entreprise', 'type_decision_attribution_accord_cadre', 'type_decision_admission_sad', 'type_decision_autre', 'id_archiveur', 'prenom_nom_agent_telechargement_plis', 'id_agent_telechargement_plis', 'path_telechargement_plis', 'date_telechargement_plis', 'old_service_validation_intermediaire', 'env_offre_technique', 'ref_org_partenaire', 'date_archivage', 'date_decision_annulation', 'commentaire_annulation', 'compte_boamp_associe', 'date_mise_en_ligne_souhaitee', 'autoriser_publicite', 'dossier_additif', 'type_marche', 'type_prestation', 'date_modification', 'delai_partiel', 'dateFinLocale', 'lieuResidence', 'alerte', 'doublon', 'denomination_adapte', 'url_consultation_avis_pub', 'doublon_de', 'entite_adjudicatrice', 'code_operation', 'clause_sociale_condition_execution', 'clause_sociale_insertion', 'clause_sociale_ateliers_proteges', 'clause_sociale_siae', 'clause_sociale_ess', 'clause_env_specs_techniques', 'clause_env_cond_execution', 'clause_env_criteres_select', 'id_donnee_complementaire', 'donnee_complementaire_obligatoire', 'mode_ouverture_reponse', 'id_fichier_annulation', 'idOperation', 'etat_en_attente_validation', 'infos_blocs_atlas', 'marche_public_simplifie', 'DATE_FIN_UNIX', 'numero_AC', 'id_contrat', 'pin_api_sgmap_mps', 'donnee_publicite_obligatoire', 'dume_demande', 'type_procedure_dume', 'marche_insertion', 'clause_specification_technique', 'type_formulaire_dume', 'source_externe', 'id_source_externe', 'id_dossier_volumineux', 'attestation_consultation', 'version_messagerie', 'plateforme_virtuelle_id', 'is_envoi_publicite_validation', 'annexe_financiere', 'envol_activation', 'service_id', 'service_associe_id', 'groupement', 'service_validation', 'autre_technique_achat', 'procedure_ouverte', 'service_validation_intermediaire', 'cms_actif', 'controle_taille_depot', 'code_dce_restreint', 'numero_projet_achat', 'referentiel_achat_id', 'agent_technique_createur', 'uuid', 'contrat_exec_uuid'),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. CommonConsultationPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('Id' => 0, 'Reference' => 1, 'CodeExterne' => 2, 'Organisme' => 3, 'ReferenceUtilisateur' => 4, 'Categorie' => 5, 'Titre' => 6, 'Resume' => 7, 'Datedebut' => 8, 'Datefin' => 9, 'Datevalidation' => 10, 'TypeProcedure' => 11, 'CodeProcedure' => 12, 'ReponseElectronique' => 13, 'NumProcedure' => 14, 'IdTypeProcedure' => 15, 'IdTypeAvis' => 16, 'LieuExecution' => 17, 'TypeMiseEnLigne' => 18, 'Datemiseenligne' => 19, 'IsTiersAvis' => 20, 'Url' => 21, 'DatefinSad' => 22, 'IsSysAcqDyn' => 23, 'ReferenceConsultationInit' => 24, 'SignatureOffre' => 25, 'IdTypeValidation' => 26, 'EtatApprobation' => 27, 'EtatValidation' => 28, 'ChampSuppInvisible' => 29, 'CodeCpv1' => 30, 'CodeCpv2' => 31, 'PublicationEurope' => 32, 'EtatPublication' => 33, 'PoursuivreAffichage' => 34, 'PoursuivreAffichageUnite' => 35, 'NbrTelechargementDce' => 36, 'OldServiceId' => 37, 'OldServiceAssocieId' => 38, 'DetailConsultation' => 39, 'DateFinAffichage' => 40, 'DepouillablePhaseConsultation' => 41, 'ConsultationTransverse' => 42, 'ConsultationAchatPublique' => 43, 'UrlConsultationAchatPublique' => 44, 'PartialDceDownload' => 45, 'TiragePlan' => 46, 'TireurPlan' => 47, 'DateMiseEnLigneCalcule' => 48, 'AccessibiliteEn' => 49, 'AccessibiliteEs' => 50, 'NbrReponse' => 51, 'IdTypeProcedureOrg' => 52, 'OrganismeConsultationInit' => 53, 'TirageDescriptif' => 54, 'DateValidationIntermediaire' => 55, 'AccessibiliteFr' => 56, 'IdTrAccessibilite' => 57, 'AccessibiliteCz' => 58, 'AccessibiliteDu' => 59, 'AccessibiliteSu' => 60, 'AccessibiliteAr' => 61, 'Alloti' => 62, 'NumeroPhase' => 63, 'ConsultationExterne' => 64, 'UrlConsultationExterne' => 65, 'OrgDenomination' => 66, 'DomainesActivites' => 67, 'IdAffaire' => 68, 'AdresseRetraisDossiers' => 69, 'CautionProvisoire' => 70, 'AdresseDepotOffres' => 71, 'LieuOuverturePlis' => 72, 'PrixAquisitionPlans' => 73, 'Qualification' => 74, 'Agrements' => 75, 'AddEchantillion' => 76, 'DateLimiteEchantillion' => 77, 'AddReunion' => 78, 'DateReunion' => 79, 'Variantes' => 80, 'AdresseDepotOffresAr' => 81, 'LieuOuverturePlisAr' => 82, 'AdresseRetraisDossiersAr' => 83, 'PiecesDossierAdmin' => 84, 'PiecesDossierAdminFr' => 85, 'PiecesDossierAdminEn' => 86, 'PiecesDossierAdminEs' => 87, 'PiecesDossierAdminSu' => 88, 'PiecesDossierAdminDu' => 89, 'PiecesDossierAdminCz' => 90, 'PiecesDossierAdminAr' => 91, 'PiecesDossierTech' => 92, 'PiecesDossierTechFr' => 93, 'PiecesDossierTechEn' => 94, 'PiecesDossierTechEs' => 95, 'PiecesDossierTechSu' => 96, 'PiecesDossierTechDu' => 97, 'PiecesDossierTechCz' => 98, 'PiecesDossierTechAr' => 99, 'PiecesDossierAdditif' => 100, 'PiecesDossierAdditifFr' => 101, 'PiecesDossierAdditifEn' => 102, 'PiecesDossierAdditifEs' => 103, 'PiecesDossierAdditifSu' => 104, 'PiecesDossierAdditifDu' => 105, 'PiecesDossierAdditifCz' => 106, 'PiecesDossierAdditifAr' => 107, 'IdRpa' => 108, 'DetailConsultationFr' => 109, 'DetailConsultationEn' => 110, 'DetailConsultationEs' => 111, 'DetailConsultationSu' => 112, 'DetailConsultationDu' => 113, 'DetailConsultationCz' => 114, 'DetailConsultationAr' => 115, 'Echantillon' => 116, 'Reunion' => 117, 'VisitesLieux' => 118, 'VarianteCalcule' => 119, 'AdresseRetraisDossiersFr' => 120, 'AdresseRetraisDossiersEn' => 121, 'AdresseRetraisDossiersEs' => 122, 'AdresseRetraisDossiersSu' => 123, 'AdresseRetraisDossiersDu' => 124, 'AdresseRetraisDossiersCz' => 125, 'AdresseDepotOffresFr' => 126, 'AdresseDepotOffresEn' => 127, 'AdresseDepotOffresEs' => 128, 'AdresseDepotOffresSu' => 129, 'AdresseDepotOffresDu' => 130, 'AdresseDepotOffresCz' => 131, 'LieuOuverturePlisFr' => 132, 'LieuOuverturePlisEn' => 133, 'LieuOuverturePlisEs' => 134, 'LieuOuverturePlisSu' => 135, 'LieuOuverturePlisDu' => 136, 'LieuOuverturePlisCz' => 137, 'AddEchantillionFr' => 138, 'AddEchantillionEn' => 139, 'AddEchantillionEs' => 140, 'AddEchantillionSu' => 141, 'AddEchantillionDu' => 142, 'AddEchantillionCz' => 143, 'AddEchantillionAr' => 144, 'AddReunionFr' => 145, 'AddReunionEn' => 146, 'AddReunionEs' => 147, 'AddReunionSu' => 148, 'AddReunionDu' => 149, 'AddReunionCz' => 150, 'AddReunionAr' => 151, 'ModePassation' => 152, 'ConsultationAnnulee' => 153, 'AccessibiliteIt' => 154, 'AdresseDepotOffresIt' => 155, 'LieuOuverturePlisIt' => 156, 'AdresseRetraisDossiersIt' => 157, 'PiecesDossierAdminIt' => 158, 'PiecesDossierTechIt' => 159, 'PiecesDossierAdditifIt' => 160, 'DetailConsultationIt' => 161, 'AddEchantillionIt' => 162, 'AddReunionIt' => 163, 'CodesNuts' => 164, 'DateDecision' => 165, 'Intitule' => 166, 'IdTrIntitule' => 167, 'Objet' => 168, 'IdTrObjet' => 169, 'TypeAcces' => 170, 'AutoriserReponseElectronique' => 171, 'RegleMiseEnLigne' => 172, 'IdRegleValidation' => 173, 'IntituleFr' => 174, 'IntituleEn' => 175, 'IntituleEs' => 176, 'IntituleSu' => 177, 'IntituleDu' => 178, 'IntituleCz' => 179, 'IntituleAr' => 180, 'IntituleIt' => 181, 'ObjetFr' => 182, 'ObjetEn' => 183, 'ObjetEs' => 184, 'ObjetSu' => 185, 'ObjetDu' => 186, 'ObjetCz' => 187, 'ObjetAr' => 188, 'ObjetIt' => 189, 'ClauseSociale' => 190, 'ClauseEnvironnementale' => 191, 'ReponseObligatoire' => 192, 'TypeEnvoi' => 193, 'ChiffrementOffre' => 194, 'EnvCandidature' => 195, 'EnvOffre' => 196, 'EnvAnonymat' => 197, 'IdEtatConsultation' => 198, 'ReferenceConnecteur' => 199, 'ConsStatut' => 200, 'IdApprobateur' => 201, 'IdValideur' => 202, 'OldServiceValidation' => 203, 'IdCreateur' => 204, 'NomCreateur' => 205, 'PrenomCreateur' => 206, 'SignatureActeEngagement' => 207, 'Archivemetadescription' => 208, 'Archivemetamotsclef' => 209, 'Archiveidblobzip' => 210, 'DecisionPartielle' => 211, 'TypeDecisionARenseigner' => 212, 'TypeDecisionAttributionMarche' => 213, 'TypeDecisionDeclarationSansSuite' => 214, 'TypeDecisionDeclarationInfructueux' => 215, 'TypeDecisionSelectionEntreprise' => 216, 'TypeDecisionAttributionAccordCadre' => 217, 'TypeDecisionAdmissionSad' => 218, 'TypeDecisionAutre' => 219, 'IdArchiveur' => 220, 'PrenomNomAgentTelechargementPlis' => 221, 'IdAgentTelechargementPlis' => 222, 'PathTelechargementPlis' => 223, 'DateTelechargementPlis' => 224, 'OldServiceValidationIntermediaire' => 225, 'EnvOffreTechnique' => 226, 'RefOrgPartenaire' => 227, 'DateArchivage' => 228, 'DateDecisionAnnulation' => 229, 'CommentaireAnnulation' => 230, 'CompteBoampAssocie' => 231, 'DateMiseEnLigneSouhaitee' => 232, 'AutoriserPublicite' => 233, 'DossierAdditif' => 234, 'TypeMarche' => 235, 'TypePrestation' => 236, 'DateModification' => 237, 'DelaiPartiel' => 238, 'Datefinlocale' => 239, 'Lieuresidence' => 240, 'Alerte' => 241, 'Doublon' => 242, 'DenominationAdapte' => 243, 'UrlConsultationAvisPub' => 244, 'DoublonDe' => 245, 'EntiteAdjudicatrice' => 246, 'CodeOperation' => 247, 'ClauseSocialeConditionExecution' => 248, 'ClauseSocialeInsertion' => 249, 'ClauseSocialeAteliersProteges' => 250, 'ClauseSocialeSiae' => 251, 'ClauseSocialeEss' => 252, 'ClauseEnvSpecsTechniques' => 253, 'ClauseEnvCondExecution' => 254, 'ClauseEnvCriteresSelect' => 255, 'IdDonneeComplementaire' => 256, 'DonneeComplementaireObligatoire' => 257, 'ModeOuvertureReponse' => 258, 'IdFichierAnnulation' => 259, 'Idoperation' => 260, 'EtatEnAttenteValidation' => 261, 'InfosBlocsAtlas' => 262, 'MarchePublicSimplifie' => 263, 'DateFinUnix' => 264, 'NumeroAc' => 265, 'IdContrat' => 266, 'PinApiSgmapMps' => 267, 'DonneePubliciteObligatoire' => 268, 'DumeDemande' => 269, 'TypeProcedureDume' => 270, 'MarcheInsertion' => 271, 'ClauseSpecificationTechnique' => 272, 'TypeFormulaireDume' => 273, 'SourceExterne' => 274, 'IdSourceExterne' => 275, 'IdDossierVolumineux' => 276, 'AttestationConsultation' => 277, 'VersionMessagerie' => 278, 'PlateformeVirtuelleId' => 279, 'IsEnvoiPubliciteValidation' => 280, 'AnnexeFinanciere' => 281, 'EnvolActivation' => 282, 'ServiceId' => 283, 'ServiceAssocieId' => 284, 'Groupement' => 285, 'ServiceValidation' => 286, 'AutreTechniqueAchat' => 287, 'ProcedureOuverte' => 288, 'ServiceValidationIntermediaire' => 289, 'CmsActif' => 290, 'ControleTailleDepot' => 291, 'CodeDceRestreint' => 292, 'NumeroProjetAchat' => 293, 'ReferentielAchatId' => 294, 'AgentTechniqueCreateur' => 295, 'Uuid' => 296, 'ContratExecUuid' => 297, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('id' => 0, 'reference' => 1, 'codeExterne' => 2, 'organisme' => 3, 'referenceUtilisateur' => 4, 'categorie' => 5, 'titre' => 6, 'resume' => 7, 'datedebut' => 8, 'datefin' => 9, 'datevalidation' => 10, 'typeProcedure' => 11, 'codeProcedure' => 12, 'reponseElectronique' => 13, 'numProcedure' => 14, 'idTypeProcedure' => 15, 'idTypeAvis' => 16, 'lieuExecution' => 17, 'typeMiseEnLigne' => 18, 'datemiseenligne' => 19, 'isTiersAvis' => 20, 'url' => 21, 'datefinSad' => 22, 'isSysAcqDyn' => 23, 'referenceConsultationInit' => 24, 'signatureOffre' => 25, 'idTypeValidation' => 26, 'etatApprobation' => 27, 'etatValidation' => 28, 'champSuppInvisible' => 29, 'codeCpv1' => 30, 'codeCpv2' => 31, 'publicationEurope' => 32, 'etatPublication' => 33, 'poursuivreAffichage' => 34, 'poursuivreAffichageUnite' => 35, 'nbrTelechargementDce' => 36, 'oldServiceId' => 37, 'oldServiceAssocieId' => 38, 'detailConsultation' => 39, 'dateFinAffichage' => 40, 'depouillablePhaseConsultation' => 41, 'consultationTransverse' => 42, 'consultationAchatPublique' => 43, 'urlConsultationAchatPublique' => 44, 'partialDceDownload' => 45, 'tiragePlan' => 46, 'tireurPlan' => 47, 'dateMiseEnLigneCalcule' => 48, 'accessibiliteEn' => 49, 'accessibiliteEs' => 50, 'nbrReponse' => 51, 'idTypeProcedureOrg' => 52, 'organismeConsultationInit' => 53, 'tirageDescriptif' => 54, 'dateValidationIntermediaire' => 55, 'accessibiliteFr' => 56, 'idTrAccessibilite' => 57, 'accessibiliteCz' => 58, 'accessibiliteDu' => 59, 'accessibiliteSu' => 60, 'accessibiliteAr' => 61, 'alloti' => 62, 'numeroPhase' => 63, 'consultationExterne' => 64, 'urlConsultationExterne' => 65, 'orgDenomination' => 66, 'domainesActivites' => 67, 'idAffaire' => 68, 'adresseRetraisDossiers' => 69, 'cautionProvisoire' => 70, 'adresseDepotOffres' => 71, 'lieuOuverturePlis' => 72, 'prixAquisitionPlans' => 73, 'qualification' => 74, 'agrements' => 75, 'addEchantillion' => 76, 'dateLimiteEchantillion' => 77, 'addReunion' => 78, 'dateReunion' => 79, 'variantes' => 80, 'adresseDepotOffresAr' => 81, 'lieuOuverturePlisAr' => 82, 'adresseRetraisDossiersAr' => 83, 'piecesDossierAdmin' => 84, 'piecesDossierAdminFr' => 85, 'piecesDossierAdminEn' => 86, 'piecesDossierAdminEs' => 87, 'piecesDossierAdminSu' => 88, 'piecesDossierAdminDu' => 89, 'piecesDossierAdminCz' => 90, 'piecesDossierAdminAr' => 91, 'piecesDossierTech' => 92, 'piecesDossierTechFr' => 93, 'piecesDossierTechEn' => 94, 'piecesDossierTechEs' => 95, 'piecesDossierTechSu' => 96, 'piecesDossierTechDu' => 97, 'piecesDossierTechCz' => 98, 'piecesDossierTechAr' => 99, 'piecesDossierAdditif' => 100, 'piecesDossierAdditifFr' => 101, 'piecesDossierAdditifEn' => 102, 'piecesDossierAdditifEs' => 103, 'piecesDossierAdditifSu' => 104, 'piecesDossierAdditifDu' => 105, 'piecesDossierAdditifCz' => 106, 'piecesDossierAdditifAr' => 107, 'idRpa' => 108, 'detailConsultationFr' => 109, 'detailConsultationEn' => 110, 'detailConsultationEs' => 111, 'detailConsultationSu' => 112, 'detailConsultationDu' => 113, 'detailConsultationCz' => 114, 'detailConsultationAr' => 115, 'echantillon' => 116, 'reunion' => 117, 'visitesLieux' => 118, 'varianteCalcule' => 119, 'adresseRetraisDossiersFr' => 120, 'adresseRetraisDossiersEn' => 121, 'adresseRetraisDossiersEs' => 122, 'adresseRetraisDossiersSu' => 123, 'adresseRetraisDossiersDu' => 124, 'adresseRetraisDossiersCz' => 125, 'adresseDepotOffresFr' => 126, 'adresseDepotOffresEn' => 127, 'adresseDepotOffresEs' => 128, 'adresseDepotOffresSu' => 129, 'adresseDepotOffresDu' => 130, 'adresseDepotOffresCz' => 131, 'lieuOuverturePlisFr' => 132, 'lieuOuverturePlisEn' => 133, 'lieuOuverturePlisEs' => 134, 'lieuOuverturePlisSu' => 135, 'lieuOuverturePlisDu' => 136, 'lieuOuverturePlisCz' => 137, 'addEchantillionFr' => 138, 'addEchantillionEn' => 139, 'addEchantillionEs' => 140, 'addEchantillionSu' => 141, 'addEchantillionDu' => 142, 'addEchantillionCz' => 143, 'addEchantillionAr' => 144, 'addReunionFr' => 145, 'addReunionEn' => 146, 'addReunionEs' => 147, 'addReunionSu' => 148, 'addReunionDu' => 149, 'addReunionCz' => 150, 'addReunionAr' => 151, 'modePassation' => 152, 'consultationAnnulee' => 153, 'accessibiliteIt' => 154, 'adresseDepotOffresIt' => 155, 'lieuOuverturePlisIt' => 156, 'adresseRetraisDossiersIt' => 157, 'piecesDossierAdminIt' => 158, 'piecesDossierTechIt' => 159, 'piecesDossierAdditifIt' => 160, 'detailConsultationIt' => 161, 'addEchantillionIt' => 162, 'addReunionIt' => 163, 'codesNuts' => 164, 'dateDecision' => 165, 'intitule' => 166, 'idTrIntitule' => 167, 'objet' => 168, 'idTrObjet' => 169, 'typeAcces' => 170, 'autoriserReponseElectronique' => 171, 'regleMiseEnLigne' => 172, 'idRegleValidation' => 173, 'intituleFr' => 174, 'intituleEn' => 175, 'intituleEs' => 176, 'intituleSu' => 177, 'intituleDu' => 178, 'intituleCz' => 179, 'intituleAr' => 180, 'intituleIt' => 181, 'objetFr' => 182, 'objetEn' => 183, 'objetEs' => 184, 'objetSu' => 185, 'objetDu' => 186, 'objetCz' => 187, 'objetAr' => 188, 'objetIt' => 189, 'clauseSociale' => 190, 'clauseEnvironnementale' => 191, 'reponseObligatoire' => 192, 'typeEnvoi' => 193, 'chiffrementOffre' => 194, 'envCandidature' => 195, 'envOffre' => 196, 'envAnonymat' => 197, 'idEtatConsultation' => 198, 'referenceConnecteur' => 199, 'consStatut' => 200, 'idApprobateur' => 201, 'idValideur' => 202, 'oldServiceValidation' => 203, 'idCreateur' => 204, 'nomCreateur' => 205, 'prenomCreateur' => 206, 'signatureActeEngagement' => 207, 'archivemetadescription' => 208, 'archivemetamotsclef' => 209, 'archiveidblobzip' => 210, 'decisionPartielle' => 211, 'typeDecisionARenseigner' => 212, 'typeDecisionAttributionMarche' => 213, 'typeDecisionDeclarationSansSuite' => 214, 'typeDecisionDeclarationInfructueux' => 215, 'typeDecisionSelectionEntreprise' => 216, 'typeDecisionAttributionAccordCadre' => 217, 'typeDecisionAdmissionSad' => 218, 'typeDecisionAutre' => 219, 'idArchiveur' => 220, 'prenomNomAgentTelechargementPlis' => 221, 'idAgentTelechargementPlis' => 222, 'pathTelechargementPlis' => 223, 'dateTelechargementPlis' => 224, 'oldServiceValidationIntermediaire' => 225, 'envOffreTechnique' => 226, 'refOrgPartenaire' => 227, 'dateArchivage' => 228, 'dateDecisionAnnulation' => 229, 'commentaireAnnulation' => 230, 'compteBoampAssocie' => 231, 'dateMiseEnLigneSouhaitee' => 232, 'autoriserPublicite' => 233, 'dossierAdditif' => 234, 'typeMarche' => 235, 'typePrestation' => 236, 'dateModification' => 237, 'delaiPartiel' => 238, 'datefinlocale' => 239, 'lieuresidence' => 240, 'alerte' => 241, 'doublon' => 242, 'denominationAdapte' => 243, 'urlConsultationAvisPub' => 244, 'doublonDe' => 245, 'entiteAdjudicatrice' => 246, 'codeOperation' => 247, 'clauseSocialeConditionExecution' => 248, 'clauseSocialeInsertion' => 249, 'clauseSocialeAteliersProteges' => 250, 'clauseSocialeSiae' => 251, 'clauseSocialeEss' => 252, 'clauseEnvSpecsTechniques' => 253, 'clauseEnvCondExecution' => 254, 'clauseEnvCriteresSelect' => 255, 'idDonneeComplementaire' => 256, 'donneeComplementaireObligatoire' => 257, 'modeOuvertureReponse' => 258, 'idFichierAnnulation' => 259, 'idoperation' => 260, 'etatEnAttenteValidation' => 261, 'infosBlocsAtlas' => 262, 'marchePublicSimplifie' => 263, 'dateFinUnix' => 264, 'numeroAc' => 265, 'idContrat' => 266, 'pinApiSgmapMps' => 267, 'donneePubliciteObligatoire' => 268, 'dumeDemande' => 269, 'typeProcedureDume' => 270, 'marcheInsertion' => 271, 'clauseSpecificationTechnique' => 272, 'typeFormulaireDume' => 273, 'sourceExterne' => 274, 'idSourceExterne' => 275, 'idDossierVolumineux' => 276, 'attestationConsultation' => 277, 'versionMessagerie' => 278, 'plateformeVirtuelleId' => 279, 'isEnvoiPubliciteValidation' => 280, 'annexeFinanciere' => 281, 'envolActivation' => 282, 'serviceId' => 283, 'serviceAssocieId' => 284, 'groupement' => 285, 'serviceValidation' => 286, 'autreTechniqueAchat' => 287, 'procedureOuverte' => 288, 'serviceValidationIntermediaire' => 289, 'cmsActif' => 290, 'controleTailleDepot' => 291, 'codeDceRestreint' => 292, 'numeroProjetAchat' => 293, 'referentielAchatId' => 294, 'agentTechniqueCreateur' => 295, 'uuid' => 296, 'contratExecUuid' => 297, ),
        BasePeer::TYPE_COLNAME => array (CommonConsultationPeer::ID => 0, CommonConsultationPeer::REFERENCE => 1, CommonConsultationPeer::CODE_EXTERNE => 2, CommonConsultationPeer::ORGANISME => 3, CommonConsultationPeer::REFERENCE_UTILISATEUR => 4, CommonConsultationPeer::CATEGORIE => 5, CommonConsultationPeer::TITRE => 6, CommonConsultationPeer::RESUME => 7, CommonConsultationPeer::DATEDEBUT => 8, CommonConsultationPeer::DATEFIN => 9, CommonConsultationPeer::DATEVALIDATION => 10, CommonConsultationPeer::TYPE_PROCEDURE => 11, CommonConsultationPeer::CODE_PROCEDURE => 12, CommonConsultationPeer::REPONSE_ELECTRONIQUE => 13, CommonConsultationPeer::NUM_PROCEDURE => 14, CommonConsultationPeer::ID_TYPE_PROCEDURE => 15, CommonConsultationPeer::ID_TYPE_AVIS => 16, CommonConsultationPeer::LIEU_EXECUTION => 17, CommonConsultationPeer::TYPE_MISE_EN_LIGNE => 18, CommonConsultationPeer::DATEMISEENLIGNE => 19, CommonConsultationPeer::IS_TIERS_AVIS => 20, CommonConsultationPeer::URL => 21, CommonConsultationPeer::DATEFIN_SAD => 22, CommonConsultationPeer::IS_SYS_ACQ_DYN => 23, CommonConsultationPeer::REFERENCE_CONSULTATION_INIT => 24, CommonConsultationPeer::SIGNATURE_OFFRE => 25, CommonConsultationPeer::ID_TYPE_VALIDATION => 26, CommonConsultationPeer::ETAT_APPROBATION => 27, CommonConsultationPeer::ETAT_VALIDATION => 28, CommonConsultationPeer::CHAMP_SUPP_INVISIBLE => 29, CommonConsultationPeer::CODE_CPV_1 => 30, CommonConsultationPeer::CODE_CPV_2 => 31, CommonConsultationPeer::PUBLICATION_EUROPE => 32, CommonConsultationPeer::ETAT_PUBLICATION => 33, CommonConsultationPeer::POURSUIVRE_AFFICHAGE => 34, CommonConsultationPeer::POURSUIVRE_AFFICHAGE_UNITE => 35, CommonConsultationPeer::NBR_TELECHARGEMENT_DCE => 36, CommonConsultationPeer::OLD_SERVICE_ID => 37, CommonConsultationPeer::OLD_SERVICE_ASSOCIE_ID => 38, CommonConsultationPeer::DETAIL_CONSULTATION => 39, CommonConsultationPeer::DATE_FIN_AFFICHAGE => 40, CommonConsultationPeer::DEPOUILLABLE_PHASE_CONSULTATION => 41, CommonConsultationPeer::CONSULTATION_TRANSVERSE => 42, CommonConsultationPeer::CONSULTATION_ACHAT_PUBLIQUE => 43, CommonConsultationPeer::URL_CONSULTATION_ACHAT_PUBLIQUE => 44, CommonConsultationPeer::PARTIAL_DCE_DOWNLOAD => 45, CommonConsultationPeer::TIRAGE_PLAN => 46, CommonConsultationPeer::TIREUR_PLAN => 47, CommonConsultationPeer::DATE_MISE_EN_LIGNE_CALCULE => 48, CommonConsultationPeer::ACCESSIBILITE_EN => 49, CommonConsultationPeer::ACCESSIBILITE_ES => 50, CommonConsultationPeer::NBR_REPONSE => 51, CommonConsultationPeer::ID_TYPE_PROCEDURE_ORG => 52, CommonConsultationPeer::ORGANISME_CONSULTATION_INIT => 53, CommonConsultationPeer::TIRAGE_DESCRIPTIF => 54, CommonConsultationPeer::DATE_VALIDATION_INTERMEDIAIRE => 55, CommonConsultationPeer::ACCESSIBILITE_FR => 56, CommonConsultationPeer::ID_TR_ACCESSIBILITE => 57, CommonConsultationPeer::ACCESSIBILITE_CZ => 58, CommonConsultationPeer::ACCESSIBILITE_DU => 59, CommonConsultationPeer::ACCESSIBILITE_SU => 60, CommonConsultationPeer::ACCESSIBILITE_AR => 61, CommonConsultationPeer::ALLOTI => 62, CommonConsultationPeer::NUMERO_PHASE => 63, CommonConsultationPeer::CONSULTATION_EXTERNE => 64, CommonConsultationPeer::URL_CONSULTATION_EXTERNE => 65, CommonConsultationPeer::ORG_DENOMINATION => 66, CommonConsultationPeer::DOMAINES_ACTIVITES => 67, CommonConsultationPeer::ID_AFFAIRE => 68, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS => 69, CommonConsultationPeer::CAUTION_PROVISOIRE => 70, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES => 71, CommonConsultationPeer::LIEU_OUVERTURE_PLIS => 72, CommonConsultationPeer::PRIX_AQUISITION_PLANS => 73, CommonConsultationPeer::QUALIFICATION => 74, CommonConsultationPeer::AGREMENTS => 75, CommonConsultationPeer::ADD_ECHANTILLION => 76, CommonConsultationPeer::DATE_LIMITE_ECHANTILLION => 77, CommonConsultationPeer::ADD_REUNION => 78, CommonConsultationPeer::DATE_REUNION => 79, CommonConsultationPeer::VARIANTES => 80, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_AR => 81, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_AR => 82, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_AR => 83, CommonConsultationPeer::PIECES_DOSSIER_ADMIN => 84, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_FR => 85, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_EN => 86, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_ES => 87, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_SU => 88, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_DU => 89, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_CZ => 90, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_AR => 91, CommonConsultationPeer::PIECES_DOSSIER_TECH => 92, CommonConsultationPeer::PIECES_DOSSIER_TECH_FR => 93, CommonConsultationPeer::PIECES_DOSSIER_TECH_EN => 94, CommonConsultationPeer::PIECES_DOSSIER_TECH_ES => 95, CommonConsultationPeer::PIECES_DOSSIER_TECH_SU => 96, CommonConsultationPeer::PIECES_DOSSIER_TECH_DU => 97, CommonConsultationPeer::PIECES_DOSSIER_TECH_CZ => 98, CommonConsultationPeer::PIECES_DOSSIER_TECH_AR => 99, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF => 100, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_FR => 101, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_EN => 102, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_ES => 103, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_SU => 104, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_DU => 105, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_CZ => 106, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_AR => 107, CommonConsultationPeer::ID_RPA => 108, CommonConsultationPeer::DETAIL_CONSULTATION_FR => 109, CommonConsultationPeer::DETAIL_CONSULTATION_EN => 110, CommonConsultationPeer::DETAIL_CONSULTATION_ES => 111, CommonConsultationPeer::DETAIL_CONSULTATION_SU => 112, CommonConsultationPeer::DETAIL_CONSULTATION_DU => 113, CommonConsultationPeer::DETAIL_CONSULTATION_CZ => 114, CommonConsultationPeer::DETAIL_CONSULTATION_AR => 115, CommonConsultationPeer::ECHANTILLON => 116, CommonConsultationPeer::REUNION => 117, CommonConsultationPeer::VISITES_LIEUX => 118, CommonConsultationPeer::VARIANTE_CALCULE => 119, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_FR => 120, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_EN => 121, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_ES => 122, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_SU => 123, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_DU => 124, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_CZ => 125, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_FR => 126, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_EN => 127, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_ES => 128, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_SU => 129, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_DU => 130, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_CZ => 131, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_FR => 132, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_EN => 133, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_ES => 134, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_SU => 135, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_DU => 136, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_CZ => 137, CommonConsultationPeer::ADD_ECHANTILLION_FR => 138, CommonConsultationPeer::ADD_ECHANTILLION_EN => 139, CommonConsultationPeer::ADD_ECHANTILLION_ES => 140, CommonConsultationPeer::ADD_ECHANTILLION_SU => 141, CommonConsultationPeer::ADD_ECHANTILLION_DU => 142, CommonConsultationPeer::ADD_ECHANTILLION_CZ => 143, CommonConsultationPeer::ADD_ECHANTILLION_AR => 144, CommonConsultationPeer::ADD_REUNION_FR => 145, CommonConsultationPeer::ADD_REUNION_EN => 146, CommonConsultationPeer::ADD_REUNION_ES => 147, CommonConsultationPeer::ADD_REUNION_SU => 148, CommonConsultationPeer::ADD_REUNION_DU => 149, CommonConsultationPeer::ADD_REUNION_CZ => 150, CommonConsultationPeer::ADD_REUNION_AR => 151, CommonConsultationPeer::MODE_PASSATION => 152, CommonConsultationPeer::CONSULTATION_ANNULEE => 153, CommonConsultationPeer::ACCESSIBILITE_IT => 154, CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_IT => 155, CommonConsultationPeer::LIEU_OUVERTURE_PLIS_IT => 156, CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_IT => 157, CommonConsultationPeer::PIECES_DOSSIER_ADMIN_IT => 158, CommonConsultationPeer::PIECES_DOSSIER_TECH_IT => 159, CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_IT => 160, CommonConsultationPeer::DETAIL_CONSULTATION_IT => 161, CommonConsultationPeer::ADD_ECHANTILLION_IT => 162, CommonConsultationPeer::ADD_REUNION_IT => 163, CommonConsultationPeer::CODES_NUTS => 164, CommonConsultationPeer::DATE_DECISION => 165, CommonConsultationPeer::INTITULE => 166, CommonConsultationPeer::ID_TR_INTITULE => 167, CommonConsultationPeer::OBJET => 168, CommonConsultationPeer::ID_TR_OBJET => 169, CommonConsultationPeer::TYPE_ACCES => 170, CommonConsultationPeer::AUTORISER_REPONSE_ELECTRONIQUE => 171, CommonConsultationPeer::REGLE_MISE_EN_LIGNE => 172, CommonConsultationPeer::ID_REGLE_VALIDATION => 173, CommonConsultationPeer::INTITULE_FR => 174, CommonConsultationPeer::INTITULE_EN => 175, CommonConsultationPeer::INTITULE_ES => 176, CommonConsultationPeer::INTITULE_SU => 177, CommonConsultationPeer::INTITULE_DU => 178, CommonConsultationPeer::INTITULE_CZ => 179, CommonConsultationPeer::INTITULE_AR => 180, CommonConsultationPeer::INTITULE_IT => 181, CommonConsultationPeer::OBJET_FR => 182, CommonConsultationPeer::OBJET_EN => 183, CommonConsultationPeer::OBJET_ES => 184, CommonConsultationPeer::OBJET_SU => 185, CommonConsultationPeer::OBJET_DU => 186, CommonConsultationPeer::OBJET_CZ => 187, CommonConsultationPeer::OBJET_AR => 188, CommonConsultationPeer::OBJET_IT => 189, CommonConsultationPeer::CLAUSE_SOCIALE => 190, CommonConsultationPeer::CLAUSE_ENVIRONNEMENTALE => 191, CommonConsultationPeer::REPONSE_OBLIGATOIRE => 192, CommonConsultationPeer::TYPE_ENVOI => 193, CommonConsultationPeer::CHIFFREMENT_OFFRE => 194, CommonConsultationPeer::ENV_CANDIDATURE => 195, CommonConsultationPeer::ENV_OFFRE => 196, CommonConsultationPeer::ENV_ANONYMAT => 197, CommonConsultationPeer::ID_ETAT_CONSULTATION => 198, CommonConsultationPeer::REFERENCE_CONNECTEUR => 199, CommonConsultationPeer::CONS_STATUT => 200, CommonConsultationPeer::ID_APPROBATEUR => 201, CommonConsultationPeer::ID_VALIDEUR => 202, CommonConsultationPeer::OLD_SERVICE_VALIDATION => 203, CommonConsultationPeer::ID_CREATEUR => 204, CommonConsultationPeer::NOM_CREATEUR => 205, CommonConsultationPeer::PRENOM_CREATEUR => 206, CommonConsultationPeer::SIGNATURE_ACTE_ENGAGEMENT => 207, CommonConsultationPeer::ARCHIVEMETADESCRIPTION => 208, CommonConsultationPeer::ARCHIVEMETAMOTSCLEF => 209, CommonConsultationPeer::ARCHIVEIDBLOBZIP => 210, CommonConsultationPeer::DECISION_PARTIELLE => 211, CommonConsultationPeer::TYPE_DECISION_A_RENSEIGNER => 212, CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_MARCHE => 213, CommonConsultationPeer::TYPE_DECISION_DECLARATION_SANS_SUITE => 214, CommonConsultationPeer::TYPE_DECISION_DECLARATION_INFRUCTUEUX => 215, CommonConsultationPeer::TYPE_DECISION_SELECTION_ENTREPRISE => 216, CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE => 217, CommonConsultationPeer::TYPE_DECISION_ADMISSION_SAD => 218, CommonConsultationPeer::TYPE_DECISION_AUTRE => 219, CommonConsultationPeer::ID_ARCHIVEUR => 220, CommonConsultationPeer::PRENOM_NOM_AGENT_TELECHARGEMENT_PLIS => 221, CommonConsultationPeer::ID_AGENT_TELECHARGEMENT_PLIS => 222, CommonConsultationPeer::PATH_TELECHARGEMENT_PLIS => 223, CommonConsultationPeer::DATE_TELECHARGEMENT_PLIS => 224, CommonConsultationPeer::OLD_SERVICE_VALIDATION_INTERMEDIAIRE => 225, CommonConsultationPeer::ENV_OFFRE_TECHNIQUE => 226, CommonConsultationPeer::REF_ORG_PARTENAIRE => 227, CommonConsultationPeer::DATE_ARCHIVAGE => 228, CommonConsultationPeer::DATE_DECISION_ANNULATION => 229, CommonConsultationPeer::COMMENTAIRE_ANNULATION => 230, CommonConsultationPeer::COMPTE_BOAMP_ASSOCIE => 231, CommonConsultationPeer::DATE_MISE_EN_LIGNE_SOUHAITEE => 232, CommonConsultationPeer::AUTORISER_PUBLICITE => 233, CommonConsultationPeer::DOSSIER_ADDITIF => 234, CommonConsultationPeer::TYPE_MARCHE => 235, CommonConsultationPeer::TYPE_PRESTATION => 236, CommonConsultationPeer::DATE_MODIFICATION => 237, CommonConsultationPeer::DELAI_PARTIEL => 238, CommonConsultationPeer::DATEFINLOCALE => 239, CommonConsultationPeer::LIEURESIDENCE => 240, CommonConsultationPeer::ALERTE => 241, CommonConsultationPeer::DOUBLON => 242, CommonConsultationPeer::DENOMINATION_ADAPTE => 243, CommonConsultationPeer::URL_CONSULTATION_AVIS_PUB => 244, CommonConsultationPeer::DOUBLON_DE => 245, CommonConsultationPeer::ENTITE_ADJUDICATRICE => 246, CommonConsultationPeer::CODE_OPERATION => 247, CommonConsultationPeer::CLAUSE_SOCIALE_CONDITION_EXECUTION => 248, CommonConsultationPeer::CLAUSE_SOCIALE_INSERTION => 249, CommonConsultationPeer::CLAUSE_SOCIALE_ATELIERS_PROTEGES => 250, CommonConsultationPeer::CLAUSE_SOCIALE_SIAE => 251, CommonConsultationPeer::CLAUSE_SOCIALE_ESS => 252, CommonConsultationPeer::CLAUSE_ENV_SPECS_TECHNIQUES => 253, CommonConsultationPeer::CLAUSE_ENV_COND_EXECUTION => 254, CommonConsultationPeer::CLAUSE_ENV_CRITERES_SELECT => 255, CommonConsultationPeer::ID_DONNEE_COMPLEMENTAIRE => 256, CommonConsultationPeer::DONNEE_COMPLEMENTAIRE_OBLIGATOIRE => 257, CommonConsultationPeer::MODE_OUVERTURE_REPONSE => 258, CommonConsultationPeer::ID_FICHIER_ANNULATION => 259, CommonConsultationPeer::IDOPERATION => 260, CommonConsultationPeer::ETAT_EN_ATTENTE_VALIDATION => 261, CommonConsultationPeer::INFOS_BLOCS_ATLAS => 262, CommonConsultationPeer::MARCHE_PUBLIC_SIMPLIFIE => 263, CommonConsultationPeer::DATE_FIN_UNIX => 264, CommonConsultationPeer::NUMERO_AC => 265, CommonConsultationPeer::ID_CONTRAT => 266, CommonConsultationPeer::PIN_API_SGMAP_MPS => 267, CommonConsultationPeer::DONNEE_PUBLICITE_OBLIGATOIRE => 268, CommonConsultationPeer::DUME_DEMANDE => 269, CommonConsultationPeer::TYPE_PROCEDURE_DUME => 270, CommonConsultationPeer::MARCHE_INSERTION => 271, CommonConsultationPeer::CLAUSE_SPECIFICATION_TECHNIQUE => 272, CommonConsultationPeer::TYPE_FORMULAIRE_DUME => 273, CommonConsultationPeer::SOURCE_EXTERNE => 274, CommonConsultationPeer::ID_SOURCE_EXTERNE => 275, CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX => 276, CommonConsultationPeer::ATTESTATION_CONSULTATION => 277, CommonConsultationPeer::VERSION_MESSAGERIE => 278, CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID => 279, CommonConsultationPeer::IS_ENVOI_PUBLICITE_VALIDATION => 280, CommonConsultationPeer::ANNEXE_FINANCIERE => 281, CommonConsultationPeer::ENVOL_ACTIVATION => 282, CommonConsultationPeer::SERVICE_ID => 283, CommonConsultationPeer::SERVICE_ASSOCIE_ID => 284, CommonConsultationPeer::GROUPEMENT => 285, CommonConsultationPeer::SERVICE_VALIDATION => 286, CommonConsultationPeer::AUTRE_TECHNIQUE_ACHAT => 287, CommonConsultationPeer::PROCEDURE_OUVERTE => 288, CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE => 289, CommonConsultationPeer::CMS_ACTIF => 290, CommonConsultationPeer::CONTROLE_TAILLE_DEPOT => 291, CommonConsultationPeer::CODE_DCE_RESTREINT => 292, CommonConsultationPeer::NUMERO_PROJET_ACHAT => 293, CommonConsultationPeer::REFERENTIEL_ACHAT_ID => 294, CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR => 295, CommonConsultationPeer::UUID => 296,  CommonConsultationPeer::CONTRAT_EXEC_UUID => 297, ),
        BasePeer::TYPE_RAW_COLNAME => array ('ID' => 0, 'REFERENCE' => 1, 'CODE_EXTERNE' => 2, 'ORGANISME' => 3, 'REFERENCE_UTILISATEUR' => 4, 'CATEGORIE' => 5, 'TITRE' => 6, 'RESUME' => 7, 'DATEDEBUT' => 8, 'DATEFIN' => 9, 'DATEVALIDATION' => 10, 'TYPE_PROCEDURE' => 11, 'CODE_PROCEDURE' => 12, 'REPONSE_ELECTRONIQUE' => 13, 'NUM_PROCEDURE' => 14, 'ID_TYPE_PROCEDURE' => 15, 'ID_TYPE_AVIS' => 16, 'LIEU_EXECUTION' => 17, 'TYPE_MISE_EN_LIGNE' => 18, 'DATEMISEENLIGNE' => 19, 'IS_TIERS_AVIS' => 20, 'URL' => 21, 'DATEFIN_SAD' => 22, 'IS_SYS_ACQ_DYN' => 23, 'REFERENCE_CONSULTATION_INIT' => 24, 'SIGNATURE_OFFRE' => 25, 'ID_TYPE_VALIDATION' => 26, 'ETAT_APPROBATION' => 27, 'ETAT_VALIDATION' => 28, 'CHAMP_SUPP_INVISIBLE' => 29, 'CODE_CPV_1' => 30, 'CODE_CPV_2' => 31, 'PUBLICATION_EUROPE' => 32, 'ETAT_PUBLICATION' => 33, 'POURSUIVRE_AFFICHAGE' => 34, 'POURSUIVRE_AFFICHAGE_UNITE' => 35, 'NBR_TELECHARGEMENT_DCE' => 36, 'OLD_SERVICE_ID' => 37, 'OLD_SERVICE_ASSOCIE_ID' => 38, 'DETAIL_CONSULTATION' => 39, 'DATE_FIN_AFFICHAGE' => 40, 'DEPOUILLABLE_PHASE_CONSULTATION' => 41, 'CONSULTATION_TRANSVERSE' => 42, 'CONSULTATION_ACHAT_PUBLIQUE' => 43, 'URL_CONSULTATION_ACHAT_PUBLIQUE' => 44, 'PARTIAL_DCE_DOWNLOAD' => 45, 'TIRAGE_PLAN' => 46, 'TIREUR_PLAN' => 47, 'DATE_MISE_EN_LIGNE_CALCULE' => 48, 'ACCESSIBILITE_EN' => 49, 'ACCESSIBILITE_ES' => 50, 'NBR_REPONSE' => 51, 'ID_TYPE_PROCEDURE_ORG' => 52, 'ORGANISME_CONSULTATION_INIT' => 53, 'TIRAGE_DESCRIPTIF' => 54, 'DATE_VALIDATION_INTERMEDIAIRE' => 55, 'ACCESSIBILITE_FR' => 56, 'ID_TR_ACCESSIBILITE' => 57, 'ACCESSIBILITE_CZ' => 58, 'ACCESSIBILITE_DU' => 59, 'ACCESSIBILITE_SU' => 60, 'ACCESSIBILITE_AR' => 61, 'ALLOTI' => 62, 'NUMERO_PHASE' => 63, 'CONSULTATION_EXTERNE' => 64, 'URL_CONSULTATION_EXTERNE' => 65, 'ORG_DENOMINATION' => 66, 'DOMAINES_ACTIVITES' => 67, 'ID_AFFAIRE' => 68, 'ADRESSE_RETRAIS_DOSSIERS' => 69, 'CAUTION_PROVISOIRE' => 70, 'ADRESSE_DEPOT_OFFRES' => 71, 'LIEU_OUVERTURE_PLIS' => 72, 'PRIX_AQUISITION_PLANS' => 73, 'QUALIFICATION' => 74, 'AGREMENTS' => 75, 'ADD_ECHANTILLION' => 76, 'DATE_LIMITE_ECHANTILLION' => 77, 'ADD_REUNION' => 78, 'DATE_REUNION' => 79, 'VARIANTES' => 80, 'ADRESSE_DEPOT_OFFRES_AR' => 81, 'LIEU_OUVERTURE_PLIS_AR' => 82, 'ADRESSE_RETRAIS_DOSSIERS_AR' => 83, 'PIECES_DOSSIER_ADMIN' => 84, 'PIECES_DOSSIER_ADMIN_FR' => 85, 'PIECES_DOSSIER_ADMIN_EN' => 86, 'PIECES_DOSSIER_ADMIN_ES' => 87, 'PIECES_DOSSIER_ADMIN_SU' => 88, 'PIECES_DOSSIER_ADMIN_DU' => 89, 'PIECES_DOSSIER_ADMIN_CZ' => 90, 'PIECES_DOSSIER_ADMIN_AR' => 91, 'PIECES_DOSSIER_TECH' => 92, 'PIECES_DOSSIER_TECH_FR' => 93, 'PIECES_DOSSIER_TECH_EN' => 94, 'PIECES_DOSSIER_TECH_ES' => 95, 'PIECES_DOSSIER_TECH_SU' => 96, 'PIECES_DOSSIER_TECH_DU' => 97, 'PIECES_DOSSIER_TECH_CZ' => 98, 'PIECES_DOSSIER_TECH_AR' => 99, 'PIECES_DOSSIER_ADDITIF' => 100, 'PIECES_DOSSIER_ADDITIF_FR' => 101, 'PIECES_DOSSIER_ADDITIF_EN' => 102, 'PIECES_DOSSIER_ADDITIF_ES' => 103, 'PIECES_DOSSIER_ADDITIF_SU' => 104, 'PIECES_DOSSIER_ADDITIF_DU' => 105, 'PIECES_DOSSIER_ADDITIF_CZ' => 106, 'PIECES_DOSSIER_ADDITIF_AR' => 107, 'ID_RPA' => 108, 'DETAIL_CONSULTATION_FR' => 109, 'DETAIL_CONSULTATION_EN' => 110, 'DETAIL_CONSULTATION_ES' => 111, 'DETAIL_CONSULTATION_SU' => 112, 'DETAIL_CONSULTATION_DU' => 113, 'DETAIL_CONSULTATION_CZ' => 114, 'DETAIL_CONSULTATION_AR' => 115, 'ECHANTILLON' => 116, 'REUNION' => 117, 'VISITES_LIEUX' => 118, 'VARIANTE_CALCULE' => 119, 'ADRESSE_RETRAIS_DOSSIERS_FR' => 120, 'ADRESSE_RETRAIS_DOSSIERS_EN' => 121, 'ADRESSE_RETRAIS_DOSSIERS_ES' => 122, 'ADRESSE_RETRAIS_DOSSIERS_SU' => 123, 'ADRESSE_RETRAIS_DOSSIERS_DU' => 124, 'ADRESSE_RETRAIS_DOSSIERS_CZ' => 125, 'ADRESSE_DEPOT_OFFRES_FR' => 126, 'ADRESSE_DEPOT_OFFRES_EN' => 127, 'ADRESSE_DEPOT_OFFRES_ES' => 128, 'ADRESSE_DEPOT_OFFRES_SU' => 129, 'ADRESSE_DEPOT_OFFRES_DU' => 130, 'ADRESSE_DEPOT_OFFRES_CZ' => 131, 'LIEU_OUVERTURE_PLIS_FR' => 132, 'LIEU_OUVERTURE_PLIS_EN' => 133, 'LIEU_OUVERTURE_PLIS_ES' => 134, 'LIEU_OUVERTURE_PLIS_SU' => 135, 'LIEU_OUVERTURE_PLIS_DU' => 136, 'LIEU_OUVERTURE_PLIS_CZ' => 137, 'ADD_ECHANTILLION_FR' => 138, 'ADD_ECHANTILLION_EN' => 139, 'ADD_ECHANTILLION_ES' => 140, 'ADD_ECHANTILLION_SU' => 141, 'ADD_ECHANTILLION_DU' => 142, 'ADD_ECHANTILLION_CZ' => 143, 'ADD_ECHANTILLION_AR' => 144, 'ADD_REUNION_FR' => 145, 'ADD_REUNION_EN' => 146, 'ADD_REUNION_ES' => 147, 'ADD_REUNION_SU' => 148, 'ADD_REUNION_DU' => 149, 'ADD_REUNION_CZ' => 150, 'ADD_REUNION_AR' => 151, 'MODE_PASSATION' => 152, 'CONSULTATION_ANNULEE' => 153, 'ACCESSIBILITE_IT' => 154, 'ADRESSE_DEPOT_OFFRES_IT' => 155, 'LIEU_OUVERTURE_PLIS_IT' => 156, 'ADRESSE_RETRAIS_DOSSIERS_IT' => 157, 'PIECES_DOSSIER_ADMIN_IT' => 158, 'PIECES_DOSSIER_TECH_IT' => 159, 'PIECES_DOSSIER_ADDITIF_IT' => 160, 'DETAIL_CONSULTATION_IT' => 161, 'ADD_ECHANTILLION_IT' => 162, 'ADD_REUNION_IT' => 163, 'CODES_NUTS' => 164, 'DATE_DECISION' => 165, 'INTITULE' => 166, 'ID_TR_INTITULE' => 167, 'OBJET' => 168, 'ID_TR_OBJET' => 169, 'TYPE_ACCES' => 170, 'AUTORISER_REPONSE_ELECTRONIQUE' => 171, 'REGLE_MISE_EN_LIGNE' => 172, 'ID_REGLE_VALIDATION' => 173, 'INTITULE_FR' => 174, 'INTITULE_EN' => 175, 'INTITULE_ES' => 176, 'INTITULE_SU' => 177, 'INTITULE_DU' => 178, 'INTITULE_CZ' => 179, 'INTITULE_AR' => 180, 'INTITULE_IT' => 181, 'OBJET_FR' => 182, 'OBJET_EN' => 183, 'OBJET_ES' => 184, 'OBJET_SU' => 185, 'OBJET_DU' => 186, 'OBJET_CZ' => 187, 'OBJET_AR' => 188, 'OBJET_IT' => 189, 'CLAUSE_SOCIALE' => 190, 'CLAUSE_ENVIRONNEMENTALE' => 191, 'REPONSE_OBLIGATOIRE' => 192, 'TYPE_ENVOI' => 193, 'CHIFFREMENT_OFFRE' => 194, 'ENV_CANDIDATURE' => 195, 'ENV_OFFRE' => 196, 'ENV_ANONYMAT' => 197, 'ID_ETAT_CONSULTATION' => 198, 'REFERENCE_CONNECTEUR' => 199, 'CONS_STATUT' => 200, 'ID_APPROBATEUR' => 201, 'ID_VALIDEUR' => 202, 'OLD_SERVICE_VALIDATION' => 203, 'ID_CREATEUR' => 204, 'NOM_CREATEUR' => 205, 'PRENOM_CREATEUR' => 206, 'SIGNATURE_ACTE_ENGAGEMENT' => 207, 'ARCHIVEMETADESCRIPTION' => 208, 'ARCHIVEMETAMOTSCLEF' => 209, 'ARCHIVEIDBLOBZIP' => 210, 'DECISION_PARTIELLE' => 211, 'TYPE_DECISION_A_RENSEIGNER' => 212, 'TYPE_DECISION_ATTRIBUTION_MARCHE' => 213, 'TYPE_DECISION_DECLARATION_SANS_SUITE' => 214, 'TYPE_DECISION_DECLARATION_INFRUCTUEUX' => 215, 'TYPE_DECISION_SELECTION_ENTREPRISE' => 216, 'TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE' => 217, 'TYPE_DECISION_ADMISSION_SAD' => 218, 'TYPE_DECISION_AUTRE' => 219, 'ID_ARCHIVEUR' => 220, 'PRENOM_NOM_AGENT_TELECHARGEMENT_PLIS' => 221, 'ID_AGENT_TELECHARGEMENT_PLIS' => 222, 'PATH_TELECHARGEMENT_PLIS' => 223, 'DATE_TELECHARGEMENT_PLIS' => 224, 'OLD_SERVICE_VALIDATION_INTERMEDIAIRE' => 225, 'ENV_OFFRE_TECHNIQUE' => 226, 'REF_ORG_PARTENAIRE' => 227, 'DATE_ARCHIVAGE' => 228, 'DATE_DECISION_ANNULATION' => 229, 'COMMENTAIRE_ANNULATION' => 230, 'COMPTE_BOAMP_ASSOCIE' => 231, 'DATE_MISE_EN_LIGNE_SOUHAITEE' => 232, 'AUTORISER_PUBLICITE' => 233, 'DOSSIER_ADDITIF' => 234, 'TYPE_MARCHE' => 235, 'TYPE_PRESTATION' => 236, 'DATE_MODIFICATION' => 237, 'DELAI_PARTIEL' => 238, 'DATEFINLOCALE' => 239, 'LIEURESIDENCE' => 240, 'ALERTE' => 241, 'DOUBLON' => 242, 'DENOMINATION_ADAPTE' => 243, 'URL_CONSULTATION_AVIS_PUB' => 244, 'DOUBLON_DE' => 245, 'ENTITE_ADJUDICATRICE' => 246, 'CODE_OPERATION' => 247, 'CLAUSE_SOCIALE_CONDITION_EXECUTION' => 248, 'CLAUSE_SOCIALE_INSERTION' => 249, 'CLAUSE_SOCIALE_ATELIERS_PROTEGES' => 250, 'CLAUSE_SOCIALE_SIAE' => 251, 'CLAUSE_SOCIALE_ESS' => 252, 'CLAUSE_ENV_SPECS_TECHNIQUES' => 253, 'CLAUSE_ENV_COND_EXECUTION' => 254, 'CLAUSE_ENV_CRITERES_SELECT' => 255, 'ID_DONNEE_COMPLEMENTAIRE' => 256, 'DONNEE_COMPLEMENTAIRE_OBLIGATOIRE' => 257, 'MODE_OUVERTURE_REPONSE' => 258, 'ID_FICHIER_ANNULATION' => 259, 'IDOPERATION' => 260, 'ETAT_EN_ATTENTE_VALIDATION' => 261, 'INFOS_BLOCS_ATLAS' => 262, 'MARCHE_PUBLIC_SIMPLIFIE' => 263, 'DATE_FIN_UNIX' => 264, 'NUMERO_AC' => 265, 'ID_CONTRAT' => 266, 'PIN_API_SGMAP_MPS' => 267, 'DONNEE_PUBLICITE_OBLIGATOIRE' => 268, 'DUME_DEMANDE' => 269, 'TYPE_PROCEDURE_DUME' => 270, 'MARCHE_INSERTION' => 271, 'CLAUSE_SPECIFICATION_TECHNIQUE' => 272, 'TYPE_FORMULAIRE_DUME' => 273, 'SOURCE_EXTERNE' => 274, 'ID_SOURCE_EXTERNE' => 275, 'ID_DOSSIER_VOLUMINEUX' => 276, 'ATTESTATION_CONSULTATION' => 277, 'VERSION_MESSAGERIE' => 278, 'PLATEFORME_VIRTUELLE_ID' => 279, 'IS_ENVOI_PUBLICITE_VALIDATION' => 280, 'ANNEXE_FINANCIERE' => 281, 'ENVOL_ACTIVATION' => 282, 'SERVICE_ID' => 283, 'SERVICE_ASSOCIE_ID' => 284, 'GROUPEMENT' => 285, 'SERVICE_VALIDATION' => 286, 'AUTRE_TECHNIQUE_ACHAT' => 287, 'PROCEDURE_OUVERTE' => 288, 'SERVICE_VALIDATION_INTERMEDIAIRE' => 289, 'CMS_ACTIF' => 290, 'CONTROLE_TAILLE_DEPOT' => 291, 'CODE_DCE_RESTREINT' => 292, 'NUMERO_PROJET_ACHAT' => 293, 'REFERENTIEL_ACHAT_ID' => 294, 'AGENT_TECHNIQUE_CREATEUR' => 295, 'UUID' => 296,  'CONTRAT_EXEC_UUID' => 297, ),
        BasePeer::TYPE_FIELDNAME => array ('id' => 0, 'reference' => 1, 'code_externe' => 2, 'organisme' => 3, 'reference_utilisateur' => 4, 'categorie' => 5, 'titre' => 6, 'resume' => 7, 'datedebut' => 8, 'datefin' => 9, 'datevalidation' => 10, 'type_procedure' => 11, 'code_procedure' => 12, 'reponse_electronique' => 13, 'num_procedure' => 14, 'id_type_procedure' => 15, 'id_type_avis' => 16, 'lieu_execution' => 17, 'type_mise_en_ligne' => 18, 'datemiseenligne' => 19, 'is_tiers_avis' => 20, 'url' => 21, 'datefin_sad' => 22, 'is_sys_acq_dyn' => 23, 'reference_consultation_init' => 24, 'signature_offre' => 25, 'id_type_validation' => 26, 'etat_approbation' => 27, 'etat_validation' => 28, 'champ_supp_invisible' => 29, 'code_cpv_1' => 30, 'code_cpv_2' => 31, 'publication_europe' => 32, 'etat_publication' => 33, 'poursuivre_affichage' => 34, 'poursuivre_affichage_unite' => 35, 'nbr_telechargement_dce' => 36, 'old_service_id' => 37, 'old_service_associe_id' => 38, 'detail_consultation' => 39, 'date_fin_affichage' => 40, 'depouillable_phase_consultation' => 41, 'consultation_transverse' => 42, 'consultation_achat_publique' => 43, 'url_consultation_achat_publique' => 44, 'partial_dce_download' => 45, 'tirage_plan' => 46, 'tireur_plan' => 47, 'date_mise_en_ligne_calcule' => 48, 'accessibilite_en' => 49, 'accessibilite_es' => 50, 'nbr_reponse' => 51, 'id_type_procedure_org' => 52, 'organisme_consultation_init' => 53, 'tirage_descriptif' => 54, 'date_validation_intermediaire' => 55, 'accessibilite_fr' => 56, 'id_tr_accessibilite' => 57, 'accessibilite_cz' => 58, 'accessibilite_du' => 59, 'accessibilite_su' => 60, 'accessibilite_ar' => 61, 'alloti' => 62, 'numero_phase' => 63, 'consultation_externe' => 64, 'url_consultation_externe' => 65, 'org_denomination' => 66, 'domaines_activites' => 67, 'id_affaire' => 68, 'adresse_retrais_dossiers' => 69, 'caution_provisoire' => 70, 'adresse_depot_offres' => 71, 'lieu_ouverture_plis' => 72, 'prix_aquisition_plans' => 73, 'qualification' => 74, 'agrements' => 75, 'add_echantillion' => 76, 'date_limite_echantillion' => 77, 'add_reunion' => 78, 'date_reunion' => 79, 'variantes' => 80, 'adresse_depot_offres_ar' => 81, 'lieu_ouverture_plis_ar' => 82, 'adresse_retrais_dossiers_ar' => 83, 'pieces_dossier_admin' => 84, 'pieces_dossier_admin_fr' => 85, 'pieces_dossier_admin_en' => 86, 'pieces_dossier_admin_es' => 87, 'pieces_dossier_admin_su' => 88, 'pieces_dossier_admin_du' => 89, 'pieces_dossier_admin_cz' => 90, 'pieces_dossier_admin_ar' => 91, 'pieces_dossier_tech' => 92, 'pieces_dossier_tech_fr' => 93, 'pieces_dossier_tech_en' => 94, 'pieces_dossier_tech_es' => 95, 'pieces_dossier_tech_su' => 96, 'pieces_dossier_tech_du' => 97, 'pieces_dossier_tech_cz' => 98, 'pieces_dossier_tech_ar' => 99, 'pieces_dossier_additif' => 100, 'pieces_dossier_additif_fr' => 101, 'pieces_dossier_additif_en' => 102, 'pieces_dossier_additif_es' => 103, 'pieces_dossier_additif_su' => 104, 'pieces_dossier_additif_du' => 105, 'pieces_dossier_additif_cz' => 106, 'pieces_dossier_additif_ar' => 107, 'id_rpa' => 108, 'detail_consultation_fr' => 109, 'detail_consultation_en' => 110, 'detail_consultation_es' => 111, 'detail_consultation_su' => 112, 'detail_consultation_du' => 113, 'detail_consultation_cz' => 114, 'detail_consultation_ar' => 115, 'echantillon' => 116, 'reunion' => 117, 'visites_lieux' => 118, 'variante_calcule' => 119, 'adresse_retrais_dossiers_fr' => 120, 'adresse_retrais_dossiers_en' => 121, 'adresse_retrais_dossiers_es' => 122, 'adresse_retrais_dossiers_su' => 123, 'adresse_retrais_dossiers_du' => 124, 'adresse_retrais_dossiers_cz' => 125, 'adresse_depot_offres_fr' => 126, 'adresse_depot_offres_en' => 127, 'adresse_depot_offres_es' => 128, 'adresse_depot_offres_su' => 129, 'adresse_depot_offres_du' => 130, 'adresse_depot_offres_cz' => 131, 'lieu_ouverture_plis_fr' => 132, 'lieu_ouverture_plis_en' => 133, 'lieu_ouverture_plis_es' => 134, 'lieu_ouverture_plis_su' => 135, 'lieu_ouverture_plis_du' => 136, 'lieu_ouverture_plis_cz' => 137, 'add_echantillion_fr' => 138, 'add_echantillion_en' => 139, 'add_echantillion_es' => 140, 'add_echantillion_su' => 141, 'add_echantillion_du' => 142, 'add_echantillion_cz' => 143, 'add_echantillion_ar' => 144, 'add_reunion_fr' => 145, 'add_reunion_en' => 146, 'add_reunion_es' => 147, 'add_reunion_su' => 148, 'add_reunion_du' => 149, 'add_reunion_cz' => 150, 'add_reunion_ar' => 151, 'mode_passation' => 152, 'consultation_annulee' => 153, 'accessibilite_it' => 154, 'adresse_depot_offres_it' => 155, 'lieu_ouverture_plis_it' => 156, 'adresse_retrais_dossiers_it' => 157, 'pieces_dossier_admin_it' => 158, 'pieces_dossier_tech_it' => 159, 'pieces_dossier_additif_it' => 160, 'detail_consultation_it' => 161, 'add_echantillion_it' => 162, 'add_reunion_it' => 163, 'codes_nuts' => 164, 'date_decision' => 165, 'intitule' => 166, 'id_tr_intitule' => 167, 'objet' => 168, 'id_tr_objet' => 169, 'type_acces' => 170, 'autoriser_reponse_electronique' => 171, 'regle_mise_en_ligne' => 172, 'id_regle_validation' => 173, 'intitule_fr' => 174, 'intitule_en' => 175, 'intitule_es' => 176, 'intitule_su' => 177, 'intitule_du' => 178, 'intitule_cz' => 179, 'intitule_ar' => 180, 'intitule_it' => 181, 'objet_fr' => 182, 'objet_en' => 183, 'objet_es' => 184, 'objet_su' => 185, 'objet_du' => 186, 'objet_cz' => 187, 'objet_ar' => 188, 'objet_it' => 189, 'clause_sociale' => 190, 'clause_environnementale' => 191, 'reponse_obligatoire' => 192, 'type_envoi' => 193, 'chiffrement_offre' => 194, 'env_candidature' => 195, 'env_offre' => 196, 'env_anonymat' => 197, 'id_etat_consultation' => 198, 'reference_connecteur' => 199, 'cons_statut' => 200, 'id_approbateur' => 201, 'id_valideur' => 202, 'old_service_validation' => 203, 'id_createur' => 204, 'nom_createur' => 205, 'prenom_createur' => 206, 'signature_acte_engagement' => 207, 'archiveMetaDescription' => 208, 'archiveMetaMotsClef' => 209, 'archiveIdBlobZip' => 210, 'decision_partielle' => 211, 'type_decision_a_renseigner' => 212, 'type_decision_attribution_marche' => 213, 'type_decision_declaration_sans_suite' => 214, 'type_decision_declaration_infructueux' => 215, 'type_decision_selection_entreprise' => 216, 'type_decision_attribution_accord_cadre' => 217, 'type_decision_admission_sad' => 218, 'type_decision_autre' => 219, 'id_archiveur' => 220, 'prenom_nom_agent_telechargement_plis' => 221, 'id_agent_telechargement_plis' => 222, 'path_telechargement_plis' => 223, 'date_telechargement_plis' => 224, 'old_service_validation_intermediaire' => 225, 'env_offre_technique' => 226, 'ref_org_partenaire' => 227, 'date_archivage' => 228, 'date_decision_annulation' => 229, 'commentaire_annulation' => 230, 'compte_boamp_associe' => 231, 'date_mise_en_ligne_souhaitee' => 232, 'autoriser_publicite' => 233, 'dossier_additif' => 234, 'type_marche' => 235, 'type_prestation' => 236, 'date_modification' => 237, 'delai_partiel' => 238, 'dateFinLocale' => 239, 'lieuResidence' => 240, 'alerte' => 241, 'doublon' => 242, 'denomination_adapte' => 243, 'url_consultation_avis_pub' => 244, 'doublon_de' => 245, 'entite_adjudicatrice' => 246, 'code_operation' => 247, 'clause_sociale_condition_execution' => 248, 'clause_sociale_insertion' => 249, 'clause_sociale_ateliers_proteges' => 250, 'clause_sociale_siae' => 251, 'clause_sociale_ess' => 252, 'clause_env_specs_techniques' => 253, 'clause_env_cond_execution' => 254, 'clause_env_criteres_select' => 255, 'id_donnee_complementaire' => 256, 'donnee_complementaire_obligatoire' => 257, 'mode_ouverture_reponse' => 258, 'id_fichier_annulation' => 259, 'idOperation' => 260, 'etat_en_attente_validation' => 261, 'infos_blocs_atlas' => 262, 'marche_public_simplifie' => 263, 'DATE_FIN_UNIX' => 264, 'numero_AC' => 265, 'id_contrat' => 266, 'pin_api_sgmap_mps' => 267, 'donnee_publicite_obligatoire' => 268, 'dume_demande' => 269, 'type_procedure_dume' => 270, 'marche_insertion' => 271, 'clause_specification_technique' => 272, 'type_formulaire_dume' => 273, 'source_externe' => 274, 'id_source_externe' => 275, 'id_dossier_volumineux' => 276, 'attestation_consultation' => 277, 'version_messagerie' => 278, 'plateforme_virtuelle_id' => 279, 'is_envoi_publicite_validation' => 280, 'annexe_financiere' => 281, 'envol_activation' => 282, 'service_id' => 283, 'service_associe_id' => 284, 'groupement' => 285, 'service_validation' => 286, 'autre_technique_achat' => 287, 'procedure_ouverte' => 288, 'service_validation_intermediaire' => 289, 'cms_actif' => 290, 'controle_taille_depot' => 291, 'code_dce_restreint' => 292, 'numero_projet_achat' => 293, 'referentiel_achat_id' => 294, 'agent_technique_createur' => 295, 'uuid' => 296,  'contrat_exec_uuid' => 297, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
        CommonConsultationPeer::ETAT_APPROBATION => array(
            CommonConsultationPeer::ETAT_APPROBATION_0,
            CommonConsultationPeer::ETAT_APPROBATION_1,
        ),
        CommonConsultationPeer::ETAT_VALIDATION => array(
            CommonConsultationPeer::ETAT_VALIDATION_0,
            CommonConsultationPeer::ETAT_VALIDATION_1,
        ),
        CommonConsultationPeer::PUBLICATION_EUROPE => array(
            CommonConsultationPeer::PUBLICATION_EUROPE_0,
            CommonConsultationPeer::PUBLICATION_EUROPE_1,
        ),
        CommonConsultationPeer::POURSUIVRE_AFFICHAGE_UNITE => array(
            CommonConsultationPeer::POURSUIVRE_AFFICHAGE_UNITE_MINUTE,
            CommonConsultationPeer::POURSUIVRE_AFFICHAGE_UNITE_HOUR,
            CommonConsultationPeer::POURSUIVRE_AFFICHAGE_UNITE_DAY,
            CommonConsultationPeer::POURSUIVRE_AFFICHAGE_UNITE_MONTH,
            CommonConsultationPeer::POURSUIVRE_AFFICHAGE_UNITE_YEAR,
        ),
        CommonConsultationPeer::DEPOUILLABLE_PHASE_CONSULTATION => array(
            CommonConsultationPeer::DEPOUILLABLE_PHASE_CONSULTATION_0,
            CommonConsultationPeer::DEPOUILLABLE_PHASE_CONSULTATION_1,
        ),
        CommonConsultationPeer::CONSULTATION_TRANSVERSE => array(
            CommonConsultationPeer::CONSULTATION_TRANSVERSE_0,
            CommonConsultationPeer::CONSULTATION_TRANSVERSE_1,
        ),
        CommonConsultationPeer::CONSULTATION_ACHAT_PUBLIQUE => array(
            CommonConsultationPeer::CONSULTATION_ACHAT_PUBLIQUE_0,
            CommonConsultationPeer::CONSULTATION_ACHAT_PUBLIQUE_1,
        ),
        CommonConsultationPeer::ACCESSIBILITE_FR => array(
            CommonConsultationPeer::ACCESSIBILITE_FR_0,
            CommonConsultationPeer::ACCESSIBILITE_FR_1,
        ),
        CommonConsultationPeer::ACCESSIBILITE_CZ => array(
            CommonConsultationPeer::ACCESSIBILITE_CZ_0,
            CommonConsultationPeer::ACCESSIBILITE_CZ_1,
        ),
        CommonConsultationPeer::ACCESSIBILITE_DU => array(
            CommonConsultationPeer::ACCESSIBILITE_DU_0,
            CommonConsultationPeer::ACCESSIBILITE_DU_1,
        ),
        CommonConsultationPeer::ACCESSIBILITE_SU => array(
            CommonConsultationPeer::ACCESSIBILITE_SU_0,
            CommonConsultationPeer::ACCESSIBILITE_SU_1,
        ),
        CommonConsultationPeer::ACCESSIBILITE_AR => array(
            CommonConsultationPeer::ACCESSIBILITE_AR_0,
            CommonConsultationPeer::ACCESSIBILITE_AR_1,
        ),
        CommonConsultationPeer::ALLOTI => array(
            CommonConsultationPeer::ALLOTI_0,
            CommonConsultationPeer::ALLOTI_1,
        ),
        CommonConsultationPeer::CONSULTATION_EXTERNE => array(
            CommonConsultationPeer::CONSULTATION_EXTERNE_0,
            CommonConsultationPeer::CONSULTATION_EXTERNE_1,
        ),
        CommonConsultationPeer::ECHANTILLON => array(
            CommonConsultationPeer::ECHANTILLON_0,
            CommonConsultationPeer::ECHANTILLON_1,
        ),
        CommonConsultationPeer::REUNION => array(
            CommonConsultationPeer::REUNION_0,
            CommonConsultationPeer::REUNION_1,
        ),
        CommonConsultationPeer::VISITES_LIEUX => array(
            CommonConsultationPeer::VISITES_LIEUX_0,
            CommonConsultationPeer::VISITES_LIEUX_1,
        ),
        CommonConsultationPeer::VARIANTE_CALCULE => array(
            CommonConsultationPeer::VARIANTE_CALCULE_0,
            CommonConsultationPeer::VARIANTE_CALCULE_1,
        ),
        CommonConsultationPeer::CONSULTATION_ANNULEE => array(
            CommonConsultationPeer::CONSULTATION_ANNULEE_0,
            CommonConsultationPeer::CONSULTATION_ANNULEE_1,
        ),
        CommonConsultationPeer::ACCESSIBILITE_IT => array(
            CommonConsultationPeer::ACCESSIBILITE_IT_0,
            CommonConsultationPeer::ACCESSIBILITE_IT_1,
        ),
        CommonConsultationPeer::CLAUSE_SOCIALE => array(
            CommonConsultationPeer::CLAUSE_SOCIALE_0,
            CommonConsultationPeer::CLAUSE_SOCIALE_1,
            CommonConsultationPeer::CLAUSE_SOCIALE_2,
        ),
        CommonConsultationPeer::CLAUSE_ENVIRONNEMENTALE => array(
            CommonConsultationPeer::CLAUSE_ENVIRONNEMENTALE_0,
            CommonConsultationPeer::CLAUSE_ENVIRONNEMENTALE_1,
            CommonConsultationPeer::CLAUSE_ENVIRONNEMENTALE_2,
        ),
        CommonConsultationPeer::SIGNATURE_ACTE_ENGAGEMENT => array(
            CommonConsultationPeer::SIGNATURE_ACTE_ENGAGEMENT_0,
            CommonConsultationPeer::SIGNATURE_ACTE_ENGAGEMENT_1,
        ),
        CommonConsultationPeer::DECISION_PARTIELLE => array(
            CommonConsultationPeer::DECISION_PARTIELLE_0,
            CommonConsultationPeer::DECISION_PARTIELLE_1,
        ),
        CommonConsultationPeer::TYPE_DECISION_A_RENSEIGNER => array(
            CommonConsultationPeer::TYPE_DECISION_A_RENSEIGNER_0,
            CommonConsultationPeer::TYPE_DECISION_A_RENSEIGNER_1,
        ),
        CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_MARCHE => array(
            CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_MARCHE_0,
            CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_MARCHE_1,
        ),
        CommonConsultationPeer::TYPE_DECISION_DECLARATION_SANS_SUITE => array(
            CommonConsultationPeer::TYPE_DECISION_DECLARATION_SANS_SUITE_0,
            CommonConsultationPeer::TYPE_DECISION_DECLARATION_SANS_SUITE_1,
        ),
        CommonConsultationPeer::TYPE_DECISION_DECLARATION_INFRUCTUEUX => array(
            CommonConsultationPeer::TYPE_DECISION_DECLARATION_INFRUCTUEUX_0,
            CommonConsultationPeer::TYPE_DECISION_DECLARATION_INFRUCTUEUX_1,
        ),
        CommonConsultationPeer::TYPE_DECISION_SELECTION_ENTREPRISE => array(
            CommonConsultationPeer::TYPE_DECISION_SELECTION_ENTREPRISE_0,
            CommonConsultationPeer::TYPE_DECISION_SELECTION_ENTREPRISE_1,
        ),
        CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE => array(
            CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE_0,
            CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE_1,
        ),
        CommonConsultationPeer::TYPE_DECISION_ADMISSION_SAD => array(
            CommonConsultationPeer::TYPE_DECISION_ADMISSION_SAD_0,
            CommonConsultationPeer::TYPE_DECISION_ADMISSION_SAD_1,
        ),
        CommonConsultationPeer::TYPE_DECISION_AUTRE => array(
            CommonConsultationPeer::TYPE_DECISION_AUTRE_0,
            CommonConsultationPeer::TYPE_DECISION_AUTRE_1,
        ),
        CommonConsultationPeer::DOSSIER_ADDITIF => array(
            CommonConsultationPeer::DOSSIER_ADDITIF_0,
            CommonConsultationPeer::DOSSIER_ADDITIF_1,
        ),
        CommonConsultationPeer::DELAI_PARTIEL => array(
            CommonConsultationPeer::DELAI_PARTIEL_0,
            CommonConsultationPeer::DELAI_PARTIEL_1,
        ),
        CommonConsultationPeer::ALERTE => array(
            CommonConsultationPeer::ALERTE_0,
            CommonConsultationPeer::ALERTE_1,
        ),
        CommonConsultationPeer::DOUBLON => array(
            CommonConsultationPeer::DOUBLON_0,
            CommonConsultationPeer::DOUBLON_1,
        ),
        CommonConsultationPeer::ENTITE_ADJUDICATRICE => array(
            CommonConsultationPeer::ENTITE_ADJUDICATRICE_0,
            CommonConsultationPeer::ENTITE_ADJUDICATRICE_1,
        ),
        CommonConsultationPeer::DONNEE_COMPLEMENTAIRE_OBLIGATOIRE => array(
            CommonConsultationPeer::DONNEE_COMPLEMENTAIRE_OBLIGATOIRE_0,
            CommonConsultationPeer::DONNEE_COMPLEMENTAIRE_OBLIGATOIRE_1,
        ),
        CommonConsultationPeer::MODE_OUVERTURE_REPONSE => array(
            CommonConsultationPeer::MODE_OUVERTURE_REPONSE_0,
            CommonConsultationPeer::MODE_OUVERTURE_REPONSE_1,
        ),
        CommonConsultationPeer::ETAT_EN_ATTENTE_VALIDATION => array(
            CommonConsultationPeer::ETAT_EN_ATTENTE_VALIDATION_0,
            CommonConsultationPeer::ETAT_EN_ATTENTE_VALIDATION_1,
        ),
        CommonConsultationPeer::MARCHE_PUBLIC_SIMPLIFIE => array(
            CommonConsultationPeer::MARCHE_PUBLIC_SIMPLIFIE_0,
            CommonConsultationPeer::MARCHE_PUBLIC_SIMPLIFIE_1,
        ),
        CommonConsultationPeer::DONNEE_PUBLICITE_OBLIGATOIRE => array(
            CommonConsultationPeer::DONNEE_PUBLICITE_OBLIGATOIRE_0,
            CommonConsultationPeer::DONNEE_PUBLICITE_OBLIGATOIRE_1,
        ),
        CommonConsultationPeer::DUME_DEMANDE => array(
            CommonConsultationPeer::DUME_DEMANDE_0,
            CommonConsultationPeer::DUME_DEMANDE_1,
        ),
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = CommonConsultationPeer::getFieldNames($toType);
        $key = isset(CommonConsultationPeer::$fieldKeys[$fromType][$name]) ? CommonConsultationPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(CommonConsultationPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, CommonConsultationPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return CommonConsultationPeer::$fieldNames[$type];
    }

    /**
     * Gets the list of values for all ENUM columns
     * @return array
     */
    public static function getValueSets()
    {
      return CommonConsultationPeer::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM column
     *
     * @param string $colname The ENUM column name.
     *
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = CommonConsultationPeer::getValueSets();

        if (!isset($valueSets[$colname])) {
            throw new PropelException(sprintf('Column "%s" has no ValueSet.', $colname));
        }

        return $valueSets[$colname];
    }

    /**
     * Gets the SQL value for the ENUM column value
     *
     * @param string $colname ENUM column name.
     * @param string $enumVal ENUM value.
     *
     * @return int SQL value
     */
    public static function getSqlValueForEnum($colname, $enumVal)
    {
        $values = CommonConsultationPeer::getValueSet($colname);
        if (!in_array($enumVal, $values)) {
            throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $colname));
        }

        return array_search($enumVal, $values);
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. CommonConsultationPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(CommonConsultationPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CommonConsultationPeer::ID);
            $criteria->addSelectColumn(CommonConsultationPeer::REFERENCE);
            $criteria->addSelectColumn(CommonConsultationPeer::CODE_EXTERNE);
            $criteria->addSelectColumn(CommonConsultationPeer::ORGANISME);
            $criteria->addSelectColumn(CommonConsultationPeer::REFERENCE_UTILISATEUR);
            $criteria->addSelectColumn(CommonConsultationPeer::CATEGORIE);
            $criteria->addSelectColumn(CommonConsultationPeer::TITRE);
            $criteria->addSelectColumn(CommonConsultationPeer::RESUME);
            $criteria->addSelectColumn(CommonConsultationPeer::DATEDEBUT);
            $criteria->addSelectColumn(CommonConsultationPeer::DATEFIN);
            $criteria->addSelectColumn(CommonConsultationPeer::DATEVALIDATION);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_PROCEDURE);
            $criteria->addSelectColumn(CommonConsultationPeer::CODE_PROCEDURE);
            $criteria->addSelectColumn(CommonConsultationPeer::REPONSE_ELECTRONIQUE);
            $criteria->addSelectColumn(CommonConsultationPeer::NUM_PROCEDURE);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_TYPE_PROCEDURE);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_TYPE_AVIS);
            $criteria->addSelectColumn(CommonConsultationPeer::LIEU_EXECUTION);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_MISE_EN_LIGNE);
            $criteria->addSelectColumn(CommonConsultationPeer::DATEMISEENLIGNE);
            $criteria->addSelectColumn(CommonConsultationPeer::IS_TIERS_AVIS);
            $criteria->addSelectColumn(CommonConsultationPeer::URL);
            $criteria->addSelectColumn(CommonConsultationPeer::DATEFIN_SAD);
            $criteria->addSelectColumn(CommonConsultationPeer::IS_SYS_ACQ_DYN);
            $criteria->addSelectColumn(CommonConsultationPeer::REFERENCE_CONSULTATION_INIT);
            $criteria->addSelectColumn(CommonConsultationPeer::SIGNATURE_OFFRE);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_TYPE_VALIDATION);
            $criteria->addSelectColumn(CommonConsultationPeer::ETAT_APPROBATION);
            $criteria->addSelectColumn(CommonConsultationPeer::ETAT_VALIDATION);
            $criteria->addSelectColumn(CommonConsultationPeer::CHAMP_SUPP_INVISIBLE);
            $criteria->addSelectColumn(CommonConsultationPeer::CODE_CPV_1);
            $criteria->addSelectColumn(CommonConsultationPeer::CODE_CPV_2);
            $criteria->addSelectColumn(CommonConsultationPeer::PUBLICATION_EUROPE);
            $criteria->addSelectColumn(CommonConsultationPeer::ETAT_PUBLICATION);
            $criteria->addSelectColumn(CommonConsultationPeer::POURSUIVRE_AFFICHAGE);
            $criteria->addSelectColumn(CommonConsultationPeer::POURSUIVRE_AFFICHAGE_UNITE);
            $criteria->addSelectColumn(CommonConsultationPeer::NBR_TELECHARGEMENT_DCE);
            $criteria->addSelectColumn(CommonConsultationPeer::OLD_SERVICE_ID);
            $criteria->addSelectColumn(CommonConsultationPeer::OLD_SERVICE_ASSOCIE_ID);
            $criteria->addSelectColumn(CommonConsultationPeer::DETAIL_CONSULTATION);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_FIN_AFFICHAGE);
            $criteria->addSelectColumn(CommonConsultationPeer::DEPOUILLABLE_PHASE_CONSULTATION);
            $criteria->addSelectColumn(CommonConsultationPeer::CONSULTATION_TRANSVERSE);
            $criteria->addSelectColumn(CommonConsultationPeer::CONSULTATION_ACHAT_PUBLIQUE);
            $criteria->addSelectColumn(CommonConsultationPeer::URL_CONSULTATION_ACHAT_PUBLIQUE);
            $criteria->addSelectColumn(CommonConsultationPeer::PARTIAL_DCE_DOWNLOAD);
            $criteria->addSelectColumn(CommonConsultationPeer::TIRAGE_PLAN);
            $criteria->addSelectColumn(CommonConsultationPeer::TIREUR_PLAN);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_MISE_EN_LIGNE_CALCULE);
            $criteria->addSelectColumn(CommonConsultationPeer::ACCESSIBILITE_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::ACCESSIBILITE_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::NBR_REPONSE);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_TYPE_PROCEDURE_ORG);
            $criteria->addSelectColumn(CommonConsultationPeer::ORGANISME_CONSULTATION_INIT);
            $criteria->addSelectColumn(CommonConsultationPeer::TIRAGE_DESCRIPTIF);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_VALIDATION_INTERMEDIAIRE);
            $criteria->addSelectColumn(CommonConsultationPeer::ACCESSIBILITE_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_TR_ACCESSIBILITE);
            $criteria->addSelectColumn(CommonConsultationPeer::ACCESSIBILITE_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::ACCESSIBILITE_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::ACCESSIBILITE_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::ACCESSIBILITE_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::ALLOTI);
            $criteria->addSelectColumn(CommonConsultationPeer::NUMERO_PHASE);
            $criteria->addSelectColumn(CommonConsultationPeer::CONSULTATION_EXTERNE);
            $criteria->addSelectColumn(CommonConsultationPeer::URL_CONSULTATION_EXTERNE);
            $criteria->addSelectColumn(CommonConsultationPeer::ORG_DENOMINATION);
            $criteria->addSelectColumn(CommonConsultationPeer::DOMAINES_ACTIVITES);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_AFFAIRE);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS);
            $criteria->addSelectColumn(CommonConsultationPeer::CAUTION_PROVISOIRE);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_DEPOT_OFFRES);
            $criteria->addSelectColumn(CommonConsultationPeer::LIEU_OUVERTURE_PLIS);
            $criteria->addSelectColumn(CommonConsultationPeer::PRIX_AQUISITION_PLANS);
            $criteria->addSelectColumn(CommonConsultationPeer::QUALIFICATION);
            $criteria->addSelectColumn(CommonConsultationPeer::AGREMENTS);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_ECHANTILLION);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_LIMITE_ECHANTILLION);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_REUNION);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_REUNION);
            $criteria->addSelectColumn(CommonConsultationPeer::VARIANTES);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::LIEU_OUVERTURE_PLIS_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADMIN);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADMIN_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADMIN_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADMIN_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADMIN_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADMIN_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADMIN_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADMIN_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_TECH);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_TECH_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_TECH_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_TECH_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_TECH_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_TECH_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_TECH_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_TECH_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADDITIF);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_RPA);
            $criteria->addSelectColumn(CommonConsultationPeer::DETAIL_CONSULTATION_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::DETAIL_CONSULTATION_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::DETAIL_CONSULTATION_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::DETAIL_CONSULTATION_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::DETAIL_CONSULTATION_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::DETAIL_CONSULTATION_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::DETAIL_CONSULTATION_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::ECHANTILLON);
            $criteria->addSelectColumn(CommonConsultationPeer::REUNION);
            $criteria->addSelectColumn(CommonConsultationPeer::VISITES_LIEUX);
            $criteria->addSelectColumn(CommonConsultationPeer::VARIANTE_CALCULE);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::LIEU_OUVERTURE_PLIS_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::LIEU_OUVERTURE_PLIS_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::LIEU_OUVERTURE_PLIS_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::LIEU_OUVERTURE_PLIS_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::LIEU_OUVERTURE_PLIS_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::LIEU_OUVERTURE_PLIS_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_ECHANTILLION_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_ECHANTILLION_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_ECHANTILLION_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_ECHANTILLION_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_ECHANTILLION_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_ECHANTILLION_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_ECHANTILLION_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_REUNION_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_REUNION_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_REUNION_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_REUNION_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_REUNION_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_REUNION_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_REUNION_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::MODE_PASSATION);
            $criteria->addSelectColumn(CommonConsultationPeer::CONSULTATION_ANNULEE);
            $criteria->addSelectColumn(CommonConsultationPeer::ACCESSIBILITE_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_DEPOT_OFFRES_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::LIEU_OUVERTURE_PLIS_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::ADRESSE_RETRAIS_DOSSIERS_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADMIN_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_TECH_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::PIECES_DOSSIER_ADDITIF_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::DETAIL_CONSULTATION_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_ECHANTILLION_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::ADD_REUNION_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::CODES_NUTS);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_DECISION);
            $criteria->addSelectColumn(CommonConsultationPeer::INTITULE);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_TR_INTITULE);
            $criteria->addSelectColumn(CommonConsultationPeer::OBJET);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_TR_OBJET);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_ACCES);
            $criteria->addSelectColumn(CommonConsultationPeer::AUTORISER_REPONSE_ELECTRONIQUE);
            $criteria->addSelectColumn(CommonConsultationPeer::REGLE_MISE_EN_LIGNE);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_REGLE_VALIDATION);
            $criteria->addSelectColumn(CommonConsultationPeer::INTITULE_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::INTITULE_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::INTITULE_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::INTITULE_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::INTITULE_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::INTITULE_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::INTITULE_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::INTITULE_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::OBJET_FR);
            $criteria->addSelectColumn(CommonConsultationPeer::OBJET_EN);
            $criteria->addSelectColumn(CommonConsultationPeer::OBJET_ES);
            $criteria->addSelectColumn(CommonConsultationPeer::OBJET_SU);
            $criteria->addSelectColumn(CommonConsultationPeer::OBJET_DU);
            $criteria->addSelectColumn(CommonConsultationPeer::OBJET_CZ);
            $criteria->addSelectColumn(CommonConsultationPeer::OBJET_AR);
            $criteria->addSelectColumn(CommonConsultationPeer::OBJET_IT);
            $criteria->addSelectColumn(CommonConsultationPeer::CLAUSE_SOCIALE);
            $criteria->addSelectColumn(CommonConsultationPeer::CLAUSE_ENVIRONNEMENTALE);
            $criteria->addSelectColumn(CommonConsultationPeer::REPONSE_OBLIGATOIRE);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_ENVOI);
            $criteria->addSelectColumn(CommonConsultationPeer::CHIFFREMENT_OFFRE);
            $criteria->addSelectColumn(CommonConsultationPeer::ENV_CANDIDATURE);
            $criteria->addSelectColumn(CommonConsultationPeer::ENV_OFFRE);
            $criteria->addSelectColumn(CommonConsultationPeer::ENV_ANONYMAT);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_ETAT_CONSULTATION);
            $criteria->addSelectColumn(CommonConsultationPeer::REFERENCE_CONNECTEUR);
            $criteria->addSelectColumn(CommonConsultationPeer::CONS_STATUT);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_APPROBATEUR);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_VALIDEUR);
            $criteria->addSelectColumn(CommonConsultationPeer::OLD_SERVICE_VALIDATION);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_CREATEUR);
            $criteria->addSelectColumn(CommonConsultationPeer::NOM_CREATEUR);
            $criteria->addSelectColumn(CommonConsultationPeer::PRENOM_CREATEUR);
            $criteria->addSelectColumn(CommonConsultationPeer::SIGNATURE_ACTE_ENGAGEMENT);
            $criteria->addSelectColumn(CommonConsultationPeer::ARCHIVEMETADESCRIPTION);
            $criteria->addSelectColumn(CommonConsultationPeer::ARCHIVEMETAMOTSCLEF);
            $criteria->addSelectColumn(CommonConsultationPeer::ARCHIVEIDBLOBZIP);
            $criteria->addSelectColumn(CommonConsultationPeer::DECISION_PARTIELLE);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_DECISION_A_RENSEIGNER);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_MARCHE);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_DECISION_DECLARATION_SANS_SUITE);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_DECISION_DECLARATION_INFRUCTUEUX);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_DECISION_SELECTION_ENTREPRISE);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_DECISION_ADMISSION_SAD);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_DECISION_AUTRE);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_ARCHIVEUR);
            $criteria->addSelectColumn(CommonConsultationPeer::PRENOM_NOM_AGENT_TELECHARGEMENT_PLIS);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_AGENT_TELECHARGEMENT_PLIS);
            $criteria->addSelectColumn(CommonConsultationPeer::PATH_TELECHARGEMENT_PLIS);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_TELECHARGEMENT_PLIS);
            $criteria->addSelectColumn(CommonConsultationPeer::OLD_SERVICE_VALIDATION_INTERMEDIAIRE);
            $criteria->addSelectColumn(CommonConsultationPeer::ENV_OFFRE_TECHNIQUE);
            $criteria->addSelectColumn(CommonConsultationPeer::REF_ORG_PARTENAIRE);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_ARCHIVAGE);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_DECISION_ANNULATION);
            $criteria->addSelectColumn(CommonConsultationPeer::COMMENTAIRE_ANNULATION);
            $criteria->addSelectColumn(CommonConsultationPeer::COMPTE_BOAMP_ASSOCIE);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_MISE_EN_LIGNE_SOUHAITEE);
            $criteria->addSelectColumn(CommonConsultationPeer::AUTORISER_PUBLICITE);
            $criteria->addSelectColumn(CommonConsultationPeer::DOSSIER_ADDITIF);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_MARCHE);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_PRESTATION);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_MODIFICATION);
            $criteria->addSelectColumn(CommonConsultationPeer::DELAI_PARTIEL);
            $criteria->addSelectColumn(CommonConsultationPeer::DATEFINLOCALE);
            $criteria->addSelectColumn(CommonConsultationPeer::LIEURESIDENCE);
            $criteria->addSelectColumn(CommonConsultationPeer::ALERTE);
            $criteria->addSelectColumn(CommonConsultationPeer::DOUBLON);
            $criteria->addSelectColumn(CommonConsultationPeer::DENOMINATION_ADAPTE);
            $criteria->addSelectColumn(CommonConsultationPeer::URL_CONSULTATION_AVIS_PUB);
            $criteria->addSelectColumn(CommonConsultationPeer::DOUBLON_DE);
            $criteria->addSelectColumn(CommonConsultationPeer::ENTITE_ADJUDICATRICE);
            $criteria->addSelectColumn(CommonConsultationPeer::CODE_OPERATION);
            $criteria->addSelectColumn(CommonConsultationPeer::CLAUSE_SOCIALE_CONDITION_EXECUTION);
            $criteria->addSelectColumn(CommonConsultationPeer::CLAUSE_SOCIALE_INSERTION);
            $criteria->addSelectColumn(CommonConsultationPeer::CLAUSE_SOCIALE_ATELIERS_PROTEGES);
            $criteria->addSelectColumn(CommonConsultationPeer::CLAUSE_SOCIALE_SIAE);
            $criteria->addSelectColumn(CommonConsultationPeer::CLAUSE_SOCIALE_ESS);
            $criteria->addSelectColumn(CommonConsultationPeer::CLAUSE_ENV_SPECS_TECHNIQUES);
            $criteria->addSelectColumn(CommonConsultationPeer::CLAUSE_ENV_COND_EXECUTION);
            $criteria->addSelectColumn(CommonConsultationPeer::CLAUSE_ENV_CRITERES_SELECT);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_DONNEE_COMPLEMENTAIRE);
            $criteria->addSelectColumn(CommonConsultationPeer::DONNEE_COMPLEMENTAIRE_OBLIGATOIRE);
            $criteria->addSelectColumn(CommonConsultationPeer::MODE_OUVERTURE_REPONSE);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_FICHIER_ANNULATION);
            $criteria->addSelectColumn(CommonConsultationPeer::IDOPERATION);
            $criteria->addSelectColumn(CommonConsultationPeer::ETAT_EN_ATTENTE_VALIDATION);
            $criteria->addSelectColumn(CommonConsultationPeer::INFOS_BLOCS_ATLAS);
            $criteria->addSelectColumn(CommonConsultationPeer::MARCHE_PUBLIC_SIMPLIFIE);
            $criteria->addSelectColumn(CommonConsultationPeer::DATE_FIN_UNIX);
            $criteria->addSelectColumn(CommonConsultationPeer::NUMERO_AC);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_CONTRAT);
            $criteria->addSelectColumn(CommonConsultationPeer::PIN_API_SGMAP_MPS);
            $criteria->addSelectColumn(CommonConsultationPeer::DONNEE_PUBLICITE_OBLIGATOIRE);
            $criteria->addSelectColumn(CommonConsultationPeer::DUME_DEMANDE);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_PROCEDURE_DUME);
            $criteria->addSelectColumn(CommonConsultationPeer::MARCHE_INSERTION);
            $criteria->addSelectColumn(CommonConsultationPeer::CLAUSE_SPECIFICATION_TECHNIQUE);
            $criteria->addSelectColumn(CommonConsultationPeer::TYPE_FORMULAIRE_DUME);
            $criteria->addSelectColumn(CommonConsultationPeer::SOURCE_EXTERNE);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_SOURCE_EXTERNE);
            $criteria->addSelectColumn(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX);
            $criteria->addSelectColumn(CommonConsultationPeer::ATTESTATION_CONSULTATION);
            $criteria->addSelectColumn(CommonConsultationPeer::VERSION_MESSAGERIE);
            $criteria->addSelectColumn(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID);
            $criteria->addSelectColumn(CommonConsultationPeer::IS_ENVOI_PUBLICITE_VALIDATION);
            $criteria->addSelectColumn(CommonConsultationPeer::ANNEXE_FINANCIERE);
            $criteria->addSelectColumn(CommonConsultationPeer::ENVOL_ACTIVATION);
            $criteria->addSelectColumn(CommonConsultationPeer::SERVICE_ID);
            $criteria->addSelectColumn(CommonConsultationPeer::SERVICE_ASSOCIE_ID);
            $criteria->addSelectColumn(CommonConsultationPeer::GROUPEMENT);
            $criteria->addSelectColumn(CommonConsultationPeer::SERVICE_VALIDATION);
            $criteria->addSelectColumn(CommonConsultationPeer::AUTRE_TECHNIQUE_ACHAT);
            $criteria->addSelectColumn(CommonConsultationPeer::PROCEDURE_OUVERTE);
            $criteria->addSelectColumn(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE);
            $criteria->addSelectColumn(CommonConsultationPeer::CMS_ACTIF);
            $criteria->addSelectColumn(CommonConsultationPeer::CONTROLE_TAILLE_DEPOT);
            $criteria->addSelectColumn(CommonConsultationPeer::CODE_DCE_RESTREINT);
            $criteria->addSelectColumn(CommonConsultationPeer::NUMERO_PROJET_ACHAT);
            $criteria->addSelectColumn(CommonConsultationPeer::REFERENTIEL_ACHAT_ID);
            $criteria->addSelectColumn(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR);
            $criteria->addSelectColumn(CommonConsultationPeer::UUID);
            $criteria->addSelectColumn(CommonConsultationPeer::CONTRAT_EXEC_UUID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.reference');
            $criteria->addSelectColumn($alias . '.code_externe');
            $criteria->addSelectColumn($alias . '.organisme');
            $criteria->addSelectColumn($alias . '.reference_utilisateur');
            $criteria->addSelectColumn($alias . '.categorie');
            $criteria->addSelectColumn($alias . '.titre');
            $criteria->addSelectColumn($alias . '.resume');
            $criteria->addSelectColumn($alias . '.datedebut');
            $criteria->addSelectColumn($alias . '.datefin');
            $criteria->addSelectColumn($alias . '.datevalidation');
            $criteria->addSelectColumn($alias . '.type_procedure');
            $criteria->addSelectColumn($alias . '.code_procedure');
            $criteria->addSelectColumn($alias . '.reponse_electronique');
            $criteria->addSelectColumn($alias . '.num_procedure');
            $criteria->addSelectColumn($alias . '.id_type_procedure');
            $criteria->addSelectColumn($alias . '.id_type_avis');
            $criteria->addSelectColumn($alias . '.lieu_execution');
            $criteria->addSelectColumn($alias . '.type_mise_en_ligne');
            $criteria->addSelectColumn($alias . '.datemiseenligne');
            $criteria->addSelectColumn($alias . '.is_tiers_avis');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.datefin_sad');
            $criteria->addSelectColumn($alias . '.is_sys_acq_dyn');
            $criteria->addSelectColumn($alias . '.reference_consultation_init');
            $criteria->addSelectColumn($alias . '.signature_offre');
            $criteria->addSelectColumn($alias . '.id_type_validation');
            $criteria->addSelectColumn($alias . '.etat_approbation');
            $criteria->addSelectColumn($alias . '.etat_validation');
            $criteria->addSelectColumn($alias . '.champ_supp_invisible');
            $criteria->addSelectColumn($alias . '.code_cpv_1');
            $criteria->addSelectColumn($alias . '.code_cpv_2');
            $criteria->addSelectColumn($alias . '.publication_europe');
            $criteria->addSelectColumn($alias . '.etat_publication');
            $criteria->addSelectColumn($alias . '.poursuivre_affichage');
            $criteria->addSelectColumn($alias . '.poursuivre_affichage_unite');
            $criteria->addSelectColumn($alias . '.nbr_telechargement_dce');
            $criteria->addSelectColumn($alias . '.old_service_id');
            $criteria->addSelectColumn($alias . '.old_service_associe_id');
            $criteria->addSelectColumn($alias . '.detail_consultation');
            $criteria->addSelectColumn($alias . '.date_fin_affichage');
            $criteria->addSelectColumn($alias . '.depouillable_phase_consultation');
            $criteria->addSelectColumn($alias . '.consultation_transverse');
            $criteria->addSelectColumn($alias . '.consultation_achat_publique');
            $criteria->addSelectColumn($alias . '.url_consultation_achat_publique');
            $criteria->addSelectColumn($alias . '.partial_dce_download');
            $criteria->addSelectColumn($alias . '.tirage_plan');
            $criteria->addSelectColumn($alias . '.tireur_plan');
            $criteria->addSelectColumn($alias . '.date_mise_en_ligne_calcule');
            $criteria->addSelectColumn($alias . '.accessibilite_en');
            $criteria->addSelectColumn($alias . '.accessibilite_es');
            $criteria->addSelectColumn($alias . '.nbr_reponse');
            $criteria->addSelectColumn($alias . '.id_type_procedure_org');
            $criteria->addSelectColumn($alias . '.organisme_consultation_init');
            $criteria->addSelectColumn($alias . '.tirage_descriptif');
            $criteria->addSelectColumn($alias . '.date_validation_intermediaire');
            $criteria->addSelectColumn($alias . '.accessibilite_fr');
            $criteria->addSelectColumn($alias . '.id_tr_accessibilite');
            $criteria->addSelectColumn($alias . '.accessibilite_cz');
            $criteria->addSelectColumn($alias . '.accessibilite_du');
            $criteria->addSelectColumn($alias . '.accessibilite_su');
            $criteria->addSelectColumn($alias . '.accessibilite_ar');
            $criteria->addSelectColumn($alias . '.alloti');
            $criteria->addSelectColumn($alias . '.numero_phase');
            $criteria->addSelectColumn($alias . '.consultation_externe');
            $criteria->addSelectColumn($alias . '.url_consultation_externe');
            $criteria->addSelectColumn($alias . '.org_denomination');
            $criteria->addSelectColumn($alias . '.domaines_activites');
            $criteria->addSelectColumn($alias . '.id_affaire');
            $criteria->addSelectColumn($alias . '.adresse_retrais_dossiers');
            $criteria->addSelectColumn($alias . '.caution_provisoire');
            $criteria->addSelectColumn($alias . '.adresse_depot_offres');
            $criteria->addSelectColumn($alias . '.lieu_ouverture_plis');
            $criteria->addSelectColumn($alias . '.prix_aquisition_plans');
            $criteria->addSelectColumn($alias . '.qualification');
            $criteria->addSelectColumn($alias . '.agrements');
            $criteria->addSelectColumn($alias . '.add_echantillion');
            $criteria->addSelectColumn($alias . '.date_limite_echantillion');
            $criteria->addSelectColumn($alias . '.add_reunion');
            $criteria->addSelectColumn($alias . '.date_reunion');
            $criteria->addSelectColumn($alias . '.variantes');
            $criteria->addSelectColumn($alias . '.adresse_depot_offres_ar');
            $criteria->addSelectColumn($alias . '.lieu_ouverture_plis_ar');
            $criteria->addSelectColumn($alias . '.adresse_retrais_dossiers_ar');
            $criteria->addSelectColumn($alias . '.pieces_dossier_admin');
            $criteria->addSelectColumn($alias . '.pieces_dossier_admin_fr');
            $criteria->addSelectColumn($alias . '.pieces_dossier_admin_en');
            $criteria->addSelectColumn($alias . '.pieces_dossier_admin_es');
            $criteria->addSelectColumn($alias . '.pieces_dossier_admin_su');
            $criteria->addSelectColumn($alias . '.pieces_dossier_admin_du');
            $criteria->addSelectColumn($alias . '.pieces_dossier_admin_cz');
            $criteria->addSelectColumn($alias . '.pieces_dossier_admin_ar');
            $criteria->addSelectColumn($alias . '.pieces_dossier_tech');
            $criteria->addSelectColumn($alias . '.pieces_dossier_tech_fr');
            $criteria->addSelectColumn($alias . '.pieces_dossier_tech_en');
            $criteria->addSelectColumn($alias . '.pieces_dossier_tech_es');
            $criteria->addSelectColumn($alias . '.pieces_dossier_tech_su');
            $criteria->addSelectColumn($alias . '.pieces_dossier_tech_du');
            $criteria->addSelectColumn($alias . '.pieces_dossier_tech_cz');
            $criteria->addSelectColumn($alias . '.pieces_dossier_tech_ar');
            $criteria->addSelectColumn($alias . '.pieces_dossier_additif');
            $criteria->addSelectColumn($alias . '.pieces_dossier_additif_fr');
            $criteria->addSelectColumn($alias . '.pieces_dossier_additif_en');
            $criteria->addSelectColumn($alias . '.pieces_dossier_additif_es');
            $criteria->addSelectColumn($alias . '.pieces_dossier_additif_su');
            $criteria->addSelectColumn($alias . '.pieces_dossier_additif_du');
            $criteria->addSelectColumn($alias . '.pieces_dossier_additif_cz');
            $criteria->addSelectColumn($alias . '.pieces_dossier_additif_ar');
            $criteria->addSelectColumn($alias . '.id_rpa');
            $criteria->addSelectColumn($alias . '.detail_consultation_fr');
            $criteria->addSelectColumn($alias . '.detail_consultation_en');
            $criteria->addSelectColumn($alias . '.detail_consultation_es');
            $criteria->addSelectColumn($alias . '.detail_consultation_su');
            $criteria->addSelectColumn($alias . '.detail_consultation_du');
            $criteria->addSelectColumn($alias . '.detail_consultation_cz');
            $criteria->addSelectColumn($alias . '.detail_consultation_ar');
            $criteria->addSelectColumn($alias . '.echantillon');
            $criteria->addSelectColumn($alias . '.reunion');
            $criteria->addSelectColumn($alias . '.visites_lieux');
            $criteria->addSelectColumn($alias . '.variante_calcule');
            $criteria->addSelectColumn($alias . '.adresse_retrais_dossiers_fr');
            $criteria->addSelectColumn($alias . '.adresse_retrais_dossiers_en');
            $criteria->addSelectColumn($alias . '.adresse_retrais_dossiers_es');
            $criteria->addSelectColumn($alias . '.adresse_retrais_dossiers_su');
            $criteria->addSelectColumn($alias . '.adresse_retrais_dossiers_du');
            $criteria->addSelectColumn($alias . '.adresse_retrais_dossiers_cz');
            $criteria->addSelectColumn($alias . '.adresse_depot_offres_fr');
            $criteria->addSelectColumn($alias . '.adresse_depot_offres_en');
            $criteria->addSelectColumn($alias . '.adresse_depot_offres_es');
            $criteria->addSelectColumn($alias . '.adresse_depot_offres_su');
            $criteria->addSelectColumn($alias . '.adresse_depot_offres_du');
            $criteria->addSelectColumn($alias . '.adresse_depot_offres_cz');
            $criteria->addSelectColumn($alias . '.lieu_ouverture_plis_fr');
            $criteria->addSelectColumn($alias . '.lieu_ouverture_plis_en');
            $criteria->addSelectColumn($alias . '.lieu_ouverture_plis_es');
            $criteria->addSelectColumn($alias . '.lieu_ouverture_plis_su');
            $criteria->addSelectColumn($alias . '.lieu_ouverture_plis_du');
            $criteria->addSelectColumn($alias . '.lieu_ouverture_plis_cz');
            $criteria->addSelectColumn($alias . '.add_echantillion_fr');
            $criteria->addSelectColumn($alias . '.add_echantillion_en');
            $criteria->addSelectColumn($alias . '.add_echantillion_es');
            $criteria->addSelectColumn($alias . '.add_echantillion_su');
            $criteria->addSelectColumn($alias . '.add_echantillion_du');
            $criteria->addSelectColumn($alias . '.add_echantillion_cz');
            $criteria->addSelectColumn($alias . '.add_echantillion_ar');
            $criteria->addSelectColumn($alias . '.add_reunion_fr');
            $criteria->addSelectColumn($alias . '.add_reunion_en');
            $criteria->addSelectColumn($alias . '.add_reunion_es');
            $criteria->addSelectColumn($alias . '.add_reunion_su');
            $criteria->addSelectColumn($alias . '.add_reunion_du');
            $criteria->addSelectColumn($alias . '.add_reunion_cz');
            $criteria->addSelectColumn($alias . '.add_reunion_ar');
            $criteria->addSelectColumn($alias . '.mode_passation');
            $criteria->addSelectColumn($alias . '.consultation_annulee');
            $criteria->addSelectColumn($alias . '.accessibilite_it');
            $criteria->addSelectColumn($alias . '.adresse_depot_offres_it');
            $criteria->addSelectColumn($alias . '.lieu_ouverture_plis_it');
            $criteria->addSelectColumn($alias . '.adresse_retrais_dossiers_it');
            $criteria->addSelectColumn($alias . '.pieces_dossier_admin_it');
            $criteria->addSelectColumn($alias . '.pieces_dossier_tech_it');
            $criteria->addSelectColumn($alias . '.pieces_dossier_additif_it');
            $criteria->addSelectColumn($alias . '.detail_consultation_it');
            $criteria->addSelectColumn($alias . '.add_echantillion_it');
            $criteria->addSelectColumn($alias . '.add_reunion_it');
            $criteria->addSelectColumn($alias . '.codes_nuts');
            $criteria->addSelectColumn($alias . '.date_decision');
            $criteria->addSelectColumn($alias . '.intitule');
            $criteria->addSelectColumn($alias . '.id_tr_intitule');
            $criteria->addSelectColumn($alias . '.objet');
            $criteria->addSelectColumn($alias . '.id_tr_objet');
            $criteria->addSelectColumn($alias . '.type_acces');
            $criteria->addSelectColumn($alias . '.autoriser_reponse_electronique');
            $criteria->addSelectColumn($alias . '.regle_mise_en_ligne');
            $criteria->addSelectColumn($alias . '.id_regle_validation');
            $criteria->addSelectColumn($alias . '.intitule_fr');
            $criteria->addSelectColumn($alias . '.intitule_en');
            $criteria->addSelectColumn($alias . '.intitule_es');
            $criteria->addSelectColumn($alias . '.intitule_su');
            $criteria->addSelectColumn($alias . '.intitule_du');
            $criteria->addSelectColumn($alias . '.intitule_cz');
            $criteria->addSelectColumn($alias . '.intitule_ar');
            $criteria->addSelectColumn($alias . '.intitule_it');
            $criteria->addSelectColumn($alias . '.objet_fr');
            $criteria->addSelectColumn($alias . '.objet_en');
            $criteria->addSelectColumn($alias . '.objet_es');
            $criteria->addSelectColumn($alias . '.objet_su');
            $criteria->addSelectColumn($alias . '.objet_du');
            $criteria->addSelectColumn($alias . '.objet_cz');
            $criteria->addSelectColumn($alias . '.objet_ar');
            $criteria->addSelectColumn($alias . '.objet_it');
            $criteria->addSelectColumn($alias . '.clause_sociale');
            $criteria->addSelectColumn($alias . '.clause_environnementale');
            $criteria->addSelectColumn($alias . '.reponse_obligatoire');
            $criteria->addSelectColumn($alias . '.type_envoi');
            $criteria->addSelectColumn($alias . '.chiffrement_offre');
            $criteria->addSelectColumn($alias . '.env_candidature');
            $criteria->addSelectColumn($alias . '.env_offre');
            $criteria->addSelectColumn($alias . '.env_anonymat');
            $criteria->addSelectColumn($alias . '.id_etat_consultation');
            $criteria->addSelectColumn($alias . '.reference_connecteur');
            $criteria->addSelectColumn($alias . '.cons_statut');
            $criteria->addSelectColumn($alias . '.id_approbateur');
            $criteria->addSelectColumn($alias . '.id_valideur');
            $criteria->addSelectColumn($alias . '.old_service_validation');
            $criteria->addSelectColumn($alias . '.id_createur');
            $criteria->addSelectColumn($alias . '.nom_createur');
            $criteria->addSelectColumn($alias . '.prenom_createur');
            $criteria->addSelectColumn($alias . '.signature_acte_engagement');
            $criteria->addSelectColumn($alias . '.archiveMetaDescription');
            $criteria->addSelectColumn($alias . '.archiveMetaMotsClef');
            $criteria->addSelectColumn($alias . '.archiveIdBlobZip');
            $criteria->addSelectColumn($alias . '.decision_partielle');
            $criteria->addSelectColumn($alias . '.type_decision_a_renseigner');
            $criteria->addSelectColumn($alias . '.type_decision_attribution_marche');
            $criteria->addSelectColumn($alias . '.type_decision_declaration_sans_suite');
            $criteria->addSelectColumn($alias . '.type_decision_declaration_infructueux');
            $criteria->addSelectColumn($alias . '.type_decision_selection_entreprise');
            $criteria->addSelectColumn($alias . '.type_decision_attribution_accord_cadre');
            $criteria->addSelectColumn($alias . '.type_decision_admission_sad');
            $criteria->addSelectColumn($alias . '.type_decision_autre');
            $criteria->addSelectColumn($alias . '.id_archiveur');
            $criteria->addSelectColumn($alias . '.prenom_nom_agent_telechargement_plis');
            $criteria->addSelectColumn($alias . '.id_agent_telechargement_plis');
            $criteria->addSelectColumn($alias . '.path_telechargement_plis');
            $criteria->addSelectColumn($alias . '.date_telechargement_plis');
            $criteria->addSelectColumn($alias . '.old_service_validation_intermediaire');
            $criteria->addSelectColumn($alias . '.env_offre_technique');
            $criteria->addSelectColumn($alias . '.ref_org_partenaire');
            $criteria->addSelectColumn($alias . '.date_archivage');
            $criteria->addSelectColumn($alias . '.date_decision_annulation');
            $criteria->addSelectColumn($alias . '.commentaire_annulation');
            $criteria->addSelectColumn($alias . '.compte_boamp_associe');
            $criteria->addSelectColumn($alias . '.date_mise_en_ligne_souhaitee');
            $criteria->addSelectColumn($alias . '.autoriser_publicite');
            $criteria->addSelectColumn($alias . '.dossier_additif');
            $criteria->addSelectColumn($alias . '.type_marche');
            $criteria->addSelectColumn($alias . '.type_prestation');
            $criteria->addSelectColumn($alias . '.date_modification');
            $criteria->addSelectColumn($alias . '.delai_partiel');
            $criteria->addSelectColumn($alias . '.dateFinLocale');
            $criteria->addSelectColumn($alias . '.lieuResidence');
            $criteria->addSelectColumn($alias . '.alerte');
            $criteria->addSelectColumn($alias . '.doublon');
            $criteria->addSelectColumn($alias . '.denomination_adapte');
            $criteria->addSelectColumn($alias . '.url_consultation_avis_pub');
            $criteria->addSelectColumn($alias . '.doublon_de');
            $criteria->addSelectColumn($alias . '.entite_adjudicatrice');
            $criteria->addSelectColumn($alias . '.code_operation');
            $criteria->addSelectColumn($alias . '.clause_sociale_condition_execution');
            $criteria->addSelectColumn($alias . '.clause_sociale_insertion');
            $criteria->addSelectColumn($alias . '.clause_sociale_ateliers_proteges');
            $criteria->addSelectColumn($alias . '.clause_sociale_siae');
            $criteria->addSelectColumn($alias . '.clause_sociale_ess');
            $criteria->addSelectColumn($alias . '.clause_env_specs_techniques');
            $criteria->addSelectColumn($alias . '.clause_env_cond_execution');
            $criteria->addSelectColumn($alias . '.clause_env_criteres_select');
            $criteria->addSelectColumn($alias . '.id_donnee_complementaire');
            $criteria->addSelectColumn($alias . '.donnee_complementaire_obligatoire');
            $criteria->addSelectColumn($alias . '.mode_ouverture_reponse');
            $criteria->addSelectColumn($alias . '.id_fichier_annulation');
            $criteria->addSelectColumn($alias . '.idOperation');
            $criteria->addSelectColumn($alias . '.etat_en_attente_validation');
            $criteria->addSelectColumn($alias . '.infos_blocs_atlas');
            $criteria->addSelectColumn($alias . '.marche_public_simplifie');
            $criteria->addSelectColumn($alias . '.DATE_FIN_UNIX');
            $criteria->addSelectColumn($alias . '.numero_AC');
            $criteria->addSelectColumn($alias . '.id_contrat');
            $criteria->addSelectColumn($alias . '.pin_api_sgmap_mps');
            $criteria->addSelectColumn($alias . '.donnee_publicite_obligatoire');
            $criteria->addSelectColumn($alias . '.dume_demande');
            $criteria->addSelectColumn($alias . '.type_procedure_dume');
            $criteria->addSelectColumn($alias . '.marche_insertion');
            $criteria->addSelectColumn($alias . '.clause_specification_technique');
            $criteria->addSelectColumn($alias . '.type_formulaire_dume');
            $criteria->addSelectColumn($alias . '.source_externe');
            $criteria->addSelectColumn($alias . '.id_source_externe');
            $criteria->addSelectColumn($alias . '.id_dossier_volumineux');
            $criteria->addSelectColumn($alias . '.attestation_consultation');
            $criteria->addSelectColumn($alias . '.version_messagerie');
            $criteria->addSelectColumn($alias . '.plateforme_virtuelle_id');
            $criteria->addSelectColumn($alias . '.is_envoi_publicite_validation');
            $criteria->addSelectColumn($alias . '.annexe_financiere');
            $criteria->addSelectColumn($alias . '.envol_activation');
            $criteria->addSelectColumn($alias . '.service_id');
            $criteria->addSelectColumn($alias . '.service_associe_id');
            $criteria->addSelectColumn($alias . '.groupement');
            $criteria->addSelectColumn($alias . '.service_validation');
            $criteria->addSelectColumn($alias . '.autre_technique_achat');
            $criteria->addSelectColumn($alias . '.procedure_ouverte');
            $criteria->addSelectColumn($alias . '.service_validation_intermediaire');
            $criteria->addSelectColumn($alias . '.cms_actif');
            $criteria->addSelectColumn($alias . '.controle_taille_depot');
            $criteria->addSelectColumn($alias . '.code_dce_restreint');
            $criteria->addSelectColumn($alias . '.numero_projet_achat');
            $criteria->addSelectColumn($alias . '.referentiel_achat_id');
            $criteria->addSelectColumn($alias . '.agent_technique_createur');
            $criteria->addSelectColumn($alias . '.uuid');
            $criteria->addSelectColumn($alias . '.contrat_exec_uuid');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 CommonConsultation
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = CommonConsultationPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return CommonConsultationPeer::populateObjects(CommonConsultationPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      CommonConsultation $obj A CommonConsultation object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getId();
            } // if key === null
            CommonConsultationPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A CommonConsultation object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof CommonConsultation) {
                $key = (string) $value->getId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or CommonConsultation object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(CommonConsultationPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   CommonConsultation Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(CommonConsultationPeer::$instances[$key])) {
                return CommonConsultationPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }

    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references) {
        foreach (CommonConsultationPeer::$instances as $instance) {
          $instance->clearAllReferences(true);
        }
      }
        CommonConsultationPeer::$instances = array();
    }

    /**
     * Method to invalidate the instance pool of all tables related to consultation
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in CommonConsultationTagsPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonConsultationTagsPeer::clearInstancePool();
        // Invalidate objects in CommonInterfaceSuiviPeer instance pool,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CommonInterfaceSuiviPeer::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (int) $row[$startcol];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = CommonConsultationPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = CommonConsultationPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CommonConsultationPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (CommonConsultation object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = CommonConsultationPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + CommonConsultationPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CommonConsultationPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            CommonConsultationPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonReferentielAchat table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonReferentielAchat(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonAgent table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonAgent(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonServiceRelatedByServiceValidation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonServiceRelatedByServiceValidation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTTypeContrat table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonTTypeContrat(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonServiceRelatedByServiceId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonServiceRelatedByServiceId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonServiceRelatedByServiceValidationIntermediaire table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonServiceRelatedByServiceValidationIntermediaire(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOperations table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonOperations(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOrganisme table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonOrganisme(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonPlateformeVirtuelle table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonPlateformeVirtuelle(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonDossierVolumineux table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinCommonDossierVolumineux(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with their CommonReferentielAchat objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonReferentielAchat(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonReferentielAchatPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultation) to $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with their CommonAgent objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonAgent(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonAgentPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonAgentPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonAgentPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonAgentPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultation) to $obj2 (CommonAgent)
                $obj2->addCommonConsultation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with their CommonService objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonServiceRelatedByServiceValidation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonServicePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonServicePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonServicePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultation) to $obj2 (CommonService)
                $obj2->addCommonConsultationRelatedByServiceValidation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with their CommonTTypeContrat objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonTTypeContrat(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonTTypeContratPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonTTypeContratPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonTTypeContratPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonTTypeContratPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultation) to $obj2 (CommonTTypeContrat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with their CommonService objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonServiceRelatedByServiceId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonServicePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonServicePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonServicePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultation) to $obj2 (CommonService)
                $obj2->addCommonConsultationRelatedByServiceId($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with their CommonService objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonServiceRelatedByServiceValidationIntermediaire(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonServicePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonServicePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonServicePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultation) to $obj2 (CommonService)
                $obj2->addCommonConsultationRelatedByServiceValidationIntermediaire($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with their CommonOperations objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonOperations(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonOperationsPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonOperationsPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonOperationsPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonOperationsPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultation) to $obj2 (CommonOperations)
                $obj2->addCommonConsultation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with their CommonOrganisme objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonOrganisme(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonOrganismePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonOrganismePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonOrganismePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonOrganismePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultation) to $obj2 (CommonOrganisme)
                $obj2->addCommonConsultation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with their CommonPlateformeVirtuelle objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonPlateformeVirtuelle(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultation) to $obj2 (CommonPlateformeVirtuelle)
                $obj2->addCommonConsultation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with their CommonDossierVolumineux objects.
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinCommonDossierVolumineux(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;
        CommonDossierVolumineuxPeer::addSelectColumns($criteria);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {

                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if $obj1 already loaded

            $key2 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol);
            if ($key2 !== null) {
                $obj2 = CommonDossierVolumineuxPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 already loaded

                // Add the $obj1 (CommonConsultation) to $obj2 (CommonDossierVolumineux)
                $obj2->addCommonConsultation($obj1);

            } // if joined row was not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining all related tables
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAll(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }

    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAll(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielAchatPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonOperationsPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonOperationsPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol11 = $startcol10 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonDossierVolumineuxPeer::addSelectColumns($criteria);
        $startcol12 = $startcol11 + CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

            // Add objects for joined CommonReferentielAchat rows

            $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
            if ($key2 !== null) {
                $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                if (!$obj2) {

                    $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if obj2 loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);
            } // if joined row not null

            // Add objects for joined CommonAgent rows

            $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
            if ($key3 !== null) {
                $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                if (!$obj3) {

                    $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if obj3 loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonConsultation($obj1);
            } // if joined row not null

            // Add objects for joined CommonService rows

            $key4 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol4);
            if ($key4 !== null) {
                $obj4 = CommonServicePeer::getInstanceFromPool($key4);
                if (!$obj4) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonServicePeer::addInstanceToPool($obj4, $key4);
                } // if obj4 loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonService)
                $obj4->addCommonConsultationRelatedByServiceValidation($obj1);
            } // if joined row not null

            // Add objects for joined CommonTTypeContrat rows

            $key5 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
            if ($key5 !== null) {
                $obj5 = CommonTTypeContratPeer::getInstanceFromPool($key5);
                if (!$obj5) {

                    $cls = CommonTTypeContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTTypeContratPeer::addInstanceToPool($obj5, $key5);
                } // if obj5 loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonTTypeContrat)
                $obj5->addCommonConsultation($obj1);
            } // if joined row not null

            // Add objects for joined CommonService rows

            $key6 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol6);
            if ($key6 !== null) {
                $obj6 = CommonServicePeer::getInstanceFromPool($key6);
                if (!$obj6) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonServicePeer::addInstanceToPool($obj6, $key6);
                } // if obj6 loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonService)
                $obj6->addCommonConsultationRelatedByServiceId($obj1);
            } // if joined row not null

            // Add objects for joined CommonService rows

            $key7 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol7);
            if ($key7 !== null) {
                $obj7 = CommonServicePeer::getInstanceFromPool($key7);
                if (!$obj7) {

                    $cls = CommonServicePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonServicePeer::addInstanceToPool($obj7, $key7);
                } // if obj7 loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonService)
                $obj7->addCommonConsultationRelatedByServiceValidationIntermediaire($obj1);
            } // if joined row not null

            // Add objects for joined CommonOperations rows

            $key8 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol8);
            if ($key8 !== null) {
                $obj8 = CommonOperationsPeer::getInstanceFromPool($key8);
                if (!$obj8) {

                    $cls = CommonOperationsPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonOperationsPeer::addInstanceToPool($obj8, $key8);
                } // if obj8 loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonOperations)
                $obj8->addCommonConsultation($obj1);
            } // if joined row not null

            // Add objects for joined CommonOrganisme rows

            $key9 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol9);
            if ($key9 !== null) {
                $obj9 = CommonOrganismePeer::getInstanceFromPool($key9);
                if (!$obj9) {

                    $cls = CommonOrganismePeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    CommonOrganismePeer::addInstanceToPool($obj9, $key9);
                } // if obj9 loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj9 (CommonOrganisme)
                $obj9->addCommonConsultation($obj1);
            } // if joined row not null

            // Add objects for joined CommonPlateformeVirtuelle rows

            $key10 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol10);
            if ($key10 !== null) {
                $obj10 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key10);
                if (!$obj10) {

                    $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj10 = new $cls();
                    $obj10->hydrate($row, $startcol10);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj10, $key10);
                } // if obj10 loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj10 (CommonPlateformeVirtuelle)
                $obj10->addCommonConsultation($obj1);
            } // if joined row not null

            // Add objects for joined CommonDossierVolumineux rows

            $key11 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol11);
            if ($key11 !== null) {
                $obj11 = CommonDossierVolumineuxPeer::getInstanceFromPool($key11);
                if (!$obj11) {

                    $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj11 = new $cls();
                    $obj11->hydrate($row, $startcol11);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj11, $key11);
                } // if obj11 loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj11 (CommonDossierVolumineux)
                $obj11->addCommonConsultation($obj1);
            } // if joined row not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonReferentielAchat table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonReferentielAchat(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonAgent table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonAgent(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonServiceRelatedByServiceValidation table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonServiceRelatedByServiceValidation(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonTTypeContrat table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonTTypeContrat(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonServiceRelatedByServiceId table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonServiceRelatedByServiceId(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonConsultationRelatedByReferenceConsultationInit table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonConsultationRelatedByReferenceConsultationInit(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonServiceRelatedByServiceValidationIntermediaire table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonServiceRelatedByServiceValidationIntermediaire(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOperations table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonOperations(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonOrganisme table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonOrganisme(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonPlateformeVirtuelle table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonPlateformeVirtuelle(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Returns the number of rows matching criteria, joining the related CommonDossierVolumineux table
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return int Number of matching rows.
     */
    public static function doCountJoinAllExceptCommonDossierVolumineux(Criteria $criteria, $distinct = false, PropelPDO $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        // we're going to modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            CommonConsultationPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY should not affect count

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects except CommonReferentielAchat.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonReferentielAchat(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonOperationsPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonOperationsPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonDossierVolumineuxPeer::addSelectColumns($criteria);
        $startcol11 = $startcol10 + CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonAgent rows

                $key2 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonAgentPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonAgentPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonAgent)
                $obj2->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key3 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonServicePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonServicePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonService)
                $obj3->addCommonConsultationRelatedByServiceValidation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key4 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTTypeContratPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTTypeContratPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonTTypeContrat)
                $obj4->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key5 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonServicePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonServicePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonService)
                $obj5->addCommonConsultationRelatedByServiceId($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key6 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonServicePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonServicePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonService)
                $obj6->addCommonConsultationRelatedByServiceValidationIntermediaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOperations rows

                $key7 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonOperationsPeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonOperationsPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonOperationsPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonOperations)
                $obj7->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key8 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = CommonOrganismePeer::getInstanceFromPool($key8);
                    if (!$obj8) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonOrganismePeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonOrganisme)
                $obj8->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key9 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key9);
                    if (!$obj9) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj9 (CommonPlateformeVirtuelle)
                $obj9->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonDossierVolumineux rows

                $key10 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol10);
                if ($key10 !== null) {
                    $obj10 = CommonDossierVolumineuxPeer::getInstanceFromPool($key10);
                    if (!$obj10) {

                        $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj10 = new $cls();
                    $obj10->hydrate($row, $startcol10);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj10, $key10);
                } // if $obj10 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj10 (CommonDossierVolumineux)
                $obj10->addCommonConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects except CommonAgent.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonAgent(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielAchatPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonOperationsPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonOperationsPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonDossierVolumineuxPeer::addSelectColumns($criteria);
        $startcol11 = $startcol10 + CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielAchat rows

                $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key3 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonServicePeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonServicePeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonService)
                $obj3->addCommonConsultationRelatedByServiceValidation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key4 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTTypeContratPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTTypeContratPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonTTypeContrat)
                $obj4->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key5 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonServicePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonServicePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonService)
                $obj5->addCommonConsultationRelatedByServiceId($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key6 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonServicePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonServicePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonService)
                $obj6->addCommonConsultationRelatedByServiceValidationIntermediaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOperations rows

                $key7 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonOperationsPeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonOperationsPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonOperationsPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonOperations)
                $obj7->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key8 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = CommonOrganismePeer::getInstanceFromPool($key8);
                    if (!$obj8) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonOrganismePeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonOrganisme)
                $obj8->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key9 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key9);
                    if (!$obj9) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj9 (CommonPlateformeVirtuelle)
                $obj9->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonDossierVolumineux rows

                $key10 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol10);
                if ($key10 !== null) {
                    $obj10 = CommonDossierVolumineuxPeer::getInstanceFromPool($key10);
                    if (!$obj10) {

                        $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj10 = new $cls();
                    $obj10->hydrate($row, $startcol10);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj10, $key10);
                } // if $obj10 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj10 (CommonDossierVolumineux)
                $obj10->addCommonConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects except CommonServiceRelatedByServiceValidation.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonServiceRelatedByServiceValidation(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielAchatPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonOperationsPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOperationsPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonDossierVolumineuxPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielAchat rows

                $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key4 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTTypeContratPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTTypeContratPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonTTypeContrat)
                $obj4->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOperations rows

                $key5 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonOperationsPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonOperationsPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOperationsPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonOperations)
                $obj5->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key6 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonOrganismePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonOrganismePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonOrganisme)
                $obj6->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key7 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonPlateformeVirtuelle)
                $obj7->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonDossierVolumineux rows

                $key8 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = CommonDossierVolumineuxPeer::getInstanceFromPool($key8);
                    if (!$obj8) {

                        $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonDossierVolumineux)
                $obj8->addCommonConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects except CommonTTypeContrat.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonTTypeContrat(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielAchatPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonOperationsPeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonOperationsPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonDossierVolumineuxPeer::addSelectColumns($criteria);
        $startcol11 = $startcol10 + CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielAchat rows

                $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key4 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonServicePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonServicePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonService)
                $obj4->addCommonConsultationRelatedByServiceValidation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key5 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonServicePeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonServicePeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonService)
                $obj5->addCommonConsultationRelatedByServiceId($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key6 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonServicePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonServicePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonService)
                $obj6->addCommonConsultationRelatedByServiceValidationIntermediaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOperations rows

                $key7 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonOperationsPeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonOperationsPeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonOperationsPeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonOperations)
                $obj7->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key8 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = CommonOrganismePeer::getInstanceFromPool($key8);
                    if (!$obj8) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonOrganismePeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonOrganisme)
                $obj8->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key9 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key9);
                    if (!$obj9) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj9 (CommonPlateformeVirtuelle)
                $obj9->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonDossierVolumineux rows

                $key10 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol10);
                if ($key10 !== null) {
                    $obj10 = CommonDossierVolumineuxPeer::getInstanceFromPool($key10);
                    if (!$obj10) {

                        $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj10 = new $cls();
                    $obj10->hydrate($row, $startcol10);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj10, $key10);
                } // if $obj10 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj10 (CommonDossierVolumineux)
                $obj10->addCommonConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects except CommonServiceRelatedByServiceId.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonServiceRelatedByServiceId(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielAchatPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonOperationsPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOperationsPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonDossierVolumineuxPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielAchat rows

                $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key4 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTTypeContratPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTTypeContratPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonTTypeContrat)
                $obj4->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOperations rows

                $key5 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonOperationsPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonOperationsPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOperationsPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonOperations)
                $obj5->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key6 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonOrganismePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonOrganismePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonOrganisme)
                $obj6->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key7 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonPlateformeVirtuelle)
                $obj7->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonDossierVolumineux rows

                $key8 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = CommonDossierVolumineuxPeer::getInstanceFromPool($key8);
                    if (!$obj8) {

                        $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonDossierVolumineux)
                $obj8->addCommonConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects except CommonConsultationRelatedByReferenceConsultationInit.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonConsultationRelatedByReferenceConsultationInit(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielAchatPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonOperationsPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonOperationsPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol11 = $startcol10 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonDossierVolumineuxPeer::addSelectColumns($criteria);
        $startcol12 = $startcol11 + CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielAchat rows

                $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key4 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonServicePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonServicePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonService)
                $obj4->addCommonConsultationRelatedByServiceValidation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key5 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTTypeContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTTypeContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonTTypeContrat)
                $obj5->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key6 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonServicePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonServicePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonService)
                $obj6->addCommonConsultationRelatedByServiceId($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key7 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonServicePeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonServicePeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonService)
                $obj7->addCommonConsultationRelatedByServiceValidationIntermediaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOperations rows

                $key8 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = CommonOperationsPeer::getInstanceFromPool($key8);
                    if (!$obj8) {

                        $cls = CommonOperationsPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonOperationsPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonOperations)
                $obj8->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key9 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = CommonOrganismePeer::getInstanceFromPool($key9);
                    if (!$obj9) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    CommonOrganismePeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj9 (CommonOrganisme)
                $obj9->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key10 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol10);
                if ($key10 !== null) {
                    $obj10 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key10);
                    if (!$obj10) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj10 = new $cls();
                    $obj10->hydrate($row, $startcol10);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj10, $key10);
                } // if $obj10 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj10 (CommonPlateformeVirtuelle)
                $obj10->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonDossierVolumineux rows

                $key11 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol11);
                if ($key11 !== null) {
                    $obj11 = CommonDossierVolumineuxPeer::getInstanceFromPool($key11);
                    if (!$obj11) {

                        $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj11 = new $cls();
                    $obj11->hydrate($row, $startcol11);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj11, $key11);
                } // if $obj11 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj11 (CommonDossierVolumineux)
                $obj11->addCommonConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects except CommonServiceRelatedByServiceValidationIntermediaire.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonServiceRelatedByServiceValidationIntermediaire(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielAchatPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonOperationsPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonOperationsPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonDossierVolumineuxPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielAchat rows

                $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key4 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonTTypeContratPeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonTTypeContratPeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonTTypeContrat)
                $obj4->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOperations rows

                $key5 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonOperationsPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonOperationsPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonOperationsPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonOperations)
                $obj5->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key6 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonOrganismePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonOrganismePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonOrganisme)
                $obj6->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key7 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonPlateformeVirtuelle)
                $obj7->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonDossierVolumineux rows

                $key8 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = CommonDossierVolumineuxPeer::getInstanceFromPool($key8);
                    if (!$obj8) {

                        $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonDossierVolumineux)
                $obj8->addCommonConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects except CommonOperations.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonOperations(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielAchatPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonDossierVolumineuxPeer::addSelectColumns($criteria);
        $startcol11 = $startcol10 + CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielAchat rows

                $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key4 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonServicePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonServicePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonService)
                $obj4->addCommonConsultationRelatedByServiceValidation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key5 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTTypeContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTTypeContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonTTypeContrat)
                $obj5->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key6 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonServicePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonServicePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonService)
                $obj6->addCommonConsultationRelatedByServiceId($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key7 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonServicePeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonServicePeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonService)
                $obj7->addCommonConsultationRelatedByServiceValidationIntermediaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key8 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = CommonOrganismePeer::getInstanceFromPool($key8);
                    if (!$obj8) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonOrganismePeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonOrganisme)
                $obj8->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key9 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key9);
                    if (!$obj9) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj9 (CommonPlateformeVirtuelle)
                $obj9->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonDossierVolumineux rows

                $key10 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol10);
                if ($key10 !== null) {
                    $obj10 = CommonDossierVolumineuxPeer::getInstanceFromPool($key10);
                    if (!$obj10) {

                        $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj10 = new $cls();
                    $obj10->hydrate($row, $startcol10);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj10, $key10);
                } // if $obj10 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj10 (CommonDossierVolumineux)
                $obj10->addCommonConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects except CommonOrganisme.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonOrganisme(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielAchatPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonOperationsPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonOperationsPeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        CommonDossierVolumineuxPeer::addSelectColumns($criteria);
        $startcol11 = $startcol10 + CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielAchat rows

                $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key4 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonServicePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonServicePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonService)
                $obj4->addCommonConsultationRelatedByServiceValidation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key5 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTTypeContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTTypeContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonTTypeContrat)
                $obj5->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key6 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonServicePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonServicePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonService)
                $obj6->addCommonConsultationRelatedByServiceId($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key7 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonServicePeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonServicePeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonService)
                $obj7->addCommonConsultationRelatedByServiceValidationIntermediaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOperations rows

                $key8 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = CommonOperationsPeer::getInstanceFromPool($key8);
                    if (!$obj8) {

                        $cls = CommonOperationsPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonOperationsPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonOperations)
                $obj8->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key9 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key9);
                    if (!$obj9) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj9 (CommonPlateformeVirtuelle)
                $obj9->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonDossierVolumineux rows

                $key10 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol10);
                if ($key10 !== null) {
                    $obj10 = CommonDossierVolumineuxPeer::getInstanceFromPool($key10);
                    if (!$obj10) {

                        $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj10 = new $cls();
                    $obj10->hydrate($row, $startcol10);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj10, $key10);
                } // if $obj10 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj10 (CommonDossierVolumineux)
                $obj10->addCommonConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects except CommonPlateformeVirtuelle.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonPlateformeVirtuelle(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielAchatPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonOperationsPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonOperationsPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonDossierVolumineuxPeer::addSelectColumns($criteria);
        $startcol11 = $startcol10 + CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ID_DOSSIER_VOLUMINEUX, CommonDossierVolumineuxPeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielAchat rows

                $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key4 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonServicePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonServicePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonService)
                $obj4->addCommonConsultationRelatedByServiceValidation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key5 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTTypeContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTTypeContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonTTypeContrat)
                $obj5->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key6 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonServicePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonServicePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonService)
                $obj6->addCommonConsultationRelatedByServiceId($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key7 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonServicePeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonServicePeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonService)
                $obj7->addCommonConsultationRelatedByServiceValidationIntermediaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOperations rows

                $key8 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = CommonOperationsPeer::getInstanceFromPool($key8);
                    if (!$obj8) {

                        $cls = CommonOperationsPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonOperationsPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonOperations)
                $obj8->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key9 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = CommonOrganismePeer::getInstanceFromPool($key9);
                    if (!$obj9) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    CommonOrganismePeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj9 (CommonOrganisme)
                $obj9->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonDossierVolumineux rows

                $key10 = CommonDossierVolumineuxPeer::getPrimaryKeyHashFromRow($row, $startcol10);
                if ($key10 !== null) {
                    $obj10 = CommonDossierVolumineuxPeer::getInstanceFromPool($key10);
                    if (!$obj10) {

                        $cls = CommonDossierVolumineuxPeer::getOMClass();

                    $obj10 = new $cls();
                    $obj10->hydrate($row, $startcol10);
                    CommonDossierVolumineuxPeer::addInstanceToPool($obj10, $key10);
                } // if $obj10 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj10 (CommonDossierVolumineux)
                $obj10->addCommonConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }


    /**
     * Selects a collection of CommonConsultation objects pre-filled with all related objects except CommonDossierVolumineux.
     *
     * @param      Criteria  $criteria
     * @param      PropelPDO $con
     * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
     * @return array           Array of CommonConsultation objects.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectJoinAllExceptCommonDossierVolumineux(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $criteria = clone $criteria;

        // Set the correct dbName if it has not been overridden
        // $criteria->getDbName() will return the same object if not set to another value
        // so == check is okay and faster
        if ($criteria->getDbName() == Propel::getDefaultDB()) {
            $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);
        }

        CommonConsultationPeer::addSelectColumns($criteria);
        $startcol2 = CommonConsultationPeer::NUM_HYDRATE_COLUMNS;

        CommonReferentielAchatPeer::addSelectColumns($criteria);
        $startcol3 = $startcol2 + CommonReferentielAchatPeer::NUM_HYDRATE_COLUMNS;

        CommonAgentPeer::addSelectColumns($criteria);
        $startcol4 = $startcol3 + CommonAgentPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol5 = $startcol4 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonTTypeContratPeer::addSelectColumns($criteria);
        $startcol6 = $startcol5 + CommonTTypeContratPeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol7 = $startcol6 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonServicePeer::addSelectColumns($criteria);
        $startcol8 = $startcol7 + CommonServicePeer::NUM_HYDRATE_COLUMNS;

        CommonOperationsPeer::addSelectColumns($criteria);
        $startcol9 = $startcol8 + CommonOperationsPeer::NUM_HYDRATE_COLUMNS;

        CommonOrganismePeer::addSelectColumns($criteria);
        $startcol10 = $startcol9 + CommonOrganismePeer::NUM_HYDRATE_COLUMNS;

        CommonPlateformeVirtuellePeer::addSelectColumns($criteria);
        $startcol11 = $startcol10 + CommonPlateformeVirtuellePeer::NUM_HYDRATE_COLUMNS;

        $criteria->addJoin(CommonConsultationPeer::REFERENTIEL_ACHAT_ID, CommonReferentielAchatPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::AGENT_TECHNIQUE_CREATEUR, CommonAgentPeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::TYPE_MARCHE, CommonTTypeContratPeer::ID_TYPE_CONTRAT, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_ID, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::SERVICE_VALIDATION_INTERMEDIAIRE, CommonServicePeer::ID, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::IDOPERATION, CommonOperationsPeer::ID_OPERATION, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, $join_behavior);

        $criteria->addJoin(CommonConsultationPeer::PLATEFORME_VIRTUELLE_ID, CommonPlateformeVirtuellePeer::ID, $join_behavior);


        $stmt = BasePeer::doSelect($criteria, $con);
        $results = array();

        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key1 = CommonConsultationPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj1 = CommonConsultationPeer::getInstanceFromPool($key1))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj1->hydrate($row, 0, true); // rehydrate
            } else {
                $cls = CommonConsultationPeer::getOMClass();

                $obj1 = new $cls();
                $obj1->hydrate($row);
                CommonConsultationPeer::addInstanceToPool($obj1, $key1);
            } // if obj1 already loaded

                // Add objects for joined CommonReferentielAchat rows

                $key2 = CommonReferentielAchatPeer::getPrimaryKeyHashFromRow($row, $startcol2);
                if ($key2 !== null) {
                    $obj2 = CommonReferentielAchatPeer::getInstanceFromPool($key2);
                    if (!$obj2) {

                        $cls = CommonReferentielAchatPeer::getOMClass();

                    $obj2 = new $cls();
                    $obj2->hydrate($row, $startcol2);
                    CommonReferentielAchatPeer::addInstanceToPool($obj2, $key2);
                } // if $obj2 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj2 (CommonReferentielAchat)
                $obj2->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonAgent rows

                $key3 = CommonAgentPeer::getPrimaryKeyHashFromRow($row, $startcol3);
                if ($key3 !== null) {
                    $obj3 = CommonAgentPeer::getInstanceFromPool($key3);
                    if (!$obj3) {

                        $cls = CommonAgentPeer::getOMClass();

                    $obj3 = new $cls();
                    $obj3->hydrate($row, $startcol3);
                    CommonAgentPeer::addInstanceToPool($obj3, $key3);
                } // if $obj3 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj3 (CommonAgent)
                $obj3->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key4 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol4);
                if ($key4 !== null) {
                    $obj4 = CommonServicePeer::getInstanceFromPool($key4);
                    if (!$obj4) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj4 = new $cls();
                    $obj4->hydrate($row, $startcol4);
                    CommonServicePeer::addInstanceToPool($obj4, $key4);
                } // if $obj4 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj4 (CommonService)
                $obj4->addCommonConsultationRelatedByServiceValidation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonTTypeContrat rows

                $key5 = CommonTTypeContratPeer::getPrimaryKeyHashFromRow($row, $startcol5);
                if ($key5 !== null) {
                    $obj5 = CommonTTypeContratPeer::getInstanceFromPool($key5);
                    if (!$obj5) {

                        $cls = CommonTTypeContratPeer::getOMClass();

                    $obj5 = new $cls();
                    $obj5->hydrate($row, $startcol5);
                    CommonTTypeContratPeer::addInstanceToPool($obj5, $key5);
                } // if $obj5 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj5 (CommonTTypeContrat)
                $obj5->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key6 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol6);
                if ($key6 !== null) {
                    $obj6 = CommonServicePeer::getInstanceFromPool($key6);
                    if (!$obj6) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj6 = new $cls();
                    $obj6->hydrate($row, $startcol6);
                    CommonServicePeer::addInstanceToPool($obj6, $key6);
                } // if $obj6 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj6 (CommonService)
                $obj6->addCommonConsultationRelatedByServiceId($obj1);

            } // if joined row is not null

                // Add objects for joined CommonService rows

                $key7 = CommonServicePeer::getPrimaryKeyHashFromRow($row, $startcol7);
                if ($key7 !== null) {
                    $obj7 = CommonServicePeer::getInstanceFromPool($key7);
                    if (!$obj7) {

                        $cls = CommonServicePeer::getOMClass();

                    $obj7 = new $cls();
                    $obj7->hydrate($row, $startcol7);
                    CommonServicePeer::addInstanceToPool($obj7, $key7);
                } // if $obj7 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj7 (CommonService)
                $obj7->addCommonConsultationRelatedByServiceValidationIntermediaire($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOperations rows

                $key8 = CommonOperationsPeer::getPrimaryKeyHashFromRow($row, $startcol8);
                if ($key8 !== null) {
                    $obj8 = CommonOperationsPeer::getInstanceFromPool($key8);
                    if (!$obj8) {

                        $cls = CommonOperationsPeer::getOMClass();

                    $obj8 = new $cls();
                    $obj8->hydrate($row, $startcol8);
                    CommonOperationsPeer::addInstanceToPool($obj8, $key8);
                } // if $obj8 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj8 (CommonOperations)
                $obj8->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonOrganisme rows

                $key9 = CommonOrganismePeer::getPrimaryKeyHashFromRow($row, $startcol9);
                if ($key9 !== null) {
                    $obj9 = CommonOrganismePeer::getInstanceFromPool($key9);
                    if (!$obj9) {

                        $cls = CommonOrganismePeer::getOMClass();

                    $obj9 = new $cls();
                    $obj9->hydrate($row, $startcol9);
                    CommonOrganismePeer::addInstanceToPool($obj9, $key9);
                } // if $obj9 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj9 (CommonOrganisme)
                $obj9->addCommonConsultation($obj1);

            } // if joined row is not null

                // Add objects for joined CommonPlateformeVirtuelle rows

                $key10 = CommonPlateformeVirtuellePeer::getPrimaryKeyHashFromRow($row, $startcol10);
                if ($key10 !== null) {
                    $obj10 = CommonPlateformeVirtuellePeer::getInstanceFromPool($key10);
                    if (!$obj10) {

                        $cls = CommonPlateformeVirtuellePeer::getOMClass();

                    $obj10 = new $cls();
                    $obj10->hydrate($row, $startcol10);
                    CommonPlateformeVirtuellePeer::addInstanceToPool($obj10, $key10);
                } // if $obj10 already loaded

                // Add the $obj1 (CommonConsultation) to the collection in $obj10 (CommonPlateformeVirtuelle)
                $obj10->addCommonConsultation($obj1);

            } // if joined row is not null

            $results[] = $obj1;
        }
        $stmt->closeCursor();

        return $results;
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(CommonConsultationPeer::DATABASE_NAME)->getTable(CommonConsultationPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseCommonConsultationPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseCommonConsultationPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new CommonConsultationTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass($row = 0, $colnum = 0)
    {
        return CommonConsultationPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a CommonConsultation or Criteria object.
     *
     * @param      mixed $values Criteria or CommonConsultation object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from CommonConsultation object
        }

        if ($criteria->containsKey(CommonConsultationPeer::ID) && $criteria->keyContainsValue(CommonConsultationPeer::ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CommonConsultationPeer::ID.')');
        }


        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a CommonConsultation or Criteria object.
     *
     * @param      mixed $values Criteria or CommonConsultation object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(CommonConsultationPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(CommonConsultationPeer::ID);
            $value = $criteria->remove(CommonConsultationPeer::ID);
            if ($value) {
                $selectCriteria->add(CommonConsultationPeer::ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(CommonConsultationPeer::TABLE_NAME);
            }

        } else { // $values is CommonConsultation object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the consultation table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += CommonConsultationPeer::doOnDeleteCascade(new Criteria(CommonConsultationPeer::DATABASE_NAME), $con);
            $affectedRows += BasePeer::doDeleteAll(CommonConsultationPeer::TABLE_NAME, $con, CommonConsultationPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CommonConsultationPeer::clearInstancePool();
            CommonConsultationPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a CommonConsultation or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or CommonConsultation object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof CommonConsultation) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CommonConsultationPeer::DATABASE_NAME);
            $criteria->add(CommonConsultationPeer::ID, (array) $values, Criteria::IN);
        }

        // Set the correct dbName
        $criteria->setDbName(CommonConsultationPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();

            // cloning the Criteria in case it's modified by doSelect() or doSelectStmt()
            $c = clone $criteria;
            $affectedRows += CommonConsultationPeer::doOnDeleteCascade($c, $con);

            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            if ($values instanceof Criteria) {
                CommonConsultationPeer::clearInstancePool();
            } elseif ($values instanceof CommonConsultation) { // it's a model object
                CommonConsultationPeer::removeInstanceFromPool($values);
            } else { // it's a primary key, or an array of pks
                foreach ((array) $values as $singleval) {
                    CommonConsultationPeer::removeInstanceFromPool($singleval);
                }
            }

            $affectedRows += BasePeer::doDelete($criteria, $con);
            CommonConsultationPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * This is a method for emulating ON DELETE CASCADE for DBs that don't support this
     * feature (like MySQL or SQLite).
     *
     * This method is not very speedy because it must perform a query first to get
     * the implicated records and then perform the deletes by calling those Peer classes.
     *
     * This method should be used within a transaction if possible.
     *
     * @param      Criteria $criteria
     * @param      PropelPDO $con
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    protected static function doOnDeleteCascade(Criteria $criteria, PropelPDO $con)
    {
        // initialize var to track total num of affected rows
        $affectedRows = 0;

        // first find the objects that are implicated by the $criteria
        $objects = CommonConsultationPeer::doSelect($criteria, $con);
        foreach ($objects as $obj) {


            // delete related CommonConsultationTags objects
            $criteria = new Criteria(CommonConsultationTagsPeer::DATABASE_NAME);

            $criteria->add(CommonConsultationTagsPeer::CONSULTATION_ID, $obj->getId());
            $affectedRows += CommonConsultationTagsPeer::doDelete($criteria, $con);

            // delete related CommonInterfaceSuivi objects
            $criteria = new Criteria(CommonInterfaceSuiviPeer::DATABASE_NAME);

            $criteria->add(CommonInterfaceSuiviPeer::ID_CONSULTATION, $obj->getId());
            $affectedRows += CommonInterfaceSuiviPeer::doDelete($criteria, $con);
        }

        return $affectedRows;
    }

    /**
     * Validates all modified columns of given CommonConsultation object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      CommonConsultation $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(CommonConsultationPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(CommonConsultationPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(CommonConsultationPeer::DATABASE_NAME, CommonConsultationPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      int $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return CommonConsultation
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = CommonConsultationPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(CommonConsultationPeer::DATABASE_NAME);
        $criteria->add(CommonConsultationPeer::ID, $pk);

        $v = CommonConsultationPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return CommonConsultation[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(CommonConsultationPeer::DATABASE_NAME);
            $criteria->add(CommonConsultationPeer::ID, $pks, Criteria::IN);
            $objs = CommonConsultationPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseCommonConsultationPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseCommonConsultationPeer::buildTableMap();

