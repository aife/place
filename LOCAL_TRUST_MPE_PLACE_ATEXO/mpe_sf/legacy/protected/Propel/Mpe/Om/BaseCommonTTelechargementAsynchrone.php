<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServiceQuery;
use Application\Propel\Mpe\CommonTTelechargementAsynchrone;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneFichier;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneFichierQuery;
use Application\Propel\Mpe\CommonTTelechargementAsynchronePeer;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneQuery;

/**
 * Base class that represents a row from the 'T_Telechargement_Asynchrone' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTTelechargementAsynchrone extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTTelechargementAsynchronePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTTelechargementAsynchronePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the id_agent field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $id_agent;

    /**
     * The value for the nom_prenom_agent field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $nom_prenom_agent;

    /**
     * The value for the email_agent field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $email_agent;

    /**
     * The value for the id_service_agent field.
     * @var        string
     */
    protected $id_service_agent;

    /**
     * The value for the old_id_service_agent field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $old_id_service_agent;

    /**
     * The value for the organisme_agent field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $organisme_agent;

    /**
     * The value for the nom_fichier_telechargement field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $nom_fichier_telechargement;

    /**
     * The value for the taille_fichier field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $taille_fichier;

    /**
     * The value for the date_generation field.
     * @var        string
     */
    protected $date_generation;

    /**
     * The value for the id_blob_fichier field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $id_blob_fichier;

    /**
     * The value for the tag_fichier_genere field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $tag_fichier_genere;

    /**
     * The value for the tag_fichier_supprime field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $tag_fichier_supprime;

    /**
     * The value for the type_telechargement field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $type_telechargement;

    /**
     * @var        CommonService
     */
    protected $aCommonService;

    /**
     * @var        PropelObjectCollection|CommonTTelechargementAsynchroneFichier[] Collection to store aggregation of CommonTTelechargementAsynchroneFichier objects.
     */
    protected $collCommonTTelechargementAsynchroneFichiers;
    protected $collCommonTTelechargementAsynchroneFichiersPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTTelechargementAsynchroneFichiersScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->id_agent = 0;
        $this->nom_prenom_agent = '';
        $this->email_agent = '';
        $this->old_id_service_agent = 0;
        $this->organisme_agent = '';
        $this->nom_fichier_telechargement = '';
        $this->taille_fichier = 0;
        $this->id_blob_fichier = 0;
        $this->tag_fichier_genere = '0';
        $this->tag_fichier_supprime = '0';
        $this->type_telechargement = 0;
    }

    /**
     * Initializes internal state of BaseCommonTTelechargementAsynchrone object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [id_agent] column value.
     *
     * @return int
     */
    public function getIdAgent()
    {

        return $this->id_agent;
    }

    /**
     * Get the [nom_prenom_agent] column value.
     *
     * @return string
     */
    public function getNomPrenomAgent()
    {

        return $this->nom_prenom_agent;
    }

    /**
     * Get the [email_agent] column value.
     *
     * @return string
     */
    public function getEmailAgent()
    {

        return $this->email_agent;
    }

    /**
     * Get the [id_service_agent] column value.
     *
     * @return string
     */
    public function getIdServiceAgent()
    {

        return $this->id_service_agent;
    }

    /**
     * Get the [old_id_service_agent] column value.
     *
     * @return int
     */
    public function getOldIdServiceAgent()
    {

        return $this->old_id_service_agent;
    }

    /**
     * Get the [organisme_agent] column value.
     *
     * @return string
     */
    public function getOrganismeAgent()
    {

        return $this->organisme_agent;
    }

    /**
     * Get the [nom_fichier_telechargement] column value.
     *
     * @return string
     */
    public function getNomFichierTelechargement()
    {

        return $this->nom_fichier_telechargement;
    }

    /**
     * Get the [taille_fichier] column value.
     *
     * @return int
     */
    public function getTailleFichier()
    {

        return $this->taille_fichier;
    }

    /**
     * Get the [optionally formatted] temporal [date_generation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateGeneration($format = 'Y-m-d H:i:s')
    {
        if ($this->date_generation === null) {
            return null;
        }

        if ($this->date_generation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_generation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_generation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [id_blob_fichier] column value.
     *
     * @return int
     */
    public function getIdBlobFichier()
    {

        return $this->id_blob_fichier;
    }

    /**
     * Get the [tag_fichier_genere] column value.
     *
     * @return string
     */
    public function getTagFichierGenere()
    {

        return $this->tag_fichier_genere;
    }

    /**
     * Get the [tag_fichier_supprime] column value.
     *
     * @return string
     */
    public function getTagFichierSupprime()
    {

        return $this->tag_fichier_supprime;
    }

    /**
     * Get the [type_telechargement] column value.
     *
     * @return int
     */
    public function getTypeTelechargement()
    {

        return $this->type_telechargement;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [id_agent] column.
     *
     * @param int $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setIdAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_agent !== $v) {
            $this->id_agent = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::ID_AGENT;
        }


        return $this;
    } // setIdAgent()

    /**
     * Set the value of [nom_prenom_agent] column.
     *
     * @param string $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setNomPrenomAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_prenom_agent !== $v) {
            $this->nom_prenom_agent = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::NOM_PRENOM_AGENT;
        }


        return $this;
    } // setNomPrenomAgent()

    /**
     * Set the value of [email_agent] column.
     *
     * @param string $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setEmailAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email_agent !== $v) {
            $this->email_agent = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::EMAIL_AGENT;
        }


        return $this;
    } // setEmailAgent()

    /**
     * Set the value of [id_service_agent] column.
     *
     * @param string $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setIdServiceAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_service_agent !== $v) {
            $this->id_service_agent = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT;
        }

        if ($this->aCommonService !== null && $this->aCommonService->getId() !== $v) {
            $this->aCommonService = null;
        }


        return $this;
    } // setIdServiceAgent()

    /**
     * Set the value of [old_id_service_agent] column.
     *
     * @param int $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setOldIdServiceAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_id_service_agent !== $v) {
            $this->old_id_service_agent = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::OLD_ID_SERVICE_AGENT;
        }


        return $this;
    } // setOldIdServiceAgent()

    /**
     * Set the value of [organisme_agent] column.
     *
     * @param string $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setOrganismeAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme_agent !== $v) {
            $this->organisme_agent = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::ORGANISME_AGENT;
        }


        return $this;
    } // setOrganismeAgent()

    /**
     * Set the value of [nom_fichier_telechargement] column.
     *
     * @param string $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setNomFichierTelechargement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_fichier_telechargement !== $v) {
            $this->nom_fichier_telechargement = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::NOM_FICHIER_TELECHARGEMENT;
        }


        return $this;
    } // setNomFichierTelechargement()

    /**
     * Set the value of [taille_fichier] column.
     *
     * @param int $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setTailleFichier($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->taille_fichier !== $v) {
            $this->taille_fichier = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::TAILLE_FICHIER;
        }


        return $this;
    } // setTailleFichier()

    /**
     * Sets the value of [date_generation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setDateGeneration($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_generation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_generation !== null && $tmpDt = new DateTime($this->date_generation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_generation = $newDateAsString;
                $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::DATE_GENERATION;
            }
        } // if either are not null


        return $this;
    } // setDateGeneration()

    /**
     * Set the value of [id_blob_fichier] column.
     *
     * @param int $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setIdBlobFichier($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_blob_fichier !== $v) {
            $this->id_blob_fichier = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::ID_BLOB_FICHIER;
        }


        return $this;
    } // setIdBlobFichier()

    /**
     * Set the value of [tag_fichier_genere] column.
     *
     * @param string $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setTagFichierGenere($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tag_fichier_genere !== $v) {
            $this->tag_fichier_genere = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE;
        }


        return $this;
    } // setTagFichierGenere()

    /**
     * Set the value of [tag_fichier_supprime] column.
     *
     * @param string $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setTagFichierSupprime($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tag_fichier_supprime !== $v) {
            $this->tag_fichier_supprime = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME;
        }


        return $this;
    } // setTagFichierSupprime()

    /**
     * Set the value of [type_telechargement] column.
     *
     * @param int $v new value
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setTypeTelechargement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->type_telechargement !== $v) {
            $this->type_telechargement = $v;
            $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT;
        }


        return $this;
    } // setTypeTelechargement()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->id_agent !== 0) {
                return false;
            }

            if ($this->nom_prenom_agent !== '') {
                return false;
            }

            if ($this->email_agent !== '') {
                return false;
            }

            if ($this->old_id_service_agent !== 0) {
                return false;
            }

            if ($this->organisme_agent !== '') {
                return false;
            }

            if ($this->nom_fichier_telechargement !== '') {
                return false;
            }

            if ($this->taille_fichier !== 0) {
                return false;
            }

            if ($this->id_blob_fichier !== 0) {
                return false;
            }

            if ($this->tag_fichier_genere !== '0') {
                return false;
            }

            if ($this->tag_fichier_supprime !== '0') {
                return false;
            }

            if ($this->type_telechargement !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_agent = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->nom_prenom_agent = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->email_agent = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->id_service_agent = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->old_id_service_agent = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->organisme_agent = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->nom_fichier_telechargement = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->taille_fichier = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->date_generation = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->id_blob_fichier = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->tag_fichier_genere = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->tag_fichier_supprime = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->type_telechargement = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 14; // 14 = CommonTTelechargementAsynchronePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTTelechargementAsynchrone object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonService !== null && $this->id_service_agent !== $this->aCommonService->getId()) {
            $this->aCommonService = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTTelechargementAsynchronePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonService = null;
            $this->collCommonTTelechargementAsynchroneFichiers = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTTelechargementAsynchroneQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTTelechargementAsynchronePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTTelechargementAsynchronePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonService !== null) {
                if ($this->aCommonService->isModified() || $this->aCommonService->isNew()) {
                    $affectedRows += $this->aCommonService->save($con);
                }
                $this->setCommonService($this->aCommonService);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonTTelechargementAsynchroneFichiersScheduledForDeletion !== null) {
                if (!$this->commonTTelechargementAsynchroneFichiersScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTTelechargementAsynchroneFichiersScheduledForDeletion as $commonTTelechargementAsynchroneFichier) {
                        // need to save related object because we set the relation to null
                        $commonTTelechargementAsynchroneFichier->save($con);
                    }
                    $this->commonTTelechargementAsynchroneFichiersScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTTelechargementAsynchroneFichiers !== null) {
                foreach ($this->collCommonTTelechargementAsynchroneFichiers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTTelechargementAsynchronePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTTelechargementAsynchronePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::ID_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_agent`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::NOM_PRENOM_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`nom_prenom_agent`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::EMAIL_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`email_agent`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_service_agent`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::OLD_ID_SERVICE_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`old_id_service_agent`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::ORGANISME_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`organisme_agent`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::NOM_FICHIER_TELECHARGEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`nom_fichier_telechargement`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::TAILLE_FICHIER)) {
            $modifiedColumns[':p' . $index++]  = '`taille_fichier`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::DATE_GENERATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_generation`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::ID_BLOB_FICHIER)) {
            $modifiedColumns[':p' . $index++]  = '`id_blob_fichier`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE)) {
            $modifiedColumns[':p' . $index++]  = '`tag_fichier_genere`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME)) {
            $modifiedColumns[':p' . $index++]  = '`tag_fichier_supprime`';
        }
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`type_telechargement`';
        }

        $sql = sprintf(
            'INSERT INTO `T_Telechargement_Asynchrone` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`id_agent`':
                        $stmt->bindValue($identifier, $this->id_agent, PDO::PARAM_INT);
                        break;
                    case '`nom_prenom_agent`':
                        $stmt->bindValue($identifier, $this->nom_prenom_agent, PDO::PARAM_STR);
                        break;
                    case '`email_agent`':
                        $stmt->bindValue($identifier, $this->email_agent, PDO::PARAM_STR);
                        break;
                    case '`id_service_agent`':
                        $stmt->bindValue($identifier, $this->id_service_agent, PDO::PARAM_STR);
                        break;
                    case '`old_id_service_agent`':
                        $stmt->bindValue($identifier, $this->old_id_service_agent, PDO::PARAM_INT);
                        break;
                    case '`organisme_agent`':
                        $stmt->bindValue($identifier, $this->organisme_agent, PDO::PARAM_STR);
                        break;
                    case '`nom_fichier_telechargement`':
                        $stmt->bindValue($identifier, $this->nom_fichier_telechargement, PDO::PARAM_STR);
                        break;
                    case '`taille_fichier`':
                        $stmt->bindValue($identifier, $this->taille_fichier, PDO::PARAM_INT);
                        break;
                    case '`date_generation`':
                        $stmt->bindValue($identifier, $this->date_generation, PDO::PARAM_STR);
                        break;
                    case '`id_blob_fichier`':
                        $stmt->bindValue($identifier, $this->id_blob_fichier, PDO::PARAM_INT);
                        break;
                    case '`tag_fichier_genere`':
                        $stmt->bindValue($identifier, $this->tag_fichier_genere, PDO::PARAM_STR);
                        break;
                    case '`tag_fichier_supprime`':
                        $stmt->bindValue($identifier, $this->tag_fichier_supprime, PDO::PARAM_STR);
                        break;
                    case '`type_telechargement`':
                        $stmt->bindValue($identifier, $this->type_telechargement, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonService !== null) {
                if (!$this->aCommonService->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonService->getValidationFailures());
                }
            }


            if (($retval = CommonTTelechargementAsynchronePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTTelechargementAsynchroneFichiers !== null) {
                    foreach ($this->collCommonTTelechargementAsynchroneFichiers as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTTelechargementAsynchronePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getIdAgent();
                break;
            case 2:
                return $this->getNomPrenomAgent();
                break;
            case 3:
                return $this->getEmailAgent();
                break;
            case 4:
                return $this->getIdServiceAgent();
                break;
            case 5:
                return $this->getOldIdServiceAgent();
                break;
            case 6:
                return $this->getOrganismeAgent();
                break;
            case 7:
                return $this->getNomFichierTelechargement();
                break;
            case 8:
                return $this->getTailleFichier();
                break;
            case 9:
                return $this->getDateGeneration();
                break;
            case 10:
                return $this->getIdBlobFichier();
                break;
            case 11:
                return $this->getTagFichierGenere();
                break;
            case 12:
                return $this->getTagFichierSupprime();
                break;
            case 13:
                return $this->getTypeTelechargement();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTTelechargementAsynchrone'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTTelechargementAsynchrone'][$this->getPrimaryKey()] = true;
        $keys = CommonTTelechargementAsynchronePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getIdAgent(),
            $keys[2] => $this->getNomPrenomAgent(),
            $keys[3] => $this->getEmailAgent(),
            $keys[4] => $this->getIdServiceAgent(),
            $keys[5] => $this->getOldIdServiceAgent(),
            $keys[6] => $this->getOrganismeAgent(),
            $keys[7] => $this->getNomFichierTelechargement(),
            $keys[8] => $this->getTailleFichier(),
            $keys[9] => $this->getDateGeneration(),
            $keys[10] => $this->getIdBlobFichier(),
            $keys[11] => $this->getTagFichierGenere(),
            $keys[12] => $this->getTagFichierSupprime(),
            $keys[13] => $this->getTypeTelechargement(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonService) {
                $result['CommonService'] = $this->aCommonService->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonTTelechargementAsynchroneFichiers) {
                $result['CommonTTelechargementAsynchroneFichiers'] = $this->collCommonTTelechargementAsynchroneFichiers->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTTelechargementAsynchronePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setIdAgent($value);
                break;
            case 2:
                $this->setNomPrenomAgent($value);
                break;
            case 3:
                $this->setEmailAgent($value);
                break;
            case 4:
                $this->setIdServiceAgent($value);
                break;
            case 5:
                $this->setOldIdServiceAgent($value);
                break;
            case 6:
                $this->setOrganismeAgent($value);
                break;
            case 7:
                $this->setNomFichierTelechargement($value);
                break;
            case 8:
                $this->setTailleFichier($value);
                break;
            case 9:
                $this->setDateGeneration($value);
                break;
            case 10:
                $this->setIdBlobFichier($value);
                break;
            case 11:
                $this->setTagFichierGenere($value);
                break;
            case 12:
                $this->setTagFichierSupprime($value);
                break;
            case 13:
                $this->setTypeTelechargement($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTTelechargementAsynchronePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdAgent($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNomPrenomAgent($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setEmailAgent($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIdServiceAgent($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setOldIdServiceAgent($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setOrganismeAgent($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setNomFichierTelechargement($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setTailleFichier($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setDateGeneration($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setIdBlobFichier($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setTagFichierGenere($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setTagFichierSupprime($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setTypeTelechargement($arr[$keys[13]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTTelechargementAsynchronePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::ID)) $criteria->add(CommonTTelechargementAsynchronePeer::ID, $this->id);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::ID_AGENT)) $criteria->add(CommonTTelechargementAsynchronePeer::ID_AGENT, $this->id_agent);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::NOM_PRENOM_AGENT)) $criteria->add(CommonTTelechargementAsynchronePeer::NOM_PRENOM_AGENT, $this->nom_prenom_agent);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::EMAIL_AGENT)) $criteria->add(CommonTTelechargementAsynchronePeer::EMAIL_AGENT, $this->email_agent);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT)) $criteria->add(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, $this->id_service_agent);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::OLD_ID_SERVICE_AGENT)) $criteria->add(CommonTTelechargementAsynchronePeer::OLD_ID_SERVICE_AGENT, $this->old_id_service_agent);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::ORGANISME_AGENT)) $criteria->add(CommonTTelechargementAsynchronePeer::ORGANISME_AGENT, $this->organisme_agent);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::NOM_FICHIER_TELECHARGEMENT)) $criteria->add(CommonTTelechargementAsynchronePeer::NOM_FICHIER_TELECHARGEMENT, $this->nom_fichier_telechargement);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::TAILLE_FICHIER)) $criteria->add(CommonTTelechargementAsynchronePeer::TAILLE_FICHIER, $this->taille_fichier);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::DATE_GENERATION)) $criteria->add(CommonTTelechargementAsynchronePeer::DATE_GENERATION, $this->date_generation);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::ID_BLOB_FICHIER)) $criteria->add(CommonTTelechargementAsynchronePeer::ID_BLOB_FICHIER, $this->id_blob_fichier);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE)) $criteria->add(CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE, $this->tag_fichier_genere);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME)) $criteria->add(CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME, $this->tag_fichier_supprime);
        if ($this->isColumnModified(CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT)) $criteria->add(CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT, $this->type_telechargement);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTTelechargementAsynchronePeer::DATABASE_NAME);
        $criteria->add(CommonTTelechargementAsynchronePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTTelechargementAsynchrone (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdAgent($this->getIdAgent());
        $copyObj->setNomPrenomAgent($this->getNomPrenomAgent());
        $copyObj->setEmailAgent($this->getEmailAgent());
        $copyObj->setIdServiceAgent($this->getIdServiceAgent());
        $copyObj->setOldIdServiceAgent($this->getOldIdServiceAgent());
        $copyObj->setOrganismeAgent($this->getOrganismeAgent());
        $copyObj->setNomFichierTelechargement($this->getNomFichierTelechargement());
        $copyObj->setTailleFichier($this->getTailleFichier());
        $copyObj->setDateGeneration($this->getDateGeneration());
        $copyObj->setIdBlobFichier($this->getIdBlobFichier());
        $copyObj->setTagFichierGenere($this->getTagFichierGenere());
        $copyObj->setTagFichierSupprime($this->getTagFichierSupprime());
        $copyObj->setTypeTelechargement($this->getTypeTelechargement());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTTelechargementAsynchroneFichiers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTTelechargementAsynchroneFichier($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTTelechargementAsynchrone Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTTelechargementAsynchronePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTTelechargementAsynchronePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonService object.
     *
     * @param   CommonService $v
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonService(CommonService $v = null)
    {
        if ($v === null) {
            $this->setIdServiceAgent(NULL);
        } else {
            $this->setIdServiceAgent($v->getId());
        }

        $this->aCommonService = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonService object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTTelechargementAsynchrone($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonService object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonService The associated CommonService object.
     * @throws PropelException
     */
    public function getCommonService(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonService === null && (($this->id_service_agent !== "" && $this->id_service_agent !== null)) && $doQuery) {
            $this->aCommonService = CommonServiceQuery::create()->findPk($this->id_service_agent, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonService->addCommonTTelechargementAsynchrones($this);
             */
        }

        return $this->aCommonService;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTTelechargementAsynchroneFichier' == $relationName) {
            $this->initCommonTTelechargementAsynchroneFichiers();
        }
    }

    /**
     * Clears out the collCommonTTelechargementAsynchroneFichiers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     * @see        addCommonTTelechargementAsynchroneFichiers()
     */
    public function clearCommonTTelechargementAsynchroneFichiers()
    {
        $this->collCommonTTelechargementAsynchroneFichiers = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTTelechargementAsynchroneFichiersPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTTelechargementAsynchroneFichiers collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTTelechargementAsynchroneFichiers($v = true)
    {
        $this->collCommonTTelechargementAsynchroneFichiersPartial = $v;
    }

    /**
     * Initializes the collCommonTTelechargementAsynchroneFichiers collection.
     *
     * By default this just sets the collCommonTTelechargementAsynchroneFichiers collection to an empty array (like clearcollCommonTTelechargementAsynchroneFichiers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTTelechargementAsynchroneFichiers($overrideExisting = true)
    {
        if (null !== $this->collCommonTTelechargementAsynchroneFichiers && !$overrideExisting) {
            return;
        }
        $this->collCommonTTelechargementAsynchroneFichiers = new PropelObjectCollection();
        $this->collCommonTTelechargementAsynchroneFichiers->setModel('CommonTTelechargementAsynchroneFichier');
    }

    /**
     * Gets an array of CommonTTelechargementAsynchroneFichier objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTTelechargementAsynchrone is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTTelechargementAsynchroneFichier[] List of CommonTTelechargementAsynchroneFichier objects
     * @throws PropelException
     */
    public function getCommonTTelechargementAsynchroneFichiers($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTTelechargementAsynchroneFichiersPartial && !$this->isNew();
        if (null === $this->collCommonTTelechargementAsynchroneFichiers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTTelechargementAsynchroneFichiers) {
                // return empty collection
                $this->initCommonTTelechargementAsynchroneFichiers();
            } else {
                $collCommonTTelechargementAsynchroneFichiers = CommonTTelechargementAsynchroneFichierQuery::create(null, $criteria)
                    ->filterByCommonTTelechargementAsynchrone($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTTelechargementAsynchroneFichiersPartial && count($collCommonTTelechargementAsynchroneFichiers)) {
                      $this->initCommonTTelechargementAsynchroneFichiers(false);

                      foreach ($collCommonTTelechargementAsynchroneFichiers as $obj) {
                        if (false == $this->collCommonTTelechargementAsynchroneFichiers->contains($obj)) {
                          $this->collCommonTTelechargementAsynchroneFichiers->append($obj);
                        }
                      }

                      $this->collCommonTTelechargementAsynchroneFichiersPartial = true;
                    }

                    $collCommonTTelechargementAsynchroneFichiers->getInternalIterator()->rewind();

                    return $collCommonTTelechargementAsynchroneFichiers;
                }

                if ($partial && $this->collCommonTTelechargementAsynchroneFichiers) {
                    foreach ($this->collCommonTTelechargementAsynchroneFichiers as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTTelechargementAsynchroneFichiers[] = $obj;
                        }
                    }
                }

                $this->collCommonTTelechargementAsynchroneFichiers = $collCommonTTelechargementAsynchroneFichiers;
                $this->collCommonTTelechargementAsynchroneFichiersPartial = false;
            }
        }

        return $this->collCommonTTelechargementAsynchroneFichiers;
    }

    /**
     * Sets a collection of CommonTTelechargementAsynchroneFichier objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTTelechargementAsynchroneFichiers A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function setCommonTTelechargementAsynchroneFichiers(PropelCollection $commonTTelechargementAsynchroneFichiers, PropelPDO $con = null)
    {
        $commonTTelechargementAsynchroneFichiersToDelete = $this->getCommonTTelechargementAsynchroneFichiers(new Criteria(), $con)->diff($commonTTelechargementAsynchroneFichiers);


        $this->commonTTelechargementAsynchroneFichiersScheduledForDeletion = $commonTTelechargementAsynchroneFichiersToDelete;

        foreach ($commonTTelechargementAsynchroneFichiersToDelete as $commonTTelechargementAsynchroneFichierRemoved) {
            $commonTTelechargementAsynchroneFichierRemoved->setCommonTTelechargementAsynchrone(null);
        }

        $this->collCommonTTelechargementAsynchroneFichiers = null;
        foreach ($commonTTelechargementAsynchroneFichiers as $commonTTelechargementAsynchroneFichier) {
            $this->addCommonTTelechargementAsynchroneFichier($commonTTelechargementAsynchroneFichier);
        }

        $this->collCommonTTelechargementAsynchroneFichiers = $commonTTelechargementAsynchroneFichiers;
        $this->collCommonTTelechargementAsynchroneFichiersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTTelechargementAsynchroneFichier objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTTelechargementAsynchroneFichier objects.
     * @throws PropelException
     */
    public function countCommonTTelechargementAsynchroneFichiers(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTTelechargementAsynchroneFichiersPartial && !$this->isNew();
        if (null === $this->collCommonTTelechargementAsynchroneFichiers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTTelechargementAsynchroneFichiers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTTelechargementAsynchroneFichiers());
            }
            $query = CommonTTelechargementAsynchroneFichierQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTTelechargementAsynchrone($this)
                ->count($con);
        }

        return count($this->collCommonTTelechargementAsynchroneFichiers);
    }

    /**
     * Method called to associate a CommonTTelechargementAsynchroneFichier object to this object
     * through the CommonTTelechargementAsynchroneFichier foreign key attribute.
     *
     * @param   CommonTTelechargementAsynchroneFichier $l CommonTTelechargementAsynchroneFichier
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function addCommonTTelechargementAsynchroneFichier(CommonTTelechargementAsynchroneFichier $l)
    {
        if ($this->collCommonTTelechargementAsynchroneFichiers === null) {
            $this->initCommonTTelechargementAsynchroneFichiers();
            $this->collCommonTTelechargementAsynchroneFichiersPartial = true;
        }
        if (!in_array($l, $this->collCommonTTelechargementAsynchroneFichiers->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTTelechargementAsynchroneFichier($l);
        }

        return $this;
    }

    /**
     * @param	CommonTTelechargementAsynchroneFichier $commonTTelechargementAsynchroneFichier The commonTTelechargementAsynchroneFichier object to add.
     */
    protected function doAddCommonTTelechargementAsynchroneFichier($commonTTelechargementAsynchroneFichier)
    {
        $this->collCommonTTelechargementAsynchroneFichiers[]= $commonTTelechargementAsynchroneFichier;
        $commonTTelechargementAsynchroneFichier->setCommonTTelechargementAsynchrone($this);
    }

    /**
     * @param	CommonTTelechargementAsynchroneFichier $commonTTelechargementAsynchroneFichier The commonTTelechargementAsynchroneFichier object to remove.
     * @return CommonTTelechargementAsynchrone The current object (for fluent API support)
     */
    public function removeCommonTTelechargementAsynchroneFichier($commonTTelechargementAsynchroneFichier)
    {
        if ($this->getCommonTTelechargementAsynchroneFichiers()->contains($commonTTelechargementAsynchroneFichier)) {
            $this->collCommonTTelechargementAsynchroneFichiers->remove($this->collCommonTTelechargementAsynchroneFichiers->search($commonTTelechargementAsynchroneFichier));
            if (null === $this->commonTTelechargementAsynchroneFichiersScheduledForDeletion) {
                $this->commonTTelechargementAsynchroneFichiersScheduledForDeletion = clone $this->collCommonTTelechargementAsynchroneFichiers;
                $this->commonTTelechargementAsynchroneFichiersScheduledForDeletion->clear();
            }
            $this->commonTTelechargementAsynchroneFichiersScheduledForDeletion[]= clone $commonTTelechargementAsynchroneFichier;
            $commonTTelechargementAsynchroneFichier->setCommonTTelechargementAsynchrone(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->id_agent = null;
        $this->nom_prenom_agent = null;
        $this->email_agent = null;
        $this->id_service_agent = null;
        $this->old_id_service_agent = null;
        $this->organisme_agent = null;
        $this->nom_fichier_telechargement = null;
        $this->taille_fichier = null;
        $this->date_generation = null;
        $this->id_blob_fichier = null;
        $this->tag_fichier_genere = null;
        $this->tag_fichier_supprime = null;
        $this->type_telechargement = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTTelechargementAsynchroneFichiers) {
                foreach ($this->collCommonTTelechargementAsynchroneFichiers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonService instanceof Persistent) {
              $this->aCommonService->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTTelechargementAsynchroneFichiers instanceof PropelCollection) {
            $this->collCommonTTelechargementAsynchroneFichiers->clearIterator();
        }
        $this->collCommonTTelechargementAsynchroneFichiers = null;
        $this->aCommonService = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTTelechargementAsynchronePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
