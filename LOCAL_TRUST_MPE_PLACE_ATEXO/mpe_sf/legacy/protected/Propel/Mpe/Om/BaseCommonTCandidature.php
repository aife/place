<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTCandidaturePeer;
use Application\Propel\Mpe\CommonTCandidatureQuery;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Propel\Mpe\CommonTDumeContexteQuery;
use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Propel\Mpe\CommonTGroupementEntrepriseQuery;
use Application\Propel\Mpe\CommonTListeLotsCandidature;
use Application\Propel\Mpe\CommonTListeLotsCandidatureQuery;

/**
 * Base class that represents a row from the 't_candidature' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTCandidature extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTCandidaturePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTCandidaturePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the ref_consultation field.
     * @var        int
     */
    protected $ref_consultation;

    /**
     * The value for the organisme field.
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the old_id_inscrit field.
     * @var        int
     */
    protected $old_id_inscrit;

    /**
     * The value for the id_entreprise field.
     * @var        int
     */
    protected $id_entreprise;

    /**
     * The value for the id_etablissement field.
     * @var        int
     */
    protected $id_etablissement;

    /**
     * The value for the status field.
     * Note: this column has a database default value of: 99
     * @var        int
     */
    protected $status;

    /**
     * The value for the type_candidature field.
     * @var        string
     */
    protected $type_candidature;

    /**
     * The value for the type_candidature_dume field.
     * @var        string
     */
    protected $type_candidature_dume;

    /**
     * The value for the id_dume_contexte field.
     * @var        int
     */
    protected $id_dume_contexte;

    /**
     * The value for the id_offre field.
     * @var        int
     */
    protected $id_offre;

    /**
     * The value for the role_inscrit field.
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $role_inscrit;

    /**
     * The value for the date_derniere_validation_dume field.
     * @var        string
     */
    protected $date_derniere_validation_dume;

    /**
     * The value for the consultation_id field.
     * @var        int
     */
    protected $consultation_id;

    /**
     * The value for the id_inscrit field.
     * @var        string
     */
    protected $id_inscrit;

    /**
     * @var        CommonConsultation
     */
    protected $aCommonConsultation;

    /**
     * @var        CommonTDumeContexte
     */
    protected $aCommonTDumeContexte;

    /**
     * @var        CommonInscrit
     */
    protected $aCommonInscrit;

    /**
     * @var        CommonOffres
     */
    protected $aCommonOffres;

    /**
     * @var        CommonOrganisme
     */
    protected $aCommonOrganisme;

    /**
     * @var        PropelObjectCollection|CommonTGroupementEntreprise[] Collection to store aggregation of CommonTGroupementEntreprise objects.
     */
    protected $collCommonTGroupementEntreprises;
    protected $collCommonTGroupementEntreprisesPartial;

    /**
     * @var        PropelObjectCollection|CommonTListeLotsCandidature[] Collection to store aggregation of CommonTListeLotsCandidature objects.
     */
    protected $collCommonTListeLotsCandidatures;
    protected $collCommonTListeLotsCandidaturesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTGroupementEntreprisesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTListeLotsCandidaturesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->status = 99;
        $this->role_inscrit = 1;
    }

    /**
     * Initializes internal state of BaseCommonTCandidature object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [ref_consultation] column value.
     *
     * @return int
     */
    public function getRefConsultation()
    {

        return $this->ref_consultation;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [old_id_inscrit] column value.
     *
     * @return int
     */
    public function getOldIdInscrit()
    {

        return $this->old_id_inscrit;
    }

    /**
     * Get the [id_entreprise] column value.
     *
     * @return int
     */
    public function getIdEntreprise()
    {

        return $this->id_entreprise;
    }

    /**
     * Get the [id_etablissement] column value.
     *
     * @return int
     */
    public function getIdEtablissement()
    {

        return $this->id_etablissement;
    }

    /**
     * Get the [status] column value.
     *
     * @return int
     */
    public function getStatus()
    {

        return $this->status;
    }

    /**
     * Get the [type_candidature] column value.
     *
     * @return string
     */
    public function getTypeCandidature()
    {

        return $this->type_candidature;
    }

    /**
     * Get the [type_candidature_dume] column value.
     *
     * @return string
     */
    public function getTypeCandidatureDume()
    {

        return $this->type_candidature_dume;
    }

    /**
     * Get the [id_dume_contexte] column value.
     *
     * @return int
     */
    public function getIdDumeContexte()
    {

        return $this->id_dume_contexte;
    }

    /**
     * Get the [id_offre] column value.
     *
     * @return int
     */
    public function getIdOffre()
    {

        return $this->id_offre;
    }

    /**
     * Get the [role_inscrit] column value.
     *
     * @return int
     */
    public function getRoleInscrit()
    {

        return $this->role_inscrit;
    }

    /**
     * Get the [optionally formatted] temporal [date_derniere_validation_dume] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateDerniereValidationDume($format = 'Y-m-d H:i:s')
    {
        if ($this->date_derniere_validation_dume === null) {
            return null;
        }

        if ($this->date_derniere_validation_dume === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_derniere_validation_dume);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_derniere_validation_dume, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [consultation_id] column value.
     *
     * @return int
     */
    public function getConsultationId()
    {

        return $this->consultation_id;
    }

    /**
     * Get the [id_inscrit] column value.
     *
     * @return string
     */
    public function getIdInscrit()
    {

        return $this->id_inscrit;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [ref_consultation] column.
     *
     * @param int $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setRefConsultation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->ref_consultation !== $v) {
            $this->ref_consultation = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::REF_CONSULTATION;
        }


        return $this;
    } // setRefConsultation()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::ORGANISME;
        }

        if ($this->aCommonOrganisme !== null && $this->aCommonOrganisme->getAcronyme() !== $v) {
            $this->aCommonOrganisme = null;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [old_id_inscrit] column.
     *
     * @param int $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setOldIdInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_id_inscrit !== $v) {
            $this->old_id_inscrit = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::OLD_ID_INSCRIT;
        }


        return $this;
    } // setOldIdInscrit()

    /**
     * Set the value of [id_entreprise] column.
     *
     * @param int $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setIdEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_entreprise !== $v) {
            $this->id_entreprise = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::ID_ENTREPRISE;
        }


        return $this;
    } // setIdEntreprise()

    /**
     * Set the value of [id_etablissement] column.
     *
     * @param int $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setIdEtablissement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_etablissement !== $v) {
            $this->id_etablissement = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::ID_ETABLISSEMENT;
        }


        return $this;
    } // setIdEtablissement()

    /**
     * Set the value of [status] column.
     *
     * @param int $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::STATUS;
        }


        return $this;
    } // setStatus()

    /**
     * Set the value of [type_candidature] column.
     *
     * @param string $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setTypeCandidature($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_candidature !== $v) {
            $this->type_candidature = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::TYPE_CANDIDATURE;
        }


        return $this;
    } // setTypeCandidature()

    /**
     * Set the value of [type_candidature_dume] column.
     *
     * @param string $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setTypeCandidatureDume($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_candidature_dume !== $v) {
            $this->type_candidature_dume = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::TYPE_CANDIDATURE_DUME;
        }


        return $this;
    } // setTypeCandidatureDume()

    /**
     * Set the value of [id_dume_contexte] column.
     *
     * @param int $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setIdDumeContexte($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_dume_contexte !== $v) {
            $this->id_dume_contexte = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::ID_DUME_CONTEXTE;
        }

        if ($this->aCommonTDumeContexte !== null && $this->aCommonTDumeContexte->getId() !== $v) {
            $this->aCommonTDumeContexte = null;
        }


        return $this;
    } // setIdDumeContexte()

    /**
     * Set the value of [id_offre] column.
     *
     * @param int $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setIdOffre($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_offre !== $v) {
            $this->id_offre = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::ID_OFFRE;
        }

        if ($this->aCommonOffres !== null && $this->aCommonOffres->getId() !== $v) {
            $this->aCommonOffres = null;
        }


        return $this;
    } // setIdOffre()

    /**
     * Set the value of [role_inscrit] column.
     *
     * @param int $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setRoleInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->role_inscrit !== $v) {
            $this->role_inscrit = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::ROLE_INSCRIT;
        }


        return $this;
    } // setRoleInscrit()

    /**
     * Sets the value of [date_derniere_validation_dume] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setDateDerniereValidationDume($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_derniere_validation_dume !== null || $dt !== null) {
            $currentDateAsString = ($this->date_derniere_validation_dume !== null && $tmpDt = new DateTime($this->date_derniere_validation_dume)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_derniere_validation_dume = $newDateAsString;
                $this->modifiedColumns[] = CommonTCandidaturePeer::DATE_DERNIERE_VALIDATION_DUME;
            }
        } // if either are not null


        return $this;
    } // setDateDerniereValidationDume()

    /**
     * Set the value of [consultation_id] column.
     *
     * @param int $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setConsultationId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->consultation_id !== $v) {
            $this->consultation_id = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::CONSULTATION_ID;
        }

        if ($this->aCommonConsultation !== null && $this->aCommonConsultation->getId() !== $v) {
            $this->aCommonConsultation = null;
        }


        return $this;
    } // setConsultationId()

    /**
     * Set the value of [id_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setIdInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_inscrit !== $v) {
            $this->id_inscrit = $v;
            $this->modifiedColumns[] = CommonTCandidaturePeer::ID_INSCRIT;
        }

        if ($this->aCommonInscrit !== null && $this->aCommonInscrit->getId() !== $v) {
            $this->aCommonInscrit = null;
        }


        return $this;
    } // setIdInscrit()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->status !== 99) {
                return false;
            }

            if ($this->role_inscrit !== 1) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->ref_consultation = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->organisme = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->old_id_inscrit = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->id_entreprise = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->id_etablissement = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->status = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->type_candidature = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->type_candidature_dume = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->id_dume_contexte = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->id_offre = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->role_inscrit = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->date_derniere_validation_dume = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->consultation_id = ($row[$startcol + 13] !== null) ? (int) $row[$startcol + 13] : null;
            $this->id_inscrit = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 15; // 15 = CommonTCandidaturePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTCandidature object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonOrganisme !== null && $this->organisme !== $this->aCommonOrganisme->getAcronyme()) {
            $this->aCommonOrganisme = null;
        }
        if ($this->aCommonTDumeContexte !== null && $this->id_dume_contexte !== $this->aCommonTDumeContexte->getId()) {
            $this->aCommonTDumeContexte = null;
        }
        if ($this->aCommonOffres !== null && $this->id_offre !== $this->aCommonOffres->getId()) {
            $this->aCommonOffres = null;
        }
        if ($this->aCommonConsultation !== null && $this->consultation_id !== $this->aCommonConsultation->getId()) {
            $this->aCommonConsultation = null;
        }
        if ($this->aCommonInscrit !== null && $this->id_inscrit !== $this->aCommonInscrit->getId()) {
            $this->aCommonInscrit = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTCandidaturePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonConsultation = null;
            $this->aCommonTDumeContexte = null;
            $this->aCommonInscrit = null;
            $this->aCommonOffres = null;
            $this->aCommonOrganisme = null;
            $this->collCommonTGroupementEntreprises = null;

            $this->collCommonTListeLotsCandidatures = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTCandidatureQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTCandidaturePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTCandidaturePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonConsultation !== null) {
                if ($this->aCommonConsultation->isModified() || $this->aCommonConsultation->isNew()) {
                    $affectedRows += $this->aCommonConsultation->save($con);
                }
                $this->setCommonConsultation($this->aCommonConsultation);
            }

            if ($this->aCommonTDumeContexte !== null) {
                if ($this->aCommonTDumeContexte->isModified() || $this->aCommonTDumeContexte->isNew()) {
                    $affectedRows += $this->aCommonTDumeContexte->save($con);
                }
                $this->setCommonTDumeContexte($this->aCommonTDumeContexte);
            }

            if ($this->aCommonInscrit !== null) {
                if ($this->aCommonInscrit->isModified() || $this->aCommonInscrit->isNew()) {
                    $affectedRows += $this->aCommonInscrit->save($con);
                }
                $this->setCommonInscrit($this->aCommonInscrit);
            }

            if ($this->aCommonOffres !== null) {
                if ($this->aCommonOffres->isModified() || $this->aCommonOffres->isNew()) {
                    $affectedRows += $this->aCommonOffres->save($con);
                }
                $this->setCommonOffres($this->aCommonOffres);
            }

            if ($this->aCommonOrganisme !== null) {
                if ($this->aCommonOrganisme->isModified() || $this->aCommonOrganisme->isNew()) {
                    $affectedRows += $this->aCommonOrganisme->save($con);
                }
                $this->setCommonOrganisme($this->aCommonOrganisme);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonTGroupementEntreprisesScheduledForDeletion !== null) {
                if (!$this->commonTGroupementEntreprisesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTGroupementEntreprisesScheduledForDeletion as $commonTGroupementEntreprise) {
                        // need to save related object because we set the relation to null
                        $commonTGroupementEntreprise->save($con);
                    }
                    $this->commonTGroupementEntreprisesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTGroupementEntreprises !== null) {
                foreach ($this->collCommonTGroupementEntreprises as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTListeLotsCandidaturesScheduledForDeletion !== null) {
                if (!$this->commonTListeLotsCandidaturesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTListeLotsCandidaturesScheduledForDeletion as $commonTListeLotsCandidature) {
                        // need to save related object because we set the relation to null
                        $commonTListeLotsCandidature->save($con);
                    }
                    $this->commonTListeLotsCandidaturesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTListeLotsCandidatures !== null) {
                foreach ($this->collCommonTListeLotsCandidatures as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTCandidaturePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTCandidaturePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTCandidaturePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::REF_CONSULTATION)) {
            $modifiedColumns[':p' . $index++]  = '`ref_consultation`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::OLD_ID_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`old_id_inscrit`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::ID_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entreprise`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::ID_ETABLISSEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_etablissement`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::STATUS)) {
            $modifiedColumns[':p' . $index++]  = '`status`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::TYPE_CANDIDATURE)) {
            $modifiedColumns[':p' . $index++]  = '`type_candidature`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::TYPE_CANDIDATURE_DUME)) {
            $modifiedColumns[':p' . $index++]  = '`type_candidature_dume`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::ID_DUME_CONTEXTE)) {
            $modifiedColumns[':p' . $index++]  = '`id_dume_contexte`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::ID_OFFRE)) {
            $modifiedColumns[':p' . $index++]  = '`id_offre`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::ROLE_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`role_inscrit`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::DATE_DERNIERE_VALIDATION_DUME)) {
            $modifiedColumns[':p' . $index++]  = '`date_derniere_validation_dume`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::CONSULTATION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`consultation_id`';
        }
        if ($this->isColumnModified(CommonTCandidaturePeer::ID_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`id_inscrit`';
        }

        $sql = sprintf(
            'INSERT INTO `t_candidature` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`ref_consultation`':
                        $stmt->bindValue($identifier, $this->ref_consultation, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`old_id_inscrit`':
                        $stmt->bindValue($identifier, $this->old_id_inscrit, PDO::PARAM_INT);
                        break;
                    case '`id_entreprise`':
                        $stmt->bindValue($identifier, $this->id_entreprise, PDO::PARAM_INT);
                        break;
                    case '`id_etablissement`':
                        $stmt->bindValue($identifier, $this->id_etablissement, PDO::PARAM_INT);
                        break;
                    case '`status`':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_INT);
                        break;
                    case '`type_candidature`':
                        $stmt->bindValue($identifier, $this->type_candidature, PDO::PARAM_STR);
                        break;
                    case '`type_candidature_dume`':
                        $stmt->bindValue($identifier, $this->type_candidature_dume, PDO::PARAM_STR);
                        break;
                    case '`id_dume_contexte`':
                        $stmt->bindValue($identifier, $this->id_dume_contexte, PDO::PARAM_INT);
                        break;
                    case '`id_offre`':
                        $stmt->bindValue($identifier, $this->id_offre, PDO::PARAM_INT);
                        break;
                    case '`role_inscrit`':
                        $stmt->bindValue($identifier, $this->role_inscrit, PDO::PARAM_INT);
                        break;
                    case '`date_derniere_validation_dume`':
                        $stmt->bindValue($identifier, $this->date_derniere_validation_dume, PDO::PARAM_STR);
                        break;
                    case '`consultation_id`':
                        $stmt->bindValue($identifier, $this->consultation_id, PDO::PARAM_INT);
                        break;
                    case '`id_inscrit`':
                        $stmt->bindValue($identifier, $this->id_inscrit, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonConsultation !== null) {
                if (!$this->aCommonConsultation->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonConsultation->getValidationFailures());
                }
            }

            if ($this->aCommonTDumeContexte !== null) {
                if (!$this->aCommonTDumeContexte->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTDumeContexte->getValidationFailures());
                }
            }

            if ($this->aCommonInscrit !== null) {
                if (!$this->aCommonInscrit->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonInscrit->getValidationFailures());
                }
            }

            if ($this->aCommonOffres !== null) {
                if (!$this->aCommonOffres->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonOffres->getValidationFailures());
                }
            }

            if ($this->aCommonOrganisme !== null) {
                if (!$this->aCommonOrganisme->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonOrganisme->getValidationFailures());
                }
            }


            if (($retval = CommonTCandidaturePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTGroupementEntreprises !== null) {
                    foreach ($this->collCommonTGroupementEntreprises as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTListeLotsCandidatures !== null) {
                    foreach ($this->collCommonTListeLotsCandidatures as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTCandidaturePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getRefConsultation();
                break;
            case 2:
                return $this->getOrganisme();
                break;
            case 3:
                return $this->getOldIdInscrit();
                break;
            case 4:
                return $this->getIdEntreprise();
                break;
            case 5:
                return $this->getIdEtablissement();
                break;
            case 6:
                return $this->getStatus();
                break;
            case 7:
                return $this->getTypeCandidature();
                break;
            case 8:
                return $this->getTypeCandidatureDume();
                break;
            case 9:
                return $this->getIdDumeContexte();
                break;
            case 10:
                return $this->getIdOffre();
                break;
            case 11:
                return $this->getRoleInscrit();
                break;
            case 12:
                return $this->getDateDerniereValidationDume();
                break;
            case 13:
                return $this->getConsultationId();
                break;
            case 14:
                return $this->getIdInscrit();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTCandidature'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTCandidature'][$this->getPrimaryKey()] = true;
        $keys = CommonTCandidaturePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getRefConsultation(),
            $keys[2] => $this->getOrganisme(),
            $keys[3] => $this->getOldIdInscrit(),
            $keys[4] => $this->getIdEntreprise(),
            $keys[5] => $this->getIdEtablissement(),
            $keys[6] => $this->getStatus(),
            $keys[7] => $this->getTypeCandidature(),
            $keys[8] => $this->getTypeCandidatureDume(),
            $keys[9] => $this->getIdDumeContexte(),
            $keys[10] => $this->getIdOffre(),
            $keys[11] => $this->getRoleInscrit(),
            $keys[12] => $this->getDateDerniereValidationDume(),
            $keys[13] => $this->getConsultationId(),
            $keys[14] => $this->getIdInscrit(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonConsultation) {
                $result['CommonConsultation'] = $this->aCommonConsultation->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonTDumeContexte) {
                $result['CommonTDumeContexte'] = $this->aCommonTDumeContexte->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonInscrit) {
                $result['CommonInscrit'] = $this->aCommonInscrit->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonOffres) {
                $result['CommonOffres'] = $this->aCommonOffres->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonOrganisme) {
                $result['CommonOrganisme'] = $this->aCommonOrganisme->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonTGroupementEntreprises) {
                $result['CommonTGroupementEntreprises'] = $this->collCommonTGroupementEntreprises->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTListeLotsCandidatures) {
                $result['CommonTListeLotsCandidatures'] = $this->collCommonTListeLotsCandidatures->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTCandidaturePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setRefConsultation($value);
                break;
            case 2:
                $this->setOrganisme($value);
                break;
            case 3:
                $this->setOldIdInscrit($value);
                break;
            case 4:
                $this->setIdEntreprise($value);
                break;
            case 5:
                $this->setIdEtablissement($value);
                break;
            case 6:
                $this->setStatus($value);
                break;
            case 7:
                $this->setTypeCandidature($value);
                break;
            case 8:
                $this->setTypeCandidatureDume($value);
                break;
            case 9:
                $this->setIdDumeContexte($value);
                break;
            case 10:
                $this->setIdOffre($value);
                break;
            case 11:
                $this->setRoleInscrit($value);
                break;
            case 12:
                $this->setDateDerniereValidationDume($value);
                break;
            case 13:
                $this->setConsultationId($value);
                break;
            case 14:
                $this->setIdInscrit($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTCandidaturePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setRefConsultation($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setOrganisme($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setOldIdInscrit($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setIdEntreprise($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setIdEtablissement($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setStatus($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setTypeCandidature($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setTypeCandidatureDume($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setIdDumeContexte($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setIdOffre($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setRoleInscrit($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setDateDerniereValidationDume($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setConsultationId($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setIdInscrit($arr[$keys[14]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTCandidaturePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTCandidaturePeer::ID)) $criteria->add(CommonTCandidaturePeer::ID, $this->id);
        if ($this->isColumnModified(CommonTCandidaturePeer::REF_CONSULTATION)) $criteria->add(CommonTCandidaturePeer::REF_CONSULTATION, $this->ref_consultation);
        if ($this->isColumnModified(CommonTCandidaturePeer::ORGANISME)) $criteria->add(CommonTCandidaturePeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonTCandidaturePeer::OLD_ID_INSCRIT)) $criteria->add(CommonTCandidaturePeer::OLD_ID_INSCRIT, $this->old_id_inscrit);
        if ($this->isColumnModified(CommonTCandidaturePeer::ID_ENTREPRISE)) $criteria->add(CommonTCandidaturePeer::ID_ENTREPRISE, $this->id_entreprise);
        if ($this->isColumnModified(CommonTCandidaturePeer::ID_ETABLISSEMENT)) $criteria->add(CommonTCandidaturePeer::ID_ETABLISSEMENT, $this->id_etablissement);
        if ($this->isColumnModified(CommonTCandidaturePeer::STATUS)) $criteria->add(CommonTCandidaturePeer::STATUS, $this->status);
        if ($this->isColumnModified(CommonTCandidaturePeer::TYPE_CANDIDATURE)) $criteria->add(CommonTCandidaturePeer::TYPE_CANDIDATURE, $this->type_candidature);
        if ($this->isColumnModified(CommonTCandidaturePeer::TYPE_CANDIDATURE_DUME)) $criteria->add(CommonTCandidaturePeer::TYPE_CANDIDATURE_DUME, $this->type_candidature_dume);
        if ($this->isColumnModified(CommonTCandidaturePeer::ID_DUME_CONTEXTE)) $criteria->add(CommonTCandidaturePeer::ID_DUME_CONTEXTE, $this->id_dume_contexte);
        if ($this->isColumnModified(CommonTCandidaturePeer::ID_OFFRE)) $criteria->add(CommonTCandidaturePeer::ID_OFFRE, $this->id_offre);
        if ($this->isColumnModified(CommonTCandidaturePeer::ROLE_INSCRIT)) $criteria->add(CommonTCandidaturePeer::ROLE_INSCRIT, $this->role_inscrit);
        if ($this->isColumnModified(CommonTCandidaturePeer::DATE_DERNIERE_VALIDATION_DUME)) $criteria->add(CommonTCandidaturePeer::DATE_DERNIERE_VALIDATION_DUME, $this->date_derniere_validation_dume);
        if ($this->isColumnModified(CommonTCandidaturePeer::CONSULTATION_ID)) $criteria->add(CommonTCandidaturePeer::CONSULTATION_ID, $this->consultation_id);
        if ($this->isColumnModified(CommonTCandidaturePeer::ID_INSCRIT)) $criteria->add(CommonTCandidaturePeer::ID_INSCRIT, $this->id_inscrit);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTCandidaturePeer::DATABASE_NAME);
        $criteria->add(CommonTCandidaturePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTCandidature (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setRefConsultation($this->getRefConsultation());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setOldIdInscrit($this->getOldIdInscrit());
        $copyObj->setIdEntreprise($this->getIdEntreprise());
        $copyObj->setIdEtablissement($this->getIdEtablissement());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setTypeCandidature($this->getTypeCandidature());
        $copyObj->setTypeCandidatureDume($this->getTypeCandidatureDume());
        $copyObj->setIdDumeContexte($this->getIdDumeContexte());
        $copyObj->setIdOffre($this->getIdOffre());
        $copyObj->setRoleInscrit($this->getRoleInscrit());
        $copyObj->setDateDerniereValidationDume($this->getDateDerniereValidationDume());
        $copyObj->setConsultationId($this->getConsultationId());
        $copyObj->setIdInscrit($this->getIdInscrit());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTGroupementEntreprises() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTGroupementEntreprise($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTListeLotsCandidatures() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTListeLotsCandidature($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTCandidature Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTCandidaturePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTCandidaturePeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonConsultation object.
     *
     * @param   CommonConsultation $v
     * @return CommonTCandidature The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonConsultation(CommonConsultation $v = null)
    {
        if ($v === null) {
            $this->setConsultationId(NULL);
        } else {
            $this->setConsultationId($v->getId());
        }

        $this->aCommonConsultation = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonConsultation object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTCandidature($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonConsultation object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonConsultation The associated CommonConsultation object.
     * @throws PropelException
     */
    public function getCommonConsultation(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonConsultation === null && ($this->consultation_id !== null) && $doQuery) {
            $this->aCommonConsultation = CommonConsultationQuery::create()->findPk($this->consultation_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonConsultation->addCommonTCandidatures($this);
             */
        }

        return $this->aCommonConsultation;
    }

    /**
     * Declares an association between this object and a CommonTDumeContexte object.
     *
     * @param   CommonTDumeContexte $v
     * @return CommonTCandidature The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTDumeContexte(CommonTDumeContexte $v = null)
    {
        if ($v === null) {
            $this->setIdDumeContexte(NULL);
        } else {
            $this->setIdDumeContexte($v->getId());
        }

        $this->aCommonTDumeContexte = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTDumeContexte object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTCandidature($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTDumeContexte object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTDumeContexte The associated CommonTDumeContexte object.
     * @throws PropelException
     */
    public function getCommonTDumeContexte(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTDumeContexte === null && ($this->id_dume_contexte !== null) && $doQuery) {
            $this->aCommonTDumeContexte = CommonTDumeContexteQuery::create()->findPk($this->id_dume_contexte, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTDumeContexte->addCommonTCandidatures($this);
             */
        }

        return $this->aCommonTDumeContexte;
    }

    /**
     * Declares an association between this object and a CommonInscrit object.
     *
     * @param   CommonInscrit $v
     * @return CommonTCandidature The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonInscrit(CommonInscrit $v = null)
    {
        if ($v === null) {
            $this->setIdInscrit(NULL);
        } else {
            $this->setIdInscrit($v->getId());
        }

        $this->aCommonInscrit = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonInscrit object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTCandidature($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonInscrit object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonInscrit The associated CommonInscrit object.
     * @throws PropelException
     */
    public function getCommonInscrit(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonInscrit === null && (($this->id_inscrit !== "" && $this->id_inscrit !== null)) && $doQuery) {
            $this->aCommonInscrit = CommonInscritQuery::create()->findPk($this->id_inscrit, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonInscrit->addCommonTCandidatures($this);
             */
        }

        return $this->aCommonInscrit;
    }

    /**
     * Declares an association between this object and a CommonOffres object.
     *
     * @param   CommonOffres $v
     * @return CommonTCandidature The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonOffres(CommonOffres $v = null)
    {
        if ($v === null) {
            $this->setIdOffre(NULL);
        } else {
            $this->setIdOffre($v->getId());
        }

        $this->aCommonOffres = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonOffres object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTCandidature($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonOffres object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonOffres The associated CommonOffres object.
     * @throws PropelException
     */
    public function getCommonOffres(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonOffres === null && ($this->id_offre !== null) && $doQuery) {
            $this->aCommonOffres = CommonOffresQuery::create()
                ->filterByCommonTCandidature($this) // here
                ->findOne($con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonOffres->addCommonTCandidatures($this);
             */
        }

        return $this->aCommonOffres;
    }

    /**
     * Declares an association between this object and a CommonOrganisme object.
     *
     * @param   CommonOrganisme $v
     * @return CommonTCandidature The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonOrganisme(CommonOrganisme $v = null)
    {
        if ($v === null) {
            $this->setOrganisme(NULL);
        } else {
            $this->setOrganisme($v->getAcronyme());
        }

        $this->aCommonOrganisme = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonOrganisme object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTCandidature($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonOrganisme object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonOrganisme The associated CommonOrganisme object.
     * @throws PropelException
     */
    public function getCommonOrganisme(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonOrganisme === null && (($this->organisme !== "" && $this->organisme !== null)) && $doQuery) {
            $this->aCommonOrganisme = CommonOrganismeQuery::create()
                ->filterByCommonTCandidature($this) // here
                ->findOne($con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonOrganisme->addCommonTCandidatures($this);
             */
        }

        return $this->aCommonOrganisme;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTGroupementEntreprise' == $relationName) {
            $this->initCommonTGroupementEntreprises();
        }
        if ('CommonTListeLotsCandidature' == $relationName) {
            $this->initCommonTListeLotsCandidatures();
        }
    }

    /**
     * Clears out the collCommonTGroupementEntreprises collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTCandidature The current object (for fluent API support)
     * @see        addCommonTGroupementEntreprises()
     */
    public function clearCommonTGroupementEntreprises()
    {
        $this->collCommonTGroupementEntreprises = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTGroupementEntreprisesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTGroupementEntreprises collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTGroupementEntreprises($v = true)
    {
        $this->collCommonTGroupementEntreprisesPartial = $v;
    }

    /**
     * Initializes the collCommonTGroupementEntreprises collection.
     *
     * By default this just sets the collCommonTGroupementEntreprises collection to an empty array (like clearcollCommonTGroupementEntreprises());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTGroupementEntreprises($overrideExisting = true)
    {
        if (null !== $this->collCommonTGroupementEntreprises && !$overrideExisting) {
            return;
        }
        $this->collCommonTGroupementEntreprises = new PropelObjectCollection();
        $this->collCommonTGroupementEntreprises->setModel('CommonTGroupementEntreprise');
    }

    /**
     * Gets an array of CommonTGroupementEntreprise objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTCandidature is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTGroupementEntreprise[] List of CommonTGroupementEntreprise objects
     * @throws PropelException
     */
    public function getCommonTGroupementEntreprises($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTGroupementEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTGroupementEntreprises || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTGroupementEntreprises) {
                // return empty collection
                $this->initCommonTGroupementEntreprises();
            } else {
                $collCommonTGroupementEntreprises = CommonTGroupementEntrepriseQuery::create(null, $criteria)
                    ->filterByCommonTCandidature($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTGroupementEntreprisesPartial && count($collCommonTGroupementEntreprises)) {
                      $this->initCommonTGroupementEntreprises(false);

                      foreach ($collCommonTGroupementEntreprises as $obj) {
                        if (false == $this->collCommonTGroupementEntreprises->contains($obj)) {
                          $this->collCommonTGroupementEntreprises->append($obj);
                        }
                      }

                      $this->collCommonTGroupementEntreprisesPartial = true;
                    }

                    $collCommonTGroupementEntreprises->getInternalIterator()->rewind();

                    return $collCommonTGroupementEntreprises;
                }

                if ($partial && $this->collCommonTGroupementEntreprises) {
                    foreach ($this->collCommonTGroupementEntreprises as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTGroupementEntreprises[] = $obj;
                        }
                    }
                }

                $this->collCommonTGroupementEntreprises = $collCommonTGroupementEntreprises;
                $this->collCommonTGroupementEntreprisesPartial = false;
            }
        }

        return $this->collCommonTGroupementEntreprises;
    }

    /**
     * Sets a collection of CommonTGroupementEntreprise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTGroupementEntreprises A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setCommonTGroupementEntreprises(PropelCollection $commonTGroupementEntreprises, PropelPDO $con = null)
    {
        $commonTGroupementEntreprisesToDelete = $this->getCommonTGroupementEntreprises(new Criteria(), $con)->diff($commonTGroupementEntreprises);


        $this->commonTGroupementEntreprisesScheduledForDeletion = $commonTGroupementEntreprisesToDelete;

        foreach ($commonTGroupementEntreprisesToDelete as $commonTGroupementEntrepriseRemoved) {
            $commonTGroupementEntrepriseRemoved->setCommonTCandidature(null);
        }

        $this->collCommonTGroupementEntreprises = null;
        foreach ($commonTGroupementEntreprises as $commonTGroupementEntreprise) {
            $this->addCommonTGroupementEntreprise($commonTGroupementEntreprise);
        }

        $this->collCommonTGroupementEntreprises = $commonTGroupementEntreprises;
        $this->collCommonTGroupementEntreprisesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTGroupementEntreprise objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTGroupementEntreprise objects.
     * @throws PropelException
     */
    public function countCommonTGroupementEntreprises(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTGroupementEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTGroupementEntreprises || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTGroupementEntreprises) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTGroupementEntreprises());
            }
            $query = CommonTGroupementEntrepriseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTCandidature($this)
                ->count($con);
        }

        return count($this->collCommonTGroupementEntreprises);
    }

    /**
     * Method called to associate a CommonTGroupementEntreprise object to this object
     * through the CommonTGroupementEntreprise foreign key attribute.
     *
     * @param   CommonTGroupementEntreprise $l CommonTGroupementEntreprise
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function addCommonTGroupementEntreprise(CommonTGroupementEntreprise $l)
    {
        if ($this->collCommonTGroupementEntreprises === null) {
            $this->initCommonTGroupementEntreprises();
            $this->collCommonTGroupementEntreprisesPartial = true;
        }
        if (!in_array($l, $this->collCommonTGroupementEntreprises->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTGroupementEntreprise($l);
        }

        return $this;
    }

    /**
     * @param	CommonTGroupementEntreprise $commonTGroupementEntreprise The commonTGroupementEntreprise object to add.
     */
    protected function doAddCommonTGroupementEntreprise($commonTGroupementEntreprise)
    {
        $this->collCommonTGroupementEntreprises[]= $commonTGroupementEntreprise;
        $commonTGroupementEntreprise->setCommonTCandidature($this);
    }

    /**
     * @param	CommonTGroupementEntreprise $commonTGroupementEntreprise The commonTGroupementEntreprise object to remove.
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function removeCommonTGroupementEntreprise($commonTGroupementEntreprise)
    {
        if ($this->getCommonTGroupementEntreprises()->contains($commonTGroupementEntreprise)) {
            $this->collCommonTGroupementEntreprises->remove($this->collCommonTGroupementEntreprises->search($commonTGroupementEntreprise));
            if (null === $this->commonTGroupementEntreprisesScheduledForDeletion) {
                $this->commonTGroupementEntreprisesScheduledForDeletion = clone $this->collCommonTGroupementEntreprises;
                $this->commonTGroupementEntreprisesScheduledForDeletion->clear();
            }
            $this->commonTGroupementEntreprisesScheduledForDeletion[]= $commonTGroupementEntreprise;
            $commonTGroupementEntreprise->setCommonTCandidature(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTCandidature is new, it will return
     * an empty collection; or if this CommonTCandidature has previously
     * been saved, it will retrieve related CommonTGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTCandidature.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTGroupementEntreprise[] List of CommonTGroupementEntreprise objects
     */
    public function getCommonTGroupementEntreprisesJoinCommonOffres($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonOffres', $join_behavior);

        return $this->getCommonTGroupementEntreprises($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTCandidature is new, it will return
     * an empty collection; or if this CommonTCandidature has previously
     * been saved, it will retrieve related CommonTGroupementEntreprises from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTCandidature.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTGroupementEntreprise[] List of CommonTGroupementEntreprise objects
     */
    public function getCommonTGroupementEntreprisesJoinCommonTTypeGroupementEntreprise($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTGroupementEntrepriseQuery::create(null, $criteria);
        $query->joinWith('CommonTTypeGroupementEntreprise', $join_behavior);

        return $this->getCommonTGroupementEntreprises($query, $con);
    }

    /**
     * Clears out the collCommonTListeLotsCandidatures collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTCandidature The current object (for fluent API support)
     * @see        addCommonTListeLotsCandidatures()
     */
    public function clearCommonTListeLotsCandidatures()
    {
        $this->collCommonTListeLotsCandidatures = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTListeLotsCandidaturesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTListeLotsCandidatures collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTListeLotsCandidatures($v = true)
    {
        $this->collCommonTListeLotsCandidaturesPartial = $v;
    }

    /**
     * Initializes the collCommonTListeLotsCandidatures collection.
     *
     * By default this just sets the collCommonTListeLotsCandidatures collection to an empty array (like clearcollCommonTListeLotsCandidatures());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTListeLotsCandidatures($overrideExisting = true)
    {
        if (null !== $this->collCommonTListeLotsCandidatures && !$overrideExisting) {
            return;
        }
        $this->collCommonTListeLotsCandidatures = new PropelObjectCollection();
        $this->collCommonTListeLotsCandidatures->setModel('CommonTListeLotsCandidature');
    }

    /**
     * Gets an array of CommonTListeLotsCandidature objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTCandidature is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     * @throws PropelException
     */
    public function getCommonTListeLotsCandidatures($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTListeLotsCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTListeLotsCandidatures || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTListeLotsCandidatures) {
                // return empty collection
                $this->initCommonTListeLotsCandidatures();
            } else {
                $collCommonTListeLotsCandidatures = CommonTListeLotsCandidatureQuery::create(null, $criteria)
                    ->filterByCommonTCandidature($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTListeLotsCandidaturesPartial && count($collCommonTListeLotsCandidatures)) {
                      $this->initCommonTListeLotsCandidatures(false);

                      foreach ($collCommonTListeLotsCandidatures as $obj) {
                        if (false == $this->collCommonTListeLotsCandidatures->contains($obj)) {
                          $this->collCommonTListeLotsCandidatures->append($obj);
                        }
                      }

                      $this->collCommonTListeLotsCandidaturesPartial = true;
                    }

                    $collCommonTListeLotsCandidatures->getInternalIterator()->rewind();

                    return $collCommonTListeLotsCandidatures;
                }

                if ($partial && $this->collCommonTListeLotsCandidatures) {
                    foreach ($this->collCommonTListeLotsCandidatures as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTListeLotsCandidatures[] = $obj;
                        }
                    }
                }

                $this->collCommonTListeLotsCandidatures = $collCommonTListeLotsCandidatures;
                $this->collCommonTListeLotsCandidaturesPartial = false;
            }
        }

        return $this->collCommonTListeLotsCandidatures;
    }

    /**
     * Sets a collection of CommonTListeLotsCandidature objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTListeLotsCandidatures A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function setCommonTListeLotsCandidatures(PropelCollection $commonTListeLotsCandidatures, PropelPDO $con = null)
    {
        $commonTListeLotsCandidaturesToDelete = $this->getCommonTListeLotsCandidatures(new Criteria(), $con)->diff($commonTListeLotsCandidatures);


        $this->commonTListeLotsCandidaturesScheduledForDeletion = $commonTListeLotsCandidaturesToDelete;

        foreach ($commonTListeLotsCandidaturesToDelete as $commonTListeLotsCandidatureRemoved) {
            $commonTListeLotsCandidatureRemoved->setCommonTCandidature(null);
        }

        $this->collCommonTListeLotsCandidatures = null;
        foreach ($commonTListeLotsCandidatures as $commonTListeLotsCandidature) {
            $this->addCommonTListeLotsCandidature($commonTListeLotsCandidature);
        }

        $this->collCommonTListeLotsCandidatures = $commonTListeLotsCandidatures;
        $this->collCommonTListeLotsCandidaturesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTListeLotsCandidature objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTListeLotsCandidature objects.
     * @throws PropelException
     */
    public function countCommonTListeLotsCandidatures(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTListeLotsCandidaturesPartial && !$this->isNew();
        if (null === $this->collCommonTListeLotsCandidatures || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTListeLotsCandidatures) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTListeLotsCandidatures());
            }
            $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTCandidature($this)
                ->count($con);
        }

        return count($this->collCommonTListeLotsCandidatures);
    }

    /**
     * Method called to associate a CommonTListeLotsCandidature object to this object
     * through the CommonTListeLotsCandidature foreign key attribute.
     *
     * @param   CommonTListeLotsCandidature $l CommonTListeLotsCandidature
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function addCommonTListeLotsCandidature(CommonTListeLotsCandidature $l)
    {
        if ($this->collCommonTListeLotsCandidatures === null) {
            $this->initCommonTListeLotsCandidatures();
            $this->collCommonTListeLotsCandidaturesPartial = true;
        }
        if (!in_array($l, $this->collCommonTListeLotsCandidatures->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTListeLotsCandidature($l);
        }

        return $this;
    }

    /**
     * @param	CommonTListeLotsCandidature $commonTListeLotsCandidature The commonTListeLotsCandidature object to add.
     */
    protected function doAddCommonTListeLotsCandidature($commonTListeLotsCandidature)
    {
        $this->collCommonTListeLotsCandidatures[]= $commonTListeLotsCandidature;
        $commonTListeLotsCandidature->setCommonTCandidature($this);
    }

    /**
     * @param	CommonTListeLotsCandidature $commonTListeLotsCandidature The commonTListeLotsCandidature object to remove.
     * @return CommonTCandidature The current object (for fluent API support)
     */
    public function removeCommonTListeLotsCandidature($commonTListeLotsCandidature)
    {
        if ($this->getCommonTListeLotsCandidatures()->contains($commonTListeLotsCandidature)) {
            $this->collCommonTListeLotsCandidatures->remove($this->collCommonTListeLotsCandidatures->search($commonTListeLotsCandidature));
            if (null === $this->commonTListeLotsCandidaturesScheduledForDeletion) {
                $this->commonTListeLotsCandidaturesScheduledForDeletion = clone $this->collCommonTListeLotsCandidatures;
                $this->commonTListeLotsCandidaturesScheduledForDeletion->clear();
            }
            $this->commonTListeLotsCandidaturesScheduledForDeletion[]= $commonTListeLotsCandidature;
            $commonTListeLotsCandidature->setCommonTCandidature(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTCandidature is new, it will return
     * an empty collection; or if this CommonTCandidature has previously
     * been saved, it will retrieve related CommonTListeLotsCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTCandidature.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     */
    public function getCommonTListeLotsCandidaturesJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonTListeLotsCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTCandidature is new, it will return
     * an empty collection; or if this CommonTCandidature has previously
     * been saved, it will retrieve related CommonTListeLotsCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTCandidature.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     */
    public function getCommonTListeLotsCandidaturesJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonTListeLotsCandidatures($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTCandidature is new, it will return
     * an empty collection; or if this CommonTCandidature has previously
     * been saved, it will retrieve related CommonTListeLotsCandidatures from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTCandidature.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTListeLotsCandidature[] List of CommonTListeLotsCandidature objects
     */
    public function getCommonTListeLotsCandidaturesJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTListeLotsCandidatureQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonTListeLotsCandidatures($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->ref_consultation = null;
        $this->organisme = null;
        $this->old_id_inscrit = null;
        $this->id_entreprise = null;
        $this->id_etablissement = null;
        $this->status = null;
        $this->type_candidature = null;
        $this->type_candidature_dume = null;
        $this->id_dume_contexte = null;
        $this->id_offre = null;
        $this->role_inscrit = null;
        $this->date_derniere_validation_dume = null;
        $this->consultation_id = null;
        $this->id_inscrit = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTGroupementEntreprises) {
                foreach ($this->collCommonTGroupementEntreprises as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTListeLotsCandidatures) {
                foreach ($this->collCommonTListeLotsCandidatures as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonConsultation instanceof Persistent) {
              $this->aCommonConsultation->clearAllReferences($deep);
            }
            if ($this->aCommonTDumeContexte instanceof Persistent) {
              $this->aCommonTDumeContexte->clearAllReferences($deep);
            }
            if ($this->aCommonInscrit instanceof Persistent) {
              $this->aCommonInscrit->clearAllReferences($deep);
            }
            if ($this->aCommonOffres instanceof Persistent) {
              $this->aCommonOffres->clearAllReferences($deep);
            }
            if ($this->aCommonOrganisme instanceof Persistent) {
              $this->aCommonOrganisme->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTGroupementEntreprises instanceof PropelCollection) {
            $this->collCommonTGroupementEntreprises->clearIterator();
        }
        $this->collCommonTGroupementEntreprises = null;
        if ($this->collCommonTListeLotsCandidatures instanceof PropelCollection) {
            $this->collCommonTListeLotsCandidatures->clearIterator();
        }
        $this->collCommonTListeLotsCandidatures = null;
        $this->aCommonConsultation = null;
        $this->aCommonTDumeContexte = null;
        $this->aCommonInscrit = null;
        $this->aCommonOffres = null;
        $this->aCommonOrganisme = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTCandidaturePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
