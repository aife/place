<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonEchangeDocApplicationClient;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientOrganisme;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientOrganismePeer;
use Application\Propel\Mpe\CommonEchangeDocApplicationClientOrganismeQuery;
use Application\Propel\Mpe\CommonOrganisme;

/**
 * Base class that represents a query for the 'echange_doc_application_client_organisme' table.
 *
 *
 *
 * @method CommonEchangeDocApplicationClientOrganismeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonEchangeDocApplicationClientOrganismeQuery orderByEchangeDocApplicationClientId($order = Criteria::ASC) Order by the echange_doc_application_client_id column
 * @method CommonEchangeDocApplicationClientOrganismeQuery orderByOrganismeAcronyme($order = Criteria::ASC) Order by the organisme_acronyme column
 *
 * @method CommonEchangeDocApplicationClientOrganismeQuery groupById() Group by the id column
 * @method CommonEchangeDocApplicationClientOrganismeQuery groupByEchangeDocApplicationClientId() Group by the echange_doc_application_client_id column
 * @method CommonEchangeDocApplicationClientOrganismeQuery groupByOrganismeAcronyme() Group by the organisme_acronyme column
 *
 * @method CommonEchangeDocApplicationClientOrganismeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonEchangeDocApplicationClientOrganismeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonEchangeDocApplicationClientOrganismeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonEchangeDocApplicationClientOrganismeQuery leftJoinCommonEchangeDocApplicationClient($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocApplicationClient relation
 * @method CommonEchangeDocApplicationClientOrganismeQuery rightJoinCommonEchangeDocApplicationClient($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocApplicationClient relation
 * @method CommonEchangeDocApplicationClientOrganismeQuery innerJoinCommonEchangeDocApplicationClient($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocApplicationClient relation
 *
 * @method CommonEchangeDocApplicationClientOrganismeQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonEchangeDocApplicationClientOrganismeQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonEchangeDocApplicationClientOrganismeQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonEchangeDocApplicationClientOrganisme findOne(PropelPDO $con = null) Return the first CommonEchangeDocApplicationClientOrganisme matching the query
 * @method CommonEchangeDocApplicationClientOrganisme findOneOrCreate(PropelPDO $con = null) Return the first CommonEchangeDocApplicationClientOrganisme matching the query, or a new CommonEchangeDocApplicationClientOrganisme object populated from the query conditions when no match is found
 *
 * @method CommonEchangeDocApplicationClientOrganisme findOneByEchangeDocApplicationClientId(int $echange_doc_application_client_id) Return the first CommonEchangeDocApplicationClientOrganisme filtered by the echange_doc_application_client_id column
 * @method CommonEchangeDocApplicationClientOrganisme findOneByOrganismeAcronyme(string $organisme_acronyme) Return the first CommonEchangeDocApplicationClientOrganisme filtered by the organisme_acronyme column
 *
 * @method array findById(int $id) Return CommonEchangeDocApplicationClientOrganisme objects filtered by the id column
 * @method array findByEchangeDocApplicationClientId(int $echange_doc_application_client_id) Return CommonEchangeDocApplicationClientOrganisme objects filtered by the echange_doc_application_client_id column
 * @method array findByOrganismeAcronyme(string $organisme_acronyme) Return CommonEchangeDocApplicationClientOrganisme objects filtered by the organisme_acronyme column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangeDocApplicationClientOrganismeQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonEchangeDocApplicationClientOrganismeQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonEchangeDocApplicationClientOrganisme', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonEchangeDocApplicationClientOrganismeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonEchangeDocApplicationClientOrganismeQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonEchangeDocApplicationClientOrganismeQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonEchangeDocApplicationClientOrganismeQuery) {
            return $criteria;
        }
        $query = new CommonEchangeDocApplicationClientOrganismeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonEchangeDocApplicationClientOrganisme|CommonEchangeDocApplicationClientOrganisme[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonEchangeDocApplicationClientOrganismePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocApplicationClientOrganismePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangeDocApplicationClientOrganisme A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangeDocApplicationClientOrganisme A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `echange_doc_application_client_id`, `organisme_acronyme` FROM `echange_doc_application_client_organisme` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonEchangeDocApplicationClientOrganisme();
            $obj->hydrate($row);
            CommonEchangeDocApplicationClientOrganismePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonEchangeDocApplicationClientOrganisme|CommonEchangeDocApplicationClientOrganisme[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonEchangeDocApplicationClientOrganisme[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonEchangeDocApplicationClientOrganismeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonEchangeDocApplicationClientOrganismeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientOrganismeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the echange_doc_application_client_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEchangeDocApplicationClientId(1234); // WHERE echange_doc_application_client_id = 1234
     * $query->filterByEchangeDocApplicationClientId(array(12, 34)); // WHERE echange_doc_application_client_id IN (12, 34)
     * $query->filterByEchangeDocApplicationClientId(array('min' => 12)); // WHERE echange_doc_application_client_id >= 12
     * $query->filterByEchangeDocApplicationClientId(array('max' => 12)); // WHERE echange_doc_application_client_id <= 12
     * </code>
     *
     * @see       filterByCommonEchangeDocApplicationClient()
     *
     * @param     mixed $echangeDocApplicationClientId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientOrganismeQuery The current query, for fluid interface
     */
    public function filterByEchangeDocApplicationClientId($echangeDocApplicationClientId = null, $comparison = null)
    {
        if (is_array($echangeDocApplicationClientId)) {
            $useMinMax = false;
            if (isset($echangeDocApplicationClientId['min'])) {
                $this->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, $echangeDocApplicationClientId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($echangeDocApplicationClientId['max'])) {
                $this->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, $echangeDocApplicationClientId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, $echangeDocApplicationClientId, $comparison);
    }

    /**
     * Filter the query on the organisme_acronyme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganismeAcronyme('fooValue');   // WHERE organisme_acronyme = 'fooValue'
     * $query->filterByOrganismeAcronyme('%fooValue%'); // WHERE organisme_acronyme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organismeAcronyme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocApplicationClientOrganismeQuery The current query, for fluid interface
     */
    public function filterByOrganismeAcronyme($organismeAcronyme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organismeAcronyme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organismeAcronyme)) {
                $organismeAcronyme = str_replace('*', '%', $organismeAcronyme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ORGANISME_ACRONYME, $organismeAcronyme, $comparison);
    }

    /**
     * Filter the query by a related CommonEchangeDocApplicationClient object
     *
     * @param   CommonEchangeDocApplicationClient|PropelObjectCollection $commonEchangeDocApplicationClient The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocApplicationClientOrganismeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocApplicationClient($commonEchangeDocApplicationClient, $comparison = null)
    {
        if ($commonEchangeDocApplicationClient instanceof CommonEchangeDocApplicationClient) {
            return $this
                ->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, $commonEchangeDocApplicationClient->getId(), $comparison);
        } elseif ($commonEchangeDocApplicationClient instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ECHANGE_DOC_APPLICATION_CLIENT_ID, $commonEchangeDocApplicationClient->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonEchangeDocApplicationClient() only accepts arguments of type CommonEchangeDocApplicationClient or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocApplicationClient relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocApplicationClientOrganismeQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocApplicationClient($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocApplicationClient');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocApplicationClient');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocApplicationClient relation CommonEchangeDocApplicationClient object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocApplicationClientQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocApplicationClientQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocApplicationClient($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocApplicationClient', '\Application\Propel\Mpe\CommonEchangeDocApplicationClientQuery');
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocApplicationClientOrganismeQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ORGANISME_ACRONYME, $commonOrganisme->getAcronyme(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ORGANISME_ACRONYME, $commonOrganisme->toKeyValue('PrimaryKey', 'Acronyme'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocApplicationClientOrganismeQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonEchangeDocApplicationClientOrganisme $commonEchangeDocApplicationClientOrganisme Object to remove from the list of results
     *
     * @return CommonEchangeDocApplicationClientOrganismeQuery The current query, for fluid interface
     */
    public function prune($commonEchangeDocApplicationClientOrganisme = null)
    {
        if ($commonEchangeDocApplicationClientOrganisme) {
            $this->addUsingAlias(CommonEchangeDocApplicationClientOrganismePeer::ID, $commonEchangeDocApplicationClientOrganisme->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
