<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAffiliationService;
use Application\Propel\Mpe\CommonAffiliationServiceQuery;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonAgentTechniqueAssociation;
use Application\Propel\Mpe\CommonAgentTechniqueAssociationQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonEncherePmi;
use Application\Propel\Mpe\CommonEncherePmiQuery;
use Application\Propel\Mpe\CommonInvitePermanentTransverse;
use Application\Propel\Mpe\CommonInvitePermanentTransverseQuery;
use Application\Propel\Mpe\CommonNewsletter;
use Application\Propel\Mpe\CommonNewsletterQuery;
use Application\Propel\Mpe\CommonParametrageEnchere;
use Application\Propel\Mpe\CommonParametrageEnchereQuery;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonServiceQuery;
use Application\Propel\Mpe\CommonTFusionnerServices;
use Application\Propel\Mpe\CommonTFusionnerServicesQuery;
use Application\Propel\Mpe\CommonTTelechargementAsynchrone;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneQuery;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Propel\Mpe\CommonTypeProcedureOrganismeQuery;

/**
 * Base class that represents a row from the 'Service' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonService extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonServicePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonServicePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the old_id field.
     * @var        int
     */
    protected $old_id;

    /**
     * The value for the organisme field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the type_service field.
     * Note: this column has a database default value of: '2'
     * @var        string
     */
    protected $type_service;

    /**
     * The value for the libelle field.
     * @var        string
     */
    protected $libelle;

    /**
     * The value for the sigle field.
     * @var        string
     */
    protected $sigle;

    /**
     * The value for the adresse field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse;

    /**
     * The value for the adresse_suite field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_suite;

    /**
     * The value for the cp field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $cp;

    /**
     * The value for the ville field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville;

    /**
     * The value for the telephone field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $telephone;

    /**
     * The value for the fax field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $fax;

    /**
     * The value for the mail field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $mail;

    /**
     * The value for the pays field.
     * @var        string
     */
    protected $pays;

    /**
     * The value for the id_externe field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $id_externe;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_modification field.
     * @var        string
     */
    protected $date_modification;

    /**
     * The value for the siren field.
     * @var        string
     */
    protected $siren;

    /**
     * The value for the complement field.
     * @var        string
     */
    protected $complement;

    /**
     * The value for the libelle_ar field.
     * @var        string
     */
    protected $libelle_ar;

    /**
     * The value for the adresse_ar field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_ar;

    /**
     * The value for the adresse_suite_ar field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_suite_ar;

    /**
     * The value for the ville_ar field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_ar;

    /**
     * The value for the pays_ar field.
     * @var        string
     */
    protected $pays_ar;

    /**
     * The value for the libelle_fr field.
     * @var        string
     */
    protected $libelle_fr;

    /**
     * The value for the adresse_fr field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_fr;

    /**
     * The value for the adresse_suite_fr field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_suite_fr;

    /**
     * The value for the ville_fr field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_fr;

    /**
     * The value for the pays_fr field.
     * @var        string
     */
    protected $pays_fr;

    /**
     * The value for the libelle_es field.
     * @var        string
     */
    protected $libelle_es;

    /**
     * The value for the adresse_es field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_es;

    /**
     * The value for the adresse_suite_es field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_suite_es;

    /**
     * The value for the ville_es field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_es;

    /**
     * The value for the pays_es field.
     * @var        string
     */
    protected $pays_es;

    /**
     * The value for the libelle_en field.
     * @var        string
     */
    protected $libelle_en;

    /**
     * The value for the adresse_en field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_en;

    /**
     * The value for the adresse_suite_en field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_suite_en;

    /**
     * The value for the ville_en field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_en;

    /**
     * The value for the pays_en field.
     * @var        string
     */
    protected $pays_en;

    /**
     * The value for the libelle_su field.
     * @var        string
     */
    protected $libelle_su;

    /**
     * The value for the adresse_su field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_su;

    /**
     * The value for the adresse_suite_su field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_suite_su;

    /**
     * The value for the ville_su field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_su;

    /**
     * The value for the pays_su field.
     * @var        string
     */
    protected $pays_su;

    /**
     * The value for the libelle_du field.
     * @var        string
     */
    protected $libelle_du;

    /**
     * The value for the adresse_du field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_du;

    /**
     * The value for the adresse_suite_du field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_suite_du;

    /**
     * The value for the ville_du field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_du;

    /**
     * The value for the pays_du field.
     * @var        string
     */
    protected $pays_du;

    /**
     * The value for the libelle_cz field.
     * @var        string
     */
    protected $libelle_cz;

    /**
     * The value for the adresse_cz field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_cz;

    /**
     * The value for the adresse_suite_cz field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_suite_cz;

    /**
     * The value for the ville_cz field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_cz;

    /**
     * The value for the pays_cz field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $pays_cz;

    /**
     * The value for the libelle_it field.
     * @var        string
     */
    protected $libelle_it;

    /**
     * The value for the adresse_it field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_it;

    /**
     * The value for the adresse_suite_it field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adresse_suite_it;

    /**
     * The value for the ville_it field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $ville_it;

    /**
     * The value for the pays_it field.
     * @var        string
     */
    protected $pays_it;

    /**
     * The value for the chemin_complet field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $chemin_complet;

    /**
     * The value for the chemin_complet_fr field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $chemin_complet_fr;

    /**
     * The value for the chemin_complet_en field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $chemin_complet_en;

    /**
     * The value for the chemin_complet_es field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $chemin_complet_es;

    /**
     * The value for the chemin_complet_su field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $chemin_complet_su;

    /**
     * The value for the chemin_complet_du field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $chemin_complet_du;

    /**
     * The value for the chemin_complet_cz field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $chemin_complet_cz;

    /**
     * The value for the chemin_complet_ar field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $chemin_complet_ar;

    /**
     * The value for the chemin_complet_it field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $chemin_complet_it;

    /**
     * The value for the nom_service_archiveur field.
     * @var        string
     */
    protected $nom_service_archiveur;

    /**
     * The value for the identifiant_service_archiveur field.
     * @var        string
     */
    protected $identifiant_service_archiveur;

    /**
     * The value for the affichage_service field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $affichage_service;

    /**
     * The value for the activation_fuseau_horaire field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $activation_fuseau_horaire;

    /**
     * The value for the decalage_horaire field.
     * @var        string
     */
    protected $decalage_horaire;

    /**
     * The value for the lieu_residence field.
     * @var        string
     */
    protected $lieu_residence;

    /**
     * The value for the alerte field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte;

    /**
     * The value for the acces_chorus field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $acces_chorus;

    /**
     * The value for the forme_juridique field.
     * @var        string
     */
    protected $forme_juridique;

    /**
     * The value for the forme_juridique_code field.
     * @var        string
     */
    protected $forme_juridique_code;

    /**
     * The value for the synchronisation_exec field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $synchronisation_exec;

    /**
     * The value for the id_entite field.
     * @var        int
     */
    protected $id_entite;

    /**
     * The value for the id field.
     * @var        string
     */
    protected $id;

    /**
     * @var        PropelObjectCollection|CommonAffiliationService[] Collection to store aggregation of CommonAffiliationService objects.
     */
    protected $collCommonAffiliationServicesRelatedByServiceId;
    protected $collCommonAffiliationServicesRelatedByServiceIdPartial;

    /**
     * @var        PropelObjectCollection|CommonAffiliationService[] Collection to store aggregation of CommonAffiliationService objects.
     */
    protected $collCommonAffiliationServicesRelatedByServiceParentId;
    protected $collCommonAffiliationServicesRelatedByServiceParentIdPartial;

    /**
     * @var        PropelObjectCollection|CommonAgent[] Collection to store aggregation of CommonAgent objects.
     */
    protected $collCommonAgents;
    protected $collCommonAgentsPartial;

    /**
     * @var        PropelObjectCollection|CommonEncherePmi[] Collection to store aggregation of CommonEncherePmi objects.
     */
    protected $collCommonEncherePmis;
    protected $collCommonEncherePmisPartial;

    /**
     * @var        PropelObjectCollection|CommonNewsletter[] Collection to store aggregation of CommonNewsletter objects.
     */
    protected $collCommonNewslettersRelatedByIdServiceDestinataire;
    protected $collCommonNewslettersRelatedByIdServiceDestinatairePartial;

    /**
     * @var        PropelObjectCollection|CommonNewsletter[] Collection to store aggregation of CommonNewsletter objects.
     */
    protected $collCommonNewslettersRelatedByIdServiceRedacteur;
    protected $collCommonNewslettersRelatedByIdServiceRedacteurPartial;

    /**
     * @var        PropelObjectCollection|CommonParametrageEnchere[] Collection to store aggregation of CommonParametrageEnchere objects.
     */
    protected $collCommonParametrageEncheres;
    protected $collCommonParametrageEncheresPartial;

    /**
     * @var        PropelObjectCollection|CommonTTelechargementAsynchrone[] Collection to store aggregation of CommonTTelechargementAsynchrone objects.
     */
    protected $collCommonTTelechargementAsynchrones;
    protected $collCommonTTelechargementAsynchronesPartial;

    /**
     * @var        PropelObjectCollection|CommonTypeProcedureOrganisme[] Collection to store aggregation of CommonTypeProcedureOrganisme objects.
     */
    protected $collCommonTypeProcedureOrganismes;
    protected $collCommonTypeProcedureOrganismesPartial;

    /**
     * @var        PropelObjectCollection|CommonAgentTechniqueAssociation[] Collection to store aggregation of CommonAgentTechniqueAssociation objects.
     */
    protected $collCommonAgentTechniqueAssociations;
    protected $collCommonAgentTechniqueAssociationsPartial;

    /**
     * @var        PropelObjectCollection|CommonConsultation[] Collection to store aggregation of CommonConsultation objects.
     */
    protected $collCommonConsultationsRelatedByServiceValidation;
    protected $collCommonConsultationsRelatedByServiceValidationPartial;

    /**
     * @var        PropelObjectCollection|CommonConsultation[] Collection to store aggregation of CommonConsultation objects.
     */
    protected $collCommonConsultationsRelatedByServiceId;
    protected $collCommonConsultationsRelatedByServiceIdPartial;

    /**
     * @var        PropelObjectCollection|CommonConsultation[] Collection to store aggregation of CommonConsultation objects.
     */
    protected $collCommonConsultationsRelatedByServiceValidationIntermediaire;
    protected $collCommonConsultationsRelatedByServiceValidationIntermediairePartial;

    /**
     * @var        PropelObjectCollection|CommonInvitePermanentTransverse[] Collection to store aggregation of CommonInvitePermanentTransverse objects.
     */
    protected $collCommonInvitePermanentTransverses;
    protected $collCommonInvitePermanentTransversesPartial;

    /**
     * @var        PropelObjectCollection|CommonTFusionnerServices[] Collection to store aggregation of CommonTFusionnerServices objects.
     */
    protected $collCommonTFusionnerServicessRelatedByIdServiceCible;
    protected $collCommonTFusionnerServicessRelatedByIdServiceCiblePartial;

    /**
     * @var        PropelObjectCollection|CommonTFusionnerServices[] Collection to store aggregation of CommonTFusionnerServices objects.
     */
    protected $collCommonTFusionnerServicessRelatedByIdServiceSource;
    protected $collCommonTFusionnerServicessRelatedByIdServiceSourcePartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAffiliationServicesRelatedByServiceIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAffiliationServicesRelatedByServiceParentIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAgentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEncherePmisScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonNewslettersRelatedByIdServiceDestinataireScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonNewslettersRelatedByIdServiceRedacteurScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonParametrageEncheresScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTTelechargementAsynchronesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTypeProcedureOrganismesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAgentTechniqueAssociationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationsRelatedByServiceValidationScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationsRelatedByServiceIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationsRelatedByServiceValidationIntermediaireScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonInvitePermanentTransversesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTFusionnerServicessRelatedByIdServiceCibleScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTFusionnerServicessRelatedByIdServiceSourceScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->organisme = '';
        $this->type_service = '2';
        $this->adresse = '';
        $this->adresse_suite = '';
        $this->cp = '';
        $this->ville = '';
        $this->telephone = '';
        $this->fax = '';
        $this->mail = '';
        $this->id_externe = '0';
        $this->adresse_ar = '';
        $this->adresse_suite_ar = '';
        $this->ville_ar = '';
        $this->adresse_fr = '';
        $this->adresse_suite_fr = '';
        $this->ville_fr = '';
        $this->adresse_es = '';
        $this->adresse_suite_es = '';
        $this->ville_es = '';
        $this->adresse_en = '';
        $this->adresse_suite_en = '';
        $this->ville_en = '';
        $this->adresse_su = '';
        $this->adresse_suite_su = '';
        $this->ville_su = '';
        $this->adresse_du = '';
        $this->adresse_suite_du = '';
        $this->ville_du = '';
        $this->adresse_cz = '';
        $this->adresse_suite_cz = '';
        $this->ville_cz = '';
        $this->pays_cz = '';
        $this->adresse_it = '';
        $this->adresse_suite_it = '';
        $this->ville_it = '';
        $this->chemin_complet = '';
        $this->chemin_complet_fr = '';
        $this->chemin_complet_en = '';
        $this->chemin_complet_es = '';
        $this->chemin_complet_su = '';
        $this->chemin_complet_du = '';
        $this->chemin_complet_cz = '';
        $this->chemin_complet_ar = '';
        $this->chemin_complet_it = '';
        $this->affichage_service = '1';
        $this->activation_fuseau_horaire = '0';
        $this->alerte = '0';
        $this->acces_chorus = '0';
        $this->synchronisation_exec = '1';
    }

    /**
     * Initializes internal state of BaseCommonService object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [old_id] column value.
     *
     * @return int
     */
    public function getOldId()
    {

        return $this->old_id;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [type_service] column value.
     *
     * @return string
     */
    public function getTypeService()
    {

        return $this->type_service;
    }

    /**
     * Get the [libelle] column value.
     *
     * @return string
     */
    public function getLibelle()
    {

        return $this->libelle;
    }

    /**
     * Get the [sigle] column value.
     *
     * @return string
     */
    public function getSigle()
    {

        return $this->sigle;
    }

    /**
     * Get the [adresse] column value.
     *
     * @return string
     */
    public function getAdresse()
    {

        return $this->adresse;
    }

    /**
     * Get the [adresse_suite] column value.
     *
     * @return string
     */
    public function getAdresseSuite()
    {

        return $this->adresse_suite;
    }

    /**
     * Get the [cp] column value.
     *
     * @return string
     */
    public function getCp()
    {

        return $this->cp;
    }

    /**
     * Get the [ville] column value.
     *
     * @return string
     */
    public function getVille()
    {

        return $this->ville;
    }

    /**
     * Get the [telephone] column value.
     *
     * @return string
     */
    public function getTelephone()
    {

        return $this->telephone;
    }

    /**
     * Get the [fax] column value.
     *
     * @return string
     */
    public function getFax()
    {

        return $this->fax;
    }

    /**
     * Get the [mail] column value.
     *
     * @return string
     */
    public function getMail()
    {

        return $this->mail;
    }

    /**
     * Get the [pays] column value.
     *
     * @return string
     */
    public function getPays()
    {

        return $this->pays;
    }

    /**
     * Get the [id_externe] column value.
     *
     * @return string
     */
    public function getIdExterne()
    {

        return $this->id_externe;
    }

    /**
     * Get the [date_creation] column value.
     *
     * @return string
     */
    public function getDateCreation()
    {

        return $this->date_creation;
    }

    /**
     * Get the [date_modification] column value.
     *
     * @return string
     */
    public function getDateModification()
    {

        return $this->date_modification;
    }

    /**
     * Get the [siren] column value.
     *
     * @return string
     */
    public function getSiren()
    {

        return $this->siren;
    }

    /**
     * Get the [complement] column value.
     *
     * @return string
     */
    public function getComplement()
    {

        return $this->complement;
    }

    /**
     * Get the [libelle_ar] column value.
     *
     * @return string
     */
    public function getLibelleAr()
    {

        return $this->libelle_ar;
    }

    /**
     * Get the [adresse_ar] column value.
     *
     * @return string
     */
    public function getAdresseAr()
    {

        return $this->adresse_ar;
    }

    /**
     * Get the [adresse_suite_ar] column value.
     *
     * @return string
     */
    public function getAdresseSuiteAr()
    {

        return $this->adresse_suite_ar;
    }

    /**
     * Get the [ville_ar] column value.
     *
     * @return string
     */
    public function getVilleAr()
    {

        return $this->ville_ar;
    }

    /**
     * Get the [pays_ar] column value.
     *
     * @return string
     */
    public function getPaysAr()
    {

        return $this->pays_ar;
    }

    /**
     * Get the [libelle_fr] column value.
     *
     * @return string
     */
    public function getLibelleFr()
    {

        return $this->libelle_fr;
    }

    /**
     * Get the [adresse_fr] column value.
     *
     * @return string
     */
    public function getAdresseFr()
    {

        return $this->adresse_fr;
    }

    /**
     * Get the [adresse_suite_fr] column value.
     *
     * @return string
     */
    public function getAdresseSuiteFr()
    {

        return $this->adresse_suite_fr;
    }

    /**
     * Get the [ville_fr] column value.
     *
     * @return string
     */
    public function getVilleFr()
    {

        return $this->ville_fr;
    }

    /**
     * Get the [pays_fr] column value.
     *
     * @return string
     */
    public function getPaysFr()
    {

        return $this->pays_fr;
    }

    /**
     * Get the [libelle_es] column value.
     *
     * @return string
     */
    public function getLibelleEs()
    {

        return $this->libelle_es;
    }

    /**
     * Get the [adresse_es] column value.
     *
     * @return string
     */
    public function getAdresseEs()
    {

        return $this->adresse_es;
    }

    /**
     * Get the [adresse_suite_es] column value.
     *
     * @return string
     */
    public function getAdresseSuiteEs()
    {

        return $this->adresse_suite_es;
    }

    /**
     * Get the [ville_es] column value.
     *
     * @return string
     */
    public function getVilleEs()
    {

        return $this->ville_es;
    }

    /**
     * Get the [pays_es] column value.
     *
     * @return string
     */
    public function getPaysEs()
    {

        return $this->pays_es;
    }

    /**
     * Get the [libelle_en] column value.
     *
     * @return string
     */
    public function getLibelleEn()
    {

        return $this->libelle_en;
    }

    /**
     * Get the [adresse_en] column value.
     *
     * @return string
     */
    public function getAdresseEn()
    {

        return $this->adresse_en;
    }

    /**
     * Get the [adresse_suite_en] column value.
     *
     * @return string
     */
    public function getAdresseSuiteEn()
    {

        return $this->adresse_suite_en;
    }

    /**
     * Get the [ville_en] column value.
     *
     * @return string
     */
    public function getVilleEn()
    {

        return $this->ville_en;
    }

    /**
     * Get the [pays_en] column value.
     *
     * @return string
     */
    public function getPaysEn()
    {

        return $this->pays_en;
    }

    /**
     * Get the [libelle_su] column value.
     *
     * @return string
     */
    public function getLibelleSu()
    {

        return $this->libelle_su;
    }

    /**
     * Get the [adresse_su] column value.
     *
     * @return string
     */
    public function getAdresseSu()
    {

        return $this->adresse_su;
    }

    /**
     * Get the [adresse_suite_su] column value.
     *
     * @return string
     */
    public function getAdresseSuiteSu()
    {

        return $this->adresse_suite_su;
    }

    /**
     * Get the [ville_su] column value.
     *
     * @return string
     */
    public function getVilleSu()
    {

        return $this->ville_su;
    }

    /**
     * Get the [pays_su] column value.
     *
     * @return string
     */
    public function getPaysSu()
    {

        return $this->pays_su;
    }

    /**
     * Get the [libelle_du] column value.
     *
     * @return string
     */
    public function getLibelleDu()
    {

        return $this->libelle_du;
    }

    /**
     * Get the [adresse_du] column value.
     *
     * @return string
     */
    public function getAdresseDu()
    {

        return $this->adresse_du;
    }

    /**
     * Get the [adresse_suite_du] column value.
     *
     * @return string
     */
    public function getAdresseSuiteDu()
    {

        return $this->adresse_suite_du;
    }

    /**
     * Get the [ville_du] column value.
     *
     * @return string
     */
    public function getVilleDu()
    {

        return $this->ville_du;
    }

    /**
     * Get the [pays_du] column value.
     *
     * @return string
     */
    public function getPaysDu()
    {

        return $this->pays_du;
    }

    /**
     * Get the [libelle_cz] column value.
     *
     * @return string
     */
    public function getLibelleCz()
    {

        return $this->libelle_cz;
    }

    /**
     * Get the [adresse_cz] column value.
     *
     * @return string
     */
    public function getAdresseCz()
    {

        return $this->adresse_cz;
    }

    /**
     * Get the [adresse_suite_cz] column value.
     *
     * @return string
     */
    public function getAdresseSuiteCz()
    {

        return $this->adresse_suite_cz;
    }

    /**
     * Get the [ville_cz] column value.
     *
     * @return string
     */
    public function getVilleCz()
    {

        return $this->ville_cz;
    }

    /**
     * Get the [pays_cz] column value.
     *
     * @return string
     */
    public function getPaysCz()
    {

        return $this->pays_cz;
    }

    /**
     * Get the [libelle_it] column value.
     *
     * @return string
     */
    public function getLibelleIt()
    {

        return $this->libelle_it;
    }

    /**
     * Get the [adresse_it] column value.
     *
     * @return string
     */
    public function getAdresseIt()
    {

        return $this->adresse_it;
    }

    /**
     * Get the [adresse_suite_it] column value.
     *
     * @return string
     */
    public function getAdresseSuiteIt()
    {

        return $this->adresse_suite_it;
    }

    /**
     * Get the [ville_it] column value.
     *
     * @return string
     */
    public function getVilleIt()
    {

        return $this->ville_it;
    }

    /**
     * Get the [pays_it] column value.
     *
     * @return string
     */
    public function getPaysIt()
    {

        return $this->pays_it;
    }

    /**
     * Get the [chemin_complet] column value.
     *
     * @return string
     */
    public function getCheminComplet()
    {

        return $this->chemin_complet;
    }

    /**
     * Get the [chemin_complet_fr] column value.
     *
     * @return string
     */
    public function getCheminCompletFr()
    {

        return $this->chemin_complet_fr;
    }

    /**
     * Get the [chemin_complet_en] column value.
     *
     * @return string
     */
    public function getCheminCompletEn()
    {

        return $this->chemin_complet_en;
    }

    /**
     * Get the [chemin_complet_es] column value.
     *
     * @return string
     */
    public function getCheminCompletEs()
    {

        return $this->chemin_complet_es;
    }

    /**
     * Get the [chemin_complet_su] column value.
     *
     * @return string
     */
    public function getCheminCompletSu()
    {

        return $this->chemin_complet_su;
    }

    /**
     * Get the [chemin_complet_du] column value.
     *
     * @return string
     */
    public function getCheminCompletDu()
    {

        return $this->chemin_complet_du;
    }

    /**
     * Get the [chemin_complet_cz] column value.
     *
     * @return string
     */
    public function getCheminCompletCz()
    {

        return $this->chemin_complet_cz;
    }

    /**
     * Get the [chemin_complet_ar] column value.
     *
     * @return string
     */
    public function getCheminCompletAr()
    {

        return $this->chemin_complet_ar;
    }

    /**
     * Get the [chemin_complet_it] column value.
     *
     * @return string
     */
    public function getCheminCompletIt()
    {

        return $this->chemin_complet_it;
    }

    /**
     * Get the [nom_service_archiveur] column value.
     *
     * @return string
     */
    public function getNomServiceArchiveur()
    {

        return $this->nom_service_archiveur;
    }

    /**
     * Get the [identifiant_service_archiveur] column value.
     *
     * @return string
     */
    public function getIdentifiantServiceArchiveur()
    {

        return $this->identifiant_service_archiveur;
    }

    /**
     * Get the [affichage_service] column value.
     *
     * @return string
     */
    public function getAffichageService()
    {

        return $this->affichage_service;
    }

    /**
     * Get the [activation_fuseau_horaire] column value.
     *
     * @return string
     */
    public function getActivationFuseauHoraire()
    {

        return $this->activation_fuseau_horaire;
    }

    /**
     * Get the [decalage_horaire] column value.
     *
     * @return string
     */
    public function getDecalageHoraire()
    {

        return $this->decalage_horaire;
    }

    /**
     * Get the [lieu_residence] column value.
     *
     * @return string
     */
    public function getLieuResidence()
    {

        return $this->lieu_residence;
    }

    /**
     * Get the [alerte] column value.
     *
     * @return string
     */
    public function getAlerte()
    {

        return $this->alerte;
    }

    /**
     * Get the [acces_chorus] column value.
     *
     * @return string
     */
    public function getAccesChorus()
    {

        return $this->acces_chorus;
    }

    /**
     * Get the [forme_juridique] column value.
     *
     * @return string
     */
    public function getFormeJuridique()
    {

        return $this->forme_juridique;
    }

    /**
     * Get the [forme_juridique_code] column value.
     *
     * @return string
     */
    public function getFormeJuridiqueCode()
    {

        return $this->forme_juridique_code;
    }

    /**
     * Get the [synchronisation_exec] column value.
     *
     * @return string
     */
    public function getSynchronisationExec()
    {

        return $this->synchronisation_exec;
    }

    /**
     * Get the [id_entite] column value.
     *
     * @return int
     */
    public function getIdEntite()
    {

        return $this->id_entite;
    }

    /**
     * Get the [id] column value.
     *
     * @return string
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Set the value of [old_id] column.
     *
     * @param int $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setOldId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_id !== $v) {
            $this->old_id = $v;
            $this->modifiedColumns[] = CommonServicePeer::OLD_ID;
        }


        return $this;
    } // setOldId()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonServicePeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [type_service] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setTypeService($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_service !== $v) {
            $this->type_service = $v;
            $this->modifiedColumns[] = CommonServicePeer::TYPE_SERVICE;
        }


        return $this;
    } // setTypeService()

    /**
     * Set the value of [libelle] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setLibelle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle !== $v) {
            $this->libelle = $v;
            $this->modifiedColumns[] = CommonServicePeer::LIBELLE;
        }


        return $this;
    } // setLibelle()

    /**
     * Set the value of [sigle] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setSigle($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sigle !== $v) {
            $this->sigle = $v;
            $this->modifiedColumns[] = CommonServicePeer::SIGLE;
        }


        return $this;
    } // setSigle()

    /**
     * Set the value of [adresse] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresse($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse !== $v) {
            $this->adresse = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE;
        }


        return $this;
    } // setAdresse()

    /**
     * Set the value of [adresse_suite] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseSuite($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_suite !== $v) {
            $this->adresse_suite = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_SUITE;
        }


        return $this;
    } // setAdresseSuite()

    /**
     * Set the value of [cp] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setCp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->cp !== $v) {
            $this->cp = $v;
            $this->modifiedColumns[] = CommonServicePeer::CP;
        }


        return $this;
    } // setCp()

    /**
     * Set the value of [ville] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setVille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville !== $v) {
            $this->ville = $v;
            $this->modifiedColumns[] = CommonServicePeer::VILLE;
        }


        return $this;
    } // setVille()

    /**
     * Set the value of [telephone] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setTelephone($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->telephone !== $v) {
            $this->telephone = $v;
            $this->modifiedColumns[] = CommonServicePeer::TELEPHONE;
        }


        return $this;
    } // setTelephone()

    /**
     * Set the value of [fax] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[] = CommonServicePeer::FAX;
        }


        return $this;
    } // setFax()

    /**
     * Set the value of [mail] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setMail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mail !== $v) {
            $this->mail = $v;
            $this->modifiedColumns[] = CommonServicePeer::MAIL;
        }


        return $this;
    } // setMail()

    /**
     * Set the value of [pays] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setPays($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays !== $v) {
            $this->pays = $v;
            $this->modifiedColumns[] = CommonServicePeer::PAYS;
        }


        return $this;
    } // setPays()

    /**
     * Set the value of [id_externe] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setIdExterne($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_externe !== $v) {
            $this->id_externe = $v;
            $this->modifiedColumns[] = CommonServicePeer::ID_EXTERNE;
        }


        return $this;
    } // setIdExterne()

    /**
     * Set the value of [date_creation] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_creation !== $v) {
            $this->date_creation = $v;
            $this->modifiedColumns[] = CommonServicePeer::DATE_CREATION;
        }


        return $this;
    } // setDateCreation()

    /**
     * Set the value of [date_modification] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setDateModification($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_modification !== $v) {
            $this->date_modification = $v;
            $this->modifiedColumns[] = CommonServicePeer::DATE_MODIFICATION;
        }


        return $this;
    } // setDateModification()

    /**
     * Set the value of [siren] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setSiren($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->siren !== $v) {
            $this->siren = $v;
            $this->modifiedColumns[] = CommonServicePeer::SIREN;
        }


        return $this;
    } // setSiren()

    /**
     * Set the value of [complement] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setComplement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->complement !== $v) {
            $this->complement = $v;
            $this->modifiedColumns[] = CommonServicePeer::COMPLEMENT;
        }


        return $this;
    } // setComplement()

    /**
     * Set the value of [libelle_ar] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setLibelleAr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_ar !== $v) {
            $this->libelle_ar = $v;
            $this->modifiedColumns[] = CommonServicePeer::LIBELLE_AR;
        }


        return $this;
    } // setLibelleAr()

    /**
     * Set the value of [adresse_ar] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseAr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_ar !== $v) {
            $this->adresse_ar = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_AR;
        }


        return $this;
    } // setAdresseAr()

    /**
     * Set the value of [adresse_suite_ar] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseSuiteAr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_suite_ar !== $v) {
            $this->adresse_suite_ar = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_SUITE_AR;
        }


        return $this;
    } // setAdresseSuiteAr()

    /**
     * Set the value of [ville_ar] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setVilleAr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_ar !== $v) {
            $this->ville_ar = $v;
            $this->modifiedColumns[] = CommonServicePeer::VILLE_AR;
        }


        return $this;
    } // setVilleAr()

    /**
     * Set the value of [pays_ar] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setPaysAr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_ar !== $v) {
            $this->pays_ar = $v;
            $this->modifiedColumns[] = CommonServicePeer::PAYS_AR;
        }


        return $this;
    } // setPaysAr()

    /**
     * Set the value of [libelle_fr] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setLibelleFr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_fr !== $v) {
            $this->libelle_fr = $v;
            $this->modifiedColumns[] = CommonServicePeer::LIBELLE_FR;
        }


        return $this;
    } // setLibelleFr()

    /**
     * Set the value of [adresse_fr] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseFr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_fr !== $v) {
            $this->adresse_fr = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_FR;
        }


        return $this;
    } // setAdresseFr()

    /**
     * Set the value of [adresse_suite_fr] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseSuiteFr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_suite_fr !== $v) {
            $this->adresse_suite_fr = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_SUITE_FR;
        }


        return $this;
    } // setAdresseSuiteFr()

    /**
     * Set the value of [ville_fr] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setVilleFr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_fr !== $v) {
            $this->ville_fr = $v;
            $this->modifiedColumns[] = CommonServicePeer::VILLE_FR;
        }


        return $this;
    } // setVilleFr()

    /**
     * Set the value of [pays_fr] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setPaysFr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_fr !== $v) {
            $this->pays_fr = $v;
            $this->modifiedColumns[] = CommonServicePeer::PAYS_FR;
        }


        return $this;
    } // setPaysFr()

    /**
     * Set the value of [libelle_es] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setLibelleEs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_es !== $v) {
            $this->libelle_es = $v;
            $this->modifiedColumns[] = CommonServicePeer::LIBELLE_ES;
        }


        return $this;
    } // setLibelleEs()

    /**
     * Set the value of [adresse_es] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseEs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_es !== $v) {
            $this->adresse_es = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_ES;
        }


        return $this;
    } // setAdresseEs()

    /**
     * Set the value of [adresse_suite_es] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseSuiteEs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_suite_es !== $v) {
            $this->adresse_suite_es = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_SUITE_ES;
        }


        return $this;
    } // setAdresseSuiteEs()

    /**
     * Set the value of [ville_es] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setVilleEs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_es !== $v) {
            $this->ville_es = $v;
            $this->modifiedColumns[] = CommonServicePeer::VILLE_ES;
        }


        return $this;
    } // setVilleEs()

    /**
     * Set the value of [pays_es] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setPaysEs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_es !== $v) {
            $this->pays_es = $v;
            $this->modifiedColumns[] = CommonServicePeer::PAYS_ES;
        }


        return $this;
    } // setPaysEs()

    /**
     * Set the value of [libelle_en] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setLibelleEn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_en !== $v) {
            $this->libelle_en = $v;
            $this->modifiedColumns[] = CommonServicePeer::LIBELLE_EN;
        }


        return $this;
    } // setLibelleEn()

    /**
     * Set the value of [adresse_en] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseEn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_en !== $v) {
            $this->adresse_en = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_EN;
        }


        return $this;
    } // setAdresseEn()

    /**
     * Set the value of [adresse_suite_en] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseSuiteEn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_suite_en !== $v) {
            $this->adresse_suite_en = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_SUITE_EN;
        }


        return $this;
    } // setAdresseSuiteEn()

    /**
     * Set the value of [ville_en] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setVilleEn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_en !== $v) {
            $this->ville_en = $v;
            $this->modifiedColumns[] = CommonServicePeer::VILLE_EN;
        }


        return $this;
    } // setVilleEn()

    /**
     * Set the value of [pays_en] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setPaysEn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_en !== $v) {
            $this->pays_en = $v;
            $this->modifiedColumns[] = CommonServicePeer::PAYS_EN;
        }


        return $this;
    } // setPaysEn()

    /**
     * Set the value of [libelle_su] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setLibelleSu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_su !== $v) {
            $this->libelle_su = $v;
            $this->modifiedColumns[] = CommonServicePeer::LIBELLE_SU;
        }


        return $this;
    } // setLibelleSu()

    /**
     * Set the value of [adresse_su] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseSu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_su !== $v) {
            $this->adresse_su = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_SU;
        }


        return $this;
    } // setAdresseSu()

    /**
     * Set the value of [adresse_suite_su] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseSuiteSu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_suite_su !== $v) {
            $this->adresse_suite_su = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_SUITE_SU;
        }


        return $this;
    } // setAdresseSuiteSu()

    /**
     * Set the value of [ville_su] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setVilleSu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_su !== $v) {
            $this->ville_su = $v;
            $this->modifiedColumns[] = CommonServicePeer::VILLE_SU;
        }


        return $this;
    } // setVilleSu()

    /**
     * Set the value of [pays_su] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setPaysSu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_su !== $v) {
            $this->pays_su = $v;
            $this->modifiedColumns[] = CommonServicePeer::PAYS_SU;
        }


        return $this;
    } // setPaysSu()

    /**
     * Set the value of [libelle_du] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setLibelleDu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_du !== $v) {
            $this->libelle_du = $v;
            $this->modifiedColumns[] = CommonServicePeer::LIBELLE_DU;
        }


        return $this;
    } // setLibelleDu()

    /**
     * Set the value of [adresse_du] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseDu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_du !== $v) {
            $this->adresse_du = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_DU;
        }


        return $this;
    } // setAdresseDu()

    /**
     * Set the value of [adresse_suite_du] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseSuiteDu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_suite_du !== $v) {
            $this->adresse_suite_du = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_SUITE_DU;
        }


        return $this;
    } // setAdresseSuiteDu()

    /**
     * Set the value of [ville_du] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setVilleDu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_du !== $v) {
            $this->ville_du = $v;
            $this->modifiedColumns[] = CommonServicePeer::VILLE_DU;
        }


        return $this;
    } // setVilleDu()

    /**
     * Set the value of [pays_du] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setPaysDu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_du !== $v) {
            $this->pays_du = $v;
            $this->modifiedColumns[] = CommonServicePeer::PAYS_DU;
        }


        return $this;
    } // setPaysDu()

    /**
     * Set the value of [libelle_cz] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setLibelleCz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_cz !== $v) {
            $this->libelle_cz = $v;
            $this->modifiedColumns[] = CommonServicePeer::LIBELLE_CZ;
        }


        return $this;
    } // setLibelleCz()

    /**
     * Set the value of [adresse_cz] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseCz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_cz !== $v) {
            $this->adresse_cz = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_CZ;
        }


        return $this;
    } // setAdresseCz()

    /**
     * Set the value of [adresse_suite_cz] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseSuiteCz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_suite_cz !== $v) {
            $this->adresse_suite_cz = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_SUITE_CZ;
        }


        return $this;
    } // setAdresseSuiteCz()

    /**
     * Set the value of [ville_cz] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setVilleCz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_cz !== $v) {
            $this->ville_cz = $v;
            $this->modifiedColumns[] = CommonServicePeer::VILLE_CZ;
        }


        return $this;
    } // setVilleCz()

    /**
     * Set the value of [pays_cz] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setPaysCz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_cz !== $v) {
            $this->pays_cz = $v;
            $this->modifiedColumns[] = CommonServicePeer::PAYS_CZ;
        }


        return $this;
    } // setPaysCz()

    /**
     * Set the value of [libelle_it] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setLibelleIt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_it !== $v) {
            $this->libelle_it = $v;
            $this->modifiedColumns[] = CommonServicePeer::LIBELLE_IT;
        }


        return $this;
    } // setLibelleIt()

    /**
     * Set the value of [adresse_it] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseIt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_it !== $v) {
            $this->adresse_it = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_IT;
        }


        return $this;
    } // setAdresseIt()

    /**
     * Set the value of [adresse_suite_it] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAdresseSuiteIt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adresse_suite_it !== $v) {
            $this->adresse_suite_it = $v;
            $this->modifiedColumns[] = CommonServicePeer::ADRESSE_SUITE_IT;
        }


        return $this;
    } // setAdresseSuiteIt()

    /**
     * Set the value of [ville_it] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setVilleIt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->ville_it !== $v) {
            $this->ville_it = $v;
            $this->modifiedColumns[] = CommonServicePeer::VILLE_IT;
        }


        return $this;
    } // setVilleIt()

    /**
     * Set the value of [pays_it] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setPaysIt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pays_it !== $v) {
            $this->pays_it = $v;
            $this->modifiedColumns[] = CommonServicePeer::PAYS_IT;
        }


        return $this;
    } // setPaysIt()

    /**
     * Set the value of [chemin_complet] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setCheminComplet($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_complet !== $v) {
            $this->chemin_complet = $v;
            $this->modifiedColumns[] = CommonServicePeer::CHEMIN_COMPLET;
        }


        return $this;
    } // setCheminComplet()

    /**
     * Set the value of [chemin_complet_fr] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setCheminCompletFr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_complet_fr !== $v) {
            $this->chemin_complet_fr = $v;
            $this->modifiedColumns[] = CommonServicePeer::CHEMIN_COMPLET_FR;
        }


        return $this;
    } // setCheminCompletFr()

    /**
     * Set the value of [chemin_complet_en] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setCheminCompletEn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_complet_en !== $v) {
            $this->chemin_complet_en = $v;
            $this->modifiedColumns[] = CommonServicePeer::CHEMIN_COMPLET_EN;
        }


        return $this;
    } // setCheminCompletEn()

    /**
     * Set the value of [chemin_complet_es] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setCheminCompletEs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_complet_es !== $v) {
            $this->chemin_complet_es = $v;
            $this->modifiedColumns[] = CommonServicePeer::CHEMIN_COMPLET_ES;
        }


        return $this;
    } // setCheminCompletEs()

    /**
     * Set the value of [chemin_complet_su] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setCheminCompletSu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_complet_su !== $v) {
            $this->chemin_complet_su = $v;
            $this->modifiedColumns[] = CommonServicePeer::CHEMIN_COMPLET_SU;
        }


        return $this;
    } // setCheminCompletSu()

    /**
     * Set the value of [chemin_complet_du] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setCheminCompletDu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_complet_du !== $v) {
            $this->chemin_complet_du = $v;
            $this->modifiedColumns[] = CommonServicePeer::CHEMIN_COMPLET_DU;
        }


        return $this;
    } // setCheminCompletDu()

    /**
     * Set the value of [chemin_complet_cz] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setCheminCompletCz($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_complet_cz !== $v) {
            $this->chemin_complet_cz = $v;
            $this->modifiedColumns[] = CommonServicePeer::CHEMIN_COMPLET_CZ;
        }


        return $this;
    } // setCheminCompletCz()

    /**
     * Set the value of [chemin_complet_ar] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setCheminCompletAr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_complet_ar !== $v) {
            $this->chemin_complet_ar = $v;
            $this->modifiedColumns[] = CommonServicePeer::CHEMIN_COMPLET_AR;
        }


        return $this;
    } // setCheminCompletAr()

    /**
     * Set the value of [chemin_complet_it] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setCheminCompletIt($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_complet_it !== $v) {
            $this->chemin_complet_it = $v;
            $this->modifiedColumns[] = CommonServicePeer::CHEMIN_COMPLET_IT;
        }


        return $this;
    } // setCheminCompletIt()

    /**
     * Set the value of [nom_service_archiveur] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setNomServiceArchiveur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_service_archiveur !== $v) {
            $this->nom_service_archiveur = $v;
            $this->modifiedColumns[] = CommonServicePeer::NOM_SERVICE_ARCHIVEUR;
        }


        return $this;
    } // setNomServiceArchiveur()

    /**
     * Set the value of [identifiant_service_archiveur] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setIdentifiantServiceArchiveur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->identifiant_service_archiveur !== $v) {
            $this->identifiant_service_archiveur = $v;
            $this->modifiedColumns[] = CommonServicePeer::IDENTIFIANT_SERVICE_ARCHIVEUR;
        }


        return $this;
    } // setIdentifiantServiceArchiveur()

    /**
     * Set the value of [affichage_service] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAffichageService($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->affichage_service !== $v) {
            $this->affichage_service = $v;
            $this->modifiedColumns[] = CommonServicePeer::AFFICHAGE_SERVICE;
        }


        return $this;
    } // setAffichageService()

    /**
     * Set the value of [activation_fuseau_horaire] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setActivationFuseauHoraire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->activation_fuseau_horaire !== $v) {
            $this->activation_fuseau_horaire = $v;
            $this->modifiedColumns[] = CommonServicePeer::ACTIVATION_FUSEAU_HORAIRE;
        }


        return $this;
    } // setActivationFuseauHoraire()

    /**
     * Set the value of [decalage_horaire] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setDecalageHoraire($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->decalage_horaire !== $v) {
            $this->decalage_horaire = $v;
            $this->modifiedColumns[] = CommonServicePeer::DECALAGE_HORAIRE;
        }


        return $this;
    } // setDecalageHoraire()

    /**
     * Set the value of [lieu_residence] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setLieuResidence($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->lieu_residence !== $v) {
            $this->lieu_residence = $v;
            $this->modifiedColumns[] = CommonServicePeer::LIEU_RESIDENCE;
        }


        return $this;
    } // setLieuResidence()

    /**
     * Set the value of [alerte] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAlerte($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte !== $v) {
            $this->alerte = $v;
            $this->modifiedColumns[] = CommonServicePeer::ALERTE;
        }


        return $this;
    } // setAlerte()

    /**
     * Set the value of [acces_chorus] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setAccesChorus($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->acces_chorus !== $v) {
            $this->acces_chorus = $v;
            $this->modifiedColumns[] = CommonServicePeer::ACCES_CHORUS;
        }


        return $this;
    } // setAccesChorus()

    /**
     * Set the value of [forme_juridique] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setFormeJuridique($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->forme_juridique !== $v) {
            $this->forme_juridique = $v;
            $this->modifiedColumns[] = CommonServicePeer::FORME_JURIDIQUE;
        }


        return $this;
    } // setFormeJuridique()

    /**
     * Set the value of [forme_juridique_code] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setFormeJuridiqueCode($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->forme_juridique_code !== $v) {
            $this->forme_juridique_code = $v;
            $this->modifiedColumns[] = CommonServicePeer::FORME_JURIDIQUE_CODE;
        }


        return $this;
    } // setFormeJuridiqueCode()

    /**
     * Set the value of [synchronisation_exec] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setSynchronisationExec($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->synchronisation_exec !== $v) {
            $this->synchronisation_exec = $v;
            $this->modifiedColumns[] = CommonServicePeer::SYNCHRONISATION_EXEC;
        }


        return $this;
    } // setSynchronisationExec()

    /**
     * Set the value of [id_entite] column.
     *
     * @param int $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setIdEntite($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_entite !== $v) {
            $this->id_entite = $v;
            $this->modifiedColumns[] = CommonServicePeer::ID_ENTITE;
        }


        return $this;
    } // setIdEntite()

    /**
     * Set the value of [id] column.
     *
     * @param string $v new value
     * @return CommonService The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonServicePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->organisme !== '') {
                return false;
            }

            if ($this->type_service !== '2') {
                return false;
            }

            if ($this->adresse !== '') {
                return false;
            }

            if ($this->adresse_suite !== '') {
                return false;
            }

            if ($this->cp !== '') {
                return false;
            }

            if ($this->ville !== '') {
                return false;
            }

            if ($this->telephone !== '') {
                return false;
            }

            if ($this->fax !== '') {
                return false;
            }

            if ($this->mail !== '') {
                return false;
            }

            if ($this->id_externe !== '0') {
                return false;
            }

            if ($this->adresse_ar !== '') {
                return false;
            }

            if ($this->adresse_suite_ar !== '') {
                return false;
            }

            if ($this->ville_ar !== '') {
                return false;
            }

            if ($this->adresse_fr !== '') {
                return false;
            }

            if ($this->adresse_suite_fr !== '') {
                return false;
            }

            if ($this->ville_fr !== '') {
                return false;
            }

            if ($this->adresse_es !== '') {
                return false;
            }

            if ($this->adresse_suite_es !== '') {
                return false;
            }

            if ($this->ville_es !== '') {
                return false;
            }

            if ($this->adresse_en !== '') {
                return false;
            }

            if ($this->adresse_suite_en !== '') {
                return false;
            }

            if ($this->ville_en !== '') {
                return false;
            }

            if ($this->adresse_su !== '') {
                return false;
            }

            if ($this->adresse_suite_su !== '') {
                return false;
            }

            if ($this->ville_su !== '') {
                return false;
            }

            if ($this->adresse_du !== '') {
                return false;
            }

            if ($this->adresse_suite_du !== '') {
                return false;
            }

            if ($this->ville_du !== '') {
                return false;
            }

            if ($this->adresse_cz !== '') {
                return false;
            }

            if ($this->adresse_suite_cz !== '') {
                return false;
            }

            if ($this->ville_cz !== '') {
                return false;
            }

            if ($this->pays_cz !== '') {
                return false;
            }

            if ($this->adresse_it !== '') {
                return false;
            }

            if ($this->adresse_suite_it !== '') {
                return false;
            }

            if ($this->ville_it !== '') {
                return false;
            }

            if ($this->chemin_complet !== '') {
                return false;
            }

            if ($this->chemin_complet_fr !== '') {
                return false;
            }

            if ($this->chemin_complet_en !== '') {
                return false;
            }

            if ($this->chemin_complet_es !== '') {
                return false;
            }

            if ($this->chemin_complet_su !== '') {
                return false;
            }

            if ($this->chemin_complet_du !== '') {
                return false;
            }

            if ($this->chemin_complet_cz !== '') {
                return false;
            }

            if ($this->chemin_complet_ar !== '') {
                return false;
            }

            if ($this->chemin_complet_it !== '') {
                return false;
            }

            if ($this->affichage_service !== '1') {
                return false;
            }

            if ($this->activation_fuseau_horaire !== '0') {
                return false;
            }

            if ($this->alerte !== '0') {
                return false;
            }

            if ($this->acces_chorus !== '0') {
                return false;
            }

            if ($this->synchronisation_exec !== '1') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->old_id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->organisme = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->type_service = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->libelle = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->sigle = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->adresse = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->adresse_suite = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->cp = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->ville = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->telephone = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->fax = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->mail = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->pays = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->id_externe = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->date_creation = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->date_modification = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->siren = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->complement = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->libelle_ar = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->adresse_ar = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->adresse_suite_ar = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->ville_ar = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->pays_ar = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->libelle_fr = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->adresse_fr = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->adresse_suite_fr = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->ville_fr = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->pays_fr = ($row[$startcol + 27] !== null) ? (string) $row[$startcol + 27] : null;
            $this->libelle_es = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->adresse_es = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->adresse_suite_es = ($row[$startcol + 30] !== null) ? (string) $row[$startcol + 30] : null;
            $this->ville_es = ($row[$startcol + 31] !== null) ? (string) $row[$startcol + 31] : null;
            $this->pays_es = ($row[$startcol + 32] !== null) ? (string) $row[$startcol + 32] : null;
            $this->libelle_en = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->adresse_en = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->adresse_suite_en = ($row[$startcol + 35] !== null) ? (string) $row[$startcol + 35] : null;
            $this->ville_en = ($row[$startcol + 36] !== null) ? (string) $row[$startcol + 36] : null;
            $this->pays_en = ($row[$startcol + 37] !== null) ? (string) $row[$startcol + 37] : null;
            $this->libelle_su = ($row[$startcol + 38] !== null) ? (string) $row[$startcol + 38] : null;
            $this->adresse_su = ($row[$startcol + 39] !== null) ? (string) $row[$startcol + 39] : null;
            $this->adresse_suite_su = ($row[$startcol + 40] !== null) ? (string) $row[$startcol + 40] : null;
            $this->ville_su = ($row[$startcol + 41] !== null) ? (string) $row[$startcol + 41] : null;
            $this->pays_su = ($row[$startcol + 42] !== null) ? (string) $row[$startcol + 42] : null;
            $this->libelle_du = ($row[$startcol + 43] !== null) ? (string) $row[$startcol + 43] : null;
            $this->adresse_du = ($row[$startcol + 44] !== null) ? (string) $row[$startcol + 44] : null;
            $this->adresse_suite_du = ($row[$startcol + 45] !== null) ? (string) $row[$startcol + 45] : null;
            $this->ville_du = ($row[$startcol + 46] !== null) ? (string) $row[$startcol + 46] : null;
            $this->pays_du = ($row[$startcol + 47] !== null) ? (string) $row[$startcol + 47] : null;
            $this->libelle_cz = ($row[$startcol + 48] !== null) ? (string) $row[$startcol + 48] : null;
            $this->adresse_cz = ($row[$startcol + 49] !== null) ? (string) $row[$startcol + 49] : null;
            $this->adresse_suite_cz = ($row[$startcol + 50] !== null) ? (string) $row[$startcol + 50] : null;
            $this->ville_cz = ($row[$startcol + 51] !== null) ? (string) $row[$startcol + 51] : null;
            $this->pays_cz = ($row[$startcol + 52] !== null) ? (string) $row[$startcol + 52] : null;
            $this->libelle_it = ($row[$startcol + 53] !== null) ? (string) $row[$startcol + 53] : null;
            $this->adresse_it = ($row[$startcol + 54] !== null) ? (string) $row[$startcol + 54] : null;
            $this->adresse_suite_it = ($row[$startcol + 55] !== null) ? (string) $row[$startcol + 55] : null;
            $this->ville_it = ($row[$startcol + 56] !== null) ? (string) $row[$startcol + 56] : null;
            $this->pays_it = ($row[$startcol + 57] !== null) ? (string) $row[$startcol + 57] : null;
            $this->chemin_complet = ($row[$startcol + 58] !== null) ? (string) $row[$startcol + 58] : null;
            $this->chemin_complet_fr = ($row[$startcol + 59] !== null) ? (string) $row[$startcol + 59] : null;
            $this->chemin_complet_en = ($row[$startcol + 60] !== null) ? (string) $row[$startcol + 60] : null;
            $this->chemin_complet_es = ($row[$startcol + 61] !== null) ? (string) $row[$startcol + 61] : null;
            $this->chemin_complet_su = ($row[$startcol + 62] !== null) ? (string) $row[$startcol + 62] : null;
            $this->chemin_complet_du = ($row[$startcol + 63] !== null) ? (string) $row[$startcol + 63] : null;
            $this->chemin_complet_cz = ($row[$startcol + 64] !== null) ? (string) $row[$startcol + 64] : null;
            $this->chemin_complet_ar = ($row[$startcol + 65] !== null) ? (string) $row[$startcol + 65] : null;
            $this->chemin_complet_it = ($row[$startcol + 66] !== null) ? (string) $row[$startcol + 66] : null;
            $this->nom_service_archiveur = ($row[$startcol + 67] !== null) ? (string) $row[$startcol + 67] : null;
            $this->identifiant_service_archiveur = ($row[$startcol + 68] !== null) ? (string) $row[$startcol + 68] : null;
            $this->affichage_service = ($row[$startcol + 69] !== null) ? (string) $row[$startcol + 69] : null;
            $this->activation_fuseau_horaire = ($row[$startcol + 70] !== null) ? (string) $row[$startcol + 70] : null;
            $this->decalage_horaire = ($row[$startcol + 71] !== null) ? (string) $row[$startcol + 71] : null;
            $this->lieu_residence = ($row[$startcol + 72] !== null) ? (string) $row[$startcol + 72] : null;
            $this->alerte = ($row[$startcol + 73] !== null) ? (string) $row[$startcol + 73] : null;
            $this->acces_chorus = ($row[$startcol + 74] !== null) ? (string) $row[$startcol + 74] : null;
            $this->forme_juridique = ($row[$startcol + 75] !== null) ? (string) $row[$startcol + 75] : null;
            $this->forme_juridique_code = ($row[$startcol + 76] !== null) ? (string) $row[$startcol + 76] : null;
            $this->synchronisation_exec = ($row[$startcol + 77] !== null) ? (string) $row[$startcol + 77] : null;
            $this->id_entite = ($row[$startcol + 78] !== null) ? (int) $row[$startcol + 78] : null;
            $this->id = ($row[$startcol + 79] !== null) ? (string) $row[$startcol + 79] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 80; // 80 = CommonServicePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonService object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonServicePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonServicePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCommonAffiliationServicesRelatedByServiceId = null;

            $this->collCommonAffiliationServicesRelatedByServiceParentId = null;

            $this->collCommonAgents = null;

            $this->collCommonEncherePmis = null;

            $this->collCommonNewslettersRelatedByIdServiceDestinataire = null;

            $this->collCommonNewslettersRelatedByIdServiceRedacteur = null;

            $this->collCommonParametrageEncheres = null;

            $this->collCommonTTelechargementAsynchrones = null;

            $this->collCommonTypeProcedureOrganismes = null;

            $this->collCommonAgentTechniqueAssociations = null;

            $this->collCommonConsultationsRelatedByServiceValidation = null;

            $this->collCommonConsultationsRelatedByServiceId = null;

            $this->collCommonConsultationsRelatedByServiceValidationIntermediaire = null;

            $this->collCommonInvitePermanentTransverses = null;

            $this->collCommonTFusionnerServicessRelatedByIdServiceCible = null;

            $this->collCommonTFusionnerServicessRelatedByIdServiceSource = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonServicePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonServiceQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonServicePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonServicePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonAffiliationServicesRelatedByServiceIdScheduledForDeletion !== null) {
                if (!$this->commonAffiliationServicesRelatedByServiceIdScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonAffiliationServiceQuery::create()
                        ->filterByPrimaryKeys($this->commonAffiliationServicesRelatedByServiceIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonAffiliationServicesRelatedByServiceIdScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAffiliationServicesRelatedByServiceId !== null) {
                foreach ($this->collCommonAffiliationServicesRelatedByServiceId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonAffiliationServicesRelatedByServiceParentIdScheduledForDeletion !== null) {
                if (!$this->commonAffiliationServicesRelatedByServiceParentIdScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonAffiliationServiceQuery::create()
                        ->filterByPrimaryKeys($this->commonAffiliationServicesRelatedByServiceParentIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonAffiliationServicesRelatedByServiceParentIdScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAffiliationServicesRelatedByServiceParentId !== null) {
                foreach ($this->collCommonAffiliationServicesRelatedByServiceParentId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonAgentsScheduledForDeletion !== null) {
                if (!$this->commonAgentsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonAgentsScheduledForDeletion as $commonAgent) {
                        // need to save related object because we set the relation to null
                        $commonAgent->save($con);
                    }
                    $this->commonAgentsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAgents !== null) {
                foreach ($this->collCommonAgents as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonEncherePmisScheduledForDeletion !== null) {
                if (!$this->commonEncherePmisScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEncherePmisScheduledForDeletion as $commonEncherePmi) {
                        // need to save related object because we set the relation to null
                        $commonEncherePmi->save($con);
                    }
                    $this->commonEncherePmisScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEncherePmis !== null) {
                foreach ($this->collCommonEncherePmis as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonNewslettersRelatedByIdServiceDestinataireScheduledForDeletion !== null) {
                if (!$this->commonNewslettersRelatedByIdServiceDestinataireScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonNewsletterQuery::create()
                        ->filterByPrimaryKeys($this->commonNewslettersRelatedByIdServiceDestinataireScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonNewslettersRelatedByIdServiceDestinataireScheduledForDeletion = null;
                }
            }

            if ($this->collCommonNewslettersRelatedByIdServiceDestinataire !== null) {
                foreach ($this->collCommonNewslettersRelatedByIdServiceDestinataire as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonNewslettersRelatedByIdServiceRedacteurScheduledForDeletion !== null) {
                if (!$this->commonNewslettersRelatedByIdServiceRedacteurScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonNewsletterQuery::create()
                        ->filterByPrimaryKeys($this->commonNewslettersRelatedByIdServiceRedacteurScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonNewslettersRelatedByIdServiceRedacteurScheduledForDeletion = null;
                }
            }

            if ($this->collCommonNewslettersRelatedByIdServiceRedacteur !== null) {
                foreach ($this->collCommonNewslettersRelatedByIdServiceRedacteur as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonParametrageEncheresScheduledForDeletion !== null) {
                if (!$this->commonParametrageEncheresScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonParametrageEnchereQuery::create()
                        ->filterByPrimaryKeys($this->commonParametrageEncheresScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonParametrageEncheresScheduledForDeletion = null;
                }
            }

            if ($this->collCommonParametrageEncheres !== null) {
                foreach ($this->collCommonParametrageEncheres as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTTelechargementAsynchronesScheduledForDeletion !== null) {
                if (!$this->commonTTelechargementAsynchronesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTTelechargementAsynchroneQuery::create()
                        ->filterByPrimaryKeys($this->commonTTelechargementAsynchronesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTTelechargementAsynchronesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTTelechargementAsynchrones !== null) {
                foreach ($this->collCommonTTelechargementAsynchrones as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTypeProcedureOrganismesScheduledForDeletion !== null) {
                if (!$this->commonTypeProcedureOrganismesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTypeProcedureOrganismeQuery::create()
                        ->filterByPrimaryKeys($this->commonTypeProcedureOrganismesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTypeProcedureOrganismesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTypeProcedureOrganismes !== null) {
                foreach ($this->collCommonTypeProcedureOrganismes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonAgentTechniqueAssociationsScheduledForDeletion !== null) {
                if (!$this->commonAgentTechniqueAssociationsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonAgentTechniqueAssociationQuery::create()
                        ->filterByPrimaryKeys($this->commonAgentTechniqueAssociationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonAgentTechniqueAssociationsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAgentTechniqueAssociations !== null) {
                foreach ($this->collCommonAgentTechniqueAssociations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonConsultationsRelatedByServiceValidationScheduledForDeletion !== null) {
                if (!$this->commonConsultationsRelatedByServiceValidationScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationsRelatedByServiceValidationScheduledForDeletion as $commonConsultationRelatedByServiceValidation) {
                        // need to save related object because we set the relation to null
                        $commonConsultationRelatedByServiceValidation->save($con);
                    }
                    $this->commonConsultationsRelatedByServiceValidationScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultationsRelatedByServiceValidation !== null) {
                foreach ($this->collCommonConsultationsRelatedByServiceValidation as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonConsultationsRelatedByServiceIdScheduledForDeletion !== null) {
                if (!$this->commonConsultationsRelatedByServiceIdScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonConsultationQuery::create()
                        ->filterByPrimaryKeys($this->commonConsultationsRelatedByServiceIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonConsultationsRelatedByServiceIdScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultationsRelatedByServiceId !== null) {
                foreach ($this->collCommonConsultationsRelatedByServiceId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonConsultationsRelatedByServiceValidationIntermediaireScheduledForDeletion !== null) {
                if (!$this->commonConsultationsRelatedByServiceValidationIntermediaireScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationsRelatedByServiceValidationIntermediaireScheduledForDeletion as $commonConsultationRelatedByServiceValidationIntermediaire) {
                        // need to save related object because we set the relation to null
                        $commonConsultationRelatedByServiceValidationIntermediaire->save($con);
                    }
                    $this->commonConsultationsRelatedByServiceValidationIntermediaireScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultationsRelatedByServiceValidationIntermediaire !== null) {
                foreach ($this->collCommonConsultationsRelatedByServiceValidationIntermediaire as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonInvitePermanentTransversesScheduledForDeletion !== null) {
                if (!$this->commonInvitePermanentTransversesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonInvitePermanentTransverseQuery::create()
                        ->filterByPrimaryKeys($this->commonInvitePermanentTransversesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonInvitePermanentTransversesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonInvitePermanentTransverses !== null) {
                foreach ($this->collCommonInvitePermanentTransverses as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTFusionnerServicessRelatedByIdServiceCibleScheduledForDeletion !== null) {
                if (!$this->commonTFusionnerServicessRelatedByIdServiceCibleScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTFusionnerServicesQuery::create()
                        ->filterByPrimaryKeys($this->commonTFusionnerServicessRelatedByIdServiceCibleScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTFusionnerServicessRelatedByIdServiceCibleScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTFusionnerServicessRelatedByIdServiceCible !== null) {
                foreach ($this->collCommonTFusionnerServicessRelatedByIdServiceCible as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTFusionnerServicessRelatedByIdServiceSourceScheduledForDeletion !== null) {
                if (!$this->commonTFusionnerServicessRelatedByIdServiceSourceScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonTFusionnerServicesQuery::create()
                        ->filterByPrimaryKeys($this->commonTFusionnerServicessRelatedByIdServiceSourceScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonTFusionnerServicessRelatedByIdServiceSourceScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTFusionnerServicessRelatedByIdServiceSource !== null) {
                foreach ($this->collCommonTFusionnerServicessRelatedByIdServiceSource as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonServicePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonServicePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonServicePeer::OLD_ID)) {
            $modifiedColumns[':p' . $index++]  = '`old_id`';
        }
        if ($this->isColumnModified(CommonServicePeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonServicePeer::TYPE_SERVICE)) {
            $modifiedColumns[':p' . $index++]  = '`type_service`';
        }
        if ($this->isColumnModified(CommonServicePeer::LIBELLE)) {
            $modifiedColumns[':p' . $index++]  = '`libelle`';
        }
        if ($this->isColumnModified(CommonServicePeer::SIGLE)) {
            $modifiedColumns[':p' . $index++]  = '`sigle`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = '`adresse`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_suite`';
        }
        if ($this->isColumnModified(CommonServicePeer::CP)) {
            $modifiedColumns[':p' . $index++]  = '`cp`';
        }
        if ($this->isColumnModified(CommonServicePeer::VILLE)) {
            $modifiedColumns[':p' . $index++]  = '`ville`';
        }
        if ($this->isColumnModified(CommonServicePeer::TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`telephone`';
        }
        if ($this->isColumnModified(CommonServicePeer::FAX)) {
            $modifiedColumns[':p' . $index++]  = '`fax`';
        }
        if ($this->isColumnModified(CommonServicePeer::MAIL)) {
            $modifiedColumns[':p' . $index++]  = '`mail`';
        }
        if ($this->isColumnModified(CommonServicePeer::PAYS)) {
            $modifiedColumns[':p' . $index++]  = '`pays`';
        }
        if ($this->isColumnModified(CommonServicePeer::ID_EXTERNE)) {
            $modifiedColumns[':p' . $index++]  = '`id_externe`';
        }
        if ($this->isColumnModified(CommonServicePeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonServicePeer::DATE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification`';
        }
        if ($this->isColumnModified(CommonServicePeer::SIREN)) {
            $modifiedColumns[':p' . $index++]  = '`siren`';
        }
        if ($this->isColumnModified(CommonServicePeer::COMPLEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`complement`';
        }
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_AR)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_ar`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_AR)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_ar`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_AR)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_suite_ar`';
        }
        if ($this->isColumnModified(CommonServicePeer::VILLE_AR)) {
            $modifiedColumns[':p' . $index++]  = '`ville_ar`';
        }
        if ($this->isColumnModified(CommonServicePeer::PAYS_AR)) {
            $modifiedColumns[':p' . $index++]  = '`pays_ar`';
        }
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_FR)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_fr`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_FR)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_fr`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_FR)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_suite_fr`';
        }
        if ($this->isColumnModified(CommonServicePeer::VILLE_FR)) {
            $modifiedColumns[':p' . $index++]  = '`ville_fr`';
        }
        if ($this->isColumnModified(CommonServicePeer::PAYS_FR)) {
            $modifiedColumns[':p' . $index++]  = '`pays_fr`';
        }
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_ES)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_es`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_ES)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_es`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_ES)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_suite_es`';
        }
        if ($this->isColumnModified(CommonServicePeer::VILLE_ES)) {
            $modifiedColumns[':p' . $index++]  = '`ville_es`';
        }
        if ($this->isColumnModified(CommonServicePeer::PAYS_ES)) {
            $modifiedColumns[':p' . $index++]  = '`pays_es`';
        }
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_EN)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_en`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_EN)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_en`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_EN)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_suite_en`';
        }
        if ($this->isColumnModified(CommonServicePeer::VILLE_EN)) {
            $modifiedColumns[':p' . $index++]  = '`ville_en`';
        }
        if ($this->isColumnModified(CommonServicePeer::PAYS_EN)) {
            $modifiedColumns[':p' . $index++]  = '`pays_en`';
        }
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_SU)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_su`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SU)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_su`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_SU)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_suite_su`';
        }
        if ($this->isColumnModified(CommonServicePeer::VILLE_SU)) {
            $modifiedColumns[':p' . $index++]  = '`ville_su`';
        }
        if ($this->isColumnModified(CommonServicePeer::PAYS_SU)) {
            $modifiedColumns[':p' . $index++]  = '`pays_su`';
        }
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_DU)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_du`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_DU)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_du`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_DU)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_suite_du`';
        }
        if ($this->isColumnModified(CommonServicePeer::VILLE_DU)) {
            $modifiedColumns[':p' . $index++]  = '`ville_du`';
        }
        if ($this->isColumnModified(CommonServicePeer::PAYS_DU)) {
            $modifiedColumns[':p' . $index++]  = '`pays_du`';
        }
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_cz`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_cz`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_suite_cz`';
        }
        if ($this->isColumnModified(CommonServicePeer::VILLE_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`ville_cz`';
        }
        if ($this->isColumnModified(CommonServicePeer::PAYS_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`pays_cz`';
        }
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_IT)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_it`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_IT)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_it`';
        }
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_IT)) {
            $modifiedColumns[':p' . $index++]  = '`adresse_suite_it`';
        }
        if ($this->isColumnModified(CommonServicePeer::VILLE_IT)) {
            $modifiedColumns[':p' . $index++]  = '`ville_it`';
        }
        if ($this->isColumnModified(CommonServicePeer::PAYS_IT)) {
            $modifiedColumns[':p' . $index++]  = '`pays_it`';
        }
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_complet`';
        }
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_FR)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_complet_fr`';
        }
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_EN)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_complet_en`';
        }
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_ES)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_complet_es`';
        }
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_SU)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_complet_su`';
        }
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_DU)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_complet_du`';
        }
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_CZ)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_complet_cz`';
        }
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_AR)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_complet_ar`';
        }
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_IT)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_complet_it`';
        }
        if ($this->isColumnModified(CommonServicePeer::NOM_SERVICE_ARCHIVEUR)) {
            $modifiedColumns[':p' . $index++]  = '`nom_service_archiveur`';
        }
        if ($this->isColumnModified(CommonServicePeer::IDENTIFIANT_SERVICE_ARCHIVEUR)) {
            $modifiedColumns[':p' . $index++]  = '`identifiant_service_archiveur`';
        }
        if ($this->isColumnModified(CommonServicePeer::AFFICHAGE_SERVICE)) {
            $modifiedColumns[':p' . $index++]  = '`affichage_service`';
        }
        if ($this->isColumnModified(CommonServicePeer::ACTIVATION_FUSEAU_HORAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`activation_fuseau_horaire`';
        }
        if ($this->isColumnModified(CommonServicePeer::DECALAGE_HORAIRE)) {
            $modifiedColumns[':p' . $index++]  = '`decalage_horaire`';
        }
        if ($this->isColumnModified(CommonServicePeer::LIEU_RESIDENCE)) {
            $modifiedColumns[':p' . $index++]  = '`lieu_residence`';
        }
        if ($this->isColumnModified(CommonServicePeer::ALERTE)) {
            $modifiedColumns[':p' . $index++]  = '`alerte`';
        }
        if ($this->isColumnModified(CommonServicePeer::ACCES_CHORUS)) {
            $modifiedColumns[':p' . $index++]  = '`acces_chorus`';
        }
        if ($this->isColumnModified(CommonServicePeer::FORME_JURIDIQUE)) {
            $modifiedColumns[':p' . $index++]  = '`forme_juridique`';
        }
        if ($this->isColumnModified(CommonServicePeer::FORME_JURIDIQUE_CODE)) {
            $modifiedColumns[':p' . $index++]  = '`forme_juridique_code`';
        }
        if ($this->isColumnModified(CommonServicePeer::SYNCHRONISATION_EXEC)) {
            $modifiedColumns[':p' . $index++]  = '`synchronisation_exec`';
        }
        if ($this->isColumnModified(CommonServicePeer::ID_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entite`';
        }
        if ($this->isColumnModified(CommonServicePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }

        $sql = sprintf(
            'INSERT INTO `Service` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`old_id`':
                        $stmt->bindValue($identifier, $this->old_id, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`type_service`':
                        $stmt->bindValue($identifier, $this->type_service, PDO::PARAM_STR);
                        break;
                    case '`libelle`':
                        $stmt->bindValue($identifier, $this->libelle, PDO::PARAM_STR);
                        break;
                    case '`sigle`':
                        $stmt->bindValue($identifier, $this->sigle, PDO::PARAM_STR);
                        break;
                    case '`adresse`':
                        $stmt->bindValue($identifier, $this->adresse, PDO::PARAM_STR);
                        break;
                    case '`adresse_suite`':
                        $stmt->bindValue($identifier, $this->adresse_suite, PDO::PARAM_STR);
                        break;
                    case '`cp`':
                        $stmt->bindValue($identifier, $this->cp, PDO::PARAM_STR);
                        break;
                    case '`ville`':
                        $stmt->bindValue($identifier, $this->ville, PDO::PARAM_STR);
                        break;
                    case '`telephone`':
                        $stmt->bindValue($identifier, $this->telephone, PDO::PARAM_STR);
                        break;
                    case '`fax`':
                        $stmt->bindValue($identifier, $this->fax, PDO::PARAM_STR);
                        break;
                    case '`mail`':
                        $stmt->bindValue($identifier, $this->mail, PDO::PARAM_STR);
                        break;
                    case '`pays`':
                        $stmt->bindValue($identifier, $this->pays, PDO::PARAM_STR);
                        break;
                    case '`id_externe`':
                        $stmt->bindValue($identifier, $this->id_externe, PDO::PARAM_STR);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_modification`':
                        $stmt->bindValue($identifier, $this->date_modification, PDO::PARAM_STR);
                        break;
                    case '`siren`':
                        $stmt->bindValue($identifier, $this->siren, PDO::PARAM_STR);
                        break;
                    case '`complement`':
                        $stmt->bindValue($identifier, $this->complement, PDO::PARAM_STR);
                        break;
                    case '`libelle_ar`':
                        $stmt->bindValue($identifier, $this->libelle_ar, PDO::PARAM_STR);
                        break;
                    case '`adresse_ar`':
                        $stmt->bindValue($identifier, $this->adresse_ar, PDO::PARAM_STR);
                        break;
                    case '`adresse_suite_ar`':
                        $stmt->bindValue($identifier, $this->adresse_suite_ar, PDO::PARAM_STR);
                        break;
                    case '`ville_ar`':
                        $stmt->bindValue($identifier, $this->ville_ar, PDO::PARAM_STR);
                        break;
                    case '`pays_ar`':
                        $stmt->bindValue($identifier, $this->pays_ar, PDO::PARAM_STR);
                        break;
                    case '`libelle_fr`':
                        $stmt->bindValue($identifier, $this->libelle_fr, PDO::PARAM_STR);
                        break;
                    case '`adresse_fr`':
                        $stmt->bindValue($identifier, $this->adresse_fr, PDO::PARAM_STR);
                        break;
                    case '`adresse_suite_fr`':
                        $stmt->bindValue($identifier, $this->adresse_suite_fr, PDO::PARAM_STR);
                        break;
                    case '`ville_fr`':
                        $stmt->bindValue($identifier, $this->ville_fr, PDO::PARAM_STR);
                        break;
                    case '`pays_fr`':
                        $stmt->bindValue($identifier, $this->pays_fr, PDO::PARAM_STR);
                        break;
                    case '`libelle_es`':
                        $stmt->bindValue($identifier, $this->libelle_es, PDO::PARAM_STR);
                        break;
                    case '`adresse_es`':
                        $stmt->bindValue($identifier, $this->adresse_es, PDO::PARAM_STR);
                        break;
                    case '`adresse_suite_es`':
                        $stmt->bindValue($identifier, $this->adresse_suite_es, PDO::PARAM_STR);
                        break;
                    case '`ville_es`':
                        $stmt->bindValue($identifier, $this->ville_es, PDO::PARAM_STR);
                        break;
                    case '`pays_es`':
                        $stmt->bindValue($identifier, $this->pays_es, PDO::PARAM_STR);
                        break;
                    case '`libelle_en`':
                        $stmt->bindValue($identifier, $this->libelle_en, PDO::PARAM_STR);
                        break;
                    case '`adresse_en`':
                        $stmt->bindValue($identifier, $this->adresse_en, PDO::PARAM_STR);
                        break;
                    case '`adresse_suite_en`':
                        $stmt->bindValue($identifier, $this->adresse_suite_en, PDO::PARAM_STR);
                        break;
                    case '`ville_en`':
                        $stmt->bindValue($identifier, $this->ville_en, PDO::PARAM_STR);
                        break;
                    case '`pays_en`':
                        $stmt->bindValue($identifier, $this->pays_en, PDO::PARAM_STR);
                        break;
                    case '`libelle_su`':
                        $stmt->bindValue($identifier, $this->libelle_su, PDO::PARAM_STR);
                        break;
                    case '`adresse_su`':
                        $stmt->bindValue($identifier, $this->adresse_su, PDO::PARAM_STR);
                        break;
                    case '`adresse_suite_su`':
                        $stmt->bindValue($identifier, $this->adresse_suite_su, PDO::PARAM_STR);
                        break;
                    case '`ville_su`':
                        $stmt->bindValue($identifier, $this->ville_su, PDO::PARAM_STR);
                        break;
                    case '`pays_su`':
                        $stmt->bindValue($identifier, $this->pays_su, PDO::PARAM_STR);
                        break;
                    case '`libelle_du`':
                        $stmt->bindValue($identifier, $this->libelle_du, PDO::PARAM_STR);
                        break;
                    case '`adresse_du`':
                        $stmt->bindValue($identifier, $this->adresse_du, PDO::PARAM_STR);
                        break;
                    case '`adresse_suite_du`':
                        $stmt->bindValue($identifier, $this->adresse_suite_du, PDO::PARAM_STR);
                        break;
                    case '`ville_du`':
                        $stmt->bindValue($identifier, $this->ville_du, PDO::PARAM_STR);
                        break;
                    case '`pays_du`':
                        $stmt->bindValue($identifier, $this->pays_du, PDO::PARAM_STR);
                        break;
                    case '`libelle_cz`':
                        $stmt->bindValue($identifier, $this->libelle_cz, PDO::PARAM_STR);
                        break;
                    case '`adresse_cz`':
                        $stmt->bindValue($identifier, $this->adresse_cz, PDO::PARAM_STR);
                        break;
                    case '`adresse_suite_cz`':
                        $stmt->bindValue($identifier, $this->adresse_suite_cz, PDO::PARAM_STR);
                        break;
                    case '`ville_cz`':
                        $stmt->bindValue($identifier, $this->ville_cz, PDO::PARAM_STR);
                        break;
                    case '`pays_cz`':
                        $stmt->bindValue($identifier, $this->pays_cz, PDO::PARAM_STR);
                        break;
                    case '`libelle_it`':
                        $stmt->bindValue($identifier, $this->libelle_it, PDO::PARAM_STR);
                        break;
                    case '`adresse_it`':
                        $stmt->bindValue($identifier, $this->adresse_it, PDO::PARAM_STR);
                        break;
                    case '`adresse_suite_it`':
                        $stmt->bindValue($identifier, $this->adresse_suite_it, PDO::PARAM_STR);
                        break;
                    case '`ville_it`':
                        $stmt->bindValue($identifier, $this->ville_it, PDO::PARAM_STR);
                        break;
                    case '`pays_it`':
                        $stmt->bindValue($identifier, $this->pays_it, PDO::PARAM_STR);
                        break;
                    case '`chemin_complet`':
                        $stmt->bindValue($identifier, $this->chemin_complet, PDO::PARAM_STR);
                        break;
                    case '`chemin_complet_fr`':
                        $stmt->bindValue($identifier, $this->chemin_complet_fr, PDO::PARAM_STR);
                        break;
                    case '`chemin_complet_en`':
                        $stmt->bindValue($identifier, $this->chemin_complet_en, PDO::PARAM_STR);
                        break;
                    case '`chemin_complet_es`':
                        $stmt->bindValue($identifier, $this->chemin_complet_es, PDO::PARAM_STR);
                        break;
                    case '`chemin_complet_su`':
                        $stmt->bindValue($identifier, $this->chemin_complet_su, PDO::PARAM_STR);
                        break;
                    case '`chemin_complet_du`':
                        $stmt->bindValue($identifier, $this->chemin_complet_du, PDO::PARAM_STR);
                        break;
                    case '`chemin_complet_cz`':
                        $stmt->bindValue($identifier, $this->chemin_complet_cz, PDO::PARAM_STR);
                        break;
                    case '`chemin_complet_ar`':
                        $stmt->bindValue($identifier, $this->chemin_complet_ar, PDO::PARAM_STR);
                        break;
                    case '`chemin_complet_it`':
                        $stmt->bindValue($identifier, $this->chemin_complet_it, PDO::PARAM_STR);
                        break;
                    case '`nom_service_archiveur`':
                        $stmt->bindValue($identifier, $this->nom_service_archiveur, PDO::PARAM_STR);
                        break;
                    case '`identifiant_service_archiveur`':
                        $stmt->bindValue($identifier, $this->identifiant_service_archiveur, PDO::PARAM_STR);
                        break;
                    case '`affichage_service`':
                        $stmt->bindValue($identifier, $this->affichage_service, PDO::PARAM_STR);
                        break;
                    case '`activation_fuseau_horaire`':
                        $stmt->bindValue($identifier, $this->activation_fuseau_horaire, PDO::PARAM_STR);
                        break;
                    case '`decalage_horaire`':
                        $stmt->bindValue($identifier, $this->decalage_horaire, PDO::PARAM_STR);
                        break;
                    case '`lieu_residence`':
                        $stmt->bindValue($identifier, $this->lieu_residence, PDO::PARAM_STR);
                        break;
                    case '`alerte`':
                        $stmt->bindValue($identifier, $this->alerte, PDO::PARAM_STR);
                        break;
                    case '`acces_chorus`':
                        $stmt->bindValue($identifier, $this->acces_chorus, PDO::PARAM_STR);
                        break;
                    case '`forme_juridique`':
                        $stmt->bindValue($identifier, $this->forme_juridique, PDO::PARAM_STR);
                        break;
                    case '`forme_juridique_code`':
                        $stmt->bindValue($identifier, $this->forme_juridique_code, PDO::PARAM_STR);
                        break;
                    case '`synchronisation_exec`':
                        $stmt->bindValue($identifier, $this->synchronisation_exec, PDO::PARAM_STR);
                        break;
                    case '`id_entite`':
                        $stmt->bindValue($identifier, $this->id_entite, PDO::PARAM_INT);
                        break;
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonServicePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonAffiliationServicesRelatedByServiceId !== null) {
                    foreach ($this->collCommonAffiliationServicesRelatedByServiceId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonAffiliationServicesRelatedByServiceParentId !== null) {
                    foreach ($this->collCommonAffiliationServicesRelatedByServiceParentId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonAgents !== null) {
                    foreach ($this->collCommonAgents as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonEncherePmis !== null) {
                    foreach ($this->collCommonEncherePmis as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonNewslettersRelatedByIdServiceDestinataire !== null) {
                    foreach ($this->collCommonNewslettersRelatedByIdServiceDestinataire as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonNewslettersRelatedByIdServiceRedacteur !== null) {
                    foreach ($this->collCommonNewslettersRelatedByIdServiceRedacteur as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonParametrageEncheres !== null) {
                    foreach ($this->collCommonParametrageEncheres as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTTelechargementAsynchrones !== null) {
                    foreach ($this->collCommonTTelechargementAsynchrones as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTypeProcedureOrganismes !== null) {
                    foreach ($this->collCommonTypeProcedureOrganismes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonAgentTechniqueAssociations !== null) {
                    foreach ($this->collCommonAgentTechniqueAssociations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonConsultationsRelatedByServiceValidation !== null) {
                    foreach ($this->collCommonConsultationsRelatedByServiceValidation as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonConsultationsRelatedByServiceId !== null) {
                    foreach ($this->collCommonConsultationsRelatedByServiceId as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonConsultationsRelatedByServiceValidationIntermediaire !== null) {
                    foreach ($this->collCommonConsultationsRelatedByServiceValidationIntermediaire as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonInvitePermanentTransverses !== null) {
                    foreach ($this->collCommonInvitePermanentTransverses as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTFusionnerServicessRelatedByIdServiceCible !== null) {
                    foreach ($this->collCommonTFusionnerServicessRelatedByIdServiceCible as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTFusionnerServicessRelatedByIdServiceSource !== null) {
                    foreach ($this->collCommonTFusionnerServicessRelatedByIdServiceSource as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonServicePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getOldId();
                break;
            case 1:
                return $this->getOrganisme();
                break;
            case 2:
                return $this->getTypeService();
                break;
            case 3:
                return $this->getLibelle();
                break;
            case 4:
                return $this->getSigle();
                break;
            case 5:
                return $this->getAdresse();
                break;
            case 6:
                return $this->getAdresseSuite();
                break;
            case 7:
                return $this->getCp();
                break;
            case 8:
                return $this->getVille();
                break;
            case 9:
                return $this->getTelephone();
                break;
            case 10:
                return $this->getFax();
                break;
            case 11:
                return $this->getMail();
                break;
            case 12:
                return $this->getPays();
                break;
            case 13:
                return $this->getIdExterne();
                break;
            case 14:
                return $this->getDateCreation();
                break;
            case 15:
                return $this->getDateModification();
                break;
            case 16:
                return $this->getSiren();
                break;
            case 17:
                return $this->getComplement();
                break;
            case 18:
                return $this->getLibelleAr();
                break;
            case 19:
                return $this->getAdresseAr();
                break;
            case 20:
                return $this->getAdresseSuiteAr();
                break;
            case 21:
                return $this->getVilleAr();
                break;
            case 22:
                return $this->getPaysAr();
                break;
            case 23:
                return $this->getLibelleFr();
                break;
            case 24:
                return $this->getAdresseFr();
                break;
            case 25:
                return $this->getAdresseSuiteFr();
                break;
            case 26:
                return $this->getVilleFr();
                break;
            case 27:
                return $this->getPaysFr();
                break;
            case 28:
                return $this->getLibelleEs();
                break;
            case 29:
                return $this->getAdresseEs();
                break;
            case 30:
                return $this->getAdresseSuiteEs();
                break;
            case 31:
                return $this->getVilleEs();
                break;
            case 32:
                return $this->getPaysEs();
                break;
            case 33:
                return $this->getLibelleEn();
                break;
            case 34:
                return $this->getAdresseEn();
                break;
            case 35:
                return $this->getAdresseSuiteEn();
                break;
            case 36:
                return $this->getVilleEn();
                break;
            case 37:
                return $this->getPaysEn();
                break;
            case 38:
                return $this->getLibelleSu();
                break;
            case 39:
                return $this->getAdresseSu();
                break;
            case 40:
                return $this->getAdresseSuiteSu();
                break;
            case 41:
                return $this->getVilleSu();
                break;
            case 42:
                return $this->getPaysSu();
                break;
            case 43:
                return $this->getLibelleDu();
                break;
            case 44:
                return $this->getAdresseDu();
                break;
            case 45:
                return $this->getAdresseSuiteDu();
                break;
            case 46:
                return $this->getVilleDu();
                break;
            case 47:
                return $this->getPaysDu();
                break;
            case 48:
                return $this->getLibelleCz();
                break;
            case 49:
                return $this->getAdresseCz();
                break;
            case 50:
                return $this->getAdresseSuiteCz();
                break;
            case 51:
                return $this->getVilleCz();
                break;
            case 52:
                return $this->getPaysCz();
                break;
            case 53:
                return $this->getLibelleIt();
                break;
            case 54:
                return $this->getAdresseIt();
                break;
            case 55:
                return $this->getAdresseSuiteIt();
                break;
            case 56:
                return $this->getVilleIt();
                break;
            case 57:
                return $this->getPaysIt();
                break;
            case 58:
                return $this->getCheminComplet();
                break;
            case 59:
                return $this->getCheminCompletFr();
                break;
            case 60:
                return $this->getCheminCompletEn();
                break;
            case 61:
                return $this->getCheminCompletEs();
                break;
            case 62:
                return $this->getCheminCompletSu();
                break;
            case 63:
                return $this->getCheminCompletDu();
                break;
            case 64:
                return $this->getCheminCompletCz();
                break;
            case 65:
                return $this->getCheminCompletAr();
                break;
            case 66:
                return $this->getCheminCompletIt();
                break;
            case 67:
                return $this->getNomServiceArchiveur();
                break;
            case 68:
                return $this->getIdentifiantServiceArchiveur();
                break;
            case 69:
                return $this->getAffichageService();
                break;
            case 70:
                return $this->getActivationFuseauHoraire();
                break;
            case 71:
                return $this->getDecalageHoraire();
                break;
            case 72:
                return $this->getLieuResidence();
                break;
            case 73:
                return $this->getAlerte();
                break;
            case 74:
                return $this->getAccesChorus();
                break;
            case 75:
                return $this->getFormeJuridique();
                break;
            case 76:
                return $this->getFormeJuridiqueCode();
                break;
            case 77:
                return $this->getSynchronisationExec();
                break;
            case 78:
                return $this->getIdEntite();
                break;
            case 79:
                return $this->getId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonService'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonService'][$this->getPrimaryKey()] = true;
        $keys = CommonServicePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getOldId(),
            $keys[1] => $this->getOrganisme(),
            $keys[2] => $this->getTypeService(),
            $keys[3] => $this->getLibelle(),
            $keys[4] => $this->getSigle(),
            $keys[5] => $this->getAdresse(),
            $keys[6] => $this->getAdresseSuite(),
            $keys[7] => $this->getCp(),
            $keys[8] => $this->getVille(),
            $keys[9] => $this->getTelephone(),
            $keys[10] => $this->getFax(),
            $keys[11] => $this->getMail(),
            $keys[12] => $this->getPays(),
            $keys[13] => $this->getIdExterne(),
            $keys[14] => $this->getDateCreation(),
            $keys[15] => $this->getDateModification(),
            $keys[16] => $this->getSiren(),
            $keys[17] => $this->getComplement(),
            $keys[18] => $this->getLibelleAr(),
            $keys[19] => $this->getAdresseAr(),
            $keys[20] => $this->getAdresseSuiteAr(),
            $keys[21] => $this->getVilleAr(),
            $keys[22] => $this->getPaysAr(),
            $keys[23] => $this->getLibelleFr(),
            $keys[24] => $this->getAdresseFr(),
            $keys[25] => $this->getAdresseSuiteFr(),
            $keys[26] => $this->getVilleFr(),
            $keys[27] => $this->getPaysFr(),
            $keys[28] => $this->getLibelleEs(),
            $keys[29] => $this->getAdresseEs(),
            $keys[30] => $this->getAdresseSuiteEs(),
            $keys[31] => $this->getVilleEs(),
            $keys[32] => $this->getPaysEs(),
            $keys[33] => $this->getLibelleEn(),
            $keys[34] => $this->getAdresseEn(),
            $keys[35] => $this->getAdresseSuiteEn(),
            $keys[36] => $this->getVilleEn(),
            $keys[37] => $this->getPaysEn(),
            $keys[38] => $this->getLibelleSu(),
            $keys[39] => $this->getAdresseSu(),
            $keys[40] => $this->getAdresseSuiteSu(),
            $keys[41] => $this->getVilleSu(),
            $keys[42] => $this->getPaysSu(),
            $keys[43] => $this->getLibelleDu(),
            $keys[44] => $this->getAdresseDu(),
            $keys[45] => $this->getAdresseSuiteDu(),
            $keys[46] => $this->getVilleDu(),
            $keys[47] => $this->getPaysDu(),
            $keys[48] => $this->getLibelleCz(),
            $keys[49] => $this->getAdresseCz(),
            $keys[50] => $this->getAdresseSuiteCz(),
            $keys[51] => $this->getVilleCz(),
            $keys[52] => $this->getPaysCz(),
            $keys[53] => $this->getLibelleIt(),
            $keys[54] => $this->getAdresseIt(),
            $keys[55] => $this->getAdresseSuiteIt(),
            $keys[56] => $this->getVilleIt(),
            $keys[57] => $this->getPaysIt(),
            $keys[58] => $this->getCheminComplet(),
            $keys[59] => $this->getCheminCompletFr(),
            $keys[60] => $this->getCheminCompletEn(),
            $keys[61] => $this->getCheminCompletEs(),
            $keys[62] => $this->getCheminCompletSu(),
            $keys[63] => $this->getCheminCompletDu(),
            $keys[64] => $this->getCheminCompletCz(),
            $keys[65] => $this->getCheminCompletAr(),
            $keys[66] => $this->getCheminCompletIt(),
            $keys[67] => $this->getNomServiceArchiveur(),
            $keys[68] => $this->getIdentifiantServiceArchiveur(),
            $keys[69] => $this->getAffichageService(),
            $keys[70] => $this->getActivationFuseauHoraire(),
            $keys[71] => $this->getDecalageHoraire(),
            $keys[72] => $this->getLieuResidence(),
            $keys[73] => $this->getAlerte(),
            $keys[74] => $this->getAccesChorus(),
            $keys[75] => $this->getFormeJuridique(),
            $keys[76] => $this->getFormeJuridiqueCode(),
            $keys[77] => $this->getSynchronisationExec(),
            $keys[78] => $this->getIdEntite(),
            $keys[79] => $this->getId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collCommonAffiliationServicesRelatedByServiceId) {
                $result['CommonAffiliationServicesRelatedByServiceId'] = $this->collCommonAffiliationServicesRelatedByServiceId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonAffiliationServicesRelatedByServiceParentId) {
                $result['CommonAffiliationServicesRelatedByServiceParentId'] = $this->collCommonAffiliationServicesRelatedByServiceParentId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonAgents) {
                $result['CommonAgents'] = $this->collCommonAgents->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonEncherePmis) {
                $result['CommonEncherePmis'] = $this->collCommonEncherePmis->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonNewslettersRelatedByIdServiceDestinataire) {
                $result['CommonNewslettersRelatedByIdServiceDestinataire'] = $this->collCommonNewslettersRelatedByIdServiceDestinataire->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonNewslettersRelatedByIdServiceRedacteur) {
                $result['CommonNewslettersRelatedByIdServiceRedacteur'] = $this->collCommonNewslettersRelatedByIdServiceRedacteur->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonParametrageEncheres) {
                $result['CommonParametrageEncheres'] = $this->collCommonParametrageEncheres->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTTelechargementAsynchrones) {
                $result['CommonTTelechargementAsynchrones'] = $this->collCommonTTelechargementAsynchrones->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTypeProcedureOrganismes) {
                $result['CommonTypeProcedureOrganismes'] = $this->collCommonTypeProcedureOrganismes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonAgentTechniqueAssociations) {
                $result['CommonAgentTechniqueAssociations'] = $this->collCommonAgentTechniqueAssociations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonConsultationsRelatedByServiceValidation) {
                $result['CommonConsultationsRelatedByServiceValidation'] = $this->collCommonConsultationsRelatedByServiceValidation->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonConsultationsRelatedByServiceId) {
                $result['CommonConsultationsRelatedByServiceId'] = $this->collCommonConsultationsRelatedByServiceId->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonConsultationsRelatedByServiceValidationIntermediaire) {
                $result['CommonConsultationsRelatedByServiceValidationIntermediaire'] = $this->collCommonConsultationsRelatedByServiceValidationIntermediaire->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonInvitePermanentTransverses) {
                $result['CommonInvitePermanentTransverses'] = $this->collCommonInvitePermanentTransverses->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTFusionnerServicessRelatedByIdServiceCible) {
                $result['CommonTFusionnerServicessRelatedByIdServiceCible'] = $this->collCommonTFusionnerServicessRelatedByIdServiceCible->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTFusionnerServicessRelatedByIdServiceSource) {
                $result['CommonTFusionnerServicessRelatedByIdServiceSource'] = $this->collCommonTFusionnerServicessRelatedByIdServiceSource->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonServicePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setOldId($value);
                break;
            case 1:
                $this->setOrganisme($value);
                break;
            case 2:
                $this->setTypeService($value);
                break;
            case 3:
                $this->setLibelle($value);
                break;
            case 4:
                $this->setSigle($value);
                break;
            case 5:
                $this->setAdresse($value);
                break;
            case 6:
                $this->setAdresseSuite($value);
                break;
            case 7:
                $this->setCp($value);
                break;
            case 8:
                $this->setVille($value);
                break;
            case 9:
                $this->setTelephone($value);
                break;
            case 10:
                $this->setFax($value);
                break;
            case 11:
                $this->setMail($value);
                break;
            case 12:
                $this->setPays($value);
                break;
            case 13:
                $this->setIdExterne($value);
                break;
            case 14:
                $this->setDateCreation($value);
                break;
            case 15:
                $this->setDateModification($value);
                break;
            case 16:
                $this->setSiren($value);
                break;
            case 17:
                $this->setComplement($value);
                break;
            case 18:
                $this->setLibelleAr($value);
                break;
            case 19:
                $this->setAdresseAr($value);
                break;
            case 20:
                $this->setAdresseSuiteAr($value);
                break;
            case 21:
                $this->setVilleAr($value);
                break;
            case 22:
                $this->setPaysAr($value);
                break;
            case 23:
                $this->setLibelleFr($value);
                break;
            case 24:
                $this->setAdresseFr($value);
                break;
            case 25:
                $this->setAdresseSuiteFr($value);
                break;
            case 26:
                $this->setVilleFr($value);
                break;
            case 27:
                $this->setPaysFr($value);
                break;
            case 28:
                $this->setLibelleEs($value);
                break;
            case 29:
                $this->setAdresseEs($value);
                break;
            case 30:
                $this->setAdresseSuiteEs($value);
                break;
            case 31:
                $this->setVilleEs($value);
                break;
            case 32:
                $this->setPaysEs($value);
                break;
            case 33:
                $this->setLibelleEn($value);
                break;
            case 34:
                $this->setAdresseEn($value);
                break;
            case 35:
                $this->setAdresseSuiteEn($value);
                break;
            case 36:
                $this->setVilleEn($value);
                break;
            case 37:
                $this->setPaysEn($value);
                break;
            case 38:
                $this->setLibelleSu($value);
                break;
            case 39:
                $this->setAdresseSu($value);
                break;
            case 40:
                $this->setAdresseSuiteSu($value);
                break;
            case 41:
                $this->setVilleSu($value);
                break;
            case 42:
                $this->setPaysSu($value);
                break;
            case 43:
                $this->setLibelleDu($value);
                break;
            case 44:
                $this->setAdresseDu($value);
                break;
            case 45:
                $this->setAdresseSuiteDu($value);
                break;
            case 46:
                $this->setVilleDu($value);
                break;
            case 47:
                $this->setPaysDu($value);
                break;
            case 48:
                $this->setLibelleCz($value);
                break;
            case 49:
                $this->setAdresseCz($value);
                break;
            case 50:
                $this->setAdresseSuiteCz($value);
                break;
            case 51:
                $this->setVilleCz($value);
                break;
            case 52:
                $this->setPaysCz($value);
                break;
            case 53:
                $this->setLibelleIt($value);
                break;
            case 54:
                $this->setAdresseIt($value);
                break;
            case 55:
                $this->setAdresseSuiteIt($value);
                break;
            case 56:
                $this->setVilleIt($value);
                break;
            case 57:
                $this->setPaysIt($value);
                break;
            case 58:
                $this->setCheminComplet($value);
                break;
            case 59:
                $this->setCheminCompletFr($value);
                break;
            case 60:
                $this->setCheminCompletEn($value);
                break;
            case 61:
                $this->setCheminCompletEs($value);
                break;
            case 62:
                $this->setCheminCompletSu($value);
                break;
            case 63:
                $this->setCheminCompletDu($value);
                break;
            case 64:
                $this->setCheminCompletCz($value);
                break;
            case 65:
                $this->setCheminCompletAr($value);
                break;
            case 66:
                $this->setCheminCompletIt($value);
                break;
            case 67:
                $this->setNomServiceArchiveur($value);
                break;
            case 68:
                $this->setIdentifiantServiceArchiveur($value);
                break;
            case 69:
                $this->setAffichageService($value);
                break;
            case 70:
                $this->setActivationFuseauHoraire($value);
                break;
            case 71:
                $this->setDecalageHoraire($value);
                break;
            case 72:
                $this->setLieuResidence($value);
                break;
            case 73:
                $this->setAlerte($value);
                break;
            case 74:
                $this->setAccesChorus($value);
                break;
            case 75:
                $this->setFormeJuridique($value);
                break;
            case 76:
                $this->setFormeJuridiqueCode($value);
                break;
            case 77:
                $this->setSynchronisationExec($value);
                break;
            case 78:
                $this->setIdEntite($value);
                break;
            case 79:
                $this->setId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonServicePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setOldId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setOrganisme($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setTypeService($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setLibelle($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setSigle($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setAdresse($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setAdresseSuite($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setCp($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setVille($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setTelephone($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setFax($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setMail($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setPays($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setIdExterne($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setDateCreation($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setDateModification($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setSiren($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setComplement($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setLibelleAr($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setAdresseAr($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setAdresseSuiteAr($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setVilleAr($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setPaysAr($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setLibelleFr($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setAdresseFr($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setAdresseSuiteFr($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setVilleFr($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setPaysFr($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setLibelleEs($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setAdresseEs($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setAdresseSuiteEs($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setVilleEs($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setPaysEs($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setLibelleEn($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setAdresseEn($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setAdresseSuiteEn($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setVilleEn($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setPaysEn($arr[$keys[37]]);
        if (array_key_exists($keys[38], $arr)) $this->setLibelleSu($arr[$keys[38]]);
        if (array_key_exists($keys[39], $arr)) $this->setAdresseSu($arr[$keys[39]]);
        if (array_key_exists($keys[40], $arr)) $this->setAdresseSuiteSu($arr[$keys[40]]);
        if (array_key_exists($keys[41], $arr)) $this->setVilleSu($arr[$keys[41]]);
        if (array_key_exists($keys[42], $arr)) $this->setPaysSu($arr[$keys[42]]);
        if (array_key_exists($keys[43], $arr)) $this->setLibelleDu($arr[$keys[43]]);
        if (array_key_exists($keys[44], $arr)) $this->setAdresseDu($arr[$keys[44]]);
        if (array_key_exists($keys[45], $arr)) $this->setAdresseSuiteDu($arr[$keys[45]]);
        if (array_key_exists($keys[46], $arr)) $this->setVilleDu($arr[$keys[46]]);
        if (array_key_exists($keys[47], $arr)) $this->setPaysDu($arr[$keys[47]]);
        if (array_key_exists($keys[48], $arr)) $this->setLibelleCz($arr[$keys[48]]);
        if (array_key_exists($keys[49], $arr)) $this->setAdresseCz($arr[$keys[49]]);
        if (array_key_exists($keys[50], $arr)) $this->setAdresseSuiteCz($arr[$keys[50]]);
        if (array_key_exists($keys[51], $arr)) $this->setVilleCz($arr[$keys[51]]);
        if (array_key_exists($keys[52], $arr)) $this->setPaysCz($arr[$keys[52]]);
        if (array_key_exists($keys[53], $arr)) $this->setLibelleIt($arr[$keys[53]]);
        if (array_key_exists($keys[54], $arr)) $this->setAdresseIt($arr[$keys[54]]);
        if (array_key_exists($keys[55], $arr)) $this->setAdresseSuiteIt($arr[$keys[55]]);
        if (array_key_exists($keys[56], $arr)) $this->setVilleIt($arr[$keys[56]]);
        if (array_key_exists($keys[57], $arr)) $this->setPaysIt($arr[$keys[57]]);
        if (array_key_exists($keys[58], $arr)) $this->setCheminComplet($arr[$keys[58]]);
        if (array_key_exists($keys[59], $arr)) $this->setCheminCompletFr($arr[$keys[59]]);
        if (array_key_exists($keys[60], $arr)) $this->setCheminCompletEn($arr[$keys[60]]);
        if (array_key_exists($keys[61], $arr)) $this->setCheminCompletEs($arr[$keys[61]]);
        if (array_key_exists($keys[62], $arr)) $this->setCheminCompletSu($arr[$keys[62]]);
        if (array_key_exists($keys[63], $arr)) $this->setCheminCompletDu($arr[$keys[63]]);
        if (array_key_exists($keys[64], $arr)) $this->setCheminCompletCz($arr[$keys[64]]);
        if (array_key_exists($keys[65], $arr)) $this->setCheminCompletAr($arr[$keys[65]]);
        if (array_key_exists($keys[66], $arr)) $this->setCheminCompletIt($arr[$keys[66]]);
        if (array_key_exists($keys[67], $arr)) $this->setNomServiceArchiveur($arr[$keys[67]]);
        if (array_key_exists($keys[68], $arr)) $this->setIdentifiantServiceArchiveur($arr[$keys[68]]);
        if (array_key_exists($keys[69], $arr)) $this->setAffichageService($arr[$keys[69]]);
        if (array_key_exists($keys[70], $arr)) $this->setActivationFuseauHoraire($arr[$keys[70]]);
        if (array_key_exists($keys[71], $arr)) $this->setDecalageHoraire($arr[$keys[71]]);
        if (array_key_exists($keys[72], $arr)) $this->setLieuResidence($arr[$keys[72]]);
        if (array_key_exists($keys[73], $arr)) $this->setAlerte($arr[$keys[73]]);
        if (array_key_exists($keys[74], $arr)) $this->setAccesChorus($arr[$keys[74]]);
        if (array_key_exists($keys[75], $arr)) $this->setFormeJuridique($arr[$keys[75]]);
        if (array_key_exists($keys[76], $arr)) $this->setFormeJuridiqueCode($arr[$keys[76]]);
        if (array_key_exists($keys[77], $arr)) $this->setSynchronisationExec($arr[$keys[77]]);
        if (array_key_exists($keys[78], $arr)) $this->setIdEntite($arr[$keys[78]]);
        if (array_key_exists($keys[79], $arr)) $this->setId($arr[$keys[79]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonServicePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonServicePeer::OLD_ID)) $criteria->add(CommonServicePeer::OLD_ID, $this->old_id);
        if ($this->isColumnModified(CommonServicePeer::ORGANISME)) $criteria->add(CommonServicePeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonServicePeer::TYPE_SERVICE)) $criteria->add(CommonServicePeer::TYPE_SERVICE, $this->type_service);
        if ($this->isColumnModified(CommonServicePeer::LIBELLE)) $criteria->add(CommonServicePeer::LIBELLE, $this->libelle);
        if ($this->isColumnModified(CommonServicePeer::SIGLE)) $criteria->add(CommonServicePeer::SIGLE, $this->sigle);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE)) $criteria->add(CommonServicePeer::ADRESSE, $this->adresse);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE)) $criteria->add(CommonServicePeer::ADRESSE_SUITE, $this->adresse_suite);
        if ($this->isColumnModified(CommonServicePeer::CP)) $criteria->add(CommonServicePeer::CP, $this->cp);
        if ($this->isColumnModified(CommonServicePeer::VILLE)) $criteria->add(CommonServicePeer::VILLE, $this->ville);
        if ($this->isColumnModified(CommonServicePeer::TELEPHONE)) $criteria->add(CommonServicePeer::TELEPHONE, $this->telephone);
        if ($this->isColumnModified(CommonServicePeer::FAX)) $criteria->add(CommonServicePeer::FAX, $this->fax);
        if ($this->isColumnModified(CommonServicePeer::MAIL)) $criteria->add(CommonServicePeer::MAIL, $this->mail);
        if ($this->isColumnModified(CommonServicePeer::PAYS)) $criteria->add(CommonServicePeer::PAYS, $this->pays);
        if ($this->isColumnModified(CommonServicePeer::ID_EXTERNE)) $criteria->add(CommonServicePeer::ID_EXTERNE, $this->id_externe);
        if ($this->isColumnModified(CommonServicePeer::DATE_CREATION)) $criteria->add(CommonServicePeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonServicePeer::DATE_MODIFICATION)) $criteria->add(CommonServicePeer::DATE_MODIFICATION, $this->date_modification);
        if ($this->isColumnModified(CommonServicePeer::SIREN)) $criteria->add(CommonServicePeer::SIREN, $this->siren);
        if ($this->isColumnModified(CommonServicePeer::COMPLEMENT)) $criteria->add(CommonServicePeer::COMPLEMENT, $this->complement);
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_AR)) $criteria->add(CommonServicePeer::LIBELLE_AR, $this->libelle_ar);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_AR)) $criteria->add(CommonServicePeer::ADRESSE_AR, $this->adresse_ar);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_AR)) $criteria->add(CommonServicePeer::ADRESSE_SUITE_AR, $this->adresse_suite_ar);
        if ($this->isColumnModified(CommonServicePeer::VILLE_AR)) $criteria->add(CommonServicePeer::VILLE_AR, $this->ville_ar);
        if ($this->isColumnModified(CommonServicePeer::PAYS_AR)) $criteria->add(CommonServicePeer::PAYS_AR, $this->pays_ar);
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_FR)) $criteria->add(CommonServicePeer::LIBELLE_FR, $this->libelle_fr);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_FR)) $criteria->add(CommonServicePeer::ADRESSE_FR, $this->adresse_fr);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_FR)) $criteria->add(CommonServicePeer::ADRESSE_SUITE_FR, $this->adresse_suite_fr);
        if ($this->isColumnModified(CommonServicePeer::VILLE_FR)) $criteria->add(CommonServicePeer::VILLE_FR, $this->ville_fr);
        if ($this->isColumnModified(CommonServicePeer::PAYS_FR)) $criteria->add(CommonServicePeer::PAYS_FR, $this->pays_fr);
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_ES)) $criteria->add(CommonServicePeer::LIBELLE_ES, $this->libelle_es);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_ES)) $criteria->add(CommonServicePeer::ADRESSE_ES, $this->adresse_es);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_ES)) $criteria->add(CommonServicePeer::ADRESSE_SUITE_ES, $this->adresse_suite_es);
        if ($this->isColumnModified(CommonServicePeer::VILLE_ES)) $criteria->add(CommonServicePeer::VILLE_ES, $this->ville_es);
        if ($this->isColumnModified(CommonServicePeer::PAYS_ES)) $criteria->add(CommonServicePeer::PAYS_ES, $this->pays_es);
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_EN)) $criteria->add(CommonServicePeer::LIBELLE_EN, $this->libelle_en);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_EN)) $criteria->add(CommonServicePeer::ADRESSE_EN, $this->adresse_en);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_EN)) $criteria->add(CommonServicePeer::ADRESSE_SUITE_EN, $this->adresse_suite_en);
        if ($this->isColumnModified(CommonServicePeer::VILLE_EN)) $criteria->add(CommonServicePeer::VILLE_EN, $this->ville_en);
        if ($this->isColumnModified(CommonServicePeer::PAYS_EN)) $criteria->add(CommonServicePeer::PAYS_EN, $this->pays_en);
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_SU)) $criteria->add(CommonServicePeer::LIBELLE_SU, $this->libelle_su);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SU)) $criteria->add(CommonServicePeer::ADRESSE_SU, $this->adresse_su);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_SU)) $criteria->add(CommonServicePeer::ADRESSE_SUITE_SU, $this->adresse_suite_su);
        if ($this->isColumnModified(CommonServicePeer::VILLE_SU)) $criteria->add(CommonServicePeer::VILLE_SU, $this->ville_su);
        if ($this->isColumnModified(CommonServicePeer::PAYS_SU)) $criteria->add(CommonServicePeer::PAYS_SU, $this->pays_su);
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_DU)) $criteria->add(CommonServicePeer::LIBELLE_DU, $this->libelle_du);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_DU)) $criteria->add(CommonServicePeer::ADRESSE_DU, $this->adresse_du);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_DU)) $criteria->add(CommonServicePeer::ADRESSE_SUITE_DU, $this->adresse_suite_du);
        if ($this->isColumnModified(CommonServicePeer::VILLE_DU)) $criteria->add(CommonServicePeer::VILLE_DU, $this->ville_du);
        if ($this->isColumnModified(CommonServicePeer::PAYS_DU)) $criteria->add(CommonServicePeer::PAYS_DU, $this->pays_du);
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_CZ)) $criteria->add(CommonServicePeer::LIBELLE_CZ, $this->libelle_cz);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_CZ)) $criteria->add(CommonServicePeer::ADRESSE_CZ, $this->adresse_cz);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_CZ)) $criteria->add(CommonServicePeer::ADRESSE_SUITE_CZ, $this->adresse_suite_cz);
        if ($this->isColumnModified(CommonServicePeer::VILLE_CZ)) $criteria->add(CommonServicePeer::VILLE_CZ, $this->ville_cz);
        if ($this->isColumnModified(CommonServicePeer::PAYS_CZ)) $criteria->add(CommonServicePeer::PAYS_CZ, $this->pays_cz);
        if ($this->isColumnModified(CommonServicePeer::LIBELLE_IT)) $criteria->add(CommonServicePeer::LIBELLE_IT, $this->libelle_it);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_IT)) $criteria->add(CommonServicePeer::ADRESSE_IT, $this->adresse_it);
        if ($this->isColumnModified(CommonServicePeer::ADRESSE_SUITE_IT)) $criteria->add(CommonServicePeer::ADRESSE_SUITE_IT, $this->adresse_suite_it);
        if ($this->isColumnModified(CommonServicePeer::VILLE_IT)) $criteria->add(CommonServicePeer::VILLE_IT, $this->ville_it);
        if ($this->isColumnModified(CommonServicePeer::PAYS_IT)) $criteria->add(CommonServicePeer::PAYS_IT, $this->pays_it);
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET)) $criteria->add(CommonServicePeer::CHEMIN_COMPLET, $this->chemin_complet);
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_FR)) $criteria->add(CommonServicePeer::CHEMIN_COMPLET_FR, $this->chemin_complet_fr);
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_EN)) $criteria->add(CommonServicePeer::CHEMIN_COMPLET_EN, $this->chemin_complet_en);
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_ES)) $criteria->add(CommonServicePeer::CHEMIN_COMPLET_ES, $this->chemin_complet_es);
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_SU)) $criteria->add(CommonServicePeer::CHEMIN_COMPLET_SU, $this->chemin_complet_su);
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_DU)) $criteria->add(CommonServicePeer::CHEMIN_COMPLET_DU, $this->chemin_complet_du);
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_CZ)) $criteria->add(CommonServicePeer::CHEMIN_COMPLET_CZ, $this->chemin_complet_cz);
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_AR)) $criteria->add(CommonServicePeer::CHEMIN_COMPLET_AR, $this->chemin_complet_ar);
        if ($this->isColumnModified(CommonServicePeer::CHEMIN_COMPLET_IT)) $criteria->add(CommonServicePeer::CHEMIN_COMPLET_IT, $this->chemin_complet_it);
        if ($this->isColumnModified(CommonServicePeer::NOM_SERVICE_ARCHIVEUR)) $criteria->add(CommonServicePeer::NOM_SERVICE_ARCHIVEUR, $this->nom_service_archiveur);
        if ($this->isColumnModified(CommonServicePeer::IDENTIFIANT_SERVICE_ARCHIVEUR)) $criteria->add(CommonServicePeer::IDENTIFIANT_SERVICE_ARCHIVEUR, $this->identifiant_service_archiveur);
        if ($this->isColumnModified(CommonServicePeer::AFFICHAGE_SERVICE)) $criteria->add(CommonServicePeer::AFFICHAGE_SERVICE, $this->affichage_service);
        if ($this->isColumnModified(CommonServicePeer::ACTIVATION_FUSEAU_HORAIRE)) $criteria->add(CommonServicePeer::ACTIVATION_FUSEAU_HORAIRE, $this->activation_fuseau_horaire);
        if ($this->isColumnModified(CommonServicePeer::DECALAGE_HORAIRE)) $criteria->add(CommonServicePeer::DECALAGE_HORAIRE, $this->decalage_horaire);
        if ($this->isColumnModified(CommonServicePeer::LIEU_RESIDENCE)) $criteria->add(CommonServicePeer::LIEU_RESIDENCE, $this->lieu_residence);
        if ($this->isColumnModified(CommonServicePeer::ALERTE)) $criteria->add(CommonServicePeer::ALERTE, $this->alerte);
        if ($this->isColumnModified(CommonServicePeer::ACCES_CHORUS)) $criteria->add(CommonServicePeer::ACCES_CHORUS, $this->acces_chorus);
        if ($this->isColumnModified(CommonServicePeer::FORME_JURIDIQUE)) $criteria->add(CommonServicePeer::FORME_JURIDIQUE, $this->forme_juridique);
        if ($this->isColumnModified(CommonServicePeer::FORME_JURIDIQUE_CODE)) $criteria->add(CommonServicePeer::FORME_JURIDIQUE_CODE, $this->forme_juridique_code);
        if ($this->isColumnModified(CommonServicePeer::SYNCHRONISATION_EXEC)) $criteria->add(CommonServicePeer::SYNCHRONISATION_EXEC, $this->synchronisation_exec);
        if ($this->isColumnModified(CommonServicePeer::ID_ENTITE)) $criteria->add(CommonServicePeer::ID_ENTITE, $this->id_entite);
        if ($this->isColumnModified(CommonServicePeer::ID)) $criteria->add(CommonServicePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonServicePeer::DATABASE_NAME);
        $criteria->add(CommonServicePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonService (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOldId($this->getOldId());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setTypeService($this->getTypeService());
        $copyObj->setLibelle($this->getLibelle());
        $copyObj->setSigle($this->getSigle());
        $copyObj->setAdresse($this->getAdresse());
        $copyObj->setAdresseSuite($this->getAdresseSuite());
        $copyObj->setCp($this->getCp());
        $copyObj->setVille($this->getVille());
        $copyObj->setTelephone($this->getTelephone());
        $copyObj->setFax($this->getFax());
        $copyObj->setMail($this->getMail());
        $copyObj->setPays($this->getPays());
        $copyObj->setIdExterne($this->getIdExterne());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateModification($this->getDateModification());
        $copyObj->setSiren($this->getSiren());
        $copyObj->setComplement($this->getComplement());
        $copyObj->setLibelleAr($this->getLibelleAr());
        $copyObj->setAdresseAr($this->getAdresseAr());
        $copyObj->setAdresseSuiteAr($this->getAdresseSuiteAr());
        $copyObj->setVilleAr($this->getVilleAr());
        $copyObj->setPaysAr($this->getPaysAr());
        $copyObj->setLibelleFr($this->getLibelleFr());
        $copyObj->setAdresseFr($this->getAdresseFr());
        $copyObj->setAdresseSuiteFr($this->getAdresseSuiteFr());
        $copyObj->setVilleFr($this->getVilleFr());
        $copyObj->setPaysFr($this->getPaysFr());
        $copyObj->setLibelleEs($this->getLibelleEs());
        $copyObj->setAdresseEs($this->getAdresseEs());
        $copyObj->setAdresseSuiteEs($this->getAdresseSuiteEs());
        $copyObj->setVilleEs($this->getVilleEs());
        $copyObj->setPaysEs($this->getPaysEs());
        $copyObj->setLibelleEn($this->getLibelleEn());
        $copyObj->setAdresseEn($this->getAdresseEn());
        $copyObj->setAdresseSuiteEn($this->getAdresseSuiteEn());
        $copyObj->setVilleEn($this->getVilleEn());
        $copyObj->setPaysEn($this->getPaysEn());
        $copyObj->setLibelleSu($this->getLibelleSu());
        $copyObj->setAdresseSu($this->getAdresseSu());
        $copyObj->setAdresseSuiteSu($this->getAdresseSuiteSu());
        $copyObj->setVilleSu($this->getVilleSu());
        $copyObj->setPaysSu($this->getPaysSu());
        $copyObj->setLibelleDu($this->getLibelleDu());
        $copyObj->setAdresseDu($this->getAdresseDu());
        $copyObj->setAdresseSuiteDu($this->getAdresseSuiteDu());
        $copyObj->setVilleDu($this->getVilleDu());
        $copyObj->setPaysDu($this->getPaysDu());
        $copyObj->setLibelleCz($this->getLibelleCz());
        $copyObj->setAdresseCz($this->getAdresseCz());
        $copyObj->setAdresseSuiteCz($this->getAdresseSuiteCz());
        $copyObj->setVilleCz($this->getVilleCz());
        $copyObj->setPaysCz($this->getPaysCz());
        $copyObj->setLibelleIt($this->getLibelleIt());
        $copyObj->setAdresseIt($this->getAdresseIt());
        $copyObj->setAdresseSuiteIt($this->getAdresseSuiteIt());
        $copyObj->setVilleIt($this->getVilleIt());
        $copyObj->setPaysIt($this->getPaysIt());
        $copyObj->setCheminComplet($this->getCheminComplet());
        $copyObj->setCheminCompletFr($this->getCheminCompletFr());
        $copyObj->setCheminCompletEn($this->getCheminCompletEn());
        $copyObj->setCheminCompletEs($this->getCheminCompletEs());
        $copyObj->setCheminCompletSu($this->getCheminCompletSu());
        $copyObj->setCheminCompletDu($this->getCheminCompletDu());
        $copyObj->setCheminCompletCz($this->getCheminCompletCz());
        $copyObj->setCheminCompletAr($this->getCheminCompletAr());
        $copyObj->setCheminCompletIt($this->getCheminCompletIt());
        $copyObj->setNomServiceArchiveur($this->getNomServiceArchiveur());
        $copyObj->setIdentifiantServiceArchiveur($this->getIdentifiantServiceArchiveur());
        $copyObj->setAffichageService($this->getAffichageService());
        $copyObj->setActivationFuseauHoraire($this->getActivationFuseauHoraire());
        $copyObj->setDecalageHoraire($this->getDecalageHoraire());
        $copyObj->setLieuResidence($this->getLieuResidence());
        $copyObj->setAlerte($this->getAlerte());
        $copyObj->setAccesChorus($this->getAccesChorus());
        $copyObj->setFormeJuridique($this->getFormeJuridique());
        $copyObj->setFormeJuridiqueCode($this->getFormeJuridiqueCode());
        $copyObj->setSynchronisationExec($this->getSynchronisationExec());
        $copyObj->setIdEntite($this->getIdEntite());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonAffiliationServicesRelatedByServiceId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAffiliationServiceRelatedByServiceId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonAffiliationServicesRelatedByServiceParentId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAffiliationServiceRelatedByServiceParentId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonAgents() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAgent($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonEncherePmis() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEncherePmi($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonNewslettersRelatedByIdServiceDestinataire() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonNewsletterRelatedByIdServiceDestinataire($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonNewslettersRelatedByIdServiceRedacteur() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonNewsletterRelatedByIdServiceRedacteur($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonParametrageEncheres() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonParametrageEnchere($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTTelechargementAsynchrones() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTTelechargementAsynchrone($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTypeProcedureOrganismes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTypeProcedureOrganisme($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonAgentTechniqueAssociations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAgentTechniqueAssociation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonConsultationsRelatedByServiceValidation() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultationRelatedByServiceValidation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonConsultationsRelatedByServiceId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultationRelatedByServiceId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonConsultationsRelatedByServiceValidationIntermediaire() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultationRelatedByServiceValidationIntermediaire($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonInvitePermanentTransverses() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonInvitePermanentTransverse($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTFusionnerServicessRelatedByIdServiceCible() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTFusionnerServicesRelatedByIdServiceCible($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTFusionnerServicessRelatedByIdServiceSource() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTFusionnerServicesRelatedByIdServiceSource($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonService Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonServicePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonServicePeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonAffiliationServiceRelatedByServiceId' == $relationName) {
            $this->initCommonAffiliationServicesRelatedByServiceId();
        }
        if ('CommonAffiliationServiceRelatedByServiceParentId' == $relationName) {
            $this->initCommonAffiliationServicesRelatedByServiceParentId();
        }
        if ('CommonAgent' == $relationName) {
            $this->initCommonAgents();
        }
        if ('CommonEncherePmi' == $relationName) {
            $this->initCommonEncherePmis();
        }
        if ('CommonNewsletterRelatedByIdServiceDestinataire' == $relationName) {
            $this->initCommonNewslettersRelatedByIdServiceDestinataire();
        }
        if ('CommonNewsletterRelatedByIdServiceRedacteur' == $relationName) {
            $this->initCommonNewslettersRelatedByIdServiceRedacteur();
        }
        if ('CommonParametrageEnchere' == $relationName) {
            $this->initCommonParametrageEncheres();
        }
        if ('CommonTTelechargementAsynchrone' == $relationName) {
            $this->initCommonTTelechargementAsynchrones();
        }
        if ('CommonTypeProcedureOrganisme' == $relationName) {
            $this->initCommonTypeProcedureOrganismes();
        }
        if ('CommonAgentTechniqueAssociation' == $relationName) {
            $this->initCommonAgentTechniqueAssociations();
        }
        if ('CommonConsultationRelatedByServiceValidation' == $relationName) {
            $this->initCommonConsultationsRelatedByServiceValidation();
        }
        if ('CommonConsultationRelatedByServiceId' == $relationName) {
            $this->initCommonConsultationsRelatedByServiceId();
        }
        if ('CommonConsultationRelatedByServiceValidationIntermediaire' == $relationName) {
            $this->initCommonConsultationsRelatedByServiceValidationIntermediaire();
        }
        if ('CommonInvitePermanentTransverse' == $relationName) {
            $this->initCommonInvitePermanentTransverses();
        }
        if ('CommonTFusionnerServicesRelatedByIdServiceCible' == $relationName) {
            $this->initCommonTFusionnerServicessRelatedByIdServiceCible();
        }
        if ('CommonTFusionnerServicesRelatedByIdServiceSource' == $relationName) {
            $this->initCommonTFusionnerServicessRelatedByIdServiceSource();
        }
    }

    /**
     * Clears out the collCommonAffiliationServicesRelatedByServiceId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonAffiliationServicesRelatedByServiceId()
     */
    public function clearCommonAffiliationServicesRelatedByServiceId()
    {
        $this->collCommonAffiliationServicesRelatedByServiceId = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAffiliationServicesRelatedByServiceIdPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAffiliationServicesRelatedByServiceId collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAffiliationServicesRelatedByServiceId($v = true)
    {
        $this->collCommonAffiliationServicesRelatedByServiceIdPartial = $v;
    }

    /**
     * Initializes the collCommonAffiliationServicesRelatedByServiceId collection.
     *
     * By default this just sets the collCommonAffiliationServicesRelatedByServiceId collection to an empty array (like clearcollCommonAffiliationServicesRelatedByServiceId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAffiliationServicesRelatedByServiceId($overrideExisting = true)
    {
        if (null !== $this->collCommonAffiliationServicesRelatedByServiceId && !$overrideExisting) {
            return;
        }
        $this->collCommonAffiliationServicesRelatedByServiceId = new PropelObjectCollection();
        $this->collCommonAffiliationServicesRelatedByServiceId->setModel('CommonAffiliationService');
    }

    /**
     * Gets an array of CommonAffiliationService objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAffiliationService[] List of CommonAffiliationService objects
     * @throws PropelException
     */
    public function getCommonAffiliationServicesRelatedByServiceId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAffiliationServicesRelatedByServiceIdPartial && !$this->isNew();
        if (null === $this->collCommonAffiliationServicesRelatedByServiceId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAffiliationServicesRelatedByServiceId) {
                // return empty collection
                $this->initCommonAffiliationServicesRelatedByServiceId();
            } else {
                $collCommonAffiliationServicesRelatedByServiceId = CommonAffiliationServiceQuery::create(null, $criteria)
                    ->filterByCommonServiceRelatedByServiceId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAffiliationServicesRelatedByServiceIdPartial && count($collCommonAffiliationServicesRelatedByServiceId)) {
                      $this->initCommonAffiliationServicesRelatedByServiceId(false);

                      foreach ($collCommonAffiliationServicesRelatedByServiceId as $obj) {
                        if (false == $this->collCommonAffiliationServicesRelatedByServiceId->contains($obj)) {
                          $this->collCommonAffiliationServicesRelatedByServiceId->append($obj);
                        }
                      }

                      $this->collCommonAffiliationServicesRelatedByServiceIdPartial = true;
                    }

                    $collCommonAffiliationServicesRelatedByServiceId->getInternalIterator()->rewind();

                    return $collCommonAffiliationServicesRelatedByServiceId;
                }

                if ($partial && $this->collCommonAffiliationServicesRelatedByServiceId) {
                    foreach ($this->collCommonAffiliationServicesRelatedByServiceId as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAffiliationServicesRelatedByServiceId[] = $obj;
                        }
                    }
                }

                $this->collCommonAffiliationServicesRelatedByServiceId = $collCommonAffiliationServicesRelatedByServiceId;
                $this->collCommonAffiliationServicesRelatedByServiceIdPartial = false;
            }
        }

        return $this->collCommonAffiliationServicesRelatedByServiceId;
    }

    /**
     * Sets a collection of CommonAffiliationServiceRelatedByServiceId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAffiliationServicesRelatedByServiceId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonAffiliationServicesRelatedByServiceId(PropelCollection $commonAffiliationServicesRelatedByServiceId, PropelPDO $con = null)
    {
        $commonAffiliationServicesRelatedByServiceIdToDelete = $this->getCommonAffiliationServicesRelatedByServiceId(new Criteria(), $con)->diff($commonAffiliationServicesRelatedByServiceId);


        $this->commonAffiliationServicesRelatedByServiceIdScheduledForDeletion = $commonAffiliationServicesRelatedByServiceIdToDelete;

        foreach ($commonAffiliationServicesRelatedByServiceIdToDelete as $commonAffiliationServiceRelatedByServiceIdRemoved) {
            $commonAffiliationServiceRelatedByServiceIdRemoved->setCommonServiceRelatedByServiceId(null);
        }

        $this->collCommonAffiliationServicesRelatedByServiceId = null;
        foreach ($commonAffiliationServicesRelatedByServiceId as $commonAffiliationServiceRelatedByServiceId) {
            $this->addCommonAffiliationServiceRelatedByServiceId($commonAffiliationServiceRelatedByServiceId);
        }

        $this->collCommonAffiliationServicesRelatedByServiceId = $commonAffiliationServicesRelatedByServiceId;
        $this->collCommonAffiliationServicesRelatedByServiceIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAffiliationService objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAffiliationService objects.
     * @throws PropelException
     */
    public function countCommonAffiliationServicesRelatedByServiceId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAffiliationServicesRelatedByServiceIdPartial && !$this->isNew();
        if (null === $this->collCommonAffiliationServicesRelatedByServiceId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAffiliationServicesRelatedByServiceId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAffiliationServicesRelatedByServiceId());
            }
            $query = CommonAffiliationServiceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonServiceRelatedByServiceId($this)
                ->count($con);
        }

        return count($this->collCommonAffiliationServicesRelatedByServiceId);
    }

    /**
     * Method called to associate a CommonAffiliationService object to this object
     * through the CommonAffiliationService foreign key attribute.
     *
     * @param   CommonAffiliationService $l CommonAffiliationService
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonAffiliationServiceRelatedByServiceId(CommonAffiliationService $l)
    {
        if ($this->collCommonAffiliationServicesRelatedByServiceId === null) {
            $this->initCommonAffiliationServicesRelatedByServiceId();
            $this->collCommonAffiliationServicesRelatedByServiceIdPartial = true;
        }
        if (!in_array($l, $this->collCommonAffiliationServicesRelatedByServiceId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAffiliationServiceRelatedByServiceId($l);
        }

        return $this;
    }

    /**
     * @param	CommonAffiliationServiceRelatedByServiceId $commonAffiliationServiceRelatedByServiceId The commonAffiliationServiceRelatedByServiceId object to add.
     */
    protected function doAddCommonAffiliationServiceRelatedByServiceId($commonAffiliationServiceRelatedByServiceId)
    {
        $this->collCommonAffiliationServicesRelatedByServiceId[]= $commonAffiliationServiceRelatedByServiceId;
        $commonAffiliationServiceRelatedByServiceId->setCommonServiceRelatedByServiceId($this);
    }

    /**
     * @param	CommonAffiliationServiceRelatedByServiceId $commonAffiliationServiceRelatedByServiceId The commonAffiliationServiceRelatedByServiceId object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonAffiliationServiceRelatedByServiceId($commonAffiliationServiceRelatedByServiceId)
    {
        if ($this->getCommonAffiliationServicesRelatedByServiceId()->contains($commonAffiliationServiceRelatedByServiceId)) {
            $this->collCommonAffiliationServicesRelatedByServiceId->remove($this->collCommonAffiliationServicesRelatedByServiceId->search($commonAffiliationServiceRelatedByServiceId));
            if (null === $this->commonAffiliationServicesRelatedByServiceIdScheduledForDeletion) {
                $this->commonAffiliationServicesRelatedByServiceIdScheduledForDeletion = clone $this->collCommonAffiliationServicesRelatedByServiceId;
                $this->commonAffiliationServicesRelatedByServiceIdScheduledForDeletion->clear();
            }
            $this->commonAffiliationServicesRelatedByServiceIdScheduledForDeletion[]= clone $commonAffiliationServiceRelatedByServiceId;
            $commonAffiliationServiceRelatedByServiceId->setCommonServiceRelatedByServiceId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonAffiliationServicesRelatedByServiceId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAffiliationService[] List of CommonAffiliationService objects
     */
    public function getCommonAffiliationServicesRelatedByServiceIdJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAffiliationServiceQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonAffiliationServicesRelatedByServiceId($query, $con);
    }

    /**
     * Clears out the collCommonAffiliationServicesRelatedByServiceParentId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonAffiliationServicesRelatedByServiceParentId()
     */
    public function clearCommonAffiliationServicesRelatedByServiceParentId()
    {
        $this->collCommonAffiliationServicesRelatedByServiceParentId = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAffiliationServicesRelatedByServiceParentIdPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAffiliationServicesRelatedByServiceParentId collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAffiliationServicesRelatedByServiceParentId($v = true)
    {
        $this->collCommonAffiliationServicesRelatedByServiceParentIdPartial = $v;
    }

    /**
     * Initializes the collCommonAffiliationServicesRelatedByServiceParentId collection.
     *
     * By default this just sets the collCommonAffiliationServicesRelatedByServiceParentId collection to an empty array (like clearcollCommonAffiliationServicesRelatedByServiceParentId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAffiliationServicesRelatedByServiceParentId($overrideExisting = true)
    {
        if (null !== $this->collCommonAffiliationServicesRelatedByServiceParentId && !$overrideExisting) {
            return;
        }
        $this->collCommonAffiliationServicesRelatedByServiceParentId = new PropelObjectCollection();
        $this->collCommonAffiliationServicesRelatedByServiceParentId->setModel('CommonAffiliationService');
    }

    /**
     * Gets an array of CommonAffiliationService objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAffiliationService[] List of CommonAffiliationService objects
     * @throws PropelException
     */
    public function getCommonAffiliationServicesRelatedByServiceParentId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAffiliationServicesRelatedByServiceParentIdPartial && !$this->isNew();
        if (null === $this->collCommonAffiliationServicesRelatedByServiceParentId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAffiliationServicesRelatedByServiceParentId) {
                // return empty collection
                $this->initCommonAffiliationServicesRelatedByServiceParentId();
            } else {
                $collCommonAffiliationServicesRelatedByServiceParentId = CommonAffiliationServiceQuery::create(null, $criteria)
                    ->filterByCommonServiceRelatedByServiceParentId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAffiliationServicesRelatedByServiceParentIdPartial && count($collCommonAffiliationServicesRelatedByServiceParentId)) {
                      $this->initCommonAffiliationServicesRelatedByServiceParentId(false);

                      foreach ($collCommonAffiliationServicesRelatedByServiceParentId as $obj) {
                        if (false == $this->collCommonAffiliationServicesRelatedByServiceParentId->contains($obj)) {
                          $this->collCommonAffiliationServicesRelatedByServiceParentId->append($obj);
                        }
                      }

                      $this->collCommonAffiliationServicesRelatedByServiceParentIdPartial = true;
                    }

                    $collCommonAffiliationServicesRelatedByServiceParentId->getInternalIterator()->rewind();

                    return $collCommonAffiliationServicesRelatedByServiceParentId;
                }

                if ($partial && $this->collCommonAffiliationServicesRelatedByServiceParentId) {
                    foreach ($this->collCommonAffiliationServicesRelatedByServiceParentId as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAffiliationServicesRelatedByServiceParentId[] = $obj;
                        }
                    }
                }

                $this->collCommonAffiliationServicesRelatedByServiceParentId = $collCommonAffiliationServicesRelatedByServiceParentId;
                $this->collCommonAffiliationServicesRelatedByServiceParentIdPartial = false;
            }
        }

        return $this->collCommonAffiliationServicesRelatedByServiceParentId;
    }

    /**
     * Sets a collection of CommonAffiliationServiceRelatedByServiceParentId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAffiliationServicesRelatedByServiceParentId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonAffiliationServicesRelatedByServiceParentId(PropelCollection $commonAffiliationServicesRelatedByServiceParentId, PropelPDO $con = null)
    {
        $commonAffiliationServicesRelatedByServiceParentIdToDelete = $this->getCommonAffiliationServicesRelatedByServiceParentId(new Criteria(), $con)->diff($commonAffiliationServicesRelatedByServiceParentId);


        $this->commonAffiliationServicesRelatedByServiceParentIdScheduledForDeletion = $commonAffiliationServicesRelatedByServiceParentIdToDelete;

        foreach ($commonAffiliationServicesRelatedByServiceParentIdToDelete as $commonAffiliationServiceRelatedByServiceParentIdRemoved) {
            $commonAffiliationServiceRelatedByServiceParentIdRemoved->setCommonServiceRelatedByServiceParentId(null);
        }

        $this->collCommonAffiliationServicesRelatedByServiceParentId = null;
        foreach ($commonAffiliationServicesRelatedByServiceParentId as $commonAffiliationServiceRelatedByServiceParentId) {
            $this->addCommonAffiliationServiceRelatedByServiceParentId($commonAffiliationServiceRelatedByServiceParentId);
        }

        $this->collCommonAffiliationServicesRelatedByServiceParentId = $commonAffiliationServicesRelatedByServiceParentId;
        $this->collCommonAffiliationServicesRelatedByServiceParentIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAffiliationService objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAffiliationService objects.
     * @throws PropelException
     */
    public function countCommonAffiliationServicesRelatedByServiceParentId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAffiliationServicesRelatedByServiceParentIdPartial && !$this->isNew();
        if (null === $this->collCommonAffiliationServicesRelatedByServiceParentId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAffiliationServicesRelatedByServiceParentId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAffiliationServicesRelatedByServiceParentId());
            }
            $query = CommonAffiliationServiceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonServiceRelatedByServiceParentId($this)
                ->count($con);
        }

        return count($this->collCommonAffiliationServicesRelatedByServiceParentId);
    }

    /**
     * Method called to associate a CommonAffiliationService object to this object
     * through the CommonAffiliationService foreign key attribute.
     *
     * @param   CommonAffiliationService $l CommonAffiliationService
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonAffiliationServiceRelatedByServiceParentId(CommonAffiliationService $l)
    {
        if ($this->collCommonAffiliationServicesRelatedByServiceParentId === null) {
            $this->initCommonAffiliationServicesRelatedByServiceParentId();
            $this->collCommonAffiliationServicesRelatedByServiceParentIdPartial = true;
        }
        if (!in_array($l, $this->collCommonAffiliationServicesRelatedByServiceParentId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAffiliationServiceRelatedByServiceParentId($l);
        }

        return $this;
    }

    /**
     * @param	CommonAffiliationServiceRelatedByServiceParentId $commonAffiliationServiceRelatedByServiceParentId The commonAffiliationServiceRelatedByServiceParentId object to add.
     */
    protected function doAddCommonAffiliationServiceRelatedByServiceParentId($commonAffiliationServiceRelatedByServiceParentId)
    {
        $this->collCommonAffiliationServicesRelatedByServiceParentId[]= $commonAffiliationServiceRelatedByServiceParentId;
        $commonAffiliationServiceRelatedByServiceParentId->setCommonServiceRelatedByServiceParentId($this);
    }

    /**
     * @param	CommonAffiliationServiceRelatedByServiceParentId $commonAffiliationServiceRelatedByServiceParentId The commonAffiliationServiceRelatedByServiceParentId object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonAffiliationServiceRelatedByServiceParentId($commonAffiliationServiceRelatedByServiceParentId)
    {
        if ($this->getCommonAffiliationServicesRelatedByServiceParentId()->contains($commonAffiliationServiceRelatedByServiceParentId)) {
            $this->collCommonAffiliationServicesRelatedByServiceParentId->remove($this->collCommonAffiliationServicesRelatedByServiceParentId->search($commonAffiliationServiceRelatedByServiceParentId));
            if (null === $this->commonAffiliationServicesRelatedByServiceParentIdScheduledForDeletion) {
                $this->commonAffiliationServicesRelatedByServiceParentIdScheduledForDeletion = clone $this->collCommonAffiliationServicesRelatedByServiceParentId;
                $this->commonAffiliationServicesRelatedByServiceParentIdScheduledForDeletion->clear();
            }
            $this->commonAffiliationServicesRelatedByServiceParentIdScheduledForDeletion[]= clone $commonAffiliationServiceRelatedByServiceParentId;
            $commonAffiliationServiceRelatedByServiceParentId->setCommonServiceRelatedByServiceParentId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonAffiliationServicesRelatedByServiceParentId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAffiliationService[] List of CommonAffiliationService objects
     */
    public function getCommonAffiliationServicesRelatedByServiceParentIdJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAffiliationServiceQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonAffiliationServicesRelatedByServiceParentId($query, $con);
    }

    /**
     * Clears out the collCommonAgents collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonAgents()
     */
    public function clearCommonAgents()
    {
        $this->collCommonAgents = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAgentsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAgents collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAgents($v = true)
    {
        $this->collCommonAgentsPartial = $v;
    }

    /**
     * Initializes the collCommonAgents collection.
     *
     * By default this just sets the collCommonAgents collection to an empty array (like clearcollCommonAgents());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAgents($overrideExisting = true)
    {
        if (null !== $this->collCommonAgents && !$overrideExisting) {
            return;
        }
        $this->collCommonAgents = new PropelObjectCollection();
        $this->collCommonAgents->setModel('CommonAgent');
    }

    /**
     * Gets an array of CommonAgent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAgent[] List of CommonAgent objects
     * @throws PropelException
     */
    public function getCommonAgents($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAgentsPartial && !$this->isNew();
        if (null === $this->collCommonAgents || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAgents) {
                // return empty collection
                $this->initCommonAgents();
            } else {
                $collCommonAgents = CommonAgentQuery::create(null, $criteria)
                    ->filterByCommonService($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAgentsPartial && count($collCommonAgents)) {
                      $this->initCommonAgents(false);

                      foreach ($collCommonAgents as $obj) {
                        if (false == $this->collCommonAgents->contains($obj)) {
                          $this->collCommonAgents->append($obj);
                        }
                      }

                      $this->collCommonAgentsPartial = true;
                    }

                    $collCommonAgents->getInternalIterator()->rewind();

                    return $collCommonAgents;
                }

                if ($partial && $this->collCommonAgents) {
                    foreach ($this->collCommonAgents as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAgents[] = $obj;
                        }
                    }
                }

                $this->collCommonAgents = $collCommonAgents;
                $this->collCommonAgentsPartial = false;
            }
        }

        return $this->collCommonAgents;
    }

    /**
     * Sets a collection of CommonAgent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAgents A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonAgents(PropelCollection $commonAgents, PropelPDO $con = null)
    {
        $commonAgentsToDelete = $this->getCommonAgents(new Criteria(), $con)->diff($commonAgents);


        $this->commonAgentsScheduledForDeletion = $commonAgentsToDelete;

        foreach ($commonAgentsToDelete as $commonAgentRemoved) {
            $commonAgentRemoved->setCommonService(null);
        }

        $this->collCommonAgents = null;
        foreach ($commonAgents as $commonAgent) {
            $this->addCommonAgent($commonAgent);
        }

        $this->collCommonAgents = $commonAgents;
        $this->collCommonAgentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAgent objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAgent objects.
     * @throws PropelException
     */
    public function countCommonAgents(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAgentsPartial && !$this->isNew();
        if (null === $this->collCommonAgents || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAgents) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAgents());
            }
            $query = CommonAgentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonService($this)
                ->count($con);
        }

        return count($this->collCommonAgents);
    }

    /**
     * Method called to associate a CommonAgent object to this object
     * through the CommonAgent foreign key attribute.
     *
     * @param   CommonAgent $l CommonAgent
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonAgent(CommonAgent $l)
    {
        if ($this->collCommonAgents === null) {
            $this->initCommonAgents();
            $this->collCommonAgentsPartial = true;
        }
        if (!in_array($l, $this->collCommonAgents->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAgent($l);
        }

        return $this;
    }

    /**
     * @param	CommonAgent $commonAgent The commonAgent object to add.
     */
    protected function doAddCommonAgent($commonAgent)
    {
        $this->collCommonAgents[]= $commonAgent;
        $commonAgent->setCommonService($this);
    }

    /**
     * @param	CommonAgent $commonAgent The commonAgent object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonAgent($commonAgent)
    {
        if ($this->getCommonAgents()->contains($commonAgent)) {
            $this->collCommonAgents->remove($this->collCommonAgents->search($commonAgent));
            if (null === $this->commonAgentsScheduledForDeletion) {
                $this->commonAgentsScheduledForDeletion = clone $this->collCommonAgents;
                $this->commonAgentsScheduledForDeletion->clear();
            }
            $this->commonAgentsScheduledForDeletion[]= $commonAgent;
            $commonAgent->setCommonService(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonAgents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAgent[] List of CommonAgent objects
     */
    public function getCommonAgentsJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAgentQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonAgents($query, $con);
    }

    /**
     * Clears out the collCommonEncherePmis collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonEncherePmis()
     */
    public function clearCommonEncherePmis()
    {
        $this->collCommonEncherePmis = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEncherePmisPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEncherePmis collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEncherePmis($v = true)
    {
        $this->collCommonEncherePmisPartial = $v;
    }

    /**
     * Initializes the collCommonEncherePmis collection.
     *
     * By default this just sets the collCommonEncherePmis collection to an empty array (like clearcollCommonEncherePmis());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEncherePmis($overrideExisting = true)
    {
        if (null !== $this->collCommonEncherePmis && !$overrideExisting) {
            return;
        }
        $this->collCommonEncherePmis = new PropelObjectCollection();
        $this->collCommonEncherePmis->setModel('CommonEncherePmi');
    }

    /**
     * Gets an array of CommonEncherePmi objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEncherePmi[] List of CommonEncherePmi objects
     * @throws PropelException
     */
    public function getCommonEncherePmis($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEncherePmisPartial && !$this->isNew();
        if (null === $this->collCommonEncherePmis || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEncherePmis) {
                // return empty collection
                $this->initCommonEncherePmis();
            } else {
                $collCommonEncherePmis = CommonEncherePmiQuery::create(null, $criteria)
                    ->filterByCommonService($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEncherePmisPartial && count($collCommonEncherePmis)) {
                      $this->initCommonEncherePmis(false);

                      foreach ($collCommonEncherePmis as $obj) {
                        if (false == $this->collCommonEncherePmis->contains($obj)) {
                          $this->collCommonEncherePmis->append($obj);
                        }
                      }

                      $this->collCommonEncherePmisPartial = true;
                    }

                    $collCommonEncherePmis->getInternalIterator()->rewind();

                    return $collCommonEncherePmis;
                }

                if ($partial && $this->collCommonEncherePmis) {
                    foreach ($this->collCommonEncherePmis as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEncherePmis[] = $obj;
                        }
                    }
                }

                $this->collCommonEncherePmis = $collCommonEncherePmis;
                $this->collCommonEncherePmisPartial = false;
            }
        }

        return $this->collCommonEncherePmis;
    }

    /**
     * Sets a collection of CommonEncherePmi objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEncherePmis A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonEncherePmis(PropelCollection $commonEncherePmis, PropelPDO $con = null)
    {
        $commonEncherePmisToDelete = $this->getCommonEncherePmis(new Criteria(), $con)->diff($commonEncherePmis);


        $this->commonEncherePmisScheduledForDeletion = $commonEncherePmisToDelete;

        foreach ($commonEncherePmisToDelete as $commonEncherePmiRemoved) {
            $commonEncherePmiRemoved->setCommonService(null);
        }

        $this->collCommonEncherePmis = null;
        foreach ($commonEncherePmis as $commonEncherePmi) {
            $this->addCommonEncherePmi($commonEncherePmi);
        }

        $this->collCommonEncherePmis = $commonEncherePmis;
        $this->collCommonEncherePmisPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEncherePmi objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEncherePmi objects.
     * @throws PropelException
     */
    public function countCommonEncherePmis(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEncherePmisPartial && !$this->isNew();
        if (null === $this->collCommonEncherePmis || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEncherePmis) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEncherePmis());
            }
            $query = CommonEncherePmiQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonService($this)
                ->count($con);
        }

        return count($this->collCommonEncherePmis);
    }

    /**
     * Method called to associate a CommonEncherePmi object to this object
     * through the CommonEncherePmi foreign key attribute.
     *
     * @param   CommonEncherePmi $l CommonEncherePmi
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonEncherePmi(CommonEncherePmi $l)
    {
        if ($this->collCommonEncherePmis === null) {
            $this->initCommonEncherePmis();
            $this->collCommonEncherePmisPartial = true;
        }
        if (!in_array($l, $this->collCommonEncherePmis->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEncherePmi($l);
        }

        return $this;
    }

    /**
     * @param	CommonEncherePmi $commonEncherePmi The commonEncherePmi object to add.
     */
    protected function doAddCommonEncherePmi($commonEncherePmi)
    {
        $this->collCommonEncherePmis[]= $commonEncherePmi;
        $commonEncherePmi->setCommonService($this);
    }

    /**
     * @param	CommonEncherePmi $commonEncherePmi The commonEncherePmi object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonEncherePmi($commonEncherePmi)
    {
        if ($this->getCommonEncherePmis()->contains($commonEncherePmi)) {
            $this->collCommonEncherePmis->remove($this->collCommonEncherePmis->search($commonEncherePmi));
            if (null === $this->commonEncherePmisScheduledForDeletion) {
                $this->commonEncherePmisScheduledForDeletion = clone $this->collCommonEncherePmis;
                $this->commonEncherePmisScheduledForDeletion->clear();
            }
            $this->commonEncherePmisScheduledForDeletion[]= $commonEncherePmi;
            $commonEncherePmi->setCommonService(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonNewslettersRelatedByIdServiceDestinataire collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonNewslettersRelatedByIdServiceDestinataire()
     */
    public function clearCommonNewslettersRelatedByIdServiceDestinataire()
    {
        $this->collCommonNewslettersRelatedByIdServiceDestinataire = null; // important to set this to null since that means it is uninitialized
        $this->collCommonNewslettersRelatedByIdServiceDestinatairePartial = null;

        return $this;
    }

    /**
     * reset is the collCommonNewslettersRelatedByIdServiceDestinataire collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonNewslettersRelatedByIdServiceDestinataire($v = true)
    {
        $this->collCommonNewslettersRelatedByIdServiceDestinatairePartial = $v;
    }

    /**
     * Initializes the collCommonNewslettersRelatedByIdServiceDestinataire collection.
     *
     * By default this just sets the collCommonNewslettersRelatedByIdServiceDestinataire collection to an empty array (like clearcollCommonNewslettersRelatedByIdServiceDestinataire());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonNewslettersRelatedByIdServiceDestinataire($overrideExisting = true)
    {
        if (null !== $this->collCommonNewslettersRelatedByIdServiceDestinataire && !$overrideExisting) {
            return;
        }
        $this->collCommonNewslettersRelatedByIdServiceDestinataire = new PropelObjectCollection();
        $this->collCommonNewslettersRelatedByIdServiceDestinataire->setModel('CommonNewsletter');
    }

    /**
     * Gets an array of CommonNewsletter objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonNewsletter[] List of CommonNewsletter objects
     * @throws PropelException
     */
    public function getCommonNewslettersRelatedByIdServiceDestinataire($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonNewslettersRelatedByIdServiceDestinatairePartial && !$this->isNew();
        if (null === $this->collCommonNewslettersRelatedByIdServiceDestinataire || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonNewslettersRelatedByIdServiceDestinataire) {
                // return empty collection
                $this->initCommonNewslettersRelatedByIdServiceDestinataire();
            } else {
                $collCommonNewslettersRelatedByIdServiceDestinataire = CommonNewsletterQuery::create(null, $criteria)
                    ->filterByCommonServiceRelatedByIdServiceDestinataire($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonNewslettersRelatedByIdServiceDestinatairePartial && count($collCommonNewslettersRelatedByIdServiceDestinataire)) {
                      $this->initCommonNewslettersRelatedByIdServiceDestinataire(false);

                      foreach ($collCommonNewslettersRelatedByIdServiceDestinataire as $obj) {
                        if (false == $this->collCommonNewslettersRelatedByIdServiceDestinataire->contains($obj)) {
                          $this->collCommonNewslettersRelatedByIdServiceDestinataire->append($obj);
                        }
                      }

                      $this->collCommonNewslettersRelatedByIdServiceDestinatairePartial = true;
                    }

                    $collCommonNewslettersRelatedByIdServiceDestinataire->getInternalIterator()->rewind();

                    return $collCommonNewslettersRelatedByIdServiceDestinataire;
                }

                if ($partial && $this->collCommonNewslettersRelatedByIdServiceDestinataire) {
                    foreach ($this->collCommonNewslettersRelatedByIdServiceDestinataire as $obj) {
                        if ($obj->isNew()) {
                            $collCommonNewslettersRelatedByIdServiceDestinataire[] = $obj;
                        }
                    }
                }

                $this->collCommonNewslettersRelatedByIdServiceDestinataire = $collCommonNewslettersRelatedByIdServiceDestinataire;
                $this->collCommonNewslettersRelatedByIdServiceDestinatairePartial = false;
            }
        }

        return $this->collCommonNewslettersRelatedByIdServiceDestinataire;
    }

    /**
     * Sets a collection of CommonNewsletterRelatedByIdServiceDestinataire objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonNewslettersRelatedByIdServiceDestinataire A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonNewslettersRelatedByIdServiceDestinataire(PropelCollection $commonNewslettersRelatedByIdServiceDestinataire, PropelPDO $con = null)
    {
        $commonNewslettersRelatedByIdServiceDestinataireToDelete = $this->getCommonNewslettersRelatedByIdServiceDestinataire(new Criteria(), $con)->diff($commonNewslettersRelatedByIdServiceDestinataire);


        $this->commonNewslettersRelatedByIdServiceDestinataireScheduledForDeletion = $commonNewslettersRelatedByIdServiceDestinataireToDelete;

        foreach ($commonNewslettersRelatedByIdServiceDestinataireToDelete as $commonNewsletterRelatedByIdServiceDestinataireRemoved) {
            $commonNewsletterRelatedByIdServiceDestinataireRemoved->setCommonServiceRelatedByIdServiceDestinataire(null);
        }

        $this->collCommonNewslettersRelatedByIdServiceDestinataire = null;
        foreach ($commonNewslettersRelatedByIdServiceDestinataire as $commonNewsletterRelatedByIdServiceDestinataire) {
            $this->addCommonNewsletterRelatedByIdServiceDestinataire($commonNewsletterRelatedByIdServiceDestinataire);
        }

        $this->collCommonNewslettersRelatedByIdServiceDestinataire = $commonNewslettersRelatedByIdServiceDestinataire;
        $this->collCommonNewslettersRelatedByIdServiceDestinatairePartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonNewsletter objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonNewsletter objects.
     * @throws PropelException
     */
    public function countCommonNewslettersRelatedByIdServiceDestinataire(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonNewslettersRelatedByIdServiceDestinatairePartial && !$this->isNew();
        if (null === $this->collCommonNewslettersRelatedByIdServiceDestinataire || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonNewslettersRelatedByIdServiceDestinataire) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonNewslettersRelatedByIdServiceDestinataire());
            }
            $query = CommonNewsletterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonServiceRelatedByIdServiceDestinataire($this)
                ->count($con);
        }

        return count($this->collCommonNewslettersRelatedByIdServiceDestinataire);
    }

    /**
     * Method called to associate a CommonNewsletter object to this object
     * through the CommonNewsletter foreign key attribute.
     *
     * @param   CommonNewsletter $l CommonNewsletter
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonNewsletterRelatedByIdServiceDestinataire(CommonNewsletter $l)
    {
        if ($this->collCommonNewslettersRelatedByIdServiceDestinataire === null) {
            $this->initCommonNewslettersRelatedByIdServiceDestinataire();
            $this->collCommonNewslettersRelatedByIdServiceDestinatairePartial = true;
        }
        if (!in_array($l, $this->collCommonNewslettersRelatedByIdServiceDestinataire->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonNewsletterRelatedByIdServiceDestinataire($l);
        }

        return $this;
    }

    /**
     * @param	CommonNewsletterRelatedByIdServiceDestinataire $commonNewsletterRelatedByIdServiceDestinataire The commonNewsletterRelatedByIdServiceDestinataire object to add.
     */
    protected function doAddCommonNewsletterRelatedByIdServiceDestinataire($commonNewsletterRelatedByIdServiceDestinataire)
    {
        $this->collCommonNewslettersRelatedByIdServiceDestinataire[]= $commonNewsletterRelatedByIdServiceDestinataire;
        $commonNewsletterRelatedByIdServiceDestinataire->setCommonServiceRelatedByIdServiceDestinataire($this);
    }

    /**
     * @param	CommonNewsletterRelatedByIdServiceDestinataire $commonNewsletterRelatedByIdServiceDestinataire The commonNewsletterRelatedByIdServiceDestinataire object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonNewsletterRelatedByIdServiceDestinataire($commonNewsletterRelatedByIdServiceDestinataire)
    {
        if ($this->getCommonNewslettersRelatedByIdServiceDestinataire()->contains($commonNewsletterRelatedByIdServiceDestinataire)) {
            $this->collCommonNewslettersRelatedByIdServiceDestinataire->remove($this->collCommonNewslettersRelatedByIdServiceDestinataire->search($commonNewsletterRelatedByIdServiceDestinataire));
            if (null === $this->commonNewslettersRelatedByIdServiceDestinataireScheduledForDeletion) {
                $this->commonNewslettersRelatedByIdServiceDestinataireScheduledForDeletion = clone $this->collCommonNewslettersRelatedByIdServiceDestinataire;
                $this->commonNewslettersRelatedByIdServiceDestinataireScheduledForDeletion->clear();
            }
            $this->commonNewslettersRelatedByIdServiceDestinataireScheduledForDeletion[]= $commonNewsletterRelatedByIdServiceDestinataire;
            $commonNewsletterRelatedByIdServiceDestinataire->setCommonServiceRelatedByIdServiceDestinataire(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonNewslettersRelatedByIdServiceRedacteur collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonNewslettersRelatedByIdServiceRedacteur()
     */
    public function clearCommonNewslettersRelatedByIdServiceRedacteur()
    {
        $this->collCommonNewslettersRelatedByIdServiceRedacteur = null; // important to set this to null since that means it is uninitialized
        $this->collCommonNewslettersRelatedByIdServiceRedacteurPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonNewslettersRelatedByIdServiceRedacteur collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonNewslettersRelatedByIdServiceRedacteur($v = true)
    {
        $this->collCommonNewslettersRelatedByIdServiceRedacteurPartial = $v;
    }

    /**
     * Initializes the collCommonNewslettersRelatedByIdServiceRedacteur collection.
     *
     * By default this just sets the collCommonNewslettersRelatedByIdServiceRedacteur collection to an empty array (like clearcollCommonNewslettersRelatedByIdServiceRedacteur());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonNewslettersRelatedByIdServiceRedacteur($overrideExisting = true)
    {
        if (null !== $this->collCommonNewslettersRelatedByIdServiceRedacteur && !$overrideExisting) {
            return;
        }
        $this->collCommonNewslettersRelatedByIdServiceRedacteur = new PropelObjectCollection();
        $this->collCommonNewslettersRelatedByIdServiceRedacteur->setModel('CommonNewsletter');
    }

    /**
     * Gets an array of CommonNewsletter objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonNewsletter[] List of CommonNewsletter objects
     * @throws PropelException
     */
    public function getCommonNewslettersRelatedByIdServiceRedacteur($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonNewslettersRelatedByIdServiceRedacteurPartial && !$this->isNew();
        if (null === $this->collCommonNewslettersRelatedByIdServiceRedacteur || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonNewslettersRelatedByIdServiceRedacteur) {
                // return empty collection
                $this->initCommonNewslettersRelatedByIdServiceRedacteur();
            } else {
                $collCommonNewslettersRelatedByIdServiceRedacteur = CommonNewsletterQuery::create(null, $criteria)
                    ->filterByCommonServiceRelatedByIdServiceRedacteur($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonNewslettersRelatedByIdServiceRedacteurPartial && count($collCommonNewslettersRelatedByIdServiceRedacteur)) {
                      $this->initCommonNewslettersRelatedByIdServiceRedacteur(false);

                      foreach ($collCommonNewslettersRelatedByIdServiceRedacteur as $obj) {
                        if (false == $this->collCommonNewslettersRelatedByIdServiceRedacteur->contains($obj)) {
                          $this->collCommonNewslettersRelatedByIdServiceRedacteur->append($obj);
                        }
                      }

                      $this->collCommonNewslettersRelatedByIdServiceRedacteurPartial = true;
                    }

                    $collCommonNewslettersRelatedByIdServiceRedacteur->getInternalIterator()->rewind();

                    return $collCommonNewslettersRelatedByIdServiceRedacteur;
                }

                if ($partial && $this->collCommonNewslettersRelatedByIdServiceRedacteur) {
                    foreach ($this->collCommonNewslettersRelatedByIdServiceRedacteur as $obj) {
                        if ($obj->isNew()) {
                            $collCommonNewslettersRelatedByIdServiceRedacteur[] = $obj;
                        }
                    }
                }

                $this->collCommonNewslettersRelatedByIdServiceRedacteur = $collCommonNewslettersRelatedByIdServiceRedacteur;
                $this->collCommonNewslettersRelatedByIdServiceRedacteurPartial = false;
            }
        }

        return $this->collCommonNewslettersRelatedByIdServiceRedacteur;
    }

    /**
     * Sets a collection of CommonNewsletterRelatedByIdServiceRedacteur objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonNewslettersRelatedByIdServiceRedacteur A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonNewslettersRelatedByIdServiceRedacteur(PropelCollection $commonNewslettersRelatedByIdServiceRedacteur, PropelPDO $con = null)
    {
        $commonNewslettersRelatedByIdServiceRedacteurToDelete = $this->getCommonNewslettersRelatedByIdServiceRedacteur(new Criteria(), $con)->diff($commonNewslettersRelatedByIdServiceRedacteur);


        $this->commonNewslettersRelatedByIdServiceRedacteurScheduledForDeletion = $commonNewslettersRelatedByIdServiceRedacteurToDelete;

        foreach ($commonNewslettersRelatedByIdServiceRedacteurToDelete as $commonNewsletterRelatedByIdServiceRedacteurRemoved) {
            $commonNewsletterRelatedByIdServiceRedacteurRemoved->setCommonServiceRelatedByIdServiceRedacteur(null);
        }

        $this->collCommonNewslettersRelatedByIdServiceRedacteur = null;
        foreach ($commonNewslettersRelatedByIdServiceRedacteur as $commonNewsletterRelatedByIdServiceRedacteur) {
            $this->addCommonNewsletterRelatedByIdServiceRedacteur($commonNewsletterRelatedByIdServiceRedacteur);
        }

        $this->collCommonNewslettersRelatedByIdServiceRedacteur = $commonNewslettersRelatedByIdServiceRedacteur;
        $this->collCommonNewslettersRelatedByIdServiceRedacteurPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonNewsletter objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonNewsletter objects.
     * @throws PropelException
     */
    public function countCommonNewslettersRelatedByIdServiceRedacteur(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonNewslettersRelatedByIdServiceRedacteurPartial && !$this->isNew();
        if (null === $this->collCommonNewslettersRelatedByIdServiceRedacteur || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonNewslettersRelatedByIdServiceRedacteur) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonNewslettersRelatedByIdServiceRedacteur());
            }
            $query = CommonNewsletterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonServiceRelatedByIdServiceRedacteur($this)
                ->count($con);
        }

        return count($this->collCommonNewslettersRelatedByIdServiceRedacteur);
    }

    /**
     * Method called to associate a CommonNewsletter object to this object
     * through the CommonNewsletter foreign key attribute.
     *
     * @param   CommonNewsletter $l CommonNewsletter
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonNewsletterRelatedByIdServiceRedacteur(CommonNewsletter $l)
    {
        if ($this->collCommonNewslettersRelatedByIdServiceRedacteur === null) {
            $this->initCommonNewslettersRelatedByIdServiceRedacteur();
            $this->collCommonNewslettersRelatedByIdServiceRedacteurPartial = true;
        }
        if (!in_array($l, $this->collCommonNewslettersRelatedByIdServiceRedacteur->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonNewsletterRelatedByIdServiceRedacteur($l);
        }

        return $this;
    }

    /**
     * @param	CommonNewsletterRelatedByIdServiceRedacteur $commonNewsletterRelatedByIdServiceRedacteur The commonNewsletterRelatedByIdServiceRedacteur object to add.
     */
    protected function doAddCommonNewsletterRelatedByIdServiceRedacteur($commonNewsletterRelatedByIdServiceRedacteur)
    {
        $this->collCommonNewslettersRelatedByIdServiceRedacteur[]= $commonNewsletterRelatedByIdServiceRedacteur;
        $commonNewsletterRelatedByIdServiceRedacteur->setCommonServiceRelatedByIdServiceRedacteur($this);
    }

    /**
     * @param	CommonNewsletterRelatedByIdServiceRedacteur $commonNewsletterRelatedByIdServiceRedacteur The commonNewsletterRelatedByIdServiceRedacteur object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonNewsletterRelatedByIdServiceRedacteur($commonNewsletterRelatedByIdServiceRedacteur)
    {
        if ($this->getCommonNewslettersRelatedByIdServiceRedacteur()->contains($commonNewsletterRelatedByIdServiceRedacteur)) {
            $this->collCommonNewslettersRelatedByIdServiceRedacteur->remove($this->collCommonNewslettersRelatedByIdServiceRedacteur->search($commonNewsletterRelatedByIdServiceRedacteur));
            if (null === $this->commonNewslettersRelatedByIdServiceRedacteurScheduledForDeletion) {
                $this->commonNewslettersRelatedByIdServiceRedacteurScheduledForDeletion = clone $this->collCommonNewslettersRelatedByIdServiceRedacteur;
                $this->commonNewslettersRelatedByIdServiceRedacteurScheduledForDeletion->clear();
            }
            $this->commonNewslettersRelatedByIdServiceRedacteurScheduledForDeletion[]= $commonNewsletterRelatedByIdServiceRedacteur;
            $commonNewsletterRelatedByIdServiceRedacteur->setCommonServiceRelatedByIdServiceRedacteur(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonParametrageEncheres collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonParametrageEncheres()
     */
    public function clearCommonParametrageEncheres()
    {
        $this->collCommonParametrageEncheres = null; // important to set this to null since that means it is uninitialized
        $this->collCommonParametrageEncheresPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonParametrageEncheres collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonParametrageEncheres($v = true)
    {
        $this->collCommonParametrageEncheresPartial = $v;
    }

    /**
     * Initializes the collCommonParametrageEncheres collection.
     *
     * By default this just sets the collCommonParametrageEncheres collection to an empty array (like clearcollCommonParametrageEncheres());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonParametrageEncheres($overrideExisting = true)
    {
        if (null !== $this->collCommonParametrageEncheres && !$overrideExisting) {
            return;
        }
        $this->collCommonParametrageEncheres = new PropelObjectCollection();
        $this->collCommonParametrageEncheres->setModel('CommonParametrageEnchere');
    }

    /**
     * Gets an array of CommonParametrageEnchere objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonParametrageEnchere[] List of CommonParametrageEnchere objects
     * @throws PropelException
     */
    public function getCommonParametrageEncheres($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonParametrageEncheresPartial && !$this->isNew();
        if (null === $this->collCommonParametrageEncheres || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonParametrageEncheres) {
                // return empty collection
                $this->initCommonParametrageEncheres();
            } else {
                $collCommonParametrageEncheres = CommonParametrageEnchereQuery::create(null, $criteria)
                    ->filterByCommonService($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonParametrageEncheresPartial && count($collCommonParametrageEncheres)) {
                      $this->initCommonParametrageEncheres(false);

                      foreach ($collCommonParametrageEncheres as $obj) {
                        if (false == $this->collCommonParametrageEncheres->contains($obj)) {
                          $this->collCommonParametrageEncheres->append($obj);
                        }
                      }

                      $this->collCommonParametrageEncheresPartial = true;
                    }

                    $collCommonParametrageEncheres->getInternalIterator()->rewind();

                    return $collCommonParametrageEncheres;
                }

                if ($partial && $this->collCommonParametrageEncheres) {
                    foreach ($this->collCommonParametrageEncheres as $obj) {
                        if ($obj->isNew()) {
                            $collCommonParametrageEncheres[] = $obj;
                        }
                    }
                }

                $this->collCommonParametrageEncheres = $collCommonParametrageEncheres;
                $this->collCommonParametrageEncheresPartial = false;
            }
        }

        return $this->collCommonParametrageEncheres;
    }

    /**
     * Sets a collection of CommonParametrageEnchere objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonParametrageEncheres A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonParametrageEncheres(PropelCollection $commonParametrageEncheres, PropelPDO $con = null)
    {
        $commonParametrageEncheresToDelete = $this->getCommonParametrageEncheres(new Criteria(), $con)->diff($commonParametrageEncheres);


        $this->commonParametrageEncheresScheduledForDeletion = $commonParametrageEncheresToDelete;

        foreach ($commonParametrageEncheresToDelete as $commonParametrageEnchereRemoved) {
            $commonParametrageEnchereRemoved->setCommonService(null);
        }

        $this->collCommonParametrageEncheres = null;
        foreach ($commonParametrageEncheres as $commonParametrageEnchere) {
            $this->addCommonParametrageEnchere($commonParametrageEnchere);
        }

        $this->collCommonParametrageEncheres = $commonParametrageEncheres;
        $this->collCommonParametrageEncheresPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonParametrageEnchere objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonParametrageEnchere objects.
     * @throws PropelException
     */
    public function countCommonParametrageEncheres(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonParametrageEncheresPartial && !$this->isNew();
        if (null === $this->collCommonParametrageEncheres || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonParametrageEncheres) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonParametrageEncheres());
            }
            $query = CommonParametrageEnchereQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonService($this)
                ->count($con);
        }

        return count($this->collCommonParametrageEncheres);
    }

    /**
     * Method called to associate a CommonParametrageEnchere object to this object
     * through the CommonParametrageEnchere foreign key attribute.
     *
     * @param   CommonParametrageEnchere $l CommonParametrageEnchere
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonParametrageEnchere(CommonParametrageEnchere $l)
    {
        if ($this->collCommonParametrageEncheres === null) {
            $this->initCommonParametrageEncheres();
            $this->collCommonParametrageEncheresPartial = true;
        }
        if (!in_array($l, $this->collCommonParametrageEncheres->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonParametrageEnchere($l);
        }

        return $this;
    }

    /**
     * @param	CommonParametrageEnchere $commonParametrageEnchere The commonParametrageEnchere object to add.
     */
    protected function doAddCommonParametrageEnchere($commonParametrageEnchere)
    {
        $this->collCommonParametrageEncheres[]= $commonParametrageEnchere;
        $commonParametrageEnchere->setCommonService($this);
    }

    /**
     * @param	CommonParametrageEnchere $commonParametrageEnchere The commonParametrageEnchere object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonParametrageEnchere($commonParametrageEnchere)
    {
        if ($this->getCommonParametrageEncheres()->contains($commonParametrageEnchere)) {
            $this->collCommonParametrageEncheres->remove($this->collCommonParametrageEncheres->search($commonParametrageEnchere));
            if (null === $this->commonParametrageEncheresScheduledForDeletion) {
                $this->commonParametrageEncheresScheduledForDeletion = clone $this->collCommonParametrageEncheres;
                $this->commonParametrageEncheresScheduledForDeletion->clear();
            }
            $this->commonParametrageEncheresScheduledForDeletion[]= $commonParametrageEnchere;
            $commonParametrageEnchere->setCommonService(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonParametrageEncheres from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonParametrageEnchere[] List of CommonParametrageEnchere objects
     */
    public function getCommonParametrageEncheresJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonParametrageEnchereQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonParametrageEncheres($query, $con);
    }

    /**
     * Clears out the collCommonTTelechargementAsynchrones collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonTTelechargementAsynchrones()
     */
    public function clearCommonTTelechargementAsynchrones()
    {
        $this->collCommonTTelechargementAsynchrones = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTTelechargementAsynchronesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTTelechargementAsynchrones collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTTelechargementAsynchrones($v = true)
    {
        $this->collCommonTTelechargementAsynchronesPartial = $v;
    }

    /**
     * Initializes the collCommonTTelechargementAsynchrones collection.
     *
     * By default this just sets the collCommonTTelechargementAsynchrones collection to an empty array (like clearcollCommonTTelechargementAsynchrones());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTTelechargementAsynchrones($overrideExisting = true)
    {
        if (null !== $this->collCommonTTelechargementAsynchrones && !$overrideExisting) {
            return;
        }
        $this->collCommonTTelechargementAsynchrones = new PropelObjectCollection();
        $this->collCommonTTelechargementAsynchrones->setModel('CommonTTelechargementAsynchrone');
    }

    /**
     * Gets an array of CommonTTelechargementAsynchrone objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTTelechargementAsynchrone[] List of CommonTTelechargementAsynchrone objects
     * @throws PropelException
     */
    public function getCommonTTelechargementAsynchrones($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTTelechargementAsynchronesPartial && !$this->isNew();
        if (null === $this->collCommonTTelechargementAsynchrones || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTTelechargementAsynchrones) {
                // return empty collection
                $this->initCommonTTelechargementAsynchrones();
            } else {
                $collCommonTTelechargementAsynchrones = CommonTTelechargementAsynchroneQuery::create(null, $criteria)
                    ->filterByCommonService($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTTelechargementAsynchronesPartial && count($collCommonTTelechargementAsynchrones)) {
                      $this->initCommonTTelechargementAsynchrones(false);

                      foreach ($collCommonTTelechargementAsynchrones as $obj) {
                        if (false == $this->collCommonTTelechargementAsynchrones->contains($obj)) {
                          $this->collCommonTTelechargementAsynchrones->append($obj);
                        }
                      }

                      $this->collCommonTTelechargementAsynchronesPartial = true;
                    }

                    $collCommonTTelechargementAsynchrones->getInternalIterator()->rewind();

                    return $collCommonTTelechargementAsynchrones;
                }

                if ($partial && $this->collCommonTTelechargementAsynchrones) {
                    foreach ($this->collCommonTTelechargementAsynchrones as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTTelechargementAsynchrones[] = $obj;
                        }
                    }
                }

                $this->collCommonTTelechargementAsynchrones = $collCommonTTelechargementAsynchrones;
                $this->collCommonTTelechargementAsynchronesPartial = false;
            }
        }

        return $this->collCommonTTelechargementAsynchrones;
    }

    /**
     * Sets a collection of CommonTTelechargementAsynchrone objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTTelechargementAsynchrones A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonTTelechargementAsynchrones(PropelCollection $commonTTelechargementAsynchrones, PropelPDO $con = null)
    {
        $commonTTelechargementAsynchronesToDelete = $this->getCommonTTelechargementAsynchrones(new Criteria(), $con)->diff($commonTTelechargementAsynchrones);


        $this->commonTTelechargementAsynchronesScheduledForDeletion = $commonTTelechargementAsynchronesToDelete;

        foreach ($commonTTelechargementAsynchronesToDelete as $commonTTelechargementAsynchroneRemoved) {
            $commonTTelechargementAsynchroneRemoved->setCommonService(null);
        }

        $this->collCommonTTelechargementAsynchrones = null;
        foreach ($commonTTelechargementAsynchrones as $commonTTelechargementAsynchrone) {
            $this->addCommonTTelechargementAsynchrone($commonTTelechargementAsynchrone);
        }

        $this->collCommonTTelechargementAsynchrones = $commonTTelechargementAsynchrones;
        $this->collCommonTTelechargementAsynchronesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTTelechargementAsynchrone objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTTelechargementAsynchrone objects.
     * @throws PropelException
     */
    public function countCommonTTelechargementAsynchrones(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTTelechargementAsynchronesPartial && !$this->isNew();
        if (null === $this->collCommonTTelechargementAsynchrones || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTTelechargementAsynchrones) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTTelechargementAsynchrones());
            }
            $query = CommonTTelechargementAsynchroneQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonService($this)
                ->count($con);
        }

        return count($this->collCommonTTelechargementAsynchrones);
    }

    /**
     * Method called to associate a CommonTTelechargementAsynchrone object to this object
     * through the CommonTTelechargementAsynchrone foreign key attribute.
     *
     * @param   CommonTTelechargementAsynchrone $l CommonTTelechargementAsynchrone
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonTTelechargementAsynchrone(CommonTTelechargementAsynchrone $l)
    {
        if ($this->collCommonTTelechargementAsynchrones === null) {
            $this->initCommonTTelechargementAsynchrones();
            $this->collCommonTTelechargementAsynchronesPartial = true;
        }
        if (!in_array($l, $this->collCommonTTelechargementAsynchrones->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTTelechargementAsynchrone($l);
        }

        return $this;
    }

    /**
     * @param	CommonTTelechargementAsynchrone $commonTTelechargementAsynchrone The commonTTelechargementAsynchrone object to add.
     */
    protected function doAddCommonTTelechargementAsynchrone($commonTTelechargementAsynchrone)
    {
        $this->collCommonTTelechargementAsynchrones[]= $commonTTelechargementAsynchrone;
        $commonTTelechargementAsynchrone->setCommonService($this);
    }

    /**
     * @param	CommonTTelechargementAsynchrone $commonTTelechargementAsynchrone The commonTTelechargementAsynchrone object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonTTelechargementAsynchrone($commonTTelechargementAsynchrone)
    {
        if ($this->getCommonTTelechargementAsynchrones()->contains($commonTTelechargementAsynchrone)) {
            $this->collCommonTTelechargementAsynchrones->remove($this->collCommonTTelechargementAsynchrones->search($commonTTelechargementAsynchrone));
            if (null === $this->commonTTelechargementAsynchronesScheduledForDeletion) {
                $this->commonTTelechargementAsynchronesScheduledForDeletion = clone $this->collCommonTTelechargementAsynchrones;
                $this->commonTTelechargementAsynchronesScheduledForDeletion->clear();
            }
            $this->commonTTelechargementAsynchronesScheduledForDeletion[]= $commonTTelechargementAsynchrone;
            $commonTTelechargementAsynchrone->setCommonService(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTypeProcedureOrganismes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonTypeProcedureOrganismes()
     */
    public function clearCommonTypeProcedureOrganismes()
    {
        $this->collCommonTypeProcedureOrganismes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTypeProcedureOrganismesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTypeProcedureOrganismes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTypeProcedureOrganismes($v = true)
    {
        $this->collCommonTypeProcedureOrganismesPartial = $v;
    }

    /**
     * Initializes the collCommonTypeProcedureOrganismes collection.
     *
     * By default this just sets the collCommonTypeProcedureOrganismes collection to an empty array (like clearcollCommonTypeProcedureOrganismes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTypeProcedureOrganismes($overrideExisting = true)
    {
        if (null !== $this->collCommonTypeProcedureOrganismes && !$overrideExisting) {
            return;
        }
        $this->collCommonTypeProcedureOrganismes = new PropelObjectCollection();
        $this->collCommonTypeProcedureOrganismes->setModel('CommonTypeProcedureOrganisme');
    }

    /**
     * Gets an array of CommonTypeProcedureOrganisme objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTypeProcedureOrganisme[] List of CommonTypeProcedureOrganisme objects
     * @throws PropelException
     */
    public function getCommonTypeProcedureOrganismes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTypeProcedureOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonTypeProcedureOrganismes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTypeProcedureOrganismes) {
                // return empty collection
                $this->initCommonTypeProcedureOrganismes();
            } else {
                $collCommonTypeProcedureOrganismes = CommonTypeProcedureOrganismeQuery::create(null, $criteria)
                    ->filterByCommonService($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTypeProcedureOrganismesPartial && count($collCommonTypeProcedureOrganismes)) {
                      $this->initCommonTypeProcedureOrganismes(false);

                      foreach ($collCommonTypeProcedureOrganismes as $obj) {
                        if (false == $this->collCommonTypeProcedureOrganismes->contains($obj)) {
                          $this->collCommonTypeProcedureOrganismes->append($obj);
                        }
                      }

                      $this->collCommonTypeProcedureOrganismesPartial = true;
                    }

                    $collCommonTypeProcedureOrganismes->getInternalIterator()->rewind();

                    return $collCommonTypeProcedureOrganismes;
                }

                if ($partial && $this->collCommonTypeProcedureOrganismes) {
                    foreach ($this->collCommonTypeProcedureOrganismes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTypeProcedureOrganismes[] = $obj;
                        }
                    }
                }

                $this->collCommonTypeProcedureOrganismes = $collCommonTypeProcedureOrganismes;
                $this->collCommonTypeProcedureOrganismesPartial = false;
            }
        }

        return $this->collCommonTypeProcedureOrganismes;
    }

    /**
     * Sets a collection of CommonTypeProcedureOrganisme objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTypeProcedureOrganismes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonTypeProcedureOrganismes(PropelCollection $commonTypeProcedureOrganismes, PropelPDO $con = null)
    {
        $commonTypeProcedureOrganismesToDelete = $this->getCommonTypeProcedureOrganismes(new Criteria(), $con)->diff($commonTypeProcedureOrganismes);


        $this->commonTypeProcedureOrganismesScheduledForDeletion = $commonTypeProcedureOrganismesToDelete;

        foreach ($commonTypeProcedureOrganismesToDelete as $commonTypeProcedureOrganismeRemoved) {
            $commonTypeProcedureOrganismeRemoved->setCommonService(null);
        }

        $this->collCommonTypeProcedureOrganismes = null;
        foreach ($commonTypeProcedureOrganismes as $commonTypeProcedureOrganisme) {
            $this->addCommonTypeProcedureOrganisme($commonTypeProcedureOrganisme);
        }

        $this->collCommonTypeProcedureOrganismes = $commonTypeProcedureOrganismes;
        $this->collCommonTypeProcedureOrganismesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTypeProcedureOrganisme objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTypeProcedureOrganisme objects.
     * @throws PropelException
     */
    public function countCommonTypeProcedureOrganismes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTypeProcedureOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonTypeProcedureOrganismes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTypeProcedureOrganismes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTypeProcedureOrganismes());
            }
            $query = CommonTypeProcedureOrganismeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonService($this)
                ->count($con);
        }

        return count($this->collCommonTypeProcedureOrganismes);
    }

    /**
     * Method called to associate a CommonTypeProcedureOrganisme object to this object
     * through the CommonTypeProcedureOrganisme foreign key attribute.
     *
     * @param   CommonTypeProcedureOrganisme $l CommonTypeProcedureOrganisme
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonTypeProcedureOrganisme(CommonTypeProcedureOrganisme $l)
    {
        if ($this->collCommonTypeProcedureOrganismes === null) {
            $this->initCommonTypeProcedureOrganismes();
            $this->collCommonTypeProcedureOrganismesPartial = true;
        }
        if (!in_array($l, $this->collCommonTypeProcedureOrganismes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTypeProcedureOrganisme($l);
        }

        return $this;
    }

    /**
     * @param	CommonTypeProcedureOrganisme $commonTypeProcedureOrganisme The commonTypeProcedureOrganisme object to add.
     */
    protected function doAddCommonTypeProcedureOrganisme($commonTypeProcedureOrganisme)
    {
        $this->collCommonTypeProcedureOrganismes[]= $commonTypeProcedureOrganisme;
        $commonTypeProcedureOrganisme->setCommonService($this);
    }

    /**
     * @param	CommonTypeProcedureOrganisme $commonTypeProcedureOrganisme The commonTypeProcedureOrganisme object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonTypeProcedureOrganisme($commonTypeProcedureOrganisme)
    {
        if ($this->getCommonTypeProcedureOrganismes()->contains($commonTypeProcedureOrganisme)) {
            $this->collCommonTypeProcedureOrganismes->remove($this->collCommonTypeProcedureOrganismes->search($commonTypeProcedureOrganisme));
            if (null === $this->commonTypeProcedureOrganismesScheduledForDeletion) {
                $this->commonTypeProcedureOrganismesScheduledForDeletion = clone $this->collCommonTypeProcedureOrganismes;
                $this->commonTypeProcedureOrganismesScheduledForDeletion->clear();
            }
            $this->commonTypeProcedureOrganismesScheduledForDeletion[]= $commonTypeProcedureOrganisme;
            $commonTypeProcedureOrganisme->setCommonService(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonAgentTechniqueAssociations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonAgentTechniqueAssociations()
     */
    public function clearCommonAgentTechniqueAssociations()
    {
        $this->collCommonAgentTechniqueAssociations = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAgentTechniqueAssociationsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAgentTechniqueAssociations collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAgentTechniqueAssociations($v = true)
    {
        $this->collCommonAgentTechniqueAssociationsPartial = $v;
    }

    /**
     * Initializes the collCommonAgentTechniqueAssociations collection.
     *
     * By default this just sets the collCommonAgentTechniqueAssociations collection to an empty array (like clearcollCommonAgentTechniqueAssociations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAgentTechniqueAssociations($overrideExisting = true)
    {
        if (null !== $this->collCommonAgentTechniqueAssociations && !$overrideExisting) {
            return;
        }
        $this->collCommonAgentTechniqueAssociations = new PropelObjectCollection();
        $this->collCommonAgentTechniqueAssociations->setModel('CommonAgentTechniqueAssociation');
    }

    /**
     * Gets an array of CommonAgentTechniqueAssociation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAgentTechniqueAssociation[] List of CommonAgentTechniqueAssociation objects
     * @throws PropelException
     */
    public function getCommonAgentTechniqueAssociations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAgentTechniqueAssociationsPartial && !$this->isNew();
        if (null === $this->collCommonAgentTechniqueAssociations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAgentTechniqueAssociations) {
                // return empty collection
                $this->initCommonAgentTechniqueAssociations();
            } else {
                $collCommonAgentTechniqueAssociations = CommonAgentTechniqueAssociationQuery::create(null, $criteria)
                    ->filterByCommonService($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAgentTechniqueAssociationsPartial && count($collCommonAgentTechniqueAssociations)) {
                      $this->initCommonAgentTechniqueAssociations(false);

                      foreach ($collCommonAgentTechniqueAssociations as $obj) {
                        if (false == $this->collCommonAgentTechniqueAssociations->contains($obj)) {
                          $this->collCommonAgentTechniqueAssociations->append($obj);
                        }
                      }

                      $this->collCommonAgentTechniqueAssociationsPartial = true;
                    }

                    $collCommonAgentTechniqueAssociations->getInternalIterator()->rewind();

                    return $collCommonAgentTechniqueAssociations;
                }

                if ($partial && $this->collCommonAgentTechniqueAssociations) {
                    foreach ($this->collCommonAgentTechniqueAssociations as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAgentTechniqueAssociations[] = $obj;
                        }
                    }
                }

                $this->collCommonAgentTechniqueAssociations = $collCommonAgentTechniqueAssociations;
                $this->collCommonAgentTechniqueAssociationsPartial = false;
            }
        }

        return $this->collCommonAgentTechniqueAssociations;
    }

    /**
     * Sets a collection of CommonAgentTechniqueAssociation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAgentTechniqueAssociations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonAgentTechniqueAssociations(PropelCollection $commonAgentTechniqueAssociations, PropelPDO $con = null)
    {
        $commonAgentTechniqueAssociationsToDelete = $this->getCommonAgentTechniqueAssociations(new Criteria(), $con)->diff($commonAgentTechniqueAssociations);


        $this->commonAgentTechniqueAssociationsScheduledForDeletion = $commonAgentTechniqueAssociationsToDelete;

        foreach ($commonAgentTechniqueAssociationsToDelete as $commonAgentTechniqueAssociationRemoved) {
            $commonAgentTechniqueAssociationRemoved->setCommonService(null);
        }

        $this->collCommonAgentTechniqueAssociations = null;
        foreach ($commonAgentTechniqueAssociations as $commonAgentTechniqueAssociation) {
            $this->addCommonAgentTechniqueAssociation($commonAgentTechniqueAssociation);
        }

        $this->collCommonAgentTechniqueAssociations = $commonAgentTechniqueAssociations;
        $this->collCommonAgentTechniqueAssociationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAgentTechniqueAssociation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAgentTechniqueAssociation objects.
     * @throws PropelException
     */
    public function countCommonAgentTechniqueAssociations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAgentTechniqueAssociationsPartial && !$this->isNew();
        if (null === $this->collCommonAgentTechniqueAssociations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAgentTechniqueAssociations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAgentTechniqueAssociations());
            }
            $query = CommonAgentTechniqueAssociationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonService($this)
                ->count($con);
        }

        return count($this->collCommonAgentTechniqueAssociations);
    }

    /**
     * Method called to associate a CommonAgentTechniqueAssociation object to this object
     * through the CommonAgentTechniqueAssociation foreign key attribute.
     *
     * @param   CommonAgentTechniqueAssociation $l CommonAgentTechniqueAssociation
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonAgentTechniqueAssociation(CommonAgentTechniqueAssociation $l)
    {
        if ($this->collCommonAgentTechniqueAssociations === null) {
            $this->initCommonAgentTechniqueAssociations();
            $this->collCommonAgentTechniqueAssociationsPartial = true;
        }
        if (!in_array($l, $this->collCommonAgentTechniqueAssociations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAgentTechniqueAssociation($l);
        }

        return $this;
    }

    /**
     * @param	CommonAgentTechniqueAssociation $commonAgentTechniqueAssociation The commonAgentTechniqueAssociation object to add.
     */
    protected function doAddCommonAgentTechniqueAssociation($commonAgentTechniqueAssociation)
    {
        $this->collCommonAgentTechniqueAssociations[]= $commonAgentTechniqueAssociation;
        $commonAgentTechniqueAssociation->setCommonService($this);
    }

    /**
     * @param	CommonAgentTechniqueAssociation $commonAgentTechniqueAssociation The commonAgentTechniqueAssociation object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonAgentTechniqueAssociation($commonAgentTechniqueAssociation)
    {
        if ($this->getCommonAgentTechniqueAssociations()->contains($commonAgentTechniqueAssociation)) {
            $this->collCommonAgentTechniqueAssociations->remove($this->collCommonAgentTechniqueAssociations->search($commonAgentTechniqueAssociation));
            if (null === $this->commonAgentTechniqueAssociationsScheduledForDeletion) {
                $this->commonAgentTechniqueAssociationsScheduledForDeletion = clone $this->collCommonAgentTechniqueAssociations;
                $this->commonAgentTechniqueAssociationsScheduledForDeletion->clear();
            }
            $this->commonAgentTechniqueAssociationsScheduledForDeletion[]= $commonAgentTechniqueAssociation;
            $commonAgentTechniqueAssociation->setCommonService(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonConsultationsRelatedByServiceValidation collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonConsultationsRelatedByServiceValidation()
     */
    public function clearCommonConsultationsRelatedByServiceValidation()
    {
        $this->collCommonConsultationsRelatedByServiceValidation = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationsRelatedByServiceValidationPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultationsRelatedByServiceValidation collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultationsRelatedByServiceValidation($v = true)
    {
        $this->collCommonConsultationsRelatedByServiceValidationPartial = $v;
    }

    /**
     * Initializes the collCommonConsultationsRelatedByServiceValidation collection.
     *
     * By default this just sets the collCommonConsultationsRelatedByServiceValidation collection to an empty array (like clearcollCommonConsultationsRelatedByServiceValidation());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultationsRelatedByServiceValidation($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultationsRelatedByServiceValidation && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultationsRelatedByServiceValidation = new PropelObjectCollection();
        $this->collCommonConsultationsRelatedByServiceValidation->setModel('CommonConsultation');
    }

    /**
     * Gets an array of CommonConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     * @throws PropelException
     */
    public function getCommonConsultationsRelatedByServiceValidation($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsRelatedByServiceValidationPartial && !$this->isNew();
        if (null === $this->collCommonConsultationsRelatedByServiceValidation || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationsRelatedByServiceValidation) {
                // return empty collection
                $this->initCommonConsultationsRelatedByServiceValidation();
            } else {
                $collCommonConsultationsRelatedByServiceValidation = CommonConsultationQuery::create(null, $criteria)
                    ->filterByCommonServiceRelatedByServiceValidation($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationsRelatedByServiceValidationPartial && count($collCommonConsultationsRelatedByServiceValidation)) {
                      $this->initCommonConsultationsRelatedByServiceValidation(false);

                      foreach ($collCommonConsultationsRelatedByServiceValidation as $obj) {
                        if (false == $this->collCommonConsultationsRelatedByServiceValidation->contains($obj)) {
                          $this->collCommonConsultationsRelatedByServiceValidation->append($obj);
                        }
                      }

                      $this->collCommonConsultationsRelatedByServiceValidationPartial = true;
                    }

                    $collCommonConsultationsRelatedByServiceValidation->getInternalIterator()->rewind();

                    return $collCommonConsultationsRelatedByServiceValidation;
                }

                if ($partial && $this->collCommonConsultationsRelatedByServiceValidation) {
                    foreach ($this->collCommonConsultationsRelatedByServiceValidation as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultationsRelatedByServiceValidation[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultationsRelatedByServiceValidation = $collCommonConsultationsRelatedByServiceValidation;
                $this->collCommonConsultationsRelatedByServiceValidationPartial = false;
            }
        }

        return $this->collCommonConsultationsRelatedByServiceValidation;
    }

    /**
     * Sets a collection of CommonConsultationRelatedByServiceValidation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultationsRelatedByServiceValidation A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonConsultationsRelatedByServiceValidation(PropelCollection $commonConsultationsRelatedByServiceValidation, PropelPDO $con = null)
    {
        $commonConsultationsRelatedByServiceValidationToDelete = $this->getCommonConsultationsRelatedByServiceValidation(new Criteria(), $con)->diff($commonConsultationsRelatedByServiceValidation);


        $this->commonConsultationsRelatedByServiceValidationScheduledForDeletion = $commonConsultationsRelatedByServiceValidationToDelete;

        foreach ($commonConsultationsRelatedByServiceValidationToDelete as $commonConsultationRelatedByServiceValidationRemoved) {
            $commonConsultationRelatedByServiceValidationRemoved->setCommonServiceRelatedByServiceValidation(null);
        }

        $this->collCommonConsultationsRelatedByServiceValidation = null;
        foreach ($commonConsultationsRelatedByServiceValidation as $commonConsultationRelatedByServiceValidation) {
            $this->addCommonConsultationRelatedByServiceValidation($commonConsultationRelatedByServiceValidation);
        }

        $this->collCommonConsultationsRelatedByServiceValidation = $commonConsultationsRelatedByServiceValidation;
        $this->collCommonConsultationsRelatedByServiceValidationPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultation objects.
     * @throws PropelException
     */
    public function countCommonConsultationsRelatedByServiceValidation(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsRelatedByServiceValidationPartial && !$this->isNew();
        if (null === $this->collCommonConsultationsRelatedByServiceValidation || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationsRelatedByServiceValidation) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultationsRelatedByServiceValidation());
            }
            $query = CommonConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonServiceRelatedByServiceValidation($this)
                ->count($con);
        }

        return count($this->collCommonConsultationsRelatedByServiceValidation);
    }

    /**
     * Method called to associate a CommonConsultation object to this object
     * through the CommonConsultation foreign key attribute.
     *
     * @param   CommonConsultation $l CommonConsultation
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonConsultationRelatedByServiceValidation(CommonConsultation $l)
    {
        if ($this->collCommonConsultationsRelatedByServiceValidation === null) {
            $this->initCommonConsultationsRelatedByServiceValidation();
            $this->collCommonConsultationsRelatedByServiceValidationPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultationsRelatedByServiceValidation->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultationRelatedByServiceValidation($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultationRelatedByServiceValidation $commonConsultationRelatedByServiceValidation The commonConsultationRelatedByServiceValidation object to add.
     */
    protected function doAddCommonConsultationRelatedByServiceValidation($commonConsultationRelatedByServiceValidation)
    {
        $this->collCommonConsultationsRelatedByServiceValidation[]= $commonConsultationRelatedByServiceValidation;
        $commonConsultationRelatedByServiceValidation->setCommonServiceRelatedByServiceValidation($this);
    }

    /**
     * @param	CommonConsultationRelatedByServiceValidation $commonConsultationRelatedByServiceValidation The commonConsultationRelatedByServiceValidation object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonConsultationRelatedByServiceValidation($commonConsultationRelatedByServiceValidation)
    {
        if ($this->getCommonConsultationsRelatedByServiceValidation()->contains($commonConsultationRelatedByServiceValidation)) {
            $this->collCommonConsultationsRelatedByServiceValidation->remove($this->collCommonConsultationsRelatedByServiceValidation->search($commonConsultationRelatedByServiceValidation));
            if (null === $this->commonConsultationsRelatedByServiceValidationScheduledForDeletion) {
                $this->commonConsultationsRelatedByServiceValidationScheduledForDeletion = clone $this->collCommonConsultationsRelatedByServiceValidation;
                $this->commonConsultationsRelatedByServiceValidationScheduledForDeletion->clear();
            }
            $this->commonConsultationsRelatedByServiceValidationScheduledForDeletion[]= $commonConsultationRelatedByServiceValidation;
            $commonConsultationRelatedByServiceValidation->setCommonServiceRelatedByServiceValidation(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidation from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationJoinCommonTTypeContrat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonTTypeContrat', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidation($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidation from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationJoinCommonConsultationRelatedByReferenceConsultationInit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonConsultationRelatedByReferenceConsultationInit', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidation($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidation from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationJoinCommonOperations($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOperations', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidation($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidation from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidation($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidation from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidation($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidation from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationJoinCommonDossierVolumineux($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonDossierVolumineux', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidation($query, $con);
    }

    /**
     * Clears out the collCommonConsultationsRelatedByServiceId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonConsultationsRelatedByServiceId()
     */
    public function clearCommonConsultationsRelatedByServiceId()
    {
        $this->collCommonConsultationsRelatedByServiceId = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationsRelatedByServiceIdPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultationsRelatedByServiceId collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultationsRelatedByServiceId($v = true)
    {
        $this->collCommonConsultationsRelatedByServiceIdPartial = $v;
    }

    /**
     * Initializes the collCommonConsultationsRelatedByServiceId collection.
     *
     * By default this just sets the collCommonConsultationsRelatedByServiceId collection to an empty array (like clearcollCommonConsultationsRelatedByServiceId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultationsRelatedByServiceId($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultationsRelatedByServiceId && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultationsRelatedByServiceId = new PropelObjectCollection();
        $this->collCommonConsultationsRelatedByServiceId->setModel('CommonConsultation');
    }

    /**
     * Gets an array of CommonConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     * @throws PropelException
     */
    public function getCommonConsultationsRelatedByServiceId($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsRelatedByServiceIdPartial && !$this->isNew();
        if (null === $this->collCommonConsultationsRelatedByServiceId || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationsRelatedByServiceId) {
                // return empty collection
                $this->initCommonConsultationsRelatedByServiceId();
            } else {
                $collCommonConsultationsRelatedByServiceId = CommonConsultationQuery::create(null, $criteria)
                    ->filterByCommonServiceRelatedByServiceId($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationsRelatedByServiceIdPartial && count($collCommonConsultationsRelatedByServiceId)) {
                      $this->initCommonConsultationsRelatedByServiceId(false);

                      foreach ($collCommonConsultationsRelatedByServiceId as $obj) {
                        if (false == $this->collCommonConsultationsRelatedByServiceId->contains($obj)) {
                          $this->collCommonConsultationsRelatedByServiceId->append($obj);
                        }
                      }

                      $this->collCommonConsultationsRelatedByServiceIdPartial = true;
                    }

                    $collCommonConsultationsRelatedByServiceId->getInternalIterator()->rewind();

                    return $collCommonConsultationsRelatedByServiceId;
                }

                if ($partial && $this->collCommonConsultationsRelatedByServiceId) {
                    foreach ($this->collCommonConsultationsRelatedByServiceId as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultationsRelatedByServiceId[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultationsRelatedByServiceId = $collCommonConsultationsRelatedByServiceId;
                $this->collCommonConsultationsRelatedByServiceIdPartial = false;
            }
        }

        return $this->collCommonConsultationsRelatedByServiceId;
    }

    /**
     * Sets a collection of CommonConsultationRelatedByServiceId objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultationsRelatedByServiceId A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonConsultationsRelatedByServiceId(PropelCollection $commonConsultationsRelatedByServiceId, PropelPDO $con = null)
    {
        $commonConsultationsRelatedByServiceIdToDelete = $this->getCommonConsultationsRelatedByServiceId(new Criteria(), $con)->diff($commonConsultationsRelatedByServiceId);


        $this->commonConsultationsRelatedByServiceIdScheduledForDeletion = $commonConsultationsRelatedByServiceIdToDelete;

        foreach ($commonConsultationsRelatedByServiceIdToDelete as $commonConsultationRelatedByServiceIdRemoved) {
            $commonConsultationRelatedByServiceIdRemoved->setCommonServiceRelatedByServiceId(null);
        }

        $this->collCommonConsultationsRelatedByServiceId = null;
        foreach ($commonConsultationsRelatedByServiceId as $commonConsultationRelatedByServiceId) {
            $this->addCommonConsultationRelatedByServiceId($commonConsultationRelatedByServiceId);
        }

        $this->collCommonConsultationsRelatedByServiceId = $commonConsultationsRelatedByServiceId;
        $this->collCommonConsultationsRelatedByServiceIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultation objects.
     * @throws PropelException
     */
    public function countCommonConsultationsRelatedByServiceId(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsRelatedByServiceIdPartial && !$this->isNew();
        if (null === $this->collCommonConsultationsRelatedByServiceId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationsRelatedByServiceId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultationsRelatedByServiceId());
            }
            $query = CommonConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonServiceRelatedByServiceId($this)
                ->count($con);
        }

        return count($this->collCommonConsultationsRelatedByServiceId);
    }

    /**
     * Method called to associate a CommonConsultation object to this object
     * through the CommonConsultation foreign key attribute.
     *
     * @param   CommonConsultation $l CommonConsultation
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonConsultationRelatedByServiceId(CommonConsultation $l)
    {
        if ($this->collCommonConsultationsRelatedByServiceId === null) {
            $this->initCommonConsultationsRelatedByServiceId();
            $this->collCommonConsultationsRelatedByServiceIdPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultationsRelatedByServiceId->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultationRelatedByServiceId($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultationRelatedByServiceId $commonConsultationRelatedByServiceId The commonConsultationRelatedByServiceId object to add.
     */
    protected function doAddCommonConsultationRelatedByServiceId($commonConsultationRelatedByServiceId)
    {
        $this->collCommonConsultationsRelatedByServiceId[]= $commonConsultationRelatedByServiceId;
        $commonConsultationRelatedByServiceId->setCommonServiceRelatedByServiceId($this);
    }

    /**
     * @param	CommonConsultationRelatedByServiceId $commonConsultationRelatedByServiceId The commonConsultationRelatedByServiceId object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonConsultationRelatedByServiceId($commonConsultationRelatedByServiceId)
    {
        if ($this->getCommonConsultationsRelatedByServiceId()->contains($commonConsultationRelatedByServiceId)) {
            $this->collCommonConsultationsRelatedByServiceId->remove($this->collCommonConsultationsRelatedByServiceId->search($commonConsultationRelatedByServiceId));
            if (null === $this->commonConsultationsRelatedByServiceIdScheduledForDeletion) {
                $this->commonConsultationsRelatedByServiceIdScheduledForDeletion = clone $this->collCommonConsultationsRelatedByServiceId;
                $this->commonConsultationsRelatedByServiceIdScheduledForDeletion->clear();
            }
            $this->commonConsultationsRelatedByServiceIdScheduledForDeletion[]= $commonConsultationRelatedByServiceId;
            $commonConsultationRelatedByServiceId->setCommonServiceRelatedByServiceId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceIdJoinCommonTTypeContrat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonTTypeContrat', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceIdJoinCommonConsultationRelatedByReferenceConsultationInit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonConsultationRelatedByReferenceConsultationInit', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceIdJoinCommonOperations($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOperations', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceIdJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceIdJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceIdJoinCommonDossierVolumineux($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonDossierVolumineux', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceId($query, $con);
    }

    /**
     * Clears out the collCommonConsultationsRelatedByServiceValidationIntermediaire collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonConsultationsRelatedByServiceValidationIntermediaire()
     */
    public function clearCommonConsultationsRelatedByServiceValidationIntermediaire()
    {
        $this->collCommonConsultationsRelatedByServiceValidationIntermediaire = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationsRelatedByServiceValidationIntermediairePartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultationsRelatedByServiceValidationIntermediaire collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultationsRelatedByServiceValidationIntermediaire($v = true)
    {
        $this->collCommonConsultationsRelatedByServiceValidationIntermediairePartial = $v;
    }

    /**
     * Initializes the collCommonConsultationsRelatedByServiceValidationIntermediaire collection.
     *
     * By default this just sets the collCommonConsultationsRelatedByServiceValidationIntermediaire collection to an empty array (like clearcollCommonConsultationsRelatedByServiceValidationIntermediaire());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultationsRelatedByServiceValidationIntermediaire($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultationsRelatedByServiceValidationIntermediaire && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultationsRelatedByServiceValidationIntermediaire = new PropelObjectCollection();
        $this->collCommonConsultationsRelatedByServiceValidationIntermediaire->setModel('CommonConsultation');
    }

    /**
     * Gets an array of CommonConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     * @throws PropelException
     */
    public function getCommonConsultationsRelatedByServiceValidationIntermediaire($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsRelatedByServiceValidationIntermediairePartial && !$this->isNew();
        if (null === $this->collCommonConsultationsRelatedByServiceValidationIntermediaire || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationsRelatedByServiceValidationIntermediaire) {
                // return empty collection
                $this->initCommonConsultationsRelatedByServiceValidationIntermediaire();
            } else {
                $collCommonConsultationsRelatedByServiceValidationIntermediaire = CommonConsultationQuery::create(null, $criteria)
                    ->filterByCommonServiceRelatedByServiceValidationIntermediaire($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationsRelatedByServiceValidationIntermediairePartial && count($collCommonConsultationsRelatedByServiceValidationIntermediaire)) {
                      $this->initCommonConsultationsRelatedByServiceValidationIntermediaire(false);

                      foreach ($collCommonConsultationsRelatedByServiceValidationIntermediaire as $obj) {
                        if (false == $this->collCommonConsultationsRelatedByServiceValidationIntermediaire->contains($obj)) {
                          $this->collCommonConsultationsRelatedByServiceValidationIntermediaire->append($obj);
                        }
                      }

                      $this->collCommonConsultationsRelatedByServiceValidationIntermediairePartial = true;
                    }

                    $collCommonConsultationsRelatedByServiceValidationIntermediaire->getInternalIterator()->rewind();

                    return $collCommonConsultationsRelatedByServiceValidationIntermediaire;
                }

                if ($partial && $this->collCommonConsultationsRelatedByServiceValidationIntermediaire) {
                    foreach ($this->collCommonConsultationsRelatedByServiceValidationIntermediaire as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultationsRelatedByServiceValidationIntermediaire[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultationsRelatedByServiceValidationIntermediaire = $collCommonConsultationsRelatedByServiceValidationIntermediaire;
                $this->collCommonConsultationsRelatedByServiceValidationIntermediairePartial = false;
            }
        }

        return $this->collCommonConsultationsRelatedByServiceValidationIntermediaire;
    }

    /**
     * Sets a collection of CommonConsultationRelatedByServiceValidationIntermediaire objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultationsRelatedByServiceValidationIntermediaire A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonConsultationsRelatedByServiceValidationIntermediaire(PropelCollection $commonConsultationsRelatedByServiceValidationIntermediaire, PropelPDO $con = null)
    {
        $commonConsultationsRelatedByServiceValidationIntermediaireToDelete = $this->getCommonConsultationsRelatedByServiceValidationIntermediaire(new Criteria(), $con)->diff($commonConsultationsRelatedByServiceValidationIntermediaire);


        $this->commonConsultationsRelatedByServiceValidationIntermediaireScheduledForDeletion = $commonConsultationsRelatedByServiceValidationIntermediaireToDelete;

        foreach ($commonConsultationsRelatedByServiceValidationIntermediaireToDelete as $commonConsultationRelatedByServiceValidationIntermediaireRemoved) {
            $commonConsultationRelatedByServiceValidationIntermediaireRemoved->setCommonServiceRelatedByServiceValidationIntermediaire(null);
        }

        $this->collCommonConsultationsRelatedByServiceValidationIntermediaire = null;
        foreach ($commonConsultationsRelatedByServiceValidationIntermediaire as $commonConsultationRelatedByServiceValidationIntermediaire) {
            $this->addCommonConsultationRelatedByServiceValidationIntermediaire($commonConsultationRelatedByServiceValidationIntermediaire);
        }

        $this->collCommonConsultationsRelatedByServiceValidationIntermediaire = $commonConsultationsRelatedByServiceValidationIntermediaire;
        $this->collCommonConsultationsRelatedByServiceValidationIntermediairePartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultation objects.
     * @throws PropelException
     */
    public function countCommonConsultationsRelatedByServiceValidationIntermediaire(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsRelatedByServiceValidationIntermediairePartial && !$this->isNew();
        if (null === $this->collCommonConsultationsRelatedByServiceValidationIntermediaire || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationsRelatedByServiceValidationIntermediaire) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultationsRelatedByServiceValidationIntermediaire());
            }
            $query = CommonConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonServiceRelatedByServiceValidationIntermediaire($this)
                ->count($con);
        }

        return count($this->collCommonConsultationsRelatedByServiceValidationIntermediaire);
    }

    /**
     * Method called to associate a CommonConsultation object to this object
     * through the CommonConsultation foreign key attribute.
     *
     * @param   CommonConsultation $l CommonConsultation
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonConsultationRelatedByServiceValidationIntermediaire(CommonConsultation $l)
    {
        if ($this->collCommonConsultationsRelatedByServiceValidationIntermediaire === null) {
            $this->initCommonConsultationsRelatedByServiceValidationIntermediaire();
            $this->collCommonConsultationsRelatedByServiceValidationIntermediairePartial = true;
        }
        if (!in_array($l, $this->collCommonConsultationsRelatedByServiceValidationIntermediaire->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultationRelatedByServiceValidationIntermediaire($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultationRelatedByServiceValidationIntermediaire $commonConsultationRelatedByServiceValidationIntermediaire The commonConsultationRelatedByServiceValidationIntermediaire object to add.
     */
    protected function doAddCommonConsultationRelatedByServiceValidationIntermediaire($commonConsultationRelatedByServiceValidationIntermediaire)
    {
        $this->collCommonConsultationsRelatedByServiceValidationIntermediaire[]= $commonConsultationRelatedByServiceValidationIntermediaire;
        $commonConsultationRelatedByServiceValidationIntermediaire->setCommonServiceRelatedByServiceValidationIntermediaire($this);
    }

    /**
     * @param	CommonConsultationRelatedByServiceValidationIntermediaire $commonConsultationRelatedByServiceValidationIntermediaire The commonConsultationRelatedByServiceValidationIntermediaire object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonConsultationRelatedByServiceValidationIntermediaire($commonConsultationRelatedByServiceValidationIntermediaire)
    {
        if ($this->getCommonConsultationsRelatedByServiceValidationIntermediaire()->contains($commonConsultationRelatedByServiceValidationIntermediaire)) {
            $this->collCommonConsultationsRelatedByServiceValidationIntermediaire->remove($this->collCommonConsultationsRelatedByServiceValidationIntermediaire->search($commonConsultationRelatedByServiceValidationIntermediaire));
            if (null === $this->commonConsultationsRelatedByServiceValidationIntermediaireScheduledForDeletion) {
                $this->commonConsultationsRelatedByServiceValidationIntermediaireScheduledForDeletion = clone $this->collCommonConsultationsRelatedByServiceValidationIntermediaire;
                $this->commonConsultationsRelatedByServiceValidationIntermediaireScheduledForDeletion->clear();
            }
            $this->commonConsultationsRelatedByServiceValidationIntermediaireScheduledForDeletion[]= $commonConsultationRelatedByServiceValidationIntermediaire;
            $commonConsultationRelatedByServiceValidationIntermediaire->setCommonServiceRelatedByServiceValidationIntermediaire(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidationIntermediaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationIntermediaireJoinCommonTTypeContrat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonTTypeContrat', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidationIntermediaire($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidationIntermediaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationIntermediaireJoinCommonConsultationRelatedByReferenceConsultationInit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonConsultationRelatedByReferenceConsultationInit', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidationIntermediaire($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidationIntermediaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationIntermediaireJoinCommonOperations($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOperations', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidationIntermediaire($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidationIntermediaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationIntermediaireJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidationIntermediaire($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidationIntermediaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationIntermediaireJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidationIntermediaire($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonConsultationsRelatedByServiceValidationIntermediaire from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsRelatedByServiceValidationIntermediaireJoinCommonDossierVolumineux($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonDossierVolumineux', $join_behavior);

        return $this->getCommonConsultationsRelatedByServiceValidationIntermediaire($query, $con);
    }

    /**
     * Clears out the collCommonInvitePermanentTransverses collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonInvitePermanentTransverses()
     */
    public function clearCommonInvitePermanentTransverses()
    {
        $this->collCommonInvitePermanentTransverses = null; // important to set this to null since that means it is uninitialized
        $this->collCommonInvitePermanentTransversesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonInvitePermanentTransverses collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonInvitePermanentTransverses($v = true)
    {
        $this->collCommonInvitePermanentTransversesPartial = $v;
    }

    /**
     * Initializes the collCommonInvitePermanentTransverses collection.
     *
     * By default this just sets the collCommonInvitePermanentTransverses collection to an empty array (like clearcollCommonInvitePermanentTransverses());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonInvitePermanentTransverses($overrideExisting = true)
    {
        if (null !== $this->collCommonInvitePermanentTransverses && !$overrideExisting) {
            return;
        }
        $this->collCommonInvitePermanentTransverses = new PropelObjectCollection();
        $this->collCommonInvitePermanentTransverses->setModel('CommonInvitePermanentTransverse');
    }

    /**
     * Gets an array of CommonInvitePermanentTransverse objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonInvitePermanentTransverse[] List of CommonInvitePermanentTransverse objects
     * @throws PropelException
     */
    public function getCommonInvitePermanentTransverses($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonInvitePermanentTransversesPartial && !$this->isNew();
        if (null === $this->collCommonInvitePermanentTransverses || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonInvitePermanentTransverses) {
                // return empty collection
                $this->initCommonInvitePermanentTransverses();
            } else {
                $collCommonInvitePermanentTransverses = CommonInvitePermanentTransverseQuery::create(null, $criteria)
                    ->filterByCommonService($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonInvitePermanentTransversesPartial && count($collCommonInvitePermanentTransverses)) {
                      $this->initCommonInvitePermanentTransverses(false);

                      foreach ($collCommonInvitePermanentTransverses as $obj) {
                        if (false == $this->collCommonInvitePermanentTransverses->contains($obj)) {
                          $this->collCommonInvitePermanentTransverses->append($obj);
                        }
                      }

                      $this->collCommonInvitePermanentTransversesPartial = true;
                    }

                    $collCommonInvitePermanentTransverses->getInternalIterator()->rewind();

                    return $collCommonInvitePermanentTransverses;
                }

                if ($partial && $this->collCommonInvitePermanentTransverses) {
                    foreach ($this->collCommonInvitePermanentTransverses as $obj) {
                        if ($obj->isNew()) {
                            $collCommonInvitePermanentTransverses[] = $obj;
                        }
                    }
                }

                $this->collCommonInvitePermanentTransverses = $collCommonInvitePermanentTransverses;
                $this->collCommonInvitePermanentTransversesPartial = false;
            }
        }

        return $this->collCommonInvitePermanentTransverses;
    }

    /**
     * Sets a collection of CommonInvitePermanentTransverse objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonInvitePermanentTransverses A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonInvitePermanentTransverses(PropelCollection $commonInvitePermanentTransverses, PropelPDO $con = null)
    {
        $commonInvitePermanentTransversesToDelete = $this->getCommonInvitePermanentTransverses(new Criteria(), $con)->diff($commonInvitePermanentTransverses);


        $this->commonInvitePermanentTransversesScheduledForDeletion = $commonInvitePermanentTransversesToDelete;

        foreach ($commonInvitePermanentTransversesToDelete as $commonInvitePermanentTransverseRemoved) {
            $commonInvitePermanentTransverseRemoved->setCommonService(null);
        }

        $this->collCommonInvitePermanentTransverses = null;
        foreach ($commonInvitePermanentTransverses as $commonInvitePermanentTransverse) {
            $this->addCommonInvitePermanentTransverse($commonInvitePermanentTransverse);
        }

        $this->collCommonInvitePermanentTransverses = $commonInvitePermanentTransverses;
        $this->collCommonInvitePermanentTransversesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonInvitePermanentTransverse objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonInvitePermanentTransverse objects.
     * @throws PropelException
     */
    public function countCommonInvitePermanentTransverses(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonInvitePermanentTransversesPartial && !$this->isNew();
        if (null === $this->collCommonInvitePermanentTransverses || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonInvitePermanentTransverses) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonInvitePermanentTransverses());
            }
            $query = CommonInvitePermanentTransverseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonService($this)
                ->count($con);
        }

        return count($this->collCommonInvitePermanentTransverses);
    }

    /**
     * Method called to associate a CommonInvitePermanentTransverse object to this object
     * through the CommonInvitePermanentTransverse foreign key attribute.
     *
     * @param   CommonInvitePermanentTransverse $l CommonInvitePermanentTransverse
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonInvitePermanentTransverse(CommonInvitePermanentTransverse $l)
    {
        if ($this->collCommonInvitePermanentTransverses === null) {
            $this->initCommonInvitePermanentTransverses();
            $this->collCommonInvitePermanentTransversesPartial = true;
        }
        if (!in_array($l, $this->collCommonInvitePermanentTransverses->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonInvitePermanentTransverse($l);
        }

        return $this;
    }

    /**
     * @param	CommonInvitePermanentTransverse $commonInvitePermanentTransverse The commonInvitePermanentTransverse object to add.
     */
    protected function doAddCommonInvitePermanentTransverse($commonInvitePermanentTransverse)
    {
        $this->collCommonInvitePermanentTransverses[]= $commonInvitePermanentTransverse;
        $commonInvitePermanentTransverse->setCommonService($this);
    }

    /**
     * @param	CommonInvitePermanentTransverse $commonInvitePermanentTransverse The commonInvitePermanentTransverse object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonInvitePermanentTransverse($commonInvitePermanentTransverse)
    {
        if ($this->getCommonInvitePermanentTransverses()->contains($commonInvitePermanentTransverse)) {
            $this->collCommonInvitePermanentTransverses->remove($this->collCommonInvitePermanentTransverses->search($commonInvitePermanentTransverse));
            if (null === $this->commonInvitePermanentTransversesScheduledForDeletion) {
                $this->commonInvitePermanentTransversesScheduledForDeletion = clone $this->collCommonInvitePermanentTransverses;
                $this->commonInvitePermanentTransversesScheduledForDeletion->clear();
            }
            $this->commonInvitePermanentTransversesScheduledForDeletion[]= $commonInvitePermanentTransverse;
            $commonInvitePermanentTransverse->setCommonService(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonInvitePermanentTransverses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonInvitePermanentTransverse[] List of CommonInvitePermanentTransverse objects
     */
    public function getCommonInvitePermanentTransversesJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonInvitePermanentTransverseQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonInvitePermanentTransverses($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonService is new, it will return
     * an empty collection; or if this CommonService has previously
     * been saved, it will retrieve related CommonInvitePermanentTransverses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonInvitePermanentTransverse[] List of CommonInvitePermanentTransverse objects
     */
    public function getCommonInvitePermanentTransversesJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonInvitePermanentTransverseQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonInvitePermanentTransverses($query, $con);
    }

    /**
     * Clears out the collCommonTFusionnerServicessRelatedByIdServiceCible collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonTFusionnerServicessRelatedByIdServiceCible()
     */
    public function clearCommonTFusionnerServicessRelatedByIdServiceCible()
    {
        $this->collCommonTFusionnerServicessRelatedByIdServiceCible = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTFusionnerServicessRelatedByIdServiceCiblePartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTFusionnerServicessRelatedByIdServiceCible collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTFusionnerServicessRelatedByIdServiceCible($v = true)
    {
        $this->collCommonTFusionnerServicessRelatedByIdServiceCiblePartial = $v;
    }

    /**
     * Initializes the collCommonTFusionnerServicessRelatedByIdServiceCible collection.
     *
     * By default this just sets the collCommonTFusionnerServicessRelatedByIdServiceCible collection to an empty array (like clearcollCommonTFusionnerServicessRelatedByIdServiceCible());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTFusionnerServicessRelatedByIdServiceCible($overrideExisting = true)
    {
        if (null !== $this->collCommonTFusionnerServicessRelatedByIdServiceCible && !$overrideExisting) {
            return;
        }
        $this->collCommonTFusionnerServicessRelatedByIdServiceCible = new PropelObjectCollection();
        $this->collCommonTFusionnerServicessRelatedByIdServiceCible->setModel('CommonTFusionnerServices');
    }

    /**
     * Gets an array of CommonTFusionnerServices objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTFusionnerServices[] List of CommonTFusionnerServices objects
     * @throws PropelException
     */
    public function getCommonTFusionnerServicessRelatedByIdServiceCible($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTFusionnerServicessRelatedByIdServiceCiblePartial && !$this->isNew();
        if (null === $this->collCommonTFusionnerServicessRelatedByIdServiceCible || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTFusionnerServicessRelatedByIdServiceCible) {
                // return empty collection
                $this->initCommonTFusionnerServicessRelatedByIdServiceCible();
            } else {
                $collCommonTFusionnerServicessRelatedByIdServiceCible = CommonTFusionnerServicesQuery::create(null, $criteria)
                    ->filterByCommonServiceRelatedByIdServiceCible($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTFusionnerServicessRelatedByIdServiceCiblePartial && count($collCommonTFusionnerServicessRelatedByIdServiceCible)) {
                      $this->initCommonTFusionnerServicessRelatedByIdServiceCible(false);

                      foreach ($collCommonTFusionnerServicessRelatedByIdServiceCible as $obj) {
                        if (false == $this->collCommonTFusionnerServicessRelatedByIdServiceCible->contains($obj)) {
                          $this->collCommonTFusionnerServicessRelatedByIdServiceCible->append($obj);
                        }
                      }

                      $this->collCommonTFusionnerServicessRelatedByIdServiceCiblePartial = true;
                    }

                    $collCommonTFusionnerServicessRelatedByIdServiceCible->getInternalIterator()->rewind();

                    return $collCommonTFusionnerServicessRelatedByIdServiceCible;
                }

                if ($partial && $this->collCommonTFusionnerServicessRelatedByIdServiceCible) {
                    foreach ($this->collCommonTFusionnerServicessRelatedByIdServiceCible as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTFusionnerServicessRelatedByIdServiceCible[] = $obj;
                        }
                    }
                }

                $this->collCommonTFusionnerServicessRelatedByIdServiceCible = $collCommonTFusionnerServicessRelatedByIdServiceCible;
                $this->collCommonTFusionnerServicessRelatedByIdServiceCiblePartial = false;
            }
        }

        return $this->collCommonTFusionnerServicessRelatedByIdServiceCible;
    }

    /**
     * Sets a collection of CommonTFusionnerServicesRelatedByIdServiceCible objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTFusionnerServicessRelatedByIdServiceCible A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonTFusionnerServicessRelatedByIdServiceCible(PropelCollection $commonTFusionnerServicessRelatedByIdServiceCible, PropelPDO $con = null)
    {
        $commonTFusionnerServicessRelatedByIdServiceCibleToDelete = $this->getCommonTFusionnerServicessRelatedByIdServiceCible(new Criteria(), $con)->diff($commonTFusionnerServicessRelatedByIdServiceCible);


        $this->commonTFusionnerServicessRelatedByIdServiceCibleScheduledForDeletion = $commonTFusionnerServicessRelatedByIdServiceCibleToDelete;

        foreach ($commonTFusionnerServicessRelatedByIdServiceCibleToDelete as $commonTFusionnerServicesRelatedByIdServiceCibleRemoved) {
            $commonTFusionnerServicesRelatedByIdServiceCibleRemoved->setCommonServiceRelatedByIdServiceCible(null);
        }

        $this->collCommonTFusionnerServicessRelatedByIdServiceCible = null;
        foreach ($commonTFusionnerServicessRelatedByIdServiceCible as $commonTFusionnerServicesRelatedByIdServiceCible) {
            $this->addCommonTFusionnerServicesRelatedByIdServiceCible($commonTFusionnerServicesRelatedByIdServiceCible);
        }

        $this->collCommonTFusionnerServicessRelatedByIdServiceCible = $commonTFusionnerServicessRelatedByIdServiceCible;
        $this->collCommonTFusionnerServicessRelatedByIdServiceCiblePartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTFusionnerServices objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTFusionnerServices objects.
     * @throws PropelException
     */
    public function countCommonTFusionnerServicessRelatedByIdServiceCible(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTFusionnerServicessRelatedByIdServiceCiblePartial && !$this->isNew();
        if (null === $this->collCommonTFusionnerServicessRelatedByIdServiceCible || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTFusionnerServicessRelatedByIdServiceCible) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTFusionnerServicessRelatedByIdServiceCible());
            }
            $query = CommonTFusionnerServicesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonServiceRelatedByIdServiceCible($this)
                ->count($con);
        }

        return count($this->collCommonTFusionnerServicessRelatedByIdServiceCible);
    }

    /**
     * Method called to associate a CommonTFusionnerServices object to this object
     * through the CommonTFusionnerServices foreign key attribute.
     *
     * @param   CommonTFusionnerServices $l CommonTFusionnerServices
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonTFusionnerServicesRelatedByIdServiceCible(CommonTFusionnerServices $l)
    {
        if ($this->collCommonTFusionnerServicessRelatedByIdServiceCible === null) {
            $this->initCommonTFusionnerServicessRelatedByIdServiceCible();
            $this->collCommonTFusionnerServicessRelatedByIdServiceCiblePartial = true;
        }
        if (!in_array($l, $this->collCommonTFusionnerServicessRelatedByIdServiceCible->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTFusionnerServicesRelatedByIdServiceCible($l);
        }

        return $this;
    }

    /**
     * @param	CommonTFusionnerServicesRelatedByIdServiceCible $commonTFusionnerServicesRelatedByIdServiceCible The commonTFusionnerServicesRelatedByIdServiceCible object to add.
     */
    protected function doAddCommonTFusionnerServicesRelatedByIdServiceCible($commonTFusionnerServicesRelatedByIdServiceCible)
    {
        $this->collCommonTFusionnerServicessRelatedByIdServiceCible[]= $commonTFusionnerServicesRelatedByIdServiceCible;
        $commonTFusionnerServicesRelatedByIdServiceCible->setCommonServiceRelatedByIdServiceCible($this);
    }

    /**
     * @param	CommonTFusionnerServicesRelatedByIdServiceCible $commonTFusionnerServicesRelatedByIdServiceCible The commonTFusionnerServicesRelatedByIdServiceCible object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonTFusionnerServicesRelatedByIdServiceCible($commonTFusionnerServicesRelatedByIdServiceCible)
    {
        if ($this->getCommonTFusionnerServicessRelatedByIdServiceCible()->contains($commonTFusionnerServicesRelatedByIdServiceCible)) {
            $this->collCommonTFusionnerServicessRelatedByIdServiceCible->remove($this->collCommonTFusionnerServicessRelatedByIdServiceCible->search($commonTFusionnerServicesRelatedByIdServiceCible));
            if (null === $this->commonTFusionnerServicessRelatedByIdServiceCibleScheduledForDeletion) {
                $this->commonTFusionnerServicessRelatedByIdServiceCibleScheduledForDeletion = clone $this->collCommonTFusionnerServicessRelatedByIdServiceCible;
                $this->commonTFusionnerServicessRelatedByIdServiceCibleScheduledForDeletion->clear();
            }
            $this->commonTFusionnerServicessRelatedByIdServiceCibleScheduledForDeletion[]= $commonTFusionnerServicesRelatedByIdServiceCible;
            $commonTFusionnerServicesRelatedByIdServiceCible->setCommonServiceRelatedByIdServiceCible(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonTFusionnerServicessRelatedByIdServiceSource collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonService The current object (for fluent API support)
     * @see        addCommonTFusionnerServicessRelatedByIdServiceSource()
     */
    public function clearCommonTFusionnerServicessRelatedByIdServiceSource()
    {
        $this->collCommonTFusionnerServicessRelatedByIdServiceSource = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTFusionnerServicessRelatedByIdServiceSourcePartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTFusionnerServicessRelatedByIdServiceSource collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTFusionnerServicessRelatedByIdServiceSource($v = true)
    {
        $this->collCommonTFusionnerServicessRelatedByIdServiceSourcePartial = $v;
    }

    /**
     * Initializes the collCommonTFusionnerServicessRelatedByIdServiceSource collection.
     *
     * By default this just sets the collCommonTFusionnerServicessRelatedByIdServiceSource collection to an empty array (like clearcollCommonTFusionnerServicessRelatedByIdServiceSource());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTFusionnerServicessRelatedByIdServiceSource($overrideExisting = true)
    {
        if (null !== $this->collCommonTFusionnerServicessRelatedByIdServiceSource && !$overrideExisting) {
            return;
        }
        $this->collCommonTFusionnerServicessRelatedByIdServiceSource = new PropelObjectCollection();
        $this->collCommonTFusionnerServicessRelatedByIdServiceSource->setModel('CommonTFusionnerServices');
    }

    /**
     * Gets an array of CommonTFusionnerServices objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTFusionnerServices[] List of CommonTFusionnerServices objects
     * @throws PropelException
     */
    public function getCommonTFusionnerServicessRelatedByIdServiceSource($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTFusionnerServicessRelatedByIdServiceSourcePartial && !$this->isNew();
        if (null === $this->collCommonTFusionnerServicessRelatedByIdServiceSource || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTFusionnerServicessRelatedByIdServiceSource) {
                // return empty collection
                $this->initCommonTFusionnerServicessRelatedByIdServiceSource();
            } else {
                $collCommonTFusionnerServicessRelatedByIdServiceSource = CommonTFusionnerServicesQuery::create(null, $criteria)
                    ->filterByCommonServiceRelatedByIdServiceSource($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTFusionnerServicessRelatedByIdServiceSourcePartial && count($collCommonTFusionnerServicessRelatedByIdServiceSource)) {
                      $this->initCommonTFusionnerServicessRelatedByIdServiceSource(false);

                      foreach ($collCommonTFusionnerServicessRelatedByIdServiceSource as $obj) {
                        if (false == $this->collCommonTFusionnerServicessRelatedByIdServiceSource->contains($obj)) {
                          $this->collCommonTFusionnerServicessRelatedByIdServiceSource->append($obj);
                        }
                      }

                      $this->collCommonTFusionnerServicessRelatedByIdServiceSourcePartial = true;
                    }

                    $collCommonTFusionnerServicessRelatedByIdServiceSource->getInternalIterator()->rewind();

                    return $collCommonTFusionnerServicessRelatedByIdServiceSource;
                }

                if ($partial && $this->collCommonTFusionnerServicessRelatedByIdServiceSource) {
                    foreach ($this->collCommonTFusionnerServicessRelatedByIdServiceSource as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTFusionnerServicessRelatedByIdServiceSource[] = $obj;
                        }
                    }
                }

                $this->collCommonTFusionnerServicessRelatedByIdServiceSource = $collCommonTFusionnerServicessRelatedByIdServiceSource;
                $this->collCommonTFusionnerServicessRelatedByIdServiceSourcePartial = false;
            }
        }

        return $this->collCommonTFusionnerServicessRelatedByIdServiceSource;
    }

    /**
     * Sets a collection of CommonTFusionnerServicesRelatedByIdServiceSource objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTFusionnerServicessRelatedByIdServiceSource A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonService The current object (for fluent API support)
     */
    public function setCommonTFusionnerServicessRelatedByIdServiceSource(PropelCollection $commonTFusionnerServicessRelatedByIdServiceSource, PropelPDO $con = null)
    {
        $commonTFusionnerServicessRelatedByIdServiceSourceToDelete = $this->getCommonTFusionnerServicessRelatedByIdServiceSource(new Criteria(), $con)->diff($commonTFusionnerServicessRelatedByIdServiceSource);


        $this->commonTFusionnerServicessRelatedByIdServiceSourceScheduledForDeletion = $commonTFusionnerServicessRelatedByIdServiceSourceToDelete;

        foreach ($commonTFusionnerServicessRelatedByIdServiceSourceToDelete as $commonTFusionnerServicesRelatedByIdServiceSourceRemoved) {
            $commonTFusionnerServicesRelatedByIdServiceSourceRemoved->setCommonServiceRelatedByIdServiceSource(null);
        }

        $this->collCommonTFusionnerServicessRelatedByIdServiceSource = null;
        foreach ($commonTFusionnerServicessRelatedByIdServiceSource as $commonTFusionnerServicesRelatedByIdServiceSource) {
            $this->addCommonTFusionnerServicesRelatedByIdServiceSource($commonTFusionnerServicesRelatedByIdServiceSource);
        }

        $this->collCommonTFusionnerServicessRelatedByIdServiceSource = $commonTFusionnerServicessRelatedByIdServiceSource;
        $this->collCommonTFusionnerServicessRelatedByIdServiceSourcePartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTFusionnerServices objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTFusionnerServices objects.
     * @throws PropelException
     */
    public function countCommonTFusionnerServicessRelatedByIdServiceSource(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTFusionnerServicessRelatedByIdServiceSourcePartial && !$this->isNew();
        if (null === $this->collCommonTFusionnerServicessRelatedByIdServiceSource || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTFusionnerServicessRelatedByIdServiceSource) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTFusionnerServicessRelatedByIdServiceSource());
            }
            $query = CommonTFusionnerServicesQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonServiceRelatedByIdServiceSource($this)
                ->count($con);
        }

        return count($this->collCommonTFusionnerServicessRelatedByIdServiceSource);
    }

    /**
     * Method called to associate a CommonTFusionnerServices object to this object
     * through the CommonTFusionnerServices foreign key attribute.
     *
     * @param   CommonTFusionnerServices $l CommonTFusionnerServices
     * @return CommonService The current object (for fluent API support)
     */
    public function addCommonTFusionnerServicesRelatedByIdServiceSource(CommonTFusionnerServices $l)
    {
        if ($this->collCommonTFusionnerServicessRelatedByIdServiceSource === null) {
            $this->initCommonTFusionnerServicessRelatedByIdServiceSource();
            $this->collCommonTFusionnerServicessRelatedByIdServiceSourcePartial = true;
        }
        if (!in_array($l, $this->collCommonTFusionnerServicessRelatedByIdServiceSource->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTFusionnerServicesRelatedByIdServiceSource($l);
        }

        return $this;
    }

    /**
     * @param	CommonTFusionnerServicesRelatedByIdServiceSource $commonTFusionnerServicesRelatedByIdServiceSource The commonTFusionnerServicesRelatedByIdServiceSource object to add.
     */
    protected function doAddCommonTFusionnerServicesRelatedByIdServiceSource($commonTFusionnerServicesRelatedByIdServiceSource)
    {
        $this->collCommonTFusionnerServicessRelatedByIdServiceSource[]= $commonTFusionnerServicesRelatedByIdServiceSource;
        $commonTFusionnerServicesRelatedByIdServiceSource->setCommonServiceRelatedByIdServiceSource($this);
    }

    /**
     * @param	CommonTFusionnerServicesRelatedByIdServiceSource $commonTFusionnerServicesRelatedByIdServiceSource The commonTFusionnerServicesRelatedByIdServiceSource object to remove.
     * @return CommonService The current object (for fluent API support)
     */
    public function removeCommonTFusionnerServicesRelatedByIdServiceSource($commonTFusionnerServicesRelatedByIdServiceSource)
    {
        if ($this->getCommonTFusionnerServicessRelatedByIdServiceSource()->contains($commonTFusionnerServicesRelatedByIdServiceSource)) {
            $this->collCommonTFusionnerServicessRelatedByIdServiceSource->remove($this->collCommonTFusionnerServicessRelatedByIdServiceSource->search($commonTFusionnerServicesRelatedByIdServiceSource));
            if (null === $this->commonTFusionnerServicessRelatedByIdServiceSourceScheduledForDeletion) {
                $this->commonTFusionnerServicessRelatedByIdServiceSourceScheduledForDeletion = clone $this->collCommonTFusionnerServicessRelatedByIdServiceSource;
                $this->commonTFusionnerServicessRelatedByIdServiceSourceScheduledForDeletion->clear();
            }
            $this->commonTFusionnerServicessRelatedByIdServiceSourceScheduledForDeletion[]= $commonTFusionnerServicesRelatedByIdServiceSource;
            $commonTFusionnerServicesRelatedByIdServiceSource->setCommonServiceRelatedByIdServiceSource(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->old_id = null;
        $this->organisme = null;
        $this->type_service = null;
        $this->libelle = null;
        $this->sigle = null;
        $this->adresse = null;
        $this->adresse_suite = null;
        $this->cp = null;
        $this->ville = null;
        $this->telephone = null;
        $this->fax = null;
        $this->mail = null;
        $this->pays = null;
        $this->id_externe = null;
        $this->date_creation = null;
        $this->date_modification = null;
        $this->siren = null;
        $this->complement = null;
        $this->libelle_ar = null;
        $this->adresse_ar = null;
        $this->adresse_suite_ar = null;
        $this->ville_ar = null;
        $this->pays_ar = null;
        $this->libelle_fr = null;
        $this->adresse_fr = null;
        $this->adresse_suite_fr = null;
        $this->ville_fr = null;
        $this->pays_fr = null;
        $this->libelle_es = null;
        $this->adresse_es = null;
        $this->adresse_suite_es = null;
        $this->ville_es = null;
        $this->pays_es = null;
        $this->libelle_en = null;
        $this->adresse_en = null;
        $this->adresse_suite_en = null;
        $this->ville_en = null;
        $this->pays_en = null;
        $this->libelle_su = null;
        $this->adresse_su = null;
        $this->adresse_suite_su = null;
        $this->ville_su = null;
        $this->pays_su = null;
        $this->libelle_du = null;
        $this->adresse_du = null;
        $this->adresse_suite_du = null;
        $this->ville_du = null;
        $this->pays_du = null;
        $this->libelle_cz = null;
        $this->adresse_cz = null;
        $this->adresse_suite_cz = null;
        $this->ville_cz = null;
        $this->pays_cz = null;
        $this->libelle_it = null;
        $this->adresse_it = null;
        $this->adresse_suite_it = null;
        $this->ville_it = null;
        $this->pays_it = null;
        $this->chemin_complet = null;
        $this->chemin_complet_fr = null;
        $this->chemin_complet_en = null;
        $this->chemin_complet_es = null;
        $this->chemin_complet_su = null;
        $this->chemin_complet_du = null;
        $this->chemin_complet_cz = null;
        $this->chemin_complet_ar = null;
        $this->chemin_complet_it = null;
        $this->nom_service_archiveur = null;
        $this->identifiant_service_archiveur = null;
        $this->affichage_service = null;
        $this->activation_fuseau_horaire = null;
        $this->decalage_horaire = null;
        $this->lieu_residence = null;
        $this->alerte = null;
        $this->acces_chorus = null;
        $this->forme_juridique = null;
        $this->forme_juridique_code = null;
        $this->synchronisation_exec = null;
        $this->id_entite = null;
        $this->id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonAffiliationServicesRelatedByServiceId) {
                foreach ($this->collCommonAffiliationServicesRelatedByServiceId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonAffiliationServicesRelatedByServiceParentId) {
                foreach ($this->collCommonAffiliationServicesRelatedByServiceParentId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonAgents) {
                foreach ($this->collCommonAgents as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonEncherePmis) {
                foreach ($this->collCommonEncherePmis as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonNewslettersRelatedByIdServiceDestinataire) {
                foreach ($this->collCommonNewslettersRelatedByIdServiceDestinataire as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonNewslettersRelatedByIdServiceRedacteur) {
                foreach ($this->collCommonNewslettersRelatedByIdServiceRedacteur as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonParametrageEncheres) {
                foreach ($this->collCommonParametrageEncheres as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTTelechargementAsynchrones) {
                foreach ($this->collCommonTTelechargementAsynchrones as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTypeProcedureOrganismes) {
                foreach ($this->collCommonTypeProcedureOrganismes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonAgentTechniqueAssociations) {
                foreach ($this->collCommonAgentTechniqueAssociations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonConsultationsRelatedByServiceValidation) {
                foreach ($this->collCommonConsultationsRelatedByServiceValidation as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonConsultationsRelatedByServiceId) {
                foreach ($this->collCommonConsultationsRelatedByServiceId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonConsultationsRelatedByServiceValidationIntermediaire) {
                foreach ($this->collCommonConsultationsRelatedByServiceValidationIntermediaire as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonInvitePermanentTransverses) {
                foreach ($this->collCommonInvitePermanentTransverses as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTFusionnerServicessRelatedByIdServiceCible) {
                foreach ($this->collCommonTFusionnerServicessRelatedByIdServiceCible as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTFusionnerServicessRelatedByIdServiceSource) {
                foreach ($this->collCommonTFusionnerServicessRelatedByIdServiceSource as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonAffiliationServicesRelatedByServiceId instanceof PropelCollection) {
            $this->collCommonAffiliationServicesRelatedByServiceId->clearIterator();
        }
        $this->collCommonAffiliationServicesRelatedByServiceId = null;
        if ($this->collCommonAffiliationServicesRelatedByServiceParentId instanceof PropelCollection) {
            $this->collCommonAffiliationServicesRelatedByServiceParentId->clearIterator();
        }
        $this->collCommonAffiliationServicesRelatedByServiceParentId = null;
        if ($this->collCommonAgents instanceof PropelCollection) {
            $this->collCommonAgents->clearIterator();
        }
        $this->collCommonAgents = null;
        if ($this->collCommonEncherePmis instanceof PropelCollection) {
            $this->collCommonEncherePmis->clearIterator();
        }
        $this->collCommonEncherePmis = null;
        if ($this->collCommonNewslettersRelatedByIdServiceDestinataire instanceof PropelCollection) {
            $this->collCommonNewslettersRelatedByIdServiceDestinataire->clearIterator();
        }
        $this->collCommonNewslettersRelatedByIdServiceDestinataire = null;
        if ($this->collCommonNewslettersRelatedByIdServiceRedacteur instanceof PropelCollection) {
            $this->collCommonNewslettersRelatedByIdServiceRedacteur->clearIterator();
        }
        $this->collCommonNewslettersRelatedByIdServiceRedacteur = null;
        if ($this->collCommonParametrageEncheres instanceof PropelCollection) {
            $this->collCommonParametrageEncheres->clearIterator();
        }
        $this->collCommonParametrageEncheres = null;
        if ($this->collCommonTTelechargementAsynchrones instanceof PropelCollection) {
            $this->collCommonTTelechargementAsynchrones->clearIterator();
        }
        $this->collCommonTTelechargementAsynchrones = null;
        if ($this->collCommonTypeProcedureOrganismes instanceof PropelCollection) {
            $this->collCommonTypeProcedureOrganismes->clearIterator();
        }
        $this->collCommonTypeProcedureOrganismes = null;
        if ($this->collCommonAgentTechniqueAssociations instanceof PropelCollection) {
            $this->collCommonAgentTechniqueAssociations->clearIterator();
        }
        $this->collCommonAgentTechniqueAssociations = null;
        if ($this->collCommonConsultationsRelatedByServiceValidation instanceof PropelCollection) {
            $this->collCommonConsultationsRelatedByServiceValidation->clearIterator();
        }
        $this->collCommonConsultationsRelatedByServiceValidation = null;
        if ($this->collCommonConsultationsRelatedByServiceId instanceof PropelCollection) {
            $this->collCommonConsultationsRelatedByServiceId->clearIterator();
        }
        $this->collCommonConsultationsRelatedByServiceId = null;
        if ($this->collCommonConsultationsRelatedByServiceValidationIntermediaire instanceof PropelCollection) {
            $this->collCommonConsultationsRelatedByServiceValidationIntermediaire->clearIterator();
        }
        $this->collCommonConsultationsRelatedByServiceValidationIntermediaire = null;
        if ($this->collCommonInvitePermanentTransverses instanceof PropelCollection) {
            $this->collCommonInvitePermanentTransverses->clearIterator();
        }
        $this->collCommonInvitePermanentTransverses = null;
        if ($this->collCommonTFusionnerServicessRelatedByIdServiceCible instanceof PropelCollection) {
            $this->collCommonTFusionnerServicessRelatedByIdServiceCible->clearIterator();
        }
        $this->collCommonTFusionnerServicessRelatedByIdServiceCible = null;
        if ($this->collCommonTFusionnerServicessRelatedByIdServiceSource instanceof PropelCollection) {
            $this->collCommonTFusionnerServicessRelatedByIdServiceSource->clearIterator();
        }
        $this->collCommonTFusionnerServicessRelatedByIdServiceSource = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonServicePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
