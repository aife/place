<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonHabilitationAgentWs;
use Application\Propel\Mpe\CommonHabilitationAgentWsQuery;
use Application\Propel\Mpe\CommonWebService;
use Application\Propel\Mpe\CommonWebServicePeer;
use Application\Propel\Mpe\CommonWebServiceQuery;

/**
 * Base class that represents a row from the 'web_service' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonWebService extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonWebServicePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonWebServicePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the route_ws field.
     * @var        string
     */
    protected $route_ws;

    /**
     * The value for the method_ws field.
     * @var        string
     */
    protected $method_ws;

    /**
     * The value for the nom_ws field.
     * @var        string
     */
    protected $nom_ws;

    /**
     * The value for the uniquement_technique field.
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $uniquement_technique;

    /**
     * @var        PropelObjectCollection|CommonHabilitationAgentWs[] Collection to store aggregation of CommonHabilitationAgentWs objects.
     */
    protected $collCommonHabilitationAgentWss;
    protected $collCommonHabilitationAgentWssPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonHabilitationAgentWssScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->uniquement_technique = true;
    }

    /**
     * Initializes internal state of BaseCommonWebService object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [route_ws] column value.
     *
     * @return string
     */
    public function getRouteWs()
    {

        return $this->route_ws;
    }

    /**
     * Get the [method_ws] column value.
     *
     * @return string
     */
    public function getMethodWs()
    {

        return $this->method_ws;
    }

    /**
     * Get the [nom_ws] column value.
     *
     * @return string
     */
    public function getNomWs()
    {

        return $this->nom_ws;
    }

    /**
     * Get the [uniquement_technique] column value.
     *
     * @return boolean
     */
    public function getUniquementTechnique()
    {

        return $this->uniquement_technique;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonWebService The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonWebServicePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [route_ws] column.
     *
     * @param string $v new value
     * @return CommonWebService The current object (for fluent API support)
     */
    public function setRouteWs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->route_ws !== $v) {
            $this->route_ws = $v;
            $this->modifiedColumns[] = CommonWebServicePeer::ROUTE_WS;
        }


        return $this;
    } // setRouteWs()

    /**
     * Set the value of [method_ws] column.
     *
     * @param string $v new value
     * @return CommonWebService The current object (for fluent API support)
     */
    public function setMethodWs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->method_ws !== $v) {
            $this->method_ws = $v;
            $this->modifiedColumns[] = CommonWebServicePeer::METHOD_WS;
        }


        return $this;
    } // setMethodWs()

    /**
     * Set the value of [nom_ws] column.
     *
     * @param string $v new value
     * @return CommonWebService The current object (for fluent API support)
     */
    public function setNomWs($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_ws !== $v) {
            $this->nom_ws = $v;
            $this->modifiedColumns[] = CommonWebServicePeer::NOM_WS;
        }


        return $this;
    } // setNomWs()

    /**
     * Sets the value of the [uniquement_technique] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonWebService The current object (for fluent API support)
     */
    public function setUniquementTechnique($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->uniquement_technique !== $v) {
            $this->uniquement_technique = $v;
            $this->modifiedColumns[] = CommonWebServicePeer::UNIQUEMENT_TECHNIQUE;
        }


        return $this;
    } // setUniquementTechnique()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->uniquement_technique !== true) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->route_ws = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->method_ws = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->nom_ws = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->uniquement_technique = ($row[$startcol + 4] !== null) ? (boolean) $row[$startcol + 4] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 5; // 5 = CommonWebServicePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonWebService object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonWebServicePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonWebServicePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCommonHabilitationAgentWss = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonWebServicePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonWebServiceQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonWebServicePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonWebServicePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonHabilitationAgentWssScheduledForDeletion !== null) {
                if (!$this->commonHabilitationAgentWssScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonHabilitationAgentWsQuery::create()
                        ->filterByPrimaryKeys($this->commonHabilitationAgentWssScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonHabilitationAgentWssScheduledForDeletion = null;
                }
            }

            if ($this->collCommonHabilitationAgentWss !== null) {
                foreach ($this->collCommonHabilitationAgentWss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonWebServicePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonWebServicePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonWebServicePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonWebServicePeer::ROUTE_WS)) {
            $modifiedColumns[':p' . $index++]  = '`route_ws`';
        }
        if ($this->isColumnModified(CommonWebServicePeer::METHOD_WS)) {
            $modifiedColumns[':p' . $index++]  = '`method_ws`';
        }
        if ($this->isColumnModified(CommonWebServicePeer::NOM_WS)) {
            $modifiedColumns[':p' . $index++]  = '`nom_ws`';
        }
        if ($this->isColumnModified(CommonWebServicePeer::UNIQUEMENT_TECHNIQUE)) {
            $modifiedColumns[':p' . $index++]  = '`uniquement_technique`';
        }

        $sql = sprintf(
            'INSERT INTO `web_service` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`route_ws`':
                        $stmt->bindValue($identifier, $this->route_ws, PDO::PARAM_STR);
                        break;
                    case '`method_ws`':
                        $stmt->bindValue($identifier, $this->method_ws, PDO::PARAM_STR);
                        break;
                    case '`nom_ws`':
                        $stmt->bindValue($identifier, $this->nom_ws, PDO::PARAM_STR);
                        break;
                    case '`uniquement_technique`':
                        $stmt->bindValue($identifier, (int) $this->uniquement_technique, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonWebServicePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonHabilitationAgentWss !== null) {
                    foreach ($this->collCommonHabilitationAgentWss as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonWebServicePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getRouteWs();
                break;
            case 2:
                return $this->getMethodWs();
                break;
            case 3:
                return $this->getNomWs();
                break;
            case 4:
                return $this->getUniquementTechnique();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonWebService'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonWebService'][$this->getPrimaryKey()] = true;
        $keys = CommonWebServicePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getRouteWs(),
            $keys[2] => $this->getMethodWs(),
            $keys[3] => $this->getNomWs(),
            $keys[4] => $this->getUniquementTechnique(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collCommonHabilitationAgentWss) {
                $result['CommonHabilitationAgentWss'] = $this->collCommonHabilitationAgentWss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonWebServicePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setRouteWs($value);
                break;
            case 2:
                $this->setMethodWs($value);
                break;
            case 3:
                $this->setNomWs($value);
                break;
            case 4:
                $this->setUniquementTechnique($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonWebServicePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setRouteWs($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setMethodWs($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNomWs($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setUniquementTechnique($arr[$keys[4]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonWebServicePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonWebServicePeer::ID)) $criteria->add(CommonWebServicePeer::ID, $this->id);
        if ($this->isColumnModified(CommonWebServicePeer::ROUTE_WS)) $criteria->add(CommonWebServicePeer::ROUTE_WS, $this->route_ws);
        if ($this->isColumnModified(CommonWebServicePeer::METHOD_WS)) $criteria->add(CommonWebServicePeer::METHOD_WS, $this->method_ws);
        if ($this->isColumnModified(CommonWebServicePeer::NOM_WS)) $criteria->add(CommonWebServicePeer::NOM_WS, $this->nom_ws);
        if ($this->isColumnModified(CommonWebServicePeer::UNIQUEMENT_TECHNIQUE)) $criteria->add(CommonWebServicePeer::UNIQUEMENT_TECHNIQUE, $this->uniquement_technique);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonWebServicePeer::DATABASE_NAME);
        $criteria->add(CommonWebServicePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonWebService (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setRouteWs($this->getRouteWs());
        $copyObj->setMethodWs($this->getMethodWs());
        $copyObj->setNomWs($this->getNomWs());
        $copyObj->setUniquementTechnique($this->getUniquementTechnique());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonHabilitationAgentWss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonHabilitationAgentWs($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonWebService Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonWebServicePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonWebServicePeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonHabilitationAgentWs' == $relationName) {
            $this->initCommonHabilitationAgentWss();
        }
    }

    /**
     * Clears out the collCommonHabilitationAgentWss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonWebService The current object (for fluent API support)
     * @see        addCommonHabilitationAgentWss()
     */
    public function clearCommonHabilitationAgentWss()
    {
        $this->collCommonHabilitationAgentWss = null; // important to set this to null since that means it is uninitialized
        $this->collCommonHabilitationAgentWssPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonHabilitationAgentWss collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonHabilitationAgentWss($v = true)
    {
        $this->collCommonHabilitationAgentWssPartial = $v;
    }

    /**
     * Initializes the collCommonHabilitationAgentWss collection.
     *
     * By default this just sets the collCommonHabilitationAgentWss collection to an empty array (like clearcollCommonHabilitationAgentWss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonHabilitationAgentWss($overrideExisting = true)
    {
        if (null !== $this->collCommonHabilitationAgentWss && !$overrideExisting) {
            return;
        }
        $this->collCommonHabilitationAgentWss = new PropelObjectCollection();
        $this->collCommonHabilitationAgentWss->setModel('CommonHabilitationAgentWs');
    }

    /**
     * Gets an array of CommonHabilitationAgentWs objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonWebService is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonHabilitationAgentWs[] List of CommonHabilitationAgentWs objects
     * @throws PropelException
     */
    public function getCommonHabilitationAgentWss($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonHabilitationAgentWssPartial && !$this->isNew();
        if (null === $this->collCommonHabilitationAgentWss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonHabilitationAgentWss) {
                // return empty collection
                $this->initCommonHabilitationAgentWss();
            } else {
                $collCommonHabilitationAgentWss = CommonHabilitationAgentWsQuery::create(null, $criteria)
                    ->filterByCommonWebService($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonHabilitationAgentWssPartial && count($collCommonHabilitationAgentWss)) {
                      $this->initCommonHabilitationAgentWss(false);

                      foreach ($collCommonHabilitationAgentWss as $obj) {
                        if (false == $this->collCommonHabilitationAgentWss->contains($obj)) {
                          $this->collCommonHabilitationAgentWss->append($obj);
                        }
                      }

                      $this->collCommonHabilitationAgentWssPartial = true;
                    }

                    $collCommonHabilitationAgentWss->getInternalIterator()->rewind();

                    return $collCommonHabilitationAgentWss;
                }

                if ($partial && $this->collCommonHabilitationAgentWss) {
                    foreach ($this->collCommonHabilitationAgentWss as $obj) {
                        if ($obj->isNew()) {
                            $collCommonHabilitationAgentWss[] = $obj;
                        }
                    }
                }

                $this->collCommonHabilitationAgentWss = $collCommonHabilitationAgentWss;
                $this->collCommonHabilitationAgentWssPartial = false;
            }
        }

        return $this->collCommonHabilitationAgentWss;
    }

    /**
     * Sets a collection of CommonHabilitationAgentWs objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonHabilitationAgentWss A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonWebService The current object (for fluent API support)
     */
    public function setCommonHabilitationAgentWss(PropelCollection $commonHabilitationAgentWss, PropelPDO $con = null)
    {
        $commonHabilitationAgentWssToDelete = $this->getCommonHabilitationAgentWss(new Criteria(), $con)->diff($commonHabilitationAgentWss);


        $this->commonHabilitationAgentWssScheduledForDeletion = $commonHabilitationAgentWssToDelete;

        foreach ($commonHabilitationAgentWssToDelete as $commonHabilitationAgentWsRemoved) {
            $commonHabilitationAgentWsRemoved->setCommonWebService(null);
        }

        $this->collCommonHabilitationAgentWss = null;
        foreach ($commonHabilitationAgentWss as $commonHabilitationAgentWs) {
            $this->addCommonHabilitationAgentWs($commonHabilitationAgentWs);
        }

        $this->collCommonHabilitationAgentWss = $commonHabilitationAgentWss;
        $this->collCommonHabilitationAgentWssPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonHabilitationAgentWs objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonHabilitationAgentWs objects.
     * @throws PropelException
     */
    public function countCommonHabilitationAgentWss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonHabilitationAgentWssPartial && !$this->isNew();
        if (null === $this->collCommonHabilitationAgentWss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonHabilitationAgentWss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonHabilitationAgentWss());
            }
            $query = CommonHabilitationAgentWsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonWebService($this)
                ->count($con);
        }

        return count($this->collCommonHabilitationAgentWss);
    }

    /**
     * Method called to associate a CommonHabilitationAgentWs object to this object
     * through the CommonHabilitationAgentWs foreign key attribute.
     *
     * @param   CommonHabilitationAgentWs $l CommonHabilitationAgentWs
     * @return CommonWebService The current object (for fluent API support)
     */
    public function addCommonHabilitationAgentWs(CommonHabilitationAgentWs $l)
    {
        if ($this->collCommonHabilitationAgentWss === null) {
            $this->initCommonHabilitationAgentWss();
            $this->collCommonHabilitationAgentWssPartial = true;
        }
        if (!in_array($l, $this->collCommonHabilitationAgentWss->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonHabilitationAgentWs($l);
        }

        return $this;
    }

    /**
     * @param	CommonHabilitationAgentWs $commonHabilitationAgentWs The commonHabilitationAgentWs object to add.
     */
    protected function doAddCommonHabilitationAgentWs($commonHabilitationAgentWs)
    {
        $this->collCommonHabilitationAgentWss[]= $commonHabilitationAgentWs;
        $commonHabilitationAgentWs->setCommonWebService($this);
    }

    /**
     * @param	CommonHabilitationAgentWs $commonHabilitationAgentWs The commonHabilitationAgentWs object to remove.
     * @return CommonWebService The current object (for fluent API support)
     */
    public function removeCommonHabilitationAgentWs($commonHabilitationAgentWs)
    {
        if ($this->getCommonHabilitationAgentWss()->contains($commonHabilitationAgentWs)) {
            $this->collCommonHabilitationAgentWss->remove($this->collCommonHabilitationAgentWss->search($commonHabilitationAgentWs));
            if (null === $this->commonHabilitationAgentWssScheduledForDeletion) {
                $this->commonHabilitationAgentWssScheduledForDeletion = clone $this->collCommonHabilitationAgentWss;
                $this->commonHabilitationAgentWssScheduledForDeletion->clear();
            }
            $this->commonHabilitationAgentWssScheduledForDeletion[]= clone $commonHabilitationAgentWs;
            $commonHabilitationAgentWs->setCommonWebService(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonWebService is new, it will return
     * an empty collection; or if this CommonWebService has previously
     * been saved, it will retrieve related CommonHabilitationAgentWss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonWebService.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonHabilitationAgentWs[] List of CommonHabilitationAgentWs objects
     */
    public function getCommonHabilitationAgentWssJoinCommonAgent($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonHabilitationAgentWsQuery::create(null, $criteria);
        $query->joinWith('CommonAgent', $join_behavior);

        return $this->getCommonHabilitationAgentWss($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->route_ws = null;
        $this->method_ws = null;
        $this->nom_ws = null;
        $this->uniquement_technique = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonHabilitationAgentWss) {
                foreach ($this->collCommonHabilitationAgentWss as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonHabilitationAgentWss instanceof PropelCollection) {
            $this->collCommonHabilitationAgentWss->clearIterator();
        }
        $this->collCommonHabilitationAgentWss = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonWebServicePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
