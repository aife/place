<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTNotificationAgent;
use Application\Propel\Mpe\CommonTNotificationAgentPeer;
use Application\Propel\Mpe\CommonTNotificationAgentQuery;
use Application\Propel\Mpe\CommonTTypeNotificationAgent;
use Application\Propel\Mpe\CommonTTypeNotificationAgentQuery;

/**
 * Base class that represents a row from the 't_notification_agent' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTNotificationAgent extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTNotificationAgentPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTNotificationAgentPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_notification field.
     * @var        int
     */
    protected $id_notification;

    /**
     * The value for the id_agent field.
     * @var        int
     */
    protected $id_agent;

    /**
     * The value for the id_type field.
     * @var        int
     */
    protected $id_type;

    /**
     * The value for the service_id field.
     * @var        int
     */
    protected $service_id;

    /**
     * The value for the organisme field.
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the reference_objet field.
     * @var        string
     */
    protected $reference_objet;

    /**
     * The value for the libelle_notification field.
     * @var        string
     */
    protected $libelle_notification;

    /**
     * The value for the description field.
     * @var        string
     */
    protected $description;

    /**
     * The value for the notification_lue field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $notification_lue;

    /**
     * The value for the notification_actif field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $notification_actif;

    /**
     * The value for the url_notification field.
     * @var        string
     */
    protected $url_notification;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_lecture field.
     * @var        string
     */
    protected $date_lecture;

    /**
     * @var        CommonTTypeNotificationAgent
     */
    protected $aCommonTTypeNotificationAgent;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->notification_lue = '0';
        $this->notification_actif = '1';
    }

    /**
     * Initializes internal state of BaseCommonTNotificationAgent object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id_notification] column value.
     *
     * @return int
     */
    public function getIdNotification()
    {

        return $this->id_notification;
    }

    /**
     * Get the [id_agent] column value.
     *
     * @return int
     */
    public function getIdAgent()
    {

        return $this->id_agent;
    }

    /**
     * Get the [id_type] column value.
     *
     * @return int
     */
    public function getIdType()
    {

        return $this->id_type;
    }

    /**
     * Get the [service_id] column value.
     *
     * @return int
     */
    public function getServiceId()
    {

        return $this->service_id;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [reference_objet] column value.
     *
     * @return string
     */
    public function getReferenceObjet()
    {

        return $this->reference_objet;
    }

    /**
     * Get the [libelle_notification] column value.
     *
     * @return string
     */
    public function getLibelleNotification()
    {

        return $this->libelle_notification;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {

        return $this->description;
    }

    /**
     * Get the [notification_lue] column value.
     *
     * @return string
     */
    public function getNotificationLue()
    {

        return $this->notification_lue;
    }

    /**
     * Get the [notification_actif] column value.
     *
     * @return string
     */
    public function getNotificationActif()
    {

        return $this->notification_actif;
    }

    /**
     * Get the [url_notification] column value.
     *
     * @return string
     */
    public function getUrlNotification()
    {

        return $this->url_notification;
    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_lecture] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateLecture($format = 'Y-m-d H:i:s')
    {
        if ($this->date_lecture === null) {
            return null;
        }

        if ($this->date_lecture === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_lecture);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_lecture, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [id_notification] column.
     *
     * @param int $v new value
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setIdNotification($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_notification !== $v) {
            $this->id_notification = $v;
            $this->modifiedColumns[] = CommonTNotificationAgentPeer::ID_NOTIFICATION;
        }


        return $this;
    } // setIdNotification()

    /**
     * Set the value of [id_agent] column.
     *
     * @param int $v new value
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setIdAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_agent !== $v) {
            $this->id_agent = $v;
            $this->modifiedColumns[] = CommonTNotificationAgentPeer::ID_AGENT;
        }


        return $this;
    } // setIdAgent()

    /**
     * Set the value of [id_type] column.
     *
     * @param int $v new value
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setIdType($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_type !== $v) {
            $this->id_type = $v;
            $this->modifiedColumns[] = CommonTNotificationAgentPeer::ID_TYPE;
        }

        if ($this->aCommonTTypeNotificationAgent !== null && $this->aCommonTTypeNotificationAgent->getIdType() !== $v) {
            $this->aCommonTTypeNotificationAgent = null;
        }


        return $this;
    } // setIdType()

    /**
     * Set the value of [service_id] column.
     *
     * @param int $v new value
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setServiceId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->service_id !== $v) {
            $this->service_id = $v;
            $this->modifiedColumns[] = CommonTNotificationAgentPeer::SERVICE_ID;
        }


        return $this;
    } // setServiceId()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonTNotificationAgentPeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [reference_objet] column.
     *
     * @param string $v new value
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setReferenceObjet($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->reference_objet !== $v) {
            $this->reference_objet = $v;
            $this->modifiedColumns[] = CommonTNotificationAgentPeer::REFERENCE_OBJET;
        }


        return $this;
    } // setReferenceObjet()

    /**
     * Set the value of [libelle_notification] column.
     *
     * @param string $v new value
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setLibelleNotification($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->libelle_notification !== $v) {
            $this->libelle_notification = $v;
            $this->modifiedColumns[] = CommonTNotificationAgentPeer::LIBELLE_NOTIFICATION;
        }


        return $this;
    } // setLibelleNotification()

    /**
     * Set the value of [description] column.
     *
     * @param string $v new value
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[] = CommonTNotificationAgentPeer::DESCRIPTION;
        }


        return $this;
    } // setDescription()

    /**
     * Set the value of [notification_lue] column.
     *
     * @param string $v new value
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setNotificationLue($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->notification_lue !== $v) {
            $this->notification_lue = $v;
            $this->modifiedColumns[] = CommonTNotificationAgentPeer::NOTIFICATION_LUE;
        }


        return $this;
    } // setNotificationLue()

    /**
     * Set the value of [notification_actif] column.
     *
     * @param string $v new value
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setNotificationActif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->notification_actif !== $v) {
            $this->notification_actif = $v;
            $this->modifiedColumns[] = CommonTNotificationAgentPeer::NOTIFICATION_ACTIF;
        }


        return $this;
    } // setNotificationActif()

    /**
     * Set the value of [url_notification] column.
     *
     * @param string $v new value
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setUrlNotification($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->url_notification !== $v) {
            $this->url_notification = $v;
            $this->modifiedColumns[] = CommonTNotificationAgentPeer::URL_NOTIFICATION;
        }


        return $this;
    } // setUrlNotification()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonTNotificationAgentPeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Sets the value of [date_lecture] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTNotificationAgent The current object (for fluent API support)
     */
    public function setDateLecture($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_lecture !== null || $dt !== null) {
            $currentDateAsString = ($this->date_lecture !== null && $tmpDt = new DateTime($this->date_lecture)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_lecture = $newDateAsString;
                $this->modifiedColumns[] = CommonTNotificationAgentPeer::DATE_LECTURE;
            }
        } // if either are not null


        return $this;
    } // setDateLecture()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->notification_lue !== '0') {
                return false;
            }

            if ($this->notification_actif !== '1') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_notification = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_agent = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->id_type = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->service_id = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->organisme = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->reference_objet = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->libelle_notification = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->description = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->notification_lue = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->notification_actif = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->url_notification = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->date_creation = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->date_lecture = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 13; // 13 = CommonTNotificationAgentPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTNotificationAgent object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonTTypeNotificationAgent !== null && $this->id_type !== $this->aCommonTTypeNotificationAgent->getIdType()) {
            $this->aCommonTTypeNotificationAgent = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTNotificationAgentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTNotificationAgentPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonTTypeNotificationAgent = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTNotificationAgentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTNotificationAgentQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTNotificationAgentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTNotificationAgentPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTTypeNotificationAgent !== null) {
                if ($this->aCommonTTypeNotificationAgent->isModified() || $this->aCommonTTypeNotificationAgent->isNew()) {
                    $affectedRows += $this->aCommonTTypeNotificationAgent->save($con);
                }
                $this->setCommonTTypeNotificationAgent($this->aCommonTTypeNotificationAgent);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTNotificationAgentPeer::ID_NOTIFICATION;
        if (null !== $this->id_notification) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTNotificationAgentPeer::ID_NOTIFICATION . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTNotificationAgentPeer::ID_NOTIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`id_notification`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::ID_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_agent`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::ID_TYPE)) {
            $modifiedColumns[':p' . $index++]  = '`id_type`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::SERVICE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`service_id`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::REFERENCE_OBJET)) {
            $modifiedColumns[':p' . $index++]  = '`reference_objet`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::LIBELLE_NOTIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`libelle_notification`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = '`description`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::NOTIFICATION_LUE)) {
            $modifiedColumns[':p' . $index++]  = '`notification_lue`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::NOTIFICATION_ACTIF)) {
            $modifiedColumns[':p' . $index++]  = '`notification_actif`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::URL_NOTIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`url_notification`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonTNotificationAgentPeer::DATE_LECTURE)) {
            $modifiedColumns[':p' . $index++]  = '`date_lecture`';
        }

        $sql = sprintf(
            'INSERT INTO `t_notification_agent` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_notification`':
                        $stmt->bindValue($identifier, $this->id_notification, PDO::PARAM_INT);
                        break;
                    case '`id_agent`':
                        $stmt->bindValue($identifier, $this->id_agent, PDO::PARAM_INT);
                        break;
                    case '`id_type`':
                        $stmt->bindValue($identifier, $this->id_type, PDO::PARAM_INT);
                        break;
                    case '`service_id`':
                        $stmt->bindValue($identifier, $this->service_id, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`reference_objet`':
                        $stmt->bindValue($identifier, $this->reference_objet, PDO::PARAM_STR);
                        break;
                    case '`libelle_notification`':
                        $stmt->bindValue($identifier, $this->libelle_notification, PDO::PARAM_STR);
                        break;
                    case '`description`':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case '`notification_lue`':
                        $stmt->bindValue($identifier, $this->notification_lue, PDO::PARAM_STR);
                        break;
                    case '`notification_actif`':
                        $stmt->bindValue($identifier, $this->notification_actif, PDO::PARAM_STR);
                        break;
                    case '`url_notification`':
                        $stmt->bindValue($identifier, $this->url_notification, PDO::PARAM_STR);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_lecture`':
                        $stmt->bindValue($identifier, $this->date_lecture, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdNotification($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonTTypeNotificationAgent !== null) {
                if (!$this->aCommonTTypeNotificationAgent->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonTTypeNotificationAgent->getValidationFailures());
                }
            }


            if (($retval = CommonTNotificationAgentPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTNotificationAgentPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdNotification();
                break;
            case 1:
                return $this->getIdAgent();
                break;
            case 2:
                return $this->getIdType();
                break;
            case 3:
                return $this->getServiceId();
                break;
            case 4:
                return $this->getOrganisme();
                break;
            case 5:
                return $this->getReferenceObjet();
                break;
            case 6:
                return $this->getLibelleNotification();
                break;
            case 7:
                return $this->getDescription();
                break;
            case 8:
                return $this->getNotificationLue();
                break;
            case 9:
                return $this->getNotificationActif();
                break;
            case 10:
                return $this->getUrlNotification();
                break;
            case 11:
                return $this->getDateCreation();
                break;
            case 12:
                return $this->getDateLecture();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTNotificationAgent'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTNotificationAgent'][$this->getPrimaryKey()] = true;
        $keys = CommonTNotificationAgentPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdNotification(),
            $keys[1] => $this->getIdAgent(),
            $keys[2] => $this->getIdType(),
            $keys[3] => $this->getServiceId(),
            $keys[4] => $this->getOrganisme(),
            $keys[5] => $this->getReferenceObjet(),
            $keys[6] => $this->getLibelleNotification(),
            $keys[7] => $this->getDescription(),
            $keys[8] => $this->getNotificationLue(),
            $keys[9] => $this->getNotificationActif(),
            $keys[10] => $this->getUrlNotification(),
            $keys[11] => $this->getDateCreation(),
            $keys[12] => $this->getDateLecture(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonTTypeNotificationAgent) {
                $result['CommonTTypeNotificationAgent'] = $this->aCommonTTypeNotificationAgent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTNotificationAgentPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdNotification($value);
                break;
            case 1:
                $this->setIdAgent($value);
                break;
            case 2:
                $this->setIdType($value);
                break;
            case 3:
                $this->setServiceId($value);
                break;
            case 4:
                $this->setOrganisme($value);
                break;
            case 5:
                $this->setReferenceObjet($value);
                break;
            case 6:
                $this->setLibelleNotification($value);
                break;
            case 7:
                $this->setDescription($value);
                break;
            case 8:
                $this->setNotificationLue($value);
                break;
            case 9:
                $this->setNotificationActif($value);
                break;
            case 10:
                $this->setUrlNotification($value);
                break;
            case 11:
                $this->setDateCreation($value);
                break;
            case 12:
                $this->setDateLecture($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTNotificationAgentPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdNotification($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdAgent($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdType($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setServiceId($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setOrganisme($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setReferenceObjet($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setLibelleNotification($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setDescription($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setNotificationLue($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setNotificationActif($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setUrlNotification($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setDateCreation($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setDateLecture($arr[$keys[12]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTNotificationAgentPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTNotificationAgentPeer::ID_NOTIFICATION)) $criteria->add(CommonTNotificationAgentPeer::ID_NOTIFICATION, $this->id_notification);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::ID_AGENT)) $criteria->add(CommonTNotificationAgentPeer::ID_AGENT, $this->id_agent);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::ID_TYPE)) $criteria->add(CommonTNotificationAgentPeer::ID_TYPE, $this->id_type);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::SERVICE_ID)) $criteria->add(CommonTNotificationAgentPeer::SERVICE_ID, $this->service_id);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::ORGANISME)) $criteria->add(CommonTNotificationAgentPeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::REFERENCE_OBJET)) $criteria->add(CommonTNotificationAgentPeer::REFERENCE_OBJET, $this->reference_objet);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::LIBELLE_NOTIFICATION)) $criteria->add(CommonTNotificationAgentPeer::LIBELLE_NOTIFICATION, $this->libelle_notification);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::DESCRIPTION)) $criteria->add(CommonTNotificationAgentPeer::DESCRIPTION, $this->description);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::NOTIFICATION_LUE)) $criteria->add(CommonTNotificationAgentPeer::NOTIFICATION_LUE, $this->notification_lue);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::NOTIFICATION_ACTIF)) $criteria->add(CommonTNotificationAgentPeer::NOTIFICATION_ACTIF, $this->notification_actif);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::URL_NOTIFICATION)) $criteria->add(CommonTNotificationAgentPeer::URL_NOTIFICATION, $this->url_notification);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::DATE_CREATION)) $criteria->add(CommonTNotificationAgentPeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonTNotificationAgentPeer::DATE_LECTURE)) $criteria->add(CommonTNotificationAgentPeer::DATE_LECTURE, $this->date_lecture);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTNotificationAgentPeer::DATABASE_NAME);
        $criteria->add(CommonTNotificationAgentPeer::ID_NOTIFICATION, $this->id_notification);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdNotification();
    }

    /**
     * Generic method to set the primary key (id_notification column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdNotification($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdNotification();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTNotificationAgent (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdAgent($this->getIdAgent());
        $copyObj->setIdType($this->getIdType());
        $copyObj->setServiceId($this->getServiceId());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setReferenceObjet($this->getReferenceObjet());
        $copyObj->setLibelleNotification($this->getLibelleNotification());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setNotificationLue($this->getNotificationLue());
        $copyObj->setNotificationActif($this->getNotificationActif());
        $copyObj->setUrlNotification($this->getUrlNotification());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateLecture($this->getDateLecture());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdNotification(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTNotificationAgent Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTNotificationAgentPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTNotificationAgentPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonTTypeNotificationAgent object.
     *
     * @param   CommonTTypeNotificationAgent $v
     * @return CommonTNotificationAgent The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonTTypeNotificationAgent(CommonTTypeNotificationAgent $v = null)
    {
        if ($v === null) {
            $this->setIdType(NULL);
        } else {
            $this->setIdType($v->getIdType());
        }

        $this->aCommonTTypeNotificationAgent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonTTypeNotificationAgent object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTNotificationAgent($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonTTypeNotificationAgent object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonTTypeNotificationAgent The associated CommonTTypeNotificationAgent object.
     * @throws PropelException
     */
    public function getCommonTTypeNotificationAgent(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonTTypeNotificationAgent === null && ($this->id_type !== null) && $doQuery) {
            $this->aCommonTTypeNotificationAgent = CommonTTypeNotificationAgentQuery::create()->findPk($this->id_type, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonTTypeNotificationAgent->addCommonTNotificationAgents($this);
             */
        }

        return $this->aCommonTTypeNotificationAgent;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_notification = null;
        $this->id_agent = null;
        $this->id_type = null;
        $this->service_id = null;
        $this->organisme = null;
        $this->reference_objet = null;
        $this->libelle_notification = null;
        $this->description = null;
        $this->notification_lue = null;
        $this->notification_actif = null;
        $this->url_notification = null;
        $this->date_creation = null;
        $this->date_lecture = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aCommonTTypeNotificationAgent instanceof Persistent) {
              $this->aCommonTTypeNotificationAgent->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aCommonTTypeNotificationAgent = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTNotificationAgentPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
