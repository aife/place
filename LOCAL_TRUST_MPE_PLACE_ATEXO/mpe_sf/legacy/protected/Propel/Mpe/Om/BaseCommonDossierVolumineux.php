<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonBlobFile;
use Application\Propel\Mpe\CommonBlobFileQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonDossierVolumineux;
use Application\Propel\Mpe\CommonDossierVolumineuxPeer;
use Application\Propel\Mpe\CommonDossierVolumineuxQuery;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangeQuery;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppeQuery;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritQuery;

/**
 * Base class that represents a row from the 'dossier_volumineux' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonDossierVolumineux extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonDossierVolumineuxPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonDossierVolumineuxPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the uuid_reference field.
     * @var        string
     */
    protected $uuid_reference;

    /**
     * The value for the nom field.
     * @var        string
     */
    protected $nom;

    /**
     * The value for the taille field.
     * @var        string
     */
    protected $taille;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the statut field.
     * Note: this column has a database default value of: 'init'
     * @var        string
     */
    protected $statut;

    /**
     * The value for the id_agent field.
     * @var        int
     */
    protected $id_agent;

    /**
     * The value for the old_id_inscrit field.
     * @var        int
     */
    protected $old_id_inscrit;

    /**
     * The value for the id_blob_descripteur field.
     * @var        int
     */
    protected $id_blob_descripteur;

    /**
     * The value for the uuid_technique field.
     * @var        string
     */
    protected $uuid_technique;

    /**
     * The value for the actif field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $actif;

    /**
     * The value for the id_blob_logfile field.
     * @var        int
     */
    protected $id_blob_logfile;

    /**
     * The value for the date_modification field.
     * @var        string
     */
    protected $date_modification;

    /**
     * The value for the id_inscrit field.
     * @var        string
     */
    protected $id_inscrit;

    /**
     * @var        CommonInscrit
     */
    protected $aCommonInscrit;

    /**
     * @var        CommonBlobFile
     */
    protected $aCommonBlobFileRelatedByIdBlobDescripteur;

    /**
     * @var        CommonBlobFile
     */
    protected $aCommonBlobFileRelatedByIdBlobLogfile;

    /**
     * @var        CommonAgent
     */
    protected $aCommonAgent;

    /**
     * @var        PropelObjectCollection|CommonEchange[] Collection to store aggregation of CommonEchange objects.
     */
    protected $collCommonEchanges;
    protected $collCommonEchangesPartial;

    /**
     * @var        PropelObjectCollection|CommonEnveloppe[] Collection to store aggregation of CommonEnveloppe objects.
     */
    protected $collCommonEnveloppes;
    protected $collCommonEnveloppesPartial;

    /**
     * @var        PropelObjectCollection|CommonConsultation[] Collection to store aggregation of CommonConsultation objects.
     */
    protected $collCommonConsultations;
    protected $collCommonConsultationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEnveloppesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->statut = 'init';
        $this->actif = false;
    }

    /**
     * Initializes internal state of BaseCommonDossierVolumineux object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [uuid_reference] column value.
     *
     * @return string
     */
    public function getUuidReference()
    {

        return $this->uuid_reference;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {

        return $this->nom;
    }

    /**
     * Get the [taille] column value.
     *
     * @return string
     */
    public function getTaille()
    {

        return $this->taille;
    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [statut] column value.
     *
     * @return string
     */
    public function getStatut()
    {

        return $this->statut;
    }

    /**
     * Get the [id_agent] column value.
     *
     * @return int
     */
    public function getIdAgent()
    {

        return $this->id_agent;
    }

    /**
     * Get the [old_id_inscrit] column value.
     *
     * @return int
     */
    public function getOldIdInscrit()
    {

        return $this->old_id_inscrit;
    }

    /**
     * Get the [id_blob_descripteur] column value.
     *
     * @return int
     */
    public function getIdBlobDescripteur()
    {

        return $this->id_blob_descripteur;
    }

    /**
     * Get the [uuid_technique] column value.
     *
     * @return string
     */
    public function getUuidTechnique()
    {

        return $this->uuid_technique;
    }

    /**
     * Get the [actif] column value.
     *
     * @return boolean
     */
    public function getActif()
    {

        return $this->actif;
    }

    /**
     * Get the [id_blob_logfile] column value.
     *
     * @return int
     */
    public function getIdBlobLogfile()
    {

        return $this->id_blob_logfile;
    }

    /**
     * Get the [optionally formatted] temporal [date_modification] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateModification($format = 'Y-m-d H:i:s')
    {
        if ($this->date_modification === null) {
            return null;
        }

        if ($this->date_modification === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_modification);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_modification, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [id_inscrit] column value.
     *
     * @return string
     */
    public function getIdInscrit()
    {

        return $this->id_inscrit;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [uuid_reference] column.
     *
     * @param string $v new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setUuidReference($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->uuid_reference !== $v) {
            $this->uuid_reference = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::UUID_REFERENCE;
        }


        return $this;
    } // setUuidReference()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::NOM;
        }


        return $this;
    } // setNom()

    /**
     * Set the value of [taille] column.
     *
     * @param string $v new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setTaille($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->taille !== $v) {
            $this->taille = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::TAILLE;
        }


        return $this;
    } // setTaille()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonDossierVolumineuxPeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Set the value of [statut] column.
     *
     * @param string $v new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setStatut($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->statut !== $v) {
            $this->statut = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::STATUT;
        }


        return $this;
    } // setStatut()

    /**
     * Set the value of [id_agent] column.
     *
     * @param int $v new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setIdAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_agent !== $v) {
            $this->id_agent = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::ID_AGENT;
        }

        if ($this->aCommonAgent !== null && $this->aCommonAgent->getId() !== $v) {
            $this->aCommonAgent = null;
        }


        return $this;
    } // setIdAgent()

    /**
     * Set the value of [old_id_inscrit] column.
     *
     * @param int $v new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setOldIdInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_id_inscrit !== $v) {
            $this->old_id_inscrit = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::OLD_ID_INSCRIT;
        }


        return $this;
    } // setOldIdInscrit()

    /**
     * Set the value of [id_blob_descripteur] column.
     *
     * @param int $v new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setIdBlobDescripteur($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_blob_descripteur !== $v) {
            $this->id_blob_descripteur = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::ID_BLOB_DESCRIPTEUR;
        }

        if ($this->aCommonBlobFileRelatedByIdBlobDescripteur !== null && $this->aCommonBlobFileRelatedByIdBlobDescripteur->getId() !== $v) {
            $this->aCommonBlobFileRelatedByIdBlobDescripteur = null;
        }


        return $this;
    } // setIdBlobDescripteur()

    /**
     * Set the value of [uuid_technique] column.
     *
     * @param string $v new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setUuidTechnique($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->uuid_technique !== $v) {
            $this->uuid_technique = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::UUID_TECHNIQUE;
        }


        return $this;
    } // setUuidTechnique()

    /**
     * Sets the value of the [actif] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setActif($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->actif !== $v) {
            $this->actif = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::ACTIF;
        }


        return $this;
    } // setActif()

    /**
     * Set the value of [id_blob_logfile] column.
     *
     * @param int $v new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setIdBlobLogfile($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_blob_logfile !== $v) {
            $this->id_blob_logfile = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::ID_BLOB_LOGFILE;
        }

        if ($this->aCommonBlobFileRelatedByIdBlobLogfile !== null && $this->aCommonBlobFileRelatedByIdBlobLogfile->getId() !== $v) {
            $this->aCommonBlobFileRelatedByIdBlobLogfile = null;
        }


        return $this;
    } // setIdBlobLogfile()

    /**
     * Sets the value of [date_modification] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setDateModification($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_modification !== null || $dt !== null) {
            $currentDateAsString = ($this->date_modification !== null && $tmpDt = new DateTime($this->date_modification)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_modification = $newDateAsString;
                $this->modifiedColumns[] = CommonDossierVolumineuxPeer::DATE_MODIFICATION;
            }
        } // if either are not null


        return $this;
    } // setDateModification()

    /**
     * Set the value of [id_inscrit] column.
     *
     * @param string $v new value
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setIdInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_inscrit !== $v) {
            $this->id_inscrit = $v;
            $this->modifiedColumns[] = CommonDossierVolumineuxPeer::ID_INSCRIT;
        }

        if ($this->aCommonInscrit !== null && $this->aCommonInscrit->getId() !== $v) {
            $this->aCommonInscrit = null;
        }


        return $this;
    } // setIdInscrit()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->statut !== 'init') {
                return false;
            }

            if ($this->actif !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->uuid_reference = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->nom = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->taille = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->date_creation = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->statut = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->id_agent = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->old_id_inscrit = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->id_blob_descripteur = ($row[$startcol + 8] !== null) ? (int) $row[$startcol + 8] : null;
            $this->uuid_technique = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->actif = ($row[$startcol + 10] !== null) ? (boolean) $row[$startcol + 10] : null;
            $this->id_blob_logfile = ($row[$startcol + 11] !== null) ? (int) $row[$startcol + 11] : null;
            $this->date_modification = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->id_inscrit = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 14; // 14 = CommonDossierVolumineuxPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonDossierVolumineux object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonAgent !== null && $this->id_agent !== $this->aCommonAgent->getId()) {
            $this->aCommonAgent = null;
        }
        if ($this->aCommonBlobFileRelatedByIdBlobDescripteur !== null && $this->id_blob_descripteur !== $this->aCommonBlobFileRelatedByIdBlobDescripteur->getId()) {
            $this->aCommonBlobFileRelatedByIdBlobDescripteur = null;
        }
        if ($this->aCommonBlobFileRelatedByIdBlobLogfile !== null && $this->id_blob_logfile !== $this->aCommonBlobFileRelatedByIdBlobLogfile->getId()) {
            $this->aCommonBlobFileRelatedByIdBlobLogfile = null;
        }
        if ($this->aCommonInscrit !== null && $this->id_inscrit !== $this->aCommonInscrit->getId()) {
            $this->aCommonInscrit = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonDossierVolumineuxPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonDossierVolumineuxPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonInscrit = null;
            $this->aCommonBlobFileRelatedByIdBlobDescripteur = null;
            $this->aCommonBlobFileRelatedByIdBlobLogfile = null;
            $this->aCommonAgent = null;
            $this->collCommonEchanges = null;

            $this->collCommonEnveloppes = null;

            $this->collCommonConsultations = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonDossierVolumineuxPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonDossierVolumineuxQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonDossierVolumineuxPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonDossierVolumineuxPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonInscrit !== null) {
                if ($this->aCommonInscrit->isModified() || $this->aCommonInscrit->isNew()) {
                    $affectedRows += $this->aCommonInscrit->save($con);
                }
                $this->setCommonInscrit($this->aCommonInscrit);
            }

            if ($this->aCommonBlobFileRelatedByIdBlobDescripteur !== null) {
                if ($this->aCommonBlobFileRelatedByIdBlobDescripteur->isModified() || $this->aCommonBlobFileRelatedByIdBlobDescripteur->isNew()) {
                    $affectedRows += $this->aCommonBlobFileRelatedByIdBlobDescripteur->save($con);
                }
                $this->setCommonBlobFileRelatedByIdBlobDescripteur($this->aCommonBlobFileRelatedByIdBlobDescripteur);
            }

            if ($this->aCommonBlobFileRelatedByIdBlobLogfile !== null) {
                if ($this->aCommonBlobFileRelatedByIdBlobLogfile->isModified() || $this->aCommonBlobFileRelatedByIdBlobLogfile->isNew()) {
                    $affectedRows += $this->aCommonBlobFileRelatedByIdBlobLogfile->save($con);
                }
                $this->setCommonBlobFileRelatedByIdBlobLogfile($this->aCommonBlobFileRelatedByIdBlobLogfile);
            }

            if ($this->aCommonAgent !== null) {
                if ($this->aCommonAgent->isModified() || $this->aCommonAgent->isNew()) {
                    $affectedRows += $this->aCommonAgent->save($con);
                }
                $this->setCommonAgent($this->aCommonAgent);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonEchangesScheduledForDeletion !== null) {
                if (!$this->commonEchangesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangesScheduledForDeletion as $commonEchange) {
                        // need to save related object because we set the relation to null
                        $commonEchange->save($con);
                    }
                    $this->commonEchangesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchanges !== null) {
                foreach ($this->collCommonEchanges as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonEnveloppesScheduledForDeletion !== null) {
                if (!$this->commonEnveloppesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEnveloppesScheduledForDeletion as $commonEnveloppe) {
                        // need to save related object because we set the relation to null
                        $commonEnveloppe->save($con);
                    }
                    $this->commonEnveloppesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEnveloppes !== null) {
                foreach ($this->collCommonEnveloppes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonConsultationsScheduledForDeletion !== null) {
                if (!$this->commonConsultationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationsScheduledForDeletion as $commonConsultation) {
                        // need to save related object because we set the relation to null
                        $commonConsultation->save($con);
                    }
                    $this->commonConsultationsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultations !== null) {
                foreach ($this->collCommonConsultations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonDossierVolumineuxPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonDossierVolumineuxPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::UUID_REFERENCE)) {
            $modifiedColumns[':p' . $index++]  = '`uuid_reference`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::TAILLE)) {
            $modifiedColumns[':p' . $index++]  = '`taille`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::STATUT)) {
            $modifiedColumns[':p' . $index++]  = '`statut`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ID_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_agent`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::OLD_ID_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`old_id_inscrit`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ID_BLOB_DESCRIPTEUR)) {
            $modifiedColumns[':p' . $index++]  = '`id_blob_descripteur`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::UUID_TECHNIQUE)) {
            $modifiedColumns[':p' . $index++]  = '`uuid_technique`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ACTIF)) {
            $modifiedColumns[':p' . $index++]  = '`actif`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ID_BLOB_LOGFILE)) {
            $modifiedColumns[':p' . $index++]  = '`id_blob_logfile`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::DATE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification`';
        }
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ID_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`id_inscrit`';
        }

        $sql = sprintf(
            'INSERT INTO `dossier_volumineux` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`uuid_reference`':
                        $stmt->bindValue($identifier, $this->uuid_reference, PDO::PARAM_STR);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`taille`':
                        $stmt->bindValue($identifier, $this->taille, PDO::PARAM_STR);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`statut`':
                        $stmt->bindValue($identifier, $this->statut, PDO::PARAM_STR);
                        break;
                    case '`id_agent`':
                        $stmt->bindValue($identifier, $this->id_agent, PDO::PARAM_INT);
                        break;
                    case '`old_id_inscrit`':
                        $stmt->bindValue($identifier, $this->old_id_inscrit, PDO::PARAM_INT);
                        break;
                    case '`id_blob_descripteur`':
                        $stmt->bindValue($identifier, $this->id_blob_descripteur, PDO::PARAM_INT);
                        break;
                    case '`uuid_technique`':
                        $stmt->bindValue($identifier, $this->uuid_technique, PDO::PARAM_STR);
                        break;
                    case '`actif`':
                        $stmt->bindValue($identifier, (int) $this->actif, PDO::PARAM_INT);
                        break;
                    case '`id_blob_logfile`':
                        $stmt->bindValue($identifier, $this->id_blob_logfile, PDO::PARAM_INT);
                        break;
                    case '`date_modification`':
                        $stmt->bindValue($identifier, $this->date_modification, PDO::PARAM_STR);
                        break;
                    case '`id_inscrit`':
                        $stmt->bindValue($identifier, $this->id_inscrit, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonInscrit !== null) {
                if (!$this->aCommonInscrit->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonInscrit->getValidationFailures());
                }
            }

            if ($this->aCommonBlobFileRelatedByIdBlobDescripteur !== null) {
                if (!$this->aCommonBlobFileRelatedByIdBlobDescripteur->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonBlobFileRelatedByIdBlobDescripteur->getValidationFailures());
                }
            }

            if ($this->aCommonBlobFileRelatedByIdBlobLogfile !== null) {
                if (!$this->aCommonBlobFileRelatedByIdBlobLogfile->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonBlobFileRelatedByIdBlobLogfile->getValidationFailures());
                }
            }

            if ($this->aCommonAgent !== null) {
                if (!$this->aCommonAgent->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonAgent->getValidationFailures());
                }
            }


            if (($retval = CommonDossierVolumineuxPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonEchanges !== null) {
                    foreach ($this->collCommonEchanges as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonEnveloppes !== null) {
                    foreach ($this->collCommonEnveloppes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonConsultations !== null) {
                    foreach ($this->collCommonConsultations as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonDossierVolumineuxPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUuidReference();
                break;
            case 2:
                return $this->getNom();
                break;
            case 3:
                return $this->getTaille();
                break;
            case 4:
                return $this->getDateCreation();
                break;
            case 5:
                return $this->getStatut();
                break;
            case 6:
                return $this->getIdAgent();
                break;
            case 7:
                return $this->getOldIdInscrit();
                break;
            case 8:
                return $this->getIdBlobDescripteur();
                break;
            case 9:
                return $this->getUuidTechnique();
                break;
            case 10:
                return $this->getActif();
                break;
            case 11:
                return $this->getIdBlobLogfile();
                break;
            case 12:
                return $this->getDateModification();
                break;
            case 13:
                return $this->getIdInscrit();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonDossierVolumineux'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonDossierVolumineux'][$this->getPrimaryKey()] = true;
        $keys = CommonDossierVolumineuxPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUuidReference(),
            $keys[2] => $this->getNom(),
            $keys[3] => $this->getTaille(),
            $keys[4] => $this->getDateCreation(),
            $keys[5] => $this->getStatut(),
            $keys[6] => $this->getIdAgent(),
            $keys[7] => $this->getOldIdInscrit(),
            $keys[8] => $this->getIdBlobDescripteur(),
            $keys[9] => $this->getUuidTechnique(),
            $keys[10] => $this->getActif(),
            $keys[11] => $this->getIdBlobLogfile(),
            $keys[12] => $this->getDateModification(),
            $keys[13] => $this->getIdInscrit(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonInscrit) {
                $result['CommonInscrit'] = $this->aCommonInscrit->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonBlobFileRelatedByIdBlobDescripteur) {
                $result['CommonBlobFileRelatedByIdBlobDescripteur'] = $this->aCommonBlobFileRelatedByIdBlobDescripteur->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonBlobFileRelatedByIdBlobLogfile) {
                $result['CommonBlobFileRelatedByIdBlobLogfile'] = $this->aCommonBlobFileRelatedByIdBlobLogfile->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonAgent) {
                $result['CommonAgent'] = $this->aCommonAgent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonEchanges) {
                $result['CommonEchanges'] = $this->collCommonEchanges->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonEnveloppes) {
                $result['CommonEnveloppes'] = $this->collCommonEnveloppes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonConsultations) {
                $result['CommonConsultations'] = $this->collCommonConsultations->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonDossierVolumineuxPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUuidReference($value);
                break;
            case 2:
                $this->setNom($value);
                break;
            case 3:
                $this->setTaille($value);
                break;
            case 4:
                $this->setDateCreation($value);
                break;
            case 5:
                $this->setStatut($value);
                break;
            case 6:
                $this->setIdAgent($value);
                break;
            case 7:
                $this->setOldIdInscrit($value);
                break;
            case 8:
                $this->setIdBlobDescripteur($value);
                break;
            case 9:
                $this->setUuidTechnique($value);
                break;
            case 10:
                $this->setActif($value);
                break;
            case 11:
                $this->setIdBlobLogfile($value);
                break;
            case 12:
                $this->setDateModification($value);
                break;
            case 13:
                $this->setIdInscrit($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonDossierVolumineuxPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setUuidReference($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNom($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setTaille($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDateCreation($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setStatut($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setIdAgent($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setOldIdInscrit($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setIdBlobDescripteur($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setUuidTechnique($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setActif($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setIdBlobLogfile($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setDateModification($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setIdInscrit($arr[$keys[13]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonDossierVolumineuxPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ID)) $criteria->add(CommonDossierVolumineuxPeer::ID, $this->id);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::UUID_REFERENCE)) $criteria->add(CommonDossierVolumineuxPeer::UUID_REFERENCE, $this->uuid_reference);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::NOM)) $criteria->add(CommonDossierVolumineuxPeer::NOM, $this->nom);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::TAILLE)) $criteria->add(CommonDossierVolumineuxPeer::TAILLE, $this->taille);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::DATE_CREATION)) $criteria->add(CommonDossierVolumineuxPeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::STATUT)) $criteria->add(CommonDossierVolumineuxPeer::STATUT, $this->statut);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ID_AGENT)) $criteria->add(CommonDossierVolumineuxPeer::ID_AGENT, $this->id_agent);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::OLD_ID_INSCRIT)) $criteria->add(CommonDossierVolumineuxPeer::OLD_ID_INSCRIT, $this->old_id_inscrit);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ID_BLOB_DESCRIPTEUR)) $criteria->add(CommonDossierVolumineuxPeer::ID_BLOB_DESCRIPTEUR, $this->id_blob_descripteur);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::UUID_TECHNIQUE)) $criteria->add(CommonDossierVolumineuxPeer::UUID_TECHNIQUE, $this->uuid_technique);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ACTIF)) $criteria->add(CommonDossierVolumineuxPeer::ACTIF, $this->actif);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ID_BLOB_LOGFILE)) $criteria->add(CommonDossierVolumineuxPeer::ID_BLOB_LOGFILE, $this->id_blob_logfile);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::DATE_MODIFICATION)) $criteria->add(CommonDossierVolumineuxPeer::DATE_MODIFICATION, $this->date_modification);
        if ($this->isColumnModified(CommonDossierVolumineuxPeer::ID_INSCRIT)) $criteria->add(CommonDossierVolumineuxPeer::ID_INSCRIT, $this->id_inscrit);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonDossierVolumineuxPeer::DATABASE_NAME);
        $criteria->add(CommonDossierVolumineuxPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonDossierVolumineux (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUuidReference($this->getUuidReference());
        $copyObj->setNom($this->getNom());
        $copyObj->setTaille($this->getTaille());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setStatut($this->getStatut());
        $copyObj->setIdAgent($this->getIdAgent());
        $copyObj->setOldIdInscrit($this->getOldIdInscrit());
        $copyObj->setIdBlobDescripteur($this->getIdBlobDescripteur());
        $copyObj->setUuidTechnique($this->getUuidTechnique());
        $copyObj->setActif($this->getActif());
        $copyObj->setIdBlobLogfile($this->getIdBlobLogfile());
        $copyObj->setDateModification($this->getDateModification());
        $copyObj->setIdInscrit($this->getIdInscrit());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonEchanges() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchange($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonEnveloppes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEnveloppe($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonConsultations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultation($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonDossierVolumineux Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonDossierVolumineuxPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonDossierVolumineuxPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonInscrit object.
     *
     * @param   CommonInscrit $v
     * @return CommonDossierVolumineux The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonInscrit(CommonInscrit $v = null)
    {
        if ($v === null) {
            $this->setIdInscrit(NULL);
        } else {
            $this->setIdInscrit($v->getId());
        }

        $this->aCommonInscrit = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonInscrit object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonDossierVolumineux($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonInscrit object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonInscrit The associated CommonInscrit object.
     * @throws PropelException
     */
    public function getCommonInscrit(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonInscrit === null && (($this->id_inscrit !== "" && $this->id_inscrit !== null)) && $doQuery) {
            $this->aCommonInscrit = CommonInscritQuery::create()->findPk($this->id_inscrit, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonInscrit->addCommonDossierVolumineuxs($this);
             */
        }

        return $this->aCommonInscrit;
    }

    /**
     * Declares an association between this object and a CommonBlobFile object.
     *
     * @param   CommonBlobFile $v
     * @return CommonDossierVolumineux The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonBlobFileRelatedByIdBlobDescripteur(CommonBlobFile $v = null)
    {
        if ($v === null) {
            $this->setIdBlobDescripteur(NULL);
        } else {
            $this->setIdBlobDescripteur($v->getId());
        }

        $this->aCommonBlobFileRelatedByIdBlobDescripteur = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonBlobFile object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonDossierVolumineuxRelatedByIdBlobDescripteur($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonBlobFile object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonBlobFile The associated CommonBlobFile object.
     * @throws PropelException
     */
    public function getCommonBlobFileRelatedByIdBlobDescripteur(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonBlobFileRelatedByIdBlobDescripteur === null && ($this->id_blob_descripteur !== null) && $doQuery) {
            $this->aCommonBlobFileRelatedByIdBlobDescripteur = CommonBlobFileQuery::create()->findPk($this->id_blob_descripteur, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonBlobFileRelatedByIdBlobDescripteur->addCommonDossierVolumineuxsRelatedByIdBlobDescripteur($this);
             */
        }

        return $this->aCommonBlobFileRelatedByIdBlobDescripteur;
    }

    /**
     * Declares an association between this object and a CommonBlobFile object.
     *
     * @param   CommonBlobFile $v
     * @return CommonDossierVolumineux The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonBlobFileRelatedByIdBlobLogfile(CommonBlobFile $v = null)
    {
        if ($v === null) {
            $this->setIdBlobLogfile(NULL);
        } else {
            $this->setIdBlobLogfile($v->getId());
        }

        $this->aCommonBlobFileRelatedByIdBlobLogfile = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonBlobFile object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonDossierVolumineuxRelatedByIdBlobLogfile($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonBlobFile object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonBlobFile The associated CommonBlobFile object.
     * @throws PropelException
     */
    public function getCommonBlobFileRelatedByIdBlobLogfile(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonBlobFileRelatedByIdBlobLogfile === null && ($this->id_blob_logfile !== null) && $doQuery) {
            $this->aCommonBlobFileRelatedByIdBlobLogfile = CommonBlobFileQuery::create()->findPk($this->id_blob_logfile, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonBlobFileRelatedByIdBlobLogfile->addCommonDossierVolumineuxsRelatedByIdBlobLogfile($this);
             */
        }

        return $this->aCommonBlobFileRelatedByIdBlobLogfile;
    }

    /**
     * Declares an association between this object and a CommonAgent object.
     *
     * @param   CommonAgent $v
     * @return CommonDossierVolumineux The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonAgent(CommonAgent $v = null)
    {
        if ($v === null) {
            $this->setIdAgent(NULL);
        } else {
            $this->setIdAgent($v->getId());
        }

        $this->aCommonAgent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonAgent object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonDossierVolumineux($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonAgent object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonAgent The associated CommonAgent object.
     * @throws PropelException
     */
    public function getCommonAgent(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonAgent === null && ($this->id_agent !== null) && $doQuery) {
            $this->aCommonAgent = CommonAgentQuery::create()->findPk($this->id_agent, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonAgent->addCommonDossierVolumineuxs($this);
             */
        }

        return $this->aCommonAgent;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonEchange' == $relationName) {
            $this->initCommonEchanges();
        }
        if ('CommonEnveloppe' == $relationName) {
            $this->initCommonEnveloppes();
        }
        if ('CommonConsultation' == $relationName) {
            $this->initCommonConsultations();
        }
    }

    /**
     * Clears out the collCommonEchanges collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonDossierVolumineux The current object (for fluent API support)
     * @see        addCommonEchanges()
     */
    public function clearCommonEchanges()
    {
        $this->collCommonEchanges = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchanges collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchanges($v = true)
    {
        $this->collCommonEchangesPartial = $v;
    }

    /**
     * Initializes the collCommonEchanges collection.
     *
     * By default this just sets the collCommonEchanges collection to an empty array (like clearcollCommonEchanges());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchanges($overrideExisting = true)
    {
        if (null !== $this->collCommonEchanges && !$overrideExisting) {
            return;
        }
        $this->collCommonEchanges = new PropelObjectCollection();
        $this->collCommonEchanges->setModel('CommonEchange');
    }

    /**
     * Gets an array of CommonEchange objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonDossierVolumineux is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchange[] List of CommonEchange objects
     * @throws PropelException
     */
    public function getCommonEchanges($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangesPartial && !$this->isNew();
        if (null === $this->collCommonEchanges || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchanges) {
                // return empty collection
                $this->initCommonEchanges();
            } else {
                $collCommonEchanges = CommonEchangeQuery::create(null, $criteria)
                    ->filterByCommonDossierVolumineux($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangesPartial && count($collCommonEchanges)) {
                      $this->initCommonEchanges(false);

                      foreach ($collCommonEchanges as $obj) {
                        if (false == $this->collCommonEchanges->contains($obj)) {
                          $this->collCommonEchanges->append($obj);
                        }
                      }

                      $this->collCommonEchangesPartial = true;
                    }

                    $collCommonEchanges->getInternalIterator()->rewind();

                    return $collCommonEchanges;
                }

                if ($partial && $this->collCommonEchanges) {
                    foreach ($this->collCommonEchanges as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchanges[] = $obj;
                        }
                    }
                }

                $this->collCommonEchanges = $collCommonEchanges;
                $this->collCommonEchangesPartial = false;
            }
        }

        return $this->collCommonEchanges;
    }

    /**
     * Sets a collection of CommonEchange objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchanges A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setCommonEchanges(PropelCollection $commonEchanges, PropelPDO $con = null)
    {
        $commonEchangesToDelete = $this->getCommonEchanges(new Criteria(), $con)->diff($commonEchanges);


        $this->commonEchangesScheduledForDeletion = $commonEchangesToDelete;

        foreach ($commonEchangesToDelete as $commonEchangeRemoved) {
            $commonEchangeRemoved->setCommonDossierVolumineux(null);
        }

        $this->collCommonEchanges = null;
        foreach ($commonEchanges as $commonEchange) {
            $this->addCommonEchange($commonEchange);
        }

        $this->collCommonEchanges = $commonEchanges;
        $this->collCommonEchangesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchange objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchange objects.
     * @throws PropelException
     */
    public function countCommonEchanges(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangesPartial && !$this->isNew();
        if (null === $this->collCommonEchanges || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchanges) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchanges());
            }
            $query = CommonEchangeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonDossierVolumineux($this)
                ->count($con);
        }

        return count($this->collCommonEchanges);
    }

    /**
     * Method called to associate a CommonEchange object to this object
     * through the CommonEchange foreign key attribute.
     *
     * @param   CommonEchange $l CommonEchange
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function addCommonEchange(CommonEchange $l)
    {
        if ($this->collCommonEchanges === null) {
            $this->initCommonEchanges();
            $this->collCommonEchangesPartial = true;
        }
        if (!in_array($l, $this->collCommonEchanges->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchange($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchange $commonEchange The commonEchange object to add.
     */
    protected function doAddCommonEchange($commonEchange)
    {
        $this->collCommonEchanges[]= $commonEchange;
        $commonEchange->setCommonDossierVolumineux($this);
    }

    /**
     * @param	CommonEchange $commonEchange The commonEchange object to remove.
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function removeCommonEchange($commonEchange)
    {
        if ($this->getCommonEchanges()->contains($commonEchange)) {
            $this->collCommonEchanges->remove($this->collCommonEchanges->search($commonEchange));
            if (null === $this->commonEchangesScheduledForDeletion) {
                $this->commonEchangesScheduledForDeletion = clone $this->collCommonEchanges;
                $this->commonEchangesScheduledForDeletion->clear();
            }
            $this->commonEchangesScheduledForDeletion[]= $commonEchange;
            $commonEchange->setCommonDossierVolumineux(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonDossierVolumineux is new, it will return
     * an empty collection; or if this CommonDossierVolumineux has previously
     * been saved, it will retrieve related CommonEchanges from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonDossierVolumineux.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchange[] List of CommonEchange objects
     */
    public function getCommonEchangesJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonEchanges($query, $con);
    }

    /**
     * Clears out the collCommonEnveloppes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonDossierVolumineux The current object (for fluent API support)
     * @see        addCommonEnveloppes()
     */
    public function clearCommonEnveloppes()
    {
        $this->collCommonEnveloppes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEnveloppesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEnveloppes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEnveloppes($v = true)
    {
        $this->collCommonEnveloppesPartial = $v;
    }

    /**
     * Initializes the collCommonEnveloppes collection.
     *
     * By default this just sets the collCommonEnveloppes collection to an empty array (like clearcollCommonEnveloppes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEnveloppes($overrideExisting = true)
    {
        if (null !== $this->collCommonEnveloppes && !$overrideExisting) {
            return;
        }
        $this->collCommonEnveloppes = new PropelObjectCollection();
        $this->collCommonEnveloppes->setModel('CommonEnveloppe');
    }

    /**
     * Gets an array of CommonEnveloppe objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonDossierVolumineux is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEnveloppe[] List of CommonEnveloppe objects
     * @throws PropelException
     */
    public function getCommonEnveloppes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEnveloppesPartial && !$this->isNew();
        if (null === $this->collCommonEnveloppes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEnveloppes) {
                // return empty collection
                $this->initCommonEnveloppes();
            } else {
                $collCommonEnveloppes = CommonEnveloppeQuery::create(null, $criteria)
                    ->filterByCommonDossierVolumineux($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEnveloppesPartial && count($collCommonEnveloppes)) {
                      $this->initCommonEnveloppes(false);

                      foreach ($collCommonEnveloppes as $obj) {
                        if (false == $this->collCommonEnveloppes->contains($obj)) {
                          $this->collCommonEnveloppes->append($obj);
                        }
                      }

                      $this->collCommonEnveloppesPartial = true;
                    }

                    $collCommonEnveloppes->getInternalIterator()->rewind();

                    return $collCommonEnveloppes;
                }

                if ($partial && $this->collCommonEnveloppes) {
                    foreach ($this->collCommonEnveloppes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEnveloppes[] = $obj;
                        }
                    }
                }

                $this->collCommonEnveloppes = $collCommonEnveloppes;
                $this->collCommonEnveloppesPartial = false;
            }
        }

        return $this->collCommonEnveloppes;
    }

    /**
     * Sets a collection of CommonEnveloppe objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEnveloppes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setCommonEnveloppes(PropelCollection $commonEnveloppes, PropelPDO $con = null)
    {
        $commonEnveloppesToDelete = $this->getCommonEnveloppes(new Criteria(), $con)->diff($commonEnveloppes);


        $this->commonEnveloppesScheduledForDeletion = $commonEnveloppesToDelete;

        foreach ($commonEnveloppesToDelete as $commonEnveloppeRemoved) {
            $commonEnveloppeRemoved->setCommonDossierVolumineux(null);
        }

        $this->collCommonEnveloppes = null;
        foreach ($commonEnveloppes as $commonEnveloppe) {
            $this->addCommonEnveloppe($commonEnveloppe);
        }

        $this->collCommonEnveloppes = $commonEnveloppes;
        $this->collCommonEnveloppesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEnveloppe objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEnveloppe objects.
     * @throws PropelException
     */
    public function countCommonEnveloppes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEnveloppesPartial && !$this->isNew();
        if (null === $this->collCommonEnveloppes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEnveloppes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEnveloppes());
            }
            $query = CommonEnveloppeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonDossierVolumineux($this)
                ->count($con);
        }

        return count($this->collCommonEnveloppes);
    }

    /**
     * Method called to associate a CommonEnveloppe object to this object
     * through the CommonEnveloppe foreign key attribute.
     *
     * @param   CommonEnveloppe $l CommonEnveloppe
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function addCommonEnveloppe(CommonEnveloppe $l)
    {
        if ($this->collCommonEnveloppes === null) {
            $this->initCommonEnveloppes();
            $this->collCommonEnveloppesPartial = true;
        }
        if (!in_array($l, $this->collCommonEnveloppes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEnveloppe($l);
        }

        return $this;
    }

    /**
     * @param	CommonEnveloppe $commonEnveloppe The commonEnveloppe object to add.
     */
    protected function doAddCommonEnveloppe($commonEnveloppe)
    {
        $this->collCommonEnveloppes[]= $commonEnveloppe;
        $commonEnveloppe->setCommonDossierVolumineux($this);
    }

    /**
     * @param	CommonEnveloppe $commonEnveloppe The commonEnveloppe object to remove.
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function removeCommonEnveloppe($commonEnveloppe)
    {
        if ($this->getCommonEnveloppes()->contains($commonEnveloppe)) {
            $this->collCommonEnveloppes->remove($this->collCommonEnveloppes->search($commonEnveloppe));
            if (null === $this->commonEnveloppesScheduledForDeletion) {
                $this->commonEnveloppesScheduledForDeletion = clone $this->collCommonEnveloppes;
                $this->commonEnveloppesScheduledForDeletion->clear();
            }
            $this->commonEnveloppesScheduledForDeletion[]= $commonEnveloppe;
            $commonEnveloppe->setCommonDossierVolumineux(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonConsultations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonDossierVolumineux The current object (for fluent API support)
     * @see        addCommonConsultations()
     */
    public function clearCommonConsultations()
    {
        $this->collCommonConsultations = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultations collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultations($v = true)
    {
        $this->collCommonConsultationsPartial = $v;
    }

    /**
     * Initializes the collCommonConsultations collection.
     *
     * By default this just sets the collCommonConsultations collection to an empty array (like clearcollCommonConsultations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultations($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultations && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultations = new PropelObjectCollection();
        $this->collCommonConsultations->setModel('CommonConsultation');
    }

    /**
     * Gets an array of CommonConsultation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonDossierVolumineux is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     * @throws PropelException
     */
    public function getCommonConsultations($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonConsultations || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultations) {
                // return empty collection
                $this->initCommonConsultations();
            } else {
                $collCommonConsultations = CommonConsultationQuery::create(null, $criteria)
                    ->filterByCommonDossierVolumineux($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationsPartial && count($collCommonConsultations)) {
                      $this->initCommonConsultations(false);

                      foreach ($collCommonConsultations as $obj) {
                        if (false == $this->collCommonConsultations->contains($obj)) {
                          $this->collCommonConsultations->append($obj);
                        }
                      }

                      $this->collCommonConsultationsPartial = true;
                    }

                    $collCommonConsultations->getInternalIterator()->rewind();

                    return $collCommonConsultations;
                }

                if ($partial && $this->collCommonConsultations) {
                    foreach ($this->collCommonConsultations as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultations[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultations = $collCommonConsultations;
                $this->collCommonConsultationsPartial = false;
            }
        }

        return $this->collCommonConsultations;
    }

    /**
     * Sets a collection of CommonConsultation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultations A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function setCommonConsultations(PropelCollection $commonConsultations, PropelPDO $con = null)
    {
        $commonConsultationsToDelete = $this->getCommonConsultations(new Criteria(), $con)->diff($commonConsultations);


        $this->commonConsultationsScheduledForDeletion = $commonConsultationsToDelete;

        foreach ($commonConsultationsToDelete as $commonConsultationRemoved) {
            $commonConsultationRemoved->setCommonDossierVolumineux(null);
        }

        $this->collCommonConsultations = null;
        foreach ($commonConsultations as $commonConsultation) {
            $this->addCommonConsultation($commonConsultation);
        }

        $this->collCommonConsultations = $commonConsultations;
        $this->collCommonConsultationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultation objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultation objects.
     * @throws PropelException
     */
    public function countCommonConsultations(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationsPartial && !$this->isNew();
        if (null === $this->collCommonConsultations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultations());
            }
            $query = CommonConsultationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonDossierVolumineux($this)
                ->count($con);
        }

        return count($this->collCommonConsultations);
    }

    /**
     * Method called to associate a CommonConsultation object to this object
     * through the CommonConsultation foreign key attribute.
     *
     * @param   CommonConsultation $l CommonConsultation
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function addCommonConsultation(CommonConsultation $l)
    {
        if ($this->collCommonConsultations === null) {
            $this->initCommonConsultations();
            $this->collCommonConsultationsPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultations->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultation($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultation $commonConsultation The commonConsultation object to add.
     */
    protected function doAddCommonConsultation($commonConsultation)
    {
        $this->collCommonConsultations[]= $commonConsultation;
        $commonConsultation->setCommonDossierVolumineux($this);
    }

    /**
     * @param	CommonConsultation $commonConsultation The commonConsultation object to remove.
     * @return CommonDossierVolumineux The current object (for fluent API support)
     */
    public function removeCommonConsultation($commonConsultation)
    {
        if ($this->getCommonConsultations()->contains($commonConsultation)) {
            $this->collCommonConsultations->remove($this->collCommonConsultations->search($commonConsultation));
            if (null === $this->commonConsultationsScheduledForDeletion) {
                $this->commonConsultationsScheduledForDeletion = clone $this->collCommonConsultations;
                $this->commonConsultationsScheduledForDeletion->clear();
            }
            $this->commonConsultationsScheduledForDeletion[]= $commonConsultation;
            $commonConsultation->setCommonDossierVolumineux(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonDossierVolumineux is new, it will return
     * an empty collection; or if this CommonDossierVolumineux has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonDossierVolumineux.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceValidation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceValidation', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonDossierVolumineux is new, it will return
     * an empty collection; or if this CommonDossierVolumineux has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonDossierVolumineux.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonTTypeContrat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonTTypeContrat', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonDossierVolumineux is new, it will return
     * an empty collection; or if this CommonDossierVolumineux has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonDossierVolumineux.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceId($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceId', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonDossierVolumineux is new, it will return
     * an empty collection; or if this CommonDossierVolumineux has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonDossierVolumineux.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonConsultationRelatedByReferenceConsultationInit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonConsultationRelatedByReferenceConsultationInit', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonDossierVolumineux is new, it will return
     * an empty collection; or if this CommonDossierVolumineux has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonDossierVolumineux.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonServiceRelatedByServiceValidationIntermediaire($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonServiceRelatedByServiceValidationIntermediaire', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonDossierVolumineux is new, it will return
     * an empty collection; or if this CommonDossierVolumineux has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonDossierVolumineux.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonOperations($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOperations', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonDossierVolumineux is new, it will return
     * an empty collection; or if this CommonDossierVolumineux has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonDossierVolumineux.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonDossierVolumineux is new, it will return
     * an empty collection; or if this CommonDossierVolumineux has previously
     * been saved, it will retrieve related CommonConsultations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonDossierVolumineux.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultation[] List of CommonConsultation objects
     */
    public function getCommonConsultationsJoinCommonPlateformeVirtuelle($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationQuery::create(null, $criteria);
        $query->joinWith('CommonPlateformeVirtuelle', $join_behavior);

        return $this->getCommonConsultations($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->uuid_reference = null;
        $this->nom = null;
        $this->taille = null;
        $this->date_creation = null;
        $this->statut = null;
        $this->id_agent = null;
        $this->old_id_inscrit = null;
        $this->id_blob_descripteur = null;
        $this->uuid_technique = null;
        $this->actif = null;
        $this->id_blob_logfile = null;
        $this->date_modification = null;
        $this->id_inscrit = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonEchanges) {
                foreach ($this->collCommonEchanges as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonEnveloppes) {
                foreach ($this->collCommonEnveloppes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonConsultations) {
                foreach ($this->collCommonConsultations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonInscrit instanceof Persistent) {
              $this->aCommonInscrit->clearAllReferences($deep);
            }
            if ($this->aCommonBlobFileRelatedByIdBlobDescripteur instanceof Persistent) {
              $this->aCommonBlobFileRelatedByIdBlobDescripteur->clearAllReferences($deep);
            }
            if ($this->aCommonBlobFileRelatedByIdBlobLogfile instanceof Persistent) {
              $this->aCommonBlobFileRelatedByIdBlobLogfile->clearAllReferences($deep);
            }
            if ($this->aCommonAgent instanceof Persistent) {
              $this->aCommonAgent->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonEchanges instanceof PropelCollection) {
            $this->collCommonEchanges->clearIterator();
        }
        $this->collCommonEchanges = null;
        if ($this->collCommonEnveloppes instanceof PropelCollection) {
            $this->collCommonEnveloppes->clearIterator();
        }
        $this->collCommonEnveloppes = null;
        if ($this->collCommonConsultations instanceof PropelCollection) {
            $this->collCommonConsultations->clearIterator();
        }
        $this->collCommonConsultations = null;
        $this->aCommonInscrit = null;
        $this->aCommonBlobFileRelatedByIdBlobDescripteur = null;
        $this->aCommonBlobFileRelatedByIdBlobLogfile = null;
        $this->aCommonAgent = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonDossierVolumineuxPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
