<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTTypeAnnonceConsultation;
use Application\Propel\Mpe\CommonTTypeAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTTypeAnnonceConsultationQuery;

/**
 * Base class that represents a query for the 't_type_annonce_consultation' table.
 *
 *
 *
 * @method CommonTTypeAnnonceConsultationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTTypeAnnonceConsultationQuery orderByLibelle($order = Criteria::ASC) Order by the libelle column
 * @method CommonTTypeAnnonceConsultationQuery orderByVisible($order = Criteria::ASC) Order by the visible column
 *
 * @method CommonTTypeAnnonceConsultationQuery groupById() Group by the id column
 * @method CommonTTypeAnnonceConsultationQuery groupByLibelle() Group by the libelle column
 * @method CommonTTypeAnnonceConsultationQuery groupByVisible() Group by the visible column
 *
 * @method CommonTTypeAnnonceConsultationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTTypeAnnonceConsultationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTTypeAnnonceConsultationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTTypeAnnonceConsultationQuery leftJoinCommonTAnnonceConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTAnnonceConsultation relation
 * @method CommonTTypeAnnonceConsultationQuery rightJoinCommonTAnnonceConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTAnnonceConsultation relation
 * @method CommonTTypeAnnonceConsultationQuery innerJoinCommonTAnnonceConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTAnnonceConsultation relation
 *
 * @method CommonTTypeAnnonceConsultation findOne(PropelPDO $con = null) Return the first CommonTTypeAnnonceConsultation matching the query
 * @method CommonTTypeAnnonceConsultation findOneOrCreate(PropelPDO $con = null) Return the first CommonTTypeAnnonceConsultation matching the query, or a new CommonTTypeAnnonceConsultation object populated from the query conditions when no match is found
 *
 * @method CommonTTypeAnnonceConsultation findOneByLibelle(string $libelle) Return the first CommonTTypeAnnonceConsultation filtered by the libelle column
 * @method CommonTTypeAnnonceConsultation findOneByVisible(string $visible) Return the first CommonTTypeAnnonceConsultation filtered by the visible column
 *
 * @method array findById(int $id) Return CommonTTypeAnnonceConsultation objects filtered by the id column
 * @method array findByLibelle(string $libelle) Return CommonTTypeAnnonceConsultation objects filtered by the libelle column
 * @method array findByVisible(string $visible) Return CommonTTypeAnnonceConsultation objects filtered by the visible column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTTypeAnnonceConsultationQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTTypeAnnonceConsultationQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTTypeAnnonceConsultation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTTypeAnnonceConsultationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTTypeAnnonceConsultationQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTTypeAnnonceConsultationQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTTypeAnnonceConsultationQuery) {
            return $criteria;
        }
        $query = new CommonTTypeAnnonceConsultationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTTypeAnnonceConsultation|CommonTTypeAnnonceConsultation[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTTypeAnnonceConsultationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTTypeAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTypeAnnonceConsultation A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTTypeAnnonceConsultation A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `libelle`, `visible` FROM `t_type_annonce_consultation` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTTypeAnnonceConsultation();
            $obj->hydrate($row);
            CommonTTypeAnnonceConsultationPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTTypeAnnonceConsultation|CommonTTypeAnnonceConsultation[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTTypeAnnonceConsultation[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTTypeAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTTypeAnnonceConsultationPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTTypeAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTTypeAnnonceConsultationPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTTypeAnnonceConsultationPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTTypeAnnonceConsultationPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTTypeAnnonceConsultationPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the libelle column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelle('fooValue');   // WHERE libelle = 'fooValue'
     * $query->filterByLibelle('%fooValue%'); // WHERE libelle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByLibelle($libelle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelle)) {
                $libelle = str_replace('*', '%', $libelle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeAnnonceConsultationPeer::LIBELLE, $libelle, $comparison);
    }

    /**
     * Filter the query on the visible column
     *
     * Example usage:
     * <code>
     * $query->filterByVisible('fooValue');   // WHERE visible = 'fooValue'
     * $query->filterByVisible('%fooValue%'); // WHERE visible LIKE '%fooValue%'
     * </code>
     *
     * @param     string $visible The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTTypeAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByVisible($visible = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($visible)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $visible)) {
                $visible = str_replace('*', '%', $visible);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTTypeAnnonceConsultationPeer::VISIBLE, $visible, $comparison);
    }

    /**
     * Filter the query by a related CommonTAnnonceConsultation object
     *
     * @param   CommonTAnnonceConsultation|PropelObjectCollection $commonTAnnonceConsultation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTTypeAnnonceConsultationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTAnnonceConsultation($commonTAnnonceConsultation, $comparison = null)
    {
        if ($commonTAnnonceConsultation instanceof CommonTAnnonceConsultation) {
            return $this
                ->addUsingAlias(CommonTTypeAnnonceConsultationPeer::ID, $commonTAnnonceConsultation->getIdTypeAnnonce(), $comparison);
        } elseif ($commonTAnnonceConsultation instanceof PropelObjectCollection) {
            return $this
                ->useCommonTAnnonceConsultationQuery()
                ->filterByPrimaryKeys($commonTAnnonceConsultation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTAnnonceConsultation() only accepts arguments of type CommonTAnnonceConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTAnnonceConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTTypeAnnonceConsultationQuery The current query, for fluid interface
     */
    public function joinCommonTAnnonceConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTAnnonceConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTAnnonceConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonTAnnonceConsultation relation CommonTAnnonceConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTAnnonceConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonTAnnonceConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTAnnonceConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTAnnonceConsultation', '\Application\Propel\Mpe\CommonTAnnonceConsultationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTTypeAnnonceConsultation $commonTTypeAnnonceConsultation Object to remove from the list of results
     *
     * @return CommonTTypeAnnonceConsultationQuery The current query, for fluid interface
     */
    public function prune($commonTTypeAnnonceConsultation = null)
    {
        if ($commonTTypeAnnonceConsultation) {
            $this->addUsingAlias(CommonTTypeAnnonceConsultationPeer::ID, $commonTTypeAnnonceConsultation->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
