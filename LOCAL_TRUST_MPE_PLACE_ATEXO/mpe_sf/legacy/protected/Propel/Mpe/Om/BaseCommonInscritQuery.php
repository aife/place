<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAlerte;
use Application\Propel\Mpe\CommonCertificatsEntreprises;
use Application\Propel\Mpe\CommonDossierVolumineux;
use Application\Propel\Mpe\CommonEchangeDestinataire;
use Application\Propel\Mpe\CommonHistorisationMotDePasse;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonInscritQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonPanierEntreprise;
use Application\Propel\Mpe\CommonQuestionDCE;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Propel\Mpe\CommonReponseInscritFormulaireConsultation;
use Application\Propel\Mpe\CommonSsoEntreprise;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTListeLotsCandidature;
use Application\Propel\Mpe\CommonTReponseElecFormulaire;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Propel\Mpe\CommonTmpSiretIncorrect;
use Application\Propel\Mpe\CommonTraceOperationsInscrit;
use Application\Propel\Mpe\Entreprise;

/**
 * Base class that represents a query for the 'Inscrit' table.
 *
 *
 *
 * @method CommonInscritQuery orderByOldId($order = Criteria::ASC) Order by the old_id column
 * @method CommonInscritQuery orderByEntrepriseId($order = Criteria::ASC) Order by the entreprise_id column
 * @method CommonInscritQuery orderByIdEtablissement($order = Criteria::ASC) Order by the id_etablissement column
 * @method CommonInscritQuery orderByLogin($order = Criteria::ASC) Order by the login column
 * @method CommonInscritQuery orderByMdp($order = Criteria::ASC) Order by the mdp column
 * @method CommonInscritQuery orderByNumCert($order = Criteria::ASC) Order by the num_cert column
 * @method CommonInscritQuery orderByCert($order = Criteria::ASC) Order by the cert column
 * @method CommonInscritQuery orderByCivilite($order = Criteria::ASC) Order by the civilite column
 * @method CommonInscritQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method CommonInscritQuery orderByPrenom($order = Criteria::ASC) Order by the prenom column
 * @method CommonInscritQuery orderByAdresse($order = Criteria::ASC) Order by the adresse column
 * @method CommonInscritQuery orderByCodepostal($order = Criteria::ASC) Order by the codepostal column
 * @method CommonInscritQuery orderByVille($order = Criteria::ASC) Order by the ville column
 * @method CommonInscritQuery orderByPays($order = Criteria::ASC) Order by the pays column
 * @method CommonInscritQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method CommonInscritQuery orderByTelephone($order = Criteria::ASC) Order by the telephone column
 * @method CommonInscritQuery orderByCategorie($order = Criteria::ASC) Order by the categorie column
 * @method CommonInscritQuery orderByMotstitreresume($order = Criteria::ASC) Order by the motstitreresume column
 * @method CommonInscritQuery orderByPeriode($order = Criteria::ASC) Order by the periode column
 * @method CommonInscritQuery orderBySiret($order = Criteria::ASC) Order by the siret column
 * @method CommonInscritQuery orderByFax($order = Criteria::ASC) Order by the fax column
 * @method CommonInscritQuery orderByCodeCpv($order = Criteria::ASC) Order by the code_cpv column
 * @method CommonInscritQuery orderByIdLangue($order = Criteria::ASC) Order by the id_langue column
 * @method CommonInscritQuery orderByProfil($order = Criteria::ASC) Order by the profil column
 * @method CommonInscritQuery orderByAdresse2($order = Criteria::ASC) Order by the adresse2 column
 * @method CommonInscritQuery orderByBloque($order = Criteria::ASC) Order by the bloque column
 * @method CommonInscritQuery orderByIdInitial($order = Criteria::ASC) Order by the id_initial column
 * @method CommonInscritQuery orderByInscritAnnuaireDefense($order = Criteria::ASC) Order by the inscrit_annuaire_defense column
 * @method CommonInscritQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonInscritQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonInscritQuery orderByTentativesMdp($order = Criteria::ASC) Order by the tentatives_mdp column
 * @method CommonInscritQuery orderByUid($order = Criteria::ASC) Order by the uid column
 * @method CommonInscritQuery orderByTypeHash($order = Criteria::ASC) Order by the type_hash column
 * @method CommonInscritQuery orderByIdExterne($order = Criteria::ASC) Order by the id_externe column
 * @method CommonInscritQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonInscritQuery orderByDateValidationRgpd($order = Criteria::ASC) Order by the date_validation_rgpd column
 * @method CommonInscritQuery orderByRgpdCommunicationPlace($order = Criteria::ASC) Order by the rgpd_communication_place column
 * @method CommonInscritQuery orderByRgpdEnquete($order = Criteria::ASC) Order by the rgpd_enquete column
 * @method CommonInscritQuery orderByRgpdCommunication($order = Criteria::ASC) Order by the rgpd_communication column
 * @method CommonInscritQuery orderByDeletedAt($order = Criteria::ASC) Order by the deleted_at column
 * @method CommonInscritQuery orderByDeleted($order = Criteria::ASC) Order by the deleted column
 *
 * @method CommonInscritQuery groupByOldId() Group by the old_id column
 * @method CommonInscritQuery groupByEntrepriseId() Group by the entreprise_id column
 * @method CommonInscritQuery groupByIdEtablissement() Group by the id_etablissement column
 * @method CommonInscritQuery groupByLogin() Group by the login column
 * @method CommonInscritQuery groupByMdp() Group by the mdp column
 * @method CommonInscritQuery groupByNumCert() Group by the num_cert column
 * @method CommonInscritQuery groupByCert() Group by the cert column
 * @method CommonInscritQuery groupByCivilite() Group by the civilite column
 * @method CommonInscritQuery groupByNom() Group by the nom column
 * @method CommonInscritQuery groupByPrenom() Group by the prenom column
 * @method CommonInscritQuery groupByAdresse() Group by the adresse column
 * @method CommonInscritQuery groupByCodepostal() Group by the codepostal column
 * @method CommonInscritQuery groupByVille() Group by the ville column
 * @method CommonInscritQuery groupByPays() Group by the pays column
 * @method CommonInscritQuery groupByEmail() Group by the email column
 * @method CommonInscritQuery groupByTelephone() Group by the telephone column
 * @method CommonInscritQuery groupByCategorie() Group by the categorie column
 * @method CommonInscritQuery groupByMotstitreresume() Group by the motstitreresume column
 * @method CommonInscritQuery groupByPeriode() Group by the periode column
 * @method CommonInscritQuery groupBySiret() Group by the siret column
 * @method CommonInscritQuery groupByFax() Group by the fax column
 * @method CommonInscritQuery groupByCodeCpv() Group by the code_cpv column
 * @method CommonInscritQuery groupByIdLangue() Group by the id_langue column
 * @method CommonInscritQuery groupByProfil() Group by the profil column
 * @method CommonInscritQuery groupByAdresse2() Group by the adresse2 column
 * @method CommonInscritQuery groupByBloque() Group by the bloque column
 * @method CommonInscritQuery groupByIdInitial() Group by the id_initial column
 * @method CommonInscritQuery groupByInscritAnnuaireDefense() Group by the inscrit_annuaire_defense column
 * @method CommonInscritQuery groupByDateCreation() Group by the date_creation column
 * @method CommonInscritQuery groupByDateModification() Group by the date_modification column
 * @method CommonInscritQuery groupByTentativesMdp() Group by the tentatives_mdp column
 * @method CommonInscritQuery groupByUid() Group by the uid column
 * @method CommonInscritQuery groupByTypeHash() Group by the type_hash column
 * @method CommonInscritQuery groupByIdExterne() Group by the id_externe column
 * @method CommonInscritQuery groupById() Group by the id column
 * @method CommonInscritQuery groupByDateValidationRgpd() Group by the date_validation_rgpd column
 * @method CommonInscritQuery groupByRgpdCommunicationPlace() Group by the rgpd_communication_place column
 * @method CommonInscritQuery groupByRgpdEnquete() Group by the rgpd_enquete column
 * @method CommonInscritQuery groupByRgpdCommunication() Group by the rgpd_communication column
 * @method CommonInscritQuery groupByDeletedAt() Group by the deleted_at column
 * @method CommonInscritQuery groupByDeleted() Group by the deleted column
 *
 * @method CommonInscritQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonInscritQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonInscritQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonInscritQuery leftJoinCommonTEtablissementRelatedByIdEtablissement($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTEtablissementRelatedByIdEtablissement relation
 * @method CommonInscritQuery rightJoinCommonTEtablissementRelatedByIdEtablissement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTEtablissementRelatedByIdEtablissement relation
 * @method CommonInscritQuery innerJoinCommonTEtablissementRelatedByIdEtablissement($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTEtablissementRelatedByIdEtablissement relation
 *
 * @method CommonInscritQuery leftJoinEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the Entreprise relation
 * @method CommonInscritQuery rightJoinEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Entreprise relation
 * @method CommonInscritQuery innerJoinEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the Entreprise relation
 *
 * @method CommonInscritQuery leftJoinCommonAlerte($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAlerte relation
 * @method CommonInscritQuery rightJoinCommonAlerte($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAlerte relation
 * @method CommonInscritQuery innerJoinCommonAlerte($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAlerte relation
 *
 * @method CommonInscritQuery leftJoinCommonCertificatsEntreprises($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonCertificatsEntreprises relation
 * @method CommonInscritQuery rightJoinCommonCertificatsEntreprises($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonCertificatsEntreprises relation
 * @method CommonInscritQuery innerJoinCommonCertificatsEntreprises($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonCertificatsEntreprises relation
 *
 * @method CommonInscritQuery leftJoinCommonEchangeDestinataire($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDestinataire relation
 * @method CommonInscritQuery rightJoinCommonEchangeDestinataire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDestinataire relation
 * @method CommonInscritQuery innerJoinCommonEchangeDestinataire($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDestinataire relation
 *
 * @method CommonInscritQuery leftJoinCommonOffres($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOffres relation
 * @method CommonInscritQuery rightJoinCommonOffres($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOffres relation
 * @method CommonInscritQuery innerJoinCommonOffres($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOffres relation
 *
 * @method CommonInscritQuery leftJoinCommonPanierEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonPanierEntreprise relation
 * @method CommonInscritQuery rightJoinCommonPanierEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonPanierEntreprise relation
 * @method CommonInscritQuery innerJoinCommonPanierEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonPanierEntreprise relation
 *
 * @method CommonInscritQuery leftJoinCommonQuestionDCE($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonQuestionDCE relation
 * @method CommonInscritQuery rightJoinCommonQuestionDCE($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonQuestionDCE relation
 * @method CommonInscritQuery innerJoinCommonQuestionDCE($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonQuestionDCE relation
 *
 * @method CommonInscritQuery leftJoinCommonReponseInscritFormulaireConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonReponseInscritFormulaireConsultation relation
 * @method CommonInscritQuery rightJoinCommonReponseInscritFormulaireConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonReponseInscritFormulaireConsultation relation
 * @method CommonInscritQuery innerJoinCommonReponseInscritFormulaireConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonReponseInscritFormulaireConsultation relation
 *
 * @method CommonInscritQuery leftJoinCommonTelechargement($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTelechargement relation
 * @method CommonInscritQuery rightJoinCommonTelechargement($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTelechargement relation
 * @method CommonInscritQuery innerJoinCommonTelechargement($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTelechargement relation
 *
 * @method CommonInscritQuery leftJoinCommonDossierVolumineux($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonDossierVolumineux relation
 * @method CommonInscritQuery rightJoinCommonDossierVolumineux($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonDossierVolumineux relation
 * @method CommonInscritQuery innerJoinCommonDossierVolumineux($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonDossierVolumineux relation
 *
 * @method CommonInscritQuery leftJoinCommonHistorisationMotDePasse($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonHistorisationMotDePasse relation
 * @method CommonInscritQuery rightJoinCommonHistorisationMotDePasse($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonHistorisationMotDePasse relation
 * @method CommonInscritQuery innerJoinCommonHistorisationMotDePasse($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonHistorisationMotDePasse relation
 *
 * @method CommonInscritQuery leftJoinCommonQuestionsDce($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonQuestionsDce relation
 * @method CommonInscritQuery rightJoinCommonQuestionsDce($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonQuestionsDce relation
 * @method CommonInscritQuery innerJoinCommonQuestionsDce($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonQuestionsDce relation
 *
 * @method CommonInscritQuery leftJoinCommonSsoEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonSsoEntreprise relation
 * @method CommonInscritQuery rightJoinCommonSsoEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonSsoEntreprise relation
 * @method CommonInscritQuery innerJoinCommonSsoEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonSsoEntreprise relation
 *
 * @method CommonInscritQuery leftJoinCommonTCandidature($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCandidature relation
 * @method CommonInscritQuery rightJoinCommonTCandidature($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCandidature relation
 * @method CommonInscritQuery innerJoinCommonTCandidature($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCandidature relation
 *
 * @method CommonInscritQuery leftJoinCommonTCandidatureMps($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTCandidatureMps relation
 * @method CommonInscritQuery rightJoinCommonTCandidatureMps($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTCandidatureMps relation
 * @method CommonInscritQuery innerJoinCommonTCandidatureMps($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTCandidatureMps relation
 *
 * @method CommonInscritQuery leftJoinCommonTContactContrat($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTContactContrat relation
 * @method CommonInscritQuery rightJoinCommonTContactContrat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTContactContrat relation
 * @method CommonInscritQuery innerJoinCommonTContactContrat($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTContactContrat relation
 *
 * @method CommonInscritQuery leftJoinCommonTListeLotsCandidature($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTListeLotsCandidature relation
 * @method CommonInscritQuery rightJoinCommonTListeLotsCandidature($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTListeLotsCandidature relation
 * @method CommonInscritQuery innerJoinCommonTListeLotsCandidature($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTListeLotsCandidature relation
 *
 * @method CommonInscritQuery leftJoinCommonTReponseElecFormulaire($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTReponseElecFormulaire relation
 * @method CommonInscritQuery rightJoinCommonTReponseElecFormulaire($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTReponseElecFormulaire relation
 * @method CommonInscritQuery innerJoinCommonTReponseElecFormulaire($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTReponseElecFormulaire relation
 *
 * @method CommonInscritQuery leftJoinCommonTmpSiretIncorrect($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTmpSiretIncorrect relation
 * @method CommonInscritQuery rightJoinCommonTmpSiretIncorrect($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTmpSiretIncorrect relation
 * @method CommonInscritQuery innerJoinCommonTmpSiretIncorrect($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTmpSiretIncorrect relation
 *
 * @method CommonInscritQuery leftJoinCommonTraceOperationsInscrit($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTraceOperationsInscrit relation
 * @method CommonInscritQuery rightJoinCommonTraceOperationsInscrit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTraceOperationsInscrit relation
 * @method CommonInscritQuery innerJoinCommonTraceOperationsInscrit($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTraceOperationsInscrit relation
 *
 * @method CommonInscrit findOne(PropelPDO $con = null) Return the first CommonInscrit matching the query
 * @method CommonInscrit findOneOrCreate(PropelPDO $con = null) Return the first CommonInscrit matching the query, or a new CommonInscrit object populated from the query conditions when no match is found
 *
 * @method CommonInscrit findOneByOldId(int $old_id) Return the first CommonInscrit filtered by the old_id column
 * @method CommonInscrit findOneByEntrepriseId(int $entreprise_id) Return the first CommonInscrit filtered by the entreprise_id column
 * @method CommonInscrit findOneByIdEtablissement(int $id_etablissement) Return the first CommonInscrit filtered by the id_etablissement column
 * @method CommonInscrit findOneByLogin(string $login) Return the first CommonInscrit filtered by the login column
 * @method CommonInscrit findOneByMdp(string $mdp) Return the first CommonInscrit filtered by the mdp column
 * @method CommonInscrit findOneByNumCert(string $num_cert) Return the first CommonInscrit filtered by the num_cert column
 * @method CommonInscrit findOneByCert(string $cert) Return the first CommonInscrit filtered by the cert column
 * @method CommonInscrit findOneByCivilite(boolean $civilite) Return the first CommonInscrit filtered by the civilite column
 * @method CommonInscrit findOneByNom(string $nom) Return the first CommonInscrit filtered by the nom column
 * @method CommonInscrit findOneByPrenom(string $prenom) Return the first CommonInscrit filtered by the prenom column
 * @method CommonInscrit findOneByAdresse(string $adresse) Return the first CommonInscrit filtered by the adresse column
 * @method CommonInscrit findOneByCodepostal(string $codepostal) Return the first CommonInscrit filtered by the codepostal column
 * @method CommonInscrit findOneByVille(string $ville) Return the first CommonInscrit filtered by the ville column
 * @method CommonInscrit findOneByPays(string $pays) Return the first CommonInscrit filtered by the pays column
 * @method CommonInscrit findOneByEmail(string $email) Return the first CommonInscrit filtered by the email column
 * @method CommonInscrit findOneByTelephone(string $telephone) Return the first CommonInscrit filtered by the telephone column
 * @method CommonInscrit findOneByCategorie(string $categorie) Return the first CommonInscrit filtered by the categorie column
 * @method CommonInscrit findOneByMotstitreresume(string $motstitreresume) Return the first CommonInscrit filtered by the motstitreresume column
 * @method CommonInscrit findOneByPeriode(int $periode) Return the first CommonInscrit filtered by the periode column
 * @method CommonInscrit findOneBySiret(string $siret) Return the first CommonInscrit filtered by the siret column
 * @method CommonInscrit findOneByFax(string $fax) Return the first CommonInscrit filtered by the fax column
 * @method CommonInscrit findOneByCodeCpv(string $code_cpv) Return the first CommonInscrit filtered by the code_cpv column
 * @method CommonInscrit findOneByIdLangue(int $id_langue) Return the first CommonInscrit filtered by the id_langue column
 * @method CommonInscrit findOneByProfil(int $profil) Return the first CommonInscrit filtered by the profil column
 * @method CommonInscrit findOneByAdresse2(string $adresse2) Return the first CommonInscrit filtered by the adresse2 column
 * @method CommonInscrit findOneByBloque(string $bloque) Return the first CommonInscrit filtered by the bloque column
 * @method CommonInscrit findOneByIdInitial(int $id_initial) Return the first CommonInscrit filtered by the id_initial column
 * @method CommonInscrit findOneByInscritAnnuaireDefense(string $inscrit_annuaire_defense) Return the first CommonInscrit filtered by the inscrit_annuaire_defense column
 * @method CommonInscrit findOneByDateCreation(string $date_creation) Return the first CommonInscrit filtered by the date_creation column
 * @method CommonInscrit findOneByDateModification(string $date_modification) Return the first CommonInscrit filtered by the date_modification column
 * @method CommonInscrit findOneByTentativesMdp(int $tentatives_mdp) Return the first CommonInscrit filtered by the tentatives_mdp column
 * @method CommonInscrit findOneByUid(string $uid) Return the first CommonInscrit filtered by the uid column
 * @method CommonInscrit findOneByTypeHash(string $type_hash) Return the first CommonInscrit filtered by the type_hash column
 * @method CommonInscrit findOneByIdExterne(string $id_externe) Return the first CommonInscrit filtered by the id_externe column
 * @method CommonInscrit findOneByDateValidationRgpd(string $date_validation_rgpd) Return the first CommonInscrit filtered by the date_validation_rgpd column
 * @method CommonInscrit findOneByRgpdCommunicationPlace(boolean $rgpd_communication_place) Return the first CommonInscrit filtered by the rgpd_communication_place column
 * @method CommonInscrit findOneByRgpdEnquete(boolean $rgpd_enquete) Return the first CommonInscrit filtered by the rgpd_enquete column
 * @method CommonInscrit findOneByRgpdCommunication(boolean $rgpd_communication) Return the first CommonInscrit filtered by the rgpd_communication column
 * @method CommonInscrit findOneByDeletedAt(string $deleted_at) Return the first CommonInscrit filtered by the deleted_at column
 * @method CommonInscrit findOneByDeleted(boolean $deleted) Return the first CommonInscrit filtered by the deleted column
 *
 * @method array findByOldId(int $old_id) Return CommonInscrit objects filtered by the old_id column
 * @method array findByEntrepriseId(int $entreprise_id) Return CommonInscrit objects filtered by the entreprise_id column
 * @method array findByIdEtablissement(int $id_etablissement) Return CommonInscrit objects filtered by the id_etablissement column
 * @method array findByLogin(string $login) Return CommonInscrit objects filtered by the login column
 * @method array findByMdp(string $mdp) Return CommonInscrit objects filtered by the mdp column
 * @method array findByNumCert(string $num_cert) Return CommonInscrit objects filtered by the num_cert column
 * @method array findByCert(string $cert) Return CommonInscrit objects filtered by the cert column
 * @method array findByCivilite(boolean $civilite) Return CommonInscrit objects filtered by the civilite column
 * @method array findByNom(string $nom) Return CommonInscrit objects filtered by the nom column
 * @method array findByPrenom(string $prenom) Return CommonInscrit objects filtered by the prenom column
 * @method array findByAdresse(string $adresse) Return CommonInscrit objects filtered by the adresse column
 * @method array findByCodepostal(string $codepostal) Return CommonInscrit objects filtered by the codepostal column
 * @method array findByVille(string $ville) Return CommonInscrit objects filtered by the ville column
 * @method array findByPays(string $pays) Return CommonInscrit objects filtered by the pays column
 * @method array findByEmail(string $email) Return CommonInscrit objects filtered by the email column
 * @method array findByTelephone(string $telephone) Return CommonInscrit objects filtered by the telephone column
 * @method array findByCategorie(string $categorie) Return CommonInscrit objects filtered by the categorie column
 * @method array findByMotstitreresume(string $motstitreresume) Return CommonInscrit objects filtered by the motstitreresume column
 * @method array findByPeriode(int $periode) Return CommonInscrit objects filtered by the periode column
 * @method array findBySiret(string $siret) Return CommonInscrit objects filtered by the siret column
 * @method array findByFax(string $fax) Return CommonInscrit objects filtered by the fax column
 * @method array findByCodeCpv(string $code_cpv) Return CommonInscrit objects filtered by the code_cpv column
 * @method array findByIdLangue(int $id_langue) Return CommonInscrit objects filtered by the id_langue column
 * @method array findByProfil(int $profil) Return CommonInscrit objects filtered by the profil column
 * @method array findByAdresse2(string $adresse2) Return CommonInscrit objects filtered by the adresse2 column
 * @method array findByBloque(string $bloque) Return CommonInscrit objects filtered by the bloque column
 * @method array findByIdInitial(int $id_initial) Return CommonInscrit objects filtered by the id_initial column
 * @method array findByInscritAnnuaireDefense(string $inscrit_annuaire_defense) Return CommonInscrit objects filtered by the inscrit_annuaire_defense column
 * @method array findByDateCreation(string $date_creation) Return CommonInscrit objects filtered by the date_creation column
 * @method array findByDateModification(string $date_modification) Return CommonInscrit objects filtered by the date_modification column
 * @method array findByTentativesMdp(int $tentatives_mdp) Return CommonInscrit objects filtered by the tentatives_mdp column
 * @method array findByUid(string $uid) Return CommonInscrit objects filtered by the uid column
 * @method array findByTypeHash(string $type_hash) Return CommonInscrit objects filtered by the type_hash column
 * @method array findByIdExterne(string $id_externe) Return CommonInscrit objects filtered by the id_externe column
 * @method array findById(string $id) Return CommonInscrit objects filtered by the id column
 * @method array findByDateValidationRgpd(string $date_validation_rgpd) Return CommonInscrit objects filtered by the date_validation_rgpd column
 * @method array findByRgpdCommunicationPlace(boolean $rgpd_communication_place) Return CommonInscrit objects filtered by the rgpd_communication_place column
 * @method array findByRgpdEnquete(boolean $rgpd_enquete) Return CommonInscrit objects filtered by the rgpd_enquete column
 * @method array findByRgpdCommunication(boolean $rgpd_communication) Return CommonInscrit objects filtered by the rgpd_communication column
 * @method array findByDeletedAt(string $deleted_at) Return CommonInscrit objects filtered by the deleted_at column
 * @method array findByDeleted(boolean $deleted) Return CommonInscrit objects filtered by the deleted column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonInscritQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonInscritQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonInscrit', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonInscritQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonInscritQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonInscritQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonInscritQuery) {
            return $criteria;
        }
        $query = new CommonInscritQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonInscrit|CommonInscrit[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonInscritPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonInscritPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonInscrit A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonInscrit A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `old_id`, `entreprise_id`, `id_etablissement`, `login`, `mdp`, `num_cert`, `cert`, `civilite`, `nom`, `prenom`, `adresse`, `codepostal`, `ville`, `pays`, `email`, `telephone`, `categorie`, `motstitreresume`, `periode`, `siret`, `fax`, `code_cpv`, `id_langue`, `profil`, `adresse2`, `bloque`, `id_initial`, `inscrit_annuaire_defense`, `date_creation`, `date_modification`, `tentatives_mdp`, `uid`, `type_hash`, `id_externe`, `id`, `date_validation_rgpd`, `rgpd_communication_place`, `rgpd_enquete`, `rgpd_communication`, `deleted_at`, `deleted` FROM `Inscrit` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonInscrit();
            $obj->hydrate($row);
            CommonInscritPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonInscrit|CommonInscrit[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonInscrit[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonInscritPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonInscritPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the old_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldId(1234); // WHERE old_id = 1234
     * $query->filterByOldId(array(12, 34)); // WHERE old_id IN (12, 34)
     * $query->filterByOldId(array('min' => 12)); // WHERE old_id >= 12
     * $query->filterByOldId(array('max' => 12)); // WHERE old_id <= 12
     * </code>
     *
     * @param     mixed $oldId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByOldId($oldId = null, $comparison = null)
    {
        if (is_array($oldId)) {
            $useMinMax = false;
            if (isset($oldId['min'])) {
                $this->addUsingAlias(CommonInscritPeer::OLD_ID, $oldId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldId['max'])) {
                $this->addUsingAlias(CommonInscritPeer::OLD_ID, $oldId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::OLD_ID, $oldId, $comparison);
    }

    /**
     * Filter the query on the entreprise_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEntrepriseId(1234); // WHERE entreprise_id = 1234
     * $query->filterByEntrepriseId(array(12, 34)); // WHERE entreprise_id IN (12, 34)
     * $query->filterByEntrepriseId(array('min' => 12)); // WHERE entreprise_id >= 12
     * $query->filterByEntrepriseId(array('max' => 12)); // WHERE entreprise_id <= 12
     * </code>
     *
     * @see       filterByEntreprise()
     *
     * @param     mixed $entrepriseId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByEntrepriseId($entrepriseId = null, $comparison = null)
    {
        if (is_array($entrepriseId)) {
            $useMinMax = false;
            if (isset($entrepriseId['min'])) {
                $this->addUsingAlias(CommonInscritPeer::ENTREPRISE_ID, $entrepriseId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($entrepriseId['max'])) {
                $this->addUsingAlias(CommonInscritPeer::ENTREPRISE_ID, $entrepriseId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::ENTREPRISE_ID, $entrepriseId, $comparison);
    }

    /**
     * Filter the query on the id_etablissement column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtablissement(1234); // WHERE id_etablissement = 1234
     * $query->filterByIdEtablissement(array(12, 34)); // WHERE id_etablissement IN (12, 34)
     * $query->filterByIdEtablissement(array('min' => 12)); // WHERE id_etablissement >= 12
     * $query->filterByIdEtablissement(array('max' => 12)); // WHERE id_etablissement <= 12
     * </code>
     *
     * @see       filterByCommonTEtablissementRelatedByIdEtablissement()
     *
     * @see       filterByCommonTEtablissementRelatedByIdEtablissement()
     *
     * @param     mixed $idEtablissement The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByIdEtablissement($idEtablissement = null, $comparison = null)
    {
        if (is_array($idEtablissement)) {
            $useMinMax = false;
            if (isset($idEtablissement['min'])) {
                $this->addUsingAlias(CommonInscritPeer::ID_ETABLISSEMENT, $idEtablissement['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtablissement['max'])) {
                $this->addUsingAlias(CommonInscritPeer::ID_ETABLISSEMENT, $idEtablissement['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::ID_ETABLISSEMENT, $idEtablissement, $comparison);
    }

    /**
     * Filter the query on the login column
     *
     * Example usage:
     * <code>
     * $query->filterByLogin('fooValue');   // WHERE login = 'fooValue'
     * $query->filterByLogin('%fooValue%'); // WHERE login LIKE '%fooValue%'
     * </code>
     *
     * @param     string $login The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByLogin($login = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($login)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $login)) {
                $login = str_replace('*', '%', $login);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::LOGIN, $login, $comparison);
    }

    /**
     * Filter the query on the mdp column
     *
     * Example usage:
     * <code>
     * $query->filterByMdp('fooValue');   // WHERE mdp = 'fooValue'
     * $query->filterByMdp('%fooValue%'); // WHERE mdp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mdp The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByMdp($mdp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mdp)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mdp)) {
                $mdp = str_replace('*', '%', $mdp);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::MDP, $mdp, $comparison);
    }

    /**
     * Filter the query on the num_cert column
     *
     * Example usage:
     * <code>
     * $query->filterByNumCert('fooValue');   // WHERE num_cert = 'fooValue'
     * $query->filterByNumCert('%fooValue%'); // WHERE num_cert LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numCert The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByNumCert($numCert = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numCert)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $numCert)) {
                $numCert = str_replace('*', '%', $numCert);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::NUM_CERT, $numCert, $comparison);
    }

    /**
     * Filter the query on the cert column
     *
     * Example usage:
     * <code>
     * $query->filterByCert('fooValue');   // WHERE cert = 'fooValue'
     * $query->filterByCert('%fooValue%'); // WHERE cert LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cert The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByCert($cert = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cert)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cert)) {
                $cert = str_replace('*', '%', $cert);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::CERT, $cert, $comparison);
    }

    /**
     * Filter the query on the civilite column
     *
     * Example usage:
     * <code>
     * $query->filterByCivilite(true); // WHERE civilite = true
     * $query->filterByCivilite('yes'); // WHERE civilite = true
     * </code>
     *
     * @param     boolean|string $civilite The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByCivilite($civilite = null, $comparison = null)
    {
        if (is_string($civilite)) {
            $civilite = in_array(strtolower($civilite), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonInscritPeer::CIVILITE, $civilite, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%'); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nom)) {
                $nom = str_replace('*', '%', $nom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the prenom column
     *
     * Example usage:
     * <code>
     * $query->filterByPrenom('fooValue');   // WHERE prenom = 'fooValue'
     * $query->filterByPrenom('%fooValue%'); // WHERE prenom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prenom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByPrenom($prenom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prenom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prenom)) {
                $prenom = str_replace('*', '%', $prenom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::PRENOM, $prenom, $comparison);
    }

    /**
     * Filter the query on the adresse column
     *
     * Example usage:
     * <code>
     * $query->filterByAdresse('fooValue');   // WHERE adresse = 'fooValue'
     * $query->filterByAdresse('%fooValue%'); // WHERE adresse LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adresse The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByAdresse($adresse = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adresse)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $adresse)) {
                $adresse = str_replace('*', '%', $adresse);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::ADRESSE, $adresse, $comparison);
    }

    /**
     * Filter the query on the codepostal column
     *
     * Example usage:
     * <code>
     * $query->filterByCodepostal('fooValue');   // WHERE codepostal = 'fooValue'
     * $query->filterByCodepostal('%fooValue%'); // WHERE codepostal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codepostal The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByCodepostal($codepostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codepostal)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codepostal)) {
                $codepostal = str_replace('*', '%', $codepostal);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::CODEPOSTAL, $codepostal, $comparison);
    }

    /**
     * Filter the query on the ville column
     *
     * Example usage:
     * <code>
     * $query->filterByVille('fooValue');   // WHERE ville = 'fooValue'
     * $query->filterByVille('%fooValue%'); // WHERE ville LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ville The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByVille($ville = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ville)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $ville)) {
                $ville = str_replace('*', '%', $ville);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::VILLE, $ville, $comparison);
    }

    /**
     * Filter the query on the pays column
     *
     * Example usage:
     * <code>
     * $query->filterByPays('fooValue');   // WHERE pays = 'fooValue'
     * $query->filterByPays('%fooValue%'); // WHERE pays LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pays The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByPays($pays = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pays)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pays)) {
                $pays = str_replace('*', '%', $pays);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::PAYS, $pays, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the telephone column
     *
     * Example usage:
     * <code>
     * $query->filterByTelephone('fooValue');   // WHERE telephone = 'fooValue'
     * $query->filterByTelephone('%fooValue%'); // WHERE telephone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telephone The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByTelephone($telephone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telephone)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $telephone)) {
                $telephone = str_replace('*', '%', $telephone);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::TELEPHONE, $telephone, $comparison);
    }

    /**
     * Filter the query on the categorie column
     *
     * Example usage:
     * <code>
     * $query->filterByCategorie('fooValue');   // WHERE categorie = 'fooValue'
     * $query->filterByCategorie('%fooValue%'); // WHERE categorie LIKE '%fooValue%'
     * </code>
     *
     * @param     string $categorie The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByCategorie($categorie = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($categorie)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $categorie)) {
                $categorie = str_replace('*', '%', $categorie);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::CATEGORIE, $categorie, $comparison);
    }

    /**
     * Filter the query on the motstitreresume column
     *
     * Example usage:
     * <code>
     * $query->filterByMotstitreresume('fooValue');   // WHERE motstitreresume = 'fooValue'
     * $query->filterByMotstitreresume('%fooValue%'); // WHERE motstitreresume LIKE '%fooValue%'
     * </code>
     *
     * @param     string $motstitreresume The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByMotstitreresume($motstitreresume = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($motstitreresume)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $motstitreresume)) {
                $motstitreresume = str_replace('*', '%', $motstitreresume);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::MOTSTITRERESUME, $motstitreresume, $comparison);
    }

    /**
     * Filter the query on the periode column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriode(1234); // WHERE periode = 1234
     * $query->filterByPeriode(array(12, 34)); // WHERE periode IN (12, 34)
     * $query->filterByPeriode(array('min' => 12)); // WHERE periode >= 12
     * $query->filterByPeriode(array('max' => 12)); // WHERE periode <= 12
     * </code>
     *
     * @param     mixed $periode The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByPeriode($periode = null, $comparison = null)
    {
        if (is_array($periode)) {
            $useMinMax = false;
            if (isset($periode['min'])) {
                $this->addUsingAlias(CommonInscritPeer::PERIODE, $periode['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($periode['max'])) {
                $this->addUsingAlias(CommonInscritPeer::PERIODE, $periode['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::PERIODE, $periode, $comparison);
    }

    /**
     * Filter the query on the siret column
     *
     * Example usage:
     * <code>
     * $query->filterBySiret('fooValue');   // WHERE siret = 'fooValue'
     * $query->filterBySiret('%fooValue%'); // WHERE siret LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siret The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterBySiret($siret = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siret)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $siret)) {
                $siret = str_replace('*', '%', $siret);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::SIRET, $siret, $comparison);
    }

    /**
     * Filter the query on the fax column
     *
     * Example usage:
     * <code>
     * $query->filterByFax('fooValue');   // WHERE fax = 'fooValue'
     * $query->filterByFax('%fooValue%'); // WHERE fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fax The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByFax($fax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fax)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $fax)) {
                $fax = str_replace('*', '%', $fax);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::FAX, $fax, $comparison);
    }

    /**
     * Filter the query on the code_cpv column
     *
     * Example usage:
     * <code>
     * $query->filterByCodeCpv('fooValue');   // WHERE code_cpv = 'fooValue'
     * $query->filterByCodeCpv('%fooValue%'); // WHERE code_cpv LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codeCpv The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByCodeCpv($codeCpv = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codeCpv)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codeCpv)) {
                $codeCpv = str_replace('*', '%', $codeCpv);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::CODE_CPV, $codeCpv, $comparison);
    }

    /**
     * Filter the query on the id_langue column
     *
     * Example usage:
     * <code>
     * $query->filterByIdLangue(1234); // WHERE id_langue = 1234
     * $query->filterByIdLangue(array(12, 34)); // WHERE id_langue IN (12, 34)
     * $query->filterByIdLangue(array('min' => 12)); // WHERE id_langue >= 12
     * $query->filterByIdLangue(array('max' => 12)); // WHERE id_langue <= 12
     * </code>
     *
     * @param     mixed $idLangue The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByIdLangue($idLangue = null, $comparison = null)
    {
        if (is_array($idLangue)) {
            $useMinMax = false;
            if (isset($idLangue['min'])) {
                $this->addUsingAlias(CommonInscritPeer::ID_LANGUE, $idLangue['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idLangue['max'])) {
                $this->addUsingAlias(CommonInscritPeer::ID_LANGUE, $idLangue['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::ID_LANGUE, $idLangue, $comparison);
    }

    /**
     * Filter the query on the profil column
     *
     * Example usage:
     * <code>
     * $query->filterByProfil(1234); // WHERE profil = 1234
     * $query->filterByProfil(array(12, 34)); // WHERE profil IN (12, 34)
     * $query->filterByProfil(array('min' => 12)); // WHERE profil >= 12
     * $query->filterByProfil(array('max' => 12)); // WHERE profil <= 12
     * </code>
     *
     * @param     mixed $profil The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByProfil($profil = null, $comparison = null)
    {
        if (is_array($profil)) {
            $useMinMax = false;
            if (isset($profil['min'])) {
                $this->addUsingAlias(CommonInscritPeer::PROFIL, $profil['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($profil['max'])) {
                $this->addUsingAlias(CommonInscritPeer::PROFIL, $profil['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::PROFIL, $profil, $comparison);
    }

    /**
     * Filter the query on the adresse2 column
     *
     * Example usage:
     * <code>
     * $query->filterByAdresse2('fooValue');   // WHERE adresse2 = 'fooValue'
     * $query->filterByAdresse2('%fooValue%'); // WHERE adresse2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $adresse2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByAdresse2($adresse2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adresse2)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $adresse2)) {
                $adresse2 = str_replace('*', '%', $adresse2);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::ADRESSE2, $adresse2, $comparison);
    }

    /**
     * Filter the query on the bloque column
     *
     * Example usage:
     * <code>
     * $query->filterByBloque('fooValue');   // WHERE bloque = 'fooValue'
     * $query->filterByBloque('%fooValue%'); // WHERE bloque LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bloque The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByBloque($bloque = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bloque)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $bloque)) {
                $bloque = str_replace('*', '%', $bloque);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::BLOQUE, $bloque, $comparison);
    }

    /**
     * Filter the query on the id_initial column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInitial(1234); // WHERE id_initial = 1234
     * $query->filterByIdInitial(array(12, 34)); // WHERE id_initial IN (12, 34)
     * $query->filterByIdInitial(array('min' => 12)); // WHERE id_initial >= 12
     * $query->filterByIdInitial(array('max' => 12)); // WHERE id_initial <= 12
     * </code>
     *
     * @param     mixed $idInitial The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByIdInitial($idInitial = null, $comparison = null)
    {
        if (is_array($idInitial)) {
            $useMinMax = false;
            if (isset($idInitial['min'])) {
                $this->addUsingAlias(CommonInscritPeer::ID_INITIAL, $idInitial['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInitial['max'])) {
                $this->addUsingAlias(CommonInscritPeer::ID_INITIAL, $idInitial['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::ID_INITIAL, $idInitial, $comparison);
    }

    /**
     * Filter the query on the inscrit_annuaire_defense column
     *
     * Example usage:
     * <code>
     * $query->filterByInscritAnnuaireDefense('fooValue');   // WHERE inscrit_annuaire_defense = 'fooValue'
     * $query->filterByInscritAnnuaireDefense('%fooValue%'); // WHERE inscrit_annuaire_defense LIKE '%fooValue%'
     * </code>
     *
     * @param     string $inscritAnnuaireDefense The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByInscritAnnuaireDefense($inscritAnnuaireDefense = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($inscritAnnuaireDefense)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $inscritAnnuaireDefense)) {
                $inscritAnnuaireDefense = str_replace('*', '%', $inscritAnnuaireDefense);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::INSCRIT_ANNUAIRE_DEFENSE, $inscritAnnuaireDefense, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('fooValue');   // WHERE date_creation = 'fooValue'
     * $query->filterByDateCreation('%fooValue%'); // WHERE date_creation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dateCreation The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dateCreation)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dateCreation)) {
                $dateCreation = str_replace('*', '%', $dateCreation);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('fooValue');   // WHERE date_modification = 'fooValue'
     * $query->filterByDateModification('%fooValue%'); // WHERE date_modification LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dateModification The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dateModification)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $dateModification)) {
                $dateModification = str_replace('*', '%', $dateModification);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the tentatives_mdp column
     *
     * Example usage:
     * <code>
     * $query->filterByTentativesMdp(1234); // WHERE tentatives_mdp = 1234
     * $query->filterByTentativesMdp(array(12, 34)); // WHERE tentatives_mdp IN (12, 34)
     * $query->filterByTentativesMdp(array('min' => 12)); // WHERE tentatives_mdp >= 12
     * $query->filterByTentativesMdp(array('max' => 12)); // WHERE tentatives_mdp <= 12
     * </code>
     *
     * @param     mixed $tentativesMdp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByTentativesMdp($tentativesMdp = null, $comparison = null)
    {
        if (is_array($tentativesMdp)) {
            $useMinMax = false;
            if (isset($tentativesMdp['min'])) {
                $this->addUsingAlias(CommonInscritPeer::TENTATIVES_MDP, $tentativesMdp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tentativesMdp['max'])) {
                $this->addUsingAlias(CommonInscritPeer::TENTATIVES_MDP, $tentativesMdp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::TENTATIVES_MDP, $tentativesMdp, $comparison);
    }

    /**
     * Filter the query on the uid column
     *
     * Example usage:
     * <code>
     * $query->filterByUid('fooValue');   // WHERE uid = 'fooValue'
     * $query->filterByUid('%fooValue%'); // WHERE uid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uid The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByUid($uid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uid)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $uid)) {
                $uid = str_replace('*', '%', $uid);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::UID, $uid, $comparison);
    }

    /**
     * Filter the query on the type_hash column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeHash('fooValue');   // WHERE type_hash = 'fooValue'
     * $query->filterByTypeHash('%fooValue%'); // WHERE type_hash LIKE '%fooValue%'
     * </code>
     *
     * @param     string $typeHash The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByTypeHash($typeHash = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($typeHash)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $typeHash)) {
                $typeHash = str_replace('*', '%', $typeHash);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::TYPE_HASH, $typeHash, $comparison);
    }

    /**
     * Filter the query on the id_externe column
     *
     * Example usage:
     * <code>
     * $query->filterByIdExterne('fooValue');   // WHERE id_externe = 'fooValue'
     * $query->filterByIdExterne('%fooValue%'); // WHERE id_externe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $idExterne The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByIdExterne($idExterne = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idExterne)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $idExterne)) {
                $idExterne = str_replace('*', '%', $idExterne);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::ID_EXTERNE, $idExterne, $comparison);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonInscritPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonInscritPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the date_validation_rgpd column
     *
     * Example usage:
     * <code>
     * $query->filterByDateValidationRgpd('2011-03-14'); // WHERE date_validation_rgpd = '2011-03-14'
     * $query->filterByDateValidationRgpd('now'); // WHERE date_validation_rgpd = '2011-03-14'
     * $query->filterByDateValidationRgpd(array('max' => 'yesterday')); // WHERE date_validation_rgpd > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateValidationRgpd The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByDateValidationRgpd($dateValidationRgpd = null, $comparison = null)
    {
        if (is_array($dateValidationRgpd)) {
            $useMinMax = false;
            if (isset($dateValidationRgpd['min'])) {
                $this->addUsingAlias(CommonInscritPeer::DATE_VALIDATION_RGPD, $dateValidationRgpd['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateValidationRgpd['max'])) {
                $this->addUsingAlias(CommonInscritPeer::DATE_VALIDATION_RGPD, $dateValidationRgpd['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::DATE_VALIDATION_RGPD, $dateValidationRgpd, $comparison);
    }

    /**
     * Filter the query on the rgpd_communication_place column
     *
     * Example usage:
     * <code>
     * $query->filterByRgpdCommunicationPlace(true); // WHERE rgpd_communication_place = true
     * $query->filterByRgpdCommunicationPlace('yes'); // WHERE rgpd_communication_place = true
     * </code>
     *
     * @param     boolean|string $rgpdCommunicationPlace The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByRgpdCommunicationPlace($rgpdCommunicationPlace = null, $comparison = null)
    {
        if (is_string($rgpdCommunicationPlace)) {
            $rgpdCommunicationPlace = in_array(strtolower($rgpdCommunicationPlace), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonInscritPeer::RGPD_COMMUNICATION_PLACE, $rgpdCommunicationPlace, $comparison);
    }

    /**
     * Filter the query on the rgpd_enquete column
     *
     * Example usage:
     * <code>
     * $query->filterByRgpdEnquete(true); // WHERE rgpd_enquete = true
     * $query->filterByRgpdEnquete('yes'); // WHERE rgpd_enquete = true
     * </code>
     *
     * @param     boolean|string $rgpdEnquete The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByRgpdEnquete($rgpdEnquete = null, $comparison = null)
    {
        if (is_string($rgpdEnquete)) {
            $rgpdEnquete = in_array(strtolower($rgpdEnquete), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonInscritPeer::RGPD_ENQUETE, $rgpdEnquete, $comparison);
    }

    /**
     * Filter the query on the rgpd_communication column
     *
     * Example usage:
     * <code>
     * $query->filterByRgpdCommunication(true); // WHERE rgpd_communication = true
     * $query->filterByRgpdCommunication('yes'); // WHERE rgpd_communication = true
     * </code>
     *
     * @param     boolean|string $rgpdCommunication The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByRgpdCommunication($rgpdCommunication = null, $comparison = null)
    {
        if (is_string($rgpdCommunication)) {
            $rgpdCommunication = in_array(strtolower($rgpdCommunication), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonInscritPeer::RGPD_COMMUNICATION, $rgpdCommunication, $comparison);
    }

    /**
     * Filter the query on the deleted_at column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedAt('2011-03-14'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt('now'); // WHERE deleted_at = '2011-03-14'
     * $query->filterByDeletedAt(array('max' => 'yesterday')); // WHERE deleted_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByDeletedAt($deletedAt = null, $comparison = null)
    {
        if (is_array($deletedAt)) {
            $useMinMax = false;
            if (isset($deletedAt['min'])) {
                $this->addUsingAlias(CommonInscritPeer::DELETED_AT, $deletedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedAt['max'])) {
                $this->addUsingAlias(CommonInscritPeer::DELETED_AT, $deletedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInscritPeer::DELETED_AT, $deletedAt, $comparison);
    }

    /**
     * Filter the query on the deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByDeleted(true); // WHERE deleted = true
     * $query->filterByDeleted('yes'); // WHERE deleted = true
     * </code>
     *
     * @param     boolean|string $deleted The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function filterByDeleted($deleted = null, $comparison = null)
    {
        if (is_string($deleted)) {
            $deleted = in_array(strtolower($deleted), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonInscritPeer::DELETED, $deleted, $comparison);
    }

    /**
     * Filter the query by a related CommonTEtablissement object
     *
     * @param   CommonTEtablissement|PropelObjectCollection $commonTEtablissement The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTEtablissementRelatedByIdEtablissement($commonTEtablissement, $comparison = null)
    {
        if ($commonTEtablissement instanceof CommonTEtablissement) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID_ETABLISSEMENT, $commonTEtablissement->getIdEtablissement(), $comparison);
        } elseif ($commonTEtablissement instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonInscritPeer::ID_ETABLISSEMENT, $commonTEtablissement->toKeyValue('PrimaryKey', 'IdEtablissement'), $comparison);
        } else {
            throw new PropelException('filterByCommonTEtablissementRelatedByIdEtablissement() only accepts arguments of type CommonTEtablissement or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTEtablissementRelatedByIdEtablissement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonTEtablissementRelatedByIdEtablissement($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTEtablissementRelatedByIdEtablissement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTEtablissementRelatedByIdEtablissement');
        }

        return $this;
    }

    /**
     * Use the CommonTEtablissementRelatedByIdEtablissement relation CommonTEtablissement object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTEtablissementQuery A secondary query class using the current class as primary query
     */
    public function useCommonTEtablissementRelatedByIdEtablissementQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTEtablissementRelatedByIdEtablissement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTEtablissementRelatedByIdEtablissement', '\Application\Propel\Mpe\CommonTEtablissementQuery');
    }

    /**
     * Filter the query by a related Entreprise object
     *
     * @param   Entreprise|PropelObjectCollection $entreprise The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByEntreprise($entreprise, $comparison = null)
    {
        if ($entreprise instanceof Entreprise) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ENTREPRISE_ID, $entreprise->getId(), $comparison);
        } elseif ($entreprise instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonInscritPeer::ENTREPRISE_ID, $entreprise->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEntreprise() only accepts arguments of type Entreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Entreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinEntreprise($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Entreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Entreprise');
        }

        return $this;
    }

    /**
     * Use the Entreprise relation Entreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\EntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useEntrepriseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Entreprise', '\Application\Propel\Mpe\EntrepriseQuery');
    }

    /**
     * Filter the query by a related CommonAlerte object
     *
     * @param   CommonAlerte|PropelObjectCollection $commonAlerte  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAlerte($commonAlerte, $comparison = null)
    {
        if ($commonAlerte instanceof CommonAlerte) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonAlerte->getIdInscrit(), $comparison);
        } elseif ($commonAlerte instanceof PropelObjectCollection) {
            return $this
                ->useCommonAlerteQuery()
                ->filterByPrimaryKeys($commonAlerte->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonAlerte() only accepts arguments of type CommonAlerte or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAlerte relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonAlerte($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAlerte');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAlerte');
        }

        return $this;
    }

    /**
     * Use the CommonAlerte relation CommonAlerte object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAlerteQuery A secondary query class using the current class as primary query
     */
    public function useCommonAlerteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonAlerte($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAlerte', '\Application\Propel\Mpe\CommonAlerteQuery');
    }

    /**
     * Filter the query by a related CommonCertificatsEntreprises object
     *
     * @param   CommonCertificatsEntreprises|PropelObjectCollection $commonCertificatsEntreprises  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonCertificatsEntreprises($commonCertificatsEntreprises, $comparison = null)
    {
        if ($commonCertificatsEntreprises instanceof CommonCertificatsEntreprises) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonCertificatsEntreprises->getIdInscrit(), $comparison);
        } elseif ($commonCertificatsEntreprises instanceof PropelObjectCollection) {
            return $this
                ->useCommonCertificatsEntreprisesQuery()
                ->filterByPrimaryKeys($commonCertificatsEntreprises->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonCertificatsEntreprises() only accepts arguments of type CommonCertificatsEntreprises or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonCertificatsEntreprises relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonCertificatsEntreprises($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonCertificatsEntreprises');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonCertificatsEntreprises');
        }

        return $this;
    }

    /**
     * Use the CommonCertificatsEntreprises relation CommonCertificatsEntreprises object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonCertificatsEntreprisesQuery A secondary query class using the current class as primary query
     */
    public function useCommonCertificatsEntreprisesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonCertificatsEntreprises($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonCertificatsEntreprises', '\Application\Propel\Mpe\CommonCertificatsEntreprisesQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDestinataire object
     *
     * @param   CommonEchangeDestinataire|PropelObjectCollection $commonEchangeDestinataire  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDestinataire($commonEchangeDestinataire, $comparison = null)
    {
        if ($commonEchangeDestinataire instanceof CommonEchangeDestinataire) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonEchangeDestinataire->getIdInscrit(), $comparison);
        } elseif ($commonEchangeDestinataire instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeDestinataireQuery()
                ->filterByPrimaryKeys($commonEchangeDestinataire->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangeDestinataire() only accepts arguments of type CommonEchangeDestinataire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDestinataire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDestinataire($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDestinataire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDestinataire');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDestinataire relation CommonEchangeDestinataire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDestinataireQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDestinataireQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDestinataire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDestinataire', '\Application\Propel\Mpe\CommonEchangeDestinataireQuery');
    }

    /**
     * Filter the query by a related CommonOffres object
     *
     * @param   CommonOffres|PropelObjectCollection $commonOffres  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOffres($commonOffres, $comparison = null)
    {
        if ($commonOffres instanceof CommonOffres) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonOffres->getInscritId(), $comparison);
        } elseif ($commonOffres instanceof PropelObjectCollection) {
            return $this
                ->useCommonOffresQuery()
                ->filterByPrimaryKeys($commonOffres->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonOffres() only accepts arguments of type CommonOffres or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOffres relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonOffres($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOffres');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOffres');
        }

        return $this;
    }

    /**
     * Use the CommonOffres relation CommonOffres object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOffresQuery A secondary query class using the current class as primary query
     */
    public function useCommonOffresQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonOffres($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOffres', '\Application\Propel\Mpe\CommonOffresQuery');
    }

    /**
     * Filter the query by a related CommonPanierEntreprise object
     *
     * @param   CommonPanierEntreprise|PropelObjectCollection $commonPanierEntreprise  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonPanierEntreprise($commonPanierEntreprise, $comparison = null)
    {
        if ($commonPanierEntreprise instanceof CommonPanierEntreprise) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonPanierEntreprise->getIdInscrit(), $comparison);
        } elseif ($commonPanierEntreprise instanceof PropelObjectCollection) {
            return $this
                ->useCommonPanierEntrepriseQuery()
                ->filterByPrimaryKeys($commonPanierEntreprise->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonPanierEntreprise() only accepts arguments of type CommonPanierEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonPanierEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonPanierEntreprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonPanierEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonPanierEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonPanierEntreprise relation CommonPanierEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonPanierEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonPanierEntrepriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonPanierEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonPanierEntreprise', '\Application\Propel\Mpe\CommonPanierEntrepriseQuery');
    }

    /**
     * Filter the query by a related CommonQuestionDCE object
     *
     * @param   CommonQuestionDCE|PropelObjectCollection $commonQuestionDCE  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonQuestionDCE($commonQuestionDCE, $comparison = null)
    {
        if ($commonQuestionDCE instanceof CommonQuestionDCE) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonQuestionDCE->getIdInscrit(), $comparison);
        } elseif ($commonQuestionDCE instanceof PropelObjectCollection) {
            return $this
                ->useCommonQuestionDCEQuery()
                ->filterByPrimaryKeys($commonQuestionDCE->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonQuestionDCE() only accepts arguments of type CommonQuestionDCE or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonQuestionDCE relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonQuestionDCE($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonQuestionDCE');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonQuestionDCE');
        }

        return $this;
    }

    /**
     * Use the CommonQuestionDCE relation CommonQuestionDCE object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonQuestionDCEQuery A secondary query class using the current class as primary query
     */
    public function useCommonQuestionDCEQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonQuestionDCE($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonQuestionDCE', '\Application\Propel\Mpe\CommonQuestionDCEQuery');
    }

    /**
     * Filter the query by a related CommonReponseInscritFormulaireConsultation object
     *
     * @param   CommonReponseInscritFormulaireConsultation|PropelObjectCollection $commonReponseInscritFormulaireConsultation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonReponseInscritFormulaireConsultation($commonReponseInscritFormulaireConsultation, $comparison = null)
    {
        if ($commonReponseInscritFormulaireConsultation instanceof CommonReponseInscritFormulaireConsultation) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonReponseInscritFormulaireConsultation->getIdInscrit(), $comparison);
        } elseif ($commonReponseInscritFormulaireConsultation instanceof PropelObjectCollection) {
            return $this
                ->useCommonReponseInscritFormulaireConsultationQuery()
                ->filterByPrimaryKeys($commonReponseInscritFormulaireConsultation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonReponseInscritFormulaireConsultation() only accepts arguments of type CommonReponseInscritFormulaireConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonReponseInscritFormulaireConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonReponseInscritFormulaireConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonReponseInscritFormulaireConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonReponseInscritFormulaireConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonReponseInscritFormulaireConsultation relation CommonReponseInscritFormulaireConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonReponseInscritFormulaireConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonReponseInscritFormulaireConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonReponseInscritFormulaireConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonReponseInscritFormulaireConsultation', '\Application\Propel\Mpe\CommonReponseInscritFormulaireConsultationQuery');
    }

    /**
     * Filter the query by a related CommonTelechargement object
     *
     * @param   CommonTelechargement|PropelObjectCollection $commonTelechargement  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTelechargement($commonTelechargement, $comparison = null)
    {
        if ($commonTelechargement instanceof CommonTelechargement) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonTelechargement->getIdInscrit(), $comparison);
        } elseif ($commonTelechargement instanceof PropelObjectCollection) {
            return $this
                ->useCommonTelechargementQuery()
                ->filterByPrimaryKeys($commonTelechargement->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTelechargement() only accepts arguments of type CommonTelechargement or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTelechargement relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonTelechargement($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTelechargement');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTelechargement');
        }

        return $this;
    }

    /**
     * Use the CommonTelechargement relation CommonTelechargement object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTelechargementQuery A secondary query class using the current class as primary query
     */
    public function useCommonTelechargementQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTelechargement($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTelechargement', '\Application\Propel\Mpe\CommonTelechargementQuery');
    }

    /**
     * Filter the query by a related CommonDossierVolumineux object
     *
     * @param   CommonDossierVolumineux|PropelObjectCollection $commonDossierVolumineux  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonDossierVolumineux($commonDossierVolumineux, $comparison = null)
    {
        if ($commonDossierVolumineux instanceof CommonDossierVolumineux) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonDossierVolumineux->getIdInscrit(), $comparison);
        } elseif ($commonDossierVolumineux instanceof PropelObjectCollection) {
            return $this
                ->useCommonDossierVolumineuxQuery()
                ->filterByPrimaryKeys($commonDossierVolumineux->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonDossierVolumineux() only accepts arguments of type CommonDossierVolumineux or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonDossierVolumineux relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonDossierVolumineux($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonDossierVolumineux');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonDossierVolumineux');
        }

        return $this;
    }

    /**
     * Use the CommonDossierVolumineux relation CommonDossierVolumineux object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonDossierVolumineuxQuery A secondary query class using the current class as primary query
     */
    public function useCommonDossierVolumineuxQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonDossierVolumineux($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonDossierVolumineux', '\Application\Propel\Mpe\CommonDossierVolumineuxQuery');
    }

    /**
     * Filter the query by a related CommonHistorisationMotDePasse object
     *
     * @param   CommonHistorisationMotDePasse|PropelObjectCollection $commonHistorisationMotDePasse  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonHistorisationMotDePasse($commonHistorisationMotDePasse, $comparison = null)
    {
        if ($commonHistorisationMotDePasse instanceof CommonHistorisationMotDePasse) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonHistorisationMotDePasse->getIdInscrit(), $comparison);
        } elseif ($commonHistorisationMotDePasse instanceof PropelObjectCollection) {
            return $this
                ->useCommonHistorisationMotDePasseQuery()
                ->filterByPrimaryKeys($commonHistorisationMotDePasse->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonHistorisationMotDePasse() only accepts arguments of type CommonHistorisationMotDePasse or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonHistorisationMotDePasse relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonHistorisationMotDePasse($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonHistorisationMotDePasse');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonHistorisationMotDePasse');
        }

        return $this;
    }

    /**
     * Use the CommonHistorisationMotDePasse relation CommonHistorisationMotDePasse object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonHistorisationMotDePasseQuery A secondary query class using the current class as primary query
     */
    public function useCommonHistorisationMotDePasseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonHistorisationMotDePasse($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonHistorisationMotDePasse', '\Application\Propel\Mpe\CommonHistorisationMotDePasseQuery');
    }

    /**
     * Filter the query by a related CommonQuestionsDce object
     *
     * @param   CommonQuestionsDce|PropelObjectCollection $commonQuestionsDce  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonQuestionsDce($commonQuestionsDce, $comparison = null)
    {
        if ($commonQuestionsDce instanceof CommonQuestionsDce) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonQuestionsDce->getIdInscrit(), $comparison);
        } elseif ($commonQuestionsDce instanceof PropelObjectCollection) {
            return $this
                ->useCommonQuestionsDceQuery()
                ->filterByPrimaryKeys($commonQuestionsDce->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonQuestionsDce() only accepts arguments of type CommonQuestionsDce or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonQuestionsDce relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonQuestionsDce($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonQuestionsDce');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonQuestionsDce');
        }

        return $this;
    }

    /**
     * Use the CommonQuestionsDce relation CommonQuestionsDce object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonQuestionsDceQuery A secondary query class using the current class as primary query
     */
    public function useCommonQuestionsDceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonQuestionsDce($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonQuestionsDce', '\Application\Propel\Mpe\CommonQuestionsDceQuery');
    }

    /**
     * Filter the query by a related CommonSsoEntreprise object
     *
     * @param   CommonSsoEntreprise|PropelObjectCollection $commonSsoEntreprise  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonSsoEntreprise($commonSsoEntreprise, $comparison = null)
    {
        if ($commonSsoEntreprise instanceof CommonSsoEntreprise) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonSsoEntreprise->getIdInscrit(), $comparison);
        } elseif ($commonSsoEntreprise instanceof PropelObjectCollection) {
            return $this
                ->useCommonSsoEntrepriseQuery()
                ->filterByPrimaryKeys($commonSsoEntreprise->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonSsoEntreprise() only accepts arguments of type CommonSsoEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonSsoEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonSsoEntreprise($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonSsoEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonSsoEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonSsoEntreprise relation CommonSsoEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonSsoEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonSsoEntrepriseQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonSsoEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonSsoEntreprise', '\Application\Propel\Mpe\CommonSsoEntrepriseQuery');
    }

    /**
     * Filter the query by a related CommonTCandidature object
     *
     * @param   CommonTCandidature|PropelObjectCollection $commonTCandidature  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCandidature($commonTCandidature, $comparison = null)
    {
        if ($commonTCandidature instanceof CommonTCandidature) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonTCandidature->getIdInscrit(), $comparison);
        } elseif ($commonTCandidature instanceof PropelObjectCollection) {
            return $this
                ->useCommonTCandidatureQuery()
                ->filterByPrimaryKeys($commonTCandidature->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTCandidature() only accepts arguments of type CommonTCandidature or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCandidature relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonTCandidature($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCandidature');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCandidature');
        }

        return $this;
    }

    /**
     * Use the CommonTCandidature relation CommonTCandidature object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCandidatureQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCandidatureQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTCandidature($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCandidature', '\Application\Propel\Mpe\CommonTCandidatureQuery');
    }

    /**
     * Filter the query by a related CommonTCandidatureMps object
     *
     * @param   CommonTCandidatureMps|PropelObjectCollection $commonTCandidatureMps  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTCandidatureMps($commonTCandidatureMps, $comparison = null)
    {
        if ($commonTCandidatureMps instanceof CommonTCandidatureMps) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonTCandidatureMps->getIdInscrit(), $comparison);
        } elseif ($commonTCandidatureMps instanceof PropelObjectCollection) {
            return $this
                ->useCommonTCandidatureMpsQuery()
                ->filterByPrimaryKeys($commonTCandidatureMps->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTCandidatureMps() only accepts arguments of type CommonTCandidatureMps or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTCandidatureMps relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonTCandidatureMps($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTCandidatureMps');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTCandidatureMps');
        }

        return $this;
    }

    /**
     * Use the CommonTCandidatureMps relation CommonTCandidatureMps object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTCandidatureMpsQuery A secondary query class using the current class as primary query
     */
    public function useCommonTCandidatureMpsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTCandidatureMps($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTCandidatureMps', '\Application\Propel\Mpe\CommonTCandidatureMpsQuery');
    }

    /**
     * Filter the query by a related CommonTContactContrat object
     *
     * @param   CommonTContactContrat|PropelObjectCollection $commonTContactContrat  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTContactContrat($commonTContactContrat, $comparison = null)
    {
        if ($commonTContactContrat instanceof CommonTContactContrat) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonTContactContrat->getIdInscrit(), $comparison);
        } elseif ($commonTContactContrat instanceof PropelObjectCollection) {
            return $this
                ->useCommonTContactContratQuery()
                ->filterByPrimaryKeys($commonTContactContrat->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTContactContrat() only accepts arguments of type CommonTContactContrat or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTContactContrat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonTContactContrat($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTContactContrat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTContactContrat');
        }

        return $this;
    }

    /**
     * Use the CommonTContactContrat relation CommonTContactContrat object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTContactContratQuery A secondary query class using the current class as primary query
     */
    public function useCommonTContactContratQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTContactContrat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTContactContrat', '\Application\Propel\Mpe\CommonTContactContratQuery');
    }

    /**
     * Filter the query by a related CommonTListeLotsCandidature object
     *
     * @param   CommonTListeLotsCandidature|PropelObjectCollection $commonTListeLotsCandidature  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTListeLotsCandidature($commonTListeLotsCandidature, $comparison = null)
    {
        if ($commonTListeLotsCandidature instanceof CommonTListeLotsCandidature) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonTListeLotsCandidature->getIdInscrit(), $comparison);
        } elseif ($commonTListeLotsCandidature instanceof PropelObjectCollection) {
            return $this
                ->useCommonTListeLotsCandidatureQuery()
                ->filterByPrimaryKeys($commonTListeLotsCandidature->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTListeLotsCandidature() only accepts arguments of type CommonTListeLotsCandidature or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTListeLotsCandidature relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonTListeLotsCandidature($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTListeLotsCandidature');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTListeLotsCandidature');
        }

        return $this;
    }

    /**
     * Use the CommonTListeLotsCandidature relation CommonTListeLotsCandidature object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTListeLotsCandidatureQuery A secondary query class using the current class as primary query
     */
    public function useCommonTListeLotsCandidatureQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTListeLotsCandidature($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTListeLotsCandidature', '\Application\Propel\Mpe\CommonTListeLotsCandidatureQuery');
    }

    /**
     * Filter the query by a related CommonTReponseElecFormulaire object
     *
     * @param   CommonTReponseElecFormulaire|PropelObjectCollection $commonTReponseElecFormulaire  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTReponseElecFormulaire($commonTReponseElecFormulaire, $comparison = null)
    {
        if ($commonTReponseElecFormulaire instanceof CommonTReponseElecFormulaire) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonTReponseElecFormulaire->getIdInscrit(), $comparison);
        } elseif ($commonTReponseElecFormulaire instanceof PropelObjectCollection) {
            return $this
                ->useCommonTReponseElecFormulaireQuery()
                ->filterByPrimaryKeys($commonTReponseElecFormulaire->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTReponseElecFormulaire() only accepts arguments of type CommonTReponseElecFormulaire or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTReponseElecFormulaire relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonTReponseElecFormulaire($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTReponseElecFormulaire');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTReponseElecFormulaire');
        }

        return $this;
    }

    /**
     * Use the CommonTReponseElecFormulaire relation CommonTReponseElecFormulaire object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTReponseElecFormulaireQuery A secondary query class using the current class as primary query
     */
    public function useCommonTReponseElecFormulaireQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTReponseElecFormulaire($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTReponseElecFormulaire', '\Application\Propel\Mpe\CommonTReponseElecFormulaireQuery');
    }

    /**
     * Filter the query by a related CommonTmpSiretIncorrect object
     *
     * @param   CommonTmpSiretIncorrect|PropelObjectCollection $commonTmpSiretIncorrect  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTmpSiretIncorrect($commonTmpSiretIncorrect, $comparison = null)
    {
        if ($commonTmpSiretIncorrect instanceof CommonTmpSiretIncorrect) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonTmpSiretIncorrect->getIdInscrit(), $comparison);
        } elseif ($commonTmpSiretIncorrect instanceof PropelObjectCollection) {
            return $this
                ->useCommonTmpSiretIncorrectQuery()
                ->filterByPrimaryKeys($commonTmpSiretIncorrect->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTmpSiretIncorrect() only accepts arguments of type CommonTmpSiretIncorrect or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTmpSiretIncorrect relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonTmpSiretIncorrect($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTmpSiretIncorrect');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTmpSiretIncorrect');
        }

        return $this;
    }

    /**
     * Use the CommonTmpSiretIncorrect relation CommonTmpSiretIncorrect object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTmpSiretIncorrectQuery A secondary query class using the current class as primary query
     */
    public function useCommonTmpSiretIncorrectQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTmpSiretIncorrect($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTmpSiretIncorrect', '\Application\Propel\Mpe\CommonTmpSiretIncorrectQuery');
    }

    /**
     * Filter the query by a related CommonTraceOperationsInscrit object
     *
     * @param   CommonTraceOperationsInscrit|PropelObjectCollection $commonTraceOperationsInscrit  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInscritQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTraceOperationsInscrit($commonTraceOperationsInscrit, $comparison = null)
    {
        if ($commonTraceOperationsInscrit instanceof CommonTraceOperationsInscrit) {
            return $this
                ->addUsingAlias(CommonInscritPeer::ID, $commonTraceOperationsInscrit->getIdInscrit(), $comparison);
        } elseif ($commonTraceOperationsInscrit instanceof PropelObjectCollection) {
            return $this
                ->useCommonTraceOperationsInscritQuery()
                ->filterByPrimaryKeys($commonTraceOperationsInscrit->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTraceOperationsInscrit() only accepts arguments of type CommonTraceOperationsInscrit or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTraceOperationsInscrit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function joinCommonTraceOperationsInscrit($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTraceOperationsInscrit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTraceOperationsInscrit');
        }

        return $this;
    }

    /**
     * Use the CommonTraceOperationsInscrit relation CommonTraceOperationsInscrit object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTraceOperationsInscritQuery A secondary query class using the current class as primary query
     */
    public function useCommonTraceOperationsInscritQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTraceOperationsInscrit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTraceOperationsInscrit', '\Application\Propel\Mpe\CommonTraceOperationsInscritQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonInscrit $commonInscrit Object to remove from the list of results
     *
     * @return CommonInscritQuery The current query, for fluid interface
     */
    public function prune($commonInscrit = null)
    {
        if ($commonInscrit) {
            $this->addUsingAlias(CommonInscritPeer::ID, $commonInscrit->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
