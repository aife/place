<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonAgentServiceMetier;
use Application\Propel\Mpe\CommonAgentServiceMetierQuery;
use Application\Propel\Mpe\CommonAgentTechniqueToken;
use Application\Propel\Mpe\CommonAgentTechniqueTokenQuery;
use Application\Propel\Mpe\CommonAnnexeFinanciere;
use Application\Propel\Mpe\CommonAnnexeFinanciereQuery;
use Application\Propel\Mpe\CommonConsultationFavoris;
use Application\Propel\Mpe\CommonConsultationFavorisQuery;
use Application\Propel\Mpe\CommonDossierVolumineux;
use Application\Propel\Mpe\CommonDossierVolumineuxQuery;
use Application\Propel\Mpe\CommonEchangeDoc;
use Application\Propel\Mpe\CommonEchangeDocHistorique;
use Application\Propel\Mpe\CommonEchangeDocHistoriqueQuery;
use Application\Propel\Mpe\CommonEchangeDocQuery;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonHabilitationAgentQuery;
use Application\Propel\Mpe\CommonHabilitationAgentWs;
use Application\Propel\Mpe\CommonHabilitationAgentWsQuery;
use Application\Propel\Mpe\CommonInvitePermanentTransverse;
use Application\Propel\Mpe\CommonInvitePermanentTransverseQuery;
use Application\Propel\Mpe\CommonModificationContrat;
use Application\Propel\Mpe\CommonModificationContratQuery;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServiceQuery;
use Application\Propel\Mpe\CommonTCAOCommissionAgent;
use Application\Propel\Mpe\CommonTCAOCommissionAgentQuery;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourIntervenant;
use Application\Propel\Mpe\CommonTCAOOrdreDuJourIntervenantQuery;
use Application\Propel\Mpe\CommonTCAOSeanceAgent;
use Application\Propel\Mpe\CommonTCAOSeanceAgentQuery;
use Application\Propel\Mpe\CommonTCAOSeanceInvite;
use Application\Propel\Mpe\CommonTCAOSeanceInviteQuery;
use Application\Propel\Mpe\CommonTVisionRmaAgentOrganisme;
use Application\Propel\Mpe\CommonTVisionRmaAgentOrganismeQuery;

/**
 * Base class that represents a row from the 'Agent' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonAgent extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonAgentPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonAgentPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the login field.
     * @var        string
     */
    protected $login;

    /**
     * The value for the email field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $email;

    /**
     * The value for the mdp field.
     * @var        string
     */
    protected $mdp;

    /**
     * The value for the certificat field.
     * @var        string
     */
    protected $certificat;

    /**
     * The value for the nom field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $nom;

    /**
     * The value for the prenom field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $prenom;

    /**
     * The value for the tentatives_mdp field.
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $tentatives_mdp;

    /**
     * The value for the organisme field.
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the old_service_id field.
     * @var        int
     */
    protected $old_service_id;

    /**
     * The value for the recevoir_mail field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $recevoir_mail;

    /**
     * The value for the elu field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $elu;

    /**
     * The value for the nom_fonction field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $nom_fonction;

    /**
     * The value for the num_tel field.
     * @var        string
     */
    protected $num_tel;

    /**
     * The value for the num_fax field.
     * @var        string
     */
    protected $num_fax;

    /**
     * The value for the type_comm field.
     * Note: this column has a database default value of: '2'
     * @var        string
     */
    protected $type_comm;

    /**
     * The value for the adr_postale field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $adr_postale;

    /**
     * The value for the civilite field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $civilite;

    /**
     * The value for the alerte_reponse_electronique field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte_reponse_electronique;

    /**
     * The value for the alerte_cloture_consultation field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte_cloture_consultation;

    /**
     * The value for the alerte_reception_message field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte_reception_message;

    /**
     * The value for the alerte_publication_boamp field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte_publication_boamp;

    /**
     * The value for the alerte_echec_publication_boamp field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte_echec_publication_boamp;

    /**
     * The value for the alerte_creation_modification_agent field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte_creation_modification_agent;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_modification field.
     * @var        string
     */
    protected $date_modification;

    /**
     * The value for the id_externe field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $id_externe;

    /**
     * The value for the id_profil_socle_externe field.
     * @var        int
     */
    protected $id_profil_socle_externe;

    /**
     * The value for the lieu_execution field.
     * @var        string
     */
    protected $lieu_execution;

    /**
     * The value for the alerte_question_entreprise field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte_question_entreprise;

    /**
     * The value for the actif field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $actif;

    /**
     * The value for the deleted_at field.
     * @var        string
     */
    protected $deleted_at;

    /**
     * The value for the codes_nuts field.
     * @var        string
     */
    protected $codes_nuts;

    /**
     * The value for the num_certificat field.
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $num_certificat;

    /**
     * The value for the alerte_validation_consultation field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte_validation_consultation;

    /**
     * The value for the alerte_chorus field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $alerte_chorus;

    /**
     * The value for the password field.
     * @var        string
     */
    protected $password;

    /**
     * The value for the code_theme field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $code_theme;

    /**
     * The value for the date_connexion field.
     * @var        string
     */
    protected $date_connexion;

    /**
     * The value for the type_hash field.
     * Note: this column has a database default value of: 'SHA1'
     * @var        string
     */
    protected $type_hash;

    /**
     * The value for the alerte_mes_consultations field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $alerte_mes_consultations;

    /**
     * The value for the alerte_consultations_mon_entite field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $alerte_consultations_mon_entite;

    /**
     * The value for the alerte_consultations_des_entites_dependantes field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $alerte_consultations_des_entites_dependantes;

    /**
     * The value for the alerte_consultations_mes_entites_transverses field.
     * Note: this column has a database default value of: '1'
     * @var        string
     */
    protected $alerte_consultations_mes_entites_transverses;

    /**
     * The value for the technique field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $technique;

    /**
     * The value for the service_id field.
     * @var        string
     */
    protected $service_id;

    /**
     * The value for the date_validation_rgpd field.
     * @var        string
     */
    protected $date_validation_rgpd;

    /**
     * The value for the rgpd_communication_place field.
     * @var        boolean
     */
    protected $rgpd_communication_place;

    /**
     * The value for the rgpd_enquete field.
     * @var        boolean
     */
    protected $rgpd_enquete;

    /**
     * @var        CommonOrganisme
     */
    protected $aCommonOrganisme;

    /**
     * @var        CommonService
     */
    protected $aCommonService;

    /**
     * @var        PropelObjectCollection|CommonAgentServiceMetier[] Collection to store aggregation of CommonAgentServiceMetier objects.
     */
    protected $collCommonAgentServiceMetiers;
    protected $collCommonAgentServiceMetiersPartial;

    /**
     * @var        CommonHabilitationAgent one-to-one related CommonHabilitationAgent object
     */
    protected $singleCommonHabilitationAgent;

    /**
     * @var        PropelObjectCollection|CommonAgentTechniqueToken[] Collection to store aggregation of CommonAgentTechniqueToken objects.
     */
    protected $collCommonAgentTechniqueTokens;
    protected $collCommonAgentTechniqueTokensPartial;

    /**
     * @var        PropelObjectCollection|CommonAnnexeFinanciere[] Collection to store aggregation of CommonAnnexeFinanciere objects.
     */
    protected $collCommonAnnexeFinancieres;
    protected $collCommonAnnexeFinancieresPartial;

    /**
     * @var        PropelObjectCollection|CommonConsultationFavoris[] Collection to store aggregation of CommonConsultationFavoris objects.
     */
    protected $collCommonConsultationFavoriss;
    protected $collCommonConsultationFavorissPartial;

    /**
     * @var        PropelObjectCollection|CommonDossierVolumineux[] Collection to store aggregation of CommonDossierVolumineux objects.
     */
    protected $collCommonDossierVolumineuxs;
    protected $collCommonDossierVolumineuxsPartial;

    /**
     * @var        PropelObjectCollection|CommonEchangeDoc[] Collection to store aggregation of CommonEchangeDoc objects.
     */
    protected $collCommonEchangeDocs;
    protected $collCommonEchangeDocsPartial;

    /**
     * @var        PropelObjectCollection|CommonEchangeDocHistorique[] Collection to store aggregation of CommonEchangeDocHistorique objects.
     */
    protected $collCommonEchangeDocHistoriques;
    protected $collCommonEchangeDocHistoriquesPartial;

    /**
     * @var        PropelObjectCollection|CommonHabilitationAgentWs[] Collection to store aggregation of CommonHabilitationAgentWs objects.
     */
    protected $collCommonHabilitationAgentWss;
    protected $collCommonHabilitationAgentWssPartial;

    /**
     * @var        PropelObjectCollection|CommonInvitePermanentTransverse[] Collection to store aggregation of CommonInvitePermanentTransverse objects.
     */
    protected $collCommonInvitePermanentTransverses;
    protected $collCommonInvitePermanentTransversesPartial;

    /**
     * @var        PropelObjectCollection|CommonModificationContrat[] Collection to store aggregation of CommonModificationContrat objects.
     */
    protected $collCommonModificationContrats;
    protected $collCommonModificationContratsPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOCommissionAgent[] Collection to store aggregation of CommonTCAOCommissionAgent objects.
     */
    protected $collCommonTCAOCommissionAgents;
    protected $collCommonTCAOCommissionAgentsPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOOrdreDuJourIntervenant[] Collection to store aggregation of CommonTCAOOrdreDuJourIntervenant objects.
     */
    protected $collCommonTCAOOrdreDuJourIntervenants;
    protected $collCommonTCAOOrdreDuJourIntervenantsPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOSeanceAgent[] Collection to store aggregation of CommonTCAOSeanceAgent objects.
     */
    protected $collCommonTCAOSeanceAgents;
    protected $collCommonTCAOSeanceAgentsPartial;

    /**
     * @var        PropelObjectCollection|CommonTCAOSeanceInvite[] Collection to store aggregation of CommonTCAOSeanceInvite objects.
     */
    protected $collCommonTCAOSeanceInvites;
    protected $collCommonTCAOSeanceInvitesPartial;

    /**
     * @var        PropelObjectCollection|CommonTVisionRmaAgentOrganisme[] Collection to store aggregation of CommonTVisionRmaAgentOrganisme objects.
     */
    protected $collCommonTVisionRmaAgentOrganismes;
    protected $collCommonTVisionRmaAgentOrganismesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAgentServiceMetiersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonHabilitationAgentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAgentTechniqueTokensScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonAnnexeFinancieresScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationFavorissScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonDossierVolumineuxsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangeDocsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonEchangeDocHistoriquesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonHabilitationAgentWssScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonInvitePermanentTransversesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonModificationContratsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOCommissionAgentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOOrdreDuJourIntervenantsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOSeanceAgentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTCAOSeanceInvitesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTVisionRmaAgentOrganismesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->email = '';
        $this->nom = '';
        $this->prenom = '';
        $this->tentatives_mdp = 0;
        $this->recevoir_mail = '0';
        $this->elu = '0';
        $this->nom_fonction = '';
        $this->type_comm = '2';
        $this->adr_postale = '';
        $this->civilite = '';
        $this->alerte_reponse_electronique = '0';
        $this->alerte_cloture_consultation = '0';
        $this->alerte_reception_message = '0';
        $this->alerte_publication_boamp = '0';
        $this->alerte_echec_publication_boamp = '0';
        $this->alerte_creation_modification_agent = '0';
        $this->id_externe = '0';
        $this->alerte_question_entreprise = '0';
        $this->actif = '1';
        $this->num_certificat = '';
        $this->alerte_validation_consultation = '0';
        $this->alerte_chorus = '0';
        $this->code_theme = '0';
        $this->type_hash = 'SHA1';
        $this->alerte_mes_consultations = '1';
        $this->alerte_consultations_mon_entite = '1';
        $this->alerte_consultations_des_entites_dependantes = '1';
        $this->alerte_consultations_mes_entites_transverses = '1';
        $this->technique = false;
    }

    /**
     * Initializes internal state of BaseCommonAgent object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [login] column value.
     *
     * @return string
     */
    public function getLogin()
    {

        return $this->login;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [mdp] column value.
     *
     * @return string
     */
    public function getMdp()
    {

        return $this->mdp;
    }

    /**
     * Get the [certificat] column value.
     *
     * @return string
     */
    public function getCertificat()
    {

        return $this->certificat;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {

        return $this->nom;
    }

    /**
     * Get the [prenom] column value.
     *
     * @return string
     */
    public function getPrenom()
    {

        return $this->prenom;
    }

    /**
     * Get the [tentatives_mdp] column value.
     *
     * @return int
     */
    public function getTentativesMdp()
    {

        return $this->tentatives_mdp;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [old_service_id] column value.
     *
     * @return int
     */
    public function getOldServiceId()
    {

        return $this->old_service_id;
    }

    /**
     * Get the [recevoir_mail] column value.
     *
     * @return string
     */
    public function getRecevoirMail()
    {

        return $this->recevoir_mail;
    }

    /**
     * Get the [elu] column value.
     *
     * @return string
     */
    public function getElu()
    {

        return $this->elu;
    }

    /**
     * Get the [nom_fonction] column value.
     *
     * @return string
     */
    public function getNomFonction()
    {

        return $this->nom_fonction;
    }

    /**
     * Get the [num_tel] column value.
     *
     * @return string
     */
    public function getNumTel()
    {

        return $this->num_tel;
    }

    /**
     * Get the [num_fax] column value.
     *
     * @return string
     */
    public function getNumFax()
    {

        return $this->num_fax;
    }

    /**
     * Get the [type_comm] column value.
     *
     * @return string
     */
    public function getTypeComm()
    {

        return $this->type_comm;
    }

    /**
     * Get the [adr_postale] column value.
     *
     * @return string
     */
    public function getAdrPostale()
    {

        return $this->adr_postale;
    }

    /**
     * Get the [civilite] column value.
     *
     * @return string
     */
    public function getCivilite()
    {

        return $this->civilite;
    }

    /**
     * Get the [alerte_reponse_electronique] column value.
     *
     * @return string
     */
    public function getAlerteReponseElectronique()
    {

        return $this->alerte_reponse_electronique;
    }

    /**
     * Get the [alerte_cloture_consultation] column value.
     *
     * @return string
     */
    public function getAlerteClotureConsultation()
    {

        return $this->alerte_cloture_consultation;
    }

    /**
     * Get the [alerte_reception_message] column value.
     *
     * @return string
     */
    public function getAlerteReceptionMessage()
    {

        return $this->alerte_reception_message;
    }

    /**
     * Get the [alerte_publication_boamp] column value.
     *
     * @return string
     */
    public function getAlertePublicationBoamp()
    {

        return $this->alerte_publication_boamp;
    }

    /**
     * Get the [alerte_echec_publication_boamp] column value.
     *
     * @return string
     */
    public function getAlerteEchecPublicationBoamp()
    {

        return $this->alerte_echec_publication_boamp;
    }

    /**
     * Get the [alerte_creation_modification_agent] column value.
     *
     * @return string
     */
    public function getAlerteCreationModificationAgent()
    {

        return $this->alerte_creation_modification_agent;
    }

    /**
     * Get the [date_creation] column value.
     *
     * @return string
     */
    public function getDateCreation()
    {

        return $this->date_creation;
    }

    /**
     * Get the [date_modification] column value.
     *
     * @return string
     */
    public function getDateModification()
    {

        return $this->date_modification;
    }

    /**
     * Get the [id_externe] column value.
     *
     * @return string
     */
    public function getIdExterne()
    {

        return $this->id_externe;
    }

    /**
     * Get the [id_profil_socle_externe] column value.
     *
     * @return int
     */
    public function getIdProfilSocleExterne()
    {

        return $this->id_profil_socle_externe;
    }

    /**
     * Get the [lieu_execution] column value.
     *
     * @return string
     */
    public function getLieuExecution()
    {

        return $this->lieu_execution;
    }

    /**
     * Get the [alerte_question_entreprise] column value.
     *
     * @return string
     */
    public function getAlerteQuestionEntreprise()
    {

        return $this->alerte_question_entreprise;
    }

    /**
     * Get the [actif] column value.
     *
     * @return string
     */
    public function getActif()
    {

        return $this->actif;
    }

    /**
     * Get the [optionally formatted] temporal [deleted_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedAt($format = 'Y-m-d H:i:s')
    {
        if ($this->deleted_at === null) {
            return null;
        }

        if ($this->deleted_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->deleted_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->deleted_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [codes_nuts] column value.
     *
     * @return string
     */
    public function getCodesNuts()
    {

        return $this->codes_nuts;
    }

    /**
     * Get the [num_certificat] column value.
     *
     * @return string
     */
    public function getNumCertificat()
    {

        return $this->num_certificat;
    }

    /**
     * Get the [alerte_validation_consultation] column value.
     *
     * @return string
     */
    public function getAlerteValidationConsultation()
    {

        return $this->alerte_validation_consultation;
    }

    /**
     * Get the [alerte_chorus] column value.
     *
     * @return string
     */
    public function getAlerteChorus()
    {

        return $this->alerte_chorus;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {

        return $this->password;
    }

    /**
     * Get the [code_theme] column value.
     *
     * @return string
     */
    public function getCodeTheme()
    {

        return $this->code_theme;
    }

    /**
     * Get the [optionally formatted] temporal [date_connexion] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateConnexion($format = 'Y-m-d H:i:s')
    {
        if ($this->date_connexion === null) {
            return null;
        }

        if ($this->date_connexion === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_connexion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_connexion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [type_hash] column value.
     *
     * @return string
     */
    public function getTypeHash()
    {

        return $this->type_hash;
    }

    /**
     * Get the [alerte_mes_consultations] column value.
     *
     * @return string
     */
    public function getAlerteMesConsultations()
    {

        return $this->alerte_mes_consultations;
    }

    /**
     * Get the [alerte_consultations_mon_entite] column value.
     *
     * @return string
     */
    public function getAlerteConsultationsMonEntite()
    {

        return $this->alerte_consultations_mon_entite;
    }

    /**
     * Get the [alerte_consultations_des_entites_dependantes] column value.
     *
     * @return string
     */
    public function getAlerteConsultationsDesEntitesDependantes()
    {

        return $this->alerte_consultations_des_entites_dependantes;
    }

    /**
     * Get the [alerte_consultations_mes_entites_transverses] column value.
     *
     * @return string
     */
    public function getAlerteConsultationsMesEntitesTransverses()
    {

        return $this->alerte_consultations_mes_entites_transverses;
    }

    /**
     * Get the [technique] column value.
     *
     * @return boolean
     */
    public function getTechnique()
    {

        return $this->technique;
    }

    /**
     * Get the [service_id] column value.
     *
     * @return string
     */
    public function getServiceId()
    {

        return $this->service_id;
    }

    /**
     * Get the [optionally formatted] temporal [date_validation_rgpd] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateValidationRgpd($format = 'Y-m-d H:i:s')
    {
        if ($this->date_validation_rgpd === null) {
            return null;
        }

        if ($this->date_validation_rgpd === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_validation_rgpd);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_validation_rgpd, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [rgpd_communication_place] column value.
     *
     * @return boolean
     */
    public function getRgpdCommunicationPlace()
    {

        return $this->rgpd_communication_place;
    }

    /**
     * Get the [rgpd_enquete] column value.
     *
     * @return boolean
     */
    public function getRgpdEnquete()
    {

        return $this->rgpd_enquete;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [login] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setLogin($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->login !== $v) {
            $this->login = $v;
            $this->modifiedColumns[] = CommonAgentPeer::LOGIN;
        }


        return $this;
    } // setLogin()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = CommonAgentPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [mdp] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setMdp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mdp !== $v) {
            $this->mdp = $v;
            $this->modifiedColumns[] = CommonAgentPeer::MDP;
        }


        return $this;
    } // setMdp()

    /**
     * Set the value of [certificat] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCertificat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->certificat !== $v) {
            $this->certificat = $v;
            $this->modifiedColumns[] = CommonAgentPeer::CERTIFICAT;
        }


        return $this;
    } // setCertificat()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[] = CommonAgentPeer::NOM;
        }


        return $this;
    } // setNom()

    /**
     * Set the value of [prenom] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setPrenom($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prenom !== $v) {
            $this->prenom = $v;
            $this->modifiedColumns[] = CommonAgentPeer::PRENOM;
        }


        return $this;
    } // setPrenom()

    /**
     * Set the value of [tentatives_mdp] column.
     *
     * @param int $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setTentativesMdp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->tentatives_mdp !== $v) {
            $this->tentatives_mdp = $v;
            $this->modifiedColumns[] = CommonAgentPeer::TENTATIVES_MDP;
        }


        return $this;
    } // setTentativesMdp()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ORGANISME;
        }

        if ($this->aCommonOrganisme !== null && $this->aCommonOrganisme->getAcronyme() !== $v) {
            $this->aCommonOrganisme = null;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [old_service_id] column.
     *
     * @param int $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setOldServiceId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_service_id !== $v) {
            $this->old_service_id = $v;
            $this->modifiedColumns[] = CommonAgentPeer::OLD_SERVICE_ID;
        }


        return $this;
    } // setOldServiceId()

    /**
     * Set the value of [recevoir_mail] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setRecevoirMail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->recevoir_mail !== $v) {
            $this->recevoir_mail = $v;
            $this->modifiedColumns[] = CommonAgentPeer::RECEVOIR_MAIL;
        }


        return $this;
    } // setRecevoirMail()

    /**
     * Set the value of [elu] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setElu($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->elu !== $v) {
            $this->elu = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ELU;
        }


        return $this;
    } // setElu()

    /**
     * Set the value of [nom_fonction] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setNomFonction($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom_fonction !== $v) {
            $this->nom_fonction = $v;
            $this->modifiedColumns[] = CommonAgentPeer::NOM_FONCTION;
        }


        return $this;
    } // setNomFonction()

    /**
     * Set the value of [num_tel] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setNumTel($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->num_tel !== $v) {
            $this->num_tel = $v;
            $this->modifiedColumns[] = CommonAgentPeer::NUM_TEL;
        }


        return $this;
    } // setNumTel()

    /**
     * Set the value of [num_fax] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setNumFax($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->num_fax !== $v) {
            $this->num_fax = $v;
            $this->modifiedColumns[] = CommonAgentPeer::NUM_FAX;
        }


        return $this;
    } // setNumFax()

    /**
     * Set the value of [type_comm] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setTypeComm($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_comm !== $v) {
            $this->type_comm = $v;
            $this->modifiedColumns[] = CommonAgentPeer::TYPE_COMM;
        }


        return $this;
    } // setTypeComm()

    /**
     * Set the value of [adr_postale] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAdrPostale($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->adr_postale !== $v) {
            $this->adr_postale = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ADR_POSTALE;
        }


        return $this;
    } // setAdrPostale()

    /**
     * Set the value of [civilite] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCivilite($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->civilite !== $v) {
            $this->civilite = $v;
            $this->modifiedColumns[] = CommonAgentPeer::CIVILITE;
        }


        return $this;
    } // setCivilite()

    /**
     * Set the value of [alerte_reponse_electronique] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteReponseElectronique($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_reponse_electronique !== $v) {
            $this->alerte_reponse_electronique = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_REPONSE_ELECTRONIQUE;
        }


        return $this;
    } // setAlerteReponseElectronique()

    /**
     * Set the value of [alerte_cloture_consultation] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteClotureConsultation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_cloture_consultation !== $v) {
            $this->alerte_cloture_consultation = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_CLOTURE_CONSULTATION;
        }


        return $this;
    } // setAlerteClotureConsultation()

    /**
     * Set the value of [alerte_reception_message] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteReceptionMessage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_reception_message !== $v) {
            $this->alerte_reception_message = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_RECEPTION_MESSAGE;
        }


        return $this;
    } // setAlerteReceptionMessage()

    /**
     * Set the value of [alerte_publication_boamp] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlertePublicationBoamp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_publication_boamp !== $v) {
            $this->alerte_publication_boamp = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_PUBLICATION_BOAMP;
        }


        return $this;
    } // setAlertePublicationBoamp()

    /**
     * Set the value of [alerte_echec_publication_boamp] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteEchecPublicationBoamp($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_echec_publication_boamp !== $v) {
            $this->alerte_echec_publication_boamp = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_ECHEC_PUBLICATION_BOAMP;
        }


        return $this;
    } // setAlerteEchecPublicationBoamp()

    /**
     * Set the value of [alerte_creation_modification_agent] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteCreationModificationAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_creation_modification_agent !== $v) {
            $this->alerte_creation_modification_agent = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_CREATION_MODIFICATION_AGENT;
        }


        return $this;
    } // setAlerteCreationModificationAgent()

    /**
     * Set the value of [date_creation] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_creation !== $v) {
            $this->date_creation = $v;
            $this->modifiedColumns[] = CommonAgentPeer::DATE_CREATION;
        }


        return $this;
    } // setDateCreation()

    /**
     * Set the value of [date_modification] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setDateModification($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->date_modification !== $v) {
            $this->date_modification = $v;
            $this->modifiedColumns[] = CommonAgentPeer::DATE_MODIFICATION;
        }


        return $this;
    } // setDateModification()

    /**
     * Set the value of [id_externe] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setIdExterne($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_externe !== $v) {
            $this->id_externe = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ID_EXTERNE;
        }


        return $this;
    } // setIdExterne()

    /**
     * Set the value of [id_profil_socle_externe] column.
     *
     * @param int $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setIdProfilSocleExterne($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_profil_socle_externe !== $v) {
            $this->id_profil_socle_externe = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ID_PROFIL_SOCLE_EXTERNE;
        }


        return $this;
    } // setIdProfilSocleExterne()

    /**
     * Set the value of [lieu_execution] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setLieuExecution($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->lieu_execution !== $v) {
            $this->lieu_execution = $v;
            $this->modifiedColumns[] = CommonAgentPeer::LIEU_EXECUTION;
        }


        return $this;
    } // setLieuExecution()

    /**
     * Set the value of [alerte_question_entreprise] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteQuestionEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_question_entreprise !== $v) {
            $this->alerte_question_entreprise = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_QUESTION_ENTREPRISE;
        }


        return $this;
    } // setAlerteQuestionEntreprise()

    /**
     * Set the value of [actif] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setActif($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->actif !== $v) {
            $this->actif = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ACTIF;
        }


        return $this;
    } // setActif()

    /**
     * Sets the value of [deleted_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setDeletedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_at !== null || $dt !== null) {
            $currentDateAsString = ($this->deleted_at !== null && $tmpDt = new DateTime($this->deleted_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->deleted_at = $newDateAsString;
                $this->modifiedColumns[] = CommonAgentPeer::DELETED_AT;
            }
        } // if either are not null


        return $this;
    } // setDeletedAt()

    /**
     * Set the value of [codes_nuts] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCodesNuts($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->codes_nuts !== $v) {
            $this->codes_nuts = $v;
            $this->modifiedColumns[] = CommonAgentPeer::CODES_NUTS;
        }


        return $this;
    } // setCodesNuts()

    /**
     * Set the value of [num_certificat] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setNumCertificat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->num_certificat !== $v) {
            $this->num_certificat = $v;
            $this->modifiedColumns[] = CommonAgentPeer::NUM_CERTIFICAT;
        }


        return $this;
    } // setNumCertificat()

    /**
     * Set the value of [alerte_validation_consultation] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteValidationConsultation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_validation_consultation !== $v) {
            $this->alerte_validation_consultation = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_VALIDATION_CONSULTATION;
        }


        return $this;
    } // setAlerteValidationConsultation()

    /**
     * Set the value of [alerte_chorus] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteChorus($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_chorus !== $v) {
            $this->alerte_chorus = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_CHORUS;
        }


        return $this;
    } // setAlerteChorus()

    /**
     * Set the value of [password] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[] = CommonAgentPeer::PASSWORD;
        }


        return $this;
    } // setPassword()

    /**
     * Set the value of [code_theme] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCodeTheme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->code_theme !== $v) {
            $this->code_theme = $v;
            $this->modifiedColumns[] = CommonAgentPeer::CODE_THEME;
        }


        return $this;
    } // setCodeTheme()

    /**
     * Sets the value of [date_connexion] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setDateConnexion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_connexion !== null || $dt !== null) {
            $currentDateAsString = ($this->date_connexion !== null && $tmpDt = new DateTime($this->date_connexion)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_connexion = $newDateAsString;
                $this->modifiedColumns[] = CommonAgentPeer::DATE_CONNEXION;
            }
        } // if either are not null


        return $this;
    } // setDateConnexion()

    /**
     * Set the value of [type_hash] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setTypeHash($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->type_hash !== $v) {
            $this->type_hash = $v;
            $this->modifiedColumns[] = CommonAgentPeer::TYPE_HASH;
        }


        return $this;
    } // setTypeHash()

    /**
     * Set the value of [alerte_mes_consultations] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteMesConsultations($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_mes_consultations !== $v) {
            $this->alerte_mes_consultations = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_MES_CONSULTATIONS;
        }


        return $this;
    } // setAlerteMesConsultations()

    /**
     * Set the value of [alerte_consultations_mon_entite] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteConsultationsMonEntite($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_consultations_mon_entite !== $v) {
            $this->alerte_consultations_mon_entite = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_CONSULTATIONS_MON_ENTITE;
        }


        return $this;
    } // setAlerteConsultationsMonEntite()

    /**
     * Set the value of [alerte_consultations_des_entites_dependantes] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteConsultationsDesEntitesDependantes($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_consultations_des_entites_dependantes !== $v) {
            $this->alerte_consultations_des_entites_dependantes = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_CONSULTATIONS_DES_ENTITES_DEPENDANTES;
        }


        return $this;
    } // setAlerteConsultationsDesEntitesDependantes()

    /**
     * Set the value of [alerte_consultations_mes_entites_transverses] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setAlerteConsultationsMesEntitesTransverses($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alerte_consultations_mes_entites_transverses !== $v) {
            $this->alerte_consultations_mes_entites_transverses = $v;
            $this->modifiedColumns[] = CommonAgentPeer::ALERTE_CONSULTATIONS_MES_ENTITES_TRANSVERSES;
        }


        return $this;
    } // setAlerteConsultationsMesEntitesTransverses()

    /**
     * Sets the value of the [technique] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setTechnique($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->technique !== $v) {
            $this->technique = $v;
            $this->modifiedColumns[] = CommonAgentPeer::TECHNIQUE;
        }


        return $this;
    } // setTechnique()

    /**
     * Set the value of [service_id] column.
     *
     * @param string $v new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setServiceId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->service_id !== $v) {
            $this->service_id = $v;
            $this->modifiedColumns[] = CommonAgentPeer::SERVICE_ID;
        }

        if ($this->aCommonService !== null && $this->aCommonService->getId() !== $v) {
            $this->aCommonService = null;
        }


        return $this;
    } // setServiceId()

    /**
     * Sets the value of [date_validation_rgpd] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setDateValidationRgpd($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_validation_rgpd !== null || $dt !== null) {
            $currentDateAsString = ($this->date_validation_rgpd !== null && $tmpDt = new DateTime($this->date_validation_rgpd)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_validation_rgpd = $newDateAsString;
                $this->modifiedColumns[] = CommonAgentPeer::DATE_VALIDATION_RGPD;
            }
        } // if either are not null


        return $this;
    } // setDateValidationRgpd()

    /**
     * Sets the value of the [rgpd_communication_place] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setRgpdCommunicationPlace($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->rgpd_communication_place !== $v) {
            $this->rgpd_communication_place = $v;
            $this->modifiedColumns[] = CommonAgentPeer::RGPD_COMMUNICATION_PLACE;
        }


        return $this;
    } // setRgpdCommunicationPlace()

    /**
     * Sets the value of the [rgpd_enquete] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setRgpdEnquete($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->rgpd_enquete !== $v) {
            $this->rgpd_enquete = $v;
            $this->modifiedColumns[] = CommonAgentPeer::RGPD_ENQUETE;
        }


        return $this;
    } // setRgpdEnquete()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->email !== '') {
                return false;
            }

            if ($this->nom !== '') {
                return false;
            }

            if ($this->prenom !== '') {
                return false;
            }

            if ($this->tentatives_mdp !== 0) {
                return false;
            }

            if ($this->recevoir_mail !== '0') {
                return false;
            }

            if ($this->elu !== '0') {
                return false;
            }

            if ($this->nom_fonction !== '') {
                return false;
            }

            if ($this->type_comm !== '2') {
                return false;
            }

            if ($this->adr_postale !== '') {
                return false;
            }

            if ($this->civilite !== '') {
                return false;
            }

            if ($this->alerte_reponse_electronique !== '0') {
                return false;
            }

            if ($this->alerte_cloture_consultation !== '0') {
                return false;
            }

            if ($this->alerte_reception_message !== '0') {
                return false;
            }

            if ($this->alerte_publication_boamp !== '0') {
                return false;
            }

            if ($this->alerte_echec_publication_boamp !== '0') {
                return false;
            }

            if ($this->alerte_creation_modification_agent !== '0') {
                return false;
            }

            if ($this->id_externe !== '0') {
                return false;
            }

            if ($this->alerte_question_entreprise !== '0') {
                return false;
            }

            if ($this->actif !== '1') {
                return false;
            }

            if ($this->num_certificat !== '') {
                return false;
            }

            if ($this->alerte_validation_consultation !== '0') {
                return false;
            }

            if ($this->alerte_chorus !== '0') {
                return false;
            }

            if ($this->code_theme !== '0') {
                return false;
            }

            if ($this->type_hash !== 'SHA1') {
                return false;
            }

            if ($this->alerte_mes_consultations !== '1') {
                return false;
            }

            if ($this->alerte_consultations_mon_entite !== '1') {
                return false;
            }

            if ($this->alerte_consultations_des_entites_dependantes !== '1') {
                return false;
            }

            if ($this->alerte_consultations_mes_entites_transverses !== '1') {
                return false;
            }

            if ($this->technique !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->login = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->email = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->mdp = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->certificat = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->nom = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->prenom = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->tentatives_mdp = ($row[$startcol + 7] !== null) ? (int) $row[$startcol + 7] : null;
            $this->organisme = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->old_service_id = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->recevoir_mail = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->elu = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->nom_fonction = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->num_tel = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->num_fax = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->type_comm = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->adr_postale = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->civilite = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->alerte_reponse_electronique = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->alerte_cloture_consultation = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->alerte_reception_message = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->alerte_publication_boamp = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->alerte_echec_publication_boamp = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->alerte_creation_modification_agent = ($row[$startcol + 23] !== null) ? (string) $row[$startcol + 23] : null;
            $this->date_creation = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->date_modification = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->id_externe = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->id_profil_socle_externe = ($row[$startcol + 27] !== null) ? (int) $row[$startcol + 27] : null;
            $this->lieu_execution = ($row[$startcol + 28] !== null) ? (string) $row[$startcol + 28] : null;
            $this->alerte_question_entreprise = ($row[$startcol + 29] !== null) ? (string) $row[$startcol + 29] : null;
            $this->actif = ($row[$startcol + 30] !== null) ? (string) $row[$startcol + 30] : null;
            $this->deleted_at = ($row[$startcol + 31] !== null) ? (string) $row[$startcol + 31] : null;
            $this->codes_nuts = ($row[$startcol + 32] !== null) ? (string) $row[$startcol + 32] : null;
            $this->num_certificat = ($row[$startcol + 33] !== null) ? (string) $row[$startcol + 33] : null;
            $this->alerte_validation_consultation = ($row[$startcol + 34] !== null) ? (string) $row[$startcol + 34] : null;
            $this->alerte_chorus = ($row[$startcol + 35] !== null) ? (string) $row[$startcol + 35] : null;
            $this->password = ($row[$startcol + 36] !== null) ? (string) $row[$startcol + 36] : null;
            $this->code_theme = ($row[$startcol + 37] !== null) ? (string) $row[$startcol + 37] : null;
            $this->date_connexion = ($row[$startcol + 38] !== null) ? (string) $row[$startcol + 38] : null;
            $this->type_hash = ($row[$startcol + 39] !== null) ? (string) $row[$startcol + 39] : null;
            $this->alerte_mes_consultations = ($row[$startcol + 40] !== null) ? (string) $row[$startcol + 40] : null;
            $this->alerte_consultations_mon_entite = ($row[$startcol + 41] !== null) ? (string) $row[$startcol + 41] : null;
            $this->alerte_consultations_des_entites_dependantes = ($row[$startcol + 42] !== null) ? (string) $row[$startcol + 42] : null;
            $this->alerte_consultations_mes_entites_transverses = ($row[$startcol + 43] !== null) ? (string) $row[$startcol + 43] : null;
            $this->technique = ($row[$startcol + 44] !== null) ? (boolean) $row[$startcol + 44] : null;
            $this->service_id = ($row[$startcol + 45] !== null) ? (string) $row[$startcol + 45] : null;
            $this->date_validation_rgpd = ($row[$startcol + 46] !== null) ? (string) $row[$startcol + 46] : null;
            $this->rgpd_communication_place = ($row[$startcol + 47] !== null) ? (boolean) $row[$startcol + 47] : null;
            $this->rgpd_enquete = ($row[$startcol + 48] !== null) ? (boolean) $row[$startcol + 48] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 49; // 49 = CommonAgentPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonAgent object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonOrganisme !== null && $this->organisme !== $this->aCommonOrganisme->getAcronyme()) {
            $this->aCommonOrganisme = null;
        }
        if ($this->aCommonService !== null && $this->service_id !== $this->aCommonService->getId()) {
            $this->aCommonService = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonAgentPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonAgentPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonOrganisme = null;
            $this->aCommonService = null;
            $this->collCommonAgentServiceMetiers = null;

            $this->singleCommonHabilitationAgent = null;

            $this->collCommonAgentTechniqueTokens = null;

            $this->collCommonAnnexeFinancieres = null;

            $this->collCommonConsultationFavoriss = null;

            $this->collCommonDossierVolumineuxs = null;

            $this->collCommonEchangeDocs = null;

            $this->collCommonEchangeDocHistoriques = null;

            $this->collCommonHabilitationAgentWss = null;

            $this->collCommonInvitePermanentTransverses = null;

            $this->collCommonModificationContrats = null;

            $this->collCommonTCAOCommissionAgents = null;

            $this->collCommonTCAOOrdreDuJourIntervenants = null;

            $this->collCommonTCAOSeanceAgents = null;

            $this->collCommonTCAOSeanceInvites = null;

            $this->collCommonTVisionRmaAgentOrganismes = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonAgentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonAgentQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonAgentPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonAgentPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonOrganisme !== null) {
                if ($this->aCommonOrganisme->isModified() || $this->aCommonOrganisme->isNew()) {
                    $affectedRows += $this->aCommonOrganisme->save($con);
                }
                $this->setCommonOrganisme($this->aCommonOrganisme);
            }

            if ($this->aCommonService !== null) {
                if ($this->aCommonService->isModified() || $this->aCommonService->isNew()) {
                    $affectedRows += $this->aCommonService->save($con);
                }
                $this->setCommonService($this->aCommonService);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonAgentServiceMetiersScheduledForDeletion !== null) {
                if (!$this->commonAgentServiceMetiersScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonAgentServiceMetierQuery::create()
                        ->filterByPrimaryKeys($this->commonAgentServiceMetiersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonAgentServiceMetiersScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAgentServiceMetiers !== null) {
                foreach ($this->collCommonAgentServiceMetiers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonHabilitationAgentsScheduledForDeletion !== null) {
                if (!$this->commonHabilitationAgentsScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonHabilitationAgentQuery::create()
                        ->filterByPrimaryKeys($this->commonHabilitationAgentsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonHabilitationAgentsScheduledForDeletion = null;
                }
            }

            if ($this->singleCommonHabilitationAgent !== null) {
                if (!$this->singleCommonHabilitationAgent->isDeleted() && ($this->singleCommonHabilitationAgent->isNew() || $this->singleCommonHabilitationAgent->isModified())) {
                        $affectedRows += $this->singleCommonHabilitationAgent->save($con);
                }
            }

            if ($this->commonAgentTechniqueTokensScheduledForDeletion !== null) {
                if (!$this->commonAgentTechniqueTokensScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonAgentTechniqueTokenQuery::create()
                        ->filterByPrimaryKeys($this->commonAgentTechniqueTokensScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonAgentTechniqueTokensScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAgentTechniqueTokens !== null) {
                foreach ($this->collCommonAgentTechniqueTokens as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonAnnexeFinancieresScheduledForDeletion !== null) {
                if (!$this->commonAnnexeFinancieresScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonAnnexeFinancieresScheduledForDeletion as $commonAnnexeFinanciere) {
                        // need to save related object because we set the relation to null
                        $commonAnnexeFinanciere->save($con);
                    }
                    $this->commonAnnexeFinancieresScheduledForDeletion = null;
                }
            }

            if ($this->collCommonAnnexeFinancieres !== null) {
                foreach ($this->collCommonAnnexeFinancieres as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonConsultationFavorissScheduledForDeletion !== null) {
                if (!$this->commonConsultationFavorissScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonConsultationFavorisQuery::create()
                        ->filterByPrimaryKeys($this->commonConsultationFavorissScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonConsultationFavorissScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultationFavoriss !== null) {
                foreach ($this->collCommonConsultationFavoriss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonDossierVolumineuxsScheduledForDeletion !== null) {
                if (!$this->commonDossierVolumineuxsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonDossierVolumineuxsScheduledForDeletion as $commonDossierVolumineux) {
                        // need to save related object because we set the relation to null
                        $commonDossierVolumineux->save($con);
                    }
                    $this->commonDossierVolumineuxsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonDossierVolumineuxs !== null) {
                foreach ($this->collCommonDossierVolumineuxs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonEchangeDocsScheduledForDeletion !== null) {
                if (!$this->commonEchangeDocsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangeDocsScheduledForDeletion as $commonEchangeDoc) {
                        // need to save related object because we set the relation to null
                        $commonEchangeDoc->save($con);
                    }
                    $this->commonEchangeDocsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchangeDocs !== null) {
                foreach ($this->collCommonEchangeDocs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonEchangeDocHistoriquesScheduledForDeletion !== null) {
                if (!$this->commonEchangeDocHistoriquesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonEchangeDocHistoriquesScheduledForDeletion as $commonEchangeDocHistorique) {
                        // need to save related object because we set the relation to null
                        $commonEchangeDocHistorique->save($con);
                    }
                    $this->commonEchangeDocHistoriquesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonEchangeDocHistoriques !== null) {
                foreach ($this->collCommonEchangeDocHistoriques as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonHabilitationAgentWssScheduledForDeletion !== null) {
                if (!$this->commonHabilitationAgentWssScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonHabilitationAgentWsQuery::create()
                        ->filterByPrimaryKeys($this->commonHabilitationAgentWssScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonHabilitationAgentWssScheduledForDeletion = null;
                }
            }

            if ($this->collCommonHabilitationAgentWss !== null) {
                foreach ($this->collCommonHabilitationAgentWss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonInvitePermanentTransversesScheduledForDeletion !== null) {
                if (!$this->commonInvitePermanentTransversesScheduledForDeletion->isEmpty()) {
                    //the foreign key is flagged as `CASCADE`, so we delete the items
                    CommonInvitePermanentTransverseQuery::create()
                        ->filterByPrimaryKeys($this->commonInvitePermanentTransversesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->commonInvitePermanentTransversesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonInvitePermanentTransverses !== null) {
                foreach ($this->collCommonInvitePermanentTransverses as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonModificationContratsScheduledForDeletion !== null) {
                if (!$this->commonModificationContratsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonModificationContratsScheduledForDeletion as $commonModificationContrat) {
                        // need to save related object because we set the relation to null
                        $commonModificationContrat->save($con);
                    }
                    $this->commonModificationContratsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonModificationContrats !== null) {
                foreach ($this->collCommonModificationContrats as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOCommissionAgentsScheduledForDeletion !== null) {
                if (!$this->commonTCAOCommissionAgentsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOCommissionAgentsScheduledForDeletion as $commonTCAOCommissionAgent) {
                        // need to save related object because we set the relation to null
                        $commonTCAOCommissionAgent->save($con);
                    }
                    $this->commonTCAOCommissionAgentsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOCommissionAgents !== null) {
                foreach ($this->collCommonTCAOCommissionAgents as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion !== null) {
                if (!$this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion as $commonTCAOOrdreDuJourIntervenant) {
                        // need to save related object because we set the relation to null
                        $commonTCAOOrdreDuJourIntervenant->save($con);
                    }
                    $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOOrdreDuJourIntervenants !== null) {
                foreach ($this->collCommonTCAOOrdreDuJourIntervenants as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOSeanceAgentsScheduledForDeletion !== null) {
                if (!$this->commonTCAOSeanceAgentsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOSeanceAgentsScheduledForDeletion as $commonTCAOSeanceAgent) {
                        // need to save related object because we set the relation to null
                        $commonTCAOSeanceAgent->save($con);
                    }
                    $this->commonTCAOSeanceAgentsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOSeanceAgents !== null) {
                foreach ($this->collCommonTCAOSeanceAgents as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTCAOSeanceInvitesScheduledForDeletion !== null) {
                if (!$this->commonTCAOSeanceInvitesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTCAOSeanceInvitesScheduledForDeletion as $commonTCAOSeanceInvite) {
                        // need to save related object because we set the relation to null
                        $commonTCAOSeanceInvite->save($con);
                    }
                    $this->commonTCAOSeanceInvitesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTCAOSeanceInvites !== null) {
                foreach ($this->collCommonTCAOSeanceInvites as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTVisionRmaAgentOrganismesScheduledForDeletion !== null) {
                if (!$this->commonTVisionRmaAgentOrganismesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTVisionRmaAgentOrganismesScheduledForDeletion as $commonTVisionRmaAgentOrganisme) {
                        // need to save related object because we set the relation to null
                        $commonTVisionRmaAgentOrganisme->save($con);
                    }
                    $this->commonTVisionRmaAgentOrganismesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTVisionRmaAgentOrganismes !== null) {
                foreach ($this->collCommonTVisionRmaAgentOrganismes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonAgentPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonAgentPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonAgentPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonAgentPeer::LOGIN)) {
            $modifiedColumns[':p' . $index++]  = '`login`';
        }
        if ($this->isColumnModified(CommonAgentPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(CommonAgentPeer::MDP)) {
            $modifiedColumns[':p' . $index++]  = '`mdp`';
        }
        if ($this->isColumnModified(CommonAgentPeer::CERTIFICAT)) {
            $modifiedColumns[':p' . $index++]  = '`certificat`';
        }
        if ($this->isColumnModified(CommonAgentPeer::NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(CommonAgentPeer::PRENOM)) {
            $modifiedColumns[':p' . $index++]  = '`prenom`';
        }
        if ($this->isColumnModified(CommonAgentPeer::TENTATIVES_MDP)) {
            $modifiedColumns[':p' . $index++]  = '`tentatives_mdp`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonAgentPeer::OLD_SERVICE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`old_service_id`';
        }
        if ($this->isColumnModified(CommonAgentPeer::RECEVOIR_MAIL)) {
            $modifiedColumns[':p' . $index++]  = '`RECEVOIR_MAIL`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ELU)) {
            $modifiedColumns[':p' . $index++]  = '`elu`';
        }
        if ($this->isColumnModified(CommonAgentPeer::NOM_FONCTION)) {
            $modifiedColumns[':p' . $index++]  = '`nom_fonction`';
        }
        if ($this->isColumnModified(CommonAgentPeer::NUM_TEL)) {
            $modifiedColumns[':p' . $index++]  = '`num_tel`';
        }
        if ($this->isColumnModified(CommonAgentPeer::NUM_FAX)) {
            $modifiedColumns[':p' . $index++]  = '`num_fax`';
        }
        if ($this->isColumnModified(CommonAgentPeer::TYPE_COMM)) {
            $modifiedColumns[':p' . $index++]  = '`type_comm`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ADR_POSTALE)) {
            $modifiedColumns[':p' . $index++]  = '`adr_postale`';
        }
        if ($this->isColumnModified(CommonAgentPeer::CIVILITE)) {
            $modifiedColumns[':p' . $index++]  = '`civilite`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_REPONSE_ELECTRONIQUE)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_reponse_electronique`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CLOTURE_CONSULTATION)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_cloture_consultation`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_RECEPTION_MESSAGE)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_reception_message`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_PUBLICATION_BOAMP)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_publication_boamp`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_ECHEC_PUBLICATION_BOAMP)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_echec_publication_boamp`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CREATION_MODIFICATION_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_creation_modification_agent`';
        }
        if ($this->isColumnModified(CommonAgentPeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonAgentPeer::DATE_MODIFICATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_modification`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ID_EXTERNE)) {
            $modifiedColumns[':p' . $index++]  = '`id_externe`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ID_PROFIL_SOCLE_EXTERNE)) {
            $modifiedColumns[':p' . $index++]  = '`id_profil_socle_externe`';
        }
        if ($this->isColumnModified(CommonAgentPeer::LIEU_EXECUTION)) {
            $modifiedColumns[':p' . $index++]  = '`lieu_execution`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_QUESTION_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_question_entreprise`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ACTIF)) {
            $modifiedColumns[':p' . $index++]  = '`actif`';
        }
        if ($this->isColumnModified(CommonAgentPeer::DELETED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`deleted_at`';
        }
        if ($this->isColumnModified(CommonAgentPeer::CODES_NUTS)) {
            $modifiedColumns[':p' . $index++]  = '`codes_nuts`';
        }
        if ($this->isColumnModified(CommonAgentPeer::NUM_CERTIFICAT)) {
            $modifiedColumns[':p' . $index++]  = '`num_certificat`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_VALIDATION_CONSULTATION)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_validation_consultation`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CHORUS)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_chorus`';
        }
        if ($this->isColumnModified(CommonAgentPeer::PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = '`password`';
        }
        if ($this->isColumnModified(CommonAgentPeer::CODE_THEME)) {
            $modifiedColumns[':p' . $index++]  = '`code_theme`';
        }
        if ($this->isColumnModified(CommonAgentPeer::DATE_CONNEXION)) {
            $modifiedColumns[':p' . $index++]  = '`date_connexion`';
        }
        if ($this->isColumnModified(CommonAgentPeer::TYPE_HASH)) {
            $modifiedColumns[':p' . $index++]  = '`type_hash`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_MES_CONSULTATIONS)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_mes_consultations`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CONSULTATIONS_MON_ENTITE)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_consultations_mon_entite`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CONSULTATIONS_DES_ENTITES_DEPENDANTES)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_consultations_des_entites_dependantes`';
        }
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CONSULTATIONS_MES_ENTITES_TRANSVERSES)) {
            $modifiedColumns[':p' . $index++]  = '`alerte_consultations_mes_entites_transverses`';
        }
        if ($this->isColumnModified(CommonAgentPeer::TECHNIQUE)) {
            $modifiedColumns[':p' . $index++]  = '`technique`';
        }
        if ($this->isColumnModified(CommonAgentPeer::SERVICE_ID)) {
            $modifiedColumns[':p' . $index++]  = '`service_id`';
        }
        if ($this->isColumnModified(CommonAgentPeer::DATE_VALIDATION_RGPD)) {
            $modifiedColumns[':p' . $index++]  = '`date_validation_rgpd`';
        }
        if ($this->isColumnModified(CommonAgentPeer::RGPD_COMMUNICATION_PLACE)) {
            $modifiedColumns[':p' . $index++]  = '`rgpd_communication_place`';
        }
        if ($this->isColumnModified(CommonAgentPeer::RGPD_ENQUETE)) {
            $modifiedColumns[':p' . $index++]  = '`rgpd_enquete`';
        }

        $sql = sprintf(
            'INSERT INTO `Agent` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`login`':
                        $stmt->bindValue($identifier, $this->login, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`mdp`':
                        $stmt->bindValue($identifier, $this->mdp, PDO::PARAM_STR);
                        break;
                    case '`certificat`':
                        $stmt->bindValue($identifier, $this->certificat, PDO::PARAM_STR);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`prenom`':
                        $stmt->bindValue($identifier, $this->prenom, PDO::PARAM_STR);
                        break;
                    case '`tentatives_mdp`':
                        $stmt->bindValue($identifier, $this->tentatives_mdp, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`old_service_id`':
                        $stmt->bindValue($identifier, $this->old_service_id, PDO::PARAM_INT);
                        break;
                    case '`RECEVOIR_MAIL`':
                        $stmt->bindValue($identifier, $this->recevoir_mail, PDO::PARAM_STR);
                        break;
                    case '`elu`':
                        $stmt->bindValue($identifier, $this->elu, PDO::PARAM_STR);
                        break;
                    case '`nom_fonction`':
                        $stmt->bindValue($identifier, $this->nom_fonction, PDO::PARAM_STR);
                        break;
                    case '`num_tel`':
                        $stmt->bindValue($identifier, $this->num_tel, PDO::PARAM_STR);
                        break;
                    case '`num_fax`':
                        $stmt->bindValue($identifier, $this->num_fax, PDO::PARAM_STR);
                        break;
                    case '`type_comm`':
                        $stmt->bindValue($identifier, $this->type_comm, PDO::PARAM_STR);
                        break;
                    case '`adr_postale`':
                        $stmt->bindValue($identifier, $this->adr_postale, PDO::PARAM_STR);
                        break;
                    case '`civilite`':
                        $stmt->bindValue($identifier, $this->civilite, PDO::PARAM_STR);
                        break;
                    case '`alerte_reponse_electronique`':
                        $stmt->bindValue($identifier, $this->alerte_reponse_electronique, PDO::PARAM_STR);
                        break;
                    case '`alerte_cloture_consultation`':
                        $stmt->bindValue($identifier, $this->alerte_cloture_consultation, PDO::PARAM_STR);
                        break;
                    case '`alerte_reception_message`':
                        $stmt->bindValue($identifier, $this->alerte_reception_message, PDO::PARAM_STR);
                        break;
                    case '`alerte_publication_boamp`':
                        $stmt->bindValue($identifier, $this->alerte_publication_boamp, PDO::PARAM_STR);
                        break;
                    case '`alerte_echec_publication_boamp`':
                        $stmt->bindValue($identifier, $this->alerte_echec_publication_boamp, PDO::PARAM_STR);
                        break;
                    case '`alerte_creation_modification_agent`':
                        $stmt->bindValue($identifier, $this->alerte_creation_modification_agent, PDO::PARAM_STR);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_modification`':
                        $stmt->bindValue($identifier, $this->date_modification, PDO::PARAM_STR);
                        break;
                    case '`id_externe`':
                        $stmt->bindValue($identifier, $this->id_externe, PDO::PARAM_STR);
                        break;
                    case '`id_profil_socle_externe`':
                        $stmt->bindValue($identifier, $this->id_profil_socle_externe, PDO::PARAM_INT);
                        break;
                    case '`lieu_execution`':
                        $stmt->bindValue($identifier, $this->lieu_execution, PDO::PARAM_STR);
                        break;
                    case '`alerte_question_entreprise`':
                        $stmt->bindValue($identifier, $this->alerte_question_entreprise, PDO::PARAM_STR);
                        break;
                    case '`actif`':
                        $stmt->bindValue($identifier, $this->actif, PDO::PARAM_STR);
                        break;
                    case '`deleted_at`':
                        $stmt->bindValue($identifier, $this->deleted_at, PDO::PARAM_STR);
                        break;
                    case '`codes_nuts`':
                        $stmt->bindValue($identifier, $this->codes_nuts, PDO::PARAM_STR);
                        break;
                    case '`num_certificat`':
                        $stmt->bindValue($identifier, $this->num_certificat, PDO::PARAM_STR);
                        break;
                    case '`alerte_validation_consultation`':
                        $stmt->bindValue($identifier, $this->alerte_validation_consultation, PDO::PARAM_STR);
                        break;
                    case '`alerte_chorus`':
                        $stmt->bindValue($identifier, $this->alerte_chorus, PDO::PARAM_STR);
                        break;
                    case '`password`':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case '`code_theme`':
                        $stmt->bindValue($identifier, $this->code_theme, PDO::PARAM_STR);
                        break;
                    case '`date_connexion`':
                        $stmt->bindValue($identifier, $this->date_connexion, PDO::PARAM_STR);
                        break;
                    case '`type_hash`':
                        $stmt->bindValue($identifier, $this->type_hash, PDO::PARAM_STR);
                        break;
                    case '`alerte_mes_consultations`':
                        $stmt->bindValue($identifier, $this->alerte_mes_consultations, PDO::PARAM_STR);
                        break;
                    case '`alerte_consultations_mon_entite`':
                        $stmt->bindValue($identifier, $this->alerte_consultations_mon_entite, PDO::PARAM_STR);
                        break;
                    case '`alerte_consultations_des_entites_dependantes`':
                        $stmt->bindValue($identifier, $this->alerte_consultations_des_entites_dependantes, PDO::PARAM_STR);
                        break;
                    case '`alerte_consultations_mes_entites_transverses`':
                        $stmt->bindValue($identifier, $this->alerte_consultations_mes_entites_transverses, PDO::PARAM_STR);
                        break;
                    case '`technique`':
                        $stmt->bindValue($identifier, (int) $this->technique, PDO::PARAM_INT);
                        break;
                    case '`service_id`':
                        $stmt->bindValue($identifier, $this->service_id, PDO::PARAM_STR);
                        break;
                    case '`date_validation_rgpd`':
                        $stmt->bindValue($identifier, $this->date_validation_rgpd, PDO::PARAM_STR);
                        break;
                    case '`rgpd_communication_place`':
                        $stmt->bindValue($identifier, (int) $this->rgpd_communication_place, PDO::PARAM_INT);
                        break;
                    case '`rgpd_enquete`':
                        $stmt->bindValue($identifier, (int) $this->rgpd_enquete, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonOrganisme !== null) {
                if (!$this->aCommonOrganisme->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonOrganisme->getValidationFailures());
                }
            }

            if ($this->aCommonService !== null) {
                if (!$this->aCommonService->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonService->getValidationFailures());
                }
            }


            if (($retval = CommonAgentPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonAgentServiceMetiers !== null) {
                    foreach ($this->collCommonAgentServiceMetiers as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->singleCommonHabilitationAgent !== null) {
                    if (!$this->singleCommonHabilitationAgent->validate($columns)) {
                        $failureMap = array_merge($failureMap, $this->singleCommonHabilitationAgent->getValidationFailures());
                    }
                }

                if ($this->collCommonAgentTechniqueTokens !== null) {
                    foreach ($this->collCommonAgentTechniqueTokens as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonAnnexeFinancieres !== null) {
                    foreach ($this->collCommonAnnexeFinancieres as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonConsultationFavoriss !== null) {
                    foreach ($this->collCommonConsultationFavoriss as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonDossierVolumineuxs !== null) {
                    foreach ($this->collCommonDossierVolumineuxs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonEchangeDocs !== null) {
                    foreach ($this->collCommonEchangeDocs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonEchangeDocHistoriques !== null) {
                    foreach ($this->collCommonEchangeDocHistoriques as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonHabilitationAgentWss !== null) {
                    foreach ($this->collCommonHabilitationAgentWss as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonInvitePermanentTransverses !== null) {
                    foreach ($this->collCommonInvitePermanentTransverses as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonModificationContrats !== null) {
                    foreach ($this->collCommonModificationContrats as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOCommissionAgents !== null) {
                    foreach ($this->collCommonTCAOCommissionAgents as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOOrdreDuJourIntervenants !== null) {
                    foreach ($this->collCommonTCAOOrdreDuJourIntervenants as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOSeanceAgents !== null) {
                    foreach ($this->collCommonTCAOSeanceAgents as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTCAOSeanceInvites !== null) {
                    foreach ($this->collCommonTCAOSeanceInvites as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTVisionRmaAgentOrganismes !== null) {
                    foreach ($this->collCommonTVisionRmaAgentOrganismes as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonAgentPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getLogin();
                break;
            case 2:
                return $this->getEmail();
                break;
            case 3:
                return $this->getMdp();
                break;
            case 4:
                return $this->getCertificat();
                break;
            case 5:
                return $this->getNom();
                break;
            case 6:
                return $this->getPrenom();
                break;
            case 7:
                return $this->getTentativesMdp();
                break;
            case 8:
                return $this->getOrganisme();
                break;
            case 9:
                return $this->getOldServiceId();
                break;
            case 10:
                return $this->getRecevoirMail();
                break;
            case 11:
                return $this->getElu();
                break;
            case 12:
                return $this->getNomFonction();
                break;
            case 13:
                return $this->getNumTel();
                break;
            case 14:
                return $this->getNumFax();
                break;
            case 15:
                return $this->getTypeComm();
                break;
            case 16:
                return $this->getAdrPostale();
                break;
            case 17:
                return $this->getCivilite();
                break;
            case 18:
                return $this->getAlerteReponseElectronique();
                break;
            case 19:
                return $this->getAlerteClotureConsultation();
                break;
            case 20:
                return $this->getAlerteReceptionMessage();
                break;
            case 21:
                return $this->getAlertePublicationBoamp();
                break;
            case 22:
                return $this->getAlerteEchecPublicationBoamp();
                break;
            case 23:
                return $this->getAlerteCreationModificationAgent();
                break;
            case 24:
                return $this->getDateCreation();
                break;
            case 25:
                return $this->getDateModification();
                break;
            case 26:
                return $this->getIdExterne();
                break;
            case 27:
                return $this->getIdProfilSocleExterne();
                break;
            case 28:
                return $this->getLieuExecution();
                break;
            case 29:
                return $this->getAlerteQuestionEntreprise();
                break;
            case 30:
                return $this->getActif();
                break;
            case 31:
                return $this->getDeletedAt();
                break;
            case 32:
                return $this->getCodesNuts();
                break;
            case 33:
                return $this->getNumCertificat();
                break;
            case 34:
                return $this->getAlerteValidationConsultation();
                break;
            case 35:
                return $this->getAlerteChorus();
                break;
            case 36:
                return $this->getPassword();
                break;
            case 37:
                return $this->getCodeTheme();
                break;
            case 38:
                return $this->getDateConnexion();
                break;
            case 39:
                return $this->getTypeHash();
                break;
            case 40:
                return $this->getAlerteMesConsultations();
                break;
            case 41:
                return $this->getAlerteConsultationsMonEntite();
                break;
            case 42:
                return $this->getAlerteConsultationsDesEntitesDependantes();
                break;
            case 43:
                return $this->getAlerteConsultationsMesEntitesTransverses();
                break;
            case 44:
                return $this->getTechnique();
                break;
            case 45:
                return $this->getServiceId();
                break;
            case 46:
                return $this->getDateValidationRgpd();
                break;
            case 47:
                return $this->getRgpdCommunicationPlace();
                break;
            case 48:
                return $this->getRgpdEnquete();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonAgent'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonAgent'][$this->getPrimaryKey()] = true;
        $keys = CommonAgentPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getLogin(),
            $keys[2] => $this->getEmail(),
            $keys[3] => $this->getMdp(),
            $keys[4] => $this->getCertificat(),
            $keys[5] => $this->getNom(),
            $keys[6] => $this->getPrenom(),
            $keys[7] => $this->getTentativesMdp(),
            $keys[8] => $this->getOrganisme(),
            $keys[9] => $this->getOldServiceId(),
            $keys[10] => $this->getRecevoirMail(),
            $keys[11] => $this->getElu(),
            $keys[12] => $this->getNomFonction(),
            $keys[13] => $this->getNumTel(),
            $keys[14] => $this->getNumFax(),
            $keys[15] => $this->getTypeComm(),
            $keys[16] => $this->getAdrPostale(),
            $keys[17] => $this->getCivilite(),
            $keys[18] => $this->getAlerteReponseElectronique(),
            $keys[19] => $this->getAlerteClotureConsultation(),
            $keys[20] => $this->getAlerteReceptionMessage(),
            $keys[21] => $this->getAlertePublicationBoamp(),
            $keys[22] => $this->getAlerteEchecPublicationBoamp(),
            $keys[23] => $this->getAlerteCreationModificationAgent(),
            $keys[24] => $this->getDateCreation(),
            $keys[25] => $this->getDateModification(),
            $keys[26] => $this->getIdExterne(),
            $keys[27] => $this->getIdProfilSocleExterne(),
            $keys[28] => $this->getLieuExecution(),
            $keys[29] => $this->getAlerteQuestionEntreprise(),
            $keys[30] => $this->getActif(),
            $keys[31] => $this->getDeletedAt(),
            $keys[32] => $this->getCodesNuts(),
            $keys[33] => $this->getNumCertificat(),
            $keys[34] => $this->getAlerteValidationConsultation(),
            $keys[35] => $this->getAlerteChorus(),
            $keys[36] => $this->getPassword(),
            $keys[37] => $this->getCodeTheme(),
            $keys[38] => $this->getDateConnexion(),
            $keys[39] => $this->getTypeHash(),
            $keys[40] => $this->getAlerteMesConsultations(),
            $keys[41] => $this->getAlerteConsultationsMonEntite(),
            $keys[42] => $this->getAlerteConsultationsDesEntitesDependantes(),
            $keys[43] => $this->getAlerteConsultationsMesEntitesTransverses(),
            $keys[44] => $this->getTechnique(),
            $keys[45] => $this->getServiceId(),
            $keys[46] => $this->getDateValidationRgpd(),
            $keys[47] => $this->getRgpdCommunicationPlace(),
            $keys[48] => $this->getRgpdEnquete(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonOrganisme) {
                $result['CommonOrganisme'] = $this->aCommonOrganisme->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonService) {
                $result['CommonService'] = $this->aCommonService->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonAgentServiceMetiers) {
                $result['CommonAgentServiceMetiers'] = $this->collCommonAgentServiceMetiers->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->singleCommonHabilitationAgent) {
                $result['CommonHabilitationAgent'] = $this->singleCommonHabilitationAgent->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonAgentTechniqueTokens) {
                $result['CommonAgentTechniqueTokens'] = $this->collCommonAgentTechniqueTokens->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonAnnexeFinancieres) {
                $result['CommonAnnexeFinancieres'] = $this->collCommonAnnexeFinancieres->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonConsultationFavoriss) {
                $result['CommonConsultationFavoriss'] = $this->collCommonConsultationFavoriss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonDossierVolumineuxs) {
                $result['CommonDossierVolumineuxs'] = $this->collCommonDossierVolumineuxs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonEchangeDocs) {
                $result['CommonEchangeDocs'] = $this->collCommonEchangeDocs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonEchangeDocHistoriques) {
                $result['CommonEchangeDocHistoriques'] = $this->collCommonEchangeDocHistoriques->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonHabilitationAgentWss) {
                $result['CommonHabilitationAgentWss'] = $this->collCommonHabilitationAgentWss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonInvitePermanentTransverses) {
                $result['CommonInvitePermanentTransverses'] = $this->collCommonInvitePermanentTransverses->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonModificationContrats) {
                $result['CommonModificationContrats'] = $this->collCommonModificationContrats->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOCommissionAgents) {
                $result['CommonTCAOCommissionAgents'] = $this->collCommonTCAOCommissionAgents->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOOrdreDuJourIntervenants) {
                $result['CommonTCAOOrdreDuJourIntervenants'] = $this->collCommonTCAOOrdreDuJourIntervenants->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOSeanceAgents) {
                $result['CommonTCAOSeanceAgents'] = $this->collCommonTCAOSeanceAgents->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTCAOSeanceInvites) {
                $result['CommonTCAOSeanceInvites'] = $this->collCommonTCAOSeanceInvites->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTVisionRmaAgentOrganismes) {
                $result['CommonTVisionRmaAgentOrganismes'] = $this->collCommonTVisionRmaAgentOrganismes->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonAgentPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setLogin($value);
                break;
            case 2:
                $this->setEmail($value);
                break;
            case 3:
                $this->setMdp($value);
                break;
            case 4:
                $this->setCertificat($value);
                break;
            case 5:
                $this->setNom($value);
                break;
            case 6:
                $this->setPrenom($value);
                break;
            case 7:
                $this->setTentativesMdp($value);
                break;
            case 8:
                $this->setOrganisme($value);
                break;
            case 9:
                $this->setOldServiceId($value);
                break;
            case 10:
                $this->setRecevoirMail($value);
                break;
            case 11:
                $this->setElu($value);
                break;
            case 12:
                $this->setNomFonction($value);
                break;
            case 13:
                $this->setNumTel($value);
                break;
            case 14:
                $this->setNumFax($value);
                break;
            case 15:
                $this->setTypeComm($value);
                break;
            case 16:
                $this->setAdrPostale($value);
                break;
            case 17:
                $this->setCivilite($value);
                break;
            case 18:
                $this->setAlerteReponseElectronique($value);
                break;
            case 19:
                $this->setAlerteClotureConsultation($value);
                break;
            case 20:
                $this->setAlerteReceptionMessage($value);
                break;
            case 21:
                $this->setAlertePublicationBoamp($value);
                break;
            case 22:
                $this->setAlerteEchecPublicationBoamp($value);
                break;
            case 23:
                $this->setAlerteCreationModificationAgent($value);
                break;
            case 24:
                $this->setDateCreation($value);
                break;
            case 25:
                $this->setDateModification($value);
                break;
            case 26:
                $this->setIdExterne($value);
                break;
            case 27:
                $this->setIdProfilSocleExterne($value);
                break;
            case 28:
                $this->setLieuExecution($value);
                break;
            case 29:
                $this->setAlerteQuestionEntreprise($value);
                break;
            case 30:
                $this->setActif($value);
                break;
            case 31:
                $this->setDeletedAt($value);
                break;
            case 32:
                $this->setCodesNuts($value);
                break;
            case 33:
                $this->setNumCertificat($value);
                break;
            case 34:
                $this->setAlerteValidationConsultation($value);
                break;
            case 35:
                $this->setAlerteChorus($value);
                break;
            case 36:
                $this->setPassword($value);
                break;
            case 37:
                $this->setCodeTheme($value);
                break;
            case 38:
                $this->setDateConnexion($value);
                break;
            case 39:
                $this->setTypeHash($value);
                break;
            case 40:
                $this->setAlerteMesConsultations($value);
                break;
            case 41:
                $this->setAlerteConsultationsMonEntite($value);
                break;
            case 42:
                $this->setAlerteConsultationsDesEntitesDependantes($value);
                break;
            case 43:
                $this->setAlerteConsultationsMesEntitesTransverses($value);
                break;
            case 44:
                $this->setTechnique($value);
                break;
            case 45:
                $this->setServiceId($value);
                break;
            case 46:
                $this->setDateValidationRgpd($value);
                break;
            case 47:
                $this->setRgpdCommunicationPlace($value);
                break;
            case 48:
                $this->setRgpdEnquete($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonAgentPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setLogin($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setEmail($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setMdp($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setCertificat($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setNom($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setPrenom($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setTentativesMdp($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setOrganisme($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setOldServiceId($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setRecevoirMail($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setElu($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setNomFonction($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setNumTel($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setNumFax($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setTypeComm($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setAdrPostale($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setCivilite($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setAlerteReponseElectronique($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setAlerteClotureConsultation($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setAlerteReceptionMessage($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setAlertePublicationBoamp($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setAlerteEchecPublicationBoamp($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setAlerteCreationModificationAgent($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setDateCreation($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setDateModification($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setIdExterne($arr[$keys[26]]);
        if (array_key_exists($keys[27], $arr)) $this->setIdProfilSocleExterne($arr[$keys[27]]);
        if (array_key_exists($keys[28], $arr)) $this->setLieuExecution($arr[$keys[28]]);
        if (array_key_exists($keys[29], $arr)) $this->setAlerteQuestionEntreprise($arr[$keys[29]]);
        if (array_key_exists($keys[30], $arr)) $this->setActif($arr[$keys[30]]);
        if (array_key_exists($keys[31], $arr)) $this->setDeletedAt($arr[$keys[31]]);
        if (array_key_exists($keys[32], $arr)) $this->setCodesNuts($arr[$keys[32]]);
        if (array_key_exists($keys[33], $arr)) $this->setNumCertificat($arr[$keys[33]]);
        if (array_key_exists($keys[34], $arr)) $this->setAlerteValidationConsultation($arr[$keys[34]]);
        if (array_key_exists($keys[35], $arr)) $this->setAlerteChorus($arr[$keys[35]]);
        if (array_key_exists($keys[36], $arr)) $this->setPassword($arr[$keys[36]]);
        if (array_key_exists($keys[37], $arr)) $this->setCodeTheme($arr[$keys[37]]);
        if (array_key_exists($keys[38], $arr)) $this->setDateConnexion($arr[$keys[38]]);
        if (array_key_exists($keys[39], $arr)) $this->setTypeHash($arr[$keys[39]]);
        if (array_key_exists($keys[40], $arr)) $this->setAlerteMesConsultations($arr[$keys[40]]);
        if (array_key_exists($keys[41], $arr)) $this->setAlerteConsultationsMonEntite($arr[$keys[41]]);
        if (array_key_exists($keys[42], $arr)) $this->setAlerteConsultationsDesEntitesDependantes($arr[$keys[42]]);
        if (array_key_exists($keys[43], $arr)) $this->setAlerteConsultationsMesEntitesTransverses($arr[$keys[43]]);
        if (array_key_exists($keys[44], $arr)) $this->setTechnique($arr[$keys[44]]);
        if (array_key_exists($keys[45], $arr)) $this->setServiceId($arr[$keys[45]]);
        if (array_key_exists($keys[46], $arr)) $this->setDateValidationRgpd($arr[$keys[46]]);
        if (array_key_exists($keys[47], $arr)) $this->setRgpdCommunicationPlace($arr[$keys[47]]);
        if (array_key_exists($keys[48], $arr)) $this->setRgpdEnquete($arr[$keys[48]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonAgentPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonAgentPeer::ID)) $criteria->add(CommonAgentPeer::ID, $this->id);
        if ($this->isColumnModified(CommonAgentPeer::LOGIN)) $criteria->add(CommonAgentPeer::LOGIN, $this->login);
        if ($this->isColumnModified(CommonAgentPeer::EMAIL)) $criteria->add(CommonAgentPeer::EMAIL, $this->email);
        if ($this->isColumnModified(CommonAgentPeer::MDP)) $criteria->add(CommonAgentPeer::MDP, $this->mdp);
        if ($this->isColumnModified(CommonAgentPeer::CERTIFICAT)) $criteria->add(CommonAgentPeer::CERTIFICAT, $this->certificat);
        if ($this->isColumnModified(CommonAgentPeer::NOM)) $criteria->add(CommonAgentPeer::NOM, $this->nom);
        if ($this->isColumnModified(CommonAgentPeer::PRENOM)) $criteria->add(CommonAgentPeer::PRENOM, $this->prenom);
        if ($this->isColumnModified(CommonAgentPeer::TENTATIVES_MDP)) $criteria->add(CommonAgentPeer::TENTATIVES_MDP, $this->tentatives_mdp);
        if ($this->isColumnModified(CommonAgentPeer::ORGANISME)) $criteria->add(CommonAgentPeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonAgentPeer::OLD_SERVICE_ID)) $criteria->add(CommonAgentPeer::OLD_SERVICE_ID, $this->old_service_id);
        if ($this->isColumnModified(CommonAgentPeer::RECEVOIR_MAIL)) $criteria->add(CommonAgentPeer::RECEVOIR_MAIL, $this->recevoir_mail);
        if ($this->isColumnModified(CommonAgentPeer::ELU)) $criteria->add(CommonAgentPeer::ELU, $this->elu);
        if ($this->isColumnModified(CommonAgentPeer::NOM_FONCTION)) $criteria->add(CommonAgentPeer::NOM_FONCTION, $this->nom_fonction);
        if ($this->isColumnModified(CommonAgentPeer::NUM_TEL)) $criteria->add(CommonAgentPeer::NUM_TEL, $this->num_tel);
        if ($this->isColumnModified(CommonAgentPeer::NUM_FAX)) $criteria->add(CommonAgentPeer::NUM_FAX, $this->num_fax);
        if ($this->isColumnModified(CommonAgentPeer::TYPE_COMM)) $criteria->add(CommonAgentPeer::TYPE_COMM, $this->type_comm);
        if ($this->isColumnModified(CommonAgentPeer::ADR_POSTALE)) $criteria->add(CommonAgentPeer::ADR_POSTALE, $this->adr_postale);
        if ($this->isColumnModified(CommonAgentPeer::CIVILITE)) $criteria->add(CommonAgentPeer::CIVILITE, $this->civilite);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_REPONSE_ELECTRONIQUE)) $criteria->add(CommonAgentPeer::ALERTE_REPONSE_ELECTRONIQUE, $this->alerte_reponse_electronique);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CLOTURE_CONSULTATION)) $criteria->add(CommonAgentPeer::ALERTE_CLOTURE_CONSULTATION, $this->alerte_cloture_consultation);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_RECEPTION_MESSAGE)) $criteria->add(CommonAgentPeer::ALERTE_RECEPTION_MESSAGE, $this->alerte_reception_message);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_PUBLICATION_BOAMP)) $criteria->add(CommonAgentPeer::ALERTE_PUBLICATION_BOAMP, $this->alerte_publication_boamp);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_ECHEC_PUBLICATION_BOAMP)) $criteria->add(CommonAgentPeer::ALERTE_ECHEC_PUBLICATION_BOAMP, $this->alerte_echec_publication_boamp);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CREATION_MODIFICATION_AGENT)) $criteria->add(CommonAgentPeer::ALERTE_CREATION_MODIFICATION_AGENT, $this->alerte_creation_modification_agent);
        if ($this->isColumnModified(CommonAgentPeer::DATE_CREATION)) $criteria->add(CommonAgentPeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonAgentPeer::DATE_MODIFICATION)) $criteria->add(CommonAgentPeer::DATE_MODIFICATION, $this->date_modification);
        if ($this->isColumnModified(CommonAgentPeer::ID_EXTERNE)) $criteria->add(CommonAgentPeer::ID_EXTERNE, $this->id_externe);
        if ($this->isColumnModified(CommonAgentPeer::ID_PROFIL_SOCLE_EXTERNE)) $criteria->add(CommonAgentPeer::ID_PROFIL_SOCLE_EXTERNE, $this->id_profil_socle_externe);
        if ($this->isColumnModified(CommonAgentPeer::LIEU_EXECUTION)) $criteria->add(CommonAgentPeer::LIEU_EXECUTION, $this->lieu_execution);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_QUESTION_ENTREPRISE)) $criteria->add(CommonAgentPeer::ALERTE_QUESTION_ENTREPRISE, $this->alerte_question_entreprise);
        if ($this->isColumnModified(CommonAgentPeer::ACTIF)) $criteria->add(CommonAgentPeer::ACTIF, $this->actif);
        if ($this->isColumnModified(CommonAgentPeer::DELETED_AT)) $criteria->add(CommonAgentPeer::DELETED_AT, $this->deleted_at);
        if ($this->isColumnModified(CommonAgentPeer::CODES_NUTS)) $criteria->add(CommonAgentPeer::CODES_NUTS, $this->codes_nuts);
        if ($this->isColumnModified(CommonAgentPeer::NUM_CERTIFICAT)) $criteria->add(CommonAgentPeer::NUM_CERTIFICAT, $this->num_certificat);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_VALIDATION_CONSULTATION)) $criteria->add(CommonAgentPeer::ALERTE_VALIDATION_CONSULTATION, $this->alerte_validation_consultation);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CHORUS)) $criteria->add(CommonAgentPeer::ALERTE_CHORUS, $this->alerte_chorus);
        if ($this->isColumnModified(CommonAgentPeer::PASSWORD)) $criteria->add(CommonAgentPeer::PASSWORD, $this->password);
        if ($this->isColumnModified(CommonAgentPeer::CODE_THEME)) $criteria->add(CommonAgentPeer::CODE_THEME, $this->code_theme);
        if ($this->isColumnModified(CommonAgentPeer::DATE_CONNEXION)) $criteria->add(CommonAgentPeer::DATE_CONNEXION, $this->date_connexion);
        if ($this->isColumnModified(CommonAgentPeer::TYPE_HASH)) $criteria->add(CommonAgentPeer::TYPE_HASH, $this->type_hash);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_MES_CONSULTATIONS)) $criteria->add(CommonAgentPeer::ALERTE_MES_CONSULTATIONS, $this->alerte_mes_consultations);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CONSULTATIONS_MON_ENTITE)) $criteria->add(CommonAgentPeer::ALERTE_CONSULTATIONS_MON_ENTITE, $this->alerte_consultations_mon_entite);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CONSULTATIONS_DES_ENTITES_DEPENDANTES)) $criteria->add(CommonAgentPeer::ALERTE_CONSULTATIONS_DES_ENTITES_DEPENDANTES, $this->alerte_consultations_des_entites_dependantes);
        if ($this->isColumnModified(CommonAgentPeer::ALERTE_CONSULTATIONS_MES_ENTITES_TRANSVERSES)) $criteria->add(CommonAgentPeer::ALERTE_CONSULTATIONS_MES_ENTITES_TRANSVERSES, $this->alerte_consultations_mes_entites_transverses);
        if ($this->isColumnModified(CommonAgentPeer::TECHNIQUE)) $criteria->add(CommonAgentPeer::TECHNIQUE, $this->technique);
        if ($this->isColumnModified(CommonAgentPeer::SERVICE_ID)) $criteria->add(CommonAgentPeer::SERVICE_ID, $this->service_id);
        if ($this->isColumnModified(CommonAgentPeer::DATE_VALIDATION_RGPD)) $criteria->add(CommonAgentPeer::DATE_VALIDATION_RGPD, $this->date_validation_rgpd);
        if ($this->isColumnModified(CommonAgentPeer::RGPD_COMMUNICATION_PLACE)) $criteria->add(CommonAgentPeer::RGPD_COMMUNICATION_PLACE, $this->rgpd_communication_place);
        if ($this->isColumnModified(CommonAgentPeer::RGPD_ENQUETE)) $criteria->add(CommonAgentPeer::RGPD_ENQUETE, $this->rgpd_enquete);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonAgentPeer::DATABASE_NAME);
        $criteria->add(CommonAgentPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonAgent (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLogin($this->getLogin());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setMdp($this->getMdp());
        $copyObj->setCertificat($this->getCertificat());
        $copyObj->setNom($this->getNom());
        $copyObj->setPrenom($this->getPrenom());
        $copyObj->setTentativesMdp($this->getTentativesMdp());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setOldServiceId($this->getOldServiceId());
        $copyObj->setRecevoirMail($this->getRecevoirMail());
        $copyObj->setElu($this->getElu());
        $copyObj->setNomFonction($this->getNomFonction());
        $copyObj->setNumTel($this->getNumTel());
        $copyObj->setNumFax($this->getNumFax());
        $copyObj->setTypeComm($this->getTypeComm());
        $copyObj->setAdrPostale($this->getAdrPostale());
        $copyObj->setCivilite($this->getCivilite());
        $copyObj->setAlerteReponseElectronique($this->getAlerteReponseElectronique());
        $copyObj->setAlerteClotureConsultation($this->getAlerteClotureConsultation());
        $copyObj->setAlerteReceptionMessage($this->getAlerteReceptionMessage());
        $copyObj->setAlertePublicationBoamp($this->getAlertePublicationBoamp());
        $copyObj->setAlerteEchecPublicationBoamp($this->getAlerteEchecPublicationBoamp());
        $copyObj->setAlerteCreationModificationAgent($this->getAlerteCreationModificationAgent());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateModification($this->getDateModification());
        $copyObj->setIdExterne($this->getIdExterne());
        $copyObj->setIdProfilSocleExterne($this->getIdProfilSocleExterne());
        $copyObj->setLieuExecution($this->getLieuExecution());
        $copyObj->setAlerteQuestionEntreprise($this->getAlerteQuestionEntreprise());
        $copyObj->setActif($this->getActif());
        $copyObj->setDeletedAt($this->getDeletedAt());
        $copyObj->setCodesNuts($this->getCodesNuts());
        $copyObj->setNumCertificat($this->getNumCertificat());
        $copyObj->setAlerteValidationConsultation($this->getAlerteValidationConsultation());
        $copyObj->setAlerteChorus($this->getAlerteChorus());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setCodeTheme($this->getCodeTheme());
        $copyObj->setDateConnexion($this->getDateConnexion());
        $copyObj->setTypeHash($this->getTypeHash());
        $copyObj->setAlerteMesConsultations($this->getAlerteMesConsultations());
        $copyObj->setAlerteConsultationsMonEntite($this->getAlerteConsultationsMonEntite());
        $copyObj->setAlerteConsultationsDesEntitesDependantes($this->getAlerteConsultationsDesEntitesDependantes());
        $copyObj->setAlerteConsultationsMesEntitesTransverses($this->getAlerteConsultationsMesEntitesTransverses());
        $copyObj->setTechnique($this->getTechnique());
        $copyObj->setServiceId($this->getServiceId());
        $copyObj->setDateValidationRgpd($this->getDateValidationRgpd());
        $copyObj->setRgpdCommunicationPlace($this->getRgpdCommunicationPlace());
        $copyObj->setRgpdEnquete($this->getRgpdEnquete());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonAgentServiceMetiers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAgentServiceMetier($relObj->copy($deepCopy));
                }
            }

            $relObj = $this->getCommonHabilitationAgent();
            if ($relObj) {
                $copyObj->setCommonHabilitationAgent($relObj->copy($deepCopy));
            }

            foreach ($this->getCommonAgentTechniqueTokens() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAgentTechniqueToken($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonAnnexeFinancieres() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonAnnexeFinanciere($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonConsultationFavoriss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultationFavoris($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonDossierVolumineuxs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonDossierVolumineux($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonEchangeDocs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchangeDoc($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonEchangeDocHistoriques() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonEchangeDocHistorique($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonHabilitationAgentWss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonHabilitationAgentWs($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonInvitePermanentTransverses() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonInvitePermanentTransverse($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonModificationContrats() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonModificationContrat($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOCommissionAgents() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOCommissionAgent($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOOrdreDuJourIntervenants() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOOrdreDuJourIntervenant($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOSeanceAgents() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOSeanceAgent($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTCAOSeanceInvites() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTCAOSeanceInvite($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTVisionRmaAgentOrganismes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTVisionRmaAgentOrganisme($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonAgent Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonAgentPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonAgentPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonOrganisme object.
     *
     * @param   CommonOrganisme $v
     * @return CommonAgent The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonOrganisme(CommonOrganisme $v = null)
    {
        if ($v === null) {
            $this->setOrganisme(NULL);
        } else {
            $this->setOrganisme($v->getAcronyme());
        }

        $this->aCommonOrganisme = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonOrganisme object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonAgent($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonOrganisme object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonOrganisme The associated CommonOrganisme object.
     * @throws PropelException
     */
    public function getCommonOrganisme(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonOrganisme === null && (($this->organisme !== "" && $this->organisme !== null)) && $doQuery) {
            $this->aCommonOrganisme = CommonOrganismeQuery::create()
                ->filterByCommonAgent($this) // here
                ->findOne($con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonOrganisme->addCommonAgents($this);
             */
        }

        return $this->aCommonOrganisme;
    }

    /**
     * Declares an association between this object and a CommonService object.
     *
     * @param   CommonService $v
     * @return CommonAgent The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonService(CommonService $v = null)
    {
        if ($v === null) {
            $this->setServiceId(NULL);
        } else {
            $this->setServiceId($v->getId());
        }

        $this->aCommonService = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonService object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonAgent($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonService object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonService The associated CommonService object.
     * @throws PropelException
     */
    public function getCommonService(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonService === null && (($this->service_id !== "" && $this->service_id !== null)) && $doQuery) {
            $this->aCommonService = CommonServiceQuery::create()->findPk($this->service_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonService->addCommonAgents($this);
             */
        }

        return $this->aCommonService;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonAgentServiceMetier' == $relationName) {
            $this->initCommonAgentServiceMetiers();
        }
        if ('CommonAgentTechniqueToken' == $relationName) {
            $this->initCommonAgentTechniqueTokens();
        }
        if ('CommonAnnexeFinanciere' == $relationName) {
            $this->initCommonAnnexeFinancieres();
        }
        if ('CommonConsultationFavoris' == $relationName) {
            $this->initCommonConsultationFavoriss();
        }
        if ('CommonDossierVolumineux' == $relationName) {
            $this->initCommonDossierVolumineuxs();
        }
        if ('CommonEchangeDoc' == $relationName) {
            $this->initCommonEchangeDocs();
        }
        if ('CommonEchangeDocHistorique' == $relationName) {
            $this->initCommonEchangeDocHistoriques();
        }
        if ('CommonHabilitationAgentWs' == $relationName) {
            $this->initCommonHabilitationAgentWss();
        }
        if ('CommonInvitePermanentTransverse' == $relationName) {
            $this->initCommonInvitePermanentTransverses();
        }
        if ('CommonModificationContrat' == $relationName) {
            $this->initCommonModificationContrats();
        }
        if ('CommonTCAOCommissionAgent' == $relationName) {
            $this->initCommonTCAOCommissionAgents();
        }
        if ('CommonTCAOOrdreDuJourIntervenant' == $relationName) {
            $this->initCommonTCAOOrdreDuJourIntervenants();
        }
        if ('CommonTCAOSeanceAgent' == $relationName) {
            $this->initCommonTCAOSeanceAgents();
        }
        if ('CommonTCAOSeanceInvite' == $relationName) {
            $this->initCommonTCAOSeanceInvites();
        }
        if ('CommonTVisionRmaAgentOrganisme' == $relationName) {
            $this->initCommonTVisionRmaAgentOrganismes();
        }
    }

    /**
     * Clears out the collCommonAgentServiceMetiers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonAgentServiceMetiers()
     */
    public function clearCommonAgentServiceMetiers()
    {
        $this->collCommonAgentServiceMetiers = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAgentServiceMetiersPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAgentServiceMetiers collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAgentServiceMetiers($v = true)
    {
        $this->collCommonAgentServiceMetiersPartial = $v;
    }

    /**
     * Initializes the collCommonAgentServiceMetiers collection.
     *
     * By default this just sets the collCommonAgentServiceMetiers collection to an empty array (like clearcollCommonAgentServiceMetiers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAgentServiceMetiers($overrideExisting = true)
    {
        if (null !== $this->collCommonAgentServiceMetiers && !$overrideExisting) {
            return;
        }
        $this->collCommonAgentServiceMetiers = new PropelObjectCollection();
        $this->collCommonAgentServiceMetiers->setModel('CommonAgentServiceMetier');
    }

    /**
     * Gets an array of CommonAgentServiceMetier objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAgentServiceMetier[] List of CommonAgentServiceMetier objects
     * @throws PropelException
     */
    public function getCommonAgentServiceMetiers($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAgentServiceMetiersPartial && !$this->isNew();
        if (null === $this->collCommonAgentServiceMetiers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAgentServiceMetiers) {
                // return empty collection
                $this->initCommonAgentServiceMetiers();
            } else {
                $collCommonAgentServiceMetiers = CommonAgentServiceMetierQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAgentServiceMetiersPartial && count($collCommonAgentServiceMetiers)) {
                      $this->initCommonAgentServiceMetiers(false);

                      foreach ($collCommonAgentServiceMetiers as $obj) {
                        if (false == $this->collCommonAgentServiceMetiers->contains($obj)) {
                          $this->collCommonAgentServiceMetiers->append($obj);
                        }
                      }

                      $this->collCommonAgentServiceMetiersPartial = true;
                    }

                    $collCommonAgentServiceMetiers->getInternalIterator()->rewind();

                    return $collCommonAgentServiceMetiers;
                }

                if ($partial && $this->collCommonAgentServiceMetiers) {
                    foreach ($this->collCommonAgentServiceMetiers as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAgentServiceMetiers[] = $obj;
                        }
                    }
                }

                $this->collCommonAgentServiceMetiers = $collCommonAgentServiceMetiers;
                $this->collCommonAgentServiceMetiersPartial = false;
            }
        }

        return $this->collCommonAgentServiceMetiers;
    }

    /**
     * Sets a collection of CommonAgentServiceMetier objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAgentServiceMetiers A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonAgentServiceMetiers(PropelCollection $commonAgentServiceMetiers, PropelPDO $con = null)
    {
        $commonAgentServiceMetiersToDelete = $this->getCommonAgentServiceMetiers(new Criteria(), $con)->diff($commonAgentServiceMetiers);


        //since at least one column in the foreign key is at the same time a PK
        //we can not just set a PK to NULL in the lines below. We have to store
        //a backup of all values, so we are able to manipulate these items based on the onDelete value later.
        $this->commonAgentServiceMetiersScheduledForDeletion = clone $commonAgentServiceMetiersToDelete;

        foreach ($commonAgentServiceMetiersToDelete as $commonAgentServiceMetierRemoved) {
            $commonAgentServiceMetierRemoved->setCommonAgent(null);
        }

        $this->collCommonAgentServiceMetiers = null;
        foreach ($commonAgentServiceMetiers as $commonAgentServiceMetier) {
            $this->addCommonAgentServiceMetier($commonAgentServiceMetier);
        }

        $this->collCommonAgentServiceMetiers = $commonAgentServiceMetiers;
        $this->collCommonAgentServiceMetiersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAgentServiceMetier objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAgentServiceMetier objects.
     * @throws PropelException
     */
    public function countCommonAgentServiceMetiers(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAgentServiceMetiersPartial && !$this->isNew();
        if (null === $this->collCommonAgentServiceMetiers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAgentServiceMetiers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAgentServiceMetiers());
            }
            $query = CommonAgentServiceMetierQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonAgentServiceMetiers);
    }

    /**
     * Method called to associate a CommonAgentServiceMetier object to this object
     * through the CommonAgentServiceMetier foreign key attribute.
     *
     * @param   CommonAgentServiceMetier $l CommonAgentServiceMetier
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonAgentServiceMetier(CommonAgentServiceMetier $l)
    {
        if ($this->collCommonAgentServiceMetiers === null) {
            $this->initCommonAgentServiceMetiers();
            $this->collCommonAgentServiceMetiersPartial = true;
        }
        if (!in_array($l, $this->collCommonAgentServiceMetiers->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAgentServiceMetier($l);
        }

        return $this;
    }

    /**
     * @param	CommonAgentServiceMetier $commonAgentServiceMetier The commonAgentServiceMetier object to add.
     */
    protected function doAddCommonAgentServiceMetier($commonAgentServiceMetier)
    {
        $this->collCommonAgentServiceMetiers[]= $commonAgentServiceMetier;
        $commonAgentServiceMetier->setCommonAgent($this);
    }

    /**
     * @param	CommonAgentServiceMetier $commonAgentServiceMetier The commonAgentServiceMetier object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonAgentServiceMetier($commonAgentServiceMetier)
    {
        if ($this->getCommonAgentServiceMetiers()->contains($commonAgentServiceMetier)) {
            $this->collCommonAgentServiceMetiers->remove($this->collCommonAgentServiceMetiers->search($commonAgentServiceMetier));
            if (null === $this->commonAgentServiceMetiersScheduledForDeletion) {
                $this->commonAgentServiceMetiersScheduledForDeletion = clone $this->collCommonAgentServiceMetiers;
                $this->commonAgentServiceMetiersScheduledForDeletion->clear();
            }
            $this->commonAgentServiceMetiersScheduledForDeletion[]= clone $commonAgentServiceMetier;
            $commonAgentServiceMetier->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonAgentServiceMetiers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAgentServiceMetier[] List of CommonAgentServiceMetier objects
     */
    public function getCommonAgentServiceMetiersJoinCommonServiceMertier($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAgentServiceMetierQuery::create(null, $criteria);
        $query->joinWith('CommonServiceMertier', $join_behavior);

        return $this->getCommonAgentServiceMetiers($query, $con);
    }

    /**
     * Gets a single CommonHabilitationAgent object, which is related to this object by a one-to-one relationship.
     *
     * @param PropelPDO $con optional connection object
     * @return CommonHabilitationAgent
     * @throws PropelException
     */
    public function getCommonHabilitationAgent(PropelPDO $con = null)
    {

        if ($this->singleCommonHabilitationAgent === null && !$this->isNew()) {
            $this->singleCommonHabilitationAgent = CommonHabilitationAgentQuery::create()->findPk($this->getPrimaryKey(), $con);
        }

        return $this->singleCommonHabilitationAgent;
    }

    /**
     * Sets a single CommonHabilitationAgent object as related to this object by a one-to-one relationship.
     *
     * @param   CommonHabilitationAgent $v CommonHabilitationAgent
     * @return CommonAgent The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonHabilitationAgent(CommonHabilitationAgent $v = null)
    {
        $this->singleCommonHabilitationAgent = $v;

        // Make sure that that the passed-in CommonHabilitationAgent isn't already associated with this object
        if ($v !== null && $v->getCommonAgent(null, false) === null) {
            $v->setCommonAgent($this);
        }

        return $this;
    }

    /**
     * Clears out the collCommonAgentTechniqueTokens collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonAgentTechniqueTokens()
     */
    public function clearCommonAgentTechniqueTokens()
    {
        $this->collCommonAgentTechniqueTokens = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAgentTechniqueTokensPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAgentTechniqueTokens collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAgentTechniqueTokens($v = true)
    {
        $this->collCommonAgentTechniqueTokensPartial = $v;
    }

    /**
     * Initializes the collCommonAgentTechniqueTokens collection.
     *
     * By default this just sets the collCommonAgentTechniqueTokens collection to an empty array (like clearcollCommonAgentTechniqueTokens());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAgentTechniqueTokens($overrideExisting = true)
    {
        if (null !== $this->collCommonAgentTechniqueTokens && !$overrideExisting) {
            return;
        }
        $this->collCommonAgentTechniqueTokens = new PropelObjectCollection();
        $this->collCommonAgentTechniqueTokens->setModel('CommonAgentTechniqueToken');
    }

    /**
     * Gets an array of CommonAgentTechniqueToken objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAgentTechniqueToken[] List of CommonAgentTechniqueToken objects
     * @throws PropelException
     */
    public function getCommonAgentTechniqueTokens($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAgentTechniqueTokensPartial && !$this->isNew();
        if (null === $this->collCommonAgentTechniqueTokens || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAgentTechniqueTokens) {
                // return empty collection
                $this->initCommonAgentTechniqueTokens();
            } else {
                $collCommonAgentTechniqueTokens = CommonAgentTechniqueTokenQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAgentTechniqueTokensPartial && count($collCommonAgentTechniqueTokens)) {
                      $this->initCommonAgentTechniqueTokens(false);

                      foreach ($collCommonAgentTechniqueTokens as $obj) {
                        if (false == $this->collCommonAgentTechniqueTokens->contains($obj)) {
                          $this->collCommonAgentTechniqueTokens->append($obj);
                        }
                      }

                      $this->collCommonAgentTechniqueTokensPartial = true;
                    }

                    $collCommonAgentTechniqueTokens->getInternalIterator()->rewind();

                    return $collCommonAgentTechniqueTokens;
                }

                if ($partial && $this->collCommonAgentTechniqueTokens) {
                    foreach ($this->collCommonAgentTechniqueTokens as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAgentTechniqueTokens[] = $obj;
                        }
                    }
                }

                $this->collCommonAgentTechniqueTokens = $collCommonAgentTechniqueTokens;
                $this->collCommonAgentTechniqueTokensPartial = false;
            }
        }

        return $this->collCommonAgentTechniqueTokens;
    }

    /**
     * Sets a collection of CommonAgentTechniqueToken objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAgentTechniqueTokens A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonAgentTechniqueTokens(PropelCollection $commonAgentTechniqueTokens, PropelPDO $con = null)
    {
        $commonAgentTechniqueTokensToDelete = $this->getCommonAgentTechniqueTokens(new Criteria(), $con)->diff($commonAgentTechniqueTokens);


        $this->commonAgentTechniqueTokensScheduledForDeletion = $commonAgentTechniqueTokensToDelete;

        foreach ($commonAgentTechniqueTokensToDelete as $commonAgentTechniqueTokenRemoved) {
            $commonAgentTechniqueTokenRemoved->setCommonAgent(null);
        }

        $this->collCommonAgentTechniqueTokens = null;
        foreach ($commonAgentTechniqueTokens as $commonAgentTechniqueToken) {
            $this->addCommonAgentTechniqueToken($commonAgentTechniqueToken);
        }

        $this->collCommonAgentTechniqueTokens = $commonAgentTechniqueTokens;
        $this->collCommonAgentTechniqueTokensPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAgentTechniqueToken objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAgentTechniqueToken objects.
     * @throws PropelException
     */
    public function countCommonAgentTechniqueTokens(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAgentTechniqueTokensPartial && !$this->isNew();
        if (null === $this->collCommonAgentTechniqueTokens || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAgentTechniqueTokens) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAgentTechniqueTokens());
            }
            $query = CommonAgentTechniqueTokenQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonAgentTechniqueTokens);
    }

    /**
     * Method called to associate a CommonAgentTechniqueToken object to this object
     * through the CommonAgentTechniqueToken foreign key attribute.
     *
     * @param   CommonAgentTechniqueToken $l CommonAgentTechniqueToken
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonAgentTechniqueToken(CommonAgentTechniqueToken $l)
    {
        if ($this->collCommonAgentTechniqueTokens === null) {
            $this->initCommonAgentTechniqueTokens();
            $this->collCommonAgentTechniqueTokensPartial = true;
        }
        if (!in_array($l, $this->collCommonAgentTechniqueTokens->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAgentTechniqueToken($l);
        }

        return $this;
    }

    /**
     * @param	CommonAgentTechniqueToken $commonAgentTechniqueToken The commonAgentTechniqueToken object to add.
     */
    protected function doAddCommonAgentTechniqueToken($commonAgentTechniqueToken)
    {
        $this->collCommonAgentTechniqueTokens[]= $commonAgentTechniqueToken;
        $commonAgentTechniqueToken->setCommonAgent($this);
    }

    /**
     * @param	CommonAgentTechniqueToken $commonAgentTechniqueToken The commonAgentTechniqueToken object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonAgentTechniqueToken($commonAgentTechniqueToken)
    {
        if ($this->getCommonAgentTechniqueTokens()->contains($commonAgentTechniqueToken)) {
            $this->collCommonAgentTechniqueTokens->remove($this->collCommonAgentTechniqueTokens->search($commonAgentTechniqueToken));
            if (null === $this->commonAgentTechniqueTokensScheduledForDeletion) {
                $this->commonAgentTechniqueTokensScheduledForDeletion = clone $this->collCommonAgentTechniqueTokens;
                $this->commonAgentTechniqueTokensScheduledForDeletion->clear();
            }
            $this->commonAgentTechniqueTokensScheduledForDeletion[]= clone $commonAgentTechniqueToken;
            $commonAgentTechniqueToken->setCommonAgent(null);
        }

        return $this;
    }

    /**
     * Clears out the collCommonAnnexeFinancieres collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonAnnexeFinancieres()
     */
    public function clearCommonAnnexeFinancieres()
    {
        $this->collCommonAnnexeFinancieres = null; // important to set this to null since that means it is uninitialized
        $this->collCommonAnnexeFinancieresPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonAnnexeFinancieres collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonAnnexeFinancieres($v = true)
    {
        $this->collCommonAnnexeFinancieresPartial = $v;
    }

    /**
     * Initializes the collCommonAnnexeFinancieres collection.
     *
     * By default this just sets the collCommonAnnexeFinancieres collection to an empty array (like clearcollCommonAnnexeFinancieres());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonAnnexeFinancieres($overrideExisting = true)
    {
        if (null !== $this->collCommonAnnexeFinancieres && !$overrideExisting) {
            return;
        }
        $this->collCommonAnnexeFinancieres = new PropelObjectCollection();
        $this->collCommonAnnexeFinancieres->setModel('CommonAnnexeFinanciere');
    }

    /**
     * Gets an array of CommonAnnexeFinanciere objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonAnnexeFinanciere[] List of CommonAnnexeFinanciere objects
     * @throws PropelException
     */
    public function getCommonAnnexeFinancieres($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonAnnexeFinancieresPartial && !$this->isNew();
        if (null === $this->collCommonAnnexeFinancieres || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonAnnexeFinancieres) {
                // return empty collection
                $this->initCommonAnnexeFinancieres();
            } else {
                $collCommonAnnexeFinancieres = CommonAnnexeFinanciereQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonAnnexeFinancieresPartial && count($collCommonAnnexeFinancieres)) {
                      $this->initCommonAnnexeFinancieres(false);

                      foreach ($collCommonAnnexeFinancieres as $obj) {
                        if (false == $this->collCommonAnnexeFinancieres->contains($obj)) {
                          $this->collCommonAnnexeFinancieres->append($obj);
                        }
                      }

                      $this->collCommonAnnexeFinancieresPartial = true;
                    }

                    $collCommonAnnexeFinancieres->getInternalIterator()->rewind();

                    return $collCommonAnnexeFinancieres;
                }

                if ($partial && $this->collCommonAnnexeFinancieres) {
                    foreach ($this->collCommonAnnexeFinancieres as $obj) {
                        if ($obj->isNew()) {
                            $collCommonAnnexeFinancieres[] = $obj;
                        }
                    }
                }

                $this->collCommonAnnexeFinancieres = $collCommonAnnexeFinancieres;
                $this->collCommonAnnexeFinancieresPartial = false;
            }
        }

        return $this->collCommonAnnexeFinancieres;
    }

    /**
     * Sets a collection of CommonAnnexeFinanciere objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonAnnexeFinancieres A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonAnnexeFinancieres(PropelCollection $commonAnnexeFinancieres, PropelPDO $con = null)
    {
        $commonAnnexeFinancieresToDelete = $this->getCommonAnnexeFinancieres(new Criteria(), $con)->diff($commonAnnexeFinancieres);


        $this->commonAnnexeFinancieresScheduledForDeletion = $commonAnnexeFinancieresToDelete;

        foreach ($commonAnnexeFinancieresToDelete as $commonAnnexeFinanciereRemoved) {
            $commonAnnexeFinanciereRemoved->setCommonAgent(null);
        }

        $this->collCommonAnnexeFinancieres = null;
        foreach ($commonAnnexeFinancieres as $commonAnnexeFinanciere) {
            $this->addCommonAnnexeFinanciere($commonAnnexeFinanciere);
        }

        $this->collCommonAnnexeFinancieres = $commonAnnexeFinancieres;
        $this->collCommonAnnexeFinancieresPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonAnnexeFinanciere objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonAnnexeFinanciere objects.
     * @throws PropelException
     */
    public function countCommonAnnexeFinancieres(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonAnnexeFinancieresPartial && !$this->isNew();
        if (null === $this->collCommonAnnexeFinancieres || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonAnnexeFinancieres) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonAnnexeFinancieres());
            }
            $query = CommonAnnexeFinanciereQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonAnnexeFinancieres);
    }

    /**
     * Method called to associate a CommonAnnexeFinanciere object to this object
     * through the CommonAnnexeFinanciere foreign key attribute.
     *
     * @param   CommonAnnexeFinanciere $l CommonAnnexeFinanciere
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonAnnexeFinanciere(CommonAnnexeFinanciere $l)
    {
        if ($this->collCommonAnnexeFinancieres === null) {
            $this->initCommonAnnexeFinancieres();
            $this->collCommonAnnexeFinancieresPartial = true;
        }
        if (!in_array($l, $this->collCommonAnnexeFinancieres->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonAnnexeFinanciere($l);
        }

        return $this;
    }

    /**
     * @param	CommonAnnexeFinanciere $commonAnnexeFinanciere The commonAnnexeFinanciere object to add.
     */
    protected function doAddCommonAnnexeFinanciere($commonAnnexeFinanciere)
    {
        $this->collCommonAnnexeFinancieres[]= $commonAnnexeFinanciere;
        $commonAnnexeFinanciere->setCommonAgent($this);
    }

    /**
     * @param	CommonAnnexeFinanciere $commonAnnexeFinanciere The commonAnnexeFinanciere object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonAnnexeFinanciere($commonAnnexeFinanciere)
    {
        if ($this->getCommonAnnexeFinancieres()->contains($commonAnnexeFinanciere)) {
            $this->collCommonAnnexeFinancieres->remove($this->collCommonAnnexeFinancieres->search($commonAnnexeFinanciere));
            if (null === $this->commonAnnexeFinancieresScheduledForDeletion) {
                $this->commonAnnexeFinancieresScheduledForDeletion = clone $this->collCommonAnnexeFinancieres;
                $this->commonAnnexeFinancieresScheduledForDeletion->clear();
            }
            $this->commonAnnexeFinancieresScheduledForDeletion[]= $commonAnnexeFinanciere;
            $commonAnnexeFinanciere->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonAnnexeFinancieres from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAnnexeFinanciere[] List of CommonAnnexeFinanciere objects
     */
    public function getCommonAnnexeFinancieresJoinCommonBlobOrganismeFile($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAnnexeFinanciereQuery::create(null, $criteria);
        $query->joinWith('CommonBlobOrganismeFile', $join_behavior);

        return $this->getCommonAnnexeFinancieres($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonAnnexeFinancieres from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonAnnexeFinanciere[] List of CommonAnnexeFinanciere objects
     */
    public function getCommonAnnexeFinancieresJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonAnnexeFinanciereQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonAnnexeFinancieres($query, $con);
    }

    /**
     * Clears out the collCommonConsultationFavoriss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonConsultationFavoriss()
     */
    public function clearCommonConsultationFavoriss()
    {
        $this->collCommonConsultationFavoriss = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationFavorissPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultationFavoriss collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultationFavoriss($v = true)
    {
        $this->collCommonConsultationFavorissPartial = $v;
    }

    /**
     * Initializes the collCommonConsultationFavoriss collection.
     *
     * By default this just sets the collCommonConsultationFavoriss collection to an empty array (like clearcollCommonConsultationFavoriss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultationFavoriss($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultationFavoriss && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultationFavoriss = new PropelObjectCollection();
        $this->collCommonConsultationFavoriss->setModel('CommonConsultationFavoris');
    }

    /**
     * Gets an array of CommonConsultationFavoris objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultationFavoris[] List of CommonConsultationFavoris objects
     * @throws PropelException
     */
    public function getCommonConsultationFavoriss($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationFavorissPartial && !$this->isNew();
        if (null === $this->collCommonConsultationFavoriss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationFavoriss) {
                // return empty collection
                $this->initCommonConsultationFavoriss();
            } else {
                $collCommonConsultationFavoriss = CommonConsultationFavorisQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationFavorissPartial && count($collCommonConsultationFavoriss)) {
                      $this->initCommonConsultationFavoriss(false);

                      foreach ($collCommonConsultationFavoriss as $obj) {
                        if (false == $this->collCommonConsultationFavoriss->contains($obj)) {
                          $this->collCommonConsultationFavoriss->append($obj);
                        }
                      }

                      $this->collCommonConsultationFavorissPartial = true;
                    }

                    $collCommonConsultationFavoriss->getInternalIterator()->rewind();

                    return $collCommonConsultationFavoriss;
                }

                if ($partial && $this->collCommonConsultationFavoriss) {
                    foreach ($this->collCommonConsultationFavoriss as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultationFavoriss[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultationFavoriss = $collCommonConsultationFavoriss;
                $this->collCommonConsultationFavorissPartial = false;
            }
        }

        return $this->collCommonConsultationFavoriss;
    }

    /**
     * Sets a collection of CommonConsultationFavoris objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultationFavoriss A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonConsultationFavoriss(PropelCollection $commonConsultationFavoriss, PropelPDO $con = null)
    {
        $commonConsultationFavorissToDelete = $this->getCommonConsultationFavoriss(new Criteria(), $con)->diff($commonConsultationFavoriss);


        $this->commonConsultationFavorissScheduledForDeletion = $commonConsultationFavorissToDelete;

        foreach ($commonConsultationFavorissToDelete as $commonConsultationFavorisRemoved) {
            $commonConsultationFavorisRemoved->setCommonAgent(null);
        }

        $this->collCommonConsultationFavoriss = null;
        foreach ($commonConsultationFavoriss as $commonConsultationFavoris) {
            $this->addCommonConsultationFavoris($commonConsultationFavoris);
        }

        $this->collCommonConsultationFavoriss = $commonConsultationFavoriss;
        $this->collCommonConsultationFavorissPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultationFavoris objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultationFavoris objects.
     * @throws PropelException
     */
    public function countCommonConsultationFavoriss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationFavorissPartial && !$this->isNew();
        if (null === $this->collCommonConsultationFavoriss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationFavoriss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultationFavoriss());
            }
            $query = CommonConsultationFavorisQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonConsultationFavoriss);
    }

    /**
     * Method called to associate a CommonConsultationFavoris object to this object
     * through the CommonConsultationFavoris foreign key attribute.
     *
     * @param   CommonConsultationFavoris $l CommonConsultationFavoris
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonConsultationFavoris(CommonConsultationFavoris $l)
    {
        if ($this->collCommonConsultationFavoriss === null) {
            $this->initCommonConsultationFavoriss();
            $this->collCommonConsultationFavorissPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultationFavoriss->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultationFavoris($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultationFavoris $commonConsultationFavoris The commonConsultationFavoris object to add.
     */
    protected function doAddCommonConsultationFavoris($commonConsultationFavoris)
    {
        $this->collCommonConsultationFavoriss[]= $commonConsultationFavoris;
        $commonConsultationFavoris->setCommonAgent($this);
    }

    /**
     * @param	CommonConsultationFavoris $commonConsultationFavoris The commonConsultationFavoris object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonConsultationFavoris($commonConsultationFavoris)
    {
        if ($this->getCommonConsultationFavoriss()->contains($commonConsultationFavoris)) {
            $this->collCommonConsultationFavoriss->remove($this->collCommonConsultationFavoriss->search($commonConsultationFavoris));
            if (null === $this->commonConsultationFavorissScheduledForDeletion) {
                $this->commonConsultationFavorissScheduledForDeletion = clone $this->collCommonConsultationFavoriss;
                $this->commonConsultationFavorissScheduledForDeletion->clear();
            }
            $this->commonConsultationFavorissScheduledForDeletion[]= clone $commonConsultationFavoris;
            $commonConsultationFavoris->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonConsultationFavoriss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultationFavoris[] List of CommonConsultationFavoris objects
     */
    public function getCommonConsultationFavorissJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationFavorisQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonConsultationFavoriss($query, $con);
    }

    /**
     * Clears out the collCommonDossierVolumineuxs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonDossierVolumineuxs()
     */
    public function clearCommonDossierVolumineuxs()
    {
        $this->collCommonDossierVolumineuxs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonDossierVolumineuxsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonDossierVolumineuxs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonDossierVolumineuxs($v = true)
    {
        $this->collCommonDossierVolumineuxsPartial = $v;
    }

    /**
     * Initializes the collCommonDossierVolumineuxs collection.
     *
     * By default this just sets the collCommonDossierVolumineuxs collection to an empty array (like clearcollCommonDossierVolumineuxs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonDossierVolumineuxs($overrideExisting = true)
    {
        if (null !== $this->collCommonDossierVolumineuxs && !$overrideExisting) {
            return;
        }
        $this->collCommonDossierVolumineuxs = new PropelObjectCollection();
        $this->collCommonDossierVolumineuxs->setModel('CommonDossierVolumineux');
    }

    /**
     * Gets an array of CommonDossierVolumineux objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     * @throws PropelException
     */
    public function getCommonDossierVolumineuxs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonDossierVolumineuxsPartial && !$this->isNew();
        if (null === $this->collCommonDossierVolumineuxs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonDossierVolumineuxs) {
                // return empty collection
                $this->initCommonDossierVolumineuxs();
            } else {
                $collCommonDossierVolumineuxs = CommonDossierVolumineuxQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonDossierVolumineuxsPartial && count($collCommonDossierVolumineuxs)) {
                      $this->initCommonDossierVolumineuxs(false);

                      foreach ($collCommonDossierVolumineuxs as $obj) {
                        if (false == $this->collCommonDossierVolumineuxs->contains($obj)) {
                          $this->collCommonDossierVolumineuxs->append($obj);
                        }
                      }

                      $this->collCommonDossierVolumineuxsPartial = true;
                    }

                    $collCommonDossierVolumineuxs->getInternalIterator()->rewind();

                    return $collCommonDossierVolumineuxs;
                }

                if ($partial && $this->collCommonDossierVolumineuxs) {
                    foreach ($this->collCommonDossierVolumineuxs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonDossierVolumineuxs[] = $obj;
                        }
                    }
                }

                $this->collCommonDossierVolumineuxs = $collCommonDossierVolumineuxs;
                $this->collCommonDossierVolumineuxsPartial = false;
            }
        }

        return $this->collCommonDossierVolumineuxs;
    }

    /**
     * Sets a collection of CommonDossierVolumineux objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonDossierVolumineuxs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonDossierVolumineuxs(PropelCollection $commonDossierVolumineuxs, PropelPDO $con = null)
    {
        $commonDossierVolumineuxsToDelete = $this->getCommonDossierVolumineuxs(new Criteria(), $con)->diff($commonDossierVolumineuxs);


        $this->commonDossierVolumineuxsScheduledForDeletion = $commonDossierVolumineuxsToDelete;

        foreach ($commonDossierVolumineuxsToDelete as $commonDossierVolumineuxRemoved) {
            $commonDossierVolumineuxRemoved->setCommonAgent(null);
        }

        $this->collCommonDossierVolumineuxs = null;
        foreach ($commonDossierVolumineuxs as $commonDossierVolumineux) {
            $this->addCommonDossierVolumineux($commonDossierVolumineux);
        }

        $this->collCommonDossierVolumineuxs = $commonDossierVolumineuxs;
        $this->collCommonDossierVolumineuxsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonDossierVolumineux objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonDossierVolumineux objects.
     * @throws PropelException
     */
    public function countCommonDossierVolumineuxs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonDossierVolumineuxsPartial && !$this->isNew();
        if (null === $this->collCommonDossierVolumineuxs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonDossierVolumineuxs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonDossierVolumineuxs());
            }
            $query = CommonDossierVolumineuxQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonDossierVolumineuxs);
    }

    /**
     * Method called to associate a CommonDossierVolumineux object to this object
     * through the CommonDossierVolumineux foreign key attribute.
     *
     * @param   CommonDossierVolumineux $l CommonDossierVolumineux
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonDossierVolumineux(CommonDossierVolumineux $l)
    {
        if ($this->collCommonDossierVolumineuxs === null) {
            $this->initCommonDossierVolumineuxs();
            $this->collCommonDossierVolumineuxsPartial = true;
        }
        if (!in_array($l, $this->collCommonDossierVolumineuxs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonDossierVolumineux($l);
        }

        return $this;
    }

    /**
     * @param	CommonDossierVolumineux $commonDossierVolumineux The commonDossierVolumineux object to add.
     */
    protected function doAddCommonDossierVolumineux($commonDossierVolumineux)
    {
        $this->collCommonDossierVolumineuxs[]= $commonDossierVolumineux;
        $commonDossierVolumineux->setCommonAgent($this);
    }

    /**
     * @param	CommonDossierVolumineux $commonDossierVolumineux The commonDossierVolumineux object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonDossierVolumineux($commonDossierVolumineux)
    {
        if ($this->getCommonDossierVolumineuxs()->contains($commonDossierVolumineux)) {
            $this->collCommonDossierVolumineuxs->remove($this->collCommonDossierVolumineuxs->search($commonDossierVolumineux));
            if (null === $this->commonDossierVolumineuxsScheduledForDeletion) {
                $this->commonDossierVolumineuxsScheduledForDeletion = clone $this->collCommonDossierVolumineuxs;
                $this->commonDossierVolumineuxsScheduledForDeletion->clear();
            }
            $this->commonDossierVolumineuxsScheduledForDeletion[]= $commonDossierVolumineux;
            $commonDossierVolumineux->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonDossierVolumineuxs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     */
    public function getCommonDossierVolumineuxsJoinCommonInscrit($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonDossierVolumineuxQuery::create(null, $criteria);
        $query->joinWith('CommonInscrit', $join_behavior);

        return $this->getCommonDossierVolumineuxs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonDossierVolumineuxs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     */
    public function getCommonDossierVolumineuxsJoinCommonBlobFileRelatedByIdBlobDescripteur($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonDossierVolumineuxQuery::create(null, $criteria);
        $query->joinWith('CommonBlobFileRelatedByIdBlobDescripteur', $join_behavior);

        return $this->getCommonDossierVolumineuxs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonDossierVolumineuxs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonDossierVolumineux[] List of CommonDossierVolumineux objects
     */
    public function getCommonDossierVolumineuxsJoinCommonBlobFileRelatedByIdBlobLogfile($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonDossierVolumineuxQuery::create(null, $criteria);
        $query->joinWith('CommonBlobFileRelatedByIdBlobLogfile', $join_behavior);

        return $this->getCommonDossierVolumineuxs($query, $con);
    }

    /**
     * Clears out the collCommonEchangeDocs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonEchangeDocs()
     */
    public function clearCommonEchangeDocs()
    {
        $this->collCommonEchangeDocs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangeDocsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchangeDocs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchangeDocs($v = true)
    {
        $this->collCommonEchangeDocsPartial = $v;
    }

    /**
     * Initializes the collCommonEchangeDocs collection.
     *
     * By default this just sets the collCommonEchangeDocs collection to an empty array (like clearcollCommonEchangeDocs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchangeDocs($overrideExisting = true)
    {
        if (null !== $this->collCommonEchangeDocs && !$overrideExisting) {
            return;
        }
        $this->collCommonEchangeDocs = new PropelObjectCollection();
        $this->collCommonEchangeDocs->setModel('CommonEchangeDoc');
    }

    /**
     * Gets an array of CommonEchangeDoc objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     * @throws PropelException
     */
    public function getCommonEchangeDocs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocsPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocs) {
                // return empty collection
                $this->initCommonEchangeDocs();
            } else {
                $collCommonEchangeDocs = CommonEchangeDocQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangeDocsPartial && count($collCommonEchangeDocs)) {
                      $this->initCommonEchangeDocs(false);

                      foreach ($collCommonEchangeDocs as $obj) {
                        if (false == $this->collCommonEchangeDocs->contains($obj)) {
                          $this->collCommonEchangeDocs->append($obj);
                        }
                      }

                      $this->collCommonEchangeDocsPartial = true;
                    }

                    $collCommonEchangeDocs->getInternalIterator()->rewind();

                    return $collCommonEchangeDocs;
                }

                if ($partial && $this->collCommonEchangeDocs) {
                    foreach ($this->collCommonEchangeDocs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchangeDocs[] = $obj;
                        }
                    }
                }

                $this->collCommonEchangeDocs = $collCommonEchangeDocs;
                $this->collCommonEchangeDocsPartial = false;
            }
        }

        return $this->collCommonEchangeDocs;
    }

    /**
     * Sets a collection of CommonEchangeDoc objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchangeDocs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonEchangeDocs(PropelCollection $commonEchangeDocs, PropelPDO $con = null)
    {
        $commonEchangeDocsToDelete = $this->getCommonEchangeDocs(new Criteria(), $con)->diff($commonEchangeDocs);


        $this->commonEchangeDocsScheduledForDeletion = $commonEchangeDocsToDelete;

        foreach ($commonEchangeDocsToDelete as $commonEchangeDocRemoved) {
            $commonEchangeDocRemoved->setCommonAgent(null);
        }

        $this->collCommonEchangeDocs = null;
        foreach ($commonEchangeDocs as $commonEchangeDoc) {
            $this->addCommonEchangeDoc($commonEchangeDoc);
        }

        $this->collCommonEchangeDocs = $commonEchangeDocs;
        $this->collCommonEchangeDocsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchangeDoc objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchangeDoc objects.
     * @throws PropelException
     */
    public function countCommonEchangeDocs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocsPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchangeDocs());
            }
            $query = CommonEchangeDocQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonEchangeDocs);
    }

    /**
     * Method called to associate a CommonEchangeDoc object to this object
     * through the CommonEchangeDoc foreign key attribute.
     *
     * @param   CommonEchangeDoc $l CommonEchangeDoc
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonEchangeDoc(CommonEchangeDoc $l)
    {
        if ($this->collCommonEchangeDocs === null) {
            $this->initCommonEchangeDocs();
            $this->collCommonEchangeDocsPartial = true;
        }
        if (!in_array($l, $this->collCommonEchangeDocs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchangeDoc($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchangeDoc $commonEchangeDoc The commonEchangeDoc object to add.
     */
    protected function doAddCommonEchangeDoc($commonEchangeDoc)
    {
        $this->collCommonEchangeDocs[]= $commonEchangeDoc;
        $commonEchangeDoc->setCommonAgent($this);
    }

    /**
     * @param	CommonEchangeDoc $commonEchangeDoc The commonEchangeDoc object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonEchangeDoc($commonEchangeDoc)
    {
        if ($this->getCommonEchangeDocs()->contains($commonEchangeDoc)) {
            $this->collCommonEchangeDocs->remove($this->collCommonEchangeDocs->search($commonEchangeDoc));
            if (null === $this->commonEchangeDocsScheduledForDeletion) {
                $this->commonEchangeDocsScheduledForDeletion = clone $this->collCommonEchangeDocs;
                $this->commonEchangeDocsScheduledForDeletion->clear();
            }
            $this->commonEchangeDocsScheduledForDeletion[]= $commonEchangeDoc;
            $commonEchangeDoc->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonEchangeDocs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     */
    public function getCommonEchangeDocsJoinCommonTContratTitulaire($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocQuery::create(null, $criteria);
        $query->joinWith('CommonTContratTitulaire', $join_behavior);

        return $this->getCommonEchangeDocs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonEchangeDocs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     */
    public function getCommonEchangeDocsJoinCommonEchangeDocApplicationClient($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocQuery::create(null, $criteria);
        $query->joinWith('CommonEchangeDocApplicationClient', $join_behavior);

        return $this->getCommonEchangeDocs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonEchangeDocs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDoc[] List of CommonEchangeDoc objects
     */
    public function getCommonEchangeDocsJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocQuery::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonEchangeDocs($query, $con);
    }

    /**
     * Clears out the collCommonEchangeDocHistoriques collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonEchangeDocHistoriques()
     */
    public function clearCommonEchangeDocHistoriques()
    {
        $this->collCommonEchangeDocHistoriques = null; // important to set this to null since that means it is uninitialized
        $this->collCommonEchangeDocHistoriquesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonEchangeDocHistoriques collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonEchangeDocHistoriques($v = true)
    {
        $this->collCommonEchangeDocHistoriquesPartial = $v;
    }

    /**
     * Initializes the collCommonEchangeDocHistoriques collection.
     *
     * By default this just sets the collCommonEchangeDocHistoriques collection to an empty array (like clearcollCommonEchangeDocHistoriques());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonEchangeDocHistoriques($overrideExisting = true)
    {
        if (null !== $this->collCommonEchangeDocHistoriques && !$overrideExisting) {
            return;
        }
        $this->collCommonEchangeDocHistoriques = new PropelObjectCollection();
        $this->collCommonEchangeDocHistoriques->setModel('CommonEchangeDocHistorique');
    }

    /**
     * Gets an array of CommonEchangeDocHistorique objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonEchangeDocHistorique[] List of CommonEchangeDocHistorique objects
     * @throws PropelException
     */
    public function getCommonEchangeDocHistoriques($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocHistoriquesPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocHistoriques || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocHistoriques) {
                // return empty collection
                $this->initCommonEchangeDocHistoriques();
            } else {
                $collCommonEchangeDocHistoriques = CommonEchangeDocHistoriqueQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonEchangeDocHistoriquesPartial && count($collCommonEchangeDocHistoriques)) {
                      $this->initCommonEchangeDocHistoriques(false);

                      foreach ($collCommonEchangeDocHistoriques as $obj) {
                        if (false == $this->collCommonEchangeDocHistoriques->contains($obj)) {
                          $this->collCommonEchangeDocHistoriques->append($obj);
                        }
                      }

                      $this->collCommonEchangeDocHistoriquesPartial = true;
                    }

                    $collCommonEchangeDocHistoriques->getInternalIterator()->rewind();

                    return $collCommonEchangeDocHistoriques;
                }

                if ($partial && $this->collCommonEchangeDocHistoriques) {
                    foreach ($this->collCommonEchangeDocHistoriques as $obj) {
                        if ($obj->isNew()) {
                            $collCommonEchangeDocHistoriques[] = $obj;
                        }
                    }
                }

                $this->collCommonEchangeDocHistoriques = $collCommonEchangeDocHistoriques;
                $this->collCommonEchangeDocHistoriquesPartial = false;
            }
        }

        return $this->collCommonEchangeDocHistoriques;
    }

    /**
     * Sets a collection of CommonEchangeDocHistorique objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonEchangeDocHistoriques A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonEchangeDocHistoriques(PropelCollection $commonEchangeDocHistoriques, PropelPDO $con = null)
    {
        $commonEchangeDocHistoriquesToDelete = $this->getCommonEchangeDocHistoriques(new Criteria(), $con)->diff($commonEchangeDocHistoriques);


        $this->commonEchangeDocHistoriquesScheduledForDeletion = $commonEchangeDocHistoriquesToDelete;

        foreach ($commonEchangeDocHistoriquesToDelete as $commonEchangeDocHistoriqueRemoved) {
            $commonEchangeDocHistoriqueRemoved->setCommonAgent(null);
        }

        $this->collCommonEchangeDocHistoriques = null;
        foreach ($commonEchangeDocHistoriques as $commonEchangeDocHistorique) {
            $this->addCommonEchangeDocHistorique($commonEchangeDocHistorique);
        }

        $this->collCommonEchangeDocHistoriques = $commonEchangeDocHistoriques;
        $this->collCommonEchangeDocHistoriquesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonEchangeDocHistorique objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonEchangeDocHistorique objects.
     * @throws PropelException
     */
    public function countCommonEchangeDocHistoriques(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonEchangeDocHistoriquesPartial && !$this->isNew();
        if (null === $this->collCommonEchangeDocHistoriques || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonEchangeDocHistoriques) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonEchangeDocHistoriques());
            }
            $query = CommonEchangeDocHistoriqueQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonEchangeDocHistoriques);
    }

    /**
     * Method called to associate a CommonEchangeDocHistorique object to this object
     * through the CommonEchangeDocHistorique foreign key attribute.
     *
     * @param   CommonEchangeDocHistorique $l CommonEchangeDocHistorique
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonEchangeDocHistorique(CommonEchangeDocHistorique $l)
    {
        if ($this->collCommonEchangeDocHistoriques === null) {
            $this->initCommonEchangeDocHistoriques();
            $this->collCommonEchangeDocHistoriquesPartial = true;
        }
        if (!in_array($l, $this->collCommonEchangeDocHistoriques->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonEchangeDocHistorique($l);
        }

        return $this;
    }

    /**
     * @param	CommonEchangeDocHistorique $commonEchangeDocHistorique The commonEchangeDocHistorique object to add.
     */
    protected function doAddCommonEchangeDocHistorique($commonEchangeDocHistorique)
    {
        $this->collCommonEchangeDocHistoriques[]= $commonEchangeDocHistorique;
        $commonEchangeDocHistorique->setCommonAgent($this);
    }

    /**
     * @param	CommonEchangeDocHistorique $commonEchangeDocHistorique The commonEchangeDocHistorique object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonEchangeDocHistorique($commonEchangeDocHistorique)
    {
        if ($this->getCommonEchangeDocHistoriques()->contains($commonEchangeDocHistorique)) {
            $this->collCommonEchangeDocHistoriques->remove($this->collCommonEchangeDocHistoriques->search($commonEchangeDocHistorique));
            if (null === $this->commonEchangeDocHistoriquesScheduledForDeletion) {
                $this->commonEchangeDocHistoriquesScheduledForDeletion = clone $this->collCommonEchangeDocHistoriques;
                $this->commonEchangeDocHistoriquesScheduledForDeletion->clear();
            }
            $this->commonEchangeDocHistoriquesScheduledForDeletion[]= $commonEchangeDocHistorique;
            $commonEchangeDocHistorique->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonEchangeDocHistoriques from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonEchangeDocHistorique[] List of CommonEchangeDocHistorique objects
     */
    public function getCommonEchangeDocHistoriquesJoinCommonEchangeDoc($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonEchangeDocHistoriqueQuery::create(null, $criteria);
        $query->joinWith('CommonEchangeDoc', $join_behavior);

        return $this->getCommonEchangeDocHistoriques($query, $con);
    }

    /**
     * Clears out the collCommonHabilitationAgentWss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonHabilitationAgentWss()
     */
    public function clearCommonHabilitationAgentWss()
    {
        $this->collCommonHabilitationAgentWss = null; // important to set this to null since that means it is uninitialized
        $this->collCommonHabilitationAgentWssPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonHabilitationAgentWss collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonHabilitationAgentWss($v = true)
    {
        $this->collCommonHabilitationAgentWssPartial = $v;
    }

    /**
     * Initializes the collCommonHabilitationAgentWss collection.
     *
     * By default this just sets the collCommonHabilitationAgentWss collection to an empty array (like clearcollCommonHabilitationAgentWss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonHabilitationAgentWss($overrideExisting = true)
    {
        if (null !== $this->collCommonHabilitationAgentWss && !$overrideExisting) {
            return;
        }
        $this->collCommonHabilitationAgentWss = new PropelObjectCollection();
        $this->collCommonHabilitationAgentWss->setModel('CommonHabilitationAgentWs');
    }

    /**
     * Gets an array of CommonHabilitationAgentWs objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonHabilitationAgentWs[] List of CommonHabilitationAgentWs objects
     * @throws PropelException
     */
    public function getCommonHabilitationAgentWss($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonHabilitationAgentWssPartial && !$this->isNew();
        if (null === $this->collCommonHabilitationAgentWss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonHabilitationAgentWss) {
                // return empty collection
                $this->initCommonHabilitationAgentWss();
            } else {
                $collCommonHabilitationAgentWss = CommonHabilitationAgentWsQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonHabilitationAgentWssPartial && count($collCommonHabilitationAgentWss)) {
                      $this->initCommonHabilitationAgentWss(false);

                      foreach ($collCommonHabilitationAgentWss as $obj) {
                        if (false == $this->collCommonHabilitationAgentWss->contains($obj)) {
                          $this->collCommonHabilitationAgentWss->append($obj);
                        }
                      }

                      $this->collCommonHabilitationAgentWssPartial = true;
                    }

                    $collCommonHabilitationAgentWss->getInternalIterator()->rewind();

                    return $collCommonHabilitationAgentWss;
                }

                if ($partial && $this->collCommonHabilitationAgentWss) {
                    foreach ($this->collCommonHabilitationAgentWss as $obj) {
                        if ($obj->isNew()) {
                            $collCommonHabilitationAgentWss[] = $obj;
                        }
                    }
                }

                $this->collCommonHabilitationAgentWss = $collCommonHabilitationAgentWss;
                $this->collCommonHabilitationAgentWssPartial = false;
            }
        }

        return $this->collCommonHabilitationAgentWss;
    }

    /**
     * Sets a collection of CommonHabilitationAgentWs objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonHabilitationAgentWss A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonHabilitationAgentWss(PropelCollection $commonHabilitationAgentWss, PropelPDO $con = null)
    {
        $commonHabilitationAgentWssToDelete = $this->getCommonHabilitationAgentWss(new Criteria(), $con)->diff($commonHabilitationAgentWss);


        $this->commonHabilitationAgentWssScheduledForDeletion = $commonHabilitationAgentWssToDelete;

        foreach ($commonHabilitationAgentWssToDelete as $commonHabilitationAgentWsRemoved) {
            $commonHabilitationAgentWsRemoved->setCommonAgent(null);
        }

        $this->collCommonHabilitationAgentWss = null;
        foreach ($commonHabilitationAgentWss as $commonHabilitationAgentWs) {
            $this->addCommonHabilitationAgentWs($commonHabilitationAgentWs);
        }

        $this->collCommonHabilitationAgentWss = $commonHabilitationAgentWss;
        $this->collCommonHabilitationAgentWssPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonHabilitationAgentWs objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonHabilitationAgentWs objects.
     * @throws PropelException
     */
    public function countCommonHabilitationAgentWss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonHabilitationAgentWssPartial && !$this->isNew();
        if (null === $this->collCommonHabilitationAgentWss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonHabilitationAgentWss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonHabilitationAgentWss());
            }
            $query = CommonHabilitationAgentWsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonHabilitationAgentWss);
    }

    /**
     * Method called to associate a CommonHabilitationAgentWs object to this object
     * through the CommonHabilitationAgentWs foreign key attribute.
     *
     * @param   CommonHabilitationAgentWs $l CommonHabilitationAgentWs
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonHabilitationAgentWs(CommonHabilitationAgentWs $l)
    {
        if ($this->collCommonHabilitationAgentWss === null) {
            $this->initCommonHabilitationAgentWss();
            $this->collCommonHabilitationAgentWssPartial = true;
        }
        if (!in_array($l, $this->collCommonHabilitationAgentWss->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonHabilitationAgentWs($l);
        }

        return $this;
    }

    /**
     * @param	CommonHabilitationAgentWs $commonHabilitationAgentWs The commonHabilitationAgentWs object to add.
     */
    protected function doAddCommonHabilitationAgentWs($commonHabilitationAgentWs)
    {
        $this->collCommonHabilitationAgentWss[]= $commonHabilitationAgentWs;
        $commonHabilitationAgentWs->setCommonAgent($this);
    }

    /**
     * @param	CommonHabilitationAgentWs $commonHabilitationAgentWs The commonHabilitationAgentWs object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonHabilitationAgentWs($commonHabilitationAgentWs)
    {
        if ($this->getCommonHabilitationAgentWss()->contains($commonHabilitationAgentWs)) {
            $this->collCommonHabilitationAgentWss->remove($this->collCommonHabilitationAgentWss->search($commonHabilitationAgentWs));
            if (null === $this->commonHabilitationAgentWssScheduledForDeletion) {
                $this->commonHabilitationAgentWssScheduledForDeletion = clone $this->collCommonHabilitationAgentWss;
                $this->commonHabilitationAgentWssScheduledForDeletion->clear();
            }
            $this->commonHabilitationAgentWssScheduledForDeletion[]= clone $commonHabilitationAgentWs;
            $commonHabilitationAgentWs->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonHabilitationAgentWss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonHabilitationAgentWs[] List of CommonHabilitationAgentWs objects
     */
    public function getCommonHabilitationAgentWssJoinCommonWebService($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonHabilitationAgentWsQuery::create(null, $criteria);
        $query->joinWith('CommonWebService', $join_behavior);

        return $this->getCommonHabilitationAgentWss($query, $con);
    }

    /**
     * Clears out the collCommonInvitePermanentTransverses collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonInvitePermanentTransverses()
     */
    public function clearCommonInvitePermanentTransverses()
    {
        $this->collCommonInvitePermanentTransverses = null; // important to set this to null since that means it is uninitialized
        $this->collCommonInvitePermanentTransversesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonInvitePermanentTransverses collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonInvitePermanentTransverses($v = true)
    {
        $this->collCommonInvitePermanentTransversesPartial = $v;
    }

    /**
     * Initializes the collCommonInvitePermanentTransverses collection.
     *
     * By default this just sets the collCommonInvitePermanentTransverses collection to an empty array (like clearcollCommonInvitePermanentTransverses());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonInvitePermanentTransverses($overrideExisting = true)
    {
        if (null !== $this->collCommonInvitePermanentTransverses && !$overrideExisting) {
            return;
        }
        $this->collCommonInvitePermanentTransverses = new PropelObjectCollection();
        $this->collCommonInvitePermanentTransverses->setModel('CommonInvitePermanentTransverse');
    }

    /**
     * Gets an array of CommonInvitePermanentTransverse objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonInvitePermanentTransverse[] List of CommonInvitePermanentTransverse objects
     * @throws PropelException
     */
    public function getCommonInvitePermanentTransverses($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonInvitePermanentTransversesPartial && !$this->isNew();
        if (null === $this->collCommonInvitePermanentTransverses || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonInvitePermanentTransverses) {
                // return empty collection
                $this->initCommonInvitePermanentTransverses();
            } else {
                $collCommonInvitePermanentTransverses = CommonInvitePermanentTransverseQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonInvitePermanentTransversesPartial && count($collCommonInvitePermanentTransverses)) {
                      $this->initCommonInvitePermanentTransverses(false);

                      foreach ($collCommonInvitePermanentTransverses as $obj) {
                        if (false == $this->collCommonInvitePermanentTransverses->contains($obj)) {
                          $this->collCommonInvitePermanentTransverses->append($obj);
                        }
                      }

                      $this->collCommonInvitePermanentTransversesPartial = true;
                    }

                    $collCommonInvitePermanentTransverses->getInternalIterator()->rewind();

                    return $collCommonInvitePermanentTransverses;
                }

                if ($partial && $this->collCommonInvitePermanentTransverses) {
                    foreach ($this->collCommonInvitePermanentTransverses as $obj) {
                        if ($obj->isNew()) {
                            $collCommonInvitePermanentTransverses[] = $obj;
                        }
                    }
                }

                $this->collCommonInvitePermanentTransverses = $collCommonInvitePermanentTransverses;
                $this->collCommonInvitePermanentTransversesPartial = false;
            }
        }

        return $this->collCommonInvitePermanentTransverses;
    }

    /**
     * Sets a collection of CommonInvitePermanentTransverse objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonInvitePermanentTransverses A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonInvitePermanentTransverses(PropelCollection $commonInvitePermanentTransverses, PropelPDO $con = null)
    {
        $commonInvitePermanentTransversesToDelete = $this->getCommonInvitePermanentTransverses(new Criteria(), $con)->diff($commonInvitePermanentTransverses);


        $this->commonInvitePermanentTransversesScheduledForDeletion = $commonInvitePermanentTransversesToDelete;

        foreach ($commonInvitePermanentTransversesToDelete as $commonInvitePermanentTransverseRemoved) {
            $commonInvitePermanentTransverseRemoved->setCommonAgent(null);
        }

        $this->collCommonInvitePermanentTransverses = null;
        foreach ($commonInvitePermanentTransverses as $commonInvitePermanentTransverse) {
            $this->addCommonInvitePermanentTransverse($commonInvitePermanentTransverse);
        }

        $this->collCommonInvitePermanentTransverses = $commonInvitePermanentTransverses;
        $this->collCommonInvitePermanentTransversesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonInvitePermanentTransverse objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonInvitePermanentTransverse objects.
     * @throws PropelException
     */
    public function countCommonInvitePermanentTransverses(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonInvitePermanentTransversesPartial && !$this->isNew();
        if (null === $this->collCommonInvitePermanentTransverses || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonInvitePermanentTransverses) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonInvitePermanentTransverses());
            }
            $query = CommonInvitePermanentTransverseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonInvitePermanentTransverses);
    }

    /**
     * Method called to associate a CommonInvitePermanentTransverse object to this object
     * through the CommonInvitePermanentTransverse foreign key attribute.
     *
     * @param   CommonInvitePermanentTransverse $l CommonInvitePermanentTransverse
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonInvitePermanentTransverse(CommonInvitePermanentTransverse $l)
    {
        if ($this->collCommonInvitePermanentTransverses === null) {
            $this->initCommonInvitePermanentTransverses();
            $this->collCommonInvitePermanentTransversesPartial = true;
        }
        if (!in_array($l, $this->collCommonInvitePermanentTransverses->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonInvitePermanentTransverse($l);
        }

        return $this;
    }

    /**
     * @param	CommonInvitePermanentTransverse $commonInvitePermanentTransverse The commonInvitePermanentTransverse object to add.
     */
    protected function doAddCommonInvitePermanentTransverse($commonInvitePermanentTransverse)
    {
        $this->collCommonInvitePermanentTransverses[]= $commonInvitePermanentTransverse;
        $commonInvitePermanentTransverse->setCommonAgent($this);
    }

    /**
     * @param	CommonInvitePermanentTransverse $commonInvitePermanentTransverse The commonInvitePermanentTransverse object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonInvitePermanentTransverse($commonInvitePermanentTransverse)
    {
        if ($this->getCommonInvitePermanentTransverses()->contains($commonInvitePermanentTransverse)) {
            $this->collCommonInvitePermanentTransverses->remove($this->collCommonInvitePermanentTransverses->search($commonInvitePermanentTransverse));
            if (null === $this->commonInvitePermanentTransversesScheduledForDeletion) {
                $this->commonInvitePermanentTransversesScheduledForDeletion = clone $this->collCommonInvitePermanentTransverses;
                $this->commonInvitePermanentTransversesScheduledForDeletion->clear();
            }
            $this->commonInvitePermanentTransversesScheduledForDeletion[]= clone $commonInvitePermanentTransverse;
            $commonInvitePermanentTransverse->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonInvitePermanentTransverses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonInvitePermanentTransverse[] List of CommonInvitePermanentTransverse objects
     */
    public function getCommonInvitePermanentTransversesJoinCommonService($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonInvitePermanentTransverseQuery::create(null, $criteria);
        $query->joinWith('CommonService', $join_behavior);

        return $this->getCommonInvitePermanentTransverses($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonInvitePermanentTransverses from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonInvitePermanentTransverse[] List of CommonInvitePermanentTransverse objects
     */
    public function getCommonInvitePermanentTransversesJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonInvitePermanentTransverseQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonInvitePermanentTransverses($query, $con);
    }

    /**
     * Clears out the collCommonModificationContrats collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonModificationContrats()
     */
    public function clearCommonModificationContrats()
    {
        $this->collCommonModificationContrats = null; // important to set this to null since that means it is uninitialized
        $this->collCommonModificationContratsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonModificationContrats collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonModificationContrats($v = true)
    {
        $this->collCommonModificationContratsPartial = $v;
    }

    /**
     * Initializes the collCommonModificationContrats collection.
     *
     * By default this just sets the collCommonModificationContrats collection to an empty array (like clearcollCommonModificationContrats());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonModificationContrats($overrideExisting = true)
    {
        if (null !== $this->collCommonModificationContrats && !$overrideExisting) {
            return;
        }
        $this->collCommonModificationContrats = new PropelObjectCollection();
        $this->collCommonModificationContrats->setModel('CommonModificationContrat');
    }

    /**
     * Gets an array of CommonModificationContrat objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonModificationContrat[] List of CommonModificationContrat objects
     * @throws PropelException
     */
    public function getCommonModificationContrats($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonModificationContratsPartial && !$this->isNew();
        if (null === $this->collCommonModificationContrats || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonModificationContrats) {
                // return empty collection
                $this->initCommonModificationContrats();
            } else {
                $collCommonModificationContrats = CommonModificationContratQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonModificationContratsPartial && count($collCommonModificationContrats)) {
                      $this->initCommonModificationContrats(false);

                      foreach ($collCommonModificationContrats as $obj) {
                        if (false == $this->collCommonModificationContrats->contains($obj)) {
                          $this->collCommonModificationContrats->append($obj);
                        }
                      }

                      $this->collCommonModificationContratsPartial = true;
                    }

                    $collCommonModificationContrats->getInternalIterator()->rewind();

                    return $collCommonModificationContrats;
                }

                if ($partial && $this->collCommonModificationContrats) {
                    foreach ($this->collCommonModificationContrats as $obj) {
                        if ($obj->isNew()) {
                            $collCommonModificationContrats[] = $obj;
                        }
                    }
                }

                $this->collCommonModificationContrats = $collCommonModificationContrats;
                $this->collCommonModificationContratsPartial = false;
            }
        }

        return $this->collCommonModificationContrats;
    }

    /**
     * Sets a collection of CommonModificationContrat objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonModificationContrats A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonModificationContrats(PropelCollection $commonModificationContrats, PropelPDO $con = null)
    {
        $commonModificationContratsToDelete = $this->getCommonModificationContrats(new Criteria(), $con)->diff($commonModificationContrats);


        $this->commonModificationContratsScheduledForDeletion = $commonModificationContratsToDelete;

        foreach ($commonModificationContratsToDelete as $commonModificationContratRemoved) {
            $commonModificationContratRemoved->setCommonAgent(null);
        }

        $this->collCommonModificationContrats = null;
        foreach ($commonModificationContrats as $commonModificationContrat) {
            $this->addCommonModificationContrat($commonModificationContrat);
        }

        $this->collCommonModificationContrats = $commonModificationContrats;
        $this->collCommonModificationContratsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonModificationContrat objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonModificationContrat objects.
     * @throws PropelException
     */
    public function countCommonModificationContrats(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonModificationContratsPartial && !$this->isNew();
        if (null === $this->collCommonModificationContrats || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonModificationContrats) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonModificationContrats());
            }
            $query = CommonModificationContratQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonModificationContrats);
    }

    /**
     * Method called to associate a CommonModificationContrat object to this object
     * through the CommonModificationContrat foreign key attribute.
     *
     * @param   CommonModificationContrat $l CommonModificationContrat
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonModificationContrat(CommonModificationContrat $l)
    {
        if ($this->collCommonModificationContrats === null) {
            $this->initCommonModificationContrats();
            $this->collCommonModificationContratsPartial = true;
        }
        if (!in_array($l, $this->collCommonModificationContrats->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonModificationContrat($l);
        }

        return $this;
    }

    /**
     * @param	CommonModificationContrat $commonModificationContrat The commonModificationContrat object to add.
     */
    protected function doAddCommonModificationContrat($commonModificationContrat)
    {
        $this->collCommonModificationContrats[]= $commonModificationContrat;
        $commonModificationContrat->setCommonAgent($this);
    }

    /**
     * @param	CommonModificationContrat $commonModificationContrat The commonModificationContrat object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonModificationContrat($commonModificationContrat)
    {
        if ($this->getCommonModificationContrats()->contains($commonModificationContrat)) {
            $this->collCommonModificationContrats->remove($this->collCommonModificationContrats->search($commonModificationContrat));
            if (null === $this->commonModificationContratsScheduledForDeletion) {
                $this->commonModificationContratsScheduledForDeletion = clone $this->collCommonModificationContrats;
                $this->commonModificationContratsScheduledForDeletion->clear();
            }
            $this->commonModificationContratsScheduledForDeletion[]= clone $commonModificationContrat;
            $commonModificationContrat->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonModificationContrats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonModificationContrat[] List of CommonModificationContrat objects
     */
    public function getCommonModificationContratsJoinCommonTContratTitulaire($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonModificationContratQuery::create(null, $criteria);
        $query->joinWith('CommonTContratTitulaire', $join_behavior);

        return $this->getCommonModificationContrats($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonModificationContrats from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonModificationContrat[] List of CommonModificationContrat objects
     */
    public function getCommonModificationContratsJoinCommonTEtablissement($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonModificationContratQuery::create(null, $criteria);
        $query->joinWith('CommonTEtablissement', $join_behavior);

        return $this->getCommonModificationContrats($query, $con);
    }

    /**
     * Clears out the collCommonTCAOCommissionAgents collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonTCAOCommissionAgents()
     */
    public function clearCommonTCAOCommissionAgents()
    {
        $this->collCommonTCAOCommissionAgents = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOCommissionAgentsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOCommissionAgents collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOCommissionAgents($v = true)
    {
        $this->collCommonTCAOCommissionAgentsPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOCommissionAgents collection.
     *
     * By default this just sets the collCommonTCAOCommissionAgents collection to an empty array (like clearcollCommonTCAOCommissionAgents());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOCommissionAgents($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOCommissionAgents && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOCommissionAgents = new PropelObjectCollection();
        $this->collCommonTCAOCommissionAgents->setModel('CommonTCAOCommissionAgent');
    }

    /**
     * Gets an array of CommonTCAOCommissionAgent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOCommissionAgent[] List of CommonTCAOCommissionAgent objects
     * @throws PropelException
     */
    public function getCommonTCAOCommissionAgents($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOCommissionAgentsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOCommissionAgents || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOCommissionAgents) {
                // return empty collection
                $this->initCommonTCAOCommissionAgents();
            } else {
                $collCommonTCAOCommissionAgents = CommonTCAOCommissionAgentQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOCommissionAgentsPartial && count($collCommonTCAOCommissionAgents)) {
                      $this->initCommonTCAOCommissionAgents(false);

                      foreach ($collCommonTCAOCommissionAgents as $obj) {
                        if (false == $this->collCommonTCAOCommissionAgents->contains($obj)) {
                          $this->collCommonTCAOCommissionAgents->append($obj);
                        }
                      }

                      $this->collCommonTCAOCommissionAgentsPartial = true;
                    }

                    $collCommonTCAOCommissionAgents->getInternalIterator()->rewind();

                    return $collCommonTCAOCommissionAgents;
                }

                if ($partial && $this->collCommonTCAOCommissionAgents) {
                    foreach ($this->collCommonTCAOCommissionAgents as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOCommissionAgents[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOCommissionAgents = $collCommonTCAOCommissionAgents;
                $this->collCommonTCAOCommissionAgentsPartial = false;
            }
        }

        return $this->collCommonTCAOCommissionAgents;
    }

    /**
     * Sets a collection of CommonTCAOCommissionAgent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOCommissionAgents A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonTCAOCommissionAgents(PropelCollection $commonTCAOCommissionAgents, PropelPDO $con = null)
    {
        $commonTCAOCommissionAgentsToDelete = $this->getCommonTCAOCommissionAgents(new Criteria(), $con)->diff($commonTCAOCommissionAgents);


        $this->commonTCAOCommissionAgentsScheduledForDeletion = $commonTCAOCommissionAgentsToDelete;

        foreach ($commonTCAOCommissionAgentsToDelete as $commonTCAOCommissionAgentRemoved) {
            $commonTCAOCommissionAgentRemoved->setCommonAgent(null);
        }

        $this->collCommonTCAOCommissionAgents = null;
        foreach ($commonTCAOCommissionAgents as $commonTCAOCommissionAgent) {
            $this->addCommonTCAOCommissionAgent($commonTCAOCommissionAgent);
        }

        $this->collCommonTCAOCommissionAgents = $commonTCAOCommissionAgents;
        $this->collCommonTCAOCommissionAgentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOCommissionAgent objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOCommissionAgent objects.
     * @throws PropelException
     */
    public function countCommonTCAOCommissionAgents(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOCommissionAgentsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOCommissionAgents || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOCommissionAgents) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOCommissionAgents());
            }
            $query = CommonTCAOCommissionAgentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonTCAOCommissionAgents);
    }

    /**
     * Method called to associate a CommonTCAOCommissionAgent object to this object
     * through the CommonTCAOCommissionAgent foreign key attribute.
     *
     * @param   CommonTCAOCommissionAgent $l CommonTCAOCommissionAgent
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonTCAOCommissionAgent(CommonTCAOCommissionAgent $l)
    {
        if ($this->collCommonTCAOCommissionAgents === null) {
            $this->initCommonTCAOCommissionAgents();
            $this->collCommonTCAOCommissionAgentsPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOCommissionAgents->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOCommissionAgent($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOCommissionAgent $commonTCAOCommissionAgent The commonTCAOCommissionAgent object to add.
     */
    protected function doAddCommonTCAOCommissionAgent($commonTCAOCommissionAgent)
    {
        $this->collCommonTCAOCommissionAgents[]= $commonTCAOCommissionAgent;
        $commonTCAOCommissionAgent->setCommonAgent($this);
    }

    /**
     * @param	CommonTCAOCommissionAgent $commonTCAOCommissionAgent The commonTCAOCommissionAgent object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonTCAOCommissionAgent($commonTCAOCommissionAgent)
    {
        if ($this->getCommonTCAOCommissionAgents()->contains($commonTCAOCommissionAgent)) {
            $this->collCommonTCAOCommissionAgents->remove($this->collCommonTCAOCommissionAgents->search($commonTCAOCommissionAgent));
            if (null === $this->commonTCAOCommissionAgentsScheduledForDeletion) {
                $this->commonTCAOCommissionAgentsScheduledForDeletion = clone $this->collCommonTCAOCommissionAgents;
                $this->commonTCAOCommissionAgentsScheduledForDeletion->clear();
            }
            $this->commonTCAOCommissionAgentsScheduledForDeletion[]= clone $commonTCAOCommissionAgent;
            $commonTCAOCommissionAgent->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonTCAOCommissionAgents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOCommissionAgent[] List of CommonTCAOCommissionAgent objects
     */
    public function getCommonTCAOCommissionAgentsJoinCommonTCAOCommission($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOCommissionAgentQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOCommission', $join_behavior);

        return $this->getCommonTCAOCommissionAgents($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonTCAOCommissionAgents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOCommissionAgent[] List of CommonTCAOCommissionAgent objects
     */
    public function getCommonTCAOCommissionAgentsJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOCommissionAgentQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonTCAOCommissionAgents($query, $con);
    }

    /**
     * Clears out the collCommonTCAOOrdreDuJourIntervenants collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonTCAOOrdreDuJourIntervenants()
     */
    public function clearCommonTCAOOrdreDuJourIntervenants()
    {
        $this->collCommonTCAOOrdreDuJourIntervenants = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOOrdreDuJourIntervenantsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOOrdreDuJourIntervenants collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOOrdreDuJourIntervenants($v = true)
    {
        $this->collCommonTCAOOrdreDuJourIntervenantsPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOOrdreDuJourIntervenants collection.
     *
     * By default this just sets the collCommonTCAOOrdreDuJourIntervenants collection to an empty array (like clearcollCommonTCAOOrdreDuJourIntervenants());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOOrdreDuJourIntervenants($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOOrdreDuJourIntervenants && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOOrdreDuJourIntervenants = new PropelObjectCollection();
        $this->collCommonTCAOOrdreDuJourIntervenants->setModel('CommonTCAOOrdreDuJourIntervenant');
    }

    /**
     * Gets an array of CommonTCAOOrdreDuJourIntervenant objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOOrdreDuJourIntervenant[] List of CommonTCAOOrdreDuJourIntervenant objects
     * @throws PropelException
     */
    public function getCommonTCAOOrdreDuJourIntervenants($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOOrdreDuJourIntervenantsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOOrdreDuJourIntervenants || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOOrdreDuJourIntervenants) {
                // return empty collection
                $this->initCommonTCAOOrdreDuJourIntervenants();
            } else {
                $collCommonTCAOOrdreDuJourIntervenants = CommonTCAOOrdreDuJourIntervenantQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOOrdreDuJourIntervenantsPartial && count($collCommonTCAOOrdreDuJourIntervenants)) {
                      $this->initCommonTCAOOrdreDuJourIntervenants(false);

                      foreach ($collCommonTCAOOrdreDuJourIntervenants as $obj) {
                        if (false == $this->collCommonTCAOOrdreDuJourIntervenants->contains($obj)) {
                          $this->collCommonTCAOOrdreDuJourIntervenants->append($obj);
                        }
                      }

                      $this->collCommonTCAOOrdreDuJourIntervenantsPartial = true;
                    }

                    $collCommonTCAOOrdreDuJourIntervenants->getInternalIterator()->rewind();

                    return $collCommonTCAOOrdreDuJourIntervenants;
                }

                if ($partial && $this->collCommonTCAOOrdreDuJourIntervenants) {
                    foreach ($this->collCommonTCAOOrdreDuJourIntervenants as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOOrdreDuJourIntervenants[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOOrdreDuJourIntervenants = $collCommonTCAOOrdreDuJourIntervenants;
                $this->collCommonTCAOOrdreDuJourIntervenantsPartial = false;
            }
        }

        return $this->collCommonTCAOOrdreDuJourIntervenants;
    }

    /**
     * Sets a collection of CommonTCAOOrdreDuJourIntervenant objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOOrdreDuJourIntervenants A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonTCAOOrdreDuJourIntervenants(PropelCollection $commonTCAOOrdreDuJourIntervenants, PropelPDO $con = null)
    {
        $commonTCAOOrdreDuJourIntervenantsToDelete = $this->getCommonTCAOOrdreDuJourIntervenants(new Criteria(), $con)->diff($commonTCAOOrdreDuJourIntervenants);


        $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion = $commonTCAOOrdreDuJourIntervenantsToDelete;

        foreach ($commonTCAOOrdreDuJourIntervenantsToDelete as $commonTCAOOrdreDuJourIntervenantRemoved) {
            $commonTCAOOrdreDuJourIntervenantRemoved->setCommonAgent(null);
        }

        $this->collCommonTCAOOrdreDuJourIntervenants = null;
        foreach ($commonTCAOOrdreDuJourIntervenants as $commonTCAOOrdreDuJourIntervenant) {
            $this->addCommonTCAOOrdreDuJourIntervenant($commonTCAOOrdreDuJourIntervenant);
        }

        $this->collCommonTCAOOrdreDuJourIntervenants = $commonTCAOOrdreDuJourIntervenants;
        $this->collCommonTCAOOrdreDuJourIntervenantsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOOrdreDuJourIntervenant objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOOrdreDuJourIntervenant objects.
     * @throws PropelException
     */
    public function countCommonTCAOOrdreDuJourIntervenants(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOOrdreDuJourIntervenantsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOOrdreDuJourIntervenants || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOOrdreDuJourIntervenants) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOOrdreDuJourIntervenants());
            }
            $query = CommonTCAOOrdreDuJourIntervenantQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonTCAOOrdreDuJourIntervenants);
    }

    /**
     * Method called to associate a CommonTCAOOrdreDuJourIntervenant object to this object
     * through the CommonTCAOOrdreDuJourIntervenant foreign key attribute.
     *
     * @param   CommonTCAOOrdreDuJourIntervenant $l CommonTCAOOrdreDuJourIntervenant
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonTCAOOrdreDuJourIntervenant(CommonTCAOOrdreDuJourIntervenant $l)
    {
        if ($this->collCommonTCAOOrdreDuJourIntervenants === null) {
            $this->initCommonTCAOOrdreDuJourIntervenants();
            $this->collCommonTCAOOrdreDuJourIntervenantsPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOOrdreDuJourIntervenants->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOOrdreDuJourIntervenant($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOOrdreDuJourIntervenant $commonTCAOOrdreDuJourIntervenant The commonTCAOOrdreDuJourIntervenant object to add.
     */
    protected function doAddCommonTCAOOrdreDuJourIntervenant($commonTCAOOrdreDuJourIntervenant)
    {
        $this->collCommonTCAOOrdreDuJourIntervenants[]= $commonTCAOOrdreDuJourIntervenant;
        $commonTCAOOrdreDuJourIntervenant->setCommonAgent($this);
    }

    /**
     * @param	CommonTCAOOrdreDuJourIntervenant $commonTCAOOrdreDuJourIntervenant The commonTCAOOrdreDuJourIntervenant object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonTCAOOrdreDuJourIntervenant($commonTCAOOrdreDuJourIntervenant)
    {
        if ($this->getCommonTCAOOrdreDuJourIntervenants()->contains($commonTCAOOrdreDuJourIntervenant)) {
            $this->collCommonTCAOOrdreDuJourIntervenants->remove($this->collCommonTCAOOrdreDuJourIntervenants->search($commonTCAOOrdreDuJourIntervenant));
            if (null === $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion) {
                $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion = clone $this->collCommonTCAOOrdreDuJourIntervenants;
                $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion->clear();
            }
            $this->commonTCAOOrdreDuJourIntervenantsScheduledForDeletion[]= clone $commonTCAOOrdreDuJourIntervenant;
            $commonTCAOOrdreDuJourIntervenant->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonTCAOOrdreDuJourIntervenants from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOOrdreDuJourIntervenant[] List of CommonTCAOOrdreDuJourIntervenant objects
     */
    public function getCommonTCAOOrdreDuJourIntervenantsJoinCommonTCAOIntervenantExterne($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOOrdreDuJourIntervenantQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOIntervenantExterne', $join_behavior);

        return $this->getCommonTCAOOrdreDuJourIntervenants($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonTCAOOrdreDuJourIntervenants from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOOrdreDuJourIntervenant[] List of CommonTCAOOrdreDuJourIntervenant objects
     */
    public function getCommonTCAOOrdreDuJourIntervenantsJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOOrdreDuJourIntervenantQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonTCAOOrdreDuJourIntervenants($query, $con);
    }

    /**
     * Clears out the collCommonTCAOSeanceAgents collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonTCAOSeanceAgents()
     */
    public function clearCommonTCAOSeanceAgents()
    {
        $this->collCommonTCAOSeanceAgents = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOSeanceAgentsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOSeanceAgents collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOSeanceAgents($v = true)
    {
        $this->collCommonTCAOSeanceAgentsPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOSeanceAgents collection.
     *
     * By default this just sets the collCommonTCAOSeanceAgents collection to an empty array (like clearcollCommonTCAOSeanceAgents());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOSeanceAgents($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOSeanceAgents && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOSeanceAgents = new PropelObjectCollection();
        $this->collCommonTCAOSeanceAgents->setModel('CommonTCAOSeanceAgent');
    }

    /**
     * Gets an array of CommonTCAOSeanceAgent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOSeanceAgent[] List of CommonTCAOSeanceAgent objects
     * @throws PropelException
     */
    public function getCommonTCAOSeanceAgents($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOSeanceAgentsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOSeanceAgents || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOSeanceAgents) {
                // return empty collection
                $this->initCommonTCAOSeanceAgents();
            } else {
                $collCommonTCAOSeanceAgents = CommonTCAOSeanceAgentQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOSeanceAgentsPartial && count($collCommonTCAOSeanceAgents)) {
                      $this->initCommonTCAOSeanceAgents(false);

                      foreach ($collCommonTCAOSeanceAgents as $obj) {
                        if (false == $this->collCommonTCAOSeanceAgents->contains($obj)) {
                          $this->collCommonTCAOSeanceAgents->append($obj);
                        }
                      }

                      $this->collCommonTCAOSeanceAgentsPartial = true;
                    }

                    $collCommonTCAOSeanceAgents->getInternalIterator()->rewind();

                    return $collCommonTCAOSeanceAgents;
                }

                if ($partial && $this->collCommonTCAOSeanceAgents) {
                    foreach ($this->collCommonTCAOSeanceAgents as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOSeanceAgents[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOSeanceAgents = $collCommonTCAOSeanceAgents;
                $this->collCommonTCAOSeanceAgentsPartial = false;
            }
        }

        return $this->collCommonTCAOSeanceAgents;
    }

    /**
     * Sets a collection of CommonTCAOSeanceAgent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOSeanceAgents A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonTCAOSeanceAgents(PropelCollection $commonTCAOSeanceAgents, PropelPDO $con = null)
    {
        $commonTCAOSeanceAgentsToDelete = $this->getCommonTCAOSeanceAgents(new Criteria(), $con)->diff($commonTCAOSeanceAgents);


        $this->commonTCAOSeanceAgentsScheduledForDeletion = $commonTCAOSeanceAgentsToDelete;

        foreach ($commonTCAOSeanceAgentsToDelete as $commonTCAOSeanceAgentRemoved) {
            $commonTCAOSeanceAgentRemoved->setCommonAgent(null);
        }

        $this->collCommonTCAOSeanceAgents = null;
        foreach ($commonTCAOSeanceAgents as $commonTCAOSeanceAgent) {
            $this->addCommonTCAOSeanceAgent($commonTCAOSeanceAgent);
        }

        $this->collCommonTCAOSeanceAgents = $commonTCAOSeanceAgents;
        $this->collCommonTCAOSeanceAgentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOSeanceAgent objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOSeanceAgent objects.
     * @throws PropelException
     */
    public function countCommonTCAOSeanceAgents(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOSeanceAgentsPartial && !$this->isNew();
        if (null === $this->collCommonTCAOSeanceAgents || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOSeanceAgents) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOSeanceAgents());
            }
            $query = CommonTCAOSeanceAgentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonTCAOSeanceAgents);
    }

    /**
     * Method called to associate a CommonTCAOSeanceAgent object to this object
     * through the CommonTCAOSeanceAgent foreign key attribute.
     *
     * @param   CommonTCAOSeanceAgent $l CommonTCAOSeanceAgent
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonTCAOSeanceAgent(CommonTCAOSeanceAgent $l)
    {
        if ($this->collCommonTCAOSeanceAgents === null) {
            $this->initCommonTCAOSeanceAgents();
            $this->collCommonTCAOSeanceAgentsPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOSeanceAgents->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOSeanceAgent($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOSeanceAgent $commonTCAOSeanceAgent The commonTCAOSeanceAgent object to add.
     */
    protected function doAddCommonTCAOSeanceAgent($commonTCAOSeanceAgent)
    {
        $this->collCommonTCAOSeanceAgents[]= $commonTCAOSeanceAgent;
        $commonTCAOSeanceAgent->setCommonAgent($this);
    }

    /**
     * @param	CommonTCAOSeanceAgent $commonTCAOSeanceAgent The commonTCAOSeanceAgent object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonTCAOSeanceAgent($commonTCAOSeanceAgent)
    {
        if ($this->getCommonTCAOSeanceAgents()->contains($commonTCAOSeanceAgent)) {
            $this->collCommonTCAOSeanceAgents->remove($this->collCommonTCAOSeanceAgents->search($commonTCAOSeanceAgent));
            if (null === $this->commonTCAOSeanceAgentsScheduledForDeletion) {
                $this->commonTCAOSeanceAgentsScheduledForDeletion = clone $this->collCommonTCAOSeanceAgents;
                $this->commonTCAOSeanceAgentsScheduledForDeletion->clear();
            }
            $this->commonTCAOSeanceAgentsScheduledForDeletion[]= clone $commonTCAOSeanceAgent;
            $commonTCAOSeanceAgent->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonTCAOSeanceAgents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOSeanceAgent[] List of CommonTCAOSeanceAgent objects
     */
    public function getCommonTCAOSeanceAgentsJoinCommonTCAOSeance($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOSeanceAgentQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOSeance', $join_behavior);

        return $this->getCommonTCAOSeanceAgents($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonTCAOSeanceAgents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOSeanceAgent[] List of CommonTCAOSeanceAgent objects
     */
    public function getCommonTCAOSeanceAgentsJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOSeanceAgentQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonTCAOSeanceAgents($query, $con);
    }

    /**
     * Clears out the collCommonTCAOSeanceInvites collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonTCAOSeanceInvites()
     */
    public function clearCommonTCAOSeanceInvites()
    {
        $this->collCommonTCAOSeanceInvites = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTCAOSeanceInvitesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTCAOSeanceInvites collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTCAOSeanceInvites($v = true)
    {
        $this->collCommonTCAOSeanceInvitesPartial = $v;
    }

    /**
     * Initializes the collCommonTCAOSeanceInvites collection.
     *
     * By default this just sets the collCommonTCAOSeanceInvites collection to an empty array (like clearcollCommonTCAOSeanceInvites());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTCAOSeanceInvites($overrideExisting = true)
    {
        if (null !== $this->collCommonTCAOSeanceInvites && !$overrideExisting) {
            return;
        }
        $this->collCommonTCAOSeanceInvites = new PropelObjectCollection();
        $this->collCommonTCAOSeanceInvites->setModel('CommonTCAOSeanceInvite');
    }

    /**
     * Gets an array of CommonTCAOSeanceInvite objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTCAOSeanceInvite[] List of CommonTCAOSeanceInvite objects
     * @throws PropelException
     */
    public function getCommonTCAOSeanceInvites($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOSeanceInvitesPartial && !$this->isNew();
        if (null === $this->collCommonTCAOSeanceInvites || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOSeanceInvites) {
                // return empty collection
                $this->initCommonTCAOSeanceInvites();
            } else {
                $collCommonTCAOSeanceInvites = CommonTCAOSeanceInviteQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTCAOSeanceInvitesPartial && count($collCommonTCAOSeanceInvites)) {
                      $this->initCommonTCAOSeanceInvites(false);

                      foreach ($collCommonTCAOSeanceInvites as $obj) {
                        if (false == $this->collCommonTCAOSeanceInvites->contains($obj)) {
                          $this->collCommonTCAOSeanceInvites->append($obj);
                        }
                      }

                      $this->collCommonTCAOSeanceInvitesPartial = true;
                    }

                    $collCommonTCAOSeanceInvites->getInternalIterator()->rewind();

                    return $collCommonTCAOSeanceInvites;
                }

                if ($partial && $this->collCommonTCAOSeanceInvites) {
                    foreach ($this->collCommonTCAOSeanceInvites as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTCAOSeanceInvites[] = $obj;
                        }
                    }
                }

                $this->collCommonTCAOSeanceInvites = $collCommonTCAOSeanceInvites;
                $this->collCommonTCAOSeanceInvitesPartial = false;
            }
        }

        return $this->collCommonTCAOSeanceInvites;
    }

    /**
     * Sets a collection of CommonTCAOSeanceInvite objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTCAOSeanceInvites A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonTCAOSeanceInvites(PropelCollection $commonTCAOSeanceInvites, PropelPDO $con = null)
    {
        $commonTCAOSeanceInvitesToDelete = $this->getCommonTCAOSeanceInvites(new Criteria(), $con)->diff($commonTCAOSeanceInvites);


        $this->commonTCAOSeanceInvitesScheduledForDeletion = $commonTCAOSeanceInvitesToDelete;

        foreach ($commonTCAOSeanceInvitesToDelete as $commonTCAOSeanceInviteRemoved) {
            $commonTCAOSeanceInviteRemoved->setCommonAgent(null);
        }

        $this->collCommonTCAOSeanceInvites = null;
        foreach ($commonTCAOSeanceInvites as $commonTCAOSeanceInvite) {
            $this->addCommonTCAOSeanceInvite($commonTCAOSeanceInvite);
        }

        $this->collCommonTCAOSeanceInvites = $commonTCAOSeanceInvites;
        $this->collCommonTCAOSeanceInvitesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTCAOSeanceInvite objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTCAOSeanceInvite objects.
     * @throws PropelException
     */
    public function countCommonTCAOSeanceInvites(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTCAOSeanceInvitesPartial && !$this->isNew();
        if (null === $this->collCommonTCAOSeanceInvites || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTCAOSeanceInvites) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTCAOSeanceInvites());
            }
            $query = CommonTCAOSeanceInviteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonTCAOSeanceInvites);
    }

    /**
     * Method called to associate a CommonTCAOSeanceInvite object to this object
     * through the CommonTCAOSeanceInvite foreign key attribute.
     *
     * @param   CommonTCAOSeanceInvite $l CommonTCAOSeanceInvite
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonTCAOSeanceInvite(CommonTCAOSeanceInvite $l)
    {
        if ($this->collCommonTCAOSeanceInvites === null) {
            $this->initCommonTCAOSeanceInvites();
            $this->collCommonTCAOSeanceInvitesPartial = true;
        }
        if (!in_array($l, $this->collCommonTCAOSeanceInvites->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTCAOSeanceInvite($l);
        }

        return $this;
    }

    /**
     * @param	CommonTCAOSeanceInvite $commonTCAOSeanceInvite The commonTCAOSeanceInvite object to add.
     */
    protected function doAddCommonTCAOSeanceInvite($commonTCAOSeanceInvite)
    {
        $this->collCommonTCAOSeanceInvites[]= $commonTCAOSeanceInvite;
        $commonTCAOSeanceInvite->setCommonAgent($this);
    }

    /**
     * @param	CommonTCAOSeanceInvite $commonTCAOSeanceInvite The commonTCAOSeanceInvite object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonTCAOSeanceInvite($commonTCAOSeanceInvite)
    {
        if ($this->getCommonTCAOSeanceInvites()->contains($commonTCAOSeanceInvite)) {
            $this->collCommonTCAOSeanceInvites->remove($this->collCommonTCAOSeanceInvites->search($commonTCAOSeanceInvite));
            if (null === $this->commonTCAOSeanceInvitesScheduledForDeletion) {
                $this->commonTCAOSeanceInvitesScheduledForDeletion = clone $this->collCommonTCAOSeanceInvites;
                $this->commonTCAOSeanceInvitesScheduledForDeletion->clear();
            }
            $this->commonTCAOSeanceInvitesScheduledForDeletion[]= $commonTCAOSeanceInvite;
            $commonTCAOSeanceInvite->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonTCAOSeanceInvites from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOSeanceInvite[] List of CommonTCAOSeanceInvite objects
     */
    public function getCommonTCAOSeanceInvitesJoinCommonTCAOIntervenantExterne($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOSeanceInviteQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOIntervenantExterne', $join_behavior);

        return $this->getCommonTCAOSeanceInvites($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonTCAOSeanceInvites from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTCAOSeanceInvite[] List of CommonTCAOSeanceInvite objects
     */
    public function getCommonTCAOSeanceInvitesJoinCommonTCAOOrdreDePassage($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTCAOSeanceInviteQuery::create(null, $criteria);
        $query->joinWith('CommonTCAOOrdreDePassage', $join_behavior);

        return $this->getCommonTCAOSeanceInvites($query, $con);
    }

    /**
     * Clears out the collCommonTVisionRmaAgentOrganismes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonAgent The current object (for fluent API support)
     * @see        addCommonTVisionRmaAgentOrganismes()
     */
    public function clearCommonTVisionRmaAgentOrganismes()
    {
        $this->collCommonTVisionRmaAgentOrganismes = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTVisionRmaAgentOrganismesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTVisionRmaAgentOrganismes collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTVisionRmaAgentOrganismes($v = true)
    {
        $this->collCommonTVisionRmaAgentOrganismesPartial = $v;
    }

    /**
     * Initializes the collCommonTVisionRmaAgentOrganismes collection.
     *
     * By default this just sets the collCommonTVisionRmaAgentOrganismes collection to an empty array (like clearcollCommonTVisionRmaAgentOrganismes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTVisionRmaAgentOrganismes($overrideExisting = true)
    {
        if (null !== $this->collCommonTVisionRmaAgentOrganismes && !$overrideExisting) {
            return;
        }
        $this->collCommonTVisionRmaAgentOrganismes = new PropelObjectCollection();
        $this->collCommonTVisionRmaAgentOrganismes->setModel('CommonTVisionRmaAgentOrganisme');
    }

    /**
     * Gets an array of CommonTVisionRmaAgentOrganisme objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonAgent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTVisionRmaAgentOrganisme[] List of CommonTVisionRmaAgentOrganisme objects
     * @throws PropelException
     */
    public function getCommonTVisionRmaAgentOrganismes($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTVisionRmaAgentOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonTVisionRmaAgentOrganismes || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTVisionRmaAgentOrganismes) {
                // return empty collection
                $this->initCommonTVisionRmaAgentOrganismes();
            } else {
                $collCommonTVisionRmaAgentOrganismes = CommonTVisionRmaAgentOrganismeQuery::create(null, $criteria)
                    ->filterByCommonAgent($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTVisionRmaAgentOrganismesPartial && count($collCommonTVisionRmaAgentOrganismes)) {
                      $this->initCommonTVisionRmaAgentOrganismes(false);

                      foreach ($collCommonTVisionRmaAgentOrganismes as $obj) {
                        if (false == $this->collCommonTVisionRmaAgentOrganismes->contains($obj)) {
                          $this->collCommonTVisionRmaAgentOrganismes->append($obj);
                        }
                      }

                      $this->collCommonTVisionRmaAgentOrganismesPartial = true;
                    }

                    $collCommonTVisionRmaAgentOrganismes->getInternalIterator()->rewind();

                    return $collCommonTVisionRmaAgentOrganismes;
                }

                if ($partial && $this->collCommonTVisionRmaAgentOrganismes) {
                    foreach ($this->collCommonTVisionRmaAgentOrganismes as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTVisionRmaAgentOrganismes[] = $obj;
                        }
                    }
                }

                $this->collCommonTVisionRmaAgentOrganismes = $collCommonTVisionRmaAgentOrganismes;
                $this->collCommonTVisionRmaAgentOrganismesPartial = false;
            }
        }

        return $this->collCommonTVisionRmaAgentOrganismes;
    }

    /**
     * Sets a collection of CommonTVisionRmaAgentOrganisme objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTVisionRmaAgentOrganismes A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonAgent The current object (for fluent API support)
     */
    public function setCommonTVisionRmaAgentOrganismes(PropelCollection $commonTVisionRmaAgentOrganismes, PropelPDO $con = null)
    {
        $commonTVisionRmaAgentOrganismesToDelete = $this->getCommonTVisionRmaAgentOrganismes(new Criteria(), $con)->diff($commonTVisionRmaAgentOrganismes);


        $this->commonTVisionRmaAgentOrganismesScheduledForDeletion = $commonTVisionRmaAgentOrganismesToDelete;

        foreach ($commonTVisionRmaAgentOrganismesToDelete as $commonTVisionRmaAgentOrganismeRemoved) {
            $commonTVisionRmaAgentOrganismeRemoved->setCommonAgent(null);
        }

        $this->collCommonTVisionRmaAgentOrganismes = null;
        foreach ($commonTVisionRmaAgentOrganismes as $commonTVisionRmaAgentOrganisme) {
            $this->addCommonTVisionRmaAgentOrganisme($commonTVisionRmaAgentOrganisme);
        }

        $this->collCommonTVisionRmaAgentOrganismes = $commonTVisionRmaAgentOrganismes;
        $this->collCommonTVisionRmaAgentOrganismesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTVisionRmaAgentOrganisme objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTVisionRmaAgentOrganisme objects.
     * @throws PropelException
     */
    public function countCommonTVisionRmaAgentOrganismes(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTVisionRmaAgentOrganismesPartial && !$this->isNew();
        if (null === $this->collCommonTVisionRmaAgentOrganismes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTVisionRmaAgentOrganismes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTVisionRmaAgentOrganismes());
            }
            $query = CommonTVisionRmaAgentOrganismeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonAgent($this)
                ->count($con);
        }

        return count($this->collCommonTVisionRmaAgentOrganismes);
    }

    /**
     * Method called to associate a CommonTVisionRmaAgentOrganisme object to this object
     * through the CommonTVisionRmaAgentOrganisme foreign key attribute.
     *
     * @param   CommonTVisionRmaAgentOrganisme $l CommonTVisionRmaAgentOrganisme
     * @return CommonAgent The current object (for fluent API support)
     */
    public function addCommonTVisionRmaAgentOrganisme(CommonTVisionRmaAgentOrganisme $l)
    {
        if ($this->collCommonTVisionRmaAgentOrganismes === null) {
            $this->initCommonTVisionRmaAgentOrganismes();
            $this->collCommonTVisionRmaAgentOrganismesPartial = true;
        }
        if (!in_array($l, $this->collCommonTVisionRmaAgentOrganismes->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTVisionRmaAgentOrganisme($l);
        }

        return $this;
    }

    /**
     * @param	CommonTVisionRmaAgentOrganisme $commonTVisionRmaAgentOrganisme The commonTVisionRmaAgentOrganisme object to add.
     */
    protected function doAddCommonTVisionRmaAgentOrganisme($commonTVisionRmaAgentOrganisme)
    {
        $this->collCommonTVisionRmaAgentOrganismes[]= $commonTVisionRmaAgentOrganisme;
        $commonTVisionRmaAgentOrganisme->setCommonAgent($this);
    }

    /**
     * @param	CommonTVisionRmaAgentOrganisme $commonTVisionRmaAgentOrganisme The commonTVisionRmaAgentOrganisme object to remove.
     * @return CommonAgent The current object (for fluent API support)
     */
    public function removeCommonTVisionRmaAgentOrganisme($commonTVisionRmaAgentOrganisme)
    {
        if ($this->getCommonTVisionRmaAgentOrganismes()->contains($commonTVisionRmaAgentOrganisme)) {
            $this->collCommonTVisionRmaAgentOrganismes->remove($this->collCommonTVisionRmaAgentOrganismes->search($commonTVisionRmaAgentOrganisme));
            if (null === $this->commonTVisionRmaAgentOrganismesScheduledForDeletion) {
                $this->commonTVisionRmaAgentOrganismesScheduledForDeletion = clone $this->collCommonTVisionRmaAgentOrganismes;
                $this->commonTVisionRmaAgentOrganismesScheduledForDeletion->clear();
            }
            $this->commonTVisionRmaAgentOrganismesScheduledForDeletion[]= clone $commonTVisionRmaAgentOrganisme;
            $commonTVisionRmaAgentOrganisme->setCommonAgent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonAgent is new, it will return
     * an empty collection; or if this CommonAgent has previously
     * been saved, it will retrieve related CommonTVisionRmaAgentOrganismes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonAgent.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTVisionRmaAgentOrganisme[] List of CommonTVisionRmaAgentOrganisme objects
     */
    public function getCommonTVisionRmaAgentOrganismesJoinCommonOrganisme($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTVisionRmaAgentOrganismeQuery::create(null, $criteria);
        $query->joinWith('CommonOrganisme', $join_behavior);

        return $this->getCommonTVisionRmaAgentOrganismes($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->login = null;
        $this->email = null;
        $this->mdp = null;
        $this->certificat = null;
        $this->nom = null;
        $this->prenom = null;
        $this->tentatives_mdp = null;
        $this->organisme = null;
        $this->old_service_id = null;
        $this->recevoir_mail = null;
        $this->elu = null;
        $this->nom_fonction = null;
        $this->num_tel = null;
        $this->num_fax = null;
        $this->type_comm = null;
        $this->adr_postale = null;
        $this->civilite = null;
        $this->alerte_reponse_electronique = null;
        $this->alerte_cloture_consultation = null;
        $this->alerte_reception_message = null;
        $this->alerte_publication_boamp = null;
        $this->alerte_echec_publication_boamp = null;
        $this->alerte_creation_modification_agent = null;
        $this->date_creation = null;
        $this->date_modification = null;
        $this->id_externe = null;
        $this->id_profil_socle_externe = null;
        $this->lieu_execution = null;
        $this->alerte_question_entreprise = null;
        $this->actif = null;
        $this->deleted_at = null;
        $this->codes_nuts = null;
        $this->num_certificat = null;
        $this->alerte_validation_consultation = null;
        $this->alerte_chorus = null;
        $this->password = null;
        $this->code_theme = null;
        $this->date_connexion = null;
        $this->type_hash = null;
        $this->alerte_mes_consultations = null;
        $this->alerte_consultations_mon_entite = null;
        $this->alerte_consultations_des_entites_dependantes = null;
        $this->alerte_consultations_mes_entites_transverses = null;
        $this->technique = null;
        $this->service_id = null;
        $this->date_validation_rgpd = null;
        $this->rgpd_communication_place = null;
        $this->rgpd_enquete = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonAgentServiceMetiers) {
                foreach ($this->collCommonAgentServiceMetiers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->singleCommonHabilitationAgent) {
                $this->singleCommonHabilitationAgent->clearAllReferences($deep);
            }
            if ($this->collCommonAgentTechniqueTokens) {
                foreach ($this->collCommonAgentTechniqueTokens as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonAnnexeFinancieres) {
                foreach ($this->collCommonAnnexeFinancieres as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonConsultationFavoriss) {
                foreach ($this->collCommonConsultationFavoriss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonDossierVolumineuxs) {
                foreach ($this->collCommonDossierVolumineuxs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonEchangeDocs) {
                foreach ($this->collCommonEchangeDocs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonEchangeDocHistoriques) {
                foreach ($this->collCommonEchangeDocHistoriques as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonHabilitationAgentWss) {
                foreach ($this->collCommonHabilitationAgentWss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonInvitePermanentTransverses) {
                foreach ($this->collCommonInvitePermanentTransverses as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonModificationContrats) {
                foreach ($this->collCommonModificationContrats as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOCommissionAgents) {
                foreach ($this->collCommonTCAOCommissionAgents as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOOrdreDuJourIntervenants) {
                foreach ($this->collCommonTCAOOrdreDuJourIntervenants as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOSeanceAgents) {
                foreach ($this->collCommonTCAOSeanceAgents as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTCAOSeanceInvites) {
                foreach ($this->collCommonTCAOSeanceInvites as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTVisionRmaAgentOrganismes) {
                foreach ($this->collCommonTVisionRmaAgentOrganismes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonOrganisme instanceof Persistent) {
              $this->aCommonOrganisme->clearAllReferences($deep);
            }
            if ($this->aCommonService instanceof Persistent) {
              $this->aCommonService->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonAgentServiceMetiers instanceof PropelCollection) {
            $this->collCommonAgentServiceMetiers->clearIterator();
        }
        $this->collCommonAgentServiceMetiers = null;
        if ($this->singleCommonHabilitationAgent instanceof PropelCollection) {
            $this->singleCommonHabilitationAgent->clearIterator();
        }
        $this->singleCommonHabilitationAgent = null;
        if ($this->collCommonAgentTechniqueTokens instanceof PropelCollection) {
            $this->collCommonAgentTechniqueTokens->clearIterator();
        }
        $this->collCommonAgentTechniqueTokens = null;
        if ($this->collCommonAnnexeFinancieres instanceof PropelCollection) {
            $this->collCommonAnnexeFinancieres->clearIterator();
        }
        $this->collCommonAnnexeFinancieres = null;
        if ($this->collCommonConsultationFavoriss instanceof PropelCollection) {
            $this->collCommonConsultationFavoriss->clearIterator();
        }
        $this->collCommonConsultationFavoriss = null;
        if ($this->collCommonDossierVolumineuxs instanceof PropelCollection) {
            $this->collCommonDossierVolumineuxs->clearIterator();
        }
        $this->collCommonDossierVolumineuxs = null;
        if ($this->collCommonEchangeDocs instanceof PropelCollection) {
            $this->collCommonEchangeDocs->clearIterator();
        }
        $this->collCommonEchangeDocs = null;
        if ($this->collCommonEchangeDocHistoriques instanceof PropelCollection) {
            $this->collCommonEchangeDocHistoriques->clearIterator();
        }
        $this->collCommonEchangeDocHistoriques = null;
        if ($this->collCommonHabilitationAgentWss instanceof PropelCollection) {
            $this->collCommonHabilitationAgentWss->clearIterator();
        }
        $this->collCommonHabilitationAgentWss = null;
        if ($this->collCommonInvitePermanentTransverses instanceof PropelCollection) {
            $this->collCommonInvitePermanentTransverses->clearIterator();
        }
        $this->collCommonInvitePermanentTransverses = null;
        if ($this->collCommonModificationContrats instanceof PropelCollection) {
            $this->collCommonModificationContrats->clearIterator();
        }
        $this->collCommonModificationContrats = null;
        if ($this->collCommonTCAOCommissionAgents instanceof PropelCollection) {
            $this->collCommonTCAOCommissionAgents->clearIterator();
        }
        $this->collCommonTCAOCommissionAgents = null;
        if ($this->collCommonTCAOOrdreDuJourIntervenants instanceof PropelCollection) {
            $this->collCommonTCAOOrdreDuJourIntervenants->clearIterator();
        }
        $this->collCommonTCAOOrdreDuJourIntervenants = null;
        if ($this->collCommonTCAOSeanceAgents instanceof PropelCollection) {
            $this->collCommonTCAOSeanceAgents->clearIterator();
        }
        $this->collCommonTCAOSeanceAgents = null;
        if ($this->collCommonTCAOSeanceInvites instanceof PropelCollection) {
            $this->collCommonTCAOSeanceInvites->clearIterator();
        }
        $this->collCommonTCAOSeanceInvites = null;
        if ($this->collCommonTVisionRmaAgentOrganismes instanceof PropelCollection) {
            $this->collCommonTVisionRmaAgentOrganismes->clearIterator();
        }
        $this->collCommonTVisionRmaAgentOrganismes = null;
        $this->aCommonOrganisme = null;
        $this->aCommonService = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonAgentPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
