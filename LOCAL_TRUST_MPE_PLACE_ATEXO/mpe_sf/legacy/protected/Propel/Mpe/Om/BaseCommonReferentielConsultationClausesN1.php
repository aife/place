<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationClausesN1;
use Application\Propel\Mpe\CommonConsultationClausesN1Query;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN1;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN1Peer;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN1Query;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN2;
use Application\Propel\Mpe\CommonReferentielConsultationClausesN2Query;

/**
 * Base class that represents a row from the 'referentiel_consultation_clauses_n1' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonReferentielConsultationClausesN1 extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonReferentielConsultationClausesN1Peer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonReferentielConsultationClausesN1Peer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the label field.
     * @var        string
     */
    protected $label;

    /**
     * The value for the slug field.
     * @var        string
     */
    protected $slug;

    /**
     * @var        PropelObjectCollection|CommonConsultationClausesN1[] Collection to store aggregation of CommonConsultationClausesN1 objects.
     */
    protected $collCommonConsultationClausesN1s;
    protected $collCommonConsultationClausesN1sPartial;

    /**
     * @var        PropelObjectCollection|CommonReferentielConsultationClausesN2[] Collection to store aggregation of CommonReferentielConsultationClausesN2 objects.
     */
    protected $collCommonReferentielConsultationClausesN2s;
    protected $collCommonReferentielConsultationClausesN2sPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationClausesN1sScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonReferentielConsultationClausesN2sScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [label] column value.
     *
     * @return string
     */
    public function getLabel()
    {

        return $this->label;
    }

    /**
     * Get the [slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {

        return $this->slug;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonReferentielConsultationClausesN1 The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonReferentielConsultationClausesN1Peer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [label] column.
     *
     * @param string $v new value
     * @return CommonReferentielConsultationClausesN1 The current object (for fluent API support)
     */
    public function setLabel($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->label !== $v) {
            $this->label = $v;
            $this->modifiedColumns[] = CommonReferentielConsultationClausesN1Peer::LABEL;
        }


        return $this;
    } // setLabel()

    /**
     * Set the value of [slug] column.
     *
     * @param string $v new value
     * @return CommonReferentielConsultationClausesN1 The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->slug !== $v) {
            $this->slug = $v;
            $this->modifiedColumns[] = CommonReferentielConsultationClausesN1Peer::SLUG;
        }


        return $this;
    } // setSlug()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->label = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->slug = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 3; // 3 = CommonReferentielConsultationClausesN1Peer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonReferentielConsultationClausesN1 object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonReferentielConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonReferentielConsultationClausesN1Peer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCommonConsultationClausesN1s = null;

            $this->collCommonReferentielConsultationClausesN2s = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonReferentielConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonReferentielConsultationClausesN1Query::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonReferentielConsultationClausesN1Peer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonReferentielConsultationClausesN1Peer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonConsultationClausesN1sScheduledForDeletion !== null) {
                if (!$this->commonConsultationClausesN1sScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationClausesN1sScheduledForDeletion as $commonConsultationClausesN1) {
                        // need to save related object because we set the relation to null
                        $commonConsultationClausesN1->save($con);
                    }
                    $this->commonConsultationClausesN1sScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultationClausesN1s !== null) {
                foreach ($this->collCommonConsultationClausesN1s as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonReferentielConsultationClausesN2sScheduledForDeletion !== null) {
                if (!$this->commonReferentielConsultationClausesN2sScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonReferentielConsultationClausesN2sScheduledForDeletion as $commonReferentielConsultationClausesN2) {
                        // need to save related object because we set the relation to null
                        $commonReferentielConsultationClausesN2->save($con);
                    }
                    $this->commonReferentielConsultationClausesN2sScheduledForDeletion = null;
                }
            }

            if ($this->collCommonReferentielConsultationClausesN2s !== null) {
                foreach ($this->collCommonReferentielConsultationClausesN2s as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonReferentielConsultationClausesN1Peer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonReferentielConsultationClausesN1Peer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonReferentielConsultationClausesN1Peer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonReferentielConsultationClausesN1Peer::LABEL)) {
            $modifiedColumns[':p' . $index++]  = '`label`';
        }
        if ($this->isColumnModified(CommonReferentielConsultationClausesN1Peer::SLUG)) {
            $modifiedColumns[':p' . $index++]  = '`slug`';
        }

        $sql = sprintf(
            'INSERT INTO `referentiel_consultation_clauses_n1` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`label`':
                        $stmt->bindValue($identifier, $this->label, PDO::PARAM_STR);
                        break;
                    case '`slug`':
                        $stmt->bindValue($identifier, $this->slug, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonReferentielConsultationClausesN1Peer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonConsultationClausesN1s !== null) {
                    foreach ($this->collCommonConsultationClausesN1s as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonReferentielConsultationClausesN2s !== null) {
                    foreach ($this->collCommonReferentielConsultationClausesN2s as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonReferentielConsultationClausesN1Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getLabel();
                break;
            case 2:
                return $this->getSlug();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonReferentielConsultationClausesN1'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonReferentielConsultationClausesN1'][$this->getPrimaryKey()] = true;
        $keys = CommonReferentielConsultationClausesN1Peer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getLabel(),
            $keys[2] => $this->getSlug(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->collCommonConsultationClausesN1s) {
                $result['CommonConsultationClausesN1s'] = $this->collCommonConsultationClausesN1s->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonReferentielConsultationClausesN2s) {
                $result['CommonReferentielConsultationClausesN2s'] = $this->collCommonReferentielConsultationClausesN2s->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonReferentielConsultationClausesN1Peer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setLabel($value);
                break;
            case 2:
                $this->setSlug($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonReferentielConsultationClausesN1Peer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setLabel($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSlug($arr[$keys[2]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonReferentielConsultationClausesN1Peer::DATABASE_NAME);

        if ($this->isColumnModified(CommonReferentielConsultationClausesN1Peer::ID)) $criteria->add(CommonReferentielConsultationClausesN1Peer::ID, $this->id);
        if ($this->isColumnModified(CommonReferentielConsultationClausesN1Peer::LABEL)) $criteria->add(CommonReferentielConsultationClausesN1Peer::LABEL, $this->label);
        if ($this->isColumnModified(CommonReferentielConsultationClausesN1Peer::SLUG)) $criteria->add(CommonReferentielConsultationClausesN1Peer::SLUG, $this->slug);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonReferentielConsultationClausesN1Peer::DATABASE_NAME);
        $criteria->add(CommonReferentielConsultationClausesN1Peer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonReferentielConsultationClausesN1 (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setLabel($this->getLabel());
        $copyObj->setSlug($this->getSlug());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonConsultationClausesN1s() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultationClausesN1($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonReferentielConsultationClausesN2s() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonReferentielConsultationClausesN2($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonReferentielConsultationClausesN1 Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonReferentielConsultationClausesN1Peer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonReferentielConsultationClausesN1Peer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonConsultationClausesN1' == $relationName) {
            $this->initCommonConsultationClausesN1s();
        }
        if ('CommonReferentielConsultationClausesN2' == $relationName) {
            $this->initCommonReferentielConsultationClausesN2s();
        }
    }

    /**
     * Clears out the collCommonConsultationClausesN1s collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonReferentielConsultationClausesN1 The current object (for fluent API support)
     * @see        addCommonConsultationClausesN1s()
     */
    public function clearCommonConsultationClausesN1s()
    {
        $this->collCommonConsultationClausesN1s = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationClausesN1sPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultationClausesN1s collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultationClausesN1s($v = true)
    {
        $this->collCommonConsultationClausesN1sPartial = $v;
    }

    /**
     * Initializes the collCommonConsultationClausesN1s collection.
     *
     * By default this just sets the collCommonConsultationClausesN1s collection to an empty array (like clearcollCommonConsultationClausesN1s());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultationClausesN1s($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultationClausesN1s && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultationClausesN1s = new PropelObjectCollection();
        $this->collCommonConsultationClausesN1s->setModel('CommonConsultationClausesN1');
    }

    /**
     * Gets an array of CommonConsultationClausesN1 objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonReferentielConsultationClausesN1 is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultationClausesN1[] List of CommonConsultationClausesN1 objects
     * @throws PropelException
     */
    public function getCommonConsultationClausesN1s($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationClausesN1sPartial && !$this->isNew();
        if (null === $this->collCommonConsultationClausesN1s || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationClausesN1s) {
                // return empty collection
                $this->initCommonConsultationClausesN1s();
            } else {
                $collCommonConsultationClausesN1s = CommonConsultationClausesN1Query::create(null, $criteria)
                    ->filterByCommonReferentielConsultationClausesN1($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationClausesN1sPartial && count($collCommonConsultationClausesN1s)) {
                      $this->initCommonConsultationClausesN1s(false);

                      foreach ($collCommonConsultationClausesN1s as $obj) {
                        if (false == $this->collCommonConsultationClausesN1s->contains($obj)) {
                          $this->collCommonConsultationClausesN1s->append($obj);
                        }
                      }

                      $this->collCommonConsultationClausesN1sPartial = true;
                    }

                    $collCommonConsultationClausesN1s->getInternalIterator()->rewind();

                    return $collCommonConsultationClausesN1s;
                }

                if ($partial && $this->collCommonConsultationClausesN1s) {
                    foreach ($this->collCommonConsultationClausesN1s as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultationClausesN1s[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultationClausesN1s = $collCommonConsultationClausesN1s;
                $this->collCommonConsultationClausesN1sPartial = false;
            }
        }

        return $this->collCommonConsultationClausesN1s;
    }

    /**
     * Sets a collection of CommonConsultationClausesN1 objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultationClausesN1s A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonReferentielConsultationClausesN1 The current object (for fluent API support)
     */
    public function setCommonConsultationClausesN1s(PropelCollection $commonConsultationClausesN1s, PropelPDO $con = null)
    {
        $commonConsultationClausesN1sToDelete = $this->getCommonConsultationClausesN1s(new Criteria(), $con)->diff($commonConsultationClausesN1s);


        $this->commonConsultationClausesN1sScheduledForDeletion = $commonConsultationClausesN1sToDelete;

        foreach ($commonConsultationClausesN1sToDelete as $commonConsultationClausesN1Removed) {
            $commonConsultationClausesN1Removed->setCommonReferentielConsultationClausesN1(null);
        }

        $this->collCommonConsultationClausesN1s = null;
        foreach ($commonConsultationClausesN1s as $commonConsultationClausesN1) {
            $this->addCommonConsultationClausesN1($commonConsultationClausesN1);
        }

        $this->collCommonConsultationClausesN1s = $commonConsultationClausesN1s;
        $this->collCommonConsultationClausesN1sPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultationClausesN1 objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultationClausesN1 objects.
     * @throws PropelException
     */
    public function countCommonConsultationClausesN1s(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationClausesN1sPartial && !$this->isNew();
        if (null === $this->collCommonConsultationClausesN1s || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationClausesN1s) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultationClausesN1s());
            }
            $query = CommonConsultationClausesN1Query::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonReferentielConsultationClausesN1($this)
                ->count($con);
        }

        return count($this->collCommonConsultationClausesN1s);
    }

    /**
     * Method called to associate a CommonConsultationClausesN1 object to this object
     * through the CommonConsultationClausesN1 foreign key attribute.
     *
     * @param   CommonConsultationClausesN1 $l CommonConsultationClausesN1
     * @return CommonReferentielConsultationClausesN1 The current object (for fluent API support)
     */
    public function addCommonConsultationClausesN1(CommonConsultationClausesN1 $l)
    {
        if ($this->collCommonConsultationClausesN1s === null) {
            $this->initCommonConsultationClausesN1s();
            $this->collCommonConsultationClausesN1sPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultationClausesN1s->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultationClausesN1($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultationClausesN1 $commonConsultationClausesN1 The commonConsultationClausesN1 object to add.
     */
    protected function doAddCommonConsultationClausesN1($commonConsultationClausesN1)
    {
        $this->collCommonConsultationClausesN1s[]= $commonConsultationClausesN1;
        $commonConsultationClausesN1->setCommonReferentielConsultationClausesN1($this);
    }

    /**
     * @param	CommonConsultationClausesN1 $commonConsultationClausesN1 The commonConsultationClausesN1 object to remove.
     * @return CommonReferentielConsultationClausesN1 The current object (for fluent API support)
     */
    public function removeCommonConsultationClausesN1($commonConsultationClausesN1)
    {
        if ($this->getCommonConsultationClausesN1s()->contains($commonConsultationClausesN1)) {
            $this->collCommonConsultationClausesN1s->remove($this->collCommonConsultationClausesN1s->search($commonConsultationClausesN1));
            if (null === $this->commonConsultationClausesN1sScheduledForDeletion) {
                $this->commonConsultationClausesN1sScheduledForDeletion = clone $this->collCommonConsultationClausesN1s;
                $this->commonConsultationClausesN1sScheduledForDeletion->clear();
            }
            $this->commonConsultationClausesN1sScheduledForDeletion[]= clone $commonConsultationClausesN1;
            $commonConsultationClausesN1->setCommonReferentielConsultationClausesN1(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonReferentielConsultationClausesN1 is new, it will return
     * an empty collection; or if this CommonReferentielConsultationClausesN1 has previously
     * been saved, it will retrieve related CommonConsultationClausesN1s from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonReferentielConsultationClausesN1.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultationClausesN1[] List of CommonConsultationClausesN1 objects
     */
    public function getCommonConsultationClausesN1sJoinCommonTContratTitulaire($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationClausesN1Query::create(null, $criteria);
        $query->joinWith('CommonTContratTitulaire', $join_behavior);

        return $this->getCommonConsultationClausesN1s($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonReferentielConsultationClausesN1 is new, it will return
     * an empty collection; or if this CommonReferentielConsultationClausesN1 has previously
     * been saved, it will retrieve related CommonConsultationClausesN1s from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonReferentielConsultationClausesN1.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultationClausesN1[] List of CommonConsultationClausesN1 objects
     */
    public function getCommonConsultationClausesN1sJoinCommonConsultation($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationClausesN1Query::create(null, $criteria);
        $query->joinWith('CommonConsultation', $join_behavior);

        return $this->getCommonConsultationClausesN1s($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonReferentielConsultationClausesN1 is new, it will return
     * an empty collection; or if this CommonReferentielConsultationClausesN1 has previously
     * been saved, it will retrieve related CommonConsultationClausesN1s from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonReferentielConsultationClausesN1.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonConsultationClausesN1[] List of CommonConsultationClausesN1 objects
     */
    public function getCommonConsultationClausesN1sJoinCommonCategorieLot($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonConsultationClausesN1Query::create(null, $criteria);
        $query->joinWith('CommonCategorieLot', $join_behavior);

        return $this->getCommonConsultationClausesN1s($query, $con);
    }

    /**
     * Clears out the collCommonReferentielConsultationClausesN2s collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonReferentielConsultationClausesN1 The current object (for fluent API support)
     * @see        addCommonReferentielConsultationClausesN2s()
     */
    public function clearCommonReferentielConsultationClausesN2s()
    {
        $this->collCommonReferentielConsultationClausesN2s = null; // important to set this to null since that means it is uninitialized
        $this->collCommonReferentielConsultationClausesN2sPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonReferentielConsultationClausesN2s collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonReferentielConsultationClausesN2s($v = true)
    {
        $this->collCommonReferentielConsultationClausesN2sPartial = $v;
    }

    /**
     * Initializes the collCommonReferentielConsultationClausesN2s collection.
     *
     * By default this just sets the collCommonReferentielConsultationClausesN2s collection to an empty array (like clearcollCommonReferentielConsultationClausesN2s());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonReferentielConsultationClausesN2s($overrideExisting = true)
    {
        if (null !== $this->collCommonReferentielConsultationClausesN2s && !$overrideExisting) {
            return;
        }
        $this->collCommonReferentielConsultationClausesN2s = new PropelObjectCollection();
        $this->collCommonReferentielConsultationClausesN2s->setModel('CommonReferentielConsultationClausesN2');
    }

    /**
     * Gets an array of CommonReferentielConsultationClausesN2 objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonReferentielConsultationClausesN1 is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonReferentielConsultationClausesN2[] List of CommonReferentielConsultationClausesN2 objects
     * @throws PropelException
     */
    public function getCommonReferentielConsultationClausesN2s($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonReferentielConsultationClausesN2sPartial && !$this->isNew();
        if (null === $this->collCommonReferentielConsultationClausesN2s || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonReferentielConsultationClausesN2s) {
                // return empty collection
                $this->initCommonReferentielConsultationClausesN2s();
            } else {
                $collCommonReferentielConsultationClausesN2s = CommonReferentielConsultationClausesN2Query::create(null, $criteria)
                    ->filterByCommonReferentielConsultationClausesN1($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonReferentielConsultationClausesN2sPartial && count($collCommonReferentielConsultationClausesN2s)) {
                      $this->initCommonReferentielConsultationClausesN2s(false);

                      foreach ($collCommonReferentielConsultationClausesN2s as $obj) {
                        if (false == $this->collCommonReferentielConsultationClausesN2s->contains($obj)) {
                          $this->collCommonReferentielConsultationClausesN2s->append($obj);
                        }
                      }

                      $this->collCommonReferentielConsultationClausesN2sPartial = true;
                    }

                    $collCommonReferentielConsultationClausesN2s->getInternalIterator()->rewind();

                    return $collCommonReferentielConsultationClausesN2s;
                }

                if ($partial && $this->collCommonReferentielConsultationClausesN2s) {
                    foreach ($this->collCommonReferentielConsultationClausesN2s as $obj) {
                        if ($obj->isNew()) {
                            $collCommonReferentielConsultationClausesN2s[] = $obj;
                        }
                    }
                }

                $this->collCommonReferentielConsultationClausesN2s = $collCommonReferentielConsultationClausesN2s;
                $this->collCommonReferentielConsultationClausesN2sPartial = false;
            }
        }

        return $this->collCommonReferentielConsultationClausesN2s;
    }

    /**
     * Sets a collection of CommonReferentielConsultationClausesN2 objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonReferentielConsultationClausesN2s A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonReferentielConsultationClausesN1 The current object (for fluent API support)
     */
    public function setCommonReferentielConsultationClausesN2s(PropelCollection $commonReferentielConsultationClausesN2s, PropelPDO $con = null)
    {
        $commonReferentielConsultationClausesN2sToDelete = $this->getCommonReferentielConsultationClausesN2s(new Criteria(), $con)->diff($commonReferentielConsultationClausesN2s);


        $this->commonReferentielConsultationClausesN2sScheduledForDeletion = $commonReferentielConsultationClausesN2sToDelete;

        foreach ($commonReferentielConsultationClausesN2sToDelete as $commonReferentielConsultationClausesN2Removed) {
            $commonReferentielConsultationClausesN2Removed->setCommonReferentielConsultationClausesN1(null);
        }

        $this->collCommonReferentielConsultationClausesN2s = null;
        foreach ($commonReferentielConsultationClausesN2s as $commonReferentielConsultationClausesN2) {
            $this->addCommonReferentielConsultationClausesN2($commonReferentielConsultationClausesN2);
        }

        $this->collCommonReferentielConsultationClausesN2s = $commonReferentielConsultationClausesN2s;
        $this->collCommonReferentielConsultationClausesN2sPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonReferentielConsultationClausesN2 objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonReferentielConsultationClausesN2 objects.
     * @throws PropelException
     */
    public function countCommonReferentielConsultationClausesN2s(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonReferentielConsultationClausesN2sPartial && !$this->isNew();
        if (null === $this->collCommonReferentielConsultationClausesN2s || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonReferentielConsultationClausesN2s) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonReferentielConsultationClausesN2s());
            }
            $query = CommonReferentielConsultationClausesN2Query::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonReferentielConsultationClausesN1($this)
                ->count($con);
        }

        return count($this->collCommonReferentielConsultationClausesN2s);
    }

    /**
     * Method called to associate a CommonReferentielConsultationClausesN2 object to this object
     * through the CommonReferentielConsultationClausesN2 foreign key attribute.
     *
     * @param   CommonReferentielConsultationClausesN2 $l CommonReferentielConsultationClausesN2
     * @return CommonReferentielConsultationClausesN1 The current object (for fluent API support)
     */
    public function addCommonReferentielConsultationClausesN2(CommonReferentielConsultationClausesN2 $l)
    {
        if ($this->collCommonReferentielConsultationClausesN2s === null) {
            $this->initCommonReferentielConsultationClausesN2s();
            $this->collCommonReferentielConsultationClausesN2sPartial = true;
        }
        if (!in_array($l, $this->collCommonReferentielConsultationClausesN2s->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonReferentielConsultationClausesN2($l);
        }

        return $this;
    }

    /**
     * @param	CommonReferentielConsultationClausesN2 $commonReferentielConsultationClausesN2 The commonReferentielConsultationClausesN2 object to add.
     */
    protected function doAddCommonReferentielConsultationClausesN2($commonReferentielConsultationClausesN2)
    {
        $this->collCommonReferentielConsultationClausesN2s[]= $commonReferentielConsultationClausesN2;
        $commonReferentielConsultationClausesN2->setCommonReferentielConsultationClausesN1($this);
    }

    /**
     * @param	CommonReferentielConsultationClausesN2 $commonReferentielConsultationClausesN2 The commonReferentielConsultationClausesN2 object to remove.
     * @return CommonReferentielConsultationClausesN1 The current object (for fluent API support)
     */
    public function removeCommonReferentielConsultationClausesN2($commonReferentielConsultationClausesN2)
    {
        if ($this->getCommonReferentielConsultationClausesN2s()->contains($commonReferentielConsultationClausesN2)) {
            $this->collCommonReferentielConsultationClausesN2s->remove($this->collCommonReferentielConsultationClausesN2s->search($commonReferentielConsultationClausesN2));
            if (null === $this->commonReferentielConsultationClausesN2sScheduledForDeletion) {
                $this->commonReferentielConsultationClausesN2sScheduledForDeletion = clone $this->collCommonReferentielConsultationClausesN2s;
                $this->commonReferentielConsultationClausesN2sScheduledForDeletion->clear();
            }
            $this->commonReferentielConsultationClausesN2sScheduledForDeletion[]= clone $commonReferentielConsultationClausesN2;
            $commonReferentielConsultationClausesN2->setCommonReferentielConsultationClausesN1(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->label = null;
        $this->slug = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonConsultationClausesN1s) {
                foreach ($this->collCommonConsultationClausesN1s as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonReferentielConsultationClausesN2s) {
                foreach ($this->collCommonReferentielConsultationClausesN2s as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonConsultationClausesN1s instanceof PropelCollection) {
            $this->collCommonConsultationClausesN1s->clearIterator();
        }
        $this->collCommonConsultationClausesN1s = null;
        if ($this->collCommonReferentielConsultationClausesN2s instanceof PropelCollection) {
            $this->collCommonReferentielConsultationClausesN2s->clearIterator();
        }
        $this->collCommonReferentielConsultationClausesN2s = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonReferentielConsultationClausesN1Peer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
