<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonHabilitationTypeProcedure;
use Application\Propel\Mpe\CommonHabilitationTypeProcedurePeer;
use Application\Propel\Mpe\CommonHabilitationTypeProcedureQuery;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonReferentielHabilitation;
use Application\Propel\Mpe\CommonTypeProcedure;

/**
 * Base class that represents a query for the 'habilitation_type_procedure' table.
 *
 *
 *
 * @method CommonHabilitationTypeProcedureQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonHabilitationTypeProcedureQuery orderByHabilitationId($order = Criteria::ASC) Order by the habilitation_id column
 * @method CommonHabilitationTypeProcedureQuery orderByAgentId($order = Criteria::ASC) Order by the agent_id column
 * @method CommonHabilitationTypeProcedureQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonHabilitationTypeProcedureQuery orderByTypeprocedure($order = Criteria::ASC) Order by the typeProcedure column
 *
 * @method CommonHabilitationTypeProcedureQuery groupById() Group by the id column
 * @method CommonHabilitationTypeProcedureQuery groupByHabilitationId() Group by the habilitation_id column
 * @method CommonHabilitationTypeProcedureQuery groupByAgentId() Group by the agent_id column
 * @method CommonHabilitationTypeProcedureQuery groupByOrganisme() Group by the organisme column
 * @method CommonHabilitationTypeProcedureQuery groupByTypeprocedure() Group by the typeProcedure column
 *
 * @method CommonHabilitationTypeProcedureQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonHabilitationTypeProcedureQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonHabilitationTypeProcedureQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonHabilitationTypeProcedureQuery leftJoinCommonAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAgent relation
 * @method CommonHabilitationTypeProcedureQuery rightJoinCommonAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAgent relation
 * @method CommonHabilitationTypeProcedureQuery innerJoinCommonAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAgent relation
 *
 * @method CommonHabilitationTypeProcedureQuery leftJoinCommonReferentielHabilitation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonReferentielHabilitation relation
 * @method CommonHabilitationTypeProcedureQuery rightJoinCommonReferentielHabilitation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonReferentielHabilitation relation
 * @method CommonHabilitationTypeProcedureQuery innerJoinCommonReferentielHabilitation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonReferentielHabilitation relation
 *
 * @method CommonHabilitationTypeProcedureQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonHabilitationTypeProcedureQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonHabilitationTypeProcedureQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonHabilitationTypeProcedureQuery leftJoinCommonTypeProcedure($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTypeProcedure relation
 * @method CommonHabilitationTypeProcedureQuery rightJoinCommonTypeProcedure($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTypeProcedure relation
 * @method CommonHabilitationTypeProcedureQuery innerJoinCommonTypeProcedure($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTypeProcedure relation
 *
 * @method CommonHabilitationTypeProcedure findOne(PropelPDO $con = null) Return the first CommonHabilitationTypeProcedure matching the query
 * @method CommonHabilitationTypeProcedure findOneOrCreate(PropelPDO $con = null) Return the first CommonHabilitationTypeProcedure matching the query, or a new CommonHabilitationTypeProcedure object populated from the query conditions when no match is found
 *
 * @method CommonHabilitationTypeProcedure findOneByHabilitationId(int $habilitation_id) Return the first CommonHabilitationTypeProcedure filtered by the habilitation_id column
 * @method CommonHabilitationTypeProcedure findOneByAgentId(int $agent_id) Return the first CommonHabilitationTypeProcedure filtered by the agent_id column
 * @method CommonHabilitationTypeProcedure findOneByOrganisme(int $organisme) Return the first CommonHabilitationTypeProcedure filtered by the organisme column
 * @method CommonHabilitationTypeProcedure findOneByTypeprocedure(int $typeProcedure) Return the first CommonHabilitationTypeProcedure filtered by the typeProcedure column
 *
 * @method array findById(int $id) Return CommonHabilitationTypeProcedure objects filtered by the id column
 * @method array findByHabilitationId(int $habilitation_id) Return CommonHabilitationTypeProcedure objects filtered by the habilitation_id column
 * @method array findByAgentId(int $agent_id) Return CommonHabilitationTypeProcedure objects filtered by the agent_id column
 * @method array findByOrganisme(int $organisme) Return CommonHabilitationTypeProcedure objects filtered by the organisme column
 * @method array findByTypeprocedure(int $typeProcedure) Return CommonHabilitationTypeProcedure objects filtered by the typeProcedure column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonHabilitationTypeProcedureQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonHabilitationTypeProcedureQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonHabilitationTypeProcedure', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonHabilitationTypeProcedureQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonHabilitationTypeProcedureQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonHabilitationTypeProcedureQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonHabilitationTypeProcedureQuery) {
            return $criteria;
        }
        $query = new CommonHabilitationTypeProcedureQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonHabilitationTypeProcedure|CommonHabilitationTypeProcedure[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonHabilitationTypeProcedurePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonHabilitationTypeProcedurePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonHabilitationTypeProcedure A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonHabilitationTypeProcedure A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `habilitation_id`, `agent_id`, `organisme`, `typeProcedure` FROM `habilitation_type_procedure` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonHabilitationTypeProcedure();
            $obj->hydrate($row);
            CommonHabilitationTypeProcedurePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonHabilitationTypeProcedure|CommonHabilitationTypeProcedure[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonHabilitationTypeProcedure[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the habilitation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByHabilitationId(1234); // WHERE habilitation_id = 1234
     * $query->filterByHabilitationId(array(12, 34)); // WHERE habilitation_id IN (12, 34)
     * $query->filterByHabilitationId(array('min' => 12)); // WHERE habilitation_id >= 12
     * $query->filterByHabilitationId(array('max' => 12)); // WHERE habilitation_id <= 12
     * </code>
     *
     * @see       filterByCommonReferentielHabilitation()
     *
     * @param     mixed $habilitationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function filterByHabilitationId($habilitationId = null, $comparison = null)
    {
        if (is_array($habilitationId)) {
            $useMinMax = false;
            if (isset($habilitationId['min'])) {
                $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::HABILITATION_ID, $habilitationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($habilitationId['max'])) {
                $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::HABILITATION_ID, $habilitationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::HABILITATION_ID, $habilitationId, $comparison);
    }

    /**
     * Filter the query on the agent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAgentId(1234); // WHERE agent_id = 1234
     * $query->filterByAgentId(array(12, 34)); // WHERE agent_id IN (12, 34)
     * $query->filterByAgentId(array('min' => 12)); // WHERE agent_id >= 12
     * $query->filterByAgentId(array('max' => 12)); // WHERE agent_id <= 12
     * </code>
     *
     * @see       filterByCommonAgent()
     *
     * @param     mixed $agentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function filterByAgentId($agentId = null, $comparison = null)
    {
        if (is_array($agentId)) {
            $useMinMax = false;
            if (isset($agentId['min'])) {
                $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::AGENT_ID, $agentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($agentId['max'])) {
                $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::AGENT_ID, $agentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::AGENT_ID, $agentId, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme(1234); // WHERE organisme = 1234
     * $query->filterByOrganisme(array(12, 34)); // WHERE organisme IN (12, 34)
     * $query->filterByOrganisme(array('min' => 12)); // WHERE organisme >= 12
     * $query->filterByOrganisme(array('max' => 12)); // WHERE organisme <= 12
     * </code>
     *
     * @see       filterByCommonOrganisme()
     *
     * @param     mixed $organisme The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (is_array($organisme)) {
            $useMinMax = false;
            if (isset($organisme['min'])) {
                $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::ORGANISME, $organisme['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($organisme['max'])) {
                $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::ORGANISME, $organisme['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the typeProcedure column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeprocedure(1234); // WHERE typeProcedure = 1234
     * $query->filterByTypeprocedure(array(12, 34)); // WHERE typeProcedure IN (12, 34)
     * $query->filterByTypeprocedure(array('min' => 12)); // WHERE typeProcedure >= 12
     * $query->filterByTypeprocedure(array('max' => 12)); // WHERE typeProcedure <= 12
     * </code>
     *
     * @see       filterByCommonTypeProcedure()
     *
     * @param     mixed $typeprocedure The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function filterByTypeprocedure($typeprocedure = null, $comparison = null)
    {
        if (is_array($typeprocedure)) {
            $useMinMax = false;
            if (isset($typeprocedure['min'])) {
                $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::TYPEPROCEDURE, $typeprocedure['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeprocedure['max'])) {
                $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::TYPEPROCEDURE, $typeprocedure['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::TYPEPROCEDURE, $typeprocedure, $comparison);
    }

    /**
     * Filter the query by a related CommonAgent object
     *
     * @param   CommonAgent|PropelObjectCollection $commonAgent The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAgent($commonAgent, $comparison = null)
    {
        if ($commonAgent instanceof CommonAgent) {
            return $this
                ->addUsingAlias(CommonHabilitationTypeProcedurePeer::AGENT_ID, $commonAgent->getId(), $comparison);
        } elseif ($commonAgent instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonHabilitationTypeProcedurePeer::AGENT_ID, $commonAgent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonAgent() only accepts arguments of type CommonAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function joinCommonAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAgent');
        }

        return $this;
    }

    /**
     * Use the CommonAgent relation CommonAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAgent', '\Application\Propel\Mpe\CommonAgentQuery');
    }

    /**
     * Filter the query by a related CommonReferentielHabilitation object
     *
     * @param   CommonReferentielHabilitation|PropelObjectCollection $commonReferentielHabilitation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonReferentielHabilitation($commonReferentielHabilitation, $comparison = null)
    {
        if ($commonReferentielHabilitation instanceof CommonReferentielHabilitation) {
            return $this
                ->addUsingAlias(CommonHabilitationTypeProcedurePeer::HABILITATION_ID, $commonReferentielHabilitation->getId(), $comparison);
        } elseif ($commonReferentielHabilitation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonHabilitationTypeProcedurePeer::HABILITATION_ID, $commonReferentielHabilitation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonReferentielHabilitation() only accepts arguments of type CommonReferentielHabilitation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonReferentielHabilitation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function joinCommonReferentielHabilitation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonReferentielHabilitation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonReferentielHabilitation');
        }

        return $this;
    }

    /**
     * Use the CommonReferentielHabilitation relation CommonReferentielHabilitation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonReferentielHabilitationQuery A secondary query class using the current class as primary query
     */
    public function useCommonReferentielHabilitationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonReferentielHabilitation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonReferentielHabilitation', '\Application\Propel\Mpe\CommonReferentielHabilitationQuery');
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonHabilitationTypeProcedurePeer::ORGANISME, $commonOrganisme->getId(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonHabilitationTypeProcedurePeer::ORGANISME, $commonOrganisme->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Filter the query by a related CommonTypeProcedure object
     *
     * @param   CommonTypeProcedure|PropelObjectCollection $commonTypeProcedure The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTypeProcedure($commonTypeProcedure, $comparison = null)
    {
        if ($commonTypeProcedure instanceof CommonTypeProcedure) {
            return $this
                ->addUsingAlias(CommonHabilitationTypeProcedurePeer::TYPEPROCEDURE, $commonTypeProcedure->getIdTypeProcedure(), $comparison);
        } elseif ($commonTypeProcedure instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonHabilitationTypeProcedurePeer::TYPEPROCEDURE, $commonTypeProcedure->toKeyValue('PrimaryKey', 'IdTypeProcedure'), $comparison);
        } else {
            throw new PropelException('filterByCommonTypeProcedure() only accepts arguments of type CommonTypeProcedure or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTypeProcedure relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function joinCommonTypeProcedure($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTypeProcedure');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTypeProcedure');
        }

        return $this;
    }

    /**
     * Use the CommonTypeProcedure relation CommonTypeProcedure object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTypeProcedureQuery A secondary query class using the current class as primary query
     */
    public function useCommonTypeProcedureQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTypeProcedure($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTypeProcedure', '\Application\Propel\Mpe\CommonTypeProcedureQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonHabilitationTypeProcedure $commonHabilitationTypeProcedure Object to remove from the list of results
     *
     * @return CommonHabilitationTypeProcedureQuery The current query, for fluid interface
     */
    public function prune($commonHabilitationTypeProcedure = null)
    {
        if ($commonHabilitationTypeProcedure) {
            $this->addUsingAlias(CommonHabilitationTypeProcedurePeer::ID, $commonHabilitationTypeProcedure->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
