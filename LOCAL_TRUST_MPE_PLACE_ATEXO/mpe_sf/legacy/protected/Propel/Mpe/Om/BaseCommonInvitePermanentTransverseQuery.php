<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonInvitePermanentTransverse;
use Application\Propel\Mpe\CommonInvitePermanentTransversePeer;
use Application\Propel\Mpe\CommonInvitePermanentTransverseQuery;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;

/**
 * Base class that represents a query for the 'invite_permanent_transverse' table.
 *
 *
 *
 * @method CommonInvitePermanentTransverseQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonInvitePermanentTransverseQuery orderByOldServiceId($order = Criteria::ASC) Order by the old_service_id column
 * @method CommonInvitePermanentTransverseQuery orderByAgentId($order = Criteria::ASC) Order by the agent_id column
 * @method CommonInvitePermanentTransverseQuery orderByAcronyme($order = Criteria::ASC) Order by the acronyme column
 * @method CommonInvitePermanentTransverseQuery orderByServiceId($order = Criteria::ASC) Order by the service_id column
 *
 * @method CommonInvitePermanentTransverseQuery groupById() Group by the id column
 * @method CommonInvitePermanentTransverseQuery groupByOldServiceId() Group by the old_service_id column
 * @method CommonInvitePermanentTransverseQuery groupByAgentId() Group by the agent_id column
 * @method CommonInvitePermanentTransverseQuery groupByAcronyme() Group by the acronyme column
 * @method CommonInvitePermanentTransverseQuery groupByServiceId() Group by the service_id column
 *
 * @method CommonInvitePermanentTransverseQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonInvitePermanentTransverseQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonInvitePermanentTransverseQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonInvitePermanentTransverseQuery leftJoinCommonService($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonService relation
 * @method CommonInvitePermanentTransverseQuery rightJoinCommonService($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonService relation
 * @method CommonInvitePermanentTransverseQuery innerJoinCommonService($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonService relation
 *
 * @method CommonInvitePermanentTransverseQuery leftJoinCommonOrganisme($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonInvitePermanentTransverseQuery rightJoinCommonOrganisme($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonOrganisme relation
 * @method CommonInvitePermanentTransverseQuery innerJoinCommonOrganisme($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonOrganisme relation
 *
 * @method CommonInvitePermanentTransverseQuery leftJoinCommonAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAgent relation
 * @method CommonInvitePermanentTransverseQuery rightJoinCommonAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAgent relation
 * @method CommonInvitePermanentTransverseQuery innerJoinCommonAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAgent relation
 *
 * @method CommonInvitePermanentTransverse findOne(PropelPDO $con = null) Return the first CommonInvitePermanentTransverse matching the query
 * @method CommonInvitePermanentTransverse findOneOrCreate(PropelPDO $con = null) Return the first CommonInvitePermanentTransverse matching the query, or a new CommonInvitePermanentTransverse object populated from the query conditions when no match is found
 *
 * @method CommonInvitePermanentTransverse findOneByOldServiceId(int $old_service_id) Return the first CommonInvitePermanentTransverse filtered by the old_service_id column
 * @method CommonInvitePermanentTransverse findOneByAgentId(int $agent_id) Return the first CommonInvitePermanentTransverse filtered by the agent_id column
 * @method CommonInvitePermanentTransverse findOneByAcronyme(string $acronyme) Return the first CommonInvitePermanentTransverse filtered by the acronyme column
 * @method CommonInvitePermanentTransverse findOneByServiceId(string $service_id) Return the first CommonInvitePermanentTransverse filtered by the service_id column
 *
 * @method array findById(int $id) Return CommonInvitePermanentTransverse objects filtered by the id column
 * @method array findByOldServiceId(int $old_service_id) Return CommonInvitePermanentTransverse objects filtered by the old_service_id column
 * @method array findByAgentId(int $agent_id) Return CommonInvitePermanentTransverse objects filtered by the agent_id column
 * @method array findByAcronyme(string $acronyme) Return CommonInvitePermanentTransverse objects filtered by the acronyme column
 * @method array findByServiceId(string $service_id) Return CommonInvitePermanentTransverse objects filtered by the service_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonInvitePermanentTransverseQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonInvitePermanentTransverseQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonInvitePermanentTransverse', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonInvitePermanentTransverseQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonInvitePermanentTransverseQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonInvitePermanentTransverseQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonInvitePermanentTransverseQuery) {
            return $criteria;
        }
        $query = new CommonInvitePermanentTransverseQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonInvitePermanentTransverse|CommonInvitePermanentTransverse[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonInvitePermanentTransversePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonInvitePermanentTransversePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonInvitePermanentTransverse A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonInvitePermanentTransverse A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `old_service_id`, `agent_id`, `acronyme`, `service_id` FROM `invite_permanent_transverse` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonInvitePermanentTransverse();
            $obj->hydrate($row);
            CommonInvitePermanentTransversePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonInvitePermanentTransverse|CommonInvitePermanentTransverse[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonInvitePermanentTransverse[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonInvitePermanentTransverseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonInvitePermanentTransversePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonInvitePermanentTransverseQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonInvitePermanentTransversePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentTransverseQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonInvitePermanentTransversePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonInvitePermanentTransversePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentTransversePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the old_service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldServiceId(1234); // WHERE old_service_id = 1234
     * $query->filterByOldServiceId(array(12, 34)); // WHERE old_service_id IN (12, 34)
     * $query->filterByOldServiceId(array('min' => 12)); // WHERE old_service_id >= 12
     * $query->filterByOldServiceId(array('max' => 12)); // WHERE old_service_id <= 12
     * </code>
     *
     * @param     mixed $oldServiceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentTransverseQuery The current query, for fluid interface
     */
    public function filterByOldServiceId($oldServiceId = null, $comparison = null)
    {
        if (is_array($oldServiceId)) {
            $useMinMax = false;
            if (isset($oldServiceId['min'])) {
                $this->addUsingAlias(CommonInvitePermanentTransversePeer::OLD_SERVICE_ID, $oldServiceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldServiceId['max'])) {
                $this->addUsingAlias(CommonInvitePermanentTransversePeer::OLD_SERVICE_ID, $oldServiceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentTransversePeer::OLD_SERVICE_ID, $oldServiceId, $comparison);
    }

    /**
     * Filter the query on the agent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAgentId(1234); // WHERE agent_id = 1234
     * $query->filterByAgentId(array(12, 34)); // WHERE agent_id IN (12, 34)
     * $query->filterByAgentId(array('min' => 12)); // WHERE agent_id >= 12
     * $query->filterByAgentId(array('max' => 12)); // WHERE agent_id <= 12
     * </code>
     *
     * @see       filterByCommonAgent()
     *
     * @param     mixed $agentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentTransverseQuery The current query, for fluid interface
     */
    public function filterByAgentId($agentId = null, $comparison = null)
    {
        if (is_array($agentId)) {
            $useMinMax = false;
            if (isset($agentId['min'])) {
                $this->addUsingAlias(CommonInvitePermanentTransversePeer::AGENT_ID, $agentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($agentId['max'])) {
                $this->addUsingAlias(CommonInvitePermanentTransversePeer::AGENT_ID, $agentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentTransversePeer::AGENT_ID, $agentId, $comparison);
    }

    /**
     * Filter the query on the acronyme column
     *
     * Example usage:
     * <code>
     * $query->filterByAcronyme('fooValue');   // WHERE acronyme = 'fooValue'
     * $query->filterByAcronyme('%fooValue%'); // WHERE acronyme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $acronyme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentTransverseQuery The current query, for fluid interface
     */
    public function filterByAcronyme($acronyme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($acronyme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $acronyme)) {
                $acronyme = str_replace('*', '%', $acronyme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentTransversePeer::ACRONYME, $acronyme, $comparison);
    }

    /**
     * Filter the query on the service_id column
     *
     * Example usage:
     * <code>
     * $query->filterByServiceId(1234); // WHERE service_id = 1234
     * $query->filterByServiceId(array(12, 34)); // WHERE service_id IN (12, 34)
     * $query->filterByServiceId(array('min' => 12)); // WHERE service_id >= 12
     * $query->filterByServiceId(array('max' => 12)); // WHERE service_id <= 12
     * </code>
     *
     * @see       filterByCommonService()
     *
     * @param     mixed $serviceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonInvitePermanentTransverseQuery The current query, for fluid interface
     */
    public function filterByServiceId($serviceId = null, $comparison = null)
    {
        if (is_array($serviceId)) {
            $useMinMax = false;
            if (isset($serviceId['min'])) {
                $this->addUsingAlias(CommonInvitePermanentTransversePeer::SERVICE_ID, $serviceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($serviceId['max'])) {
                $this->addUsingAlias(CommonInvitePermanentTransversePeer::SERVICE_ID, $serviceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonInvitePermanentTransversePeer::SERVICE_ID, $serviceId, $comparison);
    }

    /**
     * Filter the query by a related CommonService object
     *
     * @param   CommonService|PropelObjectCollection $commonService The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInvitePermanentTransverseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonService($commonService, $comparison = null)
    {
        if ($commonService instanceof CommonService) {
            return $this
                ->addUsingAlias(CommonInvitePermanentTransversePeer::SERVICE_ID, $commonService->getId(), $comparison);
        } elseif ($commonService instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonInvitePermanentTransversePeer::SERVICE_ID, $commonService->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonService() only accepts arguments of type CommonService or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonService relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInvitePermanentTransverseQuery The current query, for fluid interface
     */
    public function joinCommonService($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonService');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonService');
        }

        return $this;
    }

    /**
     * Use the CommonService relation CommonService object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonServiceQuery A secondary query class using the current class as primary query
     */
    public function useCommonServiceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonService($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonService', '\Application\Propel\Mpe\CommonServiceQuery');
    }

    /**
     * Filter the query by a related CommonOrganisme object
     *
     * @param   CommonOrganisme|PropelObjectCollection $commonOrganisme The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInvitePermanentTransverseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonOrganisme($commonOrganisme, $comparison = null)
    {
        if ($commonOrganisme instanceof CommonOrganisme) {
            return $this
                ->addUsingAlias(CommonInvitePermanentTransversePeer::ACRONYME, $commonOrganisme->getAcronyme(), $comparison);
        } elseif ($commonOrganisme instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonInvitePermanentTransversePeer::ACRONYME, $commonOrganisme->toKeyValue('PrimaryKey', 'Acronyme'), $comparison);
        } else {
            throw new PropelException('filterByCommonOrganisme() only accepts arguments of type CommonOrganisme or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonOrganisme relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInvitePermanentTransverseQuery The current query, for fluid interface
     */
    public function joinCommonOrganisme($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonOrganisme');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonOrganisme');
        }

        return $this;
    }

    /**
     * Use the CommonOrganisme relation CommonOrganisme object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonOrganismeQuery A secondary query class using the current class as primary query
     */
    public function useCommonOrganismeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonOrganisme($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonOrganisme', '\Application\Propel\Mpe\CommonOrganismeQuery');
    }

    /**
     * Filter the query by a related CommonAgent object
     *
     * @param   CommonAgent|PropelObjectCollection $commonAgent The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonInvitePermanentTransverseQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAgent($commonAgent, $comparison = null)
    {
        if ($commonAgent instanceof CommonAgent) {
            return $this
                ->addUsingAlias(CommonInvitePermanentTransversePeer::AGENT_ID, $commonAgent->getId(), $comparison);
        } elseif ($commonAgent instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonInvitePermanentTransversePeer::AGENT_ID, $commonAgent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonAgent() only accepts arguments of type CommonAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonInvitePermanentTransverseQuery The current query, for fluid interface
     */
    public function joinCommonAgent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAgent');
        }

        return $this;
    }

    /**
     * Use the CommonAgent relation CommonAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonAgentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAgent', '\Application\Propel\Mpe\CommonAgentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonInvitePermanentTransverse $commonInvitePermanentTransverse Object to remove from the list of results
     *
     * @return CommonInvitePermanentTransverseQuery The current query, for fluid interface
     */
    public function prune($commonInvitePermanentTransverse = null)
    {
        if ($commonInvitePermanentTransverse) {
            $this->addUsingAlias(CommonInvitePermanentTransversePeer::ID, $commonInvitePermanentTransverse->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
