<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritQuery;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTContactContratPeer;
use Application\Propel\Mpe\CommonTContactContratQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTDecisionSelectionEntreprise;
use Application\Propel\Mpe\CommonTDecisionSelectionEntrepriseQuery;

/**
 * Base class that represents a row from the 't_contact_contrat' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTContactContrat extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTContactContratPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTContactContratPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id_contact_contrat field.
     * @var        int
     */
    protected $id_contact_contrat;

    /**
     * The value for the id_entreprise field.
     * @var        int
     */
    protected $id_entreprise;

    /**
     * The value for the id_etablissement field.
     * @var        int
     */
    protected $id_etablissement;

    /**
     * The value for the old_id_inscrit field.
     * @var        int
     */
    protected $old_id_inscrit;

    /**
     * The value for the nom field.
     * @var        string
     */
    protected $nom;

    /**
     * The value for the prenom field.
     * @var        string
     */
    protected $prenom;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the telephone field.
     * @var        string
     */
    protected $telephone;

    /**
     * The value for the fax field.
     * @var        string
     */
    protected $fax;

    /**
     * The value for the created_at field.
     * @var        string
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     * @var        string
     */
    protected $updated_at;

    /**
     * The value for the id_inscrit field.
     * @var        string
     */
    protected $id_inscrit;

    /**
     * @var        CommonInscrit
     */
    protected $aCommonInscrit;

    /**
     * @var        PropelObjectCollection|CommonTContratTitulaire[] Collection to store aggregation of CommonTContratTitulaire objects.
     */
    protected $collCommonTContratTitulaires;
    protected $collCommonTContratTitulairesPartial;

    /**
     * @var        PropelObjectCollection|CommonTDecisionSelectionEntreprise[] Collection to store aggregation of CommonTDecisionSelectionEntreprise objects.
     */
    protected $collCommonTDecisionSelectionEntreprises;
    protected $collCommonTDecisionSelectionEntreprisesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTContratTitulairesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonTDecisionSelectionEntreprisesScheduledForDeletion = null;

    /**
     * Get the [id_contact_contrat] column value.
     *
     * @return int
     */
    public function getIdContactContrat()
    {

        return $this->id_contact_contrat;
    }

    /**
     * Get the [id_entreprise] column value.
     *
     * @return int
     */
    public function getIdEntreprise()
    {

        return $this->id_entreprise;
    }

    /**
     * Get the [id_etablissement] column value.
     *
     * @return int
     */
    public function getIdEtablissement()
    {

        return $this->id_etablissement;
    }

    /**
     * Get the [old_id_inscrit] column value.
     *
     * @return int
     */
    public function getOldIdInscrit()
    {

        return $this->old_id_inscrit;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {

        return $this->nom;
    }

    /**
     * Get the [prenom] column value.
     *
     * @return string
     */
    public function getPrenom()
    {

        return $this->prenom;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [telephone] column value.
     *
     * @return string
     */
    public function getTelephone()
    {

        return $this->telephone;
    }

    /**
     * Get the [fax] column value.
     *
     * @return string
     */
    public function getFax()
    {

        return $this->fax;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = 'Y-m-d H:i:s')
    {
        if ($this->created_at === null) {
            return null;
        }

        if ($this->created_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->created_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->created_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = 'Y-m-d H:i:s')
    {
        if ($this->updated_at === null) {
            return null;
        }

        if ($this->updated_at === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->updated_at);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->updated_at, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [id_inscrit] column value.
     *
     * @return string
     */
    public function getIdInscrit()
    {

        return $this->id_inscrit;
    }

    /**
     * Set the value of [id_contact_contrat] column.
     *
     * @param int $v new value
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setIdContactContrat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_contact_contrat !== $v) {
            $this->id_contact_contrat = $v;
            $this->modifiedColumns[] = CommonTContactContratPeer::ID_CONTACT_CONTRAT;
        }


        return $this;
    } // setIdContactContrat()

    /**
     * Set the value of [id_entreprise] column.
     *
     * @param int $v new value
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setIdEntreprise($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_entreprise !== $v) {
            $this->id_entreprise = $v;
            $this->modifiedColumns[] = CommonTContactContratPeer::ID_ENTREPRISE;
        }


        return $this;
    } // setIdEntreprise()

    /**
     * Set the value of [id_etablissement] column.
     *
     * @param int $v new value
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setIdEtablissement($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_etablissement !== $v) {
            $this->id_etablissement = $v;
            $this->modifiedColumns[] = CommonTContactContratPeer::ID_ETABLISSEMENT;
        }


        return $this;
    } // setIdEtablissement()

    /**
     * Set the value of [old_id_inscrit] column.
     *
     * @param int $v new value
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setOldIdInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_id_inscrit !== $v) {
            $this->old_id_inscrit = $v;
            $this->modifiedColumns[] = CommonTContactContratPeer::OLD_ID_INSCRIT;
        }


        return $this;
    } // setOldIdInscrit()

    /**
     * Set the value of [nom] column.
     *
     * @param string $v new value
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[] = CommonTContactContratPeer::NOM;
        }


        return $this;
    } // setNom()

    /**
     * Set the value of [prenom] column.
     *
     * @param string $v new value
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setPrenom($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->prenom !== $v) {
            $this->prenom = $v;
            $this->modifiedColumns[] = CommonTContactContratPeer::PRENOM;
        }


        return $this;
    } // setPrenom()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = CommonTContactContratPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [telephone] column.
     *
     * @param string $v new value
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setTelephone($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->telephone !== $v) {
            $this->telephone = $v;
            $this->modifiedColumns[] = CommonTContactContratPeer::TELEPHONE;
        }


        return $this;
    } // setTelephone()

    /**
     * Set the value of [fax] column.
     *
     * @param string $v new value
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[] = CommonTContactContratPeer::FAX;
        }


        return $this;
    } // setFax()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            $currentDateAsString = ($this->created_at !== null && $tmpDt = new DateTime($this->created_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->created_at = $newDateAsString;
                $this->modifiedColumns[] = CommonTContactContratPeer::CREATED_AT;
            }
        } // if either are not null


        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            $currentDateAsString = ($this->updated_at !== null && $tmpDt = new DateTime($this->updated_at)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->updated_at = $newDateAsString;
                $this->modifiedColumns[] = CommonTContactContratPeer::UPDATED_AT;
            }
        } // if either are not null


        return $this;
    } // setUpdatedAt()

    /**
     * Set the value of [id_inscrit] column.
     *
     * @param string $v new value
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setIdInscrit($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_inscrit !== $v) {
            $this->id_inscrit = $v;
            $this->modifiedColumns[] = CommonTContactContratPeer::ID_INSCRIT;
        }

        if ($this->aCommonInscrit !== null && $this->aCommonInscrit->getId() !== $v) {
            $this->aCommonInscrit = null;
        }


        return $this;
    } // setIdInscrit()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id_contact_contrat = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_entreprise = ($row[$startcol + 1] !== null) ? (int) $row[$startcol + 1] : null;
            $this->id_etablissement = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->old_id_inscrit = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->nom = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->prenom = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->email = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->telephone = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->fax = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->created_at = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->updated_at = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->id_inscrit = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 12; // 12 = CommonTContactContratPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTContactContrat object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonInscrit !== null && $this->id_inscrit !== $this->aCommonInscrit->getId()) {
            $this->aCommonInscrit = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTContactContratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTContactContratPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonInscrit = null;
            $this->collCommonTContratTitulaires = null;

            $this->collCommonTDecisionSelectionEntreprises = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTContactContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTContactContratQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTContactContratPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTContactContratPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonInscrit !== null) {
                if ($this->aCommonInscrit->isModified() || $this->aCommonInscrit->isNew()) {
                    $affectedRows += $this->aCommonInscrit->save($con);
                }
                $this->setCommonInscrit($this->aCommonInscrit);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonTContratTitulairesScheduledForDeletion !== null) {
                if (!$this->commonTContratTitulairesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTContratTitulairesScheduledForDeletion as $commonTContratTitulaire) {
                        // need to save related object because we set the relation to null
                        $commonTContratTitulaire->save($con);
                    }
                    $this->commonTContratTitulairesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTContratTitulaires !== null) {
                foreach ($this->collCommonTContratTitulaires as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->commonTDecisionSelectionEntreprisesScheduledForDeletion !== null) {
                if (!$this->commonTDecisionSelectionEntreprisesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonTDecisionSelectionEntreprisesScheduledForDeletion as $commonTDecisionSelectionEntreprise) {
                        // need to save related object because we set the relation to null
                        $commonTDecisionSelectionEntreprise->save($con);
                    }
                    $this->commonTDecisionSelectionEntreprisesScheduledForDeletion = null;
                }
            }

            if ($this->collCommonTDecisionSelectionEntreprises !== null) {
                foreach ($this->collCommonTDecisionSelectionEntreprises as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTContactContratPeer::ID_CONTACT_CONTRAT;
        if (null !== $this->id_contact_contrat) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTContactContratPeer::ID_CONTACT_CONTRAT . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTContactContratPeer::ID_CONTACT_CONTRAT)) {
            $modifiedColumns[':p' . $index++]  = '`id_contact_contrat`';
        }
        if ($this->isColumnModified(CommonTContactContratPeer::ID_ENTREPRISE)) {
            $modifiedColumns[':p' . $index++]  = '`id_entreprise`';
        }
        if ($this->isColumnModified(CommonTContactContratPeer::ID_ETABLISSEMENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_etablissement`';
        }
        if ($this->isColumnModified(CommonTContactContratPeer::OLD_ID_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`old_id_inscrit`';
        }
        if ($this->isColumnModified(CommonTContactContratPeer::NOM)) {
            $modifiedColumns[':p' . $index++]  = '`nom`';
        }
        if ($this->isColumnModified(CommonTContactContratPeer::PRENOM)) {
            $modifiedColumns[':p' . $index++]  = '`prenom`';
        }
        if ($this->isColumnModified(CommonTContactContratPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(CommonTContactContratPeer::TELEPHONE)) {
            $modifiedColumns[':p' . $index++]  = '`telephone`';
        }
        if ($this->isColumnModified(CommonTContactContratPeer::FAX)) {
            $modifiedColumns[':p' . $index++]  = '`fax`';
        }
        if ($this->isColumnModified(CommonTContactContratPeer::CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`created_at`';
        }
        if ($this->isColumnModified(CommonTContactContratPeer::UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = '`updated_at`';
        }
        if ($this->isColumnModified(CommonTContactContratPeer::ID_INSCRIT)) {
            $modifiedColumns[':p' . $index++]  = '`id_inscrit`';
        }

        $sql = sprintf(
            'INSERT INTO `t_contact_contrat` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id_contact_contrat`':
                        $stmt->bindValue($identifier, $this->id_contact_contrat, PDO::PARAM_INT);
                        break;
                    case '`id_entreprise`':
                        $stmt->bindValue($identifier, $this->id_entreprise, PDO::PARAM_INT);
                        break;
                    case '`id_etablissement`':
                        $stmt->bindValue($identifier, $this->id_etablissement, PDO::PARAM_INT);
                        break;
                    case '`old_id_inscrit`':
                        $stmt->bindValue($identifier, $this->old_id_inscrit, PDO::PARAM_INT);
                        break;
                    case '`nom`':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);
                        break;
                    case '`prenom`':
                        $stmt->bindValue($identifier, $this->prenom, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`telephone`':
                        $stmt->bindValue($identifier, $this->telephone, PDO::PARAM_STR);
                        break;
                    case '`fax`':
                        $stmt->bindValue($identifier, $this->fax, PDO::PARAM_STR);
                        break;
                    case '`created_at`':
                        $stmt->bindValue($identifier, $this->created_at, PDO::PARAM_STR);
                        break;
                    case '`updated_at`':
                        $stmt->bindValue($identifier, $this->updated_at, PDO::PARAM_STR);
                        break;
                    case '`id_inscrit`':
                        $stmt->bindValue($identifier, $this->id_inscrit, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setIdContactContrat($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonInscrit !== null) {
                if (!$this->aCommonInscrit->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonInscrit->getValidationFailures());
                }
            }


            if (($retval = CommonTContactContratPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collCommonTContratTitulaires !== null) {
                    foreach ($this->collCommonTContratTitulaires as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collCommonTDecisionSelectionEntreprises !== null) {
                    foreach ($this->collCommonTDecisionSelectionEntreprises as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTContactContratPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdContactContrat();
                break;
            case 1:
                return $this->getIdEntreprise();
                break;
            case 2:
                return $this->getIdEtablissement();
                break;
            case 3:
                return $this->getOldIdInscrit();
                break;
            case 4:
                return $this->getNom();
                break;
            case 5:
                return $this->getPrenom();
                break;
            case 6:
                return $this->getEmail();
                break;
            case 7:
                return $this->getTelephone();
                break;
            case 8:
                return $this->getFax();
                break;
            case 9:
                return $this->getCreatedAt();
                break;
            case 10:
                return $this->getUpdatedAt();
                break;
            case 11:
                return $this->getIdInscrit();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTContactContrat'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTContactContrat'][$this->getPrimaryKey()] = true;
        $keys = CommonTContactContratPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getIdContactContrat(),
            $keys[1] => $this->getIdEntreprise(),
            $keys[2] => $this->getIdEtablissement(),
            $keys[3] => $this->getOldIdInscrit(),
            $keys[4] => $this->getNom(),
            $keys[5] => $this->getPrenom(),
            $keys[6] => $this->getEmail(),
            $keys[7] => $this->getTelephone(),
            $keys[8] => $this->getFax(),
            $keys[9] => $this->getCreatedAt(),
            $keys[10] => $this->getUpdatedAt(),
            $keys[11] => $this->getIdInscrit(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonInscrit) {
                $result['CommonInscrit'] = $this->aCommonInscrit->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonTContratTitulaires) {
                $result['CommonTContratTitulaires'] = $this->collCommonTContratTitulaires->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCommonTDecisionSelectionEntreprises) {
                $result['CommonTDecisionSelectionEntreprises'] = $this->collCommonTDecisionSelectionEntreprises->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTContactContratPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdContactContrat($value);
                break;
            case 1:
                $this->setIdEntreprise($value);
                break;
            case 2:
                $this->setIdEtablissement($value);
                break;
            case 3:
                $this->setOldIdInscrit($value);
                break;
            case 4:
                $this->setNom($value);
                break;
            case 5:
                $this->setPrenom($value);
                break;
            case 6:
                $this->setEmail($value);
                break;
            case 7:
                $this->setTelephone($value);
                break;
            case 8:
                $this->setFax($value);
                break;
            case 9:
                $this->setCreatedAt($value);
                break;
            case 10:
                $this->setUpdatedAt($value);
                break;
            case 11:
                $this->setIdInscrit($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTContactContratPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setIdContactContrat($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdEntreprise($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdEtablissement($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setOldIdInscrit($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setNom($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPrenom($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setEmail($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setTelephone($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setFax($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setCreatedAt($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setUpdatedAt($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setIdInscrit($arr[$keys[11]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTContactContratPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTContactContratPeer::ID_CONTACT_CONTRAT)) $criteria->add(CommonTContactContratPeer::ID_CONTACT_CONTRAT, $this->id_contact_contrat);
        if ($this->isColumnModified(CommonTContactContratPeer::ID_ENTREPRISE)) $criteria->add(CommonTContactContratPeer::ID_ENTREPRISE, $this->id_entreprise);
        if ($this->isColumnModified(CommonTContactContratPeer::ID_ETABLISSEMENT)) $criteria->add(CommonTContactContratPeer::ID_ETABLISSEMENT, $this->id_etablissement);
        if ($this->isColumnModified(CommonTContactContratPeer::OLD_ID_INSCRIT)) $criteria->add(CommonTContactContratPeer::OLD_ID_INSCRIT, $this->old_id_inscrit);
        if ($this->isColumnModified(CommonTContactContratPeer::NOM)) $criteria->add(CommonTContactContratPeer::NOM, $this->nom);
        if ($this->isColumnModified(CommonTContactContratPeer::PRENOM)) $criteria->add(CommonTContactContratPeer::PRENOM, $this->prenom);
        if ($this->isColumnModified(CommonTContactContratPeer::EMAIL)) $criteria->add(CommonTContactContratPeer::EMAIL, $this->email);
        if ($this->isColumnModified(CommonTContactContratPeer::TELEPHONE)) $criteria->add(CommonTContactContratPeer::TELEPHONE, $this->telephone);
        if ($this->isColumnModified(CommonTContactContratPeer::FAX)) $criteria->add(CommonTContactContratPeer::FAX, $this->fax);
        if ($this->isColumnModified(CommonTContactContratPeer::CREATED_AT)) $criteria->add(CommonTContactContratPeer::CREATED_AT, $this->created_at);
        if ($this->isColumnModified(CommonTContactContratPeer::UPDATED_AT)) $criteria->add(CommonTContactContratPeer::UPDATED_AT, $this->updated_at);
        if ($this->isColumnModified(CommonTContactContratPeer::ID_INSCRIT)) $criteria->add(CommonTContactContratPeer::ID_INSCRIT, $this->id_inscrit);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTContactContratPeer::DATABASE_NAME);
        $criteria->add(CommonTContactContratPeer::ID_CONTACT_CONTRAT, $this->id_contact_contrat);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdContactContrat();
    }

    /**
     * Generic method to set the primary key (id_contact_contrat column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setIdContactContrat($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getIdContactContrat();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTContactContrat (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdEntreprise($this->getIdEntreprise());
        $copyObj->setIdEtablissement($this->getIdEtablissement());
        $copyObj->setOldIdInscrit($this->getOldIdInscrit());
        $copyObj->setNom($this->getNom());
        $copyObj->setPrenom($this->getPrenom());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setTelephone($this->getTelephone());
        $copyObj->setFax($this->getFax());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        $copyObj->setIdInscrit($this->getIdInscrit());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getCommonTContratTitulaires() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTContratTitulaire($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCommonTDecisionSelectionEntreprises() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonTDecisionSelectionEntreprise($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdContactContrat(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTContactContrat Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTContactContratPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTContactContratPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonInscrit object.
     *
     * @param   CommonInscrit $v
     * @return CommonTContactContrat The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonInscrit(CommonInscrit $v = null)
    {
        if ($v === null) {
            $this->setIdInscrit(NULL);
        } else {
            $this->setIdInscrit($v->getId());
        }

        $this->aCommonInscrit = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonInscrit object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTContactContrat($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonInscrit object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonInscrit The associated CommonInscrit object.
     * @throws PropelException
     */
    public function getCommonInscrit(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonInscrit === null && (($this->id_inscrit !== "" && $this->id_inscrit !== null)) && $doQuery) {
            $this->aCommonInscrit = CommonInscritQuery::create()->findPk($this->id_inscrit, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonInscrit->addCommonTContactContrats($this);
             */
        }

        return $this->aCommonInscrit;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonTContratTitulaire' == $relationName) {
            $this->initCommonTContratTitulaires();
        }
        if ('CommonTDecisionSelectionEntreprise' == $relationName) {
            $this->initCommonTDecisionSelectionEntreprises();
        }
    }

    /**
     * Clears out the collCommonTContratTitulaires collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTContactContrat The current object (for fluent API support)
     * @see        addCommonTContratTitulaires()
     */
    public function clearCommonTContratTitulaires()
    {
        $this->collCommonTContratTitulaires = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTContratTitulairesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTContratTitulaires collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTContratTitulaires($v = true)
    {
        $this->collCommonTContratTitulairesPartial = $v;
    }

    /**
     * Initializes the collCommonTContratTitulaires collection.
     *
     * By default this just sets the collCommonTContratTitulaires collection to an empty array (like clearcollCommonTContratTitulaires());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTContratTitulaires($overrideExisting = true)
    {
        if (null !== $this->collCommonTContratTitulaires && !$overrideExisting) {
            return;
        }
        $this->collCommonTContratTitulaires = new PropelObjectCollection();
        $this->collCommonTContratTitulaires->setModel('CommonTContratTitulaire');
    }

    /**
     * Gets an array of CommonTContratTitulaire objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTContactContrat is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     * @throws PropelException
     */
    public function getCommonTContratTitulaires($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTContratTitulairesPartial && !$this->isNew();
        if (null === $this->collCommonTContratTitulaires || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTContratTitulaires) {
                // return empty collection
                $this->initCommonTContratTitulaires();
            } else {
                $collCommonTContratTitulaires = CommonTContratTitulaireQuery::create(null, $criteria)
                    ->filterByCommonTContactContrat($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTContratTitulairesPartial && count($collCommonTContratTitulaires)) {
                      $this->initCommonTContratTitulaires(false);

                      foreach ($collCommonTContratTitulaires as $obj) {
                        if (false == $this->collCommonTContratTitulaires->contains($obj)) {
                          $this->collCommonTContratTitulaires->append($obj);
                        }
                      }

                      $this->collCommonTContratTitulairesPartial = true;
                    }

                    $collCommonTContratTitulaires->getInternalIterator()->rewind();

                    return $collCommonTContratTitulaires;
                }

                if ($partial && $this->collCommonTContratTitulaires) {
                    foreach ($this->collCommonTContratTitulaires as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTContratTitulaires[] = $obj;
                        }
                    }
                }

                $this->collCommonTContratTitulaires = $collCommonTContratTitulaires;
                $this->collCommonTContratTitulairesPartial = false;
            }
        }

        return $this->collCommonTContratTitulaires;
    }

    /**
     * Sets a collection of CommonTContratTitulaire objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTContratTitulaires A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setCommonTContratTitulaires(PropelCollection $commonTContratTitulaires, PropelPDO $con = null)
    {
        $commonTContratTitulairesToDelete = $this->getCommonTContratTitulaires(new Criteria(), $con)->diff($commonTContratTitulaires);


        $this->commonTContratTitulairesScheduledForDeletion = $commonTContratTitulairesToDelete;

        foreach ($commonTContratTitulairesToDelete as $commonTContratTitulaireRemoved) {
            $commonTContratTitulaireRemoved->setCommonTContactContrat(null);
        }

        $this->collCommonTContratTitulaires = null;
        foreach ($commonTContratTitulaires as $commonTContratTitulaire) {
            $this->addCommonTContratTitulaire($commonTContratTitulaire);
        }

        $this->collCommonTContratTitulaires = $commonTContratTitulaires;
        $this->collCommonTContratTitulairesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTContratTitulaire objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTContratTitulaire objects.
     * @throws PropelException
     */
    public function countCommonTContratTitulaires(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTContratTitulairesPartial && !$this->isNew();
        if (null === $this->collCommonTContratTitulaires || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTContratTitulaires) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTContratTitulaires());
            }
            $query = CommonTContratTitulaireQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTContactContrat($this)
                ->count($con);
        }

        return count($this->collCommonTContratTitulaires);
    }

    /**
     * Method called to associate a BaseCommonTContratTitulaire object to this object
     * through the BaseCommonTContratTitulaire foreign key attribute.
     *
     * @param   BaseCommonTContratTitulaire $l BaseCommonTContratTitulaire
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function addCommonTContratTitulaire(BaseCommonTContratTitulaire $l)
    {
        if ($this->collCommonTContratTitulaires === null) {
            $this->initCommonTContratTitulaires();
            $this->collCommonTContratTitulairesPartial = true;
        }
        if (!in_array($l, $this->collCommonTContratTitulaires->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTContratTitulaire($l);
        }

        return $this;
    }

    /**
     * @param	CommonTContratTitulaire $commonTContratTitulaire The commonTContratTitulaire object to add.
     */
    protected function doAddCommonTContratTitulaire($commonTContratTitulaire)
    {
        $this->collCommonTContratTitulaires[]= $commonTContratTitulaire;
        $commonTContratTitulaire->setCommonTContactContrat($this);
    }

    /**
     * @param	CommonTContratTitulaire $commonTContratTitulaire The commonTContratTitulaire object to remove.
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function removeCommonTContratTitulaire($commonTContratTitulaire)
    {
        if ($this->getCommonTContratTitulaires()->contains($commonTContratTitulaire)) {
            $this->collCommonTContratTitulaires->remove($this->collCommonTContratTitulaires->search($commonTContratTitulaire));
            if (null === $this->commonTContratTitulairesScheduledForDeletion) {
                $this->commonTContratTitulairesScheduledForDeletion = clone $this->collCommonTContratTitulaires;
                $this->commonTContratTitulairesScheduledForDeletion->clear();
            }
            $this->commonTContratTitulairesScheduledForDeletion[]= $commonTContratTitulaire;
            $commonTContratTitulaire->setCommonTContactContrat(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContactContrat is new, it will return
     * an empty collection; or if this CommonTContactContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContactContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTypeContratConcessionPivot($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeContratConcessionPivot', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContactContrat is new, it will return
     * an empty collection; or if this CommonTContactContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContactContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTypeContratPivot($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeContratPivot', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContactContrat is new, it will return
     * an empty collection; or if this CommonTContactContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContactContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTypeProcedureConcessionPivot($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeProcedureConcessionPivot', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContactContrat is new, it will return
     * an empty collection; or if this CommonTContactContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContactContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTContratTitulaireRelatedByIdContratMulti($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTContratTitulaireRelatedByIdContratMulti', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContactContrat is new, it will return
     * an empty collection; or if this CommonTContactContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContactContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTTypeContrat($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTTypeContrat', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CommonTContactContrat is new, it will return
     * an empty collection; or if this CommonTContactContrat has previously
     * been saved, it will retrieve related CommonTContratTitulaires from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CommonTContactContrat.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|CommonTContratTitulaire[] List of CommonTContratTitulaire objects
     */
    public function getCommonTContratTitulairesJoinCommonTypeProcedure($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = CommonTContratTitulaireQuery::create(null, $criteria);
        $query->joinWith('CommonTypeProcedure', $join_behavior);

        return $this->getCommonTContratTitulaires($query, $con);
    }

    /**
     * Clears out the collCommonTDecisionSelectionEntreprises collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonTContactContrat The current object (for fluent API support)
     * @see        addCommonTDecisionSelectionEntreprises()
     */
    public function clearCommonTDecisionSelectionEntreprises()
    {
        $this->collCommonTDecisionSelectionEntreprises = null; // important to set this to null since that means it is uninitialized
        $this->collCommonTDecisionSelectionEntreprisesPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonTDecisionSelectionEntreprises collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonTDecisionSelectionEntreprises($v = true)
    {
        $this->collCommonTDecisionSelectionEntreprisesPartial = $v;
    }

    /**
     * Initializes the collCommonTDecisionSelectionEntreprises collection.
     *
     * By default this just sets the collCommonTDecisionSelectionEntreprises collection to an empty array (like clearcollCommonTDecisionSelectionEntreprises());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonTDecisionSelectionEntreprises($overrideExisting = true)
    {
        if (null !== $this->collCommonTDecisionSelectionEntreprises && !$overrideExisting) {
            return;
        }
        $this->collCommonTDecisionSelectionEntreprises = new PropelObjectCollection();
        $this->collCommonTDecisionSelectionEntreprises->setModel('CommonTDecisionSelectionEntreprise');
    }

    /**
     * Gets an array of CommonTDecisionSelectionEntreprise objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonTContactContrat is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonTDecisionSelectionEntreprise[] List of CommonTDecisionSelectionEntreprise objects
     * @throws PropelException
     */
    public function getCommonTDecisionSelectionEntreprises($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonTDecisionSelectionEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTDecisionSelectionEntreprises || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonTDecisionSelectionEntreprises) {
                // return empty collection
                $this->initCommonTDecisionSelectionEntreprises();
            } else {
                $collCommonTDecisionSelectionEntreprises = CommonTDecisionSelectionEntrepriseQuery::create(null, $criteria)
                    ->filterByCommonTContactContrat($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonTDecisionSelectionEntreprisesPartial && count($collCommonTDecisionSelectionEntreprises)) {
                      $this->initCommonTDecisionSelectionEntreprises(false);

                      foreach ($collCommonTDecisionSelectionEntreprises as $obj) {
                        if (false == $this->collCommonTDecisionSelectionEntreprises->contains($obj)) {
                          $this->collCommonTDecisionSelectionEntreprises->append($obj);
                        }
                      }

                      $this->collCommonTDecisionSelectionEntreprisesPartial = true;
                    }

                    $collCommonTDecisionSelectionEntreprises->getInternalIterator()->rewind();

                    return $collCommonTDecisionSelectionEntreprises;
                }

                if ($partial && $this->collCommonTDecisionSelectionEntreprises) {
                    foreach ($this->collCommonTDecisionSelectionEntreprises as $obj) {
                        if ($obj->isNew()) {
                            $collCommonTDecisionSelectionEntreprises[] = $obj;
                        }
                    }
                }

                $this->collCommonTDecisionSelectionEntreprises = $collCommonTDecisionSelectionEntreprises;
                $this->collCommonTDecisionSelectionEntreprisesPartial = false;
            }
        }

        return $this->collCommonTDecisionSelectionEntreprises;
    }

    /**
     * Sets a collection of CommonTDecisionSelectionEntreprise objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonTDecisionSelectionEntreprises A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function setCommonTDecisionSelectionEntreprises(PropelCollection $commonTDecisionSelectionEntreprises, PropelPDO $con = null)
    {
        $commonTDecisionSelectionEntreprisesToDelete = $this->getCommonTDecisionSelectionEntreprises(new Criteria(), $con)->diff($commonTDecisionSelectionEntreprises);


        $this->commonTDecisionSelectionEntreprisesScheduledForDeletion = $commonTDecisionSelectionEntreprisesToDelete;

        foreach ($commonTDecisionSelectionEntreprisesToDelete as $commonTDecisionSelectionEntrepriseRemoved) {
            $commonTDecisionSelectionEntrepriseRemoved->setCommonTContactContrat(null);
        }

        $this->collCommonTDecisionSelectionEntreprises = null;
        foreach ($commonTDecisionSelectionEntreprises as $commonTDecisionSelectionEntreprise) {
            $this->addCommonTDecisionSelectionEntreprise($commonTDecisionSelectionEntreprise);
        }

        $this->collCommonTDecisionSelectionEntreprises = $commonTDecisionSelectionEntreprises;
        $this->collCommonTDecisionSelectionEntreprisesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonTDecisionSelectionEntreprise objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonTDecisionSelectionEntreprise objects.
     * @throws PropelException
     */
    public function countCommonTDecisionSelectionEntreprises(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonTDecisionSelectionEntreprisesPartial && !$this->isNew();
        if (null === $this->collCommonTDecisionSelectionEntreprises || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonTDecisionSelectionEntreprises) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonTDecisionSelectionEntreprises());
            }
            $query = CommonTDecisionSelectionEntrepriseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonTContactContrat($this)
                ->count($con);
        }

        return count($this->collCommonTDecisionSelectionEntreprises);
    }

    /**
     * Method called to associate a CommonTDecisionSelectionEntreprise object to this object
     * through the CommonTDecisionSelectionEntreprise foreign key attribute.
     *
     * @param   CommonTDecisionSelectionEntreprise $l CommonTDecisionSelectionEntreprise
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function addCommonTDecisionSelectionEntreprise(CommonTDecisionSelectionEntreprise $l)
    {
        if ($this->collCommonTDecisionSelectionEntreprises === null) {
            $this->initCommonTDecisionSelectionEntreprises();
            $this->collCommonTDecisionSelectionEntreprisesPartial = true;
        }
        if (!in_array($l, $this->collCommonTDecisionSelectionEntreprises->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonTDecisionSelectionEntreprise($l);
        }

        return $this;
    }

    /**
     * @param	CommonTDecisionSelectionEntreprise $commonTDecisionSelectionEntreprise The commonTDecisionSelectionEntreprise object to add.
     */
    protected function doAddCommonTDecisionSelectionEntreprise($commonTDecisionSelectionEntreprise)
    {
        $this->collCommonTDecisionSelectionEntreprises[]= $commonTDecisionSelectionEntreprise;
        $commonTDecisionSelectionEntreprise->setCommonTContactContrat($this);
    }

    /**
     * @param	CommonTDecisionSelectionEntreprise $commonTDecisionSelectionEntreprise The commonTDecisionSelectionEntreprise object to remove.
     * @return CommonTContactContrat The current object (for fluent API support)
     */
    public function removeCommonTDecisionSelectionEntreprise($commonTDecisionSelectionEntreprise)
    {
        if ($this->getCommonTDecisionSelectionEntreprises()->contains($commonTDecisionSelectionEntreprise)) {
            $this->collCommonTDecisionSelectionEntreprises->remove($this->collCommonTDecisionSelectionEntreprises->search($commonTDecisionSelectionEntreprise));
            if (null === $this->commonTDecisionSelectionEntreprisesScheduledForDeletion) {
                $this->commonTDecisionSelectionEntreprisesScheduledForDeletion = clone $this->collCommonTDecisionSelectionEntreprises;
                $this->commonTDecisionSelectionEntreprisesScheduledForDeletion->clear();
            }
            $this->commonTDecisionSelectionEntreprisesScheduledForDeletion[]= $commonTDecisionSelectionEntreprise;
            $commonTDecisionSelectionEntreprise->setCommonTContactContrat(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id_contact_contrat = null;
        $this->id_entreprise = null;
        $this->id_etablissement = null;
        $this->old_id_inscrit = null;
        $this->nom = null;
        $this->prenom = null;
        $this->email = null;
        $this->telephone = null;
        $this->fax = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->id_inscrit = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collCommonTContratTitulaires) {
                foreach ($this->collCommonTContratTitulaires as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCommonTDecisionSelectionEntreprises) {
                foreach ($this->collCommonTDecisionSelectionEntreprises as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->aCommonInscrit instanceof Persistent) {
              $this->aCommonInscrit->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collCommonTContratTitulaires instanceof PropelCollection) {
            $this->collCommonTContratTitulaires->clearIterator();
        }
        $this->collCommonTContratTitulaires = null;
        if ($this->collCommonTDecisionSelectionEntreprises instanceof PropelCollection) {
            $this->collCommonTDecisionSelectionEntreprises->clearIterator();
        }
        $this->collCommonTDecisionSelectionEntreprises = null;
        $this->aCommonInscrit = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTContactContratPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
