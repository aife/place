<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServiceQuery;
use Application\Propel\Mpe\CommonTFusionnerServices;
use Application\Propel\Mpe\CommonTFusionnerServicesPeer;
use Application\Propel\Mpe\CommonTFusionnerServicesQuery;

/**
 * Base class that represents a row from the 't_fusionner_services' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTFusionnerServices extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonTFusionnerServicesPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonTFusionnerServicesPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the id_service_source field.
     * @var        string
     */
    protected $id_service_source;

    /**
     * The value for the id_service_cible field.
     * @var        string
     */
    protected $id_service_cible;

    /**
     * The value for the old_id_service_source field.
     * @var        int
     */
    protected $old_id_service_source;

    /**
     * The value for the old_id_service_cible field.
     * @var        int
     */
    protected $old_id_service_cible;

    /**
     * The value for the organisme field.
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the id_agent field.
     * @var        int
     */
    protected $id_agent;

    /**
     * The value for the date_creation field.
     * @var        string
     */
    protected $date_creation;

    /**
     * The value for the date_fusion field.
     * @var        string
     */
    protected $date_fusion;

    /**
     * The value for the donnees_fusionnees field.
     * Note: this column has a database default value of: '0'
     * @var        string
     */
    protected $donnees_fusionnees;

    /**
     * @var        CommonService
     */
    protected $aCommonServiceRelatedByIdServiceCible;

    /**
     * @var        CommonService
     */
    protected $aCommonServiceRelatedByIdServiceSource;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->donnees_fusionnees = '0';
    }

    /**
     * Initializes internal state of BaseCommonTFusionnerServices object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [id_service_source] column value.
     *
     * @return string
     */
    public function getIdServiceSource()
    {

        return $this->id_service_source;
    }

    /**
     * Get the [id_service_cible] column value.
     *
     * @return string
     */
    public function getIdServiceCible()
    {

        return $this->id_service_cible;
    }

    /**
     * Get the [old_id_service_source] column value.
     *
     * @return int
     */
    public function getOldIdServiceSource()
    {

        return $this->old_id_service_source;
    }

    /**
     * Get the [old_id_service_cible] column value.
     *
     * @return int
     */
    public function getOldIdServiceCible()
    {

        return $this->old_id_service_cible;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [id_agent] column value.
     *
     * @return int
     */
    public function getIdAgent()
    {

        return $this->id_agent;
    }

    /**
     * Get the [optionally formatted] temporal [date_creation] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateCreation($format = 'Y-m-d H:i:s')
    {
        if ($this->date_creation === null) {
            return null;
        }

        if ($this->date_creation === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_creation);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_creation, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [date_fusion] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateFusion($format = 'Y-m-d H:i:s')
    {
        if ($this->date_fusion === null) {
            return null;
        }

        if ($this->date_fusion === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_fusion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_fusion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [donnees_fusionnees] column value.
     *
     * @return string
     */
    public function getDonneesFusionnees()
    {

        return $this->donnees_fusionnees;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonTFusionnerServices The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonTFusionnerServicesPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [id_service_source] column.
     *
     * @param string $v new value
     * @return CommonTFusionnerServices The current object (for fluent API support)
     */
    public function setIdServiceSource($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_service_source !== $v) {
            $this->id_service_source = $v;
            $this->modifiedColumns[] = CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE;
        }

        if ($this->aCommonServiceRelatedByIdServiceSource !== null && $this->aCommonServiceRelatedByIdServiceSource->getId() !== $v) {
            $this->aCommonServiceRelatedByIdServiceSource = null;
        }


        return $this;
    } // setIdServiceSource()

    /**
     * Set the value of [id_service_cible] column.
     *
     * @param string $v new value
     * @return CommonTFusionnerServices The current object (for fluent API support)
     */
    public function setIdServiceCible($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->id_service_cible !== $v) {
            $this->id_service_cible = $v;
            $this->modifiedColumns[] = CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE;
        }

        if ($this->aCommonServiceRelatedByIdServiceCible !== null && $this->aCommonServiceRelatedByIdServiceCible->getId() !== $v) {
            $this->aCommonServiceRelatedByIdServiceCible = null;
        }


        return $this;
    } // setIdServiceCible()

    /**
     * Set the value of [old_id_service_source] column.
     *
     * @param int $v new value
     * @return CommonTFusionnerServices The current object (for fluent API support)
     */
    public function setOldIdServiceSource($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_id_service_source !== $v) {
            $this->old_id_service_source = $v;
            $this->modifiedColumns[] = CommonTFusionnerServicesPeer::OLD_ID_SERVICE_SOURCE;
        }


        return $this;
    } // setOldIdServiceSource()

    /**
     * Set the value of [old_id_service_cible] column.
     *
     * @param int $v new value
     * @return CommonTFusionnerServices The current object (for fluent API support)
     */
    public function setOldIdServiceCible($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->old_id_service_cible !== $v) {
            $this->old_id_service_cible = $v;
            $this->modifiedColumns[] = CommonTFusionnerServicesPeer::OLD_ID_SERVICE_CIBLE;
        }


        return $this;
    } // setOldIdServiceCible()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonTFusionnerServices The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonTFusionnerServicesPeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [id_agent] column.
     *
     * @param int $v new value
     * @return CommonTFusionnerServices The current object (for fluent API support)
     */
    public function setIdAgent($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id_agent !== $v) {
            $this->id_agent = $v;
            $this->modifiedColumns[] = CommonTFusionnerServicesPeer::ID_AGENT;
        }


        return $this;
    } // setIdAgent()

    /**
     * Sets the value of [date_creation] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTFusionnerServices The current object (for fluent API support)
     */
    public function setDateCreation($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_creation !== null || $dt !== null) {
            $currentDateAsString = ($this->date_creation !== null && $tmpDt = new DateTime($this->date_creation)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_creation = $newDateAsString;
                $this->modifiedColumns[] = CommonTFusionnerServicesPeer::DATE_CREATION;
            }
        } // if either are not null


        return $this;
    } // setDateCreation()

    /**
     * Sets the value of [date_fusion] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonTFusionnerServices The current object (for fluent API support)
     */
    public function setDateFusion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_fusion !== null || $dt !== null) {
            $currentDateAsString = ($this->date_fusion !== null && $tmpDt = new DateTime($this->date_fusion)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_fusion = $newDateAsString;
                $this->modifiedColumns[] = CommonTFusionnerServicesPeer::DATE_FUSION;
            }
        } // if either are not null


        return $this;
    } // setDateFusion()

    /**
     * Set the value of [donnees_fusionnees] column.
     *
     * @param string $v new value
     * @return CommonTFusionnerServices The current object (for fluent API support)
     */
    public function setDonneesFusionnees($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->donnees_fusionnees !== $v) {
            $this->donnees_fusionnees = $v;
            $this->modifiedColumns[] = CommonTFusionnerServicesPeer::DONNEES_FUSIONNEES;
        }


        return $this;
    } // setDonneesFusionnees()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->donnees_fusionnees !== '0') {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->id_service_source = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->id_service_cible = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->old_id_service_source = ($row[$startcol + 3] !== null) ? (int) $row[$startcol + 3] : null;
            $this->old_id_service_cible = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->organisme = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->id_agent = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->date_creation = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->date_fusion = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->donnees_fusionnees = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 10; // 10 = CommonTFusionnerServicesPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonTFusionnerServices object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

        if ($this->aCommonServiceRelatedByIdServiceSource !== null && $this->id_service_source !== $this->aCommonServiceRelatedByIdServiceSource->getId()) {
            $this->aCommonServiceRelatedByIdServiceSource = null;
        }
        if ($this->aCommonServiceRelatedByIdServiceCible !== null && $this->id_service_cible !== $this->aCommonServiceRelatedByIdServiceCible->getId()) {
            $this->aCommonServiceRelatedByIdServiceCible = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonTFusionnerServicesPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCommonServiceRelatedByIdServiceCible = null;
            $this->aCommonServiceRelatedByIdServiceSource = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonTFusionnerServicesQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonTFusionnerServicesPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonTFusionnerServicesPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonServiceRelatedByIdServiceCible !== null) {
                if ($this->aCommonServiceRelatedByIdServiceCible->isModified() || $this->aCommonServiceRelatedByIdServiceCible->isNew()) {
                    $affectedRows += $this->aCommonServiceRelatedByIdServiceCible->save($con);
                }
                $this->setCommonServiceRelatedByIdServiceCible($this->aCommonServiceRelatedByIdServiceCible);
            }

            if ($this->aCommonServiceRelatedByIdServiceSource !== null) {
                if ($this->aCommonServiceRelatedByIdServiceSource->isModified() || $this->aCommonServiceRelatedByIdServiceSource->isNew()) {
                    $affectedRows += $this->aCommonServiceRelatedByIdServiceSource->save($con);
                }
                $this->setCommonServiceRelatedByIdServiceSource($this->aCommonServiceRelatedByIdServiceSource);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonTFusionnerServicesPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonTFusionnerServicesPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE)) {
            $modifiedColumns[':p' . $index++]  = '`id_service_source`';
        }
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE)) {
            $modifiedColumns[':p' . $index++]  = '`id_service_cible`';
        }
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_SOURCE)) {
            $modifiedColumns[':p' . $index++]  = '`old_id_service_source`';
        }
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_CIBLE)) {
            $modifiedColumns[':p' . $index++]  = '`old_id_service_cible`';
        }
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::ID_AGENT)) {
            $modifiedColumns[':p' . $index++]  = '`id_agent`';
        }
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::DATE_CREATION)) {
            $modifiedColumns[':p' . $index++]  = '`date_creation`';
        }
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::DATE_FUSION)) {
            $modifiedColumns[':p' . $index++]  = '`date_fusion`';
        }
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::DONNEES_FUSIONNEES)) {
            $modifiedColumns[':p' . $index++]  = '`donnees_fusionnees`';
        }

        $sql = sprintf(
            'INSERT INTO `t_fusionner_services` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`id_service_source`':
                        $stmt->bindValue($identifier, $this->id_service_source, PDO::PARAM_STR);
                        break;
                    case '`id_service_cible`':
                        $stmt->bindValue($identifier, $this->id_service_cible, PDO::PARAM_STR);
                        break;
                    case '`old_id_service_source`':
                        $stmt->bindValue($identifier, $this->old_id_service_source, PDO::PARAM_INT);
                        break;
                    case '`old_id_service_cible`':
                        $stmt->bindValue($identifier, $this->old_id_service_cible, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`id_agent`':
                        $stmt->bindValue($identifier, $this->id_agent, PDO::PARAM_INT);
                        break;
                    case '`date_creation`':
                        $stmt->bindValue($identifier, $this->date_creation, PDO::PARAM_STR);
                        break;
                    case '`date_fusion`':
                        $stmt->bindValue($identifier, $this->date_fusion, PDO::PARAM_STR);
                        break;
                    case '`donnees_fusionnees`':
                        $stmt->bindValue($identifier, $this->donnees_fusionnees, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            // We call the validate method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCommonServiceRelatedByIdServiceCible !== null) {
                if (!$this->aCommonServiceRelatedByIdServiceCible->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonServiceRelatedByIdServiceCible->getValidationFailures());
                }
            }

            if ($this->aCommonServiceRelatedByIdServiceSource !== null) {
                if (!$this->aCommonServiceRelatedByIdServiceSource->validate($columns)) {
                    $failureMap = array_merge($failureMap, $this->aCommonServiceRelatedByIdServiceSource->getValidationFailures());
                }
            }


            if (($retval = CommonTFusionnerServicesPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTFusionnerServicesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getIdServiceSource();
                break;
            case 2:
                return $this->getIdServiceCible();
                break;
            case 3:
                return $this->getOldIdServiceSource();
                break;
            case 4:
                return $this->getOldIdServiceCible();
                break;
            case 5:
                return $this->getOrganisme();
                break;
            case 6:
                return $this->getIdAgent();
                break;
            case 7:
                return $this->getDateCreation();
                break;
            case 8:
                return $this->getDateFusion();
                break;
            case 9:
                return $this->getDonneesFusionnees();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonTFusionnerServices'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonTFusionnerServices'][$this->getPrimaryKey()] = true;
        $keys = CommonTFusionnerServicesPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getIdServiceSource(),
            $keys[2] => $this->getIdServiceCible(),
            $keys[3] => $this->getOldIdServiceSource(),
            $keys[4] => $this->getOldIdServiceCible(),
            $keys[5] => $this->getOrganisme(),
            $keys[6] => $this->getIdAgent(),
            $keys[7] => $this->getDateCreation(),
            $keys[8] => $this->getDateFusion(),
            $keys[9] => $this->getDonneesFusionnees(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->aCommonServiceRelatedByIdServiceCible) {
                $result['CommonServiceRelatedByIdServiceCible'] = $this->aCommonServiceRelatedByIdServiceCible->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCommonServiceRelatedByIdServiceSource) {
                $result['CommonServiceRelatedByIdServiceSource'] = $this->aCommonServiceRelatedByIdServiceSource->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonTFusionnerServicesPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setIdServiceSource($value);
                break;
            case 2:
                $this->setIdServiceCible($value);
                break;
            case 3:
                $this->setOldIdServiceSource($value);
                break;
            case 4:
                $this->setOldIdServiceCible($value);
                break;
            case 5:
                $this->setOrganisme($value);
                break;
            case 6:
                $this->setIdAgent($value);
                break;
            case 7:
                $this->setDateCreation($value);
                break;
            case 8:
                $this->setDateFusion($value);
                break;
            case 9:
                $this->setDonneesFusionnees($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonTFusionnerServicesPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setIdServiceSource($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setIdServiceCible($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setOldIdServiceSource($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setOldIdServiceCible($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setOrganisme($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setIdAgent($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setDateCreation($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setDateFusion($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setDonneesFusionnees($arr[$keys[9]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonTFusionnerServicesPeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonTFusionnerServicesPeer::ID)) $criteria->add(CommonTFusionnerServicesPeer::ID, $this->id);
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE)) $criteria->add(CommonTFusionnerServicesPeer::ID_SERVICE_SOURCE, $this->id_service_source);
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE)) $criteria->add(CommonTFusionnerServicesPeer::ID_SERVICE_CIBLE, $this->id_service_cible);
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_SOURCE)) $criteria->add(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_SOURCE, $this->old_id_service_source);
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_CIBLE)) $criteria->add(CommonTFusionnerServicesPeer::OLD_ID_SERVICE_CIBLE, $this->old_id_service_cible);
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::ORGANISME)) $criteria->add(CommonTFusionnerServicesPeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::ID_AGENT)) $criteria->add(CommonTFusionnerServicesPeer::ID_AGENT, $this->id_agent);
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::DATE_CREATION)) $criteria->add(CommonTFusionnerServicesPeer::DATE_CREATION, $this->date_creation);
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::DATE_FUSION)) $criteria->add(CommonTFusionnerServicesPeer::DATE_FUSION, $this->date_fusion);
        if ($this->isColumnModified(CommonTFusionnerServicesPeer::DONNEES_FUSIONNEES)) $criteria->add(CommonTFusionnerServicesPeer::DONNEES_FUSIONNEES, $this->donnees_fusionnees);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonTFusionnerServicesPeer::DATABASE_NAME);
        $criteria->add(CommonTFusionnerServicesPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonTFusionnerServices (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdServiceSource($this->getIdServiceSource());
        $copyObj->setIdServiceCible($this->getIdServiceCible());
        $copyObj->setOldIdServiceSource($this->getOldIdServiceSource());
        $copyObj->setOldIdServiceCible($this->getOldIdServiceCible());
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setIdAgent($this->getIdAgent());
        $copyObj->setDateCreation($this->getDateCreation());
        $copyObj->setDateFusion($this->getDateFusion());
        $copyObj->setDonneesFusionnees($this->getDonneesFusionnees());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonTFusionnerServices Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonTFusionnerServicesPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonTFusionnerServicesPeer();
        }

        return self::$peer;
    }

    /**
     * Declares an association between this object and a CommonService object.
     *
     * @param   CommonService $v
     * @return CommonTFusionnerServices The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonServiceRelatedByIdServiceCible(CommonService $v = null)
    {
        if ($v === null) {
            $this->setIdServiceCible(NULL);
        } else {
            $this->setIdServiceCible($v->getId());
        }

        $this->aCommonServiceRelatedByIdServiceCible = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonService object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTFusionnerServicesRelatedByIdServiceCible($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonService object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonService The associated CommonService object.
     * @throws PropelException
     */
    public function getCommonServiceRelatedByIdServiceCible(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonServiceRelatedByIdServiceCible === null && (($this->id_service_cible !== "" && $this->id_service_cible !== null)) && $doQuery) {
            $this->aCommonServiceRelatedByIdServiceCible = CommonServiceQuery::create()->findPk($this->id_service_cible, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonServiceRelatedByIdServiceCible->addCommonTFusionnerServicessRelatedByIdServiceCible($this);
             */
        }

        return $this->aCommonServiceRelatedByIdServiceCible;
    }

    /**
     * Declares an association between this object and a CommonService object.
     *
     * @param   CommonService $v
     * @return CommonTFusionnerServices The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonServiceRelatedByIdServiceSource(CommonService $v = null)
    {
        if ($v === null) {
            $this->setIdServiceSource(NULL);
        } else {
            $this->setIdServiceSource($v->getId());
        }

        $this->aCommonServiceRelatedByIdServiceSource = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the CommonService object, it will not be re-added.
        if ($v !== null) {
            $v->addCommonTFusionnerServicesRelatedByIdServiceSource($this);
        }


        return $this;
    }


    /**
     * Get the associated CommonService object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return CommonService The associated CommonService object.
     * @throws PropelException
     */
    public function getCommonServiceRelatedByIdServiceSource(PropelPDO $con = null, $doQuery = true)
    {
        if ($this->aCommonServiceRelatedByIdServiceSource === null && (($this->id_service_source !== "" && $this->id_service_source !== null)) && $doQuery) {
            $this->aCommonServiceRelatedByIdServiceSource = CommonServiceQuery::create()->findPk($this->id_service_source, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCommonServiceRelatedByIdServiceSource->addCommonTFusionnerServicessRelatedByIdServiceSource($this);
             */
        }

        return $this->aCommonServiceRelatedByIdServiceSource;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->id_service_source = null;
        $this->id_service_cible = null;
        $this->old_id_service_source = null;
        $this->old_id_service_cible = null;
        $this->organisme = null;
        $this->id_agent = null;
        $this->date_creation = null;
        $this->date_fusion = null;
        $this->donnees_fusionnees = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->aCommonServiceRelatedByIdServiceCible instanceof Persistent) {
              $this->aCommonServiceRelatedByIdServiceCible->clearAllReferences($deep);
            }
            if ($this->aCommonServiceRelatedByIdServiceSource instanceof Persistent) {
              $this->aCommonServiceRelatedByIdServiceSource->clearAllReferences($deep);
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        $this->aCommonServiceRelatedByIdServiceCible = null;
        $this->aCommonServiceRelatedByIdServiceSource = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonTFusionnerServicesPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
