<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonBlobFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDossierVolumineux;
use Application\Propel\Mpe\CommonDossierVolumineuxPeer;
use Application\Propel\Mpe\CommonDossierVolumineuxQuery;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonInscrit;

/**
 * Base class that represents a query for the 'dossier_volumineux' table.
 *
 *
 *
 * @method CommonDossierVolumineuxQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonDossierVolumineuxQuery orderByUuidReference($order = Criteria::ASC) Order by the uuid_reference column
 * @method CommonDossierVolumineuxQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method CommonDossierVolumineuxQuery orderByTaille($order = Criteria::ASC) Order by the taille column
 * @method CommonDossierVolumineuxQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonDossierVolumineuxQuery orderByStatut($order = Criteria::ASC) Order by the statut column
 * @method CommonDossierVolumineuxQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 * @method CommonDossierVolumineuxQuery orderByOldIdInscrit($order = Criteria::ASC) Order by the old_id_inscrit column
 * @method CommonDossierVolumineuxQuery orderByIdBlobDescripteur($order = Criteria::ASC) Order by the id_blob_descripteur column
 * @method CommonDossierVolumineuxQuery orderByUuidTechnique($order = Criteria::ASC) Order by the uuid_technique column
 * @method CommonDossierVolumineuxQuery orderByActif($order = Criteria::ASC) Order by the actif column
 * @method CommonDossierVolumineuxQuery orderByIdBlobLogfile($order = Criteria::ASC) Order by the id_blob_logfile column
 * @method CommonDossierVolumineuxQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonDossierVolumineuxQuery orderByIdInscrit($order = Criteria::ASC) Order by the id_inscrit column
 *
 * @method CommonDossierVolumineuxQuery groupById() Group by the id column
 * @method CommonDossierVolumineuxQuery groupByUuidReference() Group by the uuid_reference column
 * @method CommonDossierVolumineuxQuery groupByNom() Group by the nom column
 * @method CommonDossierVolumineuxQuery groupByTaille() Group by the taille column
 * @method CommonDossierVolumineuxQuery groupByDateCreation() Group by the date_creation column
 * @method CommonDossierVolumineuxQuery groupByStatut() Group by the statut column
 * @method CommonDossierVolumineuxQuery groupByIdAgent() Group by the id_agent column
 * @method CommonDossierVolumineuxQuery groupByOldIdInscrit() Group by the old_id_inscrit column
 * @method CommonDossierVolumineuxQuery groupByIdBlobDescripteur() Group by the id_blob_descripteur column
 * @method CommonDossierVolumineuxQuery groupByUuidTechnique() Group by the uuid_technique column
 * @method CommonDossierVolumineuxQuery groupByActif() Group by the actif column
 * @method CommonDossierVolumineuxQuery groupByIdBlobLogfile() Group by the id_blob_logfile column
 * @method CommonDossierVolumineuxQuery groupByDateModification() Group by the date_modification column
 * @method CommonDossierVolumineuxQuery groupByIdInscrit() Group by the id_inscrit column
 *
 * @method CommonDossierVolumineuxQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonDossierVolumineuxQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonDossierVolumineuxQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonDossierVolumineuxQuery leftJoinCommonInscrit($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonInscrit relation
 * @method CommonDossierVolumineuxQuery rightJoinCommonInscrit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonInscrit relation
 * @method CommonDossierVolumineuxQuery innerJoinCommonInscrit($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonInscrit relation
 *
 * @method CommonDossierVolumineuxQuery leftJoinCommonBlobFileRelatedByIdBlobDescripteur($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonBlobFileRelatedByIdBlobDescripteur relation
 * @method CommonDossierVolumineuxQuery rightJoinCommonBlobFileRelatedByIdBlobDescripteur($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonBlobFileRelatedByIdBlobDescripteur relation
 * @method CommonDossierVolumineuxQuery innerJoinCommonBlobFileRelatedByIdBlobDescripteur($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonBlobFileRelatedByIdBlobDescripteur relation
 *
 * @method CommonDossierVolumineuxQuery leftJoinCommonBlobFileRelatedByIdBlobLogfile($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonBlobFileRelatedByIdBlobLogfile relation
 * @method CommonDossierVolumineuxQuery rightJoinCommonBlobFileRelatedByIdBlobLogfile($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonBlobFileRelatedByIdBlobLogfile relation
 * @method CommonDossierVolumineuxQuery innerJoinCommonBlobFileRelatedByIdBlobLogfile($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonBlobFileRelatedByIdBlobLogfile relation
 *
 * @method CommonDossierVolumineuxQuery leftJoinCommonAgent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonAgent relation
 * @method CommonDossierVolumineuxQuery rightJoinCommonAgent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonAgent relation
 * @method CommonDossierVolumineuxQuery innerJoinCommonAgent($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonAgent relation
 *
 * @method CommonDossierVolumineuxQuery leftJoinCommonEchange($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchange relation
 * @method CommonDossierVolumineuxQuery rightJoinCommonEchange($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchange relation
 * @method CommonDossierVolumineuxQuery innerJoinCommonEchange($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchange relation
 *
 * @method CommonDossierVolumineuxQuery leftJoinCommonEnveloppe($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEnveloppe relation
 * @method CommonDossierVolumineuxQuery rightJoinCommonEnveloppe($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEnveloppe relation
 * @method CommonDossierVolumineuxQuery innerJoinCommonEnveloppe($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEnveloppe relation
 *
 * @method CommonDossierVolumineuxQuery leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonDossierVolumineuxQuery rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonDossierVolumineuxQuery innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonDossierVolumineux findOne(PropelPDO $con = null) Return the first CommonDossierVolumineux matching the query
 * @method CommonDossierVolumineux findOneOrCreate(PropelPDO $con = null) Return the first CommonDossierVolumineux matching the query, or a new CommonDossierVolumineux object populated from the query conditions when no match is found
 *
 * @method CommonDossierVolumineux findOneByUuidReference(string $uuid_reference) Return the first CommonDossierVolumineux filtered by the uuid_reference column
 * @method CommonDossierVolumineux findOneByNom(string $nom) Return the first CommonDossierVolumineux filtered by the nom column
 * @method CommonDossierVolumineux findOneByTaille(string $taille) Return the first CommonDossierVolumineux filtered by the taille column
 * @method CommonDossierVolumineux findOneByDateCreation(string $date_creation) Return the first CommonDossierVolumineux filtered by the date_creation column
 * @method CommonDossierVolumineux findOneByStatut(string $statut) Return the first CommonDossierVolumineux filtered by the statut column
 * @method CommonDossierVolumineux findOneByIdAgent(int $id_agent) Return the first CommonDossierVolumineux filtered by the id_agent column
 * @method CommonDossierVolumineux findOneByOldIdInscrit(int $old_id_inscrit) Return the first CommonDossierVolumineux filtered by the old_id_inscrit column
 * @method CommonDossierVolumineux findOneByIdBlobDescripteur(int $id_blob_descripteur) Return the first CommonDossierVolumineux filtered by the id_blob_descripteur column
 * @method CommonDossierVolumineux findOneByUuidTechnique(string $uuid_technique) Return the first CommonDossierVolumineux filtered by the uuid_technique column
 * @method CommonDossierVolumineux findOneByActif(boolean $actif) Return the first CommonDossierVolumineux filtered by the actif column
 * @method CommonDossierVolumineux findOneByIdBlobLogfile(int $id_blob_logfile) Return the first CommonDossierVolumineux filtered by the id_blob_logfile column
 * @method CommonDossierVolumineux findOneByDateModification(string $date_modification) Return the first CommonDossierVolumineux filtered by the date_modification column
 * @method CommonDossierVolumineux findOneByIdInscrit(string $id_inscrit) Return the first CommonDossierVolumineux filtered by the id_inscrit column
 *
 * @method array findById(int $id) Return CommonDossierVolumineux objects filtered by the id column
 * @method array findByUuidReference(string $uuid_reference) Return CommonDossierVolumineux objects filtered by the uuid_reference column
 * @method array findByNom(string $nom) Return CommonDossierVolumineux objects filtered by the nom column
 * @method array findByTaille(string $taille) Return CommonDossierVolumineux objects filtered by the taille column
 * @method array findByDateCreation(string $date_creation) Return CommonDossierVolumineux objects filtered by the date_creation column
 * @method array findByStatut(string $statut) Return CommonDossierVolumineux objects filtered by the statut column
 * @method array findByIdAgent(int $id_agent) Return CommonDossierVolumineux objects filtered by the id_agent column
 * @method array findByOldIdInscrit(int $old_id_inscrit) Return CommonDossierVolumineux objects filtered by the old_id_inscrit column
 * @method array findByIdBlobDescripteur(int $id_blob_descripteur) Return CommonDossierVolumineux objects filtered by the id_blob_descripteur column
 * @method array findByUuidTechnique(string $uuid_technique) Return CommonDossierVolumineux objects filtered by the uuid_technique column
 * @method array findByActif(boolean $actif) Return CommonDossierVolumineux objects filtered by the actif column
 * @method array findByIdBlobLogfile(int $id_blob_logfile) Return CommonDossierVolumineux objects filtered by the id_blob_logfile column
 * @method array findByDateModification(string $date_modification) Return CommonDossierVolumineux objects filtered by the date_modification column
 * @method array findByIdInscrit(string $id_inscrit) Return CommonDossierVolumineux objects filtered by the id_inscrit column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonDossierVolumineuxQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonDossierVolumineuxQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonDossierVolumineux', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonDossierVolumineuxQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonDossierVolumineuxQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonDossierVolumineuxQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonDossierVolumineuxQuery) {
            return $criteria;
        }
        $query = new CommonDossierVolumineuxQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonDossierVolumineux|CommonDossierVolumineux[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonDossierVolumineuxPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonDossierVolumineuxPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonDossierVolumineux A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonDossierVolumineux A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `uuid_reference`, `nom`, `taille`, `date_creation`, `statut`, `id_agent`, `old_id_inscrit`, `id_blob_descripteur`, `uuid_technique`, `actif`, `id_blob_logfile`, `date_modification`, `id_inscrit` FROM `dossier_volumineux` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonDossierVolumineux();
            $obj->hydrate($row);
            CommonDossierVolumineuxPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonDossierVolumineux|CommonDossierVolumineux[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonDossierVolumineux[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the uuid_reference column
     *
     * Example usage:
     * <code>
     * $query->filterByUuidReference('fooValue');   // WHERE uuid_reference = 'fooValue'
     * $query->filterByUuidReference('%fooValue%'); // WHERE uuid_reference LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uuidReference The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByUuidReference($uuidReference = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuidReference)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $uuidReference)) {
                $uuidReference = str_replace('*', '%', $uuidReference);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::UUID_REFERENCE, $uuidReference, $comparison);
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%'); // WHERE nom LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nom The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByNom($nom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nom)) {
                $nom = str_replace('*', '%', $nom);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::NOM, $nom, $comparison);
    }

    /**
     * Filter the query on the taille column
     *
     * Example usage:
     * <code>
     * $query->filterByTaille(1234); // WHERE taille = 1234
     * $query->filterByTaille(array(12, 34)); // WHERE taille IN (12, 34)
     * $query->filterByTaille(array('min' => 12)); // WHERE taille >= 12
     * $query->filterByTaille(array('max' => 12)); // WHERE taille <= 12
     * </code>
     *
     * @param     mixed $taille The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByTaille($taille = null, $comparison = null)
    {
        if (is_array($taille)) {
            $useMinMax = false;
            if (isset($taille['min'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::TAILLE, $taille['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($taille['max'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::TAILLE, $taille['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::TAILLE, $taille, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the statut column
     *
     * Example usage:
     * <code>
     * $query->filterByStatut('fooValue');   // WHERE statut = 'fooValue'
     * $query->filterByStatut('%fooValue%'); // WHERE statut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statut The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByStatut($statut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statut)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $statut)) {
                $statut = str_replace('*', '%', $statut);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::STATUT, $statut, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @see       filterByCommonAgent()
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Filter the query on the old_id_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByOldIdInscrit(1234); // WHERE old_id_inscrit = 1234
     * $query->filterByOldIdInscrit(array(12, 34)); // WHERE old_id_inscrit IN (12, 34)
     * $query->filterByOldIdInscrit(array('min' => 12)); // WHERE old_id_inscrit >= 12
     * $query->filterByOldIdInscrit(array('max' => 12)); // WHERE old_id_inscrit <= 12
     * </code>
     *
     * @param     mixed $oldIdInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByOldIdInscrit($oldIdInscrit = null, $comparison = null)
    {
        if (is_array($oldIdInscrit)) {
            $useMinMax = false;
            if (isset($oldIdInscrit['min'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::OLD_ID_INSCRIT, $oldIdInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldIdInscrit['max'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::OLD_ID_INSCRIT, $oldIdInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::OLD_ID_INSCRIT, $oldIdInscrit, $comparison);
    }

    /**
     * Filter the query on the id_blob_descripteur column
     *
     * Example usage:
     * <code>
     * $query->filterByIdBlobDescripteur(1234); // WHERE id_blob_descripteur = 1234
     * $query->filterByIdBlobDescripteur(array(12, 34)); // WHERE id_blob_descripteur IN (12, 34)
     * $query->filterByIdBlobDescripteur(array('min' => 12)); // WHERE id_blob_descripteur >= 12
     * $query->filterByIdBlobDescripteur(array('max' => 12)); // WHERE id_blob_descripteur <= 12
     * </code>
     *
     * @see       filterByCommonBlobFileRelatedByIdBlobDescripteur()
     *
     * @param     mixed $idBlobDescripteur The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByIdBlobDescripteur($idBlobDescripteur = null, $comparison = null)
    {
        if (is_array($idBlobDescripteur)) {
            $useMinMax = false;
            if (isset($idBlobDescripteur['min'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_BLOB_DESCRIPTEUR, $idBlobDescripteur['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idBlobDescripteur['max'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_BLOB_DESCRIPTEUR, $idBlobDescripteur['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_BLOB_DESCRIPTEUR, $idBlobDescripteur, $comparison);
    }

    /**
     * Filter the query on the uuid_technique column
     *
     * Example usage:
     * <code>
     * $query->filterByUuidTechnique('fooValue');   // WHERE uuid_technique = 'fooValue'
     * $query->filterByUuidTechnique('%fooValue%'); // WHERE uuid_technique LIKE '%fooValue%'
     * </code>
     *
     * @param     string $uuidTechnique The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByUuidTechnique($uuidTechnique = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($uuidTechnique)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $uuidTechnique)) {
                $uuidTechnique = str_replace('*', '%', $uuidTechnique);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::UUID_TECHNIQUE, $uuidTechnique, $comparison);
    }

    /**
     * Filter the query on the actif column
     *
     * Example usage:
     * <code>
     * $query->filterByActif(true); // WHERE actif = true
     * $query->filterByActif('yes'); // WHERE actif = true
     * </code>
     *
     * @param     boolean|string $actif The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByActif($actif = null, $comparison = null)
    {
        if (is_string($actif)) {
            $actif = in_array(strtolower($actif), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::ACTIF, $actif, $comparison);
    }

    /**
     * Filter the query on the id_blob_logfile column
     *
     * Example usage:
     * <code>
     * $query->filterByIdBlobLogfile(1234); // WHERE id_blob_logfile = 1234
     * $query->filterByIdBlobLogfile(array(12, 34)); // WHERE id_blob_logfile IN (12, 34)
     * $query->filterByIdBlobLogfile(array('min' => 12)); // WHERE id_blob_logfile >= 12
     * $query->filterByIdBlobLogfile(array('max' => 12)); // WHERE id_blob_logfile <= 12
     * </code>
     *
     * @see       filterByCommonBlobFileRelatedByIdBlobLogfile()
     *
     * @param     mixed $idBlobLogfile The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByIdBlobLogfile($idBlobLogfile = null, $comparison = null)
    {
        if (is_array($idBlobLogfile)) {
            $useMinMax = false;
            if (isset($idBlobLogfile['min'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_BLOB_LOGFILE, $idBlobLogfile['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idBlobLogfile['max'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_BLOB_LOGFILE, $idBlobLogfile['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_BLOB_LOGFILE, $idBlobLogfile, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('2011-03-14'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification('now'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification(array('max' => 'yesterday')); // WHERE date_modification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (is_array($dateModification)) {
            $useMinMax = false;
            if (isset($dateModification['min'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::DATE_MODIFICATION, $dateModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModification['max'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::DATE_MODIFICATION, $dateModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the id_inscrit column
     *
     * Example usage:
     * <code>
     * $query->filterByIdInscrit(1234); // WHERE id_inscrit = 1234
     * $query->filterByIdInscrit(array(12, 34)); // WHERE id_inscrit IN (12, 34)
     * $query->filterByIdInscrit(array('min' => 12)); // WHERE id_inscrit >= 12
     * $query->filterByIdInscrit(array('max' => 12)); // WHERE id_inscrit <= 12
     * </code>
     *
     * @see       filterByCommonInscrit()
     *
     * @param     mixed $idInscrit The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function filterByIdInscrit($idInscrit = null, $comparison = null)
    {
        if (is_array($idInscrit)) {
            $useMinMax = false;
            if (isset($idInscrit['min'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_INSCRIT, $idInscrit['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idInscrit['max'])) {
                $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_INSCRIT, $idInscrit['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonDossierVolumineuxPeer::ID_INSCRIT, $idInscrit, $comparison);
    }

    /**
     * Filter the query by a related CommonInscrit object
     *
     * @param   CommonInscrit|PropelObjectCollection $commonInscrit The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonDossierVolumineuxQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonInscrit($commonInscrit, $comparison = null)
    {
        if ($commonInscrit instanceof CommonInscrit) {
            return $this
                ->addUsingAlias(CommonDossierVolumineuxPeer::ID_INSCRIT, $commonInscrit->getId(), $comparison);
        } elseif ($commonInscrit instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonDossierVolumineuxPeer::ID_INSCRIT, $commonInscrit->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonInscrit() only accepts arguments of type CommonInscrit or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonInscrit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function joinCommonInscrit($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonInscrit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonInscrit');
        }

        return $this;
    }

    /**
     * Use the CommonInscrit relation CommonInscrit object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonInscritQuery A secondary query class using the current class as primary query
     */
    public function useCommonInscritQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonInscrit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonInscrit', '\Application\Propel\Mpe\CommonInscritQuery');
    }

    /**
     * Filter the query by a related CommonBlobFile object
     *
     * @param   CommonBlobFile|PropelObjectCollection $commonBlobFile The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonDossierVolumineuxQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonBlobFileRelatedByIdBlobDescripteur($commonBlobFile, $comparison = null)
    {
        if ($commonBlobFile instanceof CommonBlobFile) {
            return $this
                ->addUsingAlias(CommonDossierVolumineuxPeer::ID_BLOB_DESCRIPTEUR, $commonBlobFile->getId(), $comparison);
        } elseif ($commonBlobFile instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonDossierVolumineuxPeer::ID_BLOB_DESCRIPTEUR, $commonBlobFile->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonBlobFileRelatedByIdBlobDescripteur() only accepts arguments of type CommonBlobFile or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonBlobFileRelatedByIdBlobDescripteur relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function joinCommonBlobFileRelatedByIdBlobDescripteur($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonBlobFileRelatedByIdBlobDescripteur');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonBlobFileRelatedByIdBlobDescripteur');
        }

        return $this;
    }

    /**
     * Use the CommonBlobFileRelatedByIdBlobDescripteur relation CommonBlobFile object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonBlobFileQuery A secondary query class using the current class as primary query
     */
    public function useCommonBlobFileRelatedByIdBlobDescripteurQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonBlobFileRelatedByIdBlobDescripteur($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonBlobFileRelatedByIdBlobDescripteur', '\Application\Propel\Mpe\CommonBlobFileQuery');
    }

    /**
     * Filter the query by a related CommonBlobFile object
     *
     * @param   CommonBlobFile|PropelObjectCollection $commonBlobFile The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonDossierVolumineuxQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonBlobFileRelatedByIdBlobLogfile($commonBlobFile, $comparison = null)
    {
        if ($commonBlobFile instanceof CommonBlobFile) {
            return $this
                ->addUsingAlias(CommonDossierVolumineuxPeer::ID_BLOB_LOGFILE, $commonBlobFile->getId(), $comparison);
        } elseif ($commonBlobFile instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonDossierVolumineuxPeer::ID_BLOB_LOGFILE, $commonBlobFile->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonBlobFileRelatedByIdBlobLogfile() only accepts arguments of type CommonBlobFile or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonBlobFileRelatedByIdBlobLogfile relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function joinCommonBlobFileRelatedByIdBlobLogfile($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonBlobFileRelatedByIdBlobLogfile');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonBlobFileRelatedByIdBlobLogfile');
        }

        return $this;
    }

    /**
     * Use the CommonBlobFileRelatedByIdBlobLogfile relation CommonBlobFile object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonBlobFileQuery A secondary query class using the current class as primary query
     */
    public function useCommonBlobFileRelatedByIdBlobLogfileQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonBlobFileRelatedByIdBlobLogfile($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonBlobFileRelatedByIdBlobLogfile', '\Application\Propel\Mpe\CommonBlobFileQuery');
    }

    /**
     * Filter the query by a related CommonAgent object
     *
     * @param   CommonAgent|PropelObjectCollection $commonAgent The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonDossierVolumineuxQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonAgent($commonAgent, $comparison = null)
    {
        if ($commonAgent instanceof CommonAgent) {
            return $this
                ->addUsingAlias(CommonDossierVolumineuxPeer::ID_AGENT, $commonAgent->getId(), $comparison);
        } elseif ($commonAgent instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonDossierVolumineuxPeer::ID_AGENT, $commonAgent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonAgent() only accepts arguments of type CommonAgent or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonAgent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function joinCommonAgent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonAgent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonAgent');
        }

        return $this;
    }

    /**
     * Use the CommonAgent relation CommonAgent object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonAgentQuery A secondary query class using the current class as primary query
     */
    public function useCommonAgentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonAgent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonAgent', '\Application\Propel\Mpe\CommonAgentQuery');
    }

    /**
     * Filter the query by a related CommonEchange object
     *
     * @param   CommonEchange|PropelObjectCollection $commonEchange  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonDossierVolumineuxQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchange($commonEchange, $comparison = null)
    {
        if ($commonEchange instanceof CommonEchange) {
            return $this
                ->addUsingAlias(CommonDossierVolumineuxPeer::ID, $commonEchange->getIdDossierVolumineux(), $comparison);
        } elseif ($commonEchange instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeQuery()
                ->filterByPrimaryKeys($commonEchange->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchange() only accepts arguments of type CommonEchange or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchange relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function joinCommonEchange($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchange');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchange');
        }

        return $this;
    }

    /**
     * Use the CommonEchange relation CommonEchange object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchange($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchange', '\Application\Propel\Mpe\CommonEchangeQuery');
    }

    /**
     * Filter the query by a related CommonEnveloppe object
     *
     * @param   CommonEnveloppe|PropelObjectCollection $commonEnveloppe  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonDossierVolumineuxQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEnveloppe($commonEnveloppe, $comparison = null)
    {
        if ($commonEnveloppe instanceof CommonEnveloppe) {
            return $this
                ->addUsingAlias(CommonDossierVolumineuxPeer::ID, $commonEnveloppe->getIdDossierVolumineux(), $comparison);
        } elseif ($commonEnveloppe instanceof PropelObjectCollection) {
            return $this
                ->useCommonEnveloppeQuery()
                ->filterByPrimaryKeys($commonEnveloppe->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEnveloppe() only accepts arguments of type CommonEnveloppe or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEnveloppe relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function joinCommonEnveloppe($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEnveloppe');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEnveloppe');
        }

        return $this;
    }

    /**
     * Use the CommonEnveloppe relation CommonEnveloppe object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEnveloppeQuery A secondary query class using the current class as primary query
     */
    public function useCommonEnveloppeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEnveloppe($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEnveloppe', '\Application\Propel\Mpe\CommonEnveloppeQuery');
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonDossierVolumineuxQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonDossierVolumineuxPeer::ID, $commonConsultation->getIdDossierVolumineux(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            return $this
                ->useCommonConsultationQuery()
                ->filterByPrimaryKeys($commonConsultation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonDossierVolumineux $commonDossierVolumineux Object to remove from the list of results
     *
     * @return CommonDossierVolumineuxQuery The current query, for fluid interface
     */
    public function prune($commonDossierVolumineux = null)
    {
        if ($commonDossierVolumineux) {
            $this->addUsingAlias(CommonDossierVolumineuxPeer::ID, $commonDossierVolumineux->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
