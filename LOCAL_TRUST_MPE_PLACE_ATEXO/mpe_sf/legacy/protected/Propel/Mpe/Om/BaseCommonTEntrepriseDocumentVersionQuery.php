<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTDocumentEntreprise;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersion;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersionPeer;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersionQuery;

/**
 * Base class that represents a query for the 't_entreprise_document_version' table.
 *
 *
 *
 * @method CommonTEntrepriseDocumentVersionQuery orderByIdVersionDocument($order = Criteria::ASC) Order by the id_version_document column
 * @method CommonTEntrepriseDocumentVersionQuery orderByIdDocument($order = Criteria::ASC) Order by the id_document column
 * @method CommonTEntrepriseDocumentVersionQuery orderByDateRecuperation($order = Criteria::ASC) Order by the date_recuperation column
 * @method CommonTEntrepriseDocumentVersionQuery orderByHash($order = Criteria::ASC) Order by the hash column
 * @method CommonTEntrepriseDocumentVersionQuery orderByIdBlob($order = Criteria::ASC) Order by the id_blob column
 * @method CommonTEntrepriseDocumentVersionQuery orderByTailleDocument($order = Criteria::ASC) Order by the taille_document column
 * @method CommonTEntrepriseDocumentVersionQuery orderByExtensionDocument($order = Criteria::ASC) Order by the extension_document column
 * @method CommonTEntrepriseDocumentVersionQuery orderByJetonDocument($order = Criteria::ASC) Order by the jeton_document column
 * @method CommonTEntrepriseDocumentVersionQuery orderByJsonDocument($order = Criteria::ASC) Order by the json_document column
 *
 * @method CommonTEntrepriseDocumentVersionQuery groupByIdVersionDocument() Group by the id_version_document column
 * @method CommonTEntrepriseDocumentVersionQuery groupByIdDocument() Group by the id_document column
 * @method CommonTEntrepriseDocumentVersionQuery groupByDateRecuperation() Group by the date_recuperation column
 * @method CommonTEntrepriseDocumentVersionQuery groupByHash() Group by the hash column
 * @method CommonTEntrepriseDocumentVersionQuery groupByIdBlob() Group by the id_blob column
 * @method CommonTEntrepriseDocumentVersionQuery groupByTailleDocument() Group by the taille_document column
 * @method CommonTEntrepriseDocumentVersionQuery groupByExtensionDocument() Group by the extension_document column
 * @method CommonTEntrepriseDocumentVersionQuery groupByJetonDocument() Group by the jeton_document column
 * @method CommonTEntrepriseDocumentVersionQuery groupByJsonDocument() Group by the json_document column
 *
 * @method CommonTEntrepriseDocumentVersionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTEntrepriseDocumentVersionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTEntrepriseDocumentVersionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTEntrepriseDocumentVersionQuery leftJoinCommonTDocumentEntreprise($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTDocumentEntreprise relation
 * @method CommonTEntrepriseDocumentVersionQuery rightJoinCommonTDocumentEntreprise($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTDocumentEntreprise relation
 * @method CommonTEntrepriseDocumentVersionQuery innerJoinCommonTDocumentEntreprise($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTDocumentEntreprise relation
 *
 * @method CommonTEntrepriseDocumentVersion findOne(PropelPDO $con = null) Return the first CommonTEntrepriseDocumentVersion matching the query
 * @method CommonTEntrepriseDocumentVersion findOneOrCreate(PropelPDO $con = null) Return the first CommonTEntrepriseDocumentVersion matching the query, or a new CommonTEntrepriseDocumentVersion object populated from the query conditions when no match is found
 *
 * @method CommonTEntrepriseDocumentVersion findOneByIdDocument(int $id_document) Return the first CommonTEntrepriseDocumentVersion filtered by the id_document column
 * @method CommonTEntrepriseDocumentVersion findOneByDateRecuperation(string $date_recuperation) Return the first CommonTEntrepriseDocumentVersion filtered by the date_recuperation column
 * @method CommonTEntrepriseDocumentVersion findOneByHash(string $hash) Return the first CommonTEntrepriseDocumentVersion filtered by the hash column
 * @method CommonTEntrepriseDocumentVersion findOneByIdBlob(int $id_blob) Return the first CommonTEntrepriseDocumentVersion filtered by the id_blob column
 * @method CommonTEntrepriseDocumentVersion findOneByTailleDocument(string $taille_document) Return the first CommonTEntrepriseDocumentVersion filtered by the taille_document column
 * @method CommonTEntrepriseDocumentVersion findOneByExtensionDocument(string $extension_document) Return the first CommonTEntrepriseDocumentVersion filtered by the extension_document column
 * @method CommonTEntrepriseDocumentVersion findOneByJetonDocument(string $jeton_document) Return the first CommonTEntrepriseDocumentVersion filtered by the jeton_document column
 * @method CommonTEntrepriseDocumentVersion findOneByJsonDocument(string $json_document) Return the first CommonTEntrepriseDocumentVersion filtered by the json_document column
 *
 * @method array findByIdVersionDocument(int $id_version_document) Return CommonTEntrepriseDocumentVersion objects filtered by the id_version_document column
 * @method array findByIdDocument(int $id_document) Return CommonTEntrepriseDocumentVersion objects filtered by the id_document column
 * @method array findByDateRecuperation(string $date_recuperation) Return CommonTEntrepriseDocumentVersion objects filtered by the date_recuperation column
 * @method array findByHash(string $hash) Return CommonTEntrepriseDocumentVersion objects filtered by the hash column
 * @method array findByIdBlob(int $id_blob) Return CommonTEntrepriseDocumentVersion objects filtered by the id_blob column
 * @method array findByTailleDocument(string $taille_document) Return CommonTEntrepriseDocumentVersion objects filtered by the taille_document column
 * @method array findByExtensionDocument(string $extension_document) Return CommonTEntrepriseDocumentVersion objects filtered by the extension_document column
 * @method array findByJetonDocument(string $jeton_document) Return CommonTEntrepriseDocumentVersion objects filtered by the jeton_document column
 * @method array findByJsonDocument(string $json_document) Return CommonTEntrepriseDocumentVersion objects filtered by the json_document column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTEntrepriseDocumentVersionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTEntrepriseDocumentVersionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTEntrepriseDocumentVersion', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTEntrepriseDocumentVersionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTEntrepriseDocumentVersionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTEntrepriseDocumentVersionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTEntrepriseDocumentVersionQuery) {
            return $criteria;
        }
        $query = new CommonTEntrepriseDocumentVersionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTEntrepriseDocumentVersion|CommonTEntrepriseDocumentVersion[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTEntrepriseDocumentVersionPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTEntrepriseDocumentVersionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTEntrepriseDocumentVersion A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByIdVersionDocument($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTEntrepriseDocumentVersion A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id_version_document`, `id_document`, `date_recuperation`, `hash`, `id_blob`, `taille_document`, `extension_document`, `jeton_document`, `json_document` FROM `t_entreprise_document_version` WHERE `id_version_document` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTEntrepriseDocumentVersion();
            $obj->hydrate($row);
            CommonTEntrepriseDocumentVersionPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTEntrepriseDocumentVersion|CommonTEntrepriseDocumentVersion[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTEntrepriseDocumentVersion[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_VERSION_DOCUMENT, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_VERSION_DOCUMENT, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id_version_document column
     *
     * Example usage:
     * <code>
     * $query->filterByIdVersionDocument(1234); // WHERE id_version_document = 1234
     * $query->filterByIdVersionDocument(array(12, 34)); // WHERE id_version_document IN (12, 34)
     * $query->filterByIdVersionDocument(array('min' => 12)); // WHERE id_version_document >= 12
     * $query->filterByIdVersionDocument(array('max' => 12)); // WHERE id_version_document <= 12
     * </code>
     *
     * @param     mixed $idVersionDocument The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function filterByIdVersionDocument($idVersionDocument = null, $comparison = null)
    {
        if (is_array($idVersionDocument)) {
            $useMinMax = false;
            if (isset($idVersionDocument['min'])) {
                $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_VERSION_DOCUMENT, $idVersionDocument['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idVersionDocument['max'])) {
                $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_VERSION_DOCUMENT, $idVersionDocument['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_VERSION_DOCUMENT, $idVersionDocument, $comparison);
    }

    /**
     * Filter the query on the id_document column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDocument(1234); // WHERE id_document = 1234
     * $query->filterByIdDocument(array(12, 34)); // WHERE id_document IN (12, 34)
     * $query->filterByIdDocument(array('min' => 12)); // WHERE id_document >= 12
     * $query->filterByIdDocument(array('max' => 12)); // WHERE id_document <= 12
     * </code>
     *
     * @see       filterByCommonTDocumentEntreprise()
     *
     * @param     mixed $idDocument The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function filterByIdDocument($idDocument = null, $comparison = null)
    {
        if (is_array($idDocument)) {
            $useMinMax = false;
            if (isset($idDocument['min'])) {
                $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_DOCUMENT, $idDocument['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDocument['max'])) {
                $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_DOCUMENT, $idDocument['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_DOCUMENT, $idDocument, $comparison);
    }

    /**
     * Filter the query on the date_recuperation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateRecuperation('2011-03-14'); // WHERE date_recuperation = '2011-03-14'
     * $query->filterByDateRecuperation('now'); // WHERE date_recuperation = '2011-03-14'
     * $query->filterByDateRecuperation(array('max' => 'yesterday')); // WHERE date_recuperation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateRecuperation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function filterByDateRecuperation($dateRecuperation = null, $comparison = null)
    {
        if (is_array($dateRecuperation)) {
            $useMinMax = false;
            if (isset($dateRecuperation['min'])) {
                $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::DATE_RECUPERATION, $dateRecuperation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateRecuperation['max'])) {
                $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::DATE_RECUPERATION, $dateRecuperation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::DATE_RECUPERATION, $dateRecuperation, $comparison);
    }

    /**
     * Filter the query on the hash column
     *
     * Example usage:
     * <code>
     * $query->filterByHash('fooValue');   // WHERE hash = 'fooValue'
     * $query->filterByHash('%fooValue%'); // WHERE hash LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hash The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function filterByHash($hash = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hash)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $hash)) {
                $hash = str_replace('*', '%', $hash);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::HASH, $hash, $comparison);
    }

    /**
     * Filter the query on the id_blob column
     *
     * Example usage:
     * <code>
     * $query->filterByIdBlob(1234); // WHERE id_blob = 1234
     * $query->filterByIdBlob(array(12, 34)); // WHERE id_blob IN (12, 34)
     * $query->filterByIdBlob(array('min' => 12)); // WHERE id_blob >= 12
     * $query->filterByIdBlob(array('max' => 12)); // WHERE id_blob <= 12
     * </code>
     *
     * @param     mixed $idBlob The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function filterByIdBlob($idBlob = null, $comparison = null)
    {
        if (is_array($idBlob)) {
            $useMinMax = false;
            if (isset($idBlob['min'])) {
                $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_BLOB, $idBlob['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idBlob['max'])) {
                $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_BLOB, $idBlob['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_BLOB, $idBlob, $comparison);
    }

    /**
     * Filter the query on the taille_document column
     *
     * Example usage:
     * <code>
     * $query->filterByTailleDocument('fooValue');   // WHERE taille_document = 'fooValue'
     * $query->filterByTailleDocument('%fooValue%'); // WHERE taille_document LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tailleDocument The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function filterByTailleDocument($tailleDocument = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tailleDocument)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tailleDocument)) {
                $tailleDocument = str_replace('*', '%', $tailleDocument);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::TAILLE_DOCUMENT, $tailleDocument, $comparison);
    }

    /**
     * Filter the query on the extension_document column
     *
     * Example usage:
     * <code>
     * $query->filterByExtensionDocument('fooValue');   // WHERE extension_document = 'fooValue'
     * $query->filterByExtensionDocument('%fooValue%'); // WHERE extension_document LIKE '%fooValue%'
     * </code>
     *
     * @param     string $extensionDocument The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function filterByExtensionDocument($extensionDocument = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($extensionDocument)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $extensionDocument)) {
                $extensionDocument = str_replace('*', '%', $extensionDocument);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::EXTENSION_DOCUMENT, $extensionDocument, $comparison);
    }

    /**
     * Filter the query on the jeton_document column
     *
     * Example usage:
     * <code>
     * $query->filterByJetonDocument('fooValue');   // WHERE jeton_document = 'fooValue'
     * $query->filterByJetonDocument('%fooValue%'); // WHERE jeton_document LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jetonDocument The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function filterByJetonDocument($jetonDocument = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jetonDocument)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $jetonDocument)) {
                $jetonDocument = str_replace('*', '%', $jetonDocument);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::JETON_DOCUMENT, $jetonDocument, $comparison);
    }

    /**
     * Filter the query on the json_document column
     *
     * Example usage:
     * <code>
     * $query->filterByJsonDocument('fooValue');   // WHERE json_document = 'fooValue'
     * $query->filterByJsonDocument('%fooValue%'); // WHERE json_document LIKE '%fooValue%'
     * </code>
     *
     * @param     string $jsonDocument The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function filterByJsonDocument($jsonDocument = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($jsonDocument)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $jsonDocument)) {
                $jsonDocument = str_replace('*', '%', $jsonDocument);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::JSON_DOCUMENT, $jsonDocument, $comparison);
    }

    /**
     * Filter the query by a related CommonTDocumentEntreprise object
     *
     * @param   CommonTDocumentEntreprise|PropelObjectCollection $commonTDocumentEntreprise The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTDocumentEntreprise($commonTDocumentEntreprise, $comparison = null)
    {
        if ($commonTDocumentEntreprise instanceof CommonTDocumentEntreprise) {
            return $this
                ->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_DOCUMENT, $commonTDocumentEntreprise->getIdDocument(), $comparison);
        } elseif ($commonTDocumentEntreprise instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_DOCUMENT, $commonTDocumentEntreprise->toKeyValue('PrimaryKey', 'IdDocument'), $comparison);
        } else {
            throw new PropelException('filterByCommonTDocumentEntreprise() only accepts arguments of type CommonTDocumentEntreprise or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTDocumentEntreprise relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function joinCommonTDocumentEntreprise($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTDocumentEntreprise');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTDocumentEntreprise');
        }

        return $this;
    }

    /**
     * Use the CommonTDocumentEntreprise relation CommonTDocumentEntreprise object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTDocumentEntrepriseQuery A secondary query class using the current class as primary query
     */
    public function useCommonTDocumentEntrepriseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTDocumentEntreprise($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTDocumentEntreprise', '\Application\Propel\Mpe\CommonTDocumentEntrepriseQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTEntrepriseDocumentVersion $commonTEntrepriseDocumentVersion Object to remove from the list of results
     *
     * @return CommonTEntrepriseDocumentVersionQuery The current query, for fluid interface
     */
    public function prune($commonTEntrepriseDocumentVersion = null)
    {
        if ($commonTEntrepriseDocumentVersion) {
            $this->addUsingAlias(CommonTEntrepriseDocumentVersionPeer::ID_VERSION_DOCUMENT, $commonTEntrepriseDocumentVersion->getIdVersionDocument(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
