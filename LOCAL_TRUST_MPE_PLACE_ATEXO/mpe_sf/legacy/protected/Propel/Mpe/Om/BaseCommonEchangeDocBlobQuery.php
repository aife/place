<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonBlobOrganismeFile;
use Application\Propel\Mpe\CommonEchangeDoc;
use Application\Propel\Mpe\CommonEchangeDocBlob;
use Application\Propel\Mpe\CommonEchangeDocBlobPeer;
use Application\Propel\Mpe\CommonEchangeDocBlobQuery;
use Application\Propel\Mpe\CommonEchangeDocTypePieceStandard;

/**
 * Base class that represents a query for the 'echange_doc_blob' table.
 *
 *
 *
 * @method CommonEchangeDocBlobQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonEchangeDocBlobQuery orderByEchangeDocId($order = Criteria::ASC) Order by the echange_doc_id column
 * @method CommonEchangeDocBlobQuery orderByBlobOrganismeId($order = Criteria::ASC) Order by the blob_organisme_id column
 * @method CommonEchangeDocBlobQuery orderByChemin($order = Criteria::ASC) Order by the chemin column
 * @method CommonEchangeDocBlobQuery orderByPoids($order = Criteria::ASC) Order by the poids column
 * @method CommonEchangeDocBlobQuery orderByChecksum($order = Criteria::ASC) Order by the checksum column
 * @method CommonEchangeDocBlobQuery orderByEchangeTypePieceActesId($order = Criteria::ASC) Order by the echange_type_piece_actes_id column
 * @method CommonEchangeDocBlobQuery orderByEchangeTypePieceStandardId($order = Criteria::ASC) Order by the echange_type_piece_standard_id column
 * @method CommonEchangeDocBlobQuery orderByCategoriePiece($order = Criteria::ASC) Order by the categorie_piece column
 * @method CommonEchangeDocBlobQuery orderByDocBlobPrincipalId($order = Criteria::ASC) Order by the doc_blob_principal_id column
 *
 * @method CommonEchangeDocBlobQuery groupById() Group by the id column
 * @method CommonEchangeDocBlobQuery groupByEchangeDocId() Group by the echange_doc_id column
 * @method CommonEchangeDocBlobQuery groupByBlobOrganismeId() Group by the blob_organisme_id column
 * @method CommonEchangeDocBlobQuery groupByChemin() Group by the chemin column
 * @method CommonEchangeDocBlobQuery groupByPoids() Group by the poids column
 * @method CommonEchangeDocBlobQuery groupByChecksum() Group by the checksum column
 * @method CommonEchangeDocBlobQuery groupByEchangeTypePieceActesId() Group by the echange_type_piece_actes_id column
 * @method CommonEchangeDocBlobQuery groupByEchangeTypePieceStandardId() Group by the echange_type_piece_standard_id column
 * @method CommonEchangeDocBlobQuery groupByCategoriePiece() Group by the categorie_piece column
 * @method CommonEchangeDocBlobQuery groupByDocBlobPrincipalId() Group by the doc_blob_principal_id column
 *
 * @method CommonEchangeDocBlobQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonEchangeDocBlobQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonEchangeDocBlobQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonEchangeDocBlobQuery leftJoinCommonEchangeDoc($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDoc relation
 * @method CommonEchangeDocBlobQuery rightJoinCommonEchangeDoc($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDoc relation
 * @method CommonEchangeDocBlobQuery innerJoinCommonEchangeDoc($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDoc relation
 *
 * @method CommonEchangeDocBlobQuery leftJoinCommonBlobOrganismeFile($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonBlobOrganismeFile relation
 * @method CommonEchangeDocBlobQuery rightJoinCommonBlobOrganismeFile($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonBlobOrganismeFile relation
 * @method CommonEchangeDocBlobQuery innerJoinCommonBlobOrganismeFile($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonBlobOrganismeFile relation
 *
 * @method CommonEchangeDocBlobQuery leftJoinCommonEchangeDocBlobRelatedByDocBlobPrincipalId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocBlobRelatedByDocBlobPrincipalId relation
 * @method CommonEchangeDocBlobQuery rightJoinCommonEchangeDocBlobRelatedByDocBlobPrincipalId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocBlobRelatedByDocBlobPrincipalId relation
 * @method CommonEchangeDocBlobQuery innerJoinCommonEchangeDocBlobRelatedByDocBlobPrincipalId($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocBlobRelatedByDocBlobPrincipalId relation
 *
 * @method CommonEchangeDocBlobQuery leftJoinCommonEchangeDocTypePieceStandard($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocTypePieceStandard relation
 * @method CommonEchangeDocBlobQuery rightJoinCommonEchangeDocTypePieceStandard($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocTypePieceStandard relation
 * @method CommonEchangeDocBlobQuery innerJoinCommonEchangeDocTypePieceStandard($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocTypePieceStandard relation
 *
 * @method CommonEchangeDocBlobQuery leftJoinCommonEchangeDocBlobRelatedById($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonEchangeDocBlobRelatedById relation
 * @method CommonEchangeDocBlobQuery rightJoinCommonEchangeDocBlobRelatedById($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonEchangeDocBlobRelatedById relation
 * @method CommonEchangeDocBlobQuery innerJoinCommonEchangeDocBlobRelatedById($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonEchangeDocBlobRelatedById relation
 *
 * @method CommonEchangeDocBlob findOne(PropelPDO $con = null) Return the first CommonEchangeDocBlob matching the query
 * @method CommonEchangeDocBlob findOneOrCreate(PropelPDO $con = null) Return the first CommonEchangeDocBlob matching the query, or a new CommonEchangeDocBlob object populated from the query conditions when no match is found
 *
 * @method CommonEchangeDocBlob findOneByEchangeDocId(int $echange_doc_id) Return the first CommonEchangeDocBlob filtered by the echange_doc_id column
 * @method CommonEchangeDocBlob findOneByBlobOrganismeId(int $blob_organisme_id) Return the first CommonEchangeDocBlob filtered by the blob_organisme_id column
 * @method CommonEchangeDocBlob findOneByChemin(string $chemin) Return the first CommonEchangeDocBlob filtered by the chemin column
 * @method CommonEchangeDocBlob findOneByPoids(int $poids) Return the first CommonEchangeDocBlob filtered by the poids column
 * @method CommonEchangeDocBlob findOneByChecksum(string $checksum) Return the first CommonEchangeDocBlob filtered by the checksum column
 * @method CommonEchangeDocBlob findOneByEchangeTypePieceActesId(int $echange_type_piece_actes_id) Return the first CommonEchangeDocBlob filtered by the echange_type_piece_actes_id column
 * @method CommonEchangeDocBlob findOneByEchangeTypePieceStandardId(int $echange_type_piece_standard_id) Return the first CommonEchangeDocBlob filtered by the echange_type_piece_standard_id column
 * @method CommonEchangeDocBlob findOneByCategoriePiece(int $categorie_piece) Return the first CommonEchangeDocBlob filtered by the categorie_piece column
 * @method CommonEchangeDocBlob findOneByDocBlobPrincipalId(int $doc_blob_principal_id) Return the first CommonEchangeDocBlob filtered by the doc_blob_principal_id column
 *
 * @method array findById(int $id) Return CommonEchangeDocBlob objects filtered by the id column
 * @method array findByEchangeDocId(int $echange_doc_id) Return CommonEchangeDocBlob objects filtered by the echange_doc_id column
 * @method array findByBlobOrganismeId(int $blob_organisme_id) Return CommonEchangeDocBlob objects filtered by the blob_organisme_id column
 * @method array findByChemin(string $chemin) Return CommonEchangeDocBlob objects filtered by the chemin column
 * @method array findByPoids(int $poids) Return CommonEchangeDocBlob objects filtered by the poids column
 * @method array findByChecksum(string $checksum) Return CommonEchangeDocBlob objects filtered by the checksum column
 * @method array findByEchangeTypePieceActesId(int $echange_type_piece_actes_id) Return CommonEchangeDocBlob objects filtered by the echange_type_piece_actes_id column
 * @method array findByEchangeTypePieceStandardId(int $echange_type_piece_standard_id) Return CommonEchangeDocBlob objects filtered by the echange_type_piece_standard_id column
 * @method array findByCategoriePiece(int $categorie_piece) Return CommonEchangeDocBlob objects filtered by the categorie_piece column
 * @method array findByDocBlobPrincipalId(int $doc_blob_principal_id) Return CommonEchangeDocBlob objects filtered by the doc_blob_principal_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonEchangeDocBlobQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonEchangeDocBlobQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonEchangeDocBlob', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonEchangeDocBlobQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonEchangeDocBlobQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonEchangeDocBlobQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonEchangeDocBlobQuery) {
            return $criteria;
        }
        $query = new CommonEchangeDocBlobQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonEchangeDocBlob|CommonEchangeDocBlob[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonEchangeDocBlobPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonEchangeDocBlobPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangeDocBlob A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonEchangeDocBlob A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `echange_doc_id`, `blob_organisme_id`, `chemin`, `poids`, `checksum`, `echange_type_piece_actes_id`, `echange_type_piece_standard_id`, `categorie_piece`, `doc_blob_principal_id` FROM `echange_doc_blob` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonEchangeDocBlob();
            $obj->hydrate($row);
            CommonEchangeDocBlobPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonEchangeDocBlob|CommonEchangeDocBlob[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonEchangeDocBlob[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the echange_doc_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEchangeDocId(1234); // WHERE echange_doc_id = 1234
     * $query->filterByEchangeDocId(array(12, 34)); // WHERE echange_doc_id IN (12, 34)
     * $query->filterByEchangeDocId(array('min' => 12)); // WHERE echange_doc_id >= 12
     * $query->filterByEchangeDocId(array('max' => 12)); // WHERE echange_doc_id <= 12
     * </code>
     *
     * @see       filterByCommonEchangeDoc()
     *
     * @param     mixed $echangeDocId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterByEchangeDocId($echangeDocId = null, $comparison = null)
    {
        if (is_array($echangeDocId)) {
            $useMinMax = false;
            if (isset($echangeDocId['min'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, $echangeDocId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($echangeDocId['max'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, $echangeDocId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, $echangeDocId, $comparison);
    }

    /**
     * Filter the query on the blob_organisme_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBlobOrganismeId(1234); // WHERE blob_organisme_id = 1234
     * $query->filterByBlobOrganismeId(array(12, 34)); // WHERE blob_organisme_id IN (12, 34)
     * $query->filterByBlobOrganismeId(array('min' => 12)); // WHERE blob_organisme_id >= 12
     * $query->filterByBlobOrganismeId(array('max' => 12)); // WHERE blob_organisme_id <= 12
     * </code>
     *
     * @see       filterByCommonBlobOrganismeFile()
     *
     * @param     mixed $blobOrganismeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterByBlobOrganismeId($blobOrganismeId = null, $comparison = null)
    {
        if (is_array($blobOrganismeId)) {
            $useMinMax = false;
            if (isset($blobOrganismeId['min'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, $blobOrganismeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($blobOrganismeId['max'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, $blobOrganismeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, $blobOrganismeId, $comparison);
    }

    /**
     * Filter the query on the chemin column
     *
     * Example usage:
     * <code>
     * $query->filterByChemin('fooValue');   // WHERE chemin = 'fooValue'
     * $query->filterByChemin('%fooValue%'); // WHERE chemin LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chemin The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterByChemin($chemin = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chemin)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $chemin)) {
                $chemin = str_replace('*', '%', $chemin);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::CHEMIN, $chemin, $comparison);
    }

    /**
     * Filter the query on the poids column
     *
     * Example usage:
     * <code>
     * $query->filterByPoids(1234); // WHERE poids = 1234
     * $query->filterByPoids(array(12, 34)); // WHERE poids IN (12, 34)
     * $query->filterByPoids(array('min' => 12)); // WHERE poids >= 12
     * $query->filterByPoids(array('max' => 12)); // WHERE poids <= 12
     * </code>
     *
     * @param     mixed $poids The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterByPoids($poids = null, $comparison = null)
    {
        if (is_array($poids)) {
            $useMinMax = false;
            if (isset($poids['min'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::POIDS, $poids['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($poids['max'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::POIDS, $poids['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::POIDS, $poids, $comparison);
    }

    /**
     * Filter the query on the checksum column
     *
     * Example usage:
     * <code>
     * $query->filterByChecksum('fooValue');   // WHERE checksum = 'fooValue'
     * $query->filterByChecksum('%fooValue%'); // WHERE checksum LIKE '%fooValue%'
     * </code>
     *
     * @param     string $checksum The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterByChecksum($checksum = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($checksum)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $checksum)) {
                $checksum = str_replace('*', '%', $checksum);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::CHECKSUM, $checksum, $comparison);
    }

    /**
     * Filter the query on the echange_type_piece_actes_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEchangeTypePieceActesId(1234); // WHERE echange_type_piece_actes_id = 1234
     * $query->filterByEchangeTypePieceActesId(array(12, 34)); // WHERE echange_type_piece_actes_id IN (12, 34)
     * $query->filterByEchangeTypePieceActesId(array('min' => 12)); // WHERE echange_type_piece_actes_id >= 12
     * $query->filterByEchangeTypePieceActesId(array('max' => 12)); // WHERE echange_type_piece_actes_id <= 12
     * </code>
     *
     * @param     mixed $echangeTypePieceActesId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterByEchangeTypePieceActesId($echangeTypePieceActesId = null, $comparison = null)
    {
        if (is_array($echangeTypePieceActesId)) {
            $useMinMax = false;
            if (isset($echangeTypePieceActesId['min'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_ACTES_ID, $echangeTypePieceActesId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($echangeTypePieceActesId['max'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_ACTES_ID, $echangeTypePieceActesId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_ACTES_ID, $echangeTypePieceActesId, $comparison);
    }

    /**
     * Filter the query on the echange_type_piece_standard_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEchangeTypePieceStandardId(1234); // WHERE echange_type_piece_standard_id = 1234
     * $query->filterByEchangeTypePieceStandardId(array(12, 34)); // WHERE echange_type_piece_standard_id IN (12, 34)
     * $query->filterByEchangeTypePieceStandardId(array('min' => 12)); // WHERE echange_type_piece_standard_id >= 12
     * $query->filterByEchangeTypePieceStandardId(array('max' => 12)); // WHERE echange_type_piece_standard_id <= 12
     * </code>
     *
     * @see       filterByCommonEchangeDocTypePieceStandard()
     *
     * @param     mixed $echangeTypePieceStandardId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterByEchangeTypePieceStandardId($echangeTypePieceStandardId = null, $comparison = null)
    {
        if (is_array($echangeTypePieceStandardId)) {
            $useMinMax = false;
            if (isset($echangeTypePieceStandardId['min'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, $echangeTypePieceStandardId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($echangeTypePieceStandardId['max'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, $echangeTypePieceStandardId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, $echangeTypePieceStandardId, $comparison);
    }

    /**
     * Filter the query on the categorie_piece column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoriePiece(1234); // WHERE categorie_piece = 1234
     * $query->filterByCategoriePiece(array(12, 34)); // WHERE categorie_piece IN (12, 34)
     * $query->filterByCategoriePiece(array('min' => 12)); // WHERE categorie_piece >= 12
     * $query->filterByCategoriePiece(array('max' => 12)); // WHERE categorie_piece <= 12
     * </code>
     *
     * @param     mixed $categoriePiece The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterByCategoriePiece($categoriePiece = null, $comparison = null)
    {
        if (is_array($categoriePiece)) {
            $useMinMax = false;
            if (isset($categoriePiece['min'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::CATEGORIE_PIECE, $categoriePiece['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoriePiece['max'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::CATEGORIE_PIECE, $categoriePiece['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::CATEGORIE_PIECE, $categoriePiece, $comparison);
    }

    /**
     * Filter the query on the doc_blob_principal_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDocBlobPrincipalId(1234); // WHERE doc_blob_principal_id = 1234
     * $query->filterByDocBlobPrincipalId(array(12, 34)); // WHERE doc_blob_principal_id IN (12, 34)
     * $query->filterByDocBlobPrincipalId(array('min' => 12)); // WHERE doc_blob_principal_id >= 12
     * $query->filterByDocBlobPrincipalId(array('max' => 12)); // WHERE doc_blob_principal_id <= 12
     * </code>
     *
     * @see       filterByCommonEchangeDocBlobRelatedByDocBlobPrincipalId()
     *
     * @param     mixed $docBlobPrincipalId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function filterByDocBlobPrincipalId($docBlobPrincipalId = null, $comparison = null)
    {
        if (is_array($docBlobPrincipalId)) {
            $useMinMax = false;
            if (isset($docBlobPrincipalId['min'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID, $docBlobPrincipalId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($docBlobPrincipalId['max'])) {
                $this->addUsingAlias(CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID, $docBlobPrincipalId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID, $docBlobPrincipalId, $comparison);
    }

    /**
     * Filter the query by a related CommonEchangeDoc object
     *
     * @param   CommonEchangeDoc|PropelObjectCollection $commonEchangeDoc The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocBlobQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDoc($commonEchangeDoc, $comparison = null)
    {
        if ($commonEchangeDoc instanceof CommonEchangeDoc) {
            return $this
                ->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, $commonEchangeDoc->getId(), $comparison);
        } elseif ($commonEchangeDoc instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_DOC_ID, $commonEchangeDoc->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonEchangeDoc() only accepts arguments of type CommonEchangeDoc or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDoc relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDoc($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDoc');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDoc');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDoc relation CommonEchangeDoc object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDoc($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDoc', '\Application\Propel\Mpe\CommonEchangeDocQuery');
    }

    /**
     * Filter the query by a related CommonBlobOrganismeFile object
     *
     * @param   CommonBlobOrganismeFile|PropelObjectCollection $commonBlobOrganismeFile The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocBlobQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonBlobOrganismeFile($commonBlobOrganismeFile, $comparison = null)
    {
        if ($commonBlobOrganismeFile instanceof CommonBlobOrganismeFile) {
            return $this
                ->addUsingAlias(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, $commonBlobOrganismeFile->getId(), $comparison);
        } elseif ($commonBlobOrganismeFile instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocBlobPeer::BLOB_ORGANISME_ID, $commonBlobOrganismeFile->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonBlobOrganismeFile() only accepts arguments of type CommonBlobOrganismeFile or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonBlobOrganismeFile relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function joinCommonBlobOrganismeFile($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonBlobOrganismeFile');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonBlobOrganismeFile');
        }

        return $this;
    }

    /**
     * Use the CommonBlobOrganismeFile relation CommonBlobOrganismeFile object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonBlobOrganismeFileQuery A secondary query class using the current class as primary query
     */
    public function useCommonBlobOrganismeFileQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonBlobOrganismeFile($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonBlobOrganismeFile', '\Application\Propel\Mpe\CommonBlobOrganismeFileQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDocBlob object
     *
     * @param   CommonEchangeDocBlob|PropelObjectCollection $commonEchangeDocBlob The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocBlobQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocBlobRelatedByDocBlobPrincipalId($commonEchangeDocBlob, $comparison = null)
    {
        if ($commonEchangeDocBlob instanceof CommonEchangeDocBlob) {
            return $this
                ->addUsingAlias(CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID, $commonEchangeDocBlob->getId(), $comparison);
        } elseif ($commonEchangeDocBlob instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocBlobPeer::DOC_BLOB_PRINCIPAL_ID, $commonEchangeDocBlob->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonEchangeDocBlobRelatedByDocBlobPrincipalId() only accepts arguments of type CommonEchangeDocBlob or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocBlobRelatedByDocBlobPrincipalId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocBlobRelatedByDocBlobPrincipalId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocBlobRelatedByDocBlobPrincipalId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocBlobRelatedByDocBlobPrincipalId');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocBlobRelatedByDocBlobPrincipalId relation CommonEchangeDocBlob object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocBlobQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocBlobRelatedByDocBlobPrincipalIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocBlobRelatedByDocBlobPrincipalId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocBlobRelatedByDocBlobPrincipalId', '\Application\Propel\Mpe\CommonEchangeDocBlobQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDocTypePieceStandard object
     *
     * @param   CommonEchangeDocTypePieceStandard|PropelObjectCollection $commonEchangeDocTypePieceStandard The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocBlobQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocTypePieceStandard($commonEchangeDocTypePieceStandard, $comparison = null)
    {
        if ($commonEchangeDocTypePieceStandard instanceof CommonEchangeDocTypePieceStandard) {
            return $this
                ->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, $commonEchangeDocTypePieceStandard->getId(), $comparison);
        } elseif ($commonEchangeDocTypePieceStandard instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonEchangeDocBlobPeer::ECHANGE_TYPE_PIECE_STANDARD_ID, $commonEchangeDocTypePieceStandard->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonEchangeDocTypePieceStandard() only accepts arguments of type CommonEchangeDocTypePieceStandard or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocTypePieceStandard relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocTypePieceStandard($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocTypePieceStandard');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocTypePieceStandard');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocTypePieceStandard relation CommonEchangeDocTypePieceStandard object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocTypePieceStandardQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocTypePieceStandardQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocTypePieceStandard($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocTypePieceStandard', '\Application\Propel\Mpe\CommonEchangeDocTypePieceStandardQuery');
    }

    /**
     * Filter the query by a related CommonEchangeDocBlob object
     *
     * @param   CommonEchangeDocBlob|PropelObjectCollection $commonEchangeDocBlob  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonEchangeDocBlobQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonEchangeDocBlobRelatedById($commonEchangeDocBlob, $comparison = null)
    {
        if ($commonEchangeDocBlob instanceof CommonEchangeDocBlob) {
            return $this
                ->addUsingAlias(CommonEchangeDocBlobPeer::ID, $commonEchangeDocBlob->getDocBlobPrincipalId(), $comparison);
        } elseif ($commonEchangeDocBlob instanceof PropelObjectCollection) {
            return $this
                ->useCommonEchangeDocBlobRelatedByIdQuery()
                ->filterByPrimaryKeys($commonEchangeDocBlob->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonEchangeDocBlobRelatedById() only accepts arguments of type CommonEchangeDocBlob or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonEchangeDocBlobRelatedById relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function joinCommonEchangeDocBlobRelatedById($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonEchangeDocBlobRelatedById');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonEchangeDocBlobRelatedById');
        }

        return $this;
    }

    /**
     * Use the CommonEchangeDocBlobRelatedById relation CommonEchangeDocBlob object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonEchangeDocBlobQuery A secondary query class using the current class as primary query
     */
    public function useCommonEchangeDocBlobRelatedByIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonEchangeDocBlobRelatedById($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonEchangeDocBlobRelatedById', '\Application\Propel\Mpe\CommonEchangeDocBlobQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonEchangeDocBlob $commonEchangeDocBlob Object to remove from the list of results
     *
     * @return CommonEchangeDocBlobQuery The current query, for fluid interface
     */
    public function prune($commonEchangeDocBlob = null)
    {
        if ($commonEchangeDocBlob) {
            $this->addUsingAlias(CommonEchangeDocBlobPeer::ID, $commonEchangeDocBlob->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
