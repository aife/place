<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonTOffreSupportPublicite;
use Application\Propel\Mpe\CommonTOffreSupportPublicitePeer;
use Application\Propel\Mpe\CommonTOffreSupportPubliciteQuery;
use Application\Propel\Mpe\CommonTSupportPublication;

/**
 * Base class that represents a query for the 't_Offre_Support_Publicite' table.
 *
 *
 *
 * @method CommonTOffreSupportPubliciteQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTOffreSupportPubliciteQuery orderByLibelleOffre($order = Criteria::ASC) Order by the libelle_offre column
 * @method CommonTOffreSupportPubliciteQuery orderByIdSupport($order = Criteria::ASC) Order by the id_support column
 * @method CommonTOffreSupportPubliciteQuery orderByActif($order = Criteria::ASC) Order by the actif column
 * @method CommonTOffreSupportPubliciteQuery orderByLogo($order = Criteria::ASC) Order by the logo column
 * @method CommonTOffreSupportPubliciteQuery orderByMapa($order = Criteria::ASC) Order by the mapa column
 * @method CommonTOffreSupportPubliciteQuery orderByMontantInf($order = Criteria::ASC) Order by the montant_inf column
 * @method CommonTOffreSupportPubliciteQuery orderByMontantMax($order = Criteria::ASC) Order by the montant_max column
 * @method CommonTOffreSupportPubliciteQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method CommonTOffreSupportPubliciteQuery orderByOrdre($order = Criteria::ASC) Order by the ordre column
 *
 * @method CommonTOffreSupportPubliciteQuery groupById() Group by the id column
 * @method CommonTOffreSupportPubliciteQuery groupByLibelleOffre() Group by the libelle_offre column
 * @method CommonTOffreSupportPubliciteQuery groupByIdSupport() Group by the id_support column
 * @method CommonTOffreSupportPubliciteQuery groupByActif() Group by the actif column
 * @method CommonTOffreSupportPubliciteQuery groupByLogo() Group by the logo column
 * @method CommonTOffreSupportPubliciteQuery groupByMapa() Group by the mapa column
 * @method CommonTOffreSupportPubliciteQuery groupByMontantInf() Group by the montant_inf column
 * @method CommonTOffreSupportPubliciteQuery groupByMontantMax() Group by the montant_max column
 * @method CommonTOffreSupportPubliciteQuery groupByCode() Group by the code column
 * @method CommonTOffreSupportPubliciteQuery groupByOrdre() Group by the ordre column
 *
 * @method CommonTOffreSupportPubliciteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTOffreSupportPubliciteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTOffreSupportPubliciteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTOffreSupportPubliciteQuery leftJoinCommonTSupportPublication($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTSupportPublication relation
 * @method CommonTOffreSupportPubliciteQuery rightJoinCommonTSupportPublication($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTSupportPublication relation
 * @method CommonTOffreSupportPubliciteQuery innerJoinCommonTSupportPublication($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTSupportPublication relation
 *
 * @method CommonTOffreSupportPublicite findOne(PropelPDO $con = null) Return the first CommonTOffreSupportPublicite matching the query
 * @method CommonTOffreSupportPublicite findOneOrCreate(PropelPDO $con = null) Return the first CommonTOffreSupportPublicite matching the query, or a new CommonTOffreSupportPublicite object populated from the query conditions when no match is found
 *
 * @method CommonTOffreSupportPublicite findOneByLibelleOffre(string $libelle_offre) Return the first CommonTOffreSupportPublicite filtered by the libelle_offre column
 * @method CommonTOffreSupportPublicite findOneByIdSupport(int $id_support) Return the first CommonTOffreSupportPublicite filtered by the id_support column
 * @method CommonTOffreSupportPublicite findOneByActif(string $actif) Return the first CommonTOffreSupportPublicite filtered by the actif column
 * @method CommonTOffreSupportPublicite findOneByLogo(string $logo) Return the first CommonTOffreSupportPublicite filtered by the logo column
 * @method CommonTOffreSupportPublicite findOneByMapa(int $mapa) Return the first CommonTOffreSupportPublicite filtered by the mapa column
 * @method CommonTOffreSupportPublicite findOneByMontantInf(int $montant_inf) Return the first CommonTOffreSupportPublicite filtered by the montant_inf column
 * @method CommonTOffreSupportPublicite findOneByMontantMax(int $montant_max) Return the first CommonTOffreSupportPublicite filtered by the montant_max column
 * @method CommonTOffreSupportPublicite findOneByCode(string $code) Return the first CommonTOffreSupportPublicite filtered by the code column
 * @method CommonTOffreSupportPublicite findOneByOrdre(int $ordre) Return the first CommonTOffreSupportPublicite filtered by the ordre column
 *
 * @method array findById(int $id) Return CommonTOffreSupportPublicite objects filtered by the id column
 * @method array findByLibelleOffre(string $libelle_offre) Return CommonTOffreSupportPublicite objects filtered by the libelle_offre column
 * @method array findByIdSupport(int $id_support) Return CommonTOffreSupportPublicite objects filtered by the id_support column
 * @method array findByActif(string $actif) Return CommonTOffreSupportPublicite objects filtered by the actif column
 * @method array findByLogo(string $logo) Return CommonTOffreSupportPublicite objects filtered by the logo column
 * @method array findByMapa(int $mapa) Return CommonTOffreSupportPublicite objects filtered by the mapa column
 * @method array findByMontantInf(int $montant_inf) Return CommonTOffreSupportPublicite objects filtered by the montant_inf column
 * @method array findByMontantMax(int $montant_max) Return CommonTOffreSupportPublicite objects filtered by the montant_max column
 * @method array findByCode(string $code) Return CommonTOffreSupportPublicite objects filtered by the code column
 * @method array findByOrdre(int $ordre) Return CommonTOffreSupportPublicite objects filtered by the ordre column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTOffreSupportPubliciteQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTOffreSupportPubliciteQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTOffreSupportPublicite', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTOffreSupportPubliciteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTOffreSupportPubliciteQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTOffreSupportPubliciteQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTOffreSupportPubliciteQuery) {
            return $criteria;
        }
        $query = new CommonTOffreSupportPubliciteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTOffreSupportPublicite|CommonTOffreSupportPublicite[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTOffreSupportPublicitePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTOffreSupportPublicitePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTOffreSupportPublicite A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTOffreSupportPublicite A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `libelle_offre`, `id_support`, `actif`, `logo`, `mapa`, `montant_inf`, `montant_max`, `code`, `ordre` FROM `t_Offre_Support_Publicite` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTOffreSupportPublicite();
            $obj->hydrate($row);
            CommonTOffreSupportPublicitePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTOffreSupportPublicite|CommonTOffreSupportPublicite[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTOffreSupportPublicite[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the libelle_offre column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleOffre('fooValue');   // WHERE libelle_offre = 'fooValue'
     * $query->filterByLibelleOffre('%fooValue%'); // WHERE libelle_offre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $libelleOffre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterByLibelleOffre($libelleOffre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleOffre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $libelleOffre)) {
                $libelleOffre = str_replace('*', '%', $libelleOffre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::LIBELLE_OFFRE, $libelleOffre, $comparison);
    }

    /**
     * Filter the query on the id_support column
     *
     * Example usage:
     * <code>
     * $query->filterByIdSupport(1234); // WHERE id_support = 1234
     * $query->filterByIdSupport(array(12, 34)); // WHERE id_support IN (12, 34)
     * $query->filterByIdSupport(array('min' => 12)); // WHERE id_support >= 12
     * $query->filterByIdSupport(array('max' => 12)); // WHERE id_support <= 12
     * </code>
     *
     * @see       filterByCommonTSupportPublication()
     *
     * @param     mixed $idSupport The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterByIdSupport($idSupport = null, $comparison = null)
    {
        if (is_array($idSupport)) {
            $useMinMax = false;
            if (isset($idSupport['min'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ID_SUPPORT, $idSupport['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idSupport['max'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ID_SUPPORT, $idSupport['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ID_SUPPORT, $idSupport, $comparison);
    }

    /**
     * Filter the query on the actif column
     *
     * Example usage:
     * <code>
     * $query->filterByActif('fooValue');   // WHERE actif = 'fooValue'
     * $query->filterByActif('%fooValue%'); // WHERE actif LIKE '%fooValue%'
     * </code>
     *
     * @param     string $actif The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterByActif($actif = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($actif)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $actif)) {
                $actif = str_replace('*', '%', $actif);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ACTIF, $actif, $comparison);
    }

    /**
     * Filter the query on the logo column
     *
     * Example usage:
     * <code>
     * $query->filterByLogo('fooValue');   // WHERE logo = 'fooValue'
     * $query->filterByLogo('%fooValue%'); // WHERE logo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $logo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterByLogo($logo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($logo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $logo)) {
                $logo = str_replace('*', '%', $logo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::LOGO, $logo, $comparison);
    }

    /**
     * Filter the query on the mapa column
     *
     * Example usage:
     * <code>
     * $query->filterByMapa(1234); // WHERE mapa = 1234
     * $query->filterByMapa(array(12, 34)); // WHERE mapa IN (12, 34)
     * $query->filterByMapa(array('min' => 12)); // WHERE mapa >= 12
     * $query->filterByMapa(array('max' => 12)); // WHERE mapa <= 12
     * </code>
     *
     * @param     mixed $mapa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterByMapa($mapa = null, $comparison = null)
    {
        if (is_array($mapa)) {
            $useMinMax = false;
            if (isset($mapa['min'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::MAPA, $mapa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mapa['max'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::MAPA, $mapa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::MAPA, $mapa, $comparison);
    }

    /**
     * Filter the query on the montant_inf column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantInf(1234); // WHERE montant_inf = 1234
     * $query->filterByMontantInf(array(12, 34)); // WHERE montant_inf IN (12, 34)
     * $query->filterByMontantInf(array('min' => 12)); // WHERE montant_inf >= 12
     * $query->filterByMontantInf(array('max' => 12)); // WHERE montant_inf <= 12
     * </code>
     *
     * @param     mixed $montantInf The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterByMontantInf($montantInf = null, $comparison = null)
    {
        if (is_array($montantInf)) {
            $useMinMax = false;
            if (isset($montantInf['min'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::MONTANT_INF, $montantInf['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantInf['max'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::MONTANT_INF, $montantInf['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::MONTANT_INF, $montantInf, $comparison);
    }

    /**
     * Filter the query on the montant_max column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantMax(1234); // WHERE montant_max = 1234
     * $query->filterByMontantMax(array(12, 34)); // WHERE montant_max IN (12, 34)
     * $query->filterByMontantMax(array('min' => 12)); // WHERE montant_max >= 12
     * $query->filterByMontantMax(array('max' => 12)); // WHERE montant_max <= 12
     * </code>
     *
     * @param     mixed $montantMax The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterByMontantMax($montantMax = null, $comparison = null)
    {
        if (is_array($montantMax)) {
            $useMinMax = false;
            if (isset($montantMax['min'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::MONTANT_MAX, $montantMax['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantMax['max'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::MONTANT_MAX, $montantMax['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::MONTANT_MAX, $montantMax, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%'); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code)) {
                $code = str_replace('*', '%', $code);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::CODE, $code, $comparison);
    }

    /**
     * Filter the query on the ordre column
     *
     * Example usage:
     * <code>
     * $query->filterByOrdre(1234); // WHERE ordre = 1234
     * $query->filterByOrdre(array(12, 34)); // WHERE ordre IN (12, 34)
     * $query->filterByOrdre(array('min' => 12)); // WHERE ordre >= 12
     * $query->filterByOrdre(array('max' => 12)); // WHERE ordre <= 12
     * </code>
     *
     * @param     mixed $ordre The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function filterByOrdre($ordre = null, $comparison = null)
    {
        if (is_array($ordre)) {
            $useMinMax = false;
            if (isset($ordre['min'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ORDRE, $ordre['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ordre['max'])) {
                $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ORDRE, $ordre['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ORDRE, $ordre, $comparison);
    }

    /**
     * Filter the query by a related CommonTSupportPublication object
     *
     * @param   CommonTSupportPublication|PropelObjectCollection $commonTSupportPublication The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTSupportPublication($commonTSupportPublication, $comparison = null)
    {
        if ($commonTSupportPublication instanceof CommonTSupportPublication) {
            return $this
                ->addUsingAlias(CommonTOffreSupportPublicitePeer::ID_SUPPORT, $commonTSupportPublication->getId(), $comparison);
        } elseif ($commonTSupportPublication instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTOffreSupportPublicitePeer::ID_SUPPORT, $commonTSupportPublication->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTSupportPublication() only accepts arguments of type CommonTSupportPublication or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTSupportPublication relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function joinCommonTSupportPublication($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTSupportPublication');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTSupportPublication');
        }

        return $this;
    }

    /**
     * Use the CommonTSupportPublication relation CommonTSupportPublication object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTSupportPublicationQuery A secondary query class using the current class as primary query
     */
    public function useCommonTSupportPublicationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTSupportPublication($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTSupportPublication', '\Application\Propel\Mpe\CommonTSupportPublicationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTOffreSupportPublicite $commonTOffreSupportPublicite Object to remove from the list of results
     *
     * @return CommonTOffreSupportPubliciteQuery The current query, for fluid interface
     */
    public function prune($commonTOffreSupportPublicite = null)
    {
        if ($commonTOffreSupportPublicite) {
            $this->addUsingAlias(CommonTOffreSupportPublicitePeer::ID, $commonTOffreSupportPublicite->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
