<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Om\BaseObject;
use Application\Library\Propel\Util\BasePeer;
use Application\Library\Propel\Query\Criteria;
use \DateTime;
use \Exception;
use \PDO;
use Application\Library\Propel\Om\Persistent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Util\PropelDateTime;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultationArchive;
use Application\Propel\Mpe\CommonConsultationArchiveArcade;
use Application\Propel\Mpe\CommonConsultationArchiveArcadeQuery;
use Application\Propel\Mpe\CommonConsultationArchiveBloc;
use Application\Propel\Mpe\CommonConsultationArchiveBlocQuery;
use Application\Propel\Mpe\CommonConsultationArchivePeer;
use Application\Propel\Mpe\CommonConsultationArchiveQuery;

/**
 * Base class that represents a row from the 'consultation_archive' table.
 *
 *
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonConsultationArchive extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'Application\\Propel\\Mpe\\CommonConsultationArchivePeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        CommonConsultationArchivePeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the organisme field.
     * @var        string
     */
    protected $organisme;

    /**
     * The value for the consultation_ref field.
     * @var        int
     */
    protected $consultation_ref;

    /**
     * The value for the chemin_fichier field.
     * @var        string
     */
    protected $chemin_fichier;

    /**
     * The value for the date_archivage field.
     * @var        string
     */
    protected $date_archivage;

    /**
     * The value for the poids_archivage field.
     * @var        int
     */
    protected $poids_archivage;

    /**
     * The value for the annee_creation_consultation field.
     * @var        int
     */
    protected $annee_creation_consultation;

    /**
     * The value for the status_global_transmission field.
     * @var        string
     */
    protected $status_global_transmission;

    /**
     * The value for the status_fragmentation field.
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $status_fragmentation;

    /**
     * The value for the nombre_bloc field.
     * @var        int
     */
    protected $nombre_bloc;

    /**
     * The value for the consultation_id field.
     * @var        int
     */
    protected $consultation_id;

    /**
     * @var        CommonConsultationArchiveArcade one-to-one related CommonConsultationArchiveArcade object
     */
    protected $singleCommonConsultationArchiveArcade;

    /**
     * @var        PropelObjectCollection|CommonConsultationArchiveBloc[] Collection to store aggregation of CommonConsultationArchiveBloc objects.
     */
    protected $collCommonConsultationArchiveBlocs;
    protected $collCommonConsultationArchiveBlocsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationArchiveArcadesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $commonConsultationArchiveBlocsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see        __construct()
     */
    public function applyDefaultValues()
    {
        $this->status_fragmentation = false;
    }

    /**
     * Initializes internal state of BaseCommonConsultationArchive object.
     * @see        applyDefaults()
     */
    public function __construct()
    {
        parent::__construct();
        $this->applyDefaultValues();
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [organisme] column value.
     *
     * @return string
     */
    public function getOrganisme()
    {

        return $this->organisme;
    }

    /**
     * Get the [consultation_ref] column value.
     *
     * @return int
     */
    public function getConsultationRef()
    {

        return $this->consultation_ref;
    }

    /**
     * Get the [chemin_fichier] column value.
     *
     * @return string
     */
    public function getCheminFichier()
    {

        return $this->chemin_fichier;
    }

    /**
     * Get the [optionally formatted] temporal [date_archivage] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00 00:00:00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDateArchivage($format = 'Y-m-d H:i:s')
    {
        if ($this->date_archivage === null) {
            return null;
        }

        if ($this->date_archivage === '0000-00-00 00:00:00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->date_archivage);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->date_archivage, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [poids_archivage] column value.
     *
     * @return int
     */
    public function getPoidsArchivage()
    {

        return $this->poids_archivage;
    }

    /**
     * Get the [annee_creation_consultation] column value.
     *
     * @return int
     */
    public function getAnneeCreationConsultation()
    {

        return $this->annee_creation_consultation;
    }

    /**
     * Get the [status_global_transmission] column value.
     *
     * @return string
     */
    public function getStatusGlobalTransmission()
    {

        return $this->status_global_transmission;
    }

    /**
     * Get the [status_fragmentation] column value.
     *
     * @return boolean
     */
    public function getStatusFragmentation()
    {

        return $this->status_fragmentation;
    }

    /**
     * Get the [nombre_bloc] column value.
     *
     * @return int
     */
    public function getNombreBloc()
    {

        return $this->nombre_bloc;
    }

    /**
     * Get the [consultation_id] column value.
     *
     * @return int
     */
    public function getConsultationId()
    {

        return $this->consultation_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = CommonConsultationArchivePeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [organisme] column.
     *
     * @param string $v new value
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setOrganisme($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->organisme !== $v) {
            $this->organisme = $v;
            $this->modifiedColumns[] = CommonConsultationArchivePeer::ORGANISME;
        }


        return $this;
    } // setOrganisme()

    /**
     * Set the value of [consultation_ref] column.
     *
     * @param int $v new value
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setConsultationRef($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->consultation_ref !== $v) {
            $this->consultation_ref = $v;
            $this->modifiedColumns[] = CommonConsultationArchivePeer::CONSULTATION_REF;
        }


        return $this;
    } // setConsultationRef()

    /**
     * Set the value of [chemin_fichier] column.
     *
     * @param string $v new value
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setCheminFichier($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->chemin_fichier !== $v) {
            $this->chemin_fichier = $v;
            $this->modifiedColumns[] = CommonConsultationArchivePeer::CHEMIN_FICHIER;
        }


        return $this;
    } // setCheminFichier()

    /**
     * Sets the value of [date_archivage] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setDateArchivage($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->date_archivage !== null || $dt !== null) {
            $currentDateAsString = ($this->date_archivage !== null && $tmpDt = new DateTime($this->date_archivage)) ? $tmpDt->format('Y-m-d H:i:s') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d H:i:s') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->date_archivage = $newDateAsString;
                $this->modifiedColumns[] = CommonConsultationArchivePeer::DATE_ARCHIVAGE;
            }
        } // if either are not null


        return $this;
    } // setDateArchivage()

    /**
     * Set the value of [poids_archivage] column.
     *
     * @param int $v new value
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setPoidsArchivage($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->poids_archivage !== $v) {
            $this->poids_archivage = $v;
            $this->modifiedColumns[] = CommonConsultationArchivePeer::POIDS_ARCHIVAGE;
        }


        return $this;
    } // setPoidsArchivage()

    /**
     * Set the value of [annee_creation_consultation] column.
     *
     * @param int $v new value
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setAnneeCreationConsultation($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->annee_creation_consultation !== $v) {
            $this->annee_creation_consultation = $v;
            $this->modifiedColumns[] = CommonConsultationArchivePeer::ANNEE_CREATION_CONSULTATION;
        }


        return $this;
    } // setAnneeCreationConsultation()

    /**
     * Set the value of [status_global_transmission] column.
     *
     * @param string $v new value
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setStatusGlobalTransmission($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->status_global_transmission !== $v) {
            $this->status_global_transmission = $v;
            $this->modifiedColumns[] = CommonConsultationArchivePeer::STATUS_GLOBAL_TRANSMISSION;
        }


        return $this;
    } // setStatusGlobalTransmission()

    /**
     * Sets the value of the [status_fragmentation] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setStatusFragmentation($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->status_fragmentation !== $v) {
            $this->status_fragmentation = $v;
            $this->modifiedColumns[] = CommonConsultationArchivePeer::STATUS_FRAGMENTATION;
        }


        return $this;
    } // setStatusFragmentation()

    /**
     * Set the value of [nombre_bloc] column.
     *
     * @param int $v new value
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setNombreBloc($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->nombre_bloc !== $v) {
            $this->nombre_bloc = $v;
            $this->modifiedColumns[] = CommonConsultationArchivePeer::NOMBRE_BLOC;
        }


        return $this;
    } // setNombreBloc()

    /**
     * Set the value of [consultation_id] column.
     *
     * @param int $v new value
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setConsultationId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->consultation_id !== $v) {
            $this->consultation_id = $v;
            $this->modifiedColumns[] = CommonConsultationArchivePeer::CONSULTATION_ID;
        }


        return $this;
    } // setConsultationId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->status_fragmentation !== false) {
                return false;
            }

        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->organisme = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->consultation_ref = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->chemin_fichier = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->date_archivage = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->poids_archivage = ($row[$startcol + 5] !== null) ? (int) $row[$startcol + 5] : null;
            $this->annee_creation_consultation = ($row[$startcol + 6] !== null) ? (int) $row[$startcol + 6] : null;
            $this->status_global_transmission = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->status_fragmentation = ($row[$startcol + 8] !== null) ? (boolean) $row[$startcol + 8] : null;
            $this->nombre_bloc = ($row[$startcol + 9] !== null) ? (int) $row[$startcol + 9] : null;
            $this->consultation_id = ($row[$startcol + 10] !== null) ? (int) $row[$startcol + 10] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 11; // 11 = CommonConsultationArchivePeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating CommonConsultationArchive object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationArchivePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = CommonConsultationArchivePeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->singleCommonConsultationArchiveArcade = null;

            $this->collCommonConsultationArchiveBlocs = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationArchivePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = CommonConsultationArchiveQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(CommonConsultationArchivePeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CommonConsultationArchivePeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->commonConsultationArchiveArcadesScheduledForDeletion !== null) {
                if (!$this->commonConsultationArchiveArcadesScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationArchiveArcadesScheduledForDeletion as $commonConsultationArchiveArcade) {
                        // need to save related object because we set the relation to null
                        $commonConsultationArchiveArcade->save($con);
                    }
                    $this->commonConsultationArchiveArcadesScheduledForDeletion = null;
                }
            }

            if ($this->singleCommonConsultationArchiveArcade !== null) {
                if (!$this->singleCommonConsultationArchiveArcade->isDeleted() && ($this->singleCommonConsultationArchiveArcade->isNew() || $this->singleCommonConsultationArchiveArcade->isModified())) {
                        $affectedRows += $this->singleCommonConsultationArchiveArcade->save($con);
                }
            }

            if ($this->commonConsultationArchiveBlocsScheduledForDeletion !== null) {
                if (!$this->commonConsultationArchiveBlocsScheduledForDeletion->isEmpty()) {
                    foreach ($this->commonConsultationArchiveBlocsScheduledForDeletion as $commonConsultationArchiveBloc) {
                        // need to save related object because we set the relation to null
                        $commonConsultationArchiveBloc->save($con);
                    }
                    $this->commonConsultationArchiveBlocsScheduledForDeletion = null;
                }
            }

            if ($this->collCommonConsultationArchiveBlocs !== null) {
                foreach ($this->collCommonConsultationArchiveBlocs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = CommonConsultationArchivePeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CommonConsultationArchivePeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CommonConsultationArchivePeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(CommonConsultationArchivePeer::ORGANISME)) {
            $modifiedColumns[':p' . $index++]  = '`organisme`';
        }
        if ($this->isColumnModified(CommonConsultationArchivePeer::CONSULTATION_REF)) {
            $modifiedColumns[':p' . $index++]  = '`consultation_ref`';
        }
        if ($this->isColumnModified(CommonConsultationArchivePeer::CHEMIN_FICHIER)) {
            $modifiedColumns[':p' . $index++]  = '`chemin_fichier`';
        }
        if ($this->isColumnModified(CommonConsultationArchivePeer::DATE_ARCHIVAGE)) {
            $modifiedColumns[':p' . $index++]  = '`date_archivage`';
        }
        if ($this->isColumnModified(CommonConsultationArchivePeer::POIDS_ARCHIVAGE)) {
            $modifiedColumns[':p' . $index++]  = '`poids_archivage`';
        }
        if ($this->isColumnModified(CommonConsultationArchivePeer::ANNEE_CREATION_CONSULTATION)) {
            $modifiedColumns[':p' . $index++]  = '`annee_creation_consultation`';
        }
        if ($this->isColumnModified(CommonConsultationArchivePeer::STATUS_GLOBAL_TRANSMISSION)) {
            $modifiedColumns[':p' . $index++]  = '`status_global_transmission`';
        }
        if ($this->isColumnModified(CommonConsultationArchivePeer::STATUS_FRAGMENTATION)) {
            $modifiedColumns[':p' . $index++]  = '`status_fragmentation`';
        }
        if ($this->isColumnModified(CommonConsultationArchivePeer::NOMBRE_BLOC)) {
            $modifiedColumns[':p' . $index++]  = '`nombre_bloc`';
        }
        if ($this->isColumnModified(CommonConsultationArchivePeer::CONSULTATION_ID)) {
            $modifiedColumns[':p' . $index++]  = '`consultation_id`';
        }

        $sql = sprintf(
            'INSERT INTO `consultation_archive` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`organisme`':
                        $stmt->bindValue($identifier, $this->organisme, PDO::PARAM_STR);
                        break;
                    case '`consultation_ref`':
                        $stmt->bindValue($identifier, $this->consultation_ref, PDO::PARAM_INT);
                        break;
                    case '`chemin_fichier`':
                        $stmt->bindValue($identifier, $this->chemin_fichier, PDO::PARAM_STR);
                        break;
                    case '`date_archivage`':
                        $stmt->bindValue($identifier, $this->date_archivage, PDO::PARAM_STR);
                        break;
                    case '`poids_archivage`':
                        $stmt->bindValue($identifier, $this->poids_archivage, PDO::PARAM_INT);
                        break;
                    case '`annee_creation_consultation`':
                        $stmt->bindValue($identifier, $this->annee_creation_consultation, PDO::PARAM_INT);
                        break;
                    case '`status_global_transmission`':
                        $stmt->bindValue($identifier, $this->status_global_transmission, PDO::PARAM_STR);
                        break;
                    case '`status_fragmentation`':
                        $stmt->bindValue($identifier, (int) $this->status_fragmentation, PDO::PARAM_INT);
                        break;
                    case '`nombre_bloc`':
                        $stmt->bindValue($identifier, $this->nombre_bloc, PDO::PARAM_INT);
                        break;
                    case '`consultation_id`':
                        $stmt->bindValue($identifier, $this->consultation_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = CommonConsultationArchivePeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->singleCommonConsultationArchiveArcade !== null) {
                    if (!$this->singleCommonConsultationArchiveArcade->validate($columns)) {
                        $failureMap = array_merge($failureMap, $this->singleCommonConsultationArchiveArcade->getValidationFailures());
                    }
                }

                if ($this->collCommonConsultationArchiveBlocs !== null) {
                    foreach ($this->collCommonConsultationArchiveBlocs as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonConsultationArchivePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getOrganisme();
                break;
            case 2:
                return $this->getConsultationRef();
                break;
            case 3:
                return $this->getCheminFichier();
                break;
            case 4:
                return $this->getDateArchivage();
                break;
            case 5:
                return $this->getPoidsArchivage();
                break;
            case 6:
                return $this->getAnneeCreationConsultation();
                break;
            case 7:
                return $this->getStatusGlobalTransmission();
                break;
            case 8:
                return $this->getStatusFragmentation();
                break;
            case 9:
                return $this->getNombreBloc();
                break;
            case 10:
                return $this->getConsultationId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['CommonConsultationArchive'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CommonConsultationArchive'][$this->getPrimaryKey()] = true;
        $keys = CommonConsultationArchivePeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getOrganisme(),
            $keys[2] => $this->getConsultationRef(),
            $keys[3] => $this->getCheminFichier(),
            $keys[4] => $this->getDateArchivage(),
            $keys[5] => $this->getPoidsArchivage(),
            $keys[6] => $this->getAnneeCreationConsultation(),
            $keys[7] => $this->getStatusGlobalTransmission(),
            $keys[8] => $this->getStatusFragmentation(),
            $keys[9] => $this->getNombreBloc(),
            $keys[10] => $this->getConsultationId(),
        );
        if ($includeForeignObjects) {
            if (null !== $this->singleCommonConsultationArchiveArcade) {
                $result['CommonConsultationArchiveArcade'] = $this->singleCommonConsultationArchiveArcade->toArray($keyType, $includeLazyLoadColumns, $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCommonConsultationArchiveBlocs) {
                $result['CommonConsultationArchiveBlocs'] = $this->collCommonConsultationArchiveBlocs->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = CommonConsultationArchivePeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setOrganisme($value);
                break;
            case 2:
                $this->setConsultationRef($value);
                break;
            case 3:
                $this->setCheminFichier($value);
                break;
            case 4:
                $this->setDateArchivage($value);
                break;
            case 5:
                $this->setPoidsArchivage($value);
                break;
            case 6:
                $this->setAnneeCreationConsultation($value);
                break;
            case 7:
                $this->setStatusGlobalTransmission($value);
                break;
            case 8:
                $this->setStatusFragmentation($value);
                break;
            case 9:
                $this->setNombreBloc($value);
                break;
            case 10:
                $this->setConsultationId($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = CommonConsultationArchivePeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setOrganisme($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setConsultationRef($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCheminFichier($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setDateArchivage($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setPoidsArchivage($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setAnneeCreationConsultation($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setStatusGlobalTransmission($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setStatusFragmentation($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setNombreBloc($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setConsultationId($arr[$keys[10]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CommonConsultationArchivePeer::DATABASE_NAME);

        if ($this->isColumnModified(CommonConsultationArchivePeer::ID)) $criteria->add(CommonConsultationArchivePeer::ID, $this->id);
        if ($this->isColumnModified(CommonConsultationArchivePeer::ORGANISME)) $criteria->add(CommonConsultationArchivePeer::ORGANISME, $this->organisme);
        if ($this->isColumnModified(CommonConsultationArchivePeer::CONSULTATION_REF)) $criteria->add(CommonConsultationArchivePeer::CONSULTATION_REF, $this->consultation_ref);
        if ($this->isColumnModified(CommonConsultationArchivePeer::CHEMIN_FICHIER)) $criteria->add(CommonConsultationArchivePeer::CHEMIN_FICHIER, $this->chemin_fichier);
        if ($this->isColumnModified(CommonConsultationArchivePeer::DATE_ARCHIVAGE)) $criteria->add(CommonConsultationArchivePeer::DATE_ARCHIVAGE, $this->date_archivage);
        if ($this->isColumnModified(CommonConsultationArchivePeer::POIDS_ARCHIVAGE)) $criteria->add(CommonConsultationArchivePeer::POIDS_ARCHIVAGE, $this->poids_archivage);
        if ($this->isColumnModified(CommonConsultationArchivePeer::ANNEE_CREATION_CONSULTATION)) $criteria->add(CommonConsultationArchivePeer::ANNEE_CREATION_CONSULTATION, $this->annee_creation_consultation);
        if ($this->isColumnModified(CommonConsultationArchivePeer::STATUS_GLOBAL_TRANSMISSION)) $criteria->add(CommonConsultationArchivePeer::STATUS_GLOBAL_TRANSMISSION, $this->status_global_transmission);
        if ($this->isColumnModified(CommonConsultationArchivePeer::STATUS_FRAGMENTATION)) $criteria->add(CommonConsultationArchivePeer::STATUS_FRAGMENTATION, $this->status_fragmentation);
        if ($this->isColumnModified(CommonConsultationArchivePeer::NOMBRE_BLOC)) $criteria->add(CommonConsultationArchivePeer::NOMBRE_BLOC, $this->nombre_bloc);
        if ($this->isColumnModified(CommonConsultationArchivePeer::CONSULTATION_ID)) $criteria->add(CommonConsultationArchivePeer::CONSULTATION_ID, $this->consultation_id);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(CommonConsultationArchivePeer::DATABASE_NAME);
        $criteria->add(CommonConsultationArchivePeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of CommonConsultationArchive (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOrganisme($this->getOrganisme());
        $copyObj->setConsultationRef($this->getConsultationRef());
        $copyObj->setCheminFichier($this->getCheminFichier());
        $copyObj->setDateArchivage($this->getDateArchivage());
        $copyObj->setPoidsArchivage($this->getPoidsArchivage());
        $copyObj->setAnneeCreationConsultation($this->getAnneeCreationConsultation());
        $copyObj->setStatusGlobalTransmission($this->getStatusGlobalTransmission());
        $copyObj->setStatusFragmentation($this->getStatusFragmentation());
        $copyObj->setNombreBloc($this->getNombreBloc());
        $copyObj->setConsultationId($this->getConsultationId());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            $relObj = $this->getCommonConsultationArchiveArcade();
            if ($relObj) {
                $copyObj->setCommonConsultationArchiveArcade($relObj->copy($deepCopy));
            }

            foreach ($this->getCommonConsultationArchiveBlocs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCommonConsultationArchiveBloc($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return CommonConsultationArchive Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return CommonConsultationArchivePeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new CommonConsultationArchivePeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CommonConsultationArchiveBloc' == $relationName) {
            $this->initCommonConsultationArchiveBlocs();
        }
    }

    /**
     * Gets a single CommonConsultationArchiveArcade object, which is related to this object by a one-to-one relationship.
     *
     * @param PropelPDO $con optional connection object
     * @return CommonConsultationArchiveArcade
     * @throws PropelException
     */
    public function getCommonConsultationArchiveArcade(PropelPDO $con = null)
    {

        if ($this->singleCommonConsultationArchiveArcade === null && !$this->isNew()) {
            $this->singleCommonConsultationArchiveArcade = CommonConsultationArchiveArcadeQuery::create()->findPk($this->getPrimaryKey(), $con);
        }

        return $this->singleCommonConsultationArchiveArcade;
    }

    /**
     * Sets a single CommonConsultationArchiveArcade object as related to this object by a one-to-one relationship.
     *
     * @param   CommonConsultationArchiveArcade $v CommonConsultationArchiveArcade
     * @return CommonConsultationArchive The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCommonConsultationArchiveArcade(CommonConsultationArchiveArcade $v = null)
    {
        $this->singleCommonConsultationArchiveArcade = $v;

        // Make sure that that the passed-in CommonConsultationArchiveArcade isn't already associated with this object
        if ($v !== null && $v->getCommonConsultationArchive(null, false) === null) {
            $v->setCommonConsultationArchive($this);
        }

        return $this;
    }

    /**
     * Clears out the collCommonConsultationArchiveBlocs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return CommonConsultationArchive The current object (for fluent API support)
     * @see        addCommonConsultationArchiveBlocs()
     */
    public function clearCommonConsultationArchiveBlocs()
    {
        $this->collCommonConsultationArchiveBlocs = null; // important to set this to null since that means it is uninitialized
        $this->collCommonConsultationArchiveBlocsPartial = null;

        return $this;
    }

    /**
     * reset is the collCommonConsultationArchiveBlocs collection loaded partially
     *
     * @return void
     */
    public function resetPartialCommonConsultationArchiveBlocs($v = true)
    {
        $this->collCommonConsultationArchiveBlocsPartial = $v;
    }

    /**
     * Initializes the collCommonConsultationArchiveBlocs collection.
     *
     * By default this just sets the collCommonConsultationArchiveBlocs collection to an empty array (like clearcollCommonConsultationArchiveBlocs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCommonConsultationArchiveBlocs($overrideExisting = true)
    {
        if (null !== $this->collCommonConsultationArchiveBlocs && !$overrideExisting) {
            return;
        }
        $this->collCommonConsultationArchiveBlocs = new PropelObjectCollection();
        $this->collCommonConsultationArchiveBlocs->setModel('CommonConsultationArchiveBloc');
    }

    /**
     * Gets an array of CommonConsultationArchiveBloc objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this CommonConsultationArchive is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|CommonConsultationArchiveBloc[] List of CommonConsultationArchiveBloc objects
     * @throws PropelException
     */
    public function getCommonConsultationArchiveBlocs($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationArchiveBlocsPartial && !$this->isNew();
        if (null === $this->collCommonConsultationArchiveBlocs || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationArchiveBlocs) {
                // return empty collection
                $this->initCommonConsultationArchiveBlocs();
            } else {
                $collCommonConsultationArchiveBlocs = CommonConsultationArchiveBlocQuery::create(null, $criteria)
                    ->filterByCommonConsultationArchive($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collCommonConsultationArchiveBlocsPartial && count($collCommonConsultationArchiveBlocs)) {
                      $this->initCommonConsultationArchiveBlocs(false);

                      foreach ($collCommonConsultationArchiveBlocs as $obj) {
                        if (false == $this->collCommonConsultationArchiveBlocs->contains($obj)) {
                          $this->collCommonConsultationArchiveBlocs->append($obj);
                        }
                      }

                      $this->collCommonConsultationArchiveBlocsPartial = true;
                    }

                    $collCommonConsultationArchiveBlocs->getInternalIterator()->rewind();

                    return $collCommonConsultationArchiveBlocs;
                }

                if ($partial && $this->collCommonConsultationArchiveBlocs) {
                    foreach ($this->collCommonConsultationArchiveBlocs as $obj) {
                        if ($obj->isNew()) {
                            $collCommonConsultationArchiveBlocs[] = $obj;
                        }
                    }
                }

                $this->collCommonConsultationArchiveBlocs = $collCommonConsultationArchiveBlocs;
                $this->collCommonConsultationArchiveBlocsPartial = false;
            }
        }

        return $this->collCommonConsultationArchiveBlocs;
    }

    /**
     * Sets a collection of CommonConsultationArchiveBloc objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $commonConsultationArchiveBlocs A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function setCommonConsultationArchiveBlocs(PropelCollection $commonConsultationArchiveBlocs, PropelPDO $con = null)
    {
        $commonConsultationArchiveBlocsToDelete = $this->getCommonConsultationArchiveBlocs(new Criteria(), $con)->diff($commonConsultationArchiveBlocs);


        $this->commonConsultationArchiveBlocsScheduledForDeletion = $commonConsultationArchiveBlocsToDelete;

        foreach ($commonConsultationArchiveBlocsToDelete as $commonConsultationArchiveBlocRemoved) {
            $commonConsultationArchiveBlocRemoved->setCommonConsultationArchive(null);
        }

        $this->collCommonConsultationArchiveBlocs = null;
        foreach ($commonConsultationArchiveBlocs as $commonConsultationArchiveBloc) {
            $this->addCommonConsultationArchiveBloc($commonConsultationArchiveBloc);
        }

        $this->collCommonConsultationArchiveBlocs = $commonConsultationArchiveBlocs;
        $this->collCommonConsultationArchiveBlocsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CommonConsultationArchiveBloc objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related CommonConsultationArchiveBloc objects.
     * @throws PropelException
     */
    public function countCommonConsultationArchiveBlocs(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collCommonConsultationArchiveBlocsPartial && !$this->isNew();
        if (null === $this->collCommonConsultationArchiveBlocs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCommonConsultationArchiveBlocs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCommonConsultationArchiveBlocs());
            }
            $query = CommonConsultationArchiveBlocQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCommonConsultationArchive($this)
                ->count($con);
        }

        return count($this->collCommonConsultationArchiveBlocs);
    }

    /**
     * Method called to associate a CommonConsultationArchiveBloc object to this object
     * through the CommonConsultationArchiveBloc foreign key attribute.
     *
     * @param   CommonConsultationArchiveBloc $l CommonConsultationArchiveBloc
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function addCommonConsultationArchiveBloc(CommonConsultationArchiveBloc $l)
    {
        if ($this->collCommonConsultationArchiveBlocs === null) {
            $this->initCommonConsultationArchiveBlocs();
            $this->collCommonConsultationArchiveBlocsPartial = true;
        }
        if (!in_array($l, $this->collCommonConsultationArchiveBlocs->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddCommonConsultationArchiveBloc($l);
        }

        return $this;
    }

    /**
     * @param	CommonConsultationArchiveBloc $commonConsultationArchiveBloc The commonConsultationArchiveBloc object to add.
     */
    protected function doAddCommonConsultationArchiveBloc($commonConsultationArchiveBloc)
    {
        $this->collCommonConsultationArchiveBlocs[]= $commonConsultationArchiveBloc;
        $commonConsultationArchiveBloc->setCommonConsultationArchive($this);
    }

    /**
     * @param	CommonConsultationArchiveBloc $commonConsultationArchiveBloc The commonConsultationArchiveBloc object to remove.
     * @return CommonConsultationArchive The current object (for fluent API support)
     */
    public function removeCommonConsultationArchiveBloc($commonConsultationArchiveBloc)
    {
        if ($this->getCommonConsultationArchiveBlocs()->contains($commonConsultationArchiveBloc)) {
            $this->collCommonConsultationArchiveBlocs->remove($this->collCommonConsultationArchiveBlocs->search($commonConsultationArchiveBloc));
            if (null === $this->commonConsultationArchiveBlocsScheduledForDeletion) {
                $this->commonConsultationArchiveBlocsScheduledForDeletion = clone $this->collCommonConsultationArchiveBlocs;
                $this->commonConsultationArchiveBlocsScheduledForDeletion->clear();
            }
            $this->commonConsultationArchiveBlocsScheduledForDeletion[]= clone $commonConsultationArchiveBloc;
            $commonConsultationArchiveBloc->setCommonConsultationArchive(null);
        }

        return $this;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->organisme = null;
        $this->consultation_ref = null;
        $this->chemin_fichier = null;
        $this->date_archivage = null;
        $this->poids_archivage = null;
        $this->annee_creation_consultation = null;
        $this->status_global_transmission = null;
        $this->status_fragmentation = null;
        $this->nombre_bloc = null;
        $this->consultation_id = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->singleCommonConsultationArchiveArcade) {
                $this->singleCommonConsultationArchiveArcade->clearAllReferences($deep);
            }
            if ($this->collCommonConsultationArchiveBlocs) {
                foreach ($this->collCommonConsultationArchiveBlocs as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->singleCommonConsultationArchiveArcade instanceof PropelCollection) {
            $this->singleCommonConsultationArchiveArcade->clearIterator();
        }
        $this->singleCommonConsultationArchiveArcade = null;
        if ($this->collCommonConsultationArchiveBlocs instanceof PropelCollection) {
            $this->collCommonConsultationArchiveBlocs->clearIterator();
        }
        $this->collCommonConsultationArchiveBlocs = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CommonConsultationArchivePeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
