<?php

namespace Application\Propel\Mpe\Om;

use Application\Library\Propel\Query\Criteria;
use \Exception;
use Application\Library\Propel\Query\ModelCriteria;
use Application\Library\Propel\Query\ModelJoin;
use \PDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTAnnonceConsultationQuery;
use Application\Propel\Mpe\CommonTHistoriqueAnnonce;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTTypeAnnonceConsultation;

/**
 * Base class that represents a query for the 't_annonce_consultation' table.
 *
 *
 *
 * @method CommonTAnnonceConsultationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method CommonTAnnonceConsultationQuery orderByRefConsultation($order = Criteria::ASC) Order by the ref_consultation column
 * @method CommonTAnnonceConsultationQuery orderByOrganisme($order = Criteria::ASC) Order by the organisme column
 * @method CommonTAnnonceConsultationQuery orderByIdDossierSub($order = Criteria::ASC) Order by the id_dossier_sub column
 * @method CommonTAnnonceConsultationQuery orderByIdDispositif($order = Criteria::ASC) Order by the id_dispositif column
 * @method CommonTAnnonceConsultationQuery orderByIdCompteBoamp($order = Criteria::ASC) Order by the id_compte_boamp column
 * @method CommonTAnnonceConsultationQuery orderByStatut($order = Criteria::ASC) Order by the statut column
 * @method CommonTAnnonceConsultationQuery orderByIdTypeAnnonce($order = Criteria::ASC) Order by the id_type_annonce column
 * @method CommonTAnnonceConsultationQuery orderByIdAgent($order = Criteria::ASC) Order by the id_agent column
 * @method CommonTAnnonceConsultationQuery orderByPrenomNomAgent($order = Criteria::ASC) Order by the prenom_nom_agent column
 * @method CommonTAnnonceConsultationQuery orderByDateCreation($order = Criteria::ASC) Order by the date_creation column
 * @method CommonTAnnonceConsultationQuery orderByDateModification($order = Criteria::ASC) Order by the date_modification column
 * @method CommonTAnnonceConsultationQuery orderByDateStatut($order = Criteria::ASC) Order by the date_statut column
 * @method CommonTAnnonceConsultationQuery orderByMotifStatut($order = Criteria::ASC) Order by the motif_statut column
 * @method CommonTAnnonceConsultationQuery orderByIdDossierParent($order = Criteria::ASC) Order by the id_dossier_parent column
 * @method CommonTAnnonceConsultationQuery orderByIdDispositifParent($order = Criteria::ASC) Order by the id_dispositif_parent column
 * @method CommonTAnnonceConsultationQuery orderByPubliciteSimplifie($order = Criteria::ASC) Order by the publicite_simplifie column
 * @method CommonTAnnonceConsultationQuery orderByConsultationId($order = Criteria::ASC) Order by the consultation_id column
 *
 * @method CommonTAnnonceConsultationQuery groupById() Group by the id column
 * @method CommonTAnnonceConsultationQuery groupByRefConsultation() Group by the ref_consultation column
 * @method CommonTAnnonceConsultationQuery groupByOrganisme() Group by the organisme column
 * @method CommonTAnnonceConsultationQuery groupByIdDossierSub() Group by the id_dossier_sub column
 * @method CommonTAnnonceConsultationQuery groupByIdDispositif() Group by the id_dispositif column
 * @method CommonTAnnonceConsultationQuery groupByIdCompteBoamp() Group by the id_compte_boamp column
 * @method CommonTAnnonceConsultationQuery groupByStatut() Group by the statut column
 * @method CommonTAnnonceConsultationQuery groupByIdTypeAnnonce() Group by the id_type_annonce column
 * @method CommonTAnnonceConsultationQuery groupByIdAgent() Group by the id_agent column
 * @method CommonTAnnonceConsultationQuery groupByPrenomNomAgent() Group by the prenom_nom_agent column
 * @method CommonTAnnonceConsultationQuery groupByDateCreation() Group by the date_creation column
 * @method CommonTAnnonceConsultationQuery groupByDateModification() Group by the date_modification column
 * @method CommonTAnnonceConsultationQuery groupByDateStatut() Group by the date_statut column
 * @method CommonTAnnonceConsultationQuery groupByMotifStatut() Group by the motif_statut column
 * @method CommonTAnnonceConsultationQuery groupByIdDossierParent() Group by the id_dossier_parent column
 * @method CommonTAnnonceConsultationQuery groupByIdDispositifParent() Group by the id_dispositif_parent column
 * @method CommonTAnnonceConsultationQuery groupByPubliciteSimplifie() Group by the publicite_simplifie column
 * @method CommonTAnnonceConsultationQuery groupByConsultationId() Group by the consultation_id column
 *
 * @method CommonTAnnonceConsultationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method CommonTAnnonceConsultationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method CommonTAnnonceConsultationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method CommonTAnnonceConsultationQuery leftJoinCommonConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonConsultation relation
 * @method CommonTAnnonceConsultationQuery rightJoinCommonConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonConsultation relation
 * @method CommonTAnnonceConsultationQuery innerJoinCommonConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonConsultation relation
 *
 * @method CommonTAnnonceConsultationQuery leftJoinCommonTTypeAnnonceConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTTypeAnnonceConsultation relation
 * @method CommonTAnnonceConsultationQuery rightJoinCommonTTypeAnnonceConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTTypeAnnonceConsultation relation
 * @method CommonTAnnonceConsultationQuery innerJoinCommonTTypeAnnonceConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTTypeAnnonceConsultation relation
 *
 * @method CommonTAnnonceConsultationQuery leftJoinCommonTHistoriqueAnnonce($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTHistoriqueAnnonce relation
 * @method CommonTAnnonceConsultationQuery rightJoinCommonTHistoriqueAnnonce($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTHistoriqueAnnonce relation
 * @method CommonTAnnonceConsultationQuery innerJoinCommonTHistoriqueAnnonce($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTHistoriqueAnnonce relation
 *
 * @method CommonTAnnonceConsultationQuery leftJoinCommonTSupportAnnonceConsultation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CommonTSupportAnnonceConsultation relation
 * @method CommonTAnnonceConsultationQuery rightJoinCommonTSupportAnnonceConsultation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CommonTSupportAnnonceConsultation relation
 * @method CommonTAnnonceConsultationQuery innerJoinCommonTSupportAnnonceConsultation($relationAlias = null) Adds a INNER JOIN clause to the query using the CommonTSupportAnnonceConsultation relation
 *
 * @method CommonTAnnonceConsultation findOne(PropelPDO $con = null) Return the first CommonTAnnonceConsultation matching the query
 * @method CommonTAnnonceConsultation findOneOrCreate(PropelPDO $con = null) Return the first CommonTAnnonceConsultation matching the query, or a new CommonTAnnonceConsultation object populated from the query conditions when no match is found
 *
 * @method CommonTAnnonceConsultation findOneByRefConsultation(int $ref_consultation) Return the first CommonTAnnonceConsultation filtered by the ref_consultation column
 * @method CommonTAnnonceConsultation findOneByOrganisme(string $organisme) Return the first CommonTAnnonceConsultation filtered by the organisme column
 * @method CommonTAnnonceConsultation findOneByIdDossierSub(int $id_dossier_sub) Return the first CommonTAnnonceConsultation filtered by the id_dossier_sub column
 * @method CommonTAnnonceConsultation findOneByIdDispositif(int $id_dispositif) Return the first CommonTAnnonceConsultation filtered by the id_dispositif column
 * @method CommonTAnnonceConsultation findOneByIdCompteBoamp(int $id_compte_boamp) Return the first CommonTAnnonceConsultation filtered by the id_compte_boamp column
 * @method CommonTAnnonceConsultation findOneByStatut(string $statut) Return the first CommonTAnnonceConsultation filtered by the statut column
 * @method CommonTAnnonceConsultation findOneByIdTypeAnnonce(int $id_type_annonce) Return the first CommonTAnnonceConsultation filtered by the id_type_annonce column
 * @method CommonTAnnonceConsultation findOneByIdAgent(int $id_agent) Return the first CommonTAnnonceConsultation filtered by the id_agent column
 * @method CommonTAnnonceConsultation findOneByPrenomNomAgent(string $prenom_nom_agent) Return the first CommonTAnnonceConsultation filtered by the prenom_nom_agent column
 * @method CommonTAnnonceConsultation findOneByDateCreation(string $date_creation) Return the first CommonTAnnonceConsultation filtered by the date_creation column
 * @method CommonTAnnonceConsultation findOneByDateModification(string $date_modification) Return the first CommonTAnnonceConsultation filtered by the date_modification column
 * @method CommonTAnnonceConsultation findOneByDateStatut(string $date_statut) Return the first CommonTAnnonceConsultation filtered by the date_statut column
 * @method CommonTAnnonceConsultation findOneByMotifStatut(string $motif_statut) Return the first CommonTAnnonceConsultation filtered by the motif_statut column
 * @method CommonTAnnonceConsultation findOneByIdDossierParent(int $id_dossier_parent) Return the first CommonTAnnonceConsultation filtered by the id_dossier_parent column
 * @method CommonTAnnonceConsultation findOneByIdDispositifParent(int $id_dispositif_parent) Return the first CommonTAnnonceConsultation filtered by the id_dispositif_parent column
 * @method CommonTAnnonceConsultation findOneByPubliciteSimplifie(string $publicite_simplifie) Return the first CommonTAnnonceConsultation filtered by the publicite_simplifie column
 * @method CommonTAnnonceConsultation findOneByConsultationId(int $consultation_id) Return the first CommonTAnnonceConsultation filtered by the consultation_id column
 *
 * @method array findById(int $id) Return CommonTAnnonceConsultation objects filtered by the id column
 * @method array findByRefConsultation(int $ref_consultation) Return CommonTAnnonceConsultation objects filtered by the ref_consultation column
 * @method array findByOrganisme(string $organisme) Return CommonTAnnonceConsultation objects filtered by the organisme column
 * @method array findByIdDossierSub(int $id_dossier_sub) Return CommonTAnnonceConsultation objects filtered by the id_dossier_sub column
 * @method array findByIdDispositif(int $id_dispositif) Return CommonTAnnonceConsultation objects filtered by the id_dispositif column
 * @method array findByIdCompteBoamp(int $id_compte_boamp) Return CommonTAnnonceConsultation objects filtered by the id_compte_boamp column
 * @method array findByStatut(string $statut) Return CommonTAnnonceConsultation objects filtered by the statut column
 * @method array findByIdTypeAnnonce(int $id_type_annonce) Return CommonTAnnonceConsultation objects filtered by the id_type_annonce column
 * @method array findByIdAgent(int $id_agent) Return CommonTAnnonceConsultation objects filtered by the id_agent column
 * @method array findByPrenomNomAgent(string $prenom_nom_agent) Return CommonTAnnonceConsultation objects filtered by the prenom_nom_agent column
 * @method array findByDateCreation(string $date_creation) Return CommonTAnnonceConsultation objects filtered by the date_creation column
 * @method array findByDateModification(string $date_modification) Return CommonTAnnonceConsultation objects filtered by the date_modification column
 * @method array findByDateStatut(string $date_statut) Return CommonTAnnonceConsultation objects filtered by the date_statut column
 * @method array findByMotifStatut(string $motif_statut) Return CommonTAnnonceConsultation objects filtered by the motif_statut column
 * @method array findByIdDossierParent(int $id_dossier_parent) Return CommonTAnnonceConsultation objects filtered by the id_dossier_parent column
 * @method array findByIdDispositifParent(int $id_dispositif_parent) Return CommonTAnnonceConsultation objects filtered by the id_dispositif_parent column
 * @method array findByPubliciteSimplifie(string $publicite_simplifie) Return CommonTAnnonceConsultation objects filtered by the publicite_simplifie column
 * @method array findByConsultationId(int $consultation_id) Return CommonTAnnonceConsultation objects filtered by the consultation_id column
 *
 * @package    propel.generator.Application.Propel.Mpe.om
 */
abstract class BaseCommonTAnnonceConsultationQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseCommonTAnnonceConsultationQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'mpe', $modelName = 'Application\\Propel\\Mpe\\CommonTAnnonceConsultation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new CommonTAnnonceConsultationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   CommonTAnnonceConsultationQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return CommonTAnnonceConsultationQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof CommonTAnnonceConsultationQuery) {
            return $criteria;
        }
        $query = new CommonTAnnonceConsultationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   CommonTAnnonceConsultation|CommonTAnnonceConsultation[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = CommonTAnnonceConsultationPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(CommonTAnnonceConsultationPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTAnnonceConsultation A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 CommonTAnnonceConsultation A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `ref_consultation`, `organisme`, `id_dossier_sub`, `id_dispositif`, `id_compte_boamp`, `statut`, `id_type_annonce`, `id_agent`, `prenom_nom_agent`, `date_creation`, `date_modification`, `date_statut`, `motif_statut`, `id_dossier_parent`, `id_dispositif_parent`, `publicite_simplifie`, `consultation_id` FROM `t_annonce_consultation` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new CommonTAnnonceConsultation();
            $obj->hydrate($row);
            CommonTAnnonceConsultationPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return CommonTAnnonceConsultation|CommonTAnnonceConsultation[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|CommonTAnnonceConsultation[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the ref_consultation column
     *
     * Example usage:
     * <code>
     * $query->filterByRefConsultation(1234); // WHERE ref_consultation = 1234
     * $query->filterByRefConsultation(array(12, 34)); // WHERE ref_consultation IN (12, 34)
     * $query->filterByRefConsultation(array('min' => 12)); // WHERE ref_consultation >= 12
     * $query->filterByRefConsultation(array('max' => 12)); // WHERE ref_consultation <= 12
     * </code>
     *
     * @param     mixed $refConsultation The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByRefConsultation($refConsultation = null, $comparison = null)
    {
        if (is_array($refConsultation)) {
            $useMinMax = false;
            if (isset($refConsultation['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::REF_CONSULTATION, $refConsultation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($refConsultation['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::REF_CONSULTATION, $refConsultation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::REF_CONSULTATION, $refConsultation, $comparison);
    }

    /**
     * Filter the query on the organisme column
     *
     * Example usage:
     * <code>
     * $query->filterByOrganisme('fooValue');   // WHERE organisme = 'fooValue'
     * $query->filterByOrganisme('%fooValue%'); // WHERE organisme LIKE '%fooValue%'
     * </code>
     *
     * @param     string $organisme The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByOrganisme($organisme = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($organisme)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $organisme)) {
                $organisme = str_replace('*', '%', $organisme);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::ORGANISME, $organisme, $comparison);
    }

    /**
     * Filter the query on the id_dossier_sub column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDossierSub(1234); // WHERE id_dossier_sub = 1234
     * $query->filterByIdDossierSub(array(12, 34)); // WHERE id_dossier_sub IN (12, 34)
     * $query->filterByIdDossierSub(array('min' => 12)); // WHERE id_dossier_sub >= 12
     * $query->filterByIdDossierSub(array('max' => 12)); // WHERE id_dossier_sub <= 12
     * </code>
     *
     * @param     mixed $idDossierSub The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByIdDossierSub($idDossierSub = null, $comparison = null)
    {
        if (is_array($idDossierSub)) {
            $useMinMax = false;
            if (isset($idDossierSub['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DOSSIER_SUB, $idDossierSub['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDossierSub['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DOSSIER_SUB, $idDossierSub['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DOSSIER_SUB, $idDossierSub, $comparison);
    }

    /**
     * Filter the query on the id_dispositif column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDispositif(1234); // WHERE id_dispositif = 1234
     * $query->filterByIdDispositif(array(12, 34)); // WHERE id_dispositif IN (12, 34)
     * $query->filterByIdDispositif(array('min' => 12)); // WHERE id_dispositif >= 12
     * $query->filterByIdDispositif(array('max' => 12)); // WHERE id_dispositif <= 12
     * </code>
     *
     * @param     mixed $idDispositif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByIdDispositif($idDispositif = null, $comparison = null)
    {
        if (is_array($idDispositif)) {
            $useMinMax = false;
            if (isset($idDispositif['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DISPOSITIF, $idDispositif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDispositif['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DISPOSITIF, $idDispositif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DISPOSITIF, $idDispositif, $comparison);
    }

    /**
     * Filter the query on the id_compte_boamp column
     *
     * Example usage:
     * <code>
     * $query->filterByIdCompteBoamp(1234); // WHERE id_compte_boamp = 1234
     * $query->filterByIdCompteBoamp(array(12, 34)); // WHERE id_compte_boamp IN (12, 34)
     * $query->filterByIdCompteBoamp(array('min' => 12)); // WHERE id_compte_boamp >= 12
     * $query->filterByIdCompteBoamp(array('max' => 12)); // WHERE id_compte_boamp <= 12
     * </code>
     *
     * @param     mixed $idCompteBoamp The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByIdCompteBoamp($idCompteBoamp = null, $comparison = null)
    {
        if (is_array($idCompteBoamp)) {
            $useMinMax = false;
            if (isset($idCompteBoamp['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_COMPTE_BOAMP, $idCompteBoamp['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idCompteBoamp['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_COMPTE_BOAMP, $idCompteBoamp['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_COMPTE_BOAMP, $idCompteBoamp, $comparison);
    }

    /**
     * Filter the query on the statut column
     *
     * Example usage:
     * <code>
     * $query->filterByStatut('fooValue');   // WHERE statut = 'fooValue'
     * $query->filterByStatut('%fooValue%'); // WHERE statut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $statut The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByStatut($statut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($statut)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $statut)) {
                $statut = str_replace('*', '%', $statut);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::STATUT, $statut, $comparison);
    }

    /**
     * Filter the query on the id_type_annonce column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTypeAnnonce(1234); // WHERE id_type_annonce = 1234
     * $query->filterByIdTypeAnnonce(array(12, 34)); // WHERE id_type_annonce IN (12, 34)
     * $query->filterByIdTypeAnnonce(array('min' => 12)); // WHERE id_type_annonce >= 12
     * $query->filterByIdTypeAnnonce(array('max' => 12)); // WHERE id_type_annonce <= 12
     * </code>
     *
     * @see       filterByCommonTTypeAnnonceConsultation()
     *
     * @param     mixed $idTypeAnnonce The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByIdTypeAnnonce($idTypeAnnonce = null, $comparison = null)
    {
        if (is_array($idTypeAnnonce)) {
            $useMinMax = false;
            if (isset($idTypeAnnonce['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, $idTypeAnnonce['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTypeAnnonce['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, $idTypeAnnonce['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, $idTypeAnnonce, $comparison);
    }

    /**
     * Filter the query on the id_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdAgent(1234); // WHERE id_agent = 1234
     * $query->filterByIdAgent(array(12, 34)); // WHERE id_agent IN (12, 34)
     * $query->filterByIdAgent(array('min' => 12)); // WHERE id_agent >= 12
     * $query->filterByIdAgent(array('max' => 12)); // WHERE id_agent <= 12
     * </code>
     *
     * @param     mixed $idAgent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByIdAgent($idAgent = null, $comparison = null)
    {
        if (is_array($idAgent)) {
            $useMinMax = false;
            if (isset($idAgent['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_AGENT, $idAgent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idAgent['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_AGENT, $idAgent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_AGENT, $idAgent, $comparison);
    }

    /**
     * Filter the query on the prenom_nom_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByPrenomNomAgent('fooValue');   // WHERE prenom_nom_agent = 'fooValue'
     * $query->filterByPrenomNomAgent('%fooValue%'); // WHERE prenom_nom_agent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prenomNomAgent The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByPrenomNomAgent($prenomNomAgent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prenomNomAgent)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prenomNomAgent)) {
                $prenomNomAgent = str_replace('*', '%', $prenomNomAgent);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::PRENOM_NOM_AGENT, $prenomNomAgent, $comparison);
    }

    /**
     * Filter the query on the date_creation column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreation('2011-03-14'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation('now'); // WHERE date_creation = '2011-03-14'
     * $query->filterByDateCreation(array('max' => 'yesterday')); // WHERE date_creation > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreation The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByDateCreation($dateCreation = null, $comparison = null)
    {
        if (is_array($dateCreation)) {
            $useMinMax = false;
            if (isset($dateCreation['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::DATE_CREATION, $dateCreation['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreation['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::DATE_CREATION, $dateCreation['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::DATE_CREATION, $dateCreation, $comparison);
    }

    /**
     * Filter the query on the date_modification column
     *
     * Example usage:
     * <code>
     * $query->filterByDateModification('2011-03-14'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification('now'); // WHERE date_modification = '2011-03-14'
     * $query->filterByDateModification(array('max' => 'yesterday')); // WHERE date_modification > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateModification The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByDateModification($dateModification = null, $comparison = null)
    {
        if (is_array($dateModification)) {
            $useMinMax = false;
            if (isset($dateModification['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::DATE_MODIFICATION, $dateModification['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateModification['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::DATE_MODIFICATION, $dateModification['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::DATE_MODIFICATION, $dateModification, $comparison);
    }

    /**
     * Filter the query on the date_statut column
     *
     * Example usage:
     * <code>
     * $query->filterByDateStatut('2011-03-14'); // WHERE date_statut = '2011-03-14'
     * $query->filterByDateStatut('now'); // WHERE date_statut = '2011-03-14'
     * $query->filterByDateStatut(array('max' => 'yesterday')); // WHERE date_statut > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateStatut The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByDateStatut($dateStatut = null, $comparison = null)
    {
        if (is_array($dateStatut)) {
            $useMinMax = false;
            if (isset($dateStatut['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::DATE_STATUT, $dateStatut['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateStatut['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::DATE_STATUT, $dateStatut['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::DATE_STATUT, $dateStatut, $comparison);
    }

    /**
     * Filter the query on the motif_statut column
     *
     * Example usage:
     * <code>
     * $query->filterByMotifStatut('fooValue');   // WHERE motif_statut = 'fooValue'
     * $query->filterByMotifStatut('%fooValue%'); // WHERE motif_statut LIKE '%fooValue%'
     * </code>
     *
     * @param     string $motifStatut The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByMotifStatut($motifStatut = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($motifStatut)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $motifStatut)) {
                $motifStatut = str_replace('*', '%', $motifStatut);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::MOTIF_STATUT, $motifStatut, $comparison);
    }

    /**
     * Filter the query on the id_dossier_parent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDossierParent(1234); // WHERE id_dossier_parent = 1234
     * $query->filterByIdDossierParent(array(12, 34)); // WHERE id_dossier_parent IN (12, 34)
     * $query->filterByIdDossierParent(array('min' => 12)); // WHERE id_dossier_parent >= 12
     * $query->filterByIdDossierParent(array('max' => 12)); // WHERE id_dossier_parent <= 12
     * </code>
     *
     * @param     mixed $idDossierParent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByIdDossierParent($idDossierParent = null, $comparison = null)
    {
        if (is_array($idDossierParent)) {
            $useMinMax = false;
            if (isset($idDossierParent['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DOSSIER_PARENT, $idDossierParent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDossierParent['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DOSSIER_PARENT, $idDossierParent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DOSSIER_PARENT, $idDossierParent, $comparison);
    }

    /**
     * Filter the query on the id_dispositif_parent column
     *
     * Example usage:
     * <code>
     * $query->filterByIdDispositifParent(1234); // WHERE id_dispositif_parent = 1234
     * $query->filterByIdDispositifParent(array(12, 34)); // WHERE id_dispositif_parent IN (12, 34)
     * $query->filterByIdDispositifParent(array('min' => 12)); // WHERE id_dispositif_parent >= 12
     * $query->filterByIdDispositifParent(array('max' => 12)); // WHERE id_dispositif_parent <= 12
     * </code>
     *
     * @param     mixed $idDispositifParent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByIdDispositifParent($idDispositifParent = null, $comparison = null)
    {
        if (is_array($idDispositifParent)) {
            $useMinMax = false;
            if (isset($idDispositifParent['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DISPOSITIF_PARENT, $idDispositifParent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idDispositifParent['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DISPOSITIF_PARENT, $idDispositifParent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID_DISPOSITIF_PARENT, $idDispositifParent, $comparison);
    }

    /**
     * Filter the query on the publicite_simplifie column
     *
     * Example usage:
     * <code>
     * $query->filterByPubliciteSimplifie('fooValue');   // WHERE publicite_simplifie = 'fooValue'
     * $query->filterByPubliciteSimplifie('%fooValue%'); // WHERE publicite_simplifie LIKE '%fooValue%'
     * </code>
     *
     * @param     string $publiciteSimplifie The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByPubliciteSimplifie($publiciteSimplifie = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($publiciteSimplifie)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $publiciteSimplifie)) {
                $publiciteSimplifie = str_replace('*', '%', $publiciteSimplifie);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::PUBLICITE_SIMPLIFIE, $publiciteSimplifie, $comparison);
    }

    /**
     * Filter the query on the consultation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByConsultationId(1234); // WHERE consultation_id = 1234
     * $query->filterByConsultationId(array(12, 34)); // WHERE consultation_id IN (12, 34)
     * $query->filterByConsultationId(array('min' => 12)); // WHERE consultation_id >= 12
     * $query->filterByConsultationId(array('max' => 12)); // WHERE consultation_id <= 12
     * </code>
     *
     * @see       filterByCommonConsultation()
     *
     * @param     mixed $consultationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function filterByConsultationId($consultationId = null, $comparison = null)
    {
        if (is_array($consultationId)) {
            $useMinMax = false;
            if (isset($consultationId['min'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::CONSULTATION_ID, $consultationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($consultationId['max'])) {
                $this->addUsingAlias(CommonTAnnonceConsultationPeer::CONSULTATION_ID, $consultationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CommonTAnnonceConsultationPeer::CONSULTATION_ID, $consultationId, $comparison);
    }

    /**
     * Filter the query by a related CommonConsultation object
     *
     * @param   CommonConsultation|PropelObjectCollection $commonConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTAnnonceConsultationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonConsultation($commonConsultation, $comparison = null)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            return $this
                ->addUsingAlias(CommonTAnnonceConsultationPeer::CONSULTATION_ID, $commonConsultation->getId(), $comparison);
        } elseif ($commonConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTAnnonceConsultationPeer::CONSULTATION_ID, $commonConsultation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonConsultation() only accepts arguments of type CommonConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function joinCommonConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonConsultation relation CommonConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonConsultation', '\Application\Propel\Mpe\CommonConsultationQuery');
    }

    /**
     * Filter the query by a related CommonTTypeAnnonceConsultation object
     *
     * @param   CommonTTypeAnnonceConsultation|PropelObjectCollection $commonTTypeAnnonceConsultation The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTAnnonceConsultationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTTypeAnnonceConsultation($commonTTypeAnnonceConsultation, $comparison = null)
    {
        if ($commonTTypeAnnonceConsultation instanceof CommonTTypeAnnonceConsultation) {
            return $this
                ->addUsingAlias(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, $commonTTypeAnnonceConsultation->getId(), $comparison);
        } elseif ($commonTTypeAnnonceConsultation instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CommonTAnnonceConsultationPeer::ID_TYPE_ANNONCE, $commonTTypeAnnonceConsultation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCommonTTypeAnnonceConsultation() only accepts arguments of type CommonTTypeAnnonceConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTTypeAnnonceConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function joinCommonTTypeAnnonceConsultation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTTypeAnnonceConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTTypeAnnonceConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonTTypeAnnonceConsultation relation CommonTTypeAnnonceConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTTypeAnnonceConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonTTypeAnnonceConsultationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCommonTTypeAnnonceConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTTypeAnnonceConsultation', '\Application\Propel\Mpe\CommonTTypeAnnonceConsultationQuery');
    }

    /**
     * Filter the query by a related CommonTHistoriqueAnnonce object
     *
     * @param   CommonTHistoriqueAnnonce|PropelObjectCollection $commonTHistoriqueAnnonce  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTAnnonceConsultationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTHistoriqueAnnonce($commonTHistoriqueAnnonce, $comparison = null)
    {
        if ($commonTHistoriqueAnnonce instanceof CommonTHistoriqueAnnonce) {
            return $this
                ->addUsingAlias(CommonTAnnonceConsultationPeer::ID, $commonTHistoriqueAnnonce->getIdAnnonce(), $comparison);
        } elseif ($commonTHistoriqueAnnonce instanceof PropelObjectCollection) {
            return $this
                ->useCommonTHistoriqueAnnonceQuery()
                ->filterByPrimaryKeys($commonTHistoriqueAnnonce->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTHistoriqueAnnonce() only accepts arguments of type CommonTHistoriqueAnnonce or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTHistoriqueAnnonce relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function joinCommonTHistoriqueAnnonce($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTHistoriqueAnnonce');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTHistoriqueAnnonce');
        }

        return $this;
    }

    /**
     * Use the CommonTHistoriqueAnnonce relation CommonTHistoriqueAnnonce object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTHistoriqueAnnonceQuery A secondary query class using the current class as primary query
     */
    public function useCommonTHistoriqueAnnonceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTHistoriqueAnnonce($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTHistoriqueAnnonce', '\Application\Propel\Mpe\CommonTHistoriqueAnnonceQuery');
    }

    /**
     * Filter the query by a related CommonTSupportAnnonceConsultation object
     *
     * @param   CommonTSupportAnnonceConsultation|PropelObjectCollection $commonTSupportAnnonceConsultation  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 CommonTAnnonceConsultationQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCommonTSupportAnnonceConsultation($commonTSupportAnnonceConsultation, $comparison = null)
    {
        if ($commonTSupportAnnonceConsultation instanceof CommonTSupportAnnonceConsultation) {
            return $this
                ->addUsingAlias(CommonTAnnonceConsultationPeer::ID, $commonTSupportAnnonceConsultation->getIdAnnonceCons(), $comparison);
        } elseif ($commonTSupportAnnonceConsultation instanceof PropelObjectCollection) {
            return $this
                ->useCommonTSupportAnnonceConsultationQuery()
                ->filterByPrimaryKeys($commonTSupportAnnonceConsultation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCommonTSupportAnnonceConsultation() only accepts arguments of type CommonTSupportAnnonceConsultation or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CommonTSupportAnnonceConsultation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function joinCommonTSupportAnnonceConsultation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CommonTSupportAnnonceConsultation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CommonTSupportAnnonceConsultation');
        }

        return $this;
    }

    /**
     * Use the CommonTSupportAnnonceConsultation relation CommonTSupportAnnonceConsultation object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \Application\Propel\Mpe\CommonTSupportAnnonceConsultationQuery A secondary query class using the current class as primary query
     */
    public function useCommonTSupportAnnonceConsultationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCommonTSupportAnnonceConsultation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CommonTSupportAnnonceConsultation', '\Application\Propel\Mpe\CommonTSupportAnnonceConsultationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   CommonTAnnonceConsultation $commonTAnnonceConsultation Object to remove from the list of results
     *
     * @return CommonTAnnonceConsultationQuery The current query, for fluid interface
     */
    public function prune($commonTAnnonceConsultation = null)
    {
        if ($commonTAnnonceConsultation) {
            $this->addUsingAlias(CommonTAnnonceConsultationPeer::ID, $commonTAnnonceConsultation->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
