<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\Om\BaseCommonChorusPj;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_LoggerManager;


/**
 * Skeleton subclass for representing a row from the 'Chorus_pj' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonChorusPj extends BaseCommonChorusPj {
	private $_contentHorodatage = null;
	public function getHorodatage() {
		if(!$this->_contentHorodatage) {
			$this->_contentHorodatage = stream_get_contents( parent::getHorodatage());
		}
		return $this->_contentHorodatage;
	}

    /**
     * Suppression automatique des blob apres la suppression effective de l'objet ChorusPj
     *
     * @param PropelPDO|null $con
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @since 2017-roadmap
     * @copyright Atexo 2017
     */
    public function postDelete(PropelPDO $con = null)
    {
        if(!empty($this->getFichier())) {
            $atexoBlob = new Atexo_Blob();
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $atexoBlob->deleteBlobFile($this->getFichier(), $this->getOrganisme());
            $logger->info('[echange_id = '. $this->getIdEchange() .' | org = '. $this->getOrganisme() .' | chorus_pj_id = '. $this->getId() .'] pj supprimee dans les blob (id_blob = '. $this->getFichier() .')' );
        }
        return true;
    }

} // CommonChorusPj
