<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Prado\Prado;




use Application\Propel\Mpe\Om\BaseCommonTGroupementEntreprise;

/**
 * Skeleton subclass for representing a row from the 't_groupement_entreprise' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTGroupementEntreprise extends BaseCommonTGroupementEntreprise
{
    protected $membres=array();

    /**
     * @return mixed
     */
    public function getMembres()
    {
        return $this->membres;
    }

    /**
     * @param mixed $membres
     */
    public function setMembres($membres)
    {
        $this->membres = $membres;
    }

    /**
     * @param mixed $membres
     */
    public function addOneMembres($membre)
    {
        $this->membres[$membre->getIdEntreprise()] = $membre;
    }

    /**
     * Permet de retourner le libelle de type de groupement
     *
     * @return mixed|string
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public function getLibelleTypeGroupement()
    {
        $typeGroupement = $this->getCommonTTypeGroupementEntreprise();
        if($typeGroupement instanceof CommonTTypeGroupementEntreprise) {
            return  Prado::Localize($typeGroupement->getLibelleTypeGroupement());
        }
    }
}
