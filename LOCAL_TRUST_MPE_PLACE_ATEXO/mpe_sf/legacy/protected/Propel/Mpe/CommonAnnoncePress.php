<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Query\Criteria;
use Application\Library\Propel\Collection\PropelObjectCollection;



use Application\Propel\Mpe\Om\BaseCommonAnnoncePress;

/**
 * Skeleton subclass for representing a row from the 'Annonce_Press' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAnnoncePress extends BaseCommonAnnoncePress {

	/**
	 * La méthode getCommonAnnoncePressPieceJointes() se trouvant dans le fichier BaseCommonAnnoncePress.php retourne un 
	 * objet collection "PropelObjectCollection". C'est pour cette raison que nous surchargeons la méthode pour
	 * retourner un tableau d'objets
	 * @param Criteria $criteria: critéria
     * @param $con: objet collection
     * @return: retourne un tableau d'objet recupéré dans la collection
	 */
	public function getCommonAnnoncePressPieceJointes($criteria = null, $con = null)
    {
    	$collectionAnnoncePressPieceJointes = parent::getCommonAnnoncePressPieceJointes($criteria, $con);
    	if(is_object($collectionAnnoncePressPieceJointes) && $collectionAnnoncePressPieceJointes->getData()) {
    		return $collectionAnnoncePressPieceJointes->getData();
    	}
    	return array();
    }


} // CommonAnnoncePress
