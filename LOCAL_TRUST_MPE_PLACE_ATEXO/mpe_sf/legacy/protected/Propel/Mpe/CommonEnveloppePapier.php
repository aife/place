<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffrePapier;





use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonEnveloppePapier;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for representing a row from the 'Enveloppe_papier' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEnveloppePapier extends BaseCommonEnveloppePapier {

   private ?\Application\Propel\Mpe\CommonOffrePapier $_commonOffrePapier = null;
   private string $_libelleStatutEnveloppe="";
   private $_numPli;
   private array $_arrayAdmissibiliteLot=array();
   private $_intituleLot;
   private string $_statistiqueAvisCAO = "";

    public function getCommonOffrePapier() 
    {
    	 return $this->_commonOffrePapier;
    }

    public function setCommonOffrePapier(CommonOffrePapier $commonOffrePapier) 
    {
     	$this->_commonOffrePapier=$commonOffrePapier;
    }    

	public function getLibelleStatutEnveloppe()
	{
		return $this->_libelleStatutEnveloppe;
	}

	public function setLibelleStatutEnveloppe($libelleStatutEnveloppe)
	{
		$this->_libelleStatutEnveloppe=$libelleStatutEnveloppe;
	}

	public function getNumPli() 
	{
		return $this->_numPli;
	}

	public function setNumPli($numPli) 
	{
		$this->_numPli=$numPli;
	}

	public function getCollAdmissibiliteLot() 
	{
		return $this->_arrayAdmissibiliteLot;
	}

	public function addAdmissibiliteLot($admissibiliteLot) 
	{
		$this->_arrayAdmissibiliteLot[]=$admissibiliteLot;
	}

	public function getIntituleLot()
	{
		return $this->_intituleLot;
	}

	public function setIntituleLot($intituleLot)
	{
		$this->_intituleLot=$intituleLot;
	}

	public function setStatistiqueAvisCAO($statistiqueAvisCAO) 
	{
		$this->_statistiqueAvisCAO=$statistiqueAvisCAO;
	}

	public function getStatistiqueAvisCAO() 
	{
		return $this->_statistiqueAvisCAO;
	}

	public function getOffrePapier($connexionCom=null)
    {
        if(!$connexionCom){
        	$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
    	$c=new Criteria();
        $c->add(CommonOffrePapierPeer::ID,$this->getOffrePapierId());
        $c->add(CommonOffrePapierPeer::ORGANISME,$this->getOrganisme());
        $commonOffre = CommonOffrePapierPeer::doSelectOne($c,$connexionCom);
        return $commonOffre;
    }
} // CommonEnveloppePapier
