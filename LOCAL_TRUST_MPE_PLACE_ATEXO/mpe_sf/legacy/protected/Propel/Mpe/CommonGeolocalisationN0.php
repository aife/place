<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\Om\BaseCommonGeolocalisationN0;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Skeleton subclass for representing a row from the 'GeolocalisationN0' table.
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class CommonGeolocalisationN0 extends BaseCommonGeolocalisationN0
{
    /**
     * retourne la valeur de la denomination1 ou celle traduit, en fonction de la langue.
     *
     * @return string
     */
    public function getDenomination1Traduit()
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $denomination = 'denomination_'.$langue;
        if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
            || !$this->$denomination) {
            return $this->denomination;
        } else {
            return $this->$denomination;
        }
    }
} // CommonGeolocalisationN0
