<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonTDumeContexte;

use Application\Propel\Mpe\CommonInscrit;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonTCandidature;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;


/**
 * Skeleton subclass for representing a row from the 't_candidature' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTCandidature extends BaseCommonTCandidature
{

    protected $infosUser;
    protected $nomEntreprise;
    protected $dumeNumeros;

    public function getInfosUser()
    {
        if(!$this->infosUser){
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($this->getIdInscrit());
            if($inscrit instanceof CommonInscrit){
                $this->setInfosUser($inscrit->getNom()." ".$inscrit->getPrenom()." ( ".$inscrit->getEmail()." )");
            }
        }
        return $this->infosUser;
    }

    /**
     * @param mixed $infosUser
     */
    public function setInfosUser($infosUser)
    {
        $this->infosUser = $infosUser;
    }

    /**
     * @return mixed
     */
    public function getNomEntreprise()
    {
        if(!$this->nomEntreprise){
            $nomEntreprise = Atexo_Entreprise::getNomEntreprise($this->getIdEntreprise());
            if($nomEntreprise){
                $this->setNomEntreprise($nomEntreprise);
            }
        }
        return $this->nomEntreprise;
    }

    /**
     * @param mixed $nomEntreprise
     */
    public function setNomEntreprise($nomEntreprise)
    {
        $this->nomEntreprise = $nomEntreprise;
    }


    /**
     * @return mixed
     */
    public function getDumeNumeros()
    {
        if(! $this->dumeNumeros){
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
            );
            $contexteDume = $this->getCommonTDumeContexte($connexion);
            if($contexteDume instanceof CommonTDumeContexte){
                $this->setDumeNumeros($contexteDume->getCommonTDumeNumeros($connexion));
            }
        }
        return $this->dumeNumeros;
    }

    /**
     * @param mixed $dumeNumeros
     */
    public function setDumeNumeros($dumeNumeros)
    {
        $this->dumeNumeros = $dumeNumeros;
    }
}
