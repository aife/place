<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Library\Propel\Connection\PropelPDO;
use \Exception;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;




use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonChorusEchange;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'Chorus_echange' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonChorusEchange extends BaseCommonChorusEchange {

	public function getDecisionenveloppe(){
		return (new Atexo_Consultation_Decision())->retrieveDecisionById($this->getIdDecision(), $this->getOrganisme());
	}
	/*
	 * 
	 */
	public function getCodeOrganisationAchat()
    {
    	$organisationAchat = (new Atexo_Chorus_Echange())->retrieveOrganisationAchatById($this->getIdOa(), $this->getOrganisme());
    	if($organisationAchat)
        {
        	return $organisationAchat->getCode();
        }   
    }

    /*
     * permet d'avoir le nom de statut de l'echange 
     */
    public function getLibelleStatutEchange()
    {
		$libelle = null;
		switch ($this->getStatutechange()) {
			case Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_BROUILLON'):
				$libelle = Prado::localize('TEXT_BROUILLON') . " " . Prado::localize('ENREGISTRE_LE') . " :";
        		break;
			case Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_ENVOYER'):
				if($this->getTypeFluxAEnvoyer() == Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FIR')) {
					$libelle = Prado::localize('TIERS_FOURNISSEUR_COURS_VERIFICATION');
				} else {
					$libelle = Prado::localize('DEFINE_TEXT_DATE_ENVOI') . " :";
				}
        		break;
			case Atexo_Config::getParameter('STATUT_TIERS_CHORUS_EN_COURS_VERFICATION'):
				$libelle = Prado::localize('TIERS_FOURNISSEUR_COURS_VERIFICATION');
        		break;
			case Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_REJETE_PROBLEME_FOURNISSEUR'):
				$libelle = Prado::localize('REJETE_PROBLEME_TIERS_FOURNISSEUR');
				break;
			case Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_REJETE_PROBLEME_TECHNIQUE'):
				$libelle = Prado::localize('REJETE_PROBLEME_TECHNIQUE');
				break;
			case Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_FLUX_A_GENERER'):
				$libelle = Prado::localize('TEXT_ENVOI_PLANIFIE');
				break;
			case Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_ENVOI_PLANIFIE'):
				if($this->getTypeFluxAEnvoyer() == Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FIR')) {
					$libelle = Prado::localize('TIERS_FOURNISSEUR_COURS_VERIFICATION');
				} else {
					$libelle = Prado::localize('TEXT_ENVOI_PLANIFIE');
				}
				break;
			case Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_NON_ENVOYER'):
				$libelle = Prado::localize('DEFINE_NON_ENVOYE');
				break;
		}
		return $libelle;
	}

	/**
	 * Permet de recuperer le contrat d'un echange chorus
	 *
	 * @param $connexion
	 * @return CommonTContratTitulaire
	 * @author Oumar KONATE <oumar.konate@atexo.com>
	 * @version 1.0
	 * @since 2015-place
	 * @copyright Atexo 2015
	 */
	public function getContrat($connexion=null)
	{
		$contratQuery = null;
		$logger = Atexo_LoggerManager::getLogger('chorus');
		try {
			if(empty($connexion)) {
				$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
			}
			$contratQuery = new CommonTContratTitulaireQuery();

			$contrat = $contratQuery
                ->create()
                ->filterByIdContratTitulaire($this->getIdDecision())
                ->filterByOrganisme($this->getOrganisme())
                ->findOne($connexion)
            ;

			return $contrat;
		} catch (Exception $e) {
			$logger->error("Erreur recuperation contrat : ID Echange = " . $this->getId() . " \n\nRequete = " . $contratQuery->toString() . " \n\nErreur = " . $e->getMessage() . " \n\nTrace : " . $e->getTraceAsString() . " \n\nMethode = CommonChorusEchange::getNomPdfActeModificatif");
		}
	}

	/**
	 * Permet de verifier si le type de flux correspond a un FEN211
	 *
	 * @return bool : true si FEN211, false sinon
	 * @author Oumar KONATE <oumar.konate@atexo.com>
	 * @version 1.0
	 * @since 2015-roadmap
	 * @copyright Atexo 2015
	 */
	public function isTypeFluxFen211($contrat = null)
	{
		try {
			if ($this->getTypeFlux() != null && $this->getTypeFlux() == Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN211')) {
				return true;
			} else if($this->getTypeFlux() == null
                    && ($this->getContrat() instanceof CommonTContratTitulaire || !empty($contrat))
            ) {
                if (!empty($contrat) && is_array($contrat) && Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                    $type = $contrat['type'];
                    $typeContratQuery = new CommonTTypeContratQuery();
                    $typeContrat = $typeContratQuery->findOneByLibelleTypeContrat('TYPE_CONTRAT_' . $type['codeExterne']);

                    if ($typeContrat instanceof CommonTTypeContrat) {
                        return $typeContrat->getModeEchangeChorus() == Atexo_Config::getParameter('TYPE_CONTRAT_MODE_ECHANGE_CHORUS_INTEGRER');
                    }

                    return false;
                }

				return (new Atexo_Consultation_Contrat())->isRetourChorusModeIntegre($this->getContrat()->getIdContratTitulaire());
			}
			return false;
		} catch (\Exception $e) {
			(new Atexo_Chorus_Util())->loggerErreur("Erreur lors de la precision du type de flux : ". $e->getMessage());
			return false;
		}
	}

	/*
     * Permet de retourner si l'echange à un code pay de type passer en parametre
     */
	public function hasToutPaysEtranger()
	{
		try {
			$resultat = true;
			if ($this->getCodePaysTitulaire() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')) {
				$resultat = false;
			} elseif (($this->getIdTypeGroupement() != Atexo_Config::getParameter('ID_TYPE_GROUPEMEN_SANS_CO_TITULAIRE_CHORUS')) && $this->getNbrEntreprisesCotraitantes() != '0') {
				$listeCodesPays = explode('#', $this->getCodesPaysCoTitulaire());
				if (in_array(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'), $listeCodesPays)) {
					$resultat = false;
				}
			}
			return $resultat;
		} catch (\Exception $e) {
			(new Atexo_Chorus_Util())->loggerErreur("Erreur : ". $e->getMessage());
		}
	}

	/*
     * Permet de retourner si l'echange à un code pays de type passer en parametre
     */
	public function hasCodePaysByType($type)
	{
		try {
			$resultat = false;
			if ($this->getCodePaysTitulaire() == $type && $this->getNumeroSirenTitulaire() && $this->getNumeroSiretTitulaire()) {
				$resultat = true;
			} elseif (($this->getIdTypeGroupement() != Atexo_Config::getParameter('ID_TYPE_GROUPEMEN_SANS_CO_TITULAIRE_CHORUS')) && $this->getNbrEntreprisesCotraitantes() != '0') {
				$listeCodesPays = explode('#', $this->getCodesPaysCoTitulaire());
				if (in_array($type, $listeCodesPays)) {
					$arraySirenTitulaires = explode('#', $this->getNumeroSirenCoTitulaire());
					$arraySiretTitulaires = explode('#', $this->getNumeroSiretCoTitulaire());
					if (is_array($arraySirenTitulaires) && count($arraySirenTitulaires) && is_array($arraySiretTitulaires) && count($arraySiretTitulaires)) {
						$resultat = true;
					}
				}
			}
			return $resultat;
		} catch (\Exception $e) {
			(new Atexo_Chorus_Util())->loggerErreur("Erreur : ". $e->getMessage());
		}
	}

	/**
	 * Permet de determiner le libelle du type de flux en fonction de l'identifiant
	 *
	 * @return string
	 */
	public function getLibelleTypeFlux()
	{
		$libelle = null;
		$libelle = match ($this->getTypeFluxAEnvoyer()) {
            (int) Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FIR') => Atexo_Config::getParameter('TYPE_FLUX_FIR_CHORUS'),
            (int) Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN211') => Atexo_Config::getParameter('TYPE_FLUX_FEN_CHORUS_FEN211'),
            (int) Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN111') => Atexo_Config::getParameter('TYPE_FLUX_FEN_CHORUS_FEN111'),
			default => $libelle,
		};
		return $libelle;
	}

    public function preUpdate(PropelPDO $con = null)
    {
        $this->setDateModification(date('Y-m-d H:i:s'));
        return true;
    }
} // CommonChorusEchange
