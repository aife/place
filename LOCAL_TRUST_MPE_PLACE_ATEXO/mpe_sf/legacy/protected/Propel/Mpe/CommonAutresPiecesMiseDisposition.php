<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonAutresPiecesMiseDisposition;

/**
 * Skeleton subclass for representing a row from the 'Autres_Pieces_Mise_Disposition' table.
 *
 * 
 *
 * This class was autogenerated by Propel on:
 *
 * Sun Feb  3 14:36:36 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAutresPiecesMiseDisposition extends BaseCommonAutresPiecesMiseDisposition {
	private $_contentHorodatage = null;
	public function getHorodatage() {
		if(!$this->_contentHorodatage) {
			$this->_contentHorodatage = stream_get_contents( parent::getHorodatage());
		}
		return $this->_contentHorodatage;
	}

} // CommonAutresPiecesMiseDisposition
