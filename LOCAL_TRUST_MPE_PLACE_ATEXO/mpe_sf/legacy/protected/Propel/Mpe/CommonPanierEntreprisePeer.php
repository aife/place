<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\Entreprise;

use Application\Propel\Mpe\CommonPanierEntreprise;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonPanierEntreprisePeer;

// include base peer class


  // include object class


/**
 * Skeleton subclass for performing query and update operations on the 'Panier_Entreprise' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonPanierEntreprisePeer extends BaseCommonPanierEntreprisePeer {
    /**
				 * Selects a collection of CommonPanierEntreprise objects pre-filled with their Entreprise objects.
				 * @param      PropelPDO $con
				 * @param      String    $join_behavior the type of joins to use, defaults to Criteria::LEFT_JOIN
				 * @return array           Array of CommonPanierEntreprise objects.
				 * @throws PropelException Any exceptions caught during processing will be
				 *		 rethrown wrapped into a PropelException.
				 */
				public static function doSelectJoinCommonEntreprise(Criteria $criteria, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        return self::doSelectJoinEntreprise($criteria, $con, $join_behavior);
    }
} // CommonPanierEntreprisePeer
