<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonTMembreGroupementEntrepriseQuery;
use Application\Service\Atexo\Atexo_Config;


/**
 * Skeleton subclass for performing query and update operations on the 't_membre_groupement_entreprise' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTMembreGroupementEntrepriseQuery extends BaseCommonTMembreGroupementEntrepriseQuery
{

    /**
     * Permet de retourner le groupement by id conadidature mps
     *
     * @param $idCandidature
     * @param $connexion
     * @return CommonTGroupementEntreprise
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function getSousGroupementByIdParent($idParent,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        return (array) $this->filterByIdMembreParent($idParent)->find($connexion);
    }

    /**
     * Permet de retourner le groupement by id conadidature mps
     *
     * @param $idCandidature
     * @param $connexion
     * @return CommonTGroupementEntreprise
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function getNombreSousMemebreGroupement($idOffre,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $this->addJoin(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE,CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE);
        $this->add(CommonTGroupementEntreprisePeer::ID_OFFRE, $idOffre);
        $this->addAlias('tMG', CommonTMembreGroupementEntreprisePeer::TABLE_NAME);
        $this->addJoin(CommonTMembreGroupementEntreprisePeer::ID_MEMBRE_GROUPEMENT_ENTREPRISE, 'tMG.id_membre_parent', Criteria::INNER_JOIN);
        //$this->add(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, $idRole);
        return $this->count($connexion);
    }


    /**
     * Permet de retourner le groupement by id conadidature mps
     *
     * @param $idCandidature
     * @param $connexion
     * @return CommonTGroupementEntreprise
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function getMemebreGroupementSansMondataire($idOffre,$idRoleMondataire,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $this->addJoin(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE,CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE);
        $this->add(CommonTGroupementEntreprisePeer::ID_OFFRE, $idOffre);
        $this->add(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, $idRoleMondataire,Criteria::NOT_LIKE);
        return (array)$this->find($connexion);
    }
}
