<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Library\Propel\Connection\PropelPDO;
use Prado\Prado;
use \PDO;
use Application\Propel\Mpe\CommonEnchereValeursInitialesPeer;

use Application\Propel\Mpe\CommonEncherePmiPeer;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonEnchereReference;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;


/**
 * Skeleton subclass for representing a row from the 'EnchereReference' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEnchereReference extends BaseCommonEnchereReference {
	public $_valeursInitiales = array();
    public $_tmpId;
    public array $_tranchesBaremeReference = array();

    public function getCommonValeursInitiales()
    {
        return $this->_valeursInitiales;
    }
    public function setCommonValeursInitiales($valeursInitiales)
    {
        $this->_valeursInitiales=$valeursInitiales;
    }

    public function setValeurInitiale($emailEntreprise, $value)
    {
        $this->_valeursInitiales[$emailEntreprise] = $value;
    }
    public function getValeurInitiale($emailEntreprise)
    {
        return $this->_valeursInitiales[$emailEntreprise];
    }


    public function getValeurReferenceMinimal($idEntreprise, $org)
    {
         $con = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        $idEnchere = $this->getCommonEncherePmi($con)->getId();
        $idEnchereReference = $this->getId();

        $tmpTable = "CREATE TEMPORARY TABLE lastoffre_tmp (
                       `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
						idEnchereEntreprise int(11),
						organisme varchar( 30 ) ,
						maximum datetime,
                        PRIMARY KEY (`id`)
						);";
		//$lock = "LOCK TABLES EnchereOffre READ";
		$insert = "INSERT INTO lastoffre_tmp 
					SELECT id, idEnchereEntreprise, organisme, MAX(date) as `maximum` FROM EnchereOffre
					WHERE idEnchere = '".(new Atexo_Db())->quote($idEnchere)."'
					AND organisme='".$org."'
					AND idEnchereEntreprise <> ".(new Atexo_Db())->quote($idEntreprise)."
					GROUP BY idEnchereEntreprise;";
		$requete = "SELECT MIN(valeur)
					FROM lastoffre_tmp
					JOIN EnchereOffre on EnchereOffre.date = lastoffre_tmp.maximum AND EnchereOffre.organisme = lastoffre_tmp.organisme
					JOIN EnchereOffreReference on EnchereOffreReference.id_enchere_offre = EnchereOffre.id AND EnchereOffreReference.organisme = EnchereOffre.organisme
					WHERE EnchereOffreReference.idEnchereReference = '".(new Atexo_Db())->quote($idEnchereReference)."' AND EnchereOffreReference.organisme='".$org."' ";
		//$unlock = "UNLOCK TABLES";
        $delTmpTable = "DROP TABLE lastoffre_tmp";

        /*$stmt = $con->createStatement();
        $stmt->executeQuery($tmpTable, ResultSet::FETCHMODE_NUM);
//        $stmt->executeQuery($lock, ResultSet::FETCHMODE_NUM);
        $stmt->executeQuery($insert, ResultSet::FETCHMODE_NUM);

	    $rs = $stmt->executeQuery($requete, ResultSet::FETCHMODE_NUM);

//	    $stmt->executeQuery($unlock, ResultSet::FETCHMODE_NUM);
	    $stmt->executeQuery($delTmpTable, ResultSet::FETCHMODE_NUM);*/

	    $stmt = Atexo_Db::getLinkCommon()->prepare($tmpTable);
	    $stmt->execute();
	    //$stmt = Atexo_Db::getLinkCommon(true)->prepare($lock);
	    //$stmt->execute();
	    $stmt = Atexo_Db::getLinkCommon()->prepare($insert);
	    $stmt->execute();
	    $stmt = Atexo_Db::getLinkCommon()->prepare($requete);
	    $stmt->execute();
	    $rs=$stmt->fetchAll(PDO::FETCH_NUM);
	    //$stmt = Atexo_Db::getLinkCommon(true)->prepare($unlock);
	    //$stmt->execute();
	    $stmt = Atexo_Db::getLinkCommon()->prepare($delTmpTable);
	    $stmt->execute();

	    if ($rs) {
		    foreach($rs as $row) {
		        return $row[0];
		    }
	    } else {
	        return null;
	    }  

    }

    public function getLibelleBareme()
    {
        if ($this->getTypebaremereference() == Atexo_Config::getParameter('BAREME_REFERENCE_RELATIF')) {
            return Prado::localize('BAREME_REFERENCE_RELATIF_LIBELLE');
        } else if ($this->getTypebaremereference() == Atexo_Config::getParameter('BAREME_REFERENCE_TRANCHES')) {
            return Prado::localize('BAREME_REFERENCE_TRANCHES_LIBELLE');
        } else if ($this->getTypebaremereference() == Atexo_Config::getParameter('BAREME_REFERENCE_PRIX_REFERENCE')) {
            return Prado::localize('BAREME_REFERENCE_PRIX_REFERENCE_LIBELLE');
        } else {
            return "";
        }
    }

    public function setCommonTranchesBaremeReference($tranches)
    {
        $this->_tranchesBaremeReference = $tranches;
    }

    public function getCommonTranchesBaremeReference()
    {
        return $this->_tranchesBaremeReference;
    }

    public function getTmpId()
    {
        return $this->_tmpId;
    }

    public function setTmpId($tmpId)
    {
        $this->_tmpId = $tmpId;
    }

    /*public function getBestOffre($org)
    {
            $con = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        $idEnchereReference = $this->getId();

        $select = "SELECT MIN(EnchereOffreReference.valeur), EnchereOffre.idEnchereEntreprise 
                    FROM EnchereOffreReference 
        			JOIN EnchereOffre ON EnchereOffreReference.idEnchereOffre = EnchereOffre.id AND EnchereOffreReference.organisme = EnchereOffre.organisme
        			GROUP BY EnchereOffreReference.idEnchereReference
        			HAVING EnchereOffreReference.idEnchereReference = ".Atexo_Db::quote($idEnchereReference)." AND EnchereOffreReference.organisme='".$org."'";


        $stmt = $con->createStatement();
	    $rs = $stmt->executeQuery($select, ResultSet::FETCHMODE_NUM);

        if ($rs) {
		    foreach($rs as $row) {
		        return $row[0];
		    }
	    } else {
	        return null;
	    }  
    }*/

	public function getCommonEncheretranchebaremereferences($criteria = null, $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CommonEnchereTrancheBaremeReferencePeer::IDREFERENCE, $this->getId());
		$criteria->add(CommonEnchereTrancheBaremeReferencePeer::ORGANISME, $this->getOrganisme());

		return CommonEnchereTrancheBaremeReferencePeer::doSelect($criteria, $con);
	}

	public function getCommonEncherevaleursinitialess($criteria = null, $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CommonEnchereValeursInitialesPeer::IDENCHEREREFERENCE, $this->getId());
		$criteria->add(CommonEnchereValeursInitialesPeer::ORGANISME, $this->getOrganisme());

		return CommonEnchereValeursInitialesPeer::doSelect($criteria, $con);
	}

	public function getCommonEncherePmi(PropelPDO $con = null, $doQuery = true): ?CommonEncherePmi
	{
		if ($this->aCommonEncherePmi === null && ($this->idenchere !== null) && ($this->organisme !== null)) {
			$this->aCommonEncherePmi = CommonEncherePmiPeer::retrieveByPK($this->idenchere, $con);
		}
		return $this->aCommonEncherePmi;
	}
} // CommonEnchereReference
