<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Prado\Util\TLogger;
use \PDO;
use \Exception;
use AtexoCrypto\Dto\Signature;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonFichierEnveloppeQuery;

use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonBlocFichierEnveloppePeer;



use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonFichierEnveloppe;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'fichierEnveloppe' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */
class CommonFichierEnveloppe extends BaseCommonFichierEnveloppe {
	private $_fileCertificat;
	protected $_libelleTypeFichier;
	protected $chiffrement;
	protected $resultatValiditeSignature;

	public function setLibelleTypeFichier($value){
		$this->_libelleTypeFichier = $value;
	}

	public function getLibelleTypeFichier(){
		if($this->getTypeFichier() == Atexo_Config::getParameter('REPONSE_FICHIER_ACTE_ENGAGEMENT')){
			return "ACTE_ENGAGEMENT";
		}else{
			return "PRINCIPALE";
		}
	}

	/*
	 * Pour Registre de depot du WS Rest
	 */
	public function getChiffrement()
	{
		return $this->chiffrement;
	}

	public function setChiffrement($chiffrement)
	{
		$this->chiffrement=$chiffrement;
	}

	private function putCertificatInFile($typeSignature=null)
	{
		$signature = null;
		if($typeSignature=="XML") {
			if(trim($this->getSignatureFichier())!="" && $this->getSignatureFichier()!=null) {
				$signature = base64_decode(trim($this->getSignatureFichier()));
			}
		} else {
			if(trim($this->getSignatureFichier())!="" && $this->getSignatureFichier()!=null) {
				$signature="-----BEGIN PKCS7-----\n".trim($this->getSignatureFichier())."\n-----END PKCS7-----";
			}
		}
		if($signature!="" && $signature!=null) {
			$this->_fileCertificat=(new Atexo_Crypto())->getCertificatFromSignature($signature, false, true, $typeSignature);
		}
	}

	private function unlinkCertificatFile()
	{
		if(is_file($this->_fileCertificat)) {
			unlink($this->_fileCertificat);
		}
	}

	public function getEmailSubject($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			if($typeSignature=="XML") {
				$this->putCertificatInFile($typeSignature);
			} else {
				$this->putCertificatInFile();
			}
			$emailCeritifcat=(new Atexo_Crypto_Certificat())->getEmailCertificatFromFile($this->_fileCertificat);
			$this->unlinkCertificatFile();
			return $emailCeritifcat;
		}
		return "";
	}

	public function getCnSubject($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			if($typeSignature=="XML") {
				$this->putCertificatInFile($typeSignature);
			} else {
				$this->putCertificatInFile();
			}
			$cnCeritifcat=(new Atexo_Crypto_Certificat())->getCnCertificatFromFile($this->_fileCertificat);
			$this->unlinkCertificatFile();
			return $cnCeritifcat;
		}
		return "";
	}

	public function getOuSubject($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			if($typeSignature=="XML") {
				$this->putCertificatInFile($typeSignature);
			} else {
				$this->putCertificatInFile();
			}
			$ouCeritifcat=(new Atexo_Crypto_Certificat())->getOuCertificatFromFile($this->_fileCertificat);
			$this->unlinkCertificatFile();
			return $ouCeritifcat;
		}
		return "";
	}

	public function getOSubject($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			if($typeSignature=="XML") {
				$this->putCertificatInFile($typeSignature);
			} else {
				$this->putCertificatInFile();
			}
			$oCeritifcat=(new Atexo_Crypto_Certificat())->getOCertificatFromFile($this->_fileCertificat);
			$this->unlinkCertificatFile();
			return $oCeritifcat;
		}
		return "";
	}

	public function getCObject($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			if($typeSignature=="XML") {
				$this->putCertificatInFile($typeSignature);
			} else {
				$this->putCertificatInFile();
			}
			$cCeritifcat=(new Atexo_Crypto_Certificat())->getCCertificatFromFile($this->_fileCertificat);
			$this->unlinkCertificatFile();
			return $cCeritifcat;
		}
		return "";
	}

	public function getStatutCertificatPeriodeValidite($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			$chaine=explode('-',$this->getVerificationCertificat());
			if($chaine[2]=="0") {
				return Atexo_Config::getParameter('OK');
			} else if($chaine[2]=="2") {
				return Atexo_Config::getParameter('NOK');
			} else {
				return Atexo_Config::getParameter('UNKNOWN');
			}
		}
		return Atexo_Config::getParameter('SANS');
	}

	public function getDateValidTo($isFichierSigne=false, $typeSignature=null)
    {
        if($isFichierSigne) {
			if($typeSignature=="XML") {
				$this->putCertificatInFile($typeSignature);
			} else {
				$this->putCertificatInFile();
			}
			$date=(new Atexo_Crypto_Certificat())->getDateCertficateValidTo($this->_fileCertificat);
			$this->unlinkCertificatFile();
			return $date;
		}
		return "";

    }

	public function getDateValidFrom($isFichierSigne=false, $typeSignature=null)
    {
    	if($isFichierSigne) {
			if($typeSignature=="XML") {
				$this->putCertificatInFile($typeSignature);
			} else {
				$this->putCertificatInFile();
			}
			$date=(new Atexo_Crypto_Certificat())->getDateCertficateValidFrom($this->_fileCertificat);
			$this->unlinkCertificatFile();
			return $date;
		}
		return "";
    }

	public function getStatutValiditeChaineCertification($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			$chaine=explode('-',$this->getVerificationCertificat());
			$statutRefCertif = $this->getStatutReferentielCertificat();
			if(!Atexo_Module::isEnabled('SurchargeReferentiels') || ($statutRefCertif===null)){
				if($chaine[0]=="0") {
					return Atexo_Config::getParameter('OK');
				}else if($chaine[0]=="2") {
					return Atexo_Config::getParameter('NOK');
				}else {
					return Atexo_Config::getParameter('UNKNOWN');
				}
			}else{
				return self::getStatutValiditeReferentielCertificat($isFichierSigne);
			}
		}
		return Atexo_Config::getParameter('SANS');
	}

	public function getStatutValiditeCrlCertificat($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			$chaine=explode('-',$this->getVerificationCertificat());
			if($chaine[1]=="0") {
				return Atexo_Config::getParameter('OK');
			} else if($chaine[1]=="2") {
				return Atexo_Config::getParameter('NOK');
			} else {
				return Atexo_Config::getParameter('UNKNOWN');
			}
		}

		return Atexo_Config::getParameter('SANS');
	}

	public function getSignatureValidite($isFichierSigne, $crypte , $calledFrom = false)
	{
		try{

			if(!Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') && $crypte && $calledFrom != 'entreprise' ) {
				return Atexo_Config::getParameter('UNKNOWN');
			}

			if($isFichierSigne) {
				if((trim($this->getSignatureFichier())!="" && $this->getSignatureFichier()!=null) || !empty($this->getIdFichierSignature())) {
					$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
					$enveloppe=$this->getCommonEnveloppe($connexionCom);
					$validite = (new Atexo_Crypto())->verifySignatureValidity($this->getIdBlob(), $enveloppe->getOffreId(), $this, $calledFrom);
				}else {
					$validite = -1;
				}
			} else {
				$validite = !((new Atexo_Crypto())->verifyHashValidity($this->getIdBlob(), $this->getHash()));
			}
			$statut =  Atexo_Config::getParameter('NOK');
			$statut = match ($validite) {
				false => Atexo_Config::getParameter('OK'),
				0 => Atexo_Config::getParameter('OK'),
				-2 => Atexo_Config::getParameter('UNVERIFIED'),
				default => $statut,
			};
			return $statut;
		}catch (\Exception $e){
			Prado::log(" Erreur lors de la verification de la validite de la signature " . $e->getMessage() . $e->getTraceAsString() , TLogger::ERROR, "CommonFichierEnveloppe.php");
		}

	}

	public function getCnEmmeteur($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			if($typeSignature=="XML") {
				$this->putCertificatInFile($typeSignature);
			} else {
				$this->putCertificatInFile();
			}
			$cnCeritifcat=(new Atexo_Crypto_Certificat())->getCnEmetteurCertificatFromFile($this->_fileCertificat);
			$this->unlinkCertificatFile();
			return $cnCeritifcat;
		}
		return "";
	}

	public function getOuEmetteur($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			if($typeSignature=="XML") {
				$this->putCertificatInFile($typeSignature);
			} else {
				$this->putCertificatInFile();
			}
			$ouCeritifcat=(new Atexo_Crypto_Certificat())->getOuEmetteurCertificatFromFile($this->_fileCertificat);
			$this->unlinkCertificatFile();
			return $ouCeritifcat;
		}
		return "";
	}

	public function getOEmetteur($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			if($typeSignature=="XML") {
				$this->putCertificatInFile($typeSignature);
			} else {
				$this->putCertificatInFile();
			}
			$oCeritifcat=(new Atexo_Crypto_Certificat())->getOEmetteurCertificatFromFile($this->_fileCertificat);
			$this->unlinkCertificatFile();
			return $oCeritifcat;
		}
		return "";
	}

	public function getCEmetteur($isFichierSigne=false, $typeSignature=null)
	{
		if($isFichierSigne) {
			if($typeSignature=="XML") {
				$this->putCertificatInFile($typeSignature);
			} else {
				$this->putCertificatInFile();
			}
			$oCeritifcat=(new Atexo_Crypto_Certificat())->getCEmetteurCertificatFromFile($this->_fileCertificat);
			$this->unlinkCertificatFile();
			return $oCeritifcat;
		}
		return "";
	}

	public function getNomFichier() {
		return Atexo_Util::utf8ToIso($this->nom_fichier);
	}

	public function getBlocfichierenveloppes($criteria, $connexion)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}
		$criteria->add(CommonBlocFichierEnveloppePeer::ORGANISME, $this->getOrganisme());
		return $this->getCommonBlocFichierEnveloppes($criteria, $connexion);
	}

	public function getCommonEnveloppe($connexion){
		return CommonEnveloppePeer::retrieveByPK( $this->getIdEnveloppe(), $this->getOrganisme(), $connexion);
	}

	public function getCommonBlocFichierEnveloppes($criteria = null, $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CommonBlocFichierEnveloppePeer::ID_FICHIER, $this->getIdFichier());
		$criteria->add(CommonBlocFichierEnveloppePeer::ORGANISME, $this->getOrganisme());

		return CommonBlocFichierEnveloppePeer::doSelect($criteria, $con);
	}

	/**
	 * Cette méthode permet de récupérer le type de signature associé à l'objet 
	 * type XML dans ce cas signature XADES, sinon type P7S dans ce cas signature PKCS7
	 */
	public function getTypeSignature () {
		$typeSignature = "P7S";

		$signature= base64_decode($this->signature_fichier);

		$pos = strpos($signature, "ds:Signature");
		if($pos>0) {
			$typeSignature = "XML";
		}

		return $typeSignature;

	}

	public function getCommonConsultation($connexionCom=null){
		if(!$connexionCom){
        	$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $enveloppe = $this->getCommonEnveloppe($connexionCom);
        if($enveloppe instanceof CommonEnveloppe){
        	$offre = $enveloppe->getOffre($connexionCom);
        	if($offre instanceof CommonOffres){
        		return  $offre->getCommonConsultation($connexionCom);
        	}
        }
        return false;
	}
	 /*
	  * Permet de renvoyer le statut de signature des enveloppes d'un fichier enveloppe
	  * Remarque le statut 'NOK' est plus prioritaire que le statut 'UNKNOWN'
	  */
	public function getResultatValiditeSignature($connexionCom = null, $calledFrom = false)
	{
		try{
			if(empty($this->resultatValiditeSignature)) {
				$resultat = self::getResultatValiditeSignatureWithMessage($connexionCom, null, $calledFrom);
				$this->resultatValiditeSignature = $resultat['statut'];
			}
			return $this->resultatValiditeSignature;
		}catch (\Exception $e){
			Prado::log(" Erreur lors de la verification de la validite de la signature " . $e->getMessage() . $e->getTraceAsString() , TLogger::ERROR, "CommonFichierEnveloppe.php");
		}
	}

	/**
	 * permet de recuperer les infos de la validite de la signature
	 *
	 * @param \PDO         $connexionCom
	 * @param Boolean     $forExport
	 * @param string $calledFrom
	 * @return array
	 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
	 * @version 1.0
	 * @since 4.9.0
	 * @copyright Atexo 2015
	 */

    public function getResultatValiditeSignatureWithMessage($connexionCom = null,$forExport = null, $calledFrom = "")
 	{
		$cryptage = null;
		try{
			if($forExport){
				$li = "";
				$finLi = "";
			}else {
				$li = "<li>";
				$finLi = "</li>";
			}
			if(!$connexionCom){
				$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
			}

			$resultat = array();
			$message = "";
			$consultation = $this->getCommonConsultation($connexionCom);
			if($consultation instanceof CommonConsultation) {
				if(!empty($this->getSignatureFichier()) || !empty($this->getIdFichierSignature())) {
					$consultationSigne =($consultation->getSignatureOffre()=="1");
					$enveloppe = $this->getCommonEnveloppe($connexionCom);
					if($calledFrom != 'entreprise'){
						$cryptage =($enveloppe->getCryptage()=="1");
					}
					$resUnkown = false;
					$resUnverified = false;
					$resNok = false;
					$arrayValidite = explode('-', $this->getVerificationCertificat());

					$referentielCertif = $this->getStatutReferentielCertificat();

					if(!Atexo_Module::isEnabled('SurchargeReferentiels') || ($referentielCertif===null)){
						if($arrayValidite[0] == 2){
							$message .=  $li . Prado::localize('DEFINE_REFERENTIEL_CERTIFICAT_NOK') . $finLi;
							$resNok = true;
						}else if($arrayValidite[0] == 1){
							$message .=  $li . Prado::localize('DEFINE_REFERENTIEL_CERTIFICAT_UNKNOWN') . $finLi;
							$resUnkown = true;
						}
					}else{
						if($referentielCertif == 2){
							$message .=  $li . Prado::localize('DEFINE_REFERENTIEL_CERTIFICAT_NOK') . $finLi;
							$resNok = true;
						}else if($referentielCertif == 1){
							$message .=  $li . Prado::localize('DEFINE_REFERENTIEL_CERTIFICAT_UNKNOWN') . $finLi;
							$resUnkown = true;
						}
					}

					if($arrayValidite[1] !=='0') { //Non révocation non valide
						if($arrayValidite[1]=='2') {
							$message .=  "<li>" . Prado::localize('DEFINE_NON_REVOCATION_NOK') . "</li>";
							$resNok = true;
						} else{
							$message .=  "<li>" . Prado::localize('DEFINE_LISTE_NON_REVOCATION_AUTORITE') . "</li>";
							$resUnkown = true;
						}
					}
					if($arrayValidite[2] !=='0') { // Période de validité  non valide
						if($arrayValidite[2]=='2') {
							$message .=  "<li>" . Prado::localize('DEFINE_PERIODE_VALIDITE_NOK') . "</li>";
							$resNok = true;
						}
					}

					//Non répudiation
					$validiteSignature = $this->getSignatureValidite($consultationSigne, $cryptage,$calledFrom);
					if($validiteSignature == Atexo_Config::getParameter('NOK')){
						$resNok = true;
						$message .=  $li . Prado::localize('DEFINE_NON_REPUDIATION_NOK') . $finLi;
					}elseif ($validiteSignature == Atexo_Config::getParameter('UNKNOWN')){
						$message .=  $li . Prado::localize('DEFINE_NON_REPUDIATION_UNKNOWN') . $finLi;
						$resUnkown = true;
					}elseif ($validiteSignature == Atexo_Config::getParameter('UNVERIFIED')){
						$message =  $li . Prado::localize('DEFINE_MESSAGE_VALIDITE_JETON_SIGNATURE_NON_VERIFIABLE') . $finLi;
						$resUnverified = true;
					}
					if ($resUnverified) {
						$resultat['statut'] = Atexo_Config::getParameter('UNVERIFIED');
					}else if($resNok) {
						$resultat['statut'] = Atexo_Config::getParameter('NOK');
					} else if ($resUnkown) {
						$resultat['statut'] = Atexo_Config::getParameter('UNKNOWN');
					}  else {
						$resultat['statut'] = Atexo_Config::getParameter('OK');
					}
				}else{
					$message = $li . Prado::localize('DEFINE_AUCUNE_SIGNATURE_ENVOYE') . $finLi;
					$resultat['statut'] = Atexo_Config::getParameter('SANS');
				}
				$resultat['message'] = $message;

				return $resultat;
			}
		}catch (\Exception $e){
			Prado::log(" Erreur lors de la verification de la validite de la signature " . $e->getMessage() . $e->getTraceAsString() , TLogger::ERROR, "CommonFichierEnveloppe.php");
		}

 	}

 	public function getCommonOffres(){
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		$enveloppe = $this->getCommonEnveloppe($connexionCom);
        if($enveloppe instanceof CommonEnveloppe){
        	$offre = $enveloppe->getOffre();
        	if($offre instanceof CommonOffres){
        		return  $offre;
        	}
        }
        return false;
 	}

 	/*
 	 * Permet d'avoir le statut de validite de referentiel pour le certificat d'un fichierEnveloppe
 	 */
	public function getStatutValiditeReferentielCertificat($isFichierSigne=false)
	{
		if($isFichierSigne) {
			$chaine = $this->getStatutReferentielCertificat();
			if($chaine===0) {
				return Atexo_Config::getParameter('OK');
			} else if($chaine == 2) {
				return Atexo_Config::getParameter('NOK');
			} else {
				return Atexo_Config::getParameter('UNKNOWN');
			}
		}
		return Atexo_Config::getParameter('SANS');
	}

    /**
     * @return mixed|string
     */
    public function getNameReferentielCertificat()
    {
        $nomReferentiel = Prado::localize('DEFINE_NON_REFERENCE');
        if ($this->getNomReferentielFonctionnel()) {
            $nomReferentiel = $this->getNomReferentielFonctionnel();
            $nomReferentiel .= " ";
            $nomReferentiel .= $this->getMessage();
        }

        return $nomReferentiel;
    }


	/*
	 * Permet de retourne le nom du jeton
	 * param $extension = boolean true retourne le nom avec extension sinn sans extension,
	 * $fileName = le nom de fichier (si null c'est le nom du fichier)
	 * retunr nom du jeton
	 */
 	public function getJetonFileName($extension = true,$fileName = null){
		$date = null;
		if(!$fileName){
			$fileName = $this->getNomFichier();
		}
	 	if($this->getDateSignature()){
		 	$date = $this->getDateSignature();
	 	}else{
	 	 	$offre = $this->getCommonOffres();
	 	 	if($offre instanceof CommonOffres){
	 	 		$date =Atexo_Util::isoDateTimeToString($offre->getUntrusteddate());
	 	 	}
	 	 }
	 	 if($extension){
	 		 $extension = strtolower($this->getTypeSignature());
		 	 return $fileName . " - " . $date . " - Signature 1." . $extension;
	 	 }else{
	 	 	 return $fileName . " - " . $date . " - Signature 1";
	 	 }
	}

	public function getNomFichierSignature($extension = true,$fileName = null){
		$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		if($this->getIdFichierSignature() == $this->getIdFichier()){
			$fichierSign = $this;
		}else {
			$fichierQuery = new CommonFichierEnveloppeQuery();
			$fichierSign = $fichierQuery->filterByIdEnveloppe($this->getIdEnveloppe())
				->filterByOrganisme($this->getOrganisme())
				->filterByIdFichier($this->getIdFichierSignature())
				->filterByTypeFichier("SIG")
				->findOne($connexionCom);
		}
		if($fichierSign instanceof CommonFichierEnveloppe){
			$name = $fichierSign->getNomFichier();
		}else{
			$name = $this->getJetonFileName($extension,$fileName);
		}
		return $name;
	}


    public function getSignatureFichier()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') .
            Atexo_Config::getParameter('CONST_READ_ONLY')
        );
        $content = null;
        if ($this->getIdFichierSignature() > 0
            && $this->getIdFichierSignature() != $this->getIdFichier() )
        {
            $fichierQuery = new CommonFichierEnveloppeQuery();
            $fichier = $fichierQuery->findOneBy('idFichier', $this->getIdFichierSignature(), $connexionCom);
            if ($fichier) {
                $atexoBlob = new Atexo_Blob();
                $content =  base64_encode($atexoBlob->return_blob_to_client($fichier->getIdBlob(), $this->getOrganisme()));
            } else {
                throw new \Exception("Le fichier n'existe pas !");
            }
        } else {
            $content = $this->signature_fichier;
        }
        return $content;
    }

} // CommonFichierEnveloppe
