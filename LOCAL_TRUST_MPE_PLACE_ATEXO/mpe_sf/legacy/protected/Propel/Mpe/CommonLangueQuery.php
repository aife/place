<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonLanguePeer;



use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonLangueQuery;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

/**
 * Skeleton subclass for performing query and update operations on the 'Langue' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.7.0-dev on:
 *
 * Mon Jul 15 17:53:39 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonLangueQuery extends BaseCommonLangueQuery
{
    public function findActiveLanguages() {
        $cachedActiveLanguages = Prado::getApplication()->Cache->get('cachedActiveLanguages');

        if (!$cachedActiveLanguages) {
            $con = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

            $c=new Criteria();
            $c->add(CommonLanguePeer::ACTIVE, '1');

            $cachedActiveLanguages = CommonLanguePeer::doSelect($c, $con);
            Prado::getApplication()->Cache->set('cachedActiveLanguages', $cachedActiveLanguages,Atexo_Config::getParameter('TTL_PRADO_CACHE'));
        }

        return $cachedActiveLanguages;
    }
}
