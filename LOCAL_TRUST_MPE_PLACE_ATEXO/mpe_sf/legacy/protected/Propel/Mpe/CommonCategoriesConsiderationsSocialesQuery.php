<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonCategoriesConsiderationsSocialesQuery;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for performing query and update operations on the 'categories_considerations_sociales' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonCategoriesConsiderationsSocialesQuery extends BaseCommonCategoriesConsiderationsSocialesQuery
{

    /**
     * Permet de recuperer la liste des CategoriesConsiderationsSociales par des ids
     *
     * @param array ids
     * @return liste of object CategoriesConsiderationsSociales
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2018-esr
     * @copyright Atexo 2018
     */
    public function getCategorieConsiderationsSocialesByIds($ids,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        return (array)$this->filterById($ids)->find($connexion);

    }
}
