<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonService;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;

/**
 * Skeleton subclass for representing a row from the 'Service' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonService extends BaseCommonService {


	private int $_depth = 0;
	protected ?array $_collAffiliationServiceRelatedByIdService = null;


	public function addCommonAffiliationServiceRelatedByIdService(CommonAffiliationService $affeliationService){
		$this->_collAffiliationServiceRelatedByIdService[] = $affeliationService;
	}

	public function getDepth() 
	{
		return $this->_depth;
	}

	public function setDepth($value)
	{
		$this->_depth = $value;
	}

	public function toString()
	{
		return "Id=".$this->getId()." ; Libellé=".$this->getLibelle()." ; Profondeur=".$this->getDepth();
	}
	/*
	 * retourne un libelle ou libelle traduit selon la langue
	 */
	public function getLibelleTraduit()
	{
		$langue = Atexo_CurrentUser::readFromSession("lang");
 	    $libelle= "libelle_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$libelle){
            return $this->libelle;
        } else {
            return $this->$libelle;
        }
	}
	// Service
	/*
	 * retourne la path service traduit selon la langue
	 */
	public function getPathServiceTraduit()
	{
		$langue = Atexo_CurrentUser::readFromSession("lang");
 	    $path_service= "chemin_complet_".$langue;

 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$path_service){
            return $this->chemin_complet;
        } else { //echo $this->$path_service; exit;
            return $this->$path_service;
        }
	}

    /**
     * @param $idEntite
     */
	public function setIdEntite($idEntite){
        if(empty($idEntite) || is_null($idEntite)){
           $idEntite = null;
        }

        parent::setIdEntite($idEntite);
    }

	public function saveService($connexion){

		$afectedRows = parent::save($connexion);

		if($this->_collAffiliationServiceRelatedByIdService){
			foreach ($this->_collAffiliationServiceRelatedByIdService as $affiliationService){
				$affiliationService->setServiceId($this->getId());
				$affiliationService->setOrganisme($this->getOrganisme());
				$affiliationService->save($connexion);
				$afectedRows++;
			}
		}
		return $afectedRows;
	}

	public function getAdresseTraduit()
	{
		$langue = Atexo_CurrentUser::readFromSession("lang");
 	    $adresse= "adresse_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$adresse){
            return $this->adresse;
        } else {
            return $this->$adresse;
        }
	}

	public function getVilleTraduit()
	{
		$langue = Atexo_CurrentUser::readFromSession("lang");
 	    $ville= "ville_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$ville){
            return $this->ville;
        } else {
            return $this->$ville;
        }
	}

	public function getPaysTraduit()
	{
		$langue = Atexo_CurrentUser::readFromSession("lang");
 	    $pays= "pays_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$pays){
            return $this->pays;
        } else {
            return $this->$pays;
        }
	}

	public function getAlertes($open = NULL){
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		$c = new Criteria();
		$c->add(CommonAlerteMetierPeer::SERVICE_ID,$this->getId());
		if($open !== NULL){
			$c->add(CommonAlerteMetierPeer::CLOTUREE,2);
		}
		$alertes = CommonAlerteMetierPeer::doSelect($c, $connexion);
		return $alertes;
	}

	public function getProgrammePrevisonnel(){
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$c = new Criteria();
		$c->add(CommonProgrammePrevisionnelPeer::ORGANISME,$this->getOrganisme());
		$c->add(CommonProgrammePrevisionnelPeer::ANNEE,date('Y'));
		$programmes = CommonProgrammePrevisionnelPeer::doSelect($c, $connexion);
		return $programmes;
	}

} // CommonService
