<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use \PDO;
use \Exception;




use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonTPreferenceSupportPublicationQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;

/**
 * Skeleton subclass for performing query and update operations on the 't_preference_support_publication' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.7.0-dev on:
 *
 * Mon Oct 26 11:13:22 2015
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTPreferenceSupportPublicationQuery extends BaseCommonTPreferenceSupportPublicationQuery
{
    /**
     * Permet de recuperer les preferences des supports de publication de l'agent
     *
     * @param string $idAgent : identifiant de l'agent
     * @param string $organisme : organisme de l'agent
     * @param \PDO $connexion
     * @return array|mixed|PropelObjectCollection : liste des preferences des supports de l'agent
     * @author OKO <oumar.konate@atexo.com>
     * @version 1.0
     * @since esr-2015
     * @copyright Atexo 2015
     * @package Publicite
     */
    public function getPreferencesSupportsAgent($idAgent, $organisme, $connexion=null)
    {
        $query = null;
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }
            $query = $this->filterByIdAgent($idAgent)
                             ->filterByOrganisme($organisme)
                                ->filterByActive('1');
            return $query->find($connexion);
            return array();
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors de la recuperation des preferences des supports de publication d'un agent : Id_Agent = " . $idAgent . " , Organisme = $organisme , Requete = " . $query->toString() . " , Erreur = " . $e->getMessage() . "\n Methode = CommonTPreferenceSupportPublicationQuery::getPreferencesSupportsAgent");
        }
    }
}
