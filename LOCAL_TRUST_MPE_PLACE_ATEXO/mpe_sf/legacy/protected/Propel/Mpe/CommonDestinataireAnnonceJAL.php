<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonDestinataireAnnonceJAL;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Jal;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;


/**
 * Skeleton subclass for representing a row from the 'DestinataireAnnonceJAL' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonDestinataireAnnonceJAL extends BaseCommonDestinataireAnnonceJAL {

	public function retreiveDateEnvoi()
 	{
 		if($this->getDateEnvoi()!='') {
 			return Atexo_Util::iso2frnDate($this->getDateEnvoi());
 		}
 		else {
 		return Prado::localize('ACCUSE_RECEPTION_NA');
 		}
 	}

     public function retreiveDatePublication()
 	{
 		if($this->getDatepub()!='') {
 			return Atexo_Util::iso2frnDate($this->getDatepub());
 		}
 		else {
 		return Prado::localize('ACCUSE_RECEPTION_NA');
 		}
 	}

	public function retreiveDateAr()
 	{
 		if($this->getDateAr()!='') {
 			return Atexo_Util::iso2frnDateTime($this->getDateAr());
 		}
 		else {
 		return Prado::localize('ACCUSE_RECEPTION_NA');
 		}
 	}

 	public function getAnnonceJAL(CommonDestinataireAnnonceJAL $objetAnnonceDestinataireJal, $connexion = null) 
 	{
 	    if(!$connexion) {
 	        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

 	    }
        return CommonAnnonceJALPeer::retrieveByPK($objetAnnonceDestinataireJal->getIdannoncejal(), $objetAnnonceDestinataireJal->getOrganisme(), $connexion);
 	}

 	/**
 	 * Surcharge de la methode: BaseCommonDestinataireAnnonceJAL::getCommonJAL()
 	 * Permet de recuperer un JAL
 	 * @see BaseCommonDestinataireAnnonceJAL::getCommonJAL()
 	 * 
 	 * @author Oumar KONATE <oumar.konate@atexo.com>
     * @version 1.0
     * @since 4.8.0
     * @copyright Atexo 2014
 	 */
    public function getCommonJAL(PropelPDO $con = null, $doQuery = true)
 	{
 	    $c = new Criteria();
	    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
	    $jal = Atexo_Jal::retrieveJalById($this->getIdjal(), $this->getOrganisme());
	    return $jal;
 	}

} // CommonDestinataireAnnonceJAL
