<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonItemFormulaireConsultationPeer;




use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonConsultationFormulaire;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'ConsultationFormulaire' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonConsultationFormulaire extends BaseCommonConsultationFormulaire {
    private $_totalBdHt;

	public function getCountItems()
	{
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		$c = new Criteria();
        $c->add(CommonItemFormulaireConsultationPeer::IDFORMULAIRECONSULTATION, $this->getId(), Criteria::EQUAL);
        return CommonItemFormulaireConsultationPeer::doCount($c, false, $connexionCom);
	}



 	public function getLibelleTypeFormulaire() {
 		$typeForm = $this->getTypeFormulaire();
 		if($typeForm == Atexo_Config::getParameter('QUESTIONNAIRE')) {
 			return Prado::localize('TEXT_QUESTIONNAIRE');
 		}
 		if($typeForm == Atexo_Config::getParameter('SAISIE_UNITAIRE_DE_FICHIERS')) {
 			return Prado::localize('TEXT_SAISIE_UNITAIRE');
 		}
 		if($typeForm == Atexo_Config::getParameter('BORDEREAU_DE_PRIX')) {
 			return Prado::localize('TEXT_BORDEREAU_PRIX');
 		}
 		if($typeForm == Atexo_Config::getParameter('ACTE_D_ENGAGEMENT_DISSOCIE')) {
 			return Prado::localize('TEXT_AE_SOUS_FORME_DISSOCIEE');
 		}
 	}

 	public function getLibelleTypeEnveloppe() {
 		$typeEnveloppe = $this->getTypeEnveloppe();
 		if($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
 			return Prado::localize('CANDIDATURE');
 		}
 		if($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
 			return Prado::localize('OFFRE');
 		}
 		if($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
 			return Prado::localize('ANONYMAT_3_ENVELOPPE');
 		}
 		if($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
 			return Prado::localize('OFFRE_TECHNIQUE');
 		}
 	}

 	public function setTotalBdHt($value) {
 		$this->_totalBdHt = $value;
 	}

 	public function getTotalBdHt() {
 		return $this->_totalBdHt;
 	}
} // CommonConsultationFormulaire
