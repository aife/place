<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonValeurReferentielOrg;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Traduction;

/**
 * Skeleton subclass for representing a row from the 'ValeurReferentielOrg' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonValeurReferentielOrg extends BaseCommonValeurReferentielOrg {
	public function getLibelleValeurReferentiel()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $libelle_valeur_referentiel = "libelle_valeur_referentiel_".$langue;
 	    if(!$this->$libelle_valeur_referentiel){
            return $this->libelle_valeur_referentiel;
        } else {
            return $this->$libelle_valeur_referentiel;
        }
	}
	/**
     * 
     * Permet de traduire un libellé
     * @param string $idLibelle
     * @param string $libelle: le libellé à traduire
     * @param string $langue
     */
    public function getTraduction($libelle, $langue = null) {
    	$codeLibelle = 'code_'.$libelle;
    	$libelleTraduit = (new Atexo_Traduction())->traduire($this->$codeLibelle, $langue);	
    	if($libelleTraduit) {
    		return $libelleTraduit;
    	} else {
    		return $this->$libelle;	
    	}	
    }
} // CommonValeurReferentielOrg
