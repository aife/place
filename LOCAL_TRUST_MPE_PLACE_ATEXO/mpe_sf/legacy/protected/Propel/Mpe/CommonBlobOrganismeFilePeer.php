<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonBlobOrganismeFile;

  // include base peer class

  // include object class
  use Application\Library\Propel\Connection\PropelPDO;
  use Application\Library\Propel\Exception\PropelException;
  use Application\Library\Propel\Propel;
  use Application\Library\Propel\Query\Criteria;
  use Application\Propel\Mpe\Om\BaseCommonBlobOrganismeFilePeer;
  use Application\Service\Atexo\Atexo_Config;
  use Application\Service\Atexo\Config\Atexo_Config_Exception;


  /**
 * Skeleton subclass for performing query and update operations on the 'blobOrganisme_file' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonBlobOrganismeFilePeer extends BaseCommonBlobOrganismeFilePeer {
    /**
     * @param int $pk
     * @param PropelPDO|null $con
     * @param null $organisme
     * @return \Application\Propel\Mpe\CommonBlobOrganismeFile|null
     */
    public static function retrieveByPK($pk, PropelPDO $con = null, $organisme = null)
    {
        return  parent::retrieveByPK($pk, $con);
    }
} // CommonBlobOrganismeFilePeer
