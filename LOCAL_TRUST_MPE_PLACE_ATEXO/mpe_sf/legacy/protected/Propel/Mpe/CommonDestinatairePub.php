<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonAVIS;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonDestinatairePub;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;


/**
 * Skeleton subclass for representing a row from the 'Destinataire_Pub' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonDestinatairePub extends BaseCommonDestinatairePub {

    protected $avisPub;
    protected $avis;

    public function getAvisPub(){
        if(!$this->avisPub instanceof CommonAvisPub){
            $this->avisPub = (new Atexo_Publicite_AvisPub())->retreiveAvis($this->getIdAvis(), $this->getOrganisme());
        }
        return $this->avisPub;
    }

    public  function getIdAvisOpoce(){
        $avisPub = $this->getAvisPub();
        if($avisPub instanceof CommonAvisPub){
            return $avisPub->getIdAvisPdfOpoce();
        }

    }

    public  function getNomFichierAvis($sufix = ""){
        $avisPub = $this->getAvisPub();
        if($avisPub instanceof CommonAvisPub){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $avis = (new Atexo_Publicite_Avis())->retreiveAvisById($avisPub->getIdAvisPdfOpoce(), $connexion, $avisPub->getOrganisme());
            if($avis instanceof CommonAVIS) {
                return $avis->getNomFichier() . $sufix;
            }
        }

    }
} // CommonDestinatairePub
