<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\Om\BaseCommonMarche;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_TrancheBudgetaire;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'Marche' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonMarche extends BaseCommonMarche {
      public function getPmePmiLabelExcel()
    {
        if (strcmp($this->getPmepmi(), '0') == 0) {
            return Prado::localize('DEFINE_NON');
        } elseif (strcmp($this->getPmepmi(), '1') == 0) {
            return Prado::localize('DEFINE_OUI');
        } else {
            return Prado::localize('NON_RENSEIGNE');
        }
    }

    public function getOrigineLibelleExcel()
    {
        if ($this->getIsmanuel() == '1') {
            return Prado::localize('AJOUT_MANUEL_EXCEL');
        } else {
            return Prado::localize('AJOUT_PLATEFORME_EXCEL');
        }
    }

    public function getOrigineLibelle()
    {
        if ($this->getIsmanuel() == '1') {
            return Prado::localize('AJOUT_MANUEL');
        } else {
            return Prado::localize('AJOUT_PLATEFORME');
        }
    }

    public function getEtatLibelle()
    {
        if ($this->getValide() == '1') {
            return Prado::localize('DEFINE_OUI');
        } else {
            return Prado::localize('DEFINE_NON');
        }
    }

    public function getAbbrNaturePrestation()
    {
        if($this->getNaturemarche() == '1') {
            return "T";
        } elseif ($this->getNaturemarche() == '2') {
            return "F";
        } else {
            return "S";
        }
    }

    public function getMarchetranchebudgetaire($organismeAcronyme = null , $annee = null)
    {
        if ($organismeAcronyme == null) {
            $organismeAcronyme = Atexo_CurrentUser::getCurrentOrganism();
        }
        if($annee){
        	 $annee = Atexo_Util::getAnneeFromDate($annee);
        }
        $arrayReferentiel = (new Atexo_TrancheBudgetaire())->retrieveTrancheBudgetaireByOrgAnne($annee, $organismeAcronyme);
        if(isset($arrayReferentiel[$this->getIdmarchetranchebudgetaire()])) {
        	 return $arrayReferentiel[$this->getIdmarchetranchebudgetaire()];
        }else {
        	return "-";
        }

    }

    public function getPmePmiLabel($organismeAcronyme = null)
    {
        if ($organismeAcronyme == null) {
            $organismeAcronyme = Atexo_CurrentUser::getCurrentOrganism();
        }

        $arrayReferentiel = (new Atexo_ValeursReferentielles())->retrieveValeursReferentiellesByReference(Atexo_Config::getParameter('REFERENTIEL_PME_PMI'), $organismeAcronyme);
        return $arrayReferentiel[$this->getPmepmi()];
    }

    public function getServiceLibelle($organismeAcronyme = null, $identifiantService=null)
    {

	    if ($this->getIdservice() != 0) {
			$service = Atexo_EntityPurchase::retrieveEntityById($this->getIdservice(), $organismeAcronyme);
            return $service->getSigle().' - '.$service->getLibelle();
        } else {

	            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organismeAcronyme);
    	    	return $organismeO->getDenominationOrg();
        }
    }

} // CommonMarche
