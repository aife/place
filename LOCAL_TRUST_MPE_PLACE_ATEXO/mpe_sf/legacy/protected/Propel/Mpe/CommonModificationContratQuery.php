<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonModificationContratQuery;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for performing query and update operations on the 'modification_contrat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonModificationContratQuery extends BaseCommonModificationContratQuery
{

    public function getModificationsContrat($idContrat,$limit = null,$offset = null,$nbrElement = false,$connexion = '')
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }

        $this->filterByIdContratTitulaire($idContrat);
        if($limit){
            $this->limit($limit);
        }
        if($offset){
            $this->offset($offset);
        }
        $this->orderByNumOrdre(Criteria::ASC);
        if($nbrElement){
            return $this->count($connexion);
        }else{
            return (array) $this->find($connexion);
        }
    }
}
