<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\Entreprise;


use Application\Propel\Mpe\Om\BaseCommonEchangePieceJointe;
use Application\Service\Atexo\Atexo_Util;



/**
 * Skeleton subclass for representing a row from the 'EchangePieceJointe' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEchangePieceJointe extends BaseCommonEchangePieceJointe {

	private $_contentHorodatage = null;
	/**
	 * retourne l'url de téléchargement de la PJ
	 *
	 * @param string $role agent|entreprise
	 */  
     public function retrieveUrl($role,$acrOrg,$uid)
	 {
	 	if($role == "entreprise"){
	        return '?page=Entreprise.EntrepriseDownloadPjEchange&idPj='.$this->getPiece()."&orgAcronyme=".$acrOrg."&uid=".$uid."&idMsg=".$this->getIdMessage();
	    }
	    elseif ($role == "agent"){
	        return '?page=Agent.AgentDownloadPjEchange&idPj='.$this->getPiece();
	    }
	 }
	public function getHorodatage() {
		if(!$this->_contentHorodatage) {
			$this->_contentHorodatage = stream_get_contents( parent::getHorodatage());
		}
		return $this->_contentHorodatage;
	}

    /**
     * Permet de formatter le nom du fichier
     *
     * @param CommonEchangePieceJointe $file : objet fichier
     * @return string : nom du fichier
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @version 1.0
     * @since 4.11.0
     * @copyright Atexo 2015
     */
    public function getNomFichierFormate()
    {
        $extension  = Atexo_Util::getExtension($this->getNomFichier());
        if(empty($extension)) {
            return $this->getNomFichier().'.'.$this->getExtension();
        }
        return $this->getNomFichier();
    }

} // CommonEchangePieceJointe
