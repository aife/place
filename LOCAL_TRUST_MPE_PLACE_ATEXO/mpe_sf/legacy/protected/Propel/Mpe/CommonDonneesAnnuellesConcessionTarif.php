<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonDonneesAnnuellesConcessionTarif;

/**
 * Skeleton subclass for representing a row from the 'donnees_annuelles_concession_tarif' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonDonneesAnnuellesConcessionTarif extends BaseCommonDonneesAnnuellesConcessionTarif
{

    protected $numOrdre;

    /**
     * @return mixed
     */
    public function getNumOrdre()
    {
        return $this->numOrdre;
    }

    /**
     * @param mixed $numOrdre
     */
    public function setNumOrdre($numOrdre)
    {
        $this->numOrdre = $numOrdre;
    }


}
