<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonAnnonceMoniteur;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'AnnonceMoniteur' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAnnonceMoniteur extends BaseCommonAnnonceMoniteur {

	private $_contentTimestamp = null;
    public function retreiveDateEnvoi()
    {
        if($this->getDateEnvoi()!='0000-00-00 00:00:00' && $this->getDateEnvoi()!='0000-00-00') {
            return Atexo_Util::iso2frnDateTime($this->getDateEnvoi(),false);
        }
        else {
            return Prado::localize('NON_RENSEIGNE');
        }
    }

    public function retreiveDatePublication()
    {
        $code = $this->getStatutDestinataire();
        if($this->getDatepub()!='0000-00-00 00:00:00' && $this->getDatepub()!='0000-00-00') {
            if($code==Atexo_Config::getParameter('STATUT_MONITEUR_PUBLIE') ){
                return Atexo_Util::iso2frnDate($this->getDatepub());
            }
            else {
                return Prado::localize('NON_RENSEIGNE');
            }

        }
        else {
            return Prado::localize('NON_RENSEIGNE');
        }
    }

    public function retreiveAccuseReception()
    {
        $code = $this->getStatutDestinataire();
        if($code==Atexo_Config::getParameter('STATUT_MONITEUR_REJETE')
        ||$code==Atexo_Config::getParameter('STATUT_MONITEUR_EN_ATTENTE_REEMISSION')){
             return Atexo_Util::iso2frnDate($this->getDateMaj());
        }
        else {
            return '-';
        }
    }
	public function getTimestamp() {
		if(!$this->_contentTimestamp) {
			$this->_contentTimestamp = stream_get_contents( parent::getTimestamp());
		}
		return $this->_contentTimestamp;
	}
} // CommonAnnonceMoniteur
