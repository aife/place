<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonReferentielDestinationFormXmlPeer;

use Application\Propel\Mpe\CommonAnnonceMoniteur;

  // include base peer class

  // include object class
  use Application\Library\Propel\Query\Criteria;
  use Application\Propel\Mpe\Om\BaseCommonAnnonceMoniteurPeer;


  /**
 * Skeleton subclass for performing query and update operations on the 'AnnonceMoniteur' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAnnonceMoniteurPeer extends BaseCommonAnnonceMoniteurPeer {


		public static function doSelectJoinCommonReferentielDestinationFormXml(Criteria $c, $con = null, $organisme = false)
		{
			$c->addJoin(CommonAnnonceMoniteurPeer::ID_DESTINATION_FORM_XML, CommonReferentielDestinationFormXmlPeer::ID);
			$annonces = CommonAnnonceMoniteurPeer::doSelect($c, $con);
			$resultat = array();
			if($annonces[0] instanceof CommonAnnonceMoniteur) {
				$referentielDestinationFormXml = CommonReferentielDestinationFormXmlPeer::retrieveByPK($annonces[0]->getIdDestinationFormXml(), $organisme, $con);
				if($referentielDestinationFormXml) {
					if(is_array($annonces) && count($annonces)) {
						foreach($annonces as $annonce) {
							$referentielDestinationFormXml->addCommonAnnonceMoniteur($annonce);  
							$resultat[] = $annonce;
						}
					}
				}
			}
			return $resultat;
		}

} // CommonAnnonceMoniteurPeer
