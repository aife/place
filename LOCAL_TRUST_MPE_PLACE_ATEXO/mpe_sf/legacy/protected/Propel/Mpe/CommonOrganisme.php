<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonAlerteMetierPeer;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonOrganisme;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;


/**
 * Skeleton subclass for representing a row from the 'Organisme' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonOrganisme extends BaseCommonOrganisme {

	protected $logo;
	protected $addresseComplete;

    public function getDenominationOrgTraduit()
	{
		$langue = Atexo_CurrentUser::readFromSession("lang");
 	    $denomination_org= "denomination_org_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$denomination_org){
            return $this->denomination_org;
        } else {
            return $this->$denomination_org;
        }
	}

	public function getAdresseTraduit()
	{
		$langue = Atexo_CurrentUser::readFromSession("lang");
 	    $adresse= "adresse_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$adresse){
            return $this->adresse;
        } else {
            return $this->$adresse;
        }
	}

	public function getVilleTraduit()
	{
		$langue = Atexo_CurrentUser::readFromSession("lang");
 	    $ville= "ville_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$ville){
            return $this->ville;
        } else {
            return $this->$ville;
        }
	}

	public function getPaysTraduit()
	{
		$langue = Atexo_CurrentUser::readFromSession("lang");
 	    $pays= "pays_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$pays){
            return $this->pays;
        } else {
            return $this->$pays;
        }
	}


	public function getAlertes($open = NULL){
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		$c = new Criteria();
		$c->add(CommonAlerteMetierPeer::ORGANISME,$this->getAcronyme());
		if($open !== NULL){
			$c->add(CommonAlerteMetierPeer::CLOTUREE,2);
		}
		$alertes = CommonAlerteMetierPeer::doSelect($c, $connexion);
		return $alertes;
	}

	/**
  	 * Permet d'avoir les services Metiers
	 */

    public function getServiceMetierActive() {

		$listeService = (new Atexo_Socle_AgentServicesMetiers())->retrieveServicesByOrganisme($this->getAcronyme());
		$sigleServices ='';
		if(is_array($listeService)) {
		    foreach($listeService as $oneService) 
		    {
		    	if($oneService['checked']){
			        $sigleServices .= $oneService['sigle'];
			        $sigleServices .=' / ';
		    	}
		    }

		}
		 return  substr(trim($sigleServices,' '),0,-1);
	}

	/**
	 * @return mixed
	 */
	public function getLogo()
	{
		return $this->logo;
	}

	/**
	 * @param mixed $logo
	 */
	public function setLogo($logo)
	{
		$this->logo = $logo;
	}

	/**
	 * @return mixed
	 */
	public function getAddresseComplete()
	{
		return $this->getAdresse().($this->getAdresse2()?(" ".$this->getAdresse2()):"");
	}

	/**
	 * @param mixed $addresseComplete
	 */
	public function setAddresseComplete($addresseComplete)
	{
		$this->addresseComplete = $addresseComplete;
	}




} // CommonOrganisme
