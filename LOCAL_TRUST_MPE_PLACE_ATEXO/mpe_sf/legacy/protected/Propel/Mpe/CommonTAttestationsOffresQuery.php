<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonTAttestationsOffresQuery;

/**
 * Skeleton subclass for performing query and update operations on the 't_attestations_offres' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTAttestationsOffresQuery extends BaseCommonTAttestationsOffresQuery
{

    public function getAttestationOffre($idOffre,$typeDocument, $connexion = null){
        return static::create()->filterByIdOffre($idOffre)->filterByIdTypeDocument($typeDocument)->findOne($connexion);
    }

    public function getAllAttestationsOffre($idOffre, $connexion = null){
       return (array) static::create()->filterByIdOffre($idOffre)->find($connexion);
    }
}
