<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonDCE;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonAVISQuery;
use Application\Service\Atexo\Atexo_Config;


/**
 * Skeleton subclass for performing query and update operations on the 'AVIS' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.7.0-dev on:
 *
 * Mon Jul 15 17:53:26 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonAVISQuery extends BaseCommonAVISQuery
{

    /**
     * Permet de reourner le regelemnt by id
     *
     * @param int $id l'id de regelement
     * @param string $organisme Acronyme de l'organisme
     * @param optional $connexion
     * @return CommonDCE
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function getAvisById($id,$type = null,$statut= null, $organisme = null, $connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $this->filterById($id);
        if($type){
            $this->filterByType($type);
        }
        if($statut){
            $this->filterByStatut($statut);
        }
        if($organisme !== null){
            $this->filterByOrganisme($organisme, Criteria::LIKE);
        }
        return $this->findOne($connexion);
    }
}
