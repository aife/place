<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Query\Criteria;
use Application\Library\Propel\Collection\PropelObjectCollection;



use Application\Propel\Mpe\Om\BaseCommonAnnonceJAL;

/**
 * Skeleton subclass for representing a row from the 'AnnonceJAL' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAnnonceJAL extends BaseCommonAnnonceJAL {

/* La méthode getCommonAnnonceJALPieceJointes() se trouvant dans le fichier BaseCommonAnnonceJAL.php retourne un 
	 * objet collection "PropelObjectCollection". C'est pour cette raison que nous surchargeons la méthode pour
	 * retourner un tableau d'objets
	 * @param Criteria $criteria: critéria
     * @param $con: objet collection
     * @return: retourne un tableau d'objet recupéré dans la collection
	 */
	public function getCommonAnnonceJALPieceJointes($criteria = null, $con = null)
    {
    	$collectionAnnonceJALPieceJointe = parent::getCommonAnnonceJALPieceJointes($criteria, $con);
    	if(is_object($collectionAnnonceJALPieceJointe) && $collectionAnnonceJALPieceJointe->getData()) {
    		return $collectionAnnonceJALPieceJointe->getData();
    	}
    }
} // CommonAnnonceJAL
