<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonDecisionLot;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;

/**
 * Skeleton subclass for representing a row from the 'DecisionLot' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */
class CommonDecisionLot extends BaseCommonDecisionLot {

    public const DECISION_SANS_SUITE = 3;
    public const DECISION_INFRUCTUEUX = 4;
    public const DECISION_ENTREPRISE_SELECTIONNEES = 5;
    public const DECISION_AUTRE = 8;

	public function getLibelleTypeDecision($organisme=null)
	{
		if(!$organisme) {
			$organisme = Atexo_CurrentUser::getCurrentOrganism();
		}
		$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		$c=new Criteria();
		$c->add(CommonTypeDecisionPeer::ID_TYPE_DECISION, $this->getIdTypeDecision());
		$typeDecision=CommonTypeDecisionPeer::doSelectOne($c,$connexionCom);
		if($typeDecision instanceof CommonTypeDecision) {
			return $typeDecision->getLibelleTypeDecision();
		} else {
			return "";
		}
	}

	public function getDateDecisionAffiche($valeurRetourSiDateVide='')
	{
		if($this->getDateDecision() && $this->getDateDecision()!="0000-00-00") {
			return Atexo_Util::iso2frnDate($this->getDateDecision());
		} else {
			return $valeurRetourSiDateVide;
		}
	}
} // CommonDecisionLot
