<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonTypeAvisPubPeer;





use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonTypeAvisPub;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Skeleton subclass for representing a row from the 'Type_Avis_Pub' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonTypeAvisPub extends BaseCommonTypeAvisPub {

	public function getResourceForm($idTypeAvis) {
		$c=new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeAvisPubPeer::ID,$idTypeAvis);
        $avis = CommonTypeAvisPubPeer::doSelectOne($c,$connexion);
        if($avis instanceof CommonTypeAvisPub) {
        	return $avis->getResourceFormulaire();
        }
	}

	public function getTypeAvisById($idTypeAvis)
	{
		$c=new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeAvisPubPeer::ID,$idTypeAvis);
        $typeAvis = CommonTypeAvisPubPeer::doSelectOne($c,$connexion);

        return $typeAvis;
	}

	public function getLibelleTraduit() {
		$langue = Atexo_CurrentUser::readFromSession("lang");
 	    $libelle = "libelle_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$libelle){
            return $this->libelle;
        } else {
            return $this->libelle;
        }
	}

	public function getLibelleAvisById($idAvis) {
		$avisPub = (new self())->getTypeAvisById($idAvis);
		if($avisPub) {
	        return $avisPub->getLibelleTraduit();
		}
		return "";
	}
} // CommonTypeAvisPub
