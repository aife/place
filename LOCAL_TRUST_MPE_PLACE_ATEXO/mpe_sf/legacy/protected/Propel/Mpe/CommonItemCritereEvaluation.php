<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonItemCritereEvaluation;

/**
 * Skeleton subclass for representing a row from the 'ItemCritereEvaluation' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonItemCritereEvaluation extends BaseCommonItemCritereEvaluation {
	private bool $_isVisible = true;
	private $_reponse_fournisseur;
	private $_note;
	private $_commentaire_acheteur;
	private $_idenveloppecritereevaluation;

	public function setIsVisible($value)
	{
		 $this->_isVisible = $value;
	}
	public function getIsVisible()
	{
		return $this->_isVisible;
	}

	public function setReponseFournisseur($value)
	{
		 $this->_reponse_fournisseur = $value;
	}
	public function getReponseFournisseur()
	{
		return $this->_reponse_fournisseur;
	}

	public function setNote($value)
	{
		 $this->_note = $value;
	}
	public function getNote()
	{
		return $this->_note;
	}

	public function setCommentaireAcheteur($value)
	{
		 $this->_commentaire_acheteur = $value;
	}
	public function getCommentaireAcheteur()
	{
		return $this->_commentaire_acheteur;
	}

	public function setIdenveloppecritereevaluation($value)
	{
		 $this->_idenveloppecritereevaluation = $value;
	}
	public function getIdenveloppecritereevaluation()
	{
		return $this->_idenveloppecritereevaluation;
	}
} // CommonItemCritereEvaluation
