<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonItemFormulaireConsultation;

/**
 * Skeleton subclass for representing a row from the 'ItemFormulaireConsultation' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonItemFormulaireConsultation extends BaseCommonItemFormulaireConsultation {

	private bool $_isVisible = true;
	private $_valeur;
	private $_valuePrecisionentreprise;
	private $_tva;
	private $_prixUnitaireValue;
	private $_idReponseInscritItemFormulaireConsultation;

	public function setIsVisible($value)
	{
		 $this->_isVisible = $value;
	}
	public function getIsVisible()
	{
		return $this->_isVisible;
	}
	public function getIsObligatoire()
	{
		return ($this->getObligatoire()== '1');
	}
	public function setIsObligatoire($value)
	{
		$this->setObligatoire($value);
	}

	public function getValeur() 
	{
		return $this->_valeur;
	}
	public function getTva() 
	{
		return $this->_tva;
	}
	public function getPrixUnitaireValue() 
	{
		return $this->_prixUnitaireValue;
	}

	public function getValuePrecisionentreprise() 
	{
		return $this->_valuePrecisionentreprise;
	}

	public function getIdReponseInscritItemFormulaireConsultation() 
	{
		return $this->_idReponseInscritItemFormulaireConsultation;
	}

	public function setIdReponseInscritItemFormulaireConsultation($value) 
	{
		$this->_idReponseInscritItemFormulaireConsultation = $value;
	}

	public function setValeur($value)
	{
		$this->_valeur = $value;
	}

	public function setValuePrecisionentreprise($value)
	{
		$this->_valuePrecisionentreprise = $value;
	}

	public function setTva($value) 
	{
		$this->_tva = $value; 
	}
	public function setPrixUnitaireValue($value) 
	{
		$this->_prixUnitaireValue = $value;
	}


} // CommonItemFormulaireConsultation
