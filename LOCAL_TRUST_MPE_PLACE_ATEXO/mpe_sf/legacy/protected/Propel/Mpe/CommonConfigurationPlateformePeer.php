<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

  // include base peer class


  // include object class
use Application\Library\Propel\Connection\PropelPDO;
  use Application\Library\Propel\Exception\PropelException;
  use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonConfigurationPlateformePeer;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;
  /**
 * Skeleton subclass for performing query and update operations on the 'configuration_plateforme' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonConfigurationPlateformePeer extends BaseCommonConfigurationPlateformePeer {

	/**
     * Selects first row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects using the cache
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getConfigurationPlateforme()
    {
		$cachedConfigurationPlateforme = Prado::getApplication()->Cache->get('cachedConfigurationPlateforme');

		if(!$cachedConfigurationPlateforme) {
			$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

			$cachedConfigurationPlateforme = parent::doSelectOne(new Criteria(),$connexion);
			Prado::getApplication()->Cache->set('cachedConfigurationPlateforme', $cachedConfigurationPlateforme,Atexo_Config::getParameter('TTL_PRADO_CACHE'));
		}

		return $cachedConfigurationPlateforme;
    }

} // CommonConfigurationPlateformePeer
