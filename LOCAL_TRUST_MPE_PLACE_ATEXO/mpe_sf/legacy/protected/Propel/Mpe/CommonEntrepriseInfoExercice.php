<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonEntrepriseInfoExercice;
use Application\Service\Atexo\Atexo_Config;
use Exception;


/**
 * Skeleton subclass for representing a row from the 'CommonEntrepriseInfoExercice' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEntrepriseInfoExercice extends BaseCommonEntrepriseInfoExercice {

    public static function getCommonEntrepriseInfoExercice($idEntreprise,$annee,$connexion)
    {
        return self::retrieveEntrepriseInfoExerciceByIdEntreprise($idEntreprise,$annee,$connexion);
    }

    public static function retrieveEntrepriseInfoExerciceByIdEntreprise($idEntreprise,$annee,$connexion)
    {
    	 if(!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c = new Criteria();
        $c->add(CommonEntrepriseInfoExercicePeer::ID_ENTREPRISE,$idEntreprise);
        $c->add(CommonEntrepriseInfoExercicePeer::ANNEE_CLOTURE_EXERCICE,$annee);
        $Info = CommonEntrepriseInfoExercicePeer::doSelectOne($c,$connexion);
        if ($Info) {
            return $Info;
        }
        else {
            return false;
        }

    }

    public static function retrieveInfoExercicebyIdEntrepriseAndAnnee($idEntreprise,$annee,$connexion)
    {
     if(!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
         $c = new Criteria();
        $c->add(CommonEntrepriseInfoExercicePeer::ID_ENTREPRISE,$idEntreprise);
        $c->add(CommonEntrepriseInfoExercicePeer::ANNEE_CLOTURE_EXERCICE ,$annee);
   		$Info = CommonEntrepriseInfoExercicePeer::doSelect($c,$connexion);
        if ($Info) {
            return $Info[0];
        }
        else {
            return  new CommonEntrepriseInfoExercice();
        }
    }

    /* Migration la liste d'entreprise vers une nouvelle table nommée Entreprise_Info_Exercice
     * fonction recoit comme parametre une liste objets entreprise et mis a jour la table Entreprise_Info_Exercice.
     */
    public static function UpdateEntrepriseInfoExercice($Entreprises,$connexion=null)
    {
    	if ($Entreprises)
    	{
	    	if(!$connexion) {
	            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
	        }
    		foreach ($Entreprises as $entreprise)
    		{
    			$EntrepriseInfoExercice=new CommonEntrepriseInfoExercice();
	        	$EntrepriseInfoExercice->setIdEntreprise($entreprise->getId());
	    	 	$EntrepriseInfoExercice->setAnneeClotureExercice($entreprise->getAnneeClotureExercice1());
	            $EntrepriseInfoExercice->setDebutexerciceglob($entreprise->getDebutexerciceglob1());
	            $EntrepriseInfoExercice->setFinexerciceglob($entreprise->getFinexerciceglob1());
	            $EntrepriseInfoExercice->setVentesGlob($entreprise->getVentesGlob1());
	            $EntrepriseInfoExercice->setBiensGlob($entreprise->getBiensGlob1());
	            $EntrepriseInfoExercice->setServicesGlob($entreprise->getServicesGlob1());
	            $EntrepriseInfoExercice->setTotalGlob($entreprise->getTotalGlob1());  
	            $EntrepriseInfoExercice->setPme($entreprise->getPme1());
	            $EntrepriseInfoExercice->setEffectifMoyen($entreprise->getEffectifMoyen1());
	            $EntrepriseInfoExercice->setEffectifEncadrement($entreprise->getEffectifEncadrement1());  
	            try{
	           		 $EntrepriseInfoExercice->save($connexion);
	           		 echo "-> Succés de : Id entreprise = ".$entreprise->getId()." Année = ".$entreprise->getAnneeClotureExercice1()." \n";
	            }
	    		catch (Exception $e) {
	     			echo "\n-> Erreur de : Id entreprise = ".$entreprise->getId()." Année = ".$entreprise->getAnneeClotureExercice1()." Message : ".$e->getMessage()." \n";
	  			}

	            $EntrepriseInfoExercice=null;
    			$EntrepriseInfoExercice=new CommonEntrepriseInfoExercice();
	        	$EntrepriseInfoExercice->setIdEntreprise($entreprise->getId());
	    	 	$EntrepriseInfoExercice->setAnneeClotureExercice($entreprise->getAnneeClotureExercice2());
	            $EntrepriseInfoExercice->setDebutexerciceglob($entreprise->getDebutexerciceglob2());
	            $EntrepriseInfoExercice->setFinexerciceglob($entreprise->getFinexerciceglob2());
	            $EntrepriseInfoExercice->setVentesGlob($entreprise->getVentesGlob2());
	            $EntrepriseInfoExercice->setBiensGlob($entreprise->getBiensGlob2());
	            $EntrepriseInfoExercice->setServicesGlob($entreprise->getServicesGlob2());
	            $EntrepriseInfoExercice->setTotalGlob($entreprise->getTotalGlob2());  
	            $EntrepriseInfoExercice->setPme($entreprise->getPme2());
	            $EntrepriseInfoExercice->setEffectifMoyen($entreprise->getEffectifMoyen2());
	            $EntrepriseInfoExercice->setEffectifEncadrement($entreprise->getEffectifEncadrement2());  

    			try{
	           		 $EntrepriseInfoExercice->save($connexion);
	           		 echo "-> Succés de : Id entreprise = ".$entreprise->getId()." Année = ".$entreprise->getAnneeClotureExercice2()." \n";
	            }
	    		catch (Exception $e) {
	     			echo "\n-> Erreur de : Id entreprise = ".$entreprise->getId()." Année = ".$entreprise->getAnneeClotureExercice2()." Message : ".$e->getMessage()." \n";
	  			}

	            $EntrepriseInfoExercice=null;
    			$EntrepriseInfoExercice=new CommonEntrepriseInfoExercice();
	        	$EntrepriseInfoExercice->setIdEntreprise($entreprise->getId());
	    	 	$EntrepriseInfoExercice->setAnneeClotureExercice($entreprise->getAnneeClotureExercice3());
	            $EntrepriseInfoExercice->setDebutexerciceglob($entreprise->getDebutexerciceglob3());
	            $EntrepriseInfoExercice->setFinexerciceglob($entreprise->getFinexerciceglob3());
	            $EntrepriseInfoExercice->setVentesGlob($entreprise->getVentesGlob3());
	            $EntrepriseInfoExercice->setBiensGlob($entreprise->getBiensGlob3());
	            $EntrepriseInfoExercice->setServicesGlob($entreprise->getServicesGlob3());
	            $EntrepriseInfoExercice->setTotalGlob($entreprise->getTotalGlob3());  
	            $EntrepriseInfoExercice->setPme($entreprise->getPme3());
	            $EntrepriseInfoExercice->setEffectifMoyen($entreprise->getEffectifMoyen3());
	            $EntrepriseInfoExercice->setEffectifEncadrement($entreprise->getEffectifEncadrement3());  

	            try{
	           		 $EntrepriseInfoExercice->save($connexion);
	           		 echo "-> Succés de : Id entreprise = ".$entreprise->getId()." Année = ".$entreprise->getAnneeClotureExercice3()." \n";
	            }
	    		catch (Exception $e) {
	     			echo "\n-> Erreur de : Id entreprise = ".$entreprise->getId()." Année = ".$entreprise->getAnneeClotureExercice3()." Message : ".$e->getMessage()." \n";
	  			} 
    		}
    		return true;
    	}
    	return false;
    }

    /**
     * Récupère les enregistrements de la table Entreprise_info_exercice 
     * @param $idEntreprise: id de l'entreprise
     * @param $connexion: l'objet connexion
     * @return $infos: enregistrement de la table si trouvé, false sinon
     */
	public static function retrieveEntrepriseInfoExerciceByIdEtp($idEntreprise, $connexion)
    {
    	 if(!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c = new Criteria();
        $c->add(CommonEntrepriseInfoExercicePeer::ID_ENTREPRISE,$idEntreprise);
        $infos = CommonEntrepriseInfoExercicePeer::doSelect($c,$connexion);
        if ($infos) {
            return $infos;
        }
        else {
            return false;
        }

    }

    /**
     * Get the associated Entreprise object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Entreprise The associated Entreprise object.
     * @throws PropelException
     */
    public function getCommonEntreprise(PropelPDO $con = null, $doQuery = true)
    {
        return $this->getEntreprise($con,$doQuery);
    }
} // CommonEntrepriseInfoExercice

