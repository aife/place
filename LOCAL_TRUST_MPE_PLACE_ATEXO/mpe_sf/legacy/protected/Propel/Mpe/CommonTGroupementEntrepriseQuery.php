<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonTGroupementEntreprise;



use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonTGroupementEntrepriseQuery;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for performing query and update operations on the 't_groupement_entreprise' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTGroupementEntrepriseQuery extends BaseCommonTGroupementEntrepriseQuery
{

    /**
     * Permet de retourner les memebre d'un groupement d'une offre le groupement entreprise
     *
     * @param $type
     * @param $idOffre
     * @param null $connexion
     * @return void
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public function getMemebreGroupement($idOffre,$idRole = null,bool $justNombre = true,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $this->addJoin(CommonTGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE,CommonTMembreGroupementEntreprisePeer::ID_GROUPEMENT_ENTREPRISE);
        $this->add(CommonTGroupementEntreprisePeer::ID_OFFRE, $idOffre);
        if($idRole){
            $this->add(CommonTMembreGroupementEntreprisePeer::ID_ROLE_JURIDIQUE, $idRole);
        }
        if($justNombre){
            return $this->count($connexion);
        }else{
            return (array)$this->find($connexion);
        }

    }

    /**
     * Permet de retourner le groupement d'une offre
     *
     * @param $idOffre
     * @param null $connexion
     * @return CommonTGroupementEntreprise
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public function getGroupementByIdOffre($idOffre,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        return $this->filterByIdOffre($idOffre)->findOne($connexion);

    }
}
