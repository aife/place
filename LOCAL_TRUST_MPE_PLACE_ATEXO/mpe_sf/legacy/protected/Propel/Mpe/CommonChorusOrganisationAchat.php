<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonChorusOrganisationAchat;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;

/**
 * Skeleton subclass for representing a row from the 'Chorus_organisation_achat' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonChorusOrganisationAchat extends BaseCommonChorusOrganisationAchat {


    public function getGroupementAchat($idService = 0,$active = NULL)
    {
        $listGAByService = (new Atexo_Chorus_Echange())->retreiveGroupementAchatByIdOA(
            $this->getId(),
            $this->getOrganisme(),
            $idService,
            $active
        );

        $listAllGA = (new Atexo_Chorus_Echange())->retreiveGroupementAchatByIdOA(
            $this->getId(),
            $this->getOrganisme(),
            null,
            $active
        );

        foreach ($listAllGA as $keyServiceLess => $gaServiceLess) {
            foreach ($listGAByService as $ga) {
                if(
                    $gaServiceLess->getLibelle() === $ga->getLibelle()
                    && $gaServiceLess->getIdOa() === $ga->getIdOa()
                    && $gaServiceLess->getCode() === $ga->getCode()
                ) {
                    $listAllGA[$keyServiceLess] = $ga;
                }
            }
        }

        if(is_array($listAllGA) && count($listAllGA)){
            return $listAllGA;
        }

        return [];
    }
} // CommonChorusOrganisationAchat
