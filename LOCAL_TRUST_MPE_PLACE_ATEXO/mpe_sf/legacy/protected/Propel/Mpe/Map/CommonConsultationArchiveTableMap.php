<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'consultation_archive' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonConsultationArchiveTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonConsultationArchiveTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('consultation_archive');
        $this->setPhpName('CommonConsultationArchive');
        $this->setClassname('Application\\Propel\\Mpe\\CommonConsultationArchive');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', true, 30, null);
        $this->addColumn('consultation_ref', 'ConsultationRef', 'INTEGER', true, null, null);
        $this->addColumn('chemin_fichier', 'CheminFichier', 'VARCHAR', false, 256, null);
        $this->addColumn('date_archivage', 'DateArchivage', 'TIMESTAMP', false, null, null);
        $this->addColumn('poids_archivage', 'PoidsArchivage', 'INTEGER', false, null, null);
        $this->addColumn('annee_creation_consultation', 'AnneeCreationConsultation', 'INTEGER', false, null, null);
        $this->addColumn('status_global_transmission', 'StatusGlobalTransmission', 'VARCHAR', true, 50, null);
        $this->addColumn('status_fragmentation', 'StatusFragmentation', 'BOOLEAN', false, 1, false);
        $this->addColumn('nombre_bloc', 'NombreBloc', 'INTEGER', false, null, null);
        $this->addColumn('consultation_id', 'ConsultationId', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultationArchiveArcade', 'Application\\Propel\\Mpe\\CommonConsultationArchiveArcade', RelationMap::ONE_TO_ONE, array('id' => 'consultation_archive_id', ), null, null);
        $this->addRelation('CommonConsultationArchiveBloc', 'Application\\Propel\\Mpe\\CommonConsultationArchiveBloc', RelationMap::ONE_TO_MANY, array('id' => 'consultation_archive_id', ), null, null, 'CommonConsultationArchiveBlocs');
    } // buildRelations()

} // CommonConsultationArchiveTableMap
