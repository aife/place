<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'consultation_archive_atlas' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonConsultationArchiveAtlasTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonConsultationArchiveAtlasTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('consultation_archive_atlas');
        $this->setPhpName('CommonConsultationArchiveAtlas');
        $this->setClassname('Application\\Propel\\Mpe\\CommonConsultationArchiveAtlas');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('doc_id', 'DocId', 'VARCHAR', true, 256, null);
        $this->addColumn('numero_bloc', 'NumeroBloc', 'INTEGER', true, null, null);
        $this->addColumn('comp_id', 'CompId', 'VARCHAR', true, 256, null);
        $this->addColumn('consultation_ref', 'ConsultationRef', 'INTEGER', true, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', true, 256, null);
        $this->addColumn('taille', 'Taille', 'INTEGER', true, null, null);
        $this->addColumn('date_envoi', 'DateEnvoi', 'TIMESTAMP', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonConsultationArchiveAtlasTableMap
