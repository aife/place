<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'consultation_clauses_n3' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonConsultationClausesN3TableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonConsultationClausesN3TableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('consultation_clauses_n3');
        $this->setPhpName('CommonConsultationClausesN3');
        $this->setClassname('Application\\Propel\\Mpe\\CommonConsultationClausesN3');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('clause_n2_id', 'ClauseN2Id', 'INTEGER', 'consultation_clauses_n2', 'id', true, null, null);
        $this->addForeignKey('referentiel_clause_n3_id', 'ReferentielClauseN3Id', 'INTEGER', 'referentiel_consultation_clauses_n3', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultationClausesN2', 'Application\\Propel\\Mpe\\CommonConsultationClausesN2', RelationMap::MANY_TO_ONE, array('clause_n2_id' => 'id', ), null, null);
        $this->addRelation('CommonReferentielConsultationClausesN3', 'Application\\Propel\\Mpe\\CommonReferentielConsultationClausesN3', RelationMap::MANY_TO_ONE, array('referentiel_clause_n3_id' => 'id', ), null, null);
    } // buildRelations()

} // CommonConsultationClausesN3TableMap
