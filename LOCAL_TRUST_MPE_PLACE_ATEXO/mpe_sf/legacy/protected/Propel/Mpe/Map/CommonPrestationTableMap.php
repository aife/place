<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Prestation' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonPrestationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonPrestationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Prestation');
        $this->setPhpName('CommonPrestation');
        $this->setClassname('Application\\Propel\\Mpe\\CommonPrestation');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('num_marche', 'NumMarche', 'VARCHAR', false, 255, null);
        $this->addColumn('type_procedure', 'TypeProcedure', 'INTEGER', false, null, null);
        $this->addColumn('objet', 'Objet', 'CLOB', false, null, null);
        $this->addColumn('montant', 'Montant', 'VARCHAR', false, 255, null);
        $this->addColumn('maitre_ouvrage', 'MaitreOuvrage', 'VARCHAR', false, 255, null);
        $this->addColumn('date_debut_execution', 'DateDebutExecution', 'VARCHAR', false, 20, null);
        $this->addColumn('date_fin_execution', 'DateFinExecution', 'VARCHAR', false, 20, null);
        $this->addForeignKey('id_entreprise', 'IdEntreprise', 'INTEGER', 'Entreprise', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entreprise', 'Application\\Propel\\Mpe\\Entreprise', RelationMap::MANY_TO_ONE, array('id_entreprise' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonPrestationTableMap
