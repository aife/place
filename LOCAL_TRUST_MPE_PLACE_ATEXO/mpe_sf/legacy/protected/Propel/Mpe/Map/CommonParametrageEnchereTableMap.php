<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Parametrage_Enchere' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonParametrageEnchereTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonParametrageEnchereTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Parametrage_Enchere');
        $this->setPhpName('CommonParametrageEnchere');
        $this->setClassname('Application\\Propel\\Mpe\\CommonParametrageEnchere');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addForeignPrimaryKey('organisme', 'Organisme', 'VARCHAR' , 'Organisme', 'acronyme', true, 30, '');
        $this->addColumn('refConsultation', 'Refconsultation', 'INTEGER', false, 10, null);
        $this->addColumn('old_service_id', 'OldServiceId', 'INTEGER', false, null, null);
        $this->addColumn('referenceUtilisateur', 'Referenceutilisateur', 'VARCHAR', false, 45, null);
        $this->addColumn('auteur', 'Auteur', 'VARCHAR', false, 255, null);
        $this->addColumn('idLot', 'Idlot', 'INTEGER', false, 10, null);
        $this->addColumn('objet', 'Objet', 'CLOB', false, null, null);
        $this->addColumn('dateDebut', 'Datedebut', 'VARCHAR', false, 20, '0000-00-00 00:00:00');
        $this->addColumn('dateFin', 'Datefin', 'VARCHAR', false, 20, '0000-00-00 00:00:00');
        $this->addColumn('dateSuspension', 'Datesuspension', 'VARCHAR', false, 20, '0000-00-00 00:00:00');
        $this->addColumn('delaiProlongation', 'Delaiprolongation', 'INTEGER', false, null, null);
        $this->addColumn('commentaire', 'Commentaire', 'CLOB', false, null, null);
        $this->addColumn('meilleureEnchereObligatoire', 'Meilleureenchereobligatoire', 'CHAR', true, null, '0');
        $this->getColumn('meilleureEnchereObligatoire', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('typeBaremeNETC', 'Typebaremenetc', 'CHAR', true, null, '1');
        $this->getColumn('typeBaremeNETC', false)->setValueSet(array (
  0 => '1',
  1 => '2',
  2 => '3',
  3 => '4',
));
        $this->addColumn('typeBaremeEnchereGlobale', 'Typebaremeenchereglobale', 'CHAR', true, null, '1');
        $this->getColumn('typeBaremeEnchereGlobale', false)->setValueSet(array (
  0 => '1',
  1 => '2',
));
        $this->addColumn('meilleurNoteHaute', 'Meilleurnotehaute', 'CHAR', true, null, '0');
        $this->getColumn('meilleurNoteHaute', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('nbrCandidatsVisible', 'Nbrcandidatsvisible', 'CHAR', true, null, '1');
        $this->getColumn('nbrCandidatsVisible', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('listeCandidatsVisible', 'Listecandidatsvisible', 'CHAR', true, null, '1');
        $this->getColumn('listeCandidatsVisible', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('rangVisible', 'Rangvisible', 'CHAR', true, null, '1');
        $this->getColumn('rangVisible', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('meilleureOffreVisible', 'Meilleureoffrevisible', 'CHAR', true, null, '1');
        $this->getColumn('meilleureOffreVisible', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('montantReserve', 'Montantreserve', 'DOUBLE', false, null, null);
        $this->addColumn('noteMaxBaremeRelatif', 'Notemaxbaremerelatif', 'DOUBLE', false, null, null);
        $this->addColumn('coeffA', 'Coeffa', 'DOUBLE', false, null, null);
        $this->addColumn('coeffB', 'Coeffb', 'DOUBLE', false, null, null);
        $this->addColumn('coeffC', 'Coeffc', 'DOUBLE', false, null, null);
        $this->addColumn('mail', 'Mail', 'CLOB', false, null, null);
        $this->addColumn('note_entreprises', 'NoteEntreprises', 'VARCHAR', false, 50, null);
        $this->addColumn('consultation_id', 'ConsultationId', 'INTEGER', false, null, null);
        $this->addForeignKey('service_id', 'ServiceId', 'BIGINT', 'Service', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonService', 'Application\\Propel\\Mpe\\CommonService', RelationMap::MANY_TO_ONE, array('service_id' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), null, 'CASCADE');
        $this->addRelation('CommonParametrageEnchereReferenceRelatedByIdenchere', 'Application\\Propel\\Mpe\\CommonParametrageEnchereReference', RelationMap::ONE_TO_MANY, array('id' => 'idEnchere', ), null, 'CASCADE', 'CommonParametrageEnchereReferencesRelatedByIdenchere');
        $this->addRelation('CommonParametrageEnchereReferenceRelatedByIdenchereOrganisme', 'Application\\Propel\\Mpe\\CommonParametrageEnchereReference', RelationMap::ONE_TO_MANY, array('id' => 'idEnchere', 'organisme' => 'organisme', ), null, 'CASCADE', 'CommonParametrageEnchereReferencesRelatedByIdenchereOrganisme');
        $this->addRelation('CommonParametrageEnchereTranchesBaremeNETCRelatedByIdenchere', 'Application\\Propel\\Mpe\\CommonParametrageEnchereTranchesBaremeNETC', RelationMap::ONE_TO_MANY, array('id' => 'idEnchere', ), null, 'CASCADE', 'CommonParametrageEnchereTranchesBaremeNETCsRelatedByIdenchere');
        $this->addRelation('CommonParametrageEnchereTranchesBaremeNETCRelatedByIdenchereOrganisme', 'Application\\Propel\\Mpe\\CommonParametrageEnchereTranchesBaremeNETC', RelationMap::ONE_TO_MANY, array('id' => 'idEnchere', 'organisme' => 'organisme', ), null, 'CASCADE', 'CommonParametrageEnchereTranchesBaremeNETCsRelatedByIdenchereOrganisme');
    } // buildRelations()

} // CommonParametrageEnchereTableMap
