<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Organisme' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonOrganismeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonOrganismeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Organisme');
        $this->setPhpName('CommonOrganisme');
        $this->setClassname('Application\\Propel\\Mpe\\CommonOrganisme');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('acronyme', 'Acronyme', 'VARCHAR', true, 30, '');
        $this->addColumn('type_article_org', 'TypeArticleOrg', 'INTEGER', true, 1, 0);
        $this->addColumn('denomination_org', 'DenominationOrg', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('categorie_insee', 'CategorieInsee', 'VARCHAR', 'CategorieINSEE', 'id', false, 20, null);
        $this->addColumn('description_org', 'DescriptionOrg', 'CLOB', false, null, null);
        $this->addColumn('adresse', 'Adresse', 'VARCHAR', true, 100, '');
        $this->addColumn('cp', 'Cp', 'VARCHAR', true, 5, '');
        $this->addColumn('ville', 'Ville', 'VARCHAR', true, 100, '');
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, '');
        $this->addColumn('url', 'Url', 'VARCHAR', true, 100, '');
        $this->addColumn('id_attrib_file', 'IdAttribFile', 'VARCHAR', false, 11, null);
        $this->addColumn('attrib_file', 'AttribFile', 'VARCHAR', true, 150, '');
        $this->addColumn('date_creation', 'DateCreation', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('active', 'Active', 'CHAR', true, null, '1');
        $this->addColumn('id_client_ANM', 'IdClientAnm', 'VARCHAR', true, 32, '0');
        $this->addColumn('status', 'Status', 'CHAR', true, null, '0');
        $this->getColumn('status', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('signataire_cao', 'SignataireCao', 'LONGVARCHAR', false, null, null);
        $this->addColumn('sigle', 'Sigle', 'VARCHAR', true, 100, '');
        $this->addColumn('adresse2', 'Adresse2', 'VARCHAR', true, 100, '');
        $this->addColumn('tel', 'Tel', 'VARCHAR', true, 50, '');
        $this->addColumn('telecopie', 'Telecopie', 'VARCHAR', true, 50, '');
        $this->addColumn('pays', 'Pays', 'VARCHAR', false, 150, null);
        $this->addColumn('affichage_entite', 'AffichageEntite', 'CHAR', true, null, '');
        $this->addColumn('id_initial', 'IdInitial', 'INTEGER', true, null, 0);
        $this->addColumn('denomination_org_ar', 'DenominationOrgAr', 'VARCHAR', true, 100, '');
        $this->addColumn('description_org_ar', 'DescriptionOrgAr', 'CLOB', true, null, null);
        $this->addColumn('adresse_ar', 'AdresseAr', 'VARCHAR', true, 100, '');
        $this->addColumn('ville_ar', 'VilleAr', 'VARCHAR', true, 100, '');
        $this->addColumn('adresse2_ar', 'Adresse2Ar', 'VARCHAR', true, 100, '');
        $this->addColumn('pays_ar', 'PaysAr', 'VARCHAR', true, 150, '');
        $this->addColumn('denomination_org_fr', 'DenominationOrgFr', 'VARCHAR', true, 100, '');
        $this->addColumn('description_org_fr', 'DescriptionOrgFr', 'CLOB', true, null, null);
        $this->addColumn('adresse_fr', 'AdresseFr', 'VARCHAR', true, 100, '');
        $this->addColumn('ville_fr', 'VilleFr', 'VARCHAR', true, 100, '');
        $this->addColumn('adresse2_fr', 'Adresse2Fr', 'VARCHAR', true, 100, '');
        $this->addColumn('pays_fr', 'PaysFr', 'VARCHAR', true, 150, '');
        $this->addColumn('denomination_org_es', 'DenominationOrgEs', 'VARCHAR', true, 100, '');
        $this->addColumn('description_org_es', 'DescriptionOrgEs', 'CLOB', true, null, null);
        $this->addColumn('adresse_es', 'AdresseEs', 'VARCHAR', true, 100, '');
        $this->addColumn('ville_es', 'VilleEs', 'VARCHAR', true, 100, '');
        $this->addColumn('adresse2_es', 'Adresse2Es', 'VARCHAR', true, 100, '');
        $this->addColumn('pays_es', 'PaysEs', 'VARCHAR', true, 150, '');
        $this->addColumn('denomination_org_en', 'DenominationOrgEn', 'VARCHAR', true, 100, '');
        $this->addColumn('description_org_en', 'DescriptionOrgEn', 'CLOB', true, null, null);
        $this->addColumn('adresse_en', 'AdresseEn', 'VARCHAR', true, 100, '');
        $this->addColumn('ville_en', 'VilleEn', 'VARCHAR', true, 100, '');
        $this->addColumn('adresse2_en', 'Adresse2En', 'VARCHAR', true, 100, '');
        $this->addColumn('pays_en', 'PaysEn', 'VARCHAR', true, 150, '');
        $this->addColumn('denomination_org_su', 'DenominationOrgSu', 'VARCHAR', true, 100, '');
        $this->addColumn('description_org_su', 'DescriptionOrgSu', 'CLOB', true, null, null);
        $this->addColumn('adresse_su', 'AdresseSu', 'VARCHAR', true, 100, '');
        $this->addColumn('ville_su', 'VilleSu', 'VARCHAR', true, 100, '');
        $this->addColumn('adresse2_su', 'Adresse2Su', 'VARCHAR', true, 100, '');
        $this->addColumn('pays_su', 'PaysSu', 'VARCHAR', true, 150, '');
        $this->addColumn('denomination_org_du', 'DenominationOrgDu', 'VARCHAR', true, 100, '');
        $this->addColumn('description_org_du', 'DescriptionOrgDu', 'CLOB', true, null, null);
        $this->addColumn('adresse_du', 'AdresseDu', 'VARCHAR', true, 100, '');
        $this->addColumn('ville_du', 'VilleDu', 'VARCHAR', true, 100, '');
        $this->addColumn('adresse2_du', 'Adresse2Du', 'VARCHAR', true, 100, '');
        $this->addColumn('pays_du', 'PaysDu', 'VARCHAR', true, 150, '');
        $this->addColumn('denomination_org_cz', 'DenominationOrgCz', 'VARCHAR', true, 100, '');
        $this->addColumn('description_org_cz', 'DescriptionOrgCz', 'CLOB', true, null, null);
        $this->addColumn('adresse_cz', 'AdresseCz', 'VARCHAR', true, 100, '');
        $this->addColumn('ville_cz', 'VilleCz', 'VARCHAR', true, 100, '');
        $this->addColumn('adresse2_cz', 'Adresse2Cz', 'VARCHAR', true, 100, '');
        $this->addColumn('pays_cz', 'PaysCz', 'VARCHAR', true, 150, '');
        $this->addColumn('denomination_org_it', 'DenominationOrgIt', 'VARCHAR', true, 100, '');
        $this->addColumn('description_org_it', 'DescriptionOrgIt', 'CLOB', true, null, null);
        $this->addColumn('adresse_it', 'AdresseIt', 'VARCHAR', true, 100, '');
        $this->addColumn('ville_it', 'VilleIt', 'VARCHAR', true, 100, '');
        $this->addColumn('adresse2_it', 'Adresse2It', 'VARCHAR', true, 100, '');
        $this->addColumn('pays_it', 'PaysIt', 'VARCHAR', true, 150, '');
        $this->addColumn('siren', 'Siren', 'VARCHAR', true, 9, null);
        $this->addColumn('complement', 'Complement', 'VARCHAR', true, 5, null);
        $this->addColumn('moniteur_provenance', 'MoniteurProvenance', 'INTEGER', false, null, null);
        $this->addColumn('code_acces_logiciel', 'CodeAccesLogiciel', 'VARCHAR', false, 30, null);
        $this->addColumn('decalage_horaire', 'DecalageHoraire', 'VARCHAR', false, 5, null);
        $this->addColumn('lieu_residence', 'LieuResidence', 'VARCHAR', false, 255, null);
        $this->addColumn('activation_fuseau_horaire', 'ActivationFuseauHoraire', 'CHAR', true, null, '0');
        $this->getColumn('activation_fuseau_horaire', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alerte', 'Alerte', 'CHAR', true, null, '0');
        $this->getColumn('alerte', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('ordre', 'Ordre', 'INTEGER', true, null, 0);
        $this->addColumn('URL_INTERFACE_ANM', 'UrlInterfaceAnm', 'VARCHAR', false, 100, null);
        $this->addColumn('sous_type_organisme', 'SousTypeOrganisme', 'INTEGER', true, null, 2);
        $this->addColumn('pf_url', 'PfUrl', 'VARCHAR', false, 256, null);
        $this->addColumn('tag_purge', 'TagPurge', 'BOOLEAN', false, 1, false);
        $this->addColumn('id_externe', 'IdExterne', 'VARCHAR', true, 50, '0');
        $this->addColumn('id_entite', 'IdEntite', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonCategorieINSEE', 'Application\\Propel\\Mpe\\CommonCategorieINSEE', RelationMap::MANY_TO_ONE, array('categorie_insee' => 'id', ), 'SET NULL', 'CASCADE');
        $this->addRelation('CommonAdministrateur', 'Application\\Propel\\Mpe\\CommonAdministrateur', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonAdministrateurs');
        $this->addRelation('CommonAffiliationService', 'Application\\Propel\\Mpe\\CommonAffiliationService', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, 'CASCADE', 'CommonAffiliationServices');
        $this->addRelation('CommonAgent', 'Application\\Propel\\Mpe\\CommonAgent', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, 'CASCADE', 'CommonAgents');
        $this->addRelation('CommonEchange', 'Application\\Propel\\Mpe\\CommonEchange', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonEchanges');
        $this->addRelation('CommonOrganismeServiceMetier', 'Application\\Propel\\Mpe\\CommonOrganismeServiceMetier', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, 'CASCADE', 'CommonOrganismeServiceMetiers');
        $this->addRelation('CommonParametrageEnchere', 'Application\\Propel\\Mpe\\CommonParametrageEnchere', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, 'CASCADE', 'CommonParametrageEncheres');
        $this->addRelation('CommonRPA', 'Application\\Propel\\Mpe\\CommonRPA', RelationMap::ONE_TO_MANY, array('acronyme' => 'acronymeOrg', ), null, 'CASCADE', 'CommonRPAs');
        $this->addRelation('CommonTelechargement', 'Application\\Propel\\Mpe\\CommonTelechargement', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, 'CASCADE', 'CommonTelechargements');
        $this->addRelation('CommonTiers', 'Application\\Propel\\Mpe\\CommonTiers', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonTierss');
        $this->addRelation('CommonConfigurationOrganisme', 'Application\\Propel\\Mpe\\CommonConfigurationOrganisme', RelationMap::ONE_TO_ONE, array('acronyme' => 'organisme', ), null, 'CASCADE');
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, 'CASCADE', 'CommonConsultations');
        $this->addRelation('CommonDestinataireMiseDisposition', 'Application\\Propel\\Mpe\\CommonDestinataireMiseDisposition', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, 'CASCADE', 'CommonDestinataireMiseDispositions');
        $this->addRelation('CommonEchangeDocApplicationClientOrganisme', 'Application\\Propel\\Mpe\\CommonEchangeDocApplicationClientOrganisme', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme_acronyme', ), null, 'CASCADE', 'CommonEchangeDocApplicationClientOrganismes');
        $this->addRelation('CommonInvitePermanentTransverse', 'Application\\Propel\\Mpe\\CommonInvitePermanentTransverse', RelationMap::ONE_TO_MANY, array('acronyme' => 'acronyme', ), null, 'CASCADE', 'CommonInvitePermanentTransverses');
        $this->addRelation('CommonPlateformeVirtuelleOrganisme', 'Application\\Propel\\Mpe\\CommonPlateformeVirtuelleOrganisme', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme_acronyme', ), null, 'CASCADE', 'CommonPlateformeVirtuelleOrganismes');
        $this->addRelation('CommonTCAOCommission', 'Application\\Propel\\Mpe\\CommonTCAOCommission', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonTCAOCommissions');
        $this->addRelation('CommonTCAOCommissionAgent', 'Application\\Propel\\Mpe\\CommonTCAOCommissionAgent', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonTCAOCommissionAgents');
        $this->addRelation('CommonTCAOCommissionIntervenantExterne', 'Application\\Propel\\Mpe\\CommonTCAOCommissionIntervenantExterne', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonTCAOCommissionIntervenantExternes');
        $this->addRelation('CommonTCAOIntervenantExterne', 'Application\\Propel\\Mpe\\CommonTCAOIntervenantExterne', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonTCAOIntervenantExternes');
        $this->addRelation('CommonTCAOOrdreDuJour', 'Application\\Propel\\Mpe\\CommonTCAOOrdreDuJour', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonTCAOOrdreDuJours');
        $this->addRelation('CommonTCAOOrdreDuJourIntervenant', 'Application\\Propel\\Mpe\\CommonTCAOOrdreDuJourIntervenant', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonTCAOOrdreDuJourIntervenants');
        $this->addRelation('CommonTCAOSeance', 'Application\\Propel\\Mpe\\CommonTCAOSeance', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonTCAOSeances');
        $this->addRelation('CommonTCAOSeanceAgent', 'Application\\Propel\\Mpe\\CommonTCAOSeanceAgent', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonTCAOSeanceAgents');
        $this->addRelation('CommonTCAOSeanceIntervenantExterne', 'Application\\Propel\\Mpe\\CommonTCAOSeanceIntervenantExterne', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, null, 'CommonTCAOSeanceIntervenantExternes');
        $this->addRelation('CommonTCandidature', 'Application\\Propel\\Mpe\\CommonTCandidature', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, 'CASCADE', 'CommonTCandidatures');
        $this->addRelation('CommonTDumeContexte', 'Application\\Propel\\Mpe\\CommonTDumeContexte', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, 'CASCADE', 'CommonTDumeContextes');
        $this->addRelation('CommonTListeLotsCandidature', 'Application\\Propel\\Mpe\\CommonTListeLotsCandidature', RelationMap::ONE_TO_MANY, array('acronyme' => 'organisme', ), null, 'CASCADE', 'CommonTListeLotsCandidatures');
        $this->addRelation('CommonTVisionRmaAgentOrganisme', 'Application\\Propel\\Mpe\\CommonTVisionRmaAgentOrganisme', RelationMap::ONE_TO_MANY, array('acronyme' => 'acronyme', ), null, null, 'CommonTVisionRmaAgentOrganismes');
    } // buildRelations()

} // CommonOrganismeTableMap
