<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_information_modification_password' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTInformationModificationPasswordTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTInformationModificationPasswordTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_information_modification_password');
        $this->setPhpName('CommonTInformationModificationPassword');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTInformationModificationPassword');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('id_user', 'IdUser', 'INTEGER', true, null, null);
        $this->addColumn('type_user', 'TypeUser', 'VARCHAR', true, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, null);
        $this->addColumn('date_demande_modification', 'DateDemandeModification', 'VARCHAR', true, 20, null);
        $this->addColumn('date_fin_validite', 'DateFinValidite', 'VARCHAR', true, 20, null);
        $this->addColumn('jeton', 'Jeton', 'VARCHAR', true, 255, null);
        $this->addColumn('modification_faite', 'ModificationFaite', 'CHAR', true, null, '0');
        $this->getColumn('modification_faite', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonTInformationModificationPasswordTableMap
