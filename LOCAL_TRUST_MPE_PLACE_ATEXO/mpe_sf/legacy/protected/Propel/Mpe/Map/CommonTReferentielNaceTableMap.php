<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_referentiel_nace' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTReferentielNaceTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTReferentielNaceTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_referentiel_nace');
        $this->setPhpName('CommonTReferentielNace');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTReferentielNace');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('code_nace_5', 'CodeNace5', 'VARCHAR', true, 255, null);
        $this->addColumn('libelle_activite_detaillee', 'LibelleActiviteDetaillee', 'LONGVARCHAR', true, null, null);
        $this->addColumn('code_nace_2', 'CodeNace2', 'VARCHAR', true, 255, null);
        $this->addColumn('libelle_activite_general', 'LibelleActiviteGeneral', 'LONGVARCHAR', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonTReferentielNaceTableMap
