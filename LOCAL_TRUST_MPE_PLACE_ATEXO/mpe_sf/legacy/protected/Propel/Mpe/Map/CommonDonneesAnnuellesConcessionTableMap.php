<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'donnees_annuelles_concession' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonDonneesAnnuellesConcessionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonDonneesAnnuellesConcessionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('donnees_annuelles_concession');
        $this->setPhpName('CommonDonneesAnnuellesConcession');
        $this->setClassname('Application\\Propel\\Mpe\\CommonDonneesAnnuellesConcession');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('id_contrat', 'IdContrat', 'INTEGER', 't_contrat_titulaire', 'id_contrat_titulaire', true, null, null);
        $this->addColumn('valeur_depense', 'ValeurDepense', 'DOUBLE', false, null, null);
        $this->addColumn('date_saisie', 'DateSaisie', 'TIMESTAMP', false, null, null);
        $this->addColumn('num_ordre', 'NumOrdre', 'INTEGER', false, null, null);
        $this->addColumn('suivi_publication_sn', 'SuiviPublicationSn', 'INTEGER', true, 2, 0);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTContratTitulaire', 'Application\\Propel\\Mpe\\CommonTContratTitulaire', RelationMap::MANY_TO_ONE, array('id_contrat' => 'id_contrat_titulaire', ), null, null);
        $this->addRelation('CommonDonneesAnnuellesConcessionTarif', 'Application\\Propel\\Mpe\\CommonDonneesAnnuellesConcessionTarif', RelationMap::ONE_TO_MANY, array('id' => 'id_donnees_annuelle', ), null, null, 'CommonDonneesAnnuellesConcessionTarifs');
    } // buildRelations()

} // CommonDonneesAnnuellesConcessionTableMap
