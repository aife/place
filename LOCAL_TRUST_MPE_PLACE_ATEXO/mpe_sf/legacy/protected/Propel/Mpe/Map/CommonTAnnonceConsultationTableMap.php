<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_annonce_consultation' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTAnnonceConsultationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTAnnonceConsultationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_annonce_consultation');
        $this->setPhpName('CommonTAnnonceConsultation');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTAnnonceConsultation');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('ref_consultation', 'RefConsultation', 'INTEGER', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', true, 30, null);
        $this->addColumn('id_dossier_sub', 'IdDossierSub', 'INTEGER', true, null, null);
        $this->addColumn('id_dispositif', 'IdDispositif', 'INTEGER', true, null, null);
        $this->addColumn('id_compte_boamp', 'IdCompteBoamp', 'INTEGER', true, null, null);
        $this->addColumn('statut', 'Statut', 'VARCHAR', true, 255, null);
        $this->addForeignKey('id_type_annonce', 'IdTypeAnnonce', 'INTEGER', 't_type_annonce_consultation', 'id', false, null, null);
        $this->addColumn('id_agent', 'IdAgent', 'INTEGER', true, null, null);
        $this->addColumn('prenom_nom_agent', 'PrenomNomAgent', 'VARCHAR', true, 255, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', true, null, null);
        $this->addColumn('date_modification', 'DateModification', 'TIMESTAMP', true, null, null);
        $this->addColumn('date_statut', 'DateStatut', 'TIMESTAMP', true, null, null);
        $this->addColumn('motif_statut', 'MotifStatut', 'VARCHAR', false, 255, null);
        $this->addColumn('id_dossier_parent', 'IdDossierParent', 'INTEGER', false, null, null);
        $this->addColumn('id_dispositif_parent', 'IdDispositifParent', 'INTEGER', false, null, null);
        $this->addColumn('publicite_simplifie', 'PubliciteSimplifie', 'CHAR', true, null, '0');
        $this->getColumn('publicite_simplifie', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonTTypeAnnonceConsultation', 'Application\\Propel\\Mpe\\CommonTTypeAnnonceConsultation', RelationMap::MANY_TO_ONE, array('id_type_annonce' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonTHistoriqueAnnonce', 'Application\\Propel\\Mpe\\CommonTHistoriqueAnnonce', RelationMap::ONE_TO_MANY, array('id' => 'id_annonce', ), 'CASCADE', 'CASCADE', 'CommonTHistoriqueAnnonces');
        $this->addRelation('CommonTSupportAnnonceConsultation', 'Application\\Propel\\Mpe\\CommonTSupportAnnonceConsultation', RelationMap::ONE_TO_MANY, array('id' => 'id_annonce_cons', ), 'CASCADE', 'CASCADE', 'CommonTSupportAnnonceConsultations');
    } // buildRelations()

} // CommonTAnnonceConsultationTableMap
