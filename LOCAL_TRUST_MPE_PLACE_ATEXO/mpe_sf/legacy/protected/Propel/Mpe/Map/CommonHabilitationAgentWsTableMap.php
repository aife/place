<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'habilitation_agent_ws' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonHabilitationAgentWsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonHabilitationAgentWsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('habilitation_agent_ws');
        $this->setPhpName('CommonHabilitationAgentWs');
        $this->setClassname('Application\\Propel\\Mpe\\CommonHabilitationAgentWs');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('agent_id', 'AgentId', 'INTEGER', 'Agent', 'id', true, null, null);
        $this->addForeignKey('web_service_id', 'WebServiceId', 'INTEGER', 'web_service', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonAgent', 'Application\\Propel\\Mpe\\CommonAgent', RelationMap::MANY_TO_ONE, array('agent_id' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonWebService', 'Application\\Propel\\Mpe\\CommonWebService', RelationMap::MANY_TO_ONE, array('web_service_id' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonHabilitationAgentWsTableMap
