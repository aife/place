<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'AVIS' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonAVISTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonAVISTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('AVIS');
        $this->setPhpName('CommonAVIS');
        $this->setClassname('Application\\Propel\\Mpe\\CommonAVIS');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 30, null);
        $this->addColumn('consultation_ref', 'ConsultationRef', 'VARCHAR', false, 256, null);
        $this->addColumn('avis', 'Avis', 'INTEGER', true, null, 0);
        $this->addColumn('intitule_avis', 'IntituleAvis', 'INTEGER', true, 2, 0);
        $this->addColumn('nom_avis', 'NomAvis', 'VARCHAR', true, 200, null);
        $this->addColumn('statut', 'Statut', 'CHAR', true, null, '0');
        $this->addColumn('nom_fichier', 'NomFichier', 'VARCHAR', true, 200, null);
        $this->addColumn('horodatage', 'Horodatage', 'BLOB', true, null, null);
        $this->addColumn('untrusteddate', 'Untrusteddate', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('agent_id', 'AgentId', 'INTEGER', true, null, 0);
        $this->addColumn('avis_telechargeable', 'AvisTelechargeable', 'INTEGER', true, null, 0);
        $this->addColumn('url', 'Url', 'LONGVARCHAR', false, null, null);
        $this->addColumn('type', 'Type', 'CHAR', false, null, '0');
        $this->addColumn('date_creation', 'DateCreation', 'VARCHAR', true, 20, null);
        $this->addColumn('date_pub', 'DatePub', 'VARCHAR', false, 20, null);
        $this->addColumn('type_doc_genere', 'TypeDocGenere', 'INTEGER', true, null, 0);
        $this->addColumn('langue', 'Langue', 'VARCHAR', true, 10, null);
        $this->addColumn('type_avis_pub', 'TypeAvisPub', 'INTEGER', false, 5, null);
        $this->addColumn('chemin', 'Chemin', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonAVISTableMap
