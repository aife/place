<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'referentiel_consultation_clauses_n2' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonReferentielConsultationClausesN2TableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonReferentielConsultationClausesN2TableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('referentiel_consultation_clauses_n2');
        $this->setPhpName('CommonReferentielConsultationClausesN2');
        $this->setClassname('Application\\Propel\\Mpe\\CommonReferentielConsultationClausesN2');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('clause_n1_id', 'ClauseN1Id', 'INTEGER', 'referentiel_consultation_clauses_n1', 'id', true, null, null);
        $this->addColumn('label', 'Label', 'VARCHAR', true, 255, null);
        $this->addColumn('slug', 'Slug', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonReferentielConsultationClausesN1', 'Application\\Propel\\Mpe\\CommonReferentielConsultationClausesN1', RelationMap::MANY_TO_ONE, array('clause_n1_id' => 'id', ), null, null);
        $this->addRelation('CommonConsultationClausesN2', 'Application\\Propel\\Mpe\\CommonConsultationClausesN2', RelationMap::ONE_TO_MANY, array('id' => 'referentiel_clause_n2_id', ), null, null, 'CommonConsultationClausesN2s');
        $this->addRelation('CommonReferentielConsultationClausesN2ClausesN3', 'Application\\Propel\\Mpe\\CommonReferentielConsultationClausesN2ClausesN3', RelationMap::ONE_TO_MANY, array('id' => 'clauses_n2_id', ), 'CASCADE', null, 'CommonReferentielConsultationClausesN2ClausesN3s');
    } // buildRelations()

} // CommonReferentielConsultationClausesN2TableMap
