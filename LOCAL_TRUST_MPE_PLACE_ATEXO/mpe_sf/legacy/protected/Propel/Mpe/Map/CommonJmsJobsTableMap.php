<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'jms_jobs' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonJmsJobsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonJmsJobsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jms_jobs');
        $this->setPhpName('CommonJmsJobs');
        $this->setClassname('Application\\Propel\\Mpe\\CommonJmsJobs');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'BIGINT', true, null, null);
        $this->addColumn('state', 'State', 'VARCHAR', true, 15, null);
        $this->addColumn('queue', 'Queue', 'VARCHAR', true, 50, null);
        $this->addColumn('priority', 'Priority', 'SMALLINT', true, null, null);
        $this->addColumn('createdAt', 'Createdat', 'TIMESTAMP', true, null, null);
        $this->addColumn('startedAt', 'Startedat', 'TIMESTAMP', false, null, null);
        $this->addColumn('checkedAt', 'Checkedat', 'TIMESTAMP', false, null, null);
        $this->addColumn('workerName', 'Workername', 'VARCHAR', false, 50, null);
        $this->addColumn('executeAfter', 'Executeafter', 'TIMESTAMP', false, null, null);
        $this->addColumn('closedAt', 'Closedat', 'TIMESTAMP', false, null, null);
        $this->addColumn('command', 'Command', 'VARCHAR', true, 255, null);
        $this->addColumn('args', 'Args', 'CLOB', true, null, null);
        $this->addColumn('output', 'Output', 'CLOB', false, null, null);
        $this->addColumn('errorOutput', 'Erroroutput', 'CLOB', false, null, null);
        $this->addColumn('exitCode', 'Exitcode', 'SMALLINT', false, 5, null);
        $this->addColumn('maxRuntime', 'Maxruntime', 'SMALLINT', true, 5, null);
        $this->addColumn('maxRetries', 'Maxretries', 'SMALLINT', true, 5, null);
        $this->addColumn('stackTrace', 'Stacktrace', 'BLOB', false, null, null);
        $this->addColumn('runtime', 'Runtime', 'SMALLINT', false, 5, null);
        $this->addColumn('memoryUsage', 'Memoryusage', 'INTEGER', false, 10, null);
        $this->addColumn('memoryUsageReal', 'Memoryusagereal', 'INTEGER', false, 10, null);
        $this->addForeignKey('originalJob_id', 'OriginaljobId', 'BIGINT', 'jms_jobs', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonJmsJobsRelatedByOriginaljobId', 'Application\\Propel\\Mpe\\CommonJmsJobs', RelationMap::MANY_TO_ONE, array('originalJob_id' => 'id', ), null, null);
        $this->addRelation('CommonJmsJobDependenciesRelatedByDestJobId', 'Application\\Propel\\Mpe\\CommonJmsJobDependencies', RelationMap::ONE_TO_MANY, array('id' => 'dest_job_id', ), null, null, 'CommonJmsJobDependenciessRelatedByDestJobId');
        $this->addRelation('CommonJmsJobDependenciesRelatedBySourceJobId', 'Application\\Propel\\Mpe\\CommonJmsJobDependencies', RelationMap::ONE_TO_MANY, array('id' => 'source_job_id', ), null, null, 'CommonJmsJobDependenciessRelatedBySourceJobId');
        $this->addRelation('CommonJmsJobRelatedEntities', 'Application\\Propel\\Mpe\\CommonJmsJobRelatedEntities', RelationMap::ONE_TO_MANY, array('id' => 'job_id', ), null, null, 'CommonJmsJobRelatedEntitiess');
        $this->addRelation('CommonJmsJobsRelatedById', 'Application\\Propel\\Mpe\\CommonJmsJobs', RelationMap::ONE_TO_MANY, array('id' => 'originalJob_id', ), null, null, 'CommonJmsJobssRelatedById');
    } // buildRelations()

} // CommonJmsJobsTableMap
