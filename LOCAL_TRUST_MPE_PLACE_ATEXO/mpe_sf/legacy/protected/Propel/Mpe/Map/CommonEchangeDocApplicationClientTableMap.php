<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'echange_doc_application_client' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEchangeDocApplicationClientTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEchangeDocApplicationClientTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('echange_doc_application_client');
        $this->setPhpName('CommonEchangeDocApplicationClient');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEchangeDocApplicationClient');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('echange_doc_application_id', 'EchangeDocApplicationId', 'INTEGER', 'echange_doc_application', 'id', false, null, null);
        $this->addColumn('code', 'Code', 'VARCHAR', true, 255, null);
        $this->addColumn('libelle', 'Libelle', 'VARCHAR', true, 255, null);
        $this->addColumn('actif', 'Actif', 'BOOLEAN', true, 1, true);
        $this->addColumn('cheminement_signature', 'CheminementSignature', 'BOOLEAN', false, 1, false);
        $this->addColumn('cheminement_ged', 'CheminementGed', 'BOOLEAN', false, 1, false);
        $this->addColumn('cheminement_sae', 'CheminementSae', 'BOOLEAN', false, 1, false);
        $this->addColumn('cheminement_tdt', 'CheminementTdt', 'BOOLEAN', false, 1, false);
        $this->addColumn('classification_1', 'Classification1', 'VARCHAR', false, 255, null);
        $this->addColumn('classification_2', 'Classification2', 'VARCHAR', false, 255, null);
        $this->addColumn('classification_3', 'Classification3', 'VARCHAR', false, 255, null);
        $this->addColumn('classification_4', 'Classification4', 'VARCHAR', false, 255, null);
        $this->addColumn('classification_5', 'Classification5', 'VARCHAR', false, 255, null);
        $this->addColumn('multi_docs_principaux', 'MultiDocsPrincipaux', 'BOOLEAN', true, 1, false);
        $this->addColumn('multi_docs_principaux', 'MultiDocsPrincipaux', 'BOOLEAN', true, 1, false);
        $this->addColumn('envoi_auto_archivage', 'EnvoiAutoArchivage', 'BOOLEAN', true, 1, false);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEchangeDocApplication', 'Application\\Propel\\Mpe\\CommonEchangeDocApplication', RelationMap::MANY_TO_ONE, array('echange_doc_application_id' => 'id', ), null, null);
        $this->addRelation('CommonEchangeDoc', 'Application\\Propel\\Mpe\\CommonEchangeDoc', RelationMap::ONE_TO_MANY, array('id' => 'echange_doc_application_client_id', ), null, null, 'CommonEchangeDocs');
        $this->addRelation('CommonEchangeDocApplicationClientOrganisme', 'Application\\Propel\\Mpe\\CommonEchangeDocApplicationClientOrganisme', RelationMap::ONE_TO_MANY, array('id' => 'echange_doc_application_client_id', ), null, null, 'CommonEchangeDocApplicationClientOrganismes');
    } // buildRelations()

} // CommonEchangeDocApplicationClientTableMap
