<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'TelechargementAnonyme' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTelechargementAnonymeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTelechargementAnonymeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('TelechargementAnonyme');
        $this->setPhpName('CommonTelechargementAnonyme');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTelechargementAnonyme');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 22, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 30, '');
        $this->addColumn('consultation_ref', 'ConsultationRef', 'INTEGER', false, null, null);
        $this->addColumn('datetelechargement', 'Datetelechargement', 'TIMESTAMP', true, null, null);
        $this->addColumn('tirage_plan', 'TiragePlan', 'INTEGER', true, null, 0);
        $this->addColumn('support', 'Support', 'CHAR', true, null, '1');
        $this->getColumn('support', false)->setValueSet(array (
  0 => '1',
  1 => '2',
  2 => '3',
));
        $this->addColumn('noms_fichiers_dce', 'NomsFichiersDce', 'LONGVARCHAR', false, null, null);
        $this->addColumn('poids_telechargement', 'PoidsTelechargement', 'INTEGER', true, null, 0);
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), null, 'CASCADE');
    } // buildRelations()

} // CommonTelechargementAnonymeTableMap
