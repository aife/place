<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'echanges_interfaces' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEchangesInterfacesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEchangesInterfacesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('echanges_interfaces');
        $this->setPhpName('CommonEchangesInterfaces');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEchangesInterfaces');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('code_retour', 'CodeRetour', 'INTEGER', 'code_retour', 'code', true, null, null);
        $this->addColumn('service', 'Service', 'VARCHAR', true, 255, null);
        $this->addColumn('nom_batch', 'NomBatch', 'VARCHAR', true, 255, null);
        $this->addColumn('variables_entree', 'VariablesEntree', 'CLOB', false, null, null);
        $this->addColumn('resultat', 'Resultat', 'CLOB', false, null, null);
        $this->addColumn('type_flux', 'TypeFlux', 'VARCHAR', false, 255, null);
        $this->addColumn('poids', 'Poids', 'INTEGER', false, null, null);
        $this->addColumn('information_metier', 'InformationMetier', 'CLOB', false, null, null);
        $this->addColumn('nb_flux', 'NbFlux', 'INTEGER', false, null, null);
        $this->addColumn('debut_execution', 'DebutExecution', 'TIMESTAMP', true, null, null);
        $this->addColumn('fin_execution', 'FinExecution', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('id_echanges_interfaces', 'IdEchangesInterfaces', 'INTEGER', 'echanges_interfaces', 'id', false, null, null);
        $this->addColumn('nom_interface', 'NomInterface', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonCodeRetour', 'Application\\Propel\\Mpe\\CommonCodeRetour', RelationMap::MANY_TO_ONE, array('code_retour' => 'code', ), null, null);
        $this->addRelation('CommonEchangesInterfacesRelatedByIdEchangesInterfaces', 'Application\\Propel\\Mpe\\CommonEchangesInterfaces', RelationMap::MANY_TO_ONE, array('id_echanges_interfaces' => 'id', ), 'CASCADE', null);
        $this->addRelation('CommonEchangesInterfacesRelatedById', 'Application\\Propel\\Mpe\\CommonEchangesInterfaces', RelationMap::ONE_TO_MANY, array('id' => 'id_echanges_interfaces', ), 'CASCADE', null, 'CommonEchangesInterfacessRelatedById');
    } // buildRelations()

} // CommonEchangesInterfacesTableMap
