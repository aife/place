<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'referentiel_sous_type_parapheur' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonReferentielSousTypeParapheurTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonReferentielSousTypeParapheurTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('referentiel_sous_type_parapheur');
        $this->setPhpName('CommonReferentielSousTypeParapheur');
        $this->setClassname('Application\\Propel\\Mpe\\CommonReferentielSousTypeParapheur');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('code', 'Code', 'VARCHAR', true, 100, null);
        $this->addColumn('label', 'Label', 'VARCHAR', true, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEchangeDoc', 'Application\\Propel\\Mpe\\CommonEchangeDoc', RelationMap::ONE_TO_MANY, array('id' => 'referentiel_sous_type_parapheur_id', ), null, null, 'CommonEchangeDocs');
    } // buildRelations()

} // CommonReferentielSousTypeParapheurTableMap
