<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'dossier_volumineux' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonDossierVolumineuxTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonDossierVolumineuxTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('dossier_volumineux');
        $this->setPhpName('CommonDossierVolumineux');
        $this->setClassname('Application\\Propel\\Mpe\\CommonDossierVolumineux');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('uuid_reference', 'UuidReference', 'VARCHAR', true, 30, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', false, 30, null);
        $this->addColumn('taille', 'Taille', 'BIGINT', false, null, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', true, null, null);
        $this->addColumn('statut', 'Statut', 'VARCHAR', true, 30, 'init');
        $this->addForeignKey('id_agent', 'IdAgent', 'INTEGER', 'Agent', 'id', false, null, null);
        $this->addColumn('old_id_inscrit', 'OldIdInscrit', 'INTEGER', false, null, null);
        $this->addForeignKey('id_blob_descripteur', 'IdBlobDescripteur', 'INTEGER', 'blob_file', 'id', false, null, null);
        $this->addColumn('uuid_technique', 'UuidTechnique', 'VARCHAR', true, 256, null);
        $this->addColumn('actif', 'Actif', 'BOOLEAN', false, 1, false);
        $this->addForeignKey('id_blob_logfile', 'IdBlobLogfile', 'INTEGER', 'blob_file', 'id', false, null, null);
        $this->addColumn('date_modification', 'DateModification', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('id_inscrit', 'IdInscrit', 'BIGINT', 'Inscrit', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonInscrit', 'Application\\Propel\\Mpe\\CommonInscrit', RelationMap::MANY_TO_ONE, array('id_inscrit' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonBlobFileRelatedByIdBlobDescripteur', 'Application\\Propel\\Mpe\\CommonBlobFile', RelationMap::MANY_TO_ONE, array('id_blob_descripteur' => 'id', ), null, null);
        $this->addRelation('CommonBlobFileRelatedByIdBlobLogfile', 'Application\\Propel\\Mpe\\CommonBlobFile', RelationMap::MANY_TO_ONE, array('id_blob_logfile' => 'id', ), null, null);
        $this->addRelation('CommonAgent', 'Application\\Propel\\Mpe\\CommonAgent', RelationMap::MANY_TO_ONE, array('id_agent' => 'id', ), null, null);
        $this->addRelation('CommonEchange', 'Application\\Propel\\Mpe\\CommonEchange', RelationMap::ONE_TO_MANY, array('id' => 'id_dossier_volumineux', ), null, null, 'CommonEchanges');
        $this->addRelation('CommonEnveloppe', 'Application\\Propel\\Mpe\\CommonEnveloppe', RelationMap::ONE_TO_MANY, array('id' => 'id_dossier_volumineux', ), null, null, 'CommonEnveloppes');
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::ONE_TO_MANY, array('id' => 'id_dossier_volumineux', ), null, null, 'CommonConsultations');
    } // buildRelations()

} // CommonDossierVolumineuxTableMap
