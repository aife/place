<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'modification_contrat' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonModificationContratTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonModificationContratTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('modification_contrat');
        $this->setPhpName('CommonModificationContrat');
        $this->setClassname('Application\\Propel\\Mpe\\CommonModificationContrat');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('id_contrat_titulaire', 'IdContratTitulaire', 'INTEGER', 't_contrat_titulaire', 'id_contrat_titulaire', true, null, null);
        $this->addColumn('num_ordre', 'NumOrdre', 'INTEGER', true, null, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', true, null, null);
        $this->addColumn('date_modification', 'DateModification', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('id_agent', 'IdAgent', 'INTEGER', 'Agent', 'id', true, null, null);
        $this->addColumn('objet_modification', 'ObjetModification', 'VARCHAR', true, 255, null);
        $this->addColumn('date_signature', 'DateSignature', 'TIMESTAMP', true, null, null);
        $this->addColumn('montant', 'Montant', 'VARCHAR', false, 256, null);
        $this->addForeignKey('id_etablissement', 'IdEtablissement', 'INTEGER', 't_etablissement', 'id_etablissement', false, null, null);
        $this->addColumn('duree_marche', 'DureeMarche', 'INTEGER', false, null, null);
        $this->addColumn('statut_publication_sn', 'StatutPublicationSn', 'INTEGER', true, 2, 0);
        $this->addColumn('date_publication_sn', 'DatePublicationSn', 'TIMESTAMP', false, null, null);
        $this->addColumn('erreur_sn', 'ErreurSn', 'VARCHAR', false, 255, null);
        $this->addColumn('date_modification_sn', 'DateModificationSn', 'TIMESTAMP', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonAgent', 'Application\\Propel\\Mpe\\CommonAgent', RelationMap::MANY_TO_ONE, array('id_agent' => 'id', ), null, null);
        $this->addRelation('CommonTContratTitulaire', 'Application\\Propel\\Mpe\\CommonTContratTitulaire', RelationMap::MANY_TO_ONE, array('id_contrat_titulaire' => 'id_contrat_titulaire', ), null, null);
        $this->addRelation('CommonTEtablissement', 'Application\\Propel\\Mpe\\CommonTEtablissement', RelationMap::MANY_TO_ONE, array('id_etablissement' => 'id_etablissement', ), null, null);
    } // buildRelations()

} // CommonModificationContratTableMap
