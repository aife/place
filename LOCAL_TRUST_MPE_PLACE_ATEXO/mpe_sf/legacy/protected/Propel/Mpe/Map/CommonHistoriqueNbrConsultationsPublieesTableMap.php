<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'HistoriqueNbrConsultationsPubliees' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonHistoriqueNbrConsultationsPublieesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonHistoriqueNbrConsultationsPublieesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('HistoriqueNbrConsultationsPubliees');
        $this->setPhpName('CommonHistoriqueNbrConsultationsPubliees');
        $this->setClassname('Application\\Propel\\Mpe\\CommonHistoriqueNbrConsultationsPubliees');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_historique_nbr_cons', 'IdHistoriqueNbrCons', 'INTEGER', true, null, null);
        $this->addColumn('date_publication', 'DatePublication', 'DATE', true, null, '0000-00-00');
        $this->addColumn('nbr_consultations_publiees', 'NbrConsultationsPubliees', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonHistoriqueNbrConsultationsPublieesTableMap
