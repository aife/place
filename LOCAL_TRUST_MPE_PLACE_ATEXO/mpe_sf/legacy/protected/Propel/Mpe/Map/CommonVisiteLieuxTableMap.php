<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'visite_lieux' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonVisiteLieuxTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonVisiteLieuxTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('visite_lieux');
        $this->setPhpName('CommonVisiteLieux');
        $this->setClassname('Application\\Propel\\Mpe\\CommonVisiteLieux');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 50, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 30, '');
        $this->addColumn('reference', 'Reference', 'INTEGER', true, 50, 0);
        $this->addColumn('adresse', 'Adresse', 'VARCHAR', true, 250, '');
        $this->addColumn('id_tr_adresse', 'IdTrAdresse', 'INTEGER', false, null, null);
        $this->addColumn('date', 'Date', 'VARCHAR', true, 50, '');
        $this->addColumn('lot', 'Lot', 'CHAR', true, null, '');
        $this->addColumn('adresse_fr', 'AdresseFr', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_en', 'AdresseEn', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_es', 'AdresseEs', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_su', 'AdresseSu', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_du', 'AdresseDu', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_cz', 'AdresseCz', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_ar', 'AdresseAr', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_it', 'AdresseIt', 'VARCHAR', false, 255, null);
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonVisiteLieuxTableMap
