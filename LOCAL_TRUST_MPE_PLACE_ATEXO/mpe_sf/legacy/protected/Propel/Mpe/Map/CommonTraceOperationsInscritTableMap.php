<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'trace_operations_inscrit' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTraceOperationsInscritTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTraceOperationsInscritTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('trace_operations_inscrit');
        $this->setPhpName('CommonTraceOperationsInscrit');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTraceOperationsInscrit');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_trace', 'IdTrace', 'INTEGER', true, null, null);
        $this->addColumn('old_id_inscrit', 'OldIdInscrit', 'INTEGER', false, null, null);
        $this->addColumn('id_entreprise', 'IdEntreprise', 'INTEGER', true, null, 0);
        $this->addColumn('addr_ip', 'AddrIp', 'VARCHAR', true, 15, null);
        $this->addColumn('date', 'Date', 'DATE', true, null, null);
        $this->addColumn('operations', 'Operations', 'LONGVARCHAR', true, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', false, 30, null);
        $this->addColumn('ref_consultation', 'RefConsultation', 'VARCHAR', false, 30, null);
        $this->addColumn('afficher', 'Afficher', 'CHAR', true, null, '0');
        $this->getColumn('afficher', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('consultation_id', 'ConsultationId', 'INTEGER', false, null, null);
        $this->addForeignKey('id_inscrit', 'IdInscrit', 'BIGINT', 'Inscrit', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonInscrit', 'Application\\Propel\\Mpe\\CommonInscrit', RelationMap::MANY_TO_ONE, array('id_inscrit' => 'id', ), null, 'CASCADE');
    } // buildRelations()

} // CommonTraceOperationsInscritTableMap
