<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_support_annonce_consultation' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTSupportAnnonceConsultationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTSupportAnnonceConsultationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_support_annonce_consultation');
        $this->setPhpName('CommonTSupportAnnonceConsultation');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTSupportAnnonceConsultation');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('id_support', 'IdSupport', 'INTEGER', 't_support_publication', 'id', true, null, null);
        $this->addForeignKey('id_annonce_cons', 'IdAnnonceCons', 'INTEGER', 't_annonce_consultation', 'id', true, null, null);
        $this->addColumn('prenom_nom_agent_createur', 'PrenomNomAgentCreateur', 'VARCHAR', true, 255, null);
        $this->addColumn('id_agent', 'IdAgent', 'INTEGER', true, null, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', true, null, null);
        $this->addColumn('statut', 'Statut', 'VARCHAR', true, 255, null);
        $this->addColumn('date_statut', 'DateStatut', 'TIMESTAMP', true, null, null);
        $this->addColumn('numero_avis', 'NumeroAvis', 'VARCHAR', false, 255, null);
        $this->addColumn('message_statut', 'MessageStatut', 'VARCHAR', false, 255, null);
        $this->addColumn('lien_publication', 'LienPublication', 'LONGVARCHAR', false, null, null);
        $this->addColumn('date_envoi_support', 'DateEnvoiSupport', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_publication_support', 'DatePublicationSupport', 'TIMESTAMP', false, null, null);
        $this->addColumn('numero_avis_parent', 'NumeroAvisParent', 'VARCHAR', false, 255, null);
        $this->addColumn('id_offre', 'IdOffre', 'INTEGER', false, null, null);
        $this->addColumn('message_acheteur', 'MessageAcheteur', 'VARCHAR', false, 255, null);
        $this->addColumn('departements_parution_annonce', 'DepartementsParutionAnnonce', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTAnnonceConsultation', 'Application\\Propel\\Mpe\\CommonTAnnonceConsultation', RelationMap::MANY_TO_ONE, array('id_annonce_cons' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonTSupportPublication', 'Application\\Propel\\Mpe\\CommonTSupportPublication', RelationMap::MANY_TO_ONE, array('id_support' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonTSupportAnnonceConsultationTableMap
