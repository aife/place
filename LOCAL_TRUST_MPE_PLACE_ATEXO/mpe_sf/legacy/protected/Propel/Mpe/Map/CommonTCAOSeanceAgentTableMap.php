<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_CAO_Seance_Agent' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTCAOSeanceAgentTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTCAOSeanceAgentTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_CAO_Seance_Agent');
        $this->setPhpName('CommonTCAOSeanceAgent');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTCAOSeanceAgent');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_seance_agent', 'IdSeanceAgent', 'BIGINT', true, null, null);
        $this->addForeignKey('id_seance', 'IdSeance', 'BIGINT', 't_CAO_Seance', 'id_seance', true, null, null);
        $this->addForeignKey('id_agent', 'IdAgent', 'INTEGER', 'Agent', 'id', true, null, null);
        $this->addForeignPrimaryKey('organisme', 'Organisme', 'VARCHAR' , 'Organisme', 'acronyme', true, 30, '');
        $this->addColumn('id_ref_val_type_voix', 'IdRefValTypeVoix', 'INTEGER', true, null, null);
        $this->addColumn('type_compte', 'TypeCompte', 'TINYINT', true, null, 0);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonAgent', 'Application\\Propel\\Mpe\\CommonAgent', RelationMap::MANY_TO_ONE, array('id_agent' => 'id', ), null, null);
        $this->addRelation('CommonTCAOSeance', 'Application\\Propel\\Mpe\\CommonTCAOSeance', RelationMap::MANY_TO_ONE, array('id_seance' => 'id_seance', ), null, null);
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), null, null);
    } // buildRelations()

} // CommonTCAOSeanceAgentTableMap
