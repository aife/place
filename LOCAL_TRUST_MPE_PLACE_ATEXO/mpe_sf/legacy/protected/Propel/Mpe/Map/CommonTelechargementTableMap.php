<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Telechargement' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTelechargementTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTelechargementTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Telechargement');
        $this->setPhpName('CommonTelechargement');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTelechargement');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 22, null);
        $this->addForeignPrimaryKey('organisme', 'Organisme', 'VARCHAR' , 'Organisme', 'acronyme', true, 30, '');
        $this->addColumn('consultation_ref', 'ConsultationRef', 'INTEGER', false, null, null);
        $this->addColumn('datetelechargement', 'Datetelechargement', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('tirage_plan', 'TiragePlan', 'INTEGER', true, null, 0);
        $this->addColumn('old_id_inscrit', 'OldIdInscrit', 'INTEGER', false, null, null);
        $this->addColumn('id_entreprise', 'IdEntreprise', 'INTEGER', false, null, 0);
        $this->addColumn('support', 'Support', 'CHAR', true, null, '1');
        $this->getColumn('support', false)->setValueSet(array (
  0 => '1',
  1 => '2',
  2 => '3',
));
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 100, '');
        $this->addColumn('adresse', 'Adresse', 'VARCHAR', true, 100, '');
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, '');
        $this->addColumn('entreprise', 'Entreprise', 'VARCHAR', true, 100, '');
        $this->addColumn('codepostal', 'Codepostal', 'VARCHAR', true, 5, '0');
        $this->addColumn('ville', 'Ville', 'VARCHAR', true, 50, '');
        $this->addColumn('pays', 'Pays', 'VARCHAR', false, 50, null);
        $this->addColumn('telephone', 'Telephone', 'VARCHAR', true, 20, '');
        $this->addColumn('siret', 'Siret', 'VARCHAR', true, 14, '');
        $this->addColumn('fax', 'Fax', 'VARCHAR', true, 30, '');
        $this->addColumn('lots', 'Lots', 'VARCHAR', false, 255, '');
        $this->addColumn('sirenEtranger', 'Sirenetranger', 'VARCHAR', false, 20, '0');
        $this->addColumn('adresse2', 'Adresse2', 'VARCHAR', true, 80, '0');
        $this->addColumn('prenom', 'Prenom', 'VARCHAR', false, 11, '');
        $this->addColumn('noms_fichiers_dce', 'NomsFichiersDce', 'LONGVARCHAR', false, null, null);
        $this->addColumn('Observation', 'Observation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('acronyme_pays', 'AcronymePays', 'VARCHAR', false, 10, null);
        $this->addColumn('poids_telechargement', 'PoidsTelechargement', 'INTEGER', true, null, 0);
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        $this->addForeignKey('plateforme_virtuelle_id', 'PlateformeVirtuelleId', 'INTEGER', 'plateforme_virtuelle', 'id', false, null, null);
        $this->addForeignKey('id_inscrit', 'IdInscrit', 'BIGINT', 'Inscrit', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonInscrit', 'Application\\Propel\\Mpe\\CommonInscrit', RelationMap::MANY_TO_ONE, array('id_inscrit' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), null, 'CASCADE');
        $this->addRelation('CommonPlateformeVirtuelle', 'Application\\Propel\\Mpe\\CommonPlateformeVirtuelle', RelationMap::MANY_TO_ONE, array('plateforme_virtuelle_id' => 'id', ), null, null);
    } // buildRelations()

} // CommonTelechargementTableMap
