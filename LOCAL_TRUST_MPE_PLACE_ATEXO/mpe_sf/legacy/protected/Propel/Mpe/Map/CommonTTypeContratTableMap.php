<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_type_contrat' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTTypeContratTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTTypeContratTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_type_contrat');
        $this->setPhpName('CommonTTypeContrat');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTTypeContrat');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_type_contrat', 'IdTypeContrat', 'INTEGER', true, null, null);
        $this->addColumn('libelle_type_contrat', 'LibelleTypeContrat', 'VARCHAR', true, 255, null);
        $this->addColumn('abreviation_type_contrat', 'AbreviationTypeContrat', 'CHAR', false, 2, null);
        $this->addColumn('type_contrat_statistique', 'TypeContratStatistique', 'SMALLINT', true, null, 0);
        $this->addColumn('multi', 'Multi', 'CHAR', true, null, '0');
        $this->getColumn('multi', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('accord_cadre_sad', 'AccordCadreSad', 'CHAR', true, null, '0');
        $this->getColumn('accord_cadre_sad', false)->setValueSet(array (
  0 => '0',
  1 => '1',
  2 => '2',
));
        $this->addColumn('avec_chapeau', 'AvecChapeau', 'CHAR', true, null, '0');
        $this->getColumn('avec_chapeau', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('avec_montant', 'AvecMontant', 'CHAR', true, null, '0');
        $this->getColumn('avec_montant', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('mode_echange_chorus', 'ModeEchangeChorus', 'CHAR', true, null, '0');
        $this->addColumn('marche_subsequent', 'MarcheSubsequent', 'CHAR', true, null, '0');
        $this->addColumn('avec_montant_max', 'AvecMontantMax', 'CHAR', true, null, '0');
        $this->getColumn('avec_montant_max', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('ordre_affichage', 'OrdreAffichage', 'INTEGER', false, null, null);
        $this->addColumn('article_133', 'Article133', 'CHAR', true, null, '0');
        $this->getColumn('article_133', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('code_dume', 'CodeDume', 'VARCHAR', false, 50, null);
        $this->addColumn('concession', 'Concession', 'BOOLEAN', true, 1, false);
        $this->addColumn('id_externe', 'IdExterne', 'VARCHAR', false, 256, '');
        $this->addForeignKey('technique_achat', 'TechniqueAchat', 'INTEGER', 'technique_achat', 'id_technique_achat', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTechniqueAchat', 'Application\\Propel\\Mpe\\CommonTechniqueAchat', RelationMap::MANY_TO_ONE, array('technique_achat' => 'id_technique_achat', ), null, 'CASCADE');
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::ONE_TO_MANY, array('id_type_contrat' => 'type_marche', ), null, 'CASCADE', 'CommonConsultations');
        $this->addRelation('CommonTContratTitulaire', 'Application\\Propel\\Mpe\\CommonTContratTitulaire', RelationMap::ONE_TO_MANY, array('id_type_contrat' => 'id_type_contrat', ), null, 'CASCADE', 'CommonTContratTitulaires');
    } // buildRelations()

} // CommonTTypeContratTableMap
