<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'sso_entreprise' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonSsoEntrepriseTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonSsoEntrepriseTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sso_entreprise');
        $this->setPhpName('CommonSsoEntreprise');
        $this->setClassname('Application\\Propel\\Mpe\\CommonSsoEntreprise');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id_sso', 'IdSso', 'VARCHAR', true, 40, '');
        $this->addColumn('old_id_inscrit', 'OldIdInscrit', 'INTEGER', false, null, null);
        $this->addColumn('date_connexion', 'DateConnexion', 'VARCHAR', true, 20, '');
        $this->addColumn('date_last_request', 'DateLastRequest', 'VARCHAR', true, 20, '');
        $this->addColumn('login', 'Login', 'VARCHAR', false, 255, null);
        $this->addForeignKey('id_inscrit', 'IdInscrit', 'BIGINT', 'Inscrit', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonInscrit', 'Application\\Propel\\Mpe\\CommonInscrit', RelationMap::MANY_TO_ONE, array('id_inscrit' => 'id', ), null, 'CASCADE');
    } // buildRelations()

} // CommonSsoEntrepriseTableMap
