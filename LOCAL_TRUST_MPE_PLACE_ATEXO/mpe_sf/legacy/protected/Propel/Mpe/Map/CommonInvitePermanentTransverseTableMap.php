<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'invite_permanent_transverse' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonInvitePermanentTransverseTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonInvitePermanentTransverseTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('invite_permanent_transverse');
        $this->setPhpName('CommonInvitePermanentTransverse');
        $this->setClassname('Application\\Propel\\Mpe\\CommonInvitePermanentTransverse');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('old_service_id', 'OldServiceId', 'INTEGER', false, null, null);
        $this->addForeignKey('agent_id', 'AgentId', 'INTEGER', 'Agent', 'id', true, null, null);
        $this->addForeignKey('acronyme', 'Acronyme', 'VARCHAR', 'Organisme', 'acronyme', true, 30, null);
        $this->addForeignKey('service_id', 'ServiceId', 'BIGINT', 'Service', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonService', 'Application\\Propel\\Mpe\\CommonService', RelationMap::MANY_TO_ONE, array('service_id' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('acronyme' => 'acronyme', ), null, 'CASCADE');
        $this->addRelation('CommonAgent', 'Application\\Propel\\Mpe\\CommonAgent', RelationMap::MANY_TO_ONE, array('agent_id' => 'id', ), null, 'CASCADE');
    } // buildRelations()

} // CommonInvitePermanentTransverseTableMap
