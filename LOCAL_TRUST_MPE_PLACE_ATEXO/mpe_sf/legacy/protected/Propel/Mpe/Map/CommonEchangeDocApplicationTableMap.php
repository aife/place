<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'echange_doc_application' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEchangeDocApplicationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEchangeDocApplicationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('echange_doc_application');
        $this->setPhpName('CommonEchangeDocApplication');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEchangeDocApplication');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('code', 'Code', 'VARCHAR', true, 255, null);
        $this->addColumn('libelle', 'Libelle', 'VARCHAR', true, 255, null);
        $this->addColumn('flux_actes', 'FluxActes', 'BOOLEAN', false, 1, false);
        $this->addColumn('primo_signature', 'PrimoSignature', 'BOOLEAN', false, 1, false);
        $this->addColumn('envoi_dic', 'EnvoiDic', 'BOOLEAN', true, 1, false);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEchangeDocApplicationClient', 'Application\\Propel\\Mpe\\CommonEchangeDocApplicationClient', RelationMap::ONE_TO_MANY, array('id' => 'echange_doc_application_id', ), null, null, 'CommonEchangeDocApplicationClients');
    } // buildRelations()

} // CommonEchangeDocApplicationTableMap
