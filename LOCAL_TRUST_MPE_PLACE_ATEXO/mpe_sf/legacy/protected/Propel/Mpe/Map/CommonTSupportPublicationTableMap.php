<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_support_publication' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTSupportPublicationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTSupportPublicationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_support_publication');
        $this->setPhpName('CommonTSupportPublication');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTSupportPublication');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('image_logo', 'ImageLogo', 'VARCHAR', false, 255, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 255, null);
        $this->addColumn('visible', 'Visible', 'CHAR', true, null, '1');
        $this->getColumn('visible', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('ordre', 'Ordre', 'INTEGER', false, null, null);
        $this->addColumn('default_value', 'DefaultValue', 'CHAR', true, null, '0');
        $this->getColumn('default_value', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('code', 'Code', 'VARCHAR', false, 255, null);
        $this->addColumn('actif', 'Actif', 'CHAR', true, null, '0');
        $this->getColumn('actif', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('url', 'Url', 'VARCHAR', false, 255, null);
        $this->addColumn('groupe', 'Groupe', 'VARCHAR', false, 50, null);
        $this->addColumn('tag_debut_fin_groupe', 'TagDebutFinGroupe', 'VARCHAR', false, 50, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('nbre_total_groupe', 'NbreTotalGroupe', 'INTEGER', true, null, 1);
        $this->addColumn('lieux_execution', 'LieuxExecution', 'LONGVARCHAR', false, null, null);
        $this->addColumn('detail_info', 'DetailInfo', 'LONGVARCHAR', false, null, null);
        $this->addColumn('type_info', 'TypeInfo', 'CHAR', false, null, null);
        $this->addColumn('affichage_infos', 'AffichageInfos', 'CHAR', false, null, '0');
        $this->getColumn('affichage_infos', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('affichage_message_support', 'AffichageMessageSupport', 'CHAR', false, null, '0');
        $this->getColumn('affichage_message_support', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('selection_departements_parution', 'SelectionDepartementsParution', 'CHAR', false, null, '0');
        $this->getColumn('selection_departements_parution', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('departements_parution', 'DepartementsParution', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTOffreSupportPublicite', 'Application\\Propel\\Mpe\\CommonTOffreSupportPublicite', RelationMap::ONE_TO_MANY, array('id' => 'id_support', ), 'CASCADE', 'CASCADE', 'CommonTOffreSupportPublicites');
        $this->addRelation('CommonTSupportAnnonceConsultation', 'Application\\Propel\\Mpe\\CommonTSupportAnnonceConsultation', RelationMap::ONE_TO_MANY, array('id' => 'id_support', ), 'CASCADE', 'CASCADE', 'CommonTSupportAnnonceConsultations');
    } // buildRelations()

} // CommonTSupportPublicationTableMap
