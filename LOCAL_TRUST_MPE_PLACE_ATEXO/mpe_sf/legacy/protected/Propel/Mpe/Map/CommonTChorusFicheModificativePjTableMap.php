<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_chorus_fiche_modificative_pj' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTChorusFicheModificativePjTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTChorusFicheModificativePjTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_chorus_fiche_modificative_pj');
        $this->setPhpName('CommonTChorusFicheModificativePj');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTChorusFicheModificativePj');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_fiche_modificative_pj', 'IdFicheModificativePj', 'INTEGER', true, 20, null);
        $this->addForeignKey('id_fiche_modificative', 'IdFicheModificative', 'INTEGER', 't_chorus_fiche_modificative', 'id_fiche_modificative', true, null, 0);
        $this->addColumn('nom_fichier', 'NomFichier', 'VARCHAR', true, 300, '');
        $this->addColumn('fichier', 'Fichier', 'VARCHAR', true, 20, '');
        $this->addColumn('horodatage', 'Horodatage', 'BLOB', true, null, null);
        $this->addColumn('untrusteddate', 'Untrusteddate', 'VARCHAR', true, 20, '');
        $this->addColumn('taille', 'Taille', 'INTEGER', true, 20, 0);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTChorusFicheModificative', 'Application\\Propel\\Mpe\\CommonTChorusFicheModificative', RelationMap::MANY_TO_ONE, array('id_fiche_modificative' => 'id_fiche_modificative', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonTChorusFicheModificativePjTableMap
