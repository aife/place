<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_type_annonce_consultation' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTTypeAnnonceConsultationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTTypeAnnonceConsultationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_type_annonce_consultation');
        $this->setPhpName('CommonTTypeAnnonceConsultation');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTTypeAnnonceConsultation');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('libelle', 'Libelle', 'VARCHAR', true, 255, null);
        $this->addColumn('visible', 'Visible', 'CHAR', true, null, '1');
        $this->getColumn('visible', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTAnnonceConsultation', 'Application\\Propel\\Mpe\\CommonTAnnonceConsultation', RelationMap::ONE_TO_MANY, array('id' => 'id_type_annonce', ), 'CASCADE', 'CASCADE', 'CommonTAnnonceConsultations');
    } // buildRelations()

} // CommonTTypeAnnonceConsultationTableMap
