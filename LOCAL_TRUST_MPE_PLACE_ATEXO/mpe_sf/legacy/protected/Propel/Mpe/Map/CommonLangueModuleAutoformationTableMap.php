<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'langue_module_autoformation' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonLangueModuleAutoformationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonLangueModuleAutoformationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('langue_module_autoformation');
        $this->setPhpName('CommonLangueModuleAutoformation');
        $this->setClassname('Application\\Propel\\Mpe\\CommonLangueModuleAutoformation');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('langue_id', 'LangueId', 'INTEGER', 'Langue', 'id_langue', true, null, null);
        $this->addForeignKey('module_autoformation_id', 'ModuleAutoformationId', 'INTEGER', 'module_autoformation', 'id', false, null, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 100, null);
        $this->addColumn('description', 'Description', 'VARCHAR', true, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonLangue', 'Application\\Propel\\Mpe\\CommonLangue', RelationMap::MANY_TO_ONE, array('langue_id' => 'id_langue', ), null, null);
        $this->addRelation('CommonModuleAutoformation', 'Application\\Propel\\Mpe\\CommonModuleAutoformation', RelationMap::MANY_TO_ONE, array('module_autoformation_id' => 'id', ), null, null);
    } // buildRelations()

} // CommonLangueModuleAutoformationTableMap
