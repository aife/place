<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'echange_doc' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEchangeDocTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEchangeDocTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('echange_doc');
        $this->setPhpName('CommonEchangeDoc');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEchangeDoc');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('echange_doc_application_client_id', 'EchangeDocApplicationClientId', 'INTEGER', 'echange_doc_application_client', 'id', false, null, null);
        $this->addColumn('objet', 'Objet', 'VARCHAR', true, 100, null);
        $this->addColumn('description', 'Description', 'VARCHAR', true, 500, null);
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        $this->addForeignKey('agent_id', 'AgentId', 'INTEGER', 'Agent', 'id', false, null, null);
        $this->addColumn('statut', 'Statut', 'VARCHAR', false, 50, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('cheminement_signature', 'CheminementSignature', 'BOOLEAN', false, 1, false);
        $this->addColumn('cheminement_ged', 'CheminementGed', 'BOOLEAN', false, 1, false);
        $this->addColumn('cheminement_sae', 'CheminementSae', 'BOOLEAN', false, 1, false);
        $this->addColumn('cheminement_tdt', 'CheminementTdt', 'BOOLEAN', false, 1, false);
        $this->addForeignKey('id_contrat_titulaire', 'IdContratTitulaire', 'INTEGER', 't_contrat_titulaire', 'id_contrat_titulaire', false, null, null);
        $this->addForeignKey('referentiel_sous_type_parapheur_id', 'ReferentielSousTypeParapheurId', 'INTEGER', 'referentiel_sous_type_parapheur', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonReferentielSousTypeParapheur', 'Application\\Propel\\Mpe\\CommonReferentielSousTypeParapheur', RelationMap::MANY_TO_ONE, array('referentiel_sous_type_parapheur_id' => 'id', ), null, null);
        $this->addRelation('CommonTContratTitulaire', 'Application\\Propel\\Mpe\\CommonTContratTitulaire', RelationMap::MANY_TO_ONE, array('id_contrat_titulaire' => 'id_contrat_titulaire', ), null, null);
        $this->addRelation('CommonAgent', 'Application\\Propel\\Mpe\\CommonAgent', RelationMap::MANY_TO_ONE, array('agent_id' => 'id', ), null, null);
        $this->addRelation('CommonEchangeDocApplicationClient', 'Application\\Propel\\Mpe\\CommonEchangeDocApplicationClient', RelationMap::MANY_TO_ONE, array('echange_doc_application_client_id' => 'id', ), null, null);
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), null, null);
        $this->addRelation('CommonEchangeDocBlob', 'Application\\Propel\\Mpe\\CommonEchangeDocBlob', RelationMap::ONE_TO_MANY, array('id' => 'echange_doc_id', ), null, null, 'CommonEchangeDocBlobs');
        $this->addRelation('CommonEchangeDocHistorique', 'Application\\Propel\\Mpe\\CommonEchangeDocHistorique', RelationMap::ONE_TO_MANY, array('id' => 'echange_doc_id', ), null, null, 'CommonEchangeDocHistoriques');
    } // buildRelations()

} // CommonEchangeDocTableMap
