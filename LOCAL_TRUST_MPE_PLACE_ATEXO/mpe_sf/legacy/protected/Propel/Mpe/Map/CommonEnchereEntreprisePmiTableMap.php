<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'EnchereEntreprisePmi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEnchereEntreprisePmiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEnchereEntreprisePmiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('EnchereEntreprisePmi');
        $this->setPhpName('CommonEnchereEntreprisePmi');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEnchereEntreprisePmi');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('old_id', 'OldId', 'INTEGER', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', true, 30, null);
        $this->addColumn('old_id_enchere', 'OldIdEnchere', 'INTEGER', false, null, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 256, null);
        $this->addColumn('numeroAnonyme', 'Numeroanonyme', 'INTEGER', false, null, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 256, null);
        $this->addColumn('mdp', 'Mdp', 'VARCHAR', true, 256, null);
        $this->addColumn('noteTechnique', 'Notetechnique', 'DOUBLE', false, null, null);
        $this->addColumn('idEntreprise', 'Identreprise', 'INTEGER', false, 10, null);
        $this->addColumn('datePing', 'Dateping', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('nomAgentConnecte', 'Nomagentconnecte', 'VARCHAR', false, 255, null);
        $this->addForeignKey('idEnchere', 'Idenchere', 'BIGINT', 'EncherePmi', 'id', false, null, null);
        $this->addPrimaryKey('id', 'Id', 'BIGINT', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEncherePmi', 'Application\\Propel\\Mpe\\CommonEncherePmi', RelationMap::MANY_TO_ONE, array('idEnchere' => 'id', ), null, null);
        $this->addRelation('CommonEnchereOffre', 'Application\\Propel\\Mpe\\CommonEnchereOffre', RelationMap::ONE_TO_MANY, array('id' => 'idEnchereEntreprise', ), null, null, 'CommonEnchereOffres');
        $this->addRelation('CommonEnchereValeursInitiales', 'Application\\Propel\\Mpe\\CommonEnchereValeursInitiales', RelationMap::ONE_TO_MANY, array('id' => 'idEnchereEntreprise', ), null, 'CASCADE', 'CommonEnchereValeursInitialess');
    } // buildRelations()

} // CommonEnchereEntreprisePmiTableMap
