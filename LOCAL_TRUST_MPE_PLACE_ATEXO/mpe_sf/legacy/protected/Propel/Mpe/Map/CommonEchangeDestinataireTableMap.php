<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'EchangeDestinataire' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEchangeDestinataireTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEchangeDestinataireTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('EchangeDestinataire');
        $this->setPhpName('CommonEchangeDestinataire');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEchangeDestinataire');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 30, null);
        $this->addColumn('old_id_echange', 'OldIdEchange', 'INTEGER', false, null, null);
        $this->addColumn('mail_destinataire', 'MailDestinataire', 'VARCHAR', true, 255, '');
        $this->addColumn('ar', 'Ar', 'CHAR', true, null, '0');
        $this->getColumn('ar', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('date_ar', 'DateAr', 'VARCHAR', true, 25, '');
        $this->addColumn('uid', 'Uid', 'VARCHAR', true, 32, '');
        $this->addColumn('type_ar', 'TypeAr', 'INTEGER', true, null, 0);
        $this->addColumn('old_id_inscrit', 'OldIdInscrit', 'INTEGER', false, null, null);
        $this->addColumn('id_entreprise', 'IdEntreprise', 'INTEGER', true, null, 0);
        $this->addForeignKey('id_echange', 'IdEchange', 'BIGINT', 'Echange', 'id', false, null, null);
        $this->addForeignKey('id_inscrit', 'IdInscrit', 'BIGINT', 'Inscrit', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEchange', 'Application\\Propel\\Mpe\\CommonEchange', RelationMap::MANY_TO_ONE, array('id_echange' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonInscrit', 'Application\\Propel\\Mpe\\CommonInscrit', RelationMap::MANY_TO_ONE, array('id_inscrit' => 'id', ), null, 'CASCADE');
    } // buildRelations()

} // CommonEchangeDestinataireTableMap
