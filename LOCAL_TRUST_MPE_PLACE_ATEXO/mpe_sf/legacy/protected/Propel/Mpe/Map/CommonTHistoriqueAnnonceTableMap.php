<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_historique_annonce' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTHistoriqueAnnonceTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTHistoriqueAnnonceTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_historique_annonce');
        $this->setPhpName('CommonTHistoriqueAnnonce');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTHistoriqueAnnonce');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('id_annonce', 'IdAnnonce', 'INTEGER', 't_annonce_consultation', 'id', true, null, null);
        $this->addColumn('statut', 'Statut', 'VARCHAR', true, 255, null);
        $this->addColumn('motif', 'Motif', 'VARCHAR', false, 255, null);
        $this->addColumn('date_modification', 'DateModification', 'TIMESTAMP', true, null, null);
        $this->addColumn('id_support', 'IdSupport', 'INTEGER', true, null, 0);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTAnnonceConsultation', 'Application\\Propel\\Mpe\\CommonTAnnonceConsultation', RelationMap::MANY_TO_ONE, array('id_annonce' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonTHistoriqueAnnonceTableMap
