<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'EnchereTranchesBaremeNETC' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEnchereTranchesBaremeNETCTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEnchereTranchesBaremeNETCTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('EnchereTranchesBaremeNETC');
        $this->setPhpName('CommonEnchereTranchesBaremeNETC');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEnchereTranchesBaremeNETC');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 30, '');
        $this->addColumn('old_id_enchere', 'OldIdEnchere', 'INTEGER', false, null, null);
        $this->addColumn('borneInf', 'Borneinf', 'DOUBLE', false, null, null);
        $this->addColumn('borneSup', 'Bornesup', 'DOUBLE', false, null, null);
        $this->addColumn('note', 'Note', 'DOUBLE', false, null, null);
        $this->addForeignKey('idEnchere', 'Idenchere', 'BIGINT', 'EncherePmi', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEncherePmi', 'Application\\Propel\\Mpe\\CommonEncherePmi', RelationMap::MANY_TO_ONE, array('idEnchere' => 'id', ), null, 'CASCADE');
    } // buildRelations()

} // CommonEnchereTranchesBaremeNETCTableMap
