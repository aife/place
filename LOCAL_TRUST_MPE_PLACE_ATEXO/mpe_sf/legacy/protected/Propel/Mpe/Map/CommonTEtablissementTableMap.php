<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_etablissement' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTEtablissementTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTEtablissementTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_etablissement');
        $this->setPhpName('CommonTEtablissement');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTEtablissement');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_etablissement', 'IdEtablissement', 'INTEGER', true, null, null);
        $this->addForeignKey('id_entreprise', 'IdEntreprise', 'INTEGER', 'Entreprise', 'id', true, null, null);
        $this->addColumn('code_etablissement', 'CodeEtablissement', 'CHAR', true, 5, null);
        $this->addColumn('est_siege', 'EstSiege', 'CHAR', true, null, '0');
        $this->getColumn('est_siege', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('adresse', 'Adresse', 'VARCHAR', true, 80, null);
        $this->addColumn('adresse2', 'Adresse2', 'VARCHAR', false, 80, null);
        $this->addColumn('code_postal', 'CodePostal', 'VARCHAR', true, 20, null);
        $this->addColumn('ville', 'Ville', 'VARCHAR', true, 50, null);
        $this->addColumn('pays', 'Pays', 'VARCHAR', true, 50, null);
        $this->addColumn('saisie_manuelle', 'SaisieManuelle', 'CHAR', true, null, '0');
        $this->getColumn('saisie_manuelle', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('id_initial', 'IdInitial', 'INTEGER', true, null, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', true, null, null);
        $this->addColumn('date_modification', 'DateModification', 'TIMESTAMP', true, null, null);
        $this->addColumn('date_suppression', 'DateSuppression', 'TIMESTAMP', false, null, null);
        $this->addColumn('statut_actif', 'StatutActif', 'CHAR', true, null, '1');
        $this->getColumn('statut_actif', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('inscrit_annuaire_defense', 'InscritAnnuaireDefense', 'CHAR', true, null, '0');
        $this->getColumn('inscrit_annuaire_defense', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('long', 'Long', 'DOUBLE', false, null, null);
        $this->addColumn('lat', 'Lat', 'DOUBLE', false, null, null);
        $this->addColumn('maj_long_lat', 'MajLongLat', 'TIMESTAMP', false, null, null);
        $this->addColumn('tva_intracommunautaire', 'TvaIntracommunautaire', 'VARCHAR', false, 20, null);
        $this->addColumn('etat_administratif', 'EtatAdministratif', 'CHAR', false, null, null);
        $this->addColumn('date_fermeture', 'DateFermeture', 'TIMESTAMP', false, null, null);
        $this->addColumn('id_externe', 'IdExterne', 'VARCHAR', true, 50, '0');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entreprise', 'Application\\Propel\\Mpe\\Entreprise', RelationMap::MANY_TO_ONE, array('id_entreprise' => 'id', ), null, null);
        $this->addRelation('CommonInscritRelatedByIdEtablissement', 'Application\\Propel\\Mpe\\CommonInscrit', RelationMap::ONE_TO_MANY, array('id_etablissement' => 'id_etablissement', ), null, null, 'CommonInscritsRelatedByIdEtablissement');
        $this->addRelation('CommonInscritRelatedByIdEtablissement', 'Application\\Propel\\Mpe\\CommonInscrit', RelationMap::ONE_TO_MANY, array('id_etablissement' => 'id_etablissement', ), null, null, 'CommonInscritsRelatedByIdEtablissement');
        $this->addRelation('CommonModificationContrat', 'Application\\Propel\\Mpe\\CommonModificationContrat', RelationMap::ONE_TO_MANY, array('id_etablissement' => 'id_etablissement', ), null, null, 'CommonModificationContrats');
        $this->addRelation('CommonTMembreGroupementEntreprise', 'Application\\Propel\\Mpe\\CommonTMembreGroupementEntreprise', RelationMap::ONE_TO_MANY, array('id_etablissement' => 'id_etablissement', ), null, null, 'CommonTMembreGroupementEntreprises');
    } // buildRelations()

} // CommonTEtablissementTableMap
