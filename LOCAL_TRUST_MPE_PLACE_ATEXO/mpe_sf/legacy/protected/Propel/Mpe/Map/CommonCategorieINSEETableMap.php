<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'CategorieINSEE' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonCategorieINSEETableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonCategorieINSEETableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('CategorieINSEE');
        $this->setPhpName('CommonCategorieINSEE');
        $this->setClassname('Application\\Propel\\Mpe\\CommonCategorieINSEE');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('id', 'Id', 'VARCHAR', true, 20, '');
        $this->addColumn('libelle', 'Libelle', 'VARCHAR', true, 100, '');
        $this->addColumn('libelle_fr', 'LibelleFr', 'VARCHAR', true, 100, '');
        $this->addColumn('libelle_en', 'LibelleEn', 'VARCHAR', true, 100, '');
        $this->addColumn('libelle_es', 'LibelleEs', 'VARCHAR', true, 100, '');
        $this->addColumn('libelle_su', 'LibelleSu', 'VARCHAR', true, 100, '');
        $this->addColumn('libelle_du', 'LibelleDu', 'VARCHAR', true, 100, '');
        $this->addColumn('libelle_cz', 'LibelleCz', 'VARCHAR', true, 100, '');
        $this->addColumn('libelle_ar', 'LibelleAr', 'VARCHAR', true, 100, '');
        $this->addColumn('libelle_it', 'LibelleIt', 'VARCHAR', true, 100, '');
        $this->addColumn('code', 'Code', 'VARCHAR', false, 10, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::ONE_TO_MANY, array('id' => 'categorie_insee', ), 'SET NULL', 'CASCADE', 'CommonOrganismes');
    } // buildRelations()

} // CommonCategorieINSEETableMap
