<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'interface_suivi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonInterfaceSuiviTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonInterfaceSuiviTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('interface_suivi');
        $this->setPhpName('CommonInterfaceSuivi');
        $this->setClassname('Application\\Propel\\Mpe\\CommonInterfaceSuivi');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('uuid', 'Uuid', 'CHAR', true, 36, null);
        $this->addForeignKey('id_consultation', 'IdConsultation', 'INTEGER', 'consultation', 'id', false, null, null);
        $this->addColumn('flux', 'Flux', 'VARCHAR', true, 30, null);
        $this->addColumn('action', 'Action', 'VARCHAR', true, 30, null);
        $this->addColumn('statut', 'Statut', 'INTEGER', true, null, null);
        $this->addColumn('message', 'Message', 'CLOB', false, null, null);
        $this->addColumn('id_objet_destination', 'IdObjetDestination', 'VARCHAR', false, 30, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', true, null, null);
        $this->addColumn('date_modification', 'DateModification', 'TIMESTAMP', true, null, null);
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('message_details', 'MessageDetails', 'CLOB', false, null, null);
        $this->addColumn('etape', 'Etape', 'VARCHAR', false, 30, null);
        $this->addForeignKey('annonce_id', 'AnnonceId', 'INTEGER', 'AnnonceBoamp', 'id_boamp', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonAnnonceBoamp', 'Application\\Propel\\Mpe\\CommonAnnonceBoamp', RelationMap::MANY_TO_ONE, array('annonce_id' => 'id_boamp', ), null, null);
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('id_consultation' => 'id', ), 'CASCADE', null);
    } // buildRelations()

} // CommonInterfaceSuiviTableMap
