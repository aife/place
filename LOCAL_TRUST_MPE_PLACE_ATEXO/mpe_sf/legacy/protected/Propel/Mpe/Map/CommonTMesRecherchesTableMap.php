<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'T_MesRecherches' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTMesRecherchesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTMesRecherchesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('T_MesRecherches');
        $this->setPhpName('CommonTMesRecherches');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTMesRecherches');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('id_createur', 'IdCreateur', 'INTEGER', true, null, 0);
        $this->addColumn('type_createur', 'TypeCreateur', 'VARCHAR', true, 20, null);
        $this->addColumn('denomination', 'Denomination', 'VARCHAR', true, 200, '');
        $this->addColumn('periodicite', 'Periodicite', 'CHAR', true, null, '');
        $this->addColumn('xmlCriteria', 'Xmlcriteria', 'LONGVARCHAR', false, null, null);
        $this->addColumn('categorie', 'Categorie', 'VARCHAR', false, 30, null);
        $this->addColumn('id_initial', 'IdInitial', 'INTEGER', true, null, 0);
        $this->addColumn('format', 'Format', 'VARCHAR', true, 3, '1');
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_modification', 'DateModification', 'TIMESTAMP', false, null, null);
        $this->addColumn('recherche', 'Recherche', 'CHAR', true, null, '0');
        $this->getColumn('recherche', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alerte', 'Alerte', 'CHAR', true, null, '0');
        $this->getColumn('alerte', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('type_avis', 'TypeAvis', 'INTEGER', false, 2, 3);
        $this->addForeignKey('plateforme_virtuelle_id', 'PlateformeVirtuelleId', 'INTEGER', 'plateforme_virtuelle', 'id', false, null, null);
        $this->addColumn('criteria', 'Criteria', 'CLOB', false, null, null);
        $this->addColumn('hashed_criteria', 'HashedCriteria', 'VARCHAR', false, 32, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonPlateformeVirtuelle', 'Application\\Propel\\Mpe\\CommonPlateformeVirtuelle', RelationMap::MANY_TO_ONE, array('plateforme_virtuelle_id' => 'id', ), null, null);
    } // buildRelations()

} // CommonTMesRecherchesTableMap
