<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_membre_groupement_entreprise' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTMembreGroupementEntrepriseTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTMembreGroupementEntrepriseTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_membre_groupement_entreprise');
        $this->setPhpName('CommonTMembreGroupementEntreprise');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTMembreGroupementEntreprise');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_membre_groupement_entreprise', 'IdMembreGroupementEntreprise', 'INTEGER', true, null, null);
        $this->addForeignKey('id_role_juridique', 'IdRoleJuridique', 'INTEGER', 't_role_juridique', 'id_role_juridique', false, null, null);
        $this->addForeignKey('id_groupement_entreprise', 'IdGroupementEntreprise', 'INTEGER', 't_groupement_entreprise', 'id_groupement_entreprise', false, null, null);
        $this->addForeignKey('id_entreprise', 'IdEntreprise', 'INTEGER', 'Entreprise', 'id', false, null, null);
        $this->addForeignKey('id_etablissement', 'IdEtablissement', 'INTEGER', 't_etablissement', 'id_etablissement', false, null, null);
        $this->addForeignKey('id_membre_parent', 'IdMembreParent', 'INTEGER', 't_membre_groupement_entreprise', 'id_membre_groupement_entreprise', false, null, null);
        $this->addColumn('numeroSN', 'Numerosn', 'VARCHAR', false, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entreprise', 'Application\\Propel\\Mpe\\Entreprise', RelationMap::MANY_TO_ONE, array('id_entreprise' => 'id', ), null, null);
        $this->addRelation('CommonTEtablissement', 'Application\\Propel\\Mpe\\CommonTEtablissement', RelationMap::MANY_TO_ONE, array('id_etablissement' => 'id_etablissement', ), null, null);
        $this->addRelation('CommonTGroupementEntreprise', 'Application\\Propel\\Mpe\\CommonTGroupementEntreprise', RelationMap::MANY_TO_ONE, array('id_groupement_entreprise' => 'id_groupement_entreprise', ), null, null);
        $this->addRelation('CommonTMembreGroupementEntrepriseRelatedByIdMembreParent', 'Application\\Propel\\Mpe\\CommonTMembreGroupementEntreprise', RelationMap::MANY_TO_ONE, array('id_membre_parent' => 'id_membre_groupement_entreprise', ), null, null);
        $this->addRelation('CommonTRoleJuridique', 'Application\\Propel\\Mpe\\CommonTRoleJuridique', RelationMap::MANY_TO_ONE, array('id_role_juridique' => 'id_role_juridique', ), null, null);
        $this->addRelation('CommonTMembreGroupementEntrepriseRelatedByIdMembreGroupementEntreprise', 'Application\\Propel\\Mpe\\CommonTMembreGroupementEntreprise', RelationMap::ONE_TO_MANY, array('id_membre_groupement_entreprise' => 'id_membre_parent', ), null, null, 'CommonTMembreGroupementEntreprisesRelatedByIdMembreGroupementEntreprise');
    } // buildRelations()

} // CommonTMembreGroupementEntrepriseTableMap
