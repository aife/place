<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_CAO_Intervenant_Externe' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTCAOIntervenantExterneTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTCAOIntervenantExterneTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_CAO_Intervenant_Externe');
        $this->setPhpName('CommonTCAOIntervenantExterne');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTCAOIntervenantExterne');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_intervenant_externe', 'IdIntervenantExterne', 'BIGINT', true, null, null);
        $this->addForeignPrimaryKey('organisme', 'Organisme', 'VARCHAR' , 'Organisme', 'acronyme', true, 30, null);
        $this->addColumn('id_ref_val_civilite', 'IdRefValCivilite', 'INTEGER', true, null, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 50, null);
        $this->addColumn('prenom', 'Prenom', 'VARCHAR', true, 50, null);
        $this->addColumn('organisation', 'Organisation', 'VARCHAR', true, 50, null);
        $this->addColumn('fonction', 'Fonction', 'VARCHAR', true, 100, null);
        $this->addColumn('id_ref_val_type_voix_defaut', 'IdRefValTypeVoixDefaut', 'INTEGER', true, null, null);
        $this->addColumn('adresse', 'Adresse', 'VARCHAR', true, 255, '');
        $this->addColumn('code_postal', 'CodePostal', 'CHAR', true, 15, null);
        $this->addColumn('ville', 'Ville', 'VARCHAR', true, 100, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, '');
        $this->addColumn('id_ref_val_mode_communication', 'IdRefValModeCommunication', 'TINYINT', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), null, null);
        $this->addRelation('CommonTCAOCommissionIntervenantExterne', 'Application\\Propel\\Mpe\\CommonTCAOCommissionIntervenantExterne', RelationMap::ONE_TO_MANY, array('id_intervenant_externe' => 'id_intervenant_externe', ), null, null, 'CommonTCAOCommissionIntervenantExternes');
        $this->addRelation('CommonTCAOOrdreDuJourIntervenant', 'Application\\Propel\\Mpe\\CommonTCAOOrdreDuJourIntervenant', RelationMap::ONE_TO_MANY, array('id_intervenant_externe' => 'id_intervenant_externe', ), null, null, 'CommonTCAOOrdreDuJourIntervenants');
        $this->addRelation('CommonTCAOSeanceIntervenantExterne', 'Application\\Propel\\Mpe\\CommonTCAOSeanceIntervenantExterne', RelationMap::ONE_TO_MANY, array('id_intervenant_externe' => 'id_intervenant_externe', ), null, null, 'CommonTCAOSeanceIntervenantExternes');
        $this->addRelation('CommonTCAOSeanceInvite', 'Application\\Propel\\Mpe\\CommonTCAOSeanceInvite', RelationMap::ONE_TO_MANY, array('id_intervenant_externe' => 'id_intervenant_externe', ), null, null, 'CommonTCAOSeanceInvites');
    } // buildRelations()

} // CommonTCAOIntervenantExterneTableMap
