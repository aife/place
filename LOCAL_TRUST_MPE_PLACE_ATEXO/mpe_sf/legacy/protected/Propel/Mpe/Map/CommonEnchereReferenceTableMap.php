<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'EnchereReference' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEnchereReferenceTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEnchereReferenceTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('EnchereReference');
        $this->setPhpName('CommonEnchereReference');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEnchereReference');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 30, '');
        $this->addColumn('old_id_enchere', 'OldIdEnchere', 'INTEGER', false, null, null);
        $this->addColumn('libelle', 'Libelle', 'VARCHAR', true, 255, '');
        $this->addColumn('quantite', 'Quantite', 'DOUBLE', false, null, null);
        $this->addColumn('isMontant', 'Ismontant', 'CHAR', true, null, '1');
        $this->getColumn('isMontant', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('unite', 'Unite', 'VARCHAR', false, 20, null);
        $this->addColumn('pasMin', 'Pasmin', 'DOUBLE', false, null, 0);
        $this->addColumn('pasMax', 'Pasmax', 'DOUBLE', false, null, null);
        $this->addColumn('valeurReference', 'Valeurreference', 'DOUBLE', false, null, null);
        $this->addColumn('valeurDepartCommune', 'Valeurdepartcommune', 'CHAR', true, null, '1');
        $this->getColumn('valeurDepartCommune', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('valeurDepart', 'Valeurdepart', 'DOUBLE', false, null, null);
        $this->addColumn('typeBaremeReference', 'Typebaremereference', 'CHAR', false, null, null);
        $this->getColumn('typeBaremeReference', false)->setValueSet(array (
  0 => '1',
  1 => '2',
  2 => '3',
));
        $this->addColumn('ponderationNoteReference', 'Ponderationnotereference', 'DOUBLE', false, null, 1);
        $this->addColumn('noteMaxBaremeRelatif', 'Notemaxbaremerelatif', 'DOUBLE', false, null, null);
        $this->addForeignKey('idEnchere', 'Idenchere', 'BIGINT', 'EncherePmi', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEncherePmi', 'Application\\Propel\\Mpe\\CommonEncherePmi', RelationMap::MANY_TO_ONE, array('idEnchere' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonEnchereOffreReferenceRelatedByIdencherereferenceOrganisme', 'Application\\Propel\\Mpe\\CommonEnchereOffreReference', RelationMap::ONE_TO_MANY, array('id' => 'idEnchereReference', 'organisme' => 'organisme', ), null, 'CASCADE', 'CommonEnchereOffreReferencesRelatedByIdencherereferenceOrganisme');
        $this->addRelation('CommonEnchereOffreReferenceRelatedByIdencherereference', 'Application\\Propel\\Mpe\\CommonEnchereOffreReference', RelationMap::ONE_TO_MANY, array('id' => 'idEnchereReference', ), null, 'CASCADE', 'CommonEnchereOffreReferencesRelatedByIdencherereference');
        $this->addRelation('CommonEnchereTrancheBaremeReferenceRelatedByIdreference', 'Application\\Propel\\Mpe\\CommonEnchereTrancheBaremeReference', RelationMap::ONE_TO_MANY, array('id' => 'idReference', ), null, 'CASCADE', 'CommonEnchereTrancheBaremeReferencesRelatedByIdreference');
        $this->addRelation('CommonEnchereTrancheBaremeReferenceRelatedByIdreferenceOrganisme', 'Application\\Propel\\Mpe\\CommonEnchereTrancheBaremeReference', RelationMap::ONE_TO_MANY, array('id' => 'idReference', 'organisme' => 'organisme', ), null, 'CASCADE', 'CommonEnchereTrancheBaremeReferencesRelatedByIdreferenceOrganisme');
        $this->addRelation('CommonEnchereValeursInitiales', 'Application\\Propel\\Mpe\\CommonEnchereValeursInitiales', RelationMap::ONE_TO_MANY, array('id' => 'idEnchereReference', 'organisme' => 'organisme', ), null, 'CASCADE', 'CommonEnchereValeursInitialess');
    } // buildRelations()

} // CommonEnchereReferenceTableMap
