<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_candidature' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTCandidatureTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTCandidatureTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_candidature');
        $this->setPhpName('CommonTCandidature');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTCandidature');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('ref_consultation', 'RefConsultation', 'INTEGER', false, null, null);
        $this->addForeignKey('organisme', 'Organisme', 'VARCHAR', 'Organisme', 'acronyme', true, 45, null);
        $this->addColumn('old_id_inscrit', 'OldIdInscrit', 'INTEGER', false, null, null);
        $this->addColumn('id_entreprise', 'IdEntreprise', 'INTEGER', true, null, null);
        $this->addColumn('id_etablissement', 'IdEtablissement', 'INTEGER', true, null, null);
        $this->addColumn('status', 'Status', 'INTEGER', true, 2, 99);
        $this->addColumn('type_candidature', 'TypeCandidature', 'VARCHAR', false, 45, null);
        $this->addColumn('type_candidature_dume', 'TypeCandidatureDume', 'VARCHAR', false, 45, null);
        $this->addForeignKey('id_dume_contexte', 'IdDumeContexte', 'INTEGER', 't_dume_contexte', 'id', false, null, null);
        $this->addForeignKey('id_offre', 'IdOffre', 'INTEGER', 'Offres', 'id', false, null, null);
        $this->addColumn('role_inscrit', 'RoleInscrit', 'INTEGER', true, null, 1);
        $this->addColumn('date_derniere_validation_dume', 'DateDerniereValidationDume', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        $this->addForeignKey('id_inscrit', 'IdInscrit', 'BIGINT', 'Inscrit', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonTDumeContexte', 'Application\\Propel\\Mpe\\CommonTDumeContexte', RelationMap::MANY_TO_ONE, array('id_dume_contexte' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonInscrit', 'Application\\Propel\\Mpe\\CommonInscrit', RelationMap::MANY_TO_ONE, array('id_inscrit' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonOffres', 'Application\\Propel\\Mpe\\CommonOffres', RelationMap::MANY_TO_ONE, array('id_offre' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), null, 'CASCADE');
        $this->addRelation('CommonTGroupementEntreprise', 'Application\\Propel\\Mpe\\CommonTGroupementEntreprise', RelationMap::ONE_TO_MANY, array('id' => 'id_candidature', ), null, 'CASCADE', 'CommonTGroupementEntreprises');
        $this->addRelation('CommonTListeLotsCandidature', 'Application\\Propel\\Mpe\\CommonTListeLotsCandidature', RelationMap::ONE_TO_MANY, array('id' => 'id_candidature', ), null, 'CASCADE', 'CommonTListeLotsCandidatures');
    } // buildRelations()

} // CommonTCandidatureTableMap
