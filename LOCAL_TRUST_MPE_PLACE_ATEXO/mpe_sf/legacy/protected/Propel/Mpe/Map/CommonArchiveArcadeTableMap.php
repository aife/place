<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'archive_arcade' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonArchiveArcadeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonArchiveArcadeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('archive_arcade');
        $this->setPhpName('CommonArchiveArcade');
        $this->setClassname('Application\\Propel\\Mpe\\CommonArchiveArcade');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('annee', 'Annee', 'INTEGER', true, null, null);
        $this->addColumn('num_semaine', 'NumSemaine', 'INTEGER', true, null, null);
        $this->addColumn('poids_archive', 'PoidsArchive', 'INTEGER', false, null, null);
        $this->addColumn('chemin_fichier', 'CheminFichier', 'VARCHAR', false, 255, null);
        $this->addColumn('date_envoi_debut', 'DateEnvoiDebut', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_envoi_fin', 'DateEnvoiFin', 'TIMESTAMP', false, null, null);
        $this->addColumn('status_transmission', 'StatusTransmission', 'BOOLEAN', true, 1, false);
        $this->addColumn('erreur', 'Erreur', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultationArchiveArcade', 'Application\\Propel\\Mpe\\CommonConsultationArchiveArcade', RelationMap::ONE_TO_MANY, array('id' => 'archive_arcade_id', ), null, null, 'CommonConsultationArchiveArcades');
    } // buildRelations()

} // CommonArchiveArcadeTableMap
