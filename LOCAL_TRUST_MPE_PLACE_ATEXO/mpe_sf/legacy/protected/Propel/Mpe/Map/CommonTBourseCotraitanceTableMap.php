<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_bourse_cotraitance' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTBourseCotraitanceTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTBourseCotraitanceTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_bourse_cotraitance');
        $this->setPhpName('CommonTBourseCotraitance');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTBourseCotraitance');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_auto_BC', 'IdAutoBc', 'INTEGER', true, null, null);
        $this->addColumn('reference_consultation', 'ReferenceConsultation', 'INTEGER', true, null, null);
        $this->addColumn('id_entreprise', 'IdEntreprise', 'INTEGER', true, null, null);
        $this->addColumn('id_etablissement_inscrite', 'IdEtablissementInscrite', 'INTEGER', true, null, null);
        $this->addColumn('nom_inscrit', 'NomInscrit', 'VARCHAR', true, 100, null);
        $this->addColumn('prenom_inscrit', 'PrenomInscrit', 'VARCHAR', true, 100, null);
        $this->addColumn('adresse_inscrit', 'AdresseInscrit', 'VARCHAR', true, 100, null);
        $this->addColumn('adresse2_incsrit', 'Adresse2Incsrit', 'VARCHAR', false, 100, null);
        $this->addColumn('cp_inscrit', 'CpInscrit', 'VARCHAR', true, 20, null);
        $this->addColumn('ville_inscrit', 'VilleInscrit', 'VARCHAR', true, 100, null);
        $this->addColumn('pays_inscrit', 'PaysInscrit', 'VARCHAR', false, 250, null);
        $this->addColumn('fonction_inscrit', 'FonctionInscrit', 'VARCHAR', true, 100, null);
        $this->addColumn('email_inscrit', 'EmailInscrit', 'VARCHAR', true, 100, null);
        $this->addColumn('tel_fixe_inscrit', 'TelFixeInscrit', 'VARCHAR', false, 100, null);
        $this->addColumn('tel_mobile_inscrit', 'TelMobileInscrit', 'VARCHAR', false, 100, null);
        $this->addColumn('mandataire_groupement', 'MandataireGroupement', 'CHAR', true, null, '0');
        $this->getColumn('mandataire_groupement', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('cotraitant_solidaire', 'CotraitantSolidaire', 'CHAR', true, null, '0');
        $this->getColumn('cotraitant_solidaire', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('cotraitant_conjoint', 'CotraitantConjoint', 'CHAR', true, null, '0');
        $this->getColumn('cotraitant_conjoint', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('desc_mon_apport_marche', 'DescMonApportMarche', 'LONGVARCHAR', false, null, null);
        $this->addColumn('desc_type_cotraitance_recherche', 'DescTypeCotraitanceRecherche', 'LONGVARCHAR', false, null, null);
        $this->addColumn('clause_social', 'ClauseSocial', 'CHAR', true, null, '0');
        $this->getColumn('clause_social', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('entreprise_adapte', 'EntrepriseAdapte', 'CHAR', true, null, '0');
        $this->getColumn('entreprise_adapte', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('long', 'Long', 'DOUBLE', false, null, null);
        $this->addColumn('lat', 'Lat', 'DOUBLE', false, null, null);
        $this->addColumn('maj_long_lat', 'MajLongLat', 'TIMESTAMP', false, null, null);
        $this->addColumn('sous_traitant', 'SousTraitant', 'CHAR', false, null, '0');
        $this->getColumn('sous_traitant', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonTBourseCotraitanceTableMap
