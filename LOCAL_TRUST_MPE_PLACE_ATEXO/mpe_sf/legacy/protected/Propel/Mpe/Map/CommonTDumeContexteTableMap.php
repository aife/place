<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_dume_contexte' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTDumeContexteTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTDumeContexteTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_dume_contexte');
        $this->setPhpName('CommonTDumeContexte');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTDumeContexte');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('ref_consultation', 'RefConsultation', 'INTEGER', false, null, null);
        $this->addForeignKey('organisme', 'Organisme', 'VARCHAR', 'Organisme', 'acronyme', true, 45, null);
        $this->addColumn('contexte_lt_dume_id', 'ContexteLtDumeId', 'VARCHAR', false, 255, null);
        $this->addColumn('type_dume', 'TypeDume', 'VARCHAR', false, 45, null);
        $this->addColumn('status', 'Status', 'INTEGER', true, 2, 99);
        $this->addColumn('is_standard', 'IsStandard', 'BOOLEAN', true, 1, true);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', true, null, 'now');
        $this->addColumn('date_modification', 'DateModification', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonTCandidature', 'Application\\Propel\\Mpe\\CommonTCandidature', RelationMap::ONE_TO_MANY, array('id' => 'id_dume_contexte', ), 'CASCADE', 'CASCADE', 'CommonTCandidatures');
        $this->addRelation('CommonTDumeNumero', 'Application\\Propel\\Mpe\\CommonTDumeNumero', RelationMap::ONE_TO_MANY, array('id' => 'id_dume_contexte', ), 'CASCADE', 'CASCADE', 'CommonTDumeNumeros');
    } // buildRelations()

} // CommonTDumeContexteTableMap
