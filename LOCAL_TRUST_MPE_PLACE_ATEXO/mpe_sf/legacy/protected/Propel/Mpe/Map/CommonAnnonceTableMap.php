<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Annonce' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonAnnonceTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonAnnonceTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Annonce');
        $this->setPhpName('CommonAnnonce');
        $this->setClassname('Application\\Propel\\Mpe\\CommonAnnonce');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_boamp', 'IdBoamp', 'INTEGER', true, null, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 100, '');
        $this->addColumn('envoi_boamp', 'EnvoiBoamp', 'INTEGER', true, null, 0);
        $this->addColumn('date_envoi', 'DateEnvoi', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('datepub', 'Datepub', 'VARCHAR', true, 10, '0000-00-00');
        $this->addColumn('consultation_ref', 'ConsultationRef', 'INTEGER', false, null, null);
        $this->addColumn('nom_fichier_xml', 'NomFichierXml', 'VARCHAR', true, 255, '');
        $this->addColumn('envoi_joue', 'EnvoiJoue', 'CHAR', true, null, '0');
        $this->getColumn('envoi_joue', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('mapa', 'Mapa', 'CHAR', true, null, '0');
        $this->getColumn('mapa', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('implique_SAD', 'ImpliqueSad', 'CHAR', true, null, '0');
        $this->getColumn('implique_SAD', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('date_maj', 'DateMaj', 'VARCHAR', true, 10, '0000-00-00');
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonAnnonceTableMap
