<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'consultation_clauses_n1' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonConsultationClausesN1TableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonConsultationClausesN1TableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('consultation_clauses_n1');
        $this->setPhpName('CommonConsultationClausesN1');
        $this->setClassname('Application\\Propel\\Mpe\\CommonConsultationClausesN1');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        $this->addForeignKey('lot_id', 'LotId', 'INTEGER', 'CategorieLot', 'id', false, null, null);
        $this->addForeignKey('referentiel_clause_n1_id', 'ReferentielClauseN1Id', 'INTEGER', 'referentiel_consultation_clauses_n1', 'id', true, null, null);
        $this->addForeignKey('contrat_id', 'ContratId', 'INTEGER', 't_contrat_titulaire', 'id_contrat_titulaire', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTContratTitulaire', 'Application\\Propel\\Mpe\\CommonTContratTitulaire', RelationMap::MANY_TO_ONE, array('contrat_id' => 'id_contrat_titulaire', ), null, null);
        $this->addRelation('CommonReferentielConsultationClausesN1', 'Application\\Propel\\Mpe\\CommonReferentielConsultationClausesN1', RelationMap::MANY_TO_ONE, array('referentiel_clause_n1_id' => 'id', ), null, null);
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), null, null);
        $this->addRelation('CommonCategorieLot', 'Application\\Propel\\Mpe\\CommonCategorieLot', RelationMap::MANY_TO_ONE, array('lot_id' => 'id', ), null, null);
        $this->addRelation('CommonConsultationClausesN2', 'Application\\Propel\\Mpe\\CommonConsultationClausesN2', RelationMap::ONE_TO_MANY, array('id' => 'clause_n1_id', ), null, null, 'CommonConsultationClausesN2s');
    } // buildRelations()

} // CommonConsultationClausesN1TableMap
