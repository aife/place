<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Enveloppe' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEnveloppeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEnveloppeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Enveloppe');
        $this->setPhpName('CommonEnveloppe');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEnveloppe');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_enveloppe_electro', 'IdEnveloppeElectro', 'INTEGER', true, 22, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 30, '');
        $this->addColumn('offre_id', 'OffreId', 'INTEGER', true, 22, 0);
        $this->addColumn('champs_optionnels', 'ChampsOptionnels', 'BLOB', true, null, null);
        $this->addColumn('fichier', 'Fichier', 'INTEGER', true, null, 0);
        $this->addColumn('supprime', 'Supprime', 'CHAR', true, null, '0');
        $this->addColumn('cryptage', 'Cryptage', 'CHAR', true, null, '1');
        $this->addColumn('nom_fichier', 'NomFichier', 'VARCHAR', true, 255, '');
        $this->addColumn('hash', 'Hash', 'VARCHAR', true, 40, '');
        $this->addColumn('type_env', 'TypeEnv', 'INTEGER', true, 1, 0);
        $this->addColumn('sous_pli', 'SousPli', 'INTEGER', true, 3, 0);
        $this->addColumn('attribue', 'Attribue', 'CHAR', true, null, '0');
        $this->addColumn('dateheure_ouverture', 'DateheureOuverture', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('agent_id_ouverture', 'AgentIdOuverture', 'INTEGER', false, null, null);
        $this->addColumn('agent_id_ouverture2', 'AgentIdOuverture2', 'INTEGER', false, null, null);
        $this->addColumn('donnees_ouverture', 'DonneesOuverture', 'BLOB', true, null, null);
        $this->addColumn('horodatage_donnees_ouverture', 'HorodatageDonneesOuverture', 'BLOB', true, null, null);
        $this->addColumn('statut_enveloppe', 'StatutEnveloppe', 'INTEGER', true, 2, 1);
        $this->addColumn('agent_telechargement', 'AgentTelechargement', 'INTEGER', false, null, null);
        $this->addColumn('date_telechargement', 'DateTelechargement', 'VARCHAR', false, 20, null);
        $this->addColumn('repertoire_telechargement', 'RepertoireTelechargement', 'VARCHAR', false, 100, null);
        $this->addColumn('nom_agent_ouverture', 'NomAgentOuverture', 'VARCHAR', true, 100, '');
        $this->addColumn('dateheure_ouverture_agent2', 'DateheureOuvertureAgent2', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('enveloppe_fictive', 'EnveloppeFictive', 'CHAR', false, null, '0');
        $this->getColumn('enveloppe_fictive', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('integrite_fichier', 'IntegriteFichier', 'INTEGER', true, 1, 0);
        $this->addColumn('verification_signature', 'VerificationSignature', 'VARCHAR', true, 50, '');
        $this->addColumn('uid_response', 'UidResponse', 'LONGVARCHAR', false, null, null);
        $this->addColumn('resultat_verification_hash_files', 'ResultatVerificationHashFiles', 'CHAR', false, null, '1');
        $this->addForeignKey('id_dossier_volumineux', 'IdDossierVolumineux', 'INTEGER', 'dossier_volumineux', 'id', false, null, null);
        $this->addColumn('date_debut_dechiffrement', 'DateDebutDechiffrement', 'TIMESTAMP', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonDossierVolumineux', 'Application\\Propel\\Mpe\\CommonDossierVolumineux', RelationMap::MANY_TO_ONE, array('id_dossier_volumineux' => 'id', ), null, null);
    } // buildRelations()

} // CommonEnveloppeTableMap
