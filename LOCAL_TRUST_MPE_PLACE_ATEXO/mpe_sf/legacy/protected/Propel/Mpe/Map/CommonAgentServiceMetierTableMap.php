<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Agent_Service_Metier' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonAgentServiceMetierTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonAgentServiceMetierTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Agent_Service_Metier');
        $this->setPhpName('CommonAgentServiceMetier');
        $this->setClassname('Application\\Propel\\Mpe\\CommonAgentServiceMetier');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('id_agent', 'IdAgent', 'INTEGER' , 'Agent', 'id', true, null, 0);
        $this->addForeignPrimaryKey('id_service_metier', 'IdServiceMetier', 'INTEGER' , 'Service_Mertier', 'id', true, null, 0);
        $this->addColumn('id_profil_service', 'IdProfilService', 'INTEGER', true, null, 0);
        $this->addColumn('date_creation', 'DateCreation', 'VARCHAR', false, 20, null);
        $this->addColumn('date_modification', 'DateModification', 'VARCHAR', false, 20, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonAgent', 'Application\\Propel\\Mpe\\CommonAgent', RelationMap::MANY_TO_ONE, array('id_agent' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonServiceMertier', 'Application\\Propel\\Mpe\\CommonServiceMertier', RelationMap::MANY_TO_ONE, array('id_service_metier' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonAgentServiceMetierTableMap
