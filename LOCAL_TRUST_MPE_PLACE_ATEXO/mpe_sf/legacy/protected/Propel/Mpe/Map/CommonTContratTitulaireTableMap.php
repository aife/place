<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_contrat_titulaire' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTContratTitulaireTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTContratTitulaireTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_contrat_titulaire');
        $this->setPhpName('CommonTContratTitulaire');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTContratTitulaire');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        $this->setSingleTableInheritance(true);
        // columns
        $this->addPrimaryKey('id_contrat_titulaire', 'IdContratTitulaire', 'INTEGER', true, null, null);
        $this->addForeignKey('id_type_contrat', 'IdTypeContrat', 'INTEGER', 't_type_contrat', 'id_type_contrat', false, null, null);
        $this->addForeignKey('id_contact_contrat', 'IdContactContrat', 'INTEGER', 't_contact_contrat', 'id_contact_contrat', false, null, null);
        $this->addColumn('numero_contrat', 'NumeroContrat', 'VARCHAR', false, 255, null);
        $this->addColumn('lien_AC_SAD', 'LienAcSad', 'INTEGER', false, null, null);
        $this->addForeignKey('id_contrat_multi', 'IdContratMulti', 'INTEGER', 't_contrat_titulaire', 'id_contrat_titulaire', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', false, 45, null);
        $this->addColumn('old_service_id', 'OldServiceId', 'INTEGER', false, null, null);
        $this->addColumn('id_titulaire', 'IdTitulaire', 'INTEGER', false, null, null);
        $this->addColumn('id_titulaire_etab', 'IdTitulaireEtab', 'INTEGER', false, null, null);
        $this->addColumn('id_offre', 'IdOffre', 'INTEGER', false, null, null);
        $this->addColumn('type_depot_reponse', 'TypeDepotReponse', 'CHAR', false, null, null);
        $this->addColumn('objet_contrat', 'ObjetContrat', 'LONGVARCHAR', false, null, null);
        $this->addColumn('intitule', 'Intitule', 'VARCHAR', false, 255, '');
        $this->addColumn('montant_contrat', 'MontantContrat', 'DOUBLE', false, null, null);
        $this->addColumn('id_tranche_budgetaire', 'IdTrancheBudgetaire', 'INTEGER', false, null, null);
        $this->addColumn('montant_max_estime', 'MontantMaxEstime', 'DOUBLE', false, null, null);
        $this->addColumn('publication_montant', 'PublicationMontant', 'TINYINT', false, null, null);
        $this->addColumn('id_motif_non_publication_montant', 'IdMotifNonPublicationMontant', 'INTEGER', false, null, null);
        $this->addColumn('desc_motif_non_publication_montant', 'DescMotifNonPublicationMontant', 'VARCHAR', false, 255, null);
        $this->addColumn('publication_contrat', 'PublicationContrat', 'TINYINT', false, null, null);
        $this->addColumn('num_EJ', 'NumEj', 'VARCHAR', false, 100, null);
        $this->addColumn('statutEJ', 'Statutej', 'LONGVARCHAR', false, null, null);
        $this->addColumn('num_long_OEAP', 'NumLongOeap', 'VARCHAR', false, 45, null);
        $this->addColumn('reference_libre', 'ReferenceLibre', 'VARCHAR', false, 45, null);
        $this->addColumn('statut_contrat', 'StatutContrat', 'INTEGER', false, null, 0);
        $this->addColumn('categorie', 'Categorie', 'INTEGER', false, null, null);
        $this->addColumn('ccag_applicable', 'CcagApplicable', 'INTEGER', false, null, null);
        $this->addColumn('clause_sociale', 'ClauseSociale', 'VARCHAR', false, 255, null);
        $this->addColumn('clause_sociale_condition_execution', 'ClauseSocialeConditionExecution', 'VARCHAR', false, 225, null);
        $this->addColumn('clause_sociale_insertion', 'ClauseSocialeInsertion', 'VARCHAR', false, 225, null);
        $this->addColumn('clause_sociale_ateliers_proteges', 'ClauseSocialeAteliersProteges', 'VARCHAR', false, 225, null);
        $this->addColumn('clause_sociale_siae', 'ClauseSocialeSiae', 'VARCHAR', false, 225, '0');
        $this->addColumn('clause_sociale_ess', 'ClauseSocialeEss', 'VARCHAR', false, 225, '0');
        $this->addColumn('clause_environnementale', 'ClauseEnvironnementale', 'VARCHAR', false, 225, null);
        $this->addColumn('clause_env_specs_techniques', 'ClauseEnvSpecsTechniques', 'VARCHAR', false, 225, null);
        $this->addColumn('clause_env_cond_execution', 'ClauseEnvCondExecution', 'VARCHAR', false, 225, null);
        $this->addColumn('clause_env_criteres_select', 'ClauseEnvCriteresSelect', 'VARCHAR', false, 225, null);
        $this->addColumn('date_prevue_notification', 'DatePrevueNotification', 'DATE', false, null, null);
        $this->addColumn('date_prevue_fin_contrat', 'DatePrevueFinContrat', 'DATE', false, null, null);
        $this->addColumn('date_prevue_max_fin_contrat', 'DatePrevueMaxFinContrat', 'DATE', false, null, null);
        $this->addColumn('date_notification', 'DateNotification', 'DATE', false, null, null);
        $this->addColumn('date_fin_contrat', 'DateFinContrat', 'DATE', false, null, null);
        $this->addColumn('date_max_fin_contrat', 'DateMaxFinContrat', 'DATE', false, null, null);
        $this->addColumn('date_attribution', 'DateAttribution', 'DATE', false, null, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_modification', 'DateModification', 'TIMESTAMP', false, null, null);
        $this->addColumn('envoi_interface', 'EnvoiInterface', 'CHAR', true, null, '0');
        $this->getColumn('envoi_interface', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('contrat_class_key', 'ContratClassKey', 'INTEGER', true, 1, 1);
        $this->addColumn('pme_pmi', 'PmePmi', 'INTEGER', true, null, 0);
        $this->addColumn('reference_consultation', 'ReferenceConsultation', 'VARCHAR', false, 255, null);
        $this->addColumn('hors_passation', 'HorsPassation', 'INTEGER', true, 1, 0);
        $this->addColumn('id_agent', 'IdAgent', 'INTEGER', false, null, null);
        $this->addColumn('nom_agent', 'NomAgent', 'VARCHAR', false, 255, null);
        $this->addColumn('prenom_agent', 'PrenomAgent', 'VARCHAR', false, 255, null);
        $this->addColumn('lieu_execution', 'LieuExecution', 'LONGVARCHAR', false, null, null);
        $this->addColumn('code_cpv_1', 'CodeCpv1', 'VARCHAR', false, 8, null);
        $this->addColumn('code_cpv_2', 'CodeCpv2', 'VARCHAR', false, 255, null);
        $this->addColumn('marche_insertion', 'MarcheInsertion', 'BOOLEAN', false, 1, false);
        $this->addColumn('clause_specification_technique', 'ClauseSpecificationTechnique', 'VARCHAR', false, 255, '0');
        $this->addColumn('procedure_passation_pivot', 'ProcedurePassationPivot', 'VARCHAR', false, 256, null);
        $this->addColumn('nom_lieu_principal_execution', 'NomLieuPrincipalExecution', 'VARCHAR', true, 255, null);
        $this->addColumn('code_lieu_principal_execution', 'CodeLieuPrincipalExecution', 'VARCHAR', true, 20, null);
        $this->addColumn('type_code_lieu_principal_execution', 'TypeCodeLieuPrincipalExecution', 'VARCHAR', true, 20, null);
        $this->addColumn('duree_initiale_contrat', 'DureeInitialeContrat', 'INTEGER', false, null, null);
        $this->addColumn('forme_prix', 'FormePrix', 'VARCHAR', true, 255, null);
        $this->addColumn('date_publication_initiale_de', 'DatePublicationInitialeDe', 'TIMESTAMP', true, null, '0000-00-00 00:00:00');
        $this->addColumn('num_id_unique_marche_public', 'NumIdUniqueMarchePublic', 'VARCHAR', true, 255, null);
        $this->addColumn('libelle_type_contrat_pivot', 'LibelleTypeContratPivot', 'VARCHAR', true, 255, null);
        $this->addColumn('siret_pa_accord_cadre', 'SiretPaAccordCadre', 'VARCHAR', false, 255, null);
        $this->addColumn('ac_marche_subsequent', 'AcMarcheSubsequent', 'BOOLEAN', true, 1, false);
        $this->addColumn('libelle_type_procedure_mpe', 'LibelleTypeProcedureMpe', 'VARCHAR', false, 255, null);
        $this->addColumn('nb_total_propositions_lot', 'NbTotalPropositionsLot', 'INTEGER', false, null, null);
        $this->addColumn('nb_total_propositions_demat_lot', 'NbTotalPropositionsDematLot', 'INTEGER', false, null, null);
        $this->addColumn('marche_defense', 'MarcheDefense', 'CHAR', false, null, '0');
        $this->addColumn('siret', 'Siret', 'VARCHAR', false, 255, null);
        $this->addColumn('nom_entite_acheteur', 'NomEntiteAcheteur', 'VARCHAR', false, 255, null);
        $this->addColumn('statut_publication_sn', 'StatutPublicationSn', 'INTEGER', true, 2, 0);
        $this->addColumn('date_publication_sn', 'DatePublicationSn', 'TIMESTAMP', false, null, null);
        $this->addColumn('erreur_sn', 'ErreurSn', 'LONGVARCHAR', false, null, null);
        $this->addColumn('date_modification_sn', 'DateModificationSn', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('id_type_procedure_pivot', 'IdTypeProcedurePivot', 'INTEGER', 'TypeProcedure', 'id_type_procedure', false, null, null);
        $this->addForeignKey('id_type_procedure_concession_pivot', 'IdTypeProcedureConcessionPivot', 'INTEGER', 'type_procedure_concession_pivot', 'id', false, null, null);
        $this->addForeignKey('id_type_contrat_pivot', 'IdTypeContratPivot', 'INTEGER', 'type_contrat_pivot', 'id', false, null, null);
        $this->addForeignKey('id_type_contrat_concession_pivot', 'IdTypeContratConcessionPivot', 'INTEGER', 'type_contrat_concession_pivot', 'id', false, null, null);
        $this->addColumn('montant_subvention_publique', 'MontantSubventionPublique', 'DOUBLE', false, null, null);
        $this->addColumn('date_debut_execution', 'DateDebutExecution', 'DATE', false, null, null);
        $this->addColumn('uuid', 'Uuid', 'VARCHAR', false, 36, null);
        $this->addColumn('marche_innovant', 'MarcheInnovant', 'BOOLEAN', true, 1, null);
        $this->addColumn('service_id', 'ServiceId', 'BIGINT', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTypeContratConcessionPivot', 'Application\\Propel\\Mpe\\CommonTypeContratConcessionPivot', RelationMap::MANY_TO_ONE, array('id_type_contrat_concession_pivot' => 'id', ), null, null);
        $this->addRelation('CommonTypeContratPivot', 'Application\\Propel\\Mpe\\CommonTypeContratPivot', RelationMap::MANY_TO_ONE, array('id_type_contrat_pivot' => 'id', ), null, null);
        $this->addRelation('CommonTypeProcedureConcessionPivot', 'Application\\Propel\\Mpe\\CommonTypeProcedureConcessionPivot', RelationMap::MANY_TO_ONE, array('id_type_procedure_concession_pivot' => 'id', ), null, null);
        $this->addRelation('CommonTContactContrat', 'Application\\Propel\\Mpe\\CommonTContactContrat', RelationMap::MANY_TO_ONE, array('id_contact_contrat' => 'id_contact_contrat', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonTContratTitulaireRelatedByIdContratMulti', 'Application\\Propel\\Mpe\\CommonTContratTitulaire', RelationMap::MANY_TO_ONE, array('id_contrat_multi' => 'id_contrat_titulaire', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonTTypeContrat', 'Application\\Propel\\Mpe\\CommonTTypeContrat', RelationMap::MANY_TO_ONE, array('id_type_contrat' => 'id_type_contrat', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonTypeProcedure', 'Application\\Propel\\Mpe\\CommonTypeProcedure', RelationMap::MANY_TO_ONE, array('id_type_procedure_pivot' => 'id_type_procedure', ), null, null);
        $this->addRelation('CommonMarche', 'Application\\Propel\\Mpe\\CommonMarche', RelationMap::ONE_TO_MANY, array('id_contrat_titulaire' => 'id_contrat_titulaire', ), 'CASCADE', 'CASCADE', 'CommonMarches');
        $this->addRelation('CommonDonneesAnnuellesConcession', 'Application\\Propel\\Mpe\\CommonDonneesAnnuellesConcession', RelationMap::ONE_TO_MANY, array('id_contrat_titulaire' => 'id_contrat', ), null, null, 'CommonDonneesAnnuellesConcessions');
        $this->addRelation('CommonEchangeDoc', 'Application\\Propel\\Mpe\\CommonEchangeDoc', RelationMap::ONE_TO_MANY, array('id_contrat_titulaire' => 'id_contrat_titulaire', ), null, null, 'CommonEchangeDocs');
        $this->addRelation('CommonModificationContrat', 'Application\\Propel\\Mpe\\CommonModificationContrat', RelationMap::ONE_TO_MANY, array('id_contrat_titulaire' => 'id_contrat_titulaire', ), null, null, 'CommonModificationContrats');
        $this->addRelation('CommonTConsLotContrat', 'Application\\Propel\\Mpe\\CommonTConsLotContrat', RelationMap::ONE_TO_MANY, array('id_contrat_titulaire' => 'id_contrat_titulaire', ), 'CASCADE', 'CASCADE', 'CommonTConsLotContrats');
        $this->addRelation('CommonTContratTitulaireRelatedByIdContratTitulaire', 'Application\\Propel\\Mpe\\CommonTContratTitulaire', RelationMap::ONE_TO_MANY, array('id_contrat_titulaire' => 'id_contrat_multi', ), 'CASCADE', 'CASCADE', 'CommonTContratTitulairesRelatedByIdContratTitulaire');
        $this->addRelation('CommonTDonneesConsultation', 'Application\\Propel\\Mpe\\CommonTDonneesConsultation', RelationMap::ONE_TO_MANY, array('id_contrat_titulaire' => 'id_contrat_titulaire', ), 'CASCADE', 'CASCADE', 'CommonTDonneesConsultations');
    } // buildRelations()

} // CommonTContratTitulaireTableMap
