<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'code_retour' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonCodeRetourTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonCodeRetourTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('code_retour');
        $this->setPhpName('CommonCodeRetour');
        $this->setClassname('Application\\Propel\\Mpe\\CommonCodeRetour');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('code', 'Code', 'INTEGER', true, null, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEchangesInterfaces', 'Application\\Propel\\Mpe\\CommonEchangesInterfaces', RelationMap::ONE_TO_MANY, array('code' => 'code_retour', ), null, null, 'CommonEchangesInterfacess');
    } // buildRelations()

} // CommonCodeRetourTableMap
