<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Referentiel' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonReferentielTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonReferentielTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Referentiel');
        $this->setPhpName('CommonReferentiel');
        $this->setClassname('Application\\Propel\\Mpe\\CommonReferentiel');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_referentiel', 'IdReferentiel', 'INTEGER', true, 10, null);
        $this->addColumn('libelle_referentiel', 'LibelleReferentiel', 'VARCHAR', true, 200, '');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonValeurReferentiel', 'Application\\Propel\\Mpe\\CommonValeurReferentiel', RelationMap::ONE_TO_MANY, array('id_referentiel' => 'id_referentiel', ), null, null, 'CommonValeurReferentiels');
    } // buildRelations()

} // CommonReferentielTableMap
