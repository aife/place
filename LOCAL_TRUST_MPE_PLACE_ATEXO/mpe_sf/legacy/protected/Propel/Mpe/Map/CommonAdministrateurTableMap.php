<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Administrateur' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonAdministrateurTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonAdministrateurTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Administrateur');
        $this->setPhpName('CommonAdministrateur');
        $this->setClassname('Application\\Propel\\Mpe\\CommonAdministrateur');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('original_login', 'OriginalLogin', 'VARCHAR', true, 100, null);
        $this->addColumn('login', 'Login', 'VARCHAR', true, 100, null);
        $this->addColumn('certificat', 'Certificat', 'LONGVARCHAR', false, null, null);
        $this->addColumn('mdp', 'Mdp', 'VARCHAR', false, 255, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 100, null);
        $this->addColumn('prenom', 'Prenom', 'VARCHAR', true, 100, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, null);
        $this->addForeignKey('organisme', 'Organisme', 'VARCHAR', 'Organisme', 'acronyme', false, 30, null);
        $this->addColumn('admin_general', 'AdminGeneral', 'CHAR', true, null, null);
        $this->addColumn('tentatives_mdp', 'TentativesMdp', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), null, null);
    } // buildRelations()

} // CommonAdministrateurTableMap
