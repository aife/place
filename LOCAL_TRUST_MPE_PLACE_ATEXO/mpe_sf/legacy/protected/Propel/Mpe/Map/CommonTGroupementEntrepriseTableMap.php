<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_groupement_entreprise' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTGroupementEntrepriseTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTGroupementEntrepriseTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_groupement_entreprise');
        $this->setPhpName('CommonTGroupementEntreprise');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTGroupementEntreprise');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_groupement_entreprise', 'IdGroupementEntreprise', 'INTEGER', true, null, null);
        $this->addForeignKey('id_type_groupement', 'IdTypeGroupement', 'INTEGER', 't_type_groupement_entreprise', 'id_type_groupement', false, null, null);
        $this->addForeignKey('id_offre', 'IdOffre', 'INTEGER', 'Offres', 'id', false, null, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('id_candidature', 'IdCandidature', 'INTEGER', 't_candidature', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonOffres', 'Application\\Propel\\Mpe\\CommonOffres', RelationMap::MANY_TO_ONE, array('id_offre' => 'id', ), null, null);
        $this->addRelation('CommonTTypeGroupementEntreprise', 'Application\\Propel\\Mpe\\CommonTTypeGroupementEntreprise', RelationMap::MANY_TO_ONE, array('id_type_groupement' => 'id_type_groupement', ), null, null);
        $this->addRelation('CommonTCandidature', 'Application\\Propel\\Mpe\\CommonTCandidature', RelationMap::MANY_TO_ONE, array('id_candidature' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonTMembreGroupementEntreprise', 'Application\\Propel\\Mpe\\CommonTMembreGroupementEntreprise', RelationMap::ONE_TO_MANY, array('id_groupement_entreprise' => 'id_groupement_entreprise', ), null, null, 'CommonTMembreGroupementEntreprises');
    } // buildRelations()

} // CommonTGroupementEntrepriseTableMap
