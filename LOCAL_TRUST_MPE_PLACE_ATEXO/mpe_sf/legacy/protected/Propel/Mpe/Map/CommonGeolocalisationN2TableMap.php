<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'GeolocalisationN2' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonGeolocalisationN2TableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonGeolocalisationN2TableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('GeolocalisationN2');
        $this->setPhpName('CommonGeolocalisationN2');
        $this->setClassname('Application\\Propel\\Mpe\\CommonGeolocalisationN2');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('id_geolocalisationN1', 'IdGeolocalisationn1', 'INTEGER', 'GeolocalisationN1', 'id', true, null, 0);
        $this->addColumn('denomination1', 'Denomination1', 'VARCHAR', true, 50, null);
        $this->addColumn('denomination2', 'Denomination2', 'VARCHAR', true, 30, '');
        $this->addColumn('valeur_avec_sous_categorie', 'ValeurAvecSousCategorie', 'CHAR', true, null, '1');
        $this->getColumn('valeur_avec_sous_categorie', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('denomination1_ar', 'Denomination1Ar', 'VARCHAR', true, 50, '');
        $this->addColumn('denomination2_ar', 'Denomination2Ar', 'VARCHAR', true, 30, '');
        $this->addColumn('denomination1_fr', 'Denomination1Fr', 'VARCHAR', true, 50, '');
        $this->addColumn('denomination2_fr', 'Denomination2Fr', 'VARCHAR', true, 30, '');
        $this->addColumn('denomination1_en', 'Denomination1En', 'VARCHAR', true, 50, '');
        $this->addColumn('denomination2_en', 'Denomination2En', 'VARCHAR', true, 30, '');
        $this->addColumn('denomination1_es', 'Denomination1Es', 'VARCHAR', true, 50, '');
        $this->addColumn('denomination2_es', 'Denomination2Es', 'VARCHAR', true, 30, '');
        $this->addColumn('denomination1_su', 'Denomination1Su', 'VARCHAR', true, 50, '');
        $this->addColumn('denomination2_su', 'Denomination2Su', 'VARCHAR', true, 30, '');
        $this->addColumn('denomination1_du', 'Denomination1Du', 'VARCHAR', true, 50, '');
        $this->addColumn('denomination2_du', 'Denomination2Du', 'VARCHAR', true, 30, '');
        $this->addColumn('denomination1_cz', 'Denomination1Cz', 'VARCHAR', true, 50, '');
        $this->addColumn('denomination2_cz', 'Denomination2Cz', 'VARCHAR', true, 30, '');
        $this->addColumn('denomination1_it', 'Denomination1It', 'VARCHAR', true, 50, '');
        $this->addColumn('denomination2_it', 'Denomination2It', 'VARCHAR', true, 30, '');
        $this->addColumn('code_interface', 'CodeInterface', 'VARCHAR', false, 255, null);
        $this->addColumn('valeur_sub', 'ValeurSub', 'VARCHAR', true, 255, null);
        $this->addColumn('type_lieu', 'TypeLieu', 'CHAR', true, null, null);
        $this->addColumn('code_nuts', 'CodeNuts', 'VARCHAR', false, 10, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonGeolocalisationN1', 'Application\\Propel\\Mpe\\CommonGeolocalisationN1', RelationMap::MANY_TO_ONE, array('id_geolocalisationN1' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonGeolocalisationN2TableMap
