<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_CAO_Commission' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTCAOCommissionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTCAOCommissionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_CAO_Commission');
        $this->setPhpName('CommonTCAOCommission');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTCAOCommission');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_commission', 'IdCommission', 'BIGINT', true, null, null);
        $this->addForeignPrimaryKey('organisme', 'Organisme', 'VARCHAR' , 'Organisme', 'acronyme', true, 30, null);
        $this->addColumn('sigle', 'Sigle', 'CHAR', true, 10, null);
        $this->addColumn('intitule', 'Intitule', 'VARCHAR', true, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), null, null);
        $this->addRelation('CommonTCAOCommissionAgent', 'Application\\Propel\\Mpe\\CommonTCAOCommissionAgent', RelationMap::ONE_TO_MANY, array('id_commission' => 'id_commission', ), null, null, 'CommonTCAOCommissionAgents');
        $this->addRelation('CommonTCAOCommissionConsultation', 'Application\\Propel\\Mpe\\CommonTCAOCommissionConsultation', RelationMap::ONE_TO_MANY, array('id_commission' => 'id_commission', ), null, null, 'CommonTCAOCommissionConsultations');
        $this->addRelation('CommonTCAOCommissionIntervenantExterne', 'Application\\Propel\\Mpe\\CommonTCAOCommissionIntervenantExterne', RelationMap::ONE_TO_MANY, array('id_commission' => 'id_commission', ), null, null, 'CommonTCAOCommissionIntervenantExternes');
        $this->addRelation('CommonTCAOOrdreDePassage', 'Application\\Propel\\Mpe\\CommonTCAOOrdreDePassage', RelationMap::ONE_TO_MANY, array('id_commission' => 'id_commission', ), null, null, 'CommonTCAOOrdreDePassages');
        $this->addRelation('CommonTCAOOrdreDuJour', 'Application\\Propel\\Mpe\\CommonTCAOOrdreDuJour', RelationMap::ONE_TO_MANY, array('id_commission' => 'id_commission', ), null, null, 'CommonTCAOOrdreDuJours');
        $this->addRelation('CommonTCAOSeance', 'Application\\Propel\\Mpe\\CommonTCAOSeance', RelationMap::ONE_TO_MANY, array('id_commission' => 'id_commission', ), null, null, 'CommonTCAOSeances');
    } // buildRelations()

} // CommonTCAOCommissionTableMap
