<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'jms_job_statistics' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonJmsJobStatisticsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonJmsJobStatisticsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jms_job_statistics');
        $this->setPhpName('CommonJmsJobStatistics');
        $this->setClassname('Application\\Propel\\Mpe\\CommonJmsJobStatistics');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('job_id', 'JobId', 'BIGINT', true, null, null);
        $this->addPrimaryKey('characteristic', 'Characteristic', 'VARCHAR', true, 30, null);
        $this->addPrimaryKey('createdAt', 'Createdat', 'TIMESTAMP', true, null, null);
        $this->addColumn('charValue', 'Charvalue', 'DOUBLE', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonJmsJobStatisticsTableMap
