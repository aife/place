<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Inscrit' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonInscritTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonInscritTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Inscrit');
        $this->setPhpName('CommonInscrit');
        $this->setClassname('Application\\Propel\\Mpe\\CommonInscrit');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('old_id', 'OldId', 'INTEGER', false, null, null);
        $this->addForeignKey('entreprise_id', 'EntrepriseId', 'INTEGER', 'Entreprise', 'id', true, null, null);
        $this->addForeignKey('id_etablissement', 'IdEtablissement', 'INTEGER', 't_etablissement', 'id_etablissement', false, null, null);
        $this->addColumn('login', 'Login', 'VARCHAR', false, 100, null);
        $this->addColumn('mdp', 'Mdp', 'VARCHAR', false, 255, null);
        $this->addColumn('num_cert', 'NumCert', 'VARCHAR', false, 64, null);
        $this->addColumn('cert', 'Cert', 'CLOB', false, null, null);
        $this->addColumn('civilite', 'Civilite', 'BOOLEAN', true, 1, false);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 30, null);
        $this->addColumn('prenom', 'Prenom', 'VARCHAR', true, 30, null);
        $this->addColumn('adresse', 'Adresse', 'VARCHAR', true, 80, null);
        $this->addColumn('codepostal', 'Codepostal', 'VARCHAR', true, 20, null);
        $this->addColumn('ville', 'Ville', 'VARCHAR', true, 50, null);
        $this->addColumn('pays', 'Pays', 'VARCHAR', true, 50, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, null);
        $this->addColumn('telephone', 'Telephone', 'VARCHAR', true, 20, null);
        $this->addColumn('categorie', 'Categorie', 'LONGVARCHAR', false, null, null);
        $this->addColumn('motstitreresume', 'Motstitreresume', 'CLOB', false, null, null);
        $this->addColumn('periode', 'Periode', 'SMALLINT', true, null, 0);
        $this->addColumn('siret', 'Siret', 'VARCHAR', true, 5, null);
        $this->addColumn('fax', 'Fax', 'VARCHAR', true, 20, null);
        $this->addColumn('code_cpv', 'CodeCpv', 'LONGVARCHAR', false, null, null);
        $this->addColumn('id_langue', 'IdLangue', 'INTEGER', false, null, null);
        $this->addColumn('profil', 'Profil', 'INTEGER', true, 1, 1);
        $this->addColumn('adresse2', 'Adresse2', 'VARCHAR', false, 80, null);
        $this->addColumn('bloque', 'Bloque', 'CHAR', true, null, '0');
        $this->getColumn('bloque', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('id_initial', 'IdInitial', 'INTEGER', true, null, 0);
        $this->addColumn('inscrit_annuaire_defense', 'InscritAnnuaireDefense', 'CHAR', true, null, '0');
        $this->getColumn('inscrit_annuaire_defense', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('date_creation', 'DateCreation', 'VARCHAR', true, 20, '');
        $this->addColumn('date_modification', 'DateModification', 'VARCHAR', true, 20, '');
        $this->addColumn('tentatives_mdp', 'TentativesMdp', 'INTEGER', true, 1, 0);
        $this->addColumn('uid', 'Uid', 'VARCHAR', false, 50, null);
        $this->addColumn('type_hash', 'TypeHash', 'VARCHAR', false, 10, null);
        $this->addColumn('id_externe', 'IdExterne', 'VARCHAR', true, 50, '0');
        $this->addPrimaryKey('id', 'Id', 'BIGINT', true, null, null);
        $this->addColumn('date_validation_rgpd', 'DateValidationRgpd', 'TIMESTAMP', false, null, null);
        $this->addColumn('rgpd_communication_place', 'RgpdCommunicationPlace', 'BOOLEAN', false, 1, null);
        $this->addColumn('rgpd_enquete', 'RgpdEnquete', 'BOOLEAN', false, 1, null);
        $this->addColumn('rgpd_communication', 'RgpdCommunication', 'BOOLEAN', false, 1, null);
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted', 'Deleted', 'BOOLEAN', false, 1, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entreprise', 'Application\\Propel\\Mpe\\Entreprise', RelationMap::MANY_TO_ONE, array('entreprise_id' => 'id', ), null, null);
        $this->addRelation('CommonTEtablissement', 'Application\\Propel\\Mpe\\CommonTEtablissement', RelationMap::MANY_TO_ONE, array('id_etablissement' => 'id_etablissement', ), null, null);
        $this->addRelation('CommonAlerte', 'Application\\Propel\\Mpe\\CommonAlerte', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonAlertes');
        $this->addRelation('CommonCertificatsEntreprises', 'Application\\Propel\\Mpe\\CommonCertificatsEntreprises', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonCertificatsEntreprisess');
        $this->addRelation('CommonEchangeDestinataire', 'Application\\Propel\\Mpe\\CommonEchangeDestinataire', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonEchangeDestinataires');
        $this->addRelation('CommonOffres', 'Application\\Propel\\Mpe\\CommonOffres', RelationMap::ONE_TO_MANY, array('id' => 'inscrit_id', ), null, 'CASCADE', 'CommonOffress');
        $this->addRelation('CommonPanierEntreprise', 'Application\\Propel\\Mpe\\CommonPanierEntreprise', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonPanierEntreprises');
        $this->addRelation('CommonQuestionDCE', 'Application\\Propel\\Mpe\\CommonQuestionDCE', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonQuestionDCEs');
        $this->addRelation('CommonReponseInscritFormulaireConsultation', 'Application\\Propel\\Mpe\\CommonReponseInscritFormulaireConsultation', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonReponseInscritFormulaireConsultations');
        $this->addRelation('CommonTelechargement', 'Application\\Propel\\Mpe\\CommonTelechargement', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonTelechargements');
        $this->addRelation('CommonDossierVolumineux', 'Application\\Propel\\Mpe\\CommonDossierVolumineux', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonDossierVolumineuxs');
        $this->addRelation('CommonHistorisationMotDePasse', 'Application\\Propel\\Mpe\\CommonHistorisationMotDePasse', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonHistorisationMotDePasses');
        $this->addRelation('CommonQuestionsDce', 'Application\\Propel\\Mpe\\CommonQuestionsDce', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonQuestionsDces');
        $this->addRelation('CommonSsoEntreprise', 'Application\\Propel\\Mpe\\CommonSsoEntreprise', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonSsoEntreprises');
        $this->addRelation('CommonTCandidature', 'Application\\Propel\\Mpe\\CommonTCandidature', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonTCandidatures');
        $this->addRelation('CommonTCandidatureMps', 'Application\\Propel\\Mpe\\CommonTCandidatureMps', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonTCandidatureMpss');
        $this->addRelation('CommonTContactContrat', 'Application\\Propel\\Mpe\\CommonTContactContrat', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonTContactContrats');
        $this->addRelation('CommonTListeLotsCandidature', 'Application\\Propel\\Mpe\\CommonTListeLotsCandidature', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonTListeLotsCandidatures');
        $this->addRelation('CommonTReponseElecFormulaire', 'Application\\Propel\\Mpe\\CommonTReponseElecFormulaire', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonTReponseElecFormulaires');
        $this->addRelation('CommonTmpSiretIncorrect', 'Application\\Propel\\Mpe\\CommonTmpSiretIncorrect', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), 'CASCADE', 'CASCADE', 'CommonTmpSiretIncorrects');
        $this->addRelation('CommonTraceOperationsInscrit', 'Application\\Propel\\Mpe\\CommonTraceOperationsInscrit', RelationMap::ONE_TO_MANY, array('id' => 'id_inscrit', ), null, 'CASCADE', 'CommonTraceOperationsInscrits');
    } // buildRelations()

} // CommonInscritTableMap
