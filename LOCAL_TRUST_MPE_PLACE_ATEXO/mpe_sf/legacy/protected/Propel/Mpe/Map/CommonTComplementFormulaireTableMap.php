<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_complement_formulaire' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTComplementFormulaireTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTComplementFormulaireTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_complement_formulaire');
        $this->setPhpName('CommonTComplementFormulaire');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTComplementFormulaire');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_complement_formulaire', 'IdComplementFormulaire', 'INTEGER', true, null, null);
        $this->addForeignKey('id_dossier_formulaire', 'IdDossierFormulaire', 'INTEGER', 't_dossier_formulaire', 'id_dossier_formulaire', true, null, null);
        $this->addColumn('numero_complement', 'NumeroComplement', 'INTEGER', false, null, null);
        $this->addColumn('date_a_remettre', 'DateARemettre', 'VARCHAR', false, 50, null);
        $this->addColumn('date_remis_le', 'DateRemisLe', 'VARCHAR', false, 50, null);
        $this->addColumn('motif', 'Motif', 'VARCHAR', false, 255, null);
        $this->addColumn('commentaire', 'Commentaire', 'LONGVARCHAR', false, null, null);
        $this->addColumn('statut_demande', 'StatutDemande', 'CHAR', true, null, '1');
        $this->getColumn('statut_demande', false)->setValueSet(array (
  0 => '1',
  1 => '2',
));
        $this->addColumn('date_envoi_1er_mail', 'DateEnvoi1erMail', 'VARCHAR', false, 255, null);
        $this->addColumn('date_1er_ar', 'Date1erAr', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTDossierFormulaire', 'Application\\Propel\\Mpe\\CommonTDossierFormulaire', RelationMap::MANY_TO_ONE, array('id_dossier_formulaire' => 'id_dossier_formulaire', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonTComplementFormulaireTableMap
