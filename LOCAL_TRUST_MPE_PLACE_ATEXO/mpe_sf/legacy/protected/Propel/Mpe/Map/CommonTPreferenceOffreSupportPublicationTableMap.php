<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_preference_offre_support_publication' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTPreferenceOffreSupportPublicationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTPreferenceOffreSupportPublicationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_preference_offre_support_publication');
        $this->setPhpName('CommonTPreferenceOffreSupportPublication');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTPreferenceOffreSupportPublication');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('id_preference', 'IdPreference', 'INTEGER', true, null, null);
        $this->addColumn('id_support', 'IdSupport', 'INTEGER', true, null, null);
        $this->addColumn('id_offre', 'IdOffre', 'INTEGER', true, null, null);
        $this->addColumn('active', 'Active', 'CHAR', true, null, '0');
        $this->getColumn('active', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('id_agent', 'IdAgent', 'INTEGER', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonTPreferenceOffreSupportPublicationTableMap
