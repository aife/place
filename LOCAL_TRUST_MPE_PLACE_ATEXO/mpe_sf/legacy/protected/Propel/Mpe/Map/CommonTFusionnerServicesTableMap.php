<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_fusionner_services' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTFusionnerServicesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTFusionnerServicesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_fusionner_services');
        $this->setPhpName('CommonTFusionnerServices');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTFusionnerServices');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('id_service_source', 'IdServiceSource', 'BIGINT', 'Service', 'id', false, null, null);
        $this->addForeignKey('id_service_cible', 'IdServiceCible', 'BIGINT', 'Service', 'id', false, null, null);
        $this->addColumn('old_id_service_source', 'OldIdServiceSource', 'INTEGER', false, null, null);
        $this->addColumn('old_id_service_cible', 'OldIdServiceCible', 'INTEGER', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', false, 50, null);
        $this->addColumn('id_agent', 'IdAgent', 'INTEGER', false, null, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_fusion', 'DateFusion', 'TIMESTAMP', false, null, null);
        $this->addColumn('donnees_fusionnees', 'DonneesFusionnees', 'CHAR', false, null, '0');
        $this->getColumn('donnees_fusionnees', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonServiceRelatedByIdServiceCible', 'Application\\Propel\\Mpe\\CommonService', RelationMap::MANY_TO_ONE, array('id_service_cible' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonServiceRelatedByIdServiceSource', 'Application\\Propel\\Mpe\\CommonService', RelationMap::MANY_TO_ONE, array('id_service_source' => 'id', ), null, 'CASCADE');
    } // buildRelations()

} // CommonTFusionnerServicesTableMap
