<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'echange_doc_historique' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEchangeDocHistoriqueTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEchangeDocHistoriqueTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('echange_doc_historique');
        $this->setPhpName('CommonEchangeDocHistorique');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEchangeDocHistorique');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('echange_doc_id', 'EchangeDocId', 'INTEGER', 'echange_doc', 'id', false, null, null);
        $this->addForeignKey('agent_id', 'AgentId', 'INTEGER', 'Agent', 'id', false, null, null);
        $this->addColumn('statut', 'Statut', 'VARCHAR', true, 50, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('message_fonctionnel', 'MessageFonctionnel', 'VARCHAR', false, 500, null);
        $this->addColumn('message_technique', 'MessageTechnique', 'VARCHAR', false, 1000, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonAgent', 'Application\\Propel\\Mpe\\CommonAgent', RelationMap::MANY_TO_ONE, array('agent_id' => 'id', ), null, null);
        $this->addRelation('CommonEchangeDoc', 'Application\\Propel\\Mpe\\CommonEchangeDoc', RelationMap::MANY_TO_ONE, array('echange_doc_id' => 'id', ), null, null);
    } // buildRelations()

} // CommonEchangeDocHistoriqueTableMap
