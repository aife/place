<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'MarchePublie' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonMarchePublieTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonMarchePublieTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('MarchePublie');
        $this->setPhpName('CommonMarchePublie');
        $this->setClassname('Application\\Propel\\Mpe\\CommonMarchePublie');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 30, '');
        $this->addColumn('numeroMarcheAnnee', 'Numeromarcheannee', 'INTEGER', false, null, 0);
        $this->addColumn('old_service_id', 'OldServiceId', 'INTEGER', false, null, null);
        $this->addColumn('isPubliee', 'Ispubliee', 'CHAR', true, null, '0');
        $this->getColumn('isPubliee', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('isImportee', 'Isimportee', 'CHAR', true, null, '0');
        $this->getColumn('isImportee', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('newVersion', 'Newversion', 'CHAR', true, null, '0');
        $this->getColumn('newVersion', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('dataSource', 'Datasource', 'INTEGER', true, 2, 0);
        $this->addColumn('service_id', 'ServiceId', 'BIGINT', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonMarchePublieTableMap
