<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_document_type' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTDocumentTypeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTDocumentTypeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_document_type');
        $this->setPhpName('CommonTDocumentType');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTDocumentType');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_type_document', 'IdTypeDocument', 'INTEGER', true, null, null);
        $this->addColumn('nom_type_document', 'NomTypeDocument', 'VARCHAR', true, 100, null);
        $this->addColumn('code', 'Code', 'VARCHAR', false, 50, null);
        $this->addColumn('type_doc_entreprise_etablissement', 'TypeDocEntrepriseEtablissement', 'CHAR', false, null, null);
        $this->getColumn('type_doc_entreprise_etablissement', false)->setValueSet(array (
  0 => '1',
  1 => '2',
));
        $this->addColumn('uri', 'Uri', 'VARCHAR', false, 100, null);
        $this->addColumn('params_uri', 'ParamsUri', 'VARCHAR', true, 100, null);
        $this->addColumn('class_name', 'ClassName', 'VARCHAR', true, 100, null);
        $this->addColumn('nature_document', 'NatureDocument', 'CHAR', true, null, '1');
        $this->getColumn('nature_document', false)->setValueSet(array (
  0 => '1',
  1 => '2',
));
        $this->addColumn('type_retour_ws', 'TypeRetourWs', 'CHAR', false, null, '1');
        $this->addColumn('synchro_actif', 'SynchroActif', 'CHAR', true, null, '1');
        $this->getColumn('synchro_actif', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('message_desactivation_synchro', 'MessageDesactivationSynchro', 'VARCHAR', true, 255, '');
        $this->addColumn('afficher_type_doc', 'AfficherTypeDoc', 'CHAR', true, null, '1');
        $this->getColumn('afficher_type_doc', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTDocumentEntreprise', 'Application\\Propel\\Mpe\\CommonTDocumentEntreprise', RelationMap::ONE_TO_MANY, array('id_type_document' => 'id_type_document', ), null, null, 'CommonTDocumentEntreprises');
    } // buildRelations()

} // CommonTDocumentTypeTableMap
