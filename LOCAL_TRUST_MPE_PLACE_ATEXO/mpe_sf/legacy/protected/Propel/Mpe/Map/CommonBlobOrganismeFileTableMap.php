<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'blobOrganisme_file' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonBlobOrganismeFileTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonBlobOrganismeFileTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('blobOrganisme_file');
        $this->setPhpName('CommonBlobOrganismeFile');
        $this->setClassname('Application\\Propel\\Mpe\\CommonBlobOrganismeFile');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('old_id', 'OldId', 'INTEGER', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', true, 30, '');
        $this->addColumn('name', 'Name', 'LONGVARCHAR', true, null, null);
        $this->addColumn('deletion_datetime', 'DeletionDatetime', 'TIMESTAMP', false, null, null);
        $this->addColumn('chemin', 'Chemin', 'LONGVARCHAR', false, null, null);
        $this->addColumn('dossier', 'Dossier', 'VARCHAR', false, 256, null);
        $this->addColumn('statut_synchro', 'StatutSynchro', 'INTEGER', true, null, 0);
        $this->addColumn('hash', 'Hash', 'VARCHAR', false, 256, 'ND');
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('extension', 'Extension', 'VARCHAR', true, 50, '');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonDocumentExterne', 'Application\\Propel\\Mpe\\CommonDocumentExterne', RelationMap::ONE_TO_MANY, array('id' => 'idBlob', ), 'CASCADE', 'CASCADE', 'CommonDocumentExternes');
        $this->addRelation('CommonOffresRelatedByIdBlobHorodatageHash', 'Application\\Propel\\Mpe\\CommonOffres', RelationMap::ONE_TO_MANY, array('id' => 'id_blob_horodatage_hash', ), null, null, 'CommonOffressRelatedByIdBlobHorodatageHash');
        $this->addRelation('CommonOffresRelatedByIdBlobXmlReponse', 'Application\\Propel\\Mpe\\CommonOffres', RelationMap::ONE_TO_MANY, array('id' => 'id_blob_xml_reponse', ), null, null, 'CommonOffressRelatedByIdBlobXmlReponse');
        $this->addRelation('CommonAnnexeFinanciere', 'Application\\Propel\\Mpe\\CommonAnnexeFinanciere', RelationMap::ONE_TO_MANY, array('id' => 'id_blob', ), null, null, 'CommonAnnexeFinancieres');
        $this->addRelation('CommonAutrePieceConsultation', 'Application\\Propel\\Mpe\\CommonAutrePieceConsultation', RelationMap::ONE_TO_MANY, array('id' => 'blob_id', ), null, null, 'CommonAutrePieceConsultations');
        $this->addRelation('CommonEchangeDocBlob', 'Application\\Propel\\Mpe\\CommonEchangeDocBlob', RelationMap::ONE_TO_MANY, array('id' => 'blob_organisme_id', ), null, null, 'CommonEchangeDocBlobs');
        $this->addRelation('CommonPieceGenereConsultation', 'Application\\Propel\\Mpe\\CommonPieceGenereConsultation', RelationMap::ONE_TO_MANY, array('id' => 'blob_id', ), 'CASCADE', 'CASCADE', 'CommonPieceGenereConsultations');
    } // buildRelations()

} // CommonBlobOrganismeFileTableMap
