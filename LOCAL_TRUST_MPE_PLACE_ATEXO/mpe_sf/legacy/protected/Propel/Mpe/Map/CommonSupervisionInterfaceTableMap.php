<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'supervision_interface' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonSupervisionInterfaceTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonSupervisionInterfaceTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('supervision_interface');
        $this->setPhpName('CommonSupervisionInterface');
        $this->setClassname('Application\\Propel\\Mpe\\CommonSupervisionInterface');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('created_date', 'CreatedDate', 'DATE', true, null, null);
        $this->addColumn('webservice_batch', 'WebserviceBatch', 'VARCHAR', true, 50, null);
        $this->addColumn('service', 'Service', 'VARCHAR', true, 50, null);
        $this->addColumn('nom_interface', 'NomInterface', 'VARCHAR', false, 50, null);
        $this->addColumn('total', 'Total', 'INTEGER', true, null, null);
        $this->addColumn('total_ok', 'TotalOk', 'INTEGER', false, null, null);
        $this->addColumn('poids', 'Poids', 'BIGINT', false, null, null);
        $this->addColumn('poids_ok', 'PoidsOk', 'BIGINT', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonSupervisionInterfaceTableMap
