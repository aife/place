<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_consultation_compte_pub' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTConsultationComptePubTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTConsultationComptePubTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_consultation_compte_pub');
        $this->setPhpName('CommonTConsultationComptePub');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTConsultationComptePub');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 22, null);
        $this->addForeignKey('id_donnees_complementaires', 'IdDonneesComplementaires', 'INTEGER', 't_donnee_complementaire', 'id_donnee_complementaire', true, 22, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', true, 30, null);
        $this->addColumn('id_compte_pub', 'IdComptePub', 'INTEGER', true, 22, null);
        $this->addColumn('boamp_login', 'BoampLogin', 'VARCHAR', true, 100, '');
        $this->addColumn('boamp_password', 'BoampPassword', 'VARCHAR', true, 100, '');
        $this->addColumn('boamp_mail', 'BoampMail', 'VARCHAR', true, 100, '');
        $this->addColumn('boamp_target', 'BoampTarget', 'CHAR', true, null, '0');
        $this->addColumn('denomination', 'Denomination', 'VARCHAR', true, 100, '');
        $this->addColumn('prm', 'Prm', 'VARCHAR', true, 100, '');
        $this->addColumn('adresse', 'Adresse', 'VARCHAR', true, 100, '');
        $this->addColumn('cp', 'Cp', 'VARCHAR', true, 5, '');
        $this->addColumn('ville', 'Ville', 'VARCHAR', true, 100, '');
        $this->addColumn('url', 'Url', 'VARCHAR', true, 100, '');
        $this->addColumn('facture_denomination', 'FactureDenomination', 'VARCHAR', false, 100, null);
        $this->addColumn('facture_adresse', 'FactureAdresse', 'VARCHAR', true, 255, '');
        $this->addColumn('facture_cp', 'FactureCp', 'VARCHAR', true, 10, '');
        $this->addColumn('facture_ville', 'FactureVille', 'VARCHAR', true, 100, '');
        $this->addColumn('instance_recours_organisme', 'InstanceRecoursOrganisme', 'VARCHAR', false, 200, null);
        $this->addColumn('instance_recours_adresse', 'InstanceRecoursAdresse', 'VARCHAR', false, 200, null);
        $this->addColumn('instance_recours_cp', 'InstanceRecoursCp', 'VARCHAR', false, 200, null);
        $this->addColumn('instance_recours_ville', 'InstanceRecoursVille', 'VARCHAR', false, 200, null);
        $this->addColumn('instance_recours_url', 'InstanceRecoursUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('telephone', 'Telephone', 'VARCHAR', false, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTDonneeComplementaire', 'Application\\Propel\\Mpe\\CommonTDonneeComplementaire', RelationMap::MANY_TO_ONE, array('id_donnees_complementaires' => 'id_donnee_complementaire', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonTConsultationComptePubTableMap
