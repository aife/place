<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'configuration_messages_traduction' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonConfigurationMessagesTraductionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonConfigurationMessagesTraductionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('configuration_messages_traduction');
        $this->setPhpName('CommonConfigurationMessagesTraduction');
        $this->setClassname('Application\\Propel\\Mpe\\CommonConfigurationMessagesTraduction');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('source', 'Source', 'VARCHAR', true, 300, null);
        $this->addColumn('target', 'Target', 'LONGVARCHAR', true, null, null);
        $this->addForeignKey('langue_id', 'LangueId', 'INTEGER', 'Langue', 'id_langue', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonLangue', 'Application\\Propel\\Mpe\\CommonLangue', RelationMap::MANY_TO_ONE, array('langue_id' => 'id_langue', ), null, null);
    } // buildRelations()

} // CommonConfigurationMessagesTraductionTableMap
