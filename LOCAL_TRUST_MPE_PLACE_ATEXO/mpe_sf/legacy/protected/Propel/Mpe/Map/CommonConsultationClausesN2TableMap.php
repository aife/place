<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'consultation_clauses_n2' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonConsultationClausesN2TableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonConsultationClausesN2TableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('consultation_clauses_n2');
        $this->setPhpName('CommonConsultationClausesN2');
        $this->setClassname('Application\\Propel\\Mpe\\CommonConsultationClausesN2');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('clause_n1_id', 'ClauseN1Id', 'INTEGER', 'consultation_clauses_n1', 'id', true, null, null);
        $this->addForeignKey('referentiel_clause_n2_id', 'ReferentielClauseN2Id', 'INTEGER', 'referentiel_consultation_clauses_n2', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonReferentielConsultationClausesN2', 'Application\\Propel\\Mpe\\CommonReferentielConsultationClausesN2', RelationMap::MANY_TO_ONE, array('referentiel_clause_n2_id' => 'id', ), null, null);
        $this->addRelation('CommonConsultationClausesN1', 'Application\\Propel\\Mpe\\CommonConsultationClausesN1', RelationMap::MANY_TO_ONE, array('clause_n1_id' => 'id', ), null, null);
        $this->addRelation('CommonConsultationClausesN3', 'Application\\Propel\\Mpe\\CommonConsultationClausesN3', RelationMap::ONE_TO_MANY, array('id' => 'clause_n2_id', ), null, null, 'CommonConsultationClausesN3s');
    } // buildRelations()

} // CommonConsultationClausesN2TableMap
