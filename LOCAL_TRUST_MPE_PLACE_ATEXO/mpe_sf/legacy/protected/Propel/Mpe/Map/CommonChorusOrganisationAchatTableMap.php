<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Chorus_organisation_achat' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonChorusOrganisationAchatTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonChorusOrganisationAchatTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Chorus_organisation_achat');
        $this->setPhpName('CommonChorusOrganisationAchat');
        $this->setClassname('Application\\Propel\\Mpe\\CommonChorusOrganisationAchat');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 20, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 10, null);
        $this->addColumn('libelle', 'Libelle', 'VARCHAR', true, 250, '');
        $this->addColumn('code', 'Code', 'VARCHAR', true, 50, '');
        $this->addColumn('actif', 'Actif', 'INTEGER', true, null, 1);
        $this->addColumn('flux_fen111', 'FluxFen111', 'CHAR', true, null, '1');
        $this->getColumn('flux_fen111', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('flux_fen211', 'FluxFen211', 'CHAR', true, null, '1');
        $this->getColumn('flux_fen211', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonChorusOrganisationAchatTableMap
