<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_dume_numero' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTDumeNumeroTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTDumeNumeroTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_dume_numero');
        $this->setPhpName('CommonTDumeNumero');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTDumeNumero');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('numero_dume_national', 'NumeroDumeNational', 'VARCHAR', false, 100, null);
        $this->addColumn('blob_id', 'BlobId', 'INTEGER', false, null, null);
        $this->addColumn('list_lot', 'ListLot', 'VARCHAR', false, 255, null);
        $this->addForeignKey('id_dume_contexte', 'IdDumeContexte', 'INTEGER', 't_dume_contexte', 'id', false, null, null);
        $this->addColumn('date_recuperation_pdf', 'DateRecuperationPdf', 'TIMESTAMP', false, null, null);
        $this->addColumn('blob_id_xml', 'BlobIdXml', 'INTEGER', false, null, null);
        $this->addColumn('date_recuperation_xml', 'DateRecuperationXml', 'TIMESTAMP', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTDumeContexte', 'Application\\Propel\\Mpe\\CommonTDumeContexte', RelationMap::MANY_TO_ONE, array('id_dume_contexte' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonTDumeNumeroTableMap
