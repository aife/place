<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'T_Recherche_Sauvegardee' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTRechercheSauvegardeeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTRechercheSauvegardeeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('T_Recherche_Sauvegardee');
        $this->setPhpName('CommonTRechercheSauvegardee');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTRechercheSauvegardee');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('id_inscrit', 'IdInscrit', 'INTEGER', true, null, 0);
        $this->addColumn('denomination', 'Denomination', 'VARCHAR', true, 200, '');
        $this->addColumn('periodicite', 'Periodicite', 'CHAR', true, null, null);
        $this->addColumn('xmlCriteria', 'Xmlcriteria', 'LONGVARCHAR', false, null, null);
        $this->addColumn('categorie', 'Categorie', 'VARCHAR', false, 30, null);
        $this->addColumn('id_initial', 'IdInitial', 'INTEGER', true, null, 0);
        $this->addColumn('format', 'Format', 'VARCHAR', true, 3, '1');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonTRechercheSauvegardeeTableMap
