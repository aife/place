<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'echange_doc_blob' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEchangeDocBlobTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEchangeDocBlobTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('echange_doc_blob');
        $this->setPhpName('CommonEchangeDocBlob');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEchangeDocBlob');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('echange_doc_id', 'EchangeDocId', 'INTEGER', 'echange_doc', 'id', false, null, null);
        $this->addForeignKey('blob_organisme_id', 'BlobOrganismeId', 'INTEGER', 'blobOrganisme_file', 'id', false, null, null);
        $this->addColumn('chemin', 'Chemin', 'CLOB', false, null, null);
        $this->addColumn('poids', 'Poids', 'INTEGER', false, null, null);
        $this->addColumn('checksum', 'Checksum', 'CHAR', false, 32, null);
        $this->addColumn('echange_type_piece_actes_id', 'EchangeTypePieceActesId', 'INTEGER', false, null, null);
        $this->addForeignKey('echange_type_piece_standard_id', 'EchangeTypePieceStandardId', 'INTEGER', 'echange_doc_type_piece_standard', 'id', false, null, null);
        $this->addColumn('categorie_piece', 'CategoriePiece', 'INTEGER', true, null, null);
        $this->addForeignKey('doc_blob_principal_id', 'DocBlobPrincipalId', 'INTEGER', 'echange_doc_blob', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEchangeDoc', 'Application\\Propel\\Mpe\\CommonEchangeDoc', RelationMap::MANY_TO_ONE, array('echange_doc_id' => 'id', ), null, null);
        $this->addRelation('CommonBlobOrganismeFile', 'Application\\Propel\\Mpe\\CommonBlobOrganismeFile', RelationMap::MANY_TO_ONE, array('blob_organisme_id' => 'id', ), null, null);
        $this->addRelation('CommonEchangeDocBlobRelatedByDocBlobPrincipalId', 'Application\\Propel\\Mpe\\CommonEchangeDocBlob', RelationMap::MANY_TO_ONE, array('doc_blob_principal_id' => 'id', ), null, null);
        $this->addRelation('CommonEchangeDocTypePieceStandard', 'Application\\Propel\\Mpe\\CommonEchangeDocTypePieceStandard', RelationMap::MANY_TO_ONE, array('echange_type_piece_standard_id' => 'id', ), null, null);
        $this->addRelation('CommonEchangeDocBlobRelatedById', 'Application\\Propel\\Mpe\\CommonEchangeDocBlob', RelationMap::ONE_TO_MANY, array('id' => 'doc_blob_principal_id', ), null, null, 'CommonEchangeDocBlobsRelatedById');
    } // buildRelations()

} // CommonEchangeDocBlobTableMap
