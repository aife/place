<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'plateforme_virtuelle' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonPlateformeVirtuelleTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonPlateformeVirtuelleTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('plateforme_virtuelle');
        $this->setPhpName('CommonPlateformeVirtuelle');
        $this->setClassname('Application\\Propel\\Mpe\\CommonPlateformeVirtuelle');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('domain', 'Domain', 'VARCHAR', true, 256, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 256, null);
        $this->addColumn('code_design', 'CodeDesign', 'VARCHAR', false, 25, null);
        $this->addColumn('protocole', 'Protocole', 'VARCHAR', true, 5, 'https');
        $this->addColumn('no_reply', 'NoReply', 'VARCHAR', false, 255, null);
        $this->addColumn('footer_mail', 'FooterMail', 'LONGVARCHAR', false, null, null);
        $this->addColumn('from_pf_name', 'FromPfName', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonOffres', 'Application\\Propel\\Mpe\\CommonOffres', RelationMap::ONE_TO_MANY, array('id' => 'plateforme_virtuelle_id', ), null, null, 'CommonOffress');
        $this->addRelation('CommonTMesRecherches', 'Application\\Propel\\Mpe\\CommonTMesRecherches', RelationMap::ONE_TO_MANY, array('id' => 'plateforme_virtuelle_id', ), null, null, 'CommonTMesRecherchess');
        $this->addRelation('CommonTelechargement', 'Application\\Propel\\Mpe\\CommonTelechargement', RelationMap::ONE_TO_MANY, array('id' => 'plateforme_virtuelle_id', ), null, null, 'CommonTelechargements');
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::ONE_TO_MANY, array('id' => 'plateforme_virtuelle_id', ), null, null, 'CommonConsultations');
        $this->addRelation('CommonPlateformeVirtuelleOrganisme', 'Application\\Propel\\Mpe\\CommonPlateformeVirtuelleOrganisme', RelationMap::ONE_TO_MANY, array('id' => 'plateforme_id', ), 'CASCADE', 'CASCADE', 'CommonPlateformeVirtuelleOrganismes');
        $this->addRelation('CommonQuestionsDce', 'Application\\Propel\\Mpe\\CommonQuestionsDce', RelationMap::ONE_TO_MANY, array('id' => 'plateforme_virtuelle_id', ), null, null, 'CommonQuestionsDces');
    } // buildRelations()

} // CommonPlateformeVirtuelleTableMap
