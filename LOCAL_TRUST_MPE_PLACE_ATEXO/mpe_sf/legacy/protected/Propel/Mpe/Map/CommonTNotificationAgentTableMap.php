<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_notification_agent' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTNotificationAgentTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTNotificationAgentTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_notification_agent');
        $this->setPhpName('CommonTNotificationAgent');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTNotificationAgent');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_notification', 'IdNotification', 'INTEGER', true, null, null);
        $this->addColumn('id_agent', 'IdAgent', 'INTEGER', false, null, null);
        $this->addForeignKey('id_type', 'IdType', 'INTEGER', 't_type_notification_agent', 'id_type', true, null, null);
        $this->addColumn('service_id', 'ServiceId', 'INTEGER', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', false, 45, null);
        $this->addColumn('reference_objet', 'ReferenceObjet', 'VARCHAR', false, 255, null);
        $this->addColumn('libelle_notification', 'LibelleNotification', 'VARCHAR', false, 255, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('notification_lue', 'NotificationLue', 'CHAR', true, null, '0');
        $this->getColumn('notification_lue', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('notification_actif', 'NotificationActif', 'CHAR', true, null, '1');
        $this->getColumn('notification_actif', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('url_notification', 'UrlNotification', 'VARCHAR', false, 500, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_lecture', 'DateLecture', 'TIMESTAMP', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTTypeNotificationAgent', 'Application\\Propel\\Mpe\\CommonTTypeNotificationAgent', RelationMap::MANY_TO_ONE, array('id_type' => 'id_type', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonTNotificationAgentTableMap
