<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'mail_type' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonMailTypeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonMailTypeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('mail_type');
        $this->setPhpName('CommonMailType');
        $this->setClassname('Application\\Propel\\Mpe\\CommonMailType');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('mail_type_group_id', 'MailTypeGroupId', 'INTEGER', 'mail_type_group', 'id', true, null, null);
        $this->addColumn('label', 'Label', 'VARCHAR', true, 255, null);
        $this->addColumn('code', 'Code', 'VARCHAR', true, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonMailTypeGroup', 'Application\\Propel\\Mpe\\CommonMailTypeGroup', RelationMap::MANY_TO_ONE, array('mail_type_group_id' => 'id', ), null, null);
        $this->addRelation('CommonMailTemplate', 'Application\\Propel\\Mpe\\CommonMailTemplate', RelationMap::ONE_TO_MANY, array('id' => 'mail_type_id', ), null, null, 'CommonMailTemplates');
    } // buildRelations()

} // CommonMailTypeTableMap
