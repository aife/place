<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'mail_template' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonMailTemplateTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonMailTemplateTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('mail_template');
        $this->setPhpName('CommonMailTemplate');
        $this->setClassname('Application\\Propel\\Mpe\\CommonMailTemplate');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('mail_type_id', 'MailTypeId', 'INTEGER', 'mail_type', 'id', true, null, null);
        $this->addColumn('code', 'Code', 'VARCHAR', true, 255, null);
        $this->addColumn('objet', 'Objet', 'VARCHAR', true, 255, null);
        $this->addColumn('corps', 'Corps', 'CLOB', true, null, null);
        $this->addColumn('ordre_affichage', 'OrdreAffichage', 'INTEGER', false, null, null);
        $this->addColumn('envoi_modalite', 'EnvoiModalite', 'VARCHAR', true, 255, 'AVEC_AR');
        $this->addColumn('envoi_modalite_figee', 'EnvoiModaliteFigee', 'BOOLEAN', true, 1, false);
        $this->addColumn('reponse_attendue', 'ReponseAttendue', 'BOOLEAN', true, 1, false);
        $this->addColumn('reponse_attendue_figee', 'ReponseAttendueFigee', 'BOOLEAN', true, 1, false);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonMailType', 'Application\\Propel\\Mpe\\CommonMailType', RelationMap::MANY_TO_ONE, array('mail_type_id' => 'id', ), null, null);
    } // buildRelations()

} // CommonMailTemplateTableMap
