<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Pieces_DCE' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonPiecesDCETableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonPiecesDCETableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Pieces_DCE');
        $this->setPhpName('CommonPiecesDCE');
        $this->setClassname('Application\\Propel\\Mpe\\CommonPiecesDCE');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nom_Piece', 'NomPiece', 'VARCHAR', true, 255, null);
        $this->addColumn('id_blob', 'IdBlob', 'INTEGER', true, null, null);
        $this->addColumn('ref_consultation', 'RefConsultation', 'INTEGER', false, null, null);
        $this->addColumn('organisme_consultation', 'OrganismeConsultation', 'VARCHAR', true, 255, null);
        $this->addColumn('document_Redac', 'DocumentRedac', 'CHAR', true, null, '0');
        $this->getColumn('document_Redac', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('id_Redac', 'IdRedac', 'INTEGER', false, null, null);
        $this->addColumn('statut', 'Statut', 'INTEGER', true, null, 0);
        $this->addColumn('Type_Piece', 'TypePiece', 'VARCHAR', false, 255, null);
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonPiecesDCETableMap
