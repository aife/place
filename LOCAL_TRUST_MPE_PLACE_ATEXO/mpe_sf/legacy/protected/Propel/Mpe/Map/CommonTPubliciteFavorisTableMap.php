<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_publicite_favoris' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTPubliciteFavorisTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTPubliciteFavorisTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_publicite_favoris');
        $this->setPhpName('CommonTPubliciteFavoris');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTPubliciteFavoris');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 22, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', true, 30, null);
        $this->addColumn('old_service_id', 'OldServiceId', 'INTEGER', false, null, null);
        $this->addColumn('libelle', 'Libelle', 'VARCHAR', true, 100, '');
        $this->addColumn('regime_financier_cautionnement', 'RegimeFinancierCautionnement', 'CLOB', false, null, null);
        $this->addColumn('regime_financier_modalites_financement', 'RegimeFinancierModalitesFinancement', 'CLOB', false, null, null);
        $this->addColumn('acheteur_correspondant', 'AcheteurCorrespondant', 'VARCHAR', true, 100, '');
        $this->addColumn('acheteur_nom_organisme', 'AcheteurNomOrganisme', 'VARCHAR', false, 200, null);
        $this->addColumn('acheteur_adresse', 'AcheteurAdresse', 'VARCHAR', true, 100, '');
        $this->addColumn('acheteur_cp', 'AcheteurCp', 'VARCHAR', true, 5, '');
        $this->addColumn('acheteur_ville', 'AcheteurVille', 'VARCHAR', true, 100, '');
        $this->addColumn('acheteur_url', 'AcheteurUrl', 'VARCHAR', true, 100, '');
        $this->addColumn('facture_denomination', 'FactureDenomination', 'VARCHAR', false, 100, null);
        $this->addColumn('facture_adresse', 'FactureAdresse', 'VARCHAR', true, 255, '');
        $this->addColumn('facture_cp', 'FactureCp', 'VARCHAR', true, 10, '');
        $this->addColumn('facture_ville', 'FactureVille', 'VARCHAR', true, 100, '');
        $this->addColumn('instance_recours_organisme', 'InstanceRecoursOrganisme', 'VARCHAR', false, 200, null);
        $this->addColumn('instance_recours_adresse', 'InstanceRecoursAdresse', 'VARCHAR', false, 200, null);
        $this->addColumn('instance_recours_cp', 'InstanceRecoursCp', 'VARCHAR', false, 200, null);
        $this->addColumn('instance_recours_ville', 'InstanceRecoursVille', 'VARCHAR', false, 200, null);
        $this->addColumn('instance_recours_url', 'InstanceRecoursUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('acheteur_telephone', 'AcheteurTelephone', 'VARCHAR', false, 20, null);
        $this->addColumn('acheteur_email', 'AcheteurEmail', 'VARCHAR', false, 100, null);
        $this->addColumn('service_id', 'ServiceId', 'BIGINT', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonTPubliciteFavorisTableMap
