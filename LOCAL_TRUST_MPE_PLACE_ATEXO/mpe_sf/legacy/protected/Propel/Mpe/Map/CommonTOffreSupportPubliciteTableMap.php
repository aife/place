<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_Offre_Support_Publicite' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTOffreSupportPubliciteTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTOffreSupportPubliciteTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_Offre_Support_Publicite');
        $this->setPhpName('CommonTOffreSupportPublicite');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTOffreSupportPublicite');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('libelle_offre', 'LibelleOffre', 'VARCHAR', false, 255, null);
        $this->addForeignKey('id_support', 'IdSupport', 'INTEGER', 't_support_publication', 'id', true, null, null);
        $this->addColumn('actif', 'Actif', 'CHAR', true, null, '0');
        $this->getColumn('actif', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('logo', 'Logo', 'VARCHAR', false, 255, null);
        $this->addColumn('mapa', 'Mapa', 'INTEGER', true, null, 0);
        $this->addColumn('montant_inf', 'MontantInf', 'INTEGER', true, null, 0);
        $this->addColumn('montant_max', 'MontantMax', 'INTEGER', true, null, 0);
        $this->addColumn('code', 'Code', 'VARCHAR', true, 255, '');
        $this->addColumn('ordre', 'Ordre', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTSupportPublication', 'Application\\Propel\\Mpe\\CommonTSupportPublication', RelationMap::MANY_TO_ONE, array('id_support' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonTOffreSupportPubliciteTableMap
