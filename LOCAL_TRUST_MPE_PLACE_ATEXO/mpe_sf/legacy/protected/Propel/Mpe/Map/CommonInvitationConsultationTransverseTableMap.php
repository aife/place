<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'InvitationConsultationTransverse' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonInvitationConsultationTransverseTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonInvitationConsultationTransverseTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('InvitationConsultationTransverse');
        $this->setPhpName('CommonInvitationConsultationTransverse');
        $this->setClassname('Application\\Propel\\Mpe\\CommonInvitationConsultationTransverse');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('organisme_emetteur', 'OrganismeEmetteur', 'VARCHAR', true, 30, '');
        $this->addColumn('reference', 'Reference', 'VARCHAR', true, 255, '');
        $this->addColumn('organisme_invite', 'OrganismeInvite', 'VARCHAR', true, 30, '');
        $this->addColumn('lot', 'Lot', 'INTEGER', true, null, 0);
        $this->addColumn('date_decision', 'DateDecision', 'DATE', false, null, null);
        $this->addColumn('id_contrat_titulaire', 'IdContratTitulaire', 'INTEGER', false, null, null);
        $this->addColumn('consultation_id', 'ConsultationId', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonInvitationConsultationTransverseTableMap
