<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'consultation_archive_bloc' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonConsultationArchiveBlocTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonConsultationArchiveBlocTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('consultation_archive_bloc');
        $this->setPhpName('CommonConsultationArchiveBloc');
        $this->setClassname('Application\\Propel\\Mpe\\CommonConsultationArchiveBloc');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('consultation_archive_id', 'ConsultationArchiveId', 'INTEGER', 'consultation_archive', 'id', true, null, null);
        $this->addColumn('doc_id', 'DocId', 'VARCHAR', true, 256, null);
        $this->addColumn('chemin_fichier', 'CheminFichier', 'VARCHAR', false, 256, null);
        $this->addColumn('numero_bloc', 'NumeroBloc', 'INTEGER', false, null, null);
        $this->addColumn('poids_bloc', 'PoidsBloc', 'INTEGER', false, null, null);
        $this->addColumn('date_envoi_debut', 'DateEnvoiDebut', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_envoi_fin', 'DateEnvoiFin', 'TIMESTAMP', false, null, null);
        $this->addColumn('status_transmission', 'StatusTransmission', 'BOOLEAN', true, 1, false);
        $this->addColumn('erreur', 'Erreur', 'VARCHAR', false, 256, null);
        $this->addColumn('comp_id', 'CompId', 'VARCHAR', false, 256, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonConsultationArchive', 'Application\\Propel\\Mpe\\CommonConsultationArchive', RelationMap::MANY_TO_ONE, array('consultation_archive_id' => 'id', ), null, null);
    } // buildRelations()

} // CommonConsultationArchiveBlocTableMap
