<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'EnchereOffreReference' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEnchereOffreReferenceTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEnchereOffreReferenceTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('EnchereOffreReference');
        $this->setPhpName('CommonEnchereOffreReference');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEnchereOffreReference');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addForeignPrimaryKey('organisme', 'Organisme', 'VARCHAR' , 'EnchereReference', 'organisme', true, 30, '');
        $this->addColumn('old_id_enchere_offre', 'OldIdEnchereOffre', 'INTEGER', false, null, null);
        $this->addForeignKey('idEnchereReference', 'Idencherereference', 'INTEGER', 'EnchereReference', 'id', true, 10, 0);
        $this->addForeignKey('idEnchereReference', 'Idencherereference', 'INTEGER', 'EnchereReference', 'id', true, 10, 0);
        $this->addColumn('valeur', 'Valeur', 'DOUBLE', true, null, 0);
        $this->addForeignKey('id_enchere_offre', 'IdEnchereOffre', 'BIGINT', 'EnchereOffre', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEnchereReferenceRelatedByIdencherereferenceOrganisme', 'Application\\Propel\\Mpe\\CommonEnchereReference', RelationMap::MANY_TO_ONE, array('idEnchereReference' => 'id', 'organisme' => 'organisme', ), null, 'CASCADE');
        $this->addRelation('CommonEnchereReferenceRelatedByIdencherereference', 'Application\\Propel\\Mpe\\CommonEnchereReference', RelationMap::MANY_TO_ONE, array('idEnchereReference' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonEnchereOffre', 'Application\\Propel\\Mpe\\CommonEnchereOffre', RelationMap::MANY_TO_ONE, array('id_enchere_offre' => 'id', ), null, 'CASCADE');
    } // buildRelations()

} // CommonEnchereOffreReferenceTableMap
