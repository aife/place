<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'habilitation_type_procedure' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonHabilitationTypeProcedureTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonHabilitationTypeProcedureTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('habilitation_type_procedure');
        $this->setPhpName('CommonHabilitationTypeProcedure');
        $this->setClassname('Application\\Propel\\Mpe\\CommonHabilitationTypeProcedure');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('habilitation_id', 'HabilitationId', 'INTEGER', 'referentiel_habilitation', 'id', true, null, null);
        $this->addForeignKey('agent_id', 'AgentId', 'INTEGER', 'Agent', 'id', true, null, null);
        $this->addForeignKey('organisme', 'Organisme', 'INTEGER', 'Organisme', 'id', false, null, null);
        $this->addForeignKey('typeProcedure', 'Typeprocedure', 'INTEGER', 'TypeProcedure', 'id_type_procedure', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonAgent', 'Application\\Propel\\Mpe\\CommonAgent', RelationMap::MANY_TO_ONE, array('agent_id' => 'id', ), null, null);
        $this->addRelation('CommonReferentielHabilitation', 'Application\\Propel\\Mpe\\CommonReferentielHabilitation', RelationMap::MANY_TO_ONE, array('habilitation_id' => 'id', ), null, null);
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'id', ), null, null);
        $this->addRelation('CommonTypeProcedure', 'Application\\Propel\\Mpe\\CommonTypeProcedure', RelationMap::MANY_TO_ONE, array('typeProcedure' => 'id_type_procedure', ), null, null);
    } // buildRelations()

} // CommonHabilitationTypeProcedureTableMap
