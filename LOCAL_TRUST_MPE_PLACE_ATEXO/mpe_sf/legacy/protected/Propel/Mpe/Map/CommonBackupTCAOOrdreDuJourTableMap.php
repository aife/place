<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'backup_t_CAO_Ordre_Du_Jour' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonBackupTCAOOrdreDuJourTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonBackupTCAOOrdreDuJourTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('backup_t_CAO_Ordre_Du_Jour');
        $this->setPhpName('CommonBackupTCAOOrdreDuJour');
        $this->setClassname('Application\\Propel\\Mpe\\CommonBackupTCAOOrdreDuJour');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_ordre_du_jour', 'IdOrdreDuJour', 'BIGINT', true, null, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 30, null);
        $this->addColumn('id_seance', 'IdSeance', 'BIGINT', true, null, null);
        $this->addColumn('id_commission', 'IdCommission', 'BIGINT', true, null, null);
        $this->addColumn('id_commission_consultation', 'IdCommissionConsultation', 'BIGINT', true, 11, null);
        $this->addColumn('numero', 'Numero', 'TINYINT', true, null, null);
        $this->addColumn('id_ref_org_val_etape', 'IdRefOrgValEtape', 'INTEGER', true, null, null);
        $this->addColumn('intitule', 'Intitule', 'CLOB', true, null, null);
        $this->addColumn('date_seance', 'DateSeance', 'TIMESTAMP', true, null, null);
        $this->addColumn('heure_passage', 'HeurePassage', 'TIME', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonBackupTCAOOrdreDuJourTableMap
