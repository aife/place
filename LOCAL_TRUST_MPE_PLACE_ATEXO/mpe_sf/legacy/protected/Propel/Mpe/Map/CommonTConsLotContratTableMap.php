<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_cons_lot_contrat' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTConsLotContratTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTConsLotContratTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_cons_lot_contrat');
        $this->setPhpName('CommonTConsLotContrat');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTConsLotContrat');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_cons_lot_contrat', 'IdConsLotContrat', 'INTEGER', true, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', false, 45, null);
        $this->addColumn('consultation_ref', 'ConsultationRef', 'INTEGER', false, null, null);
        $this->addColumn('lot', 'Lot', 'INTEGER', false, null, null);
        $this->addForeignKey('id_contrat_titulaire', 'IdContratTitulaire', 'INTEGER', 't_contrat_titulaire', 'id_contrat_titulaire', true, null, null);
        $this->addColumn('consultation_id', 'ConsultationId', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTContratTitulaire', 'Application\\Propel\\Mpe\\CommonTContratTitulaire', RelationMap::MANY_TO_ONE, array('id_contrat_titulaire' => 'id_contrat_titulaire', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonTConsLotContratTableMap
