<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_reponse_elec_formulaire' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTReponseElecFormulaireTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTReponseElecFormulaireTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_reponse_elec_formulaire');
        $this->setPhpName('CommonTReponseElecFormulaire');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTReponseElecFormulaire');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_reponse_elec_formulaire', 'IdReponseElecFormulaire', 'INTEGER', true, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', true, 30, null);
        $this->addColumn('consultation_ref', 'ConsultationRef', 'INTEGER', false, null, null);
        $this->addForeignKey('id_entreprise', 'IdEntreprise', 'INTEGER', 'Entreprise', 'id', true, null, null);
        $this->addColumn('old_id_inscrit', 'OldIdInscrit', 'INTEGER', false, null, null);
        $this->addColumn('statut_validation_globale', 'StatutValidationGlobale', 'INTEGER', true, 1, null);
        $this->addForeignKey('consultation_id', 'ConsultationId', 'INTEGER', 'consultation', 'id', false, null, null);
        $this->addForeignKey('id_inscrit', 'IdInscrit', 'BIGINT', 'Inscrit', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entreprise', 'Application\\Propel\\Mpe\\Entreprise', RelationMap::MANY_TO_ONE, array('id_entreprise' => 'id', ), null, null);
        $this->addRelation('CommonConsultation', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('consultation_id' => 'id', ), null, null);
        $this->addRelation('CommonInscrit', 'Application\\Propel\\Mpe\\CommonInscrit', RelationMap::MANY_TO_ONE, array('id_inscrit' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonTDossierFormulaire', 'Application\\Propel\\Mpe\\CommonTDossierFormulaire', RelationMap::ONE_TO_MANY, array('id_reponse_elec_formulaire' => 'id_reponse_elec_formulaire', ), null, null, 'CommonTDossierFormulaires');
    } // buildRelations()

} // CommonTReponseElecFormulaireTableMap
