<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'blob_file' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonBlobFileTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonBlobFileTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('blob_file');
        $this->setPhpName('CommonBlobFile');
        $this->setClassname('Application\\Propel\\Mpe\\CommonBlobFile');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('old_id', 'OldId', 'INTEGER', false, null, null);
        $this->addColumn('name', 'Name', 'LONGVARCHAR', true, null, null);
        $this->addColumn('deletion_datetime', 'DeletionDatetime', 'TIMESTAMP', false, null, null);
        $this->addColumn('chemin', 'Chemin', 'LONGVARCHAR', false, null, null);
        $this->addColumn('dossier', 'Dossier', 'VARCHAR', false, 256, null);
        $this->addColumn('statut_synchro', 'StatutSynchro', 'INTEGER', true, null, 0);
        $this->addColumn('hash', 'Hash', 'VARCHAR', false, 256, 'ND');
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('extension', 'Extension', 'VARCHAR', true, 50, '');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonDossierVolumineuxRelatedByIdBlobDescripteur', 'Application\\Propel\\Mpe\\CommonDossierVolumineux', RelationMap::ONE_TO_MANY, array('id' => 'id_blob_descripteur', ), null, null, 'CommonDossierVolumineuxsRelatedByIdBlobDescripteur');
        $this->addRelation('CommonDossierVolumineuxRelatedByIdBlobLogfile', 'Application\\Propel\\Mpe\\CommonDossierVolumineux', RelationMap::ONE_TO_MANY, array('id' => 'id_blob_logfile', ), null, null, 'CommonDossierVolumineuxsRelatedByIdBlobLogfile');
    } // buildRelations()

} // CommonBlobFileTableMap
