<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'EnchereOffre' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonEnchereOffreTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonEnchereOffreTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('EnchereOffre');
        $this->setPhpName('CommonEnchereOffre');
        $this->setClassname('Application\\Propel\\Mpe\\CommonEnchereOffre');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('old_id', 'OldId', 'INTEGER', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', true, 30, null);
        $this->addColumn('old_id_enchere', 'OldIdEnchere', 'INTEGER', false, null, null);
        $this->addColumn('old_id_enchere_entreprise', 'OldIdEnchereEntreprise', 'INTEGER', false, null, null);
        $this->addColumn('date', 'Date', 'VARCHAR', false, 20, '0000-00-00 00:00:00');
        $this->addColumn('valeurTIC', 'Valeurtic', 'DOUBLE', false, null, null);
        $this->addColumn('valeurTC', 'Valeurtc', 'DOUBLE', false, null, null);
        $this->addColumn('valeurNETC', 'Valeurnetc', 'DOUBLE', false, null, null);
        $this->addColumn('valeurNGC', 'Valeurngc', 'DOUBLE', false, null, null);
        $this->addColumn('rang', 'Rang', 'INTEGER', false, null, null);
        $this->addPrimaryKey('id', 'Id', 'BIGINT', true, null, null);
        $this->addForeignKey('idEnchere', 'Idenchere', 'BIGINT', 'EncherePmi', 'id', false, null, null);
        $this->addForeignKey('idEnchereEntreprise', 'Idenchereentreprise', 'BIGINT', 'EnchereEntreprisePmi', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEncherePmi', 'Application\\Propel\\Mpe\\CommonEncherePmi', RelationMap::MANY_TO_ONE, array('idEnchere' => 'id', ), null, null);
        $this->addRelation('CommonEnchereEntreprisePmi', 'Application\\Propel\\Mpe\\CommonEnchereEntreprisePmi', RelationMap::MANY_TO_ONE, array('idEnchereEntreprise' => 'id', ), null, null);
        $this->addRelation('CommonEnchereOffreReference', 'Application\\Propel\\Mpe\\CommonEnchereOffreReference', RelationMap::ONE_TO_MANY, array('id' => 'id_enchere_offre', ), null, 'CASCADE', 'CommonEnchereOffreReferences');
    } // buildRelations()

} // CommonEnchereOffreTableMap
