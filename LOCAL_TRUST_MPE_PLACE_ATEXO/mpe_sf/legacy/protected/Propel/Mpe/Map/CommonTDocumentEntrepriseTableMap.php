<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_document_entreprise' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTDocumentEntrepriseTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTDocumentEntrepriseTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_document_entreprise');
        $this->setPhpName('CommonTDocumentEntreprise');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTDocumentEntreprise');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_document', 'IdDocument', 'INTEGER', true, null, null);
        $this->addForeignKey('id_entreprise', 'IdEntreprise', 'INTEGER', 'Entreprise', 'id', true, null, null);
        $this->addColumn('id_etablissement', 'IdEtablissement', 'INTEGER', true, null, null);
        $this->addColumn('nom_document', 'NomDocument', 'VARCHAR', true, 100, null);
        $this->addForeignKey('id_type_document', 'IdTypeDocument', 'INTEGER', 't_document_type', 'id_type_document', true, null, null);
        $this->addColumn('id_derniere_version', 'IdDerniereVersion', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Entreprise', 'Application\\Propel\\Mpe\\Entreprise', RelationMap::MANY_TO_ONE, array('id_entreprise' => 'id', ), null, null);
        $this->addRelation('CommonTDocumentType', 'Application\\Propel\\Mpe\\CommonTDocumentType', RelationMap::MANY_TO_ONE, array('id_type_document' => 'id_type_document', ), null, null);
        $this->addRelation('CommonTEntrepriseDocumentVersion', 'Application\\Propel\\Mpe\\CommonTEntrepriseDocumentVersion', RelationMap::ONE_TO_MANY, array('id_document' => 'id_document', ), null, null, 'CommonTEntrepriseDocumentVersions');
    } // buildRelations()

} // CommonTDocumentEntrepriseTableMap
