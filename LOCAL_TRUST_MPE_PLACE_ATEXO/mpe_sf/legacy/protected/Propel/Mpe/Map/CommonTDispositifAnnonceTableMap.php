<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_dispositif_annonce' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTDispositifAnnonceTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTDispositifAnnonceTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_dispositif_annonce');
        $this->setPhpName('CommonTDispositifAnnonce');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTDispositifAnnonce');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('libelle', 'Libelle', 'VARCHAR', true, 255, null);
        $this->addColumn('id_externe', 'IdExterne', 'INTEGER', true, null, null);
        $this->addColumn('type', 'Type', 'INTEGER', false, null, null);
        $this->addColumn('visible', 'Visible', 'CHAR', true, null, '1');
        $this->getColumn('visible', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('id_dispositif_rectificatif', 'IdDispositifRectificatif', 'INTEGER', false, null, null);
        $this->addColumn('id_dispositif_annulation', 'IdDispositifAnnulation', 'INTEGER', false, null, null);
        $this->addColumn('sigle', 'Sigle', 'VARCHAR', false, 255, null);
        $this->addColumn('id_type_avis_rectificatif', 'IdTypeAvisRectificatif', 'INTEGER', false, null, null);
        $this->addColumn('id_type_avis_annulation', 'IdTypeAvisAnnulation', 'INTEGER', false, null, null);
        $this->addColumn('libelle_formulaire_propose', 'LibelleFormulairePropose', 'VARCHAR', true, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonTDispositifAnnonceTableMap
