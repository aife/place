<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_decision_selection_entreprise' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTDecisionSelectionEntrepriseTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTDecisionSelectionEntrepriseTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_decision_selection_entreprise');
        $this->setPhpName('CommonTDecisionSelectionEntreprise');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTDecisionSelectionEntreprise');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('consultation_ref', 'ConsultationRef', 'INTEGER', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', false, 45, null);
        $this->addColumn('old_service_id', 'OldServiceId', 'INTEGER', false, null, null);
        $this->addColumn('lot', 'Lot', 'INTEGER', false, null, null);
        $this->addColumn('id_entreprise', 'IdEntreprise', 'INTEGER', false, null, null);
        $this->addColumn('id_etablissement', 'IdEtablissement', 'INTEGER', false, null, null);
        $this->addColumn('id_offre', 'IdOffre', 'INTEGER', false, null, null);
        $this->addColumn('type_depot_reponse', 'TypeDepotReponse', 'CHAR', false, null, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_modification', 'DateModification', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('id_contact_contrat', 'IdContactContrat', 'INTEGER', 't_contact_contrat', 'id_contact_contrat', false, null, null);
        $this->addColumn('consultation_id', 'ConsultationId', 'INTEGER', false, null, null);
        $this->addColumn('service_id', 'ServiceId', 'BIGINT', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTContactContrat', 'Application\\Propel\\Mpe\\CommonTContactContrat', RelationMap::MANY_TO_ONE, array('id_contact_contrat' => 'id_contact_contrat', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonTDecisionSelectionEntrepriseTableMap
