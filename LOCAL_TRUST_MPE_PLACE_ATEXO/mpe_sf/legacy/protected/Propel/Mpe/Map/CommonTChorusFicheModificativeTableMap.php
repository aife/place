<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_chorus_fiche_modificative' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTChorusFicheModificativeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTChorusFicheModificativeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_chorus_fiche_modificative');
        $this->setPhpName('CommonTChorusFicheModificative');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTChorusFicheModificative');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_fiche_modificative', 'IdFicheModificative', 'INTEGER', true, null, null);
        $this->addForeignKey('id_echange', 'IdEchange', 'INTEGER', 'Chorus_echange', 'id', true, null, null);
        $this->addForeignKey('organisme', 'Organisme', 'VARCHAR', 'Chorus_echange', 'organisme', true, 30, '');
        $this->addColumn('type_modification', 'TypeModification', 'INTEGER', false, null, null);
        $this->addColumn('date_prevue_notification', 'DatePrevueNotification', 'DATE', false, null, null);
        $this->addColumn('date_fin_marche', 'DateFinMarche', 'DATE', false, null, null);
        $this->addColumn('date_fin_marche_modifie', 'DateFinMarcheModifie', 'DATE', false, null, null);
        $this->addColumn('montant_marche', 'MontantMarche', 'DOUBLE', false, null, null);
        $this->addColumn('montant_acte', 'MontantActe', 'DOUBLE', false, null, null);
        $this->addColumn('taux_tva', 'TauxTva', 'VARCHAR', false, 10, null);
        $this->addColumn('nombre_fournisseur_cotraitant', 'NombreFournisseurCotraitant', 'VARCHAR', false, 250, null);
        $this->addColumn('localites_fournisseurs', 'LocalitesFournisseurs', 'VARCHAR', false, 250, null);
        $this->addColumn('siren_fournisseur', 'SirenFournisseur', 'LONGVARCHAR', false, null, null);
        $this->addColumn('siret_fournisseur', 'SiretFournisseur', 'LONGVARCHAR', false, null, null);
        $this->addColumn('nom_fournisseur', 'NomFournisseur', 'LONGVARCHAR', false, null, null);
        $this->addColumn('type_fournisseur', 'TypeFournisseur', 'LONGVARCHAR', false, null, null);
        $this->addColumn('visa_accf', 'VisaAccf', 'CHAR', true, null, '0');
        $this->getColumn('visa_accf', false)->setValueSet(array (
  0 => '0',
  1 => '1',
  2 => '2',
));
        $this->addColumn('visa_prefet', 'VisaPrefet', 'CHAR', true, null, '0');
        $this->getColumn('visa_prefet', false)->setValueSet(array (
  0 => '0',
  1 => '1',
  2 => '2',
));
        $this->addColumn('remarque', 'Remarque', 'LONGVARCHAR', false, null, null);
        $this->addColumn('id_blob_piece_justificatives', 'IdBlobPieceJustificatives', 'LONGVARCHAR', false, null, null);
        $this->addColumn('id_blob_fiche_modificative', 'IdBlobFicheModificative', 'LONGVARCHAR', false, null, null);
        $this->addColumn('date_creation', 'DateCreation', 'TIMESTAMP', false, null, null);
        $this->addColumn('date_modification', 'DateModification', 'TIMESTAMP', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonChorusEchange', 'Application\\Propel\\Mpe\\CommonChorusEchange', RelationMap::MANY_TO_ONE, array('id_echange' => 'id', 'organisme' => 'organisme', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonTChorusFicheModificativePj', 'Application\\Propel\\Mpe\\CommonTChorusFicheModificativePj', RelationMap::ONE_TO_MANY, array('id_fiche_modificative' => 'id_fiche_modificative', ), 'CASCADE', 'CASCADE', 'CommonTChorusFicheModificativePjs');
    } // buildRelations()

} // CommonTChorusFicheModificativeTableMap
