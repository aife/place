<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_entreprise_document_version' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTEntrepriseDocumentVersionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTEntrepriseDocumentVersionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_entreprise_document_version');
        $this->setPhpName('CommonTEntrepriseDocumentVersion');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTEntrepriseDocumentVersion');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_version_document', 'IdVersionDocument', 'INTEGER', true, null, null);
        $this->addForeignKey('id_document', 'IdDocument', 'INTEGER', 't_document_entreprise', 'id_document', true, null, null);
        $this->addColumn('date_recuperation', 'DateRecuperation', 'TIMESTAMP', true, null, null);
        $this->addColumn('hash', 'Hash', 'VARCHAR', false, 500, null);
        $this->addColumn('id_blob', 'IdBlob', 'INTEGER', false, null, null);
        $this->addColumn('taille_document', 'TailleDocument', 'VARCHAR', false, 50, null);
        $this->addColumn('extension_document', 'ExtensionDocument', 'VARCHAR', false, 50, null);
        $this->addColumn('jeton_document', 'JetonDocument', 'LONGVARCHAR', false, null, null);
        $this->addColumn('json_document', 'JsonDocument', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTDocumentEntreprise', 'Application\\Propel\\Mpe\\CommonTDocumentEntreprise', RelationMap::MANY_TO_ONE, array('id_document' => 'id_document', ), null, null);
    } // buildRelations()

} // CommonTEntrepriseDocumentVersionTableMap
