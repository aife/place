<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Relation_Echange' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonRelationEchangeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonRelationEchangeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Relation_Echange');
        $this->setPhpName('CommonRelationEchange');
        $this->setClassname('Application\\Propel\\Mpe\\CommonRelationEchange');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_auto', 'IdAuto', 'INTEGER', true, null, null);
        $this->addColumn('old_id_echange', 'OldIdEchange', 'INTEGER', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', true, 30, '');
        $this->addColumn('id_externe', 'IdExterne', 'INTEGER', true, null, 0);
        $this->addColumn('type_relation', 'TypeRelation', 'INTEGER', true, null, 0);
        $this->addColumn('date_envoi', 'DateEnvoi', 'VARCHAR', false, 50, null);
        $this->addForeignKey('id_echange', 'IdEchange', 'BIGINT', 'Echange', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonEchange', 'Application\\Propel\\Mpe\\CommonEchange', RelationMap::MANY_TO_ONE, array('id_echange' => 'id', ), null, null);
    } // buildRelations()

} // CommonRelationEchangeTableMap
