<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'consultation' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonConsultationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonConsultationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('consultation');
        $this->setPhpName('CommonConsultation');
        $this->setClassname('Application\\Propel\\Mpe\\CommonConsultation');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('reference', 'Reference', 'INTEGER', false, null, null);
        $this->addColumn('code_externe', 'CodeExterne', 'VARCHAR', false, 225, null);
        $this->addForeignKey('organisme', 'Organisme', 'VARCHAR', 'Organisme', 'acronyme', true, 30, null);
        $this->addColumn('reference_utilisateur', 'ReferenceUtilisateur', 'VARCHAR', true, 255, '');
        $this->addColumn('categorie', 'Categorie', 'VARCHAR', false, 30, null);
        $this->addColumn('titre', 'Titre', 'CLOB', false, null, null);
        $this->addColumn('resume', 'Resume', 'CLOB', false, null, null);
        $this->addColumn('datedebut', 'Datedebut', 'VARCHAR', true, 10, '0000-00-00');
        $this->addColumn('datefin', 'Datefin', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('datevalidation', 'Datevalidation', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('type_procedure', 'TypeProcedure', 'CHAR', true, null, '');
        $this->addColumn('code_procedure', 'CodeProcedure', 'VARCHAR', false, 15, null);
        $this->addColumn('reponse_electronique', 'ReponseElectronique', 'CHAR', true, null, '1');
        $this->addColumn('num_procedure', 'NumProcedure', 'INTEGER', true, 1, 0);
        $this->addColumn('id_type_procedure', 'IdTypeProcedure', 'INTEGER', false, null, null);
        $this->addColumn('id_type_avis', 'IdTypeAvis', 'INTEGER', true, 2, 0);
        $this->addColumn('lieu_execution', 'LieuExecution', 'LONGVARCHAR', true, null, null);
        $this->addColumn('type_mise_en_ligne', 'TypeMiseEnLigne', 'INTEGER', true, null, 1);
        $this->addColumn('datemiseenligne', 'Datemiseenligne', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('is_tiers_avis', 'IsTiersAvis', 'CHAR', true, null, '0');
        $this->addColumn('url', 'Url', 'VARCHAR', true, 255, '');
        $this->addColumn('datefin_sad', 'DatefinSad', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('is_sys_acq_dyn', 'IsSysAcqDyn', 'INTEGER', true, null, 0);
        $this->addForeignKey('reference_consultation_init', 'ReferenceConsultationInit', 'INTEGER', 'consultation', 'id', false, null, null);
        $this->addColumn('signature_offre', 'SignatureOffre', 'CHAR', false, null, null);
        $this->addColumn('id_type_validation', 'IdTypeValidation', 'INTEGER', true, null, 2);
        $this->addColumn('etat_approbation', 'EtatApprobation', 'CHAR', true, null, '0');
        $this->getColumn('etat_approbation', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('etat_validation', 'EtatValidation', 'CHAR', true, null, '0');
        $this->getColumn('etat_validation', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('champ_supp_invisible', 'ChampSuppInvisible', 'LONGVARCHAR', true, null, null);
        $this->addColumn('code_cpv_1', 'CodeCpv1', 'VARCHAR', false, 8, null);
        $this->addColumn('code_cpv_2', 'CodeCpv2', 'VARCHAR', false, 255, null);
        $this->addColumn('publication_europe', 'PublicationEurope', 'CHAR', false, null, '0');
        $this->getColumn('publication_europe', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('etat_publication', 'EtatPublication', 'INTEGER', true, null, 0);
        $this->addColumn('poursuivre_affichage', 'PoursuivreAffichage', 'INTEGER', true, null, 0);
        $this->addColumn('poursuivre_affichage_unite', 'PoursuivreAffichageUnite', 'CHAR', true, null, 'DAY');
        $this->getColumn('poursuivre_affichage_unite', false)->setValueSet(array (
  0 => 'MINUTE',
  1 => 'HOUR',
  2 => 'DAY',
  3 => 'MONTH',
  4 => 'YEAR',
));
        $this->addColumn('nbr_telechargement_dce', 'NbrTelechargementDce', 'INTEGER', false, 5, null);
        $this->addColumn('old_service_id', 'OldServiceId', 'INTEGER', false, null, null);
        $this->addColumn('old_service_associe_id', 'OldServiceAssocieId', 'INTEGER', false, null, null);
        $this->addColumn('detail_consultation', 'DetailConsultation', 'LONGVARCHAR', true, null, null);
        $this->addColumn('date_fin_affichage', 'DateFinAffichage', 'VARCHAR', true, 20, '0000-00-00 00:00:00');
        $this->addColumn('depouillable_phase_consultation', 'DepouillablePhaseConsultation', 'CHAR', true, null, '0');
        $this->getColumn('depouillable_phase_consultation', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('consultation_transverse', 'ConsultationTransverse', 'CHAR', true, null, '0');
        $this->getColumn('consultation_transverse', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('consultation_achat_publique', 'ConsultationAchatPublique', 'CHAR', true, null, '0');
        $this->getColumn('consultation_achat_publique', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('url_consultation_achat_publique', 'UrlConsultationAchatPublique', 'LONGVARCHAR', false, null, null);
        $this->addColumn('partial_dce_download', 'PartialDceDownload', 'CHAR', true, null, '0');
        $this->addColumn('tirage_plan', 'TiragePlan', 'INTEGER', true, null, 0);
        $this->addColumn('tireur_plan', 'TireurPlan', 'INTEGER', true, null, 0);
        $this->addColumn('date_mise_en_ligne_calcule', 'DateMiseEnLigneCalcule', 'TIMESTAMP', false, null, null);
        $this->addColumn('accessibilite_en', 'AccessibiliteEn', 'CHAR', true, null, '0');
        $this->addColumn('accessibilite_es', 'AccessibiliteEs', 'CHAR', true, null, '0');
        $this->addColumn('nbr_reponse', 'NbrReponse', 'INTEGER', false, 5, null);
        $this->addColumn('id_type_procedure_org', 'IdTypeProcedureOrg', 'INTEGER', true, 1, 0);
        $this->addColumn('organisme_consultation_init', 'OrganismeConsultationInit', 'VARCHAR', true, 255, '');
        $this->addColumn('tirage_descriptif', 'TirageDescriptif', 'CLOB', true, null, null);
        $this->addColumn('date_validation_intermediaire', 'DateValidationIntermediaire', 'VARCHAR', false, 20, null);
        $this->addColumn('accessibilite_fr', 'AccessibiliteFr', 'CHAR', true, null, '0');
        $this->getColumn('accessibilite_fr', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('id_tr_accessibilite', 'IdTrAccessibilite', 'INTEGER', false, null, null);
        $this->addColumn('accessibilite_cz', 'AccessibiliteCz', 'CHAR', true, null, '0');
        $this->getColumn('accessibilite_cz', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('accessibilite_du', 'AccessibiliteDu', 'CHAR', true, null, '0');
        $this->getColumn('accessibilite_du', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('accessibilite_su', 'AccessibiliteSu', 'CHAR', true, null, '0');
        $this->getColumn('accessibilite_su', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('accessibilite_ar', 'AccessibiliteAr', 'CHAR', true, null, '0');
        $this->getColumn('accessibilite_ar', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alloti', 'Alloti', 'CHAR', true, null, '0');
        $this->getColumn('alloti', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('numero_phase', 'NumeroPhase', 'INTEGER', true, 3, 0);
        $this->addColumn('consultation_externe', 'ConsultationExterne', 'CHAR', true, null, '0');
        $this->getColumn('consultation_externe', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('url_consultation_externe', 'UrlConsultationExterne', 'LONGVARCHAR', false, null, null);
        $this->addColumn('org_denomination', 'OrgDenomination', 'VARCHAR', false, 250, null);
        $this->addColumn('domaines_activites', 'DomainesActivites', 'VARCHAR', false, 250, '');
        $this->addColumn('id_affaire', 'IdAffaire', 'INTEGER', false, null, null);
        $this->addColumn('adresse_retrais_dossiers', 'AdresseRetraisDossiers', 'CLOB', false, null, null);
        $this->addColumn('caution_provisoire', 'CautionProvisoire', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_depot_offres', 'AdresseDepotOffres', 'CLOB', false, null, null);
        $this->addColumn('lieu_ouverture_plis', 'LieuOuverturePlis', 'CLOB', false, null, null);
        $this->addColumn('prix_aquisition_plans', 'PrixAquisitionPlans', 'VARCHAR', false, 255, null);
        $this->addColumn('qualification', 'Qualification', 'VARCHAR', false, 255, null);
        $this->addColumn('agrements', 'Agrements', 'VARCHAR', false, 255, null);
        $this->addColumn('add_echantillion', 'AddEchantillion', 'VARCHAR', false, 255, null);
        $this->addColumn('date_limite_echantillion', 'DateLimiteEchantillion', 'VARCHAR', false, 50, null);
        $this->addColumn('add_reunion', 'AddReunion', 'VARCHAR', false, 255, null);
        $this->addColumn('date_reunion', 'DateReunion', 'VARCHAR', false, 50, null);
        $this->addColumn('variantes', 'Variantes', 'CHAR', false, null, null);
        $this->addColumn('adresse_depot_offres_ar', 'AdresseDepotOffresAr', 'VARCHAR', false, 255, null);
        $this->addColumn('lieu_ouverture_plis_ar', 'LieuOuverturePlisAr', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_retrais_dossiers_ar', 'AdresseRetraisDossiersAr', 'VARCHAR', false, 255, null);
        $this->addColumn('pieces_dossier_admin', 'PiecesDossierAdmin', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_admin_fr', 'PiecesDossierAdminFr', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_admin_en', 'PiecesDossierAdminEn', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_admin_es', 'PiecesDossierAdminEs', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_admin_su', 'PiecesDossierAdminSu', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_admin_du', 'PiecesDossierAdminDu', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_admin_cz', 'PiecesDossierAdminCz', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_admin_ar', 'PiecesDossierAdminAr', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_tech', 'PiecesDossierTech', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_tech_fr', 'PiecesDossierTechFr', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_tech_en', 'PiecesDossierTechEn', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_tech_es', 'PiecesDossierTechEs', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_tech_su', 'PiecesDossierTechSu', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_tech_du', 'PiecesDossierTechDu', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_tech_cz', 'PiecesDossierTechCz', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_tech_ar', 'PiecesDossierTechAr', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_additif', 'PiecesDossierAdditif', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_additif_fr', 'PiecesDossierAdditifFr', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_additif_en', 'PiecesDossierAdditifEn', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_additif_es', 'PiecesDossierAdditifEs', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_additif_su', 'PiecesDossierAdditifSu', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_additif_du', 'PiecesDossierAdditifDu', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_additif_cz', 'PiecesDossierAdditifCz', 'LONGVARCHAR', false, null, null);
        $this->addColumn('pieces_dossier_additif_ar', 'PiecesDossierAdditifAr', 'LONGVARCHAR', false, null, null);
        $this->addColumn('id_rpa', 'IdRpa', 'INTEGER', false, null, null);
        $this->addColumn('detail_consultation_fr', 'DetailConsultationFr', 'LONGVARCHAR', false, null, null);
        $this->addColumn('detail_consultation_en', 'DetailConsultationEn', 'LONGVARCHAR', false, null, null);
        $this->addColumn('detail_consultation_es', 'DetailConsultationEs', 'LONGVARCHAR', false, null, null);
        $this->addColumn('detail_consultation_su', 'DetailConsultationSu', 'LONGVARCHAR', false, null, null);
        $this->addColumn('detail_consultation_du', 'DetailConsultationDu', 'LONGVARCHAR', false, null, null);
        $this->addColumn('detail_consultation_cz', 'DetailConsultationCz', 'LONGVARCHAR', false, null, null);
        $this->addColumn('detail_consultation_ar', 'DetailConsultationAr', 'LONGVARCHAR', false, null, null);
        $this->addColumn('echantillon', 'Echantillon', 'CHAR', true, null, '0');
        $this->getColumn('echantillon', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('reunion', 'Reunion', 'CHAR', true, null, '0');
        $this->getColumn('reunion', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('visites_lieux', 'VisitesLieux', 'CHAR', true, null, '0');
        $this->getColumn('visites_lieux', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('variante_calcule', 'VarianteCalcule', 'CHAR', true, null, '0');
        $this->getColumn('variante_calcule', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('adresse_retrais_dossiers_fr', 'AdresseRetraisDossiersFr', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_retrais_dossiers_en', 'AdresseRetraisDossiersEn', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_retrais_dossiers_es', 'AdresseRetraisDossiersEs', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_retrais_dossiers_su', 'AdresseRetraisDossiersSu', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_retrais_dossiers_du', 'AdresseRetraisDossiersDu', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_retrais_dossiers_cz', 'AdresseRetraisDossiersCz', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_depot_offres_fr', 'AdresseDepotOffresFr', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_depot_offres_en', 'AdresseDepotOffresEn', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_depot_offres_es', 'AdresseDepotOffresEs', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_depot_offres_su', 'AdresseDepotOffresSu', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_depot_offres_du', 'AdresseDepotOffresDu', 'VARCHAR', false, 255, null);
        $this->addColumn('adresse_depot_offres_cz', 'AdresseDepotOffresCz', 'VARCHAR', false, 255, null);
        $this->addColumn('lieu_ouverture_plis_fr', 'LieuOuverturePlisFr', 'VARCHAR', false, 255, null);
        $this->addColumn('lieu_ouverture_plis_en', 'LieuOuverturePlisEn', 'VARCHAR', false, 255, null);
        $this->addColumn('lieu_ouverture_plis_es', 'LieuOuverturePlisEs', 'VARCHAR', false, 255, null);
        $this->addColumn('lieu_ouverture_plis_su', 'LieuOuverturePlisSu', 'VARCHAR', false, 255, null);
        $this->addColumn('lieu_ouverture_plis_du', 'LieuOuverturePlisDu', 'VARCHAR', false, 255, null);
        $this->addColumn('lieu_ouverture_plis_cz', 'LieuOuverturePlisCz', 'VARCHAR', false, 255, null);
        $this->addColumn('add_echantillion_fr', 'AddEchantillionFr', 'VARCHAR', false, 255, null);
        $this->addColumn('add_echantillion_en', 'AddEchantillionEn', 'VARCHAR', false, 255, null);
        $this->addColumn('add_echantillion_es', 'AddEchantillionEs', 'VARCHAR', false, 255, null);
        $this->addColumn('add_echantillion_su', 'AddEchantillionSu', 'VARCHAR', false, 255, null);
        $this->addColumn('add_echantillion_du', 'AddEchantillionDu', 'VARCHAR', false, 255, null);
        $this->addColumn('add_echantillion_cz', 'AddEchantillionCz', 'VARCHAR', false, 255, null);
        $this->addColumn('add_echantillion_ar', 'AddEchantillionAr', 'VARCHAR', false, 255, null);
        $this->addColumn('add_reunion_fr', 'AddReunionFr', 'VARCHAR', false, 255, null);
        $this->addColumn('add_reunion_en', 'AddReunionEn', 'VARCHAR', false, 255, null);
        $this->addColumn('add_reunion_es', 'AddReunionEs', 'VARCHAR', false, 255, null);
        $this->addColumn('add_reunion_su', 'AddReunionSu', 'VARCHAR', false, 255, null);
        $this->addColumn('add_reunion_du', 'AddReunionDu', 'VARCHAR', false, 255, null);
        $this->addColumn('add_reunion_cz', 'AddReunionCz', 'VARCHAR', false, 255, null);
        $this->addColumn('add_reunion_ar', 'AddReunionAr', 'VARCHAR', false, 255, null);
        $this->addColumn('mode_passation', 'ModePassation', 'VARCHAR', false, 1, null);
        $this->addColumn('consultation_annulee', 'ConsultationAnnulee', 'CHAR', true, null, '0');
        $this->getColumn('consultation_annulee', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('accessibilite_it', 'AccessibiliteIt', 'CHAR', true, null, '0');
        $this->getColumn('accessibilite_it', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('adresse_depot_offres_it', 'AdresseDepotOffresIt', 'VARCHAR', true, 255, '');
        $this->addColumn('lieu_ouverture_plis_it', 'LieuOuverturePlisIt', 'VARCHAR', true, 255, '');
        $this->addColumn('adresse_retrais_dossiers_it', 'AdresseRetraisDossiersIt', 'VARCHAR', true, 255, '');
        $this->addColumn('pieces_dossier_admin_it', 'PiecesDossierAdminIt', 'VARCHAR', true, 255, '');
        $this->addColumn('pieces_dossier_tech_it', 'PiecesDossierTechIt', 'VARCHAR', true, 255, '');
        $this->addColumn('pieces_dossier_additif_it', 'PiecesDossierAdditifIt', 'VARCHAR', true, 255, '');
        $this->addColumn('detail_consultation_it', 'DetailConsultationIt', 'LONGVARCHAR', false, null, null);
        $this->addColumn('add_echantillion_it', 'AddEchantillionIt', 'VARCHAR', true, 250, '');
        $this->addColumn('add_reunion_it', 'AddReunionIt', 'VARCHAR', true, 250, '');
        $this->addColumn('codes_nuts', 'CodesNuts', 'LONGVARCHAR', false, null, null);
        $this->addColumn('date_decision', 'DateDecision', 'VARCHAR', true, 10, '');
        $this->addColumn('intitule', 'Intitule', 'CLOB', true, null, null);
        $this->addColumn('id_tr_intitule', 'IdTrIntitule', 'INTEGER', false, null, null);
        $this->addColumn('objet', 'Objet', 'CLOB', true, null, null);
        $this->addColumn('id_tr_objet', 'IdTrObjet', 'INTEGER', false, null, null);
        $this->addColumn('type_acces', 'TypeAcces', 'CHAR', true, null, null);
        $this->addColumn('autoriser_reponse_electronique', 'AutoriserReponseElectronique', 'CHAR', true, null, '1');
        $this->addColumn('regle_mise_en_ligne', 'RegleMiseEnLigne', 'INTEGER', true, null, 1);
        $this->addColumn('id_regle_validation', 'IdRegleValidation', 'INTEGER', true, null, 2);
        $this->addColumn('intitule_fr', 'IntituleFr', 'CLOB', false, null, null);
        $this->addColumn('intitule_en', 'IntituleEn', 'CLOB', false, null, null);
        $this->addColumn('intitule_es', 'IntituleEs', 'CLOB', false, null, null);
        $this->addColumn('intitule_su', 'IntituleSu', 'CLOB', false, null, null);
        $this->addColumn('intitule_du', 'IntituleDu', 'CLOB', false, null, null);
        $this->addColumn('intitule_cz', 'IntituleCz', 'CLOB', false, null, null);
        $this->addColumn('intitule_ar', 'IntituleAr', 'CLOB', false, null, null);
        $this->addColumn('intitule_it', 'IntituleIt', 'CLOB', false, null, null);
        $this->addColumn('objet_fr', 'ObjetFr', 'CLOB', false, null, null);
        $this->addColumn('objet_en', 'ObjetEn', 'CLOB', false, null, null);
        $this->addColumn('objet_es', 'ObjetEs', 'CLOB', false, null, null);
        $this->addColumn('objet_su', 'ObjetSu', 'CLOB', false, null, null);
        $this->addColumn('objet_du', 'ObjetDu', 'CLOB', false, null, null);
        $this->addColumn('objet_cz', 'ObjetCz', 'CLOB', false, null, null);
        $this->addColumn('objet_ar', 'ObjetAr', 'CLOB', false, null, null);
        $this->addColumn('objet_it', 'ObjetIt', 'CLOB', false, null, null);
        $this->addColumn('clause_sociale', 'ClauseSociale', 'CHAR', true, null, '0');
        $this->getColumn('clause_sociale', false)->setValueSet(array (
  0 => '0',
  1 => '1',
  2 => '2',
));
        $this->addColumn('clause_environnementale', 'ClauseEnvironnementale', 'CHAR', true, null, '0');
        $this->getColumn('clause_environnementale', false)->setValueSet(array (
  0 => '0',
  1 => '1',
  2 => '2',
));
        $this->addColumn('reponse_obligatoire', 'ReponseObligatoire', 'CHAR', true, null, '0');
        $this->addColumn('type_envoi', 'TypeEnvoi', 'VARCHAR', true, 1, '');
        $this->addColumn('chiffrement_offre', 'ChiffrementOffre', 'CHAR', true, null, '');
        $this->addColumn('env_candidature', 'EnvCandidature', 'INTEGER', true, 1, 0);
        $this->addColumn('env_offre', 'EnvOffre', 'INTEGER', true, 3, 0);
        $this->addColumn('env_anonymat', 'EnvAnonymat', 'INTEGER', true, 1, 0);
        $this->addColumn('id_etat_consultation', 'IdEtatConsultation', 'INTEGER', true, null, 0);
        $this->addColumn('reference_connecteur', 'ReferenceConnecteur', 'VARCHAR', true, 255, '');
        $this->addColumn('cons_statut', 'ConsStatut', 'CHAR', true, null, '0');
        $this->addColumn('id_approbateur', 'IdApprobateur', 'INTEGER', true, null, 0);
        $this->addColumn('id_valideur', 'IdValideur', 'INTEGER', true, null, 0);
        $this->addColumn('old_service_validation', 'OldServiceValidation', 'INTEGER', true, null, 0);
        $this->addColumn('id_createur', 'IdCreateur', 'INTEGER', true, null, 0);
        $this->addColumn('nom_createur', 'NomCreateur', 'VARCHAR', false, 100, null);
        $this->addColumn('prenom_createur', 'PrenomCreateur', 'VARCHAR', false, 100, null);
        $this->addColumn('signature_acte_engagement', 'SignatureActeEngagement', 'CHAR', true, null, '0');
        $this->getColumn('signature_acte_engagement', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('archiveMetaDescription', 'Archivemetadescription', 'CLOB', false, null, null);
        $this->addColumn('archiveMetaMotsClef', 'Archivemetamotsclef', 'CLOB', false, null, null);
        $this->addColumn('archiveIdBlobZip', 'Archiveidblobzip', 'INTEGER', false, null, null);
        $this->addColumn('decision_partielle', 'DecisionPartielle', 'CHAR', true, null, '0');
        $this->getColumn('decision_partielle', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('type_decision_a_renseigner', 'TypeDecisionARenseigner', 'CHAR', true, null, '1');
        $this->getColumn('type_decision_a_renseigner', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('type_decision_attribution_marche', 'TypeDecisionAttributionMarche', 'CHAR', true, null, '1');
        $this->getColumn('type_decision_attribution_marche', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('type_decision_declaration_sans_suite', 'TypeDecisionDeclarationSansSuite', 'CHAR', true, null, '1');
        $this->getColumn('type_decision_declaration_sans_suite', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('type_decision_declaration_infructueux', 'TypeDecisionDeclarationInfructueux', 'CHAR', true, null, '1');
        $this->getColumn('type_decision_declaration_infructueux', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('type_decision_selection_entreprise', 'TypeDecisionSelectionEntreprise', 'CHAR', true, null, '1');
        $this->getColumn('type_decision_selection_entreprise', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('type_decision_attribution_accord_cadre', 'TypeDecisionAttributionAccordCadre', 'CHAR', true, null, '1');
        $this->getColumn('type_decision_attribution_accord_cadre', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('type_decision_admission_sad', 'TypeDecisionAdmissionSad', 'CHAR', true, null, '1');
        $this->getColumn('type_decision_admission_sad', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('type_decision_autre', 'TypeDecisionAutre', 'CHAR', true, null, '1');
        $this->getColumn('type_decision_autre', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('id_archiveur', 'IdArchiveur', 'INTEGER', false, null, null);
        $this->addColumn('prenom_nom_agent_telechargement_plis', 'PrenomNomAgentTelechargementPlis', 'VARCHAR', false, 255, null);
        $this->addColumn('id_agent_telechargement_plis', 'IdAgentTelechargementPlis', 'INTEGER', true, null, 0);
        $this->addColumn('path_telechargement_plis', 'PathTelechargementPlis', 'VARCHAR', false, 255, null);
        $this->addColumn('date_telechargement_plis', 'DateTelechargementPlis', 'VARCHAR', false, 20, null);
        $this->addColumn('old_service_validation_intermediaire', 'OldServiceValidationIntermediaire', 'INTEGER', true, null, 0);
        $this->addColumn('env_offre_technique', 'EnvOffreTechnique', 'INTEGER', true, 3, 0);
        $this->addColumn('ref_org_partenaire', 'RefOrgPartenaire', 'VARCHAR', true, 250, '');
        $this->addColumn('date_archivage', 'DateArchivage', 'VARCHAR', false, 20, null);
        $this->addColumn('date_decision_annulation', 'DateDecisionAnnulation', 'VARCHAR', false, 20, null);
        $this->addColumn('commentaire_annulation', 'CommentaireAnnulation', 'LONGVARCHAR', false, null, null);
        $this->addColumn('compte_boamp_associe', 'CompteBoampAssocie', 'VARCHAR', false, 20, null);
        $this->addColumn('date_mise_en_ligne_souhaitee', 'DateMiseEnLigneSouhaitee', 'VARCHAR', false, 20, null);
        $this->addColumn('autoriser_publicite', 'AutoriserPublicite', 'INTEGER', true, 2, 1);
        $this->addColumn('dossier_additif', 'DossierAdditif', 'CHAR', true, null, '0');
        $this->getColumn('dossier_additif', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addForeignKey('type_marche', 'TypeMarche', 'INTEGER', 't_type_contrat', 'id_type_contrat', false, null, null);
        $this->addColumn('type_prestation', 'TypePrestation', 'INTEGER', true, null, 1);
        $this->addColumn('date_modification', 'DateModification', 'VARCHAR', false, 20, null);
        $this->addColumn('delai_partiel', 'DelaiPartiel', 'CHAR', true, null, '0');
        $this->getColumn('delai_partiel', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('dateFinLocale', 'Datefinlocale', 'VARCHAR', false, 20, '0000-00-00 00:00:00');
        $this->addColumn('lieuResidence', 'Lieuresidence', 'VARCHAR', false, 255, null);
        $this->addColumn('alerte', 'Alerte', 'CHAR', true, null, '0');
        $this->getColumn('alerte', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('doublon', 'Doublon', 'CHAR', true, null, '0');
        $this->getColumn('doublon', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('denomination_adapte', 'DenominationAdapte', 'VARCHAR', false, 250, null);
        $this->addColumn('url_consultation_avis_pub', 'UrlConsultationAvisPub', 'LONGVARCHAR', true, null, null);
        $this->addColumn('doublon_de', 'DoublonDe', 'VARCHAR', false, 250, null);
        $this->addColumn('entite_adjudicatrice', 'EntiteAdjudicatrice', 'CHAR', false, null, null);
        $this->getColumn('entite_adjudicatrice', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('code_operation', 'CodeOperation', 'VARCHAR', false, 255, null);
        $this->addColumn('clause_sociale_condition_execution', 'ClauseSocialeConditionExecution', 'VARCHAR', false, 255, '0');
        $this->addColumn('clause_sociale_insertion', 'ClauseSocialeInsertion', 'VARCHAR', false, 255, '0');
        $this->addColumn('clause_sociale_ateliers_proteges', 'ClauseSocialeAteliersProteges', 'VARCHAR', false, 255, '0');
        $this->addColumn('clause_sociale_siae', 'ClauseSocialeSiae', 'VARCHAR', false, 255, '0');
        $this->addColumn('clause_sociale_ess', 'ClauseSocialeEss', 'VARCHAR', false, 255, '0');
        $this->addColumn('clause_env_specs_techniques', 'ClauseEnvSpecsTechniques', 'VARCHAR', false, 255, '0');
        $this->addColumn('clause_env_cond_execution', 'ClauseEnvCondExecution', 'VARCHAR', false, 255, '0');
        $this->addColumn('clause_env_criteres_select', 'ClauseEnvCriteresSelect', 'VARCHAR', false, 255, '0');
        $this->addColumn('id_donnee_complementaire', 'IdDonneeComplementaire', 'INTEGER', false, null, null);
        $this->addColumn('donnee_complementaire_obligatoire', 'DonneeComplementaireObligatoire', 'CHAR', true, null, '0');
        $this->getColumn('donnee_complementaire_obligatoire', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('mode_ouverture_reponse', 'ModeOuvertureReponse', 'CHAR', true, null, '0');
        $this->getColumn('mode_ouverture_reponse', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('id_fichier_annulation', 'IdFichierAnnulation', 'INTEGER', false, null, null);
        $this->addForeignKey('idOperation', 'Idoperation', 'INTEGER', 'Operations', 'id_operation', false, null, null);
        $this->addColumn('etat_en_attente_validation', 'EtatEnAttenteValidation', 'CHAR', true, null, '1');
        $this->getColumn('etat_en_attente_validation', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('infos_blocs_atlas', 'InfosBlocsAtlas', 'VARCHAR', true, 10, '0##0');
        $this->addColumn('marche_public_simplifie', 'MarchePublicSimplifie', 'CHAR', true, null, '0');
        $this->getColumn('marche_public_simplifie', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('DATE_FIN_UNIX', 'DateFinUnix', 'VARCHAR', false, 20, '0');
        $this->addColumn('numero_AC', 'NumeroAc', 'VARCHAR', false, 255, null);
        $this->addColumn('id_contrat', 'IdContrat', 'INTEGER', false, null, null);
        $this->addColumn('pin_api_sgmap_mps', 'PinApiSgmapMps', 'VARCHAR', false, 20, null);
        $this->addColumn('donnee_publicite_obligatoire', 'DonneePubliciteObligatoire', 'CHAR', false, null, null);
        $this->getColumn('donnee_publicite_obligatoire', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('dume_demande', 'DumeDemande', 'CHAR', true, null, '0');
        $this->getColumn('dume_demande', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('type_procedure_dume', 'TypeProcedureDume', 'INTEGER', true, null, 0);
        $this->addColumn('marche_insertion', 'MarcheInsertion', 'BOOLEAN', false, 1, false);
        $this->addColumn('clause_specification_technique', 'ClauseSpecificationTechnique', 'VARCHAR', false, 255, '0');
        $this->addColumn('type_formulaire_dume', 'TypeFormulaireDume', 'CHAR', false, 2, '0');
        $this->addColumn('source_externe', 'SourceExterne', 'VARCHAR', false, 256, null);
        $this->addColumn('id_source_externe', 'IdSourceExterne', 'INTEGER', false, null, null);
        $this->addForeignKey('id_dossier_volumineux', 'IdDossierVolumineux', 'INTEGER', 'dossier_volumineux', 'id', false, null, null);
        $this->addColumn('attestation_consultation', 'AttestationConsultation', 'CHAR', true, 2, '0');
        $this->addColumn('version_messagerie', 'VersionMessagerie', 'INTEGER', true, null, 1);
        $this->addForeignKey('plateforme_virtuelle_id', 'PlateformeVirtuelleId', 'INTEGER', 'plateforme_virtuelle', 'id', false, 10, null);
        $this->addColumn('is_envoi_publicite_validation', 'IsEnvoiPubliciteValidation', 'BOOLEAN', true, 1, false);
        $this->addColumn('annexe_financiere', 'AnnexeFinanciere', 'BOOLEAN', false, 1, false);
        $this->addColumn('envol_activation', 'EnvolActivation', 'BOOLEAN', true, 1, false);
        $this->addForeignKey('service_id', 'ServiceId', 'BIGINT', 'Service', 'id', false, null, null);
        $this->addColumn('service_associe_id', 'ServiceAssocieId', 'BIGINT', false, null, null);
        $this->addColumn('groupement', 'Groupement', 'BOOLEAN', false, 1, null);
        $this->addForeignKey('service_validation', 'ServiceValidation', 'BIGINT', 'Service', 'id', false, null, null);
        $this->addColumn('autre_technique_achat', 'AutreTechniqueAchat', 'VARCHAR', false, 255, null);
        $this->addColumn('procedure_ouverte', 'ProcedureOuverte', 'BOOLEAN', false, 1, null);
        $this->addForeignKey('service_validation_intermediaire', 'ServiceValidationIntermediaire', 'BIGINT', 'Service', 'id', false, null, null);
        $this->addColumn('cms_actif', 'CmsActif', 'BOOLEAN', true, 1, false);
        $this->addColumn('controle_taille_depot', 'ControleTailleDepot', 'INTEGER', false, null, null);
        $this->addColumn('code_dce_restreint', 'CodeDceRestreint', 'VARCHAR', false, 15, null);
        $this->addColumn('numero_projet_achat', 'NumeroProjetAchat', 'VARCHAR', false, 255, null);
        $this->addForeignKey('referentiel_achat_id', 'ReferentielAchatId', 'INTEGER', 'referentiel_achat', 'id', false, null, null);
        $this->addForeignKey('agent_technique_createur', 'AgentTechniqueCreateur', 'INTEGER', 'Agent', 'id', false, null, null);
        $this->addColumn('uuid', 'Uuid', 'CHAR', true, 36, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonReferentielAchat', 'Application\\Propel\\Mpe\\CommonReferentielAchat', RelationMap::MANY_TO_ONE, array('referentiel_achat_id' => 'id', ), null, null);
        $this->addRelation('CommonAgent', 'Application\\Propel\\Mpe\\CommonAgent', RelationMap::MANY_TO_ONE, array('agent_technique_createur' => 'id', ), null, null);
        $this->addRelation('CommonServiceRelatedByServiceValidation', 'Application\\Propel\\Mpe\\CommonService', RelationMap::MANY_TO_ONE, array('service_validation' => 'id', ), null, null);
        $this->addRelation('CommonTTypeContrat', 'Application\\Propel\\Mpe\\CommonTTypeContrat', RelationMap::MANY_TO_ONE, array('type_marche' => 'id_type_contrat', ), null, 'CASCADE');
        $this->addRelation('CommonServiceRelatedByServiceId', 'Application\\Propel\\Mpe\\CommonService', RelationMap::MANY_TO_ONE, array('service_id' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonConsultationRelatedByReferenceConsultationInit', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::MANY_TO_ONE, array('reference_consultation_init' => 'id', ), null, null);
        $this->addRelation('CommonServiceRelatedByServiceValidationIntermediaire', 'Application\\Propel\\Mpe\\CommonService', RelationMap::MANY_TO_ONE, array('service_validation_intermediaire' => 'id', ), null, null);
        $this->addRelation('CommonOperations', 'Application\\Propel\\Mpe\\CommonOperations', RelationMap::MANY_TO_ONE, array('idOperation' => 'id_operation', ), null, null);
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), null, 'CASCADE');
        $this->addRelation('CommonPlateformeVirtuelle', 'Application\\Propel\\Mpe\\CommonPlateformeVirtuelle', RelationMap::MANY_TO_ONE, array('plateforme_virtuelle_id' => 'id', ), null, null);
        $this->addRelation('CommonDossierVolumineux', 'Application\\Propel\\Mpe\\CommonDossierVolumineux', RelationMap::MANY_TO_ONE, array('id_dossier_volumineux' => 'id', ), null, null);
        $this->addRelation('CommonAVIS', 'Application\\Propel\\Mpe\\CommonAVIS', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonAVISs');
        $this->addRelation('CommonAnnonce', 'Application\\Propel\\Mpe\\CommonAnnonce', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonAnnonces');
        $this->addRelation('CommonAnnonceBoamp', 'Application\\Propel\\Mpe\\CommonAnnonceBoamp', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonAnnonceBoamps');
        $this->addRelation('CommonAvisPub', 'Application\\Propel\\Mpe\\CommonAvisPub', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonAvisPubs');
        $this->addRelation('CommonCategorieLot', 'Application\\Propel\\Mpe\\CommonCategorieLot', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonCategorieLots');
        $this->addRelation('CommonComplement', 'Application\\Propel\\Mpe\\CommonComplement', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonComplements');
        $this->addRelation('CommonDATEFIN', 'Application\\Propel\\Mpe\\CommonDATEFIN', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonDATEFINs');
        $this->addRelation('CommonDCE', 'Application\\Propel\\Mpe\\CommonDCE', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonDCEs');
        $this->addRelation('CommonDocumentExterne', 'Application\\Propel\\Mpe\\CommonDocumentExterne', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonDocumentExternes');
        $this->addRelation('CommonHeliosPiecePublicite', 'Application\\Propel\\Mpe\\CommonHeliosPiecePublicite', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, null, 'CommonHeliosPiecePublicites');
        $this->addRelation('CommonHeliosPvConsultation', 'Application\\Propel\\Mpe\\CommonHeliosPvConsultation', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, null, 'CommonHeliosPvConsultations');
        $this->addRelation('CommonHeliosRapportPrefet', 'Application\\Propel\\Mpe\\CommonHeliosRapportPrefet', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, null, 'CommonHeliosRapportPrefets');
        $this->addRelation('CommonHeliosTableauAr', 'Application\\Propel\\Mpe\\CommonHeliosTableauAr', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, null, 'CommonHeliosTableauArs');
        $this->addRelation('CommonHeliosTeletransmission', 'Application\\Propel\\Mpe\\CommonHeliosTeletransmission', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonHeliosTeletransmissions');
        $this->addRelation('CommonInterneConsultation', 'Application\\Propel\\Mpe\\CommonInterneConsultation', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonInterneConsultations');
        $this->addRelation('CommonInterneConsultationSuiviSeul', 'Application\\Propel\\Mpe\\CommonInterneConsultationSuiviSeul', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonInterneConsultationSuiviSeuls');
        $this->addRelation('CommonOffrePapier', 'Application\\Propel\\Mpe\\CommonOffrePapier', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonOffrePapiers');
        $this->addRelation('CommonOffres', 'Application\\Propel\\Mpe\\CommonOffres', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonOffress');
        $this->addRelation('CommonPanierEntreprise', 'Application\\Propel\\Mpe\\CommonPanierEntreprise', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonPanierEntreprises');
        $this->addRelation('CommonPiecesDCE', 'Application\\Propel\\Mpe\\CommonPiecesDCE', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonPiecesDCEs');
        $this->addRelation('CommonQuestionDCE', 'Application\\Propel\\Mpe\\CommonQuestionDCE', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonQuestionDCEs');
        $this->addRelation('CommonRG', 'Application\\Propel\\Mpe\\CommonRG', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonRGs');
        $this->addRelation('CommonRetraitPapier', 'Application\\Propel\\Mpe\\CommonRetraitPapier', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonRetraitPapiers');
        $this->addRelation('CommonTelechargement', 'Application\\Propel\\Mpe\\CommonTelechargement', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonTelechargements');
        $this->addRelation('CommonTelechargementAnonyme', 'Application\\Propel\\Mpe\\CommonTelechargementAnonyme', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonTelechargementAnonymes');
        $this->addRelation('CommonAnnexeFinanciere', 'Application\\Propel\\Mpe\\CommonAnnexeFinanciere', RelationMap::ONE_TO_MANY, array('id' => 'id_consultation', ), null, null, 'CommonAnnexeFinancieres');
        $this->addRelation('CommonAutrePieceConsultation', 'Application\\Propel\\Mpe\\CommonAutrePieceConsultation', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, null, 'CommonAutrePieceConsultations');
        $this->addRelation('CommonConsultationRelatedById', 'Application\\Propel\\Mpe\\CommonConsultation', RelationMap::ONE_TO_MANY, array('id' => 'reference_consultation_init', ), null, null, 'CommonConsultationsRelatedById');
        $this->addRelation('CommonConsultationClausesN1', 'Application\\Propel\\Mpe\\CommonConsultationClausesN1', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, null, 'CommonConsultationClausesN1s');
        $this->addRelation('CommonConsultationDocumentCfe', 'Application\\Propel\\Mpe\\CommonConsultationDocumentCfe', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonConsultationDocumentCfes');
        $this->addRelation('CommonConsultationFavoris', 'Application\\Propel\\Mpe\\CommonConsultationFavoris', RelationMap::ONE_TO_MANY, array('id' => 'id_consultation', ), null, null, 'CommonConsultationFavoriss');
        $this->addRelation('CommonConsultationTags', 'Application\\Propel\\Mpe\\CommonConsultationTags', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), 'CASCADE', 'CASCADE', 'CommonConsultationTagss');
        $this->addRelation('CommonEchangeDoc', 'Application\\Propel\\Mpe\\CommonEchangeDoc', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, null, 'CommonEchangeDocs');
        $this->addRelation('CommonInterfaceSuivi', 'Application\\Propel\\Mpe\\CommonInterfaceSuivi', RelationMap::ONE_TO_MANY, array('id' => 'id_consultation', ), 'CASCADE', null, 'CommonInterfaceSuivis');
        $this->addRelation('CommonPieceGenereConsultation', 'Application\\Propel\\Mpe\\CommonPieceGenereConsultation', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, null, 'CommonPieceGenereConsultations');
        $this->addRelation('CommonTAnnonceConsultation', 'Application\\Propel\\Mpe\\CommonTAnnonceConsultation', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonTAnnonceConsultations');
        $this->addRelation('CommonTCalendrier', 'Application\\Propel\\Mpe\\CommonTCalendrier', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonTCalendriers');
        $this->addRelation('CommonTCandidature', 'Application\\Propel\\Mpe\\CommonTCandidature', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonTCandidatures');
        $this->addRelation('CommonTDumeContexte', 'Application\\Propel\\Mpe\\CommonTDumeContexte', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonTDumeContextes');
        $this->addRelation('CommonTListeLotsCandidature', 'Application\\Propel\\Mpe\\CommonTListeLotsCandidature', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonTListeLotsCandidatures');
        $this->addRelation('CommonTParamDossierFormulaire', 'Application\\Propel\\Mpe\\CommonTParamDossierFormulaire', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, null, 'CommonTParamDossierFormulaires');
        $this->addRelation('CommonTReponseElecFormulaire', 'Application\\Propel\\Mpe\\CommonTReponseElecFormulaire', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, null, 'CommonTReponseElecFormulaires');
        $this->addRelation('CommonVisiteLieux', 'Application\\Propel\\Mpe\\CommonVisiteLieux', RelationMap::ONE_TO_MANY, array('id' => 'consultation_id', ), null, 'CASCADE', 'CommonVisiteLieuxs');
    } // buildRelations()

} // CommonConsultationTableMap
