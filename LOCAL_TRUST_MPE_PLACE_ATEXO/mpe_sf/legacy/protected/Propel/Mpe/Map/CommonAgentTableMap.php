<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Agent' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonAgentTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonAgentTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Agent');
        $this->setPhpName('CommonAgent');
        $this->setClassname('Application\\Propel\\Mpe\\CommonAgent');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('login', 'Login', 'VARCHAR', false, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, '');
        $this->addColumn('mdp', 'Mdp', 'VARCHAR', true, 64, null);
        $this->addColumn('certificat', 'Certificat', 'LONGVARCHAR', false, null, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 100, '');
        $this->addColumn('prenom', 'Prenom', 'VARCHAR', true, 100, '');
        $this->addColumn('tentatives_mdp', 'TentativesMdp', 'INTEGER', true, 1, 0);
        $this->addForeignKey('organisme', 'Organisme', 'VARCHAR', 'Organisme', 'acronyme', false, 30, null);
        $this->addColumn('old_service_id', 'OldServiceId', 'INTEGER', false, null, null);
        $this->addColumn('RECEVOIR_MAIL', 'RecevoirMail', 'CHAR', true, null, '0');
        $this->getColumn('RECEVOIR_MAIL', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('elu', 'Elu', 'CHAR', true, null, '0');
        $this->addColumn('nom_fonction', 'NomFonction', 'VARCHAR', true, 100, '');
        $this->addColumn('num_tel', 'NumTel', 'VARCHAR', false, 20, null);
        $this->addColumn('num_fax', 'NumFax', 'VARCHAR', false, 20, null);
        $this->addColumn('type_comm', 'TypeComm', 'CHAR', true, null, '2');
        $this->addColumn('adr_postale', 'AdrPostale', 'VARCHAR', true, 255, '');
        $this->addColumn('civilite', 'Civilite', 'VARCHAR', true, 255, '');
        $this->addColumn('alerte_reponse_electronique', 'AlerteReponseElectronique', 'CHAR', true, null, '0');
        $this->getColumn('alerte_reponse_electronique', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alerte_cloture_consultation', 'AlerteClotureConsultation', 'CHAR', true, null, '0');
        $this->getColumn('alerte_cloture_consultation', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alerte_reception_message', 'AlerteReceptionMessage', 'CHAR', true, null, '0');
        $this->getColumn('alerte_reception_message', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alerte_publication_boamp', 'AlertePublicationBoamp', 'CHAR', true, null, '0');
        $this->getColumn('alerte_publication_boamp', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alerte_echec_publication_boamp', 'AlerteEchecPublicationBoamp', 'CHAR', true, null, '0');
        $this->getColumn('alerte_echec_publication_boamp', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alerte_creation_modification_agent', 'AlerteCreationModificationAgent', 'CHAR', true, null, '0');
        $this->getColumn('alerte_creation_modification_agent', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('date_creation', 'DateCreation', 'VARCHAR', false, 20, null);
        $this->addColumn('date_modification', 'DateModification', 'VARCHAR', false, 20, null);
        $this->addColumn('id_externe', 'IdExterne', 'VARCHAR', true, 50, '0');
        $this->addColumn('id_profil_socle_externe', 'IdProfilSocleExterne', 'INTEGER', true, null, null);
        $this->addColumn('lieu_execution', 'LieuExecution', 'LONGVARCHAR', false, null, null);
        $this->addColumn('alerte_question_entreprise', 'AlerteQuestionEntreprise', 'CHAR', true, null, '0');
        $this->getColumn('alerte_question_entreprise', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('actif', 'Actif', 'CHAR', true, null, '1');
        $this->getColumn('actif', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('deleted_at', 'DeletedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('codes_nuts', 'CodesNuts', 'LONGVARCHAR', false, null, null);
        $this->addColumn('num_certificat', 'NumCertificat', 'VARCHAR', false, 64, '');
        $this->addColumn('alerte_validation_consultation', 'AlerteValidationConsultation', 'CHAR', true, null, '0');
        $this->getColumn('alerte_validation_consultation', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alerte_chorus', 'AlerteChorus', 'CHAR', true, null, '0');
        $this->getColumn('alerte_chorus', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('password', 'Password', 'VARCHAR', false, 255, null);
        $this->addColumn('code_theme', 'CodeTheme', 'VARCHAR', false, 255, '0');
        $this->addColumn('date_connexion', 'DateConnexion', 'TIMESTAMP', false, null, null);
        $this->addColumn('type_hash', 'TypeHash', 'VARCHAR', false, 10, 'SHA1');
        $this->addColumn('alerte_mes_consultations', 'AlerteMesConsultations', 'CHAR', true, null, '1');
        $this->getColumn('alerte_mes_consultations', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alerte_consultations_mon_entite', 'AlerteConsultationsMonEntite', 'CHAR', true, null, '1');
        $this->getColumn('alerte_consultations_mon_entite', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alerte_consultations_des_entites_dependantes', 'AlerteConsultationsDesEntitesDependantes', 'CHAR', true, null, '1');
        $this->getColumn('alerte_consultations_des_entites_dependantes', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('alerte_consultations_mes_entites_transverses', 'AlerteConsultationsMesEntitesTransverses', 'CHAR', true, null, '1');
        $this->getColumn('alerte_consultations_mes_entites_transverses', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('technique', 'Technique', 'BOOLEAN', true, 1, false);
        $this->addForeignKey('service_id', 'ServiceId', 'BIGINT', 'Service', 'id', false, null, null);
        $this->addColumn('date_validation_rgpd', 'DateValidationRgpd', 'TIMESTAMP', false, null, null);
        $this->addColumn('rgpd_communication_place', 'RgpdCommunicationPlace', 'BOOLEAN', false, 1, null);
        $this->addColumn('rgpd_enquete', 'RgpdEnquete', 'BOOLEAN', false, 1, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonService', 'Application\\Propel\\Mpe\\CommonService', RelationMap::MANY_TO_ONE, array('service_id' => 'id', ), null, null);
        $this->addRelation('CommonAgentServiceMetier', 'Application\\Propel\\Mpe\\CommonAgentServiceMetier', RelationMap::ONE_TO_MANY, array('id' => 'id_agent', ), 'CASCADE', 'CASCADE', 'CommonAgentServiceMetiers');
        $this->addRelation('CommonHabilitationAgent', 'Application\\Propel\\Mpe\\CommonHabilitationAgent', RelationMap::ONE_TO_ONE, array('id' => 'id_agent', ), 'CASCADE', 'CASCADE');
        $this->addRelation('CommonAgentTechniqueToken', 'Application\\Propel\\Mpe\\CommonAgentTechniqueToken', RelationMap::ONE_TO_MANY, array('id' => 'agent_id', ), 'CASCADE', 'CASCADE', 'CommonAgentTechniqueTokens');
        $this->addRelation('CommonAnnexeFinanciere', 'Application\\Propel\\Mpe\\CommonAnnexeFinanciere', RelationMap::ONE_TO_MANY, array('id' => 'id_agent', ), null, null, 'CommonAnnexeFinancieres');
        $this->addRelation('CommonConsultationFavoris', 'Application\\Propel\\Mpe\\CommonConsultationFavoris', RelationMap::ONE_TO_MANY, array('id' => 'id_agent', ), 'CASCADE', null, 'CommonConsultationFavoriss');
        $this->addRelation('CommonDossierVolumineux', 'Application\\Propel\\Mpe\\CommonDossierVolumineux', RelationMap::ONE_TO_MANY, array('id' => 'id_agent', ), null, null, 'CommonDossierVolumineuxs');
        $this->addRelation('CommonEchangeDoc', 'Application\\Propel\\Mpe\\CommonEchangeDoc', RelationMap::ONE_TO_MANY, array('id' => 'agent_id', ), null, null, 'CommonEchangeDocs');
        $this->addRelation('CommonEchangeDocHistorique', 'Application\\Propel\\Mpe\\CommonEchangeDocHistorique', RelationMap::ONE_TO_MANY, array('id' => 'agent_id', ), null, null, 'CommonEchangeDocHistoriques');
        $this->addRelation('CommonHabilitationAgentWs', 'Application\\Propel\\Mpe\\CommonHabilitationAgentWs', RelationMap::ONE_TO_MANY, array('id' => 'agent_id', ), 'CASCADE', 'CASCADE', 'CommonHabilitationAgentWss');
        $this->addRelation('CommonInvitePermanentTransverse', 'Application\\Propel\\Mpe\\CommonInvitePermanentTransverse', RelationMap::ONE_TO_MANY, array('id' => 'agent_id', ), 'CASCADE', 'CASCADE', 'CommonInvitePermanentTransverses');
        $this->addRelation('CommonModificationContrat', 'Application\\Propel\\Mpe\\CommonModificationContrat', RelationMap::ONE_TO_MANY, array('id' => 'id_agent', ), null, null, 'CommonModificationContrats');
        $this->addRelation('CommonTCAOCommissionAgent', 'Application\\Propel\\Mpe\\CommonTCAOCommissionAgent', RelationMap::ONE_TO_MANY, array('id' => 'id_agent', ), null, null, 'CommonTCAOCommissionAgents');
        $this->addRelation('CommonTCAOOrdreDuJourIntervenant', 'Application\\Propel\\Mpe\\CommonTCAOOrdreDuJourIntervenant', RelationMap::ONE_TO_MANY, array('id' => 'id_agent', ), null, null, 'CommonTCAOOrdreDuJourIntervenants');
        $this->addRelation('CommonTCAOSeanceAgent', 'Application\\Propel\\Mpe\\CommonTCAOSeanceAgent', RelationMap::ONE_TO_MANY, array('id' => 'id_agent', ), null, null, 'CommonTCAOSeanceAgents');
        $this->addRelation('CommonTCAOSeanceInvite', 'Application\\Propel\\Mpe\\CommonTCAOSeanceInvite', RelationMap::ONE_TO_MANY, array('id' => 'id_agent', ), null, null, 'CommonTCAOSeanceInvites');
        $this->addRelation('CommonTVisionRmaAgentOrganisme', 'Application\\Propel\\Mpe\\CommonTVisionRmaAgentOrganisme', RelationMap::ONE_TO_MANY, array('id' => 'id_agent', ), null, null, 'CommonTVisionRmaAgentOrganismes');
    } // buildRelations()

} // CommonAgentTableMap
