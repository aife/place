<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'consultation_archive_arcade' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonConsultationArchiveArcadeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonConsultationArchiveArcadeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('consultation_archive_arcade');
        $this->setPhpName('CommonConsultationArchiveArcade');
        $this->setClassname('Application\\Propel\\Mpe\\CommonConsultationArchiveArcade');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('consultation_archive_id', 'ConsultationArchiveId', 'INTEGER' , 'consultation_archive', 'id', true, null, null);
        $this->addForeignKey('archive_arcade_id', 'ArchiveArcadeId', 'INTEGER', 'archive_arcade', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonArchiveArcade', 'Application\\Propel\\Mpe\\CommonArchiveArcade', RelationMap::MANY_TO_ONE, array('archive_arcade_id' => 'id', ), null, null);
        $this->addRelation('CommonConsultationArchive', 'Application\\Propel\\Mpe\\CommonConsultationArchive', RelationMap::MANY_TO_ONE, array('consultation_archive_id' => 'id', ), null, null);
    } // buildRelations()

} // CommonConsultationArchiveArcadeTableMap
