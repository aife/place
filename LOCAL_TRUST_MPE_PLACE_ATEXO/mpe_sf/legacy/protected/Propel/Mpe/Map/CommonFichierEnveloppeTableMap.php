<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'fichierEnveloppe' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonFichierEnveloppeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonFichierEnveloppeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('fichierEnveloppe');
        $this->setPhpName('CommonFichierEnveloppe');
        $this->setClassname('Application\\Propel\\Mpe\\CommonFichierEnveloppe');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_fichier', 'IdFichier', 'INTEGER', true, null, null);
        $this->addPrimaryKey('organisme', 'Organisme', 'VARCHAR', true, 30, '');
        $this->addColumn('id_enveloppe', 'IdEnveloppe', 'INTEGER', true, null, 0);
        $this->addColumn('type_fichier', 'TypeFichier', 'CHAR', true, 3, '');
        $this->addColumn('num_ordre_fichier', 'NumOrdreFichier', 'INTEGER', true, 5, 0);
        $this->addColumn('nom_fichier', 'NomFichier', 'LONGVARCHAR', true, null, null);
        $this->addColumn('taille_fichier', 'TailleFichier', 'VARCHAR', true, 50, '');
        $this->addColumn('signature_fichier', 'SignatureFichier', 'CLOB', false, null, null);
        $this->addColumn('hash', 'Hash', 'LONGVARCHAR', true, null, null);
        $this->addColumn('verification_certificat', 'VerificationCertificat', 'VARCHAR', true, 5, '');
        $this->addColumn('id_blob', 'IdBlob', 'INTEGER', false, null, null);
        $this->addColumn('id_blob_signature', 'IdBlobSignature', 'INTEGER', false, null, null);
        $this->addColumn('type_piece', 'TypePiece', 'INTEGER', true, 10, 3);
        $this->addColumn('id_type_piece', 'IdTypePiece', 'INTEGER', true, 10, 0);
        $this->addColumn('is_hash', 'IsHash', 'CHAR', true, null, '0');
        $this->getColumn('is_hash', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('nom_referentiel_certificat', 'NomReferentielCertificat', 'VARCHAR', false, 255, null);
        $this->addColumn('statut_referentiel_certificat', 'StatutReferentielCertificat', 'INTEGER', false, 1, null);
        $this->addColumn('nom_referentiel_fonctionnel', 'NomReferentielFonctionnel', 'VARCHAR', false, 255, null);
        $this->addColumn('message', 'Message', 'VARCHAR', false, 255, null);
        $this->addColumn('date_signature', 'DateSignature', 'VARCHAR', false, 20, null);
        $this->addColumn('signature_infos', 'SignatureInfos', 'LONGVARCHAR', false, null, null);
        $this->addColumn('signature_infos_date', 'SignatureInfosDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('id_fichier_signature', 'IdFichierSignature', 'INTEGER', false, null, null);
        $this->addColumn('uid_response', 'UidResponse', 'LONGVARCHAR', false, null, null);
        $this->addColumn('type_signature_fichier', 'TypeSignatureFichier', 'VARCHAR', true, 50, '');
        $this->addColumn('hash256', 'Hash256', 'LONGVARCHAR', true, null, null);
        $this->addColumn('resultat_verification_hash', 'ResultatVerificationHash', 'CHAR', false, null, '1');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonBlocFichierEnveloppe', 'Application\\Propel\\Mpe\\CommonBlocFichierEnveloppe', RelationMap::ONE_TO_MANY, array('id_fichier' => 'id_fichier', 'organisme' => 'organisme', ), 'CASCADE', null, 'CommonBlocFichierEnveloppes');
    } // buildRelations()

} // CommonFichierEnveloppeTableMap
