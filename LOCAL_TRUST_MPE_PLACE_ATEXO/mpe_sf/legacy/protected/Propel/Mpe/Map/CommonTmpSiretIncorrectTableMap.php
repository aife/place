<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'tmp_siret_incorrect' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTmpSiretIncorrectTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTmpSiretIncorrectTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tmp_siret_incorrect');
        $this->setPhpName('CommonTmpSiretIncorrect');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTmpSiretIncorrect');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 10, null);
        $this->addColumn('old_id_inscrit', 'OldIdInscrit', 'INTEGER', false, null, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 256, null);
        $this->addColumn('siret', 'Siret', 'VARCHAR', true, 256, null);
        $this->addColumn('is_send_message', 'IsSendMessage', 'BOOLEAN', true, 1, null);
        $this->addColumn('send_date', 'SendDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('id_inscrit', 'IdInscrit', 'BIGINT', 'Inscrit', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonInscrit', 'Application\\Propel\\Mpe\\CommonInscrit', RelationMap::MANY_TO_ONE, array('id_inscrit' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // CommonTmpSiretIncorrectTableMap
