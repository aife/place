<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_message_accueil' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTMessageAccueilTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTMessageAccueilTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_message_accueil');
        $this->setPhpName('CommonTMessageAccueil');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTMessageAccueil');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('type_message', 'TypeMessage', 'VARCHAR', true, 255, '');
        $this->addColumn('contenu', 'Contenu', 'LONGVARCHAR', true, null, null);
        $this->addColumn('destinataire', 'Destinataire', 'VARCHAR', true, 50, '');
        $this->addColumn('authentifier', 'Authentifier', 'INTEGER', false, 1, null);
        $this->addColumn('config', 'Config', 'VARCHAR', false, 255, '');
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // CommonTMessageAccueilTableMap
