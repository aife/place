<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'T_Telechargement_Asynchrone' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTTelechargementAsynchroneTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTTelechargementAsynchroneTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('T_Telechargement_Asynchrone');
        $this->setPhpName('CommonTTelechargementAsynchrone');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTTelechargementAsynchrone');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('id_agent', 'IdAgent', 'INTEGER', true, null, 0);
        $this->addColumn('nom_prenom_agent', 'NomPrenomAgent', 'VARCHAR', true, 50, '');
        $this->addColumn('email_agent', 'EmailAgent', 'VARCHAR', true, 50, '');
        $this->addForeignKey('id_service_agent', 'IdServiceAgent', 'BIGINT', 'Service', 'id', false, null, null);
        $this->addColumn('old_id_service_agent', 'OldIdServiceAgent', 'INTEGER', true, null, 0);
        $this->addColumn('organisme_agent', 'OrganismeAgent', 'VARCHAR', true, 50, '');
        $this->addColumn('nom_fichier_telechargement', 'NomFichierTelechargement', 'VARCHAR', true, 50, '');
        $this->addColumn('taille_fichier', 'TailleFichier', 'INTEGER', true, null, 0);
        $this->addColumn('date_generation', 'DateGeneration', 'TIMESTAMP', false, null, null);
        $this->addColumn('id_blob_fichier', 'IdBlobFichier', 'INTEGER', true, null, 0);
        $this->addColumn('tag_fichier_genere', 'TagFichierGenere', 'CHAR', true, null, '0');
        $this->getColumn('tag_fichier_genere', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('tag_fichier_supprime', 'TagFichierSupprime', 'CHAR', true, null, '0');
        $this->getColumn('tag_fichier_supprime', false)->setValueSet(array (
  0 => '0',
  1 => '1',
));
        $this->addColumn('type_telechargement', 'TypeTelechargement', 'INTEGER', true, null, 0);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonService', 'Application\\Propel\\Mpe\\CommonService', RelationMap::MANY_TO_ONE, array('id_service_agent' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonTTelechargementAsynchroneFichier', 'Application\\Propel\\Mpe\\CommonTTelechargementAsynchroneFichier', RelationMap::ONE_TO_MANY, array('id' => 'id_telechargement_asynchrone', ), null, null, 'CommonTTelechargementAsynchroneFichiers');
    } // buildRelations()

} // CommonTTelechargementAsynchroneTableMap
