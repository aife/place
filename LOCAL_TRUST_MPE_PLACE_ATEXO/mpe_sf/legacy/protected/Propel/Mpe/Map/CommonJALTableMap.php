<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'JAL' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonJALTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonJALTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('JAL');
        $this->setPhpName('CommonJAL');
        $this->setClassname('Application\\Propel\\Mpe\\CommonJAL');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('old_id', 'OldId', 'INTEGER', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', true, 30, null);
        $this->addColumn('old_service_id', 'OldServiceId', 'INTEGER', false, null, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 100, '');
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, '');
        $this->addColumn('email_ar', 'EmailAr', 'VARCHAR', true, 100, null);
        $this->addColumn('telecopie', 'Telecopie', 'VARCHAR', true, 20, null);
        $this->addColumn('information_facturation', 'InformationFacturation', 'LONGVARCHAR', true, null, null);
        $this->addColumn('service_id', 'ServiceId', 'BIGINT', false, null, null);
        $this->addPrimaryKey('id', 'Id', 'BIGINT', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonDestinataireAnnonceJAL', 'Application\\Propel\\Mpe\\CommonDestinataireAnnonceJAL', RelationMap::ONE_TO_MANY, array('id' => 'idJAL', ), 'CASCADE', 'CASCADE', 'CommonDestinataireAnnonceJALs');
    } // buildRelations()

} // CommonJALTableMap
