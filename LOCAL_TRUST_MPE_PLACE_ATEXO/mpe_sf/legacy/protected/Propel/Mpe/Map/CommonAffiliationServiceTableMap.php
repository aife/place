<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'AffiliationService' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonAffiliationServiceTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonAffiliationServiceTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('AffiliationService');
        $this->setPhpName('CommonAffiliationService');
        $this->setClassname('Application\\Propel\\Mpe\\CommonAffiliationService');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addForeignKey('organisme', 'Organisme', 'VARCHAR', 'Organisme', 'acronyme', true, 30, '');
        $this->addColumn('old_id_pole', 'OldIdPole', 'INTEGER', false, null, null);
        $this->addColumn('old_id_service', 'OldIdService', 'INTEGER', false, null, null);
        $this->addPrimaryKey('id', 'Id', 'BIGINT', true, null, null);
        $this->addForeignKey('service_parent_id', 'ServiceParentId', 'BIGINT', 'Service', 'id', true, null, null);
        $this->addForeignKey('service_id', 'ServiceId', 'BIGINT', 'Service', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), null, 'CASCADE');
        $this->addRelation('CommonServiceRelatedByServiceId', 'Application\\Propel\\Mpe\\CommonService', RelationMap::MANY_TO_ONE, array('service_id' => 'id', ), null, 'CASCADE');
        $this->addRelation('CommonServiceRelatedByServiceParentId', 'Application\\Propel\\Mpe\\CommonService', RelationMap::MANY_TO_ONE, array('service_parent_id' => 'id', ), null, 'CASCADE');
    } // buildRelations()

} // CommonAffiliationServiceTableMap
