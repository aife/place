<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'Certificats_Entreprises' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonCertificatsEntreprisesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonCertificatsEntreprisesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('Certificats_Entreprises');
        $this->setPhpName('CommonCertificatsEntreprises');
        $this->setClassname('Application\\Propel\\Mpe\\CommonCertificatsEntreprises');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, 50, null);
        $this->addColumn('certificat', 'Certificat', 'LONGVARCHAR', false, null, null);
        $this->addColumn('date_debut', 'DateDebut', 'VARCHAR', false, 20, null);
        $this->addColumn('date_fin', 'DateFin', 'VARCHAR', false, 20, null);
        $this->addColumn('old_id_inscrit', 'OldIdInscrit', 'INTEGER', false, null, null);
        $this->addColumn('nom_inscrit', 'NomInscrit', 'VARCHAR', false, 200, null);
        $this->addColumn('prenom_inscrit', 'PrenomInscrit', 'VARCHAR', false, 200, null);
        $this->addColumn('id_entreprise', 'IdEntreprise', 'INTEGER', false, 20, null);
        $this->addColumn('statut_revoque', 'StatutRevoque', 'VARCHAR', false, 20, null);
        $this->addColumn('date_revoquation', 'DateRevoquation', 'VARCHAR', false, 20, null);
        $this->addColumn('mail_inscrit', 'MailInscrit', 'VARCHAR', false, 20, null);
        $this->addForeignKey('id_inscrit', 'IdInscrit', 'BIGINT', 'Inscrit', 'id', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonInscrit', 'Application\\Propel\\Mpe\\CommonInscrit', RelationMap::MANY_TO_ONE, array('id_inscrit' => 'id', ), null, 'CASCADE');
    } // buildRelations()

} // CommonCertificatsEntreprisesTableMap
