<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 'jms_job_related_entities' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonJmsJobRelatedEntitiesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonJmsJobRelatedEntitiesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jms_job_related_entities');
        $this->setPhpName('CommonJmsJobRelatedEntities');
        $this->setClassname('Application\\Propel\\Mpe\\CommonJmsJobRelatedEntities');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('job_id', 'JobId', 'BIGINT' , 'jms_jobs', 'id', true, null, null);
        $this->addPrimaryKey('related_class', 'RelatedClass', 'VARCHAR', true, 150, null);
        $this->addPrimaryKey('related_id', 'RelatedId', 'VARCHAR', true, 100, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonJmsJobs', 'Application\\Propel\\Mpe\\CommonJmsJobs', RelationMap::MANY_TO_ONE, array('job_id' => 'id', ), null, null);
    } // buildRelations()

} // CommonJmsJobRelatedEntitiesTableMap
