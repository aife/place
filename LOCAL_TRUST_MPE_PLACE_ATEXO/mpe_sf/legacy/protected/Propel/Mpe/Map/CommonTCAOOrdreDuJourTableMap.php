<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_CAO_Ordre_Du_Jour' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTCAOOrdreDuJourTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTCAOOrdreDuJourTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_CAO_Ordre_Du_Jour');
        $this->setPhpName('CommonTCAOOrdreDuJour');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTCAOOrdreDuJour');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_ordre_du_jour', 'IdOrdreDuJour', 'BIGINT', true, null, null);
        $this->addForeignPrimaryKey('organisme', 'Organisme', 'VARCHAR' , 'Organisme', 'acronyme', true, 30, null);
        $this->addForeignKey('id_seance', 'IdSeance', 'BIGINT', 't_CAO_Seance', 'id_seance', true, null, null);
        $this->addForeignKey('id_commission', 'IdCommission', 'BIGINT', 't_CAO_Commission', 'id_commission', true, null, null);
        $this->addForeignKey('id_commission_consultation', 'IdCommissionConsultation', 'BIGINT', 't_CAO_Commission_Consultation', 'id_commission_consultation', true, 11, null);
        $this->addColumn('numero', 'Numero', 'TINYINT', true, null, null);
        $this->addColumn('id_ref_org_val_etape', 'IdRefOrgValEtape', 'INTEGER', true, null, null);
        $this->addColumn('intitule', 'Intitule', 'CLOB', true, null, null);
        $this->addForeignKey('date_seance', 'DateSeance', 'TIMESTAMP', 't_CAO_Seance', 'date', true, null, null);
        $this->addColumn('heure_passage', 'HeurePassage', 'TIME', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTCAOSeanceRelatedByDateSeance', 'Application\\Propel\\Mpe\\CommonTCAOSeance', RelationMap::MANY_TO_ONE, array('date_seance' => 'date', ), null, null);
        $this->addRelation('CommonTCAOCommissionConsultation', 'Application\\Propel\\Mpe\\CommonTCAOCommissionConsultation', RelationMap::MANY_TO_ONE, array('id_commission_consultation' => 'id_commission_consultation', ), null, null);
        $this->addRelation('CommonTCAOCommission', 'Application\\Propel\\Mpe\\CommonTCAOCommission', RelationMap::MANY_TO_ONE, array('id_commission' => 'id_commission', ), null, null);
        $this->addRelation('CommonTCAOSeanceRelatedByIdSeance', 'Application\\Propel\\Mpe\\CommonTCAOSeance', RelationMap::MANY_TO_ONE, array('id_seance' => 'id_seance', ), null, null);
        $this->addRelation('CommonOrganisme', 'Application\\Propel\\Mpe\\CommonOrganisme', RelationMap::MANY_TO_ONE, array('organisme' => 'acronyme', ), null, null);
    } // buildRelations()

} // CommonTCAOOrdreDuJourTableMap
