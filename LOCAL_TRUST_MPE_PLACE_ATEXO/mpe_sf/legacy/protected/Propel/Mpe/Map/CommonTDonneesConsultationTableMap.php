<?php

namespace Application\Propel\Mpe\Map;

use Application\Library\Propel\Map\RelationMap;
use Application\Library\Propel\Map\TableMap;


/**
 * This class defines the structure of the 't_donnees_consultation' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.Application.Propel.Mpe.map
 */
class CommonTDonneesConsultationTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Application.Propel.Mpe.map.CommonTDonneesConsultationTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('t_donnees_consultation');
        $this->setPhpName('CommonTDonneesConsultation');
        $this->setClassname('Application\\Propel\\Mpe\\CommonTDonneesConsultation');
        $this->setPackage('Application.Propel.Mpe');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id_donnees_consultation', 'IdDonneesConsultation', 'INTEGER', true, null, null);
        $this->addColumn('reference_consultation', 'ReferenceConsultation', 'INTEGER', false, null, null);
        $this->addForeignKey('id_contrat_titulaire', 'IdContratTitulaire', 'INTEGER', 't_contrat_titulaire', 'id_contrat_titulaire', false, null, null);
        $this->addColumn('organisme', 'Organisme', 'VARCHAR', false, 45, null);
        $this->addColumn('old_service_id', 'OldServiceId', 'INTEGER', false, null, null);
        $this->addColumn('id_type_procedure', 'IdTypeProcedure', 'INTEGER', false, null, null);
        $this->addColumn('libelle_type_procedure', 'LibelleTypeProcedure', 'VARCHAR', false, 100, null);
        $this->addColumn('nbre_offres_recues', 'NbreOffresRecues', 'INTEGER', false, null, null);
        $this->addColumn('nbre_offres_dematerialisees', 'NbreOffresDematerialisees', 'INTEGER', false, null, null);
        $this->addColumn('signature_offre', 'SignatureOffre', 'CHAR', false, null, null);
        $this->addColumn('service_id', 'ServiceId', 'BIGINT', false, null, null);
        $this->addColumn('uuid_externe_exec', 'UuidExterneExec', 'VARCHAR', false, 50, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CommonTContratTitulaire', 'Application\\Propel\\Mpe\\CommonTContratTitulaire', RelationMap::MANY_TO_ONE, array('id_contrat_titulaire' => 'id_contrat_titulaire', ), null, 'CASCADE');
    } // buildRelations()

} // CommonTDonneesConsultationTableMap
