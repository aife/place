<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonGestionAdresses;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Skeleton subclass for representing a row from the 'gestion_adresses' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonGestionAdresses extends BaseCommonGestionAdresses {
public function getAdresseRetraisDossiersTraduit() 
 {
     $langue = Atexo_CurrentUser::readFromSession("lang");
      $retraitDossiers = "adresse_retrais_dossiers_".$langue;
      if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
          || !$this->$retraitDossiers){
            return $this->adresse_retrais_dossiers;
        } else {
            return $this->$retraitDossiers;
        }
 }

 public function getAdresseDepotOffresTraduit() 
 {
     $langue = Atexo_CurrentUser::readFromSession("lang");
      $addDepotDossiers = "adresse_depot_offres_".$langue;
      if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
          || !$this->$addDepotDossiers){
            return $this->adresse_depot_offres;
        } else {
            return $this->$addDepotDossiers;
        }
 }

 public function getLieuOuverturePlisTraduit() 
 {
     $langue = Atexo_CurrentUser::readFromSession("lang");
      $lieuOuverturePlis = "lieu_ouverture_plis_".$langue;
      if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
          || !$this->$lieuOuverturePlis){
            return $this->lieu_ouverture_plis;
        } else {
            return $this->$lieuOuverturePlis;
        }
 }
} // CommonGestionAdresses
