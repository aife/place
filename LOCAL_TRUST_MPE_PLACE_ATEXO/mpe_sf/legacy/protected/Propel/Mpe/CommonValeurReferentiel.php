<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonValeurReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Skeleton subclass for representing a row from the 'ValeurReferentiel' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonValeurReferentiel extends BaseCommonValeurReferentiel {

	public function getLibelleValeurReferentielTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $libelle_valeur_referentiel = "libelle_valeur_referentiel_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$libelle_valeur_referentiel){
            return $this->libelle_valeur_referentiel;
        } else {
            return $this->$libelle_valeur_referentiel;
        }
	}

	public function getLibelleValeurReferentielTraduitByLang($langue)
	{
	   	$libelle_valeur_referentiel = "libelle_valeur_referentiel_".$langue;
 	    return $this->$libelle_valeur_referentiel;
	}

} // CommonValeurReferentiel
