<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

  // include base peer class


  // include object class
  use Application\Propel\Mpe\Om\BaseCommonGeolocalisationN2Peer;


  /**
 * Skeleton subclass for performing query and update operations on the 'GeolocalisationN2' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonGeolocalisationN2Peer extends BaseCommonGeolocalisationN2Peer {

} // CommonGeolocalisationN2Peer
