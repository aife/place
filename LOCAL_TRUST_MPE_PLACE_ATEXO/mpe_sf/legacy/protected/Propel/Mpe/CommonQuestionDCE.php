<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\Entreprise;




use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Exception\PropelException;
use Application\Propel\Mpe\Om\BaseCommonQuestionDCE;

/**
 * Skeleton subclass for representing a row from the 'QuestionDCE' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonQuestionDCE extends BaseCommonQuestionDCE {
    /**
     * Get the associated Entreprise object
     *
     * @param PropelPDO $con Optional Connection object.
     * @param $doQuery Executes a query to get the object if required
     * @return Entreprise The associated Entreprise object.
     * @throws PropelException
     */
    public function getCommonEntreprise(PropelPDO $con = null, $doQuery = true)
    {
        return $this->getEntreprise($con,$doQuery);
    }
} // CommonQuestionDCE
