<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonVisiteLieux;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Traduction;

/**
 * Skeleton subclass for representing a row from the 'visite_lieux' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonVisiteLieux extends BaseCommonVisiteLieux {


	public function getAdresseTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $adresse = "adresse_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$adresse){
            return $this->adresse;
        } else {
            return $this->$adresse;
        }
	}

	/**
	 * Enregistre la traduction de la réunion
	 * @param string $langue: langue de naviguation
	 * @param string $libelle: le libellé de la traduction
	 */
	public function setAdresseTraduit($langue, $libelle)
    {
        if(!$langue){
        	$langue = Atexo_CurrentUser::readFromSession("lang"); 
        }
    	if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0) {
   			$this->setAdresse($libelle);
   	    }
        $idTraduction = (new Atexo_Traduction())->setTraduction($this->getIdTrAdresse(), $langue, $libelle);
        $this->setIdTrAdresse($idTraduction);
    }

} // CommonVisiteLieux
