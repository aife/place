<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Library\Propel\Connection\PropelPDO;
use \PDO;
use \Exception;
use AtexoCrypto\Dto\Enveloppe;
use Application\Propel\Mpe\CommonOffresPeer;

use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonAgent;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonOffres;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Groupement;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;


/**
 * Skeleton subclass for representing a row from the 'Offres' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonOffres extends BaseCommonOffres {
	protected $_nombreLotsAdmissibles=0;
	protected $_nombreLotsNonAdmissibles=0;
	protected $_nombreLotsATraiter=0;
	protected $_moreThanOneLot=false;
	protected $_nomEntreprise="";
	protected $_numPli;
	protected $_commonDecisionEnveloppe;
	protected $_codePostal;
	protected $_resultatAnalyse;
	protected $_consultation;

	protected $_contentHorodatage = null;
	protected $_contentSignatureenvxml = null;
	protected $_contentHorodatageEnvoiDiffere = null;
	protected $_contentHorodatageAnnulation = null;
	protected $_contentSignatureenvxmlEnvoiDiffere = null;

	protected $_nom="";
	protected $_prenom="";
	protected $_email="";
	protected $_telephone="";
	protected $_fax="";
	protected $_siret="";
	protected $_adresse="";
	protected $_adresse2="";
	protected $_ville="";
	protected $_pays="";
	protected $_hasGroupement;
	protected $_nombreCotraitant;
	protected $_nombreSoustraitant;
	protected $_hasMembreGroupement;
	protected $_enveloppesToDechiffre = array();


	/**
	 * Get the value of [_nom] column.
	 *
	 * @return string  nom
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function getNom()
	{
		return $this->_nom ?: $this->nom_inscrit;
	}

	/**
	 * Set the value of [_nom] column.
	 *
	 * @param string $v new value
	 * @return void
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function setNom($v)
	{
		$this->_nom = $v;
	}

	/**
	 * Get the value of [_prenom] column.
	 *
	 * @return string  prenom
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function getPrenom()
	{
		return $this->_prenom ?: $this->prenom_inscrit;
	}

	/**
	 * Set the value of [_prenom] column.
	 *
	 * @param string $v new value
	 * @return void
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function setPrenom($v)
	{
		$this->_prenom = $v;
	}

	/**
	 * Get the value of [_email] column.
	 *
	 * @return string email
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function getEmail()
	{
		return $this->_email ?: $this->email_inscrit;
	}

	/**
	 * Set the value of [_email] column.
	 *
	 * @param string $v new value
	 * @return void
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function setEmail($v)
	{
		$this->_email = $v;
	}

	/**
	 * Get the value of [_telephone] column.
	 *
	 * @return string telephone
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function getTelephone()
	{
		return $this->_telephone ?: $this->telephone_inscrit;
	}

	/**
	 * Set the value of [_telephone] column.
	 *
	 * @param string $v new value
	 * @return void
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function setTelephone($v)
	{
		$this->_telephone = $v;
	}

	/**
	 * Get the value of [_fax] column.
	 *
	 * @return string fax
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function getFax()
	{
		return $this->_fax ?: $this->fax_inscrit;
	}

	/**
	 * Set the value of [_fax] column.
	 *
	 * @param string $v new value
	 * @return void
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function setFax($v)
	{
		$this->_fax = $v;
	}

	/**
	 * Get the value of [_siret] column.
	 *
	 * @return string siret
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function getSiret()
	{
		return $this->_siret ?: $this->siret_inscrit;
	}

	/**
	 * Set the value of [_siret] column.
	 *
	 * @param string $v new value
	 * @return void
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function setSiret($v)
	{
		$this->_siret = $v;
	}

	/**
	 * Set the value of [_adresse] column.
	 *
	 * @param string $v new value
	 * @return void
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function setAdresse($v)
	{
		$this->_adresse = $v;
	}

	/**
	 * Get the value of [_adresse] column.
	 *
	 * @return string adresse
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function getAdresse()
	{
		return $this->_adresse ?: $this->adresse_inscrit;
	}

	/**
	 * Set the value of [_adresse2] column.
	 *
	 * @param string $v new value
	 * @return void
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function setAdresse2($v)
	{
		$this->_adresse2 = $v;
	}

	/**
	 * Get the value of [_adresse2] column.
	 *
	 * @return string adresse
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function getAdresse2()
	{
		return$this->_adresse2 ?: $this->adresse2_inscrit;
	}

	/**
	 * Set the value of [_ville] column.
	 *
	 * @param string $v new value
	 * @return void
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function setVille($v)
	{
		$this->_ville = $v;
	}

	/**
	 * Get the value of [_ville] column.
	 *
	 * @return string ville
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function getVille()
	{
		return $this->_ville ?: $this->ville_inscrit;
	}

	/**
	 * Set the value of [_pays] column.
	 *
	 * @param string $v new value
	 * @return void
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function setPays($v)
	{
		$this->_pays = $v;
	}

	/**
	 * Get the value of [_pays] column.
	 *
	 * @return string pays
	 * @author LEZ <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function getPays()
	{
		return $this->_pays ?: $this->pays_inscrit;
	}




	public function getHorodatage() {
		if(!$this->_contentHorodatage) {
			$this->_contentHorodatage = stream_get_contents( parent::getHorodatage());
		}
		return $this->_contentHorodatage;
	}
	public function getSignatureenvxml()
    {
    	if(!$this->_contentSignatureenvxml) {
			$this->_contentSignatureenvxml = stream_get_contents( parent::getSignatureenvxml());
		}
		return $this->_contentSignatureenvxml;
	}
	public function getHorodatageEnvoiDiffere()
    {
    	if(!$this->_contentHorodatageEnvoiDiffere) {
			$this->_contentHorodatageEnvoiDiffere = stream_get_contents( parent::getHorodatageEnvoiDiffere());
		}
		return $this->_contentHorodatageEnvoiDiffere;
    }
 	public function getSignatureenvxmlEnvoiDiffere()
    {
		if(!$this->_contentSignatureenvxmlEnvoiDiffere) {
			$this->_contentSignatureenvxmlEnvoiDiffere = stream_get_contents( parent::getSignatureenvxmlEnvoiDiffere());
		}
		return $this->_contentSignatureenvxmlEnvoiDiffere;
    }
    public function getHorodatageAnnulation()
    {
    	if(!$this->_contentHorodatageAnnulation) {
			$this->_contentHorodatageAnnulation = stream_get_contents( parent::getHorodatageAnnulation());
		}
		return $this->_contentHorodatageAnnulation;

    }
	public function getNomEntreprise()
	{
		return $this->_nomEntreprise;
	}

	public function setNomEntreprise($nomEntreprise)
	{
		$this->_nomEntreprise=$nomEntreprise;
	}

	public function getDateRemisePlie()
	{
		$dateRemise = Atexo_Util::iso2frnDateTime($this->getUntrusteddate(), false, true);
		if(!$dateRemise && $this->getHorodatage()){
			$dateRemise = (new Atexo_Consultation_Responses())->saveDateRemisePlis($this->getId());
		}
		return $dateRemise;
	}

	public function getNombreLotsAdmissibles()
	{
		return $this->_nombreLotsAdmissibles;	
	}

	public function setNombreLotsAdmissibles($nombreLotsAdmissibles)
	{
		$this->_nombreLotsAdmissibles=$nombreLotsAdmissibles;	
	}

	public function getNombreLotsNonAdmissibles()
	{
		return $this->_nombreLotsNonAdmissibles;
	}

	public function setNombreLotsNonAdmissibles($nombreLotsNonAdmissibles)
	{
		$this->_nombreLotsNonAdmissibles=$nombreLotsNonAdmissibles;
	}

	public function getNombreLotsATraiter()
	{
		return $this->_nombreLotsATraiter;
	}

	public function setNombreLotsATraiter($nombreLotsATraiter)
	{
		$this->_nombreLotsATraiter=$nombreLotsATraiter;
	}

	public function getNombreLots()
	{
		return (self::getNombreLotsAdmissibles())+(self::getNombreLotsNonAdmissibles())+(self::getNombreLotsATraiter());
	}

	public function setMoreThanOneLot($moreThanOneLot)
	{
		$this->_moreThanOneLot=$moreThanOneLot;
	}

	public function getMoreThanOneLot()
	{
		return $this->_moreThanOneLot;
	}

	public function getNumPli() 
	{
		return $this->_numPli;
	}

	public function setNumPli($numPli) 
	{
		$this->_numPli=$numPli;
	}


	public function getCommonDecisionEnveloppe() 
	{
		return $this->_commonDecisionEnveloppe;
	}

	public function setCommonDecisionEnveloppe($commonDecisionEnveloppe) 
	{
		$this->_commonDecisionEnveloppe=$commonDecisionEnveloppe;
	}

	public function getCodePostal() 
	{
		return $this->_codePostal;
	}

	public function setCodePostal($codePostal) 
	{
		$this->_codePostal=$codePostal;
	}

	public function getTypeEnveloppe()
	{
		return Atexo_Config::getParameter('DEPOT_ELECTRONIQUE');
	}

	public function setResultatAnalyse($value)
	{
		$this->_resultatAnalyse=$value;
	}

	public function getResultatAnalyse()
	{
		return $this->_resultatAnalyse;
	}

	public function setConsultation($obj)
	{
		$this->_consultation=$obj;
	}

	public function getConsultation()
	{
		return $this->_consultation;
	}

    public function getCommonConsultation(PropelPDO $connexionCom = null, $doQuery = true)
    {
		if(!$connexionCom){
        	$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
    	$c=new Criteria();
        $c->add(CommonConsultationPeer::ID,$this->getConsultationId());
        $c->add(CommonConsultationPeer::ORGANISME,$this->getOrganisme());
        $commonConsultation = CommonConsultationPeer::doSelectOne($c,$connexionCom);
        return $commonConsultation;
	}
	public function countCommonEnveloppes($criteria = null, $distinct = false, $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}

		$criteria->add(CommonEnveloppePeer::OFFRE_ID, $this->getId());
		$criteria->add(CommonEnveloppePeer::ORGANISME, $this->getOrganisme());

		return CommonEnveloppePeer::doCount($criteria, $distinct, $con);
	}

	/**
	 * Permet de recuperer les offres d'une enveloppe
	 * @param $listeOrdonnee: si ce parametre est renseigné (valeur=true), 
	 * alors le resultat correspond à une liste ordonnée des enveloppes par numéro lot
	 */
	public function getCommonEnveloppes($listeOrdonnee=false, $typeEnveloppe = null, $lot = null,$connexionCom=false)
	{
		if(!$connexionCom) {
			$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
		}
		$criteria = new Criteria();
		$criteria->add(CommonEnveloppePeer::OFFRE_ID, $this->getId());
		$criteria->add(CommonEnveloppePeer::ORGANISME, $this->getOrganisme());
		if ($typeEnveloppe) {
			$criteria->add(CommonEnveloppePeer::TYPE_ENV, $typeEnveloppe);
		}
		if ($lot !== null) {
			$criteria->add(CommonEnveloppePeer::SOUS_PLI, $lot);
		}
		if($listeOrdonnee) {
			$criteria->addAscendingOrderByColumn(CommonEnveloppePeer::SOUS_PLI);	
		}
		return CommonEnveloppePeer::doSelect($criteria, $connexionCom);
	}

	/*
	 * retourne libelle de statut de l'offre
	 */
	public function getLibelleStatutOffres()
	{
		$libelleStatutOffres = "";
		$statutOffres = $this->getStatutOffres();
		if($statutOffres == Atexo_Config::getParameter('STATUT_ENV_FERME')){
			$libelleStatutOffres = Prado::localize('FERMEE');
		}elseif($statutOffres == Atexo_Config::getParameter('STATUT_ENV_REFUSEE')){
			$libelleStatutOffres = Prado::localize('DEFINE_REFUSEE');
		}elseif($statutOffres == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')){
			$libelleStatutOffres = Prado::localize('DEFINE_OUVERTE_EN_LIGNE');
		}elseif($statutOffres == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')){
			$libelleStatutOffres = Prado::localize('DEFINE_OUVERTE_HORS_LIGNE');
		}elseif($statutOffres == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')){
			$libelleStatutOffres = Prado::localize('DEFINE_OUVERTE_A_DISTANCE');
		}elseif($statutOffres == Atexo_Config::getParameter('STATUT_ENV_CHIFFREMENT_EN_COURS')){
			$libelleStatutOffres = Prado::localize('DEFINE_EN_COURS_CHIFFREMENT');
		}elseif($statutOffres == Atexo_Config::getParameter('STATUT_ENV_DECHIFFREMENT_EN_COURS')){
			$libelleStatutOffres = Prado::localize('DEFINE_EN_COURS_DECHIFFREMENT');
		}elseif($statutOffres == Atexo_Config::getParameter('STATUT_ENV_EN_ATTENTE_CHIFFREMENT')){
			$libelleStatutOffres = Prado::localize('DEFINE_EN_ATTENTE_CHIFFREMENT');
		}elseif($statutOffres == Atexo_Config::getParameter('STATUT_ENV_FERMETURE_EN_COURS')){
			$libelleStatutOffres = Prado::localize('DEFINE_EN_COURS_FERMETURE');
		}elseif($statutOffres == Atexo_Config::getParameter('STATUT_ENV_FERMETURE_EN_ATTENTE')){
			$libelleStatutOffres = Prado::localize('DEFINE_EN_ATTENTE_FERMETURE');
		}
		return $libelleStatutOffres;
	}

	public function getVerificationSignatureReponseOA($organisme, $consultationSigne = null)
	{
		$result = null;
		if ($this->getStatutOffres() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
			|| $this->getStatutOffres() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
			|| $this->getStatutOffres() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
			|| $this->getStatutOffres() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
		) {
			if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') && !Atexo_Module::isEnabled('ModeApplet')) {
				$result = $this->getVerificationSignatureOffre();
			}

			if (empty($result)) {
				$result = $this->getResultatValiditeSignatureReponse($organisme, $consultationSigne);
			}
		}

		return $result;

	}
	/*
	 * Permet d'avoir le statut de signature des enveloppes de l'offres
	 * Remarque le statut 'NOK' est plus prioritaire que le statut 'UNKNOWN'
	 */
	public function getResultatValiditeSignatureReponse($organisme, $consultationSigne=null)
	{
		$enveloppes = $this->getCommonEnveloppes();
		$resultat = Atexo_Config::getParameter('OK');
		$signatureNotOk = false;
		$signatureUnknown = false;
		$signatureSans = false;
		foreach($enveloppes as $enveloppe){
			$statut = $enveloppe->getResultatValiditeSignature($organisme, $consultationSigne);
			if($statut == Atexo_Config::getParameter('NOK')){
				$signatureNotOk = true;
				break;
			}elseif($statut == Atexo_Config::getParameter('UNKNOWN')){
				$signatureUnknown = true;
			}elseif($statut == Atexo_Config::getParameter('SANS')){
				$signatureSans = true;
			}
		}
		if($signatureNotOk){
			$resultat = Atexo_Config::getParameter('NOK');
		}else {
			if ($signatureUnknown) {
				$resultat = Atexo_Config::getParameter('UNKNOWN');
			}
			if ($signatureSans) {
				$resultat = Atexo_Config::getParameter('SANS');
			}
		}

		if(Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') && !Atexo_Module::isEnabled('ModeApplet') && $this->getVerificationSignatureOffre() != $resultat) {
			$connexion= Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
			$offre = CommonOffresPeer::retrieveByPK($this->getId(), $this->getOrganisme());
			$offre->setVerificationSignatureOffre($resultat);
			$offre->save($connexion);
		}
		return $resultat;

	}

	/*
	 * retourne la taille des fichiers pour tout les enveloppes de l'offre
	 */
	public function getFileSizeOffres()
	{
		$enveloppes = $this->getCommonEnveloppes();
		$tailleFichier=0;
		foreach($enveloppes as $enveloppe){
			$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
			$c=new Criteria();
			$c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $enveloppe->getIdEnveloppeElectro());
			$c->add(CommonFichierEnveloppePeer::ORGANISME, $this->getOrganisme());
			$fichiers=CommonFichierEnveloppePeer::doSelect($c, $connexion);
			if($fichiers && is_array($fichiers)) {
				foreach($fichiers as $fichier) {
					$tailleFichier += $fichier->getTailleFichier()/1024;
				}
			}
		}
		return '(' . Atexo_Util::arrondirSizeFile($tailleFichier) . ')';
	}
	/*
	 * retourne le nombre de reponse(enveloppe) pour l'ensemble des lots  
	 */
	public function getNombreLotsRepondus($typeEnveloppe,$alloti = true)
	{
		if($alloti){
			$count = 0;
			$sql = " SELECT count(*) as count FROM Enveloppe WHERE organisme = '".$this->getOrganisme()."' AND offre_id = '".
					$this->getId()."' AND type_env = '".$typeEnveloppe."'; ";
			$statement = Atexo_Db::getLinkCommon(true)->prepare($sql); 
			$results = $statement->execute();
			if($results){
		         while ($data = $statement->fetch(PDO::FETCH_ASSOC)) {
		         		$count = $data["count"];
		         }
	        }
	        return $count;
		} 
		return ''; 				 	 
	}

	public function isOffreOuvertOuRejete(){
		if(($this->getStatutOffres()== Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')) ||
		   ($this->getStatutOffres()==Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')) ||
		   ($this->getStatutOffres()==Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE'))||
		   ($this->getStatutOffres()==Atexo_Config::getParameter('STATUT_ENV_REFUSEE')) ){
		   	return true;
		   }
		return false;
	}

	/**
	 * Cette méthode permet de récupérer le type de signature associé à l'objet 
	 * type XML dans ce cas signature XADES, sinon type P7S dans ce cas signature PKCS7
	 */
	public function getTypeSignature () {
		$typeSignature = "P7S";

		$xmlStr = $this->getXmlString();
		$pos = strpos($xmlStr, "typeSignature");
		if($pos>0) {
			$typeSignature = "XML";
		}
		return $typeSignature;

	}
	/*
	 * Permet d'avoir le nom et prenom del l'agent qu'y a téléchargé l'offre
	 */
	public function getNomAgentTelechargementOffre($prefix="")
 	{
 		$agent=(new Atexo_Agent())->retrieveAgent($this->getAgentTelechargementOffre());
 		return ($agent instanceof CommonAgent)?  ($prefix? $prefix." " : "").$agent->getPrenom()." ".$agent->getNom() : ""; 
 	}

 	/**
 	 * Permet de retourner le numero du pays de l'offre
 	 * 
 	 * @return Intger le numero du pays de l'offre
 	 * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
 	public function getNumPays()
 	{
 		return (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdsByDenomination1(strtoupper($this->getPays()));
 	}

	/**
	 * Permet de savoir si l'entreprise qui y a depose l'offre est locale ou etranger.
	 *
	 * @return Bollean true si l'entreprise est locale,si non false
	 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function isEntrepriseOffreLocale()
	{
		if($this->getSiretEntreprise() && $this->getPaysInscrit() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')){
			return true;
		}
		return false;
	}

	public function hasGroupement()
	{
		if(!isset($this->_hasGroupement)){
			$this->setHasGroupement($this->countCommonTGroupementEntreprises());
		}
		return $this->_hasGroupement;
	}

	/**
	 * @return mixed
	 */
	public function getHasGroupement()
	{
		return $this->_hasGroupement;
	}

	/**
	 * @param mixed $hasGroupement
	 */
	public function setHasGroupement($hasGroupement)
	{
		$this->_hasGroupement = $hasGroupement;
	}

	/**
	 * @return mixed
	 */
	public function hasMembreGroupement()
	{
		return $this->getHasMembreGroupement();
	}

	/**
	 * @return mixed
	 */
	public function getHasMembreGroupement()
	{
		if(!isset($this->_hasMembreGroupement)){
			$this->_hasMembreGroupement = (new Atexo_Groupement())->retrieveMemebreGroupement($this->getId());
		}
		return $this->_hasMembreGroupement;
	}

	/**
	 * @param mixed $hasMembreGroupement
	 */
	public function setHasMembreGroupement($hasMembreGroupement)
	{
		$this->_hasMembreGroupement = $hasMembreGroupement;
	}

	/**
	 * @return mixed
	 */
	public function getNombreSoustraitant()
	{
		if(!isset($this->_nombreSoustraitant)){
			$this->_nombreSoustraitant = (new Atexo_Groupement())->getNombreSousMemebreGroupement($this->getId());
		}
		return $this->_nombreSoustraitant;
	}

	/**
	 * @param mixed $nombreSoustraitant
	 */
	public function setNombreSoustraitant($nombreSoustraitant)
	{
		$this->_nombreSoustraitant = $nombreSoustraitant;
	}

	/**
	 * @return mixed
	 */
	public function getNombreCotraitant()
	{
		if(!isset($this->_nombreCotraitant)){
			$this->_nombreCotraitant = (new Atexo_Groupement())->retrieveMemebreGroupement($this->getId(), Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_CO_TRAITANT'));
		}
		return $this->_nombreCotraitant;
	}

	/**
	 * @param mixed $nombreCotraitant
	 */
	public function setNombreCotraitant($nombreCotraitant)
	{
		$this->_nombreCotraitant = $nombreCotraitant;
	}

	/**
	 * @return array
	 */
	public function getEnveloppesToDechiffre()
	{
		return $this->_enveloppesToDechiffre;
	}

	/**
	 * @param array $enveloppeToDechiffre
	 */
	public function setEnveloppesToDechiffre($enveloppesToDechiffre)
	{
		$this->_enveloppesToDechiffre = $enveloppesToDechiffre;
	}

	/**
	 * @param array $enveloppeToDechiffre
	 */
	public function addEnveloppeToDechiffre($enveloppeToDechiffre)
	{
		$this->_enveloppesToDechiffre[] = $enveloppeToDechiffre;
	}

	/**
	 * Permet de retourner les statuts des hash des fichiers d'une offre
	 *
	 * @return boolean true si tt les fichiers sont OK,false si au moins un fichier est KO
	 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 2017-place
	 * @copyright Atexo 2018
	 */
	public function isHashFichiersOk()
	{
		$enveloppes = $this->getCommonEnveloppes();
		$verificationHashFileOk = '1';
		if(is_array($enveloppes)){
			foreach($enveloppes as $enveloppe) {
				if($enveloppe instanceof CommonEnveloppe && $enveloppe->isHashFichiersOk() === '0'){
					$verificationHashFileOk = '0';
					break;
				}
			}
		}
		return $verificationHashFileOk;

	}

    /***
     * @param string $v
     * @return CommonOffres|void
     * @throws Exception
     */
    public function setXmlString($v)
    {
        try {
            $fileName = $this->getOrganisme() . '-' . $this->getConsultationRef() . '-' . $this->getId() . '.xml';
            $filePath = Atexo_Config::getParameter('COMMON_TMP') . $fileName;
            file_put_contents($filePath, $v);
            if (is_file($filePath) && filesize($filePath)) {
                $FolderDestination = $this->getOrganisme();
                $subfolder = "xml_reponse";
                $atexoBlob = new Atexo_Blob();
                $idBlob = $atexoBlob->insert_blob($fileName, $filePath, $FolderDestination, $subfolder);
                parent::setIdBlobXmlReponse($idBlob);
                unlink($filePath);
            } else {
                throw new Exception("xml string vide, id offre : " . $this->getId());
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function getXmlString()
    {
        $subfolder = null;
        $atexoBlob = new Atexo_Blob();

         $xmlString = $atexoBlob->return_blob_to_client($this->getIdBlobXmlReponse(),
            $this->getOrganisme(),
            false,
            $subfolder);

         if ($xmlString == '') {
             $xmlString =  parent::getXmlString();
         }

        return $xmlString;
    }


} // CommonOffres
