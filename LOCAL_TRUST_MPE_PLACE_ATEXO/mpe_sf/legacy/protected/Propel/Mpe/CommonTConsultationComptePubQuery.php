<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use \Exception;

use Application\Propel\Mpe\CommonTConsultationComptePub;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonTConsultationComptePubQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;


/**
 * Skeleton subclass for performing query and update operations on the 't_consultation_compte_pub' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTConsultationComptePubQuery extends BaseCommonTConsultationComptePubQuery
{
    /**
     * Permet de recuperer le compte de publicite
     *
     * @param string $idBoamp : id du compte boamp (recupere depuis la table "AcheteurPublic")
     * @param string $organisme : organisme
     * @param Logger $logger
     * @param null|PropelPDO $connexion
     * @return CommonTConsultationComptePub
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @version 1.0
     * @since 2016-les-echos
     * @copyright Atexo 2016
     * @package controls
     */
    public function recupererComptePubByIdBoampAndOrg($idBoamp, $organisme, $logger, $connexion=null)
    {
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }
            return self::create()->filterByIdComptePub($idBoamp)->filterByOrganisme($organisme)->findOne($connexion);
        } catch (Exception $e) {
            $logger->error("Erreur lors de la recuperation du compte de publicite. Methode = CommonTConsultationComptePubQuery::recupererComptePubByIdBoampAndOrg " . PHP_EOL . " Erreur: " . $e->getMessage() . PHP_EOL . "Trace: " . $e->getTraceAsString());
        }
    }

    /**
     * Permet de recuperer le compte de publicite
     *
     * @param string $idDonneesComp : id donnees complementaires
     * @param string $organisme : organisme
     * @param Logger $logger
     * @param null|PropelPDO $connexion
     * @return CommonTConsultationComptePub
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @version 1.0
     * @since 2016-les-echos
     * @copyright Atexo 2016
     * @package controls
     */
    public function recupererComptePubByIdDonneesCompAndOrg($idDonneesComp, $organisme, $logger = null, $connexion=null)
    {
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }
            return self::create()->filterByIdDonneesComplementaires($idDonneesComp)->filterByOrganisme($organisme)->findOne($connexion);
        } catch (Exception $e) {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('publicite');
            }
            $logger->error("Erreur lors de la recuperation du compte de publicite. Methode = CommonTConsultationComptePubQuery::recupererComptePubByIdDonneesCompAndOrg " . PHP_EOL . " Erreur: " . $e->getMessage() . PHP_EOL . "Trace: " . $e->getTraceAsString());
        }
    }
}
