<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonEchangePeer;





use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonEchangeDestinataire;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for representing a row from the 'EchangeDestinataire' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEchangeDestinataire extends BaseCommonEchangeDestinataire {

    public function getCommonEchange(PropelPDO $connexion = null, $doQuery = true)
    {
        if(!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $echange = CommonEchangePeer::retrieveByPK($this->id_echange, $connexion);
        if(!$echange) {
            return false;
        }
        return $echange;
    }
} // CommonEchangeDestinataire
