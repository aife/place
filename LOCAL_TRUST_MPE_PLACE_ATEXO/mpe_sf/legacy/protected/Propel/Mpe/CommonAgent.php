<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonHabilitationAgentPeer;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonComptesAgentsAssociesPeer;




use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonAgent;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for representing a row from the 'Agent' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */
class CommonAgent extends BaseCommonAgent {
    protected bool $_applicationsMetiers = false;
    protected bool|string $_entityPath = false;
    protected string $_idCertificat = '';
    protected string $_statutCertificat = '';
    protected string $_actionRevoquer = '';
    protected string $_actionCreer = '';
    protected array $_referentiel = array();
    protected string $_denomonationOrg = ''; 

    public function getReferentiel()
    {
        return $this->_referentiel;
    }

    public function setReferentiel($_referentiel)
    {
        $this->_referentiel = $_referentiel;
    }

    public function getServiceMetiers()
    {
        return $this->_applicationsMetiers;
    }

    public function setServiceMetiers($applicationsMetiers)
    {
        $this->_applicationsMetiers = $applicationsMetiers;
    }

    public function getEntityPath()
    {
        return $this->_entityPath;
    }

    public function setEntityPath($entityPath)
    {
        $this->_entityPath = $entityPath;
    }

    public function getIdCertificat()
    {
        return $this->_idCertificat;
    }

    public function setIdCertificat($value)
    {
        $this->_idCertificat = $value;
    }

   public function getStatutCertificat()
    {
        return $this->_statutCertificat;
    }

    public function setStatutCertificat($value)
    {
        $this->_statutCertificat = $value;
    }

    public function getActionRevoquer()
    {
        return $this->_actionRevoquer;
    }

    public function setActionRevoquer($value)
    {
        $this->_actionRevoquer = $value;
    }

     public function getActionCreer()
    {
        return $this->_actionCreer;
    }

    public function setActionCreer($value)
    {
        $this->_actionCreer = $value;
    }

    //commission
	public function getOrganisation() {
 		 return ""; 
 	} 
	public function getFonction() {
		 return ""; 
	}
	public function getTypeVoix() {
		 return ""; 
	}

	public function setDenomonationOrg($value)
    {
        $this->_denomonationOrg = $value;
    }

     public function getDenomonationOrg()
    {
        return $this->_denomonationOrg;
    }
	/**
	 * Fonction permet de recuperer les comptes secondaires d'un
	 * compte principale 
	 * @param id compte principale
	 */
    public function getComptesAssocieSecondaire($idPrincipal){
   		$c=new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonComptesAgentsAssociesPeer::COMPTE_SECONDAIRE,$this->getId());
        $c->add(CommonComptesAgentsAssociesPeer::COMPTE_PRINCIPAL,$idPrincipal);
        $comptesSecondaire = CommonComptesAgentsAssociesPeer::doSelect($c,$connexion);
        if($comptesSecondaire) {
            return $comptesSecondaire;
        }
        return false;
    }
    /**
	 * Fonction permet savoir si un compte a une association 
	 * soit en tant que compte principale ou secondaire
	 * return les comptes soit  false
	 */
    public function hasAssociation(){
    	$c=new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $cton1 = $c->getNewCriterion(CommonComptesAgentsAssociesPeer::COMPTE_SECONDAIRE,$this->getId());
        $cton2 = $c->getNewCriterion(CommonComptesAgentsAssociesPeer::COMPTE_PRINCIPAL,$this->getId());
		$cton1->addOr($cton2);
		$c->add($cton1);
        $comptesSecondaire = CommonComptesAgentsAssociesPeer::doSelect($c,$connexion);
        if($comptesSecondaire) {
            return $comptesSecondaire;
        }
        return false;
    }
     /**
	 * Fonction permet de tester si l'association est confirmé pour le id
	 * compte principale passe en parametre
	 * return Boolean
	 */
    public function isAssociationCompteConfirme($idComptePrincipale , $typeAssociation){
    	$c=new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if($typeAssociation == Atexo_Config::getParameter('TYPE_ASSOCIATION_COMPTE_PRINCIPALE')) {
	        $c->add(CommonComptesAgentsAssociesPeer::COMPTE_SECONDAIRE,$this->getId());
	        $c->add(CommonComptesAgentsAssociesPeer::COMPTE_PRINCIPAL,$idComptePrincipale);
    	} else {
    	    $c->add(CommonComptesAgentsAssociesPeer::COMPTE_SECONDAIRE,$idComptePrincipale);
	        $c->add(CommonComptesAgentsAssociesPeer::COMPTE_PRINCIPAL,$this->getId());
    	}
        $c->add(CommonComptesAgentsAssociesPeer::STATUT_ACTIVATION_COMPTE_SECONDAIRE,1);
        $comptesSecondaire = CommonComptesAgentsAssociesPeer::doSelectone($c,$connexion);//print_r($connexion);exit;
        if($comptesSecondaire) {
            return true;
        }
        return false;
    }

 	/**
	 * Methode developper pour recuper les habilitations d'un agent 
	 * 
	 */
	public function getCommonHabilitationAgents($criteria = null, $con = null)
	{
		if ($con === null) {
			$con = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		}
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		} 
		if ($criteria instanceof Criteria){
			$criteria->add(CommonHabilitationAgentPeer::ID_AGENT, $this->getId());
			$habilitationAgent = CommonHabilitationAgentPeer::doSelect($criteria,$con);
			if($habilitationAgent[0] instanceOf CommonHabilitationAgent){
				return $habilitationAgent;
			}
		}
		return false;
	}
} // CommonAgent
