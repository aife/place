<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonTypeProcedureConcessionPivotPeer;

/**
 * Skeleton subclass for performing query and update operations on the 'type_procedure_concession_pivot' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTypeProcedureConcessionPivotPeer extends BaseCommonTypeProcedureConcessionPivotPeer
{
}
