<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Propel\Mpe\CommonEntreprisePeer;




use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonTraceOperationsInscrit;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;

/**
 * Skeleton subclass for representing a row from the 'trace_operations_inscrit' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonTraceOperationsInscrit extends BaseCommonTraceOperationsInscrit {

  /*
	 * retourne l'objet entreprise
	 */
	public function getEntreprise($nomEntreprise = null)
	{
        if($this->getIdEntreprise()==0) return false;

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		$c = new Criteria();
		$c->add(CommonEntreprisePeer::ID,$this->getIdEntreprise());
		$entreprise = CommonEntreprisePeer::doSelectOne($c, $connexion);
		if($entreprise){
			if($nomEntreprise){
				return $entreprise->getNom();
			}
			return $entreprise;
		}
		return false;
	}
	public function getNomPrenomInscrit()
	{
		$inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($this->getIdInscrit());
		if($inscrit){
			$nom = $inscrit->getNom();
			$nom .= " ".$inscrit->getPrenom();
			return $nom;
		}
		return false;
	}
	public function getPrenomInscrit()
	{
		$inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($this->getIdInscrit());
		if($inscrit){
			$prenom = $inscrit->getPrenom();
			return $prenom;
		}
		return false;
	}
	public function getNomInscrit()
	{
		$inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($this->getIdInscrit());
		if($inscrit){
			$nom = $inscrit->getNom();
			return $nom;
		}
		return false;
	}
} // CommonTraceOperationsInscrit
