<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonHeliosCosignature;

/**
 * Skeleton subclass for representing a row from the 'Helios_cosignature' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonHeliosCosignature extends BaseCommonHeliosCosignature {

	private $_contentHorodatageActe = null;
	public function getHorodatageActe() {
		if(!$this->_contentHorodatageActe) {
			$this->_contentHorodatageActe = stream_get_contents( parent::getHorodatageActe());
		}
		return $this->_contentHorodatageActe;
	}

} // CommonHeliosCosignature
