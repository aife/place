<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonTTypeGroupementEntrepriseQuery;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for performing query and update operations on the 't_type_groupement_entreprise' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTTypeGroupementEntrepriseQuery extends BaseCommonTTypeGroupementEntrepriseQuery
{

    /**
     * Permet de retourner le type de groupement by id
     *
     * @param $idGroupement l'id type groupement
     * @param null $connexion
     * @return CommonTTypeGroupementEntreprise
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public function getTypeGroupementById($idGroupement,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        return $this->filterByIdTypeGroupement($idGroupement)->findOne($connexion);

    }
}
