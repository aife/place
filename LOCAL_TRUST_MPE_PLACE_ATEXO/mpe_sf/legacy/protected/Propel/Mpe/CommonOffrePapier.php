<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Library\Propel\Connection\PropelPDO;
use AtexoCrypto\Dto\Offre;

use Application\Propel\Mpe\CommonEnveloppePapierPeer;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonOffrePapier;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;


/**
 * Skeleton subclass for representing a row from the 'Offre_papier' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */
class CommonOffrePapier extends BaseCommonOffrePapier {

	protected int $_nombreLotsAdmissibles=0;
	protected int $_nombreLotsNonAdmissibles=0;
	protected int $_nombreLotsATraiter=0;
	protected bool $_moreThanOneLot=false;
	protected $_numPli;
	protected $_commonDecisionEnveloppe;
	protected $_resultatAnalyse;
	protected ?string $_inscrit_id = "null";

	public function getNombreLotsAdmissibles()
	{
		return $this->_nombreLotsAdmissibles;
	}

	public function setNombreLotsAdmissibles($nombreLotsAdmissibles)
	{
		$this->_nombreLotsAdmissibles=$nombreLotsAdmissibles;
	}

	public function getNombreLotsNonAdmissibles()
	{
		return $this->_nombreLotsNonAdmissibles;
	}

	public function setNombreLotsNonAdmissibles($nombreLotsNonAdmissibles)
	{
		$this->_nombreLotsNonAdmissibles=$nombreLotsNonAdmissibles;
	}

	public function getNombreLotsATraiter()
	{
		return $this->_nombreLotsATraiter;
	}

	public function setNombreLotsATraiter($nombreLotsATraiter)
	{
		$this->_nombreLotsATraiter=$nombreLotsATraiter;
	}

	public function getNombreLots()
	{
		return (self::getNombreLotsAdmissibles())+(self::getNombreLotsNonAdmissibles())+(self::getNombreLotsATraiter());
	}

	public function setMoreThanOneLot($moreThanOneLot)
	{
		$this->_moreThanOneLot=$moreThanOneLot;
	}

	public function getMoreThanOneLot()
	{
		return $this->_moreThanOneLot;
	}

		public function getNumPli()
	{
		return $this->_numPli;
	}

	public function setNumPli($numPli)
	{
		$this->_numPli=$numPli;
	}

	public function getCommonDecisionEnveloppe()
	{
		return $this->_commonDecisionEnveloppe;
	}

	public function setCommonDecisionEnveloppe($commonDecisionEnveloppe)
	{
		$this->_commonDecisionEnveloppe=$commonDecisionEnveloppe;
	}


	/**
	 * Cette fonction retourne l'attribut $_inscrit_id qui est egale a vide (sans possibilite de faire un set)
	 * L'objet OffrePapier peut alors etre utilise comme l'objet Offre
	 */
	public function getInscritId()
	{
		return $this->_inscrit_id;
	}

	public function getTypeEnveloppe()
	{
		return Atexo_Config::getParameter('DEPOT_PAPIER');
	}

	public function setResultatAnalyse($value)
	{
		$this->_resultatAnalyse=$value;
	}

	public function getResultatAnalyse()
	{
		return $this->_resultatAnalyse;
	}

    public function getCommonConsultation(PropelPDO $connexionCom = null, $doQuery = true)
    {
        if(!$connexionCom){
        	$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
    	$c=new Criteria();
        $c->add(CommonConsultationPeer::ID,$this->getConsultationId());
        $c->add(CommonConsultationPeer::ORGANISME,$this->getOrganisme());
        $CommonConsultation = CommonConsultationPeer::doSelectOne($c,$connexionCom);
        return $CommonConsultation;
    }

	/*
	 * retourne libelle de statut de l'offre
	 */
	public function getLibelleStatutOffre()
	{
		$libelleStatutOffres = "";
		$statutOffres = $this->getStatutOffrePapier();
		if($statutOffres == Atexo_Config::getParameter('STATUT_ENV_FERME')){
			$libelleStatutOffres = Prado::localize('FERMEE');
		}elseif($statutOffres == Atexo_Config::getParameter('STATUT_ENV_REFUSEE')){
			$libelleStatutOffres = Prado::localize('TEXT_REFUSEE');
		}elseif($statutOffres == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')){
			$libelleStatutOffres = Prado::localize('OUVERTE');
		}
		return $libelleStatutOffres;
	}

	/**
	 * Permet de recuperer les enveloppes d'une offre
	 * @param $listeOrdonnee: si ce parametre est renseigné (valeur=true),
	 * alors le resultat correspond à une liste ordonnée des enveloppes par numéro lot
	 */
	public function getCommonEnveloppesPapiers($listeOrdonnee=false)
	{
		$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		$criteria = new Criteria();
		$criteria->add(CommonEnveloppePapierPeer::OFFRE_PAPIER_ID, $this->getId());
		$criteria->add(CommonEnveloppePapierPeer::ORGANISME, $this->getOrganisme());
		if($listeOrdonnee) {
			$criteria->addAscendingOrderByColumn(CommonEnveloppePapierPeer::SOUS_PLI);
		}
		return CommonEnveloppePapierPeer::doSelect($criteria, $connexionCom);
	}

	/**
	 * Permet de tester si l'offre papier est ouvert
	 */
	public function isOffreOuvertOuRejete(){
		$statutOffres = $this->getStatutOffrePapier();
		if($statutOffres == Atexo_Config::getParameter('STATUT_ENV_OUVERTE') || $statutOffres == Atexo_Config::getParameter('STATUT_ENV_REFUSEE'))
		{
		   	return true;
		}
		return false;
	}

	/**
 	 * Permet de retourner le numero du pays de l'offre
 	 * 
 	 * @return Intger le numero du pays de l'offre
 	 * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
	public function getNumPays()
 	{
 		return (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdsByDenomination1(strtoupper($this->getPays()));
 	}
	/**
	 * Permet de savoir si l'entreprise qui y a depose l'offre est locale ou etranger.
	 *
	 * @return Bollean true si l'entreprise est locale,si non false
	 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function isEntrepriseOffreLocale()
	{
		if(strtolower($this->getPays()) == strtolower(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE')) && ($this->getSiret() || !$this->getIdentifiantNational()) ){
			return true;
		}
		return false;
	}


} // CommonOffrePapier
