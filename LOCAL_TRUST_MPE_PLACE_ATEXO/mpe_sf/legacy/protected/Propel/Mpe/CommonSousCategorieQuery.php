<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonSousCategorieQuery;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

/**
 * Skeleton subclass for performing query and update operations on the 'SousCategorie' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.7.0-dev on:
 *
 * Mon Jul 15 17:53:48 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonSousCategorieQuery extends BaseCommonSousCategorieQuery
{
    public function getAllSousCategorie() {
        $cachedSousCategorie = Prado::getApplication()->Cache->get('cachedSousCategorie');

        if(!$cachedSousCategorie) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $cachedSousCategorie = $this->orderById(Criteria::ASC)->find($connexionCom);

            if((is_countable($cachedSousCategorie) ? count($cachedSousCategorie) : 0)==0) $cachedSousCategorie=array('');

            Prado::getApplication()->Cache->set('cachedSousCategorie', $cachedSousCategorie,Atexo_Config::getParameter('TTL_PRADO_CACHE'));
        }

        return $cachedSousCategorie;
    }


}
