<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonEnchereTranchesBaremeNETC;

/**
 * Skeleton subclass for representing a row from the 'EnchereTranchesBaremeNETC' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEnchereTranchesBaremeNETC extends BaseCommonEnchereTranchesBaremeNETC {
	private $_tmpId;
    protected $aCommonEncherePmi;

    public function getTmpid()
    {
        return $this->_tmpId;
    }
    public function setTmpid($id)
    {
        $this->_tmpId = $id;
    }
    public function setCommonEncherePmi(CommonEncherePmi $v = null)
	{
		if ($v === null) {
			$this->setIdenchere('0');
		} else {
			$this->setIdenchere($v->getId());
		}


		$this->aCommonEncherePmi = $v;
	}
} // CommonEnchereTranchesBaremeNETC
