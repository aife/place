<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Query\Criteria;
use Application\Library\Propel\Collection\PropelObjectCollection;




use Application\Propel\Mpe\Om\BaseCommonNewsletter;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'Newsletter' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonNewsletter extends BaseCommonNewsletter {


	/**
	 * La méthode getCommonNewsletterPieceJointes() se trouvant dans le fichier BaseCommonNewsletter.php retourne un 
	 * objet collection "PropelObjectCollection". C'est pour cette raison que nous surchargeons la méthode pour
	 * retourner un tableau d'objets
	 * @param Criteria $criteria: critéria
     * @param $con: objet collection
     * @return: retourne un tableau d'objet recupéré dans la collection
	 */
	public function getCommonNewsletterPieceJointes($criteria = null, $con = null)
    {
    	$collectionNewsletterPiecesJointes = parent::getCommonNewsletterPieceJointes($criteria, $con);
    	if(is_object($collectionNewsletterPiecesJointes) && $collectionNewsletterPiecesJointes->getData()) {
    		return $collectionNewsletterPiecesJointes->getData();
    	}
    }


    /**
     * returner le sigle
     *
     * @return string
     */

    public function getSigleRedacteur(){

        $sigle=(new Atexo_EntityPurchase())->getSigleByEntityId($this->getIdServiceRedacteur(), $this->getOrganisme());
        return $sigle;

    }

    /**
     * returner une chaine de caractère nom et prenom Redacteur selon un séparateur pour pouvoir l'utilisé en trie
     *
     * @return string
     */

    public function getRedacteur($separateur = " "){

        $redacteur=$this->getNomRedacteur() . $separateur . $this->getPrenomRedacteur();
        return $redacteur;

    }

    /**
     * returner une chaine de caractère statut message pour pouvoir l'utilisé en trie
     *
     * @return string
     */
    public function getStatutMessage(){

        $statutMessage= $this->getStatut();
        if ($statutMessage==Atexo_Config::getParameter('STATUS_BROUILLON')) {
            return Prado::localize('TEXT_BROUILLON');
        }
        elseif ($statutMessage==Atexo_Config::getParameter('STATUS_ENVOI_PLANIFIE')) {
            return Prado::localize('TEXT_ENVOI_PLANIFIE');
        }
        elseif ($statutMessage==Atexo_Config::getParameter('STATUS_ENVOYE')) {
            return Prado::localize('TEXT_ENVOYE');
        }

    }

    /**
     * returner une chaine de caractère EnvoyPar selon un séparateur pour pouvoir l'utilisé en trie
     *
     * @return string
     */

    public function getEnvoyePar($separateur = " "){

        $envoyePar=$this->getEnvoyeParNom() . $separateur . $this->getEnvoyeParPrenom();
        return $envoyePar;

    }

} // CommonNewsletter
