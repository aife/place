<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonSocietesExclues;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Organismes;

/**
 * Skeleton subclass for representing a row from the 'Societes_Exclues' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonSocietesExclues extends BaseCommonSocietesExclues {

    /*
     * Permet de verifier si l'agent connecté a l'habilitation modifier sociétés exclues
     * @param : $orgAcronyme acronyme organisme de l'utilisateur connecté
     * @param : $idService idService de l'agent connecté
     * @param : $habilitation l'habilitation modifier de l'agent connecté
     * @return : true si habilitation modifier sociétés exclues, false sinon
     */
    public function hasHabilitationModifierSupprimerSocietesExclues($orgAcronyme, $idService, $habilitation) {

        if(strcmp($orgAcronyme, $this->organisme_acronyme) == 0 && strcmp($idService, $this->id_service) == 0 ) {

            if($habilitation) {
                return 2;
            } else {
                return 1;
            }
        } else {
            return 0;
        }

    }

    /*
     * retourne la raison sociale traduite en langue stockée en session
     */
    public function getRaisonSocialeTraduit() 
	{
	    $langue = Atexo_CurrentUser::readFromSession("lang");
 	    $raisonSocialeTraduit = "raison_sociale_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$raisonSocialeTraduit){
            return $this->raison_sociale;
        } else {
            return $this->$raisonSocialeTraduit;
        }
	}

	/*
     * retourne le motif traduit en langue stockée en session
     */
    public function getMotifTraduit() 
	{
	    $langue = Atexo_CurrentUser::readFromSession("lang");
 	    $motifTraduit = "motif_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$motifTraduit){
            return $this->motif;
        } else {
            return $this->$motifTraduit;
        }
	}

/*
     * retourne le libellé de l'organisme traduit en langue stockée en session
     */
    public function getLibelleOrganismeTraduit() 
	{
	    $organismes = Atexo_Organismes::retrieveOrganismes(false, Atexo_CurrentUser::readFromSession("lang"));
 	    return $organismes[$this->organisme_acronyme];
	}

} // CommonSocietesExclues
