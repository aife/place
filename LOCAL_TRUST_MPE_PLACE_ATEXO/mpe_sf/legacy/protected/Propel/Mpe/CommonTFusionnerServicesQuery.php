<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonTFusionnerServicesQuery;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for performing query and update operations on the 't_fusionner_services' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonTFusionnerServicesQuery extends BaseCommonTFusionnerServicesQuery
{

    public function getFusionServiceByIdServiceSource($idServiceSource,$idServiceCible,$organisme,$merged = '0',$connexion = NULL)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        }
        return $this->filterByIdServiceSource($idServiceSource)->filterByIdServiceCible($idServiceCible)->filterByOrganisme($organisme)->filterByDonneesFusionnees($merged)->findOne($connexion);
    }

    public function getFusionServiceNotMerged($connexion = NULL)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        }
        return (array) $this->filterByDonneesFusionnees('0')->orderByDateCreation(Criteria::ASC)->find($connexion);
    }


}
