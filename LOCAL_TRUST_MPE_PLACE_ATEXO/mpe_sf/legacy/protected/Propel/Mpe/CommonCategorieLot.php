<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonCategorieLot;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CPV;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Traduction;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;

/**
 * Skeleton subclass for representing a row from the 'CategorieLot' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonCategorieLot extends BaseCommonCategorieLot {

	protected $_decisionLot;
	protected $_dateDecision;
	protected array $_attributaires=array();
	protected array $_colVisiteLieux=array();
	protected $_oldLot;
	protected ?\Application\Propel\Mpe\CommonTDonneeComplementaire $_donneesComplaimentaire = null;

	protected ?\Application\Service\Atexo\Atexo_CPV $_cpv = null;

	protected  string $_libelleCategorieLot='';
	protected $marcheReserve;

	protected $_donneComplementaireCons;

	public function setDecisionLot($decisionLot)
	{
		$this->_decisionLot=$decisionLot;
	}	

	public function getDecisionLot()
	{
		return $this->_decisionLot;
	}

	public function setDateDecision($decisionLot)
	{
		$this->_dateDecision=$decisionLot;
	}	

	public function getDateDecision()
	{
		return $this->_dateDecision;
	}	 

	public function getAttributaires()
	{
		return $this->_attributaires;
	}

	public function getInfoAttributaire($key)
	{
		return $this->_attributaires[$key];
	}

	public function setAttributaires(array $value)
	{
		$this->_attributaires=$value;
	}

	public function hasAttributaires()
	{
		return count($this->_attributaires)>0;
	}

	public function getColVisitesLieux()
	{
		return $this->_colVisiteLieux;
	}

	public function setColVisitesLieux(array $value)
	{
		$this->_colVisiteLieux=$value;
	}

	public function getOldLot()
	{
		return $this->_oldLot;
	}

	public function setOldLot($value)
	{
		$this->_oldLot = $value;
	}

	public function getDescription($langue=null) {
		return $this->getDescriptionTraduite($langue);
	}

	public function setLibelleCategorieLot($libelleCategorie)
    {
    	$this->_libelleCategorieLot=$libelleCategorie;
    }

	public function getLibelleCategorieLot()
    {
        if($this->_libelleCategorieLot) {
        	return $this->_libelleCategorieLot;
        }
        $categorie = Atexo_Consultation_Category::retrieveCategorie($this->getCategorie(),true);
        if($categorie) {
            return $categorie->getLibelle();
        } else {
            return "";
        }
    }

	public function setCpv(Atexo_CPV $value)
    {
    	$this->_cpv = $value;
    }
	public function getCpv()
    {
    	return $this->_cpv;
    }
	/**
     * Renvoie la traduction de l'intitulé du lot dans la langue spécifiée
     * @param string $langue: langue de naviguation
    */
	public function getDescriptionDetail($langue=null) {

		return $this->getDescriptionDetailTraduite($langue);
	}

	public function getAddEchantillionTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $add_echantillion = "add_echantillion_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$add_echantillion){
            return $this->add_echantillion;
        } else {
            return $this->$add_echantillion;
        }
	}

	public function getAddReunionTraduit()
	{
	   $langue = Atexo_CurrentUser::readFromSession("lang");
	   $add_reunion = "add_reunion_".$langue;
 	    if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0
 	        || !$this->$add_reunion){
            return $this->add_reunion;
        } else {
            return $this->$add_reunion;
        }
	}

	/**
     * Renvoie la traduction de l'intitulé du lot dans la langue spécifiée
     * @param string $langue: langue de naviguation
    */
	public function getDescriptionTraduite($langue = null) 
	{
	    if (!$langue) {
        	$langue = Atexo_CurrentUser::readFromSession("lang");
        }
        if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0) {
        	$traduction = $this->description;
        }else {
            $traduction = Atexo_Traduction::getTraduction($this->getIdTrDescription(), $langue);
            if (!$traduction) {
            	$traduction = $this->description;
            }
    	}
    	return html_entity_decode($traduction);
	}

	/**
	 * Enregistre la traduction de de l'intitulé du lot
	 * @param string $langue: langue de naviguation
	 * @param string $libelle: le libellé de la traduction
	 */
    public function setDescriptionTraduite($langue, $libelle)
    {
        if(!$langue){
        	$langue = Atexo_CurrentUser::readFromSession("lang"); 
        }
    	if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0) {
   			$this->setDescription($libelle);
   	    }
        $idTraduction = (new Atexo_Traduction())->setTraduction($this->getIdTrDescription(), $langue, $libelle); 
        $this->setIdTrDescription($idTraduction);
    }

	/**
     * Renvoie la traduction de l'objet du lot dans la langue spécifiée
     * @param string $langue: langue de naviguation
    */
	public function getDescriptionDetailTraduite($langue=null) {

		if (!$langue) {
        	$langue = Atexo_CurrentUser::readFromSession("lang");
        }
        if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0) {
        	$traduction = $this->description_detail;
        }else {
            $traduction = Atexo_Traduction::getTraduction($this->getIdTrDescriptionDetail(), $langue);
            if (!$traduction) {
            	$traduction = $this->description_detail;
            }
    	}
    	return $traduction;
	}

	/**
	 * Enregistre la traduction de de l'objet du lot
	 * @param string $langue: langue de naviguation
	 * @param string $libelle: le libellé de la traduction
	 */
    public function setDescriptionDetailTraduite($langue, $libelle)
    {
        if(!$langue){
        	$langue = Atexo_CurrentUser::readFromSession("lang"); 
        }
    	if(strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) == 0) {
   			$this->setDescriptionDetail($libelle);
   	    }
        $idTraduction = (new Atexo_Traduction())->setTraduction($this->getIdTrDescriptionDetail(), $langue, $libelle); 
        $this->setIdTrDescriptionDetail($idTraduction);
    }

	public function getCommonVisiteLieuxsLot(){
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$c = new Criteria();
		$c->add(CommonVisiteLieuxPeer::ORGANISME,$this->getOrganisme());
		$c->add(CommonVisiteLieuxPeer::CONSULTATION_ID,$this->getConsultationId());
		$c->add(CommonVisiteLieuxPeer::LOT,$this->getLot());
		$visiteLieux = CommonVisiteLieuxPeer::doSelect($c, $connexion);
		return $visiteLieux;
	}

	/*
	 * retourne le donné complementaire du lot
	 */
	public function getDonneComplementaire()
	{
		if(!($this->_donneesComplaimentaire instanceof  CommonTDonneeComplementaire) && ($this->getIdDonneeComplementaire()!== null)){
			$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
			$c = new Criteria();
			$c->add(CommonTDonneeComplementairePeer::ID_DONNEE_COMPLEMENTAIRE,$this->getIdDonneeComplementaire());
			$this->_donneesComplaimentaire = CommonTDonneeComplementairePeer::doSelectOne($c, $connexion);

		}
		if($this->_donneesComplaimentaire instanceof  CommonTDonneeComplementaire){
			return $this->_donneesComplaimentaire;
		}
		return false;
	}

	/**
     * Permet de retourner si le lot à une clause donnée
     * @param $clauseName
     */
	public function hasThisClause($clauseName) {
		$getClause = "get".ucfirst($clauseName);
		if($this-> $getClause()){
			return true;
		}
		return false;
    }

    public function hasTranches(){
	    if($donneeComplementraie = $this->getDonneComplementaire()){
	        $tranches = $donneeComplementraie->getAllTranches();
	    	if($tranches){
	    		return true;
	    	}
	    }
	    return false;
    }

    /**
     * Permet de setter L'objet Atexo_CPV  à partir du lot
     *
     * @param null $max
     * @return void l'objet contennat les informations
     * @author LEZ <loubna.ezziani@atexom.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function setObejtCpv($max = null)
    {
    		$cpv = new Atexo_CPV();
    		$cpv->setCodePrincipal($this->getCodeCpv1());
    		if($this->getCodeCpv2()) {
				$arrayCodesCPV=explode('#',$this->getCodeCpv2());
				if(is_array($arrayCodesCPV) && $arrayCodesCPV) {
					$i = 1;
					foreach($arrayCodesCPV as $codeCpv) {
						if($codeCpv) {
							$setCodeSecondaire = "setCodeSecondaire".$i;
							$cpv->$setCodeSecondaire($codeCpv);
							$i++;
                            if($max != null && $max < $i) {
                                break;
                            }
						}
					}
				}
			}
			$this->setCpv($cpv);
    }

	/**
	 * @return mixed
	 */
	public function getMarcheReserve()
	{
		return ($this->getClauseSocialeAteliersProteges() || $this->getClauseSocialeSiae() || $this->getClauseSocialeEss());
	}

	/**
	 * @param mixed $marcheReserve
	 */
	public function setMarcheReserve($marcheReserve)
	{
		$this->marcheReserve = $marcheReserve;
	}

	/**
	 * @return mixed
	 */
	public function getDonneComplementaireCons()
	{
		if(! ($this->_donneComplementaireCons instanceof  CommonTDonneeComplementaire)){
			$consultation = (new Atexo_Consultation())->getConsultationByReference($this->getConsultationId(), $this->getOrganisme());
			if($consultation instanceof CommonConsultation){
				$this->_donneComplementaireCons = $consultation->getDonneComplementaire();
			}
		}
		return $this->_donneComplementaireCons;

	}

	/**
	 * @param mixed $donneComplementaireCons
	 */
	public function setDonneComplementaireCons($donneComplementaireCons)
	{
		$this->_donneComplementaireCons = $donneComplementaireCons;
	}




} // CommonCategorieLot