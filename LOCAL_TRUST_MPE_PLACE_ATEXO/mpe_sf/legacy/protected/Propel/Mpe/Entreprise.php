<?php
namespace Application\Propel\Mpe;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonContactEntreprisePeer;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Om\BaseEntreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Ref;

/**
 * Skeleton subclass for representing a row from the 'Entreprise' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.7.0-dev on:
 *
 * Tue Dec  2 10:43:53 2014
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class Entreprise extends BaseEntreprise
{
    protected $_etablissementSiege = null;
    protected $_siretSiegeSocial = null;
    protected $mandataires = null;

    /**
 * modifie la valeur de [_etablissementSiege].
 *
 * @param CommonTEtablissement $value la valeur a mettre
 * @return void
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 * @version 1.0
 * @since 4.9.0
 * @copyright Atexo 2015
 */
    public function setEtablissementSiege($value)
    {
        $this->_etablissementSiege = $value;
    }
    /**
     * recupere la valeur du [_etablissementSiege].
     *
     * @return CommonTEtablissement la valeur courante [_etablissementSiege].
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function getEtablissementSiege()
    {
        if ($this->_etablissementSiege) {
            return $this->_etablissementSiege;
        } else {
            $etablissementSiege = $this->getMonEtablissementSiege();
            $this->setEtablissementSiege($etablissementSiege);
            return $etablissementSiege;
        }
    }

    /**
     * modifie la valeur de [_siretSiegeSocial].
     *
     * @param String $value la valeur a mettre
     * @return void
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function setSiretSiegeSocial($value)
    {
        $this->_siretSiegeSocial = $value;
    }
    /**
     * recupere la valeur du [_siretSiegeSocial].
     *
     * @return String la valeur courante [_siretSiegeSocial].
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function getSiretSiegeSocial()
    {
        if ($this->_siretSiegeSocial) {
            return $this->_siretSiegeSocial;
        } else {
            $etablissementSiege = $this->getEtablissementSiege();
            if ($etablissementSiege) {
                $this->setSiretSiegeSocial($etablissementSiege->getCodeEtablissement());
                return $etablissementSiege->getCodeEtablissement();
            }
        }
    }
    /*
     * Retourne les libellés utilisant le Lt-Référentiel
     * @Params: codes LT-REF
     * @return: libellés correspondant au code envoyé en paramatre
     */
    public function getLibelleLtRef($codesLtRef, $langue, $fichierConfig)
    {
        $atexoRef=new Atexo_Ref();
        $atexoRef->setCheminFichierConfigXML($fichierConfig);
        if ($langue == "su") {
            $langue="sv";
        }
        $atexoRef->setLocale($langue);

        if ($atexoRef instanceof Atexo_Ref) {
            $arrayConfigXml=$atexoRef->getArrayConfigXml();
            if ($arrayConfigXml) {
                $dataRefFile="./".$arrayConfigXml['cheminData'].$arrayConfigXml['fichierDonnee'].$langue.".csv";
            }

            $listeLibellesLtRef=array();
            if ($codesLtRef) {
                $arrayCodesLtRef=explode('#', $codesLtRef);
                if (is_array($arrayCodesLtRef) && $arrayCodesLtRef) {
                    $i=0;
                    foreach ($arrayCodesLtRef as $unCodeLtRef) {
                        if ($unCodeLtRef != $arrayCodesLtRef[0]) {
                            if ($unCodeLtRef) {
                                if (is_file($dataRefFile)) {
                                    $listeLibellesLtRef[$i] = $atexoRef->getLibelleRef($unCodeLtRef, $dataRefFile);
                                }
                                $i++;
                            }
                        }
                    }
                }
            }
            if ($listeLibellesLtRef) {
                $libelleRetour = "";
                for ($i=0; $i<count($listeLibellesLtRef); $i++) {
                    $libelleRetour .= $listeLibellesLtRef[$i].'<br />';
                }
                return $libelleRetour;
            }
        }
    }
    public function getContactByIdEntreprise()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($this->id) {
            $c->add(CommonContactEntreprisePeer::ID_ENTREPRISE, $this->id);
            $contact = CommonContactEntreprisePeer::doSelectOne($c, $connexionCom);
            if ($contact) {
                return $contact;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * recupere l'etablissement siege de l'entreprise.
     *
     * @return CommonTEtablissement l'etablissement siege.
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function getMonEtablissementSiege()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $etablissement = CommonTEtablissementQuery::create()->filterByEstSiege('1')->filterByIdEntreprise($this->getId())->findOne($connexion);
        if ($etablissement instanceof CommonTEtablissement) {
            return $etablissement;
        }
        return false;
    }

    /**
     * recupere l'adresse de l'etablissement siege de l'entreprise.
     *
     * @return String l'adresse de l'etablissemetn siege.
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function getAdresse()
    {
        $etablissementSiege = $this->getEtablissementSiege();
        if ($etablissementSiege) {
            return $etablissementSiege->getAdresse();
        }
        return '';
    }

    /**
     * recupere l'adresse 2 de l'etablissement siege de l'entreprise.
     *
     * @return String l'adresse 2 de l'etablissemetn siege.
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function getAdresse2()
    {
        $etablissementSiege = $this->getEtablissementSiege();
        if ($etablissementSiege) {
            return $etablissementSiege->getAdresse2();
        }
        return '';
    }

    /**
     * recupere la ville de l'etablissement siege de l'entreprise.
     *
     * @return String la ville de l'etablissemetn siege.
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function getVilleadresse()
    {
        $etablissementSiege = $this->getEtablissementSiege();
        if ($etablissementSiege) {
            return $etablissementSiege->getVille();
        }
        return '';
    }

    /**
     * recupere le code postal de l'etablissement siege de l'entreprise.
     *
     * @return String le code postal de l'etablissemetn siege.
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function getCodePostal()
    {
        $etablissementSiege = $this->getEtablissementSiege();
        if ($etablissementSiege) {
            return $etablissementSiege->getCodePostal();
        }
        return '';
    }
    
    public function setMandataires($value)
    {
        $this->mandataires = $value;
    }

    public function getMandataires()
    {
        return $this->mandataires;
    }
    
    /**
      * Permet de savoir si l'entreprise est locale ou etranger.
      *
      * @return Bollean true si l'entreprise est locale,si non false
      * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
      * @version 1.0
      * @since 4.9.0
      * @copyright Atexo 2015
      */
    public function isEntrepriseLocale()
    {
        if ($this->getSiren() && $this->getPaysenregistrement() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')) {
            return true;
        }
        return false;
    }

    /**
     * recupere la valeur du [nicsiege].
     *
     * @return String la valeur courante [nicsiege].
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function getNicsiege()
    {
        return $this->nicsiege ?: $this->getSiretSiegeSocial();
    }
}
