<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use AtexoCrypto\Dto\Enveloppe;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePeer;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonAgent;




use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonEnveloppe;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'Enveloppe' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEnveloppe extends BaseCommonEnveloppe {
	private $_libelleStatutEnveloppe;
	private $_admissibiliteLot;
	private array $_arrayAdmissibiliteLot=array();
	private $_numPli;
	private $_commonOffres;
	private $_statistiqueAvisCAO;
	private $_contentChampsOptionnels = null;
	private $_contentDonneesOuverture= null;
	private $_contentHorodatageDonneesOuverture= null;

  	public function getChampsOptionnels()
    {
		if(!$this->_contentChampsOptionnels) {
			$this->_contentChampsOptionnels = stream_get_contents( parent::getChampsOptionnels());
		}
		return $this->_contentChampsOptionnels;
    }
 	public function getDonneesOuverture()
    {
        if(!$this->_contentDonneesOuverture) {
			$this->_contentDonneesOuverture = stream_get_contents( parent::getDonneesOuverture());
		}
		return $this->_contentDonneesOuverture;
    }
	public function getHorodatageDonneesOuverture()
    {
        if(!$this->_contentHorodatageDonneesOuverture) {
			$this->_contentHorodatageDonneesOuverture = stream_get_contents( parent::getHorodatageDonneesOuverture());
		}
		return $this->_contentHorodatageDonneesOuverture;
    }

	public function getLibelleStatutEnveloppe()
	{
		return $this->_libelleStatutEnveloppe;
	}

	public function setLibelleStatutEnveloppe($libelleStatutEnveloppe)
	{
		$this->_libelleStatutEnveloppe=$libelleStatutEnveloppe;
	}

	public function getAdmissibiliteLot()
	{
		return $this->_admissibiliteLot;
	}

	public function setAdmissibiliteLot($admissibiliteLot)
	{
		 $this->_admissibiliteLot=$admissibiliteLot;
	}

	public function getCollAdmissibiliteLot() 
	{
		return $this->_arrayAdmissibiliteLot;
	}

	public function addAdmissibiliteLot($admissibiliteLot) 
	{
		$this->_arrayAdmissibiliteLot[]=$admissibiliteLot;
	}

	public function getNumPli() 
	{
		return $this->_numPli;
	}

	public function setNumPli($numPli) 
	{
		$this->_numPli=$numPli;
	}

	public function setCommonOffres($commonOffres) 
	{
		$this->_commonOffres=$commonOffres;
	}

	public function getCommonOffres() 
	{
		return $this->_commonOffres;
	}

	public function setStatistiqueAvisCAO($statistiqueAvisCAO) 
	{
		$this->_statistiqueAvisCAO=$statistiqueAvisCAO;
	}

	public function getStatistiqueAvisCAO() 
	{
		return $this->_statistiqueAvisCAO;
	}

	public function getOffre($connexionCom=null)
    {
        if(!$connexionCom){
        	$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
    	$c=new Criteria();
        $c->add(CommonOffresPeer::ID,$this->getOffreId());
        $c->add(CommonOffresPeer::ORGANISME,$this->getOrganisme());
        $commonOffre = CommonOffresPeer::doSelectOne($c,$connexionCom);
        return $commonOffre;
    }

	public function getVerificationSignatureOA($organisme, $consultationSigne = null)
	{
		$result = "";
		if ($this->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
			|| $this->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
			|| $this->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
			|| $this->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
		) {
			if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') && !Atexo_Module::isEnabled('ModeApplet')) {
				$result = $this->getVerificationSignature();
			}

			if (empty($result)) {
				$result = $this->getResultatValiditeSignature($organisme, $consultationSigne);
			}
		}
		return $result;

	}

	public function getResultatValiditeSignature($organisme, $consultationSigne = null)
 	{
		$connexion= Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

		if($consultationSigne == null) {
			$offre=$this->getOffre($connexion);
			if($offre instanceof CommonOffres) {
				$consultation = $offre->getCommonConsultation($connexion);
				if ($consultation instanceof CommonConsultation) {
					$consultationSigne = ($consultation->getSignatureOffre() == "1");
				}
			}
		}

		$cryptage=($this->getCryptage()=="1");
		$c=new Criteria();
		$c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $this->getIdEnveloppeElectro());
		$c->add(CommonFichierEnveloppePeer::ORGANISME, $this->getOrganisme());
		$c->add(CommonFichierEnveloppePeer::TYPE_FICHIER, "SIG", Criteria::NOT_EQUAL);
		$fichiers=CommonFichierEnveloppePeer::doSelect($c, $connexion);
		$resUnkown = false;
		$resNok = false;
		$resSans = false;
		$resultat = Atexo_Config::getParameter('SANS');
		if(is_array($fichiers)) {
			foreach($fichiers as $fichier) {
				$statut=array();
				if($fichier->getVerificationCertificat()){
					$arrayValidite=explode('-', $fichier->getVerificationCertificat());
					$referentielCertif = $fichier->getStatutReferentielCertificat();
					if(!Atexo_Module::isEnabled('SurchargeReferentiels') || ($referentielCertif===null)){
						if($arrayValidite[0]=='2') {
							$statut[0]=Atexo_Config::getParameter('NOK');
							$resNok = true;
						} elseif($arrayValidite[0]=='0') {
							$statut[0]=Atexo_Config::getParameter('OK');
						} else {
							$statut[0] = Atexo_Config::getParameter('UNKNOWN');
							$resUnkown = true;
						}
					}else{
						if($referentielCertif == 2){
							$statut[0]=Atexo_Config::getParameter('NOK');
							$resNok = true;
						}else if($referentielCertif == 0){
							$statut[0]=Atexo_Config::getParameter('OK');
						}else{
							$statut[0] = Atexo_Config::getParameter('UNKNOWN');
							$resUnkown = true;
						}
					}
					if($arrayValidite[1]=='1') {
						$statut[1]=Atexo_Config::getParameter('UNKNOWN');
						$resUnkown = true;
					} else if($arrayValidite[1]=='2') {
						$statut[1]=Atexo_Config::getParameter('NOK');
						$resNok = true;
					} elseif($arrayValidite[1]=='0') {
						$statut[1]=Atexo_Config::getParameter('OK');
					} else {
						$statut[1] = Atexo_Config::getParameter('UNKNOWN');
						$resUnkown = true;
					}
					if($arrayValidite[2]=='1') {
						$statut[2]=Atexo_Config::getParameter('UNKNOWN');
						$resUnkown = true;
					} else if($arrayValidite[2]=='2') {
						$statut[2]=Atexo_Config::getParameter('NOK');
						$resNok = true;
					} elseif($arrayValidite[2]=='0') {
						$statut[2]=Atexo_Config::getParameter('OK');
					}else {
						$statut[2] = Atexo_Config::getParameter('UNKNOWN');
						$resUnkown = true;
					}
					$validiteSignature=$fichier->getSignatureValidite($consultationSigne, $cryptage);
					$statut[3]=$validiteSignature;
					if($validiteSignature == Atexo_Config::getParameter('NOK')){
						$resNok = true;
					}elseif ($validiteSignature == Atexo_Config::getParameter('UNKNOWN')){
						$resUnkown = true;
					}
				}else{
					$resSans = true;
				}
			}
			if($resSans){
				$resultat = Atexo_Config::getParameter('SANS');
			}else if($resNok) {
				$resultat = Atexo_Config::getParameter('NOK');
			} else if ($resUnkown) {
				$resultat = Atexo_Config::getParameter('UNKNOWN');
			} else {
				$resultat = Atexo_Config::getParameter('OK');
			}
		}
		if(Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') && !Atexo_Module::isEnabled('ModeApplet')) {
			$connexion= Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
			$enveloppe = CommonEnveloppePeer::retrieveByPK($this->getIdEnveloppeElectro(),$this->getOrganisme(), $connexion);
			$enveloppe->setVerificationSignature($resultat);
			$enveloppe->save($connexion);
		}
		return $resultat;
	}
	public function retrieveListeIdsBlobsBlocChiffres($organisme)
 	{
 		$listeIdsBlobs=array();
 		$blocs=$this->retrieveBlocsChiffres($organisme); 
 		if($blocs) {
 			foreach($blocs as $bloc) {
 				$listeIdsBlobs[]=$bloc->getIdBlobChiffre();
 			}
 		}
 		return implode(' ',$listeIdsBlobs);
 	}

 	public function getNomAgentTelechargement($prefix="")
 	{
 		$agent=(new Atexo_Agent())->retrieveAgent($this->getAgentTelechargement());
 		return ($agent instanceof CommonAgent)?  ($prefix? $prefix." " : "").$agent->getPrenom()." ".$agent->getNom() : ""; 
 	}

	/**
	 * @return Blocfichierenveloppe[]
	 */
	public function retrieveBlocsChiffres($organisme) 
 	{
 		$connexion= Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

 		$bloc=array();
 		$c=new Criteria();
 		$c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $this->getIdEnveloppeElectro());
 		$c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
 		$fichiers=CommonFichierEnveloppePeer::doSelect($c, $connexion);

 		if($fichiers) {
 			foreach($fichiers as $fichier) {
 				$bloc=array_merge($fichier->getBlocfichierenveloppes(null, $connexion), $bloc);
 			}
 		}
 		return $bloc;


 	}

	public function getPathPlisChiffres(){
		$connexion= Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
		if($this->getRepertoireTelechargement()){
			return $this->getRepertoireTelechargement();
		}else{
			$offre=$this->getOffre($connexion);
	 		if($offre instanceof CommonOffres) {
	 			$consultation=$offre->getCommonConsultation($connexion);
	 			if($consultation instanceof CommonConsultation) {
	 				if($consultation->getPathTelechargementPlis()){
	 					return $consultation->getPathTelechargementPlis();
	 				}
	 			}	
	 		}
 		}
 	}

 	public function getCommonFichierEnveloppes($connexionCom = null,$chiffrement = null,$statutuHash = null){
 		if(!$connexionCom){
 			$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
 		}
		$criteria = new Criteria();
		$criteria->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $this->getIdEnveloppeElectro());
		$criteria->add(CommonFichierEnveloppePeer::ORGANISME, $this->getOrganisme());
		if($statutuHash!== null){
			$criteria->add(CommonFichierEnveloppePeer::RESULTAT_VERIFICATION_HASH, $statutuHash);
		}
		$commonFichierEnv = CommonFichierEnveloppePeer::doSelect($criteria,$connexionCom);
		if($chiffrement !== null){
			if(is_array($commonFichierEnv)){
				foreach($commonFichierEnv as $fichierEnv){
					$fichierEnv->setChiffrement($chiffrement);
				}
				return $commonFichierEnv;
			}
		}else{
			return $commonFichierEnv;

		}
 	}

 	public function getLibelleTypeEnveloppe($typeEnv)
 	{
 		return match ($typeEnv) {
				Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') => Prado::localize('CANDIDATURE'),
				Atexo_Config::getParameter('TYPE_ENV_OFFRE') => Prado::localize('OFFRE'),
				Atexo_Config::getParameter('TYPE_ENV_ANONYMAT') => Prado::localize('ANONYMAT'),
				Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE') => Prado::localize('OFFRE_TECHNIQUE'),
				default => "",
			};
 	}
 	/*
 	 * permet d'avoir le nom du rapport de signature
 	 */
 	public function getNomRapportSignature()
 	{
 		if($this->getSousPli() != 0){
    	 	$nomFichier = Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE')."_".
    	 	$this->getLibelleTypeEnveloppe($this->getTypeEnv())."_lot".$this->getSousPli(). '.pdf';
    	}else{
    		$nomFichier = Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE')."_".
    		$this->getLibelleTypeEnveloppe($this->getTypeEnv()).'.pdf';
    	}
    	return $nomFichier;
 	}

	/**
	 * Permet de retourner les statuts des hash des fichiers d'un enveloppe
	 *
	 * @return boolean true si tt les fichiers sont OK,false si au moins un fichier est KO
	 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 2017-place
	 * @copyright Atexo 2018
	 */
	public function isHashFichiersOk()
	{
		$fichiersEnveloppe = $this->getCommonFichierEnveloppes();
		$verificationHashFileOk = '1';
		if(is_array($fichiersEnveloppe)){
			foreach($fichiersEnveloppe as $fichier) {
				if($fichier instanceof CommonFichierEnveloppe && $fichier->getResultatVerificationHash() === '0'){
					$verificationHashFileOk = '0';
					break;
				}
			}
		}
		return $verificationHashFileOk;
	}

} // CommonEnveloppe
