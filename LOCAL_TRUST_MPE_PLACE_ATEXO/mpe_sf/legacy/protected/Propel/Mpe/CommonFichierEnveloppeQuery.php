<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use AtexoCrypto\Dto\Signature;
use AtexoCrypto\Dto\Fichier;

use Application\Propel\Mpe\CommonFichierEnveloppe;



use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonFichierEnveloppeQuery;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for performing query and update operations on the 'fichierEnveloppe' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.7.0-dev on:
 *
 * Mon Jul 15 17:53:54 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.mpe
 */
class CommonFichierEnveloppeQuery extends BaseCommonFichierEnveloppeQuery
{
    /**
     * permet de recuperer un fichier d'enveloppe by id signature
     *
     * @param int $idFichierSignature id Fichier Signature
     * @return mixed CommonFichierEnveloppe ou false
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public function getFichierEnveloppeByIdFichierSignature($idFichierSignature,$connexion = null)
    {
        if(!$connexion){
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        return $this->filterByIdFichierSignature($idFichierSignature)->findOne($connexion);
    }


}
