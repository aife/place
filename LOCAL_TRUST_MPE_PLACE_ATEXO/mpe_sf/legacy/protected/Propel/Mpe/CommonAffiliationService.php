<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonAffiliationServicePeer;




use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonAffiliationService;
use Application\Service\Atexo\Atexo_Config;

/**
 * Skeleton subclass for representing a row from the 'AffiliationService' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAffiliationService extends BaseCommonAffiliationService {


	public function doSelectJoinCommonServiceRelatedByIdService(Criteria $c, $connexion){
		if(!$c){
			$c = new Criteria();
		}

		$c->addJoin(array(CommonAffiliationServicePeer::SERVICE_ID, CommonAffiliationServicePeer::ORGANISME),array(CommonServicePeer::ID, CommonServicePeer::ORGANISME));

		$c->setdistinct();
		return CommonAffiliationServicePeer::doSelect($c, $connexion);
	}

    public function getCommonServiceRelatedByIdService(PropelPDO $con = null, $doQuery = true)
    {
		$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
		$c = new Criteria();
		$c->add(CommonAffiliationServicePeer::SERVICE_PARENT_ID, $this->getServiceParentId());
		$c->add(CommonAffiliationServicePeer::ORGANISME, $this->getOrganisme());
		$c->add(CommonServicePeer::ORGANISME, $this->getOrganisme());
		$c->add(CommonAffiliationServicePeer::SERVICE_ID, $this->getServiceId());
		$c->addJoin(CommonAffiliationServicePeer::SERVICE_ID, CommonServicePeer::ID);
		$c->addAlias('Service2', CommonServicePeer::TABLE_NAME);
		$c->addJoin(CommonAffiliationServicePeer::ORGANISME, CommonServicePeer::alias('Service2', CommonServicePeer::ORGANISME));
		return CommonServicePeer::doSelectOne($c, $connexion);
	}

} // CommonAffiliationService
