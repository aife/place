<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use \PDO;
use Application\Propel\Mpe\CommonEncherePmiPeer;
use Application\Propel\Mpe\CommonEnchereOffrePeer;

use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\Om\BaseCommonEnchereEntreprisePmi;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;


/**
 * Skeleton subclass for representing a row from the 'EnchereEntreprisePmi' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEnchereEntreprisePmi extends BaseCommonEnchereEntreprisePmi
{

	public $_isSelected;
    public $_isfromconsultation;

    public function getIsselected()
    {
        return ($this->_isSelected && $this->isEmailValide());
    }
    public function setIsselected($bool)
    {
        $this->_isSelected = $bool;
    }

    public function getIsfromconsultation()
    {
        return $this->_isfromconsultation;
    }
    public function setIsfromconsultation($bool)
    {
        $this->_isfromconsultation = $bool;
    }

    public function generateMdp()
    {
        $pass = null;
								$chaine = "abBDEFcdefghijkmnPQRSTUVWXYpqrst23456789"; 
        mt_srand((double)microtime()*1_000_000);  
        $nom = $this->getNom();
        $nom = Atexo_Util::OterAccents($nom);
        $nom = str_replace(' ','',$nom);
        $nom = substr($nom, 0, 5);

        for($i=0; $i<10; $i++) 
        { //mot de passe de 10 caractères 
            $pass .= $chaine[random_int(0, mt_getrandmax())%strlen($chaine)];  
        }   
        return $nom.$pass;
    }

    public function getNoteLastOffre()
    {
        $idEnchere = $this->getEncherepmi()->getId();
        $idEnchereEntreprise = $this->getId();

        $requete1 = "SELECT MAX(date) FROM EnchereOffre
        			WHERE idEnchere = '" . (new Atexo_Db())->quote($idEnchere) ."'
        			AND idEnchereEntreprise = '". (new Atexo_Db())->quote($idEnchereEntreprise) . "' AND ORGANISME='".$this->getOrganisme()."'";

        //$stmt = $con->createStatement();
	    //$rs = $stmt->executeQuery($requete1, ResultSet::FETCHMODE_NUM);
	    $stmt = Atexo_Db::getLinkCommon(true)->prepare($requete1); 
	    $stmt->execute();
	    $rs=$stmt->fetchAll(PDO::FETCH_NUM);

	    $dateMax = "0000-00-00 00:00";
	   	if ($rs) {
		    foreach($rs as $row) {
		        $dateMax = $row[0];
		        break;
		    }
	    } else {
	        return null;
	    }

	    $requete2 = "SELECT valeurNGC FROM EnchereOffre
        			WHERE idEnchere = '". (new Atexo_Db())->quote($idEnchere) . "'
        			AND idEnchereEntreprise = '". (new Atexo_Db())->quote($idEnchereEntreprise) . "'
        			AND date = '".(new Atexo_Db())->quote($dateMax). "' AND ORGANISME='".$this->getOrganisme()."'";

	    //$rs2 = $stmt->executeQuery($requete2, ResultSet::FETCHMODE_NUM);
	    $stmt = Atexo_Db::getLinkCommon(true)->prepare($requete2); 
	    $stmt->execute();
	    $rs2=$stmt->fetchAll(PDO::FETCH_NUM);

	    if ($rs2) {
		    foreach($rs2 as $row) {
		        return $row[0];
		    }
	    } else {
	        return null;
	    }  
    }

    public function getInfoToolTip()
    {
        $str = "<div>";
        $str .= Atexo_Util::utf8ToIso($this->getNom());
        $str .=  "<br />";
        $str .=  Atexo_Util::utf8ToIso($this->getEmail());
        $str .=  "<br />";
        $str .= "(" . Atexo_Util::utf8ToIso($this->getNomAgentConnecte()) . ")";
        $str .= "</div>";
        return $str;
    }

    public function isConnected($con = null)
    {
        $dateLatestPing = null;
								if ($con == null) {
            $con = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }

        $idEnchereEntreprise = $this->getId();
        $requete = "SELECT datePing FROM EnchereEntreprisePmi
        			WHERE id = '". (new Atexo_Db())->quote($idEnchereEntreprise) . "' AND organisme='".$this->getOrganisme()."'";

        //$stmt = $con->createStatement();
	    //$rs = $stmt->executeQuery($requete, ResultSet::FETCHMODE_NUM);

	    $stmt = Atexo_Db::getLinkCommon(true)->prepare($requete); 
	    $stmt->execute();
	    $rs=$stmt->fetchAll(PDO::FETCH_NUM);

	    if ($rs) {
		    foreach($rs as $row) {
		        $dateLatestPing = $row[0];
		    }
	    }

	    $tsNow = time();
	    $tsDateLatestPing = strtotime($dateLatestPing);
	    $delai = $tsNow - $tsDateLatestPing;
	    $delaiMaxInactivite = 2 * Atexo_Config::getParameter('PERIODE_MISE_A_JOUR_ENCHERE');

	    if ($delai > $delaiMaxInactivite) {
	        return false;
	    } else {
	        return true;
	    }

    }

    public function isEmailValide()
    {
    	return Atexo_Util::checkMail($this->getEmail());
    }

	public function getCommonEncherePmi(PropelPDO $con = null, $doQuery = true)
	{
		if ($this->aCommonEncherePmi === null && ($this->idenchere !== null) && $this->organisme !== null) {
			$this->aCommonEncherePmi = CommonEncherePmiPeer::retrieveByPK($this->idenchere, $con);
		}
		return $this->aCommonEncherePmi;
	}

	public function getCommonEnchereOffres($criteria = null, $con = null)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
		}
		elseif ($criteria instanceof Criteria)
		{
			$criteria = clone $criteria;
		}
		$criteria->add(CommonEnchereOffrePeer::IDENCHEREENTREPRISE, $this->getId());
		$criteria->add(CommonEnchereOffrePeer::ORGANISME, $this->getOrganisme());
	    $this->collCommonEnchereOffres = CommonEnchereOffrePeer::doSelect($criteria, $con);

		return $this->collCommonEnchereOffres;
	}
} // CommonEnchereEntreprisePmi
