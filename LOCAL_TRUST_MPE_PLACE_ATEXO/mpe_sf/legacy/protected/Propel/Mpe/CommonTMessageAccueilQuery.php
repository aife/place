<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonTMessageAccueilQuery;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

/**
 * Skeleton subclass for performing query and update operations on the 't_message_accueil' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.s
 *
 * @package    propel.generator.mpe
 */
class CommonTMessageAccueilQuery extends BaseCommonTMessageAccueilQuery
{
    public function findCachedMessageAccueil($destinataire, $authentifier = NULL) {

        $cachedMessageAccueilKey = self::getCachedMessageAccueilKey($destinataire, $authentifier);
        $cachedMessageAccueil = Prado::getApplication()->Cache->get($cachedMessageAccueilKey);

        if(!$cachedMessageAccueil) {
            $con = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

            $configPf =  Atexo_Config::getParameter('CONFIG_PLATEFORME_POUR_MESSAGE_ACCUEIL');
            $this->filterByConfig($configPf);

            if($authentifier !== NULL){
                $this->filterByAuthentifier($authentifier);
            }

            $this->filterByDestinataire($destinataire);
            $cachedMessageAccueil = $this->findOne($con);

            Prado::getApplication()->Cache->set($cachedMessageAccueilKey, $cachedMessageAccueil,Atexo_Config::getParameter('TTL_PRADO_CACHE_MOYEN'));
        }

        return $cachedMessageAccueil;
    }

    public function reloadCachedMessageAccueil($destinataire, $authentifier, $messageObj) {
        $cachedMessageAccueilKey = self::getCachedMessageAccueilKey($destinataire, $authentifier);
        Prado::getApplication()->Cache->set($cachedMessageAccueilKey, $messageObj, Atexo_Config::getParameter('TTL_PRADO_CACHE_MOYEN'));

    }

    private function getCachedMessageAccueilKey($destinataire, $authentifier) {
        $configPf =  Atexo_Config::getParameter('CONFIG_PLATEFORME_POUR_MESSAGE_ACCUEIL');
        $cachedMessageAccueilKey = 'cachedMessageAccueil'.$destinataire.$configPf;

        if($authentifier !== NULL){
            $cachedMessageAccueilKey = $cachedMessageAccueilKey.$authentifier;
        }
        return $cachedMessageAccueilKey;
    }
}
