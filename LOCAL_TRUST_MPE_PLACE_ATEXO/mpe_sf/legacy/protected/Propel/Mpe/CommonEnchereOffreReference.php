<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;
use Application\Propel\Mpe\CommonEnchereReferencePeer;





use Application\Propel\Mpe\Om\BaseCommonEnchereOffreReference;

/**
 * Skeleton subclass for representing a row from the 'EnchereOffreReference' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonEnchereOffreReference extends BaseCommonEnchereOffreReference {

	public $_commonEncherereference;
	private $aCommonEnchereReference;
	protected $aCommonEnchereOffre;

	/*public function getCommonEncherereference()
    {
        return $this->_commonEncherereference;
    }*/
    /*public function setCommonEncherereference($objet)
    {
        $this->_commonEncherereference=$objet;
    }*/

	public function getCommonEnchereReference($con = null)
	{
		if ($this->aCommonEnchereReference === null && ($this->idencherereference !== null)&& ($this->organisme !== null)) {
			$this->aCommonEnchereReference = CommonEnchereReferencePeer::retrieveByPK($this->idencherereference,$this->organisme,  $con);
		}
		return $this->aCommonEnchereReference;
	}

	public function setCommonEnchereReference($v)
	{
		if ($v === null) {
			$this->setIdencherereference('0');
		} else {
			$this->setIdencherereference($v->getId());
		}
		$this->aCommonEnchereReference = $v;
	}

    public function setCommonEnchereOffre(CommonEnchereOffre $v = null)
	{
		if ($v === null) {
			$this->setIdenchereoffre('0');
		} else {
			$this->setIdenchereoffre($v->getId());
		}
		$this->aCommonEnchereOffre = $v;
	}
} // CommonEnchereOffreReference
