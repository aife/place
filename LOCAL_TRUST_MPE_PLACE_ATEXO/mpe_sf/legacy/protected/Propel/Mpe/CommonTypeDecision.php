<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;




use Application\Propel\Mpe\Om\BaseCommonTypeDecision;
use Prado\Prado;

/**
 * Skeleton subclass for representing a row from the 'TypeDecision' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonTypeDecision extends BaseCommonTypeDecision {

	/*
	 * Le champ code_type_decision est en correspondance avec messages.xml
	 */
	public function getLibelleTypeDecision()
	{
		return Prado::localize($this->code_type_decision);
	}
} // CommonTypeDecision
