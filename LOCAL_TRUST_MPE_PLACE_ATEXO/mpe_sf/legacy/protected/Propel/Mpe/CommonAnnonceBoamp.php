<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\Om\BaseCommonAnnonceBoamp;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;


/**
 * Skeleton subclass for representing a row from the 'AnnonceBoamp' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAnnonceBoamp extends BaseCommonAnnonceBoamp {

     public $_commonReferentielDestinationFormXml = null;
	 public ?\Application\Propel\Mpe\CommonReferentielFormXml $_commonReferentielFormXml = null;
 	 public ?\Application\Propel\Mpe\CommonReferentielTypeXml $_commonReferentielTypeXml = null;
     private $_contentAnnXml= null;
	 private $_contentAnnPdf = null;
	 private $_contentAnnFormValues = null;


     public function retreiveDateEnvoi()
     {
             if($this->getDateEnvoi()!='0000-00-00 00:00:00' && $this->getDateEnvoi()!='0000-00-00') {
                     return Atexo_Util::iso2frnDateTime($this->getDateEnvoi(),false);
             }
             else {
             return Prado::localize('NON_RENSEIGNE');
             }
     }
     public function retreiveDatePublication()
     {
             if($this->getDatepub()!='0000-00-00 00:00:00' && $this->getDatepub()!='0000-00-00') {
                     return Atexo_Util::iso2frnDate($this->getDatepub());
             }
             else {
             return Prado::localize('NON_RENSEIGNE');
             }
     }
    public function getCommonReferentielDestinationFormXml($con = null)
	{
	    if($this->_commonReferentielDestinationFormXml != null) {
	        return $this->_commonReferentielDestinationFormXml;
	    }
		if(!$con){
			$con = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
		}
		if (($this->getIdDestinationFormXml() !== null)) {

			return CommonReferentielDestinationFormXmlPeer::retrieveByPK($this->getIdDestinationFormXml(), $this->getOrganisme(), $con);

		}
	}

	public function setCommonReferentielDestinationFormXml($commonRefDestFormXml)
	{
		if ($commonRefDestFormXml === null) {
			$this->setIdDestinationFormXml(NULL);
		} else {
			$this->setIdDestinationFormXml($commonRefDestFormXml->getId());
		}
		$this->_commonReferentielDestinationFormXml = $commonRefDestFormXml;
	}

	public function getAnnXml() {
		if(!$this->_contentAnnXml) {
			$this->_contentAnnXml = stream_get_contents( parent::getAnnXml());
		}
		return $this->_contentAnnXml;
	}
	public function getAnnPdf() {
		if(!$this->_contentAnnPdf) {
			$this->_contentAnnPdf = stream_get_contents( parent::getAnnPdf());
		}
		return $this->_contentAnnPdf;
	}
	public function getAnnFormValues() {
		if(!$this->_contentAnnFormValues) {
			$this->_contentAnnFormValues = stream_get_contents( parent::getAnnFormValues());
		}
		return $this->_contentAnnFormValues;
	}
	/**
     * recupere l'objet CommonReferentielFormXml de l'objet CommonAnnonceBoamp courant
     * 
     * @return CommonReferentielFormXml 
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
	 * @version 1.0
	 * @since 4.8.0
	 * @copyright Atexo 2014
     */
	public function getCommonReferentielFormXml($con = null){
		if($this->_commonReferentielFormXml != null) {
	        return $this->_commonReferentielFormXml;
	    }
		if(!$con){
			$con = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
		}
		if (($this->getIdFormXml() !== null)) {
			return CommonReferentielFormXmlPeer::retrieveByPK($this->getIdFormXml, $this->getOrganisme(), $con);
		}
	}
	/**
     * l'objet CommonReferentielFormXml de l'objet CommonAnnonceBoamp courant
     * 
     * @param $commonReferentielFormXml CommonReferentielFormXml
     * @return void
	 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
	 * @version 1.0
	 * @since 4.6.0
	 * @copyright Atexo 2014
     */ 
	public function setCommonReferentielFormXml($commonReferentielFormXml)
	{
		if ($commonReferentielFormXml instanceof CommonReferentielFormXml) {
			$this->setIdFormXml($commonReferentielFormXml->getId());
			$this->_commonReferentielFormXml = $commonReferentielFormXml;
		} else {
			$this->setIdFormXml(NULL);
		}
	}
	/**
     * recupere l'objet CommonReferentielTypeXml de l'objet CommonAnnonceBoamp courant
     * 
     * @return CommonReferentielTypeXml 
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
	 * @version 1.0
	 * @since 4.8.0
	 * @copyright Atexo 2014
     */
	public function getCommonReferentielTypeXml(){
		$con = null;
		if($this->_commonReferentielTypeXml != null) {
	        return $this->_commonReferentielTypeXml;
	    }
		if(!$con){
			$con = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
		}
		if (($this->getIdTypeXml() !== null)) {
			return CommonReferentielFormXmlPeer::retrieveByPK($this->getIdTypeXml, $this->getOrganisme(), $con);
		}
	}
	/**
     * l'objet CommonReferentielFormXml de l'objet CommonAnnonceBoamp courant
     * 
     * @param $commonReferentielTypeXml CommonReferentielTypeXml
     * @return void
	 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
	 * @version 1.0
	 * @since 4.6.0
	 * @copyright Atexo 2014
     */ 
	public function setCommonReferentielTypeXml($commonReferentielTypeXml)
	{
		if ($commonReferentielTypeXml instanceof CommonReferentielTypeXml) {
			$this->setIdTypeXml($commonReferentielTypeXml->getId());
			$this->_commonReferentielTypeXml = $commonReferentielTypeXml;
		} else {
			$this->setIdTypeXml(NULL);
		}
	}

} // CommonAnnonceBoamp
