<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Propel\Mpe;



use Application\Propel\Mpe\Om\BaseCommonAgentCommission;

/**
 * Skeleton subclass for representing a row from the 'Agent_Commission' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package mpe
 */	
class CommonAgentCommission extends BaseCommonAgentCommission {
	private $_contentConvocation = null;
	private $_contentFichierEnvoye = null;

	public function getConvocation() {
		if(!$this->_contentConvocation) {
			$this->_contentConvocation = stream_get_contents( parent::getConvocation());
		}
		return $this->_contentConvocation;
	}
	public function getFichierEnvoye() {
		if(!$this->_contentFichierEnvoye) {
			$this->_contentFichierEnvoye = stream_get_contents( parent::getFichierEnvoye());
		}
		return $this->_contentFichierEnvoye;
	}

} // CommonAgentCommission
