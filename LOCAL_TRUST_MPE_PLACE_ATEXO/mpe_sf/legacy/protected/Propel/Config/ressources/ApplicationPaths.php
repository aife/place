<?php
use Application\Pages\Commun\Classes\Util;

// Guillaume : à supprimer à terme.

class ApplicationPaths extends \Prado\Web\UI\TPage
{
    public function getBaseRootDir()
    {
        return \Application\Service\Atexo\Atexo_Config::getParameter('BASE_ROOT_DIR');
    }

    /**
	 * Enter description here...
	 *
	 * @return unknown
	 */
    public function getPrefixDir()
    {
        $util = new Util();
        return $util->getMinOrganisme()."/";
    }

    public function getCommonRootDir()
    {
        return $this->getBaseRootDir()."common/";
    }

    public function getRootDir()
    {
        return $this->getBaseRootDir().$this->getPrefixDir();
    }
    public function getTmpDir()
    {
        return $this->getBaseRootDir().$this->getPrefixDir()."tmp/";
    }
    public function getCADir()
    {
        if (\Application\Service\Atexo\Atexo_Config::getParameter('ARCH_AVEC_PORTAIL')){
        	return $this->getCommonRootDir()."ca/";
        }else{
        	return $this->getRootDir()."ca/";
        }
    }
    public function getCertsDir()
    {
        return $this->getCADir()."certs/";
    }

    public function getKeysDir()
    {
        return $this->getCADir()."keys/";
    }


    public function getMarchesCACert()
    {
        return $this->getCertsDir()."marches_ca.crt";
    }

    public function getMarchesCAKey()
    {
        return $this->getKeysDir()."marches_ca_pkey.pem";
    }

    public function getCaSerial()
    {
        return $this->getBaseRootDir().$this->getPrefixDir()."serial";
    }

    public function getOpensslConfig()
    {
        return $this->getBaseRootDir().$this->getPrefixDir()."openssl.cnf";
    }
    /*---------------------- PARTIE SPECIFIQUE A L'ORGANISME ------------------

    if (session_name() != "super_administrateur" && session_name() != "portail_agent" && CALLED_FROM_PORTAIL_EXTERNE != '1')
    {
    define ('PREFIX_DIR', $this->User);
    }
    else
    define ('PREFIX_DIR', 'common');
    */
    /*---------------------- PARTIE COMMUNE A TOUS LES ORGANISMES -------------

    define('BASE_ROOT_DIR', "/var/mpe-1.8/riad/");

    define('COMMON_ROOT_DIR', BASE_ROOT_DIR."common/");

    define('COMMON_TRUST', COMMON_ROOT_DIR.'trust/');
    define('COMMON_LOG', COMMON_ROOT_DIR.'logs/');
    define('COMMON_TMP', COMMON_ROOT_DIR.'tmp/');
    define('COMMON_DB', COMMON_ROOT_DIR.'database/');

    define('ROOT_DIR', BASE_ROOT_DIR.PREFIX_DIR.'/');

    require 'arch_config.inc.php';
    if (ARCH_AVEC_PORTAIL)
    {
    define('CA_DIR', COMMON_ROOT_DIR.'ca/');
    define('TRUST_DIR', COMMON_ROOT_DIR.'trust/');
    }
    else
    {
    define('CA_DIR', ROOT_DIR.'ca/');
    define('TRUST_DIR', ROOT_DIR.'trust/');
    }
    define('LOG_DIR', ROOT_DIR.'logs/');
    define('LOG_PATH', LOG_DIR);
    define('TMP_DIR', ROOT_DIR.'tmp/');
    define('TMP_PATH', TMP_DIR);
    define('DB_DIR', ROOT_DIR.'database/');

    define('CERTS_DIR', CA_DIR.'certs/');
    define('KEYS_DIR', CA_DIR.'keys/');

    define('ONLINE_SET_LD_PATH', 'On');
    define('OPENSSL', '/usr/local/ssl/bin/openssl');
    define('OPENSSL_ENGINE', '');
    define('OPENSSL_CONFIG', ROOT_DIR.'openssl.cnf');

    define('TSA_SECTION', 'tsa_config_mpe-1.8');

    define('CA_SERIAL', ROOT_DIR.'serial');

    define('ROOT_CA_CERT', CERTS_DIR.'root_ca.crt');
    define('ROOT_CA_PKEY', KEYS_DIR.'root_ca_pkey.pem');
    define('MARCHES_CA_CERT', CERTS_DIR.'marches_ca.crt');
    define('MARCHES_CA_PKEY', KEYS_DIR.'marches_ca_pkey.pem');
    define('TS_CERT', CERTS_DIR.'ts.crt');
    define('TS_PKEY', KEYS_DIR.'ts_pkey.pem');
    define('SMIME_CERT', CERTS_DIR.'s_mime.crt');
    define('SMIME_PKEY', KEYS_DIR.'s_mime_pkey.pem');

    define ('CA_CERT_EXTENSION', 'usr_ca_cert');
    define ('TS_CERT_EXTENSION', 'ts_cert');
    define ('SMIME_CERT_EXTENSION', 's_mime_cert');

    define('NCIPHER_LD_PATH', '');

    define('SIGN_CADIR', ROOT_DIR.'tmp/');
    define('WORK_DIR', ROOT_DIR.'tmp/');

    define('ERROR_FILE', LOG_DIR.'error_log');
    define('CRL_VERIF_FILE', LOG_DIR.'crl_log');
    */
    /** Pour les fichiers de logs
define('FILE_LOG','portail_cea');
define('FILE_LOG_ERROR','portail_cea_error');

define('HTTP_USER', 'nobody');

define('BOAMP_ID_APPLI', 'Local Trust MPE');
define('BOAMP_VERSION_APPLI', 'MPE 1.8');
define('BOAMP_BOUNDARY', '7d436b332049a');
define('ID_AAPC_EMETTEUR', '0000000');

//FOP Permet de transformer les .fo en .pdf
define('FOP', '/usr/local/fop-0.20.5/fop.sh');

//variable d'environnement JAVA_HOME pour fop
define('JAVA_HOME_ENV', 'JAVA_HOME=/usr/local/java/j2re1.4.2_04');

define('TS_SERVER_MODULE', '0'); // Module serveur d horodatage
define('TS_SERVER_ADDR', "https://193.149.116.176:4430/curl_rcv.php"); // Adresse du serveur d horodatage

if (ARCH_AVEC_PORTAIL)
  define('PLATFORM_ID', "portail_cnes_test"); // Identifiant de la plate forme

//Module Anti-virus
define('VIRUS_VAULT_DIR', COMMON_ROOT_DIR . 'virus_vault');
*/
}
