<?php
namespace Application\Propel\Config\Database;
// This file generated by Propel convert-props target on Tue Mar  6 19:56:29 2007
// from XML runtime conf file ./projects/mpe/runtime-conf.xml

use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;
use Prado\Util\TLogger;


function construct_array()
{
    $array_base = [];
				$commonDB = Atexo_Config::getParameter('COMMON_DB');

  	//recuperer du fichier de configuration le nom de la constante
    $const_read_write = Atexo_Config::getParameter('CONST_READ_WRITE');
	$const_read_only = Atexo_Config::getParameter('CONST_READ_ONLY');

	//recuperation des adresse IP
    $hostspec_read_write = Atexo_Config::getParameter('HOSTSPEC'.$const_read_write);
    $hostspec_read_only = Atexo_Config::getParameter('HOSTSPEC'.$const_read_only);

    $username = Atexo_Config::getParameter('DATABASE_USERNAME');
    $password = Atexo_Config::getParameter('DATABASE_PASSWORD');
    $encoding = Atexo_Config::getParameter('DB_ENCODING');
    
    if (Atexo_Config::getParameter('DB_PREFIX')) {
	    $prefix = Atexo_Config::getParameter('DB_PREFIX') . '_';
    }else{
    	$prefix = '';
    }

    $DB = $commonDB;
    $port = (int) trim(Atexo_Config::getParameter('DATABASE_PORT'));
	$arraySettings = array();
	$arraySettings["charset"]["value"] = $encoding;

    $dbParameters = [
        'adapter' => 'mysql',
        'connection' => [
            'phptype' => 'mysql',
            'hostspec' => $hostspec_read_write,
            'database' => $port,
            'port' => $prefix . $DB,
            'user' => $username,
            'password' => $password,
            'dsn' => 'mysql:host=' . $hostspec_read_write . (empty($port) ? '' : ';port=' . $port)
                . ';dbname=' . $prefix . $DB,
            'settings' => $arraySettings
        ]
    ];

    $array_base["mpe"] = $dbParameters;
	$array_base[$DB.$const_read_write] = $dbParameters;

	$array_base[$DB.$const_read_only] = $dbParameters;
    $array_base[$DB.$const_read_only]['connection']['hostspec'] = $hostspec_read_only;
    $array_base[$DB.$const_read_only]['connection']['dsn'] = 'mysql:host=' . $hostspec_read_only .
        (empty($port) ? '' : ';port=' . $port) . ';dbname=' . $prefix . $DB;


    $commonDBTechnique = Atexo_Config::getParameter('COMMON_DB_TECHNIQUE');

    //recuperation des adresse IP
    $hostspec_read_write_technique = Atexo_Config::getParameter('HOSTSPEC_TECHNIQUE'.$const_read_write);
    $hostspec_read_only_technique = Atexo_Config::getParameter('HOSTSPEC_TECHNIQUE'.$const_read_only);

    $usernameTechnique = Atexo_Config::getParameter('USERNAME_TECHNIQUE');
    $passwordTechnique = Atexo_Config::getParameter('PASSWORD_TECHNIQUE');


    if (Atexo_Config::getParameter('DB_PREFIX_TECHNIQUE')) {
        $prefixTechnique = Atexo_Config::getParameter('DB_PREFIX_TECHNIQUE') . '_';
    }else{
        $prefixTechnique = '';
    }
    try {
        $portTechnique = Atexo_Config::getParameter('DB_PORT_TECHNIQUE');
        if($port != 0){
            $hostspec_read_write_technique=$hostspec_read_write_technique.':'.$portTechnique;
            $hostspec_read_only_technique=$hostspec_read_only_technique.':'.$portTechnique;
        }
    } catch (\Exception $e) {
        Prado::log("Erreur MPEconf.php" . $e->getMessage() . $e->getTraceAsString() , TLogger::ERROR, "MPEconf.php");
    }

    $DBTechnique= $commonDBTechnique;

    $array_base[$DBTechnique.$const_read_write] = array('adapter'=>'mysql', 'connection'=>array('phptype'=>'mysql',
        'hostspec'=>$hostspec_read_write_technique,
        'database'=>$prefixTechnique.$DBTechnique,
        'user'=>$usernameTechnique,
        'password'=>$passwordTechnique,
        'dsn' => 'mysql:host='.$hostspec_read_write_technique.';dbname='.$prefixTechnique.$DB,
        'settings'=>$arraySettings)
    );

    $array_base[$DBTechnique.$const_read_only] =  array('adapter'=>'mysql', 'connection'=>array('phptype'=>'mysql',
        'hostspec'=>$hostspec_read_only_technique,
        'database'=>$prefixTechnique.$DBTechnique,
        'user'=>$usernameTechnique,
        'password'=>$passwordTechnique,
        'dsn' => 'mysql:host='.$hostspec_read_only_technique.';dbname='.$prefixTechnique.$DBTechnique,
        'settings'=>$arraySettings)
    );
    
    /*$array_base['mpe4_COMMON_DB'.$const_read_write] = 	array('adapter'=>'mysql', 'connection'=>array('phptype'=>'mysql','hostspec'=>$hostspec_read_write,
     * 	'database'=>'org1','username'=>$username,'password'=>$password, 'encoding'=>$encoding, 'dsn' => 'mysql:host='.$hostspec_read_write.';dbname='.'org1' ));
    $array_base['mpe4_COMMON_DB'.$const_read_only] = 	array('adapter'=>'mysql', 'connection'=>array('phptype'=>'mysql','hostspec'=>$hostspec_read_only,
    	'database'=>'org1','username'=>$username,'password'=>$password, 'encoding'=>$encoding, 'dsn' => 'mysql:host='.$hostspec_read_only.';dbname='.'org1' ));

    $array_base[$commonDB.$const_read_write] = 			array('adapter'=>'mysql', 'connection'=>array('phptype'=>'mysql','hostspec'=>$hostspec_read_write,	
    'database'=>$prefix.$commonDB,'username'=>$username,
    'password'=>$password, 'encoding'=>$encoding, 'dsn' => 'mysql:host='.$hostspec_read_write.';dbname='.$prefix.$commonDB ));
    $array_base[$commonDB.$const_read_only] = 			array('adapter'=>'mysql', 'connection'=>array('phptype'=>'mysql','hostspec'=>$hostspec_read_only,
    	'database'=>$prefix.$commonDB,'username'=>$username,'password'=>$password, 'encoding'=>$encoding, 'dsn' => 'mysql:host='.$hostspec_read_only.';dbname='.$prefix.$commonDB ));
    */
    $array_base['default'] = 		$commonDB.$const_read_write;

    return array (
    'log' =>
    array (
    'ident' => 'propel-mpe',
    'level' => '6',
    'name'	=>	'protected/var/log/propel.log'
    ),
    'propel' =>
    array (
    'datasources' =>  $array_base,
    )
    );
}
return construct_array();

