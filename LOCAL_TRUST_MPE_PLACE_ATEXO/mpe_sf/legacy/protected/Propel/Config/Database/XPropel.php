<?php

namespace Application\Propel\Config\Database;

use Application\Library\Propel\Propel;
use Prado\Exceptions\TConfigurationException;
use Prado\Exceptions\TInvalidOperationException;
use Prado\TModule;
use Prado\Xml\TXmlElement;

/**
 * XPropel class file
 *
 * @author Taeber Rapczak <taeber@rapczak.com>
 *     Note: A lot of this code was based on Qiang Xue's TAssetManager.
 * @license http://creativecommons.org/licenses/LGPL/2.1/ CC-GNU LGPL
 */

/**
 * XPropel class
 *
 * XPropel provides a PRADO-like way to include Propel Data Objects into your
 * PRADO/3 Applications.
 *
 * XPropel may be configured in an application configuration file as follows:
 * <code>
 * <application id="Example" mode="Debug">
 * <paths>
 *    <using namespace="Application.Models.Example.*" />
 * </paths>
 *
 * <modules>
 *    <module class="Application.Common.XPropel"
 *¦nbsp;        PropelPath="/usr/local/php5/lib/php/propel"
 *¦nbsp;         ConfigFile="protected/Data/Database-conf.php" />
 * </modules>
 * ....
 * </application>
 * </code>
 * where {@link getPropelPath PropelPath} and {@link getConfigFile ConfigFile}
 * are configurable properties of XPropel. PropelPath is the path to the propel
 * runtime directory. ConfigFile is the path to the Propel-generated
 * configuration file for your database.
 *
 * This class hasn't been exensively tested because it was created specifically
 * for the needs of the author. Any feedback or questions would greatly be
 * appreciated.
 *
 * @author Taeber Rapczak <taeber@rapczak.com>
 */
class XPropel extends TModule
{
    /**
     * Default base path where the Propel runtime files are located
     */
    const DEFAULT_PROPELPATH = 'propel';
    /**
     * @var string base path where the Propel runtime files are located (needs to be in the include_path)
     */
    private $_propelPath = null;
    /**
     * @var string path to the Propel generated PHP configuration file for the database
     */
    private ?string $_configFile = null; # Does this need to be a list to allow multiple config files?
    /**
     * @var boolean whether the module is initialized
     */
    private bool $_initialized = false;

    /**
     * Initializes the module.
     * This method is required by IModule and is invoked by application.
     *
     * @param TXmlElement module configuration
     */
    public function init($config)
    {
        if ($this->_configFile === null) {
            throw new TConfigurationException('propel_configfile_unspecified');
        }
        if ($this->_propelPath === null) {
            $this->setPropelPath(XPropel::DEFAULT_PROPELPATH);
        }
        Propel::init($this->_configFile);
        $this->_initialized = true;
    }

    /**
     * The path to the Propel-generated configuration file for the databse.
     */
    public function getConfigFile()
    {
        return $this->_configFile;
    }

    /**
     * Sets the path to the Propel generated PHP configuration file for the database
     *
     * @param string the path to the file
     * @throws TInvalidOperationException if the module is initialized already
     */
    public function setConfigFile($value)
    {
        $absoluteDir = realpath(__DIR__ . '/../../../..');
        $value = $absoluteDir . DIRECTORY_SEPARATOR . $value;

        if ($this->_initialized) {
            throw new TInvalidOperationException('propel_configfile_unchangeable');
        } else {
            if (!is_readable($value)) {
                throw new TConfigurationException('propel_configfile_invalid', $this->_configFile);
            }
            $this->_configFile = $value;
        }
    }

    /**
     * The path to the Propel runtime directory.
     */
    public function getPropelPath()
    {
        return $this->_propelPath;
    }

    /**
     * Sets the path to the Propel runtime directory.
     *
     * @param string path to the Propel runtime directory.
     * @throws TInvalidOperationException if the module is initialized already
     */
    public function setPropelPath($value)
    {
        if ($this->_initialized) {
            throw new TInvalidOperationException('propel_propelpath_invalid');
        } else {
            @include_once $value . '/Propel.php';
            @include_once $value . '/util/Criteria.php';

            $this->_propelPath = $value;
        }
    }
}
