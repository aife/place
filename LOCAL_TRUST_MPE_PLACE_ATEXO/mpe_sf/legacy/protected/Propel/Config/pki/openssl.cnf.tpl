#
# Fichier de configuration de la PKI Entreprises

HOME			= .
RANDFILE		= $ENV::HOME/.rnd

oid_section		= new_oids

[ new_oids ]

[ ca ]
default_ca	= CA_default		# The default ca section

[ CA_default ]

dir		= "__DIR__" # Where everything is kept
certs		= $dir/certs		# Where the issued certs are kept
crl_dir		= $dir/crl		# Where the issued crl are kept
database	= __CERTS_DB__	# database index file.
new_certs_dir	= $dir/newcerts		# default place for new certs.
certificate	= __CERT_PATH__ 		# The CA certificate
serial		= $dir/serial 		# The current serial number
crl		= $dir/crl.pem 		# The current CRL
private_key	= __CERT_PKEY__ # The private key
RANDFILE	= $dir/private/.rand	# private random number file

x509_extensions	= usr_cert		# The extentions to add to the cert

# Comment out the following two lines for the "traditional"
# (and highly broken) format.
name_opt 	= ca_default		# Subject Name options
cert_opt 	= ca_default		# Certificate field options

crl_extensions	= crl_ext

default_days	= 365			# how long to certify for
default_crl_days= 30			# how long before next CRL
default_md	= sha1			# which md to use.
preserve	= no			# keep passed DN ordering

# For the CA policy
[ policy_match ]
countryName		= optional
stateOrProvinceName	= optional
organizationName	= optional
organizationalUnitName	= optional
commonName		= supplied
emailAddress		= optional

# For the 'anything' policy
# At this point in time, you must list all acceptable 'object'
# types.
[ policy_anything ]
countryName		= optional
stateOrProvinceName	= optional
localityName		= optional
organizationName	= optional
organizationalUnitName	= optional
commonName		= supplied
emailAddress		= optional

[ usr_cert ]

basicConstraints=CA:FALSE

 keyUsage = nonRepudiation, digitalSignature, keyEncipherment

# PKIX recommendations harmless if included in all certificates.
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer:always

[ s_mime_cert ]

basicConstraints=CA:FALSE
 keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer:always

[ crl_ext ]
authorityKeyIdentifier=keyid:always,issuer:always