<?php

require_once('../library/Atexo/PdfGenerator.php');
require_once('../library/Atexo/Download/DowloadFile.php');
require_once('./config/config.php');

//	ODT à convertir
$nom=$_FILES['odtFile']['name'];
$tabNom = explode('.',$nom);
$ext = $tabNom[count($tabNom)-1];
$nom=base64_encode($nom);
$odt=TMP_PATH.'/'.uniqid().$nom.".".$ext;

// emplacement temporaire
$odtTmp = $_FILES['odtFile']['tmp_name'];
//Test de la présence du fichier odt

if(file_exists($odtTmp)){
	
	move_uploaded_file($odtTmp,$odt);
	//echo "<br>odt = ".$odt."<br>";
	//pdf à créer
	$extFile=$_POST['extFile'];
	
	
	$pdf=TMP_PATH.'/'.uniqid().$nom.".".$extFile;
	//echo "pdf = " .$pdf; 
	//generation PDF
	
	$result = PdfGenerator::genererPdf($odt,$pdf);
	
	//FIXME supprimer le .odt
	@unlink($odt);
	//echo "je suis ici ";exit;
	if ($result==0) {
		$nom=base64_decode($nom);
		DownloadFile::downloadFilePath($nom.".".$extFile, $pdf, true);
	
	}
	else
		echo $result;
}else {
	
	echo "Le fichier ".$odtTmp." n'existe pas.";
}




