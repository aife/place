<?php

require_once('../library/Atexo/PdfGeneratorClient.php');
require_once('../library/Atexo/Download/DowloadFile.php');
require_once('../library/Atexo/PdfGenerator/PdfGeneratorClientException.php');
require_once('./config/config.php');

require_once('./ImpressionJeb.php');
require_once('../library/Externe/dUnzip2.php');
require_once('../library/Externe/zipfile.php');


try {
	generatePdfPasseportJb();

}
catch (Exception $e)
{
	echo ($e->getMessage());
}

function generatePdfPasseportJb()
{
	$data = array();
	$data['NOM_PROJET'] = "Papéterie";
	$data['IDENTIFIANT_PROJET'] = "test";
	$data['MDP_PROJET'] = "test";
	$data['NOM_PORTEUR'] = "Atexo";
	$data['PRENOM_PORTEUR'] = "Maroc";
	$data['DATE_NAISS_PORTEUR'] = "21/04/2004";
	$data['NOM_CONTACT'] = "Atexo";
	$data['PRENOM_CONTACT'] = "Maroc";
	$data['ORG_CONTACT'] = "Atexo Maroc";
	$data['TELEPHONE_CONTACT'] = "061254433";
	$data['COURRIEL_CONTACT'] = "Atexo@gmail.com";
	$data['DATE_ENTREVUE'] = "01/06/2008";
	$data['OBJET_RENCONTRE'] = "Papéterie";
	$data['NIVEAU_DIPLOME'] = "Master";
	$data['STATUT_AVANT_CREATION'] = "test";
	$data['SITUATION_INDEMNITAIRE'] = "test";
	$data['TRAVAIL_HANDICAPE'] = "test";
	$data['VOTRE_PROJET'] = "Papéterie";
	$data['AUTRES_SUJETS'] = "Atexo";
	
	
	$impressionJeb = new ImpressionJeb();
	$impressionJeb->ImpressionPasseportJeb($data);
	$impressionJeb->send("Passeport.pdf");
}



