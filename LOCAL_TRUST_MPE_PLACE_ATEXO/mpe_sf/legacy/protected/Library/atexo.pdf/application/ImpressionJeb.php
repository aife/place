<?php
namespace AtexoPdf;

use Application\Service\Atexo\Atexo_Config;

class ImpressionJeb extends ImpressionPDF{
	
	
	public function __construct()
	{
		ImpressionPDF::__construct();
	}
	
	/***
	 * Impression FACTURE D'ACOMPTE TRIMESTRIEL
	 *
	 * @param  $tableKeys
	 */
	public function ImpressionPasseportJeb($dataPasseportJeb){
		$this->CHEMIN_MODEL_ODT = ODT_PASSEPORT_JEB;
		$name = "PasseportJeb".uniqid().".odt";
		$this->namefile=$name;
		$this->open();
		
		foreach($dataPasseportJeb as $key=>$value)
			{
					$this->odt_str_replace($key,utf8_encode($value));
			}
		$this->close();
		$this->genererFilePDF();
		
	}
	public function Impression_FAT($tableKeys,$idAction,$idcf)
	{
		$this->CHEMIN_MODEL_ODT = Atexo_Config::getParameter('CHEMIN_MODEL_ODS_FACTUREACOMPTETRIMESTRIEL');
		$name = "ACOMPTE_TRIMESTRIEL".date("Y-m-d_H_i_s").".ods";
		$this->namefile = $name;
		$this->open();
		$xmlarray=$this->replace_str1_str2("[Suite_parcour]");
		$str1=$xmlarray['str1'];
		$str2=$xmlarray['str2'];
		
		$tableKeyStagerS=StagiaireActionFormation::getDatafacturation($idAction,$idcf);
	
		$i=0;
		
		foreach($tableKeyStagerS["stag"] as $key=>$tableKeyStager)
		{	
			foreach($tableKeyStager as $key=>$value)
			{
					$this->odt_str_replace($key,utf8_encode($value));
			}
			if($i < count($tableKeyStagerS["stag"])-1)
			$this->odt_str_replace($str2,$str1.$str2);
			$i++;
		}
		foreach($tableKeyStagerS["Sommestag"] as $key=>$value)
			{
				$this->odt_str_replace($key,utf8_encode($value));
			}
		foreach($tableKeys as $key=>$value)
		{
			$this->odt_str_replace($key,utf8_encode($value));
		}
		
		$this->close();
		$this->genererFilePDF();
		$this->send("test",".ods");		
	}
	
	public function Impression_OrdrePaiementAvanceCnasea($tableKeys,$idAction)
	{
		$this->CHEMIN_MODEL_ODT = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_ORDREPAIEMENTAVANCECNASEA');
		$name = "ORDRE_APAIEMENT_AVANCE".date("Y-m-d_H_i_s").".odt";
		$this->namefile = $name;
		$this->open();
		
		foreach($tableKeys as $key=>$value)
		{
			$this->odt_str_replace($key,utf8_encode($value));
		}
		
		$this->close();
		$this->genererFilePDF();
		$this->send("test",".odt");		
	}
	public function Impression_OrdrePaiementAvancePaierie($tableKeys,$idAction)
	{
		$this->CHEMIN_MODEL_ODT = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_ORDREPAIEMENTAVANCEPAIERIE');
		$name = "ORDRE_APAIEMENT_AVANCE".date("Y-m-d_H_i_s").".odt";
		$this->namefile = $name;
		$this->open();
	
		foreach($tableKeys as $key=>$value)
		{
			$this->odt_str_replace($key,$value);
		}
		
		$this->close();
		$this->genererFilePDF();
		$this->send("test",".odt");		
	}
	
public function Impression_List_stag($tableKeys1,$tableKeys2){
		$this->CHEMIN_MODEL_ODT = Atexo_Config::getParameter('CHEMIN_MODEL_ODS_Liste_Nominative_des_stagiaires');
		$name = "Liste_Nominative_des_stagiaires".date("Y-m-d_H_i_s").".ods";
		$this->namefile = $name;
		$this->open();
		$xmlarray=$this->replace_str1_str2("[NOM_PRENOM]");
		$str1=$xmlarray['str1'];
		$str2=$xmlarray['str2'];
	
		$i=0;
		{
		
			foreach($tableKeys2 as $key=>$tableKeyStager)
			{	
				foreach($tableKeyStager as $key=>$value)
				{
						$this->odt_str_replace($key,utf8_encode($value));
				}
				if($i < count($tableKeys2)-1)
					$this->odt_str_replace($str2,$str1.$str2);
				
				$i++;
			}
		}
		
		
		foreach($tableKeys1 as $key=>$value)
		{
			$this->odt_str_replace($key,utf8_encode($value));
		}
	
		$this->close();
		$this->genererFilePDF();
		$this->send("test",".ods");		
	}
	
	public function Impression_Periode($tableKeys1,$tableKeys2,$idAction){
		$this->CHEMIN_MODEL_ODT = Atexo_Config::getParameter('CHEMIN_MODEL_ODS_Annexe_trimestrielle_2007');
		$name = "Annexe_trimestrielle_2007".date("Y-m-d_H_i_s").".ods";
		$this->namefile = $name;
		$this->open();
		$xmlarray=$this->replace_str1_str2("nomprenom");
		$str1=$xmlarray['str1'];
		$str2=$xmlarray['str2'];
		$i=0;
		
			
		foreach($tableKeys2 as $key=>$tableKeyStager)
		{	
			foreach($tableKeyStager as $key=>$value)
			{
					$this->odt_str_replace($key,utf8_encode($value));
			}
			if($i < count($tableKeys2)-1)
				$this->odt_str_replace($str2,$str1.$str2);
			$i++;
		
		}
		
		foreach($tableKeys1 as $key=>$value)
		{
			$this->odt_str_replace($key,utf8_encode($value));
		}
		$Action=ActionFormationPeer::retrieveByPK($idAction);
		$idprograme=$Action->getIdProgramme();
	
		if((int)$idprograme==7 && (int)$idprograme==8)
			$this->ods_Cocher("programe7_8");
		else
			$this->ods_Cocher("programe".$idprograme);
		$this->close();
		$this->genererFilePDF();
		$this->send("test",".ods");		
	}


public function Impression_OrdrePaiementAcompteCnasea($tableKeys,$idAction,$idcertif=null)
	{
		$this->CHEMIN_MODEL_ODT = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_ORDREPAIEMENTACOMPTECNASEA');
		$name ="ORDRE_PAIEMENT_ACOMPTE".date("Y-m-d_H_i_s").".odt";
		$this->namefile =$name;
		$this->open();

		if($idcertif)
		$dataAcompte=CertificatPaiement::getAcomptePrecedents($idAction,$idcertif);
		if(count($dataAcompte))
		{	
		$xmlarray=$this->replace_str1_str2("[MOIS_ACOMPTE]");
		$str1=$xmlarray['str1'];
		$str2=$xmlarray['str2'];
		}
		$i=0;
		
	
	if($dataAcompte[1]==null)
	{
		$this->odt_str_replace($str1,"");
		
		$dataAcompte[0]["[MONTA NT_ACOMPTE_N-2]"]=" ";
		$dataAcompte[0]["[TAUX_REAL_N-2]"]=" ";
		$dataAcompte[0]["[position]"]=" ";
		$dataAcompte[0]["[MOIS_ACOMPTE]"]=" ";
		
	}
	else
	{
		
	  foreach($dataAcompte as $key=>$tableKeyStager)
 	 	{ 
	 	 	if($key!=0){
	 	 			foreach($tableKeyStager as $key=>$value)
			   {
			     $this->odt_str_replace($key,utf8_encode($value));
			   }
			   if($i < count($dataAcompte)-2)
			   $this->odt_str_replace($str2,$str1.$str2);
			   $i++;
	 	 		}
		   
	 	}
	}
		
		foreach($tableKeys as $key=>$value)
		{
			$this->odt_str_replace($key,utf8_encode($value));
		}
		
		$this->close();
		$this->genererFilePDF();
		$this->send("test",".odt");		


	}

public function Impression_OrdrePaiementAcomptePaierie($tableKeys,$idAction,$idcertif=null)
	{
		$this->CHEMIN_MODEL_ODT = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_ORDREPAIEMENTACOMPTEPAIERIE');
		$name = "ORDRE_PAIEMENT_ACOMPTE_".date("Y-m-d_H_i_s").".odt";
		$this->namefile = $name;
		$this->open();
		$util=new Util();
		if($idcertif)
		$dataAcompte=CertificatPaiement::getAcomptePrecedents($idAction,$idcertif);
		
		/*if(count($dataAcompte))
			
		$xmlarray=$this->replace_str1_str2("[MOIS_ACOMPTE]");
		$str1=$xmlarray['str1'];
		$str2=$xmlarray['str2'];*/
		
		$i=0;
		
	
	if(!count($dataAcompte))
	{
		$dataAcompte[0]["[MONTANT_ACOMPTE_N-2]"]=" ";
		$dataAcompte[0]["[TAUX_REAL_N-2]"]=" ";
		$dataAcompte[0]["[position]"]=" ";
		$dataAcompte[0]["[MOIS_ACOMPTE]"]=" ";
		$Total_des_acomptes=0;
	}
	else
	{
		
	  for($k=1;$k<= 6;$k++)
 	 	{ 
		 	
		     $this->odt_str_replace("[MONTANT_ACOMPTE_N-2]_$k",($dataAcompte[$k]["[MONTA NT_ACOMPTE_N-2]"]!="")?$util->getMontantAvecEspace($dataAcompte[$k]["[MONTA NT_ACOMPTE_N-2]"]):"");
		     $Total_des_acomptes+=str_replace(",",".",$dataAcompte[$k]["[MONTA NT_ACOMPTE_N-2]"]);
		     $this->odt_str_replace("[n$k]",$k);
		   
		  
	 	}
	}
		$this->odt_str_replace("[Total_des_acomptes]",$util->getMontantAvecEspace($Total_des_acomptes));
		$this->odt_str_replace("[titulaire_aura_recu]",$util->getMontantAvecEspace($Total_des_acomptes+str_replace(",",".",$dataAcompte[0]["[Montant_du_solde]"])));
		/*foreach($dataAcompte[0] as $key=>$value)
		{
			$this->odt_str_replace($key,utf8_encode($value));
		}*/
		foreach($dataAcompte as $key=>$tableKeyStager)
			{	
				foreach($tableKeyStager as $key=>$value)
				{
						$this->odt_str_replace($key,$util->getMontantAvecEspace(utf8_encode($value)));
				}
				
			}
		foreach($tableKeys as $key=>$value)
		{
			$this->odt_str_replace($key,utf8_encode($value));
		}
		
		$this->close();
		$this->genererFilePDF();
		$this->send("test",".odt");		
	}
	
	public function Impression_Absence($tableKeys1,$tableKeys2,$idAction){
		$this->CHEMIN_MODEL_ODT = Atexo_Config::getParameter('CHEMIN_MODEL_ODS_Annexe_des_absences');
		$name = "Annexe_des_absences".date("Y-m-d_H_i_s").".ods";
		$this->namefile = $name;
		$this->open();
		$xmlarray=$this->replace_str1_str2("[Prenom]");
		$str1=$xmlarray['str1'];
		$str2=$xmlarray['str2'];
		$i=0;
		
		if(count($tableKeys2)==0)
			$this->odt_str_replace($str1,"");	
			foreach($tableKeys2 as $key=>$tableKeyStager)
			{	
				foreach($tableKeyStager as $key=>$value)
				{
						$this->odt_str_replace($key,utf8_encode($value));
				}
				if($i < count($tableKeys2)-1)
				$this->odt_str_replace($str2,$str1.$str2);
				$i++;
			}
		
		
		foreach($tableKeys1 as $key=>$value)
		{
			$this->odt_str_replace($key,utf8_encode($value));
		}
		
		$this->close();
		$this->genererFilePDF();
		$this->send("test",".ods");		
	}
	
	
public function Impression_OrdrePaiementPArembavancep($tableKeys,$idAction)
	{
		$this->CHEMIN_MODEL_ODT = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_RECUPERATIONAVANCE');
		$name = "Recuperation_Avance".date("Y-m-d_H_i_s").".odt";
		$this->namefile = $name;
		$this->open();
	
		foreach($tableKeys as $key=>$value)
		{
			$this->odt_str_replace($key,utf8_encode($value));
		}
		
		$this->close();
		$this->genererFilePDF();
		$this->send("test",".odt");		
	}
public function Impression_OrdrePaiementPenalites($tableKeys,$idAction)
	{
		$this->CHEMIN_MODEL_ODT = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_PENALITES');
		$name = "PENALITES".date("Y-m-d_H_i_s").".odt";
		$this->namefile = $name;
		$this->open();
	
		foreach($tableKeys as $key=>$value)
		{
			$this->odt_str_replace($key,utf8_encode($value));
		}
		
		$this->close();
		$this->genererFilePDF();
		$this->send("test",".odt");		
	}
	
public function Impression_FicheVSFA($tableKeys,$idAction)
	{
		$this->CHEMIN_MODEL_ODT = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_VSFA');
		$name = "Fiche_VSFA".date("Y-m-d_H_i_s").".odt";
		$this->namefile = $name;
		
		$this->open();
		foreach($tableKeys as $key=>$value)
		{
			$this->odt_str_replace($key,utf8_encode($value));
		}
		
		$this->close();
		$this->genererFileDOC();
		$this->sendDoc("test",".odt");		
	}
}
?>