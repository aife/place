<?php
namespace AtexoPdf;

use AtexoPdf\Download\DownloadFile;
use \Exception;
use \DomDocument;

require_once('./config/config.php');

class ImpressionPDF {

	protected $content;
	protected $styles;
	protected $CHEMIN_MODEL_ODT;
	protected $CHEMIN_TEMPLATE_PDF;
	protected $CHEMIN_TEMP_PDF;
	protected $Case_cocher_checked;
	protected $Case_cocher;
	protected $namefile;
	protected $Tmp;
	protected $pdfContent;

	public function __construct(){
		$this->CHEMIN_TEMPLATE_PDF=CHEMIN_TEMPLATE_PDF;
		$this->CHEMIN_TEMP_PDF=CHEMIN_TEMP_PDF;
		$this->Tmp=CHEMIN_TEMP;
	}


	public function get_content()
	{
		try{
			$this->content=file_get_contents($this->CHEMIN_TEMPLATE_PDF.'content.xml');

		}
		catch(Exception){

		}
	}
	public function get_styles()
	{
		try{

			$this->styles=file_get_contents($this->CHEMIN_TEMPLATE_PDF.'styles.xml');
		}
		catch(Exception){

		}
	}
	public function set_content()
	{
		try{
			$fl=fopen($this->CHEMIN_TEMPLATE_PDF.'content.xml',"w");
			fwrite($fl,$this->content);
			fclose($fl);
		}
		catch(Exception)
		{

		}
	}
	public function set_styles()
	{
		try{
			$fl=fopen($this->CHEMIN_TEMPLATE_PDF.'styles.xml',"w");
			fwrite($fl,$this->styles);
			fclose($fl);
		}
		catch(Exception)
		{

		}
	}
	public function open()
	{
		try{
			$this->unzip();
			$this->get_content();
			$this->get_styles();
		}
		catch(Exception)
		{


		}

	}
	public function close()
	{
		try{
			$this->set_content();
			$this->set_styles();
			$this->genre_odt();
			@exec("rm -rf ".$this->CHEMIN_TEMPLATE_PDF,$array,$resulta);
		}
		catch (Exception)
		{

		}


	}

	public function genre_odt()
	{

		$this->zip();
	}


	///////////////////function unzip///////////////////////////
	public function unzip()
	{

		$zip = new \dUnzip2($this->CHEMIN_MODEL_ODT);

		// Activate debug
		$zip->debug = false;

		// Unzip all the contents of the zipped file to a new folder called "/var/tmp/templates"
		$zip->getList();
		$newdossier=$this->CHEMIN_TEMPLATE_PDF.$this->namefile.session_id();
		@exec("mkdir ".$newdossier,$array,$resulta);
		@exec("chmod 7777 ".$newdossier,$array,$resulta);
		$this->CHEMIN_TEMPLATE_PDF=$newdossier."/";
		$zip->unzipAll($this->CHEMIN_TEMPLATE_PDF);
	}


	////////////////////////////////////zip
	public function zipDir($path,&$zip,$chemin)
	{

		if (!is_dir($path)){

			return;
		}


		if (!($dh = @opendir($path))) {
			echo("<b>ERREUR: Une erreur s'est produite sur ".$path."</b><br />");
			return;
		}
		while ($file = readdir($dh)) {

			if ($file == "." || $file == "..") continue; // Throw the . and .. folders
			if (is_dir($path."/".$file)) { // Recursive call
				if($chemin!="")
				$this->zipDir($path."/".$file,$zip,$chemin."/".$file);
				else
				$this->zipDir($path."/".$file,$zip,$file);
			} elseif (is_file($path."/".$file)) { // If this is a file then add to the zip file
				if($chemin!="")
				$zip->addFile(file_get_contents($path."/".$file),$chemin."/".$file);
				else
				$zip->addFile(file_get_contents($path."/".$file),$file);
				     //  echo('fichier '.$path.'/'.$file.' ajoute<br>');
			}

		}
	}
	public function zip()
	{
		try
		{
			$fichier_zip =$this->namefile;         // nom du fichier zip que l'on veut
			$zip= new \zipfile();
			$path = $this->CHEMIN_TEMPLATE_PDF;            // repertoire que l'on veut zipper
			$chemin = "";
			set_time_limit (1000);


			$this->zipDir($path,$zip,$chemin);
			$filezipped=$zip->file();       // On recupere le contenu du zip dans la variable $filezipped
				//$open = fopen($fichier_zip, "w");
			$open = fopen($this->CHEMIN_TEMP_PDF.$fichier_zip , "w");
				  // On la sauvegarde dans le meme repertoire que les fichiers a zipper
			fwrite($open, $filezipped);
			fclose($open);


				// @rename($this->CHEMIN_TEMP_PDF.$fichier_zip , "/tmp/".$fichier_zip);
			@rename($this->CHEMIN_TEMP_PDF.$fichier_zip , $this->Tmp.$fichier_zip);
				// $this->file_pdf_name=session_id().$fichier_zip;
					//print TMP.session_id().$fichier_zip;
				// 	$this->erreur=true ;
			$this->Tmp.$fichier_zip;
		}
		catch(Exception)
		{

		}


	}

	public function replace_str1_str2($value){
		$xmlarray=array();
		$NStable="urn:oasis:names:tc:opendocument:xmlns:table:1.0";
		$NStext="urn:oasis:names:tc:opendocument:xmlns:text:1.0";
		$Dom=new DomDocument();

		$Dom->loadXML($this->content);
		
		$listtablerows=$Dom->getElementsByTagNameNS($NStable,'table-row');
		$i=0;
		foreach($listtablerows as $tablerow){
			
			$listtablecells=$tablerow->getElementsByTagNameNS($NStable,'table-cell');
			foreach($listtablecells as $listtablecell){
				$listps=$listtablecell->getElementsByTagNameNS($NStext,'p');
				foreach($listps as $listp ){
					if(strcmp($listp->nodeValue,$value)==0){
							
						$xmlarray["str1"]=$Dom->saveXML($tablerow);
						$xmlarray["str2"]=$Dom->saveXML($tablerow->nextSibling);
						return $xmlarray;
					}
				}
			}
			$i++;
		}
			
			
	}
	public  function  odt_str_replace($champsARemplacer,$valeurs)
	{
		$carac = array('<','>','& ',"'");

		$valCarac = array('&lt;','&gt;','&amp; ','&apos;');
		

		
				//$val = str_replace($carac,$valCarac,$valeurs[$i]);
				//$val = utf8_encode($val);
		$valeurs=($valeurs=="")?"":$valeurs;

		$this->content = str_replace($champsARemplacer,$valeurs,$this->content);


		
	}
	public function odt_Cocher($kay){


		$NS="urn:oasis:names:tc:opendocument:xmlns:form:1.0";
		$Dom=new DomDocument();
		$Dom->loadXML($this->content);
		$listCochers=$Dom->getElementsByTagNameNS($NS,'checkbox');
		foreach($listCochers as $Cocher)
		{
			$name=$Cocher->getAttributeNS($NS,'name');

			if(strcmp($name,$kay)==0){

				$Cocher->setAttributeNS($NS,"current-state", "checked");
			}
		}
		$this->content=$Dom->saveXML();

	}
	public function ods_Cocher($kay){


		$NS="urn:oasis:names:tc:opendocument:xmlns:form:1.0";
		$Dom=new DomDocument();
		$Dom->loadXML(utf8_encode($this->content));
		$listCochers=$Dom->getElementsByTagNameNS($NS,'checkbox');
		foreach($listCochers as $Cocher)
		{
			$name=$Cocher->getAttributeNS($NS,'name');

			if(strcmp($name,$kay)==0){

				$Cocher->setAttributeNS($NS,"current-state", "checked");
			}
		}
		$this->content=utf8_decode($Dom->saveXML());

	}
	
	
	public function genererFilePDF()
	{
		$this->pdfContent = (new PdfGeneratorClient())->genererPdf($this->Tmp.$this->namefile);
	}
	
	public function genererFileDOC()
	{
		
		$pdf=new PDF();
		$pdf->genererFileDOC($this->Tmp.$this->namefile);

			// return true if  is erreur and false is not erreur
	}

	public function send($name,$type=".odt")
	{
		DownloadFile::downloadFileContent($name, $this->pdfContent);
	}
	 
	public function sendDoc($name,$type=".odt")
	{


		$nomFichier =$this->Tmp.$this->namefile.".doc";
		$chemin=$this->Tmp;
	  	//$chemin = $this->CHEMIN_TEMP_PDF;


	  	//$this->response->redirect("?page=Telecharger&fichier=$nomFichier&chemin=$chemin");
		$Telecharger=new Telecharger();
		$Telecharger->telechargerFichier($nomFichier,basename($this->namefile,$type).'.doc');



	}
	public function deletfile()
	{

		$odtfile=$this->Tmp.$this->namefile;
		$pdffile=$this->Tmp.$this->namefile.'.pdf';
		if(is_file($odtfile))unlink($odtfile);
		//  if(is_file($pdffile))unlink($pdffile);


	}
}
?>