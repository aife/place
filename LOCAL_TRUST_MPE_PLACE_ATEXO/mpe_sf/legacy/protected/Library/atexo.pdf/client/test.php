<?php

require_once('../library/Atexo/PdfGeneratorClient.php');
require_once('../library/Atexo/Download/DowloadFile.php');
require_once('../library/Atexo/PdfGenerator/PdfGeneratorClientException.php');
require_once('./config/config.php');



try {
	$pdfContent = PdfGeneratorClient::genererPdf('/tmp/PasseportRencontreCreation.odt');
	DownloadFile::downloadFileContent('test.pdf', $pdfContent);
	

}
catch (Exception $e)
{
	echo ($e->getMessage());
}




