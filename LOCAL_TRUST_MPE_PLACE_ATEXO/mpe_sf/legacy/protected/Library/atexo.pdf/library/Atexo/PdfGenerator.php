<?php
namespace AtexoPdf;

use AtexoPdf\PdfGenerator\PdfGeneratorException;

require_once('../serveur/config/config.php');

class PdfGenerator {

	const log    = '/var/log/openoffice2.log';

	private static function sl($string, $nb_of_characters) {
		return sprintf("%-{$nb_of_characters}s", $string); 
	}

	private static function log(Array $elements) {
		if (is_writable(PdfGenerator::log)) {
			file_put_contents(PdfGenerator::log, date('r') . ' - ' . implode(' - ', $elements) . "\n", FILE_APPEND);
		}
	}

	/**
	 * Generation du PDF via DocumentConverter.py
	 *
	 * @param string $odt chemin du fichier ODT
	 * @param string $pdf chemin du fichier pdf genere
	 * 
	 * @return 0 si OK, != 0 sinon
	 */
	public function genererPdf($odt,$pdf) {
		if($odt && $pdf) {
			$i = 0;
			$ip = self::sl($_SERVER['REMOTE_ADDR'], 15);
			while ($i < 6) {
				@exec(OO_PROGRAM_PATH . '/./python ' . OO_PROGRAM_PATH . '/DocumentConverter.py ' . $odt . ' ' . $pdf, $output, $return_value);
				if (!file_exists($pdf) || (file_exists($pdf) && filesize($pdf) < 204)) {
					$i++;
					sleep(2);
					self::log(array($ip, self::sl($return_value, 3), "Echec #$i", $_FILES['odtFile']['name'], implode(' - ', $output)));
					unset($output);
					continue;
				}
				self::log(array($ip, self::sl($return_value, 3), self::sl(filesize($pdf), 6), $_FILES['odtFile']['name'], implode(' - ', $output)));
				return 0;
			}
			return 1;
		} else {
		 	throw new PdfGeneratorException("Le chemin du fichier .odt ou .pdf est vide");
		}
	}

}

