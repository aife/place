<?php

namespace AtexoPdf;

use Application\Service\Atexo\Atexo_Config;

use AtexoPdf\PdfGenerator\PdfGeneratorClientException;

class RtfGeneratorClient
{
    public const SERVEUR_INDEX = 'http://ooo.local-trust.com/atexo.pdf/serveur/index.php';

    public function genererRtf($odt)
    {
        $proxyUrl = null;
		$atxPdfServeurIndex = null;
		$postData = [];

        $postData['odtFile'] = version_compare(PHP_VERSION, '5.5.0', '<') ? '@' . $odt : new \CURLFile($odt);
        $postData['extFile'] = 'rtf';

        $ch = curl_init();

        //definition du proxy si existe
        $proxyUrl = Atexo_Config::getParameter("ATX_PDF_PROXY_URL");
        $proxyPort = Atexo_Config::getParameter("ATX_PDF_PROXY_PORT");

        if (!empty($proxyUrl)) {
            $curlOptProxy = $proxyUrl . (!empty($proxyPort) ? ":" . $proxyPort : "");
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $atxPdfServeurIndex = Atexo_Config::getParameter('ATX_PDF_SERVEUR_INDEX');
        $serveurUrl = !empty($atxPdfServeurIndex) ? $atxPdfServeurIndex : self::SERVEUR_INDEX;

        curl_setopt($ch, CURLOPT_URL, $serveurUrl);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $postResult = curl_exec($ch);

        if (curl_errno($ch)) {
            throw new PdfGeneratorClientException(curl_error($ch));
        }
        curl_close($ch);
        return $postResult;
    }
}
