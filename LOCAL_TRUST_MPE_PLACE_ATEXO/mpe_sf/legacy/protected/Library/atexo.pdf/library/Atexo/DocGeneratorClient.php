<?php
namespace AtexoPdf;

use Application\Service\Atexo\Atexo_Config;
use AtexoPdf\PdfGenerator\PdfGeneratorClientException;

require_once('client/config/config.php');

class DocGeneratorClient
{
	public function genererDoc($odt)
	{  
		$atxPdfServeurIndex = null;
		$postData = array();
		
		//simulates <input type="file" name="file_name"> 
		$postData[ 'odtFile'] = version_compare(PHP_VERSION, '5.5.0', '<')?'@'.$odt :new CurlFile($odt);
		$postData[ 'extFile'] = 'doc';

		$ch = curl_init();
        //definition du proxy si existe
        if(($proxyUrl = Atexo_Config::getParameter("ATX_PDF_PROXY_URL")) && !empty($proxyUrl)) {
            $curlOptProxy = $proxyUrl.((($proxyPort = Atexo_Config::getParameter("ATX_PDF_PROXY_PORT")) && !empty($proxyPort))?":".$proxyPort:"");
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }
		curl_setopt($ch, CURLOPT_URL, ($atxPdfServeurIndex = Atexo_Config::getParameter("ATX_PDF_SERVEUR_INDEX") && !empty($atxPdfServeurIndex))?$atxPdfServeurIndex:SERVEUR_INDEX );
		curl_setopt($ch, CURLOPT_POST, 1 );
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(version_compare(PHP_VERSION, '5.5.0', '>')) {
            curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
        }
		$postResult = curl_exec($ch);
		
		
		if (curl_errno($ch)) {
			throw new PdfGeneratorClientException(curl_error($ch));
		}
		curl_close($ch);
		return  $postResult;
	}
}

