<?php
namespace AtexoPdf\Download;

class DownloadFile 
{
	
	/**
	 * Donne le fichier en téléchargement
	 *
	 * @param string $nameFile Nom du fichier reçu par le client
	 * @param string $content Contenu du fichier à envoyer au client
	 */
	public static function downloadFileContent($nameFile, $content)
	{
		header('Pragma: ');
		header('Expires: ');
		header('Cache-control: ');
		header('Content-Disposition: attachment; filename='.$nameFile);
		header('Content-Type: application/octet-stream'); // force le DL sauf dans IE
		header('Content-Tranfert-Encoding: binary');
	
		echo ($content);
		exit;
	 		
	}
	
	
	/**
	 * Donne le fichier en téléchargement
	 *
	 * @param string $nameFile Nom du fichier reçu par le client
	 * @param string $pathFile Chemin du fichier à envoyer au client
	 * @param bool $deleteFile Supprimer le fichier ?
	 */
	public static function downloadFilePath($nameFile, $pathFile, $deleteFile=false)
	{
		header('Pragma: ');
		header('Expires: ');
		header('Cache-control: ');
		header('Content-Disposition: attachment; filename='.$nameFile);
		header('Content-Type: application/octet-stream'); // force le DL sauf dans IE
		header('Content-Tranfert-Encoding: binary');
	
		echo file_get_contents($pathFile);
		//FIXME si deleteFile, supprimer le fichier pathFile
		if($deleteFile==true){
			@unlink($pathFile);
		}
		exit;
	 		
	}
}