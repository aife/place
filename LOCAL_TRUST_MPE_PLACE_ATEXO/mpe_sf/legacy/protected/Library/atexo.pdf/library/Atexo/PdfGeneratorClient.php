<?php
namespace AtexoPdf;

use Application\Service\Atexo\Atexo_Config;
use AtexoPdf\PdfGenerator\PdfGeneratorClientException;


class PdfGeneratorClient
{
    public const SERVEUR_INDEX = 'http://ooo.local-trust.com/atexo.pdf/serveur/index.php';

	public function genererPdf($odt)
	{  
		$proxyUrl = null;
		$atxPdfServeurIndex = null;
		$postData = array();
		
		//simulates <input type="file" name="file_name"> 
		$postData[ 'odtFile'] = version_compare(PHP_VERSION, '5.5.0', '<')?'@'.$odt :new \CURLFile($odt);
		$postData[ 'extFile'] = 'pdf';

		$ch = curl_init();
        $proxyUrl = Atexo_Config::getParameter("ATX_PDF_PROXY_URL");
        $proxyPort = Atexo_Config::getParameter("ATX_PDF_PROXY_PORT");
        $atxPdfServeurIndex = !empty(Atexo_Config::getParameter("ATX_PDF_SERVEUR_INDEX"))?Atexo_Config::getParameter("ATX_PDF_SERVEUR_INDEX"):"";
        //definition du proxy si existe
        if (!empty($proxyUrl)) {
            $curlOptProxy = $proxyUrl.(!empty($proxyPort)?":".$proxyPort:"");
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }
        curl_setopt($ch, CURLOPT_URL, $atxPdfServeurIndex);
		curl_setopt($ch, CURLOPT_POST, 1 );
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(version_compare(PHP_VERSION, '5.5.0', '>')) {
            curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
        }
		$postResult = curl_exec($ch);
		
		
		if (curl_errno($ch)) {
			throw new PdfGeneratorClientException(curl_error($ch));
		}
		curl_close($ch);
		return  $postResult;
	}
}

