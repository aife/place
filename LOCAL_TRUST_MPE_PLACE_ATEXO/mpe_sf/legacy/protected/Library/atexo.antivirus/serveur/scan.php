<?php
require_once(__DIR__ . '/../library/Atexo/ScanFile.php');
require_once(__DIR__ . '/../client/config/config.php');

echo (new ScanFile())->processScan($_FILES['file']['tmp_name']);
