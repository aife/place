<?php

require_once('SendFile.php');
require_once(__DIR__ . '/../../client/config/config.php');
class ScanFile{
    public static function runScanCommand($filePath)
    {
        $ret = exec("clamscan " . $filePath . " | grep Infected | awk '{print $3}'");
            return $ret;
    }

    public function scan($fileName)
    {
        $atxAntivirusLocalScan = null;
        $localScan = ($atxAntivirusLocalScan = \Application\Service\Atexo\Atexo_Config::getParameter("ATX_ANTIVIRUS_LOCAL_SCAN") && !empty($atxAntivirusLocalScan))?$atxAntivirusLocalScan:LOCAL_SCAN;
        if('1' == $localScan) {
            $status = self::runScanCommand($fileName);
            return self::scanResult($status);
        }
        if('0' == $localScan) {
            return (new SendFile())->sendFileForScan($fileName);
        }
    }

    public function processScan($fileName) {
        $status = self::runScanCommand($fileName);
        return self::scanResult($status);
    }

    public function scanResult($status)
    {
        if($status == '1')
            $status = 'virus';
        else if($status == '0')
            $status = 'clean';
        else
            $status = 'error';
         
        $resultat = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
                . "<response>\n"
                .       "\t<reply status=\"" . $status . "\" />\n"
                . "</response>";
        return $resultat;
    }
}
