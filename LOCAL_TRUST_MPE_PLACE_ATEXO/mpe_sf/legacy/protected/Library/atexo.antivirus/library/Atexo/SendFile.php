<?php
require_once (__DIR__ . '/../../client/config/config.php');

use Application\Service\Atexo\Atexo_Config;

class SendFile {
    public function sendFileForScan($file, $authentificationForte = false, $cert=null, $pwdCert=null) {
        $atxAntivirusServeurUrl = null;
        $atxAntivirusCurlOptConnectTimeOut = null;
        $proxyPort = null;
        $postfields=array();
        $postfields["file"] = '@' . $file;
    	

        $ch = curl_init();    // initialize curl handle
        curl_setopt($ch, CURLOPT_URL,  ($atxAntivirusServeurUrl = Atexo_Config::getParameter("ATX_ANTIVIRUS_SERVEUR_URL") && !empty($atxAntivirusServeurUrl))?$atxAntivirusServeurUrl:SERVEUR_URL ); // set url to post to
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);

        curl_setopt ($ch, CURLOPT_POST, 1);
        curl_setopt ($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, CURLOPT_CONNECTTIMEOUT);

        if($authentificationForte) {
            curl_setopt($ch,CURLOPT_SSLCERT,$cert);
            curl_setopt($ch,CURLOPT_SSLCERTPASSWD, $pwdCert);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //definition du proxy si existe
        if(($proxyUrl = Atexo_Config::getParameter("ATX_ANTIVIRUS_PROXY_URL")) && !empty($proxyUrl)) {
            $curlOptProxy = $proxyUrl.(($proxyPort = Atexo_Config::getParameter("ATX_ANTIVIRUS_PROXY_PORT") && !empty($proxyPort))?":".$proxyPort:"");
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $result = curl_exec($ch); // run the whole process
        curl_close($ch);
        return $result;
    }
}
