<?php
namespace Application\Library\PhpRules;

use \Prado\Web\UI\TPage;

/**
 * Classe qui permet le calcul d'une règle définit dans un xml
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 * @copyright Atexo 2011
 * @version 0.0
 * @since Atexo.Forpro
 * @package atexo
 * @subpackage atexo
 */

class PhpRules_RulesCalculator extends TPage
{
	private ?array $vars = null;
	private ?array $ids = null;
	/* Tableau de résultat à retourner */
	private array $result=array();
	private string $suffixClassPeer = "Peer";
	private string $prefixClassPeer = "Common";
	private string $fctPeer = "retrieveByPK";
	private ?string $xml = null;
	private array $var_sys = array("CURRENT_DATE_ISO"=>"date('Y-m-d')",
							 "CURRENT_DATE_FR"=>"date('d/m/Y')",
							 "CURRENT_DATETIME_ISO"=>"date('Y-m-d h:i:s')",
							 "CURRENT_DATETIME_FR"=>"date('d/m/Y h:i:s')");
	
	/**
	 * @param string $xml
	 */
	public function setXml($xml) {
		$this->xml = $xml;
	}
	
	/**
	 * @param string $cle
	 * @param array $valeur
	 */
	public function addId($cle,$valeur) {
		$this->ids[$cle] = $valeur;
	}
	
	/**
	 * @param string $cle
	 * @param object $valeur
	 */
	public function addVar($cle,$valeur) {
		$this->vars[$cle] = $valeur;
	}
	
	/**
	 * @param string $xml
	 * @param array $ids
	 * @return array
	 */
	public function getResultFromXmlRules()
	{
		
		$dom = new \DOMDocument();
		$dom->loadXML($this->xml);
		
		/* Tableau de variables */
		$this->getVariables($dom,$this->ids);
		
		$blocConditionsActions=$dom->getElementsByTagName('conditionsActions');
		
		foreach($blocConditionsActions as $conditionsActions)
		{
			$blocConditionAction=$conditionsActions->getElementsByTagName('conditionAction');
			
			foreach($blocConditionAction as $conditionAction)
			{
				if($conditionAction->getAttribute('variable_name')!="") {
					$boucle = true;
					$stringAction = '$varBoucle = '.$this->getElementConditionOrAction($conditionAction).';';
					eval($stringAction);
					
				}
				else {
					$varBoucle = array("1");
					$boucle = false;
				}
				
				foreach($varBoucle as $var) {
					
					if($boucle) {
						$this->vars[$conditionAction->getAttribute('variable_name')] = $var;
					}
					
					$listPrepareConditions=$conditionAction->getElementsByTagName('prepareConditionsActions');
					
					$this->executeActions($listPrepareConditions);
					
					$listConditions=$conditionAction->getElementsByTagName('conditions');
					
					if($this->testCondition($listConditions))
					{
						$listActions=$conditionAction->getElementsByTagName('actions');
						
						$this->executeActions($listActions);
					}
					
				}
			}
		}
		
		return $this->result;
	}
	
	private function getVariables($dom,$ids)
	{	
		$blocVariables=$dom->getElementsByTagName('tables');
		
		foreach($blocVariables as $variables)
		{
			$listVariables=$variables->getElementsByTagName('table');
			
			foreach($listVariables as $variable)
			{
				$table = $variable->getAttribute('name');
				
				if(isset($ids[$table])) {
					$this->vars[$table] = $this->getVarFromTable($table,$ids[$table]);
				}
			}
		}
		
		$blocVariables=$dom->getElementsByTagName('variables');
		
		foreach($blocVariables as $variables)
		{
			$listVariables=$variables->getElementsByTagName('variable');
			
			foreach($listVariables as $variable)
			{
				$table = $variable->getAttribute('name');

				if(isset($ids[$table])) {
					$this->vars[$table] = $this->getVarFromTable($table,$ids[$table]);
				}
			}
		}
	}
	
	private function testCondition($listConditions)
	{
		$test = false;
		
		$stringCondition = '';
		
		foreach($listConditions as $listCondition)
		{
			$conditions=$listCondition->getElementsByTagName('condition');
			
			foreach($conditions as $condition)
			{
				$stringCondition .=$this->construireCondition($condition);
			}
		}
		
		if(!$stringCondition) {
			return true;
		}
		
		$this->addLogCondition($stringCondition);
		
		$stringCondition = "if(".$stringCondition;
		
		$stringCondition .=') $test=true;';
		
		eval('$transformer = new PhpRules_Transformer();'.$stringCondition);
		
		$this->result['log'] .= ($test) ? utf8_encode(" => condition vérifiée <br>\n") : utf8_encode(" => condition non vérifiée <br>");
		
		return $test;
	}
	
	private function executeActions($listActions)
	{
		if(!(is_countable($listActions) ? count($listActions) : 0)){
			return;
		}
		$stringAction = '';
		
		foreach($listActions as $listAction)
		{
			$actions=$listAction->getElementsByTagName('action');
			
			foreach($actions as $action)
			{
				if($action->getAttribute('element_type')=="boucle")
				{
					$stringAction =$this->construireActionBoucle($action);
				}
				elseif($action->getAttribute('element_type')=="valeur")
				{
					$stringAction = '$this->vars["'.$action->getAttribute('variable').'"] = $this->result["'.$action->getAttribute('variable').'"] = ';
					if ('delimiteur' === $action->getAttribute('variable')) {
                        $stringAction .='"' . $action->getAttribute('value') . '"';
                    } else {
                        $stringAction .=$action->getAttribute('value');
                    }
					$stringAction .= ";";
				}
				elseif($action->getAttribute('element_type')=="bdd")
				{
					$stringAction = '$this->vars["'.$action->getAttribute('variable').'"] = $this->result["'.$action->getAttribute('variable').'"] = ';
					$stringAction .= $this->getElementConditionOrAction($action);
					$stringAction .= ";";
				}
				elseif($action->getAttribute('element_type')=="var_boucle")
				{
					$stringAction = '$this->result["'.$action->getAttribute('variable').'"]['.$this->getElementConditionOrActionForBoucle($action,"variable").'] = ';
					$stringAction .= $this->getElementConditionOrActionForBoucle($action,"field");
					$stringAction .= ";";
				}
				elseif($action->getAttribute('element_type')=="system") 
				{
					$stringAction = '$this->vars["'.$action->getAttribute('variable').'"] = $this->result["'.$action->getAttribute('variable').'"] = ';
					$stringAction .= $this->var_sys[$action->getAttribute('element_field')];
					$stringAction .= ";";
				}
				else
				{
					$stringAction = '$this->vars["'.$action->getAttribute('variable').'"] = $this->result["'.$action->getAttribute('variable').'"] = ';
					$stringAction .=$this->construireAction($action);
					$stringAction .= ";";
					
				}
				
				eval('$transformer = new PhpRules_Transformer();'.$stringAction);
				
				$this->addLogAction($stringAction);
			}
		}
		
		//eval($stringActions);
		
		foreach($this->result as $key=>$value) {
			$this->result["log"] = str_replace('$this->result["'.$key.'"]',$value,$this->result["log"]);
		}
	}
	
	/**
	 * Récupère l'objet 
	 * @param string $table
	 * @return object
	 */
	private function getVarFromTable($table,$ids)
	{
		$object = null;
		$classe = $this->getClassNameFromTable($table);
		
		$classePeer =  $this->prefixClassPeer.$classe.$this->suffixClassPeer;
		$function = $this->fctPeer;
		
		$classeObject = new $classePeer();
		
		$object = match (is_countable($ids) ? count($ids) : 0) {
			1 => $classeObject->$function($ids[0]),
			2 => $classeObject->$function($ids[0],$ids[1]),
			3 => $classeObject->$function($ids[0],$ids[1],$ids[2]),
			4 => $classeObject->$function($ids[0],$ids[1],$ids[2],$ids[3]),
			5 => $classeObject->$function($ids[0],$ids[1],$ids[2],$ids[3],$ids[4]),
			default => $object,
		};
		
		return $object;
	}
	
	private function getClassNameFromTable($table)
	{
		return $this->tableOrChampToName($table);
	}
	
	private function getChampNameFromChamp($champ,$element_variable="",$format_field="", $with_param = false)
	{
		$vars = [];
		if($element_variable) {
			$vars=$this->ids[$element_variable];
		}
		if ($with_param !== false) {
			$vars=$this->vars[$element_variable];
		}
		elseif($format_field) {
			$vars="'".$format_field."'";
		}
		return "get".$this->tableOrChampToName($champ)."(".$vars.")";
	}
	
	private function tableOrChampToName($table)
	{
		$names = explode("_",$table);
		
		foreach($names as $name)
		{
			$nameTable .=$name[0].strtolower(substr($name,1));
		}
		
		return $nameTable;
	}
	
	private function construireCondition($condition)
	{
		$andOr=$this->getAndOr($condition->getAttribute('andOr'));
		
		$parentheseO=$condition->getAttribute('parentheseO');
		
		$stringCondition =' '.$andOr.' '.$parentheseO;
		
		$listCondition1=$condition->getElementsByTagName('conditionLeft');
		
		foreach($listCondition1 as $condition1)
		{
			$stringCondition .=$this->getElementConditionOrAction($condition1);
		}
		
		$operator=$condition->getAttribute('operator');
		
		$stringCondition .= ' '.$this->getOperator($operator).' ';
		
		$listCondition2=$condition->getElementsByTagName('conditionRight');
		
		foreach($listCondition2 as $condition2)
		{
			$stringCondition .=$this->getElementConditionOrAction($condition2);
		}
		
		$parentheseF=$condition->getAttribute('parentheseF');
		
		$stringCondition .=$parentheseF;
		
		return $stringCondition;
	}
	
	private function getElementConditionOrAction($element)
	{
		$instr = null;
		$element_table=$element->getAttribute('element_table');
		$element_type=$element->getAttribute('element_type');
		$element_field=$element->getAttribute('element_field');
		$format_field=$element->getAttribute('format_field');
		$element_variable=$element->getAttribute('element_variable');
		$transformer=$element->getAttribute('transformer');
		$variable=$element->getAttribute('variable');
		$with_param = $element->getAttribute('with_param');
		
		switch($element_type)
		{
			case "bdd" : $instr = '$this->vars["'.$element_table.'"]->'.$this->getChampNameFromChamp($element_field,$element_variable,$format_field, $with_param);
						break;
			case "var_boucle" : $instr = '$this->vars["'.$variable.'"]->'.$this->getChampNameFromChamp($element_field,$element_variable,$format_field);
						break;
			case "var" : $instr = '$this->vars["'.$element_field.'"]';
						break;
			case "valeur" : $instr = $element_field;
						break;
			case "system" : $instr = $this->var_sys[$element_field];
						break;
			case "variable" : $instr = '$this->result["'.$element_field.'"]';
						break;
			case "object" : $instr = '$'.$element_table.'->'.$this->getChampNameFromChamp($element_field,$element_variable,$format_field);
						break;
			case "vide" : $instr = 'null';
						break;
		}
		
		if($transformer!="") {
			$instr = $this->construireAppelTransformer($element,$instr);
		}
		
		return $instr;
	}
	
	private function construireAppelTransformer($element, $instr) {
		
		$listVar = array();
		$i=1;
		while($variable=$element->getAttribute('variable'.$i)) {
			$listVar[] = $variable;
			$i++;
		}
		
		$transformer=$element->getAttribute('transformer');
		$instr = '$transformer->'.$transformer.'('.$instr;
		
		foreach($listVar as $var) {
			$instr .= ',$this->result["'.$var.'"]';
		}
		
		return $instr.")";
	}
	
	private function getElementConditionOrActionForBoucle($element,$type="field")
	{
		$value = false;
		$element_table=$element->getAttribute('element_var');
		if($type=="field") {
			$element_field=$element->getAttribute('element_field');
			if($element_field=="") {
				$valeur=$element->getAttribute('value');
				$value = true;
			}
		}
		else {
			$element_field=$element->getAttribute('variable_id');
		}
		$format_field=$element->getAttribute('format_field');
		$element_variable=$element->getAttribute('element_variable');
		$transformer=$element->getAttribute('transformer');
		
		if(!$value) {
			$instr = '$this->vars["'.$element_table.'"]->'.$this->getChampNameFromChamp($element_field,$element_variable,$format_field);
		}
		else {
			$instr = $valeur;
		}
		
		if($transformer!="") {
			$instr = '$transformer->'.$transformer.'('.$instr.')';
		}
		
		return $instr;
	}
	
	private function construireAction($action)
	{
		$stringAction = null;
		$operations=$action->getElementsByTagName('operationLeft');
		
		foreach($operations as $operationLeft)
		{
			$stringAction .=$this->getElementConditionOrAction($operationLeft);
		}
		
		$operator=$action->getAttribute('operator');
		
		$stringAction .= ' '.$this->getOperator($operator).' ';
		
		$operations=$action->getElementsByTagName('operationRight');
		
		foreach($operations as $operationRight)
		{
			$stringAction .=$this->getElementConditionOrAction($operationRight);
		}
		
		return $stringAction;
	}

	private function construireActionBoucle($action)
	{
		$stringAction = '$varBoucle = '.$this->getElementConditionOrAction($action).';';
		
		$stringAction .= 'foreach($varBoucle as $'.$action->getAttribute('element_name').'){';
		
		$stringAction .= '$this->result["'.$action->getAttribute('variable').'"]'.$action->getAttribute('operator_type').'=';
		
		$operations=$action->getElementsByTagName('operationLeft');
		
		foreach($operations as $operationLeft)
		{
			$stringAction .=$this->getElementConditionOrAction($operationLeft);
		}
		
		$operator=$action->getAttribute('operator');
		
		$stringAction .= ' '.$this->getOperator($operator).' ';
		
		$operations=$action->getElementsByTagName('operationRight');
		
		foreach($operations as $operationRight)
		{
			$stringAction .=$this->getElementConditionOrAction($operationRight);
		}
		
		$stringAction .= ';}';
		
		return $stringAction;
	}
	
	private function getAndOr($valeur)
	{
		switch($valeur)
		{
			case "et" : return '&&';
						
			case "ou" : return '||';
		}
	}
	
	private function getOperator($valeur)
	{
		switch($valeur)
		{
			case "sup" : return '>';
			
			case "inf" : return '<';
			
			case "supEgal" : return '>=';
			
			case "infEgal" : return '<=';
			
			case "diff" : return '!=';
			
			case "egale" : return '==';
						
			case "plus" : return '+';
			
			case "moins" : return '-';
			
			case "multi" : return '*';
			
			case "div" : return '/';
			case "concat" : return '.';
		}
	}
	
	private function addLogCondition($condition)
	{
		$vars=array('$this->vars["','()','"]');
		$newCondition = str_replace('"]->get','.',$condition);
		$this->result['log'] .= 'Si ('.str_replace($vars,'',$newCondition).') => (';
		
		$instruction=str_replace(" ","",$condition);
		$instruction=str_replace("->","|",$condition);
		
		$operators=array("&&","||","==","!=",">=","<=",">","<",";");
		$operation=str_replace($operators,"*",$instruction);
		
		$operation=str_replace("|","->",$operation);
		$instruction=str_replace("|","->",$instruction);
		
		$variables = explode("*",$operation);
		
		foreach($variables as $var){
			
			if(trim($var)!="null" && trim($var)!="") {
				eval('$transformer = new PhpRules_Transformer();'.'$varLocal = '.$var.';');
			}
			else {
				$varLocal="null";
			}
			if($varLocal=='') {
				$varLocal=' ';
			}
			$instruction=str_replace($var,"'".$varLocal."' ",$instruction);
		}
		
		$this->result['log'] .= $instruction.')';
	}
	
	private function addLogAction($action)
	{
		$vars=array('$this->vars["','$this->result["','()','"]',";");
		$newAction = str_replace('"]->get','.',$action);
		
		$this->result['log'] .= str_replace($vars,'',$newAction).' = ';
		
		$actions = explode("=",$action);
		
		$action = $actions[1];
		
		$instruction=str_replace(" ","",$action);
		$instruction=str_replace("->","|",$action);
		
		$operators=array("+","-","=","*","/");
		$operation=str_replace($operators,"*",$instruction);
		
		$operation=str_replace("|","->",$operation);
		$instruction=str_replace("|","->",$instruction);
		
		$variables = explode("*",$operation);
		
		foreach($variables as $var){
			
			if(trim($var)!="null") {
				eval('$transformer = new PhpRules_Transformer();'.'$varLocal = '.$var.';');
			}
			else {
				$varLocal="null";
			}
			if($varLocal=='') {
				$varLocal='0';
			}
			$instruction=str_replace($var,$varLocal,$instruction);
		}
		
		$this->result['log'] .= $instruction.' = '.$actions[0].'<br>';
	}
}
