<?php
namespace Application\Library\PhpRules;
use Prado\Web\UI\TPage;

/**
 * Classe qui permet le calcul d'une règle définit dans un xml
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 * @copyright Atexo 2011
 * @version 0.0
 * @since Atexo.Forpro
 * @package atexo
 * @subpackage atexo
 */

class PhpRules_Condition extends TPage
{
	private $operator;
	private $andOr;
	private $dom;
	private $condition;
	
	public function getCondition()
	{
		return $this->condition;
	}
	
	public function setOperator($operator)
	{
		$this->operator=$operator;
	}
	
	public function setAndOr($andOr)
	{
		$this->andOr=$andOr;
	}
	
	public function setCondition($dom)
	{
		$this->dom = $dom;
		
		$this->condition = $this->dom->createElement('condition');
		
		$operator = $this->dom->createAttribute('operator');
		
		$operator = $this->condition->appendChild($operator);
		
		$operator->value=$this->operator;
		
		$andOr = $this->dom->createAttribute('andOr');
		
		$andOr = $this->condition->appendChild($andOr);
		
		$andOr->value=$this->andOr;
	}
	
	public function addConditionLeft($element_type,$element_table,$element_field)
	{
		$this->addCondition("conditionLeft", $element_type,$element_table,$element_field);
	}
	
	public function addConditionRight($element_type,$element_table,$element_field)
	{
		$this->addCondition("conditionRight", $element_type,$element_table,$element_field);
	}
	
	public function addCondition($name, $type,$table,$field)
	{
		$condition = $this->dom->createElement($name);
		
		$element_type = $this->dom->createAttribute('element_type');
		
		$element_type = $condition->appendChild($element_type);
		
		$element_type->value=$type;
		
		$element_table = $this->dom->createAttribute('element_table');
		
		$element_table = $condition->appendChild($element_table);
		
		$element_table->value=$table;
		
		$element_field = $this->dom->createAttribute('element_field');
		
		$element_field = $condition->appendChild($element_field);
		
		$element_field->value=$field;
		
		$this->condition->appendChild($condition);
	}
}
