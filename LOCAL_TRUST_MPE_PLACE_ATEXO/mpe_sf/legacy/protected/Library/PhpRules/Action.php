<?php
use Prado\Web\UI\TPage;

/**
 * Classe qui permet le calcul d'une règle définit dans un xml
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 * @copyright Atexo 2011
 * @version 0.0
 * @since Atexo.Forpro
 * @package atexo
 * @subpackage atexo
 */

class PhpRules_Action extends TPage
{
	private $operator;
	private $dom;
	private $variable;
	private $action;
	
	public function getAction()
	{
		return $this->action;
	}
	
	public function setOperator($operator)
	{
		$this->operator=$operator;
	}
	
	public function setVariable($variable)
	{
		$this->variable=$variable;
	}
	
	public function setAction($dom)
	{
		$this->dom = $dom;
		
		$this->action = $this->dom->createElement('action');
		
		$operator = $this->dom->createAttribute('operator');
		
		$operator = $this->action->appendChild($operator);
		
		$operator->value=$this->operator;
		
		$variable = $this->dom->createAttribute('variable');
		
		$variable = $this->action->appendChild($variable);
		
		$variable->value=$this->variable;
	}
	
	public function addOperationLeft($element_type,$element_table,$element_field)
	{
		$this->addOperation("operationLeft", $element_type,$element_table,$element_field);
	}
	
	public function addOperationRight($element_type,$element_table,$element_field)
	{
		$this->addOperation("operationRight", $element_type,$element_table,$element_field);
	}
	
	public function addOperation($name, $type,$table,$field)
	{
		$action = $this->dom->createElement($name);
		
		$element_type = $this->dom->createAttribute('element_type');
		
		$element_type = $action->appendChild($element_type);
		
		$element_type->value=$type;
		
		$element_table = $this->dom->createAttribute('element_table');
		
		$element_table = $action->appendChild($element_table);
		
		$element_table->value=$table;
		
		$element_field = $this->dom->createAttribute('element_field');
		
		$element_field = $action->appendChild($element_field);
		
		$element_field->value=$field;
		
		$this->action->appendChild($action);
	}
}
