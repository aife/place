<?php

use Application\Service\Atexo\Atexo_Util;
use Prado\Web\UI\TPage;

/**
 * Classe qui permet le calcul d'une règle définit dans un xml
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 * @copyright Atexo 2012
 * @version 0.0
 * @since Atexo.Forpro
 * @package atexo
 * @subpackage atexo
 */

class PhpRules_Transformer extends TPage
{
	public function dateMoins10Jours($date) {
		$util = new Atexo_Util();
		return $util->frnDate2iso($util->dateDansFutur($date,0,-10,0,0));
	}
	
	public function datePost($date,$nbJour) {
		$util = new Atexo_Util();
		return $util->frnDate2iso($util->dateDansFutur($date,0,$nbJour,0,0));
	}
	
	public function dateAnt($date,$nbJour) {
		$util = new Atexo_Util();
		return $util->frnDate2iso($util->dateDansFutur($date,0,-$nbJour,0,0));
	}
	
	public function diffDateEnJour($dateTime1,$dateTime2) {
		$util = new Atexo_Util();
		$date1 = explode(" ", $dateTime1);
		$date2 = explode(" ", $dateTime2);
		return $util->diffJours($util->iso2frnDate($date1[0]),$util->iso2frnDate($date2[0]));
	}
	
	public function getCount($listObject) {
		
		if(!is_array($listObject)) {
			return 0;
		}
		return count($listObject);
	}
	
	public function datePlus1Jours($date) {
		$util = new Atexo_Util();
		return $util->frnDate2iso($util->dateDansFutur($date,0,1,0,0,0));
	}
	public function datePlusXMois($date,$nbMois) {
		$util = new Atexo_Util();
		return $util->frnDate2iso($util->dateDansFutur($date,0,0,$nbMois,0,0));
	}
	
	
	public function isInPeriode($date1,$periodicite,$jour) {
  		
	  $util = new Atexo_Util();
	  $date =  explode(" ", $date1);
	  $date = $util->frnDate2iso($date[0]);
	  $y = "";
	  [$y, $m, $d] = explode("-",$date);
	  $parseDate = $m."-".$d;
	  $nbMois = 12/$periodicite;
	  for($i=0;$i<$periodicite;$i++) {
		   $parseDateDeb = str_pad(($i*$nbMois+1),2,'0',STR_PAD_LEFT)."-01";
		   $dateFin = $util->dateDansFutur($util->frnDate2iso(date("Y")."-".$parseDateDeb),0,$jour,0,0);
		   $yF = "";
		   [$yF, $mF, $dF] = explode("-",$dateFin);
		   $parseDateFin = $mF."-".$dF;
		   
		   if($parseDate>=$parseDateDeb && $parseDate<=$parseDateFin) {
		    	return true;
		   }
	  }
	  
		return false;
	}
	
	public function isInFirstTrimestre($date) {
	  $date1 = explode("-",$date);
	  if($date1[1] == 1 || $date1[1] == 2 || $date1[1] == 3){
	  	return true;
	  } 
	  return false;
	}
	
	public function truncateChaine($chaine,$pos,$lenth) {
		return substr($chaine,$pos,$lenth);
	}

	public function getAnneeFromDate($date)
	{
		$util = new Atexo_Util();
		return $util->getAnneeFromDate2($util->iso2frnDate($date));
	}
}
