<?php
namespace Application\Library\PhpRules;

use \Prado\Web\UI\TPage;

/**
 * Classe qui permet le calcul d'une règle définit dans un xml
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 * @copyright Atexo 2011
 * @version 0.0
 * @since Atexo.Forpro
 * @package atexo
 * @subpackage atexo
 */

class PhpRules_RulesGenerator extends TPage
{
	private \DOMDocument $dom;
	
	/**
	 * @param string $xml
	 * @param array $ids
	 * @return array
	 */
	public function __construct()
	{		
		$this->dom = new \DOMDocument('1.0');
		
		// nous voulons un joli affichage
		$this->dom->formatOutput = true;

		$regle = $this->dom->createElement('regle');
		$regle = $this->dom->appendChild($regle);
		
		$tables = $this->dom->createElement('tables');
		$tables = $regle->appendChild($tables);
		
		$conditionsActions = $this->dom->createElement('conditionsActions');
		$conditionsActions = $regle->appendChild($conditionsActions);
	}
	
	public function getDom()
	{
		return $this->dom;
	}
	
	public function getXml()
	{
		return $this->dom->saveXML();
	}
	
	public function addTable($name)
	{
		$table = $this->dom->createElement('table');
		
		$attr = $this->dom->createAttribute('name');
		
		$attr = $table->appendChild($attr);
		
		$attr->value=$name;
		
		$listTables = $this->dom->getElementsByTagName('tables');
		
		foreach($listTables as $tables)
		{
			$table = $tables->appendChild($table);
		}
	}
	
	public function addConditionAction($conditions,$actions)
	{
		$listConditions = $this->dom->createElement('conditions');
		
		foreach($conditions as $condition)
		{
			$condition = $listConditions->appendChild($condition);
		}
		
		$listActions = $this->dom->createElement('actions');
		
		foreach($actions as $action)
		{
			$action = $listActions->appendChild($action);
		}
		
		$conditionAction = $this->dom->createElement('conditionAction');
		
		$listConditions = $conditionAction->appendChild($listConditions);
		
		$listActions = $conditionAction->appendChild($listActions);
		
		$listConditionsActions = $this->dom->getElementsByTagName('conditionsActions');
		
		foreach($listConditionsActions as $conditionsActions)
		{
			$conditionAction = $conditionsActions->appendChild($conditionAction);
		}
	}
}
