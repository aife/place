<?php

namespace Application\Service\Atexo\RetraitDce;

use Application\Service\Atexo\Atexo_Util;

/**
 * Contient la definition de l'objet RetraitDce.
 *
 * @author ASO : <ayoub.souidahmed@atexo.com>
 * @copyright Atexo 2014
 *
 * @version : 1.0
 *
 * @since : 4.6.0
 */
class Atexo_RetraitDce_RetraitDce
{
    private $_id;
    private ?int $_idEntreprise = null;
    private $_date;
    private $_nomInscrit;
    private $_prenomInscrit;
    private $_rueInscrit;
    private $_codePostalInscrit;
    private $_villeInscrit;
    private $_paysInscrit;
    private $_raisonSocialeInscrit;
    private $_emailInscrit;
    private $_telephoneInscrit;
    private $_faxInscrit;
    private $_siretInscrit;
    private $_raisonSocialEntreprise;
    private $_sirenEntreprise;
    private $_emailEntreprise;
    private $_formeJuridiqueEntreprise;
    private $_codeAPEEntreprise;
    private $_rueEntreprise;
    private $_codePostalEntreprise;
    private $_villeEntreprise;
    private $_paysEntreprise;
    private $_telephoneEntreprise;
    private $_faxEntreprise;
    private $_organisme;

    /*
    * recupere la valeur de l'attribut "Id"
    * @return $this->_id(integer)la valeur de l'attribut "Id"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getId()
    {
        return $this->_id;
    }

    /*
     * affecte la valeur en param a l'attribut "Id"
     * @param  $_id(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setId($_id)
    {
        $this->_id = $_id;
    }

    /**
     * recupere la valeur de l'attribut "_idEntreprise".
     *
     * @return int la valeur de l'attribut "_idEntreprise"
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getIdEntreprise()
    {
        return $this->_idEntreprise;
    }

    /**
     * affecte la valeur en param a l'attribut "_idEntreprise".
     *
     * @param int $_idEntreprise
     *
     * @author  LEZ : <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setIdEntreprise($_idEntreprise)
    {
        $this->_idEntreprise = $_idEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "Date"
    * @return $this->_date(varchar)la valeur de l'attribut "Date"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getDate()
    {
        return Atexo_Util::getDateXmlFromDateTimeVal($this->_date);
    }

    /*
     * affecte la valeur en param a l'attribut "Date"
     * @param  $_date(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setDate($_date)
    {
        $this->_date = $_date;
    }

    /*
    * recupere la valeur de l'attribut "NomInscrit"
    * @return $this->_nomInscrit(varchar)la valeur de l'attribut "NomInscrit"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getNomInscrit()
    {
        return $this->_nomInscrit;
    }

    /*
     * affecte la valeur en param a l'attribut "NomInscrit"
     * @param  $_nomInscrit(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setNomInscrit($_nomInscrit)
    {
        $this->_nomInscrit = $_nomInscrit;
    }

    /*
    * recupere la valeur de l'attribut "PrenomInscrit"
    * @return $this->_prenomInscrit(varchar)la valeur de l'attribut "PrenomInscrit"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getPrenomInscrit()
    {
        return $this->_prenomInscrit;
    }

    /*
     * affecte la valeur en param a l'attribut "PrenomInscrit"
     * @param  $_prenomInscrit(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setPrenomInscrit($_prenomInscrit)
    {
        $this->_prenomInscrit = $_prenomInscrit;
    }

    /*
    * recupere la valeur de l'attribut "RueInscrit"
    * @return $this->_rueInscrit(varchar)la valeur de l'attribut "RueInscrit"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getRueInscrit()
    {
        return $this->_rueInscrit;
    }

    /*
     * affecte la valeur en param a l'attribut "RueInscrit"
     * @param  $_rueInscrit(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setRueInscrit($_rueInscrit)
    {
        $this->_rueInscrit = $_rueInscrit;
    }

    /*
    * recupere la valeur de l'attribut "CodePostalInscrit"
    * @return $this->_codePostalInscrit(varchar)la valeur de l'attribut "CodePostalInscrit"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getCodePostalInscrit()
    {
        return $this->_codePostalInscrit;
    }

    /*
     * affecte la valeur en param a l'attribut "CodePostalInscrit"
     * @param  $_codePostalInscrit(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setCodePostalInscrit($_codePostalInscrit)
    {
        $this->_codePostalInscrit = $_codePostalInscrit;
    }

    /*
    * recupere la valeur de l'attribut "VilleInscrit"
    * @return $this->_villeInscrit(varchar)la valeur de l'attribut "VilleInscrit"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getVilleInscrit()
    {
        return $this->_villeInscrit;
    }

    /*
     * affecte la valeur en param a l'attribut "VilleInscrit"
     * @param  $_villeInscrit(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setVilleInscrit($_villeInscrit)
    {
        $this->_villeInscrit = $_villeInscrit;
    }

    /*
    * recupere la valeur de l'attribut "PaysInscrit"
    * @return $this->_paysInscrit(varchar)la valeur de l'attribut "PaysInscrit"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getPaysInscrit()
    {
        return $this->_paysInscrit;
    }

    /*
     * affecte la valeur en param a l'attribut "PaysInscrit"
     * @param  $_paysInscrit(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setPaysInscrit($_paysInscrit)
    {
        $this->_paysInscrit = $_paysInscrit;
    }

    /*
    * recupere la valeur de l'attribut "RaisonSocialeInscrit"
    * @return $this->_raisonSocialeInscrit(varchar)la valeur de l'attribut "RaisonSocialeInscrit"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getRaisonSocialeInscrit()
    {
        return $this->_raisonSocialeInscrit;
    }

    /*
     * affecte la valeur en param a l'attribut "RaisonSocialeInscrit"
     * @param  $_raisonSocialeInscrit(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setRaisonSocialeInscrit($_raisonSocialeInscrit)
    {
        $this->_raisonSocialeInscrit = $_raisonSocialeInscrit;
    }

    /*
    * recupere la valeur de l'attribut "EmailInscrit"
    * @return $this->_emailInscrit(varchar)la valeur de l'attribut "EmailInscrit"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getEmailInscrit()
    {
        return $this->_emailInscrit;
    }

    /*
     * affecte la valeur en param a l'attribut "EmailInscrit"
     * @param  $_emailInscrit(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setEmailInscrit($_emailInscrit)
    {
        $this->_emailInscrit = $_emailInscrit;
    }

    /*
    * recupere la valeur de l'attribut "TelephoneInscrit"
    * @return $this->_telephoneInscrit(varchar)la valeur de l'attribut "TelephoneInscrit"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getTelephoneInscrit()
    {
        return $this->_telephoneInscrit;
    }

    /*
     * affecte la valeur en param a l'attribut "TelephoneInscrit"
     * @param  $_telephoneInscrit(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setTelephoneInscrit($_telephoneInscrit)
    {
        $this->_telephoneInscrit = $_telephoneInscrit;
    }

    /*
    * recupere la valeur de l'attribut "FaxInscrit"
    * @return $this->_faxInscrit(varchar)la valeur de l'attribut "FaxInscrit"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getFaxInscrit()
    {
        return $this->_faxInscrit;
    }

    /*
     * affecte la valeur en param a l'attribut "FaxInscrit"
     * @param  $_faxInscrit(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setFaxInscrit($_faxInscrit)
    {
        $this->_faxInscrit = $_faxInscrit;
    }

    /*
    * recupere la valeur de l'attribut "SiretInscrit"
    * @return $this->_siretInscrit(varchar)la valeur de l'attribut "SiretInscrit"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getSiretInscrit()
    {
        return $this->_siretInscrit;
    }

    /*
     * affecte la valeur en param a l'attribut "SiretInscrit"
     * @param  $_siretInscrit(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setSiretInscrit($_siretInscrit)
    {
        $this->_siretInscrit = $_siretInscrit;
    }

    /*
    * recupere la valeur de l'attribut "RaisonSocialEntreprise"
    * @return $this->_raisonSocialEntreprise(varchar)la valeur de l'attribut "RaisonSocialEntreprise"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getRaisonSocialEntreprise()
    {
        return $this->_raisonSocialEntreprise;
    }

    /*
     * affecte la valeur en param a l'attribut "RaisonSocialEntreprise"
     * @param  $_raisonSocialEntreprise(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setRaisonSocialEntreprise($_raisonSocialEntreprise)
    {
        $this->_raisonSocialEntreprise = $_raisonSocialEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "SirenEntreprise"
    * @return $this->_sirenEntreprise(varchar)la valeur de l'attribut "SirenEntreprise"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getSirenEntreprise()
    {
        return $this->_sirenEntreprise;
    }

    /*
     * affecte la valeur en param a l'attribut "SirenEntreprise"
     * @param  $_sirenEntreprise(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setSirenEntreprise($_sirenEntreprise)
    {
        $this->_sirenEntreprise = $_sirenEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "EmailEntreprise"
    * @return $this->_emailEntreprise(varchar)la valeur de l'attribut "EmailEntreprise"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getEmailEntreprise()
    {
        return $this->_emailEntreprise;
    }

    /*
     * affecte la valeur en param a l'attribut "EmailEntreprise"
     * @param  $_emailEntreprise(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setEmailEntreprise($_emailEntreprise)
    {
        $this->_emailEntreprise = $_emailEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "FormeJuridiqueEntreprise"
    * @return $this->_formeJuridiqueEntreprise(varchar)la valeur de l'attribut "FormeJuridiqueEntreprise"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getFormeJuridiqueEntreprise()
    {
        return $this->_formeJuridiqueEntreprise;
    }

    /*
     * affecte la valeur en param a l'attribut "FormeJuridiqueEntreprise"
     * @param  $_formeJuridiqueEntreprise(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setFormeJuridiqueEntreprise($_formeJuridiqueEntreprise)
    {
        $this->_formeJuridiqueEntreprise = $_formeJuridiqueEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "CodeAPEEntreprise"
    * @return $this->_codeAPEEntreprise(varchar)la valeur de l'attribut "CodeAPEEntreprise"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getCodeAPEEntreprise()
    {
        return $this->_codeAPEEntreprise;
    }

    /*
     * affecte la valeur en param a l'attribut "CodeAPEEntreprise"
     * @param  $_codeAPEEntreprise(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setCodeAPEEntreprise($_codeAPEEntreprise)
    {
        $this->_codeAPEEntreprise = $_codeAPEEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "RueEntreprise"
    * @return $this->_rueEntreprise(varchar)la valeur de l'attribut "RueEntreprise"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getRueEntreprise()
    {
        return $this->_rueEntreprise;
    }

    /*
     * affecte la valeur en param a l'attribut "RueEntreprise"
     * @param  $_rueEntreprise(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setRueEntreprise($_rueEntreprise)
    {
        $this->_rueEntreprise = $_rueEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "CodePostalEntreprise"
    * @return $this->_codePostalEntreprise(varchar)la valeur de l'attribut "CodePostalEntreprise"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getCodePostalEntreprise()
    {
        return $this->_codePostalEntreprise;
    }

    /*
     * affecte la valeur en param a l'attribut "CodePostalEntreprise"
     * @param  $_codePostalEntreprise(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setCodePostalEntreprise($_codePostalEntreprise)
    {
        $this->_codePostalEntreprise = $_codePostalEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "VilleEntreprise"
    * @return $this->_villeEntreprise(varchar)la valeur de l'attribut "VilleEntreprise"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getVilleEntreprise()
    {
        return $this->_villeEntreprise;
    }

    /*
     * affecte la valeur en param a l'attribut "VilleEntreprise"
     * @param  $_villeEntreprise(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setVilleEntreprise($_villeEntreprise)
    {
        $this->_villeEntreprise = $_villeEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "PaysEntreprise"
    * @return $this->_paysEntreprise(varchar)la valeur de l'attribut "PaysEntreprise"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getPaysEntreprise()
    {
        return $this->_paysEntreprise;
    }

    /*
     * affecte la valeur en param a l'attribut "PaysEntreprise"
     * @param  $_paysEntreprise(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setPaysEntreprise($_paysEntreprise)
    {
        $this->_paysEntreprise = $_paysEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "TelephoneEntreprise"
    * @return $this->_telephoneEntreprise(varchar)la valeur de l'attribut "TelephoneEntreprise"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getTelephoneEntreprise()
    {
        return $this->_telephoneEntreprise;
    }

    /*
     * affecte la valeur en param a l'attribut "TelephoneEntreprise"
     * @param  $_telephoneEntreprise(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setTelephoneEntreprise($_telephoneEntreprise)
    {
        $this->_telephoneEntreprise = $_telephoneEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "FaxEntreprise"
    * @return $this->_faxEntreprise(varchar)la valeur de l'attribut "FaxEntreprise"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getFaxEntreprise()
    {
        return $this->_faxEntreprise;
    }

    /*
     * affecte la valeur en param a l'attribut "FaxEntreprise"
     * @param  $_faxEntreprise(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setFaxEntreprise($_faxEntreprise)
    {
        $this->_faxEntreprise = $_faxEntreprise;
    }

    /*
    * recupere la valeur de l'attribut "$_organisme"
    * @return $this->_organisme (varchar) la valeur de l'attribut "Organisme"
    * @author  YEL : <youssef.elalaoui@atexo.com>
    * @copyright Atexo 2016
    * @version : 1.0
    * @since : 2015-place
    */
    public function getOrganisme()
    {
        return $this->_organisme;
    }

    /*
     * affecte la valeur en param a l'attribut "$_organisme"
     * @param  $organisme(string)
     * @author  YEL : <youssef.elalaoui@atexo.com>
     * @copyright Atexo 2016
     * @version : 1.0
     * @since : 2015-place
     */
    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }
}
