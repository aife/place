<?php

namespace Application\Service\Atexo\RetraitDce;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Propel\Mpe\CommonTelechargementPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;

class Atexo_RetraitDce_TelechargementsDce
{
    /*
     * retourne tableau des objets CommonTelechargement associe a une consultation(ou juste leurs ids si $justIds=true)
     * @param $ref(int) : reference de la consultation
     * @param $org(string) :  l'organisme
     * $justIds(bool) :  pour retourner juste les ids
     * $connexion objet PropelPDO
     * @return (array) : tableau resultat
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     * @version : depuis 4.6.0
     */
    public function getAllRetraitsByConsultation($consultationId, $org = false, $justIds = false, $connexion = false)
    {
        $arrayIds = [];
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonTelechargementPeer::CONSULTATION_ID, $consultationId);
        if ($org) {
            $c->add(CommonTelechargementPeer::ORGANISME, $org);
        }
        $telechargements = CommonTelechargementPeer::doSelect($c, $connexion);
        if ($justIds) {
            foreach ($telechargements as $oneTelechargement) {
                $arrayIds[] = $oneTelechargement->getId();
            }

            return $arrayIds;
        }

        return $telechargements;
    }

    /*
     * retourne un objet CommonTelechargement a partir de son id
     * @param $idTelechargement(int) : id de telechargement
     * @return objet CommonTelechargement
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     * @version : depuis 4.6.0
     */
    public function getTelechargementById($idTelechargement)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonTelechargementPeer::ID, $idTelechargement);
        $telechargementObject = CommonTelechargementPeer::doSelectOne($c, $connexion);

        return $telechargementObject;
    }

    /*
     * retourne un objet Atexo_RetraitDce_RetraitDce remplis des infos
     * @param $idTelechargement(int) : id de telechargement
     * @return objet Atexo_RetraitDce_RetraitDce
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     * @version : depuis 4.6.0
     */
    public function getObjectRetraitDce($idTelechargement)
    {
        $retraitDce = new Atexo_RetraitDce_RetraitDce();
        $telechargementObject = self::getTelechargementById($idTelechargement);
        if ($telechargementObject) {
            $retraitDce = self::fillRetraitDceFromTelechargement($retraitDce, $telechargementObject);
            $idEntreprise = $telechargementObject->getIdEntreprise();
            $entrepriseObj = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise);
            if ($entrepriseObj) {
                $retraitDce = self::fillRetraitDceFromEntreprise($retraitDce, $entrepriseObj);
            }

            return $retraitDce;
        }

        return false;
    }

    /*
     * remplis les infos de Atexo_RetraitDce_RetraitDce depuis $telechargementObject
     * @param Atexo_RetraitDce_RetraitDce $retraitDce
     * @param $telechargementObject (CommonTelechargement)
     * @return objet Atexo_RetraitDce_RetraitDce
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     * @version : depuis 4.6.0
     */
    public function fillRetraitDceFromTelechargement($retraitDce, $telechargementObject)
    {
        $retraitDce->setId($telechargementObject->getId());
        $retraitDce->setIdEntreprise($telechargementObject->getIdEntreprise());
        $retraitDce->setDate($telechargementObject->getDatetelechargement());
        $retraitDce->setNomInscrit($telechargementObject->getNom());
        $retraitDce->setPrenomInscrit($telechargementObject->getPrenom());
        $retraitDce->setRueInscrit($telechargementObject->getAdresse().' '.$telechargementObject->getAdresse2());
        $retraitDce->setCodePostalInscrit($telechargementObject->getCodepostal());
        $retraitDce->setVilleInscrit($telechargementObject->getVille());
        $retraitDce->setPaysInscrit($telechargementObject->getPays());
        $retraitDce->setTelephoneInscrit($telechargementObject->getTelephone());
        $retraitDce->setFaxInscrit($telechargementObject->getFax());
        $retraitDce->setRaisonSocialeInscrit($telechargementObject->getEntreprise());
        $retraitDce->setRaisonSocialEntreprise($telechargementObject->getEntreprise());
        $retraitDce->setSirenEntreprise(substr($telechargementObject->getSiret(), 0, 9));
        $retraitDce->setSiretInscrit($telechargementObject->getSiret());
        // TODO wait for GCH response
        //$retrait->setRaisonSocialeInscrit($telechargementObject->());
        $retraitDce->setEmailInscrit(trim($telechargementObject->getEmail()));
        $retraitDce->setOrganisme($telechargementObject->getOrganisme());

        return $retraitDce;
    }

    /*
     * remplis les infos de Atexo_RetraitDce_RetraitDce depuis $entrepriseObj
     * @param $retraitDce(int) : objet Atexo_RetraitDce_RetraitDce
     * @param $entrepriseObj (Entreprise)
     * @return objet Atexo_RetraitDce_RetraitDce
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     * @version : depuis 4.6.0
     */
    public function fillRetraitDceFromEntreprise($retraitDce, $entrepriseObj)
    {
        $retraitDce->setEmailEntreprise(trim($entrepriseObj->getEmail()));
        $retraitDce->setFormeJuridiqueEntreprise($entrepriseObj->getFormejuridique());
        $retraitDce->setCodeAPEEntreprise($entrepriseObj->getCodeape());
        $retraitDce->setRueEntreprise($entrepriseObj->getAdresse().' '.$entrepriseObj->getAdresse2());
        $retraitDce->setCodePostalEntreprise($entrepriseObj->getCodepostal());
        $retraitDce->setVilleEntreprise($entrepriseObj->getVilleadresse());
        $retraitDce->setPaysEntreprise($entrepriseObj->getPaysadresse());
        $retraitDce->setTelephoneEntreprise($entrepriseObj->getTelephone());
        $retraitDce->setFaxEntreprise($entrepriseObj->getFax());

        return $retraitDce;
    }
}
