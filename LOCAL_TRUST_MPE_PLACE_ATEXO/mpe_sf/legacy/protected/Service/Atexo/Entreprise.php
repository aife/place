<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEntrepriseInfoExercice;
use Application\Propel\Mpe\CommonEntrepriseQuery;
use Application\Propel\Mpe\CommonFormejuridiquePeer;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonResponsableengagementPeer;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Propel\Mpe\EntrepriseQuery;
use Application\Propel\Mpe\Om\BaseEntreprisePeer;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Capacite;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CriteriaVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Exercice;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_HistoriqueSynchroSGMAP;
use Application\Service\Atexo\Entreprise\InterfaceSynchroApiGouvEntreprise;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Leaders;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_SearchLucene;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use DateTime;
use Exception;
use PDO;
use Prado\Prado;

/**
 * Classe de manipulation des entreprises.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise
{
    /**
     * Retourne un objet entreprise.
     *
     * @param $siren
     *
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retrieveCompanyBdeBySiren($siren): bool|\Application\Propel\Mpe\Entreprise
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $c = new Criteria();
        $c->add(EntreprisePeer::SIREN, $siren);
        $company = EntreprisePeer::doSelectOne($c, $connexionCom);

        if ($company) {
            return $company;
        } else {
            return false;
        }
    }

    /**
     * Retourne un objet entrprise identifié par l'idNational.
     *
     * @param $idNational
     * @param $pays
     *
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retrieveCompanyByIdNational($idNational, $pays): bool|\Application\Propel\Mpe\Entreprise
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $c = new Criteria();
        $query = " replace( sirenEtranger, ' ', '' ) LIKE '".str_replace(' ', '', $idNational)."'";
        $c->add(EntreprisePeer::SIRENETRANGER, $query, Criteria::CUSTOM);
        $c->add(EntreprisePeer::PAYSADRESSE, $pays);
        $company = EntreprisePeer::doSelectOne($c, $connexionCom);

        if ($company) {
            return $company;
        } else {
            return false;
        }
    }

    /**
     * @return array
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retierveFormeJuredique()
    {
        $cachedFormeJuridique = Prado::getApplication()->Cache->get('cachedFormeJuridique');

        if (!$cachedFormeJuridique) {
            $connexionCom = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
            );

            $c = new Criteria();
            $c->addAscendingOrderByColumn(CommonFormejuridiquePeer::ORDRE);

            $cachedFormeJuridique = CommonFormejuridiquePeer::doSelect($c, $connexionCom);
            Prado::getApplication()->Cache->set(
                'cachedFormeJuridique',
                $cachedFormeJuridique,
                Atexo_Config::getParameter('TTL_PRADO_CACHE')
            );
        }

        return $cachedFormeJuridique;
    }

    /**
     * Permet de rechercher l objet Formejuridique à partir du sigle (colonne formejuridique en BD).
     *
     * @param $sigle
     *
     * @return bool|mixed
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retrieveFormeJuridiqueBySigle($sigle)
    {
        $allFormeJuridique = self::retierveFormeJuredique();

        foreach ($allFormeJuridique as $item) {
            if ($item->getFormejuridique() == $sigle) {
                return $item;
            }
        }

        return false;
    }

    /**
     * Retourne un objet entreprise.
     *
     * @param $id
     * @param null $connexion
     * @param bool $voElement
     *
     * @return Atexo_Entreprise_EntrepriseVo|bool|Entreprise|mixed
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retrieveCompanyBdeById($id, $connexion = null, $voElement = true)
    {
        if (0 == $id) {
            return false;
        }

        if (!$connexion) {
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
            );
        }

        $c = new Criteria();
        $c->add(EntreprisePeer::ID, $id);
        $entreprise = EntreprisePeer::doSelectOne($c, $connexion);

        if ($entreprise) {
            if ($voElement) {
                $company = $this->getEntrepriseVoFromEntreprise($entreprise);

                if ($company) {
                    return $company;
                }
            }

            return $entreprise;
        }

        return false;
    }

    /**
     * @deprecated use retrieveCompanyBdeById
     * Retourne les informations relatives à l'entreprise
     *
     * @param $idEntreprise
     * @param $connexion
     *
     * @return Atexo_Entreprise_EntrepriseVo|bool|Entreprise|mixed
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public static function getInfoEntreprise($idEntreprise, $connexion)
    {
        return (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise, $connexion);
    }

    /**
     * Retourne les informations relatives à l'entreprise.
     *
     * @param $idEntreprise
     * @param null $connexion
     *
     * @return string
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public static function getNomEntreprise($idEntreprise, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
            );
        }

        $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise, $connexion);

        if ($entreprise) {
            return $entreprise->getNom();
        } else {
            return '';
        }
    }

    /**
     * @return array
     *
     * @throws Atexo_Config_Exception
     * @throws Atexo_Consultation_Exception
     * @throws PropelException
     */
    public function searchCompany(Atexo_Entreprise_CriteriaVo $criteriaVo, $count = false, $etatAdministratifActif = false)
    {
        $arrayIdToFilter = null;
        $idEntrepriseByReferentiel = null;
        $arrayTotalId = null;
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $c = new Criteria();

        if ('' != $criteriaVo->getSiren()) {
            $c->add(EntreprisePeer::SIREN, trim($criteriaVo->getSiren()).'%', Criteria::LIKE);
        }

        if ('' != $criteriaVo->getSiret()) {
            $c->add(EntreprisePeer::NICSIEGE, trim($criteriaVo->getSiret()));
        }

        if ('' != $criteriaVo->getIdNational()) {
            $c->add(EntreprisePeer::SIRENETRANGER, trim($criteriaVo->getIdNational()).'%', Criteria::LIKE);
        }

        if ('' != $criteriaVo->getRaisonSociale()) {
            $c->add(EntreprisePeer::NOM, '%'.$criteriaVo->getRaisonSociale().'%', Criteria::LIKE);
        }

        if ('' != $criteriaVo->getCpSiegeSocial() || '' != $criteriaVo->getVilleSiegeSocial()) {
            $c->addJoin(EntreprisePeer::ID, CommonTEtablissementPeer::ID_ENTREPRISE);
            $c->add(CommonTEtablissementPeer::EST_SIEGE, '1');

            if ('' != $criteriaVo->getCpSiegeSocial()) {
                $c->add(
                    CommonTEtablissementPeer::CODE_POSTAL,
                    trim($criteriaVo->getCpSiegeSocial()).'%',
                    Criteria::LIKE
                );
            }

            if ('' != $criteriaVo->getVilleSiegeSocial()) {
                $c->add(CommonTEtablissementPeer::VILLE, trim($criteriaVo->getVilleSiegeSocial()));
            }
        }

        if ('' != $criteriaVo->getRegion()) {
            $c->add(EntreprisePeer::REGION, $criteriaVo->getRegion());
        }

        if ('' != $criteriaVo->getProvince()) {
            $c->add(EntreprisePeer::PROVINCE, $criteriaVo->getProvince());
        }

        if ('etranger' == $criteriaVo->getPays()) {
            $crit0 = $c->getNewCriterion(EntreprisePeer::SIREN,  '', Criteria::LIKE);
            $crit1 = $c->getNewCriterion(EntreprisePeer::SIREN, null, Criteria::ISNULL);
            $crit0->addOr($crit1);
            $c->addAnd($crit0);
            $c->add(EntreprisePeer::SIRENETRANGER, '', Criteria::NOT_LIKE);

            if ('' != $criteriaVo->getIdNational()) {
                $c->add(EntreprisePeer::SIRENETRANGER, trim($criteriaVo->getIdNational()).'%', Criteria::LIKE);
            }
        } elseif ('local' == $criteriaVo->getPays()) {
            $c->add(EntreprisePeer::SIREN, '', Criteria::NOT_LIKE);
            $crit0 = $c->getNewCriterion(EntreprisePeer::SIRENETRANGER,  '', Criteria::LIKE);
            $crit1 = $c->getNewCriterion(EntreprisePeer::SIRENETRANGER, null, Criteria::ISNULL);
            $crit0->addOr($crit1);
            $c->addAnd($crit0);

            if ('' != $criteriaVo->getSiren()) {
                $c->add(EntreprisePeer::SIREN, trim($criteriaVo->getSiren()).'%', Criteria::LIKE);
            }

            if ('' != $criteriaVo->getSiret()) {
                $c->add(EntreprisePeer::NICSIEGE, trim($criteriaVo->getSiret()));
            }
        } elseif ('' != $criteriaVo->getPays()) {
            $pays = explode('-', $criteriaVo->getPays());
            $c->add(EntreprisePeer::PAYSADRESSE, trim($pays[0]), Criteria::LIKE);

            if ('' != $criteriaVo->getIdNational()) {
                $c->add(EntreprisePeer::SIRENETRANGER, trim($criteriaVo->getIdNational()).'%', Criteria::LIKE);
            }
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseCodeApe')) {
            if ('' != $criteriaVo->getApeNafNace()) {
                $c->add(EntreprisePeer::CODEAPE, trim($criteriaVo->getApeNafNace()).'%', Criteria::LIKE);
            }
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {
            $listeCodesNace = explode('#', $criteriaVo->getApeNafNace());

            if ($listeCodesNace[1]) {
                $c->add(EntreprisePeer::CODEAPE, '#'.$listeCodesNace[1].'%', Criteria::LIKE);

                foreach ($listeCodesNace as $unCodeNace) {
                    if ($unCodeNace && $unCodeNace != $listeCodesNace[1]) {
                        $c->addOr(EntreprisePeer::CODEAPE, '#'.$unCodeNace.'%', Criteria::LIKE);
                    }
                }
            }
        }

        if (Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel')) {
            $listeDomainesActivites = explode('#', $criteriaVo->getDomaineActivite());

            if ($listeDomainesActivites[1]) {
                $c->add(
                    EntreprisePeer::DOMAINES_ACTIVITES,
                    '%#'.rtrim($listeDomainesActivites[1], ' 0').'%',
                    Criteria::LIKE
                );

                while (strlen(rtrim($listeDomainesActivites[1], '0')) > Atexo_Config::getParameter('NOMBRE_MINIMUM_CARACTERE_ASCENDANT_DIRECT_DOMAINE_ACTIVITE')) {
                    $c->addOr(
                        EntreprisePeer::DOMAINES_ACTIVITES,
                        '%#'.self::RechercheAscendantDomaineActivite($listeDomainesActivites[1]).'#%',
                        Criteria::LIKE
                    );

                    $c->addOr(
                        EntreprisePeer::DOMAINES_ACTIVITES,
                        '%#'.self::RechercheAscendantDomaineActivite($listeDomainesActivites[1]),
                        Criteria::LIKE
                    );

                    $listeDomainesActivites[1] = self::RechercheAscendantDomaineActivite($listeDomainesActivites[1]);
                }

                foreach ($listeDomainesActivites as $domaineActivite) {
                    if ($domaineActivite && $domaineActivite != $listeDomainesActivites[1]) {
                        $c->addOr(
                            EntreprisePeer::DOMAINES_ACTIVITES,
                            '%#'.rtrim($domaineActivite, ' 0').'%',
                            Criteria::LIKE
                        );

                        while (strlen(rtrim($domaineActivite, '0')) > Atexo_Config::getParameter('NOMBRE_MINIMUM_CARACTERE_ASCENDANT_DIRECT_DOMAINE_ACTIVITE')) {
                            $c->addOr(
                                EntreprisePeer::DOMAINES_ACTIVITES,
                                '%#'.self::RechercheAscendantDomaineActivite($domaineActivite).'#%',
                                Criteria::LIKE
                            );

                            $c->addOr(
                                EntreprisePeer::DOMAINES_ACTIVITES,
                                '%#'.self::RechercheAscendantDomaineActivite($domaineActivite),
                                Criteria::LIKE
                            );

                            $domaineActivite = self::RechercheAscendantDomaineActivite($domaineActivite);
                        }
                    }
                }
            }
        }

        if ('' != $criteriaVo->getKeywordSearche()) {
            if (Atexo_Module::isEnabled('UtiliserLucene')) {
                // Utilisation de Lucene :
                $keyWord = $criteriaVo->getKeywordSearche();
                $searchLucene = new Atexo_Entreprise_SearchLucene();
                $arrayIdToFilter = $searchLucene->search(
                    $keyWord,
                    Atexo_Config::getParameter('PRECISION_LUCENE_ENTREPRISE')
                );
            } else {
                $arrayMotsCle = explode(' ', $criteriaVo->getKeywordSearche());
                $i = 0;
                $criterions = [];
                foreach ($arrayMotsCle as $motCle) {
                    $criterions [$i++] = $c->getNewCriterion(
                        EntreprisePeer::NOM, '%' . $motCle .  '%', CRITERIA::LIKE
                    );
                    $criterions [$i++] = $c->getNewCriterion(
                        EntreprisePeer::DESCRIPTION_ACTIVITE, '%' . $motCle . '%', CRITERIA::LIKE
                    );
                    $criterions [$i++] = $c->getNewCriterion(
                        EntreprisePeer::ACTIVITE_DOMAINE_DEFENSE, '%' . $motCle . '%', CRITERIA::LIKE
                    );
                }
                while ($i > 1) {
                    $criterions[0]->addOr($criterions[--$i]);
                }
                $c->addAnd($criterions[0]);
            }
        }

        if ($criteriaVo->getReferentielVo()) {
            $idEntrepriseByReferentiel = (new Atexo_Referentiel_Referentiel())->searchEntreprise($criteriaVo->getReferentielVo());
        }

        if (is_array($arrayIdToFilter)) {
            $arrayTotalId = $arrayIdToFilter;

            if (is_array($criteriaVo->getIdsEntrepriseIn()) && count($criteriaVo->getIdsEntrepriseIn())) {
                $arrayTotalId = array_intersect($arrayTotalId, $criteriaVo->getIdsEntrepriseIn());
            }

            if (is_array($idEntrepriseByReferentiel)) {
                $arrayTotalId = array_intersect($arrayTotalId, $idEntrepriseByReferentiel);
            }
        } elseif (is_array($criteriaVo->getIdsEntrepriseIn()) && count($criteriaVo->getIdsEntrepriseIn())) {
            $arrayTotalId = $criteriaVo->getIdsEntrepriseIn();

            if (is_array($idEntrepriseByReferentiel)) {
                $arrayTotalId = array_intersect($arrayTotalId, $idEntrepriseByReferentiel);
            }
        } else {
            if (is_array($idEntrepriseByReferentiel)) {
                $arrayTotalId = $idEntrepriseByReferentiel;
            }
        }

        if (is_array($arrayTotalId)) {
            $c->add(EntreprisePeer::ID, $arrayTotalId, Criteria::IN);
        }

        if (Atexo_Module::isEnabled('compteEntrepriseDomaineActivite')) {
            if ('' != $criteriaVo->getDomaineActivite()) {
                $idsDomaines = explode('#', $criteriaVo->getDomaineActivite());

                foreach ($idsDomaines as $oneID) {
                    if ($oneID) {
                        $listeIds = '%#'.$oneID.'%';
                        $c->addOr(EntreprisePeer::DOMAINES_ACTIVITES, $listeIds, Criteria::LIKE);
                    }
                }
            }
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseQualification')) {
            if ('' != $criteriaVo->getQualification()) {
                $idsQualif = explode('#', $criteriaVo->getQualification());

                foreach ($idsQualif as $oneID) {
                    if ($oneID) {
                        $listeIds = '%#'.$oneID.'%';
                        $c->addOr(EntreprisePeer::QUALIFICATION, $listeIds, Criteria::LIKE);
                    }
                }
            }
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel')) {
            $listeQualifications = explode('#', $criteriaVo->getQualification());

            if ($listeQualifications[1]) {
                $c->add(
                    EntreprisePeer::QUALIFICATION,
                    '%#'.rtrim($listeQualifications[1], ' 0').'%',
                    Criteria::LIKE
                );

                foreach ($listeQualifications as $qualification) {
                    if ($qualification && $qualification != $listeQualifications[1]) {
                        $c->addOr(
                            EntreprisePeer::QUALIFICATION,
                            '%#'.rtrim($qualification, ' 0').'%',
                            Criteria::LIKE
                        );
                    }
                }
            }
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseAgrement')) {
            if ('' != $criteriaVo->getAgrements()) {
                $idsAgrements = explode(',', $criteriaVo->getAgrements());

                foreach ($idsAgrements as $oneID) {
                    if ($oneID) {
                        $listeIds = '%,'.$oneID.',%';
                        $c->addOr(EntreprisePeer::AGREMENT, $listeIds, Criteria::LIKE);
                    }
                }
            }
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel')) {
            $listeCertificats = explode('#', $criteriaVo->getAgrements());

            if ($listeCertificats[1]) {
                $c->add(
                    EntreprisePeer::AGREMENT,
                    '%#'.rtrim($listeCertificats[1], ' 0').'%',
                    Criteria::LIKE
                );

                foreach ($listeCertificats as $certificats) {
                    if ($certificats && $certificats != $listeCertificats[1]) {
                        $c->addOr(
                            EntreprisePeer::AGREMENT,
                            '%#'.rtrim($certificats, ' 0').'%',
                            Criteria::LIKE
                        );
                    }
                }
            }
        }

        if ('' != $criteriaVo->getDateCreationEnd()) {
            $c->addAnd(
                EntreprisePeer::DATE_CREATION,
                Atexo_Util::frnDate2iso($criteriaVo->getDateCreationEnd()).' 23:59:00',
                Criteria::LESS_EQUAL
            );
        }

        if ('' != $criteriaVo->getDateCreationStart()) {
            $c->addAnd(
                EntreprisePeer::DATE_CREATION,
                Atexo_Util::frnDate2iso($criteriaVo->getDateCreationStart()).' 00:00:00',
                Criteria::GREATER_EQUAL
            );
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseActivationInscriptionParAgent')) {
            if ('0' != $criteriaVo->getCompteActif()) {
                $c->add(EntreprisePeer::COMPTE_ACTIF, $criteriaVo->getCompteActif());
            }
        }

        if (Atexo_Module::isEnabled('BourseALaSousTraitance')) {
            if ($criteriaVo->getTypeCollaboration()) {
                $typeCollaboration = explode('#', $criteriaVo->getTypeCollaboration());

                if ($typeCollaboration[1]) {
                    $c->add(
                        EntreprisePeer::TYPE_COLLABORATION,
                        '%#'.$typeCollaboration[1].'#%',
                        Criteria::LIKE
                    );

                    foreach ($typeCollaboration as $oneID) {
                        if ($oneID && $oneID != $typeCollaboration[1]) {
                            $listeIds = '%#'.$oneID.'#%';
                            $c->addOr(EntreprisePeer::TYPE_COLLABORATION, $listeIds, Criteria::LIKE);
                        }
                    }
                }
            }

            if ($criteriaVo->getVisibleBourse()) {
                $c->add(EntreprisePeer::VISIBLE_BOURSE, $criteriaVo->getVisibleBourse(), Criteria::LIKE);
            }
        }

        if (Atexo_Module::isEnabled('StatutCompteEntreprise')) {
            if ($criteriaVo->getEntrepriseEA() && $criteriaVo->getEntrepriseSIAE()) {
                $crit0 = $c->getNewCriterion(EntreprisePeer::ENTREPRISE_EA, '1');
                $crit1 = $c->getNewCriterion(EntreprisePeer::ENTREPRISE_SIAE, '1');
                $crit0->addOr($crit1);
                $c->addAnd($crit0);
            } else {
                if ($criteriaVo->getEntrepriseEA()) {
                    $c->add(EntreprisePeer::ENTREPRISE_EA, '1');
                } elseif ($criteriaVo->getEntrepriseSIAE()) {
                    $c->add(EntreprisePeer::ENTREPRISE_SIAE, '1');
                }
            }
        }

        if ($etatAdministratifActif == true) {
            $crit0 = $c->getNewCriterion(EntreprisePeer::ETAT_ADMINISTRATIF, 'A');
            $crit1 = $c->getNewCriterion(EntreprisePeer::ETAT_ADMINISTRATIF, null, Criteria::ISNULL);
            $crit0->addOr($crit1);
            $c->addAnd($crit0);
        }

        if ('NOM' == $criteriaVo->getSortByElement()) {
            if ('DESC' == $criteriaVo->getSensOrderBy()) {
                $c->addDescendingOrderByColumn(EntreprisePeer::NOM);
            } else {
                $c->addAscendingOrderByColumn(EntreprisePeer::NOM);
            }
        } elseif ('CODEAPE' == $criteriaVo->getSortByElement()) {
            if ('DESC' == $criteriaVo->getSensOrderBy()) {
                $c->addDescendingOrderByColumn(EntreprisePeer::CODEAPE);
            } else {
                $c->addAscendingOrderByColumn(EntreprisePeer::CODEAPE);
            }
        } elseif ('PAYSADRESSE' == $criteriaVo->getSortByElement()) {
            if ('DESC' == $criteriaVo->getSensOrderBy()) {
                $c->addDescendingOrderByColumn(EntreprisePeer::PAYSADRESSE);
            } else {
                $c->addAscendingOrderByColumn(EntreprisePeer::PAYSADRESSE);
            }
        } elseif ('CODEPOSTAL' == $criteriaVo->getSortByElement()) {
            if ('DESC' == $criteriaVo->getSensOrderBy()) {
                $c->addDescendingOrderByColumn(EntreprisePeer::CODEPOSTAL);
            } else {
                $c->addAscendingOrderByColumn(EntreprisePeer::CODEPOSTAL);
            }
        } elseif ('VILLEADRESSE' == $criteriaVo->getSortByElement()) {
            if ('DESC' == $criteriaVo->getSensOrderBy()) {
                $c->addDescendingOrderByColumn(EntreprisePeer::VILLEADRESSE);
            } else {
                $c->addAscendingOrderByColumn(EntreprisePeer::VILLEADRESSE);
            }
        } elseif ('REGION' == $criteriaVo->getSortByElement()) {
            if ('DESC' == $criteriaVo->getSensOrderBy()) {
                $c->addDescendingOrderByColumn(EntreprisePeer::REGION);
            } else {
                $c->addAscendingOrderByColumn(EntreprisePeer::REGION);
            }
        } elseif ('PROVINCE' == $criteriaVo->getSortByElement()) {
            if ('DESC' == $criteriaVo->getSensOrderBy()) {
                $c->addDescendingOrderByColumn(EntreprisePeer::PROVINCE);
            } else {
                $c->addAscendingOrderByColumn(EntreprisePeer::PROVINCE);
            }
        }

        if ('0' != $criteriaVo->getLimit()) {
            $c->setLimit($criteriaVo->getLimit());
            $c->setOffset($criteriaVo->getOffset());
        }

        if ($count) {
            return EntreprisePeer::doCount($c, $connexion);
        }
        $result = EntreprisePeer::doSelect($c, $connexion);

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    /**
     * Retourne l'entreprise selon l'id de l'offre et de la reference de la consultation.
     *
     * @param $idOffre
     * @param $consultationRef
     *
     * @return array|Atexo_Entreprise_EntrepriseVo|bool|Entreprise|mixed
     *
     * @throws Atexo_Config_Exception
     * @throws Atexo_Consultation_Exception
     * @throws PropelException
     */
    public function getEntrepriseByIdOffre($idOffre, $consultationId)
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $organisme = Atexo_CurrentUser::getCurrentOrganism();

        $c = new Criteria();
        $c->add(CommonOffresPeer::ID, $idOffre);
        $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonOffresPeer::ORGANISME, $organisme);
        $offre = CommonOffresPeer::doSelectOne($c, $connexionCom);

        $c1 = new Criteria();
        $c1->add(CommonOffrePapierPeer::ID, $idOffre);
        $c1->add(CommonOffrePapierPeer::CONSULTATION_ID, $consultationId);
        $c1->add(CommonOffrePapierPeer::ORGANISME, $organisme);
        $offrePapier = CommonOffrePapierPeer::doSelectOne($c1, $connexionCom);

        if ($offre instanceof CommonOffres) {
            // Offre electronique
            $idEntreprise = $offre->getEntrepriseId();
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise, $connexionCom);

            if ($entreprise) {
                return $entreprise;
            } else {
                return false;
            }
        } elseif ($offrePapier instanceof CommonOffrePapier) {
            // Offre papier
            $CriteriaVo = new Atexo_Entreprise_CriteriaVo();
            $CriteriaVo->setRaisonSociale($offrePapier->getNomEntreprise());
            $entreprise = self::searchCompany($CriteriaVo);

            if ($entreprise) {
                return $entreprise;
            } else {
                return false;
            }

            return false;
        } else {
            return false;
        }
    }

    /**
     * Mettre les infos récupèrer depuis le web service dans les champs de la table entreprise du projet associé.
     *
     * @param $entreprise
     * @param $wsEntreprise
     */
    public static function remplirEntreprise($entreprise, $wsEntreprise)
    {
        $entreprise->setIdInitial($wsEntreprise['id']);
        $entreprise->setAdminId($wsEntreprise['admin_id']);
        $entreprise->setSiren($wsEntreprise['siren']);
        $entreprise->setRepmetiers($wsEntreprise['repmetiers']);
        $entreprise->setNom(utf8_decode($wsEntreprise['nom']));

        if (isset($wsEntreprise['nom_entreprise']) && '' != $wsEntreprise['nom_entreprise']) {
            $entreprise->setNom(utf8_decode($wsEntreprise['nom_entreprise']));
        }

        $entreprise->setAdresse(utf8_decode($wsEntreprise['adresse']));

        if (isset($wsEntreprise['adresse_entreprise']) && '' != $wsEntreprise['adresse_entreprise']) {
            $entreprise->setAdresse(utf8_decode($wsEntreprise['adresse_entreprise']));
        }

        $entreprise->setCodePostal($wsEntreprise['codepostal']);

        if (isset($wsEntreprise['codepostal_entreprise']) && '' != $wsEntreprise['codepostal_entreprise']) {
            $entreprise->setCodePostal($wsEntreprise['codepostal_entreprise']);
        }

        $entreprise->setVilleadresse(utf8_decode($wsEntreprise['villeadresse']));
        $entreprise->setPaysadresse(utf8_decode($wsEntreprise['paysadresse']));
        $entreprise->setEmail(trim($wsEntreprise['email']));

        if (isset($wsEntreprise['email_entreprise']) && '' != $wsEntreprise['email_entreprise']) {
            $entreprise->setEmail($wsEntreprise['email']);
        }

        $entreprise->setFormejuridique(utf8_decode($wsEntreprise['formejuridique']));
        $entreprise->setVilleenregistrement(utf8_decode($wsEntreprise['villeenregistrement']));
        $entreprise->setMotifNonIndNum(utf8_decode($wsEntreprise['motifNonIndNum']));
        $entreprise->setOrdreProfOuAgrement($wsEntreprise['ordreProfOuAgrement']);
        $entreprise->setDateConstSociete($wsEntreprise['dateConstSociete']);
        $entreprise->setNomOrgInscription(utf8_decode($wsEntreprise['nomOrgInscription']));
        $entreprise->setAdrOrgInscription(utf8_decode($wsEntreprise['adrOrgInscription']));
        $entreprise->setDateConstAssoc($wsEntreprise['dateConstAssoc']);
        $entreprise->setDateConstAssocEtrangere($wsEntreprise['dateConstAssocEtrangere']);
        $entreprise->setNomPersonnePublique(utf8_decode($wsEntreprise['nomPersonnePublique']));
        $entreprise->setNationalite(utf8_decode($wsEntreprise['nationalite']));
        $entreprise->setRedressement($wsEntreprise['redressement']);
        $entreprise->setPaysenregistrement(utf8_decode($wsEntreprise['paysenregistrement']));
        $entreprise->setSirenEtranger($wsEntreprise['sirenEtranger']);
        $entreprise->setNumAssoEtrangere($wsEntreprise['numAssoEtrangere']);
        $entreprise->setDebutExerciceGlob1($wsEntreprise['debutExerciceGlob1']);
        $entreprise->setFinExerciceGlob1($wsEntreprise['finExerciceGlob1']);
        $entreprise->setDebutExerciceGlob2($wsEntreprise['debutExerciceGlob2']);
        $entreprise->setFinExerciceGlob2($wsEntreprise['finExerciceGlob2']);
        $entreprise->setDebutExerciceGlob3($wsEntreprise['debutExerciceGlob3']);
        $entreprise->setFinExerciceGlob3($wsEntreprise['finExerciceGlob3']);
        $entreprise->setVentesGlob1($wsEntreprise['ventesGlob1']);
        $entreprise->setVentesGlob2($wsEntreprise['ventesGlob2']);
        $entreprise->setVentesGlob3($wsEntreprise['ventesGlob3']);
        $entreprise->setBiensGlob1($wsEntreprise['biensGlob1']);
        $entreprise->setBiensGlob2($wsEntreprise['biensGlob2']);
        $entreprise->setBiensGlob3($wsEntreprise['biensGlob3']);
        $entreprise->setServicesGlob1($wsEntreprise['servicesGlob1']);
        $entreprise->setServicesGlob2($wsEntreprise['servicesGlob2']);
        $entreprise->setServicesGlob3($wsEntreprise['servicesGlob3']);
        $entreprise->setTotalGlob1($wsEntreprise['totalGlob1']);
        $entreprise->setTotalGlob2($wsEntreprise['totalGlob2']);
        $entreprise->setTotalGlob3($wsEntreprise['totalGlob3']);
        $entreprise->setCodeape($wsEntreprise['code_naf']);
        $entreprise->setNicsiege($wsEntreprise['code_siege']);
        $entreprise->setSiteInternet($wsEntreprise['site_web']);
        $entreprise->setDescriptionActivite(utf8_decode($wsEntreprise['descriptif_activite']));
    }

    /**
     * Retourne les entreprises des inscrits de l'annuraire defonse.
     *
     * @param null $fromDate
     * @param int  $defensePortalRegistered
     *
     * @return array
     *
     * @throws Atexo_Config_Exception
     */
    public function retreiveEntreprises($fromDate = null, $defensePortalRegistered = 0)
    {
        $queryParams = [];

        $querySelect = ' SELECT DISTINCT Entreprise.ID, Entreprise.SIREN,Entreprise.NOM, Entreprise.ADRESSE, Entreprise.CODEPOSTAL,
         					Entreprise.VILLEADRESSE, Entreprise.PAYSADRESSE, Entreprise.EMAIL, Entreprise.TAILLE, 
         					Entreprise.FORMEJURIDIQUE, Entreprise.VILLEENREGISTREMENT, Entreprise.MOTIFNONINDNUM, 
         					Entreprise.PAYSENREGISTREMENT,Entreprise.SIRENETRANGER, Entreprise.NUMASSOETRANGERE,
         				    Entreprise.CODEAPE,Entreprise.TELEPHONE, Entreprise.FAX, Entreprise.SITE_INTERNET,
         					Entreprise.DESCRIPTION_ACTIVITE, Entreprise.ACTIVITE_DOMAINE_DEFENSE,Entreprise.SITE_INTERNET,Entreprise.description_activite,
         					Entreprise.ADRESSE2, Entreprise.NICSIEGE, Entreprise.ACRONYME_PAYS,
         					Entreprise.pme1, Entreprise.pme2, Entreprise.pme3,
         					Entreprise.effectif_encadrement1, Entreprise.effectif_encadrement2, Entreprise.effectif_encadrement3,
         					Entreprise.effectif_moyen1, Entreprise.effectif_moyen2, Entreprise.effectif_moyen3,
         					Entreprise.totalGlob1, Entreprise.totalGlob2, Entreprise.totalGlob3,
         					Entreprise.servicesGlob1, Entreprise.servicesGlob2, Entreprise.servicesGlob3,
         					Entreprise.biensGlob1, Entreprise.biensGlob2, Entreprise.biensGlob3,
         					Entreprise.ventesGlob1, Entreprise.ventesGlob2, Entreprise.ventesGlob3,
         					Entreprise.finExerciceGlob1, Entreprise.finExerciceGlob2, Entreprise.finExerciceGlob3,
         					Entreprise.debutExerciceGlob1, Entreprise.debutExerciceGlob2, Entreprise.debutExerciceGlob3,
         					Entreprise.annee_cloture_exercice1, Entreprise.annee_cloture_exercice2, Entreprise.annee_cloture_exercice3,
         					Entreprise.DATE_CREATION, Entreprise.DATE_MODIFICATION, Entreprise.ID_INITIAL
         					FROM Entreprise  ';

        if ($defensePortalRegistered) {
            $querySelect .= " , t_etablissement,Inscrit WHERE t_etablissement.id_entreprise=Entreprise.ID AND t_etablissement.inscrit_annuaire_defense='1' and Inscrit.entreprise_id=t_etablissement.id_entreprise and Inscrit.id_etablissement=t_etablissement.id_etablissement";
        } else {
            $querySelect .= ' WHERE 1 ';
        }

        if ($fromDate) {
            $querySelect .= ' AND Entreprise.DATE_MODIFICATION > :fromDate';
            $queryParams[':fromDate'] = Atexo_Util::iso860TimeToIso($fromDate);
        }

        if (Atexo_Config::getParameter('TEST_SOCLE_PPP')) { // Juste pour les testes Unitaires
            $querySelect .= ' LIMIT 0 , 100';
        }

        $statement = Atexo_Db::getLinkCommon(true)->prepare($querySelect);
        $results = $statement->execute($queryParams);
        $arrayResult = [];

        if ($results) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $arrayResult[] = $row;
            }
        }

        return $arrayResult;
    }

    /**
     * Retourne les entreprises des inscrits de l'annuraire défense.
     *
     * @param null $fromDate
     *
     * @return array
     *
     * @throws Atexo_Config_Exception
     */
    public function retreiveEntrepriseAnnuaireDefense($fromDate = null)
    {
        return self::retreiveEntreprises($fromDate, 1);
    }

    /**
     * Retourne la liste des dirigents d'une entreprise.
     *
     * @param $idEntreprise
     *
     * @return array
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retreiveDirigentsCompany($idEntreprise)
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $c = new Criteria();
        $c->add(CommonResponsableengagementPeer::ENTREPRISE_ID, $idEntreprise);
        $result = CommonResponsableengagementPeer::doSelect($c, $connexionCom);

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    /**
     * Retourne toutes les entreprises.
     *
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public static function retrieveAllCompany(): array|bool
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $c = new Criteria();
        $company = EntreprisePeer::doSelect($c, $connexionCom);

        if ($company) {
            return $company;
        } else {
            return false;
        }
    }

    /**
     * @param $siret
     *
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retrieveCompanyBdeBySiret($siret): bool|\Application\Propel\Mpe\Entreprise
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $c = new Criteria();
        $c->add(EntreprisePeer::SIREN, substr($siret, 0, 9));
        $c->add(EntreprisePeer::NICSIEGE, (substr($siret, 9)));
        $company = EntreprisePeer::doSelectOne($c, $connexionCom);

        if ($company) {
            return $company;
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public static function retrieveIdExterneIdEntrprise()
    {
        $querySelect = 'SELECT DISTINCT id ,id_initial from Entreprise';
        $results = Atexo_Db::getLinkCommon(true)->query($querySelect);

        $arrayResult = [];

        if ($results) {
            foreach ($results as $row) {
                if ($row['id_initial']) {
                    $arrayResult[$row['id_initial']] = $row['id'];
                }
            }
        }

        return $arrayResult;
    }

    /**
     * Retourne l'ascendant direct passé en parametre.
     *
     * @param $domaineActivite
     *
     * @return mixed
     */
    public function RechercheAscendantDomaineActivite($domaineActivite)
    {
        for ($i = strlen($domaineActivite) - 1; $i >= 0; --$i) {
            if (0 != $domaineActivite[$i]) {
                $domaineActivite[$i] = 0;
                break;
            }
        }

        return $domaineActivite;
    }

    /**
     * @param $typesCollaboration
     * @param $typeRecherche
     *
     * @return bool
     */
    public function isTypeCollaborationExiste($typesCollaboration, $typeRecherche)
    {
        if ($typesCollaboration) {
            $arrayColl = explode('#', $typesCollaboration);

            for ($i = 0; $i < count($arrayColl); ++$i) {
                if ($arrayColl[$i] == $typeRecherche) {
                    return true;
                }
            }

            return false;
        } else {
            return false;
        }
    }

    /**
     * @param $identreprise
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function autoriseAccesEntreprises($identreprise)
    {
        if (0 == $identreprise) {
            return false;
        }

        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $c = new Criteria();
        $c->add(EntreprisePeer::ID, $identreprise);
        $company = EntreprisePeer::doSelectOne($c, $connexionCom);

        if ($company) {
            if ('1' == $company->getVisibleBourse()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param $idDomaine
     *
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function getEntrepriseByIdDomaineActivite($idDomaine): array|bool
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
        );

        $c = new Criteria();
        $crit0 = $c->getNewCriterion(EntreprisePeer::DOMAINES_ACTIVITES, '%#'.$idDomaine.'#%', Criteria::LIKE);
        $crit1 = $c->getNewCriterion(EntreprisePeer::DOMAINES_ACTIVITES, $idDomaine.'#%', Criteria::LIKE);
        $crit2 = $c->getNewCriterion(EntreprisePeer::DOMAINES_ACTIVITES, '%#'.$idDomaine, Criteria::LIKE);
        $crit0->addOr($crit1);
        $crit0->addOr($crit2);
        $c->add($crit0);
        $arrayEntreprises = EntreprisePeer::doSelect($c, $connexion);

        if ($arrayEntreprises) {
            return $arrayEntreprises;
        } else {
            return false;
        }
    }

    /**
     * Permet de recuperer une entreprise à partir de son raison sociale.
     *
     * @param $raisonS
     *
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retrieveCompanyBdeByName($raisonS): bool|\Application\Propel\Mpe\Entreprise
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $c = new Criteria();
        $c->add(EntreprisePeer::NOM, $raisonS);
        $company = EntreprisePeer::doSelectOne($c, $connexion);

        if ($company) {
            return $company;
        } else {
            return false;
        }
    }

    /**
     * Permet d'initialiser la capacite de l'entreprise.
     *
     * @return array
     *
     * @throws Atexo_Config_Exception
     */
    public function initialiserCapacite()
    {
        $capacites = [];

        $listeAnnees = Atexo_Util::arrayAnneesDerniers(
            Atexo_Config::getParameter('NOMBRE_ANNEES_MAX_TRANCHE_EFFECTIF')
        );

        if (is_array($listeAnnees)) {
            foreach ($listeAnnees as $annee) {
                $capacite = new Atexo_Entreprise_Capacite();
                $capacite->setAnneeCloture($annee);
                $capacite->setDateDebut('01/01/'.$annee);
                $capacite->setDateFin('31/12/'.$annee);
                $capacites[] = $capacite;
            }
        }

        return $capacites;
    }

    /**
     * Permet de remplir la capcites par les donnees entreprise.
     *
     * @param $capacites
     * @param $idEntreprise
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function fillCapaciteFormEntreprise(&$capacites, $idEntreprise)
    {
        if (is_array($capacites) && count($capacites)) {
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
            );

            foreach ($capacites as $capacite) {
                $annee = $capacite->getAnneeCloture();
                $EntrepriseInfoExercice = CommonEntrepriseInfoExercice::getCommonEntrepriseInfoExercice(
                    $idEntreprise,
                    $annee,
                    $connexion
                );

                if ($EntrepriseInfoExercice) {
                    $capacite->setDateDebut(Atexo_Util::iso2frnDate($EntrepriseInfoExercice->getDebutExerciceGlob()));
                    $capacite->setDateFin(Atexo_Util::iso2frnDate($EntrepriseInfoExercice->getFinExerciceGlob()));
                    $capacite->setChiffreAffaire($EntrepriseInfoExercice->getVentesglob());
                    $capacite->setSaisieManuelle($EntrepriseInfoExercice->getSaisieManuelle());
                }
            }
        }
    }

    /**
     * @param $newCompany
     * @param null $connexion
     *
     * @return mixed
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function save($newCompany, $connexion = null)
    {
        return self::saveEntreprise($newCompany, $connexion);
    }

    /**
     * @param $newCompany
     * @param null $connexion
     *
     * @return mixed
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function saveEntreprise($newCompany, $connexion = null)
    {
        if (empty($connexion)) {
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
            );
        }

        $entreprise = self::getEntrepriseFromEntrepriseVo($newCompany);

        /** @var Entreprise $entreprise */
        $entreprise->save($connexion);
        return $entreprise;
    }

    /**
     * Permet de recuperer un objet Entreprise from Atexo_Entreprise_EntrepriseVo.
     *
     * @param Atexo_Entreprise_EntrepriseVo
     *
     * @return mixed
     *
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getEntrepriseFromEntrepriseVo($entrepriseVo)
    {
        if ($entrepriseVo->getId()) {
            $entreprise = EntreprisePeer::retrieveByPK($entrepriseVo->getId());
        } else {
            $entreprise = new Entreprise();
        }

        return self::fillInfosEse($entrepriseVo, $entreprise, true);
    }

    /**
     * @param $entrepriseObj
     *
     * @return Atexo_Entreprise_EntrepriseVo|mixed
     */
    public function getEntrepriseVoFromEntreprise($entrepriseObj)
    {
        $entrepriseVo = new Atexo_Entreprise_EntrepriseVo();
        $entrepriseVo = self::fillInfosEse($entrepriseObj, $entrepriseVo);
        $entrepriseVo->setId($entrepriseObj->getId());

        return $entrepriseVo;
    }

    /**
     * @param $from
     * @param $to
     * @param bool $avecObjetRelie
     * @param bool $ecraserInfo
     *
     * @return mixed
     */
    public static function fillInfosEse($from, $to, $avecObjetRelie = false, $ecraserInfo = false)
    {
        if (null !== $from->getSiren() || $ecraserInfo) {
            $to->setSiren($from->getSiren());
        }

        if (null !== $from->getNicSiege()) {
            $to->setNicSiege($from->getNicSiege());
        }

        if (null !== $from->getPaysenregistrement()) {
            $to->setPaysenregistrement($from->getPaysenregistrement());
        }

        if (null !== $from->getSirenetranger()) {
            $to->setSirenetranger($from->getSirenetranger());
        }

        if (null !== $from->getPaysadresse()) {
            $to->setPaysadresse($from->getPaysadresse());
        }

        if (null !== $from->getAcronymePays()) {
            $to->setAcronymePays($from->getAcronymePays());
        }

        if (null !== $from->getNom() || $ecraserInfo) {
            $to->setNom($from->getNom());
        }

        if (null !== $from->getAdresse()) {
            $to->setAdresse($from->getAdresse());
        }

        if (null !== $from->getAdresse2()) {
            $to->setAdresse2($from->getAdresse2());
        }

        if (null !== $from->getCodepostal()) {
            $to->setCodepostal($from->getCodepostal());
        }

        if (null !== $from->getVilleadresse()) {
            $to->setVilleadresse($from->getVilleadresse());
        }

        if (null !== $from->getTelephone()) {
            $to->setTelephone($from->getTelephone());
        }

        if (null !== $from->getFax()) {
            $to->setFax($from->getFax());
        }
        if (null !== $from->getEntrepriseEA()) {
            $to->setEntrepriseEA($from->getEntrepriseEA());
        }

        if (null !== $from->getEntrepriseSIAE()) {
            $to->setEntrepriseSIAE($from->getEntrepriseSIAE());
        }

        if (null !== $from->getCodeape() || $ecraserInfo) {
            $to->setCodeape($from->getCodeape());
        }

        if (null !== $from->getLibelleApe() || $ecraserInfo) {
            $to->setLibelleApe($from->getLibelleApe());
        }

        if (null !== $from->getFormejuridique() || $ecraserInfo) {
            $to->setFormejuridique($from->getFormejuridique());
        }

        if (null !== $from->getDateCreation()) {
            $to->setDateCreation($from->getDateCreation());
        }

        if (null !== $from->getDateModification()) {
            $to->setDateModification($from->getDateModification());
        }

        if (null !== $from->getRegion()) {
            $to->setRegion($from->getRegion());
        }

        if (null !== $from->getProvince()) {
            $to->setProvince($from->getProvince());
        }

        if (null !== $from->getTelephone2()) {
            $to->setTelephone2($from->getTelephone2());
        }

        if (null !== $from->getTelephone3()) {
            $to->setTelephone3($from->getTelephone3());
        }

        if (null !== $from->getCnss()) {
            $to->setCnss($from->getCnss());
        }

        if (null !== $from->getCapitalSocial() || $ecraserInfo) {
            $to->setCapitalSocial($from->getCapitalSocial());
        }

        if (null !== $from->getNumTax()) {
            $to->setNumTax($from->getNumTax());
        }

        if (null !== $from->getRcNum()) {
            $to->setRcNum($from->getRcNum());
        }

        if (null !== $from->getRcVille()) {
            $to->setRcVille($from->getRcVille());
        }

        if (null !== $from->getDomainesActivites()) {
            $to->setDomainesActivites($from->getDomainesActivites());
        }

        if (null !== $from->getSiteInternet()) {
            $to->setSiteInternet($from->getSiteInternet());
        }

        if (null !== $from->getDescriptionActivite()) {
            $to->setDescriptionActivite($from->getDescriptionActivite());
        }

        if (null !== $from->getActiviteDomaineDefense()) {
            $to->setActiviteDomaineDefense($from->getActiviteDomaineDefense());
        }

        if (null !== $from->getAdressesElectroniques()) {
            $to->setAdressesElectroniques($from->getAdressesElectroniques());
        }

        if (null !== $from->getMoyensTechnique()) {
            $to->setMoyensTechnique($from->getMoyensTechnique());
        }

        if (null !== $from->getMoyensHumains()) {
            $to->setMoyensHumains($from->getMoyensHumains());
        }

        if (null !== $from->getVisibleBourse()) {
            $to->setVisibleBourse($from->getVisibleBourse());
        }

        if (null !== $from->getTypeCollaboration()) {
            $to->setTypeCollaboration($from->getTypeCollaboration());
        }

        if (null !== $from->getCompteActif()) {
            $to->setCompteActif($from->getCompteActif());
        }

        if (null !== $from->getIfu()) {
            $to->setIfu($from->getIfu());
        }

        if (null !== $from->getSaisieManuelle()) {
            $to->setSaisieManuelle($from->getSaisieManuelle());
        }

        if (null !== $from->getSiretSiegeSocial()) {
            $to->setSiretSiegeSocial($from->getSiretSiegeSocial());
        }

        if (null !== $from->getCreatedFromDecision()) {
            $to->setCreatedFromDecision($from->getCreatedFromDecision());
        }

        if ($avecObjetRelie && null !== $from->getCommonInscrits()) {
            foreach ($from->getCommonInscrits() as $oneInscrit) {
                $to->addCommonInscrit($oneInscrit);
            }
        }

        if ($avecObjetRelie && null !== $from->getListeEtablissements()) {
            (new Atexo_Entreprise_Etablissement())->setArrayCommonTEtablissementInEntreprise($from->getListeEtablissements(), $to, true);
        }

        if (null !== $from->getMandataires()) {
            (new Atexo_Entreprise_Leaders())->setArrayCommonResponsableengagementInEntreprise($from->getMandataires(), $to);
        }

        if (null !== $from->getEtablissementSiege()) {
            $to->setEtablissementSiege($from->getEtablissementSiege());
        }

        if (null !== $from->getIdCodeEffectif()) {
            $to->setIdCodeEffectif($from->getIdCodeEffectif());
        }
        if (null !== $from->getCategorieEntreprise()) {
            $to->setCategorieEntreprise($from->getCategorieEntreprise());
        }

        if (null !== $from->getEtatAdministratif()) {
            $to->setEtatAdministratif($from->getEtatAdministratif());
        }

        if (null !== $from->getDateCessation()) {
            $to->setDateCessation($from->getDateCessation());
        }

        if (null !== $from->getdocumentsCommerciaux()) {
            $to->setdocumentsCommerciaux($from->getdocumentsCommerciaux());
        }

        if (null !== $from->getIntituleDocumentsCommerciaux()) {
            $to->setIntituleDocumentsCommerciaux($from->getIntituleDocumentsCommerciaux());
        }

        if (null !== $from->getTailleDocumentsCommerciaux()) {
            $to->setTailleDocumentsCommerciaux($from->getTailleDocumentsCommerciaux());
        }

        return $to;
    }

    /**
     * Retourne le mode de synchronisation de l'entreprise.
     *
     * @param $siren
     *
     * @return mixed
     *
     * @throws Atexo_Config_Exception
     */
    public function getModeSynchroEntreprise($siren)
    {
        $token = Atexo_Config::getParameter('API_ENTREPRISE_TOKEN');

        $paramsWs = [
            'token' => $token,
            'apiEntrepriseUrl' => Atexo_Config::getParameter('URL_API_ENTREPRISE_UNITE_LEGALE'),
        ];

        $loggerWs = Atexo_LoggerManager::getLogger();
        $ws = new InterfaceSynchroApiGouvEntreprise($loggerWs, $paramsWs);
        $reponse = $ws->getEntrepriseBySiren($siren);
        $casWs = $reponse[0];

        return self::getModeSynchro($casWs);
    }

    /**
     * Permet de determiner si l'entreprise est Francaise ou etrangere.
     *
     * @param $idEntreprise
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function isEntrepriseFrancaise($idEntreprise)
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $entreprise = BaseEntreprisePeer::retrieveByPK($idEntreprise, $connexion);

        if ($entreprise instanceof Entreprise && '' != $entreprise->getSiren()) {
            return true;
        }

        return false;
    }

    /**
     * Permet de recuperer une entreprise selon le siren.
     *
     * @param $siren
     * @param bool $withEtablissement
     * @param null $connexion
     * @param null $idObjet
     *
     * @return bool|mixed
     *
     * @throws Atexo_Config_Exception
     */
    public static function recupererEntrepriseApiGouvEntreprise($siren, $withEtablissement = false, $connexion = null, $idObjet = null)
    {
        $token = Atexo_Config::getParameter('API_ENTREPRISE_TOKEN');

        $paramsWs = [
            'token' => $token,
            'apiEntrepriseUrl' => Atexo_Config::getParameter('URL_API_ENTREPRISE_UNITE_LEGALE'),
            'apiEtablissementUrl' => Atexo_Config::getParameter('URL_API_ENTREPRISE_ETABLISSEMENT'),
        ];

        $loggerWs = Atexo_LoggerManager::getLogger();
        $interfaceWS = new InterfaceSynchroApiGouvEntreprise($loggerWs, $paramsWs);
        $resultat = $interfaceWS->getEntrepriseBySiren($siren, $withEtablissement);

        if (is_array($resultat) && InterfaceSynchroApiGouvEntreprise::isResponseWsApiGouvEntrepriseSucces($resultat) && $resultat[1]) {
            $jeton = $resultat[2];
            $loggerWs->info("Debut de l'enregistrement de l'historique de la synchronisation de l'entreprise");
            (new Atexo_Entreprise_HistoriqueSynchroSGMAP())->enregistrerHistroique('ENTREPRISE', $siren, $jeton, $connexion, $idObjet);
            $loggerWs->info("Fin de l'enregistrement de l'historique de la synchronisation de l'entreprise");

            return $resultat[1];
        }

        return false;
    }

    /**
     * Permet de synchroniser une entreprise identifiee par son siren ou un etablissement identifie par son siret.
     *
     * @param string $identifiant     : siren sur 9 caracteres ou siret sur 14 caracteres
     * @param int    $typeEntreprise: type de l'entreprise
     *                                => 1 pour nationale
     *                                => 2 pour etrangere
     * @param string $modeSynchro     : mode de saisie (cette valeur sera retournee et pourra etre utilisee pour l'affichage de la page appelante)
     *                                => mode1 = Mode degradee sans message d'avertissement
     *                                => mode2 = Mode degradee avec message d'avertissement
     *                                => mode3 = Mode degradee avec message d'avertissement
     * @param bool   $forceSynchro    : permet de forcer la synchronisation
     *                                => true force la synchronisation
     *                                => false sinon
     *
     * @return Atexo_Entreprise_EntrepriseVo|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2016
     */
    public static function synchroniserEntrepriseEtablissementAvecSGMAP(
        $identifiant,
        $typeEntreprise,
        &$modeSynchro,
        $forceSynchro = false,
        $connexion = null
    ) {
        $objetToSynchronise = null;
        $idEchangesInterfaces = null;
        $params = [];

        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'synchroniserEntrepriseEtablissementAvecSGMAP';
            $params['service'] = 'API_GOUV_ENTREPRISE';

            $entree = serialize(
                ['identifiant' => $identifiant, 'typeEntreprise' => $typeEntreprise, 'forceSynchro' => $forceSynchro]
            );
            $params['variablesEntree'] = $entree;
            $params['informationMetier'] = $entree;
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $idEchangesInterfaces = null;

            if ($params['oversight']) {
                $idEchangesInterfaces = $params['oversight']->getId();
            }

            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        $loggerWs = Atexo_LoggerManager::getLogger();
        $loggerWs->info("Debut de la synchronisation avec Api Gouv Entreprise : identifiant = $identifiant");

        try {
            $retourSynchro = null;

            if (!$connexion) {
                $connexion = Propel::getConnection(
                    Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
                );
            }

            //Recuperation de l'entreprise a partir du siren ou du siret
            $entreprise = EntrepriseQuery::create()->findOneBySiren(substr($identifiant, 0, 9));

            //Recuperation des objets a synchroniser
            if (9 == strlen($identifiant)) {
                $loggerWs->info("L'objet a synchroniser avec Api Gouv Entreprise est une ENTREPRISE");
                $objetToSynchronise = $entreprise;

                if (!($objetToSynchronise instanceof Entreprise)) {
                    $loggerWs->info("L'entreprise a synchroniser avec Api Gouv Entreprise n'existe pas en base de donnees");
                    $objetToSynchronise = new Entreprise();
                    $forceSynchro = true;
                }
            } elseif (14 == strlen($identifiant)) {
                $loggerWs->info("L'objet a synchroniser avec Api Gouv Entreprise est un ETABLISSEMENT");

                if ($entreprise instanceof Entreprise) {
                    $objetToSynchronise = CommonTEtablissementQuery::create()
                        ->filterByCodeEtablissement(substr($identifiant, 9))
                        ->filterByIdEntreprise($entreprise->getId())
                        ->findOne($connexion);
                }

                if (!($objetToSynchronise instanceof CommonTEtablissement)) {
                    $loggerWs->info("L'etablissement a synchroniser avec Api Gouv Entreprise n'existe pas en base de donnees");
                    $objetToSynchronise = new CommonTEtablissement();
                    $forceSynchro = true;
                }
            }

            switch (Atexo_Module::isEnabled('SynchronisationSgmap')) {
                case false:
                    $modeSynchro = 'mode1'; //Mode degradee sans message d'avertissement
                    break;
                case true:
                    switch ($typeEntreprise) {
                        case 2:
                            $modeSynchro = 'mode1'; //Mode degradee sans message d'avertissement
                            break;
                        case 1:
                            if (self::isEtpEtabSynchronisable($objetToSynchronise, $forceSynchro)) {
                                $objetSynchronisable = true;
                                $loggerWs->info("L'objet a synchroniser n'est pas a jour avec Api Gouv Entreprise");

                                switch (strlen($identifiant)) {
                                    case 9: // type objet Entreprise
                                        $retourSynchro = self::synchroEntrepriseAvecApiGouvEntreprise(
                                            $identifiant,
                                            $objetToSynchronise,
                                            $connexion,
                                            $idEchangesInterfaces
                                        );
                                        break;
                                    case 14: // type objet Etablissement
                                        $retourSynchro = (new Atexo_Entreprise_Etablissement())->synchroEtablissementAvecApiGouvEntreprise($identifiant, $objetToSynchronise, $connexion, $idEchangesInterfaces);
                                        break;
                                }
                            } else {
                                $objetSynchronisable = false;
                                $loggerWs->info("L'objet a synchroniser est deja a jour avec Api Gouv Entreprise");
                                $retourSynchro = 'Objet Synchronise';
                                $loggerWs->info("Fin de la synchronisation avec Api Gouv Entreprise : identifiant = $identifiant");
                            }

                            $modeSynchro = 'mode3'; //Mode automatique

                            if ($objetSynchronisable && !$retourSynchro) {
                                $modeSynchro = 'mode2'; //Mode degradee avec message d'avertissement
                            }
                    }
                    break;
            }

            try {
                Atexo_Oversight_Oversight::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            return $retourSynchro;
        } catch (Exception $e) {
            $loggerWs->error("Erreur lors de la synchronisation avec Api Gouv Entreprise : identifiant = $identifiant , typeEntreprise = $typeEntreprise, modeSynchro = $modeSynchro, forceSynchro = $forceSynchro \\Erreur: ".$e->getMessage()." \n\nTrace: ".$e->getTraceAsString());
        }

        $loggerWs->info("Fin de la synchronisation avec Api Gouv Entreprise : identifiant = $identifiant");

        try {
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            Atexo_Oversight_Oversight::oversight($params);
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }
    }

    /**
     * Permet de synchroniser une entreprise identifiee par son siren avec Api Gouv Entreprise.
     *
     * @param string     $siren      : siren de l'entreprise sur 9 caracteres
     * @param Entreprise $entreprise : objet entreprise
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public static function synchroEntrepriseAvecApiGouvEntreprise($siren, $entreprise, $connexion = null, $idEchangesInterfaces = null)
    {
        $params = [];

        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'synchroEntrepriseAvecSGMAP';
            $params['service'] = 'API_GOUV_ENTREPRISE';
            Atexo_Oversight_Oversight::oversight($params);
            $entree = serialize(['siren' => $siren, 'entreprise' => $entreprise]);
            $params['variablesEntree'] = $entree;

            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;

            if (!is_null($idEchangesInterfaces)) {
                $params['idEchangesInterfaces'] = $idEchangesInterfaces;
            }
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        $loggerWs = Atexo_LoggerManager::getLogger();
        $loggerWs->info("Debut de la synchronisation de l'entreprise dont le siren = $siren avec Api Gouv Entreprise");

        try {
            if (!$connexion) {
                $connexion = Propel::getConnection(
                    Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
                );
            }

            $idEntreprise = null;

            if ($entreprise instanceof Entreprise && !empty($entreprise->getId())) {
                $idEntreprise = $entreprise->getId();
            } else {
                $entreprise = new Entreprise();
                $loggerWs->info("L'entreprise dont le siren = $siren n'existe pas en base de donnees");
            }

            $entrepriseVoSgmap = self::recupererEntrepriseApiGouvEntreprise($siren, true, $connexion, $idEntreprise);

            if ($entrepriseVoSgmap instanceof Atexo_Entreprise_EntrepriseVo) {
                $loggerWs->info("L'objet existe dans la BD SGMAP");
                $listeEtablissements = $entrepriseVoSgmap->getEtablissements();

                if (is_array($listeEtablissements) && !empty($listeEtablissements)) {
                    //Enregistrement du code APE dans l'entreprise $entrepriseVoSgmap
                    $loggerWs->info("La récuperation du codeApe a partir de l'etablisssement siége");
                    $codeApe = self::getCodeApeEtablissementSiege($listeEtablissements);
                    $loggerWs->info("Le codeApe = '".$codeApe."'");
                    $entrepriseVoSgmap->setCodeape($codeApe);
                }

                $loggerWs->info("convertir l'objet Atexo_Entreprise_EntrepriseVo to Entreprise");
                $ecraserInfo = true;
                $loggerWs->info("Debut de la suppression des anciens dirigeants de l'entreprise ");
                Atexo_Entreprise_Leaders::deleteLeaders($entreprise->getId(), $connexion);
                $loggerWs->info("Fin de la suppression des anciens dirigeants de l'entreprise ");
                $entreprise = self::fillInfosEse($entrepriseVoSgmap, $entreprise, false, $ecraserInfo);

                //Enregistrement de l'entreprise
                $entreprise->setDateModification(date('Y-m-d H:i:s'));
                $entreprise->setSaisieManuelle('0');

                $entreprise->setPaysenregistrement(
                    Atexo_Util::encodeToHttp(
                        Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')
                    )
                );
                $entreprise->setPaysadresse(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
                $entreprise->setAcronymePays(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
                $entreprise->save($connexion);
                $loggerWs->info("L'entreprise synchronise avec Api Gouv Entreprise est mis a jour avec SUCCES sur MPE");

                //Enregistrer l'etablissement siege
                $loggerWs->info("Debut enregistrement de l'etablisssement siége");

                if ($entreprise instanceof Entreprise && !empty($entreprise->getId())) {
                    foreach ($listeEtablissements as $etablissementVo) {
                        if ($etablissementVo instanceof Atexo_Entreprise_EtablissementVo) {
                            if ($etablissementVo->getEstSiege()) {
                                $etablissementVo->setIdEntreprise($entreprise->getId());
                                (new Atexo_Entreprise_Etablissement())->enregistrerEtablissementApiGouvEntreprise($etablissementVo);

                                $etabDb = CommonTEtablissementQuery::create()->filterByCodeEtablissement($etablissementVo->getCodeEtablissement())->filterByIdEntreprise($etablissementVo->getIdEntreprise())->findOne($connexion);

                                if ($etabDb instanceof CommonTEtablissement) {
                                    $entreprise->setEtablissementSiege($etabDb->getEtablissementVo());
                                }
                            }
                        }
                    }
                }

                $loggerWs->info("Fin enregistrement de l'etablisssement siége");

                (new Atexo_Entreprise_Exercice())->synchroExerciceAvecSGMAP($entreprise->getSiren(), $loggerWs, $connexion);

                $loggerWs->info("Conversion de l'entreprise en Atexo_Entreprise_EntrepriseVo");
                $loggerWs->info("Fin de la synchronisation de l'entreprise dont le siren = $siren avec Api Gouv Entreprise");

                try {
                    Atexo_Oversight_Oversight::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger();
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return (new self())->getEntrepriseVoFromEntreprise($entreprise);
            } else {
                $loggerWs->error("une erreur est survenue lors de la synchronisation de l'entreprise avec Api Gouv Entreprise");
            }
        } catch (Exception $e) {
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorRemoteServiceReturnsAnError;

            $loggerWs->error("Erreur lors de la synchronisation avec Api Gouv Entreprise  : siren = $siren \n\nErreur: ".$e->getMessage()."\n\nTrace: ".$e->getTraceAsString());
        }

        $loggerWs->info("Fin de la synchronisation de l'entreprise dont le siren = $siren avec Api Gouv Entreprise");
    }

    /**
     * Permet de retourner le code ape (NAF) de l'etablissement siege.
     *
     * @param $etablissements
     *
     * @return string|null
     */
    public static function getCodeApeEtablissementSiege($etablissements)
    {
        if (is_array($etablissements)) {
            foreach ($etablissements as $etablissement) {
                if ($etablissement instanceof Atexo_Entreprise_EtablissementVo) {
                    if ($etablissement->getEstSiege()) {
                        return $etablissement->getCodeape();
                    }
                }
            }
        }

        return null;
    }

    /**
     * Permet de recuperer l'entreprise par son siren.
     *
     * @param $siren
     *
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public static function retrieveCompanyBySiren($siren): bool|\Application\Propel\Mpe\Entreprise
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $c = new Criteria();
        $c->add(EntreprisePeer::SIREN, $siren);
        $company = EntreprisePeer::doSelectOne($c, $connexionCom);

        if ($company) {
            return $company;
        } else {
            return false;
        }
    }

    /**
     * Permet de retourner le mode d'affichage des informations de l'entreprise selon l'attribut saisieManuelle.
     *
     * @param $entreprise
     * @param bool $calledFrom
     */
    public function getCssSaisieMnuelle($entreprise, $calledFrom = false): bool|string
    {
        if ($entreprise) {
            if ($calledFrom) {
                if (Atexo_Module::isEnabled('SynchronisationSGMAP') &&
                    (1 == $entreprise->getSaisieManuelle()) &&
                    strstr($calledFrom, 'Agent.')
                ) {
                    return 'italique';
                }
            } else {
                if (Atexo_Module::isEnabled('SynchronisationSGMAP') && (1 == $entreprise->getSaisieManuelle())) {
                    return 'italique';
                }
            }
        }

        return false;
    }

    /**
     * Permet de verifier si les conditions de la synchronisation sont verifiees.
     *
     * @param $etpEtab
     * @param bool $forceSynchro
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public static function isEtpEtabSynchronisable($etpEtab, $forceSynchro = false)
    {
        if (true === $forceSynchro) {
            return true;
        }

        if ($etpEtab instanceof Entreprise &&
            !empty($etpEtab->getId()) &&
            (
                $etpEtab->getSaisieManuelle() ||
                Atexo_Util::differenceJours(date('d/m/Y'), Atexo_Util::getDateTimeByFormat($etpEtab->getDateModification(), 'd/m/Y')) > Atexo_Config::getParameter('NBR_JOUR_TO_SYNCHRONISE_WITH_API_SGMAP')
            )
        ) {
            return true;
        }

        if ($etpEtab instanceof CommonTEtablissement &&
            !empty($etpEtab->getIdEtablissement()) &&
            (
                $etpEtab->getSaisieManuelle() ||
                Atexo_Util::differenceJours(date('d/m/Y'), $etpEtab->getDateModification('d/m/Y')) > Atexo_Config::getParameter('NBR_JOUR_TO_SYNCHRONISE_WITH_API_SGMAP')
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Permet de recuperer l'intentifant de l'entreprise.
     *
     * @param $entreprise
     *
     * @return string
     */
    public function getIndentifiantLocalOuEtranger($entreprise)
    {
        if ($entreprise instanceof Entreprise) {
            return $entreprise->getSiren() ?: ($entreprise->getSirenetranger() ?: '');
        }
    }

    /**
     * Permet de recuperer l'entrepriseVo  par son siren.
     *
     * @param $siren
     *
     * @return Atexo_Entreprise_EntrepriseVo|bool|mixed
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retrieveCompanyVoBySiren($siren)
    {
        $entreprise = self::retrieveCompanyBySiren($siren);

        if ($entreprise) {
            $company = $this->getEntrepriseVoFromEntreprise($entreprise);

            if ($company) {
                return $company;
            }
        }

        return false;
    }

    /**
     * Retourne un objet entrepriseVo identifié par l'idNational.
     *
     * @param $idNational
     * @param $pays
     *
     * @return Atexo_Entreprise_EntrepriseVo|bool|mixed
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retrieveCompanyVoByIdNational($idNational, $pays)
    {
        $entreprise = $this->retrieveCompanyByIdNational($idNational, $pays);

        if ($entreprise) {
            $company = $this->getEntrepriseVoFromEntreprise($entreprise);

            if ($company) {
                return $company;
            }
        }

        return false;
    }

    /**
     * Permet de savoir si l'entreprise est locale ou etranger.
     *
     * @param $entreprise
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     */
    public function isEntrepriseLocale($entreprise)
    {
        if ($entreprise->getSiren() &&
            $entreprise->getPaysenregistrement() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')
        ) {
            return true;
        }

        return false;
    }

    /**
     * Permet de retourner l'id code effectif d'une entreprise.
     *
     * @param int $id l'id de l'entreprise
     *
     * @return int l'id code effectif
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getIdCodeEffectifById($id)
    {
        $entrepriseQuery = new EntrepriseQuery();

        return $entrepriseQuery->getIdCodeEffectifByIdEntreprise($id);
    }

    /**
     * Permet de recuperer une entreprise etrangere par son identifiant national.
     *
     * @param $idNational
     *
     * @return Entreprise
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function getEntrepriseEtrangereByIdentifiantNational($idNational)
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        return CommonEntrepriseQuery::create()->filterBySirenetranger($idNational)->findOne($connexion);
    }

    /**
     * @param $idEntreprise
     *
     * @return Entreprise
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function getEntripseByIdEtablissement($idEntreprise)
    {
        $commonEntrepriseQuery = new CommonEntrepriseQuery(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
        );

        return $commonEntrepriseQuery->findOneById($idEntreprise);
    }
}
