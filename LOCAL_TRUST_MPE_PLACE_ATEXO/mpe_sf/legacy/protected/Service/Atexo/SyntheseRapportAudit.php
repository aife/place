<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTSyntheseRapportAudit;
use Application\Propel\Mpe\CommonTSyntheseRapportAuditPeer;

/*
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class Atexo_SyntheseRapportAudit
{
    /*
     *retourne un tableau d'objet CommonTSyntheseRapportAudit
     */

    public function retreiveSyntheseRapportAudit($organisme, $serviceId, $annee = false, $classification = false)
    {
        $connexionCom = null;
        if (!$connexionCom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $c = new Criteria();
        if ($organisme) {
            $c->add(CommonTSyntheseRapportAuditPeer::ORGANISME, $organisme);
            if (false !== $serviceId || 0 === $serviceId) {
                $c->add(CommonTSyntheseRapportAuditPeer::SERVICE_ID, $serviceId, Criteria::EQUAL);
            }
        } else {
            if ($classification) {
                $arrayAcronymes = Atexo_Organismes::retrieveOrganismes(false, null, true, $classification, true);
                if (is_array($arrayAcronymes)) {
                    $c->add(CommonTSyntheseRapportAuditPeer::ORGANISME, $arrayAcronymes, Criteria::IN);
                }
            }
        }
        if ($annee) {
            $c->add(CommonTSyntheseRapportAuditPeer::ANNEE, $annee, Criteria::EQUAL);
        }
        $c->addDescendingOrderByColumn(CommonTSyntheseRapportAuditPeer::UNTRUSTEDDATE);
        $result = CommonTSyntheseRapportAuditPeer::doSelect($c, $connexionCom);
        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    /**
     * suppression d'un objet CommonTSyntheseRapportAudit.
     */
    public function doDeleteSRA($identifiant, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTSyntheseRapportAuditPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonTSyntheseRapportAuditPeer::ID, $identifiant, Criteria::EQUAL);
        CommonTSyntheseRapportAuditPeer::doDelete($c, $connexionCom);
    }
}
