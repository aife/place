<?php

namespace Application\Service\Atexo;

use APPPrado\Service\Atexo\Atexo_CurrentUser;
use PDO;

/**
 * Accès à la base de données.
 *
 * Cf. les exemples d'accès à la base par Atexo_Db dans les tests unitaires.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Db
{
    /**
     * Instance de PDO.
     */
    private static ?\PDO $linkCommon = null;

    /**
     * Instance de PDO pour l'organisme en cours.
     */
    private static array $linkOrganisms = [];

    /**
     * Retourne l'instance PDO de la base commune
     * Si le parametre read_only est egale a false, alors on a une connexion sur les bases dédiées en lecture/ecriture
     * Sinon, on a une connexion sur les bases dédiées à la lecture.
     *
     * @return \PDO
     */
    public static function getLinkCommon($read_only = false)
    {
        self::initCommon($read_only);

        return self::$linkCommon;
    }

    /**
     * Retourne l'instance PDO de la base de l'organisme courant
     * Si le parametre read_only est egale a false, alors on a une connexion sur les bases dédiées en lecture/ecriture
     * Sinon, on a une connexion sur les bases dédiées à la lecture.
     *
     * @return \PDO
     */
    public static function getLinkOrganism($org = null, $read_only = false)
    {
        if (null == $org) {
            self::initOrganism($org, $read_only);
            $currentOrganism = Atexo_CurrentUser::getCurrentOrganism();
        } else {
            self::initOrganism($org, $read_only);
            $currentOrganism = $org;
        }
        if (isset(self::$linkOrganisms[$currentOrganism])) {
            return self::$linkOrganisms[$currentOrganism];
        }

        return false;
    }

    public static function closeOrganisms()
    {
        $v = '';
        foreach (self::$linkOrganisms as $k => $v) {
            self::$linkOrganisms[$k] = null;
        }
        self::$linkOrganisms = [];
    }

    public static function closeOrganism($organisme)
    {
        self::$linkOrganisms[$organisme] = null;
        self::init($organisme, true, true);
        unset(self::$linkOrganisms[$organisme]);
    }

    public static function closeCommon()
    {
        self::$linkCommon = null;
        self::init(Atexo_Config::getParameter('COMMON_DB'), true, true);
        //unset(self::$linkCommon);
    }

    /**
     * Initialisation du lien vers la base commune.
     *
     * @return bool
     */
    private static function initCommon($read_only)
    {
        if (!self::$linkCommon) {
            self::$linkCommon = self::init(Atexo_Config::getParameter('COMMON_DB'), $read_only);
        }

        return (bool) self::$linkCommon;
    }

    /**
     * Initialisation du lien vers la base de l'organisme courant.
     *
     * @return bool
     */
    private static function initOrganism($org = null, $read_only = false)
    {
        if (null == $org) {
            $currentOrganism = Atexo_CurrentUser::getCurrentOrganism();
        } else {
            $currentOrganism = $org;
        }
        if (!isset(self::$linkOrganisms[$currentOrganism])) {
            self::$linkOrganisms[$currentOrganism] = self::init($currentOrganism, $read_only);
        }

        return (bool) self::$linkOrganisms[$currentOrganism];
    }

    /**
     * Initialise le lien vers une base de données.
     *
     * @param unknown_type $database
     *
     * @return PDO
     */
    private static function init($database, $read_only, $dead = false)
    {
        static $initialized = [];

        if (true === $dead) {
            unset($initialized[$database]);

            return;
        }

        $const_db = Atexo_Config::getParameter('CONST_READ_WRITE');
        //recuperer du fichier de configuration le nom de la constante
        if ($read_only) {
            $const_db = Atexo_Config::getParameter('CONST_READ_ONLY');
        }
        $link = null;
        if (!isset($initialized[$database])) {
            $initialized[$database] = true;
            $user = Atexo_Config::getParameter('DATABASE_USERNAME');
            $pass = Atexo_Config::getParameter('DATABASE_PASSWORD');
            try {
                $port = 'port='.Atexo_Config::getParameter('DATABASE_PORT').';';
            } catch (\Exception) {
                $port = '';
            }
            if (Atexo_Config::getParameter('DB_PREFIX')) {
                $prefix = Atexo_Config::getParameter('DB_PREFIX').'_';
            } else {
                $prefix = '';
            }
            $dsn = 'mysql:host='.Atexo_Config::getParameter('HOSTSPEC'.$const_db).';'.$port.'dbname='.$prefix.$database;
            $link = new \PDO($dsn, $user, $pass);
            if ($link) {
                $link->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                $link->setAttribute(\PDO::ATTR_CASE, \PDO::CASE_NATURAL);
                if (version_compare(PHP_VERSION, '5.2.1', '<')) {
                    $link->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
                    $link->setAttribute(\PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
                }
                $link->query('SET NAMES '.Atexo_Config::getParameter('DB_ENCODING').';');

                return $link;
            }

            return false;
        }

        return $link;
    }

    /*
     * Retourne la chaîne str, après avoir échappé tous les caractères qui doivent l'être, pour être utilisée dans une requête de base de données
     * @param: chaine à échapper
     * @return: chaine échappée
     */
    public function quote($chaine)
    {
        return addslashes($chaine);
    }

    public function getNomColumns($table, $element)
    {
        $arrayField = [];
        $sqlEntete = "SHOW COLUMNS FROM `$table`";
        $statement = Atexo_Db::getLinkCommon();
        $dataChamp = $statement->query($sqlEntete, PDO::FETCH_ASSOC);
        foreach ($dataChamp as $champ) {
            if (strtolower($champ['Field']) != $element) {
                $arrayField[] = $champ['Field'];
            }
        }

        return $arrayField;
    }
}
