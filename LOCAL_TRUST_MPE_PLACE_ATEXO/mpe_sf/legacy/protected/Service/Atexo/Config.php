<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Exception;
use Prado\Prado;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentTechniqueToken;
use Application\Propel\Mpe\CommonAgentTechniqueTokenQuery;
use Application\Propel\Mpe\CommonAgentQuery;

/**
 * Gestion de la configuration.
 *
 * Cette classe permet d'accéder aux directives de configuration
 * n'importe ou dans le code ainsi que de gérer la génération du
 * fichier de configuration application.xml en tenant compte de
 * la configuration spécifique de l'application en cours.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Config
{
    // Chemin vers les fichiers de configuration (à partir de la racine du site)
    const MPE_SF_MESSAGES_FILE = 'messages';

    // Modes de fonctionnement de Prado
    const APP_MODE_DEBUG = 'Debug';
    const APP_MODE_NORMAL = 'Normal';
    const APP_MODE_PERFORMANCE = 'Performance';

    public static string $configMpeFile = '';
    public static string $configGenFile = '';
    public static string $configCacheDir = '';

    protected static bool $configInitialized = false;

    protected static ?string $mpePradoDirectory = null;

    public static function getMpePradoDirectory()
    {
        if (!self::$mpePradoDirectory) {
            $classPath = substr(__DIR__, strlen(Atexo_Controller_Front::getRootPath()));
            $classPathArr = explode(DIRECTORY_SEPARATOR, trim($classPath, DIRECTORY_SEPARATOR));
            self::$mpePradoDirectory = $classPathArr[0];
        }

        return self::$mpePradoDirectory;
    }

    /**
     * Retourne un paramètre du fichier de configuration.
     *
     * Utilisable tout le temps.
     *
     * @param string $key
     *
     * @return string
     */
    public static function getParameter($key, $selectedOrg = null)
    {
        return Atexo_Util::getSfContainer()->getParameter($key);
    }

    public static function getCommonPathDir()
    {
        return self::getParameter('BASE_ROOT_DIR') . self::getParameter('COMMON_DIRECTORY');
    }

    public static function getProcedureEquivalenceDefault()
    {
        $procEquivalence = new CommonProcedureEquivalence();
        $procEquivalence->setCipherDisabled('+0');
        $procEquivalence->setCipherEnabled('+1');
        $procEquivalence->setConstitutionDossierReponse('+0');
        $procEquivalence->setDelaiDateLimiteRemisePli('+0');
        $procEquivalence->setDossierDce('+1');
        $procEquivalence->setElecResp('+1');
        if (Atexo_Module::isEnabled('EnveloppeAnonymat')) {
            $procEquivalence->setEnvAnonymat('+0');
        } else {
            $procEquivalence->setEnvAnonymat('-0');
        }
        $procEquivalence->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $procEquivalence->setEnvCandidature('+0');
        $procEquivalence->setEnvOffre('+1');
        $procEquivalence->setEnvOffreTypeMultiple('+0');
        $procEquivalence->setEnvOffreTypeMultiple2('+0');
        $procEquivalence->setEnvOffreTypeUnique('+1');
        $procEquivalence->setEnvOffreTypeUnique2('+1');
        $procEquivalence->setEnvoiComplet('-1');
        $procEquivalence->setEnvoiDiffere('-0');
        $procEquivalence->setEnvoiMailParMpe('+0');
        $procEquivalence->setFichierBoamp('+0');
        $procEquivalence->setFichierImporte('+0');
        $procEquivalence->setGestionEnvoisPostaux('+1');
        $procEquivalence->setMiseEnLigne1('+0');
        $procEquivalence->setMiseEnLigne2('+1');
        if (!Atexo_Module::isEnabled('PubliciteFormatXml')) {
            $procEquivalence->setMiseEnLigne3('-0');
            $procEquivalence->setMiseEnLigne4('-0');
        } else {
            $procEquivalence->setMiseEnLigne3('+0');
            $procEquivalence->setMiseEnLigne4('+0');
        }
        $procEquivalence->setNoElecResp('+0');
        $procEquivalence->setNoEnvoiMailParMpe('+0');
        $procEquivalence->setNoFichierAnnonce('+1');
        $procEquivalence->setPartialDceDownload('+1');
        $procEquivalence->setProcedurePublicite('+1');
        $procEquivalence->setProcedureRestreinte('+0');
        $procEquivalence->setProcedureRestreinteCandidature('+0');
        $procEquivalence->setProcedureRestreinteOffre('+0');
        $procEquivalence->setReglementCons('+1');
        $procEquivalence->setService('+1');
        $procEquivalence->setSignatureDisabled('+0');
        $procEquivalence->setSignatureEnabled('+1');
        $procEquivalence->setSignaturePropre('+0');
        $procEquivalence->setTirageDescriptif('+1');
        $procEquivalence->setTireurPlanCdrom('+0');
        $procEquivalence->setTireurPlanNom('+1');
        $procEquivalence->setTireurPlanNon('+1');
        $procEquivalence->setTireurPlanOui('+0');
        $procEquivalence->setTireurPlanPapier('+0');
        $procEquivalence->setRepObligatoire('+0');
        $procEquivalence->setNoRepObligatoire('+1');
        $procEquivalence->setMiseEnLigneEntiteCoordinatrice('-0');

        return $procEquivalence;
    }

    public static function getProcedureEquivalenceMAPA()
    {
        $procEquivalence = new CommonProcedureEquivalence();
        $procEquivalence->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $procEquivalence->setCipherDisabled('+0');
        $procEquivalence->setCipherEnabled('+1');
        $procEquivalence->setConstitutionDossierReponse('-0');
        $procEquivalence->setDelaiDateLimiteRemisePli('-0');
        $procEquivalence->setDossierDce('+1');
        $procEquivalence->setElecResp('+1');
        $procEquivalence->setEnvAnonymat('-0');
        $procEquivalence->setEnvCandidature('+0');
        $procEquivalence->setEnvOffre('+1');
        $procEquivalence->setEnvOffreTypeMultiple('-0');
        $procEquivalence->setEnvOffreTypeMultiple2('+0');
        $procEquivalence->setEnvOffreTypeUnique('1');
        $procEquivalence->setEnvOffreTypeUnique2('1');
        $procEquivalence->setEnvoiComplet('-1');
        $procEquivalence->setEnvoiDiffere('-0');
        $procEquivalence->setEnvoiMailParMpe('+0');
        $procEquivalence->setFichierBoamp('+0');
        $procEquivalence->setFichierImporte('+0');
        $procEquivalence->setGestionEnvoisPostaux('-1');
        $procEquivalence->setMiseEnLigne1('+0');
        $procEquivalence->setMiseEnLigne2('+1');
        $procEquivalence->setMiseEnLigne3('-0');
        $procEquivalence->setMiseEnLigne4('-0');
        $procEquivalence->setNoElecResp('+0');
        $procEquivalence->setNoEnvoiMailParMpe('+0');
        $procEquivalence->setNoFichierAnnonce('+1');
        $procEquivalence->setPartialDceDownload('+1');
        $procEquivalence->setProcedurePublicite('+1');
        $procEquivalence->setProcedureRestreinte('+0');
        $procEquivalence->setProcedureRestreinteCandidature('+0');
        $procEquivalence->setProcedureRestreinteOffre('+0');
        $procEquivalence->setReglementCons('+1');
        $procEquivalence->setService('+1');
        $procEquivalence->setSignatureDisabled('+0');
        $procEquivalence->setSignatureEnabled('+1');
        $procEquivalence->setSignaturePropre('-0');
        $procEquivalence->setTirageDescriptif('+1');
        $procEquivalence->setTireurPlanCdrom('-0');
        $procEquivalence->setTireurPlanNom('+1');
        $procEquivalence->setTireurPlanNon('+1');
        $procEquivalence->setTireurPlanOui('+0');
        $procEquivalence->setTireurPlanPapier('-0');
        $procEquivalence->setTypeDecisionARenseigner('+0');
        $procEquivalence->setTypeDecisionAttributionMarche('+0');
        $procEquivalence->setTypeDecisionDeclarationSansSuite('+0');
        $procEquivalence->setTypeDecisionDeclarationInfructueux('+0');
        $procEquivalence->setTypeDecisionSelectionEntreprise('+0');
        $procEquivalence->setTypeDecisionAutre('+0');
        $procEquivalence->setRepObligatoire('+0');
        $procEquivalence->setNoRepObligatoire('+1');

        return $procEquivalence;
    }

    public static function toPfEncoding($chaine)
    {
        if ('utf-8' == strtolower(Atexo_Config::getParameter('HTTP_ENCODING'))) {
            if (Atexo_Util::isUTF8($chaine)) {
                return $chaine;
            } else {
                return utf8_encode($chaine);
            }
        } else {
            if (Atexo_Util::isUTF8($chaine)) {
                return utf8_decode($chaine);
            } else {
                return $chaine;
            }
        }
    }

    /**
     * Permet d'inclure le chemin de la librairie php.
     */
    public function includePhpLibPath()
    {
        set_include_path(get_include_path() . PATH_SEPARATOR . Atexo_Config::getParameter('PHP_LIB_PATH'));
    }

    /**
     * Permet de retourner les chemin des messages pour MPE-SF.
     *
     * @param null $langue
     *
     * @return string
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public static function getMessageFileMpeSf($langue)
    {
        $directory = Atexo_Controller_Front::getRootPath() . '/translations';

        return $directory . DIRECTORY_SEPARATOR . self::MPE_SF_MESSAGES_FILE . '.' . $langue . '.xlf';
    }

    /**
     * @throws \Application\Library\Propel\Exception\PropelException
     */
    public function getTokenForMampAgent(): string|bool
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') .
            Atexo_Config::getParameter('CONST_READ_WRITE'));
        $agentTechnique = (new CommonAgentQuery())
            ->getAgentTechniqueByLogin('mamp', $connexionCom);
        return (new Atexo_TiersAuthentification())->createNewAgentTechniqueToken($agentTechnique, $connexionCom);
    }
}
