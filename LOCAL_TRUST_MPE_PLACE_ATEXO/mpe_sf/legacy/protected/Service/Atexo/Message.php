<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationFavorisQuery;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangeDestinatairePeer;
use Application\Propel\Mpe\CommonEchangeFormatPeer;
use Application\Propel\Mpe\CommonEchangePeer;
use Application\Propel\Mpe\CommonEchangePieceJointePeer;
use Application\Propel\Mpe\CommonEchangeTypeARPeer;
use Application\Propel\Mpe\CommonEchangeTypeMessagePeer;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Propel\Mpe\CommonQuestionsDcePeer;
use Application\Propel\Mpe\CommonRelationEchange;
use Application\Propel\Mpe\CommonRetraitPapierPeer;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServiceQuery;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTComplementFormulaire;
use Application\Propel\Mpe\CommonTComplementFormulairePeer;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Propel\Mpe\CommonTelechargementPeer;
use Application\Propel\Mpe\CommonTMessageAccueilQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Atexo_Consultation_GuestVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_ComplementFormulaire;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Echange;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Message\Atexo_Message_RelationEchange;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * classe Message.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Message
{
    /*
     * cette function va retourner un identifiant de 32 caractares :
     * un nombre hexadecimal de 128) qui sera tres difficile a predire
    */
    public function getUniqueId()
    {
        return md5(uniqid(random_int(0, mt_getrandmax()), true));
    }

    /**
     * Retourne le message encode en quoted_pintable pour les mails.
     */
    private function atx_quoted_printable_encode($string)
    {
        return quoted_printable_encode($string);
    }

    private function getContentType($fichier)
    {
        $extensionFile = substr($fichier, -3);

        if ('jpg' == $extensionFile) {
            $contentType = 'image/jpeg';
        } elseif ('gif' == $extensionFile) {
            $contentType = 'image/gif';
        } elseif ('bmp' == $extensionFile) {
            $contentType = 'image/bmp';
        } elseif ('doc' == $extensionFile) {
            $contentType = 'application/msword';
        } elseif ('xls' == $extensionFile) {
            $contentType = 'application/msexcel';
        } elseif ('pdf' == $extensionFile) {
            $contentType = 'application/pdf';
        } elseif ('pps' == $extensionFile) {
            $contentType = 'application/mspowerpoint';
        } elseif ('ppt' == $extensionFile) {
            $contentType = 'application/mspowerpoint';
        } elseif ('zip' == $extensionFile) {
            $contentType = 'application/zip';
        } else {
            $contentType = 'text/plain';
        }

        return $contentType;
    }

    /**
     * Genere une URL.
     */
    private function getUrlAR($numAR, $acrOrg, $html = false)
    {
        if (!$numAR) {
            PrintMessage(Prado::localize('GENERATION_URL_ACCUSE_RECEPTION_ECHEC').' : '.Prado::localize('NUMERO_ACCUSE_RECEPTION').".\n");

            return false;
        }

        if (Atexo_CurrentUser::hasHabilitation('AgentCommission') && 'cao' == Atexo_CurrentUser::readFromSession('ServiceMetier')) {
            $pfUrl = Atexo_MultiDomaine::getPfUrl($acrOrg, true, Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE'));
            $url = $pfUrl.'?page=Agent.CommissionDetailEchangeMail&num_ar='.$numAR.'&orgAcronyme='.$acrOrg;
        } else {

            $pfUrl = Atexo_MultiDomaine::replaceDomain(
                Atexo_Config::getParameter('PF_URL_REFERENCE'),
                Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
            );
            $url = $pfUrl.'?page=Entreprise.EntrepriseDetailEchangeMail&num_ar='.$numAR.'&orgAcronyme='.$acrOrg;
        }
        if ($html) {
            $confirmationUrl = "<a href='".$url."'>".$url.'</a>';
        } else {
            $confirmationUrl = $url;
        }
        $confirmationUrl .= '
';

        return $confirmationUrl;
    }

    private function getTextARNum($confirmUrlText, $cas)
    {
        if ($cas == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR')) {
            return Prado::localize('VEUILLEZ_CLIQUER_LIEN_CONFIRMER_RECEPTION_MAIL_CAS_2')."\n\n".$confirmUrlText."\n______________________\n\n";
        } elseif ($cas == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN')) {
            return Prado::localize('VEUILLEZ_CLIQUER_LIEN_CONFIRMER_RECEPTION_MAIL_CAS_3')."\n\n".$confirmUrlText;
        }
    }

    private function AccuseReception($bodyMsg, $arNum, $idEmail, $cas, $acrOrg, $html = false)
    {
        $body = null;
        if ($idEmail) {
            $confirmUrl = self::getUrlAR($arNum, $acrOrg, $html);
            $message = self::getTextARNum($confirmUrl, $cas);
            $body = $message.$bodyMsg;
        }

        return $body;
    }

    /**
     * fabrique un zip avec les PJ du message.
     *
     * @param EchangePieceJointe[] $PJs
     *
     * @return chemin du zip cree | ""
     */
    public function getZipPj($PJs, &$taille, $org, &$path = '')
    {
        if (is_array($PJs) && count($PJs)) {
            $path = Atexo_Config::getParameter('COMMON_TMP').session_id().time().'_echange_pj/';
            if (0 != Atexo_Files::createDir($path)) {
                //TODO log
                return false;
            }
            $destZipPath = $path.Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');
            foreach ($PJs as $PJ) {
                $fileName = '';
                $fileName = $PJ->getNomFichier();
                $taille += $PJ->getTaille();
                if (0 != Atexo_Blob::copyBlob($PJ->getPiece(), $path.$fileName, $org)) {
                    //TODO log
                    return false;
                }
                if ('' === (new Atexo_Zip())->addFileToZip($path.$fileName, $destZipPath)) {
                    //TODO log
                    return false;
                }
            }

            return $destZipPath;
        }
    }

    /**
     * @param $from
     * @param $to : mail destinataire, une seule adresse mail, (la fonction filter_var valide une adresse pas plusieurs adresses séparées par des , ou ; )
     * @param $subject
     * @param $message
     * @param string      $cc
     * @param string      $return_path
     * @param string|null $pfName
     *
     * @return bool
     */
    public static function simpleMail(
        $from,
        $to,
        $subject,
        $message,
        $cc = '',
        $return_path = '',
        bool $signed = false,
        bool $html = false,
        bool $backup = true,
        $pfName = null
    ) {
        try {
            $mail = self::initMail($from, $to, $subject, $message, $cc, $return_path, $html, $pfName);
            if ($signed) {
                $mail->attachSigner(self::signMail());
            }

            return self::sendMail($mail, $backup);
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de l'envoi du mail : from = $from, to = $to, subjet = $subject, cc = $cc, return_path = $return_path, signed = $signed, html = $html \n\nmessage : \n$message \n\nErreur : ".$e->getMessage());
        }
    }

    /**
     * @param $from
     * @param $to : mail destinataire, une seule adresse mail, (la fonction filter_var valide une adresse pas plusieurs adresses séparées par des , ou ; )
     * @param $subject
     * @param $message
     * @param $cc
     * @param $code
     * @param $fileName
     * @param string      $return_path
     * @param string|null $pfName
     *
     * @return bool
     */
    public function mailWithAttachement($from, $to, $subject, $message, $cc, $code, $fileName, $return_path = '', bool $html = false, bool $signed = false, bool $backup = true, $pfName = null)
    {
        try {
            $fileAttachement = (new \Swift_Attachment())
                ->setFilename($fileName)
                ->setContentType('text/plain')
                ->setBody($code);

            $mail = self::initMail($from, $to, $subject, $message, $cc, $return_path, $html, $pfName);
            $mail->attach($fileAttachement);
            if ($signed) {
                $mail->attachSigner(self::signMail());
            }

            return self::sendMail($mail, $backup);
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de l'envoi du mail : from = $from, to = $to, subjet = $subject, cc = $cc, return_path = $return_path, signed = $signed, html = $html \n\nmessage : \n$message \n\nErreur : ".$e->getMessage());
        }
    }

    public static function getGenericEmailFormat($content)
    {
        $message = file_get_contents(Atexo_Config::getParameter('PATH_FILE_MAIL_GENERIC_TEMPLATE'));

        $message = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $message);
        $message = str_replace('SALUTATION', Prado::localize('DEFINE_TEXT_BONJOUR'), $message);
        $message = str_replace('TITRE', '', $message);
        $message = str_replace('CORPS_MAIL', $content, $message);
        $message = str_replace('PLACE_MARCHE_PUBLIC_INTERMINISTERE', Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE'), $message);

        return $message;
    }

    /**
     * Initialise le contenu du mail.
     *
     * @param        $from
     * @param        $to
     * @param        $subject
     * @param        $message
     * @param string $cc
     * @param string $return_path
     * @param bool   $html
     *
     * @return Swift_Message
     *
     * @author Gregory CHEVRET <gregory@atexo.com>
     */
    private static function initMail($from, $to, $subject, $message, $cc = '', $return_path = '', $html = false, $pfName = null)
    {
        $plateformName = (new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('PF_SHORT_NAME'))
            . " - "
            . (new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('PF_LONG_NAME'));

        if ($pfName) {
            $plateformName = $pfName;
        }

        $mail = new \Swift_Message();
        $mail->setSubject($subject)
            ->setFrom([$from => $plateformName])
            ->setTo([$to])
            ->setCharset(Atexo_Config::getParameter('HTTP_ENCODING'))
            ->setContentType('text/'.($html ? 'html' : 'plain'))
            ->setBody($message);

        if (!empty($cc)) {
            $mail->setCc([$cc]);
        }
        if ('' != $return_path) {
            $mail->setReplyTo([$return_path]);
        }

        return $mail;
    }

    /**
     * envoie le mail et le sauvagarde.
     *
     *
     * @return int
     *
     * @author Gregory CHEVRET <gregory@atexo.com>
     */
    private static function sendMail(\Swift_Message $mail, bool $backup = true)
    {
        $mailer = new \Swift_Mailer(new \Swift_SmtpTransport());
        if ($backup) {
            $mailBackup = clone $mail;
            self::backupMail($mailBackup);
        }

        return $mailer->send($mail);
    }

    /**
     * Retourne un objet Swift_Signers_SMimeSigner permettant
     * la signature du mail.
     *
     * @return Swift_Signers_SMimeSigner
     *
     * @author Gregory CHEVRET <gregory@atexo.com>
     */
    private static function signMail()
    {
        $smimeSigner = (new \Swift_Signers_SMimeSigner())->newInstance();
        $extraCerts = Atexo_Config::getParameter('SIGNATURE_MAIL_CHAINE') ? Atexo_Config::getParameter('SIGNATURE_MAIL_CHAINE_CERTIFICAT') : null;
        $smimeSigner->setSignCertificate(Atexo_Config::getParameter('SIGNATURE_MAIL_CERTIFICAT'), Atexo_Config::getParameter('SIGNATURE_MAIL_CLE'), PKCS7_DETACHED, $extraCerts);

        return $smimeSigner;
    }

    /**
     * Sauvegarde sur le disque le mail envoyé.
     *
     *
     * @author Gregory CHEVRET <gregory@atexo.com>
     */
    public static function backupMail(\Swift_Message $mail)
    {
        $saveMailDir = Atexo_Config::getParameter('BASE_ROOT_DIR').'/sendMail/'.date('Y/m/d');
        if (!is_dir($saveMailDir)) {
            mkdir($saveMailDir, 0755, true);
        }
        if (!is_dir($saveMailDir.DIRECTORY_SEPARATOR.'eml')) {
            mkdir($saveMailDir.DIRECTORY_SEPARATOR.'eml', 0755, true);
        }
        if (!is_dir($saveMailDir.DIRECTORY_SEPARATOR.'html')) {
            mkdir($saveMailDir.DIRECTORY_SEPARATOR.'html', 0755, true);
        }
        $mailTo = $mail->getTo();
        $to = implode('-', array_keys($mailTo));
        $filenameMail = 'mail_'.$to.'_'.date('Ymd_His').'_'.uniqid();
        file_put_contents(
            $saveMailDir.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.$filenameMail.'.html',
            $mail->getBody()
        );
        file_put_contents(
            $saveMailDir.DIRECTORY_SEPARATOR.'eml'.DIRECTORY_SEPARATOR.$filenameMail.'.eml',
            $mail->toString()
        );
    }

    /**
     * Fonction d'envoi de mail.
     *
     * @param string $org        : organisme
     * @param string $callFrom   : "entreprise" si appele depuis la pf entreprise, "agent" si appele depuis la pf agent
     * @param string $destUnique : true si l'on est en mode destinataire unique par mail, false sinon
     *
     * @return boolean|string: l'identifiant du mail si envoi reussi, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function EnvoyerEmail(CommonEchange $objetMessageVO, $org, $callFrom = false, $destUnique = false): bool|string
    {
        $complement = null;
        $logger = Atexo_LoggerManager::getLogger('messec');
        $logger->info('Debut EnvoyerEmail id echange : '.$objetMessageVO->getId());
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonEchangePieceJointePeer::ORGANISME, $org);
        $cc = '';
        $taille = 0;
        $validePJ = false;
        $envoiMail = false;

        if (!$destUnique) {
            if ($objetMessageVO->getId()) {
                $objetMessage = self::retrieveMessageById($objetMessageVO->getId(), $org);
            } else {
                $objetMessage = $objetMessageVO->copy(true);
            }
            $echangeDestinataires = $objetMessageVO->getCommonEchangeDestinataires(null, $connexionCom);
            if (is_array($echangeDestinataires) && 0 != count($echangeDestinataires)) {
                foreach ($echangeDestinataires as $echangeDest) {
                    $dest = $echangeDest->copy(true);
                    $dest->setOrganisme($org);
                    $objetMessage->addCommonEchangeDestinataire($dest);
                }
            }
            $objetMessage->setOrganisme($org);
            $objetMessage->setIdTypeMessage($objetMessageVO->getIdTypeMessage());
            $objetMessage->setObjet($objetMessageVO->getObjet());
            $objetMessage->setCorps($objetMessageVO->getCorps());
            $objetMessage->setOptionEnvoi($objetMessageVO->getOptionEnvoi());
            if (Atexo_Module::isEnabled('dossierVolumineux')) {
                $objetMessage->setIdDossierVolumineux($objetMessageVO->getIdDossierVolumineux());
            }
        } else {
            $objetMessage = self::retrieveMessageById($objetMessageVO->getId(), $org);
        }
        if ($objetMessage->getStatus() == Atexo_Config::getParameter('STATUS_ECHANGE_VALIDE')) {
            return false;
        }
        //Debut traitement interface module SUB
        if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
            $logger->info('Debut traitement interface module SUB');
            $corpsMessage = $objetMessage->getCorps();
            $intituleMessage = $objetMessage->getObjet();
            $idEchange = $objetMessage->getId();
            $typeRelation = Atexo_Config::getParameter('TYPE_RELATION_ECHANGE_COMPLEMENT_FORMULAIRE');
            $relationEchange = (new Atexo_Message_RelationEchange())->getRelationByIdEchangeAndTypeRelation($idEchange, $typeRelation, $org);
            $complement = null;
            if ($relationEchange instanceof CommonRelationEchange) {
                $complement = CommonTComplementFormulairePeer::retrieveByPK($relationEchange->getIdExterne(), $connexionCom);
                if ($complement instanceof CommonTComplementFormulaire) {
                    $corpsMessage = str_replace('[__DATE_REMISE_DOSSIER_REOUVERTURE_]', Atexo_Util::iso2frnDateTime($complement->getDateARemettre(), true), $corpsMessage);
                    $corpsMessage = str_replace('[__PRECISIONS_COMPLEMENTS_SUB_SUB__]', $complement->getCommentaire(), $corpsMessage);
                    $corpsMessage = str_replace('[__MOTIF_REOUVERTURE__]', $complement->getMotif(), $corpsMessage);
                    $intituleMessage = str_replace('[__MOTIF_REOUVERTURE__]', $complement->getMotif(), $intituleMessage);
                    $intituleMessage = str_replace('[__MAIL_PREFIXE_OBJET__]', Atexo_Config::getParameter('PF_SHORT_NAME'), $intituleMessage);
                    $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireById($complement->getIdDossierFormulaire());
                    if ($dossier instanceof CommonTDossierFormulaire) {
                        $corpsMessage = str_replace('[__NUM_DOSSIER_SUB__]', $dossier->getReferenceDossierSub(), $corpsMessage);
                    }
                }
            }
            $objetMessage->setCorps($corpsMessage);
            $objetMessage->setObjet($intituleMessage);
            $logger->info('Fin traitement interface module SUB');
        }
        //Fin traitement interface module SUB
        $PJs = $objetMessage->getCommonEchangepiecejointes($c, $connexionCom);
        if (is_array($PJs) && 0 != count($PJs)) {
            $validePJ = true;
        }
        $pfName = null;
        $from = $objetMessage->getEmailExpediteur();
        if ($objetMessage->getConsultationId()) {
            $consultation = (new CommonConsultationQuery())->findOneById($objetMessage->getConsultationId());

            if ($consultation instanceof CommonConsultation
                && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
                $from = $plateformeVirtuelle->getNoReply();
                $pfName = $plateformeVirtuelle->getFooterMail();
            }
        }
        $subject = $objetMessage->getObjet();
        $destinataires = $objetMessageVO->getCommonEchangeDestinataires(null, $connexionCom);
        $dossier = '';
        $zipPj = (new self())->getZipPj($PJs, $taille, $org, $dossier);
        $taille = str_replace('.', ',', round($taille / (1_048_985.64)));
        if ($zipPj) {
            $code = (new Atexo_Files())->read_file($zipPj);
        }
        $fileName = Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');
        if ($taille >= 2 && $objetMessage->getOptionEnvoi() != Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE')) {//application
            $objetMessage->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN'));
        }
        if (is_array($destinataires) && 0 != count($destinataires)) {
            $logger->info('Debut foreach traitement destinataires');
            foreach ($destinataires as $destinataire) {
                $logger->info('Traitement destinataire : '.$destinataire->getMailDestinataire());
                $message = '';
                if ($objetMessage->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE')) {
                    $message = (new self())->getHeaderEchange().$objetMessage->getCorps();
                } elseif ($objetMessage->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR')) {
                    $message = (new self())->AccuseReception(
                        (new self())->getHeaderEchange().$objetMessage->getCorps(),
                        $destinataire->getUid(),
                        $destinataire->getIdEchange(),
                        Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR'),
                        $org,
                        true
                    );
                } elseif ($objetMessage->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN')) {
                    $placeMarchePublicInterministere = Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');
                    $consultation = (new CommonConsultationQuery())->findOneById($objetMessage->getConsultationId());

                    if ($consultation instanceof CommonConsultation
                        && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                        $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
                        $placeMarchePublicInterministere = $plateformeVirtuelle->getFooterMail();
                    }

                    $message = (new self())->getCorpsEchange($objetMessage, $org, $callFrom)
                        .(new self())->AccuseReception(
                            '',
                            $destinataire->getUid(),
                            $destinataire->getIdEchange(),
                            Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN'),
                            $org,
                            true
                        )
                        .(new self())->getBasPage(
                            $placeMarchePublicInterministere
                        );
                } elseif ($objetMessage->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE')) {
                    $placeMarchePublicInterministere = Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');
                    $consultation = (new CommonConsultationQuery())->findOneById($objetMessage->getConsultationId());

                    if ($consultation instanceof CommonConsultation
                        && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                        $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
                        $placeMarchePublicInterministere = $plateformeVirtuelle->getFooterMail();
                    }

                    $message = (new self())->getCorpsEchange($objetMessage, $org, $callFrom).(new self())->AccuseReception(
                        '',
                        $destinataire->getUid(),
                        $destinataire->getIdEchange(),
                        Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN'),
                        $org,
                        true
                    )
                        .(new self())->getBasPage($placeMarchePublicInterministere);
                } elseif ($objetMessage->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_PJ')) {
                    $message = $objetMessage->getCorps();
                } elseif ($objetMessage->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_NO_HEADER')) {
                    $message = $objetMessage->getCorps();
                }
                if ('entreprise' == $callFrom) {//ne pas envoyer le mail
                    $envoiMail = true;
                } else {
                    $message = (new self())->generateMailFormatHtml($subject, $message);
                    if ($validePJ
                        && $objetMessage->getOptionEnvoi() != Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN')
                        && $objetMessage->getOptionEnvoi() != Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE')) {
                        if ((new self())->mailWithAttachement($from, $destinataire->getMailDestinataire(), $subject, $message, $cc, $code, $fileName, $from, true)) {
                            $envoiMail = true;
                        }
                        $dirToRemove = str_replace($fileName, '', $zipPj);
                        if (is_dir($dirToRemove)) {
                            Atexo_Util::removeDirectoryRecursively($dirToRemove);
                        }
                    } else {
                        if (self::simpleMail(
                            $from,
                            $destinataire->getMailDestinataire(),
                            $subject,
                            $message,
                            $cc,
                            $from,
                            false,
                            true,
                            true,
                            $pfName
                        )) {
                            $envoiMail = true;
                        }
                    }
                }
            }
            $logger->info('Fin foreach traitement destinataires');
        }
        $envoieOk = false;
        if ($envoiMail) {
            $logger->info('Debut MAJ donnees echanges ');
            $dateMessage = date('Y-m-d H:i:s');
            $objetMessage->setDateMessage($dateMessage);
            $objetMessage->setStatus(Atexo_Config::getParameter('STATUS_ECHANGE_VALIDE'));
            $objetMessage->save($connexionCom);
            $logger->info('Fin MAJ donnees echanges avec date message : '.$dateMessage);
            //Dans le cas de l'utilisation de l'interface SUB, On met à jour la date de premier envoi de la demande de complement à l'OF
            //On fait une mise a jour des dossiers
            if (Atexo_Module::isEnabled('InterfaceModuleSub') && $complement instanceof CommonTComplementFormulaire) {
                //Enregistrer la date de reponse a la demande de complements
                $logger->info('Debut InterfaceModuleSub Enregistrer la date de reponse a la demande de complements  ');
                (new Atexo_FormulaireSub_ComplementFormulaire())->enregistrerDatePremierEnvoiMailDemandeComplements($complement);
                try {
                    $arrayInfos = [];
                    $arrayInfos[0]['binding']['type'] = 'dateType';
                    $arrayInfos[0]['binding']['nom'] = 'AOF_Candidature_Date_Reouverture_OF';
                    $arrayInfos[0]['binding']['valeur'] = date('d/m/Y');
                    $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireById($complement->getIdDossierFormulaire());
                    if ($dossier instanceof CommonTDossierFormulaire) {
                        (new Atexo_FormulaireSub_Echange())->updateDossierInterfaceSub($dossier->getCleExterneDossier(), $arrayInfos);
                    }
                } catch (\Exception $e) {
                    $logger = Atexo_LoggerManager::getLogger('publicite');
                    $logger->error("Erreur lors de l'envoi de la requete a l'interface SUB : ".$e->getMessage());
                }
                $logger->info('Fin InterfaceModuleSub Enregistrer la date de reponse a la demande de complements  ');
            }
            $logger->info('Fin EnvoyerEmail id echange : '.$objetMessageVO->getId());
            $envoieOk = $objetMessage->getId();
        }

        if (is_file($zipPj)) {
            if (!unlink($zipPj)) {
                $logger->error('Impossible de supprimer le fichier : '.$zipPj.' fonction EnvoyerEmail');
            }
            Atexo_Util::removeDirectoryRecursively($dossier);
        }

        return $envoieOk;
    }

    private function getCorpsEchange($objetMessage, $org, $callFrom = false)
    {
        $string = '';
        $string = (new self())->getHeaderEchange();
        $string .= (new self())->getInfoConsultation($objetMessage->getConsultationId(), $org, $callFrom);
        $string .= '
';

        return $string;
    }

    private function getHeaderEchange()
    {
        $string = '';
        $string = '
';
        $string .= '
';
        $string .= '
';
        $string .= Prado::localize('DEFINE_TEXT_BONJOUR');
        $string .= '
';
        $string .= Prado::localize('DEFINE_TEXT_RAISON_MESSAGE');
        $string .= '
';
        $string .= '
';

        return $string;
    }

    /**
     * Retourne la liste des Types de messages.
     *
     * @param bool $organisme : l'organisme de l'agent connecte,
     * @param bool $withPath
     *
     * @return array
     */
    public static function getAllTypeMessages()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();

        return CommonEchangeTypeMessagePeer::doSelect($c, $connexionCom);
    }

    /**
     * Retourne la liste des Types de messages.
     *
     * @param bool $organisme : l'organisme de l'agent connecte,
     *
     * @return objet
     */
    public static function retrieveTypeMessageById($IdTypeMessage)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        return CommonEchangeTypeMessagePeer::retrieveByPk($IdTypeMessage, $connexionCom);
    }

    /**
     * Retourne la liste des adresses du registre retrait.
     *
     * @param int  $consultationId : correspend a la consultation en cour,
     * @param bool $organisme      : l'organisme de l'agent connecte,
     *
     * @return array
     */
    public static function getAdressesRegistreRetraitAndTelechargement($consultationId, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c1 = new Criteria();
        $c1->add(CommonRetraitPapierPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c1->add(CommonRetraitPapierPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $ObjetsCriteria1 = CommonRetraitPapierPeer::doSelect($c1, $connexionCom);

        $c2 = new Criteria();
        $c2->add(CommonTelechargementPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c2->add(CommonTelechargementPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $ObjetsCriteria2 = CommonTelechargementPeer::doSelect($c2, $connexionCom);

        $Result = [];
        if (is_array($ObjetsCriteria1) && 0 != count($ObjetsCriteria1)) {
            foreach ($ObjetsCriteria1 as $ObjetCriteria1) {
                if (!in_array($ObjetCriteria1->getEmail(), $Result) && '' != $ObjetCriteria1->getEmail() && $ObjetCriteria1->getEmail()) {
                    $Result[] = $ObjetCriteria1->getEmail();
                }
            }
        }
        if (is_array($ObjetsCriteria2) && 0 != count($ObjetsCriteria2)) {
            foreach ($ObjetsCriteria2 as $ObjetCriteria2) {
                if (!in_array($ObjetCriteria2->getEmail(), $Result) && '' != $ObjetCriteria2->getEmail() && $ObjetCriteria2->getEmail()) {
                    $Result[] = $ObjetCriteria2->getEmail();
                }
            }
        }

        return implode(' , ', $Result);
    }

    /**
     * Retourne la liste des adresses du registre question.
     *
     * @param int  $consultationId : correspend a la consultation en cour,
     * @param bool $organisme      : l'organisme de l'agent connecte,
     *
     * @return array
     */
    public static function getAdressesRegistreQuestion($consultationId, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonQuestionsDcePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonQuestionsDcePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $ObjetsCriteria = CommonQuestionsDcePeer::doSelect($c, $connexionCom);

        $Result = [];
        if (is_array($ObjetsCriteria) && 0 != count($ObjetsCriteria)) {
            foreach ($ObjetsCriteria as $ObjetCriteria) {
                if (!in_array($ObjetCriteria->getEmail(), $Result) && $ObjetCriteria->getEmail() && '' != $ObjetCriteria->getEmail()) {
                    $Result[] = $ObjetCriteria->getEmail();
                }
            }
        }

        return implode(' , ', $Result);
    }

    /**
     * Retourne la liste des adresses du registre depot.
     *
     * @param int  $consultationId : correspend a la consultation en cour,
     * @param bool $organisme      : l'organisme de l'agent connecte,
     *
     * @return array
     */
    public static function getAdressesRegistreDepot($consultationId, $organisme, $depotPapier = false, $idOffre = false)
    {
        $ResultPapier = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonOffresPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonOffresPeer::STATUT_OFFRES, Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);
        if ($idOffre) {
            $c->add(CommonOffresPeer::ID, $idOffre, Criteria::EQUAL);
        }
        $ObjetsCriteria = CommonOffresPeer::doSelect($c, $connexionCom);

        $Result = [];
        if (is_array($ObjetsCriteria) && 0 != count($ObjetsCriteria)) {
            foreach ($ObjetsCriteria as $ObjetCriteria) {
                if (!in_array($ObjetCriteria->getMailsignataire(), $Result) && $ObjetCriteria->getMailsignataire() && '' != $ObjetCriteria->getMailsignataire()) {
                    $Result[] = $ObjetCriteria->getMailsignataire();
                }
            }
        }
        //Ajout des depots papier
        if ($depotPapier) {
            $criteria = new Criteria();
            $criteria->add(CommonOffrePapierPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
            $criteria->add(CommonOffrePapierPeer::ORGANISME, $organisme, Criteria::EQUAL);
            $newObjetCriteria = CommonOffrePapierPeer::doSelect($criteria, $connexionCom);
            if (is_array($newObjetCriteria) && 0 != count($newObjetCriteria)) {
                foreach ($newObjetCriteria as $ObjetCriteria) {
                    if (!in_array($ObjetCriteria->getEmail(), $Result) && $ObjetCriteria->getEmail() && '' != $ObjetCriteria->getEmail()) {
                        $ResultPapier[] = $ObjetCriteria->getEmail();
                    }
                }
            }
            if (is_array($Result) && is_array($ResultPapier)) {
                $Result = [...$Result, ...$ResultPapier];
            }
        }

        return implode(' , ', $Result);
    }

    /**
     * Retourne l'objet message'.
     *
     * @param int  $Id        : correspend a l'id du message,
     * @param bool $organisme : l'organisme de l'agent connecte,
     *
     * @return objet
     */
    public static function retrieveMessageById($IdMessage, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        return CommonEchangePeer::retrieveByPK($IdMessage, $connexionCom);
    }

    /**
     * Récupere des objets pj à partir de l'idBlob, de l'organisme et de l'id message.
     *
     * @param  $idpj : idBlob de la pièce jointe
     * @param $org : Organisme
     * @param $idEchange : Identifiant de l'echange
     *
     * @return array: un tableau d'objets CommonEchangePieceJointe
     */
    public static function getPjByIdPiece($idpj, $org, $idEchange = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEchangePieceJointePeer::PIECE, $idpj, Criteria::EQUAL);
        $c->add(CommonEchangePieceJointePeer::ORGANISME, $org, Criteria::EQUAL);
        if ($idEchange) {
            $c->add(CommonEchangePieceJointePeer::ID_MESSAGE, $idEchange, Criteria::EQUAL);
        }
        $PJ = CommonEchangePieceJointePeer::doSelect($c, $connexionCom);

        return $PJ[0];
    }

    public static function getPjByIdEchange($idIchange, $org)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEchangePieceJointePeer::ID_MESSAGE, $idIchange, Criteria::EQUAL);
        $c->add(CommonEchangePieceJointePeer::ORGANISME, $org, Criteria::EQUAL);
        $PJs = CommonEchangePieceJointePeer::doSelect($c, $connexionCom);

        return $PJs;
    }

    /**
     * supprime l'objet pj a partir de l'id passe en parametre.
     *
     * @param  $id id de la pj a supprimer
     * @param $org Organismes
     *
     * @return bool
     */
    public static function DeletePj($idpj, $org)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $PJ = CommonEchangePieceJointePeer::retrieveByPk($idpj, $org, $connexionCom);
        if (is_object($PJ)) {
            $PJ->delete($connexionCom);
        }
    }

    /**
     * Retourne la liste des Formats.
     *
     * @param bool $organisme : l'organisme de l'agent connecte,
     *
     * @return array
     */
    public static function getAllFormatsEchange($organisme, $plateForme = null)
    {
        $resultat = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $resultCriterias = CommonEchangeFormatPeer::doSelect($c, $connexionCom);

        foreach ($resultCriterias as $resultCriteria) {
            if ($plateForme) {
                $resultat[$resultCriteria->getId()] = $resultCriteria->getLibelle();
            } else {
                if ($resultCriteria->getId() != Atexo_Config::getParameter('ECHANGE_PLATE_FORME')) {
                    $resultat[$resultCriteria->getId()] = $resultCriteria->getLibelle();
                }
            }
        }

        return $resultat;
    }

    /**
     * Retourne la liste des accuses de reception.
     *
     * @param bool $organisme : l'organisme de l'agent connecte,
     *
     * @return array
     */
    public static function getAllTypesAR($organisme)
    {
        $resultat = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $resultCriterias = CommonEchangeTypeARPeer::doSelect($c, $connexionCom);

        foreach ($resultCriterias as $resultCriteria) {
            $resultat[$resultCriteria->getId()] = $resultCriteria->getLibelle();
        }

        return $resultat;
    }

    public static function getFormatEchangeById($IdFormat, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonEchangeFormatPeer::ID, $IdFormat, Criteria::EQUAL);
        $c->add(CommonEchangeFormatPeer::ORGANISME, $organisme);
        $format = CommonEchangeFormatPeer::doSelect($c, $connexion);

        return $format[0];
    }

    public static function getTypeARById($IdTypeAR, $organisme)
    {
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonEchangeFormatPeer::ORGANISME, $organisme);
        $c->add(CommonEchangeFormatPeer::ID, $IdTypeAR, Criteria::EQUAL);
        $format = CommonEchangeFormatPeer::doSelect($c, $connexionOrg);

        return $format[0];
    }

    public static function getAllEchangesDestinataireByIdRef($consultationId, $organisme, $criterTri = null, $addLimit = true, $resultLimeted = true, $idInscrit = null, $callFromPortail = false, $typeMessage = null)
    {
        $param = [];
        try {
            if ($callFromPortail) {
                $emailInscrit = '';
                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($idInscrit);
                if ($inscrit instanceof CommonInscrit) {
                    $emailInscrit = $inscrit->getEmail();
                }
            }
            if (!$addLimit) {
                $sql = ' SELECT COUNT(EchangeDestinataire.ID) as numberOfElement ';
            } else {
                $sql = ' SELECT EchangeDestinataire.ID, EchangeDestinataire.UID, EchangeDestinataire.ID_ECHANGE, EchangeDestinataire.MAIL_DESTINATAIRE, EchangeDestinataire.AR,
        			 EchangeDestinataire.DATE_AR, EchangeDestinataire.TYPE_AR, Echange.OBJET, Echange.EXPEDITEUR, Echange.DATE_MESSAGE, Echange.FORMAT, Echange.STATUS, Echange.CORPS ';
            }
            $sql .= ' FROM EchangeDestinataire, Echange ';
            $sql .= ' WHERE EchangeDestinataire.ID_ECHANGE=Echange.ID AND EchangeDestinataire.organisme=Echange.organisme ';
            $sql .= ' AND Echange.CONSULTATION_ID=:consultation_id ';
            $sql .= ' AND Echange.organisme = :acronymeOrg ';
            if (!empty($typeMessage)) {
                $sql .= ' AND Echange.id_type_message = :typeMessage ';
                $param[':typeMessage'] = $typeMessage;
            }
            if ($callFromPortail) {
                if ($emailInscrit) {
                    $sql .= " AND ( Echange.email_expediteur = '$emailInscrit' OR EchangeDestinataire.mail_destinataire = '$emailInscrit' ) ";
                } else {
                    $sql .= ' AND 0 ';
                }
            }
            $param[':consultation_id'] = $consultationId;
            $param[':acronymeOrg'] = $organisme;
            if ($criterTri['FORMAT']) {
                $sql .= 'AND Echange.FORMAT =:format ';
                $param[':format'] = $criterTri['FORMAT'];
            }
            if ($criterTri['DATE_MESSAGE']) {
                $sql .= 'AND Echange.DATE_MESSAGE LIKE :DATE_MESSAGE ';
                $param[':DATE_MESSAGE'] = $criterTri['DATE_MESSAGE'].'%';
            }
            if ($criterTri['OBJET']) {
                $sql .= 'AND Echange.OBJET =:OBJET ';
                $param[':OBJET'] = $criterTri['OBJET'];
            }
            if ($criterTri['MAIL_DESTINATAIRE']) {
                $sql .= 'AND EchangeDestinataire.MAIL_DESTINATAIRE =:MAIL_DESTINATAIRE ';
                $param[':MAIL_DESTINATAIRE'] = $criterTri['MAIL_DESTINATAIRE'];
            }
            if ($criterTri['De']) {
                $sql .= 'ORDER BY  Echange.EXPEDITEUR '.$criterTri['De'].' ';
            }
            if ($criterTri['DATE']) {
                $sql .= 'ORDER BY  Echange.DATE_MESSAGE '.$criterTri['DATE'].' ';
            }
            if ($criterTri['A']) {
                $sql .= 'ORDER BY  EchangeDestinataire.MAIL_DESTINATAIRE '.$criterTri['A'].' ';
            }
            if ($addLimit && $resultLimeted) {
                $sql .= ' limit '.$criterTri['offSet'].','.$criterTri['limit'].' ';
            }

            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $statement->execute($param);

            if (!$addLimit) {
                $nombreElement = 0;

                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $nombreElement = $row['numberOfElement'];
                }

                return $nombreElement;
            } else {
                $arrayDestinataire = [];
                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $arrayDestinataire[] = $row;
                }

                return $arrayDestinataire;
            }
        } catch (\Exception $e) {
            $erreurMsg = 'Erreur = '.$e->getMessage().PHP_EOL.'Trace = '.$e->getTraceAsString();
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de la recuperation des echanges : {refCons = $consultationId},{organisme = $organisme}, {idInscrit = $idInscrit}, {typeMessage = $typeMessage}".PHP_EOL.$erreurMsg);
        }
    }

    public static function getEchangeDestinataireByUid($uid, $org)
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonEchangeDestinatairePeer::UID, $uid, Criteria::EQUAL);
            $c->add(CommonEchangeDestinatairePeer::ORGANISME, $org, Criteria::EQUAL);
            $EchangesDest = CommonEchangeDestinatairePeer::doSelect($c, $connexionCom);

            return $EchangesDest[0];
        } catch (\Exception $e) {
            Prado::log('Erreur Message.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Message.php');
        }
    }

    public static function getEchangeDestinataireByid($id, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEchangeDestinatairePeer::ID, $id, Criteria::EQUAL);
        $c->add(CommonEchangeDestinatairePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $EchangesDest = CommonEchangeDestinatairePeer::doSelect($c, $connexionCom);

        return $EchangesDest[0];
    }

    /**
     * supprime l'objet destinataire a partir de l'id passe en parametre.
     *
     * @param  $id id du destinataire a supprimer
     * @param $org Organismes
     *
     * @return bool
     */
    public static function deleteObjetDestinataire($iddestinataire)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $echangeDestinataire = CommonEchangeDestinatairePeer::retrieveByPk($iddestinataire, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        if (is_object($echangeDestinataire)) {
            $echangeDestinataire->delete($connexionCom);
        }
    }

    public function getInfoConsultation($reference, $org, $callFromAgent = false)
    {
        $consultation = (new Atexo_Consultation())->retrieveCommonConsultation($reference, $org);
        if (null != $consultation && $consultation instanceof CommonConsultation) {
            return self::getInfoConsultationFromObject($consultation, $callFromAgent, true);
        }

        return '';
    }

    public function getInfoConsultationV2($reference, $org, $callFromAgent = false, $corpsEmail = '')
    {
        $consultation = (new Atexo_Consultation())->retrieveCommonConsultation($reference, $org);
        if (null != $consultation && $consultation instanceof CommonConsultation) {
            return self::getInformationsConsultation($consultation, $callFromAgent, true, $corpsEmail);
        }

        return '';
    }

    /**
     * @param bool $callFromAgent
     * @param bool $withDateFin
     *
     * @return string
     *
     * @throws PropelException
     *
     * @author updated by Mohamed BLAL
     * @Deprecated
     */
    public function getInfoConsultationFromObject(CommonConsultation $consultation, $callFromAgent = false, $withDateFin = true)
    {
        $langue = null;
        $string = '';
        $string = Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' : ';
        if (Atexo_Module::isEnabled('AnnoncesMarches')) {
            $string .= $consultation->getOrgDenomination();
        } else {
            $entityPurchase = Atexo_EntityPurchase::retrieveEntityById($consultation->getServiceId(), $consultation->getOrganisme());
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($consultation->getOrganisme());
            $string .= $organismeO->getSigle().' - '.$organismeO->getDenominationOrgTraduit();
            $string .= '
';
            if ($entityPurchase) {
                $string .= Prado::localize('DEFINE_ENTITE_ACHAT').' : '.$entityPurchase->getSigle().' - '.$entityPurchase->getLibelleTraduit();
            } else {
                $string .= Prado::localize('DEFINE_ENTITE_ACHAT').' : '.$organismeO->getSigle().' - '.$organismeO->getDenominationOrgTraduit();
            }
        }
        $string .= '
';
        $string .= Prado::localize('DEFINE_INTITULE_CONSULTATION').' : '.$consultation->getIntituleTraduit();
        $string .= '
';
        $string .= Prado::localize('DEFINE_OBJET_CONSULTATION').' : '.$consultation->getObjetTraduit();
        $string .= '
';
        $string .= Prado::localize('REFERENCE_CONSULTATION').' : '.$consultation->getReferenceUtilisateur();
        $string .= '
';

        $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedure(), true, null, null);

        $string .= Prado::localize('TEXT_TYPE_PROCEDURE').' : '.$typeProcedure;

        $string .= '
';

        if (Atexo_Module::isEnabled('PubliciteOpoce')) {
            $string .= Prado::localize('DEFINE_DATE_MISE_EN_LIGNE').' '.Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneSouhaitee());
            $string .= '
';
        } else {
            $string .= Prado::localize('DEFINE_DATE_MISE_EN_LIGNE').' '.Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneCalcule());
            $string .= '
';
        }
        if ($withDateFin) {
            $dateLimite = '';
            if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                $dateLimite = Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLI');
            } else {
                $dateLimite = Prado::localize('DEFINE_TEXT_DATE_FIN_AFFICHAGE');
            }
            $string .= $dateLimite.' : '.Atexo_Util::iso2frnDateTime($consultation->getDatefin());
        }

        return $string;
    }

    /**
     * Generate an Html email format from a static template.
     *
     * @param bool $callFromAgent
     * @param bool $withDateFin
     * @param $corpsMail
     *
     * @return mixed
     *
     * @throws PropelException
     */
    public function _getInfoConsultationFromObject(CommonConsultation $consultation, $callFromAgent = false, $withDateFin = true, $corpsMail = '')
    {
        $langue = null;
        $corpsMail = str_replace('DEFINE_TEXT_ENTITE_PUBLIC', Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC'), $corpsMail);
        $corpsMail = str_replace('DEFINE_ENTITE_ACHAT', Prado::localize('DEFINE_ENTITE_ACHAT'), $corpsMail);

        if (Atexo_Module::isEnabled('AnnoncesMarches')) {
            $corpsMail = str_replace('VALEUR_ENTITE_PUBLIC', $consultation->getOrgDenomination(), $corpsMail);
        } else {
            $entityPurchase = Atexo_EntityPurchase::retrieveEntityById($consultation->getServiceId(), $consultation->getOrganisme());
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($consultation->getOrganisme());
            $corpsMail = str_replace('VALEUR_ENTITE_PUBLIC', $organismeO->getSigle().' - '.$organismeO->getDenominationOrgTraduit(), $corpsMail);
            if ($entityPurchase) {
                $corpsMail = str_replace('VALEUR_ENTITE_ACHAT', $entityPurchase->getSigle().' - '.$entityPurchase->getLibelleTraduit(), $corpsMail);
            } else {
                $corpsMail = str_replace('VALEUR_ENTITE_ACHAT', $organismeO->getSigle().' - '.$organismeO->getDenominationOrgTraduit(), $corpsMail);
            }
        }
        $corpsMail = str_replace('DEFINE_INTITULE_CONSULTATION', Prado::localize('DEFINE_INTITULE_CONSULTATION'), $corpsMail);
        $corpsMail = str_replace('VALEUR_INTITULE_CONSULTATION', $consultation->getIntituleTraduit(), $corpsMail);
        $corpsMail = str_replace('DEFINE_OBJET_CONSULTATION', Prado::localize('DEFINE_OBJET_CONSULTATION'), $corpsMail);
        $corpsMail = str_replace('VALEUR_OBJET_CONSULTATION', $consultation->getObjetTraduit(), $corpsMail);
        $corpsMail = str_replace('REFERENCE_CONSULTATION', Prado::localize('REFERENCE_CONSULTATION'), $corpsMail);
        $corpsMail = str_replace('VALEUR_REFERENCE_CONSULTATION', $consultation->getReferenceUtilisateur(), $corpsMail);

        $typeProcedure = '';
        if ($callFromAgent) {
            $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $consultation->getOrganisme(), null);
        } else {
            $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedure(), true, null, null);
        }

        $corpsMail = str_replace('TEXT_TYPE_PROCEDURE', Prado::localize('TEXT_TYPE_PROCEDURE'), $corpsMail);
        $corpsMail = str_replace('VALEUR_TYPE_PROCEDURE', $typeProcedure, $corpsMail);

        $corpsMail = str_replace('DEFINE_DATE_MISE_EN_LIGNE', Prado::localize('DEFINE_DATE_MISE_EN_LIGNE'), $corpsMail);
        if (Atexo_Module::isEnabled('PubliciteOpoce')) {
            $corpsMail = str_replace('VALEUR_DATE_MISE_EN_LIGNE', Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneSouhaitee()), $corpsMail);
        } else {
            $corpsMail = str_replace('VALEUR_DATE_MISE_EN_LIGNE', Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneCalcule()), $corpsMail);
        }
        if ($withDateFin) {
            $dateLimite = '';
            if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                $corpsMail = str_replace('DEFINE_DATE_LIMITE', Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLI'), $corpsMail);
            } else {
                $corpsMail = str_replace('DEFINE_DATE_LIMITE', Prado::localize('DEFINE_TEXT_DATE_FIN_AFFICHAGE'), $corpsMail);
            }
            $corpsMail = str_replace('VALEUR_DATE_LIMITE', Atexo_Util::iso2frnDateTime($consultation->getDatefin()), $corpsMail);
        }
        $placeMarchePublicInterministere = Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');

        if ($consultation instanceof CommonConsultation
            && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
            $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
            $placeMarchePublicInterministere = $plateformeVirtuelle->getFooterMail();
        }

        $corpsMail = str_replace(
            'PLACE_MARCHE_PUBLIC_INTERMINISTERE',
            $placeMarchePublicInterministere,
            $corpsMail
        );

        return $corpsMail;
    }

    /**
     * @param bool $callFromAgent
     * @param bool $withDateFin
     * @param $corpsMail
     *
     * @return mixed
     *
     * @throws PropelException
     */
    public function getInformationsConsultation(CommonConsultation $consultation, $callFromAgent = false, $withDateFin = true, $corpsMail = '')
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');

        if (Atexo_Module::isEnabled('AnnoncesMarches')) {
            $corpsMail = str_replace('VALEUR_ENTITE_PUBLIC', $consultation->getOrgDenomination(), $corpsMail);
        } else {
            $entityPurchase = Atexo_EntityPurchase::retrieveEntityById($consultation->getServiceId(), $consultation->getOrganisme());
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($consultation->getOrganisme());
            $corpsMail = str_replace('VALEUR_ENTITE_PUBLIC', $organismeO->getSigle().' - '.$organismeO->getDenominationOrgTraduit(), $corpsMail);

            if ($entityPurchase) {
                $corpsMail = str_replace('VALEUR_ENTITE_ACHAT', $entityPurchase->getSigle().' - '.$entityPurchase->getLibelleTraduit(), $corpsMail);
            } else {
                $corpsMail = str_replace('VALEUR_ENTITE_ACHAT', $organismeO->getSigle().' - '.$organismeO->getDenominationOrgTraduit(), $corpsMail);
            }
        }
        $corpsMail = str_replace('VALEUR_INTITULE_CONSULTATION', $consultation->getIntituleTraduit(), $corpsMail);
        $corpsMail = str_replace('VALEUR_OBJET_CONSULTATION', $consultation->getObjetTraduit(), $corpsMail);
        $corpsMail = str_replace('VALEUR_REFERENCE_CONSULTATION', $consultation->getReferenceUtilisateur(), $corpsMail);

        $typeProcedure = '';
        if ($callFromAgent) {
            $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $consultation->getOrganisme(), $langue);
        } else {
            $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedure(), true, null, $langue);
        }
        $corpsMail = str_replace('VALEUR_TYPE_PROCEDURE', $typeProcedure, $corpsMail);

        if (Atexo_Module::isEnabled('PubliciteOpoce')) {
            $corpsMail = str_replace('VALEUR_DATE_MISE_EN_LIGNE', Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneSouhaitee()), $corpsMail);
        } else {
            $corpsMail = str_replace('VALEUR_DATE_MISE_EN_LIGNE', Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneCalcule()), $corpsMail);
        }
        if ($withDateFin) {
            /*$dateLimite = "";
            if($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')){
                $dateLimite = Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLI');
            } else {
                $dateLimite = Prado::localize('DEFINE_TEXT_DATE_FIN_AFFICHAGE');
            }*/
            $corpsMail = str_replace('VALEUR_HORODATE_LIMITE_REMISE_PLIS', Atexo_Util::iso2frnDateTime($consultation->getDatefin()), $corpsMail);
        } else {
            $corpsMail = str_replace('VALEUR_HORODATE_LIMITE_REMISE_PLIS', '', $corpsMail);
        }

        return $corpsMail;
    }

    public function getBasPage($placeMarchePublicInterministere = null)
    {
        $string = '';
        $string .= '
';
        $string .= Prado::localize('MESSAGE_REMERCIMENT');
        $string .= '
';
        $string .= $placeMarchePublicInterministere;
        $string .= '
';
        $string .= '
';
        $string .= '
';

        return $string;
    }

    public static function getMessageModificationCompte($agent, $pwd = false, $actif = true)
    {
        $corpsEmail = file_get_contents(Atexo_Config::getParameter('PATH_FILE_MAIL_MODIFICATION_COMPTE_AGENT'));
        $corpsEmail = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $corpsEmail);
        $corpsEmail = str_replace('DEFINE_TEXT_BONJOUR', Prado::localize('DEFINE_TEXT_BONJOUR'), $corpsEmail);
        $corpsEmail = str_replace('TEXT_PASSWORD', Prado::localize('TEXT_PASSWORD'), $corpsEmail);
        $pass = '';
        if(!$actif) {
            $text = Prado::localize('DEFINE_TEXT_HEADER_MESSAGE_DESACTIVATION_MON_COMPTE');
        } else {
            if ($pwd) {
                $text = Prado::localize('DEFINE_TEXT_HEADER_MESSAGE_CREATION_COMPTE_AGENT');
                $pass = Prado::localize('TEXT_PASSWORD').' '.Prado::localize('GENERATION_AUTOMATIQUE_PWD').' : '.$pwd;
            } else {
                $text = Prado::localize('DEFINE_TEXT_HEADER_MESSAGE_CREATION_MODIFICATION_MON_COMPTE');
            }
        }
        $corpsEmail = str_replace(
            'DEFINE_TEXT_HEADER_MESSAGE_CREATION_MODIFICATION_MON_COMPTE',
           $text,
            $corpsEmail
        );
        $corpsEmail = str_replace('VALEUR_PASSWORD', $pass, $corpsEmail);
        $corpsEmail = str_replace('DEFINE_PRENOM', Prado::localize('DEFINE_PRENOM'), $corpsEmail);
        $corpsEmail = str_replace('VALEUR_PRENOM', $agent->getPrenom(), $corpsEmail);
        $corpsEmail = str_replace('DEFINE_NOM_MAJ', Prado::localize('DEFINE_NOM_MAJ'), $corpsEmail);
        $corpsEmail = str_replace('VALEUR_NOM_MAJ', $agent->getNom(), $corpsEmail);
        $corpsEmail = str_replace('TEXT_ADRESSE_ELECTRONIQUE', Prado::localize('TEXT_ADRESSE_ELECTRONIQUE'), $corpsEmail);
        $corpsEmail = str_replace('VALEUR_ADRESSE_ELECTRONIQUE', $agent->getEmail(), $corpsEmail);
        $corpsEmail = str_replace('DEFINE_TEXT_IDENTIFIANT', Prado::localize('DEFINE_TEXT_IDENTIFIANT'), $corpsEmail);
        $corpsEmail = str_replace('VALEUR_IDENTIFIANT', $agent->getLogin(), $corpsEmail);

        $entityPurchase = Atexo_EntityPurchase::retrieveEntityById($agent->getServiceId(), $agent->getOrganisme());
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($agent->getOrganisme());

        $corpsEmail = str_replace('DEFINE_TEXT_ENTITE_PUBLIC', Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC'), $corpsEmail);
        $corpsEmail = str_replace('VALEUR_ENTITE_PUBLIC', Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' : '.$organismeO->getSigle().' - '.$organismeO->getDenominationOrg(), $corpsEmail);
        $corpsEmail = str_replace('DEFINE_ENTITE_ACHAT', Prado::localize('DEFINE_ENTITE_ACHAT'), $corpsEmail);
        if ($entityPurchase) {
            $corpsEmail = str_replace('VALEUR_ENTITE_ACHAT', Prado::localize('DEFINE_ENTITE_ACHAT').' : '.$entityPurchase->getSigle().' - '.$entityPurchase->getLibelle(), $corpsEmail);
        } else {
            $corpsEmail = str_replace('VALEUR_ENTITE_ACHAT', Prado::localize('DEFINE_ENTITE_ACHAT').' : '.$organismeO->getSigle().' - '.$organismeO->getDenominationOrg(), $corpsEmail);
        }
        $corpsEmail = str_replace('DEFINE_TEXT_FIN_MESSAGE_CREATION_MODIFICATION_MON_COMPTE', Prado::localize('DEFINE_TEXT_FIN_MESSAGE_CREATION_MODIFICATION_MON_COMPTE'), $corpsEmail);
        $corpsEmail = str_replace('CORDIALEMENT', Prado::localize('CORDIALEMENT'), $corpsEmail);
        $corpsEmail = str_replace('BAS_PAGE_MAIL', Prado::localize('BAS_PAGE_MAIL'), $corpsEmail);
        $corpsEmail = str_replace('PLACE_MARCHE_PUBLIC_INTERMINISTERE', Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE'), $corpsEmail);

        return $corpsEmail;
    }

    public static function getMessageSoliciteDesEntreprise($ref, $org)
    {
        $msg = Prado::localize('DEFINE_TEXT_BONJOUR');
        $msg .= '
';
        $msg .= '
';
        $msg .= Prado::localize('DEFINE_TEXT_HEADER_MESSAGE_REPONSE_ENTREPRISE');
        $msg .= '
';
        $msg .= '
';
        $msg .= (new self())->getInfoConsultation($ref, $org);
        $msg .= '
';
        $msg .= '
';
        $msg .= Prado::localize('DEFINE_TEXT_NB_MESSAGE_REPONSE_ENTREPRISE');
        $msg .= '
';
        $msg .= '
';
        $placeMarchePublicInterministere = Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');
        $consultation = (new Atexo_Consultation())->retrieveCommonConsultation($ref, $org);

        if ($consultation instanceof CommonConsultation
            && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
            $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
            $placeMarchePublicInterministere = $plateformeVirtuelle->getFooterMail();
        }

        $msg .= (new self())->getBasPage($placeMarchePublicInterministere);

        return $msg;
    }

    public static function retreiveNotificationByIdRef($consultationId, $organisme)
    {
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonEchangePeer::ID_TYPE_MESSAGE, Atexo_Config::getParameter('TYPE_MESSAGE_NOTIFICATION'));
        $c->add(CommonEchangePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonEchangePeer::ORGANISME, $organisme);
        $notification = CommonEchangePeer::doSelect($c, $connexionOrg);
        if ('' != $notification) {
            return $notification;
        } else {
            return false;
        }
    }

    /**
     * Genere un fichier txt du message.
     *
     * @param objet $consultation
     * @param $organisme
     */
    public function generateNotificationFile(CommonEchange $echange, $organisme, $filePath = false)
    {
        $trimMailDestinataire = null;
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        if (!$filePath) {
            $filePath = str_replace(' ', '', Atexo_Config::getParameter('COMMON_TMP').'notice1_'.session_id().microtime().'.txt');
        }

        //Recuperation des destinataires
        $c = new Criteria();
        $c->add(CommonEchangeDestinatairePeer::ID_ECHANGE, $echange->getId());
        $c->add(CommonEchangeDestinatairePeer::ORGANISME, $organisme);
        $destinataires = CommonEchangeDestinatairePeer::doSelect($c, $connexionOrg);

        $mailDestinataire = '';
        foreach ($destinataires as $destinataire) {
            $mailDestinataire .= $destinataire->getMailDestinataire().';';
        }
        if ($mailDestinataire) {
            $trimMailDestinataire = trim($mailDestinataire, ';');
        }
        $msg = 'Destinataires : '.$trimMailDestinataire."\r\n\r\n";
        $msg .= 'Objet : '.$echange->getObjet()."\r\n\r\n\r\n";
        $msg .= $echange->getCorps();
        Atexo_Util::write_file($filePath, $msg);

        //Recuperation des pieces jointes relatives au message
        $c = new Criteria();
        $c->add(CommonEchangePieceJointePeer::ID_MESSAGE, $echange->getId());
        $c->add(CommonEchangePieceJointePeer::ORGANISME, $organisme);
        $PJ = CommonEchangePieceJointePeer::doSelect($c, $connexionOrg);
        $arrayPj = [];
        $index = 0;
        foreach ($PJ as $onePJ) {
            $idBlob = $onePJ->getPiece();
            $nameFile = $onePJ->getNomFichier();
            $idMessage = $onePJ->getIdMessage();
            $arrayPj[$index][0] = $idBlob;
            $arrayPj[$index][1] = $nameFile;
            $arrayPj[$index][2] = $idMessage;
            ++$index;
        }

        return ['mailNotification' => $filePath, 'piecesjointes' => $arrayPj];
    }

    public function getBodyForQuestionEntreprise($objectQuestion)
    {
        $string = Prado::localize('TEXT_PERSONNE_DEPOSEE_QUESTION');
        $string .= '
';
        $string .= Prado::localize('DEFINE_NOM').' : '.$objectQuestion->getNom();
        $string .= '
';
        $string .= Prado::localize('DEFINE_PRENOM').' : '.$objectQuestion->getPrenom();
        $string .= '
';
        $string .= Prado::localize('TEXT_ADRESSE_ELECTRONIQUE').' : '.$objectQuestion->getEmail();
        $string .= '

';
        $string .= Prado::localize('TEXT_CONTENU_QUESTION').' : ';
        $string .= '
';
        $string .= $objectQuestion->getQuestion();
        $string .= '

';
        if ($objectQuestion->getIdFichier()) {
            $pjExist = Prado::localize('TEXT_OUI');
        } else {
            $pjExist = Prado::localize('TEXT_NON');
        }
        $string .= Prado::localize('TEXT_PJ_DISPO_QUESTION').' : '.$pjExist;
        $string .= '

';
        $string .= Prado::localize('TEXT_DATE_DEPOT_QUESTION').' : '.Atexo_Util::iso2frnDateTime($objectQuestion->getDateDepot());

        return $string;
    }

    /*
     * Envoi d'un mail a l'inscrit pour rappeler les coordonnees principales
        d'inscription
     */
    public static function getMessageConfirmationInscrit($inscrit, $entreprise, $pwd = false, $update = false)
    {
        $msg = '';
        if ($update) {
            $msg .= Prado::localize('DEFINE_TEXT_HEADER_MESSAGE_MODIFICATION_INSCRIT');
            $msg .= '
';
        } else {
            $msg .= Prado::localize('DEFINE_TEXT_HEADER_MESSAGE_CONFIRMATION_INSCRIT');
            $msg .= '
';
        }

        $msg .= Prado::localize('DEFINE_PRENOM').' : '.$inscrit->getPrenom();
        $msg .= '
';
        $msg .= Prado::localize('TEXT_NOM').' '.$inscrit->getNom();
        $msg .= '
';
        $msg .= Prado::localize('TEXT_ADRESSE_ELECTRONIQUE').' : '.$inscrit->getEmail();
        $msg .= '
';
        $msg .= Prado::localize('TEXT_IDENTIFIANT').' : '.$inscrit->getLogin();
        $msg .= '

';
        if ($pwd) {
            $msg .= Prado::localize('TEXT_PASSWORD').' '.Prado::localize('GENERATION_AUTOMATIQUE_PWD').' : '.$pwd;
            $msg .= '
';
        }

        $msg .= Prado::localize('DEFINE_TEXT_CORPS1_MESSAGE_CONFIRMATION_INSCRIT').' : '.$entreprise->getNom();
        $msg .= '

';
        if (Atexo_Module::isEnabled('EntrepriseValidationEmailInscription')) {
            $msg .= Prado::localize('MESSAGE_VALIDATION_COMPTE_INSCRIT_ENTREPRISE');
            $msg .= '
';
            $pfReference =  Atexo_Config::getParameter('PF_URL_AGENT') != "/" ?
                Atexo_Config::getParameter('PF_URL_AGENT') :
                Atexo_Config::getParameter('PF_URL_REFERENCE');

	  	  		$msg .= Atexo_MultiDomaine::replaceDomain(
                        $pfReference
                    )
                    . '?page=Entreprise.EntrepriseValidationCompteInscrit&uid='
                    . $inscrit->getUid();
            $msg .= '

';
        }

        $msg .= Prado::localize('DEFINE_TEXT_CORPS2_MESSAGE_CONFIRMATION_INSCRIT');
        $msg .= '

';

        return $msg;
    }

    /*
 * recupere la liste des objets d'ECHANGE.
 */
    public static function getAllObjetsEchanges($idRef, $criterTri = null)
    {
        $querySelect = ' SELECT DISTINCT (objet) from EchangeDestinataire, Echange ';
        $querySelect .= " WHERE consultation_id='".(new Atexo_Db())->quote($idRef)."'";
        $querySelect .= " AND objet <>''";
        $querySelect .= " AND EchangeDestinataire.ID_ECHANGE=Echange.ID AND EchangeDestinataire.organisme=Echange.organisme and Echange.organisme = '".Atexo_CurrentUser::getCurrentOrganism()."' ";

        if ($criterTri['De']) {
            if (0 == strcmp($criterTri['De'], 'DESC')) {
                $querySelect .= 'ORDER BY objet DESC';
            } elseif (0 == strcmp($criterTri['De'], 'ASC')) {
                $querySelect .= 'ORDER BY objet ASC';
            }
        }
        $results = Atexo_Db::getLinkCommon(true)->query($querySelect, PDO::FETCH_OBJ);
        $objets = [];
        if ($results) {
            foreach ($results as $row) {
                $objets[] = $row->objet;
            }
        }

        return $objets;
    }

    /*
 * recupere la liste des objets d'ECHANGE.
 */
    public static function getAllDatesMessage($idRef, $criterTri = null)
    {
        $querySelect = ' SELECT DISTINCT substr(date_message,1,10) as date_message from Echange ';
        $querySelect .= " WHERE consultation_id='".(new Atexo_Db())->quote($idRef)."'";
        $querySelect .= " AND date_message <>'' and Echange.organisme = '".Atexo_CurrentUser::getCurrentOrganism()."' ";

        if ($criterTri['De']) {
            if (0 == strcmp($criterTri['De'], 'DESC')) {
                $querySelect .= 'ORDER BY date_message DESC';
            } elseif (0 == strcmp($criterTri['De'], 'ASC')) {
                $querySelect .= 'ORDER BY date_message ASC';
            }
        }
        $results = Atexo_Db::getLinkCommon(true)->query($querySelect, PDO::FETCH_OBJ);
        $dates = [];
        if ($results) {
            foreach ($results as $row) {
                $dates[] = Atexo_Util::iso2frnDate($row->date_message);
            }
        }

        return $dates;
    }

    /*
    * recupere la liste des Expediteurs.
    */
    public static function getAllMailDestinataire($idRef, $criterTri = null)
    {
        $querySelect = ' SELECT DISTINCT (mail_destinataire) from EchangeDestinataire inner join Echange on Echange.id=EchangeDestinataire.id_echange ';
        $querySelect .= " WHERE consultation_id='".(new Atexo_Db())->quote($idRef)."' and Echange.organisme='".Atexo_CurrentUser::getCurrentOrganism()."' ";

        if ($criterTri['De']) {
            if (0 == strcmp($criterTri['De'], 'DESC')) {
                $querySelect .= 'ORDER BY mail_destinataire DESC';
            } elseif (0 == strcmp($criterTri['De'], 'ASC')) {
                $querySelect .= 'ORDER BY mail_destinataire ASC';
            }
        }

        $results = Atexo_Db::getLinkCommon(true)->query($querySelect, PDO::FETCH_OBJ);
        $destinataires = [];
        if ($results) {
            foreach ($results as $row) {
                $destinataires[] = $row->mail_destinataire;
            }
        }

        return $destinataires;
    }

    /*
     * Envoi d'un mail a l'inscrit pour rappeler les coordonnaes principales
        d'inscription
     */
    public static function getMessageConfirmationInscritWithCompanyInfo($inscrit, $entreprise, $pwd = false, $update = false)
    {
        $msg = '';
        if ($update) {
            $msg .= Prado::localize('DEFINE_TEXT_HEADER_MESSAGE_MODIFICATION_INSCRIT');
            $msg .= '
';
        } else {
            $msg .= Prado::localize('DEFINE_TEXT_MESSAGE_CONFIRMATION_INSCRIT');
            $msg .= '
';
        }
        $msg .= '
';

        $msg .= Prado::localize('TEXT_ENTREPRISE').':';
        $msg .= '
';
        $msg .= Prado::localize('DEFINE_TEXT_CORPS1_MESSAGE_CONFIRMATION_INSCRIT').' : '.$entreprise->getNom();
        $msg .= '
';
        $msg .= Prado::localize('TEXT_ADRESSE').' : '.$entreprise->getAdresse();
        $msg .= '
';
        $msg .= Prado::localize('TEXT_VILLE').' : '.$entreprise->getVilleadresse();
        $msg .= '
';
        if ($entreprise->getTelephone()) {
            $msg .= Prado::localize('TEXT_TEL').' : '.$entreprise->getTelephone();
            $msg .= '
';
        }
        if ($entreprise->getCnss()) {
            $msg .= Prado::localize('TEXT_CNSS').' : '.$entreprise->getCnss();
            $msg .= '
';
        }
        if ($entreprise->getRcNum() && $entreprise->getRcVille()) {
            $msg .= Prado::localize('TEXT_RC_NUM_VILLE').' : '.$entreprise->getRcNum().' / '.$entreprise->getRcVille();
            $msg .= '
';
        }
        $msg .= '
';
        $msg .= '
';

        $msg .= Prado::localize('DEFINE_UTILISATEUR_ADMINISTRATEUR').' : ';
        $msg .= '
';
        $msg .= Prado::localize('DEFINE_PRENOM').' : '.$inscrit->getPrenom();
        $msg .= '
';
        $msg .= Prado::localize('TEXT_NOM').' '.$inscrit->getNom();
        $msg .= '
';
        $msg .= Prado::localize('TEXT_ADRESSE_ELECTRONIQUE').' : '.$inscrit->getEmail();
        $msg .= '
';
        $msg .= Prado::localize('TEXT_IDENTIFIANT').' : '.$inscrit->getLogin();
        $msg .= '

';
        if ($pwd) {
            $msg .= Prado::localize('TEXT_PASSWORD').' '.Prado::localize('GENERATION_AUTOMATIQUE_PWD').' : '.$pwd;
            $msg .= '
';
        }
        $msg .= '

';

        $msg .= Prado::localize('TEXT_BAS_MAIL_COINFIRMATION_INSCRIT_01');
        $msg .= '

';
        $msg .= Prado::localize('TEXT_BAS_MAIL_COINFIRMATION_INSCRIT_02');
        $msg .= '

';
        $msg .= Prado::localize('DEFINE_NOM_PRENOM_DIRIGENT');
        $msg .= '

';
        $msg .= Prado::localize('DEFINE_SIGNATURE_CACHET');
        $msg .= '

';
        $msg .= '

';
        $msg .= '

';

        return $msg;
    }

    public function getCorpsMessageConfirmationReponseEntreprise($arrayMail, $idIchange, $ref, $org)
    {
        $corpsMail = '';
        $corpsMail .= Prado::localize('DEFINE_REPONSE_DEMANDE_COMPLEMENT_RECU_CONSULTATION_CI_DESSOUS').' : '.'

';
        $corpsMail .= (new self())->getInfoConsultation($ref, $org, false).'

';
        $corpsMail .= Prado::localize('OBJET').' : '.$arrayMail['objetMessage'].'

';
        $corpsMail .= Prado::localize('DEFINE_CONTENU_MAIL_ACCESSIBLE_CI_DESSOUS').' : '.'

';
        $corpsMail .= $arrayMail['CorpsMessage'].'

';
        $corpsMail .= Prado::localize('DEFINE_DETAIL_PIECES').' : '.'
';
        $nomsFichiers = '';
        $pjs = self::getPjByIdEchange($idIchange, $org);

        if ($pjs && (is_countable($pjs) ? count($pjs) : 0)) {
            foreach ($pjs as $pj) {
                $nomsFichiers .= $pj->getNomFichier().' : '.Atexo_Util::arrondirSizeFile($pj->getTaille() / 1024).'
';
            }
        }
        $corpsMail .= $nomsFichiers.'
';

        $placeMarchePublicInterministere = prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');
        $consultation = (new Atexo_Consultation())->retrieveCommonConsultation($ref, $org);

        if ($consultation instanceof CommonConsultation
            && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
            $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
            $placeMarchePublicInterministere = $plateformeVirtuelle->getFooterMail();
        }

        $corpsMail .= $placeMarchePublicInterministere.'

';
        $corpsMail .= $arrayMail['CorpsMessageAR'].'
';

        return $corpsMail;
    }

    public function getCorpsMessageAlerteValidation(CommonConsultation $consultation, $validationFinal = true)
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $corpsEmail = file_get_contents(Atexo_Config::getParameter('PATH_FILE_MAIL_CONSULTATION_EN_ATTENTE'));

        //$corpsEmail = $this->render('PATH_FILE_MAIL_HEADER','PATH_FILE_MAIL_CONSULTATION_EN_ATTENTE','PATH_FILE_MAIL_FOOTER');
        $corpsEmail = str_replace('DEFINE_TEXT_BONJOUR', Prado::localize('DEFINE_TEXT_BONJOUR'), $corpsEmail);
        if ($validationFinal) {
            $corpsEmail = str_replace('DEFINE_TEXT_CONSULTATION_ATTENTE', Prado::localize('DEFINE_TEXT_CONSULTATION_ATTENTE_VALIDATION'), $corpsEmail);
        } else {
            $corpsEmail = str_replace('DEFINE_TEXT_CONSULTATION_ATTENTE', Prado::localize('DEFINE_TEXT_CONSULTATION_ATTENTE_APPROBATION_AVANT_VALIDATION'), $corpsEmail);
        }
        $entityPurchase = Atexo_EntityPurchase::retrieveEntityById($consultation->getServiceId(), $consultation->getOrganisme());
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($consultation->getOrganisme());

        $corpsEmail = str_replace('DEFINE_TEXT_ENTITE_PUBLIC', Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC'), $corpsEmail);
        $corpsEmail = str_replace('VALEUR_ENTITE_PUBLIC', $organismeO->getSigle().' - '.$organismeO->getDenominationOrg(), $corpsEmail);

        $corpsEmail = str_replace('DEFINE_ENTITE_ACHAT', Prado::localize('DEFINE_ENTITE_ACHAT'), $corpsEmail);
        if ($entityPurchase) {
            $corpsEmail = str_replace('VALEUR_ENTITE_ACHAT', $entityPurchase->getSigle().' - '.$entityPurchase->getLibelle(), $corpsEmail);
        } else {
            $corpsEmail = str_replace('VALEUR_ENTITE_ACHAT', $organismeO->getSigle().' - '.$organismeO->getDenominationOrg(), $corpsEmail);
        }
        $corpsEmail = str_replace('DEFINE_INTITULE_CONSULTATION', Prado::localize('DEFINE_INTITULE_CONSULTATION'), $corpsEmail);
        $corpsEmail = str_replace('VALEUR_INTITULE_CONSULTATION', $consultation->getIntituleTraduit(), $corpsEmail);

        $corpsEmail = str_replace('DEFINE_OBJET_CONSULTATION', Prado::localize('DEFINE_OBJET_CONSULTATION'), $corpsEmail);
        $corpsEmail = str_replace('VALEUR_OBJET_CONSULTATION', $consultation->getObjetTraduit(), $corpsEmail);

        $corpsEmail = str_replace('DEFINE_REFERENCE_CONSULTATION', Prado::localize('REFERENCE_CONSULTATION'), $corpsEmail);
        $corpsEmail = str_replace('VALEUR_REFERENCE_CONSULTATION', $consultation->getReferenceUtilisateur(), $corpsEmail);

        $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $consultation->getOrganisme(), $langue);

        $corpsEmail = str_replace('TEXT_TYPE_PROCEDURE', Prado::localize('TEXT_TYPE_PROCEDURE'), $corpsEmail);
        $corpsEmail = str_replace('VALEUR_TYPE_PROCEDURE', $typeProcedure, $corpsEmail);

        $corpsEmail = str_replace('DEFINE_DATE_LIMITE_REMISE_PLI', Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLI'), $corpsEmail);
        $corpsEmail = str_replace('VALEUR_DATE_LIMITE_REMISE_PLI', Atexo_Util::iso2frnDateTime($consultation->getDatefin()), $corpsEmail);

        $corpsEmail = str_replace('TEXT_LIEN_ACCES_DIRECT_CONSULTATION', Prado::localize('TEXT_LIEN_ACCES_DIRECT_CONSULTATION'), $corpsEmail);

        $pfReference =  Atexo_Config::getParameter('PF_URL_AGENT') != "/" ?
            Atexo_Config::getParameter('PF_URL_AGENT') :
            Atexo_Config::getParameter('PF_URL_MESSAGERIE');

        $pfUrl = Atexo_MultiDomaine::replaceDomain(
            $pfReference,
            Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
        );
        $corpsEmail = str_replace('VALEUR_LIEN_ACCES_DIRECT_CONSULTATION', $pfUrl.'?page=Agent.TableauDeBord&id='.$consultation->getId(), $corpsEmail);

        $corpsEmail = str_replace('TEXT_CORDIALEMNT', Prado::localize('TEXT_CORDIALEMNT'), $corpsEmail);

        /**
         * Gestion des bandeau !
         */
        $plateformeVirtuelle = null;
        if ($consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
            $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
        }
        $corpsEmail = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau($plateformeVirtuelle), $corpsEmail);

        return $corpsEmail;
    }

    public function sendMailToAgentsAlertConsAttenteValidation(CommonConsultation $consultation, $service, $typeValidation = '', $option = true)
    {
        $tabValidation = [
            'validation_simple' => 0,
            'validation_intermediaire' => 0,
            'validation_finale' => 0,
        ];
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($consultation->getOrganisme());
        $denomination_org = $organisme->getDenominationOrg();
        if ($option
            || $typeValidation == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1')
            || $typeValidation == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_2')
            || $typeValidation == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_3')) {
            if ($typeValidation == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1')) {
                $tabValidation['validation_simple'] = 1;
            } else {
                $tabValidation['validation_finale'] = 1;
            }
            $objet = Prado::localize('OBJET_VALIDATION', ['organisme' => $denomination_org]);
            $corpsMail = self::getCorpsMessageAlerteValidation($consultation);
        } else {
            $tabValidation['validation_intermediaire'] = 1;
            $objet = Prado::localize('OBJET_APPROBATION', ['organisme' => $denomination_org]);
            $corpsMail = self::getCorpsMessageAlerteValidation($consultation, false);
        }
        $agentsMail = self::listInvited($consultation);
        $tabAgentFavoris = self::listfavoris($consultation);

        foreach ($agentsMail as $agentMail) {
            if ($agentMail instanceof CommonAgent) {
                if (in_array($agentMail->getId(), $tabAgentFavoris)) {
                    $habilitation = $agentMail->getCommonHabilitationAgents();
                    if ((is_countable($habilitation) ? count($habilitation) : 0) > 0) {
                        if ($habilitation[0] instanceof CommonHabilitationAgent) {
                            if (('1' == $habilitation[0]->getValidationSimple()
                                    && $tabValidation['validation_simple'] == $habilitation[0]->getValidationSimple())
                                || ('1' == $habilitation[0]->getValidationIntermediaire()
                                    && $tabValidation['validation_intermediaire'] == $habilitation[0]->getValidationIntermediaire())
                                || ('1' == $habilitation[0]->getValidationFinale()
                                    && $tabValidation['validation_finale'] == $habilitation[0]->getValidationFinale())
                            ) {
                                if ($typeValidation == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_3')
                                    || $typeValidation == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_5')) {
                                    if ($agentMail->getServiceId() != $service) {
                                        continue;
                                    }
                                }

                                $pfName = null;
                                $footer = Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');
                                $from = Atexo_Config::getParameter('PF_MAIL_FROM');

                                if ($consultation instanceof CommonConsultation
                                    && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                                    $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
                                    $from = $plateformeVirtuelle->getNoReply();
                                    $pfName = $plateformeVirtuelle->getFromPfName();
                                    $footer = $plateformeVirtuelle->getFooterMail();
                                }

                                $corpsMail = str_replace(
                                    'PLACE_MARCHE_PUBLIC_INTERMINISTERE',
                                    $footer,
                                    $corpsMail
                                );

                                self::simpleMail(
                                    $from,
                                    $agentMail->getEmail(),
                                    $objet,
                                    $corpsMail,
                                    '',
                                    '',
                                    false,
                                    true,
                                    true,
                                    $pfName
                                );
                            }
                        }
                    }
                }
            }
        }
    }

    public static function listInvited(CommonConsultation $consultation)
    {
        $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents($consultation->getId(), $consultation->getOrganisme());
        $listeIdsAgents = (new Atexo_Consultation_Guests())->getIdsAgentFromArrayGuestsVo($guests);
        $agentsMail = (new Atexo_Agent())->retrieveAgentsAvecAlerteValidation($listeIdsAgents);

        return $agentsMail;
    }

    /**
     * @param CommonConsultation $consultation
     * @return array
     * @throws PropelException
     */
    public static function listfavoris(CommonConsultation $consultation)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').
            Atexo_Config::getParameter('CONST_READ_ONLY'));
        $favoris = new CommonConsultationFavorisQuery();
        $listFavoris = $favoris->findByIdConsultation((int) $consultation->getId());
        $tabAgentFavoris = [];
        foreach ($listFavoris as $favoris) {
            $tabAgentFavoris[] = $favoris->getIdAgent();
        }

        return $tabAgentFavoris;
    }

    /**
     * Permet de formater les échanges messec pour l'archivage.
     *
     * @param $echanges
     *
     * @return array
     *
     * @throws Atexo_Config_Exception
     */
    public function generateMessageEchangeFilesForMessec($echanges)
    {
        $arrayDonnees = [];
        $retunArray = [];

        foreach ($echanges as $echange) {
            $idEchange = $echange['ID'];

            $messageFolderPath = Atexo_Config::getParameter('COMMON_TMP');
            $messageFilePath = $messageFolderPath.'/Message_'.$idEchange.'.txt';

            $arrayDonnees[$idEchange]['Destinataires'] = $echange['MAIL_DESTINATAIRE'];
            $arrayDonnees[$idEchange]['Objet'] = $echange['OBJET'];
            $arrayDonnees[$idEchange]['Corps'] = $echange['CORPS'];

            $retunArray[$idEchange]['PDF_ECHANGE'][0] = $messageFolderPath.$echange['PDF_ECHANGE'];
            $pathFileExploded = preg_split("/\//", $echange['PDF_ECHANGE']);
            $retunArray[$idEchange]['PDF_ECHANGE'][1] = end($pathFileExploded);
            $retunArray[$idEchange]['PDF_ECHANGE'][2] = $idEchange;

            $index = 0;
            if (!empty($echange['PJ_MESSAGE_1']) && (is_countable($echange['PJ_MESSAGE_1']) ? count($echange['PJ_MESSAGE_1']) : 0) > 0) {
                foreach ($echange['PJ_MESSAGE_1'] as $pathFile) {
                    $retunArray[$idEchange]['PJ_MESSAGE_1'][$index][0] = $messageFolderPath.$pathFile;
                    $pathFileExploded = preg_split("/\//", $pathFile);
                    $retunArray[$idEchange]['PJ_MESSAGE_1'][$index][1] = end($pathFileExploded);
                    $retunArray[$idEchange]['PJ_MESSAGE_1'][$index][2] = $idEchange;
                    ++$index;
                }
            }
            $index = 0;
            if (!empty($echange['PJ_MESSAGE_2']) && (is_countable($echange['PJ_MESSAGE_2']) ? count($echange['PJ_MESSAGE_2']) : 0) > 0) {
                foreach ($echange['PJ_MESSAGE_2'] as $pathFile) {
                    $retunArray[$idEchange]['PJ_MESSAGE_2'][$index][0] = $messageFolderPath.$pathFile;
                    $pathFileExploded = preg_split("/\//", $pathFile);
                    $retunArray[$idEchange]['PJ_MESSAGE_2'][$index][1] = end($pathFileExploded);
                    $retunArray[$idEchange]['PJ_MESSAGE_2'][$index][2] = $idEchange;
                    ++$index;
                }
            }
            $retunArray[$idEchange]['MessageEchangeFile'] = $echange['PDF_ECHANGE'];
            $retunArray[$idEchange]['DateEchange'] = $echange['DATE_MESSAGE'];
        }

        return $retunArray;
    }

    public function generateMessageEchangeFiles($echanges, $organisme)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        $arrayDonnees = [];
        $retunArray = [];

        foreach ($echanges as $echange) {
            $idEchange = $echange['ID_ECHANGE'];
            $messageFolderPath = Atexo_Config::getParameter('COMMON_TMP');
            $messageFilePath = $messageFolderPath.'/Message_'.$idEchange.'.txt';

            if (is_array($arrayDonnees[$idEchange])) {
                $arrayDonnees[$idEchange]['Destinataires'] .= '; '.$echange['MAIL_DESTINATAIRE'];
            } else {
                $arrayDonnees[$idEchange]['Destinataires'] = $echange['MAIL_DESTINATAIRE'];
                $arrayDonnees[$idEchange]['Objet'] = $echange['OBJET'];
                $arrayDonnees[$idEchange]['Corps'] = $echange['CORPS'];
                $c = new Criteria();
                $c->add(CommonEchangePieceJointePeer::ID_MESSAGE, $idEchange);
                $c->add(CommonEchangePieceJointePeer::ORGANISME, $organisme);
                $pjs = CommonEchangePieceJointePeer::doSelect($c, $connexion);
                $index = 0;
                if ($pjs && count($pjs)) {
                    foreach ($pjs as $onePJ) {
                        $idBlob = $onePJ->getPiece();
                        $nameFile = $onePJ->getNomFichier();
                        $idMessage = $onePJ->getIdMessage();
                        $retunArray[$idEchange]['PiecesJointes'][$index][0] = $idBlob;
                        $retunArray[$idEchange]['PiecesJointes'][$index][1] = $nameFile;
                        $retunArray[$idEchange]['PiecesJointes'][$index][2] = $idMessage;
                        ++$index;
                    }
                }
            }

            $msg = 'Destinataires : '.$arrayDonnees[$idEchange]['Destinataires']."\r\n\r\n";
            $msg .= 'Objet : '.$arrayDonnees[$idEchange]['Objet']."\r\n\r\n\r\n";
            $msg .= $arrayDonnees[$idEchange]['Corps'];
            Atexo_Files::createDir($messageFolderPath);
            Atexo_Util::write_file($messageFilePath, $msg);
            $retunArray[$idEchange]['MessageEchangeFile'] = $messageFilePath;
            $retunArray[$idEchange]['DateEchange'] = $echange['DATE_MESSAGE'];
        }

        return $retunArray;
    }

    public static function getDestinataireNotificationByIdEchange($idEchange, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonEchangeDestinatairePeer::ID_ECHANGE, $idEchange);
        $c->add(CommonEchangeDestinatairePeer::ORGANISME, $organisme);
        $destinataireNotification = CommonEchangeDestinatairePeer::doSelect($c, $connexion);

        return $destinataireNotification;
    }

    public function getEchangeTypeMessage($org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEchangeTypeMessagePeer::ORGANISME, $org);
        $EchangeTypeMessageList = CommonEchangeTypeMessagePeer::doSelect($c, $connexion);
        $allEchangeTypeMessage = [];
        foreach ($EchangeTypeMessageList as $unType) {
            $allEchangeTypeMessage[] = $unType->getCode();
        }

        return $allEchangeTypeMessage;
    }

    public function remplirObjetEtCorpsMessage($org, $ref, $idTypeMsg, $invitation = null)
    {
        $string = null;

        $pfReference =
            Atexo_Config::getParameter('PF_URL_MESSAGERIE');

        $pfUrl = Atexo_MultiDomaine::replaceDomain(
            $pfReference,
            Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
        );

        $res = [];

        if (isset($ref)) {
            $string = (new self())->getInfoConsultation(Atexo_Util::atexoHtmlEntities($ref), $org, true);
        }
        $string .= '
';
        $consultation = (new Atexo_Consultation())->retrieveConsultation($ref, $org);
        if (!Atexo_Module::isEnabled('InterfaceModuleSub')) {
            if ($consultation->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE')) {
                $string .= '
';
                $string .= Prado::localize('DEFINE_ACCES_DIRECT').' : '.$pfUrl.
                    '?page=Entreprise.EntrepriseAdvancedSearch&AllCons&id='.
                    Atexo_Util::atexoHtmlEntities($ref).'&orgAcronyme='.$org;
                $string .= '
';
            } elseif ($consultation->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
                $string .= Prado::localize('DEFINE_ACCES_DIRECT').' : '.$pfUrl.
                    '?page=Entreprise.EntrepriseDetailConsultation&id='.
                    Atexo_Util::atexoHtmlEntities($ref).'&orgAcronyme='.$org.'&code='.$consultation->getCodeProcedure();
                $string .= '
';
            }
        }
        if ($invitation) {
            $string .= '
';
            if (1 == $consultation->getConsultationTransverse()) {
                $string .= Prado::localize('TEXT_CONCOURIR_CONSULTATION_TRANSVERSE');
            } elseif (1 == $consultation->getDepouillablePhaseConsultation()) {
                $string .= Prado::localize('TEXT_CONCOURIR_DEPOUIABLE_PHASE_CONSULT');
            } else {
                $string .= Prado::localize('TEXT_CONCOURIR_INVITE');
            }

            if ($consultation->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
                $string .= '
';
                $string .= Prado::localize('TEXT_CONCOURIR_CODE_ACCES').$consultation->getCodeProcedure();
            }
        }
        $objetMsgModifCons = self::retrieveTypeMessageById($idTypeMsg);
        if ($objetMsgModifCons) {
            $string .= '
';
            $string .= $objetMsgModifCons->getCorps();
            if ($invitation) {
                $res['objet'] = Prado::localize('TEXT_INVITATION_CONCOURIR');
            } else {
                $res['objet'] = $objetMsgModifCons->getLibelle();
            }
        }

        $placeMarchePublicInterministere = Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');

        if ($consultation instanceof CommonConsultation
            && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
            $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
            $placeMarchePublicInterministere = $plateformeVirtuelle->getFooterMail();
        }

        $string .= (new self())->getBasPage($placeMarchePublicInterministere);

        $res['corps'] = $string;

        return $res;
    }

    public function getAllAdressesRegistres($ref, $org)
    {
        $tabMails = [];
        $destinataires = '';
        $destinataires = self::getAdressesRegistreRetraitAndTelechargement($ref, $org);

        $registreQUestion = self::getAdressesRegistreQuestion($ref, $org);
        if ($registreQUestion) {
            $destinataires = ('' != $destinataires) ? ($destinataires.','.$registreQUestion) : $registreQUestion;
        }

        $adresseDepot = self::getAdressesRegistreDepot($ref, $org, true);
        if ($adresseDepot) {
            $destinataires = ('' != $destinataires) ? ($destinataires.','.$adresseDepot) : $adresseDepot;
        }
        if (Atexo_Module::isEnabled('InterfaceDume')) {
            $registreDepotDume = self::getAdressesRegistreDepotDume($ref, $org);
            if ($registreDepotDume) {
                $destinataires = ('' != $destinataires) ? ($destinataires.','.$registreDepotDume) : $registreDepotDume;
            }
        }
        $trimmedDestinataire = str_replace(' ', '', $destinataires);
        $mailsDestinataire = explode(',', $trimmedDestinataire);
        if (is_array($mailsDestinataire) && 0 != count($mailsDestinataire)) {
            foreach ($mailsDestinataire as $mail) {
                $tabMails[$mail] = $mail;
            }
        }

        return $tabMails;
    }

    /**
     * construit l'entête du mail ou du pdf alerte.
     *
     * @param string $footerMail
     * @param string $criteria
     *
     * @return string
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function genererEntetMailFormatHtml($typeConsultation = true, $bandeauPFV = false)
    {
        $bandeau = Atexo_Message::getImageBandeau();
        if ($bandeauPFV) {
            $bandeau = 'IMAGE_BANDEAU';
        }
        $enteteMail = file_get_contents(Atexo_Config::getParameter('CHEMIN_FICHIER_SUMMARY_MAIL_ALERTE'));
        if ($typeConsultation) {
            $enteteNouvelleAvis = Prado::localize('TEXT_ENTETE_NOUVELLE_CONSULTATION');
            $nouvelleAvis = Prado::localize('TEXT_NOUVELLE_CONSULTATION');
        } else {
            $enteteNouvelleAvis = Prado::localize('TEXT_ENTETE_NOUVELLE_ANNONCE');
            $nouvelleAvis = Prado::localize('TEXT_NOUVELLE_ANNONCE');
        }
        $chainesAremplacer = [
            'TEXT_PF_LONG_NAME', //Atexo_Config::getParameter('PF_LONG_NAME')
            'DEFINE_TEXT_BONJOUR', // Prado::localize('DEFINE_TEXT_BONJOUR')
            'TEXT_ENTETE_NOUVELLE_CONSULTATION', //Prado::localize('TEXT_ENTETE_NOUVELLE_CONSULTATION')
            'TEXT_DATE_ALERTE', //date('d/m/Y')
            'TEXT_NOM_ALERTE', // Prado::localize('TEXT_NOM_ALERTE')
            'CLAUSES_SOCIALES', //Prado::localize('CLAUSES_SOCIALES')
            'CLAUSES_ENVIRONNEMENTALES', // Prado::localize('CLAUSES_ENVIRONNEMENTALES')
            'DEFINE_PROCEDURE', //Prado::localize('DEFINE_PROCEDURE')
            'TEXT_CATEGORIE', // Prado::localize('TEXT_CATEGORIE')
            'DEFINE_PUBLIEE_LE', // Prado::localize('DEFINE_PUBLIEE_LE')
            'DEFINE_TEXT_REFERENCE', // Prado::localize('DEFINE_TEXT_REFERENCE')
            'DEFINE_INTITULE', // Prado::localize('DEFINE_INTITULE')
            'TEXT_OBJET', // Prado::localize('OBJET')
            'TEXT_ORGANISME', // Prado::localize('ORGANISME')
            'TEXT_DATE_LIMITE_BR_REMISE_PLIS', // Prado::localize('DEFINE_DATE_LIMITE_REMIS_PLIS')
            'TEXT_BR_ACCES_BR_DIERCT', //Prado::localize('TEXT_BR_ACCES_BR_DIERCT')
            'TEXT_NOUVELLE_CONSULTATION', //Prado::localize('TEXT_NOUVELLE_CONSULTATION')
            'IMAGE_BANDEAU', // Atexo_Message::getImageBandeau()
            'COULEUR_TITRE', //Atexo_Config::getParameter('MAIL_COULEUR_TITRE')
            'PF_LONG_NAME', //Atexo_Config::getParameter('PF_LONG_NAME')
        ];
        $chainesRemplacantes = [
            Atexo_Config::getParameter('PF_LONG_NAME'),
            Prado::localize('DEFINE_TEXT_BONJOUR'),
            $enteteNouvelleAvis,
            date('d/m/Y'),
            Prado::localize('TEXT_NOM_ALERTE'),
            Prado::localize('CLAUSES_SOCIALES'),
            Prado::localize('CLAUSES_ENVIRONNEMENTALES'),
            Prado::localize('DEFINE_PROCEDURE'),
            Prado::localize('TEXT_CATEGORIE'),
            Prado::localize('DEFINE_PUBLIEE_LE'),
            Prado::localize('DEFINE_TEXT_REFERENCE'),
            Prado::localize('DEFINE_INTITULE'),
            Prado::localize('OBJET'),
            Prado::localize('ORGANISME'),
            Prado::localize('DEFINE_DATE_LIMITE_REMIS_PLIS'),
            Prado::localize('TEXT_BR_ACCES_BR_DIERCT'),
            $nouvelleAvis,
            $bandeau,
            Atexo_Config::getParameter('MAIL_COULEUR_TITRE'),
            Atexo_Config::getParameter('PF_LONG_NAME'),
        ];
        $enteteMail = str_replace($chainesAremplacer, $chainesRemplacantes, $enteteMail);

        return $enteteMail;
    }

    public function genererListeConsForMailFormatHtml($oneConsultation, $linkConsultation = null)
    {
        $listeCons = (new Atexo_Util())->toHttpEncoding(file_get_contents(Atexo_Config::getParameter('CHEMIN_FICHIER_LIGNE_CONSULTATION_MAIL_ALERTE')));
        $listeCons = str_replace('TEXT_PROCEDURE', $oneConsultation->getTypeProcedure()->getAbbreviation(), $listeCons);
        $categorie = Atexo_Consultation_Category::retrieveCategorie($oneConsultation->getCategorie(), true);
        $libelleCategorie = '';
        if ($categorie) {
            $libelleCategorie = $categorie->getLibelleTraduit();
        }
        $listeCons = str_replace('TEXT_CATEGORIE', $libelleCategorie, $listeCons);
        $listeCons = str_replace('TEXT_PUBLIE_LE', Atexo_Util::iso2frnDateTime($oneConsultation->getDateMiseEnLigneCalcule(), false), $listeCons);
        $listeCons = str_replace('TEXT_REFERENCE', $oneConsultation->getReferenceutilisateur(), $listeCons);
        $listeCons = str_replace('TEXT_INTITULE', $oneConsultation->getIntitule(), $listeCons);
        $listeCons = str_replace('TEXT_OBJET', Prado::localize('OBJET'), $listeCons);
        $objetCons = Atexo_Util::truncateTexte($oneConsultation->getObjet(), 300, false);
        if (0 != strcmp($objetCons, $oneConsultation->getObjet())) {
            $objetCons .= '...';
        }
        $listeCons = str_replace('CONTENU_OBJET', $objetCons, $listeCons);
        $listeCons = str_replace('TEXT_ORGANISME', Prado::localize('ORGANISME'), $listeCons);
        if (Atexo_Module::isEnabled('AnnoncesMarches')) {
            $listeCons = str_replace('CONTENU_ORGANISME', $oneConsultation->getOrgDenomination(), $listeCons);
        } else {
            $orgObject = Atexo_Organismes::retrieveOrganismeByAcronyme($oneConsultation->getOrganisme());
            $listeCons = str_replace('CONTENU_ORGANISME', $orgObject->getDenominationOrg(), $listeCons);
        }
        $lienAccesDirect = (new Atexo_Consultation())->getUrlAccesDirectProcPubliciteOuverte($oneConsultation->getId(), $oneConsultation->getOrganisme());
        if ($linkConsultation) {
            $lienAccesDirect = $linkConsultation;
        }
        $lienAccesDirect = str_replace('&', '&amp;', $lienAccesDirect);
        $listeCons = str_replace('LIEN_ACCES_DIRECT', $lienAccesDirect, $listeCons);
        $limite = explode(' ', $oneConsultation->getDateFin());
        $listeCons = str_replace('DATE_LIMITE', Atexo_Util::iso2frnDate($limite[0]), $listeCons);
        $heure = explode(':', $limite[1]);
        $listeCons = str_replace('HEURE_LIMITE', $heure[0].':'.$heure[1], $listeCons);
        $listeCons = str_replace('TEXT_ACCES', Prado::localize('TEXT_VOIR'), $listeCons);

        return $listeCons;
    }

    /**
     * construit le footer du mail ou du pdf alerte.
     *
     * @param bool $mail
     *
     * @return string
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function genererFooterMailFormatHtml($mail)
    {
        if ($mail) {
            $footerMail = file_get_contents(Atexo_Config::getParameter('CHEMIN_FICHIER_FOOTER_MAIL_ALERTE'));
            $chainesAremplacer = [
                'TEXT_ABONNEMENT_ALERTES', //Prado::localize('TEXT_ABONNEMENT_ALERTES')
                'DEFINE_CLIQUEZ_ICI_MIN', //Prado::localize('DEFINE_CLIQUEZ_ICI_MIN')
                'TEXT_IMPRIMER_MESSAGE_PDF', //Prado::localize('TEXT_IMPRIMER_MESSAGE_PDF')
                'COULEUR_TITRE', //Atexo_Config::getParameter('MAIL_COULEUR_TITRE')
                'DEFINE_TEXT_ENTITE_ACHAT', // Prado::localize('DEFINE_TEXT_ENTITE_ACHAT')
                'TEXT_TYPE_PROCEDURE', // Prado::localize('TEXT_TYPE_PROCEDURE')
                'DEFINE_CATEGORIE_PRINCIPAL', // Prado::localize('DEFINE_CATEGORIE_PRINCIPAL')
                'DEFINE_CODE_CPV', // Prado::localize('DEFINE_CODE_CPV')
                'TEXT_LIEU_EXECUTION', // Prado::localize('TEXT_LIEU_EXECUTION')
                'TEXT_MOTS_CLES', // Prado::localize('TEXT_MOTS_CLES')
                'TEXT_RAPPEL_CRITERES', // Prado::localize('TEXT_RAPPEL_CRITERES')
                'CORDIALEMENT', // Prado::localize('CORDIALEMENT')
            ];
            $chainesRemplacantes = [
                Prado::localize('TEXT_ABONNEMENT_ALERTES'),
                Prado::localize('DEFINE_CLIQUEZ_ICI_MIN'),
                Prado::localize('TEXT_IMPRIMER_MESSAGE_PDF'),
                Atexo_Config::getParameter('MAIL_COULEUR_TITRE'),
                Prado::localize('DEFINE_TEXT_ENTITE_ACHAT'),
                Prado::localize('TEXT_TYPE_PROCEDURE'),
                Prado::localize('DEFINE_CATEGORIE_PRINCIPAL'),
                Prado::localize('DEFINE_CODE_CPV'),
                Prado::localize('TEXT_LIEU_EXECUTION'),
                Prado::localize('TEXT_MOTS_CLES'),
                Prado::localize('TEXT_RAPPEL_CRITERES'),
                Prado::localize('CORDIALEMENT'),
            ];
            $footerMail = str_replace($chainesAremplacer, $chainesRemplacantes, $footerMail);
        } else {
            $footerMail = <<<EOF
</table></td></tr></table></td></tr></table></td></tr>
							<tr>
								<td width="100%" valign="top" class="mainbar" align="left"><p style="margin-top:20px;">PF_LONG_NAME</p></td>
							</tr>
							</table></td></tr></table></td></tr></table></body></html>
EOF;
        }

        return $footerMail;
    }

    /**
     * construit le footer du mail ou du pdf alerte.
     *
     * @param string $footerMail
     * @param string $criteria
     *
     * @return string
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function genererFooterMailFormatHtmlFromCriteria(&$footerMail, $criteria)
    {
        $infosAlerte = self::getInfosAlerteForMailHtml($criteria);
        $chainesAremplacer = [
            'VALEUR_ENTITE_ACHAT', // $infosAlerte["entiteAchat"]
            'VALEUR_TYPE_PROCEDURE', // $infosAlerte["typeProcedure"]
            'VALEUR_CATEGORIE_PRINCIPAL', // $infosAlerte["categorie"]
            'VALEUR_LIEU_EXECUTION', // $infosAlerte["lieuExecution"]
            'VALEUR_CODE_CPV', // $infosAlerte["codeCPV"]
            'VALEUR_MOTS_CLES', // $infosAlerte["motCle"]
        ];
        $chainesRemplacantes = [
            $infosAlerte['entiteAchat'],
            $infosAlerte['typeProcedure'],
            $infosAlerte['categorie'],
            $infosAlerte['lieuExecution'],
            $infosAlerte['codeCPV'],
            $infosAlerte['motCle'],
        ];
        $footerMail = str_replace($chainesAremplacer, $chainesRemplacantes, $footerMail);

        return $footerMail;
    }

    /**
     * Permet de recuperer les infos de l'alerte pour l'envoi du mail html.
     *
     * @param Atexo_Consultation_CriteriaVo $criteria : objet criteres virtuels
     *
     * @return array: liste contenant les informations de l'alerte
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getInfosAlerteForMailHtml($criteria)
    {
        $criteriaVo = unserialize($criteria);
        $infosAlerte = [];
        if ($criteriaVo instanceof Atexo_Consultation_CriteriaVo) {
            $entiteAchat = Atexo_EntityPurchase::getSigleLibelleEntityById($criteriaVo->getIdService(), $criteriaVo->getAcronymeOrganisme());
            if ($entiteAchat) {
                $infosAlerte['entiteAchat'] = $entiteAchat;
            } elseif ($criteriaVo->getOrgDenomination()) {
                $infosAlerte['entiteAchat'] = $criteriaVo->getOrgDenomination();
            } else {
                $infosAlerte['entiteAchat'] = Prado::localize('TEXT_TOUTES_EA');
            }
            if ('' != $criteriaVo->getIdTypeProcedure()) {
                $infosAlerte['typeProcedure'] = $criteriaVo->getIdTypeProcedure();
            } else {
                $infosAlerte['typeProcedure'] = (Prado::localize('TEXT_RECHERCHE_AVANCEE_TOUS_TYPES_PROCEDURES'));
            }
            if ('' != $criteriaVo->getCategorieConsultation()) {
                $categorilot = Atexo_Consultation_Category::retrieveCategorie($criteriaVo->getCategorieConsultation(), true);
                $infosAlerte['categorie'] = $categorilot->getLibelleTraduit();
            } else {
                $infosAlerte['categorie'] = (Prado::localize('TEXT_INDIFFERENT'));
            }
            if ('NULL' != $criteriaVo->getKeyWordAdvancedSearch() && '' != $criteriaVo->getKeyWordAdvancedSearch()) {
                $infosAlerte['motCle'] = $criteriaVo->getKeyWordAdvancedSearch();
            } else {
                $infosAlerte['motCle'] = (Prado::localize('TEXT_NON_PRECISE'));
            }
            if ('' != $criteriaVo->getLieuxexecution()) {
                $lieuxExecutionsType1 = '';
                $arIds = explode(',', trim($criteriaVo->getLieuxexecution()));
                $list = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arIds);
                if ($list) {
                    foreach ($list as $oneGeoN2) {
                        $lieuxExecutionsType1 .= $oneGeoN2->getDenomination1Traduit().', ';
                    }
                    $infosAlerte['lieuExecution'] = $lieuxExecutionsType1;
                }
            } else {
                $infosAlerte['lieuExecution'] = (Prado::localize('TEXT_NON_PRECISE'));
            }
            if ('' != $criteriaVo->getIdCodeCpv2()) {
                $infosAlerte['codeCPV'] = str_replace('#', ';', trim($criteriaVo->getIdCodeCpv2(), '#'));
            } else {
                $infosAlerte['codeCPV'] = (Prado::localize('TEXT_NON_PRECISE'));
            }
        }

        return $infosAlerte;
    }

    public function getAllEchangeCommission($idCom, $org, $c = null, $returnNbre = false)
    {
        $param = [];
        //$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$c) {
            $c = new Criteria();
        }
        $typeRelation = Atexo_Config::getParameter('TYPE_RELATION_ECHANGE_COMMISSION');
        $relations = (new Atexo_Message_RelationEchange())->getRelationEchangeByIdExterneAndTypeRelation($idCom, $typeRelation, $org);

        $idsEchange = [];
        foreach ($relations as $relation) {
            $idsEchange[$relation->getIdEchange()] = $relation->getIdEchange();
        }

        $implode = implode("','", $idsEchange);
        if ($returnNbre) {
            $sql = ' SELECT COUNT(EchangeDestinataire.ID) as numberOfElement ';
        } else {
            $sql = ' SELECT Echange.id as ID_ECHANGE, EchangeDestinataire.MAIL_DESTINATAIRE, EchangeDestinataire.AR, EchangeDestinataire.DATE_AR,
        			 EchangeDestinataire.TYPE_AR, Echange.OBJET, Echange.EXPEDITEUR, Echange.DATE_MESSAGE ';
        }
        $sql .= ' FROM EchangeDestinataire, Echange ';
        $sql .= ' WHERE EchangeDestinataire.ID_ECHANGE=Echange.ID AND EchangeDestinataire.organisme=Echange.organisme ';
        $sql .= " AND Echange.organisme = :acronymeOrg AND Echange.ID IN ('".$implode."') ";
        $param[':acronymeOrg'] = Atexo_CurrentUser::getCurrentOrganism();

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute($param);

        if ($returnNbre) {
            $nombreElement = 0;

            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $nombreElement = $row['numberOfElement'];
            }

            return $nombreElement;
        } else {
            $arrayDestinataire = [];
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $arrayDestinataire[] = $row;
            }

            return $arrayDestinataire;
        }
    }

    public function getAllEchangeCAOSeance($idSeance, $org, $c = null, $returnNbre = false)
    {
        $param = [];
        //$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$c) {
            $c = new Criteria();
        }
        $typeRelation = Atexo_Config::getParameter('TYPE_RELATION_ECHANGE_COMMISSION_SEANCE');
        $relations = (new Atexo_Message_RelationEchange())->getRelationEchangeByIdExterneAndTypeRelation($idSeance, $typeRelation, $org);

        $idsEchange = [];
        foreach ($relations as $relation) {
            $idsEchange[$relation->getIdEchange()] = $relation->getIdEchange();
        }

        $implode = implode("','", $idsEchange);
        if ($returnNbre) {
            $sql = ' SELECT COUNT(EchangeDestinataire.ID) as numberOfElement ';
        } else {
            $sql = ' SELECT Echange.id as ID_ECHANGE, EchangeDestinataire.MAIL_DESTINATAIRE, EchangeDestinataire.AR, EchangeDestinataire.DATE_AR,
        			 EchangeDestinataire.TYPE_AR, Echange.OBJET, Echange.EXPEDITEUR, Echange.DATE_MESSAGE ';
        }
        $sql .= ' FROM EchangeDestinataire, Echange ';
        $sql .= ' WHERE EchangeDestinataire.ID_ECHANGE=Echange.ID AND EchangeDestinataire.organisme=Echange.organisme ';
        $sql .= " AND Echange.organisme = :acronymeOrg AND Echange.ID IN ('".$implode."') ";
        if (!$returnNbre && $c->getLimit()) {
            $sql .= ' limit '.$c->getOffset().','.($c->getLimit() + $c->getOffset());
        }
        $param[':acronymeOrg'] = Atexo_CurrentUser::getCurrentOrganism();

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute($param);

        if ($returnNbre) {
            $nombreElement = 0;

            while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                $nombreElement = $row['numberOfElement'];
            }

            return $nombreElement;
        } else {
            $arrayDestinataire = [];
            while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                $arrayDestinataire[] = $row;
            }

            return $arrayDestinataire;
        }
    }

    public function getEchangeDestinataireByIdEchange($idEchange, $organisme, $returnEmail = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        $c = new Criteria();
        $c->add(CommonEchangeDestinatairePeer::ID_ECHANGE, $idEchange);
        $c->add(CommonEchangeDestinatairePeer::ORGANISME, $organisme);
        $destinataires = CommonEchangeDestinatairePeer::doSelect($c, $connexion);
        if ($returnEmail) {
            $emails = [];
            foreach ($destinataires as $destinataire) {
                $emails[$destinataire->getMailDestinataire()] = $destinataire->getMailDestinataire();
            }

            return $emails;
        } else {
            return $destinataires;
        }
    }

    public function getInfoPartagerConsultation(CommonConsultation $consultation)
    {
        $pfUrl = Atexo_MultiDomaine::replaceDomain(
            Atexo_Config::getParameter('PF_URL_MESSAGERIE')
        );

        $string = '';
        $string .= addslashes(Atexo_Util::replaceCharactersMsWordWithRegularCharacters(Prado::localize('DEFINE_MESSAGE_ENVOYE_PAR_PF')).' '.
            Atexo_Config::getParameter('PF_LONG_NAME').', '.Prado::localize('TEXT_LE_MINUSCULE').' '.date('d/m/Y'));
        $string .= '
';
        $string .= addslashes(Atexo_Util::replaceCharactersMsWordWithRegularCharacters(Prado::localize('TEXT_PARTAGER_CETTE_CONSULTATION_AVEC_VOUS'))).' : ';
        $string .= '
';
        $string .= addslashes(Atexo_Util::replaceCharactersMsWordWithRegularCharacters(Prado::localize('DEFINE_TEXT_CONSULTATION').' '.
            $consultation->getReferenceUtilisateur().' - '.$consultation->getIntituleTraduit()));
        $string .= '
';
        $string .= addslashes(Atexo_Util::replaceCharactersMsWordWithRegularCharacters(Prado::localize('OBJET').' : '.$consultation->getObjetTraduit()));
        $string .= '
';
        $string .= Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' : ';
        if (Atexo_Module::isEnabled('AnnoncesMarches')) {
            $string .= addslashes(Atexo_Util::replaceCharactersMsWordWithRegularCharacters($consultation->getOrgDenomination()));
        } else {
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($consultation->getOrganisme());
            $string .= addslashes(Atexo_Util::replaceCharactersMsWordWithRegularCharacters($organismeO->getSigle().' - '.$organismeO->getDenominationOrgTraduit()));
        }

        $string .= '
';
        $string .= Prado::localize('TEXT_ACCES_DIRECT_CONSULTATION').' : '.urlencode($pfUrl.
                    'index.php?page=Entreprise.EntrepriseAdvancedSearch&AllCons&id='
                .$consultation->getId().'&orgAcronyme='.$consultation->getOrganisme());
        $string .= '
';

        return $string;
    }

    /**
     * Envoi d'un email de confirmation de l'association.
     */
    public function sendEmailConfirmation($infoCompte, $email, $uid)
    {
        $pfUrl = Atexo_MultiDomaine::replaceDomain(
            Atexo_Config::getParameter('PF_URL_MESSAGERIE'),
            Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
        );

        $urlConfirmation = $pfUrl.'?page=Agent.EchangeAssociationsCompte&uid='.base64_encode($uid.'#1');
        $urlAnnulation = $pfUrl.'?page=Agent.EchangeAssociationsCompte&uid='.base64_encode($uid.'#0');

        $corpsMail = self::render('PATH_FILE_MAIL_TEMPLATE_DEMANDE_CONFIRMATION_ASSOCIATION_COMPTE');

        $rawFieldsTemplate = [
            'IMAGE_BANDEAU', 'SALUTATION', 'TEXT_EMAIL_CONFIRMATION_ASSOCATION_COMPTE1',
            'DEFINE_NOM', 'DEFINE_PRENOM', 'TEXT_ADRESSE_ELECTRONIQUE', 'TEXT_ENTITE', 'TEXT_SERVICE_RECHERCHE_AVANCEE',
            'TEXT_EMAIL_CONFIRMATION_ASSOCATION_COMPTE2', 'TEXT_URl_ASSOCIATION_COMPTES1', 'TEXT_EMAIL_CONFIRMATION_ASSOCATION_COMPTE_NB', 'TEXT_EMAIL_CONFIRMATION_ASSOCATION_COMPTE_3',
            'TEXT_URl_ASSOCIATION_COMPTES2', 'CORDIALEMENT', 'DEFINE_COURRIER_ASSOCIATION_COMPTE',
            'VALEUR_NOM', 'VALEUR_PRENOM', 'VALEUR_ADRESSE_ELECTRONIQUE', 'VALEUR_ENTITE', 'VALEUR_SERVICE_RECHERCHE_AVANCEE',
            'VALEUR_URl_ASSOCIATION_COMPTES1', 'VALEUR_URl_ASSOCIATION_COMPTES2',
        ];
        $valueFieldsTemplate = [
            Atexo_Message::getImageBandeau(), Prado::localize('DEFINE_TEXT_BONJOUR'), Prado::localize('TEXT_EMAIL_CONFIRMATION_ASSOCATION_COMPTE1'),
            Prado::localize('DEFINE_NOM'), Prado::localize('DEFINE_PRENOM'), Prado::localize('TEXT_ADRESSE_ELECTRONIQUE'), Prado::localize('TEXT_ENTITE'), Prado::localize('SERVICE_RECHERCHE_AVANCEE'),
            Prado::localize('TEXT_EMAIL_CONFIRMATION_ASSOCATION_COMPTE2'), Atexo_Config::getParameter('URl_ASSOCIATION_COMPTES'), Prado::localize('TEXT_EMAIL_CONFIRMATION_ASSOCATION_COMPTE_NB'),
            Prado::localize('TEXT_EMAIL_CONFIRMATION_ASSOCATION_COMPTE_3'), Atexo_Config::getParameter('URl_ASSOCIATION_COMPTES'), Prado::localize('CORDIALEMENT'),
            Prado::localize('DEFINE_COURRIER_ASSOCIATION_COMPTE'),
            $infoCompte['nom'], $infoCompte['prenom'], $infoCompte['email'], $infoCompte['entite'], $infoCompte['service'], $urlConfirmation, $urlAnnulation,
        ];

        $corpsMail = str_replace($rawFieldsTemplate, $valueFieldsTemplate, $corpsMail);
        $objet = Prado::localize('DEFINE_COURRIER_ASSOCIATION_COMPTE');

        /*
         * todo: Remplacer le from
         */

        self::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), $email, $objet, $corpsMail, '', '', false, true);
    }

    /**
     * Permet de recuperer les pièces jointes d'un echange ou email (table Echange).
     */
    public function recupererPjEchange(CommonEchange $echange)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEchangePieceJointePeer::ORGANISME, $echange->getOrganisme());

        return $echange->getCommonEchangepiecejointes($c, $connexion);
    }

    /**
     * Permet de formater le mail en Html.
     *
     * @param string $objet     objet du mail
     * @param string $corpsMail corps du mail
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function generateMailFormatHtml($objet, $corpsMail, $avecMessagePersonalise = false)
    {
        // le saut de la ligne est important voir le mantis 30979
        $corpsMail = '<p>'.str_replace("\n", '</p>
<p>', $corpsMail).'</p>';

        $templateFile = Atexo_Config::getParameter('PATH_FILE_MAIL_ACHERTEUR');
        if (!file_exists($templateFile)) {
            $message = 'Impossible de récupérer le template de mail : '.$templateFile;
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error($message);
            throw new \Exception($message);
        }
        $corps = (new Atexo_Util())->toHttpEncoding(file_get_contents($templateFile));
        $corps = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $corps);
        $corps = str_replace('COULEUR_TITRE', Atexo_Config::getParameter('MAIL_COULEUR_TITRE'), $corps);
        $corps = str_replace('URL_THEMES', Atexo_Config::getParameter('PF_URL').Atexo_Controller_Front::t(), $corps);
        $corps = str_replace('PF_LONG_NAME', (new Atexo_Config())->toPfEncoding(Atexo_Config::getParameter('PF_LONG_NAME')), $corps);
        $corps = str_replace('TITRE_OBJET', $objet, $corps);
        $corps = str_replace('CORP_MAIL', $corpsMail, $corps);
        $corps = str_replace('CORDIALEMENT', Prado::localize('CORDIALEMENT'), $corps);
        $corps = str_replace('BAS_PAGE_MAIL', Prado::localize('BAS_PAGE_MAIL'), $corps);
        $facturationObligatoire = (('FACTURATION_ELECTRONIQUE_OBLIGATOIRE' == Prado::Localize('FACTURATION_ELECTRONIQUE_OBLIGATOIRE') || !$avecMessagePersonalise) ? '' : Prado::Localize('FACTURATION_ELECTRONIQUE_OBLIGATOIRE'));
        $utiliserPortail = (('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION' == Prado::Localize('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION') || !$avecMessagePersonalise) ? '' : Prado::Localize('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION'));
        $corps = str_replace('FACTURATION_OBLIGATOIRE', $facturationObligatoire, $corps);
        $corps = str_replace('UTILISER_PORTAIl_POUR_FACTURATION', $utiliserPortail, $corps);

        return $corps;
    }

    /**
     * Permet d'envoyer des mails de notification aux administrateurs et utilisateur crees supprimes.
     *
     * @param CommonInscrit $user         : objet inscrit de l'entreprise
     * @param string        $objet        : objet du mail
     * @param string        $msgCorpsMail : corps du message personnalise pour les utilisateurs (utilisateurs simples ou administrateurs)
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function envoiMailsNotificationSuppressionCreationCompteEntreprise($user, $objet, $msgCorpsMail)
    {
        try {
            if ($user instanceof CommonInscrit) {
                $corps = (new Atexo_Util())->toHttpEncoding(file_get_contents(Atexo_Config::getParameter('PATH_FILE_TEMPLATE_MAIL_CREATION_SUPPRESSION_COMPTE_ENTREPRISE')));
                $corps = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $corps);
                $corps = str_replace('COULEUR_TITRE', Atexo_Config::getParameter('MAIL_COULEUR_TITRE'), $corps);
                $corps = str_replace('PF_LONG_NAME', (new Atexo_Config())->toPfEncoding(Atexo_Config::getParameter('PF_LONG_NAME')), $corps);
                $corps = str_replace('TEXT_SALUTATIONS', Prado::localize('DEFINE_BONJOUR').' '.$user->getPrenom().' '.$user->getNom().',', $corps);
                $corps = str_replace('CORDIALEMENT', Prado::localize('CORDIALEMENT').',', $corps);
                $corps = str_replace('BAS_PAGE_MAIL', Prado::localize('BAS_PAGE_MAIL'), $corps);
                $corps = str_replace('TITRE_MAIL', $objet, $corps);
                $corps = str_replace('FACTURATION_OBLIGATOIRE', (('FACTURATION_ELECTRONIQUE_OBLIGATOIRE' == Prado::Localize('FACTURATION_ELECTRONIQUE_OBLIGATOIRE')) ? '' : Prado::Localize('FACTURATION_ELECTRONIQUE_OBLIGATOIRE')), $corps);
                $corps = str_replace('UTILISER_PORTAIl_POUR_FACTURATION', (('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION' == Prado::Localize('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION')) ? '' : Prado::Localize('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION')), $corps);
                $corps = str_replace('PF_ACHAT_D_ETAT', Prado::localize('PF_ACHAT_D_ETAT'), $corps);

                /**
                 * @todo: Remplacé le from
                 */
                $from = Atexo_Config::getParameter('PF_MAIL_FROM');
                $to = $user->getEmail();
                //Envoi mail de notification aux administrateurs
                $corpsMailToSendTo = str_replace('CORPS_MAIL', $msgCorpsMail, $corps);
                Atexo_Message::simpleMail(Atexo_Util::toUtf8($from), $to, $objet, $corpsMailToSendTo, '', '', false, true);
            }
        } catch (\Exception $ex) {
            $message = "Erreur lors de l'envoi du mail de notification:".$ex->getMessage()."\n";
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error($message);
        }
    }

    /**
     * Permet de recuperer le contenu de l'email d'accusé de reception d'une question deposée.
     *
     * @param string $reference la reference de la consultation
     * @param string $organisme l'organisme de la consultation
     * @param $question
     * @param bool $destinataire
     *
     * @return string $mail le contenu html du mail a envoyer
     *
     * @throws Atexo_Config_Exception
     *
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getContentMailForQuestionAR(
        $reference,
        $organisme,
        $question,
        $destinataire = false,
        CommonPlateformeVirtuelle $plateformeVirtuelle = null
    ) {
        $mail = null;
        $consultation = (new Atexo_Consultation())->retrieveCommonConsultation($reference, $organisme);
        if (null != $consultation && $consultation instanceof CommonConsultation) {
            if ($question instanceof CommonQuestionsDce) {
                $cheminTemplate = Atexo_Config::getParameter('CHEMIN_FICHIER_MAIL_ACCUSE_RECEPTION');
                $mail = (new self())->getTextForMailAr($cheminTemplate, $plateformeVirtuelle);
                $mail = (new self())->getInfosConsForMailAR($consultation, $mail);
                if ($destinataire instanceof CommonAgent) {
                    $titre = Prado::localize('RECEPTION_QUESTION_ELECRONIQUE');
                    $msgReception = Prado::localize('QUESTION_DEPOSE_POUR_CONSULTATION');
                    $nomDest = $destinataire->getNom();
                    $prenomDest = $destinataire->getPrenom();
                    /**
                     * @todo implémenté le PFV
                     */
                    $pfReference =  Atexo_Config::getParameter('PF_URL_AGENT') != "/" ?
                        Atexo_Config::getParameter('PF_URL_AGENT') :
                        Atexo_Config::getParameter('PF_URL_MESSAGERIE');

                    if ($plateformeVirtuelle) {
                        $pfReference = $plateformeVirtuelle->getProtocole() . '://' . $plateformeVirtuelle->getDomain();
                    }

                    $pfUrl = Atexo_MultiDomaine::replaceDomain(
                        $pfReference,
                        Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
                    );
                    $urlAccesDirect = $pfUrl.'?page=Agent.TableauDeBord&AS=0&id='.$reference;
                    $mail = str_replace('VALEUR_LIEN_ACCES_DIRECT_CONSULTATION', $urlAccesDirect, $mail);
                    $mail = str_replace('FACTURATION_OBLIGATOIRE', '', $mail);
                    $mail = str_replace('UTILISER_PORTAIl_POUR_FACTURATION', '', $mail);
                } else {
                    $titre = Prado::localize('ACCUSE_RECEPTION_QUESTION_ELECRONIQUE');
                    $msgReception = Prado::localize('AR_QUESTION_DEPOSE_POUR_CONSULTATION');
                    $nomDest = $question->getNom();
                    $prenomDest = $question->getPrenom();
                    $mail = str_replace('AGENT-->', '', $mail);
                    $mail = str_replace('<!--AGENT', '', $mail);
                    $mail = str_replace('FACTURATION_OBLIGATOIRE', (('FACTURATION_ELECTRONIQUE_OBLIGATOIRE' == Prado::Localize('FACTURATION_ELECTRONIQUE_OBLIGATOIRE')) ? '' : Prado::Localize('FACTURATION_ELECTRONIQUE_OBLIGATOIRE')), $mail);
                    $mail = str_replace('UTILISER_PORTAIl_POUR_FACTURATION', (('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION' == Prado::Localize('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION')) ? '' : Prado::Localize('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION')), $mail);
                }
                $mail = str_replace('TEXT_TITRE', $titre, $mail);
                $mail = str_replace('TEXT_ACCUSE_RECEPTION', $msgReception, $mail);
                $mail = str_replace('VALEUR_NOM_DEST', $nomDest, $mail);
                $mail = str_replace('VALEUR_PRENOM_DEST', $prenomDest, $mail);
                $mail = str_replace('VALEUR_HORODATAGE', Atexo_Util::iso2frnDateTime($question->getDateDepot()), $mail);
                if ($question->getIdFichier()) {
                    $nomFichier = $question->getNomFichier();
                    $tailleFichier = (new Atexo_Blob())->getTailFile($question->getIdFichier(), $consultation->getOrganisme()).' Ko';
                } else {
                    $nomFichier = '';
                    $tailleFichier = '';
                }
                $mail = str_replace('VALEUR_AUTRES_PIECES', $nomFichier, $mail);
                $mail = str_replace('TAILLE_AUTRES_PIECES', $tailleFichier, $mail);
                $mail = str_replace('VALEUR_NOM_ENTREPRISE', $question->getEntreprise(), $mail);
                $mail = str_replace('VALEUR_NOM_DEPOSEUR', $question->getNom(), $mail);
                $mail = str_replace('VALEUR_PRENOM_DEPOSEUR', $question->getPrenom(), $mail);
                $mail = str_replace('VALEUR_MAIL_DEPOSEUR', $question->getEmail(), $mail);
                $mail = str_replace('VALEUR_QUESTION', $question->getQuestion(), $mail);
            }

            return $mail;
        }
    }

    /**
     * Permet d'alimenter le contenu de l'email par les infos de la consultation.
     *
     * @param CommonConsultation $consultation
     * @param string             $mail         l'organisme de la consultation
     * @param bool               $fromAgent    a true si mail serau envoye a l'agent, false pour l'inscrit
     *
     * @return string $mail le contenu html du mail a envoyer
     *
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getInfosConsForMailAR($consultation, $mail, $fromAgent = false)
    {
        if (null != $consultation && $consultation instanceof CommonConsultation) {
            $pfLongName = Atexo_Config::getParameter('PF_LONG_NAME');

            if ($consultation instanceof CommonConsultation
                && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
                $pfLongName = $plateformeVirtuelle->getFooterMail();
            }

            $mail = str_replace('PF_LONG_NAME', $pfLongName, $mail);

            $langue = Atexo_CurrentUser::readFromSession('lang');
            $mail = str_replace('DEFINE_TEXT_ENTITE_PUBLIC', prado::localize('DEFINE_TEXT_ENTITE_PUBLIC'), $mail);
            if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                $mail = str_replace('VALEUR_ENTITE_PUBLIC', $consultation->getOrgDenomination(), $mail);
            } else {
                $entityPurchase = Atexo_EntityPurchase::retrieveEntityById($consultation->getServiceId(), $consultation->getOrganisme());
                $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($consultation->getOrganisme());
                $mail = str_replace('VALEUR_ENTITE_PUBLIC', $organismeO->getSigle().' - '.$organismeO->getDenominationOrgTraduit(), $mail);
                $mail = str_replace('DEFINE_ENTITE_ACHAT', Prado::localize('DEFINE_ENTITE_ACHAT'), $mail);
                if ($entityPurchase) {
                    $mail = str_replace('VALEUR_ENTITE_ACHAT', $entityPurchase->getSigle().' - '.$entityPurchase->getLibelleTraduit(), $mail);
                } else {
                    $mail = str_replace('VALEUR_ENTITE_ACHAT', $organismeO->getSigle().' - '.$organismeO->getDenominationOrgTraduit(), $mail);
                }
            }
            $mail = str_replace('VALEUR_REFERENCE', $consultation->getReferenceUtilisateur(), $mail);
            $mail = str_replace('VALEUR_INTITULE', $consultation->getIntituleTraduit(), $mail);
            $mail = str_replace('VALEUR_OBJET_CONSULTATION', $consultation->getObjetTraduit(), $mail);
            //type proc Agent
            if ($fromAgent) {
                $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $consultation->getOrganisme(), $langue);
            }//type proc Ese
            else {
                $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedure(), true, null, $langue);
            }
            $mail = str_replace('VALEUR_TYPE_PROCEDURE', Atexo_Util::supprimerCodeHtml($typeProcedure), $mail);
            if (Atexo_Module::isEnabled('PubliciteOpoce')) {
                $dateMiseEnligne = Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneSouhaitee());
            } else {
                $dateMiseEnligne = Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneCalcule());
            }
            $mail = str_replace('VALEUR_DATE_MISE_EN_LIGNE', $dateMiseEnligne, $mail);
            $mail = str_replace('VALEUR_DATE_LIMITE_REMISE_PLI', Atexo_Util::iso2frnDateTime($consultation->getDatefin()), $mail);

            return $mail;
        }
    }

    /**
     * Permet d'alimenter le contenu de l'email par les textes.
     *
     * @param string $cheminFile le chemin du fichier template de l'email
     *
     * @return string $mail le contenu html du mail a envoyer
     *
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getTextForMailAr($cheminFile, CommonPlateformeVirtuelle $commonPlateformeVirtuelle = null)
    {
        $pfName = Atexo_Config::getParameter('PF_LONG_NAME');
        if ($commonPlateformeVirtuelle instanceof CommonPlateformeVirtuelle) {
            $pfName = $commonPlateformeVirtuelle->getFromPfName();
        }

        // Compatibilité avec les versions MPE antérieures à 2020-00.01.00 (chemins configurés pour les clients par ex.)
        if (!file_exists($cheminFile)) {
            $cheminFile = str_replace(
                './ressources/',
                dirname(__DIR__, 3).'/ressources/',
                $cheminFile
            );
        }
        if (!file_exists($cheminFile)) {
            $errorMessage = 'Fichier de template d\'email non trouvé : '.$cheminFile;
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error($errorMessage);
            throw new \FileNotFoundException($errorMessage);
        }

        $mail = (new Atexo_Util())->toHttpEncoding(file_get_contents($cheminFile));
        $mail = str_replace('TEXT_PF_LONG_NAME', $pfName, $mail);
        $mail = str_replace('CORPS_DEBUT', Prado::localize('CORPS_DEBUT'), $mail);
        $mail = str_replace('DEFINE_TEXT_ENTITE_PUBLIC', Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC'), $mail);
        $mail = str_replace('DEFINE_TEXT_REFERENCE', Prado::localize('DEFINE_TEXT_REFERENCE'), $mail);
        $mail = str_replace('DEFINE_INTITULE', Prado::localize('DEFINE_INTITULE'), $mail);
        $mail = str_replace('DEFINE_OBJET_CONSULTATION', Prado::localize('DEFINE_OBJET_CONSULTATION'), $mail);
        $mail = str_replace('TEXT_TYPE_PROCEDURE', Prado::localize('TEXT_TYPE_PROCEDURE'), $mail);
        $mail = str_replace('DEFINE_DATE_MISE_LIGNE', Prado::localize('DEFINE_DATE_MISE_LIGNE'), $mail);
        $mail = str_replace('TEXT_DATE_HEURE_LIMITE_REMISE_PLIS', Prado::localize('TEXT_DATE_HEURE_LIMITE_REMISE_PLIS'), $mail);
        $mail = str_replace('TEXT_ENTREPRISE', Prado::localize('TEXT_ENTREPRISE'), $mail);
        $mail = str_replace('DEFINE_NOM', Prado::localize('DEFINE_NOM'), $mail);
        $mail = str_replace('DEFINE_PRENOM', Prado::localize('DEFINE_PRENOM'), $mail);
        $mail = str_replace('TEXT_MAIL_DEST', Prado::localize('TEXT_MAIL_DEST'), $mail);
        $mail = str_replace('TEXT_ADRESSE_ELECTRONIQUE', Prado::localize('TEXT_ADRESSE_ELECTRONIQUE'), $mail);
        $mail = str_replace('HORODATAGE_DEPOT', Prado::localize('HORODATAGE_DEPOT'), $mail);
        $mail = str_replace('DEFINE_TEXT_REFERENCE', Prado::localize('DEFINE_TEXT_REFERENCE'), $mail);
        $mail = str_replace('DEFINE_INTITULE', Prado::localize('DEFINE_INTITULE'), $mail);
        $mail = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau($commonPlateformeVirtuelle), $mail);
        $mail = str_replace('COULEUR_TITRE', Atexo_Config::getParameter('MAIL_COULEUR_TITRE'), $mail);
        $mail = str_replace('PF_LONG_NAME', $pfName, $mail);
        $mail = str_replace('CORDIALEMENT', Prado::localize('CORDIALEMENT'), $mail);
        $mail = str_replace('PIECE_JOINTE', Prado::localize('PIECE_JOINTE'), $mail);
        $mail = str_replace('CARACTERISTIQUE_DEPOT', Prado::localize('CARACTERISTIQUE_DEPOT'), $mail);
        $mail = str_replace('CONTENU_REPONSE', Prado::localize('CONTENU_REPONSE'), $mail);
        $mail = str_replace('TEXT_CONTENU_QUESTION', Prado::localize('TEXT_CONTENU_QUESTION'), $mail);
        $mail = str_replace('TEXT_QUESTION', Prado::localize('QUESTION'), $mail);
        $mail = str_replace('TEXT_PERSONNE_DEPOSEE_QUESTION', Prado::localize('TEXT_PERSONNE_DEPOSEE_QUESTION'), $mail);
        $mail = str_replace('TEXT_LIEN_ACCES_DIRECT_CONSULTATION', Prado::localize('TEXT_LIEN_ACCES_DIRECT_CONSULTATION'), $mail);
        $mail = str_replace('PF_ACHAT_D_ETAT', Prado::localize('PF_ACHAT_D_ETAT'), $mail);

        return $mail;
    }

    /**
     * Permet de recuperer le contenu de l'email d'accusé de reception d'une reponse electronique deposée.
     *
     * @param string $reference           la reference de la consultation
     * @param string $organisme           l'organisme de la consultation
     * @param array  $arrayInfosFiles     contient les infos des fichier de la reponse
     * @param array  $arrayInfosInscrit   contient les infos de l'inscrit
     * @param bool   $acteEngagementSigne
     *
     * @return string $mail le contenu html du mail a envoyer
     *
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getContentMailEseForOffreAR($reference, $organisme, $arrayInfosFiles, $arrayInfosInscrit, $acteEngagementSigne)
    {
        $filesResponse = null;
        $consultation = (new Atexo_Consultation())->retrieveCommonConsultation($reference, $organisme);
        if (null != $consultation && $consultation instanceof CommonConsultation) {
            //text from msg
            $cheminTemplate = Atexo_Config::getParameter('CHEMIN_FICHIER_MAIL_ACCUSE_RECEPTION_OFFRE_ESE');
            $mail = (new self())->getTextForMailAr($cheminTemplate);
            //infos from ConsObject
            $mail = (new self())->getInfosConsForMailAR($consultation, $mail);

            $mail = str_replace('TEXT_TITRE', Prado::localize('ACCUSE_RECEPTION_REPONSE_ELECTRONIQUE'), $mail); //pr depot ou question
            $mail = str_replace('TEXT_ACCUSE_RECEPTION', Prado::localize('REPONSE_ELECTRONIQUE_POUR_CONSULTATION_CI_DESSOUS'), $mail);
            $mail = str_replace('VALEUR_HORODATAGE', Atexo_Util::iso2frnDateTime($arrayInfosFiles['dateDepot'], false, true), $mail);
            //recupere la liste des fichier deponsé ds la reponse
            if ($arrayInfosFiles['candidatureMPS']) {
                $filesResponse = $arrayInfosFiles['candidatureMPS'];
                $filesResponse = '<p>'.$filesResponse.'</p>';
            }
            $arrayFilesResponse = (new Atexo_Entreprise_Reponses())->getNomsFichiersFromArray($arrayInfosFiles, $acteEngagementSigne, '', true, null);

            $filesResponse .= self::getPiecesFormatHtml($arrayFilesResponse);
            $mail = str_replace('TEXT_INFOS_FILE', $filesResponse, $mail);
            $mail = str_replace('VALEUR_NOM_ENTREPRISE', $arrayInfosInscrit['nomEse'], $mail);
            $mail = str_replace('VALEUR_NOM_DEPOSEUR', $arrayInfosInscrit['nomInscrit'], $mail);
            $mail = str_replace('VALEUR_PRENOM_DEPOSEUR', $arrayInfosInscrit['prenomInscrit'], $mail);
            $mail = str_replace('VALEUR_MAIL_DEPOSEUR', $arrayInfosInscrit['mailInscrit'], $mail);
            //infos from destinataire
            $mail = str_replace('VALEUR_NOM_DEST', $arrayInfosInscrit['nomInscrit'], $mail);
            $mail = str_replace('VALEUR_PRENOM_DEST', $arrayInfosInscrit['prenomInscrit'], $mail);
            $mail = str_replace('FACTURATION_OBLIGATOIRE', (('FACTURATION_ELECTRONIQUE_OBLIGATOIRE' == Prado::Localize('FACTURATION_ELECTRONIQUE_OBLIGATOIRE')) ? '' : Prado::Localize('FACTURATION_ELECTRONIQUE_OBLIGATOIRE')), $mail);
            $mail = str_replace('UTILISER_PORTAIl_POUR_FACTURATION', (('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION' == Prado::Localize('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION')) ? '' : Prado::Localize('UTILISER_PORTAIL_CHORUS_POUR_FACTURATION')), $mail);

            return $mail;
        }
    }

    /**
     * Permet de recuperer le contenu de l'email d'accusé de reception d'une reponse electronique deposée.
     *
     * @param string      $reference       la reference de la consultation
     * @param string      $organisme       l'organisme de la consultation
     * @param array       $arrayInfosFiles contient les infos des fichier de la reponse
     * @param CommonAgent $agent           objet commonAgent
     *
     * @return string $mail le contenu html du mail a envoyer
     *
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getContentMailAgentForOffreAR($reference, $organisme, $arrayInfosFiles, $agent)
    {
        $consultation = (new Atexo_Consultation())->retrieveCommonConsultation($reference, $organisme);
        if (null != $consultation && $consultation instanceof CommonConsultation) {
            //text from msg
            $cheminTemplate = Atexo_Config::getParameter('CHEMIN_FICHIER_MAIL_ACCUSE_RECEPTION_OFFRE_AGENT');
            $mail = (new self())->getTextForMailAr($cheminTemplate);
            //infos from ConsObject
            $mail = (new self())->getInfosConsForMailAR($consultation, $mail, true);

            $mail = str_replace('TEXT_TITRE', Prado::localize('RECEPTION_REPONSE_ELECRONIQUE'), $mail); //pr depot ou question
            $mail = str_replace('TEXT_ACCUSE_RECEPTION', Prado::localize('REPONSE_DEPOSE_POUR_CONSULTATION'), $mail);
            $mail = str_replace('VALEUR_HORODATAGE', Atexo_Util::iso2frnDateTime($arrayInfosFiles['dateDepot'], false, true), $mail);
            $pfUrl = Atexo_MultiDomaine::replaceDomain(
                Atexo_Config::getParameter('PF_URL_MESSAGERIE'),
                Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
            );
            $urlAccesDirect = $pfUrl.'?page=Agent.TableauDeBord&AS=0&id='.$reference;
            $mail = str_replace('VALEUR_LIEN_ACCES_DIRECT_CONSULTATION', $urlAccesDirect, $mail);
            //infos from destinataire
            $mail = str_replace('VALEUR_NOM_DEST', $agent->getNom(), $mail);
            $mail = str_replace('VALEUR_PRENOM_DEST', $agent->getPrenom(), $mail);
            return $mail;
        }
    }

    /**
     * Permet de retourner les pieces en format html.
     *
     * @param array $arrayFilesResponse array de piece de la reponse
     *
     * @return string $html le contenu html du mail a envoyer
     *
     * @author : AME : <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getPiecesFormatHtml($arrayFilesResponse)
    {
        $html = '';
        if (is_array($arrayFilesResponse)) {
            $i = 0;
            foreach ($arrayFilesResponse as $filesResponse) {
                $style = '';
                if ($i % 2) {
                    $style = ' bgcolor="#ffffff"';
                }
                $html .= '<tr '.$style.'>
							<td colspan="2" style="word-break:break-all" valign="top">'.$filesResponse['piece'].' : <strong>'.$filesResponse['nomFichier'].'</strong> ('.$filesResponse['fileSize'].')</td>
						</tr>
						<tr '.$style.'>
							<td valign="top" width="40">
								<div class="statut">
									<img src="'.self::getPictoCertificat($filesResponse['signature']).'"
									   	 alt="'.self::getTitleCertificat($filesResponse['signature']).'"
									 	 style="vertical-align:middle"> :';
                if ($filesResponse['StatutSignature'] != Atexo_Config::getParameter('SANS')) {
                    $html .= '<img src="'.self::getPictoStatutSignature($filesResponse['signature'], $filesResponse['StatutSignature']).'"
										 alt="'.self::getTitleStatutSignature($filesResponse['signature'], $filesResponse['StatutSignature']).'"
										 style="vertical-align:middle">';
                } else {
                    $html .= '&nbsp;&nbsp;&ndash;&nbsp;&nbsp;&nbsp;';
                }
                $html .= '</div>
							</td>
							<td valign="top">'.
                    self::getDetailsCertificat($filesResponse).
                    '</td>
						</tr>';
                ++$i;
            }
        }

        return $html;
    }

    /**
     * Permet de recuperer le detail du certificat signataire.
     *
     * @param array $filesResponse tableau des infos du fichier
     *
     * @return string infos du certificat
     *
     * @author : Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2015
     */
    public function getDetailsCertificat($filesResponse)
    {
        if ($filesResponse['signature']) {
            return Prado::localize('CN').' : '.$filesResponse['CnSubject'].' ('.Prado::localize('DEFINE_AC').' : '.Prado::localize('CN').'='.$filesResponse['CnEmmeteur'].')';
        }

        return Prado::localize('TEXT_AUCUNE_SIGNATURE');
    }

    /**
     * Permet de recuperer le picto du certificat.
     *
     * @param bool $signature si le fichier est signe
     *
     * @return string path du picto
     *
     * @author : Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2015
     */
    public function getPictoCertificat($signature)
    {
        if ($signature) {
            return Atexo_Controller_Front::getThemesPath().'/images/picto-certificat-small.gif';
        }

        return Atexo_Controller_Front::getThemesPath().'/images/picto-aucune-signature.gif';
    }

    /**
     * Permet de recuperer le title du certificat.
     *
     * @param bool $signature si le fichier est signe
     *
     * @return string title
     *
     * @author : Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2015
     */
    public function getTitleCertificat($signature)
    {
        if ($signature) {
            return Prado::localize('TEXT_CERTIFICAT');
        }

        return Prado::localize('TEXT_SIGNATURE_ELECTRONIQUE_MANQUANTE');
    }

    /**
     * Permet de recuperer le picto du statut de la signature.
     *
     * @param bool   $signature si le fichier est signe
     * @param string $statut    statut de la signature
     *
     * @return string path du picto correspondant au statut de la signature
     *
     * @author : Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2015
     */
    public function getPictoStatutSignature($signature, $statut)
    {
        $statut = self::getStatutSignature($signature, $statut);

        return $statut['picto'];
    }

    /**
     * Permet de recuperer le title du statut de la signature.
     *
     * @param bool   $signature si le fichier est signe
     * @param string $statut    statut de la signature
     *
     * @return string title correspondant au statut de la signature
     *
     * @author : Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2015
     */
    public function getTitleStatutSignature($signature, $statut)
    {
        $statut = self::getStatutSignature($signature, $statut);

        return $statut['title'];
    }

    /**
     * Permet de recuperer le picto du statut de la signature.
     *
     * @param bool   $signature si le fichier est signe
     * @param string $statut    statut de la signature
     *
     * @return array infos du statut de la signature
     *
     * @author : Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2015
     */
    public function getStatutSignature($signature, $statut)
    {
        $arrayStatutSignature = [];
        if ($signature) {
            switch (strtoupper($statut)) {
                case Atexo_Config::getParameter('OK'):
                    $arrayStatutSignature['picto'] = Atexo_Controller_Front::getThemesPath().'/images/picto-check-ok-small.gif';
                    $arrayStatutSignature['title'] = Prado::localize('TEXT_VERIFICATION_OK');
                    break;
                case Atexo_Config::getParameter('UNKNOWN'):
                    $arrayStatutSignature['picto'] = Atexo_Controller_Front::getThemesPath().'/images/picto-validation-incertaine.gif';
                    $arrayStatutSignature['title'] = Prado::localize('TEXT_VERIFICATION_INCERTAINE');
                    break;
                case Atexo_Config::getParameter('NOK'):
                    $arrayStatutSignature['picto'] = Atexo_Controller_Front::getThemesPath().'/images/picto-check-not-ok.gif';
                    $arrayStatutSignature['title'] = Prado::localize('TEXT_VERIFICATION_NOK');
                    break;
                default:
                    $arrayStatutSignature['picto'] = Atexo_Controller_Front::getThemesPath().'/images/picto-vide.gif';
                    $arrayStatutSignature['title'] = Prado::localize('TEXT_AUCUNE_SIGNATURE');
                    break;
            }
        } else {
            $arrayStatutSignature['picto'] = Atexo_Controller_Front::getThemesPath().'/images/picto-vide.gif';
            $arrayStatutSignature['title'] = Prado::localize('TEXT_AUCUNE_SIGNATURE');
        }

        return $arrayStatutSignature;
    }

    /**
     * Permet de recuperer l'image du bandeau des emails.
     *
     * @return string: l'image du bandeau si trouve, sinon chaine vide
     *
     * @throws Atexo_Config_Exception
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public static function getImageBandeau(CommonPlateformeVirtuelle $commonPlateformeVirtuelle = null)
    {
        $sourceImageBandeau = null;
		if(!empty(Atexo_Config::getParameter('SOURCE_IMAGE_BANDEAU'))) {
            $option = [
                'slash' => true,
                'multi_domaine' => Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
            ];
            $pfUrl = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_MESSAGERIE'), $option);

            $sourceImageBandeau = $pfUrl.trim(Atexo_Config::getParameter('SOURCE_IMAGE_BANDEAU'), '/');
            $codeBandeauImage = html_entity_decode(Atexo_Config::getParameter('CODE_BANDEAU_IMAGE'));
            $sourceImageBandeau = str_replace('SOURCE_IMAGE_BANDEAU', $sourceImageBandeau, $codeBandeauImage);
            if (isset($_COOKIE['design'])) {
                $sourceImageBandeau = str_replace(
                    '/themes/images/',
                    '/themes/'.$_COOKIE['design'].'/images/',
                    $sourceImageBandeau
                );
            }

            if (null !== $commonPlateformeVirtuelle) {
                $sourceImageBandeau = str_replace(
                    '/themes/images/',
                    '/themes/'.$commonPlateformeVirtuelle->getCodeDesign().'/images/',
                    $sourceImageBandeau
                );
            }
        }

        return $sourceImageBandeau;
    }

    /**
     * Permet de recuperer le contenu de l'email de synthèse des consultations achevees.
     *
     * @param CommonConsultation         $consultation la consultation
     * @param Atexo_Consultation_GuestVo $guest        l'invite
     *
     * @return string $mail le contenu html du mail a envoyer
     *
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getContentMailForSynthesisAchievedConsultation($consultation)
    {
        if ($consultation instanceof CommonConsultation) {
            $pfReference =  Atexo_Config::getParameter('PF_URL_AGENT') != "/" ?
                Atexo_Config::getParameter('PF_URL_AGENT') :
                Atexo_Config::getParameter('PF_URL_MESSAGERIE');

            $pfUrl = Atexo_MultiDomaine::replaceDomain(
                $pfReference,
                Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
            );
            $plateformeVirtuelle = null;

            if ($consultation instanceof CommonConsultation
                && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
                $pfUrl = $plateformeVirtuelle->getProtocole()
                    .'://'
                    .$plateformeVirtuelle->getDomain().'/';
            }

            $cheminTemplate = Atexo_Config::getParameter('CHEMIN_FICHIER_MAIL_SYNTHESE_CONSULTATION_ACHEVEES');
            $template = (new self())->getTextForMailAr($cheminTemplate, $plateformeVirtuelle);

            $mail = (new self())->getInfosConsForMailAR($consultation, $template, true);
            $mail = str_replace('TEXT_SYNTHESE', Prado::localize('DEFINE_TEXT_MAIL_CONSULTATION_CLOTURE').' '.date('d/m/Y').' :', $mail);
            $mail = str_replace('TEXT_TITRE', Prado::localize('DEFINE_TEXT_OBJET_MAIL_CONSULTATION_CLOTURE'), $mail);
            $mail = str_replace('TEXT_NOMBRE_RETRAIT_ELECTRONIQUE', Prado::localize('DEFINE_TEXT_NOMBRE_RETRAIT_ELECTRONIQUE'), $mail);
            $mail = str_replace('VALEUR_NOMBRE_RETRAIT_ELECTRONIQUE', $consultation->getNombreTelecharegementElectronique($consultation->getOrganisme()), $mail);
            $mail = str_replace('TEXT_NOMBRE_DEPOT_ELECTRONIQUE', Prado::localize('DEFINE_TEXT_NOMBRE_DEPOT_ELECTRONIQUE'), $mail);
            $mail = str_replace('VALEUR_NOMBRE_DEPOT_ELECTRONIQUE', $consultation->getNombreOffreElectronique($consultation->getOrganisme()), $mail);
            $mail = str_replace('VALEUR_LIEN_ACCES_DIRECT_CONSULTATION', $pfUrl.'?page=Agent.TableauDeBord&AS=0&id='.$consultation->getId(), $mail);

            return $mail;
        }
    }

    /**
     * Permet de recuperer le contenu de l'email de synthèse des consultations achevees.
     *
     * @param CommonConsultation         $consultation la consultation
     * @param Atexo_Consultation_GuestVo $guest        l'invite
     *
     * @return string $mail le contenu html du mail a envoyer
     *
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getContentMailForPublicite($consultation)
    {
        if ($consultation instanceof CommonConsultation) {
            $cheminTemplate = Atexo_Config::getParameter('CHEMIN_FICHIER_MAIL_PUBLICATION_BOAMP');
            $template = (new self())->getTextForMailAr($cheminTemplate);
            $mail = (new self())->getInfosConsForMailAR($consultation, $template, true);
            $pfReference =  Atexo_Config::getParameter('PF_URL_AGENT') != "/" ?
                Atexo_Config::getParameter('PF_URL_AGENT') :
                Atexo_Config::getParameter('PF_URL_MESSAGERIE');

            $pfUrl = Atexo_MultiDomaine::replaceDomain(
                $pfReference,
                Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
            );
            $mail = str_replace('VALEUR_LIEN_ACCES_DIRECT_CONSULTATION', $pfUrl.'?page=Agent.TableauDeBord&AS=0&id='.$consultation->getId(), $mail);

            return $mail;
        }
    }

    /**
     * Permet de construire le message de la cartouche d'information des mails de retour Chorus.
     *
     * @param CommonTContratTitulaire $contrat : objet contrat titulaire
     * @param string $numeroOrdre : numero d'ordre
     * @param Atexo_Consultation_GuestVo $guest : objet agent Vo
     * @param string $corps : coprs du message
     *
     * @param string $titre : titre du message
     * @param null $logger : logger
     * @return bool|string|array : le message, false sinon
     *
     * @author : Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getInfosMailsChorus($contrat, $numeroOrdre, $guest, $corps, $titre, $logger = null): bool|string|array
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        $infosCartouche = [];
        if ($contrat) {
            try {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

                $entityPurchase = CommonServiceQuery::create()->filterById($contrat['idService'])->findOne($connexion);
                $objetOrganisme = Atexo_Organismes::retrieveOrganismeByAcronyme($contrat['organisme']);
                $entitePublique = ($objetOrganisme instanceof CommonOrganisme) ? $objetOrganisme->getSigle().' - '.$objetOrganisme->getDenominationOrgTraduit() : '';
                if ($entityPurchase instanceof CommonService) {
                    $entiteAchat = $entityPurchase->getSigle().' - '.$entityPurchase->getLibelleTraduit();
                } elseif ($objetOrganisme instanceof CommonOrganisme) {
                    $entiteAchat = $objetOrganisme->getSigle().' - '.$objetOrganisme->getDenominationOrgTraduit();
                }

                $titulaireMarche = $contrat['contactTitulaire']['raisonSociale'];

                //content BLOC TABLEAU
                $infosCartouche['ENTITE_PUBLIQUE']['INFO'] = (!(empty($entitePublique))) ? $entitePublique : '-';
                $infosCartouche['ENTITE_PUBLIQUE']['LIB'] = Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC');
                $infosCartouche['ENTITE_ACHAT']['INFO'] = (!empty($entiteAchat)) ? $entiteAchat : '-';
                $infosCartouche['ENTITE_ACHAT']['LIB'] = Prado::localize('DEFINE_TEXT_ENTITE_ACHAT');
                $infosCartouche['REFERENCE_MARCHE']['INFO'] = (!empty($contrat['referenceLibre'])) ? $contrat['referenceLibre'] : '-';
                $infosCartouche['REFERENCE_MARCHE']['LIB'] = Prado::localize('TEXTE_REFERENCE_MARCHE');
                $infosCartouche['INTITULE_MARCHE']['INFO'] = (!empty($contrat['intitule'])) ? $contrat['intitule'] : '-';
                $infosCartouche['INTITULE_MARCHE']['LIB'] = Prado::localize('TEXTE_INTITULE_MARCHE');
                $infosCartouche['OBJET_MARCHE']['INFO'] = (!empty($contrat['objet'])) ? $contrat['objet'] : '-';
                $infosCartouche['OBJET_MARCHE']['LIB'] = Prado::localize('DEFINE_OBJET_MARCHE');
                $infosCartouche['TITULAIRE_MARCHE']['INFO'] = (!empty($titulaireMarche)) ? $titulaireMarche : '-';
                $infosCartouche['TITULAIRE_MARCHE']['LIB'] = Prado::localize('DEFINE_TITULAIRE_MARCHE');
                $infosCartouche['DATE_NOTIFICATION']['INFO'] = (!empty($contrat['dateNotification'])) ? Atexo_Util::iso2frnDate($contrat['dateNotification']) : '-';
                $infosCartouche['DATE_NOTIFICATION']['LIB'] = Prado::localize('DATE_NOTIFICATION');
                $infosCartouche['NUMERO_ENVOI']['INFO'] = (!empty($numeroOrdre)) ? $numeroOrdre : '-';
                $infosCartouche['NUMERO_ENVOI']['LIB'] = Prado::localize('TEXTE_NUMERO_ENVOI');
                if ((new Atexo_Consultation_Contrat())->isTypeContratAcSad($contrat['type']['codeExterne'])) {
                    $infosCartouche['NUMERO_EJ']['INFO'] = '';
                    $infosCartouche['NUMERO_EJ']['LIB'] = '';
                } else {
                    $infosCartouche['NUMERO_EJ']['INFO'] = (!empty($contrat['numero'])) ? $contrat['numero'] : '-'; #todo vérifier avec equipe exec
                    $infosCartouche['NUMERO_EJ']['LIB'] = Prado::localize('TEXT_NUMERO_EJ');
                }

                $pfReference =  Atexo_Config::getParameter('PF_URL_AGENT') != "/" ?
                    Atexo_Config::getParameter('PF_URL_AGENT') :
                    Atexo_Config::getParameter('PF_URL_MESSAGERIE');

                $pfUrl = Atexo_MultiDomaine::replaceDomain(
                    $pfReference,
                    Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
                );
                //Texte FOOTER
                $infosCartouche['LIEN_ACCES_ECHANGE']['LIB'] = Prado::localize('TEXT_LIEN_ACCES_DIRECT_DETAIL_ENVOI_CHORUS');
                $infosCartouche['LIEN_ACCES_ECHANGE']['INFO'] = $pfUrl.'?page=Agent.ChorusEchanges&uuid=' . $contrat['uuid'];

                //Texte FOOTER
                $infosCartouche['CORDIALEMENT'] = Prado::localize('CORDIALEMENT');
                $infosCartouche['BAS_PAGE_MAIL'] = Prado::localize('PF_ACHAT_D_ETAT');

                //HEADER
                $infosCartouche['IMAGE_BANDEAU'] = Atexo_Message::getImageBandeau();

                //Texte CONTENU
                $infosCartouche['SALUTATION'] = str_replace('[__PRENOM_NOM__]', ($guest instanceof Atexo_Consultation_GuestVo) ? $guest->getFirstName().' '.$guest->getLastName() : '', Prado::localize('DEFINE_TITRE_OBJET_MAIL'));
                $infosCartouche['CORPS_MAIL'] = $corps;

                //Titre OBJET
                $infosCartouche['TITRE'] = $titre;

                //Titre
                $infosCartouche['PLATEFORME_ACHAT_ETAT'] = Prado::localize('PLATEFORME_ACHAT_ETAT');
            } catch (\Exception $e) {
                $erreur = "Erreur lors de la construction de la cartouche d'infos des mails d'integration CHORUS [id_contrat=" . $contrat['uuid'] . ' , org='. $contrat['organisme'] ." , numOrdre=$numeroOrdre]";
                $erreur .= PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace:'.$e->getTraceAsString();
                $logger->error($erreur);

                return false;
            }
        } else {
            $logger->error("Le contrat n'est pas une instance de CommonTContratTitulaire : ".print_r($contrat, true));

            return false;
        }

        return $infosCartouche;
    }

    /**
     * Permet d'envoyer le mail contenant le PIN SGMAP MPS aux agents.
     *
     * @param CommonConsultation $consultation
     * @param \Logger            $logger
     *
     * @return bool
     *
     * @author : Oumar KONATE : <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR2015
     *
     * @copyright Atexo 2016
     */
    public function envoyerMailsAgentsPinSgmapMps($consultation, $logger = null)
    {
        $titre = null;
        $corps = null;
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('app');
        }
        try {
            if ($consultation instanceof CommonConsultation) {
                $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents($consultation->getId(), $consultation->getOrganisme());
                if (is_array($guests) && !empty($guests)) {
                    $pfName = null;
                    $from = (new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('PF_MAIL_FROM', $consultation->getOrganisme()));

                    if ($consultation instanceof CommonConsultation
                        && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                        $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
                        $from = $plateformeVirtuelle->getNoReply();
                        $pfName = $plateformeVirtuelle->getFromPfName();
                    }

                    $titre = str_replace('{__REF_UTILISATEUR_CONSULTATION__}', $consultation->getReferenceUtilisateur(), prado::localize('INTITULE_MAIL_AGENT_PIN_SGMAP_MPS'));
                    $corps = (new Atexo_Util())->toHttpEncoding(file_get_contents(Atexo_Config::getParameter('CHEMIN_FICHIER_MAIL_AGENT_PIN_SGMAP_MPS')));
                    //content BLOC TABLEAU
                    $corps = str_replace('REFERENCE_CONSULTATION_LIB', Prado::localize('DEFINE_TEXT_REFERENCE'), $corps);
                    $corps = str_replace('REFERENCE_CONSULTATION_INFO', (!empty($consultation->getReferenceUtilisateur())) ? $consultation->getReferenceUtilisateur() : '-', $corps);
                    $corps = str_replace('INTITULE_CONSULTATION_LIB', Prado::localize('DEFINE_INTITULE'), $corps);
                    $corps = str_replace('INTITULE_CONSULTATION_INFO', (!empty($consultation->getIntituleTraduit())) ? $consultation->getIntituleTraduit() : '-', $corps);
                    $corps = str_replace('MOT_PASSE_LIB', Prado::localize('DEFINE_VOTRE_MOT_PASSE'), $corps);
                    $corps = str_replace('MOT_PASSE_INFO', (!empty($consultation->getPinApiSgmapMps())) ? $consultation->getPinApiSgmapMps() : '-', $corps);
                    $corps = str_replace('LIEN_CANDIDATURE_MPS_LIB', Prado::localize('MESSAGE_ACCES_LIEN_CANDIDATURES_MPS'), $corps);
                    $corps = str_replace('LIEN_CANDIDATURE_MPS_INFO', '<a target=_blank href="'.(new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('URL_API_SGMAP_MPS')).'">'.(new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('URL_API_SGMAP_MPS')).'</a>', $corps);
                    $corps = str_replace('LIEN_CONSULTATION_MPE_LIB', Prado::localize('MESSAGE_ACCES_LIEN_CONSULTATION_MPE'), $corps);

                    $pfReference =  Atexo_Config::getParameter('PF_URL_AGENT') != "/" ?
                        Atexo_Config::getParameter('PF_URL_AGENT') :
                        Atexo_Config::getParameter('PF_URL_MESSAGERIE', $consultation->getOrganisme());

                    $pfUrl = Atexo_MultiDomaine::replaceDomain(
                        $pfReference,
                        Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
                    );
                    $urlConsultation = (new Atexo_Util())->toHttpEncoding($pfUrl).'?page=Agent.TableauDeBord&AS=0&id='.$consultation->getId();
                    $corps = str_replace('LIEN_CONSULTATION_MPE_INFO', '<a target=_blank href="'.$urlConsultation.'">'.$urlConsultation.'</a>', $corps);
                    //Texte FOOTER
                    $corps = str_replace('MESSAGE_FOOTER', Prado::localize('MESSAGE_FOOTER_MAIL_AGENT_PIN_SGMAP_MPS'), $corps);
                    //Texte FOOTER
                    $corps = str_replace('CORDIALEMENT', Prado::localize('CORDIALEMENT'), $corps);
                    $corps = str_replace('SIGNATURE_PLATEFORME', (new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('PF_LONG_NAME')), $corps);
                    //HEADER
                    $corps = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $corps);
                    //Texte CONTENU
                    $corps = str_replace('SALUTATION', Prado::localize('DEFINE_BONJOUR'), $corps);
                    $corsMail = str_replace('{__LINK_TO_PF_EXTERNE__}', '<a target=_blank href="'.(new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('URL_API_SGMAP_MPS')).'">'.Prado::localize('DEFINE_MARCHE_PUBLIC_SIMPLIFIE_MPS').'</a>', str_replace('{__NOM_PF_MPE__}', (new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('PF_LONG_NAME')), Prado::localize('DEFINE_CORPS_MAIL_SGMAP_MPS')));
                    $corps = str_replace('CORPS_MAIL', $corsMail, $corps);
                    //Titre OBJET
                    $corps = str_replace('TITRE', $titre, $corps);
                    //Titre
                    $corps = str_replace('PLATEFORME_ACHAT_ETAT', (new Atexo_Util())->toHttpEncoding(Atexo_Config::getParameter('PF_LONG_NAME')), $corps);

                    foreach ($guests as $guest) {
                        if ($guest instanceof Atexo_Consultation_GuestVo) {
                            Atexo_Message::simpleMail(
                                $from,
                                $guest->getMailAgent(),
                                $titre,
                                $corps,
                                '',
                                '',
                                false,
                                true,
                                true,
                                $pfName
                            );
                        } else {
                            $logger->error("Le destinataire n'est pas une instance de Atexo_Consultation_GuestVo : ".print_r($guest, true));
                            continue;
                        }
                    }
                }
            } else {
                $logger->error("La consultation n'est pas une instance de CommonConsultation : ".print_r($consultation, true));
            }
        } catch (\Exception $e) {
            $erreur = "Erreur lors de l'envoi du mail (PIN SGMAP) aux agents : [from='$from' , subjet='$titre']".PHP_EOL."Corps message : $corps";
            $erreur .= PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace:'.$e->getTraceAsString();
            $logger->error($erreur);

            return false;
        }
    }

    /**
     * retourne les ids des echanges d'une consultation.
     *
     * @param $organisme l'organisme
     * @param $consultationId la reference de la consultation
     *
     * @return array contenant les Ids des echanges d'une consultation
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public static function getEchangesIdsByRefConsultation($consultationId, $org)
    {
        $arrayIds = [];
        $echanges = (new Atexo_Chorus_Echange())->retreiveEchangesByRefConsultation($consultationId, $org);
        if (is_array($echanges) && count($echanges)) {
            foreach ($echanges as $echange) {
                $arrayIds[] = $echange->getId();
            }
        }

        return $arrayIds;
    }

    /**
     * retourne l'echange.
     *
     * @param $organisme l'organisme
     * @param $id l'id de l'echange
     *
     * @return CommonEchange l'objet echange
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public static function retrieveEchangeById($organisme, $id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEchangePeer::ID, $id);
        $c->add(CommonEchangePeer::ORGANISME, $organisme);
        $echange = CommonEchangePeer::doSelectOne($c, $connexion);
        if ($echange instanceof CommonEchange) {
            return $echange;
        }

        return false;
    }

    /**
     * Permet de setter les information de decision pour le WS.
     *
     * @param $organisme
     * @param $id
     * @param $typeDepotOffre
     * @param $xmlResponse
     * @param $logger
     * return void|string
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function setInfoEchangeForWs($echange, &$xmlResponse, $logger)
    {
        if ($echange instanceof CommonEchange) {
            $logger->info("L'echange existe");
            $serializer = new Atexo_Serializer_Serializer();
            $mappingFile = Atexo_Config::getParameter('PATH_FILE_MAPPING_ECHANGE');
            $hiddenField = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_ECHANGE');
            $logger->info("La recuperation de l'xml à partir de l'objet echange");
            $xmlResponse = $serializer->SerializeClass($echange, $echange::class, $mappingFile, $hiddenField);
        } else {
            return "Cet echange n'existe pas";
        }
    }

    public function render($template)
    {
        $template = file_get_contents(Atexo_Config::getParameter($template));

        return $template;
    }

    /**
     * Permet de construire et d'envoyer le mail de suppression de l'EJ a l'agent createur du contrat.
     *
     * @param string $tmpPathPj
     * @param string $fileName
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public static function envoiMailAgentDemandeSuppressionEj(CommonTContratTitulaire $contrat, $tmpPathPj, $fileName)
    {
        $agent = (new Atexo_Agent())->retrieveAgent($contrat->getIdAgent());
        if ($agent instanceof CommonAgent && !empty($agent->getEmail())) {
            $dernierNumEj = (new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($contrat->getNumeroContrat());

            $objetMessage = str_replace('[__NUMERO_EJ__]', $dernierNumEj, Prado::localize('OBJET_MESSAGE_AGENT_DEMANDE_SUPPRESSION_EJ_CHORUS'));

            $corpsMessage = Prado::localize('DEFINE_TEXT_BONJOUR');
            $corpsMessage .= '<br/><br/>';
            $corpsMessage .= Prado::localize('CORPS_MESSAGE_AGENT_DEMANDE_SUPPRESSION_EJ_CHORUS');
            $corpsMessage = str_replace('[__NUMERO_CONTRAT__]', '<B>'.$contrat->getNumeroContrat().'</B>', $corpsMessage);
            $corpsMessage = str_replace('[__NUMERO_EJ__]', '<B>'.$dernierNumEj.'</B>', $corpsMessage);
            $corpsMessage .= '<br/><br/>';
            $corpsMessage .= Prado::localize('CORDIALEMENT').'.';
            $corpsMessage .= '<br/><br/>';
            $corpsMessage .= Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');
            $mailFormatHtml = (new Atexo_Message())->generateMailFormatHtml($objetMessage, $corpsMessage);

            /*
             * @todo : from a supprimer
             */
            (new Atexo_Message())->mailWithAttachement(Atexo_Config::getParameter('PF_MAIL_FROM'), trim($agent->getEmail()), $objetMessage, $mailFormatHtml, '', file_get_contents($tmpPathPj), $fileName, '', true, false);
        }
    }

    public static function retrieveMessageAccueilByDestinataire($destinataire, $authentifier = null)
    {
        $messageAccueilQuery = new CommonTMessageAccueilQuery();

        return $messageAccueilQuery->findCachedMessageAccueil($destinataire, $authentifier);
    }

    public static function sendMailProblemePubDumeToAgent($content, $email)
    {
        if ($email) {
            $message = file_get_contents(Atexo_Config::getParameter('PATH_FILE_MAIL_GENERIC_TEMPLATE'));
            $message = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $message);
            $message = str_replace('SALUTATION', Prado::localize('DEFINE_BONJOUR'), $message);
            $message = str_replace('TITRE', '', $message);
            $message = str_replace('CORPS_MAIL', $content, $message);
            $message = str_replace('PLACE_MARCHE_PUBLIC_INTERMINISTERE', Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE'), $message);

            /*
             * @todo : From a remplacé !
             */
            Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), $email, Prado::localize('DUME_ACHETEUR_MAIL_AGENT_TITLE'), $message, '', '', false, true);
        }
    }

    /**
     * Permet de retourner les adresse mails des depot dume statut brouillon.
     *
     * @param int    $consultationId la reference de la consultation
     * @param string $organisme      l'acronyme de l'organisme
     *
     * @return string les adresse mails des inscrits qu'ils ont déposé un dume
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function getAdressesRegistreDepotDume($consultationId, $organisme)
    {
        $result = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $tCandidaturesDume = Atexo_Dume_AtexoDume::getCandidatureDumeOnlineByConsultation($consultationId, $organisme, Atexo_Config::getParameter('STATUT_CANDIDATUE_DUME_BROUILLON'), $connexionCom);
        if (is_array($tCandidaturesDume)) {
            foreach ($tCandidaturesDume as $tCandidatureDume) {
                if ($tCandidatureDume instanceof CommonTCandidature) {
                    $mailInscrit = (new Atexo_Entreprise_Inscrit())->getMailInscrit($tCandidatureDume->getIdInscrit());
                    if (!in_array($mailInscrit, $result) && $mailInscrit && '' != $mailInscrit) {
                        $result[] = $mailInscrit;
                    }
                }
            }
        }

        return implode(' , ', $result);
    }
}
