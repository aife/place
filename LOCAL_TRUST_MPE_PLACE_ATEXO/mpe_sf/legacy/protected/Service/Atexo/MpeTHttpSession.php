<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Service\Atexo;

use Prado\Web\THttpCookie;
use Prado\Web\THttpSession;

class MpeTHttpSession extends THttpSession
{
    /**
     * @var boolean whether the session has started
     */
    private $_started = false;

    /**
     * @var THttpCookie cookie to be used to store session ID and other data
     */
    private $_cookie = null;

    /**
     * @var boolean
     */
    private $_customStorage = false;

    public function open()
    {
        if (!$this->_started) {
            if ($this->_customStorage) {
                session_set_save_handler(
                    [$this, '_open'],
                    [$this, '_close'],
                    [$this, '_read'],
                    [$this, '_write'],
                    [$this, '_destroy'],
                    [$this, '_gc']
                );
            }
            if ($this->_cookie !== null) {
                session_set_cookie_params(
                    $this->_cookie->getExpire(),
                    $this->_cookie->getPath(),
                    $this->_cookie->getDomain(),
                    $this->_cookie->getSecure(),
                    $this->_cookie->getHttpOnly()
                );
            }

            $this->_started = true;
        }
    }
}
