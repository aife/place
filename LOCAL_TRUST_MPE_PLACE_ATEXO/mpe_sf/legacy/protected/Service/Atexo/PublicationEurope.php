<?php

namespace Application\Service\Atexo;

use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use DOMDocument;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_PublicationEurope
{
    public function generateTenderPublicationRQ($consultation, $action, $default_language)
    {
        $filePath = Atexo_Config::getParameter('CHEMIN_MODEL_TenderPublication_RQ');
        if (file_exists($filePath)) {
            $handle = fopen($filePath, 'r');
            $contents = fread($handle, filesize($filePath));
            fclose($handle);

            $lots = $consultation->getAllLots();
            $org = Atexo_Organismes::retrieveOrganismeByAcronyme($consultation->getOrganisme());
            $nominationOrg = utf8_encode($org->getDenominationOrg());
            $typeProcedureObject = (new Atexo_Consultation_ProcedureType())->getCommonTypeProcedureById($consultation->getIdTypeProcedure());
            $typeAvis = '';
            if (3 == $consultation->getIdTypeAvis()) {
                $typeAvis = 'AAPC';
            } elseif (4 == $consultation->getIdTypeAvis()) {
                $typeAvis = 'ATTRIB';
            }
            $urlConsultation = Atexo_Config::getParameter('PF_URL_REFERENCE').'?page=Entreprise.EntrepriseDetailConsultation&id='.$consultation->getId().'&orgAcronyme='.$consultation->getOrganisme().'&lang=';
            $langues = Atexo_Languages::retrieveActiveLangages();

            $noeudCpvSecondaires = '';
            $noeudTitre = '';
            $noeudObject = '';
            $noeudURL = '';
            $noeudLots = '';

            $tagCpvPrincipal = '<PrincipalObject>
					<PrincipalClass>C_TAGCPV_PRINCIPAL</PrincipalClass>
				</PrincipalObject>';
            $tagCpvSecondaire = '<SecundaryObject>
					<PrincipalClass>C_TAGCPV_PRINCIPAL</PrincipalClass>
				</SecundaryObject>';
            $tagDescription = '<Description language="TAG_LANG">
					<Content><![CDATA[TAG_VALUE]]></Content>
				</Description>';

            $arrayCodeCpvSec = explode('#', $consultation->getCodeCpv2());
            if (is_array($arrayCodeCpvSec) && $arrayCodeCpvSec[1]) {
                foreach ($arrayCodeCpvSec as $codeCpvSec) {
                    if ($codeCpvSec != $arrayCodeCpvSec[0]) {
                        $noeudCpvSecondaires .= str_replace('C_TAGCPV_PRINCIPAL', $codeCpvSec, $tagCpvSecondaire);
                    }
                }
            }
            if (is_array($langues)) {
                foreach ($langues as $langueO) {
                    $lang = '';
                    $lang = $langueO->getLangue();
                    $accessibilite = 'getAccessibilite'.strtoupper(substr($lang, 0, 1)).substr($lang, 1);

                    if ($lang == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
                        $titreLang = utf8_encode($consultation->getIntitule());
                        $noeudTitre .= str_replace('TAG_VALUE', $titreLang, str_replace('TAG_LANG', strtoupper($lang), $tagDescription));

                        $objectLang = utf8_encode($consultation->getObjet());
                        $noeudObject .= str_replace('TAG_VALUE', $objectLang, str_replace('TAG_LANG', strtoupper($lang), $tagDescription));

                        $noeudURL .= str_replace('TAG_VALUE', $urlConsultation.$lang, str_replace('TAG_LANG', strtoupper($lang), $tagDescription));
                    } elseif ($consultation->$accessibilite()) {
                        $titreLang = utf8_encode($consultation->getIntituleTraduit());
                        $noeudTitre .= str_replace('TAG_VALUE', $titreLang, str_replace('TAG_LANG', strtoupper($lang), $tagDescription));

                        $objectLang = utf8_encode($consultation->getObjetTraduit());
                        $noeudObject .= str_replace('TAG_VALUE', $objectLang, str_replace('TAG_LANG', strtoupper($lang), $tagDescription));

                        $noeudURL .= str_replace('TAG_VALUE', $urlConsultation.$lang, str_replace('TAG_LANG', strtoupper($lang), $tagDescription));
                    }
                }
            }

            if (is_array($lots) && isset($lots)) {
                foreach ($lots as $lot) {
                    $oneLot = '';
                    $oneLotCPV = '';
                    $oneLotDescription = '';
                    if ($lot->getCodeCpv1()) {
                        $oneLotCPV .= '<CPV>'.str_replace('C_TAGCPV_PRINCIPAL', $lot->getCodeCpv1(), $tagCpvPrincipal);

                        $arrayCodeCpvSec = explode('#', $lot->getCodeCpv2());
                        if (is_array($arrayCodeCpvSec) && $arrayCodeCpvSec[1]) {
                            foreach ($arrayCodeCpvSec as $codeCpvSec) {
                                if ($codeCpvSec != $arrayCodeCpvSec[0]) {
                                    $oneLotCPV .= str_replace('C_TAGCPV_PRINCIPAL', $codeCpvSec, $tagCpvSecondaire);
                                }
                            }
                        }

                        $oneLotCPV .= '</CPV>';
                    }

                    if (is_array($langues)) {
                        foreach ($langues as $langueO) {
                            $lang = $langueO->getLangue();
                            $accessibilite = 'getAccessibilite'.strtoupper(substr($lang, 0, 1)).substr($lang, 1);

                            if ($lang == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
                                $descriptionLang = utf8_encode($lot->getDescription());
                                $oneLotDescription .= str_replace('TAG_VALUE', $descriptionLang, str_replace('TAG_LANG', strtoupper($lang), $tagDescription));
                            } elseif ($consultation->$accessibilite()) {
                                $getDescription = 'getDescription'.strtoupper(substr($lang, 0, 1)).substr($lang, 1);
                                $descriptionLang = utf8_encode($lot->$getDescription());
                                $oneLotDescription .= str_replace('TAG_VALUE', $descriptionLang, str_replace('TAG_LANG', strtoupper($lang), $tagDescription));
                            }
                        }
                    }
                    $oneLot = $oneLotCPV;
                    if ('' != $oneLotDescription) {
                        $oneLot .= '<Title>'.$oneLotDescription.'</Title>';
                    }
                    if ('' != $oneLot) {
                        $noeudLots .= '<Lot>'.$oneLot.'</Lot>';
                    }
                }
            }
            $xmlLots = '';
            if ('' != $noeudLots) {
                $xmlLots = '<Lots>'.$noeudLots.'</Lots>';
            }
            $paramsNameXml = [
                                'C_TIMESTAMP',
                                'C_SIGLE_PLATEFORME',
                                'C_URL_PLATEFORME',
                                'C_NOM_PLATEFORME',
                                'C_STATUS',
                                'C_REFERENCE',
                                'C_TENDER_AVIS_TYPE',
                                'C_TENDER_PROCESS_TYPE',
                                'C_PUBLICATION_DATE',
                                'C_CLOSURE_DATE',
                                'C_DESCRIPTION_ENTITY_FR',
                                'C_DESCRIPTION_ENTITY_EN',
                                'C_CPV_PRINCIPAL',
                                '<!--C_CPV_SECONDAIRES-->',
                                '<!--C_TITRE-->',
                                '<!--C_OBJECT-->',
                                '<!--C_URL_TENDER-->',
                                '<!--C_LOTS-->', ];

            $paramsValueXml = [
                                str_replace(' ', 'T', date('Y-m-d h:i:s')).'.000+00:00',
                                utf8_encode(Atexo_Config::getParameter('SIGLE_PLATEFORME')),
                                Atexo_Config::getParameter('PF_URL_REFERENCE').'?page=Entreprise.EntrepriseHome',
                                Atexo_Util::escapeXmlChars(utf8_encode(Atexo_Config::getParameter('NOM_PLATEFORME'))),
                                $action,
                                Atexo_Util::escapeXmlChars(utf8_encode($consultation->getReferenceUtilisateur())),
                                $typeAvis,
                                $typeProcedureObject->getAbbreviationPortailEurope(),
                                str_replace(' ', 'T', $consultation->getDateMiseEnLigneCalcule()).'.000+00:00',
                                str_replace(' ', 'T', $consultation->getDatefin()).'.000+00:00',
                                $nominationOrg,
                                $nominationOrg,
                                $consultation->getCodeCpv1(),
                                $noeudCpvSecondaires,
                                $noeudTitre,
                                $noeudObject,
                                $noeudURL,
                                $xmlLots, ];
            $xmlGenere = str_replace($paramsNameXml, $paramsValueXml, $contents);

            $Dom = new DOMDocument();
            $Dom->loadXML($xmlGenere);
            $Dom->validateOnParse = true;
            $res = $Dom->schemaValidate(Atexo_Config::getParameter('CHEMIN_VALIDATE_TenderPublication_RQ'));
            if ($res) {
                return $xmlGenere;
            } else {
                echo false;
            }
        } else {
            echo 'Echec lors de l\'ouverture du fichier TenderPublication_RQ.xml.';
            exit;
        }
    }

    public function send($xml)
    {
        ini_set('soap.wsdl_cache_enabled', '0');
        $clientWsPortail = new \SoapClient(Atexo_Config::getParameter('PF_URL_REFERENCE').'ressources/consultation/PORTAIL_ETEN.wsdl');
        $msg = $clientWsPortail->importMessage(['arg0' => $xml]);

        return $msg;
    }

    public function test()
    {
        $consultation = (new Atexo_Consultation())->retrieveCommonConsultation('274', 'pmi-kbe-2');
        $xml = (new Atexo_PublicationEurope())->generateTenderPublicationRQ($consultation, 'TO_BE_PUBLISHED', 'fr');
        if ($xml) {
            $res = get_object_vars((new Atexo_PublicationEurope())->send($xml));
            $error = get_object_vars($res['return']);
            $importSuccessful = get_object_vars($res['return']);
            $arrayError = get_object_vars($error['error']);
            echo 'test '.$arrayError['error'].'</br>';
            var_dump($importSuccessful['importSuccessful']);
        } else {
            echo 'error générartion xml';
        }
        exit;
    }
}
