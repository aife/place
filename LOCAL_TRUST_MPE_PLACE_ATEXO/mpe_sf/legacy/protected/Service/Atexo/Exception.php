<?php

namespace Application\Service\Atexo;

use Exception;

/**
 * Exception spécifique Atexo.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Exception extends \Exception
{
}
