<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonJALPeer;

/**
 * commentaires.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Jal
{
    /**
     * Recherche des jals.
     */
    public static function search($idService = null, $org = null)
    {
        $c = new Criteria();
        $jals = [];
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonJALPeer::SERVICE_ID, $idService, Criteria::EQUAL);
        $c->add(CommonJALPeer::ORGANISME, $org, Criteria::EQUAL);
        $c->addAscendingOrderByColumn(CommonJALPeer::NOM);
        $jals = CommonJALPeer::doSelect($c, $connexionCom);

        return $jals;
    }

    /**
     * Supprime un jal.
     *
     * @param int $idJal l'id du jal
     */
    public static function delete($idJal, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $jal = CommonJALPeer::retrieveByPk($idJal, $connexionCom);
        CommonJALPeer::doDelete($jal, $connexionCom);
    }

    public static function retrieveJalById($idJal, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $jal = CommonJALPeer::retrieveByPk($idJal, $connexionCom);

        return $jal;
    }
}
