<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTCalendrier;
use Application\Propel\Mpe\CommonTCalendrierEtape;
use Application\Propel\Mpe\CommonTCalendrierEtapePeer;
use Application\Propel\Mpe\CommonTCalendrierEtapeReferentielPeer;
use Application\Propel\Mpe\CommonTCalendrierPeer;
use Application\Propel\Mpe\CommonTCalendrierTransition;
use Application\Propel\Mpe\CommonTCalendrierTransitionPeer;
use Application\Propel\Mpe\CommonTCalendrierTransitionReferentiel;
use Application\Propel\Mpe\CommonTCalendrierTransitionReferentielPeer;
use Exception;

/**
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class Atexo_GestionCalendrier
{
    /**
     * Recuperer le calendrier d'une consultation.
     *
     * @param reference consultation ,organisme
     *
     * @return ObjetCalendrier
     */
    public function retrieveCalendierByrefConsultationOrg($consultationId, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTCalendrierPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonTCalendrierPeer::ORGANISME, $org);
        $commonCalendrier = CommonTCalendrierPeer::doSelectOne($c, $connexion);

        return $commonCalendrier;
    }

    /**
     * Recuperer le calendrier d'une consultation.
     *
     * @param id calendrier
     *
     * @return ObjetCalendrier
     */
    public function retrieveCalendierById($idCalendier)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTCalendrierPeer::ID_CALENDRIER, $idCalendier);
        $commonCalendrier = CommonTCalendrierPeer::doSelectOne($c, $connexion);

        return $commonCalendrier;
    }

    /**
     * Recuperer les etapes d'un calendier.
     *
     * @param id Calendrier
     *
     * @return array etape
     */
    public function retrieveCalendrierEtapeByIdCal($idCalendrier)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTCalendrierEtapePeer::ID_CALENDRIER, $idCalendrier);
        $c->addAscendingOrderByColumn(CommonTCalendrierEtapePeer::POSITION);
        $ArraycommonEtape = CommonTCalendrierEtapePeer::doSelect($c, $connexion);

        return $ArraycommonEtape;
    }

    /**
     * Recuperer les etapes referentielles d'un type de procedure et d'un organisme.
     *
     * @param id type procedure, organisme
     *
     * @return Objet Etape referentiel
     */
    public function retrieveEtapeRefrentielOrg($idTypeProcedure, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTCalendrierEtapeReferentielPeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $c->add(CommonTCalendrierEtapeReferentielPeer::ORGANISME, $org);
        $ArraycommonEtape = CommonTCalendrierEtapeReferentielPeer::doSelect($c, $connexion);

        return $ArraycommonEtape;
    }

    /**
     * Recuperer un objet etape.
     *
     * @param id etape
     *
     * @return Objet Etape Referentiel
     */
    public function retrieveEtapeRefById($idEtape, $idTypeProcedure, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTCalendrierEtapeReferentielPeer::ID_ETAPE_CALENDRIER_REFERENTIEL, $idEtape);
        $c->add(CommonTCalendrierEtapeReferentielPeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $c->add(CommonTCalendrierEtapeReferentielPeer::ORGANISME, $org);
        $commonCalendrierEtape = CommonTCalendrierEtapeReferentielPeer::doSelectOne($c, $connexion);

        return $commonCalendrierEtape;
    }

    /**
     * Recuperer un objet etape.
     *
     * @param id etape
     *
     * @return Objet Etape
     */
    public function retrieveEtapeById($idEtape)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTCalendrierEtapePeer::ID_CALENDRIER_ETAPE, $idEtape);
        $commonCalendrier = CommonTCalendrierEtapePeer::doSelectOne($c, $connexion);

        return $commonCalendrier;
    }

    /**
     * Recuperer les transitions referentiel.
     *
     * @param id etape source , id etape cible
     *
     * @return array transition referentiel
     */
    public function retrieveTransationReferentielByIdEtapeSourceOrCible($Idcible, $Idsource, $typeProcedure, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($Idcible) {
            $c->add(CommonTCalendrierTransitionReferentielPeer::ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, $Idcible);
        }
        if ($Idsource) {
            $c->add(CommonTCalendrierTransitionReferentielPeer::ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, $Idsource);
        }
        $c->add(CommonTCalendrierTransitionReferentielPeer::ORGANISME, $org);
        $c->add(CommonTCalendrierTransitionReferentielPeer::ID_TYPE_PROCEDURE, $typeProcedure);
        $ArraycommonTransition = CommonTCalendrierTransitionReferentielPeer::doSelect($c, $connexion);

        return $ArraycommonTransition;
    }

    /**
     * Recuperer les transitions d'une etape.
     *
     * @param id etape source , id etape cible
     *
     * @return array transition
     */
    public function retrieveTransationByIdEtapeSourceOrCible($Idcible = false, $Idsource = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($Idcible) {
            $c->add(CommonTCalendrierTransitionPeer::ID_ETAPE_CIBLE, $Idcible);
        }
        if ($Idsource) {
            $c->add(CommonTCalendrierTransitionPeer::ID_ETAPE_SOURCE, $Idsource);
        }
        $ArraycommonTransition = CommonTCalendrierTransitionPeer::doSelect($c, $connexion);

        return $ArraycommonTransition;
    }

    /**
     * une etape est une initiale si il na pas de
     * transition.
     *
     * @param id etape , type Etape
     *
     * @return bool true transition existe , false sinon
     */
    public function isInitial($idEtape, $ref = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($ref) {
            $c->add(CommonTCalendrierTransitionReferentielPeer::ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, $idEtape);
            $ArraycommonEtape = CommonTCalendrierTransitionReferentielPeer::doSelect($c, $connexion);
        } else {
            $c->add(CommonTCalendrierTransitionPeer::ID_ETAPE_CIBLE, $idEtape);
            $ArraycommonEtape = CommonTCalendrierTransitionPeer::doSelect($c, $connexion);
        }
        if ($ArraycommonEtape) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Creer le calendrier avec ces etapes et transitions apartir du referentiel
     * correspondant a l'id procedure et l'organisme en parametre.
     *
     * @param id procedure , orgnisme , $reference consultation
     *
     * @return array etapes
     */
    public function createCalendrierFomCalendrierRef($idprocedure, $org, $consultationId)
    {
        try {
            $idCalender = self::createCalender($consultationId, $org);
            $arrayCoorespondance = [];
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c = new Criteria();
            $c->add(CommonTCalendrierEtapeReferentielPeer::ID_TYPE_PROCEDURE, $idprocedure);
            $c->add(CommonTCalendrierEtapeReferentielPeer::ORGANISME, $org);
            $arrayEtapesRef = CommonTCalendrierEtapeReferentielPeer::doSelect($c, $connexion);
            $arrayEtapes = [];

            if ($arrayEtapesRef) {
                foreach ($arrayEtapesRef as $etapeRef) {
                    $commonEtape = new CommonTCalendrierEtape();
                    $commonEtape->setIdCalendrier($idCalender);
                    $commonEtape->setCode($etapeRef->getCode());
                    $commonEtape->setLibelle($etapeRef->getLibelle());
                    $commonEtape->setlibre('0');
                    $commonEtape->setPosition($etapeRef->getPosition());
                    $commonEtape->setDateInitiale('');
                    $commonEtape->save($connexion);
                    $idEtape = $commonEtape->getIdCalendrierEtape();
                    $arrayEtapes[] = $commonEtape;
                    $arrayCoorespondance[$etapeRef->getIdEtapeCalendrierReferentiel()] = $idEtape;
                }
                // insertion pour chaque etapes les transitions conrrespondantes
                foreach ($arrayCoorespondance as $key => $value) {
                    self::insertTransitionEtape($key, $value, $arrayCoorespondance, $org, $idprocedure);
                }
            }

            return $arrayEtapes;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Insert les transitions d'un array des etapes.
     *
     * @param id Etape referentiel ,id_etape , array de correspondance
     */
    public function insertTransitionEtape($idEtapeRef, $idEtape, $arrayValue, $org, $idprocedure)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c = new Criteria();
            $c->add(CommonTCalendrierTransitionReferentielPeer::ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, $idEtapeRef);
            $c->add(CommonTCalendrierTransitionReferentielPeer::ORGANISME, $org);
            $c->add(CommonTCalendrierTransitionReferentielPeer::ID_TYPE_PROCEDURE, $idprocedure);
            $arrayTransitionRef = CommonTCalendrierTransitionReferentielPeer::doSelect($c, $connexion);
            foreach ($arrayTransitionRef as $transition) {
                $commonTransition = new CommonTCalendrierTransition();
                $commonTransition->setValeurFixe($transition->getValeurFixe());
                $commonTransition->setValeurVariableInitiale($transition->getValeurVariable());
                $commonTransition->setIdEtapeSource($arrayValue[$transition->getIdCalendrierEtapeReferentielSource()]);
                $commonTransition->setIdEtapeCible($idEtape);
                $commonTransition->save($connexion);
            }
        } catch (Exception $e) {
            $e->getMessage();
            exit;
        }
    }

    /**
     * Creer un calendrier pour une consultation.
     *
     * @param id Etape referentiel ,id_etape , array de correspondance
     * return id calendrier
     */
    public function createCalender($consultationId, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $commonCalender = new CommonTCalendrier();
        $commonCalender->setConsultationId($consultationId);
        $commonCalender->setOrganisme($org);
        $commonCalender->save($connexion);
        $idCalender = $commonCalender->getIdCalendrier();

        return $idCalender;
    }

    /***
     * Supprime les transactions et les etapes supprimées du calendrier.
     * @param id calendrier , $array etapes , array transitions
     *  @return
     */
    public function deleteEtapeAndTransition($idCalender, $idsEtapes, $connexion)
    {
        $c = new Criteria();
        $c->add(CommonTCalendrierEtapePeer::ID_CALENDRIER, $idCalender);
        $c->add(CommonTCalendrierEtapePeer::ID_CALENDRIER_ETAPE, $idsEtapes, Criteria::NOT_IN);
        $c->add(CommonTCalendrierEtapePeer::LIBRE, 1);
        $ArraycommonEtape = CommonTCalendrierEtapePeer::doSelect($c, $connexion);
        if ($ArraycommonEtape) {
            foreach ($ArraycommonEtape as $etape) {
                $etape->delete($connexion);
            }
        }
    }

    /***
     * Creation ou update des etapes et transitions a partir d'une chaine Json
     * @param $chaine Json
     * @return
     */
    public function saveJson($chaine)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $arrayId = [];
            $arrayIdTransitions = [];
            $arraySauvgarde = json_decode(Atexo_Util::toUtf8($chaine), true, 512, JSON_THROW_ON_ERROR);
            $idCalender = $arraySauvgarde['id'];
            $calendier = (new Atexo_GestionCalendrier())->retrieveCalendierById($idCalender);
            if ($calendier) {
                // retourne un tableau des etapes de ce calendrier
                $etapes = self::fillArrayEtape($idCalender);
                // on recupere les etapes du Json
                $arrayEtapes = $arraySauvgarde['etapes'];
                //on cherche si une etape json dans la BD
                foreach ($arrayEtapes as $etapeJson) {
                    $newEtape = false;
                    // recherche si etape existe  dans la Bd
                    if (array_key_exists($etapeJson['id'], $etapes)) {
                        $commonEtape = $etapes[$etapeJson['id']];
                    } else {
                        // creation d'une nouvelle etape
                        $commonEtape = new CommonTCalendrierEtape();
                        $newEtape = true;
                    }
                    $commonEtape->setIdCalendrier($idCalender);
                    $commonEtape->setCode($etapeJson['code']);
                    $commonEtape->setLibelle($etapeJson['libelle']);
                    $commonEtape->setPosition($etapeJson['position']);
                    if ('true' == $etapeJson['libre']) {
                        $commonEtape->setLibre('1');
                    } else {
                        $commonEtape->setLibre('0');
                    }
                    if ($etapeJson['datePrevue']) {
                        $commonEtape->setDatePrevue(Atexo_Util::frnDate2iso($etapeJson['datePrevue']));
                    }
                    if ($etapeJson['dateInitiale']) {
                        $commonEtape->setDateInitiale(Atexo_Util::frnDate2iso($etapeJson['dateInitiale']));
                    }
                    if ($etapeJson['dateReelleConfirmee']) {
                        $commonEtape->setDateReelleConfirmee('1');
                    }
                    $commonEtape->save($connexion);
                    $idEtape = $commonEtape->getIdCalendrierEtape();
                    // get transitions du json entrantes
                    $arrayTransitionsEtapes = $etapeJson['trEntrantes'];
                    foreach ($arrayTransitionsEtapes as $transition) {
                        $idSource = $transition['source']['id'];
                        if ($idSource) {
                            if ($newEtape) { // pour optimiser
                                $commonTransition = new CommonTCalendrierTransition();
                            } else {
                                $commonTransition = self::getTransitionByCibleSource($idSource, $idEtape);
                                if (!($commonTransition instanceof CommonTCalendrierTransition)) {
                                    $commonTransition = new CommonTCalendrierTransition();
                                }
                            }
                            //if($commonTransition){
                            if ($transition['valeurFixe']) {
                                $commonTransition->setValeurFixe($transition['valeurFixe']);
                            }
                            if ($transition['valeurVariable']) {
                                $commonTransition->setValeurVariableInitiale($transition['valeurVariable']);
                            }
                            $commonTransition->setIdEtapeSource($idSource);
                            $commonTransition->setIdEtapeCible($idEtape);
                            $commonTransition->save($connexion);
                            $arrayIdTransitions[] = $commonTransition->getIdCalendrierTransition();
                            //}
                            unset($commonTransition);
                        }
                    }
                    $arrayId[] = $idEtape;
                    unset($commonEtape);
                }
                // supprimer les etapes  libre non resignees dans le json kch revoir pour test
                (new Atexo_GestionCalendrier())->deleteEtapeAndTransition($idCalender, $arrayId, $connexion);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    /****
     * Fonction pour adaptation des données  return array des etapes key value
     * @param id calender
     * @ return array des etapes
     */
    public function fillArrayEtape($idCalender)
    {
        $arrayEtape = [];
        if ($idCalender) {
            $etapes = (new Atexo_GestionCalendrier())->retrieveCalendrierEtapeByIdCal($idCalender);
            foreach ($etapes as $etape) {
                $arrayEtape[$etape->getIdCalendrierEtape()] = $etape;
            }
        }

        return $arrayEtape;
    }

    /****
     * Fonction pour adaptation des données des transitions
     * @param id etape source , id etape cible
     * @ return array des transitions
     */
    public function fillArrayTransition($idEtapeSource, $idEtapeCible)
    {
        $arratTransitions = [];
        $transitions = (new Atexo_GestionCalendrier())->retrieveTransationByIdEtapeSourceOrCible($idEtapeCible, $idEtapeSource);
        foreach ($transitions as $transition) {
            $arratTransitions[$transition->getIdCalendrierTransition()] = $transition;
        }

        return $arratTransitions;
    }

    /****
     * Recuperer les transitions
     * @param id etape source , id etape cible
     * @ return  Objet transition
     */
    // a revoir cette fonction pour le probleme de select one ou lieu de listes d'objet kch
    public function getTransitionByCibleSource($idEtapeSource, $idEtapeCible)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($idEtapeCible) {
            $c->add(CommonTCalendrierTransitionPeer::ID_ETAPE_CIBLE, $idEtapeCible);
        }
        if ($idEtapeSource) {
            $c->add(CommonTCalendrierTransitionPeer::ID_ETAPE_SOURCE, $idEtapeSource);
        }
        $transition = CommonTCalendrierTransitionPeer::doSelectOne($c, $connexion);

        return $transition;
    }

    /****
     * Recuperer les donnees d'un calendrier
     * @param id calendrier
     * @ return  array donne pour Pdf calendrier
     */
    public function getEtapesAndTransitionByIdcalendrier($idcalendrier)
    {
        $arratResult = [];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTCalendrierEtapePeer::ID_CALENDRIER, $idcalendrier);
        $c->addAscendingOrderByColumn(CommonTCalendrierEtapePeer::POSITION);
        $arrayEtapes = CommonTCalendrierEtapePeer::doSelect($c, $connexion);
        $i = 0;
        foreach ($arrayEtapes as $etape) {
            $arratResult[$i]['ref'] = $etape->getCode();
            $arratResult[$i]['libelle'] = $etape->getLibelle();
            $dateEcheance = '';
            //Date  initiale
            if ($etape->getDateInitiale()) {
                $dateEcheance = Atexo_Util::frnDate2iso($etape->getDateInitiale());
            }
            $arratResult[$i]['echeance'] = $dateEcheance;
            // calendrier reel : par defaut date initial est egale à la date prevue
            $dateReelle = '';
            if ($etape->getDatePrevue()) {
                $dateReelle = Atexo_Util::frnDate2iso($etape->getDatePrevue());
            }
            $arratResult[$i]['dateReel'] = $dateReelle;
            $transition = self::getTransitionByCibleSource(false, $etape->getIdCalendrierEtape());
            if ($transition) {
                $etapeSource = self::retrieveEtapeById($transition->getIdEtapeSource());
                if ($etapeSource) {
                    $libelle = $etapeSource->getCode();
                }
                $valeurFixe = $transition->getValeurFixe();
                if (Atexo_Util::isPositifInteger($valeurFixe)) {
                    $valeurFixe = ' + '.abs($transition->getValeurFixe());
                } else {
                    $valeurFixe = ' - '.abs($transition->getValeurFixe());
                }
                $valeurVariable = $transition->getValeurVariableInitiale();
                if (Atexo_Util::isPositifInteger($valeurVariable)) {
                    $valeurVariable = ' + '.abs($valeurVariable);
                } else {
                    $valeurVariable = ' - '.abs($valeurVariable);
                }
                $arratResult[$i]['duree'] = $libelle.$valeurFixe.$valeurVariable;
            }
            ++$i;
        }

        return $arratResult;
    }

    /****
     * Fonction
     * @param
     * @ return array des etapes
     */
    public function fillArrayEtapeRefentiel($idTypeProcedure, $org)
    {
        $arrayEtape = [];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTCalendrierEtapeReferentielPeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $c->add(CommonTCalendrierEtapeReferentielPeer::ORGANISME, $org);
        $arrayEtapesRef = CommonTCalendrierEtapeReferentielPeer::doSelect($c, $connexion);
        foreach ($arrayEtapesRef as $etape) {
            $arrayEtape[$etape->getIdEtapeCalendrierReferentiel()] = $etape;
        }

        return $arrayEtape;
    }

    /***
     * update des transitions referentiels a partir d'une chaine Json
     * @param $chaine Json
     * @return
     */
    public function saveJsonReferentiel($chaine, $idTypProcedure, $org, $encode = false)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $arraySauvgarde = json_decode(Atexo_Util::toUtf8($chaine), true, 512, JSON_THROW_ON_ERROR);
            // on recupere les etapes du Json
            $arrayEtapes = $arraySauvgarde['etapes'];
            if (is_array($arrayEtapes)) {
                foreach ($arrayEtapes as $etapeJson) {
                    $arrayTransitionsEtapes = $etapeJson['trEntrantes'];
                    foreach ($arrayTransitionsEtapes as $transition) {
                        $idSource = $transition['source']['id'];
                        $idCible = $transition['cible']['id'];
                        if ($idSource && $idCible) {
                            $commonTransition = self::retrieveTransationReferentielByIdTypeProcedureOrg($idSource, $idCible, $idTypProcedure, $org);
                            if ($commonTransition instanceof CommonTCalendrierTransitionReferentiel) {
                                if ($transition['valeurFixe']) {
                                    $commonTransition->setValeurFixe($transition['valeurFixe']);
                                }
                                if ($transition['valeurVariable']) {
                                    $commonTransition->setValeurVariable($transition['valeurVariable']);
                                }
                                $commonTransition->save($connexion);
                                unset($commonTransition);
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }
    }

    /***
     * Recuperer une transition referentiels
     * @param id , id_type_procedure , organisme
     * @ return  un objet transition
     */
    public function retrieveTransationReferentielByIdTypeProcedureOrg($idSource, $idCible, $typeProcedure, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTCalendrierTransitionReferentielPeer::ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE, $idSource);
        $c->add(CommonTCalendrierTransitionReferentielPeer::ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE, $idCible);
        $c->add(CommonTCalendrierTransitionReferentielPeer::ORGANISME, $org);
        $c->add(CommonTCalendrierTransitionReferentielPeer::ID_TYPE_PROCEDURE, $typeProcedure);
        $transition = CommonTCalendrierTransitionReferentielPeer::doSelectOne($c, $connexion);

        return $transition;
    }

    // supprime les etapes referentiels d'un type de procedure et d'un organisme
    public function deleteEtapeReferentiel($idTypeProcedure, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTCalendrierEtapeReferentielPeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $c->add(CommonTCalendrierEtapeReferentielPeer::ORGANISME, $organisme);
        CommonTCalendrierEtapeReferentielPeer::doDelete($c, $connexionCom);
    }

    //supprime les transitions referentiels d'un type de procedure et d'un organisme
    public function deleteTransitionReferentiel($idTypeProcedure, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTCalendrierTransitionReferentielPeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $c->add(CommonTCalendrierTransitionReferentielPeer::ORGANISME, $organisme);
        CommonTCalendrierTransitionReferentielPeer::doDelete($c, $connexionCom);
    }
}
