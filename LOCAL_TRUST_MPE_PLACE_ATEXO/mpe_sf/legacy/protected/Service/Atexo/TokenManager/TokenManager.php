<?php

namespace Application\Service\Atexo\TokenManager;

use Application\Service\Atexo\Atexo_Config;
use Exception;

/**
 * Created by PhpStorm.
 * User: Aso
 * Date: 20/12/2016
 * Time: 10:18.
 */

/**
 * Mohamed BLAL | 20/12/2016.
 */
class Atexo_TokenManager_TokenManager
{
    /**
     * @param $token
     *
     * @return mixed
     *
     * @throws Exception
     * @throws SecurityException
     */
    public function getAccessToken($token)
    {
        $curlWrapper = new Atexo_TokenManager_CurlWrapper();
        $curlWrapper->addHeader('Authorization', "Bearer $token");
        $tokenUrl = Atexo_Config::getParameter('URL_FRANCECONNECT_CONCENTRATEUR_TOKEN_GET').'/'.$token;
        $result = $curlWrapper->get($tokenUrl);
        if (200 != $curlWrapper->getHTTPCode()) {
            if (!$result) {
                $messageErreur = $curlWrapper->getLastError();
            } else {
                $result_array = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
                $messageErreur = $result_array['error'];
            }
            throw new \Exception('Erreur lors de la récupération du jeton de connexion sur le concentrateur FranceConnect : '.$messageErreur);
        }
        $result = json_decode($result, null, 512, JSON_THROW_ON_ERROR);
        $result = json_decode($result, null, 512, JSON_THROW_ON_ERROR);

        return json_decode($result, null, 512, JSON_THROW_ON_ERROR);
    }

    public function deleteAccessToken($token)
    {
        $curlWrapper = new Atexo_TokenManager_CurlWrapper();
        $post_data = [
            'token' => $token,
            'client_id' => Atexo_Config::getParameter('client_id'),
            'client_secret' => Atexo_Config::getParameter('client_secret'),
        ];
        $curlWrapper->setPostDataUrlEncode($post_data);
        $token_url = Atexo_Config::getParameter('URL_FRANCECONNECT_CONCENTRATEUR_TOKEN_DELETE');
        $result = $curlWrapper->get($token_url);

        // check status code
        if (200 != $curlWrapper->getHTTPCode()) {
            if (!$result) {
                throw new \Exception($curlWrapper->getLastError());
            }
            $result_array = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
            $description = array_key_exists(
                'error_description',
                $result_array
            ) ? $result_array['error_description'] : '';

            throw new Exception('FranceConnect concentrator Error => '.$result_array['error']);
        }

        return 0;
    }
}
