<?php

namespace Application\Service\Atexo\TokenManager\Authentication;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;

/**
 * Created by PhpStorm.
 * User: Aso
 * Date: 22/12/2016
 * Time: 17:18.
 */

/**
 * Mohamed BLAL | 22/12/2016.
 */
class Atexo_TokenManager_Authentication_PivotController
{
    /**
     * @param $token
     * @param null $inscrit
     *
     * @return mixed
     */
    protected function pivotIdentityControl($token, $inscrit = null)
    {
        $siren = substr($token->siret, 0, 10);
        $url1 = Atexo_Config::getParameter('PF_URL_REFERENCE').'?page=Entreprise.FormulaireCompteEntreprise&action=creation&info=MSG&';
        $params = (array) $token;
        unset($params['sub']);
        unset($params['siret']);
        $params['siren'] = trim($siren);
        $url1 .= http_build_query($params);
        $url2 = Atexo_Config::getParameter('PF_URL_REFERENCE').'?page=Entreprise.EntrepriseHome&info=MSG';

        if (empty($_GET['goto'])) {
            $url1 .= '&goto='.'?page=Entreprise.EntrepriseAccueilAuthentifie'; //urlencode(Atexo_Util::atexoHtmlEntities($_GET["goto"]));
            $url2 .= '&goto='.'?page=Entreprise.EntrepriseAccueilAuthentifie'; //urlencode(Atexo_Util::atexoHtmlEntities($_GET["goto"]));
        }
        //Cas-1 : Si Email non reconnu.
        if (empty($inscrit)) {
            // redirection vers la page page inscription
            $this->response->redirect($url1);
        }
        //Cas-2 : Si Email existe, mais siret inscrit ne correspond pas à la siret token de l'identite pivot
        if (!empty($inscrit)) {
            $fullSiret = $inscrit->getEntreprise()->getSiren().$inscrit->getCommonTEtablissement()->getCodeEtablissement();
            if ($token->siret != $fullSiret) {
                $this->response->redirect($url2);
            } elseif ($token->siret == $fullSiret) {
                // ouvrir la session
                return true;
            }
        }
    }
}
