<?php

namespace Application\Service\Atexo;

/**
 * Vo pour définir les critères de vérification de l'utilisateur.
 *
 * @author ZAKI Anas <zaki.anas@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 * @TODO commentaires à compléter
 */
class Atexo_UserVo
{
    private $login;
    private $password;
    private $type;
    private $session;
    private $certificat;
    private $sso;
    private bool $authenticatePpp = false;
    private $email;
    private $siret;
    private int|bool $authenticatedFC = 0;

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($value)
    {
        $this->password = $value;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getSession()
    {
        return $this->session;
    }

    public function setSession($session)
    {
        $this->session = $session;
    }

    public function getCertificat()
    {
        return $this->certificat;
    }

    public function setCertificat($certificat)
    {
        $this->certificat = $certificat;
    }

    public function getSso()
    {
        return $this->sso;
    }

    public function setSso($sso)
    {
        $this->sso = $sso;
    }

    public function getAuthenticatePpp()
    {
        return $this->authenticatePpp;
    }

    public function setAuthenticatePpp($value)
    {
        $this->authenticatePpp = $value;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param mixed $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
    }

    /**
     * @return bool
     */
    public function isAuthenticatedFC()
    {
        return $this->authenticatedFC;
    }

    /**
     * @param bool $authenticatedFC
     */
    public function setAuthenticatedFC($authenticatedFC)
    {
        $this->authenticatedFC = $authenticatedFC;
    }
}
