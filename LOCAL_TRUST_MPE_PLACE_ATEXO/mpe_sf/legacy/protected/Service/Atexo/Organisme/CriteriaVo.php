<?php

namespace Application\Service\Atexo\Organisme;

use Application\Service\Atexo\Agent\Atexo_Agent_Access;

/**
 * Vo pour définir les critères de Atexo_Agent_Access::search().
 *
 * @author loubna EZZIANI loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 * @TODO commentaires à compléter
 */
class Atexo_Organisme_CriteriaVo
{
    private $acronyme;
    private $denomination;
    private $offset;
    private $limit;
    private $identifiant;
    private string $sortByElement = '';
    private string $sensOrderBy = 'DESC';
    private $active;

    public function getAcronyme()
    {
        return $this->acronyme;
    }

    public function setAcronyme($acronyme)
    {
        $this->acronyme = $acronyme;
    }

    public function getDenomination()
    {
        return $this->denomination;
    }

    public function setDenomination($denomination)
    {
        $this->denomination = $denomination;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function getIdentifiant()
    {
        return $this->identifiant;
    }

    public function setIdentifiant($identifiant)
    {
        $this->identifiant = $identifiant;
    }

    public function getSortByElement()
    {
        return $this->sortByElement;
    }

    public function setSortByElement($sortByElement)
    {
        $this->sortByElement = $sortByElement;
    }

    public function getSensOrderBy()
    {
        return $this->sensOrderBy;
    }

    public function setSensOrderBy($sensOrderBy)
    {
        $this->sensOrderBy = $sensOrderBy;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function setActive($active)
    {
        $this->active = $active;
    }
}
