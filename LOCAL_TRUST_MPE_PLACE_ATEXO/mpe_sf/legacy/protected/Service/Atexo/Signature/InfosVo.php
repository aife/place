<?php

namespace Application\Service\Atexo\Signature;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use AtexoCrypto\Dto\Signature;
use Prado\Prado;

/**
 * Class Atexo_Signature_InfosVo.
 */
class Atexo_Signature_InfosVo
{
    private $signatureValide;
    private $absenceRevocationCRL = null;
    private $dateValiditeDu;
    private $dateValiditeAu;
    private $chaineDeCertificationValide = null;
    private $periodiciteValide;
    private $class;
    private $signataire;
    private $signataireComplet;
    private $emetteur;
    private $repertoiresChaineCertification;
    private $emailSignataire;
    private $alternativeNameSignataire;
    private $cnSignataire;
    private $ouSignataire;
    private $oSignataire;
    private $cSignataire;
    private $cnEmetteur;
    private $ouEmetteur;
    private $oEmetteur;
    private $cEmetteur;
    private $alternativeNameEmetteur;
    private $qualifieEIDAS;
    private $formatSignature;
    private $dateIndicative;
    private $dateHorodatage;
    private $jetonHorodatage;

    /**
     * Atexo_Signature_InfosVo constructor.
     *
     * @param array|null $data
     */
    public function __construct($data = null)
    {
        if ($data && is_array($data)) {
            foreach ($data as $key => $value) {
                if ($value || false === $value) {
                    $setters = 'set'.ucfirst($key);

                    if (method_exists($this, $setters)) {
                        $this->$setters($value);
                    }
                }
            }
        }
    }

    /**
     * @param $objetInfo
     */
    public function setInfoFromObject($objetInfo)
    {
        foreach ($objetInfo as $key => $value) {
            $key = str_replace("\000", '', $key);
            $key = trim($key, ' *');
            $this->$key = $value;
        }
    }

    /**
     * @param $signatureValide
     */
    public function setSignatureValide($signatureValide)
    {
        $this->signatureValide = $signatureValide;
    }

    /**
     * @return mixed
     */
    public function getSignatureValide()
    {
        return $this->signatureValide;
    }

    /**
     * @param $absenceRevocationCRL
     */
    public function setAbsenceRevocationCRL($absenceRevocationCRL)
    {
        $this->absenceRevocationCRL = $absenceRevocationCRL;
    }

    /**
     * @return mixed
     */
    public function getAbsenceRevocationCRL()
    {
        return $this->absenceRevocationCRL;
    }

    /**
     * @param $dateValiditeDu
     */
    public function setDateValiditeDu($dateValiditeDu)
    {
        $this->dateValiditeDu = $dateValiditeDu;
    }

    /**
     * @return mixed
     */
    public function getDateValiditeDu()
    {
        return $this->dateValiditeDu;
    }

    /**
     * @param $dateValiditeAu
     */
    public function setDateValiditeAu($dateValiditeAu)
    {
        $this->dateValiditeAu = $dateValiditeAu;
    }

    /**
     * @return mixed
     */
    public function getDateValiditeAu()
    {
        return $this->dateValiditeAu;
    }

    /**
     * @param $chaineDeCertificationValide
     */
    public function setChaineDeCertificationValide($chaineDeCertificationValide)
    {
        $this->chaineDeCertificationValide = $chaineDeCertificationValide;
    }

    /**
     * @return mixed
     */
    public function getChaineDeCertificationValide()
    {
        return $this->chaineDeCertificationValide;
    }

    /**
     * @param $periodiciteValide
     */
    public function setPeriodiciteValide($periodiciteValide)
    {
        $this->periodiciteValide = $periodiciteValide;
    }

    /**
     * @return mixed
     */
    public function getPeriodiciteValide()
    {
        return $this->periodiciteValide;
    }

    /**
     * @param $class
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * @return mixed
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param $signataire
     */
    public function setSignataire($signataire)
    {
        $this->signataire = $signataire;
    }

    /**
     * @return mixed
     */
    public function getSignataire()
    {
        return $this->getSignataireComplet();
    }

    /**
     * @param $signataire
     */
    public function setSignataireComplet($signataire)
    {
        $this->signataireComplet = $signataire;
    }

    /**
     * @return mixed
     */
    public function getSignataireComplet()
    {
        return $this->signataireComplet;
    }

    /**
     * @param $emetteur
     */
    public function setEmetteur($emetteur)
    {
        $this->emetteur = $emetteur;
    }

    /**
     * @return mixed
     */
    public function getEmetteur()
    {
        return $this->emetteur;
    }

    /**
     * @param $repertoiresChaineCertification
     */
    public function setRepertoiresChaineCertification($repertoiresChaineCertification)
    {
        $this->repertoiresChaineCertification = $repertoiresChaineCertification;
    }

    /**
     * @return mixed
     */
    public function getRepertoiresChaineCertification()
    {
        return $this->repertoiresChaineCertification;
    }

    /**
     * @param $value
     */
    public function setEmailSignataire($value)
    {
        $this->emailSignataire = $value;
    }

    /**
     * @return mixed
     */
    public function getEmailSignataire()
    {
        return $this->emailSignataire;
    }

    /**
     * @param $value
     */
    public function setCnSignataire($value)
    {
        $this->cnSignataire = $value;
    }

    /**
     * @return mixed
     */
    public function getCnSignataire()
    {
        return $this->cnSignataire;
    }

    /**
     * @param $value
     */
    public function setOuSignataire($value)
    {
        $this->ouSignataire = $value;
    }

    /**
     * @return mixed
     */
    public function getOuSignataire()
    {
        return $this->ouSignataire;
    }

    /**
     * @param $value
     */
    public function setOSignataire($value)
    {
        $this->oSignataire = $value;
    }

    /**
     * @return mixed
     */
    public function getOSignataire()
    {
        return $this->oSignataire;
    }

    /**
     * @param $value
     */
    public function setCSignataire($value)
    {
        $this->cSignataire = $value;
    }

    /**
     * @return mixed
     */
    public function getCSignataire()
    {
        return $this->cSignataire;
    }

    /**
     * @param $value
     */
    public function setCnEmetteur($value)
    {
        $this->cnEmetteur = $value;
    }

    /**
     * @return mixed
     */
    public function getCnEmetteur()
    {
        return $this->cnEmetteur;
    }

    /**
     * @param $value
     */
    public function setOuEmetteur($value)
    {
        $this->ouEmetteur = $value;
    }

    /**
     * @return mixed
     */
    public function getOuEmetteur()
    {
        return $this->ouEmetteur;
    }

    /**
     * @param $value
     */
    public function setOEmetteur($value)
    {
        $this->oEmetteur = $value;
    }

    /**
     * @return mixed
     */
    public function getOEmetteur()
    {
        return $this->oEmetteur;
    }

    /**
     * @param $value
     */
    public function setCEmetteur($value)
    {
        $this->cEmetteur = $value;
    }

    /**
     * @return mixed
     */
    public function getCEmetteur()
    {
        return $this->cEmetteur;
    }

    /**
     * @return mixed
     */
    public function getAlternativeNameSignataire()
    {
        return $this->alternativeNameSignataire;
    }

    /**
     * @param mixed $alternativeNameSignataire
     */
    public function setAlternativeNameSignataire($alternativeNameSignataire)
    {
        $this->alternativeNameSignataire = $alternativeNameSignataire;
    }

    /**
     * @return mixed
     */
    public function getAlternativeNameEmetteur()
    {
        return $this->alternativeNameEmetteur;
    }

    /**
     * @param mixed $alternativeNameEmetteur
     */
    public function setAlternativeNameEmetteur($alternativeNameEmetteur)
    {
        $this->alternativeNameEmetteur = $alternativeNameEmetteur;
    }

    /**
     * @param $chaine
     */
    public function formaterChaine($chaine): bool|string
    {
        $arraySignataire = explode(',', $chaine);

        if (is_array($arraySignataire) && count($arraySignataire)) {
            $chaine = (new Atexo_Config())->toPfEncoding(implode('<br/>', $arraySignataire));
        }

        return $chaine;
    }

    public function getSignataireFormat(): bool|string
    {
        return $this->getSignataireCompletFormat();
    }

    public function getSignataireCompletFormat(): bool|string
    {
        return self::formaterChaine($this->signataireComplet);
    }

    public function getEmetteurFormat(): bool|string
    {
        return self::formaterChaine($this->emetteur);
    }

    /**
     * @return mixed
     */
    public function getDateValideFrom()
    {
        $date = Atexo_Util::iso2frnDate(substr($this->dateValiditeDu, 0, 10));
        $date .= ' '.substr($this->dateValiditeDu, 11, 8);

        return $date;
    }

    /**
     * @return mixed
     */
    public function getDateValideTo()
    {
        $date = Atexo_Util::iso2frnDate(substr($this->dateValiditeAu, 0, 10));
        $date .= ' '.substr($this->dateValiditeAu, 11, 8);

        return $date;
    }

    /**
     * Permet de retourner les informations du signataire sans les suites de texte illisible.
     */
    public function getSignatairePourVerification(): bool|string
    {
        $infoSignataire = explode(',', $this->signataireComplet);
        $signataire = '';

        foreach ($infoSignataire as $info) {
            if (preg_match('/^[A-Z]{1,2}=.+/', $info)) {
                $signataire .= $info.',';
            }
        }

        return self::formaterChaine(trim($signataire, ','));
    }

    /**
     * @return mixed
     */
    public function getQualifieEIDAS()
    {
        return $this->qualifieEIDAS;
    }

    /**
     * @param mixed $qualifieEIDAS
     */
    public function setQualifieEIDAS($qualifieEIDAS)
    {
        $this->qualifieEIDAS = $qualifieEIDAS;
    }

    /**
     * @return mixed
     */
    public function getFormatSignature()
    {
        return $this->formatSignature ?: '';
    }

    /**
     * @param mixed $formatSignature
     */
    public function setFormatSignature($formatSignature)
    {
        $this->formatSignature = $formatSignature;
    }

    /**
     * @return mixed
     */
    public function getDateIndicative()
    {
        return $this->dateIndicative ? Atexo_Util::getDateTimeByFormat($this->dateIndicative, 'd/m/Y H:i:s') : '';
    }

    /**
     * @param mixed $dateIndicative
     */
    public function setDateIndicative($dateIndicative)
    {
        $this->dateIndicative = $dateIndicative;
    }

    /**
     * @return mixed
     */
    public function getDateHorodatage()
    {
        return $this->dateHorodatage ?: '';
    }

    /**
     * @param mixed $dateHorodatage
     */
    public function setDateHorodatage($dateHorodatage)
    {
        $this->dateHorodatage = $dateHorodatage;
    }

    /**
     * @return mixed
     */
    public function getJetonHorodatage()
    {
        return $this->jetonHorodatage;
    }

    /**
     * @param mixed $jetonHorodatage
     */
    public function setJetonHorodatage($jetonHorodatage)
    {
        $this->jetonHorodatage = $jetonHorodatage;
    }

    public function getInfoHorodatageSignature()
    {
        if (1 == $this->jetonHorodatage) {
            $infoHorodatage = Prado::localize('JETON_SIGNATURE_HORODATE_OUI').(!empty($this->dateHorodatage) ? ' ('.Atexo_Util::getDateTimeByFormat($this->dateHorodatage, 'd/m/Y H:i:s').')' : '');
        } else {
            $infoHorodatage = Prado::localize('JETON_SIGNATURE_HORODATE_NON');
        }

        return $infoHorodatage;
    }

    public function getInfoCertificatSignature()
    {
        if (1 == $this->qualifieEIDAS) {
            $infoCertificatSignature = Prado::localize('CERTIFICAT_SIGNATURE_QUALIFIE');
        } else {
            $infoCertificatSignature = Prado::localize('CERTIFICAT_SIGNATURE_NON_QUALIFIE');
        }

        return $infoCertificatSignature;
    }

    public function getInfoBulleCertificatSignature()
    {
        if (1 == $this->qualifieEIDAS) {
            $infoBulleCertificatSignature = Prado::localize('CERTIFICAT_SIGNATURE_QUALIFIE_INFO_BULLE');
        } else {
            $infoBulleCertificatSignature = Prado::localize('CERTIFICAT_SIGNATURE_NON_QUALIFIE_INFO_BULLE');
        }

        return $infoBulleCertificatSignature;
    }

    public function hasInformationsComplementaires()
    {
        return null != $this->qualifieEIDAS
            || null != $this->formatSignature
            || null != $this->dateIndicative
            || null != $this->dateHorodatage
            || null != $this->jetonHorodatage;
    }
}
