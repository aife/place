<?php

namespace Application\Service\Atexo\Signature;

use Application\Propel\Mpe\CommonTReferentielCertificat;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ReferentielCertificat;
use AtexoCrypto\Dto\Signature;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author Cesar BOULET <cesar.boulet@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Signature_FichierVo
{
    const FICHIER = 0;
    const HASH = 1;
    private $numeroLot;
    private $contenuSignatureXML;
    private $index;
    private $typeEnveloppe;
    private $hashFichier;
    private $infosSignature;
    private $class;
    private $signatureVerifiee;
    private $cheminSignatureXML;
    private $cheminFichier;
    private $identifiant;
    private $typeFichier;
    private $nomFichier;
    private $orgineFichier;
    private $idFichier;
    private bool $fichierAeAjoute = false;
    private $cheminFichierXmlAE; //Contient le chemin du fichier de l'acte d'engagement
    private $nomFichierXmlAE; //Contient le nom du fichier de l'acte d'engagement
    private $cheminFichierSignatureXmlAE; //Contient la signature de l'acte d'engagement
    private $tailleFichier;

    public function __construct($data = null)
    {
        if ($data && is_array($data)) {
            foreach ($data as $key => $value) {
                if ('' !== $value && null !== $value) {
                    $setters = 'set'.ucfirst($key);
                    if (method_exists($this, $setters)) {
                        $this->$setters($value);
                    }
                }
            }
        }
    }

    public function setNumeroLot($numeroLot)
    {
        $this->numeroLot = $numeroLot;
    }

    public function getNumeroLot()
    {
        return $this->numeroLot;
    }

    public function setContenuSignatureXML($contenuSignatureXML)
    {
        $this->contenuSignatureXML = $contenuSignatureXML;
    }

    public function getContenuSignatureXML()
    {
        return $this->contenuSignatureXML;
    }

    public function setIndex($index)
    {
        $this->index = $index;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function setTypeEnveloppe($typeEnveloppe)
    {
        $this->typeEnveloppe = $typeEnveloppe;
    }

    public function getTypeEnveloppe()
    {
        return $this->typeEnveloppe;
    }

    public function setHashFichier($hashFichier)
    {
        $this->hashFichier = $hashFichier;
    }

    public function getHashFichier()
    {
        return $this->hashFichier;
    }

    public function setInfosSignature($infosSignature)
    {
        if (is_array($infosSignature)) {
            $this->infosSignature = new Atexo_Signature_InfosVo($infosSignature);
        } else {
            $this->infosSignature = $infosSignature;
        }
    }

    public function getInfosSignature()
    {
        return $this->infosSignature;
    }

    public function setClass($class)
    {
        $this->class = $class;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function setSignatureVerifiee($signatureVerifiee)
    {
        $this->signatureVerifiee = $signatureVerifiee;
    }

    public function getSignatureVerifiee()
    {
        return $this->signatureVerifiee;
    }

    public function setCheminSignatureXML($cheminSignatureXML)
    {
        $this->cheminSignatureXML = $cheminSignatureXML;
    }

    public function getCheminSignatureXML()
    {
        return $this->cheminSignatureXML;
    }

    public function setCheminFichier($cheminFichier)
    {
        $this->cheminFichier = $cheminFichier;
    }

    public function getCheminFichier()
    {
        return $this->cheminFichier;
    }

    public function setIdentifiant($identifiant)
    {
        $this->identifiant = $identifiant;
    }

    public function getIdentifiant()
    {
        return $this->identifiant;
    }

    public function setTypeFichier($typeFichier)
    {
        $this->typeFichier = $typeFichier;
    }

    public function getTypeFichier()
    {
        return $this->typeFichier;
    }

    public function setNomFichier($nomFichier)
    {
        $this->nomFichier = $nomFichier;
    }

    public function getNomFichier()
    {
        return $this->nomFichier;
    }

    public function setOrgineFichier($value)
    {
        $this->orgineFichier = $value;
    }

    public function getOrgineFichier()
    {
        return $this->orgineFichier;
    }

    public function setIdFichier($value)
    {
        $this->idFichier = $value;
    }

    public function getIdFichier()
    {
        return $this->idFichier;
    }

    public function setInfosFichiersAE($value)
    {
        $this->infosFichiersAE = $value;
    }

    public function getInfosFichiersAE()
    {
        return $this->infosFichiersAE;
    }

    public function setFichierAeAjoute($value)
    {
        $this->fichierAeAjoute = $value;
    }

    public function getFichierAeAjoute()
    {
        return $this->fichierAeAjoute;
    }

    public function setCheminFichierXmlAE($value)
    {
        $this->cheminFichierXmlAE = $value;
    }

    public function getCheminFichierXmlAE()
    {
        return $this->cheminFichierXmlAE;
    }

    public function setNomFichierXmlAE($value)
    {
        $this->nomFichierXmlAE = $value;
    }

    public function getNomFichierXmlAE()
    {
        return $this->nomFichierXmlAE;
    }

    public function setCheminFichierSignatureXmlAE($value)
    {
        $this->cheminFichierSignatureXmlAE = $value;
    }

    public function getCheminFichierSignatureXmlAE()
    {
        return $this->cheminFichierSignatureXmlAE;
    }

    public function getStatutPeriodeValiditeCert()
    {
        $periodeValidite = Atexo_Config::getParameter('UNKNOWN');
        $infoSignature = $this->getInfosSignature();
        if ($infoSignature instanceof Atexo_Signature_InfosVo) {
            if ($infoSignature->getPeriodiciteValide()) {
                $periodeValidite = Atexo_Config::getParameter('OK');
            } else {
                $periodeValidite = Atexo_Config::getParameter('NOK');
            }
        }

        return $periodeValidite;
    }

    public function getStatutRejeuSignature()
    {
        $statutRejeu = Atexo_Config::getParameter('UNKNOWN');
        $infoSignature = $this->getInfosSignature();
        if ($infoSignature instanceof Atexo_Signature_InfosVo) {
            if (true === $infoSignature->getSignatureValide()) {
                $statutRejeu = Atexo_Config::getParameter('OK');
            } elseif (false === $infoSignature->getSignatureValide()) {
                $statutRejeu = Atexo_Config::getParameter('NOK');
            }
        }

        return $statutRejeu;
    }

    public function getStatutRevocationCert()
    {
        $statutRevocation = Atexo_Config::getParameter('UNKNOWN');
        $infoSignature = $this->getInfosSignature();
        if ($infoSignature instanceof Atexo_Signature_InfosVo) {
            if (true === $infoSignature->getAbsenceRevocationCRL()) {
                $statutRevocation = Atexo_Config::getParameter('OK');
            } elseif (false === $infoSignature->getAbsenceRevocationCRL()) {
                $statutRevocation = Atexo_Config::getParameter('NOK');
            }
        }

        return $statutRevocation;
    }

    public function getStatutVerifChaineCert()
    {
        $statutchaineCert = Atexo_Config::getParameter('UNKNOWN');
        $infoSignature = $this->getInfosSignature();
        if ($infoSignature instanceof Atexo_Signature_InfosVo) {
            if (Atexo_Module::isEnabled('SurchargeReferentiels')) {
                $statutchaineCert = self::getStatutReferentielCertificat();
            } else {
                if (true === $infoSignature->getChaineDeCertificationValide()) {
                    $statutchaineCert = Atexo_Config::getParameter('OK');
                } elseif (false === $infoSignature->getChaineDeCertificationValide()) {
                    $statutchaineCert = Atexo_Config::getParameter('NOK');
                }
            }
        }

        return $statutchaineCert;
    }

    public function getNomReferentielCertificat($nomFoctionel = false)
    {
        $infoSignature = $this->getInfosSignature();
        $nomReferentiel = Prado::localize('DEFINE_NON_REFERENCE');
        if ($infoSignature instanceof Atexo_Signature_InfosVo) {
            $repertoiresChaineCertification = $infoSignature->getRepertoiresChaineCertification();
            if (is_array($repertoiresChaineCertification) && array_key_exists('nom', $repertoiresChaineCertification[0]) && $repertoiresChaineCertification[0]['nom']) {
                if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                    $nomReferentiel = $repertoiresChaineCertification[0]['nom'];
                } else {
                    $referentielCertif = (new Atexo_ReferentielCertificat())->getReferentielCertifByName($repertoiresChaineCertification[0]['nom']);
                    if ($referentielCertif instanceof CommonTReferentielCertificat) {
                        if ($nomFoctionel) {
                            $nomReferentiel = $referentielCertif->getNomReferentielFonctionnel();
                            $nomReferentiel .= ' ';
                            $nomReferentiel .= $referentielCertif->getMessage();
                        } else {
                            $nomReferentiel = $repertoiresChaineCertification[0]['nom'];
                        }
                    }
                }
            }
        }

        return $nomReferentiel;
    }

    public function getStatutReferentielCertificat()
    {
        $statut = null;
        $infoSignature = $this->getInfosSignature();
        if ($infoSignature instanceof Atexo_Signature_InfosVo) {
            if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                $repertoiresChaineCertification = $infoSignature->getRepertoiresChaineCertification();
                if (is_array($repertoiresChaineCertification) && array_key_exists('statut', $repertoiresChaineCertification[0]) && $repertoiresChaineCertification[0]['statut']) {
                    $statut = match ($repertoiresChaineCertification[0]['statut']) {
                        'OK' => Atexo_Config::getParameter('OK'),
                        'KO' => Atexo_Config::getParameter('NOK'),
                        default => Atexo_Config::getParameter('UNKNOWN'),
                    };
                }
            } else {
                $referentielName = self::getNomReferentielCertificat();
                $chaine = (new Atexo_ReferentielCertificat())->getStatutReferentielCertificat($referentielName);
                if (0 === $chaine) {
                    $statut = Atexo_Config::getParameter('OK');
                } elseif (2 == $chaine) {
                    $statut = Atexo_Config::getParameter('NOK');
                }
            }
        }

        return $statut;
    }

    public function isSignedFile()
    {
        if ((!$this->getContenuSignatureXML() && !$this->getCheminSignatureXML()) || !($this->getInfosSignature() instanceof Atexo_Signature_InfosVo)) {
            return false;
        } else {
            return true;
        }
    }

    public function verifierSignature()
    {
        if ($this->isSignedFile()) {
            if ($this->getStatutRejeuSignature() == Atexo_Config::getParameter('OK')
            && $this->getStatutPeriodeValiditeCert() == Atexo_Config::getParameter('OK')
            && $this->getStatutRevocationCert() == Atexo_Config::getParameter('OK')
            && $this->getStatutVerifChaineCert() == Atexo_Config::getParameter('OK')) {
                return Atexo_Config::getParameter('OK');
            } elseif ($this->getStatutRejeuSignature() == Atexo_Config::getParameter('NOK')
            || $this->getStatutPeriodeValiditeCert() == Atexo_Config::getParameter('NOK')
            || $this->getStatutRevocationCert() == Atexo_Config::getParameter('NOK')
            || $this->getStatutVerifChaineCert() == Atexo_Config::getParameter('NOK')) {
                return Atexo_Config::getParameter('NOK');
            }
        } else {
            return Atexo_Config::getParameter('UNKNOWN');
        }
    }

    public function getResultatValiditeSignatureWithMessage()
    {
        $resultat = [];
        $message = '';
        $resUnkown = false;
        $resNok = false;
        if ($this->isSignedFile()) {
            if ($this->getStatutVerifChaineCert() == Atexo_Config::getParameter('NOK')) {
                $message .= '<li>'.Prado::localize('DEFINE_REFERENTIEL_CERTIFICAT_NOK').'</li>';
                $resNok = true;
            } elseif ($this->getStatutVerifChaineCert() == Atexo_Config::getParameter('UNKNOWN')) {
                $message .= '<li>'.Prado::localize('DEFINE_REFERENTIEL_CERTIFICAT_UNKNOWN').'</li>';
                $resUnkown = true;
            }

            if ($this->getStatutRevocationCert() == Atexo_Config::getParameter('NOK')) {
                $message .= '<li>'.Prado::localize('DEFINE_NON_REVOCATION_NOK').'</li>';
                $resNok = true;
            } elseif ($this->getStatutRevocationCert() == Atexo_Config::getParameter('UNKNOWN')) {
                $message .= '<li>'.Prado::localize('DEFINE_LISTE_NON_REVOCATION_AUTORITE').'</li>';
                $resUnkown = true;
            }

            if ($this->getStatutPeriodeValiditeCert() == Atexo_Config::getParameter('NOK')) {
                $message .= '<li>'.Prado::localize('DEFINE_PERIODE_VALIDITE_NOK').'</li>';
                $resNok = true;
            }

            if ($this->getStatutRejeuSignature() == Atexo_Config::getParameter('NOK')) {
                $message .= '<li>'.Prado::localize('DEFINE_NON_REPUDIATION_NOK').'</li>';
                $resNok = true;
            } elseif ($this->getStatutRejeuSignature() == Atexo_Config::getParameter('UNKNOWN')) {
                $message .= '<li>'.Prado::localize('DEFINE_NON_REPUDIATION_UNKNOWN').'</li>';
                $resUnkown = true;
            }

            if ($resNok) {
                $resultat['statut'] = Atexo_Config::getParameter('NOK');
            } elseif ($resUnkown) {
                $resultat['statut'] = Atexo_Config::getParameter('UNKNOWN');
            } else {
                $resultat['statut'] = Atexo_Config::getParameter('OK');
            }
        } else {
            $message = '<li>'.Prado::localize('DEFINE_AUCUNE_SIGNATURE_ENVOYE').' </li>';
            $resultat['statut'] = Atexo_Config::getParameter('SANS');
        }
        $resultat['message'] = $message;

        return $resultat;
    }

    /**
     * @return mixed
     */
    public function getTailleFichier()
    {
        return $this->tailleFichier;
    }

    /**
     * @param mixed $tailleFichier
     */
    public function setTailleFichier($tailleFichier)
    {
        $this->tailleFichier = $tailleFichier;
    }
}
