<?php

namespace Application\Service\Atexo\Signature;

use AtexoCrypto\Dto\InfosSignature;
use AtexoCrypto\Dto\Signature;

/**
 * commentaires.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Signature_Gestion
{
    public function transformeJson($json)
    {
        //$json = '{"fichiers":[{"numeroLot":1,"index":0,"typeEnveloppe":1,"infosSignature":{"signatureValide":true,"absenceRevocationCRL":true,"dateValiditeDu":"2013-07-02T17:10:14+02:00","dateValiditeAu":"2013-07-02T17:10:14+02:00","chaineDeCertificationValide":true,"periodiciteValide":true,"class":"class fr.atexo.signature.applet.mpe.reponse.InfosSignature","signataire":"Mr Dupont George","emetteur":"AC emmetrice"},"class":"class fr.atexo.signature.applet.mpe.reponse.ReponseSignatureFichier","signatureVerifiee":true,"cheminSignatureXML":"chemin/version/fichierSingature1.xml","cheminFichier":"chemin/version/fichier1.pdf","identifiant":"identifiant_fichier_1"},{"numeroLot":2,"index":1,"typeEnveloppe":1,"infosSignature":{"signatureValide":true,"absenceRevocationCRL":true,"dateValiditeDu":"2013-07-02T17:10:14+02:00","dateValiditeAu":"2013-07-02T17:10:14+02:00","chaineDeCertificationValide":true,"periodiciteValide":true,"class":"class fr.atexo.signature.applet.mpe.reponse.InfosSignature","signataire":"Mr Dupont George","emetteur":"AC emmetrice"},"class":"class fr.atexo.signature.applet.mpe.reponse.ReponseSignatureFichier","signatureVerifiee":true,"cheminSignatureXML":"chemin/version/fichierSingature2.xml","cheminFichier":"chemin/version/fichier2.pdf","identifiant":"identifiant_fichier_2"}],"hashs":[{"numeroLot":1,"contenuSignatureXML":"QjhGN0IyQ0I3ODBBMTBDQzFCMEQ0RDcyQ0FCQThDQ0ExNEU4MDhBQw==","index":0,"typeEnveloppe":1,"hashFichier":"B8F7B2CB780A10CC1B0D4D72CABA8CCA14E808AC","infosSignature":{"signatureValide":true,"absenceRevocationCRL":true,"dateValiditeDu":"2013-07-02T17:10:14+02:00","dateValiditeAu":"2013-07-02T17:10:14+02:00","chaineDeCertificationValide":true,"periodiciteValide":true,"class":"class fr.atexo.signature.applet.mpe.reponse.InfosSignature","signataire":"Mr Dupont George","emetteur":"AC emmetrice"},"class":"class fr.atexo.signature.applet.mpe.reponse.ReponseSignatureHash","signatureVerifiee":true,"identifiant":"identifiant_hash_1"}],"class":"class fr.atexo.signature.applet.mpe.reponse.Reponse","contenuReponseAnnonceXML":"RXhlbXBsZVhNTA=="}';
        $res = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
        $dataSource = [];
        if (is_array($res['fichiers'])) {
            foreach ($res['fichiers'] as $onFile) {
                $fichier = new Atexo_Signature_FichierVo($onFile);
                $fichier->setTypeFichier(Atexo_Signature_FichierVo::FICHIER);
                $indexEnveloppe = $fichier->getTypeEnveloppe().'_'.$fichier->getNumeroLot();
                $dataEnveloppePiecesLibre = $dataSource[$indexEnveloppe]['autresPieces'];
                if (strstr($fichier->getIdentifiant(), '_')) {
                    $arrayIdentifiants = explode('_', $fichier->getIdentifiant());
                    if (is_array($arrayIdentifiants) && $arrayIdentifiants[0]) {
                        $identifiant = $arrayIdentifiants[0];
                        if (!$dataEnveloppePiecesLibre[$identifiant]) {
                            $dataEnveloppePiecesLibre[$identifiant] = $fichier;
                        } else {
                            $ancienFichier = $dataEnveloppePiecesLibre[$identifiant];
                            $ancienFichier->setCheminFichierXmlAE($fichier->getCheminFichier());
                            $ancienFichier->setCheminFichierSignatureXmlAE($fichier->getCheminSignatureXML());
                        }
                    }
                } else {
                    if (!$dataEnveloppePiecesLibre[$fichier->getIdentifiant()]) {
                        $dataEnveloppePiecesLibre[$fichier->getIdentifiant()] = $fichier;
                    } else {
                        $ancienFichier = $dataEnveloppePiecesLibre[$fichier->getIdentifiant()];
                        $ancienFichier->setCheminFichierXmlAE($fichier->getCheminFichier());
                        $ancienFichier->setCheminFichierSignatureXmlAE($fichier->getCheminSignatureXML());
                    }
                }
                $dataSource[$indexEnveloppe]['autresPieces'] = $dataEnveloppePiecesLibre;
            }
        }
        if (is_array($res['hashs'])) {
            foreach ($res['hashs'] as $onFile) {
                $fichier = new Atexo_Signature_FichierVo($onFile);
                $fichier->setTypeFichier(Atexo_Signature_FichierVo::HASH);
                $indexEnveloppe = $fichier->getTypeEnveloppe().'_'.$fichier->getNumeroLot();
                $dataEnveloppePiecesEdition = $dataSource[$indexEnveloppe]['pieceEdition'];
                $dataEnveloppePiecesEdition[$fichier->getIdentifiant()] = $fichier;
                $dataSource[$indexEnveloppe]['pieceEdition'] = $dataEnveloppePiecesEdition;
            }
        }

        return $dataSource;
    }

    public function merge($dataSourceOld, $dataSourceNew)
    {
        $dataSourceRetour = [];

        if (null != $dataSourceOld && (is_countable($dataSourceOld) ? count($dataSourceOld) : 0) > 0 && null != $dataSourceNew && (is_countable($dataSourceNew) ? count($dataSourceNew) : 0) > 0) {
            foreach ($dataSourceOld as $indexEnveloppe => $enveloppe) {
                // Merge les dataSources pour le type de fichier "pieceEdition"
                if (is_array($enveloppe['pieceEdition']) && count($enveloppe['pieceEdition']) > 0) {
                    $arrayFichier = $enveloppe['pieceEdition'];
                    foreach ($arrayFichier as $identifiant => $fichier) {
                        if ($dataSourceNew[$indexEnveloppe]['pieceEdition'][$identifiant]) {
                            $fichierNew = $dataSourceNew[$indexEnveloppe]['pieceEdition'][$identifiant];
                            $fichierOld = $dataSourceOld[$indexEnveloppe]['pieceEdition'][$identifiant];

                            if ($fichierNew instanceof Atexo_Signature_FichierVo && $fichierOld instanceof Atexo_Signature_FichierVo) {
                                if ($fichierOld->getNomFichier()) {
                                    $fichierNew->setNomFichier($fichierOld->getNomFichier());
                                    $fichierNew->setOrgineFichier($fichierOld->getOrgineFichier());
                                    $fichierNew->setIdFichier($fichierOld->getIdFichier());
                                    $fichierNew->setTailleFichier($fichierOld->getTailleFichier());
                                }
                            }
                            $dataSourceRetour[$indexEnveloppe]['pieceEdition'][$identifiant] = $fichierNew;
                        } else {
                            $dataSourceRetour[$indexEnveloppe]['pieceEdition'][$identifiant] = $dataSourceOld[$indexEnveloppe]['pieceEdition'][$identifiant];
                        }
                    }
                }

                // Merge les dataSources pour le type de fichier "pieceLibre"
                if (is_array($enveloppe['pieceLibre']) && count($enveloppe['pieceLibre']) > 0) {
                    $arrayFichier = $enveloppe['pieceLibre'];
                    foreach ($arrayFichier as $identifiant => $fichier) {
                        if ($dataSourceNew[$indexEnveloppe]['autresPieces'][$identifiant]) {
                            $fichierNew = $dataSourceNew[$indexEnveloppe]['autresPieces'][$identifiant];
                            $fichierOld = $dataSourceOld[$indexEnveloppe]['pieceLibre'][$identifiant];

                            if ($fichierNew instanceof Atexo_Signature_FichierVo && $fichierOld instanceof Atexo_Signature_FichierVo) {
                                if ($fichierOld->getNomFichier()) {
                                    $fichierNew->setNomFichier($fichierOld->getNomFichier());
                                    $fichierNew->setOrgineFichier($fichierOld->getOrgineFichier());
                                    $fichierNew->setTailleFichier($fichierOld->getTailleFichier());
                                }
                            }
                            $dataSourceRetour[$indexEnveloppe]['pieceLibre'][$identifiant] = $fichierNew;
                        } else {
                            $dataSourceRetour[$indexEnveloppe]['pieceLibre'][$identifiant] = $dataSourceOld[$indexEnveloppe]['pieceLibre'][$identifiant];
                        }
                    }
                }

                // Merge les dataSources pour le type de fichier "pieceTypees"
                if (is_array($enveloppe['pieceTypees']) && count($enveloppe['pieceTypees']) > 0) {
                    $arrayFichier = $enveloppe['pieceTypees'];
                    foreach ($arrayFichier as $identifiant => $fichier) {
                        if ($fichier->getFichierAeAjoute()) {
                            if ($dataSourceNew[$indexEnveloppe]['autresPieces'][$identifiant]) {
                                $fichierNew = $dataSourceNew[$indexEnveloppe]['autresPieces'][$identifiant];
                                $fichierOld = $dataSourceOld[$indexEnveloppe]['pieceTypees'][$identifiant];

                                if ($fichierNew instanceof Atexo_Signature_FichierVo && $fichierOld instanceof Atexo_Signature_FichierVo) {
                                    if ($fichierOld->getNomFichier()) {
                                        $fichierNew->setNomFichier($fichierOld->getNomFichier());
                                        $fichierNew->setOrgineFichier($fichierOld->getOrgineFichier());
                                        $fichierNew->setFichierAeAjoute($fichierOld->getFichierAeAjoute());
                                        $fichierNew->setTailleFichier($fichierOld->getTailleFichier());
                                    }
                                }
                                $dataSourceRetour[$indexEnveloppe]['pieceTypees'][$identifiant] = $fichierNew;
                            } else {
                                $dataSourceRetour[$indexEnveloppe]['pieceTypees'][$identifiant] = $dataSourceOld[$indexEnveloppe]['pieceTypees'][$identifiant];
                            }
                        }
                    }
                }
            }
        }

        return $dataSourceRetour;
    }
}
