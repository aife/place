<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonGuides;
use Application\Propel\Mpe\CommonGuidesPeer;

class Atexo_Guides
{
    /*
     * retourne un guide avec son nom de fichier
     */
    public function retrieveGuideByFileName($nameFile, $connexion = false)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $criteria = new Criteria();
        $criteria->add(CommonGuidesPeer::NOM_FICHIER, $nameFile);
        $guide = CommonGuidesPeer::doSelectOne($criteria, $connexion);
        if ($guide instanceof CommonGuides) {
            return $guide;
        }

        return false;
    }
}
