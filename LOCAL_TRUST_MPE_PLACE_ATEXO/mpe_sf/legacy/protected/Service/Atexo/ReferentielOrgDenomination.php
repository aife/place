<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonReferentielOrgDenomination;
use Application\Propel\Mpe\CommonReferentielOrgDenominationPeer;
use Application\Service\Atexo\Controller\Cli;
use PDO;

/**
 * Classe referentiel des orgDenomination.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE-4.0
 */
class Atexo_ReferentielOrgDenomination
{
    public function retreiveOrgDenomination($keyword)
    {
        $word = trim($keyword);
        $arrayMotInterdit = ' and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ';
        if (!strstr($arrayMotInterdit, $word)) {
            $word = '%'.Atexo_Util::replaceArrayCharactersByPourcentagage($word).'%';
            $actif_recherche_avancee = true;
            $results = self::retrieveListRefOrgDenomByDenomNormalise($word, $actif_recherche_avancee);
            $data = [];
            if ((is_countable($results) ? count($results) : 0) > 0) {
                foreach ($results as $res) {
                    $id = $res->getId();
                    $data[$res->getDenominationNormalise()]['id'] = $id;
                    $data[$res->getDenominationNormalise()]['denomination'] = $res->getDenominationNormalise();
                }
            }

            return $data;
        }

        return false;
    }

    public function getAllReferentiel()
    {
        $requete = 'SELECT id FROM Referentiel_org_denomination ';
        $statement = Atexo_Db::getLinkCommon(true)->prepare($requete);
        $results = $statement->execute();
        $data = [];
        if ($results) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $data[] = $row['id'];
            }

            return $data;
        } else {
            return false;
        }
    }

    public function isReferentielExiste($denomination_adapte, $org, $consultation)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c = $c->add(CommonReferentielOrgDenominationPeer::DENOMINATION_ADAPTE, $denomination_adapte);
        $c = $c->add(CommonReferentielOrgDenominationPeer::ID, $consultation, Criteria::NOT_EQUAL);
        $c = $c->add(CommonReferentielOrgDenominationPeer::ORGANISME, $org, Criteria::NOT_EQUAL);
        $arrayReferentielOrgDenom = CommonReferentielOrgDenominationPeer::doSelect($c, $connexion);
        if ($arrayReferentielOrgDenom) {
            return true;
        } else {
            return false;
        }
    }

    public function retrieveOrgDenomination($id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c = $c->add(CommonReferentielOrgDenominationPeer::ID, $id);
        $ReferentielOrgDenom = CommonReferentielOrgDenominationPeer::doSelectOne($c, $connexion);
        if ($ReferentielOrgDenom) {
            return $ReferentielOrgDenom;
        } else {
            return false;
        }
    }

    /* Cette fonction permet d'inserer un objet Referentiel org denomination qui correspont
     * à une consultation non expirée dans la base de donnée
     * elle est appeleé dans les Cli de récuperation des annonces (ANM/ANM BOAMP /ANM omni
    /*************************************************************************************/
    public function insertReferentielOrgDenomonation($commonConsultation)
    {
        if ('' != $commonConsultation->getOrgDenomination()) {
            self::insertReferentielOrgDenomination($commonConsultation->getDenominationAdapte(), $commonConsultation->getOrgDenomination());
        }
    }

    /* Cette fonction permet d'inserer un objet Referentiel org denomination qui correspont
     * à un orgDenomination et une denomination adaptée données passées en paramètre
     * elle est appellé dans les Cli de calcul de remplissage de la table Referentiel_Org_Denomination
    /*************************************************************************************/
    public function insertReferentielOrgDenomination($denominationAdaptee, $orgDenomination)
    {
        if ('' != $orgDenomination) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $c = new Criteria();
            $c->add(CommonReferentielOrgDenominationPeer::DENOMINATION_ADAPTE, $denominationAdaptee);
            $referentielOrgDenom = CommonReferentielOrgDenominationPeer::doSelectOne($c, $connexionCom);
            if (!($referentielOrgDenom instanceof CommonReferentielOrgDenomination)) {
                $referentielOrgDenom = new CommonReferentielOrgDenomination();
                $referentielOrgDenom->setDenomination($orgDenomination);
                $referentielOrgDenom->setDenominationNormalise($orgDenomination);
                $referentielOrgDenom->setDenominationAdapte($denominationAdaptee);
                //A toute valeur insérée, les valeurs ActifRechercheAvancee et DateMajActifRechercheAvancee sont 1 et NOW
                $referentielOrgDenom->setActifRechercheAvancee('1');
                $referentielOrgDenom->setDateMajActifRechercheAvancee(date('Y-m-d H:i:s'));
                $referentielOrgDenom->save($connexionCom);
            }
        }
    }

    public function retrieveListRefOrgDenomByDenomNormalise($denomNormalise, $actif_recherche_avancee = false)
    {
        $listRefOrgDenom = [];
        if ('' != $denomNormalise) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $c = new Criteria();
            $c->add(CommonReferentielOrgDenominationPeer::DENOMINATION_NORMALISE, $denomNormalise, Criteria::LIKE);
            if ($actif_recherche_avancee) {
                $c->add(CommonReferentielOrgDenominationPeer::ACTIF_RECHERCHE_AVANCEE, '1', Criteria::EQUAL);
            }
            $c->addAscendingOrderByColumn(CommonReferentielOrgDenominationPeer::DENOMINATION_NORMALISE);
            $listRefOrgDenom = CommonReferentielOrgDenominationPeer::doSelect($c, $connexion);
            //print_r($connexion);exit;
        }

        return $listRefOrgDenom;
    }

    public function retrieveListDenominationAdapteByDenomNormalise($denomNormalise)
    {
        $listRefOrgDenom = self::retrieveListRefOrgDenomByDenomNormalise($denomNormalise);
        $listDenomAdapte = [];
        foreach ($listRefOrgDenom as $res) {
            $listDenomAdapte[$res->getId()] = $res->getDenominationAdapte();
        }

        return $listDenomAdapte;
    }

    public function getRefOrgDenominationByDenomAdapter($denomAdapter)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonReferentielOrgDenominationPeer::DENOMINATION_ADAPTE, $denomAdapter, Criteria::IN);

        return CommonReferentielOrgDenominationPeer::doSelectOne($c, $connexion);
    }
}
