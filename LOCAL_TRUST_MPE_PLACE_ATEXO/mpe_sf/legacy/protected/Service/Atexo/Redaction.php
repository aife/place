<?php

namespace Application\Service\Atexo;

use App\Entity\Consultation;
use App\Entity\Lot;
use App\Entity\Referentiel\Consultation\ClausesN1;
use App\Entity\Referentiel\Consultation\ClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3;
use App\Repository\ConsultationRepository;
use App\Repository\LotRepository;
use App\Service\Version\VersionService;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTFormePrix;
use Application\Propel\Mpe\CommonTLotTechnique;
use Application\Propel\Mpe\CommonTTranche;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Propel\Mpe\CommonValeurReferentiel;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CategoriesConsiderationsSociales;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use DOMDocument;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;
use Laminas\Http\Client;
use Laminas\Http\Client\Adapter\Curl;
use Laminas\Http\Client\Adapter\Proxy;
use Laminas\Http\Request;

/**
 * $Id: Redaction.php,v 1.94 2014-11-27 13:40:09 gchevret Exp $.
 */

/**
 * commentaires.
 *
 * @author khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Redaction
{
    private string $encoding = 'iso-8859-1';
    private $consultation;
    private $agent;
    public $ticket;
    public $codeErreur;
    public $messageErreur;

    public function __construct($consultation = '', $agent = '', $encoding = 'iso-8859-1')
    {
        $this->encoding = $encoding;
        $this->consultation = $consultation;
        if (!($agent instanceof CommonAgent)) {
            $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getId());
        }
        $this->agent = $agent;
        $this->ticket = $this->getTicketAuthentification();
    }

    public function generateXmlInitialisation()
    {
        $consultation = $this->consultation;
        $domDocument = new DOMDocument('1.0', $this->encoding);
        $redaction = (new Atexo_Xml())->addElement($domDocument, $domDocument, 'redaction');
        $initialisation = (new Atexo_Xml())->addElement($domDocument, $redaction, 'initialisation');
        $domDocument = self::addInfosUserAndPF($domDocument, $initialisation);
        if ($consultation instanceof CommonConsultation) {
            $domDocument = self::addConsultation($consultation, $domDocument, $initialisation);
        }

        return $domDocument->saveXml();
    }

    public function getTicketAuthentification()
    {
        try {
            $client = $this->getZendClient();
            $login = rawurlencode($this->agent->getLogin());
            $pwd = $this->agent->getPassword();
            $client->setUri(Atexo_Config::getParameter('URL_RSEM_REST_AUTENTIFICATION').$login.'/'.$pwd);
            $client->setMethod(Request::METHOD_GET);
            $reponse = $client->send();
            $res = $reponse->getBody();
            $domDoc = (array) simplexml_load_string($res);
            // test si il y a une erreur
            $reponse = Atexo_Util::xmlFileToArray($res, true);
            if ($reponse['code']) {
                $this->codeErreur = $reponse['code'];
                $this->messageErreur = $reponse['message'];
            }

            return $domDoc[0];
        } catch (Exception $e) {
            Prado::log('Erreur Redaction.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Redaction.php');
        }
    }

    public function initialiseContext()
    {
        try {
            $xmlInitialisation = $this->generateXmlInitialisation();
            $ticket = $this->ticket;
            $client = $this->getZendClient();

            $client->setUri(Atexo_Config::getParameter('URL_RSEM_REST_INITIALISATION').'?ticket='.$ticket);

            $client->setMethod(Request::METHOD_PUT);
            $client->setRawBody($xmlInitialisation);
            $client->setEncType('application/xml');
            $reponse = $client->send();
            $res = $reponse->getBody();
            $domDoc = (array) simplexml_load_string($res);
            // test si il y a une erreur
            $reponse = Atexo_Util::xmlFileToArray($res, true);
            if ($reponse['code']) {
                $this->codeErreur = $reponse['code'];
                $this->messageErreur = $reponse['message'];
            }

            return $domDoc[0];
        } catch (Exception $e) {
            $this->codeErreur = 1;
            $this->messageErreur = $e->getMessage();
        }
    }

    //intialise context que soit il ya une consultation et non
    public function intialiseConsultation()
    {
        return $this->initialiseContext();
    }

    public function getListeDocument()
    {
        $ticket = $this->ticket;
        $refConsultation = base64_encode($this->consultation->getOrganisme().'#'.$this->consultation->getId());
        $client = $this->getZendClient();

        $client->setParameterGet(["identifiantConsultation" => $refConsultation, "ticket" => $ticket]);
        $client->setUri(Atexo_Config::getParameter('URL_RSEM_GET_LISTE_DOCUMENT'));
        $client->setMethod(Request::METHOD_GET);
        $reponse = $client->send();

        return $reponse->getBody();
    }

    public function getContentDocument($identifiant, $name, $organisme = null)
    {
        $dateFin = $this->consultation->getDatefin();
        $time = strtotime($dateFin).'000';
        $ticket = $this->ticket;
        $client = $this->getZendClient();

        $client->setParameterGet(['identifiantDocument' => $identifiant, 'ticket' => $ticket]);
        $urlDocumentRedac = Atexo_Config::getParameter('URL_RSEM_GET_CONTENT_DOCUMENT_REDAC').'?numPieceJointe=0&convertirEnPdf=true&dateLimitePlis='.$time.(!empty($organisme) ? '&acronymeOrganisme='.$organisme : '');
        $client->setUri($urlDocumentRedac);
        $client->setMethod(Request::METHOD_GET);
        $res = $client->send();
        $infileAutrePiece = Atexo_Config::getParameter('COMMON_TMP').'nouveauDossierOperation'.session_id().time().$identifiant;
        Atexo_Util::write_file($infileAutrePiece, $res->getBody());

        return $infileAutrePiece;
    }

    public function ajouterPairesType($consultation, $domDocument, $pairesType, $lot = null)
    {
        $donnesComplementaitre = $consultation->getDonneComplementaire();
        $oui = Prado::localize('DEFINE_OUI');
        $non = Prado::localize('DEFINE_NON');

        if ($lot) {
            $consultation = $lot;
            $donnesComplementaitre = $lot->getDonneComplementaire();
        }
        if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'CAUTION');
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $donnesComplementaitre->getCautionProvisoire());

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'CAUTION_ON');
            if ($donnesComplementaitre->getCautionProvisoire()) {
                $cautionOn = $oui;
            } else {
                $cautionOn = $non;
            }
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $cautionOn);
            //variantes
            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'VARIANTE');
            if ($donnesComplementaitre->getVariantes()) {
                $variante = $oui;
            } else {
                $variante = $non;
            }
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $variante);

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'CHOIX_ECHANT');
            if (0 == $donnesComplementaitre->getEchantillon()) {
                $choixEch = $non;
            } else {
                $choixEch = $oui;
            }
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $choixEch);

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'ADR_ECHANT');
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $donnesComplementaitre->getAddEchantillon());

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'DATE_ECHANT');
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', Atexo_Util::iso2frnDateTime($donnesComplementaitre->getDateLimiteEchantillon()));

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'CHOIX_REUNION');
            if (0 == $donnesComplementaitre->getReunion()) {
                $reunion = $non;
            } else {
                $reunion = $oui;
            }
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $reunion);

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'ADR_REUNION');
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $donnesComplementaitre->getAddReunion());

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'DATE_REUNION');
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', Atexo_Util::iso2frnDateTime($donnesComplementaitre->getDateReunion()));

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'CHOIX_VISITE');
            if (1 == $donnesComplementaitre->getVisitesLieux()) {
                $choixVisite = $oui;
            } else {
                $choixVisite = $non;
            }
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $choixVisite);
        }
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'AGREMENT_ON');
        if ($consultation->getAgrements()) {
            $agrementOn = $oui;
        } else {
            $agrementOn = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $agrementOn);

        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'QUALIFICATION_ON');
        if ($consultation->getQualification()) {
            $qualificationOn = $oui;
        } else {
            $qualificationOn = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $qualificationOn);
    }

    public function ajouterPairesTypeComplexe($consultationInit, $domDocument, $pairesTypeComplexe, $lot = null)
    {
        if ($lot) {
            $consultation = $lot;
            $numLot = $lot->getLot();
        } else {
            $consultation = $consultationInit;
            $numLot = 0;
        }
        // PaireTypeCompexes Agrement
        $agrements = explode(',', $consultation->getAgrements());
        if ($consultation->getAgrements() && is_array($agrements) && count($agrements) > 0) {
            $libellesAgrements = Atexo_Agrement::retrieveAgrementById($agrements);
            if (is_array($libellesAgrements) && count($libellesAgrements) > 0) {
                foreach ($libellesAgrements as $libelle) {
                    $paireTypeComplexe = (new Atexo_Xml())->addElement($domDocument, $pairesTypeComplexe, 'paireTypeComplexe');
                    (new Atexo_Xml())->addAttribute($domDocument, $paireTypeComplexe, 'idObjetComplexe', 'AGREMENT');
                    $paireType = (new Atexo_Xml())->addElement($domDocument, $paireTypeComplexe, 'paireType');
                    $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'LIBELLE');
                    $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $libelle->getLibelle());
                }
            }
        }
        // PaireTypeCompexes Qualification
        $qualifications = (new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($consultation->getQualification(), true);
        if (is_array($qualifications) && count($qualifications) > 0) {
            foreach ($qualifications as $qualification) {
                $paireTypeComplexe = (new Atexo_Xml())->addElement($domDocument, $pairesTypeComplexe, 'paireTypeComplexe');
                (new Atexo_Xml())->addAttribute($domDocument, $paireTypeComplexe, 'idObjetComplexe', 'QUALIFICATION');
                $paireType = (new Atexo_Xml())->addElement($domDocument, $paireTypeComplexe, 'paireType');
                $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'TYPE');
                $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $qualification['type']);
                $paireType = (new Atexo_Xml())->addElement($domDocument, $paireTypeComplexe, 'paireType');
                $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'SECTEUR');
                $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $qualification['secteur']);
                $paireType = (new Atexo_Xml())->addElement($domDocument, $paireTypeComplexe, 'paireType');
                $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'QUALIFICATION');
                $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $qualification['qualification']);
                $paireType = (new Atexo_Xml())->addElement($domDocument, $paireTypeComplexe, 'paireType');
                $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'CLASSE');
                $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $qualification['classe']);
            }
        }
        // PaireTypeCompexes VisitesLieux
        if (1 == $consultation->getVisitesLieux()) {
            $visitesLieux = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($consultationInit->getId(), $numLot, $consultation->getOrganisme());
            if (is_array($visitesLieux) && count($visitesLieux) > 0) {
                foreach ($visitesLieux as $visiteLieux) {
                    $paireTypeComplexe = (new Atexo_Xml())->addElement($domDocument, $pairesTypeComplexe, 'paireTypeComplexe');
                    (new Atexo_Xml())->addAttribute($domDocument, $paireTypeComplexe, 'idObjetComplexe', 'ADR_VISITE_DATE_VISITE');
                    $paireType = (new Atexo_Xml())->addElement($domDocument, $paireTypeComplexe, 'paireType');
                    $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'DATE');
                    $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', Atexo_Util::iso2frnDateTime($visiteLieux->getDate()));
                    $paireType = (new Atexo_Xml())->addElement($domDocument, $paireTypeComplexe, 'paireType');
                    $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'ADRESSE');
                    $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $visiteLieux->getAdresse());
                }
            }
        }
    }

    public function addInfosUserAndPF($domDocument, $initialisation)
    {
        $agent = $this->agent;

        (new Atexo_Xml())->addElement($domDocument, $initialisation, 'plateforme', Atexo_Config::getParameter('UID_PF_MPE'));

        (new Atexo_Xml())->addElement($domDocument, $initialisation, 'version', Atexo_Util::getSfService(VersionService::class)->getApplicationVersion());

        $utilisateur = (new Atexo_Xml())->addElement($domDocument, $initialisation, 'utilisateur');
        (new Atexo_Xml())->addElement($domDocument, $utilisateur, 'nom', Atexo_Util::toUtf8($agent->getNom()));
        (new Atexo_Xml())->addElement($domDocument, $utilisateur, 'prenom', Atexo_Util::toUtf8($agent->getPrenom()));
        (new Atexo_Xml())->addElement($domDocument, $utilisateur, 'courriel', $agent->getEmail());
        (new Atexo_Xml())->addElement($domDocument, $utilisateur, 'identifiantExterne', $agent->getLogin());
        $habilitations = (new Atexo_Xml())->addElement($domDocument, $utilisateur, 'habilitations');
        if (Atexo_CurrentUser::hasHabilitation('RedactionDocumentsRedac')) {
            $habilitationRedac = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_RedactionDocumentsREDAC');
        }
        if (Atexo_CurrentUser::hasHabilitation('ValidationDocumentsRedac')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_ValidationDocumentsREDAC');
        }
        if (Atexo_CurrentUser::hasHabilitation('AdministrerClauses')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_clause');
        }
        if (Atexo_CurrentUser::hasHabilitation('AdministrerCanevas')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_canevas');
        }
        if (Atexo_CurrentUser::hasHabilitation('ValiderClauses')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_validerClauses');
        }
        if (Atexo_CurrentUser::hasHabilitation('ValiderCanevas')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_validerCanevas');
        }
        if (Atexo_CurrentUser::hasHabilitation('ValiderClausesEditeur')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_validerClausesEditeur');
        }
        if (Atexo_CurrentUser::hasHabilitation('ValiderCanevasEditeur')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_validerCanevasEditeur');
        }
        if (Atexo_CurrentUser::hasHabilitation('AdministrerClausesEditeur')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_administrerClausesEditeur');
        }
        if (Atexo_CurrentUser::hasHabilitation('AdministrerCanevasEditeur')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_administrerCanevasEditeur');
        }
        if (Atexo_CurrentUser::hasHabilitation('GenererPiecesFormatOdt')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_editerFormatODT');
        }
        if (Atexo_CurrentUser::hasHabilitation('PublierVersionClausierEditeur')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_publierClausierEditeur');
        }
        if (Atexo_CurrentUser::hasHabilitation('AdministrerClausesEntiteAchats')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_direction_service');
        }
        if (Atexo_CurrentUser::hasHabilitation('ReprendreIntegralementArticle')) {
            $habilitationValidation = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_integralement_article');
        }

        if (Atexo_CurrentUser::hasHabilitation('GererGabaritEditeur')) {
            $habilitationGererGabaritEditeur = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_gabaritInterministeriel');
        }
        if (Atexo_CurrentUser::hasHabilitation('GererGabarit')) {
            $habilitationGererGabarit = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_gabaritMinisteriel');
        }
        if (Atexo_CurrentUser::hasHabilitation('GererGabaritEntiteAchats')) {
            $habilitationGererGabaritEntiteAchats = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_gabaritDirection');
        }
        if (Atexo_CurrentUser::hasHabilitation('ActiverVersionClausier')) {
            $habilitationActiverVersionClausier = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_activerVersionClausier');
        }

        $habilitationRedac = (new Atexo_Xml())->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_document');

        $configuration = (new Atexo_Xml())->addElement($domDocument, $utilisateur, 'configuration');
        if (Atexo_Module::isEnabled('PlateformeEditeur')) {
            $configurationPlateformeEditeur = (new Atexo_Xml())->addElement($domDocument, $configuration, 'plateformeEditeur', 'true');
        } else {
            $configurationPlateformeEditeur = (new Atexo_Xml())->addElement($domDocument, $configuration, 'plateformeEditeur', 'false');
        }

        return $domDocument;
    }

    public function addConsultation($consultation, $domDocument, $initialisation)
    {
        $oui = Prado::localize('DEFINE_OUI');
        $non = Prado::localize('DEFINE_NON');
        $donnesComplementaitre = $consultation->getDonneComplementaire();
        $consultationTag = (new Atexo_Xml())->addElement($domDocument, $initialisation, 'consultation');
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'lieuExecution', 'NAN');
        $this->insertListeLieuxExecution($consultation, $domDocument, $consultationTag);
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'referenceExterne', base64_encode($consultation->getOrganisme().'#'.$consultation->getId()));
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'numeroConsultation', $consultation->getReferenceUtilisateur());
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'numeroSadAc', $consultation->getNumeroAc());
        $urlConsultation = $_SERVER['SERVER_NAME']  . '/index.php?page=Agent.DetailConsultation&id=' . $consultation->getId();
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'url', $urlConsultation);
        if ($consultation->getIntitule() && '' != trim($consultation->getIntitule())) {
            $intitule = (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'intitule', $consultation->getIntitule());
        } else {
            $intitule = (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'intitule', $consultation->getObjet());
        }
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'objet', $consultation->getObjet());
        // Debut type contrat
        $typeContratQuery = new CommonTTypeContratQuery();
        $typeContrat = $typeContratQuery->getTypeContratById($consultation->getTypeMarche());
        if ($typeContrat instanceof CommonTTypeContrat) {
            $TypeContratTag = (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'typeContrat');
            (new Atexo_Xml())->addElement($domDocument, $TypeContratTag, 'id', $typeContrat->getIdTypeContrat());
            (new Atexo_Xml())->addElement($domDocument, $TypeContratTag, 'libelle', Prado::localize($typeContrat->getLibelleTypeContrat()));
            (new Atexo_Xml())->addElement($domDocument, $TypeContratTag, 'abreviation', $typeContrat->getAbreviationTypeContrat());
        }
        // fin type contrat
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'typeProcedure', (new Atexo_Consultation_ProcedureType())->retrieveAbreviationTypeProcedure($consultation->getIdTypeProcedureOrg()));
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'code_externe_procedure', (new Atexo_Consultation_ProcedureType())->retreiveidExterneTypeProcedure($consultation->getIdTypeProcedureOrg()));
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'dateRemisePlis', Atexo_Util::iso2iso8601DateTime($consultation->getDatefin()));
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'dateMiseLigne', Atexo_Util::iso2iso8601DateTime($consultation->getDateMiseEnLigneCalcule()));
        $autoriserReponse = $this->getLibelleAutoriserReponse($consultation->getAutoriserReponseElectronique(), $consultation->getReponseObligatoire());
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'reponseElectronique', $autoriserReponse);
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'signatureElectronique', $consultation->getSignatureOffre());
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'chiffrementPlis', $consultation->getChiffrementOffre());
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'enveloppeUnique', 'NAN');

        $naturePrestation = $consultation->getCategorie();
        if ($naturePrestation == Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')) {
            $naturePrestation = 'T';
        } elseif ($naturePrestation == Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')) {
            $naturePrestation = 'F';
        } elseif ($naturePrestation == Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')) {
            $naturePrestation = 'S';
        } else {
            $naturePrestation = null;
        }
        $naturePrestation = (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'naturePrestation', $naturePrestation);
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'pouvoirAdjudicateur', $consultation->getOrganisme());
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'statut', '1');
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'article', '1');
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'directionService', $consultation->getServiceId() ?? '0');
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'transverse', $consultation->getConsultationTransverse());
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'compatibleEntiteAdjudicatrice', $consultation->getEntiteAdjudicatrice());
        (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'mps', ($consultation->getMarchePublicSimplifie() ? 'true' : 'false'));

        if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
            self::insertListeCpv($consultation, $domDocument, $consultationTag);
        }

        // Données complémentaires
        self::ajouterInfoDonneeComplementaireConsultation($consultation, $domDocument, $consultationTag);

        $pairesType = (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'pairesType');
        if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'DELAI_PARTIEL');
            $delaiPartiel = $non;
            if ($donnesComplementaitre->getDelaiPartiel() == Atexo_Config::getParameter('DELAI_PARTIEL_OUI')) {
                $delaiPartiel = $oui;
            }
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $delaiPartiel);
            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'ADR_RETRAIT');
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', Atexo_Util::toUtf8($donnesComplementaitre->getAdresseRetraisDossiers()));

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'ADR_DEPOT');
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', Atexo_Util::toUtf8($donnesComplementaitre->getAdresseDepotOffres()));

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'LIEU_OUV');
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', Atexo_Util::toUtf8($donnesComplementaitre->getLieuOuverturePlis()));

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'PRIX_ ACQUISITION');
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', Atexo_Util::toUtf8($donnesComplementaitre->getPrixAquisitionPlans()));

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'PRIX_ACQUISITION_ON');
            if ($donnesComplementaitre->getPrixAquisitionPlans()) {
                $prixAOn = $oui;
            } else {
                $prixAOn = $non;
            }
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $prixAOn);
        }
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'MODE_PASSATION');
        $modePassation = null;
        if ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_AU_RABAIS')) {
            $modePassation = Prado::localize('AU_RABAIS');
        } elseif ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_OFFRE_PRIX')) {
            $modePassation = Prado::localize('SUR_OFFRE_DE_PRIX');
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $modePassation);

        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'MODE_PASSATION_ON');
        if ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_OFFRE_PRIX')) {
            $modePON = $oui;
        } else {
            $modePON = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $modePON);

        $articleObject = (new Atexo_Consultation_ProcedureType())->retrieveArticleByProcedureType($consultation->getIdTypeProcedure(), $consultation->getModePassation());
        if ($articleObject) {
            if ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_AU_RABAIS')) {
                $modePassation = Prado::localize('AU_RABAIS');
            } elseif ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_OFFRE_PRIX')) {
                $modePassation = Prado::localize('SUR_OFFRE_DE_PRIX');
            }
            $typeProc = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg());
            $msgArticle = $typeProc.' '.$modePassation.' en vertu de '.$articleObject->getLibelle();
            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'ARTICLE');
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $msgArticle);
        }

        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'CANDIDATURE');
        if ('1' == $consultation->getEnvCandidature()) {
            $candidature = $oui;
        } else {
            $candidature = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $candidature);

        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'OFFRE_TECHNIQUE');
        if ('0' != $consultation->getEnvOffreTechnique()) {
            $offreT = $oui;
        } else {
            $offreT = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $offreT);
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'OFFRE_TECHNIQUE_ALLOTI_OUI');
        if ('0' != $consultation->getEnvOffreTechnique() && $consultation->getAlloti()) {
            $offreTLotOui = $oui;
        } else {
            $offreTLotOui = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $offreTLotOui);
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'OFFRE_TECHNIQUE_ALLOTI_NON');
        if ('0' != $consultation->getEnvOffreTechnique() && !$consultation->getAlloti()) {
            $offreTLotNon = $oui;
        } else {
            $offreTLotNon = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $offreTLotNon);
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'OFFRE_TECHNIQUE_DOSSIER_ADDITF_OUI');
        if ('0' != $consultation->getEnvOffreTechnique() && '0' != $consultation->getDossierAdditif()) {
            $offreTAdditifOui = $oui;
        } else {
            $offreTAdditifOui = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $offreTAdditifOui);
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'OFFRE_TECH_PAS_DOSSIER_ADDITF');
        if ('0' != $consultation->getEnvOffreTechnique() && '0' == $consultation->getDossierAdditif()) {
            $offreTAdditifNon = $oui;
        } else {
            $offreTAdditifNon = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $offreTAdditifNon);

        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'OFFRE_FINANCIERE');
        if ('0' != $consultation->getEnvOffre()) {
            $offreF = $oui;
        } else {
            $offreF = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $offreF);
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'DOSSIER_ADDITIF');
        if ($consultation->getDossierAdditif()) {
            $dossierAdditif = $oui;
        } else {
            $dossierAdditif = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $dossierAdditif);
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'OFFRE_FINANCIERE_ALLOTI_OUI');
        if ('0' != $consultation->getEnvOffre() && $consultation->getAlloti()) {
            $offreFLotOui = $oui;
        } else {
            $offreFLotOui = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $offreFLotOui);
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'OFFRE_FINANCIERE_ALLOTI_NON');
        if ('0' != $consultation->getEnvOffre() && !$consultation->getAlloti()) {
            $offreFLotNon = $oui;
        } else {
            $offreFLotNon = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $offreFLotNon);

        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'TYPE_MARCHE');
        $typeMarche = null;
        if ($consultation->getTypeMarche() == Atexo_Config::getParameter('TYPE_MARCHE_MARCHE')) {
            $typeMarche = Atexo_Util::toUtf8(Prado::localize('TEXT_MARCHE'));
        } elseif ($consultation->getTypeMarche() == Atexo_Config::getParameter('TYPE_MARCHE_CADRE')) {
            $typeMarche = Atexo_Util::toUtf8(Prado::localize('DEFINE_MARCHE_CADRE'));
        } elseif ($consultation->getTypeMarche() == Atexo_Config::getParameter('TYPE_MARCHE_RECONDUCTIBLE')) {
            $typeMarche = Atexo_Util::toUtf8(Prado::localize('DEFINE_MARCHE_RECONDUCTIBLE'));
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $typeMarche);

        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'TYPE_MARCHE_ON');
        if ($consultation->getTypeMarche() == Atexo_Config::getParameter('TYPE_MARCHE_MARCHE') || $consultation->getTypeMarche() == Atexo_Config::getParameter('TYPE_MARCHE_CADRE')) {
            $typeMarcheOn = $oui;
        } else {
            $typeMarcheOn = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $typeMarcheOn);

        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'TYPE_MARCHE_CADRE_OU_RECONDUCTIBLE_ON');
        if ($consultation->getTypeMarche() == Atexo_Config::getParameter('TYPE_MARCHE_RECONDUCTIBLE') || $consultation->getTypeMarche() == Atexo_Config::getParameter('TYPE_MARCHE_CADRE')) {
            $typeMarcheCOuROn = $oui;
        } else {
            $typeMarcheCOuROn = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $typeMarcheCOuROn);
        if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'DELAI_PARTIEL_ET_TYPE_MARCHE_CADRE_OU_RECONDUCTIBLE_ON');
            if ($donnesComplementaitre->getDelaiPartiel() == Atexo_Config::getParameter('DELAI_PARTIEL_OUI') && ($consultation->getTypeMarche() == Atexo_Config::getParameter('TYPE_MARCHE_RECONDUCTIBLE') || $consultation->getTypeMarche() == Atexo_Config::getParameter('TYPE_MARCHE_CADRE'))) {
                $delaiPartMarcheCOuROn = $oui;
            } else {
                $delaiPartMarcheCOuROn = $non;
            }
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $delaiPartMarcheCOuROn);
        }
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'TYPE_MARCHE_CADRE_ON');
        if ($consultation->getTypeMarche() == Atexo_Config::getParameter('TYPE_MARCHE_CADRE')) {
            $cadreOn = $oui;
        } else {
            $cadreOn = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $cadreOn);

        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'TYPE_MARCHE_RECONDUCTIBLE_ON');
        if ($consultation->getTypeMarche() == Atexo_Config::getParameter('TYPE_MARCHE_RECONDUCTIBLE')) {
            $reconductibleOn = $oui;
        } else {
            $reconductibleOn = $non;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $reconductibleOn);

        if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'PRESTATION');
            $typePrestation = null;
            if ($donnesComplementaitre->getTypePrestation() == Atexo_Config::getParameter('TYPE_PRESTATION_COURANTE')) {
                $typePrestation = Prado::localize('DEFINE_PRESTATION_COURANTE');
            } elseif ($donnesComplementaitre->getTypePrestation() == Atexo_Config::getParameter('TYPE_PRESTATION_COMPLEXE')) {
                $typePrestation = Prado::localize('DEFINE_PRESTATION_COMPLEXE');
            }
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', Atexo_Util::toUtf8($typePrestation));

            $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
            $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'PRESTATION_ON');
            if ($donnesComplementaitre->getTypePrestation() == Atexo_Config::getParameter('TYPE_PRESTATION_COURANTE')) {
                $prestationOn = $oui;
            } else {
                $prestationOn = $non;
            }
            $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $prestationOn);

            $criteresIdentiques = $donnesComplementaitre->getCriteresIdentiques();
        }
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'NOMBRE_PLI');
        $nbrPli = 0;
        if ('0' != $consultation->getEnvOffre()) {
            ++$nbrPli;
        }
        if ('0' != $consultation->getEnvCandidature()) {
            ++$nbrPli;
        }
        if ('0' != $consultation->getEnvOffreTechnique()) {
            ++$nbrPli;
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $nbrPli);
        $paireType = (new Atexo_Xml())->addElement($domDocument, $pairesType, 'paireType');
        $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'NOMBRE_LOT');
        $nbrLot = 0;
        if ($consultation->getAlloti()) {
            $lesLots = $consultation->getAllLots();
            if (is_array($lesLots)) {
                $nbrLot = count($lesLots);
            }
        }
        $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', $nbrLot);

        // PaireTypeCompexes Activite
        $pairesTypeComplexe = (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'pairesTypeComplexe');
        $activites = explode('#', $consultation->getDomainesActivites());
        if (is_array($activites) && count($activites) > 0) {
            $souscategories = (new Atexo_Consultation_Category())->retrieveListeSousCategorie($activites);
            if (is_array($souscategories) && count($souscategories) > 0) {
                foreach ($souscategories as $souscategorie) {
                    $paireTypeComplexe = (new Atexo_Xml())->addElement($domDocument, $pairesTypeComplexe, 'paireTypeComplexe');
                    (new Atexo_Xml())->addAttribute($domDocument, $paireTypeComplexe, 'idObjetComplexe', 'ACTIVITE');
                    $paireType = (new Atexo_Xml())->addElement($domDocument, $paireTypeComplexe, 'paireType');
                    $cle = (new Atexo_Xml())->addElement($domDocument, $paireType, 'cle', 'LIBELLE');
                    $valeur = (new Atexo_Xml())->addElement($domDocument, $paireType, 'valeur', Atexo_Util::toUtf8($souscategorie->getLibelle()));
                }
            }
        }

        if ($consultation->getAlloti()) {
            $lots = (new Atexo_Xml())->addElement($domDocument, $consultationTag, 'lots');
            $arrayLots = $consultation->getAllLots();
            if (is_array($arrayLots) && count($arrayLots) > 0) {
                foreach ($arrayLots as $oneElement) {
                    $oneLot = (new Atexo_Xml())->addElement($domDocument, $lots, 'lot');
                    (new Atexo_Xml())->addElement($domDocument, $oneLot, 'description', htmlspecialchars(Atexo_Util::toUtf8($oneElement->getDescriptionDetail())));

                    (new Atexo_Xml())->addElement($domDocument, $oneLot, 'intitule', htmlspecialchars(Atexo_Util::toUtf8($oneElement->getDescription())));
                    (new Atexo_Xml())->addElement($domDocument, $oneLot, 'numeroLot', Atexo_Util::toUtf8($oneElement->getLot()));
                    $naturePrest = $oneElement->getCategorie();
                    if ($naturePrest == Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')) {
                        $naturePrest = 'T';
                    } elseif ($naturePrest == Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')) {
                        $naturePrest = 'F';
                    } elseif ($naturePrest == Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')) {
                        $naturePrest = 'S';
                    } else {
                        $naturePrest = null;
                    }

                    $naturePrestation = (new Atexo_Xml())->addElement($domDocument, $oneLot, 'naturePrestation', $naturePrest);
                    $pairesType = (new Atexo_Xml())->addElement($domDocument, $oneLot, 'pairesType');
                    self::ajouterPairesType($consultation, $domDocument, $pairesType, $oneElement);
                    $pairesTypeComplexe = (new Atexo_Xml())->addElement($domDocument, $oneLot, 'pairesTypeComplexe');
                    //Les types complexe
                    self::ajouterPairesTypeComplexe($consultation, $domDocument, $pairesTypeComplexe, $oneElement);
                    // liste des code CPV
                    if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
                        self::insertListeCpv($oneElement, $domDocument, $oneLot);
                    }
                    //Construction de l'xml des critères d'attribution
                    if ($criteresIdentiques) {
                        self::construireXmlCriteresAttribution($consultation, $domDocument, $oneLot);
                    } else {
                        self::construireXmlCriteresAttribution($oneElement, $domDocument, $oneLot);
                    }
                    // données complémentaires
                    self::ajouterInfoDonneeComplementaire($oneElement, $domDocument, $oneLot);
                }
            }
            self::initiDurreMarche($domDocument, $consultationTag, $consultation);
        } else {
            self::ajouterPairesType($consultation, $domDocument, $pairesType);
            //Les types complexe
            self::ajouterPairesTypeComplexe($consultation, $domDocument, $pairesTypeComplexe);
            //Construction de l'xml des critères d'attribution
            self::construireXmlCriteresAttribution($consultation, $domDocument, $consultationTag);
            // données complémentaires
            self::ajouterInfoDonneeComplementaire($consultation, $domDocument, $consultationTag);
        }

        return $domDocument;
    }

    /*
     * Permet d'ajouter la liste des code cpv
     */

    public function insertListeCpv($objet, $domDocument, $tag)
    {
        $dataRefFile = null;
        if ($objet->getCodeCpv1()) {
            $listeCodeCPV = (new Atexo_Xml())->addElement($domDocument, $tag, 'listeCodeCPV');
            $atexo_ref = new Atexo_Ref();
            $atexo_ref->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_DEMANDE_PRINCIPAL_CONFIG'));
            $atexo_ref->setPrincipal($objet->getCodeCpv1());
            $atexo_ref->setSecondaires($objet->getCodeCpv2());
            if ($atexo_ref instanceof Atexo_Ref) {
                // la langue
                $lang = 'fr';
                $atexo_ref->setLocale($lang);
                //fichier de données
                $arrayConfigXml = $atexo_ref->getArrayConfigXml();
                if ($arrayConfigXml) {
                    $dataRefFile = Atexo_Config::getParameter('APP_BASE_ROOT_DIR')
                        .$arrayConfigXml['cheminData']
                        .$arrayConfigXml['fichierDonnee']
                        .$lang
                        .'.csv';
                }
                //code principal
                if ($atexo_ref->getPrincipal()) {
                    // le code Cpv 1
                    $cpv = (new Atexo_Xml())->addElement($domDocument, $listeCodeCPV, 'cpv');
                    $codeCPV = (new Atexo_Xml())->addElement($domDocument, $cpv, 'codeCPV', $atexo_ref->getPrincipal());
                    if (is_file($dataRefFile)) {
                        $libelle = $atexo_ref->getLibelleRef($atexo_ref->getPrincipal(), $dataRefFile);
                        $libelleCPV = (new Atexo_Xml())->addElement($domDocument, $cpv, 'libelleCPV', Atexo_Util::toUtf8($libelle));
                    }
                }
                //codes secondaires
                if ($atexo_ref->getSecondaires()) {
                    $arrayCodesCPV = explode('#', $atexo_ref->getSecondaires());
                    if (is_array($arrayCodesCPV) && $arrayCodesCPV) {
                        foreach ($arrayCodesCPV as $cpv) {
                            if ($cpv) {
                                $cpv2 = (new Atexo_Xml())->addElement($domDocument, $listeCodeCPV, 'cpv');
                                $codeCPV = (new Atexo_Xml())->addElement($domDocument, $cpv2, 'codeCPV', $cpv);
                                if (is_file($dataRefFile)) {
                                    $libelle = $atexo_ref->getLibelleRef($cpv, $dataRefFile);
                                    $libelleCPV = (new Atexo_Xml())->addElement($domDocument, $cpv2, 'libelleCPV', Atexo_Util::toUtf8($libelle));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /*
     * ajouté les information des donnée complémentaire qui se trouve seulement dans la consultation
     * @param : $consultation,$domDocument,$tag
     */
    public function ajouterInfoDonneeComplementaireConsultation($consultation, $domDocument, $tag)
    {
        $donneeCmp = $consultation->getDonneComplementaire();
        if ($donneeCmp instanceof CommonTDonneeComplementaire) {
            $refFormeGroupement = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_FORME_GROUPEMENT'), $donneeCmp->getIdGroupementAttributaire());
            if ($refFormeGroupement instanceof CommonValeurReferentiel) {
                (new Atexo_Xml())->addElement($domDocument, $tag, 'formeGroupementAttributaire', Atexo_Util::toUtf8($refFormeGroupement->getLibelle2()));
            }
            if ($donneeCmp->getDelaiValiditeOffres()) {
                (new Atexo_Xml())->addElement($domDocument, $tag, 'dureeValiditeOffreEnJour', $donneeCmp->getDelaiValiditeOffres());
            }
            if (!$consultation->getAlloti() && $donneeCmp->getJustificationNonAlloti()) {
                $justificationNonAllotissement = (new Atexo_Xml())->addElement($domDocument, $tag, 'justificationNonAllotissement', Atexo_Util::toUtf8($donneeCmp->getJustificationNonAlloti()));
            }
            $nombreCandidats = (new Atexo_Xml())->addElement($domDocument, $tag, 'nombreCandidats');
            if ($donneeCmp->getIdNbCandidatsAdmis() == Atexo_Config::getParameter('ID_NB_CANDIDATS_ADMIS_FIXE')) {
                (new Atexo_Xml())->addElement($domDocument, $nombreCandidats, 'nombreFixe', $donneeCmp->getNombreCandidatsFixe());
            } elseif ($donneeCmp->getIdNbCandidatsAdmis() == Atexo_Config::getParameter('ID_NB_CANDIDATS_ADMIS_FOURCHETTE')) {
                (new Atexo_Xml())->addElement($domDocument, $nombreCandidats, 'nombreMax', $donneeCmp->getNombreCandidatsMax());
                (new Atexo_Xml())->addElement($domDocument, $nombreCandidats, 'nombreMin', $donneeCmp->getNombreCandidatsMin());
            }
            if ($donneeCmp->getPhaseSuccessive()) {
                (new Atexo_Xml())->addElement($domDocument, $nombreCandidats, 'reductionProgressive', 'true');
            }
        }
    }

    /*
     * ajouté les information des donnée complémentaire qui se trouve seulement dans la consultation
     * @param : $objet (consultation ou lot),$domDocument,$tag
     */
    public function ajouterInfoDonneeComplementaire($objet, $domDocument, $tag)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $informationsComplementaires = (new Atexo_Xml())->addElement($domDocument, $tag, 'informationsComplementaires');
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            if ($objet instanceof CommonConsultation) {
                $repoClass = ConsultationRepository::class;
            } else {
                $repoClass = LotRepository::class;
            }
            $consultationOrLotRepository = Atexo_Util::getSfService($repoClass);
            /** @var Consultation|Lot $symfonyObject */
            $symfonyObject = $consultationOrLotRepository->find($objet->getId());

            $clauses = $symfonyObject->getClausesN1();

            $structuresSocialeReserves = (new Atexo_Xml())->addElement($domDocument, $informationsComplementaires, 'structuresSocialeReserves');
            if (!$clauses->isEmpty()) {
                foreach ($clauses as $key => $clause) {
                    if ($clause->getReferentielClauseN1()->getSlug() == ClausesN1::CLAUSES_ENVIRONNEMENTALES) {
                        $clauseEnvIndex = $key;
                    } elseif ($clause->getReferentielClauseN1()->getSlug() == ClausesN1::CLAUSES_SOCIALES) {
                        $clauseSocIndex = $key;
                    }
                }

                if (isset($clauseEnvIndex)) {
                    $clauseEnv = (new Atexo_Xml())->addElement($domDocument, $tag, 'clausesEnvironnementales');
                    foreach ($clauses[$clauseEnvIndex]->getClausesN2() as $clauseN2) {
                        if ($clauseN2->getReferentielClauseN2()->getSlug() == ClausesN2::SPECIFICATIONS_TECHNIQUES) {
                            $clauseEnvSpecTech = true;
                        } elseif ($clauseN2->getReferentielClauseN2()->getSlug() == ClausesN2::CONDITIONS_EXECUTIONS) {
                            $clauseEnvCondExec = true;
                        } elseif ($clauseN2->getReferentielClauseN2()->getSlug() == ClausesN2::CRITERES_SELECTIONS) {
                            $clauseEnvCriSel = true;
                        }
                    }
                    (new Atexo_Xml())->addElement($domDocument, $clauseEnv, 'specificationTechnique', (!empty($clauseEnvSpecTech) ? 'true' : 'false'));
                    (new Atexo_Xml())->addElement($domDocument, $clauseEnv, 'conditionExecution', (!empty($clauseEnvCondExec) ? 'true' : 'false'));
                    (new Atexo_Xml())->addElement($domDocument, $clauseEnv, 'critereAttribution', (!empty($clauseEnvCriSel) ? 'true' : 'false'));
                }

                if (isset($clauseSocIndex)) {
                    $clauseSoc = (new Atexo_Xml())->addElement($domDocument, $tag, 'clausesSociales');
                    (new Atexo_Xml())->addElement($domDocument, $clauseSoc, 'nombreHeure', '1');
                    (new Atexo_Xml())->addElement($domDocument, $clauseSoc, 'pourcentage', '1');
                    foreach ($clauses[$clauseSocIndex]->getClausesN2() as $key => $clauseN2) {
                        if ($clauseN2->getReferentielClauseN2()->getSlug() == ClausesN2::MARCHE_RESERVE) {
                            $clauseSocMarRes = $key;
                        } elseif ($clauseN2->getReferentielClauseN2()->getSlug() == ClausesN2::CONDITION_EXECUTION) {
                            $clauseSocCondExec = $key;
                        } elseif ($clauseN2->getReferentielClauseN2()->getSlug() == ClausesN2::CRITERE_ATTRIBUTION_MARCHE) {
                            $clauseSocCriAtt = $key;
                        } elseif ($clauseN2->getReferentielClauseN2()->getSlug() == ClausesN2::SPECIFICATION_TECHNIQUE) {
                            $clauseSocSpeTech = $key;
                        } elseif ($clauseN2->getReferentielClauseN2()->getSlug() == ClausesN2::INSERTION) {
                            $clauseSocIns = true;
                        }
                    }

                    (new Atexo_Xml())->addElement($domDocument, $clauseSoc, 'marcheReserve', (isset($clauseSocMarRes) ? 'true' : 'false'));
                    (new Atexo_Xml())->addElement($domDocument, $clauseSoc, 'conditionExecution', (isset($clauseSocCondExec) ? 'true' : 'false'));
                    (new Atexo_Xml())->addElement($domDocument, $clauseSoc, 'critereAttribution', (isset($clauseSocCriAtt) ? 'true' : 'false'));
                    (new Atexo_Xml())->addElement($domDocument, $clauseSoc, 'marchePublicObjetEstInsertion', (isset($clauseSocIns) ? 'true' : 'false'));

                    if (isset($clauseSocCondExec)) {
                        $fatherNode = (new Atexo_Xml())->addElement($domDocument, $clauseSoc, 'marchePublicClauseSocialeConditionExecution');
                        foreach ($clauses[$clauseSocIndex]->getClausesN2()[$clauseSocCondExec]->getClausesN3() as $clauseN3) {
                            (new Atexo_Xml())->addElement($domDocument, $fatherNode, 'value', $clauseN3->getReferentielClauseN3()->getSlug());
                        }
                    }
                    if (isset($clauseSocSpeTech)) {
                        $fatherNode = (new Atexo_Xml())->addElement($domDocument, $clauseSoc, 'marchPublicClauseSocialeSpecificationTechnique');
                        foreach ($clauses[$clauseSocIndex]->getClausesN2()[$clauseSocSpeTech]->getClausesN3() as $clauseN3) {
                            (new Atexo_Xml())->addElement($domDocument, $fatherNode, 'value', $clauseN3->getReferentielClauseN3()->getSlug());
                        }
                    }
                    if (isset($clauseSocCriAtt)) {
                        $fatherNode = (new Atexo_Xml())->addElement($domDocument, $clauseSoc, 'marchePublicCritèreSocialCritereAttribution');
                        foreach ($clauses[$clauseSocIndex]->getClausesN2()[$clauseSocCriAtt]->getClausesN3() as $clauseN3) {
                            (new Atexo_Xml())->addElement($domDocument, $fatherNode, 'value', $clauseN3->getReferentielClauseN3()->getSlug());
                        }
                    }
                    if (isset($clauseSocMarRes)) {
                        $fatherNode = (new Atexo_Xml())->addElement($domDocument, $clauseSoc, 'marcheReserveADes');
                        foreach ($clauses[$clauseSocIndex]->getClausesN2()[$clauseSocMarRes]->getClausesN3() as $clauseN3) {
                            if ($clauseN3->getReferentielClauseN3()->getSlug() == ClausesN3::CLAUSE_SOCIALE_RESERVE_ATELIER_PROTEGE) {
                                (new Atexo_Xml())->addAttribute($domDocument, $fatherNode, 'entreprisesAdapteesEtablissementsServicesAide', 'true');
                                (new Atexo_Xml())->addElement($domDocument, $structuresSocialeReserves, 'structureSocialeReserve', 'ESAT_EA');
                            } elseif ($clauseN3->getReferentielClauseN3()->getSlug() == ClausesN3::CLAUSE_SOCIALE_SIAE) {
                                (new Atexo_Xml())->addAttribute($domDocument, $fatherNode, 'StructuresInsertionActiviteEconomique', 'true');
                                (new Atexo_Xml())->addElement($domDocument, $structuresSocialeReserves, 'structureSocialeReserve', 'SIAE');
                            } elseif ($clauseN3->getReferentielClauseN3()->getSlug() == ClausesN3::CLAUSE_SOCIALE_EESS) {
                                (new Atexo_Xml())->addAttribute($domDocument, $fatherNode, 'EntreprisesEconomieSocialeSolidaire', 'true');
                                (new Atexo_Xml())->addElement($domDocument, $structuresSocialeReserves, 'structureSocialeReserve', 'EESS');
                            }
                        }
                    }
                }
            }
        }

        $donneeCmp = $objet->getDonneComplementaire();

        if ($donneeCmp instanceof CommonTDonneeComplementaire) {
            self::initiDurreMarche($domDocument, $tag, $objet);
            $refCCAG = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_CCAG_REFERENCE'), $donneeCmp->getIdCcagReference());
            if ($refCCAG instanceof CommonValeurReferentiel) {
                if (6 != $refCCAG->getId() && 7 != $refCCAG->getId()) {
                    $valueReferenceccagRsem = Atexo_Util::toUtf8($refCCAG->getLibelleValeurReferentiel());
                } else {
                    $valueReferenceccagRsem = self::getValuReferenceCcagRsem($refCCAG->getId());
                }
                (new Atexo_Xml())->addElement($domDocument, $informationsComplementaires, 'ccag', $valueReferenceccagRsem);
            }
            (new Atexo_Xml())->addElement($domDocument, $informationsComplementaires, 'varianteAutorisee', $donneeCmp->getVariantesAutorisees());
            (new Atexo_Xml())->addElement($domDocument, $informationsComplementaires, 'variantesExigees', $donneeCmp->getVarianteExigee());
            (new Atexo_Xml())->addElement($domDocument, $informationsComplementaires, 'variantesTechniquesObligatoires', Atexo_Util::toUtf8($donneeCmp->getVariantesTechniquesDescription()));

            $ccagDpi = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_CCAG_DPI'), $donneeCmp->getIdCcagDpi());
            if ($ccagDpi instanceof CommonValeurReferentiel) {
                (new Atexo_Xml())->addElement($domDocument, $informationsComplementaires, 'droitProprieteIntellectuelle', self::getValuDpiRsem($ccagDpi->getId()));
            }

            if ($donneeCmp->getReconductible()) {
                $marcheReconductible = (new Atexo_Xml())->addElement($domDocument, $informationsComplementaires, 'marcheReconductible');
                (new Atexo_Xml())->addElement($domDocument, $marcheReconductible, 'nombreDeReconduction', $donneeCmp->getNombreReconductions());
                (new Atexo_Xml())->addElement($domDocument, $marcheReconductible, 'modalitesDeReconduction', Atexo_Util::toUtf8($donneeCmp->getModalitesReconduction()));
            }

            if ($donneeCmp->getIdFormePrix()) {
                // forme Prix
                $formeDePrix = (new Atexo_Xml())->addElement($domDocument, $informationsComplementaires, 'formeDePrix');
                $objetFormePrix = $donneeCmp->getFormePrix();
                if ($objetFormePrix instanceof CommonTFormePrix) {
                    self::addFormePrix($objetFormePrix, $domDocument, $formeDePrix);
                }
            } else {
                // Tranches
                $listeTranhces = $donneeCmp->getAllTranches();
                if (is_array($listeTranhces) && count($listeTranhces)) {
                    $tranches = (new Atexo_Xml())->addElement($domDocument, $informationsComplementaires, 'tranches');
                    foreach ($listeTranhces as $oneTranche) {
                        if ($oneTranche->getNatureTranche() == Atexo_Config::getParameter('TRANCHE_FIXE')) {
                            $trancheElement = (new Atexo_Xml())->addElement($domDocument, $tranches, 'trancheFixe');
                        } else {
                            $trancheElement = (new Atexo_Xml())->addElement($domDocument, $tranches, 'tranchesCondionnelle');
                            $identifiant = (new Atexo_Xml())->addElement($domDocument, $trancheElement, 'identifiant', Atexo_Util::toUtf8($oneTranche->getCodeTranche()));
                        }
                        $intitule = (new Atexo_Xml())->addElement($domDocument, $trancheElement, 'intitule', Atexo_Util::toUtf8($oneTranche->getIntituleTranche()));
                        $objetFormePrix = $oneTranche->getFormePrix();
                        if ($objetFormePrix) {
                            $formePrix = (new Atexo_Xml())->addElement($domDocument, $trancheElement, 'formeDePrix');
                            self::addFormePrix($objetFormePrix, $domDocument, $formePrix);
                        }
                    }
                }
            }

            // Lots technique
            $lotsTechnique = (new CommonTLotTechnique())->retrieveLotTechniqueByDonneComplementaire($donneeCmp->getIdDonneeComplementaire());

            if (is_array($lotsTechnique) && count($lotsTechnique)) {
                $lotsTechniqueElement = (new Atexo_Xml())->addElement($domDocument, $informationsComplementaires, 'lotsTechniques');
                foreach ($lotsTechnique as $oneLotTechnique) {
                    $lotTechniqueElement = (new Atexo_Xml())->addElement($domDocument, $lotsTechniqueElement, 'lotTechnique');
                    $identifiant = (new Atexo_Xml())->addElement($domDocument, $lotTechniqueElement, 'identifiant', $oneLotTechnique->getNumeroLot());
                    $intitule = (new Atexo_Xml())->addElement($domDocument, $lotTechniqueElement, 'intitule', Atexo_Util::toUtf8($oneLotTechnique->getIntituleLot()));
                    (new Atexo_Xml())->addElement($domDocument, $lotTechniqueElement, 'lotPrincipal', $oneLotTechnique->getPrincipal());
                    $lotHasTranche = $oneLotTechnique->getAllLotTechniqueTranche();
                    if (is_array($lotHasTranche) && count($lotHasTranche)) {
                        $tranches = (new Atexo_Xml())->addElement($domDocument, $lotTechniqueElement, 'tranches');
                        foreach ($lotHasTranche as $trancheLot) {
                            $objetTranche = CommonTTranchePeer::retrieveByPK($trancheLot->getIdTranche(), $connexion);
                            if ($objetTranche instanceof CommonTTranche) {
                                (new Atexo_Xml())->addElement($domDocument, $tranches, 'codeTranche', Atexo_Util::toUtf8($objetTranche->getCodeTranche()));
                            }
                        }
                    }
                }
            }
        }
    }

    /*
     * Permet d'ajouter l'objet forme prix forfaitaire
     */
    public function addFormePrixForfaitaire($objetFormePrix, $domDocument, $tag, $typeFormePrix)
    {
        $formeDePrixForfaitaire = (new Atexo_Xml())->addElement($domDocument, $tag, $typeFormePrix);
        (new Atexo_Xml())->addElement($domDocument, $formeDePrixForfaitaire, 'estimationInterneHT', $objetFormePrix->getPfEstimationHt());
        (new Atexo_Xml())->addElement($domDocument, $formeDePrixForfaitaire, 'estimationInterneTTC', $objetFormePrix->getPfEstimationTtc());
        $date = Atexo_Util::iso2frnDate($objetFormePrix->getPfDateValeur());
        if ($date) {
            $dateVal = explode('/', $date);
            $dateForme = $dateVal[1].'/'.$dateVal[2];
        }
        (new Atexo_Xml())->addElement($domDocument, $formeDePrixForfaitaire, 'dateDeValeur', $dateForme);
        $listeVariationsPrixForfaitaire = $objetFormePrix->getAllVariationsPrixForfaitaire();
        if (is_array($listeVariationsPrixForfaitaire) && count($listeVariationsPrixForfaitaire)) {
            $variationsPrix = (new Atexo_Xml())->addElement($domDocument, $formeDePrixForfaitaire, 'variationsPrix');
            foreach ($listeVariationsPrixForfaitaire as $variations) {
                $refVariationsPF = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_VARIATION_PRIX'), $variations->getIdVariation());
                if ($refVariationsPF instanceof CommonValeurReferentiel) {
                    (new Atexo_Xml())->addElement($domDocument, $variationsPrix, 'variationsPrix', Atexo_Util::toUtf8($refVariationsPF->getLibelle2()));
                }
            }
        }
    }

    /*
     * Permet d'ajouter l'objet forme prix unitaire
     */
    public function addFormePrixUnitaire($objetFormePrix, $domDocument, $tag, $typeFormePrix)
    {
        $formeDePrixUnitaire = (new Atexo_Xml())->addElement($domDocument, $tag, $typeFormePrix);

        $partieForfaitaire = (new Atexo_Xml())->addElement($domDocument, $formeDePrixUnitaire, 'partieForfaitaire');
        (new Atexo_Xml())->addElement($domDocument, $partieForfaitaire, 'estimationInterneHT', $objetFormePrix->getPuEstimationHt());
        (new Atexo_Xml())->addElement($domDocument, $partieForfaitaire, 'estimationInterneTTC', $objetFormePrix->getPuEstimationTtc());
        $date = Atexo_Util::iso2frnDate($objetFormePrix->getPuDateValeur());
        if ($date) {
            $dateVal = explode('/', $date);
            $dateForme = $dateVal[1].'/'.$dateVal[2];
        }
        (new Atexo_Xml())->addElement($domDocument, $partieForfaitaire, 'dateDeValeur', $dateForme);

        // liste des variations prix unitaire
        $listeVariationsPrixUnitaire = $objetFormePrix->getAllVariationsPrixUnitaire();
        if (is_array($listeVariationsPrixUnitaire) && count($listeVariationsPrixUnitaire)) {
            $variationsPrix = (new Atexo_Xml())->addElement($domDocument, $partieForfaitaire, 'variationsPrix');
            foreach ($listeVariationsPrixUnitaire as $variations) {
                $refVariationsPU = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_VARIATION_PRIX'), $variations->getIdVariation());
                if ($refVariationsPU instanceof CommonValeurReferentiel) {
                    (new Atexo_Xml())->addElement($domDocument, $variationsPrix, 'variationsPrix', Atexo_Util::toUtf8($refVariationsPU->getLibelle2()));
                }
            }
        }

        // Bon de commande
        if ($objetFormePrix->getModalite() == Atexo_Config::getParameter('MODALITE_BON_COMMANDE')) {
            $autresPrixUnitaires = (new Atexo_Xml())->addElement($domDocument, $formeDePrixUnitaire, 'autresPrixUnitaires', 'false');
            $bonDeCommande = (new Atexo_Xml())->addElement($domDocument, $formeDePrixUnitaire, 'bonDeCommande');
            if ($objetFormePrix->getIdMinMax()) {
                $sansMiniMaxi = (new Atexo_Xml())->addElement($domDocument, $bonDeCommande, 'sansMiniMaxi', 'false');
                $avecMiniMaxi = (new Atexo_Xml())->addElement($domDocument, $bonDeCommande, 'avecMiniMaxi');
                (new Atexo_Xml())->addElement($domDocument, $avecMiniMaxi, 'mini', $objetFormePrix->getPuMin());
                (new Atexo_Xml())->addElement($domDocument, $avecMiniMaxi, 'maxi', $objetFormePrix->getPuMax());
            } else {
                $sansMiniMaxi = (new Atexo_Xml())->addElement($domDocument, $bonDeCommande, 'sansMiniMaxi', 'true');
            }
        } elseif ($objetFormePrix->getModalite() == Atexo_Config::getParameter('MODALITE_QUANTITE_DEFINIER')) {
            $autresPrixUnitaires = (new Atexo_Xml())->addElement($domDocument, $formeDePrixUnitaire, 'autresPrixUnitaires', 'true');
        }

        // liste des type prix unitaire
        $listeTypePrixUnitaire = $objetFormePrix->getAllTypesPrix();
        if (is_array($listeTypePrixUnitaire) && count($listeTypePrixUnitaire)) {
            $typePrix = (new Atexo_Xml())->addElement($domDocument, $formeDePrixUnitaire, 'typePrix');
            foreach ($listeTypePrixUnitaire as $type) {
                $refTypePU = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_TYPE_PRIX'), $type->getIdTypePrix());
                if ($refTypePU instanceof CommonValeurReferentiel) {
                    (new Atexo_Xml())->addElement($domDocument, $typePrix, 'typePrix', Atexo_Util::toUtf8($refTypePU->getLibelle2()));
                }
            }
        }
    }

    /*
     * Permet d'ajouter l'objet forme prix
     */
    public function addFormePrix($objetFormePrix, $domDocument, $tag)
    {
        if ($objetFormePrix instanceof CommonTFormePrix) {
            if ($objetFormePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_FORFAITAIRE')) {
                self::addFormePrixForfaitaire($objetFormePrix, $domDocument, $tag, 'formeDePrixForfaitaire');
            } elseif ($objetFormePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_UNITAIRE')) {
                self::addFormePrixUnitaire($objetFormePrix, $domDocument, $tag, 'formeDePrixUnitaire');
            } elseif ($objetFormePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_MIXTE')) {
                $formeDePrixMixte = (new Atexo_Xml())->addElement($domDocument, $tag, 'formeDePrixMixte');
                self::addFormePrixForfaitaire($objetFormePrix, $domDocument, $formeDePrixMixte, 'partieForfaitaire');
                self::addFormePrixUnitaire($objetFormePrix, $domDocument, $formeDePrixMixte, 'partieUnitaire');
            }
        }
    }

    /*
     * Permet de retourner le libelle de l'autorisation de reponse electronique
     * Param : autoriserReponse string (0,1)
     * Param : autoriserReponse string (0,1)
     * return libelle string
     */
    public function getLibelleAutoriserReponse($autoriserReponseElec, $reponseObligatoire)
    {
        if ('0' == $autoriserReponseElec) {
            return 'REFUSEE';
        } elseif (('1' == $autoriserReponseElec) && ('1' == $reponseObligatoire)) {
            return 'OBLIGATOIRE';
        } elseif (('1' == $autoriserReponseElec) && ('0' == $reponseObligatoire)) {
            return 'AUTORISEE';
        } else {
            return null;
        }
    }

    /**
     * Permet de construire l'xml des critères et des sous-critères.
     *
     * @param $domDocument: objet domDocument
     * @param $criteresAttributionElement: correspond à la balise "criteresAttribution"
     * @param $critereTypeElement: prend deux valeurs "listeCritereLibre" et "listeCriterePondere"
     * @param $critere: objet critère
     */
    public function construireXmlCriteresEtSousCriteres($domDocument, $criteresAttributionElement, $critereTypeElement, $critere)
    {
        if ('listeCritereLibre' == $critereTypeElement) {
            $critereLibre = (new Atexo_Xml())->addElement($domDocument, $criteresAttributionElement, 'critereLibre');
            $enonce = (new Atexo_Xml())->addElement($domDocument, $critereLibre, 'enonce', Atexo_Util::toUtf8($critere->getEnonce()));
            self::construireXmlSousCriteres($domDocument, $critereLibre, $critere, $critereTypeElement);
        } elseif ('listeCriterePondere' == $critereTypeElement) {
            $criterePondere = (new Atexo_Xml())->addElement($domDocument, $criteresAttributionElement, 'criterePondere');
            $enonce = (new Atexo_Xml())->addElement($domDocument, $criterePondere, 'enonce', Atexo_Util::toUtf8($critere->getEnonce()));
            (new Atexo_Xml())->addElement($domDocument, $criterePondere, 'pourcentagePonderation', intval($critere->getPonderation()));
            self::construireXmlSousCriteres($domDocument, $criterePondere, $critere, $critereTypeElement);
        }
    }

    /**
     * Permet de construire l'xml des sous critères.
     *
     * @param $domDocument: objet domDocument
     * @param $critereLibre: correspond à la balise "critereLibre"
     * @param $critereTypeElement: prend deux valeurs "listeCritereLibre" et "listeCriterePondere"
     */
    public function construireXmlSousCriteres($domDocument, $critereLibre, $critere, $critereTypeElement)
    {
        $listeSousCriteres = $critere->getSousCriteres();
        if (is_array($listeSousCriteres) && count($listeSousCriteres)) {
            $listeSousCritAttr = (new Atexo_Xml())->addElement($domDocument, $critereLibre, 'listeSousCriteresAttribution');
            foreach ($listeSousCriteres as $sCritere) {
                $sousCriteresAttr = (new Atexo_Xml())->addElement($domDocument, $listeSousCritAttr, 'sousCriteresAttribution');
                $critere = (new Atexo_Xml())->addElement($domDocument, $sousCriteresAttr, 'critere', Atexo_Util::toUtf8($sCritere->getEnonce()));
                if ('listeCriterePondere' == $critereTypeElement) {
                    (new Atexo_Xml())->addElement($domDocument, $sousCriteresAttr, 'pourcentagePonderation', intval($sCritere->getPonderation()));
                }
            }
        }
    }

    /**
     * Permet de construire l'xml des critères d'attribution.
     *
     * @param $objet: instance de "CommonConsultation" ou de "CommonCategorieLot"
     * @param $domDocument
     * @param $tag
     */
    public function construireXmlCriteresAttribution($objet, $domDocument, $tag)
    {
        $donneeCmp = $objet->getDonneComplementaire();
        if ($donneeCmp instanceof CommonTDonneeComplementaire) {
            $criteresAttribution = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_CRITERE_ATTRIBUTION'), $donneeCmp->getIdCritereAttribution());
            if ($criteresAttribution instanceof CommonValeurReferentiel) {
                $criteresAttributionElement = (new Atexo_Xml())->addElement($domDocument, $tag, 'criteresAttribution');
                $allCriteres = $donneeCmp->getAllCriteresAttributions();
                if ('layerDefinitionVide' == $criteresAttribution->getLibelle2()) {
                    $critereElement = (new Atexo_Xml())->addElement($domDocument, $criteresAttributionElement, 'criterePrix', Atexo_Util::toUtf8($criteresAttribution->getLibelleValeurReferentiel()));
                } elseif ('layerDefinitionVide_2' == $criteresAttribution->getLibelle2()) {
                    $critereElement = (new Atexo_Xml())->addElement($domDocument, $criteresAttributionElement, 'critereCDC', Atexo_Util::toUtf8($criteresAttribution->getLibelleValeurReferentiel()));
                } elseif ('layerDefinitionCriteres_1' == $criteresAttribution->getLibelle2()) {
                    $critereTypeElement = 'listeCritereLibre';
                    $listeCriteres = (new Atexo_Xml())->addElement($domDocument, $criteresAttributionElement, 'listeCritereLibre');
                } elseif ('layerDefinitionCriteres_2' == $criteresAttribution->getLibelle2()) {
                    $critereTypeElement = 'listeCriterePondere';
                    $listeCriteres = (new Atexo_Xml())->addElement($domDocument, $criteresAttributionElement, 'listeCriterePondere');
                } elseif ('layerDefinitionVide_3' == $criteresAttribution->getLibelle2()) {
                    $critereTypeElement = 'coutglobal';
                    $listeCriteres = (new Atexo_Xml())->addElement($domDocument, $criteresAttributionElement, 'coutglobal');
                }
                if (is_array($allCriteres)) {
                    foreach ($allCriteres as $oneCritere) {
                        if ('layerDefinitionCriteres_1' == $criteresAttribution->getLibelle2() || 'layerDefinitionCriteres_2' == $criteresAttribution->getLibelle2()) {
                            self::construireXmlCriteresEtSousCriteres($domDocument, $listeCriteres, $critereTypeElement, $oneCritere);
                        }
                    }
                }
            }
        }
    }

    /**
     * Return a Zend client with proxy or without proxy setup.
     *
     * @return Zend_Http_Client
     */
    private function getZendClient()
    {
        $config = [
            'adapter' => Curl::class,
            'curloptions' => [
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_TIMEOUT => Atexo_Config::getParameter('CURLOPT_TIMEOUT'),
            ],
        ];

        if ('' != Atexo_Config::getParameter('URL_PROXY')) {
            $config = array(
                'proxy_host' => Atexo_Config::getParameter('URL_PROXY'),
                'proxy_port' => Atexo_Config::getParameter('PORT_PROXY'),
            );
        }

        return new Client(null, $config);
    }

    /**
     * recupere la valeur externe (RSEM) de la reference ccag.
     *
     * @return string la  valeur externe (RSEM) de la reference ccag
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getValuReferenceCcagRsem($idRefCCAG)
    {
        switch ($idRefCCAG) {
            case 6:
                return 'CCAG-CAC';
            case 7:
                return 'CCAG-NON-PRECISE';
        }
    }

    /**
     * recupere la valeur externe (RSEM) de Droit de propriété intellectuelle.
     *
     * @return string la  valeur externe (RSEM) de Droit de propriété intellectuelle
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getValuDpiRsem($idDpi)
    {
        switch ($idDpi) {
            case 1:
                return 'OPTION-A';
            case 2:
                return 'OPTION-B';
            case 3:
                return 'OPTION-DEROG';
        }
    }

    /**
     * ajoute les lieux execution de la consultation au xml.
     *
     * @param CommonConsultation $consultation
     * @param DomDocument        $domDocument
     * @param DomElement         $tag
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function insertListeLieuxExecution($consultation, $domDocument, $tag)
    {
        if ($consultation instanceof CommonConsultation && $consultation->getLieuExecution()) {
            $lieuxExecutions = (new Atexo_Xml())->addElement($domDocument, $tag, 'lieuExecutions');
            $arraylieuxExecution = explode(',', $consultation->getLieuExecution());
            if (is_array($arraylieuxExecution) && count($arraylieuxExecution)) {
                $denominationPays = Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE');
                $idGeoPays = Atexo_Config::getParameter('IDENTIFIANT_PAYS_GEOLOCALISATION');
                $geoN2s = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arraylieuxExecution);
                foreach ($geoN2s as $geoN2) {
                    if ($geoN2 instanceof CommonGeolocalisationN2) {
                        $lieuInfo = '';
                        if ($geoN2->getIdGeolocalisationn1() != $idGeoPays) {
                            $lieuInfo .= $denominationPays.'-';
                        }
                        $lieuInfo .= $geoN2->getDenomination2();
                        (new Atexo_Xml())->addElement($domDocument, $lieuxExecutions, 'lieuExecution', $lieuInfo);
                    }
                }
            }
        }
    }

    public function initiDurreMarche($domDocument, $tag, $objet)
    {
        $donneeCmp = $objet->getDonneComplementaire();
        if ($donneeCmp instanceof CommonTDonneeComplementaire) {
            $dureeMarche = (new Atexo_Xml())->addElement($domDocument, $tag, 'dureeMarche');
            $refDureeMarche = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_DUREE_DELAI'), $donneeCmp->getIdDureeDelaiDescription());
            if ($refDureeMarche instanceof CommonValeurReferentiel) {
                if ('DUREE_MARCHEE' == $refDureeMarche->getLibelle2()) {
                    $refUniteDureeMarche = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_UNITE_DUREE_MARCHE'), $donneeCmp->getIdChoixMoisJour());
                    if ($refUniteDureeMarche instanceof CommonValeurReferentiel) {
                        if ('dureeMois' == $refUniteDureeMarche->getLibelle2()) {
                            (new Atexo_Xml())->addElement($domDocument, $dureeMarche, 'nbMois', $donneeCmp->getDureeMarche());
                        } elseif ('dureeJours' == $refUniteDureeMarche->getLibelle2()) {
                            (new Atexo_Xml())->addElement($domDocument, $dureeMarche, 'nbJours', $donneeCmp->getDureeMarche());
                        }
                    }
                } elseif ('DELAI_EXECUTION' == $refDureeMarche->getLibelle2()) {
                    (new Atexo_Xml())->addElement($domDocument, $dureeMarche, 'dateACompterDu', Atexo_Util::frnDate2iso($donneeCmp->getDureeDateDebut()));
                    (new Atexo_Xml())->addElement($domDocument, $dureeMarche, 'dateJusquau', Atexo_Util::frnDate2iso($donneeCmp->getDureeDateFin()));
                } elseif ('DESCRIPTION_LIBRE' == $refDureeMarche->getLibelle2()) {
                    (new Atexo_Xml())->addElement($domDocument, $dureeMarche, 'descriptionLibre', Atexo_Util::toUtf8($donneeCmp->getDureeDescription()));
                }
            }
        }
    }
}
