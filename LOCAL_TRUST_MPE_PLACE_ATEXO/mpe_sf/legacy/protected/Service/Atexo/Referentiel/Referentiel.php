<?php

namespace Application\Service\Atexo\Referentiel;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonLtReferentiel;
use Application\Propel\Mpe\CommonLtReferentielPeer;
use Application\Propel\Mpe\CommonReferentielAgentPeer;
use Application\Propel\Mpe\CommonReferentielConsultation;
use Application\Propel\Mpe\CommonReferentielConsultationPeer;
use Application\Propel\Mpe\CommonReferentielEntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use PDO;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Referentiel_Referentiel
{
    public function getAllLtReferentiel()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $AllLtRef = CommonLtReferentielPeer::doSelect($c, $connexionCom);

        return $AllLtRef;
    }

    public function getAllLtReferentielForEntreprise()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonLtReferentielPeer::ENTREPRISE, 1);
        $AllLtRef = CommonLtReferentielPeer::doSelect($c, $connexionCom);

        return $AllLtRef;
    }

    public function getLtReferentielById($id)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonLtReferentielPeer::ID, $id);
        $AllLtRef = CommonLtReferentielPeer::doSelectOne($c, $connexionCom);

        return $AllLtRef;
    }

    public function getLtReferentielByLibelle($codeLibelle)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonLtReferentielPeer::CODE_LIBELLE, $codeLibelle);
        $AllLtRef = CommonLtReferentielPeer::doSelectOne($c, $connexionCom);

        return $AllLtRef;
    }

    public function getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $ltReferentiel, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReferentielConsultationPeer::ORGANISME, $organisme);
        $c->add(CommonReferentielConsultationPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonReferentielConsultationPeer::LOT, $lot);
        $c->add(CommonReferentielConsultationPeer::ID_LT_REFERENTIEL, $ltReferentiel);
        $CommonRefCons = CommonReferentielConsultationPeer::doSelectOne($c, $connexionCom);

        return $CommonRefCons;
    }

    public function getLtReferentielByEntreprise($entreprise, $ltReferentiel)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReferentielEntreprisePeer::ID_ENTREPRISE, $entreprise);
        $c->add(CommonReferentielEntreprisePeer::ID_LT_REFERENTIEL, $ltReferentiel);
        $refEntr = CommonReferentielEntreprisePeer::doSelectOne($c, $connexionCom);

        return $refEntr;
    }

    public static function search($arrayReferentielVo, $champs, $table, $ComplementCritere = '1', $org = '', $withRef = false)
    {
        $select = null;
        $conditionOrg = null;
        if (is_array($arrayReferentielVo) && count($arrayReferentielVo) > 0) {
            $from = ' FROM ';
            $join = '';
            $where = [];
            $hasValue = false;
            $ancienIdRef = 0;
            $refHasValue = [];
            foreach ($arrayReferentielVo as $referentielVo) {
                $idRef = $referentielVo->getId();
                $nomtable = ' LTR'.$idRef;
                if (1 == $referentielVo->getType()) { // lt refrentiel de type texte
                    $valeur = $referentielVo->getValue();
                    $valueMin = (int) $referentielVo->getValuePrinc();
                    $valueMax = (int) $referentielVo->getValueSecon();
                    switch ($referentielVo->getTypeSearch()) {
                        case 0: //recherche seulement du code fourni
                            if (self::rechercheExacte($valeur, $subSql, $nomtable)) {
                                $hasValue = true;
                                $refHasValue[$idRef] = true;
                            }
                            break;
                        case 4: //recherche dans un interval donné
                            if (self::rechercheInterval($valueMin, $valueMax, $subSql, $nomtable)) {
                                $hasValue = true;
                                $refHasValue[$idRef] = true;
                            }
                            break;
                        default: //recherche du code fourni approche
                            if (self::rechercheApproximatif($valeur, $subSql, $nomtable)) {
                                $hasValue = true;
                                $refHasValue[$idRef] = true;
                            }
                            break;
                    }
                } elseif (2 == $referentielVo->getType()) {
                    $valeur = $referentielVo->getValue();
                    if (self::RechrchcheSelected($valeur, $subSql, $nomtable)) {
                        $hasValue = true;
                        $refHasValue[$idRef] = true;
                    }
                } else {
                    $valeurs = explode('#', $referentielVo->getValue());
                    if (count($valeurs) > 0) {
                        switch ($referentielVo->getTypeSearch()) {
                            case 0: //recherche seulement du code fourni
                                foreach ($valeurs as $valeur) {
                                    if (self::rechercheExacte($valeur, $subSql, $nomtable)) {
                                        $hasValue = true;
                                        $refHasValue[$idRef] = true;
                                    }
                                }
                                break;
                            case 1: //recherche du code fourni et ses ascendants.
                                foreach ($valeurs as $valeur) {
                                    if (self::rechercheAscendant($valeur, $subSql, $nomtable)) {
                                        $hasValue = true;
                                        $refHasValue[$idRef] = true;
                                    }
                                }
                                break;
                            case 2: //recherche du code fourni et ses descendants.
                                foreach ($valeurs as $valeur) {
                                    if (self::rechercheDescendant($valeur, $subSql, $nomtable)) {
                                        $hasValue = true;
                                        $refHasValue[$idRef] = true;
                                    }
                                }
                                break;
                            case 3: //recherche du code fourni, ses ascendants et ses descendants.
                                foreach ($valeurs as $valeur) {
                                    $hasValue1 = self::rechercheAscendant($valeur, $subSql, $nomtable);
                                    $hasValue2 = self::rechercheDescendant($valeur, $subSql, $nomtable);
                                    if ($hasValue1 || $hasValue2) {
                                        $hasValue = true;
                                        $refHasValue[$idRef] = true;
                                    }
                                }
                                break;
                        }
                    }
                }
                if (is_array($subSql)) {
                    $subWhere = ' ( '.$ComplementCritere.' AND LTR'.$idRef.".id_Lt_Referentiel='".$idRef."' ";
                    $subWhere .= ' AND ( '.implode(' OR ', $subSql).' ))';
                    $subSql = null;
                }
                if ('' != $subWhere) {
                    $where[] = $subWhere;
                }
            }
            foreach ($refHasValue as $key => $value) {
                if ($value) {
                    $select = 'SELECT distinct(LTR'.$key.'.'.$champs.')';
                    $from .= $table.' LTR'.$key.' ,';
                    if ($org) {
                        $conditionOrg = ' AND LTR'.$key.".organisme='".$org."'";
                    }
                    if ($ancienIdRef) {
                        $join .= ' AND LTR'.$ancienIdRef.'.'.$champs.'=LTR'.$key.'.'.$champs;
                    }
                    $ancienIdRef = $key;
                }
            }
            if ($hasValue) {
                $sql = $select.rtrim($from, ',').' WHERE '.implode(' AND ', $where).$join.$conditionOrg;
                $result = null;
                $statement = Atexo_Db::getLinkCommon(true)->query($sql);
                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $result[] = $row[$champs];
                }

                return $result ?: [' NULL '];
            }//Pour recuperer just les objet qui ont le referentiel $withRef
            elseif ($withRef) {
                $sql = 'SELECT distinct('.$champs.') from '.$table.' , Lt_Referentiel'.
                        ' WHERE '.$table.'.id_Lt_Referentiel = Lt_Referentiel.id'.
                        " AND Lt_Referentiel.code_libelle LIKE '".$withRef."'";
                $result = null;
                $statement = Atexo_Db::getLinkCommon(true)->query($sql);
                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $result[] = $row[$champs];
                }

                return $result ?: [' NULL '];
            }
        }
    }

    public static function searchConsultation($arrayReferentielVo, $org = '')
    {
        return self::search($arrayReferentielVo, 'reference', 'Referentiel_Consultation', '1', $org);
    }

    public function searchEntreprise($arrayReferentielVo)
    {
        return self::search($arrayReferentielVo, 'id_Entreprise', 'Referentiel_Entreprise');
    }

    public static function valeurAscendant($valeur)
    {
        $l = strlen($valeur);
        $j = $l;
        for ($i = $l - 1; $i >= 0; --$i) {
            if (0 != $valeur[$i]) {
                $valeur[$i] = 0;
                break;
            } else {
                --$j;
            }
        }
        if (0 == $j) {
            return '';
        }

        return $valeur;
    }

    public static function rechercheAscendant($valeur, &$req, $nomtable = '')
    {
        if ($valeur && '' != $valeur) {
            $i = -1;
            $hasValue1 = false;
            $hasValue2 = false;
            while (strlen(rtrim($valeur, '0')) >= 1 && '' != $valeur) {
                $subValeur = substr($valeur, 0, $i--);
                $hasValue1 = self::rechercheExacte($subValeur, $req, $nomtable);
                $hasValue2 = self::rechercheExacte($valeur, $req, $nomtable);
                $valeur = self::valeurAscendant($valeur);
            }

            return $hasValue1 || $hasValue2;
        }

        return false;
    }

    public static function rechercheDescendant($valeur, &$req, $nomtable = '')
    {
        if ($valeur && '' != $valeur) {
            $req[] = ' '.$nomtable.".valeur_Principale_Lt_Referentiel  LIKE '".rtrim($valeur, '0')."%' ";
            $req[] = ' '.$nomtable.".valeur_Secondaire_Lt_Referentiel  LIKE '%#".rtrim($valeur, '0')."%' ";

            return true;
        }

        return false;
    }

    public static function rechercheExacte($valeur, &$req, $nomtable = '')
    {
        if ($valeur && '' != $valeur) {
            $req[] = '  '.$nomtable.".valeur_Principale_Lt_Referentiel like '".$valeur."' ";
            $req[] = '  '.$nomtable.".valeur_Secondaire_Lt_Referentiel like '%#".$valeur."#%' ";
            $req[] = '  '.$nomtable.".valeur_Secondaire_Lt_Referentiel like '".$valeur."#%' ";
            $req[] = '  '.$nomtable.".valeur_Secondaire_Lt_Referentiel like '%#".$valeur."' ";

            return true;
        }

        return false;
    }

    /*
     * Permet de faire une recherche approximatif
     */
    public static function rechercheApproximatif($valeur, &$req, $nomtable = '')
    {
        if ($valeur && '' != $valeur) {
            $arrayVal = Atexo_Util::deleteSpecialCharacteresFromChaine($valeur);
            $valeurToSearch = implode('%', $arrayVal);
            $req[] = '  '.$nomtable.".valeur_Principale_Lt_Referentiel like '%".$valeurToSearch."%' ";
            $req[] = '  '.$nomtable.".valeur_Secondaire_Lt_Referentiel like '%".$valeurToSearch."%' ";

            return true;
        }

        return false;
    }

    public function getNomAttribue($libelle)
    {
        return strtolower(str_replace('_', '', $libelle));
    }

    public static function getLibelleForReferentiel($value, $dataRefFile)
    {
        $values = explode('#', $value);
        $res = [];
        if (is_array($values) && $dataRefFile) {
            foreach ($values as $valeur) {
                if ($valeur && '' != $valeur) {
                    $res[] = (new Atexo_Ref())->getLibelleRef($valeur, $dataRefFile);
                }
            }
        }

        return $res;
    }

    public function getLibelleForValeurReferentielEntreprise($idEntreprise, $idLtRef)
    {
        $dataRefFile = null;
        $entrepriseRef = $this->getLtReferentielByEntreprise($idEntreprise, $idLtRef);
        if ($entrepriseRef) {
            $ltRef = self::getLtReferentielById($idLtRef);

            $sessionLang = Atexo_CurrentUser::readFromSession('lang');
            if (isset($sessionLang)) {
                $lang = $sessionLang;
            } else {
                $lang = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
            }
            if ('su' == $lang) {
                $lang = 'sv';
            }
            $atexoRef = new Atexo_Ref();
            $atexoRef->setLocale($lang);
            $atexoRef->setCheminFichierConfigXML($ltRef->getPathConfig());
            $arrayConfigXml = $atexoRef->getArrayConfigXml();
            if ($arrayConfigXml) {
                $dataRefFile = './'.$arrayConfigXml['cheminData'].$arrayConfigXml['fichierDonnee'].$lang.'.csv';
            }

            $value = $entrepriseRef->getValeurPrincipaleLtReferentiel().$entrepriseRef->getValeurSecondaireLtReferentiel();
            $libelles = self::getLibelleForReferentiel($value, $dataRefFile);

            return $libelles;
        }

        return [];
    }

    public function getAllLtReferentielByLotouConsulation($organisme, $consultationId, $lot = null, $consultation = null, $valeurLot = null)
    {
        $arrayReferences = [];
        $sql = 'SELECT Lt_Referentiel.code_libelle,Lt_Referentiel.lot,Lt_Referentiel.type_search,Lt_Referentiel.consultation,Referentiel_Consultation .id,
				Referentiel_Consultation.valeur_Principale_Lt_Referentiel,Referentiel_Consultation.valeur_Secondaire_Lt_Referentiel FROM Lt_Referentiel LEFT JOIN Referentiel_Consultation';
        $sql .= ' ON ( Referentiel_Consultation.id_Lt_Referentiel = Lt_Referentiel.id';
        $sql .= " AND Referentiel_Consultation.organisme = '".trim((new Atexo_Db())->quote($organisme))."'";
        $sql .= " AND Referentiel_Consultation.consultation_id ='".trim((new Atexo_Db())->quote($consultationId))."' )";

        if ($valeurLot) {
            $sql .= " AND Referentiel_Consultation.lot ='".trim((new Atexo_Db())->quote($valeurLot))."'";
        }
        if ($lot) {
            $sql .= " WHERE Lt_Referentiel.lot = '".$lot."'";
        } else {
            $sql .= " WHERE Lt_Referentiel.consultation = '".$consultation."'";
        }
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $arrayReferences[] = $row;
        }

        return $arrayReferences;
    }

    public function getInfoFromReferentielConsultation($codeLibelle, $consultationId, $lot, $organisme, $afficherLibelle = false)
    {
        $ltRef = (new Atexo_Referentiel_Referentiel())->getLtReferentielByLibelle($codeLibelle);
        if ($ltRef) {
            $valeurRef = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $ltRef->getId(), $organisme);
            if ($valeurRef) {
                $value = '';
                if ($afficherLibelle) {
                    $value .= Prado::localize($codeLibelle)."  :\n";
                }
                $value .= $valeurRef->getValeurPrincipaleLtReferentiel();
                if ($valeurRef->getValeurSecondaireLtReferentiel()) {
                    $value .= ', '.$valeurRef->getValeurSecondaireLtReferentiel();
                }

                return $value;
            }
        }

        return '';
    }

    public static function getLtReferentiels($entreprise, $consultation, $lot, $type, $page, $organisme = null, $agent = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();

        if ($entreprise) {
            $c->add(CommonLtReferentielPeer::ENTREPRISE, 1);
        }
        if ($consultation) {
            $c->add(CommonLtReferentielPeer::CONSULTATION, 1);
        }
        if ($lot) {
            $c->add(CommonLtReferentielPeer::LOT, 1);
        }
        if ($agent) {
            $c->add(CommonLtReferentielPeer::AGENT, 1);
        }
        if ($organisme) {
            $cton1 = $c->getNewCriterion(CommonLtReferentielPeer::ORGANISMES, '%#'.$organisme.'#%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(CommonLtReferentielPeer::ORGANISMES, null, Criteria::ISNULL);
            $cton3 = $c->getNewCriterion(CommonLtReferentielPeer::ORGANISMES, '');
            $cton2->addOr($cton3);
            $cton1->addOr($cton2);
            $c->add($cton1);
        }
        if ($page) {
            $c->add(CommonLtReferentielPeer::PAGES, '%#'.$page.'#%', Criteria::LIKE);
        }
        if ('-1' != $type) {
            $c->add(CommonLtReferentielPeer::TYPE, $type);
        }

        //la clé utilisée pour le cache est un hash unique du critere de recherche
        $keyCache = 'cached_critere_ref_'.sha1(serialize($c));
        $ltRefs = Prado::getApplication()->Cache->get($keyCache);

        //si non existant dans le cache on interroge la BD
        if (!$ltRefs) {
            $ltRefs = CommonLtReferentielPeer::doSelect($c, $connexionCom);
            //si pas de résultats, on stocke en cache un tableau avec une unité vide sinon on aura null et le cache APC ne sera jamais utilisé
            //du coup c'est une feinte
            if (0 == count($ltRefs)) {
                $ltRefs = [''];
            }
            Prado::getApplication()->Cache->set($keyCache, $ltRefs, Atexo_Config::getParameter('TTL_PRADO_CACHE'));
        }
        //on supprime les éléments vides si on les précédemment mis
        if (1 == (is_countable($ltRefs) ? count($ltRefs) : 0) && !($ltRefs[0] instanceof CommonLtReferentiel)) {
            unset($ltRefs[array_search('', $ltRefs)]);
        }

        return $ltRefs;
    }

    public function getCommonLtReferentielByIdAgent($idAgent, $ltReferentiel, $organisme = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        //$c->add(CommonReferentielAgentPeer::ORGANISME,$organisme);
        $c->add(CommonReferentielAgentPeer::ID_AGENT, $idAgent);
        $c->add(CommonReferentielAgentPeer::ID_LT_REFERENTIEL, $ltReferentiel);
        $CommonRefCons = CommonReferentielAgentPeer::doSelectOne($c, $connexionCom);

        return $CommonRefCons;
    }

    public function getAllLtReferentielForAgent()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonLtReferentielPeer::AGENT, 1);
        $AllLtRefAgent = CommonLtReferentielPeer::doSelect($c, $connexionCom);

        return $AllLtRefAgent;
    }

    public function searchAgent($arrayReferentielVo, $org = '', $withRef = false)
    {
        if (is_array($arrayReferentielVo) && count($arrayReferentielVo) > 0) {
            return self::search($arrayReferentielVo, 'id_Agent', 'Referentiel_Agent', '1', $org, $withRef);
        }
    }

    /*
     * rechrcher dans un interval entre $valMin et $valMax
     */
    public static function rechercheInterval($valueMin, $valueMax, &$req, $nomtable = '')
    {
        $subrequest = [];
        if (0 != $valueMin) {
            $subrequest[] = '  '.$nomtable.'.valeur_Principale_Lt_Referentiel >= '.$valueMin;
        }
        if (0 != $valueMax) {
            $subrequest[] = '  '.$nomtable.'.valeur_Principale_Lt_Referentiel <= '.$valueMax;
        }
        if (is_countable($subrequest) ? count($subrequest) : 0) {
            $req[] = implode(' AND ', $subrequest);

            return true;
        }

        return false;
    }

    public static function RechrchcheSelected($valeur, &$req, $nomtable = '')
    {
        if (null !== $valeur) {
            $req[] = '  '.$nomtable.".valeur_Principale_Lt_Referentiel like '".$valeur."' ";

            return true;
        }

        return false;
    }

    /**
     * Permet de dupliquer les critères.
     *
     * @param int    $consultationId
     * @param int    $lot
     * @param string $organisme
     *
     * @return CommonReferentielConsultation
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since  2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getLtReferentielByRefConsultationAndLot($consultationId, $lot, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReferentielConsultationPeer::ORGANISME, $organisme);
        $c->add(CommonReferentielConsultationPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonReferentielConsultationPeer::LOT, $lot);

        return CommonReferentielConsultationPeer::doSelect($c, $connexionCom);
    }
}
