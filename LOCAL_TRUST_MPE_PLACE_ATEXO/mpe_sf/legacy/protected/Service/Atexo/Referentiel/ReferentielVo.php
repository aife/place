<?php

namespace Application\Service\Atexo\Referentiel;

/**
 * commentaires.
 *
 * @author amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Referentiel_ReferentielVo
{
    private $id;
    private $value;
    private $valuePrinc;
    private $valueSecon;
    private $codeLibelle;
    private $typeSearch = 0;
    private $type;
    private $oldValue;

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function setValuePrinc($value)
    {
        $this->valuePrinc = $value;
    }

    public function getValuePrinc()
    {
        return $this->valuePrinc;
    }

    public function setValueSecon($value)
    {
        $this->valueSecon = $value;
    }

    public function getValueSecon()
    {
        return $this->valueSecon;
    }

    public function setTypeSearch($typeSearch)
    {
        $this->typeSearch = $typeSearch;
    }

    public function getTypeSearch()
    {
        return $this->typeSearch;
    }

    public function setCodeLibelle($codeLibelle)
    {
        $this->codeLibelle = $codeLibelle;
    }

    public function getCodeLibelle()
    {
        return $this->codeLibelle;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    /*
     * affecte la valeur en param a l'attribut "oldValue"
     * @param  oldValue(string)
     * @author  ASO : <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function setOldValue($oldValue)
    {
        $this->oldValue = $oldValue;
    }

    /*
    * recupere la valeur de l'attribut "oldValue"
    * @return $this->oldValue(string)la valeur de l'attribut "oldValue"
    * @author  ASO : <ayoub.souidahmed@atexo.com>
    * @copyright Atexo 2014
    * @version : 1.0
    * @since : 4.6.0
    */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    public function setValueForReferentielVo($id, $valuePrinc, $valueSecon, $typeSearch, $type = null)
    {
        $this->id = $id;
        $this->valuePrinc = $valuePrinc;
        $this->valueSecon = $valueSecon;
        $this->typeSearch = $typeSearch;
        $this->value = $valuePrinc.$valueSecon;
        if (null != $type) {
            $this->type = $type;
        }
    }
}
