<?php

namespace Application\Service\Atexo;

use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\ConfigurationOrganisme;
use App\Entity\Inscrit;
use App\Service\Agent\HabilitationTypeProcedureService;
use App\Service\ConfigurationOrganismeService;
use App\Service\User;
use Application\Propel\Mpe\CommonServicePeer;
use Prado;
use Prado\Security\IUser;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\String\Slugger\AsciiSlugger;

/**
 * Classe de CurrentUser.
 *
 * @author     Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright  Atexo 2008
 *
 * @version    1.0
 *
 * @since      MPE-3.0
 */
class Atexo_CurrentUser
{
    private static $userSf;
    public const PASSWORD_FORMAT = '/(?=.{8,})((?=.*\d)(?=.*[a-z])(?=.*[A-Z])'
    .'|(?=.*\d)(?=.*[a-zA-Z])(?=.*[\W_])'
    .'|(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_])).*/';
    public const PASSWORD_INIT_VALUE = 'xxxxxxx';

    private static $configurationOrganisme;

    public static function getUserSf(): \App\Entity\Administrateur|\App\Entity\Agent|\App\Entity\Inscrit|bool
    {
        if (!isset(self::$userSf)) {
            /** @var User $userService */
            $userService = Atexo_Util::getSfService(User::class);
            $sfUser = $userService->getUser();

            self::$userSf = is_null($sfUser) ? false : $sfUser;
        }

        return self::$userSf;
    }

    /**
     * Retourne la dénomination de l'organisme courant. S'il n'y a pas d'organisme courant, alors retourne false.
     */
    public static function getOrganismDesignation(): string|false
    {
        return (self::getUserSf()
            && method_exists(self::getUserSf(), 'getOrganisme')
            && self::getUserSf()->getOrganisme())
            ? self::getUserSf()->getOrganisme()->getDenominationOrg()
            : false;
    }

    /**
     * Retourne l'acronyme de l'organisme courant. S'il n'y a pas d'organisme courant, alors retourne false.
     */
    public static function getOrganismAcronym(): string|false
    {
        return (self::getUserSf()
            && method_exists(self::getUserSf(), 'getOrganisme')
            && self::getUserSf()->getOrganisme())
            ? self::getUserSf()->getOrganisme()->getAcronyme()
            : false;
    }

    /**
     * Retourne le nom de l'organisme courant. S'il n'y a pas d'organisme courant, alors retourne false.
     *
     *
     * @deprecated use getOrganismAcronym() instead
     */
    public static function getCurrentOrganism(): string|false
    {
        return self::getOrganismAcronym();
    }

    /**
     * Retourne l'Id de l'utilisateur connecté. S'il n'y a pas , alors retourne false.
     */
    public static function getId(): string|false
    {
        return self::getUserSf() ? self::getUserSf()->getId() : false;
    }

    /**
     * Retourne l'Id de l'agent connecté. S'il n'y a pas , alors retourne false.
     *
     *
     * @deprecated use getId() instead
     */
    public static function getIdAgentConnected(): string|false
    {
        return self::getUserSf() && self::isAgent() ? self::getId() : false;
    }

    public static function getIdEntreprise()
    {
        return self::getUserSf() && self::isEntreprise() ? self::getUserSf()->getEntrepriseId() : false;
    }

    public static function getIdEtablissement()
    {
        return self::getUserSf() && self::isEntreprise() ? self::getUserSf()->getIdEtablissement() : false;
    }

    public static function getIdInscrit()
    {
        return self::getUserSf() && self::isEntreprise() ? self::getId() : false;
    }

    public static function isATES()
    {
        return self::getUserSf()
            && self::isEntreprise()
            && Inscrit::PROFIL_ATES == self::getUserSf()->getProfil();
    }

    /**
     * Retourne le nom du service de l'agent connecté. S'il n'y a pas, alors retourne false.
     *
     * @deprecated use getCurrentServiceId() instead
     */
    public static function getIdServiceAgentConnected(): string|false
    {
        return (self::isAgent()) ? ((self::getUserSf()->getServiceId()) ?? 0) : false;
    }

    public static function getCurrentServiceId(): ?int
    {
        return self::isAgent() ? self::getUserSf()->getServiceId() : null;
    }

    /**
     * Retourne le prenom de l'agent connecté. S'il n'y a pas d'agent connecté, alors retourne false.
     */
    public static function getFirstName(): string|false
    {
        return self::getUserSf() ? self::getUserSf()->getPrenom() : false;
    }

    /**
     * Retourne le prenom de l'agent connecté. S'il n'y a pas d'agent connecté, alors retourne false.
     *
     *
     * @deprecated use getFirstName() instead
     */
    public static function getFirstNameAgentConnected(): string|false
    {
        return self::isAgent() ? self::getFirstName() : false;
    }

    /**
     * Retourne le nom de l'agent connecté. S'il n'y a pas d'agent connecté, alors retourne false.
     */
    public static function getLastName(): string|false
    {
        return self::getUserSf() ? self::getUserSf()->getNom() : false;
    }

    /**
     * Retourne le nom de l'agent connecté. S'il n'y a pas d'agent connecté, alors retourne false.
     *
     *
     * @deprecated use getLastName() instead
     */
    public static function getLastNameAgentConnected(): string|false
    {
        return self::isAgent() ? self::getLastName() : false;
    }

    /**
     * Retourne les modules de l'agent connecté. S'il n'y a pas d'agent connecté, alors retourne false.
     */
    public static function getModules(): array|false
    {
        $user = null;
        if (!isset(self::$configurationOrganisme)) {
            /** @var Agent $user */
            $user = self::getUserSf();

            if (!$user || !self::isAgent()) {
                return false;
            }
        }

        /** @var ConfigurationOrganismeService $service */
        $service = Atexo_Util::getSfService(ConfigurationOrganismeService::class);

        /** @var ConfigurationOrganisme $configurationOrganisme */
        return $service->getConfigurationOrganismeByAcronymeAsArray($user->getOrganisme()->getAcronyme());
    }

    /**
     * Vérifie si l'agent connecté a l'habilitation passée en paramètre.
     *
     * @param string $habilitation : habilitation à tester
     *
     * @return bool true si l'agent a l'habilitation false sinon
     */
    public static function hasHabilitation($habilitation)
    {
        $user = Prado::getApplication()->getUser();

        if (!$user instanceof IUser) {
            return false;
        }

        if (Atexo_Config::getParameter('ACTIVE_HABILITATION_V2')) {
            $habilitationTypeProcedureService = Atexo_Util::getSfService(HabilitationTypeProcedureService::class);
            $slugger = new AsciiSlugger();
            $slug = $slugger->slug($habilitation)->snake()->lower()->toString();
            $agent = self::getUserSf();

            if ($agent instanceof Agent && $habilitationTypeProcedureService->isHabilitationManaged($slug)) {
                return $habilitationTypeProcedureService->checkHabilitation($agent, $slug);
            }
        }

        $roles = array_flip($user->getRoles());

        return (isset($roles[$habilitation]) && $roles[$habilitation] > 0);
    }

    /**
     * Ajoute la valeur $data à la session, identifiée par la clé $key.
     *
     * @param string $key
     * @param mixed  $data
     */
    public static function writeToSession($key, $data, $idElement = null)
    {
        $symfonySession = Atexo_Util::getSfContainer()->get('session');
        $symfonySession->set($key, $data);
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public static function readFromSessionSf(string $key)
    {
        /** @var Session $symfonySession */
        $symfonySession = Atexo_Util::getSfContainer()->get('session');

        if ($key === 'lang') {
            return $symfonySession->get('_locale')
                ? $symfonySession->get('_locale')
                : $symfonySession->get($key);
        }

        return $symfonySession->get($key);
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public static function readFromSession(string $key)
    {
        return self::readFromSessionSf($key);
    }

    /**
     * @param string $key
     *
     * @return boolean
     */
    public static function isSessionSet(string $key)
    {
        $sessionVar = self::readFromSessionSf($key);

        return isset($sessionVar);
    }

    /**
     * Supprime l'element ayant la clé $key de la session.
     *
     * @param string $key
     * @param int $idElement la clé du sous-element
     */
    public static function deleteFromSession($key, $idElement = null)
    {
        $symfonySession = Atexo_Util::getSfContainer()->get('session');

        $symfonySession->remove($key);
    }

    public static function getService()
    {
        $serviceId = self::getCurrentServiceId();
        if (empty($serviceId)) {
            return null;
        }

        return CommonServicePeer::retrieveByPK($serviceId);
    }

    public static function isEntreprise()
    {
        return (false !== self::getUserSf() && Inscrit::class === self::getUserSf()::class);
    }

    public static function isAgent()
    {
        return (false !== self::getUserSf() && Agent::class === self::getUserSf()::class);
    }

    public static function isAdmin()
    {
        return (false !== self::getUserSf() && Administrateur::class === self::getUserSf()::class);
    }

    /**
     * retourne le prenom de l'inscrit authentifié.
     */
    public static function getFirstNameInscrit()
    {
        return Atexo_Util::atexoHtmlEntities(self::getFirstName());
    }

    /**
     * retourne le nom de l'inscrit authentifié.
     */
    public static function getLastNameInscrit()
    {
        return Atexo_Util::atexoHtmlEntities(self::getLastName());
    }

    public static function isConnected()
    {
        /** @var AuthorizationChecker $security */
        $security = Atexo_Util::getSfService('security.authorization_checker');

        return $security->isGranted('IS_AUTHENTICATED_FULLY');
    }

    /**
     * écrit le contenu sérialisé de la session au format attendu par
     * mpe-1.8 dans un fichier.
     */
    public static function dumpSession()
    {
        $sess = [];

        $idAgent = self::getIdAgentConnected();
        $agent = (new Atexo_Agent())->retrieveAgent($idAgent);
        $sess['loginAgent'] = $agent->getLogin();
        $sess['organisme'] = $agent->getOrganisme();

        $sess_string = serialize($sess);

        $id = md5(uniqid(random_int(0, mt_getrandmax()), true));
        $sess_file_name = Atexo_Config::getParameter('PATH_SESSION_SSO') . $id;
        Atexo_Util::write_file($sess_file_name, $sess_string);

        return $id;
    }

    public static function getBloque()
    {
        return self::getUserSf() ? self::getUserSf()->getBloque() : false;
    }

    public static function getEmailInscrit()
    {
        return self::getEmail();
    }

    public static function getRole()
    {
        if (!self::getUserSf()) {
            return;
        }

        $roles = self::getUserSf()->getRoles();

        $arrayMap = [
            'ROLE_AGENT' => 'agent',
            'ROLE_AGENT_HELIOS' => 'agenthelios',
            'ROLE_ENTREPRISE' => 'entreprise',
        ];

        $result = array_intersect(array_keys($arrayMap), $roles);

        return $arrayMap[$result[0]];
    }

    public static function isAgentSocle()
    {
        return self::isAgent()
            && Atexo_Module::isEnabled('SocleInterne');
    }

    /**
     * Recupere l'email de l'agent ou l'inscrit de l'entreprise connecte.
     */
    public static function getEmail(): false|string
    {
        return self::getUserSf() ? self::getUserSf()->getEmail() : false;
    }

    /**
     * Return 0 or 1 value to kknow if the current user is connected throught FC.
     *
     * @return int
     */
    public static function isAuthenticatedWithFc()
    {
        return false;
    }

    /**
     * Recupere le login de l'agent ou l'inscrit connecte.
     */
    public function getLoginUser(): false|string
    {
        return self::getUserSf() ? self::getUserSf()->getLogin() : false;
    }

    /**
     * Affiche la modal d'alert de SIRET ou non!
     * Fonctionne comme un message flash.
     *
     * @return mixed
     */
    public function isSiretAlert()
    {
        return (bool) Atexo_CurrentUser::readFromSession('siretAlert');
    }

    /**
     * Conditionne l'affichage de la modal d'alert de SIRET!
     *
     * @return void
     */
    public function setSiretAlert()
    {
        if (empty(Atexo_CurrentUser::readFromSession('siretAlert'))) {
            $user = Prado::getApplication()->getUser();
            if (!empty(trim($user->getSiren()))) {
                if (false === $user->isSiretValide()) {
                    Atexo_CurrentUser::writeToSession('siretAlert', 'danger');
                } elseif (Atexo_Module::isEnabled('SynchronisationSGMAP')) {
                    if (true === $user->isSaisieManuelle() || empty($user->getEtatAdministratifEtablissement())) {
                        Atexo_CurrentUser::writeToSession('siretAlert', 'ENTREPRISE_SYNCHRO_ETABLISSEMENT_NON_SYNCHRO');
                    } elseif ('F' === $user->getEtatAdministratifEtablissement()) {
                        Atexo_CurrentUser::writeToSession('siretAlert', 'ETABLISSEMENT_FERME');
                    }
                }
            }
        }
    }

    public static function getPassword(): ?string
    {
        return self::getUserSf() ? self::getUserSf()->getPassword() : null;
    }
}
