<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgrementPeer;
use Application\Propel\Mpe\Entreprise;

/* Classe de manipulation des Agrements
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 * @version 1.0
 * @since MPE-3.0
 * @package atexo
 * @subpackage Entreprise
 */
class Atexo_Agrement
{
    /***
     * Afficher tous les Agrément
     *
     * @return la liste de tous les agréments de la BD
     */
    public function getAllAgrement()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $listAgrements = CommonAgrementPeer::doSelect($c, $connexionCom);

        return $listAgrements;
    }

    /****
     * retourne l'objet de l'agrement passé en parametre
     *
     * @param les ids des agrement séléctionné $idsAgrements
     * @return l'objet Agrement
     */
    public static function retrieveAgrementById($idsAgrements)
    {
        unset($idsAgrements[array_search('', $idsAgrements)]);
        if ((is_countable($idsAgrements) ? count($idsAgrements) : 0) > 0) {
            $c = new Criteria();
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c->add(CommonAgrementPeer::ID, $idsAgrements, Criteria::IN);
            $agrements = CommonAgrementPeer::doSelect($c, $connexionCom);
            if ($agrements) {
                return $agrements;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /****
     * Afficher les Agrements dont les ids passé en parametre
     *
     * @param les ids des agremnts $idsAgrements
     * @return les libelles des agrements
     */
    public function retrieveLibelleAgrementsByIds($idsAgrements)
    {
        $libelleAgrements = '';
        $arIds = explode(',', $idsAgrements);

        $agrements = self::retrieveAgrementById($arIds);
        if ($agrements) {
            foreach ($agrements as $oneAgrement) {
                $libelleAgrements .= $oneAgrement->getLibelleTraduit().' , ';
            }
        }
        $libelleAgrements = substr($libelleAgrements, 0, -2);

        return $libelleAgrements;
    }
}
