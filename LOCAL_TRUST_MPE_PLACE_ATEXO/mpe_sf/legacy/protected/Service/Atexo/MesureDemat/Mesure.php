<?php

namespace Application\Service\Atexo\MesureDemat;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use AtexoCrypto\Dto\Enveloppe;
use PDO;

/**
 * Classe de manipulation des tables utilisé pr le module mesure démat.
 *
 * @author BELLAHCEN Nabil <nabil.bellahcen@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_MesureDemat_Mesure
{
    public function getListeOragnismes($organisme)
    {
        $ListeOragnismes = [];
        $sql = 'SELECT id,acronyme,denomination_org  from Organisme ';
        if ($organisme) {
            $sql .= " WHERE acronyme = '".(new Atexo_Db())->quote($organisme)."' ";
        }
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        $i = 0;
        foreach ($statement as $row) {
            $ListeOragnismes[$i]['id'] = $row['id'];
            $ListeOragnismes[$i]['acronyme'] = $row['acronyme'];
            $ListeOragnismes[$i]['denomination_org'] = $row['denomination_org'];
            ++$i;
        }

        return $ListeOragnismes;
    }

    public function getListeServicesByOrganisme($org)
    {
        $listeServices = [];
        $sql = "SELECT id,libelle FROM `Service` WHERE organisme='".$org."'";

        // connexion to base organisme
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        $i = 1;
        $listeServices[0]['id'] = 0;
        foreach ($statement as $row) {
            $listeServices[$i]['id'] = $row['id'];
            $listeServices[$i]['libelle'] = $row['libelle'];
            ++$i;
        }

        return $listeServices;
    }

    // Ensemble C1
    /*
     * consultation appartient à C1 si :
        1- Service associé de C = Entité sélectionné
        2- type d'annonce de C est AAPC
        3- catégorie de C =  catégorie  sélectionné (Travaux, fournisseur, service)
        Rassemblées par type de procédure.
     *
     */
    public function getNbrProcedureAAPC($org, $critere, $arraytags)
    {
        $ListeConsultations = [];
        $sql = 'SELECT count(id) as NbrProcedure,consultation.id_type_procedure FROM `consultation` left join Type_Procedure_Organisme on consultation.id_type_procedure=Type_Procedure_Organisme.id_type_procedure AND consultation.organisme=Type_Procedure_Organisme.organisme ';

        $sql .= "WHERE id_type_avis = 3 AND `consultation`.organisme='".$org."' ";
        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= " AND service_id = '".(new Atexo_Db())->quote($critere->getIdService())."'";
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= ' AND categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
        }
        $sql .= ' GROUP BY `id_type_procedure`';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (isset($ListeConsultations[$arraytags[$row['id_type_procedure']]]['NbrProcedure'])) {
                $ListeConsultations[$arraytags[$row['id_type_procedure']]]['NbrProcedure'] += $row['NbrProcedure'];
            } else {
                $ListeConsultations[$arraytags[$row['id_type_procedure']]]['NbrProcedure'] = $row['NbrProcedure'];
            }
        }

        return $ListeConsultations;
    }

    public function getNbreLotsAAPC($critere, $arraytags)
    {
        $Consultations = [];
        $sql1 = 'Select count(`consultation_id`) as nombreLots, consultation.id_type_procedure '
         .' from CategorieLot, '
         .' consultation left join Type_Procedure_Organisme on consultation.id_type_procedure=Type_Procedure_Organisme.id_type_procedure AND consultation.organisme=Type_Procedure_Organisme.organisme'
         ." WHERE consultation.id=CategorieLot.`consultation_id` AND consultation.organisme=CategorieLot.organisme AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";

        $sql2 = 'Select count(id) as nombreLots, consultation.id_type_procedure '
         .'from consultation left join Type_Procedure_Organisme on consultation.id_type_procedure=Type_Procedure_Organisme.id_type_procedure AND consultation.organisme=Type_Procedure_Organisme.organisme '
         ." where alloti='0' AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";

        $sql = '';
        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (consultation.service_id = '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= ' AND consultation.categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule is not null ';
            }
        }

        $sql .= ' GROUP BY consultation.id_type_procedure ';
        $sql1 .= $sql;
        $sql2 .= $sql;

        $statement = Atexo_Db::getLinkCommon(true)->query($sql1, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (isset($Consultations[$arraytags[$row['id_type_procedure']]]['nombreLots'])) {
                $Consultations[$arraytags[$row['id_type_procedure']]]['nombreLots'] += $row['nombreLots'];
            } else {
                $Consultations[$arraytags[$row['id_type_procedure']]]['nombreLots'] = $row['nombreLots'];
            }
        }

        $statement = Atexo_Db::getLinkCommon(true)->query($sql2, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (isset($Consultations[$arraytags[$row['id_type_procedure']]]['nombreLots'])) {
                $Consultations[$arraytags[$row['id_type_procedure']]]['nombreLots'] += $row['nombreLots'];
            } else {
                $Consultations[$arraytags[$row['id_type_procedure']]]['nombreLots'] = $row['nombreLots'];
            }
        }

        return $Consultations;
    }

    public function getNbrAvisDematAAPC($critere, $arraytags)
    {
        $conditions = null;
        if ($critere) {
            if ($critere->getIdService()) {
                $conditions .= ' AND (consultation.service_id = '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getCategorieConsultation()) {
                $conditions .= ' AND consultation.categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $conditions .= ' AND consultation.date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $conditions .= ' AND consultation.date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $conditions .= ' AND consultation.date_mise_en_ligne_calcule is not null ';
            }
        }
        // Calcul des avis demat Boamp seul (compte pour 1) et Boamp et joue (compte pour 2)
        $sql = ' SELECT distinct consultation.id, consultation.id_type_procedure, id_destination_form_xml '
                 .' FROM consultation , AnnonceBoamp '
                 .' WHERE AnnonceBoamp.consultation_id=consultation.id AND AnnonceBoamp.organisme=consultation.organisme ';
        $sql .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";
        $sql .= $conditions;
        $sql .= " AND statut_destinataire IN ('CP','PU','AP') ";
        $Consultations = [];
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (!isset($Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'])) {
                $Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'] = 0;
            }
            switch ($row['id_destination_form_xml']) {
                case '2':
                    $Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'] += 2; //(2 pour envoi BOAMP et JOUE) si == 2  comptabilisé deux fois
                    break;
                case '1':
                    ++$Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'];
                    break;
            }
        }
        // Calcul des avis demat enoi moniteur
        $sql = ' SELECT count(distinct consultation.id) as nbrCons, consultation.id_type_procedure '
                 .' FROM consultation , AnnonceMoniteur, ReferentielFormXml '
                 .' WHERE AnnonceMoniteur.id_form_xml=ReferentielFormXml.id AND AnnonceMoniteur.organisme=ReferentielFormXml.organisme AND ReferentielFormXml.consultation_id=consultation.id AND ReferentielFormXml.organisme=consultation.organisme ';
        $sql .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";
        $sql .= " AND statut_destinataire IN ('".Atexo_Config::getParameter('STATUT_MONITEUR_PUBLIE')."','".Atexo_Config::getParameter('STATUT_MONITEUR_AVIS_EMIS')."') ";
        $sql .= $conditions;
        $sql .= ' GROUP BY id_type_procedure ';
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (!isset($Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'])) {
                $Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'] = 0;
            }
            $Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'] += $row['nbrCons'];
        }
        // Calcul pour la publication côté entreprise
        $sql = ' SELECT count(distinct id) as nbrCons, consultation.id_type_procedure '
                 .' FROM consultation'
                 ." WHERE id_type_avis = 3 AND organisme='".$critere->getOrganisme()."' ";
        $sql .= $conditions;
        $sql .= ' GROUP BY id_type_procedure ';
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (!isset($Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'])) {
                $Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'] = 0;
            }
            $Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'] += $row['nbrCons'];
        }
        // Calcul des avis demat envoi JAL
        $sql = ' SELECT count(distinct consultation.id) as nbrCons, consultation.id_type_procedure '
                 .' FROM consultation , AnnonceJAL, DestinataireAnnonceJAL  '
                 .' WHERE AnnonceJAL.consultation_id=consultation.id AND AnnonceJAL.organisme=consultation.organisme AND DestinataireAnnonceJAL.idJAL=AnnonceJAL.id AND DestinataireAnnonceJAL.organisme=AnnonceJAL.organisme '
                 ." AND DestinataireAnnonceJAL.statut in ('".Atexo_Config::getParameter('STATUT_PUBLIE_JAL')."')";
        $sql .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";
        $sql .= $conditions;
        $sql .= ' GROUP BY id_type_procedure ';
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (!isset($Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'])) {
                $Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'] = 0;
            }
            $Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'] += $row['nbrCons'];
        }

        return $Consultations;
    }

    public function getTypesMarche()
    {
        $sql = 'SELECT id,libelle from CategorieConsultation ';
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        $TypesMarche = [];
        $i = 0;
        foreach ($statement as $row) {
            $TypesMarche[$i]['id'] = $row['id'];
            $TypesMarche[$i]['libelle'] = $row['libelle'];
            ++$i;
        }

        return $TypesMarche;
    }

    public function getTypesProcedures($org)
    {
        $sql = "SELECT id_type_procedure,libelle_type_procedure from Type_Procedure_Organisme  WHERE organisme='".$org."'";
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        $TypesProcedure = [];
        foreach ($statement as $row) {
            $TypesProcedure[$row['id_type_procedure']]['id'] = $row['id_type_procedure'];
            $TypesProcedure[$row['id_type_procedure']]['libelle_type_procedure'] = $row['libelle_type_procedure'];
        }

        return $TypesProcedure;
    }

    // Début partie DCE -------------------------------------------

    // Calcule le nombre de téléchargements :
    public function getNbrTelechargementsDCE($critere, $arraytags)
    {
        $Telechargements = [];
        $sqlTelecharg = 'SELECT count(consultation_id)as NbrTelechargement,consultation.id_type_procedure FROM Telechargement,consultation ';
        $sqlTelecharg .= 'WHERE Telechargement.consultation_id=consultation.id AND Telechargement.organisme=consultation.organisme ';
        $sqlTelecharg .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";
        if ($critere) {
            if ($critere->getIdService()) {
                $sqlTelecharg .= ' AND consultation.service_id = '.(new Atexo_Db())->quote($critere->getIdService()).'';
            }
            if ($critere->getCategorieConsultation()) {
                $sqlTelecharg .= ' AND consultation.categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sqlTelecharg .= ' AND Telechargement.datetelechargement >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sqlTelecharg .= ' AND Telechargement.datetelechargement <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sqlTelecharg .= ' AND Telechargement.datetelechargement is not null ';
            }
        }

        $sqlTelecharg .= ' GROUP BY id_type_procedure';
        $statement = Atexo_Db::getLinkCommon(true)->query($sqlTelecharg, PDO::FETCH_ASSOC);

        foreach ($statement as $row) {
            if (isset($Telechargements[$arraytags[$row['id_type_procedure']]]['NbrTelechargement'])) {
                $Telechargements[$arraytags[$row['id_type_procedure']]]['NbrTelechargement'] += $row['NbrTelechargement'];
            } else {
                $Telechargements[$arraytags[$row['id_type_procedure']]]['NbrTelechargement'] = $row['NbrTelechargement'];
            }
        }

        return $Telechargements;
    }

    // Calcule le nombre de retraits mixtes
    public function getNbrRetraitsMixteDCE($critere, $arraytags)
    {
        $sqlTelecharg = 'SELECT consultation_id,consultation.tirage_plan,consultation.id_type_procedure FROM Telechargement,consultation ';
        $sqlTelecharg .= 'WHERE Telechargement.consultation_id=consultation.id AND Telechargement.organisme=consultation.organisme';
        $sqlTelecharg .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";
        if ($critere) {
            if ($critere->getIdService()) {
                $sqlTelecharg .= ' AND consultation.service_id = '.(new Atexo_Db())->quote($critere->getIdService()).'';
            }
            if ($critere->getCategorieConsultation()) {
                $sqlTelecharg .= ' AND consultation.categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sqlTelecharg .= ' AND Telechargement.datetelechargement >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sqlTelecharg .= ' AND Telechargement.datetelechargement <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sqlTelecharg .= ' AND Telechargement.datetelechargement is not null ';
            }
        }

        $sqlTelecharg .= ' GROUP BY id_type_procedure';
        $statement = Atexo_Db::getLinkCommon(true)->query($sqlTelecharg, PDO::FETCH_ASSOC);

        $Telechargements = [];
        $NbrRetraitMixte = 0;

        foreach ($statement as $row) {
            if (1 == $row['tirage_plan'] || 2 == $row['tirage_plan']) {
                ++$NbrRetraitMixte;
            } elseif (3 == $row['tirage_plan']) {
                $NbrRetraitMixte += 2;
            }
            if (isset($Telechargements[$arraytags[$row['id_type_procedure']]]['NbrRetrait'])) {
                $Telechargements[$arraytags[$row['id_type_procedure']]]['NbrRetrait'] += $NbrRetraitMixte;
            } else {
                $Telechargements[$arraytags[$row['id_type_procedure']]]['NbrRetrait'] = $NbrRetraitMixte;
            }
        }

        return $Telechargements;
    }

    // Calcule le nombre de retraits papier
    public function getNbrRetraitsPapierDCE($critere, $arraytags)
    {
        $PapierRetrait = [];
        $sqlPapierRetrait = 'SELECT count(consultation_id) as NbrRetraitsPapier,consultation.id_type_procedure FROM Retrait_Papier,consultation ';
        $sqlPapierRetrait .= 'WHERE Retrait_Papier.consultation_id=consultation.id AND Retrait_Papier.organisme=consultation.organisme ';
        $sqlPapierRetrait .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";
        if ($critere) {
            if ($critere->getIdService()) {
                $sqlPapierRetrait .= ' AND (consultation.service_id = '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getCategorieConsultation()) {
                $sqlPapierRetrait .= ' AND consultation.categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sqlPapierRetrait .= ' AND Retrait_Papier.datetelechargement >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sqlPapierRetrait .= ' AND Retrait_Papier.datetelechargement <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sqlPapierRetrait .= ' AND Retrait_Papier.datetelechargement is not null ';
            }
        }

        $sqlPapierRetrait .= ' GROUP BY id_type_procedure ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sqlPapierRetrait, PDO::FETCH_ASSOC);

        foreach ($statement as $row) {
            if (isset($PapierRetrait[$arraytags[$row['id_type_procedure']]]['NbrRetraitsPapier'])) {
                $PapierRetrait[$arraytags[$row['id_type_procedure']]]['NbrRetraitsPapier'] += $row['NbrRetraitsPapier'];
            } else {
                $PapierRetrait[$arraytags[$row['id_type_procedure']]]['NbrRetraitsPapier'] = $row['NbrRetraitsPapier'];
            }
        }

        return $PapierRetrait;
    }

    public function getNbrProcedureDCE($critere, $TypesProcedures, &$Resultat, $arraytags)
    {
        $ResultatProcedures = [];
        // table Telechargement
        $sqlTelecharg = 'SELECT DISTINCT consultation_id ,id_type_procedure FROM Telechargement,consultation ';
        $sqlTelecharg .= 'WHERE Telechargement.consultation_id=consultation.id AND Telechargement.organisme=consultation.organisme';
        $sqlTelecharg .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";
        if ($critere) {
            if ($critere->getIdService()) {
                $sqlTelecharg .= ' AND consultation.service_id = '.(new Atexo_Db())->quote($critere->getIdService()).'';
            }
            if ($critere->getCategorieConsultation()) {
                $sqlTelecharg .= ' AND consultation.categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sqlTelecharg .= ' AND Telechargement.datetelechargement >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sqlTelecharg .= ' AND Telechargement.datetelechargement <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sqlTelecharg .= ' AND Telechargement.datetelechargement is not null ';
            }
        }
        $sqlTelecharg .= ' GROUP BY id_type_procedure';

        // table retrait_papier
        $sqlPapierRetrait = 'SELECT DISTINCT consultation_id ,id_type_procedure  FROM Retrait_Papier,consultation ';
        $sqlPapierRetrait .= 'WHERE Retrait_Papier.consultation_id=consultation.id AND Retrait_Papier.organisme=consultation.organisme ';
        $sqlPapierRetrait .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";
        if ($critere) {
            if ($critere->getIdService()) {
                $sqlPapierRetrait .= ' AND (consultation.service_id = '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getCategorieConsultation()) {
                $sqlPapierRetrait .= ' AND consultation.categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sqlPapierRetrait .= ' AND Retrait_Papier.datetelechargement >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sqlPapierRetrait .= ' AND Retrait_Papier.datetelechargement <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sqlPapierRetrait .= ' AND Retrait_Papier.datetelechargement is not null ';
            }
        }

        $sqlPapierRetrait .= ' GROUP BY id_type_procedure ';
        $sql = $sqlPapierRetrait.' UNION '.$sqlTelecharg; //." UNION ".$sqlSansRetrTel;

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        $i = 0;
        $NbrProcedures = 0;
        $Resultat = [];
        foreach ($statement as $row) {
            $Resultat[$i]['consultation_id'] = $row['consultation_id'];
            $Resultat[$i]['id_type_procedure'] = $row['id_type_procedure'];

            ++$i;
        }

        foreach ($TypesProcedures as $type) {
            $NbrProcedures = 0;
            if ($Resultat) {
                foreach ($Resultat as $res) {
                    if ($type['id'] == $res['id_type_procedure']) {
                        ++$NbrProcedures;
                    }
                }
            }
            if (isset($ResultatProcedures[$arraytags[$type['id']]]['NbrProcedure'])) {
                $ResultatProcedures[$arraytags[$type['id']]]['NbrProcedure'] += $NbrProcedures;
            } else {
                $ResultatProcedures[$arraytags[$type['id']]]['NbrProcedure'] = $NbrProcedures;
            }
        }

        return $ResultatProcedures;
    }

    public function getNbreLots($critere, $ListeIdConsult, $arraytags, $param = null)
    {
        $ArrayNbrLots = [];
        $sql1 = 'Select count(`consultation_id`) as nombreLots, consultation.id_type_procedure '
         .' from CategorieLot, '
         .' consultation left join Type_Procedure_Organisme on consultation.id_type_procedure=Type_Procedure_Organisme.id_type_procedure AND consultation.organisme=Type_Procedure_Organisme.organisme '
         ." WHERE consultation.id=CategorieLot.`consultation_id` AND consultation.organisme=CategorieLot.organisme AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";

        $sql2 = 'Select count(consultation.id) as nombreLots, consultation.id_type_procedure '
         .'from consultation left join Type_Procedure_Organisme on consultation.id_type_procedure=Type_Procedure_Organisme.id_type_procedure AND consultation.organisme=Type_Procedure_Organisme.organisme '
         ." where alloti='0' AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";

        if ($ListeIdConsult) {
            $sql1 .= "AND consultation_id in ('".(new Atexo_Db())->quote($ListeIdConsult[0]['consultation_id'])."'";
            $sql2 .= "AND consultation.id in ('".(new Atexo_Db())->quote($ListeIdConsult[0]['consultation_id'])."'";
            for ($i = 1; $i < (is_countable($ListeIdConsult) ? count($ListeIdConsult) : 0); ++$i) {
                $sql1 .= ",'".(new Atexo_Db())->quote($ListeIdConsult[$i]['consultation_id'])."'";
                $sql2 .= ",'".(new Atexo_Db())->quote($ListeIdConsult[$i]['consultation_id'])."'";
            }
            $sql1 .= ')';
            $sql2 .= ')';
        }

        $sql = '';
        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (consultation.service_id = '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= ' AND consultation.categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule is not null ';
            }
        }

        $sql .= ' GROUP BY consultation.id_type_procedure ';
        $sql1 .= $sql;
        $sql2 .= $sql;

        $statement = Atexo_Db::getLinkCommon(true)->query($sql1, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (isset($ArrayNbrLots[$arraytags[$row['id_type_procedure']]]['nombreLots'])) {
                $ArrayNbrLots[$arraytags[$row['id_type_procedure']]]['nombreLots'] += $row['nombreLots'];
            } else {
                $ArrayNbrLots[$arraytags[$row['id_type_procedure']]]['nombreLots'] = $row['nombreLots'];
            }
        }

        $statement = Atexo_Db::getLinkCommon(true)->query($sql2, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (isset($ArrayNbrLots[$arraytags[$row['id_type_procedure']]]['nombreLots'])) {
                $ArrayNbrLots[$arraytags[$row['id_type_procedure']]]['nombreLots'] += $row['nombreLots'];
            } else {
                $ArrayNbrLots[$arraytags[$row['id_type_procedure']]]['nombreLots'] = $row['nombreLots'];
            }
        }
        if (!$ListeIdConsult && (1 == $param || 2 == $param)) {
            $ArrayNbrLots = [];
        }

        return $ArrayNbrLots;
    }

    // Fin la partie DCE -------------------------------------------

    // Début partie Candidature / offre ------------------------------------

    public function getNbrDematerialiseCand_Offre($critere, $typeEnveloppe, $arraytags)
    {
        $ArrayNbrDematerialise = [];
        $sql = 'SELECT count(consultation_id)as NbrDematerialise,id_type_procedure FROM Enveloppe,Offres,consultation';
        $sql .= ' WHERE Enveloppe.offre_id=Offres.id AND Enveloppe.organisme=Offres.organisme ';
        $sql .= ' AND Offres.consultation_id = consultation.id AND Offres.organisme = consultation.organisme ';

        $sql .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";

        // type candidature
        $sql .= "AND type_env = '".(new Atexo_Db())->quote($typeEnveloppe)."'";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= " AND service_id = '".(new Atexo_Db())->quote($critere->getIdService())."'";
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= ' AND categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND untrusteddate >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND untrusteddate <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND untrusteddate is not null ';
            }
        }
        $sql .= ' GROUP BY id_type_procedure';
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (isset($ArrayNbrDematerialise[$arraytags[$row['id_type_procedure']]]['NbrDematerialise'])) {
                $ArrayNbrDematerialise[$arraytags[$row['id_type_procedure']]]['NbrDematerialise'] += $row['NbrDematerialise'];
            } else {
                $ArrayNbrDematerialise[$arraytags[$row['id_type_procedure']]]['NbrDematerialise'] = $row['NbrDematerialise'];
            }
        }

        return $ArrayNbrDematerialise;
    }

    public function getNbrPapiersCand_Offre($critere, $typeEnveloppe, $arraytags)
    {
        $ArrayNbrPapier = [];
        $sql = 'SELECT count(consultation_id)as nombrePapier,id_type_procedure FROM Enveloppe_papier,Offre_papier,consultation';
        $sql .= ' WHERE Enveloppe_papier.offre_papier_id=Offre_papier.id AND Enveloppe_papier.organisme=Offre_papier.organisme ';
        $sql .= ' AND Offre_papier.consultation_id = consultation.id AND Offre_papier.organisme = consultation.organisme ';

        $sql .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";

        // type d'enveloppe candidature ou offre
        $sql .= "AND type_env = '".(new Atexo_Db())->quote($typeEnveloppe)."'";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= " AND service_id = '".(new Atexo_Db())->quote($critere->getIdService())."'";
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= ' AND categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_depot >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_depot <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_depot is not null ';
            }
        }
        $sql .= ' GROUP BY id_type_procedure';
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (isset($ArrayNbrPapier[$arraytags[$row['id_type_procedure']]]['nombrePapier'])) {
                $ArrayNbrPapier[$arraytags[$row['id_type_procedure']]]['nombrePapier'] += $row['nombrePapier'];
            } else {
                $ArrayNbrPapier[$arraytags[$row['id_type_procedure']]]['nombrePapier'] = $row['nombrePapier'];
            }
        }

        return $ArrayNbrPapier;
    }

    public function getNbrProcedureCand_Offre($critere, $TypesProcedures, $typeEnveloppe, &$ListeIdConsult, $arraytags)
    {
        $ResultatProcedures = [];
        $sqlDematerialise = 'SELECT consultation_id,id_type_procedure FROM Enveloppe,Offres,consultation';
        $sqlDematerialise .= ' WHERE Enveloppe.offre_id=Offres.id AND Enveloppe.organisme=Offres.organisme ';
        $sqlDematerialise .= ' AND Offres.consultation_id = consultation.id AND Offres.organisme = consultation.organisme ';
        $sqlDematerialise .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."'";
        // type d'enveloppe candidature ou offre
        $sqlDematerialise .= "AND type_env = '".(new Atexo_Db())->quote($typeEnveloppe)."'";
        if ($critere) {
            if ($critere->getIdService()) {
                $sqlDematerialise .= " AND service_id = '".(new Atexo_Db())->quote($critere->getIdService())."'";
            }
            if ($critere->getCategorieConsultation()) {
                $sqlDematerialise .= ' AND categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sqlDematerialise .= ' AND untrusteddate >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sqlDematerialise .= ' AND untrusteddate <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sqlDematerialise .= ' AND untrusteddate is not null ';
            }
        }
        $sqlDematerialise .= ' GROUP BY id_type_procedure';

        $sqlPapiers = 'SELECT consultation_id,id_type_procedure FROM Enveloppe_papier,Offre_papier,consultation';
        $sqlPapiers .= ' WHERE Enveloppe_papier.offre_papier_id=Offre_papier.id AND Enveloppe_papier.organisme=Offre_papier.organisme ';
        $sqlPapiers .= ' AND Offre_papier.consultation_id = consultation.id AND Offre_papier.organisme = consultation.organisme';

        $sqlPapiers .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";

        //type candidature
        $sqlPapiers .= "AND type_env = '".(new Atexo_Db())->quote($typeEnveloppe)."'";

        if ($critere) {
            if ($critere->getIdService()) {
                $sqlPapiers .= " AND service_id = '".(new Atexo_Db())->quote($critere->getIdService())."'";
            }
            if ($critere->getCategorieConsultation()) {
                $sqlPapiers .= ' AND categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sqlPapiers .= ' AND date_depot >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sqlPapiers .= ' AND date_depot <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sqlPapiers .= ' AND date_depot is not null ';
            }
        }
        $sqlPapiers .= ' GROUP BY id_type_procedure';

        $sql = $sqlDematerialise.' UNION '.$sqlPapiers;

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        $i = 0;
        $NbrProcedures = 0;
        $ListeIdConsult = [];
        foreach ($statement as $row) {
            $ListeIdConsult[$i]['consultation_id'] = $row['consultation_id'];
            $ListeIdConsult[$i]['id_type_procedure'] = $row['id_type_procedure'];
            ++$i;
        }

        foreach ($TypesProcedures as $type) {
            $NbrProcedures = 0;
            if ($ListeIdConsult) {
                foreach ($ListeIdConsult as $res) {
                    if ($type['id'] == $res['id_type_procedure']) {
                        ++$NbrProcedures;
                    }
                }
            }

            if (isset($ResultatProcedures[$arraytags[$type['id']]]['NbrProcedure'])) {
                $ResultatProcedures[$arraytags[$type['id']]]['NbrProcedure'] += $NbrProcedures;
            } else {
                $ResultatProcedures[$arraytags[$type['id']]]['NbrProcedure'] = $NbrProcedures;
            }
        }

        return $ResultatProcedures;
    }

    // Début partie Notification ------------------------------------
    public function getNbrMarcheNotifieVoieDemat($critere, $arraytags)
    {
        $MarcheNotifie = [];
        $sqlMarcheNotifie = 'SELECT count(consultation_id)as NbrMarche,consultation.id_type_procedure '.
                'FROM Echange,consultation,EchangeDestinataire ';
        $sqlMarcheNotifie .= 'WHERE Echange.consultation_id=consultation.id AND Echange.organisme=consultation.organisme AND EchangeDestinataire.id_echange=Echange.id AND EchangeDestinataire.organisme=Echange.organisme';
        $sqlMarcheNotifie .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";
        $sqlMarcheNotifie .= " AND status = '".Atexo_Config::getParameter('STATUS_ECHANGE_VALIDE')."'";
        $sqlMarcheNotifie .= " AND format =  '".Atexo_Config::getParameter('ECHANGE_PLATE_FORME')."'";
        $sqlMarcheNotifie .= " AND id_type_message =  '".Atexo_Config::getParameter('TYPE_MESSAGE_NOTIFICATION')."'";
        if ($critere) {
            if ($critere->getIdService()) {
                $sqlMarcheNotifie .= ' AND consultation.service_id = '.(new Atexo_Db())->quote($critere->getIdService()).'';
            }
            if ($critere->getCategorieConsultation()) {
                $sqlMarcheNotifie .= ' AND consultation.categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sqlMarcheNotifie .= ' AND Echange.date_message >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sqlMarcheNotifie .= ' AND Echange.date_message <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sqlMarcheNotifie .= ' AND Echange.date_message is not null ';
            }
        }

        $sqlMarcheNotifie .= ' GROUP BY id_type_procedure';

        $statement = Atexo_Db::getLinkCommon(true)->query($sqlMarcheNotifie, PDO::FETCH_ASSOC);

        foreach ($statement as $row) {
            if (isset($MarcheNotifie[$arraytags[$row['id_type_procedure']]]['NbrMarche'])) {
                $MarcheNotifie[$arraytags[$row['id_type_procedure']]]['NbrMarche'] += $row['NbrMarche'];
            } else {
                $MarcheNotifie[$arraytags[$row['id_type_procedure']]]['NbrMarche'] = $row['NbrMarche'];
            }
        }

        return $MarcheNotifie;
    }

    public function getNbrProcedureNotif($critere, $TypesProcedures, &$ProcedureNotif, $arraytags)
    {
        $ResultatProcedures = [];
        $sqlProcedureNotif = 'SELECT consultation_id,consultation.id_type_procedure FROM decisionEnveloppe,consultation ';
        $sqlProcedureNotif .= 'WHERE decisionEnveloppe.consultation_id=consultation.id AND decisionEnveloppe.organisme=consultation.organisme ';

        $sqlProcedureNotif .= " AND id_type_avis = 3 AND consultation.organisme='".$critere->getOrganisme()."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sqlProcedureNotif .= ' AND (consultation.service_id = '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getCategorieConsultation()) {
                $sqlProcedureNotif .= ' AND consultation.categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sqlProcedureNotif .= ' AND decisionEnveloppe.date_notification >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sqlProcedureNotif .= ' AND decisionEnveloppe.date_notification <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sqlProcedureNotif .= ' AND decisionEnveloppe.date_notification is not null ';
            }
        }

        $sqlProcedureNotif .= ' GROUP BY id_type_procedure ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sqlProcedureNotif, PDO::FETCH_ASSOC);
        $i = 0;
        $ProcedureNotif = [];
        foreach ($statement as $row) {
            $ProcedureNotif[$i]['consultation_id'] = $row['consultation_id'];
            $ProcedureNotif[$i]['id_type_procedure'] = $row['id_type_procedure'];
            ++$i;
        }

        foreach ($TypesProcedures as $type) {
            $NbrProcedures = 0;
            if ($ProcedureNotif) {
                foreach ($ProcedureNotif as $res) {
                    if ($type['id'] == $res['id_type_procedure']) {
                        ++$NbrProcedures;
                    }
                }
            }
            if (isset($ResultatProcedures[$arraytags[$type['id']]]['NbrProcedure'])) {
                $ResultatProcedures[$arraytags[$type['id']]]['NbrProcedure'] += $NbrProcedures;
            } else {
                $ResultatProcedures[$arraytags[$type['id']]]['NbrProcedure'] = $NbrProcedures;
            }
        }

        return $ResultatProcedures;
    }

    public function getTagsTypeProcedure($org)
    {
        $listeidProcedureTags = [];
        $sql = "SELECT id_type_procedure,tag_name_mesure_avancement FROM `Type_Procedure_Organisme` WHERE organisme='".$org."'";

        // connexion to base organisme
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            $listeidProcedureTags[$row['id_type_procedure']] = $row['tag_name_mesure_avancement'];
        }

        return $listeidProcedureTags;
    }

    public function getAllTagsMesureTypeProcedure($org)
    {
        $listeTags = [];
        $sql = "SELECT * FROM `Mesure_Type_Procedure` WHERE ORGANISME='".$org."'";
        // connexion to base organisme
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            $listeTags[$row['id_tag_name']] = $row['libelle_tag_name'];
        }

        return $listeTags;
    }
}
