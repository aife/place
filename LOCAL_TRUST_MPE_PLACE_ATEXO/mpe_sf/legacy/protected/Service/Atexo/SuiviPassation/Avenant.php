<?php

namespace Application\Service\Atexo\SuiviPassation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAvenantPeer;
use Application\Propel\Mpe\CommonTypeAvenantPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use PDO;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_SuiviPassation_Avenant
{
    public function retrieveAvenantsByIdContrat($idContrat, $organisme)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAvenantPeer::ID_CONTRAT, $idContrat);
        $c->add(CommonAvenantPeer::ORGANISME, $organisme);
        $avenants = CommonAvenantPeer::doSelect($c, $propelConnection);

        return $avenants;
    }

    public function retrieveAvenantByPk($idAvenant, $organisme)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $avenant = CommonAvenantPeer::retrieveByPK($idAvenant, $organisme, $propelConnection);

        return $avenant;
    }

    public function deleteAvenantById($idAvenant, $organisme)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonAvenantPeer::ID_AVENANT, $idAvenant);
        $c->add(CommonAvenantPeer::ORGANISME, $organisme);
        CommonAvenantPeer::doDelete($c, $propelConnection);
    }

    public function retriveTypesAvenant($organisme, $returnObject = false)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $typeA = CommonTypeAvenantPeer::doSelect($c, $connexion);
        if ($returnObject) {
            return $typeA;
        } elseif (is_array($typeA)) {
            $arrayElements = [];
            foreach ($typeA as $elment) {
                $arrayElements[$elment->getId()] = $elment->getLibelle();
            }

            return $arrayElements;
        } else {
            return false;
        }
    }

    public function retriveTypesAvenantById($idTypeAvenant, $organisme)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeAvenantPeer::ID, $idTypeAvenant);
        $typeA = CommonTypeAvenantPeer::doSelectOne($c, $connexion);
        if ($typeA) {
            return $typeA->getLibelle();
        }
    }

    public function retrieveSuiviPassationAvenants($organisme)
    {
        $sql = 'SELECT '
           .' 	consultation.reference_utilisateur,consultation.id_type_procedure_org,consultation.categorie,decisionEnveloppe.lot ,decisionEnveloppe.objet_marche,decisionEnveloppe.montant_marche,'
           .' 	decisionEnveloppe.id_offre,decisionEnveloppe.type_enveloppe ,decisionEnveloppe.date_notification as dateNotificationDecision,Avenant.*,Passation_consultation.unite,Passation_consultation.mandataire,'
           .'  consultation.id as id_consultation, Passation_consultation.id as id_passation_consultation, decisionEnveloppe.id_decision_enveloppe, Avenant.id_avenant '
           .'FROM '
           .'  consultation ,decisionEnveloppe ,Contrat ,Avenant,Passation_consultation '
           .'WHERE '
           .' consultation.id=decisionEnveloppe.consultation_id  AND decisionEnveloppe.id_decision_enveloppe=Contrat.id_decision AND Avenant.id_contrat=Contrat.id_contrat AND Passation_consultation.consultation_id =consultation.id '
           .' AND consultation.organisme=decisionEnveloppe.organisme  AND decisionEnveloppe.organisme=Contrat.organisme AND Avenant.organisme=Contrat.organisme AND Passation_consultation.organisme =consultation.organisme '
           ." AND consultation.organisme= '".(new Atexo_Db())->quote($organisme)."'";
        $statement = Atexo_Db::getLinkCommon()->query($sql);

        $listeAvenants = [];

        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $avenants = [];

            $avenants['reference_utilisateur'] = $row['reference_utilisateur'];
            $avenants['type_procedure'] = $row['id_type_procedure_org'];
            $avenants['categorie'] = $row['categorie'];
            $avenants['lot'] = $row['lot'];
            $avenants['objet_marche'] = $row['objet_marche'];
            $avenants['montant_marche'] = $row['montant_marche'];
            $avenants['date_notification'] = $row['dateNotificationDecision'];
            //$avenants['nom_entreprise']=$row['nom_entreprise'];
            $avenants['nom_entreprise'] = (new Atexo_Consultation_Responses())->retrieveNomEntrepriseByIdOffre($row['id_offre'], $row['type_enveloppe'], $organisme);
            $avenants['numero_avenant'] = $row['numero_avenant'];
            $avenants['type_avenant'] = $row['type_avenant'];
            $avenants['objet_avenant'] = $row['objet_avenant'];
            $avenants['montant_avenant_ht'] = $row['montant_avenant_ht'];
            $avenants['montant_avenant_ttc'] = $row['montant_avenant_ttc'];
            $avenants['pourcentage_augmentation_marche_initial'] = $row['pourcentage_augmentation_marche_initial'];
            $avenants['pourcentage_augmentation_cumule'] = $row['pourcentage_augmentation_cumule'];
            $avenants['montant_total_marche_tout_avenant_cumule'] = $row['montant_total_marche_tout_avenant_cumule'];
            $avenants['date_reception_projet_par_secretaire_cao'] = $row['date_reception_projet_par_secretaire_cao'];
            $avenants['date_reception_projet_par_charge_etude'] = $row['date_reception_projet_par_charge_etude'];
            $avenants['date_observation_par_sv'] = $row['date_observation_par_sv'];
            $avenants['date_retour_projet'] = $row['date_retour_projet'];
            $avenants['date_validation_projet'] = $row['date_validation_projet'];
            $avenants['date_validation_projet_vu_par'] = $row['date_validation_projet_vu_par'];
            $avenants['date_cao'] = $row['date_cao'];
            $avenants['avis_cao'] = $row['avis_cao'];
            $avenants['date_cp'] = $row['date_cp'];
            $avenants['date_signature_avenant'] = $row['date_signature_avenant'];
            $avenants['date_reception_dossier'] = $row['date_reception_dossier'];
            $avenants['date_formulation_observation_par_sv_sur_sdossier'] = $row['date_formulation_observation_par_sv_sur_sdossier'];
            $avenants['date_retour_dossier_finalise'] = $row['date_retour_dossier_finalise'];
            $avenants['date_validation_dossier_finalise'] = $row['date_validation_dossier_finalise'];
            $avenants['date_transmission_prefecture'] = $row['date_notification'];
            $avenants['operation_validation_vues_par'] = $row['operation_validation_vues_par'];
            $avenants['commentaires'] = $row['commentaires'];
            $avenants['unite'] = $row['unite'];
            $avenants['mandataire'] = $row['mandataire'];
            $avenants['id_consultation'] = $row['id_consultation'];
            $avenants['id_passation_consultation'] = $row['id_passation_consultation'];
            $avenants['id_decision_enveloppe'] = $row['id_decision_enveloppe'];
            $avenants['id_avenant'] = $row['id_avenant'];

            $listeAvenants[] = $avenants;
        }

        return $listeAvenants;
    }

    public function retrieveAllAvenant($organisme = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAvenantPeer::ORGANISME, $organisme);
        $avenant = CommonAvenantPeer::doSelect($c, $propelConnection);

        return $avenant;
    }
}
