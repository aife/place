<?php

namespace Application\Service\Atexo\SuiviPassation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonContrat;
use Application\Propel\Mpe\CommonContratPeer;
use Application\Service\Atexo\Atexo_Config;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_SuiviPassation_Contrat
{
    public function retrieveContratByIdDecision($idDecision, $organisme)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonContratPeer::ID_DECISION, $idDecision);
        $c->add(CommonContratPeer::ORGANISME, $organisme);
        $contrat = CommonContratPeer::doSelectOne($c, $propelConnection);

        return $contrat;
    }

    public function createContrat($idMarche, $idDecision, $organisme)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $contrat = new CommonContrat();
        $contrat->setOrganisme($organisme);
        $contrat->setIdMarche($idMarche);
        $contrat->setIdDecision($idDecision);
        $contrat->save($propelConnection);

        return $contrat;
    }

    public function retrieveContratById($idContrat, $organisme)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $contrat = CommonContratPeer::retrieveByPK($idContrat, $organisme, $propelConnection);

        return $contrat;
    }

    public function retrieveContratByIdPassation($idPassationMarche, $organisme)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonContratPeer::ID_MARCHE, $idPassationMarche);
        $c->add(CommonContratPeer::ORGANISME, $organisme);
        $contrats = CommonContratPeer::doSelect($c, $propelConnection);

        return $contrats;
    }
}
