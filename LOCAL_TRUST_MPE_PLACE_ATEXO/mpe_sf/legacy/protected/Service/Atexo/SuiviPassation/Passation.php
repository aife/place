<?php

namespace Application\Service\Atexo\SuiviPassation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAvisCaoPeer;
use Application\Propel\Mpe\CommonCcagApplicablePeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionPassationConsultationPeer;
use Application\Propel\Mpe\CommonDecisionPassationMarcheAVenirPeer;
use Application\Propel\Mpe\CommonFcspLieuPeer;
use Application\Propel\Mpe\CommonFcspMandatairePeer;
use Application\Propel\Mpe\CommonFcspUnitePeer;
use Application\Propel\Mpe\CommonModeExecutionContratPeer;
use Application\Propel\Mpe\CommonNatureActeJuridiquePeer;
use Application\Propel\Mpe\CommonPassationConsultation;
use Application\Propel\Mpe\CommonPassationConsultationPeer;
use Application\Propel\Mpe\CommonPassationMarcheAVenir;
use Application\Propel\Mpe\CommonPassationMarcheAVenirPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_SuiviPassation_Passation
{
    public function retrieveNatureActeJuridique($organisme, $returnObject = false)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $natureActeJuridiques = CommonNatureActeJuridiquePeer::doSelect($c, $connexion);
        if ($returnObject) {
            return $natureActeJuridiques;
        } elseif (is_array($natureActeJuridiques)) {
            $arrayElements = [];
            $arrayElements[0] = Prado::localize('TEXT_SELECTIONNER').'...';
            foreach ($natureActeJuridiques as $elment) {
                $arrayElements[$elment->getId()] = $elment->getLibelle();
            }

            return $arrayElements;
        } else {
            return false;
        }
    }

    public function retrieveModeExecutionContrat($organisme, $returnObject = false)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $modeExecutionContrats = CommonModeExecutionContratPeer::doSelect($c, $connexion);
        if ($returnObject) {
            return $modeExecutionContrats;
        } elseif (is_array($modeExecutionContrats)) {
            $arrayElements = [];
            $arrayElements[0] = Prado::localize('TEXT_SELECTIONNER').'...';
            foreach ($modeExecutionContrats as $elment) {
                $arrayElements[$elment->getId()] = $elment->getLibelle();
            }

            return $arrayElements;
        } else {
            return false;
        }
    }

    public function retrieveCcagApplicable($organisme, $returnObject = false)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonCcagApplicablePeer::ORGANISME, $organisme);
        $ccagApplicables = CommonCcagApplicablePeer::doSelect($c, $connexion);
        if ($returnObject) {
            return $ccagApplicables;
        } elseif (is_array($ccagApplicables)) {
            $arrayElements = [];
            $arrayElements[0] = Prado::localize('TEXT_SELECTIONNER').'...';
            foreach ($ccagApplicables as $elment) {
                $arrayElements[$elment->getId()] = $elment->getLibelle();
            }

            return $arrayElements;
        } else {
            return false;
        }
    }

    public function retrieveFcspLieu($organisme, $returnObject = false)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonFcspLieuPeer::ORGANISME, $organisme);
        $fcspLieu = CommonFcspLieuPeer::doSelect($c, $connexion);
        if ($returnObject) {
            return $fcspLieu;
        } elseif (is_array($fcspLieu)) {
            $arrayElements = [];
            $arrayElements[0] = Prado::localize('TEXT_SELECTIONNER').'...';
            foreach ($fcspLieu as $elment) {
                $arrayElements[$elment->getId()] = $elment->getLibelle();
            }

            return $arrayElements;
        } else {
            return false;
        }
    }

    public function retrieveFcspUnite($organisme, $returnObject = false)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonFcspUnitePeer::ORGANISME, $organisme);
        $fcspUnite = CommonFcspUnitePeer::doSelect($c, $connexion);
        if ($returnObject) {
            return $fcspUnite;
        } elseif (is_array($fcspUnite)) {
            $arrayElements = [];
            $arrayElements[0] = Prado::localize('TEXT_SELECTIONNER').'...';
            foreach ($fcspUnite as $elment) {
                $arrayElements[$elment->getId()] = $elment->getLibelle();
            }

            return $arrayElements;
        } else {
            return false;
        }
    }

    public function retrieveFcspMandataire($organisme, $returnObject = false)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonFcspMandatairePeer::ORGANISME, $organisme);
        $fcspMandataire = CommonFcspMandatairePeer::doSelect($c, $connexion);
        if ($returnObject) {
            return $fcspMandataire;
        } elseif (is_array($fcspMandataire)) {
            $arrayElements = [];
            $arrayElements[0] = Prado::localize('TEXT_SELECTIONNER').'...';
            foreach ($fcspMandataire as $elment) {
                $arrayElements[$elment->getId()] = $elment->getLibelle();
            }

            return $arrayElements;
        } else {
            return false;
        }
    }

    public function retrievePassationByReferenceConsultation($consultationId, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonPassationConsultationPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonPassationConsultationPeer::ORGANISME, $organisme);
        $passationConsulation = CommonPassationConsultationPeer::doSelectOne($c, $connexion);

        return $passationConsulation;
    }

    public function retrievePassationMarcheAVenirByIdPassationConsultationAndLot($idPassationConsultation, $lot, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonPassationMarcheAVenirPeer::ID_PASSATION_CONSULTATION, $idPassationConsultation);
        $c->add(CommonPassationMarcheAVenirPeer::ORGANISME, $organisme);
        $c->add(CommonPassationMarcheAVenirPeer::LOT, $lot);
        $passationMarcheAVenir = CommonPassationMarcheAVenirPeer::doSelectOne($c, $connexion);

        return $passationMarcheAVenir;
    }

    public function retrievePassationMarcheAVenirByIdPassationConsultation($idPassationConsultation, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonPassationMarcheAVenirPeer::ID_PASSATION_CONSULTATION, $idPassationConsultation);
        $c->add(CommonPassationMarcheAVenirPeer::ORGANISME, $organisme);
        $passationMarcheAVenir = CommonPassationMarcheAVenirPeer::doSelect($c, $connexion);

        return $passationMarcheAVenir;
    }

    public function createEmptyPassationConsultation(CommonConsultation $consultation, $organisme)
    {
        $passationConsultation = new CommonPassationConsultation();
        $passationConsultation->setOrganisme($organisme);

        if ('1' == $consultation->getAlloti()) {
            $lots = $consultation->getAllLots();
            if (is_array($lots)) {
                foreach ($lots as $objLot) {
                    $passationMarcheAVenir = new CommonPassationMarcheAVenir();
                    $passationMarcheAVenir->setOrganisme($organisme);
                    $passationMarcheAVenir->setLot($objLot->getLot());
                    $passationConsultation->addCommonPassationMarcheAVenir($passationMarcheAVenir);
                }
            }
        } else {
            $passationMarcheAVenir = new CommonPassationMarcheAVenir();
            $passationMarcheAVenir->setOrganisme($organisme);
            $passationMarcheAVenir->setLot('0');
            $passationConsultation->addCommonPassationMarcheAVenir($passationMarcheAVenir);
        }
        $passationConsultation->setConsultationId($consultation->getId());
        $propelConnecion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $passationConsultation->save($propelConnecion);

        return $passationConsultation;
    }

    public function retrieveDecisionPassationConsultation($organisme, $returnObject = false)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $decision1 = CommonDecisionPassationConsultationPeer::doSelect($c, $connexion);
        if ($returnObject) {
            return $decision1;
        } elseif (is_array($decision1)) {
            $arrayElements = [];
            foreach ($decision1 as $elment) {
                $arrayElements[$elment->getId()] = $elment->getLibelle();
            }

            return $arrayElements;
        } else {
            return false;
        }
    }

    public function retrieveDecisionPassationMarcheAVenir($organisme, $returnObject = false)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $decision2 = CommonDecisionPassationMarcheAVenirPeer::doSelect($c, $connexion);
        if ($returnObject) {
            return $decision2;
        } elseif (is_array($decision2)) {
            $arrayElements = [];
            foreach ($decision2 as $elment) {
                $arrayElements[$elment->getId()] = $elment->getLibelle();
            }

            return $arrayElements;
        } else {
            return false;
        }
    }

    public function retrieveAvisCao($organisme, $returnObject = false)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $avis = CommonAvisCaoPeer::doSelect($c, $connexion);
        if ($returnObject) {
            return $avis;
        } elseif (is_array($avis)) {
            $arrayElements = [];
            foreach ($avis as $elment) {
                $arrayElements[$elment->getId()] = $elment->getLibelle();
            }

            return $arrayElements;
        } else {
            return false;
        }
    }

    public function retrieveLibelleFcspUnite($idUnite, $organisme)
    {
        $fcspUnite = self::retrieveFcspUnite($organisme);
        if (isset($fcspUnite[$idUnite])) {
            return $fcspUnite[$idUnite];
        }

        return '';
    }

    public function retrieveLibelleFcspMandataire($idMandataire, $organisme)
    {
        $fcspMandataire = self::retrieveFcspMandataire($organisme);
        if (isset($fcspMandataire[$idMandataire])) {
            return $fcspMandataire[$idMandataire];
        }

        return '';
    }

    public function retrieveLibelleNatureActeJuridique($idNatureActeJuridique, $organisme)
    {
        $fcspNatureActeJuridique = self::retrieveNatureActeJuridique($organisme);
        if (isset($fcspNatureActeJuridique[$idNatureActeJuridique])) {
            return $fcspNatureActeJuridique[$idNatureActeJuridique];
        }

        return '';
    }

    public function retrieveLibelleModeExecution($idMode, $organisme)
    {
        $modeExecution = self::retrieveModeExecutionContrat($organisme);
        if (isset($modeExecution[$idMode])) {
            return $modeExecution[$idMode];
        }

        return '';
    }

    public function retrieveLibelleDecision($idDecision, $organisme)
    {
        $decision = self::retrieveDecisionPassationConsultation($organisme);
        if (isset($decision[$idDecision])) {
            return $decision[$idDecision];
        }

        return '';
    }

    public function retrieveLibelleCcag($idCcag, $organisme)
    {
        $ccag = self::retrieveCcagApplicable($organisme);
        if (isset($ccag[$idCcag])) {
            return $ccag[$idCcag];
        }

        return '';
    }

    public function retrieveSuiviPassationInformation($organisme, $reference = null, $lot = null)
    {
        $sql = 'SELECT '

            .'		 consultation.id, consultation.date_mise_en_ligne_calcule, consultation.reference_utilisateur, consultation.id_type_procedure_org, '
            .'		 consultation.categorie, consultation.objet, consultation.lieu_execution , consultation.code_cpv_1, '
            .'		 consultation.alloti, consultation.service_id, consultation.service_associe_id, consultation.datefin, '
            .'		 consultation.clause_sociale as clause_sociale , consultation.clause_environnementale as clause_environnementale, '

            .'		 CategorieLot.lot as numLot, CategorieLot.description, CategorieLot.code_cpv_1 as cpvLot,  '
            .'		 CategorieLot.clause_sociale as clause_sociale_lot , CategorieLot.clause_environnementale as clause_environnementale_lot, '

            .'		 Passation_consultation.id as idPassation, Passation_consultation.unite, Passation_consultation.mandataire, Passation_consultation.numero_deliberation_financiere,  '
            .'		 Passation_consultation.date_deliberation_financiere, Passation_consultation.numero_deliberation_autorisant_signature_marche,  '
            .'		 Passation_consultation.date_deliberation_autorisant_signature_marche, Passation_consultation.date_notification_previsionnelle,'
            .'		 Passation_consultation.date_reception_projet_DCE_Service_Validateur, Passation_consultation.date_formulations_premieres_observations, '
            .'		 Passation_consultation.date_retour_projet_DCE_finalise, Passation_consultation.date_validation_projet_DCE_par_service_validateur, '
            .'		 Passation_consultation.date_validation_projet_DCE_vue_par, Passation_consultation.date_reception_projet_AAPC_par_Service_Validateur, '
            .' 		 Passation_consultation.date_formulations_premieres_observations_AAPC, Passation_consultation.date_retour_projet_AAPC_finalise,'
            .'		 Passation_consultation.date_validation_projet_AAPC_par_Service_Validateur, Passation_consultation.date_validation_projet_AAPC_par_Service_Validateur_vu_par, '
            .'		 Passation_consultation.date_envoi_publicite,Passation_consultation.date_envoi_invitations_remettre_offre,  '
            .'		 Passation_consultation.date_limite_remise_offres, Passation_consultation.delai_validite_offres,  '
            .'		 Passation_consultation.date_reunion_ouverture_candidatures, Passation_consultation.date_reunion_ouverture_offres, '

            .'		 passation_marche_a_venir.id as idMarche,passation_marche_a_venir.lot, passation_marche_a_venir.id_nature_acte_juridique, '
            .'		 passation_marche_a_venir.forme_groupement, passation_marche_a_venir.mode_execution_contrat, '
            .' 		 passation_marche_a_venir.min_bon_commande, passation_marche_a_venir.max_bon_commande, '
            .' 		 passation_marche_a_venir.duree_execution_marche_hors_reconduction, passation_marche_a_venir.nombre_reconduction ,'
            .'		 passation_marche_a_venir.duree_total_marche, passation_marche_a_venir.ccag_applicable,  '
            .'		 passation_marche_a_venir.date_reception_analyse_offre, passation_marche_a_venir.date_formulation_observation_projet_rapport, '
            .'		 passation_marche_a_venir.date_retour_projet_rapport_finalise, passation_marche_a_venir.date_validation_projet_rapport, '
            .'		 passation_marche_a_venir.projet_rapport_vu_par, passation_marche_a_venir.date_reunion_attribution, '
            .'		 passation_marche_a_venir.decision, passation_marche_a_venir.date_envoi_courrier_condidat_non_retenu, '
            .'		 passation_marche_a_venir.date_signature_marche_pa, passation_marche_a_venir.date_reception_controle_legalite, '
            .'		 passation_marche_a_venir.date_formulation_observation_dossier, passation_marche_a_venir.date_retour_dossier_finalise, '
            .'		 passation_marche_a_venir.date_transmission_prefecture, passation_marche_a_venir.dossier_vu_par, '
            .'		 passation_marche_a_venir.date_validation_rapport_information, passation_marche_a_venir.montant_estime, '
            .'		 passation_marche_a_venir.id_passation_consultation '

            .'	FROM '
            .'		 consultation LEFT JOIN  CategorieLot on  (CategorieLot.consultation_id=consultation.id AND CategorieLot.organisme=consultation.organisme) ';
        if ($reference) {
            //Si appellé depuis la fonction Marches Conclus créer (une ligne) csv même si passation n'existe pas

            $sql .= '		 LEFT JOIN  Passation_consultation  ON (consultation.id=Passation_consultation.consultation_id AND consultation.organisme=Passation_consultation.organisme) '
                    .'		 LEFT JOIN passation_marche_a_venir on (passation_marche_a_venir.id_passation_consultation=Passation_consultation.id AND passation_marche_a_venir.organisme=Passation_consultation.organisme) ';
            if (null !== $lot) {
                $sql .= " AND passation_marche_a_venir.lot='".(new Atexo_Db())->quote($lot)."' ";
            }
            $sql .= ' WHERE 1 ';
        } else {
            //Si appellé directement (ref null et lot null) ne mettre dans le csv que le spassations existantes.

            $sql .= '		 , Passation_consultation '
                    .'		 LEFT JOIN passation_marche_a_venir on (passation_marche_a_venir.id_passation_consultation=Passation_consultation.id AND passation_marche_a_venir.organisme=Passation_consultation.organisme) '
                    ."	WHERE consultation.id=Passation_consultation.consultation_id AND consultation.organisme=Passation_consultation.organisme AND (passation_marche_a_venir.lot = CategorieLot.lot OR passation_marche_a_venir.lot='0')";
        }
        if ($reference) {
            $sql .= " AND consultation.id='".(new Atexo_Db())->quote($reference)."' ";
            if ($lot) {
                $sql .= " AND CategorieLot.lot='".(new Atexo_Db())->quote($lot)."'";
            }
        }
        $sql .= " AND consultation.organisme = '".$organisme."'";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);

        return $statement;
    }

    public function retrieveAvisCaoById($idAvisCao, $organisme)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAvisCaoPeer::ID, $idAvisCao);
        $avis = CommonAvisCaoPeer::doSelectOne($c, $connexion);
        if ($avis) {
            return $avis->getLibelle();
        }
    }

    public function retrieveAllPassationConsultation($organisme = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonPassationConsultationPeer::ORGANISME, $organisme);
        $passationConsulation = CommonPassationConsultationPeer::doSelect($c, $connexion);

        return $passationConsulation;
    }

    public function retrieveAllPassationMarcheAVenir($organisme = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonPassationMarcheAVenirPeer::ORGANISME, $organisme);
        $passationMarcheAVenir = CommonPassationMarcheAVenirPeer::doSelect($c, $connexion);

        return $passationMarcheAVenir;
    }
}
