<?php

namespace Application\Service\Atexo;

use App\Traits\ConfigProxyTrait;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use PDO;
use Laminas\Http\Client;
use Laminas\Http\Client\Adapter\Curl;
use Laminas\Http\Request;

/**
 * Classe de gestion de la communication avec lt_mpe_sf.
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 *
 * @version 1.0
 *
 * @since 2016-develop
 *
 * @copyright Atexo 2016
 */
class Atexo_MpeSf
{
    use ConfigProxyTrait;

    /**
     * Permet de recuperer le token d'acces a l'application lt_mpe_sf.
     *
     * @param string $contexte : contexte en json
     *
     * @return mixed
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getToken($contexte)
    {
        $domaine = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE'));
        $url = $domaine.
            str_replace(
                $domaine,
                '',
                Atexo_Config::getParameter('URL_LT_MPE_SF_RECUPERATION_TOKEN')
            );
        $params = '[uri : '.$url."] , [contextes: $contexte]";
        try {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->info('Debut requete http POST pour recuperation du token');
            $logger->info('Params de la requete : '.$params);
            $client = new Client();

            $client->setAdapter(Curl::class);
            $config = [
                'maxredirects' => 0,
                'timeout' => 60,
                'curloptions' => [
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_SSL_VERIFYHOST => false,
                ],
            ];

            $configProxy = self::getConfigProxyForLaminasHttpClient(Atexo_Util::getSfContainer());
            if ($configProxy) {
                $logger->info("L'utilisation du proxy avec les parametres : ".var_export($configProxy, true));
                $config = array_merge($config, $configProxy);
            }

            $client->setOptions($config);
            $client->setUri($url);
            $client->setMethod(Request::METHOD_POST);
            $client->setEncType('application/json');
            $client->setRawBody($contexte);
            $reponse = $client->send()->getBody();
            $token = false;
            if (!empty($reponse)) {
                $decodeReponse = json_decode($reponse, null, 512, JSON_THROW_ON_ERROR);
                if (is_object($decodeReponse)) {
                    if (!empty($decodeReponse->token)) {
                        $token = $decodeReponse->token;
                        $logger->info("Token recupere avec SUCCES: $token");
                    } else {
                        $logger->info('Echec recuperation du token. Details: '.$decodeReponse->error);
                    }
                } else {
                    $logger->error('Impossible de decoder la réponse en json : '.PHP_EOL.$reponse);
                }
            } else {
                $logger->info('Echec recuperation du token. La reponse du ws est vide.');
            }
            $logger->info('Fin requete http POST pour recuperation du token '.$token);

            return $token;
        } catch (\Exception $e) {
            if (!empty($logger)) {
                $logger->error('Erreur lors de la recuperation du token lt_mpe_sf'.PHP_EOL.'Params: '.$params.PHP_EOL.'Exception: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
            }

            return false;
        }
    }

    /**
     * Permet de construire le contexte depuis les informations de la session.
     *
     * @param string $route
     * @param int    $consultationId
     * @param string $orgConsultation
     *
     * @return json
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getContexteFromSession($route, $consultationId, $orgConsultation, $contexteComplementaire = [])
    {
        $urlImageBandeau = '';
        if (Atexo_Config::getParameter('SOURCE_IMAGE_BANDEAU')) {
            $pfUrl = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE'));
            $urlImageBandeau = $pfUrl.Atexo_Config::getParameter('SOURCE_IMAGE_BANDEAU');
            if ($_COOKIE['design']) {
                $urlImageBandeau = str_replace(
                    '/themes/images/',
                    '/themes/'.$_COOKIE['design'].'/images/',
                    $urlImageBandeau
                );
            }
        }
        $logger = Atexo_LoggerManager::getLogger('app');

        $from = Atexo_Config::getParameter('PF_MAIL_FROM');

        if (Atexo_CurrentUser::isConnected()) {
            $contexte = [
                'id' => Atexo_CurrentUser::getId(),
                'session_id' => session_id(),
                'nom' => Atexo_Util::toUtf8(Atexo_CurrentUser::getFirstName()),
                'prenom' => Atexo_Util::toUtf8(Atexo_CurrentUser::getLastName()),
                'reference_consultation' => $consultationId,
                'organisme_consultation' => $orgConsultation,
                'langue' => Atexo_CurrentUser::readFromSession('lang'),
                'route' => $route,
                'paramsPage' => [$consultationId],
                'theme_url' => Atexo_Controller_Front::t(),
                'theme_url_relatif' => Atexo_Controller_Front::t(true),
                'bandeau_image' => $urlImageBandeau,
                'pf_mail_from' => $from,
                'signature_mail' => Atexo_Config::getParameter('SIGNATURE_MAIL'),
                'pf_short_name' => Atexo_Config::getParameter('PF_SHORT_NAME'),
                'pf_long_name' => Atexo_Config::getParameter('PF_LONG_NAME'),
                'signature_pdf_mail' => Atexo_Config::getParameter('SIGNATURE_PDF_MAIL'),
                'signature_pdf_mail_certificat' => Atexo_Config::getParameter('SIGNATURE_PDF_MAIL_CERTIFICAT'),
                'signature_pdf_mail_password' => Atexo_Config::getParameter('SIGNATURE_PDF_MAIL_PASSWORD'),
            ];
            if (Atexo_CurrentUser::isAgent()) {
                $contexte['type_user'] = 'agent';
                $contexte['organisme'] = Atexo_CurrentUser::getOrganismAcronym();
            } else {
                $contexte['type_user'] = 'entreprise';
                $contexte['email'] = Atexo_Util::toUtf8(Atexo_CurrentUser::getEmailInscrit());
                $contexte['entreprise_id'] = Atexo_CurrentUser::getIdEntreprise();
                $contexte['etablissement_id'] = Atexo_CurrentUser::getIdEtablissement();
                $contexte['codeAcces'] = Atexo_Util::toUtf8(($codeAcces = Atexo_CurrentUser::readFromSession('codeAcces')) ? $codeAcces : '');
            }
        } else {
            $contexte = [
                'id' => null,
                'session_id' => session_id(),
                'nom' => '',
                'prenom' => '',
                'reference_consultation' => $consultationId,
                'organisme_consultation' => $orgConsultation,
                'langue' => Atexo_CurrentUser::readFromSession('lang'),
                'route' => $route,
                'paramsPage' => [$consultationId],
                'theme_url' => Atexo_Controller_Front::t(),
                'theme_url_relatif' => Atexo_Controller_Front::t(true),
                'bandeau_image' => $urlImageBandeau,
                'pf_mail_from' => $from,
                'signature_mail' => Atexo_Config::getParameter('SIGNATURE_MAIL'),
                'pf_short_name' => Atexo_Config::getParameter('PF_SHORT_NAME'),
                'pf_long_name' => Atexo_Config::getParameter('PF_LONG_NAME'),
                'signature_pdf_mail' => Atexo_Config::getParameter('SIGNATURE_PDF_MAIL'),
                'signature_pdf_mail_certificat' => Atexo_Config::getParameter('SIGNATURE_PDF_MAIL_CERTIFICAT'),
                'signature_pdf_mail_password' => Atexo_Config::getParameter('SIGNATURE_PDF_MAIL_PASSWORD'),
                'type_user' => 'entreprise',
                'codeAcces' => Atexo_Util::toUtf8(($codeAcces = Atexo_CurrentUser::readFromSession('codeAcces')) ? $codeAcces : ''),
            ];
        }

        $contexte = array_merge($contexte, $contexteComplementaire);
        $logger->info('contexte a encode : '.var_export($contexte, true));

        return json_encode($contexte, JSON_THROW_ON_ERROR);
    }

    /**
     * Permet de construire l'url d'accès depot entreprise à lt_mpe_sf.
     *
     * @param int    $consultationId
     * @param string $orgConsultation
     * @param int    $idCandidatureMps
     * @param int    $typeCandidature
     *
     * @return mixed
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getUrlDepotMpeSf($consultationId, $orgConsultation, $contexteComplementaire = [])
    {
        $urlRedirection = Atexo_Util::getSfContainer()->get('router')->generate(Atexo_Config::getParameter('ROUTE_LT_MPE_SF_DEPOT'), ['id' => $consultationId]);

        return $urlRedirection;
    }

    /**
     * Permet de construire l'url d'accès ouverture agent à lt_mpe_sf.
     *
     * @param int    $consultationId
     * @param string $orgConsultation
     *
     * @return mixed
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getUrlOuvertureMpeSf($consultationId, $orgConsultation)
    {
        $urlRedirection = Atexo_Util::getSfContainer()->get('router')->generate(Atexo_Config::getParameter('ROUTE_LT_MPE_SF_OUVERTURE'), ['id' => $consultationId]);

        return $urlRedirection;
    }

    /**
     * Permet de construire l'url d'accès détail consultation à lt_mpe_sf.
     *
     * @param $consultationId
     * @param $orgConsultation
     * @param array $contexteComplementaire
     *
     * @return mixed
     */
    public static function getUrlDetailConsultationMpeSf($consultationId, $orgConsultation, $contexteComplementaire = [])
    {
        $urlRedirection = Atexo_Util::getSfContainer()->get('router')->generate(Atexo_Config::getParameter('ROUTE_LT_MPE_SF_DETAIL_CONSULTATION'), ['id' => $consultationId]);

        return $urlRedirection;
    }

    /**
     * @param $consultationId
     * @param $orgAcronyme
     *
     * @return string
     *
     * @throws Atexo_Config_Exception
     */
    public static function getUrlDetailsConsultation($consultationId, $extras = '')
    {
        $pfUrl = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('URL_LT_MPE_SF'));
        // Catch l'erreur dans le cas ou le paramètre n'existe pas
        // Try qui rend compatible les anciennes versions
        try {
            if (!array_key_exists('AvisAttribution',$_GET)
                && !array_key_exists('AvisInformation', $_GET)
                && !array_key_exists('AllAnn', $_GET)
            ) {
                $url = $pfUrl.'/entreprise/consultation/'.$consultationId.'?'.$extras;
                if ('/app.php' === Atexo_Config::getParameter('URL_LT_MPE_SF')
                    && '/app.php' !== $pfUrl) {
                    $url = $pfUrl.
                        Atexo_Config::getParameter('URL_LT_MPE_SF').
                        '/entreprise/consultation/'.
                        $consultationId.
                        '?'.
                        $extras;
                }

                return $url;
            }
        } catch (\Exception $e) {
            throw $e;
        }

        return $pfUrl.
            '/?page=Entreprise.EntrepriseDetailsConsultation&id='.
            $consultationId.
            '&'.
            $extras;
    }

    /**
     * @param $reference
     */
    public function consultationsAutorisees($consultationId)
    {

            $consultationsAutorisees = Atexo_CurrentUser::readFromSession('consultationsAutorisees') ?: [];
            if (!in_array($consultationId, $consultationsAutorisees)) {
                $consultationsAutorisees[] = $consultationId;
                Atexo_CurrentUser::writeToSession('consultationsAutorisees', $consultationsAutorisees);
        }
    }

    /**
     * Permet de recuperer l'utilisateur de mpe_sf.
     *
     * @param string $token
     * @param int    $userType
     *
     * @return array
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getUserIdByToken($token, $userType)
    {
        $login = '';
        $mdp = '';
        $table = '';
        $return = [];
        if ($userType == Atexo_Config::getParameter('ACCES_USER_TYPE_ENTREPRISE')) {
            $login = 'login';
            $mdp = 'mdp';
            $table = 'Inscrit';
        } elseif ($userType == Atexo_Config::getParameter('ACCES_USER_TYPE_AGENT')) {
            $login = 'login';
            $mdp = 'password';
            $table = 'Agent';
        }

        if (!empty($login) && !empty($mdp) && !empty($table)) {
            $sql = <<<QUERY
SELECT
    user.$login as login,
    user.$mdp as mdp,
    data
FROM
    t_access_token , $table as user
WHERE
        t_access_token.user_id = user.id
    AND
        t_access_token.user_type = :userType
    AND
        t_access_token.id = :token
QUERY;

            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $statement->execute([':userType' => $userType, ':token' => $token]);
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);

            $return['login'] = $result[0]['login'];
            $return['mdp'] = $result[0]['mdp'];
            $data = json_decode($result[0]['data'], null, 512, JSON_THROW_ON_ERROR);
            $return['langue'] = $data->langue;
        }

        return $return;
    }

    /**
     * @return string
     */
    public function uuid()
    {
        return strtoupper(sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            random_int(0, 0xffff),
            random_int(0, 0xffff),
            // 16 bits for "time_mid"
            random_int(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            random_int(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            random_int(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            random_int(0, 0xffff),
            random_int(0, 0xffff),
            random_int(0, 0xffff)
        ));
    }

    /**
     * @param null $idDossierVolumineux
     */
    public static function addAllowedDossiersVolumineux($idDossierVolumineux = null)
    {
        if (!empty($idDossierVolumineux)) {
            $allowedDossiersVolumineux = Atexo_CurrentUser::readFromSessionSf('allowedDossiersVolumineux');
            if (!is_array($allowedDossiersVolumineux)) {
                $allowedDossiersVolumineux = [];
            }
            if (!in_array($idDossierVolumineux, $allowedDossiersVolumineux)) {
                $allowedDossiersVolumineux[] = $idDossierVolumineux;
            }
            Atexo_CurrentUser::writeToSession('allowedDossiersVolumineux', $allowedDossiersVolumineux);
        }
    }
}
