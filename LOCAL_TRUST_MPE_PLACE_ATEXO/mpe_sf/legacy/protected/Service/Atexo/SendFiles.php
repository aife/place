<?php

namespace Application\Service\Atexo;

use Exception;

/**
 * Classe d'envoi des fichier.
 *
 * @param array $erreursEnvoiFlux : liste des erreurs d'envoi de flux a Chorus
 *
 * @author     Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright  Atexo 2009
 *
 * @version    1.0
 *
 * @since      MPE-3.0
 */
class Atexo_SendFiles
{
    public static function sendFile($file, $url, $authentificationForte = false, $cert = null, $pwdCert = null, $logger = null, &$erreursEnvoiFlux = null)
    {
        $ch = null;
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('app');
        }
        try {
            $messageParametreCurl = "file = $file , url = $url , authentificationForte = $authentificationForte, cert = $cert , pwdCert = $pwdCert";
            $messageInfo = PHP_EOL.PHP_EOL.'Appel methode  Atexo_SendFiles::sendFile avec les parametres : '.$messageParametreCurl.PHP_EOL;
            $logger->info($messageInfo);
            $logger->info("type d'interface : ".php_sapi_name().PHP_EOL.'Serveur hote : '.php_uname('n'));
            $postfields = [];
            $postfields['FileArchive'] = version_compare(PHP_VERSION, '5.5.0', '<') ? '@'.$file : new \CURLFile($file);

            $ch = curl_init();    // initialize curl handle
            curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            if (version_compare(PHP_VERSION, '5.5.0', '>')) {
                curl_setopt($ch, CURLOPT_SAFE_UPLOAD, true);
            }
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Atexo_Config::getParameter('CURLOPT_CONNECTTIMEOUT'));

            curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            if ($authentificationForte) {
                curl_setopt($ch, CURLOPT_SSLCERT, $cert);
                curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pwdCert);

                // Mode insecure pour Chorus et Arcade
                $insecureMode = !($url === Atexo_Config::getParameter('URL_ACCES_CHORUS')
                    || $url === Atexo_Config::getParameter('URL_RECEPTION_ARCADE'));
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $insecureMode);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $insecureMode);
            }

            //definition du proxy si existe
            $urlProxy = Atexo_Config::getParameter('URL_PROXY');
            $portProxy = Atexo_Config::getParameter('PORT_PROXY');
            $curlOptProxy = $urlProxy.':'.$portProxy;
            if ('' != $urlProxy) {
                curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
            }

            if (($result = curl_exec($ch)) === false) {
                $erreurExecCurl = '**** Erreur execution CURL : '.curl_error($ch).'-'.curl_errno($ch).' - Parametres : '.$messageParametreCurl.' ****'.PHP_EOL;
                $logger->error($erreurExecCurl);
                if (null !== $erreursEnvoiFlux) {
                    $erreursEnvoiFlux[] = $erreurExecCurl.PHP_EOL.PHP_EOL.'Infos Execution CURL : '.PHP_EOL.self::getInfosExecution($ch, $logger);
                }
            } // run the whole process

            $logger->info('Resultat execution CURL : '.print_r($result, true));

            //Debug
            self::tracerInfosExecutionLog($ch, $logger);
            curl_close($ch);

            return (bool) (strstr($result, 'ok'));
        } catch (Exception $e) {
            echo PHP_EOL.PHP_EOL;
            self::tracerInfosExecutionLog($ch, $logger);
            $erreur = PHP_EOL.PHP_EOL."Erreur lors de l'envoi vers CHORUS : file = $file , url = $url , cert = $cert , pwdCert = $pwdCert".PHP_EOL.' Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString();
            $logger->error($erreur);
            if (null !== $erreursEnvoiFlux) {
                $erreursEnvoiFlux[] = $erreur.PHP_EOL.PHP_EOL.'Infos Execution CURL : '.PHP_EOL.self::getInfosExecution($ch, $logger);
            }
            echo $erreur;

            return false;
        }
    }

    /**
     * Permet de logger les infos d'execution CURL.
     *
     * @param $ch
     * @param $logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public static function tracerInfosExecutionLog($ch, $logger)
    {
        $logger->info('Infos execution CURL : '.print_r(curl_getinfo($ch), true));
        $logger->info('Numero ERREUR CURL : '.curl_errno($ch));
        $logger->error('Message ERREUR CURL : '.curl_error($ch));
    }

    /**
     * Permet de une chaine d'infos d'execution CURL.
     *
     * @param $ch
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public static function getInfosExecution($ch)
    {
        return 'Numero ERREUR CURL : '.curl_errno($ch).PHP_EOL.'Message ERREUR CURL : '.curl_error($ch).PHP_EOL.'Infos execution CURL : '.print_r(curl_getinfo($ch), true);
    }
}
