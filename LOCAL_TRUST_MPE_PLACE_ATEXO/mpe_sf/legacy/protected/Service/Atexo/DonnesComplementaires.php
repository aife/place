<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTCritereAttribution;
use Application\Propel\Mpe\CommonTCritereAttributionPeer;
use Application\Propel\Mpe\CommonTDonneeComplementairePeer;
use Application\Propel\Mpe\CommonValeurReferentiel;
use Prado\Prado;

/**
 * Classe d'envoi des fichiers vers l'extrene.
 *
 * @author Amal EL BEKKAOUI <khadija.chouika@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_DonnesComplementaires
{
    public function getReunionLotsWhereDateIs($idDonnesComplementaitre, $date, $crittere = 0)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTDonneeComplementairePeer::ID_DONNEE_COMPLEMENTAIRE, $idDonnesComplementaitre, Criteria::EQUAL);
        $c->add(CommonTDonneeComplementairePeer::REUNION, '1');

        if ('-1' == $crittere) {
            $c->add(CommonTDonneeComplementairePeer::DATE_REUNION, $date, Criteria::LESS_THAN);
        } elseif ('0' == $crittere) {
            $c->add(CommonTDonneeComplementairePeer::DATE_REUNION, $date);
        } elseif ('1' == $crittere) {
            $c->add(CommonTDonneeComplementairePeer::DATE_REUNION, $date, Criteria::GREATER_THAN);
        }

        $donnesComplementaitres = CommonTDonneeComplementairePeer::doSelect($c, $connexionCom);
        if ($donnesComplementaitres) {
            return $donnesComplementaitres;
        } else {
            return false;
        }
    }

    public function getEchantillionLotsWhereDateIs($idDonnesComplementaitre, $date, $crittere = 0)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTDonneeComplementairePeer::ID_DONNEE_COMPLEMENTAIRE, $idDonnesComplementaitre, Criteria::EQUAL);
        $c->add(CommonTDonneeComplementairePeer::ECHANTILLON, 1);

        if ('-1' == $crittere) {
            $c->add(CommonTDonneeComplementairePeer::DATE_LIMITE_ECHANTILLON, $date, Criteria::LESS_THAN);
        } elseif ('0' == $crittere) {
            $c->add(CommonTDonneeComplementairePeer::DATE_LIMITE_ECHANTILLON, $date);
        } elseif ('1' == $crittere) {
            $c->add(CommonTDonneeComplementairePeer::DATE_LIMITE_ECHANTILLON, $date, Criteria::GREATER_THAN);
        }

        $donnesComplementaitres = CommonTDonneeComplementairePeer::doSelect($c, $connexionCom);
        if ($donnesComplementaitres) {
            return $donnesComplementaitres;
        } else {
            return false;
        }
    }

    /*
     * Permet de supprimer les critere et les sous criteres d'une donnees complementaire passé en parametre
     */
    public function deleteCritereAttribution(&$donneeComplementaire, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $critereAttributions = $donneeComplementaire->getAllCriteresAttributions();
        if (is_array($critereAttributions)) {
            foreach ($critereAttributions as $oneCritere) {
                if ($oneCritere instanceof CommonTCritereAttribution) {
                    //supprimer les sous critéres
                    $sousCritersToDelete = $oneCritere->getSousCriteres($connexion);
                    foreach ($sousCritersToDelete as $sousCriterToDelete) {
                        $sousCriterToDelete->delete($connexion);
                    }
                }
                CommonTCritereAttributionPeer::doDelete($oneCritere, $connexion);
            }
        }
        $donneeComplementaire->setIdCritereAttribution(null);
    }

    /*
     * Permet de valider les données des criteres d'attribution
     */
    public function validerCritereAttribution($donneeComplementaire, &$isValid, &$arrayMsgErreur, $lot)
    {
        $msg = Prado::localize('TEXT_LOT').' '.$lot.' - ';
        $idCriteresAttribution = $donneeComplementaire->getIdCritereAttribution();
        if (!$idCriteresAttribution) {
            $isValid = false;
            $arrayMsgErreur[] = $msg.Prado::localize('DEFINE_CRITERES_ATTRIBUTION');
        } else {
            $valeurRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_CRITERE_ATTRIBUTION'), $idCriteresAttribution);
            if ($valeurRef instanceof CommonValeurReferentiel) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                if ('layerDefinitionCriteres_2' == $valeurRef->getLibelle2() || 'layerDefinitionCriteres_1' == $valeurRef->getLibelle2()) {
                    $criteresAttribution = (new CommonTCritereAttribution())->retrieveCritereByIdDonneeComplementaire($donneeComplementaire->getIdDonneeComplementaire(), $connexion);
                    if (!$criteresAttribution || (is_array($criteresAttribution) && !count($criteresAttribution))) {
                        $isValid = false;
                        $arrayMsgErreur[] = $msg.Prado::localize('DEFINE_CRITERES_ATTRIBUTION');
                    } elseif ('layerDefinitionCriteres_2' == $valeurRef->getLibelle2()) {
                        $sommePonderation = 0;
                        foreach ($criteresAttribution as $oneCritere) {
                            $ponderation = intval($oneCritere->getPonderation());
                            $sommePonderation += $ponderation;
                            $sousCriteres = $oneCritere->getSousCriteres();
                            $sommeSousCriteres = 0;
                            if (is_array($sousCriteres) && count($sousCriteres)) {
                                foreach ($sousCriteres as $sousCritere) {
                                    $sommeSousCriteres += intval($sousCritere->getPonderation());
                                }
                                if (Atexo_Config::getParameter('SOMME_PONDERATION_SOUS_CRITERE_CENT')) {
                                    $ponderationPere = 100;
                                } else {
                                    $ponderationPere = $ponderation;
                                }
                                if ($ponderationPere != $sommeSousCriteres) {
                                    $isValid = false;
                                    $arrayMsgErreur[] = $msg.Prado::localize('DEFINE_CRITERES_ATTRIBUTION').' - '.Prado::localize('SOMME_PONDERATION_INCORRECTE');
                                }
                            }
                        }
                        if (100 != $sommePonderation) {
                            $isValid = false;
                            $arrayMsgErreur[] = $msg.Prado::localize('DEFINE_CRITERES_ATTRIBUTION').' - '.Prado::localize('DEFINE_SOMME_PONDERATION_EGAL_CENT');
                        }
                    }
                }
            }
        }
    }
}
