<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Application\Service\Atexo;

/**
 * Class MessageStatusCheckerUtil.
 */
class MessageStatusCheckerUtil
{
    public static ?string $entrypointJs = null;

    private static $entrypointCss = null;

    private static int $count = 0;

    private static array $js = [];

    /**
     * recupère les liens des scripts js et css depuis l'entrypoint webpack encore.
     *
     * @param $key
     */
    public static function initEntryPoints($key)
    {
        $entryPointPath = './build/entrypoints.json';
        $entrypointsContent = file_get_contents($entryPointPath);
        $entrypoints = json_decode($entrypointsContent, true, 512, JSON_THROW_ON_ERROR);
        $entrypoints = $entrypoints['entrypoints'];
        if (isset($entrypoints[$key])) {
            if (isset($entrypoints[$key]['js'])) {
                foreach ($entrypoints[$key]['js'] as $js) {
                    if (!in_array($js, self::$js)) {
                        self::$js[] = $js;
                        self::$entrypointJs .= sprintf('<script src="%s"></script>'."\n", $js);
                    }
                }
            }
            if (isset($entrypoints[$key]['css'])) {
                foreach ($entrypoints[$key]['css'] as $css) {
                    if (!strstr($css, 'vendor')) {
                        self::$entrypointCss .= sprintf(' <link rel="stylesheet" href="%s">', $css);
                    }
                }
            }
        }
    }

    /**
     * Retourne l'appel js qui déclenche l'ouverture de l'assistant.
     *
     * @param string $context Chaîne javascript  à renvoyer au navigateur
     *
     * @return string
     */
    public static function getScript(string $context)
    {
        $scriptText = '<script>';
        $scriptText .= "document.getElementById('assistance-launch').setAttribute('data-context', '%s');";
        $scriptText .= "document.getElementById('assistance-launch-link').click();";
        $scriptText .= '</script>';

        return sprintf($scriptText, base64_encode($context));
    }

    /**
     * Recupère la balise script js depuis l'entrypoint webpack encore.
     *
     * @return string
     */
    public static function getEntrypointJs()
    {
        return self::$entrypointJs;
    }

    /**
     * Sauvegarde la balise script js depuis l'entrypoint webpack encore.
     *
     * @param string $entrypointJs balise script du js
     *
     * @return void
     */
    public function setEntrypointJs($entrypointJs)
    {
        self::$entrypointJs = $entrypointJs;
    }

    /**
     * Recupère la balise script js depuis l'entrypoint webpack encore.
     *
     * @return void
     */
    public static function getEntrypointCss()
    {
        return self::$entrypointCss;
    }

    /**
     * Setter de la balise link rel css depuis l'entrypoint webpack encore.
     *
     * @param null $entrypointCss Entypoint css
     *
     * @return void
     */
    public function setEntrypointCss($entrypointCss)
    {
        self::$entrypointCss = $entrypointCss;
    }
}
