<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTTraduction;
use Application\Propel\Mpe\CommonTTraductionPeer;

/**
 * Classe de traduction des libellés.
 *
 * @author KONATE Oumar <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class Atexo_Traduction
{
    /**
     * Permet de traduire un libellé d'identifiant "$id_libelle".
     *
     * @param int    $idElement
     * @param string $langue
     */
    public static function getTraduction($idLibelle, $langue)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $objetTraduction = CommonTTraductionPeer::retrieveByPK($langue, $idLibelle, $connexion);
        if ($objetTraduction instanceof CommonTTraduction) {
            return $objetTraduction->getLibelle();
        } else {
        }
    }

    /**
     * Permet de recupérer l'identifiant "id_libelle" de la dernière traduction inserée.
     */
    public function getLastIdLibelleTraduit()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->addDescendingOrderByColumn(CommonTTraductionPeer::ID_LIBELLE);
        $traduction = CommonTTraductionPeer::doSelectOne($c, $connexion);
        $lastId = 0;
        if ($traduction instanceof CommonTTraduction) {
            $lastId = $traduction->getIdLibelle();
        }

        return $lastId;
    }

    /**
     * Permet de stocker la traduction en base.
     *
     * @param string $idElement
     * @param string $langue
     * @param int    $idLibelle
     */
    public function setTraduction($idLibelle, $langue, $libelle)
    {
        $traduction = null;
        $temp = $idLibelle;
        $logger = Atexo_LoggerManager::getLogger('app');
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        for ($i = 1; $i <= 3; ++$i) {
            try {
                if ($idLibelle && null !== $idLibelle && '' !== $idLibelle) {
                    $traduction = CommonTTraductionPeer::retrieveByPK($langue, $idLibelle, $connexion);
                    $temp = $idLibelle;
                } else {
                    $temp = (int) self::getLastIdLibelleTraduit() + $i;
                }
                if (!($traduction instanceof CommonTTraduction)) {
                    $traduction = new CommonTTraduction();
                }
                $logger->info("setTraduction => Tentative n°$i pour le libelle $libelle et l'id  ".$temp);
                $traduction->setLibelle($libelle);
                $traduction->setLangue($langue);
                $traduction->setIdLibelle($temp);
                $traduction->save($connexion);

                return $traduction->getIdLibelle();
            } catch (\Exception $e) {
                if ($i < 3) {
                    $temp = null;
                    $logger->error("[ECHEC] setTraduction => Tentative n°$i pour le libelle $libelle et l'id  ".$temp.' :'.$e->getMessage());
                } else {
                    throw new PropelException($e->getMessage(), $e);
                }
            }
        }
    }
}
