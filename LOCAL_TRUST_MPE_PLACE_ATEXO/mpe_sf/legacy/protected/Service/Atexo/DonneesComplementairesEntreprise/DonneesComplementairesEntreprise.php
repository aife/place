<?php

namespace Application\Service\Atexo\DonneesComplementairesEntreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCG76Domaine;
use Application\Propel\Mpe\CommonCG76DonneeComplementaireEntreprisePeer;
use Application\Propel\Mpe\CommonCG76PieceJointePeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\DomainesActivites\Atexo_DomainesActivites_Domaines;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use PDO;

/*
 * Created on 5 déc. 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise
{
    /**
     * Fonction qui retourne les données complémentaires de l'entreprise conectée.
     *
     * @param int $idEntreprise Identifiant de l'entreprise
     *
     * @return int
     */
    public function retrieveDonneComplementaireByIdEntreprise($idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonCG76DonneeComplementaireEntreprisePeer::IDENTREPRISE, $idEntreprise);
        $donneesComplementaires = CommonCG76DonneeComplementaireEntreprisePeer::doSelectOne($c, $connexionCom);

        return $donneesComplementaires;
    }

    /**
     * Fonction qui retourne les ids des pièces jointe de l'entreprise conectée.
     *
     * @param int $idEntreprise Identifiant de l'entreprise
     *
     * @return int
     */
    public function retrievePieceJointeByIdEntreprise($idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonCG76PieceJointePeer::IDENTREPRISE, $idEntreprise);
        $pieceJointe = CommonCG76PieceJointePeer::doSelect($c, $connexionCom);

        return $pieceJointe;
    }

    /**
     * Fonction qui supprime les pièces jointe une fois je clique sur le bouton supprimer.
     *
     * @param int $idEntreprise Identifiant de l'entreprise
     *
     * @return void
     */
    public function deletePieceJointeByIdEntreprise($idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonCG76PieceJointePeer::IDENTREPRISE, $idEntreprise);
        CommonCG76PieceJointePeer::doDelete($c, $connexionCom);
    }

    public function entreprisesAyantChoisisDomaine($listeDomaines = null)
    {
        $listeEntreprise = [];
        $sql = 'Select distinct IdEntreprise  from `CG76_Donnee_Complementaire_entreprise`, `CG76_Donnee_Complementaire_Domaine` where '
           .' `CG76_Donnee_Complementaire_entreprise`.`Ref`=`CG76_Donnee_Complementaire_Domaine`.`IdDonneeComlementaire` ';
        if ($listeDomaines) {
            $chaineDomaine = trim(implode(',', $listeDomaines), ',');
            $sql .= ' AND IdDomaine IN ('.(new Atexo_Db())->quote($chaineDomaine).')';
        }

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $listeEntreprise[] = $row['IdEntreprise'];
        }
        if (0 == count($listeEntreprise)) {
            $listeEntreprise = [0];
        }

        return $listeEntreprise;
    }

    public function searchDonneeComplementaire($listeEntreprise, $listDomaines = null)
    {
        $tab = [];
        $donneesComplementairesEntreprise = [];
        $sql = 'SELECT distinct Entreprise.sirenEtranger, Entreprise.nicSiege, Entreprise.acronyme_pays ,Entreprise.nom, Entreprise.id,Entreprise.adresse, Entreprise.adresse2, Entreprise.codepostal,'
           .' Entreprise.villeadresse, Entreprise.paysadresse, Entreprise.telephone, Entreprise.fax, Entreprise.siren,'
           .' Entreprise.formejuridique,Entreprise.codeape,'
           .' CG76_Donnee_Complementaire_entreprise.email, CG76_Donnee_Complementaire_entreprise.type_formation,'
           .' CG76_Donnee_Complementaire_entreprise.cout_moyen_journee, CG76_Donnee_Complementaire_entreprise.collaboration_fpt,'
           .' CG76_Donnee_Complementaire_entreprise.collaboration_fpe,CG76_Donnee_Complementaire_entreprise.centre_documentation,'
           .' CG76_Donnee_Complementaire_entreprise.service_reprographie, CG76_Donnee_Complementaire_entreprise.salle_info,'
           .' CG76_Donnee_Complementaire_entreprise.salle_cours, CG76_Donnee_Complementaire_entreprise.aire_geo_inter, '
           .' CG76_Donnee_Complementaire_entreprise.commentaire , CG76_Domaine.libelle, CG76_Donnee_Complementaire_entreprise.Ref,'
           .' CG76_Domaine.Id, CG76_Domaine.Parent'
           .' FROM Entreprise, CG76_Donnee_Complementaire_entreprise, CG76_Donnee_Complementaire_Domaine, CG76_Domaine'
           .' WHERE Entreprise.id=CG76_Donnee_Complementaire_entreprise.IdEntreprise AND Entreprise.id in('.(new Atexo_Db())->quote($listeEntreprise).')'
           .' AND CG76_Donnee_Complementaire_Domaine.IdDonneeComlementaire=CG76_Donnee_Complementaire_entreprise.Ref'
           .' AND CG76_Domaine.Id=CG76_Donnee_Complementaire_Domaine.IdDomaine';

        if (is_array($listDomaines) && count($listDomaines) > 0) {
            $sql .= ' AND CG76_Domaine.Id IN ('.implode(',', $listDomaines).')';
        }

        //echo $sql;exit;

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $tab['id'] = $row['id'];
            $tab['RaisonSocial'] = $row['nom'];
            $tab['Adresse'] = $row['adresse'];
            $tab['Adresse2'] = $row['adresse2'];
            $tab['CodePostal'] = $row['codepostal'];
            $tab['Ville'] = $row['villeadresse'];
            $tab['Pays'] = $row['paysadresse'];
            $tab['Tel'] = $row['telephone'];
            $tab['Fax'] = $row['fax'];
            if ($row['acronyme_pays'] == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')) {
                $tab['Siret'] = $row['siren'].$row['nicSiege'];
            } else {
                $tab['Siret'] = $row['sirenEtranger'];
            }
            $tab['FormationJuridique'] = $row['formejuridique'];
            $tab['CodeApe'] = $row['codeape'];
            $tab['Email'] = $row['email'];
            $tab['TypeFormation'] = $row['type_formation'];
            $tab['CoutMoyen'] = $row['cout_moyen_journee'];
            $tab['Fpt'] = $row['collaboration_fpt'];
            $tab['Fpe'] = $row['collaboration_fpe'];
            $tab['CentreDoc'] = $row['centre_documentation'];
            $tab['ServiceRepro'] = $row['service_reprographie'];
            $tab['SalleInfo'] = $row['salle_info'];
            $tab['SalleCours'] = $row['salle_cours'];
            $tab['AireGeoInter'] = $row['aire_geo_inter'];
            $tab['Commentaire'] = $row['commentaire'];

            if (0 == $row['Parent']) {
                $tab['Domaine'] = $row['libelle'];
                $tab['SousDomaine'] = '';
            } else {
                $tab['SousDomaine'] = $row['libelle'];
                $domaine = (new Atexo_DomainesActivites_Domaines())->retrieveDomaineById($row['Parent']);
                if ($domaine instanceof CommonCG76Domaine) {
                    $tab['Domaine'] = $domaine->getLibelle();
                }
            }

            $listMails = (new Atexo_Entreprise_Inscrit())->getListMailInscritByEntreprise($row['id']);
            $listMails = implode('; ', $listMails);
            $tab['ListMailsContacts'] = $listMails;

            //print_r($tab);
            //exit;
            $donneesComplementairesEntreprise[] = $tab;
        }

        return $donneesComplementairesEntreprise;
    }
}
