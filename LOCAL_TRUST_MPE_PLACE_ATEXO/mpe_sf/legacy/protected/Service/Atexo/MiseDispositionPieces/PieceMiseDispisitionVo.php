<?php

namespace Application\Service\Atexo\MiseDispositionPieces;

/**
 * Description de la classe.
 *
 * @author Khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4.2
 *
 * @since MPE-4.0
 */
class Atexo_MiseDispositionPieces_PieceMiseDispisitionVo
{
    private $id;
    private $_type;
    private $_name;
    private $_type_document;
    private $_statut_mise_disposition;
    private $_date_mise_disposition;
    private $_statut_recuperation;
    private $_date_recuperation;
    private $_idBlob;
    private $_urlPiece;
    private $_signature;
    private $_message;

    public function getId()
    {
        return $this->id;
    }

    public function setId($value)
    {
        $this->id = $value;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setType($value)
    {
        $this->_type = $value;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setName($value)
    {
        $this->_name = $value;
    }

    public function getTypeDocument()
    {
        return $this->_type_document;
    }

    public function setTypeDocument($value)
    {
        $this->_type_document = $value;
    }

    public function getStatutMiseDisposition()
    {
        return $this->_statut_mise_disposition;
    }

    public function setStatutMiseDisposition($value)
    {
        $this->_statut_mise_disposition = $value;
    }

    public function getDateMiseDisposition()
    {
        return $this->_date_mise_disposition;
    }

    public function setDateMiseDisposition($value)
    {
        $this->_date_mise_disposition = $value;
    }

    public function getStatutRecuperation()
    {
        return $this->_statut_recuperation;
    }

    public function setStatutRecuperation($value)
    {
        $this->_statut_recuperation = $value;
    }

    public function getDateRecuperation()
    {
        return $this->_date_recuperation;
    }

    public function setDateRecuperation($value)
    {
        $this->_date_recuperation = $value;
    }

    public function getIdBlob()
    {
        return $this->_idBlob;
    }

    public function setIdBlob($value)
    {
        $this->_idBlob = $value;
    }

    public function getUrlPiece()
    {
        return $this->_urlPiece;
    }

    public function setUrlPiece($value)
    {
        $this->_urlPiece = $value;
    }

    public function getSignature()
    {
        return $this->_signature;
    }

    public function SetSignature($value)
    {
        $this->_signature = $value;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    public function setMessage($value)
    {
        $this->_message = $value;
    }
}
