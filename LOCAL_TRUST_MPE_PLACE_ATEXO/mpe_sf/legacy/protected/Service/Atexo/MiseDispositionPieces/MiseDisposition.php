<?php

namespace Application\Service\Atexo\MiseDispositionPieces;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonPiecesMiseDisposition;
use Application\Propel\Mpe\CommonPiecesMiseDispositionPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;

/**
 * Classe de gestion pieces mise a disposition.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.1
 *
 * @since MPE-4.0
 */
class Atexo_MiseDispositionPieces_MiseDisposition
{
    public function retrievePiece($id_externe, $org, $attributaire, $destinataire, $consultationId, $lot, $type)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonPiecesMiseDispositionPeer::ID_DESTINATAIRE, $destinataire);
        $c->add(CommonPiecesMiseDispositionPeer::ORG, $org);
        $c->add(CommonPiecesMiseDispositionPeer::ID_DECISION_ENVELOPPE, $attributaire);
        $c->add(CommonPiecesMiseDispositionPeer::ID_EXTERNE, $id_externe);
        $c->add(CommonPiecesMiseDispositionPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonPiecesMiseDispositionPeer::TYPE, $type);
        if ($lot) {
            $c->add(CommonPiecesMiseDispositionPeer::LOT, $lot);
        }
        $piece = CommonPiecesMiseDispositionPeer::doSelectOne($c, $connexion);
        if ($piece) {
            return $piece;
        } else {
            return false;
        }
    }

    public function getAllFichiersOffreByLot($offre, $organisme, $consultationId, $lot)
    {
        $idEntrep = '';
        $nomEntrep = '';
        $arrayPieces = [];
        if ($offre instanceof CommonOffres) {
            $idEntrep = $offre->getEntrepriseId();
            $nomEntrep = Atexo_Entreprise::getNomEntreprise($idEntrep, null);
        } else {
            //instanceof OffrePapier
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren(substr($offre->getSiret(), 0, 9));
            if ($entreprise instanceof Entreprise) {
                $idEntrep = $entreprise->getId();
            }
            $nomEntrep = $offre->getNomEntreprise();
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($offre instanceof CommonOffres) {
            $enveloppes = $offre->getCommonEnveloppes();
            foreach ($enveloppes as $enveloppe) {
                if (($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) || ($enveloppe->getSousPli() == $lot)) {
                    $typeEnveloppe = '';
                    $c = new Criteria();
                    $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $enveloppe->getIdEnveloppeElectro());
                    $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
                    $fichiers = CommonFichierEnveloppePeer::doSelect($c, $connexion);
                    if ($enveloppe->getTypeEnv()) {
                        $typeEnveloppe = $enveloppe->getTypeEnv();
                    }
                    foreach ($fichiers as $fichier) {
                        $piece = new Atexo_MiseDispositionPieces_PieceMiseDispisitionVo();
                        $piece->setId($fichier->getIdFichier());
                        if ($fichier->getIdBlob()) {
                            $piece->setIdBlob($fichier->getIdBlob());
                        }
                        $nomFichier = Atexo_Util::OterAccents($fichier->getNomFichier());
                        if ($fichier->getTailleFichier()) {
                            $tailleFichier = $fichier->getTailleFichier() / 1024;
                            $tailleFichier = Atexo_Util::arrondirSizeFile($tailleFichier);
                            $nomFichier .= ' ( '.$tailleFichier;
                        }
                        if ('' != $fichier->getSignatureFichier()) {
                            $nomFichier .= ' - '.Prado::localize('DEFINE_FICHIER_SIGNE');
                            $piece->SetSignature('1');
                        }
                        $nomFichier .= ' )';
                        $piece->setName($nomFichier);
                        if ('' != $consultationId && '' != $typeEnveloppe && '' != $nomEntrep) {
                            $urlFichier = 'index.php?page=Agent.DownloadPli&id='.$consultationId.'&idEnveloppe='.$enveloppe->getIdEnveloppeElectro().'&nomFichier='.$fichier->getNomFichier().'&typeEnv='.$typeEnveloppe.'&nomEntreprise='.$nomEntrep;
                            $piece->setUrlPiece($urlFichier);
                        }
                        $arrayPieces[] = $piece;
                    }
                }
            }
        }

        return $arrayPieces;
    }

    public function insertArrayPieces($arrayPieces)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            foreach ($arrayPieces as $pieces) {
                $pieces->save($connexion);
            }

            return true;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    public function deleteArrayPieces($arrayPieces)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            foreach ($arrayPieces as $pieces) {
                if ($pieces instanceof CommonPiecesMiseDisposition) {
                    $pieces->delete($connexion);
                }
            }

            return true;
        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    public function retrievePiecesByDateAndTypeAndStatut($destinataire, $date, $statut)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonPiecesMiseDispositionPeer::DATE_MISE_DISPOSITION, $date, Criteria::GREATER_EQUAL);
        $c->add(CommonPiecesMiseDispositionPeer::STATUT_DISPOSITION, $statut);
        $c->add(CommonPiecesMiseDispositionPeer::ID_DESTINATAIRE, $destinataire, Criteria::IN);
        $ArrayPieces = CommonPiecesMiseDispositionPeer::doSelect($c, $connexion);

        return $ArrayPieces;
    }

    public function retrievePieceById($id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonPiecesMiseDispositionPeer::ID, $id);
        $piece = CommonPiecesMiseDispositionPeer::doSelectOne($c, $connexion);

        return $piece;
    }
}
