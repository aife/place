<?php

namespace Application\Service\Atexo\MiseDispositionPieces;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonDestinataireMiseDispositionPeer;
use Application\Service\Atexo\Atexo_Config;

/**
 * Classe de destinataires pieces mise a disposition.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.1
 *
 * @since MPE-4.0
 */
class Atexo_MiseDispositionPieces_DestinataireMiseDisposition
{
    public function getDestinatireMiseDispositionByOrg($org, $valeurParDefaut)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonDestinataireMiseDispositionPeer::ORGANISME, $org);
        $destinataires = CommonDestinataireMiseDispositionPeer::doSelect($c, $connexion);
        $arrayDestinataires = [];
        if ($valeurParDefaut) {
            $arrayDestinataires[0] = $valeurParDefaut;
        }
        if ($destinataires) {
            foreach ($destinataires as $destinataire) {
                $arrayDestinataires[$destinataire->getId()] = $destinataire->getLibelle();
            }
        }

        return $arrayDestinataires;
    }

    /*******************************************
     * return l'ID du destinataire
     * param : Code du destinataire
     *******************************************/
    public function getIdDestinataireByCode($code)
    {
        $arrayIds = [];
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonDestinataireMiseDispositionPeer::CODE, $code);
        $destinataires = CommonDestinataireMiseDispositionPeer::doSelect($c, $connexion);
        foreach ($destinataires as $destintaire) {
            $arrayIds[] = $destintaire->getId();
        }

        return $arrayIds;
    }
}
