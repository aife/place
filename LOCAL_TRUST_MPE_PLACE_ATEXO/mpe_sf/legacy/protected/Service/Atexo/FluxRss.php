<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTFluxRssPeer;

/**
 * Génération de RSS.
 *
 * @copyright Atexo 2013
 *
 * @since 4.4.0
 *
 * @category Atexo
 */
class Atexo_FluxRss
{
    /*
     * Permet d'avoir touts les flux rss
     */
    public function getAllFluxRss()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $AllFluxRss = CommonTFluxRssPeer::doSelect($c, $connexion);

        return $AllFluxRss;
    }

    /*
    * Permet d'avoir les flux rss à afficher dans la page FluxRss
    */
    public function getFluxRssAAfficher()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTFluxRssPeer::AFFICHER_FLUX_RSS, '1');
        $AllFluxRss = CommonTFluxRssPeer::doSelect($c, $connexion);

        return $AllFluxRss;
    }

    // END OF CLASS
}
