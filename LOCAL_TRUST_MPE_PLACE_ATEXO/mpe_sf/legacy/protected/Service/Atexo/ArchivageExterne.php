<?php

namespace Application\Service\Atexo;
use Application\Propel\Mpe\CommonConsultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultationArchiveBlocPeer;
use Application\Propel\Mpe\CommonConsultationQuery;
use Prado\Prado;
use \Exception;
use AtexoCrypto\Dto\Fichier;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Config;

/**
 * Classe d'envoi des fichiers vers l'extrene.
 *
 * @author chouika khadija <khadija.chouika@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_ArchivageExterne
{
    const PART = '.part';

    public static function sendDACtoAtlas($docId, $compId, $pathDac, $authentificationForte, $cert, $pwdCert, &$codeHttp, $fileLog = null)
    {
        if (!$fileLog) {
            $fileLog = Atexo_LoggerManager::getLogger('archivage');
        }

        $contRep = Atexo_Config::getParameter('ATLAS_CONT_REP');

        try {
            $ch = curl_init();    // initialize curl handle
            $url = Atexo_Config::getParameter('URL_ARCHIVAGE_EXTERNE_ENVOI').'/put?docId='.$docId.'&compId='.$compId.'&contRep='.$contRep;
            $fileLog->info('Début sendDACtoAtlas sur URL : '.$url);
            $fileLog->info('Fichier : '.$pathDac);

            $fp = fopen($pathDac, 'r');
            $sizeFile = filesize($pathDac);
            curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            curl_setopt($ch, CURLOPT_PUT, 1);
            curl_setopt($ch, CURLOPT_INFILE, $fp);
            curl_setopt($ch, CURLOPT_INFILESIZE, $sizeFile);

            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/zip']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Atexo_Config::getParameter('CURLOPT_CONNECTTIMEOUT'));
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            if ($authentificationForte) {
                curl_setopt($ch, CURLOPT_SSLCERT, $cert);
                curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pwdCert);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            }
            //definition du proxy si existe
            $urlProxy = Atexo_Config::getParameter('URL_PROXY');
            $portProxy = Atexo_Config::getParameter('PORT_PROXY');
            $curlOptProxy = $urlProxy.':'.$portProxy;
            if ('' != $urlProxy) {
                curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
            }
            curl_exec($ch); // run the whole process
            $codeHttp = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $information = curl_getinfo($ch);
            $fileLog->info('Informations  : '.print_r($information, true));
            // erreur curl
            $errorNumber = curl_errno($ch);
            if (0 != $errorNumber) {
                $errorMsg = curl_error($ch);
                curl_close($ch);
                $fileLog->error('Erreur CRUL  code erreur : '.$errorNumber.' message erreur : '.$errorMsg.' code HTTP : '.$codeHttp);
                $fileLog->info('Fin sendDACtoAtlas sur URL : '.$url.'Avec erreur ');

                return false;
            }
            fclose($fp);
            $fileLog->info('Fin sendDACtoAtlas sur URL : '.$url.'Avec Code HTTP '.$codeHttp);
            if ('201' == $codeHttp) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $fileLog->error('Erreur CRUL  : '.$e->getMessage());

            return false;
        }
    }

    public static function getBlocfromAtlas($docId, $compId, $authentificationForte = false, $cert = null, $pwdCert = null)
    {
        $fileLog = Atexo_LoggerManager::getLogger('archivage');

        $contRep = Atexo_Config::getParameter('ATLAS_CONT_REP');
        $url = Atexo_Config::getParameter('URL_ARCHIVAGE_EXTERNE_ENVOI').'/get?docId='.urlencode($docId).'&compId='.urlencode($compId).'&contRep='.$contRep;

        $fileLog->info('Récupération Archive depuis ATLAS url  : '.$url);

        // a definir dans fichier de config
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);

        if ($authentificationForte) {
            curl_setopt($ch, CURLOPT_SSLCERT, $cert);
            curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $pwdCert);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        }

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Atexo_Config::getParameter('CURLOPT_CONNECTTIMEOUT'));
        //definition du proxy si existe
        $urlProxy = Atexo_Config::getParameter('URL_PROXY');
        $portProxy = Atexo_Config::getParameter('PORT_PROXY');
        $curlOptProxy = $urlProxy.':'.$portProxy;
        if ('' != $urlProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $result = curl_exec($ch);
        /* si il ya une erreur*/
        if (false === curl_exec($ch)) {
            $fileLog->error('Une erreur est survenue : '.curl_error($ch));
        }

        // Statut HTTP
        $tabResult = [];
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        switch ($code) {
            case '200':
                $tabResult['error'] = 0;
                $tabResult['codeHttp'] = 200;
                $tabResult['resultat'] = $result;
                $fileLog->info("La récupération du bloc s'est correctement déroulée, codeHttp : ".$tabResult['codeHttp']);
                break;

            case '404':
                $tabResult['error'] = 1;
                $tabResult['codeHttp'] = 404;
                $tabResult['message'] = Prado::localize('ERROR_ARCHIVE_COMPOSANT_NON_TROUVE');
                $fileLog->error("La récupération du bloc s'est déroulée avec erreur, codeHttp : ".$tabResult['codeHttp'].' ,message : '.$tabResult['message']);
                break;
            case '500':
                $tabResult['error'] = 1;
                $tabResult['codeHttp'] = 500;
                $tabResult['message'] = Prado::localize('ERROR_ARCHIVE_COMPOSANT_NON_TROUVE');
                $fileLog->error("La récupération du bloc s'est déroulée avec erreur, codeHttp : ".$tabResult['codeHttp'].' ,message : '.$tabResult['message']);
                break;
        }

        curl_close($ch);

        return $tabResult;
    }

    /**
     * @param $consultationArchive
     *
     * @throws Atexo_Config_Exception
     */

    /**
     * Cette méthode récupère depuis ATLAS l'ensemble des données relative aux blocs d'archives.
     *
     * @param $consultationArchive l'objet correspondant à l'entrée en BD pour consultation_archive
     * @param $cheminFichierZip le chemin vers le fichier qui contient l'ensemble des blocs récupérés depusi ATLAS si la fonction retourne true
     *
     * @return bool true si la récupération de tous les blocs s'est bien déroulée, false sinon
     *
     * @throws Atexo_Config_Exception
     */
    public static function getDACFromAtlas($consultationArchive, &$cheminFichierZip)
    {
        $fileLog = Atexo_LoggerManager::getLogger('archivage');

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        //Pour la consultation_archive récupérer tous les consultation_archive_bloc avec status_transmission = 1
        $c = new Criteria();
        $c->add(CommonConsultationArchiveBlocPeer::STATUS_TRANSMISSION, '1');
        $c->addAscendingOrderByColumn(CommonConsultationArchiveBlocPeer::NUMERO_BLOC);
        //si le nombre de bloc transmis vers ATLAS est différent du nombre de bloc présent dans consultation_archive,
        //il y aura un problème à la reconstitution donc il faut renvoyer false
        if ($consultationArchive->countCommonConsultationArchiveBlocs($c, $connexion) != $consultationArchive->getNombreBloc()) {
            $fileLog->error("getDACFromAtlas : nombre de blocs transmis à ATLAS différent du nombre de bloc de l'archive");
            $cheminFichierZip = '';

            return false;
        } else { //le nombre de bloc transmis est identique au nombre de bloc stocké,
            // on peut récupérer les blocs pour reconstituer l'archive
            $cert = Atexo_Config::getParameter("ATLAS_SSLCERT") ;
            $pwdCert = Atexo_Config::getParameter("ATLAS_SSLCERTPASSWD");

            $listeConsultationArchiveBloc = $consultationArchive->getCommonConsultationArchiveBlocs($c, $connexion);
            $referenceConsultation = $consultationArchive->getConsultationId();

            if (!empty($consultationArchive->getConsultationRef())
                && $consultationArchive->getConsultationRef() != $referenceConsultation
            ) {
                $referenceConsultation = $consultationArchive->getConsultationRef();
            }


            $cheminFichierZip = Atexo_Config::getParameter('COMMON_TMP')."/".$referenceConsultation."_archive_DAC_ATLAS";
            if (file_exists($cheminFichierZip.".zip")){
                return true;
            }
            if (file_exists($cheminFichierZip.self::PART)) {
                @unlink($cheminFichierZip.self::PART);
            }
            //on tente de récupérer chaque bloc à distance, si la récupération d'un bloc échoue on arrête
            foreach ($listeConsultationArchiveBloc as $consultationArchiveBloc) {
                $docId = $consultationArchiveBloc->getDocId();
                $resultat = self::getBlocfromAtlas($docId, $referenceConsultation, true, $cert, $pwdCert);
                Atexo_Util::write_file($cheminFichierZip.self::PART, $resultat['resultat'], 'a');
                if ('0' != $resultat['error']) {
                    //si la récupération d'un bloc échoue on arrête
                    $fileLog->error("getDACFromAtlas : une erreur est survenue lors de la récupération du bloc, voici l'erreur : ".$resultat['message']);
                    $cheminFichierZip = '';

                    return false;
                }
            }
            rename($cheminFichierZip.self::PART, $cheminFichierZip.'.zip');
            $cheminFichierZip .= '.zip';
            $fileLog->info('getDACFromAtlas : La récupération des blocs a été réalisée avec succès');

            //tous les blocs ont été récupérés et sauvegardés avec succès, on renvoie true et le chemin vers le zip est initialisé vers l'archive reconstitué
            return true;
        }
    }
}
