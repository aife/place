<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use AtexoCrypto\Dto\Consultation;
use Exception;

/**
 * Classe de manipulation des fichiers.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Files
{
    // file musts exist
    public function get_uploaded_file($var)
    {
        $fp = fopen($_FILES[$var]['tmp_name'], 'r');
        $content = fread($fp, filesize($_FILES[$var]['tmp_name']));
        fclose($fp);

        return [$_FILES[$var]['name'], $content];
    }

    public function write_file($file_name, $data)
    {
        $fp = fopen($file_name, 'w');
        if (!fwrite($fp, $data)) {
            return null;
        }
        fclose($fp);

        return 1;
    }

    public function write_encrypted_file($file_name, $data)
    {
        $pos = strpos($data, "\n");
        if (!$pos || $pos > 65) {
            $data = chunk_split($data, 64);
        }
        $fp = fopen($file_name, 'w');
        if (!fwrite($fp, "-----BEGIN PKCS7-----\n".trim($data)."-----END PKCS7-----\n")) {
            return null;
        }
        fclose($fp);

        return 1;
    }

    public function read_file($file_name)
    {
        $fp = fopen($file_name, 'r');
        $data = fread($fp, filesize($file_name));
        fclose($fp);

        return $data;
    }

    public function zip_file($file_name, $zip_file_name)
    {
        system(
            'nice zip -j '.escapeshellarg($zip_file_name).' '.escapeshellarg($file_name).' > '.Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1',
            $sys_answer
        );

        if (0 == $sys_answer) {
            return $zip_file_name;
        } else {
            return '';
        }
    }

    /** lit un fichier et le met en memoire.
     * @ingroup files
     *
     * @param $_filename STRING nom du fichier
     *
     * @return $res_ STRING le contenu du fichier
     */
    public function file2string($_filename)
    {
        $fp = fopen($_filename, 'r');
        $res_ = fread($fp, filesize($_filename));
        fclose($fp);

        return $res_;
    }

    // function file2string($_filename)

    /** lit une chaine de caracteres et l'ecrit dans le fichier.
     * @ingroup files
     *
     * @param $_string STRING la chaine de caracteres
     * @param $_filename STRING le nom du fichier
     *
     * @return false / INT nombre d'octets ecrits
     */
    public function string2file($_string, $_filename)
    {
        $fp = fopen($_filename, 'w');
        $res_ = fwrite($fp, $_string);
        fclose($fp);

        return $res_;
    }

    // function string2file($_string, $_filename)

    /** retourne un dossier zip.
     * @param $zip_file_name le nom du fichier zip
     */
    public function rebuild_zip($zip_file_name, $root_name = '____DCE')
    {
        $sys_answer = null;
        $root_name = str_replace(' ', '_', $root_name);
        $unid = uniqid(random_int(0, mt_getrandmax()), true);
        $tmp_zip_dirname = Atexo_Config::getParameter('COMMON_TMP').'dce_'.$unid;
        mkdir($tmp_zip_dirname);
        mkdir($tmp_zip_dirname."/$root_name");
        $sys_answer .= shell_exec('chmod -R 777 '.escapeshellarg($tmp_zip_dirname.'/'.$root_name));
        $sys_answer .= shell_exec('chown -R nobody.nobody '.escapeshellarg($tmp_zip_dirname.'/'.$root_name));
        $tmp_zip_file_name = $tmp_zip_dirname."/$root_name/$root_name.zip";
        $sys_answer .= shell_exec('cp -f '.escapeshellarg($zip_file_name).' '.escapeshellarg($tmp_zip_file_name));
        $sys_answer .= shell_exec('chmod 777 '.escapeshellarg($tmp_zip_file_name));
        $sys_answer .= shell_exec('chown nobody.nobody '.escapeshellarg($tmp_zip_file_name));
        $sys_answer = shell_exec('cd '.escapeshellarg($tmp_zip_dirname.'/'.$root_name).'; nice unzip  '.escapeshellarg($root_name.'.zip').'; rm -f '.escapeshellarg($root_name.'.zip').'; cd '.escapeshellarg($tmp_zip_dirname).'; nice zip -r '.escapeshellarg($root_name).' '.escapeshellarg($root_name).'; rm -rf '.escapeshellarg($root_name));

        return $tmp_zip_dirname."/$root_name.zip";
    }

    public function delete_file($zip_file_name)
    {
        $base_path = substr($zip_file_name, 0, strrpos($zip_file_name, '/'));
        $rep = explode('/', $zip_file_name);
        $repTmp = $rep[count($rep) - 2];
        shell_exec('cd'.escapeshellarg($base_path).'; rm -f '.escapeshellarg($zip_file_name).'; cd ../; rmdir '.escapeshellarg($repTmp).'; ');
    }

    public function downloadPartialConsFile($reference, $organisme, $indexpiece = false)
    {
        $dce_user_item_file = null;
        if (!is_dir(Atexo_Config::getParameter('COMMON_TMP'))) {
            mkdir(Atexo_Config::getParameter('COMMON_TMP'));
        }
        $temp_dir = Atexo_Config::getParameter('COMMON_TMP').session_id().'_dce/';
        @mkdir($temp_dir);
        $cmd = "cd $temp_dir && rm -rf *";
        system($cmd, $sys_answer);

        // Ecriture du DCE dans le repertoire temporaire
        $dce = '';
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $dce = (new Atexo_Consultation_Dce())->getDce($reference, $organisme);
        $consultation = CommonConsultationPeer::retrieveByPK($reference, $connexionCom);
        if ($consultation && $dce) {
            $blob_resource = Atexo_Blob::acquire_lock_on_blob($dce->getDce(), $organisme);
        }
        $blob_file = $blob_resource['blob']['pointer'];
        $dce_user_file = Atexo_Config::getParameter('COMMON_TMP').session_id().'_dce_user.tmp';
        @unlink($dce_user_file);
        // la fonction close est plus definit au niveau de la classe PropelPDO
        unset($connexionCom);
        $nbreFile = 0;
        $zip = zip_open($blob_file);
        if ($zip) {
            $index = 0;
            while ($zip_entry = zip_read($zip)) {
                ++$index;
                if ($_POST["dce_item_$index"] || $indexpiece == $index) {
                    if (zip_entry_open($zip, $zip_entry, 'r')) {
                        $dce_user_item_file = $temp_dir.
                                (new Atexo_Zip())->formaterZipEntryName(zip_entry_name($zip_entry));


                        $path = dirname($dce_user_item_file);
                        if (!is_dir($path) && '.' != $path) {
                            system("mkdir -p \"$path\"");
                        }
                        if ('/' == substr($dce_user_item_file, -1, 1) && !is_dir($dce_user_item_file)) {
                            system("mkdir -p \"$dce_user_item_file\"");
                            continue;
                        }
                        if ($path == $dce_user_item_file) {
                            continue;
                        }
                        ++$nbreFile;
                        $block_count = floor(zip_entry_filesize($zip_entry) / Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
                        $bytes_remaining = zip_entry_filesize($zip_entry) - ($block_count * Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));

                        // TD : ajout de @ pour cacher un warning arrivant avant les headers...
                        $fp = @fopen($dce_user_item_file, 'w');
                        for ($i = 0; $i < $block_count; ++$i) {
                            $buf = zip_entry_read($zip_entry, Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
                            fwrite($fp, $buf);
                        }
                        $buf = zip_entry_read($zip_entry, $bytes_remaining);
                        fwrite($fp, $buf);
                        @fclose($fp); // TD : ajout de @ pour cacher un warning arrivant avant les headers...
                        zip_entry_close($zip_entry);
                    }
                }
            }
            zip_close($zip);
        }
        (new Atexo_Blob())->release_lock_on_blob($blob_resource);
        if ($nbreFile > 1) {
            $contentType = 'application/zip';
            $cmd = "cd $temp_dir ; find . -type f -print0 | ( xargs -0 nice zip -r ".escapeshellarg($dce_user_file).' ) ';
            system("$cmd  > ".Atexo_Config::getParameter('COMMON_TMP').'zip.log 2>&1', $sys_answer);
            $nameFile = 'DCE_partiel_'.preg_replace('/[\/\\\\:\*\?"<>|]/', '', $consultation->getReferenceUtilisateur(true)).'.zip';
        } else {
            $contentType = Atexo_Util::mime_content_type($dce_user_item_file);
            $dce_user_file = $dce_user_item_file;
            $nameFile = basename($dce_user_item_file);
        }

        header('Pragma: ');
        header('Expires: ');
        header('Cache-control: ');
        header('Content-Disposition: attachment; filename="'.$nameFile.'"');
        header('Content-Type:'.$contentType); // force le DL sauf dans IE
        header('Content-Tranfert-Encoding: binary');
        header('Content-Length: '.@filesize($dce_user_file));

        $block_count = floor(@filesize($dce_user_file) / Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
        $bytes_remaining = @filesize($dce_user_file) - ($block_count * Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
        $fp = fopen($dce_user_file, 'r');
        for ($i = 0; $i < $block_count; ++$i) {
            echo @fread($fp, Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
            flush();
            ob_flush();
        }
        echo @fread($fp, $bytes_remaining);
        fclose($fp);
        @unlink($dce_user_file);
        $cmd = 'cd '.Atexo_Config::getParameter('COMMON_TMP').' ; rm -rf '.session_id().'_dce/';
        @system($cmd, $sys_answer);
        exit;
    }

    // END download_cons_file

    /**
     * Cree un repertoire.
     *
     * @param $dirPath le chemin du repertoire a creer
     *
     * @return int
     * */
    public static function createDir($dirPath)
    {
        $cmd = 'mkdir -p '.escapeshellarg($dirPath);
        $intReturn = 0;
        system($cmd, $intReturn);

        return $intReturn;
    }

    /**
     * retroune un tableau des index des fichiers enregistres dans le ZIP.
     *
     * @param string chemin du dosser ZIP
     */
    public static function getArrayIndexSelectedFiles($zipFile, $zipItemHtml = 'dce_item', &$tailleSelectedFiles = 0)
    {
        try {
            $arrayIndexFiles = [];
            $zip = zip_open($zipFile);
            if (is_resource($zip)) {
                $index = 0;
                while ($res = zip_read($zip)) {
                    ++$index;
                    if ($_POST[$zipItemHtml."_$index"]) {
                        $tailleSelectedFiles += zip_entry_filesize($res);
                        $arrayIndexFiles[] = $index;
                    }
                }
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de l'ouverture du zip/lecture du zip ".$e->getMessage());
        }

        return $arrayIndexFiles;
    }

    /**
     * permet d'avoir le nom du fichier archive d'une consultation.
     *
     * @param CommonConsultation $consultation la reference de la Consultation
     * @param string             $idService    l'identifiant du service auquel la consultation est lié (vaut 0 si la consultation est rattachée à l'organisme
     * @param string             $nomFichier   nom du fichier
     *
     * @return string $nomFichier nom du fichier archive d'une consultation
     *
     * @author MWA <mwa@atexo.com>
     * @copyright Atexo 2019
     */
    public function getNomFileXmlAndZip($consultation, $idService)
    {
        $nomFichier = $idService.'_archives_'.preg_replace('/[^-~[:alnum:]]/', '_', Atexo_Util::OterAccents($consultation->getReferenceutilisateur()).'_'.$consultation->getId());

        return $nomFichier;
    }

    /**
     * permet d'avoir le nom du fichier archive d'une consultation
     * Cette méthode est utilisée pour la compatibilité ascendant avant l'évolution qui consiste à stocker
     * le chemin des archives en BD (avec table consultation_archive).
     *
     * @param CommonConsultation $consultation la reference de la Consultation
     * @param string             $sigleService le sigle du service auquel la consultation est lié
     * @param string             $nomFichier   nom du fichier
     *
     * @return string $nomFichier nom du fichier archive d'une consultation
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getOldNomFileXmlAndZip($consultation, $sigleService, $nomFichier = '')
    {
        //avant l'évolution qui consiste à stocker le chemin des archives en BD (avec table consultation_archive) on calculait comme suit le nom du fichier
        $nomFichier = str_replace('/', '_', $nomFichier.trim(Atexo_Util::OterAccents($sigleService)).'_archives_'.preg_replace('/[^-~[:alnum:]]/', '_', Atexo_Util::OterAccents($consultation->getReferenceutilisateur())).'_'.$consultation->getId());

        return $nomFichier;
    }

    public function getFilesFromZip($blob_file, $Item)
    {
        $pieces = [];
        if ($blob_file) {
            $ordered_res = Atexo_Zip::getOrderedFilesForZip($blob_file, false);

            if (is_array($ordered_res)) {
                $ordre = '';
                foreach ($ordered_res as $ordre => $directory_files) {
                    $key = '';
                    foreach ($directory_files as $key => $directory_one_file) {
                        if (in_array($directory_one_file['index'], $Item) && 'file' == $directory_one_file['type']) {
                            $pieces[$directory_one_file['index']] = $directory_one_file;
                        }
                    }
                }
            }
        }

        return $pieces;
    }

    /**
     * Permet de copier les fichiers.
     *
     * @param $source: le chemin du fichier à copier
     * @param $destination: le chemin du fichier destination
     */
    public function copieFichier($source, $destination)
    {
        if (is_file($source)) {
            system("cp $source $destination");
        }
    }
}
