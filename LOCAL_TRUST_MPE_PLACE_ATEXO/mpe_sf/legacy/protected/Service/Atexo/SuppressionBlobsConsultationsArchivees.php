<?php

namespace Application\Service\Atexo;

use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Complements;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Documentexterne;
use Application\Service\Atexo\Consultation\Atexo_Consultation_EchangePieceJointe;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_QuestionDce;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;

class Atexo_SuppressionBlobsConsultationsArchivees
{
    private ?string $logFileErreur = null;

    public function run()
    {
        $this->logFileErreur = __DIR__.'/../../var/log/';
        $log = "\r\n\r\n_________________________________\r\n";
        $log .= 'Log du '.date('d/m/Y H:i:s')."\r\n";
        $log .= "_________________________________\r\n";

        $organismes = (new Atexo_Organismes())->retrieveAllOrganismes();

        foreach ($organismes as $oneOrganisme) {
            $org = $oneOrganisme->getAcronyme();
            $log .= '-> ---------------------- Organisme : '.$oneOrganisme->getDenominationOrg()
                .'  -  ['.$org."] --------------\r\n";
            //Récupérer les consultations concernées pour chaque organisme.
            $consultations = (new Atexo_Consultation())->retrieveConsultationArchivee($org);
            if (0 == (is_countable($consultations) ? count($consultations) : 0)) {
                $log .= "->	Aucune Consultation trouvée pour l'organisme ".$org."\r\n";
            }
            foreach ($consultations as $one) {
                $log .= '->	Les fichiers de la Consultation : '.$one->getReferenceUtilisateur()."\r\n";
                $consultationId = $one->getId();
                //-DCE:
                $dces = (new Atexo_Consultation_Dce())->getDces($consultationId, $org);
                if (0 == (is_countable($dces) ? count($dces) : 0)) {
                    $log .= '->	Aucun DCE trouvé pour la consultation '.$one->getReferenceUtilisateur()."\r\n";
                } else {
                    $log .= "->		Debut Suppression Fichiers DCEs : \r\n";
                    foreach ($dces as $dce) {
                        if ($dce->getDce() && (new Atexo_Blob())->deleteBlobFromBaseDirectory($dce->getDce(), $org)) {
                            $log .= '->			Le fichier DCE NO : '.$dce->getDce()
                                ." a été supprimé avec succès\r\n";
                        } else {
                            $log .= '->			Echec de suppression du fichier DCE NO : '
                                .$dce->getDce()."\r\n";
                        }
                    }
                    $log .= "->		Fin Suppression Fichiers DCEs : \r\n";
                }

                //-Question DCE
                $questionsDce = (new Atexo_Entreprise_QuestionDce())->getQuestionsDces($consultationId, $org);
                if (0 == (is_countable($questionsDce) ? count($questionsDce) : 0)) {
                    $log .= '->	Aucune Question DCE trouvée pour la consultation '.
                        $one->getReferenceUtilisateur()."\r\n";
                } else {
                    $log .= "->		Debut Suppression Fichiers Questions DCEs : \r\n";
                    foreach ($questionsDce as $qDce) {
                        if ($dce->getDce() && (new Atexo_Blob())->deleteBlobFromBaseDirectory($qDce->getIdFichier(), $org)) {
                            $log .= '->			Le fichier Question DCE NO : '.$qDce->getIdFichier()
                                ." a été supprimé avec succès\r\n";
                        } else {
                            $log .= '->			Echec de suppression du fichier Question DCE NO : '
                                .$qDce->getIdFichier()."\r\n";
                        }
                    }
                    $log .= "->		Fin Suppression Fichiers Questions DCEs : \r\n";
                }
                //-RG:

                $rgs = (new Atexo_Consultation_Rg())->getRGs($consultationId, $org);

                if (0 == (is_countable($rgs) ? count($rgs) : 0)) {
                    $log .= '->	Aucun RG trouvé pour la consultation '.$one->getReferenceUtilisateur()."\r\n";
                } else {
                    $log .= "->		Debut Suppression Fichiers RGs : \r\n";
                    foreach ($rgs as $rg) {
                        if ($rg->getRg() && (new Atexo_Blob())->deleteBlobFromBaseDirectory($rg->getRg(), $org)) {
                            $log .= '->			Le fichier RG NO : '.$rg->getRg()." a été supprimé avec succès\r\n";
                        } else {
                            $log .= '->			Echec de suppression du fichier RG NO : '.$rg->getRg()."\r\n";
                        }
                    }
                    $log .= "->		Fin Suppression Fichiers RGs : \r\n";
                }
                //-Complement:

                $complements = (new Atexo_Consultation_Complements())->getComplements($consultationId, $org);
                if (0 == (is_countable($complements) ? count($complements) : 0)) {
                    $log .= '->	Aucun COMPLEMET trouvé pour la consultation '.$one->getReferenceUtilisateur()."\r\n";
                } else {
                    $log .= "->		Debut Suppression Fichiers Complements : \r\n";
                    foreach ($complements as $cmp) {
                        if ($cmp->getComplement() && (new Atexo_Blob())->deleteBlobFromBaseDirectory($cmp->getComplement(), $org)) {
                            $log .= '->			Le fichier Complement NO : '.$cmp->getComplement()
                                ." a été supprimé avec succès\r\n";
                        } else {
                            $log .= '->			Echec de suppression du fichier Complement NO : '
                                .$cmp->getComplement()."\r\n";
                        }
                    }
                    $log .= "->		Fin Suppression Fichiers Complements : \r\n";
                }
                //-Avis

                $Avis = (new Atexo_Publicite_Avis())->retreiveListAvis($consultationId, $org);
                if (0 == (is_countable($Avis) ? count($Avis) : 0)) {
                    $log .= '->	Aucun Avis trouvé pour la consultation '.$one->getReferenceUtilisateur()."\r\n";
                } else {
                    $log .= "->		Debut Suppression Fichiers Avis : \r\n";
                    foreach ($Avis as $oneAv) {
                        if ($oneAv->getAvis() && (new Atexo_Blob())->deleteBlobFromBaseDirectory($oneAv->getAvis(), $org)) {
                            $log .= '->			Le fichier Avis NO : '.$oneAv->getAvis(
                            )." a été supprimé avec succès\r\n";
                        } else {
                            $log .= '->		Echec de suppression du fichier Avis NO : '.$oneAv->getAvis()."\r\n";
                        }
                    }
                    $log .= "->		Fin Suppression Fichiers Avis : \r\n";
                }
                //-Document Externe

                $documentsExternes = (new Atexo_Consultation_Documentexterne())->getDocumentsExternes($consultationId, $org);
                if (0 == (is_countable($documentsExternes) ? count($documentsExternes) : 0)) {
                    $log .= '->	Aucun Document Externe trouvé pour la consultation '.$one->getReferenceUtilisateur()
                        ."\r\n";
                } else {
                    $log .= "->		Debut Suppression Fichiers Documents Externes : \r\n";
                    foreach ($documentsExternes as $oneDex) {
                        if ($oneDex->getIdblob() && (new Atexo_Blob())->deleteBlobFromBaseDirectory($oneDex->getIdblob(), $org)) {
                            $log .= '->			Le fichier Document externe NO : '.$oneDex->getIdblob()
                                ." a été supprimé avec succès\r\n";
                        } else {
                            $log .= '->			Echec de suppression du fichier Document externe NO : '
                                .$oneDex->getIdblob()."\r\n";
                        }
                    }
                    $log .= "->		Fin Suppression Fichiers Documents externes : \r\n";
                }
                //-offre, enveloppe,FichierEnveloppe,BlocFichierEnveloppe
                $offres = (new Atexo_Consultation_Responses())->retrieveOffreByConsRef($consultationId, $org);
                if (0 == (is_countable($offres) ? count($offres) : 0)) {
                    $log .= '->	Aucune Offre trouvée pour la consultation '.$one->getReferenceUtilisateur()."\r\n";
                } else {
                    $log .= "->		Debut Suppression Fichiers Offre : \r\n";

                    foreach ($offres as $offre) {
                        $enveloppes = (new Atexo_Consultation_Responses())->getEnveloppesByIdOffre($offre->getId(), $org);
                        if (0 == (is_countable($enveloppes) ? count($enveloppes) : 0)) {
                            $log .= "->			Aucune Enveloppe trouvée pour l'offre ".$offre->getId()."\r\n";
                        }
                        foreach ($enveloppes as $enveloppe) {
                            if ($enveloppe->getFichier() && (new Atexo_Blob())->deleteBlobFromBaseDirectory($enveloppe->getFichier(), $org)) {
                                $log .= '->			Le fichier Enveloppe NO : '.$enveloppe->getFichier(
                                )." a été supprimé avec succès\r\n";
                            } else {
                                $log .= '->			Echec de suppression du fichier Enveloppe NO : '
                                    .$enveloppe->getFichier()."\r\n";
                            }

                            $fichierEnveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppeFiles($enveloppe->getIdEnveloppeElectro(), $org);
                            if (0 == (is_countable($fichierEnveloppes) ? count($fichierEnveloppes) : 0)) {
                                $log .= "->			Aucun Fivhier Enveloppe trouvé pour l'enveloppe "
                                    .$enveloppe->getIdEnveloppeElectro()."\r\n";
                            }
                            foreach ($fichierEnveloppes as $fileEnvlp) {
                                if ($fileEnvlp->getIdBlob() && (new Atexo_Blob())->deleteBlobFromBaseDirectory($fileEnvlp->getIdBlob(), $org)) {
                                    $log .= '->			Le fichier FichierEnveloppe NO : '.$fileEnvlp->getIdBlob(
                                    )." a été supprimé avec succès\r\n";
                                } else {
                                    $log .= '->			Echec de suppression du fichier FichierEnveloppe NO : '
                                        .$fileEnvlp->getIdBlob()."\r\n";
                                }

                                $blocFichiersEnveleppes =
                                    (new Atexo_Consultation_Responses())->getBlocsFichiersEnveloppesByIdEnveloppe($fileEnvlp->getIdFichier(), $org);
                                if (0 == (is_countable($blocFichiersEnveleppes) ? count($blocFichiersEnveleppes) : 0)) {
                                    $log .= '->			Aucun Bloc de Fivhier Enveloppe trouvé pour le Fichier Enveloppe '
                                        .$fileEnvlp->getIdFichier()."\r\n";
                                }
                                foreach ($blocFichiersEnveleppes as $blocFileAnvlp) {
                                    if ($blocFileAnvlp->getIdBlobChiffre()
                                        && (new Atexo_Blob())->deleteBlobFromBaseDirectory($blocFileAnvlp->getIdBlobChiffre(), $org)) {
                                        $log .= '->			Le fichier BlocFichierEnveloppe Chiffre NO : '
                                            .$blocFileAnvlp->getIdBlobChiffre()." a été supprimé avec succès\r\n";
                                    } else {
                                        $log .= '->			Echec de suppression du fichier BlocFichierEnveloppe Chiffre NO : '
                                            .$blocFileAnvlp->getIdBlobChiffre()."\r\n";
                                    }
                                    if ($blocFileAnvlp->getIdBlobDechiffre() && (new Atexo_Blob())->deleteBlobFromBaseDirectory($blocFileAnvlp->getIdBlobDechiffre(), $org)) {
                                        $log .= '->			Le fichier BlocFichierEnveloppe Dechiffre NO : '
                                            .$blocFileAnvlp->getIdBlobDechiffre()." a été supprimé avec succès\r\n";
                                    } else {
                                        $log .= '->			Echec de suppression du fichier BlocFichierEnveloppe Dechiffre NO : '
                                            .$blocFileAnvlp->getIdBlobDechiffre()."\r\n";
                                    }
                                }
                            }
                        }
                    }
                    $log .= "->		Fin Suppression Fichiers Offre : \r\n";
                }
                //-Echange
                $echanges = (new Atexo_Chorus_Echange())->retreiveEchangesByRefConsultation($consultationId, $org);
                if (0 == (is_countable($echanges) ? count($echanges) : 0)) {
                    $log .= '->	Aucun Echange trouvé pour la consultation '.$one->getReferenceUtilisateur(
                    )."\r\n";
                }
                foreach ($echanges as $ehange) {
                    $echangesPJ = (new Atexo_Consultation_EchangePieceJointe())->getEchangePiecesJointes($ehange->getId(), $org);
                    if (0 == (is_countable($echangesPJ) ? count($echangesPJ) : 0)) {
                        $log .= "->	Aucune Piece Jointe trouvée pour l'Echange ".$ehange->getId()."\r\n";
                    }
                    foreach ($echangesPJ as $pj) {
                        if ($pj->getPiece() && (new Atexo_Blob())->deleteBlobFromBaseDirectory($pj->getPiece(), $org)) {
                            $log .= '->		Le fichier EchangePieceJointe NO : '.$pj->getPiece(
                            )." a été supprimé avec succès\r\n";
                        } else {
                            $log .= '->		Echec de suppression du fichier EchangePieceJointe NO : '
                                .$blocFileAnvlp->getPiece()."\r\n";
                        }
                    }
                }
                $log .= "  			**************************************************************\r\n";
            }
            $log .= "  			**************************************************************\r\n";
        }
        if ($log) {
            Atexo_Util::write_file(
                $this->logFileErreur.'log_SuppressionBlobsConsultationsArchivees_'
                .Atexo_Util::getDateAujourdhui2(),
                $log,
                'a'
            );
        }
        echo "Suppression terminée.\r\n";
        exit;
    }
}
