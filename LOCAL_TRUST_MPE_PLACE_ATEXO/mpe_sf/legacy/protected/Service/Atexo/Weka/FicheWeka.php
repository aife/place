<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Service\Atexo\Weka;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonParametrageFicheWeka;
use Application\Propel\Mpe\CommonParametrageFicheWekaPeer;
use Application\Propel\Mpe\Om\BaseCommonParametrageFicheWekaPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe: fonctions metiers WEKA.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE4
 */
class Atexo_Weka_FicheWeka
{
    /**
     * Permet de recuperer une fiche weka en base.
     *
     * @param string $page
     */
    public static function recupererFicheWekaByPage($page)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonParametrageFicheWekaPeer::PAGES, '%#'.$page.'#%', Criteria::LIKE);
        $fichesWeka = CommonParametrageFicheWekaPeer::doSelect($c, $connexion);

        return $fichesWeka;
    }

    /**
     * Permet d'inserer des enregistrements dans la table 'Parametrage_Fiche_Weka' à partir d'un fichier xml.
     *
     * @param string $xmlFile
     */
    public static function insererDonneesWeka($xmlFile)
    {
        $arrayData = Atexo_Util::xmlFileToArray($xmlFile);
        if (is_array($arrayData['card']) && count($arrayData['card'])) {
            try {
                foreach ($arrayData['card'] as $data) {
                    $new = false;
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $parametrageFicheWeka = BaseCommonParametrageFicheWekaPeer::retrieveByPK($data['@attributes']['id'], $connexion);
                    if (!$parametrageFicheWeka instanceof CommonParametrageFicheWeka) {
                        $parametrageFicheWeka = new CommonParametrageFicheWeka();
                        $new = true;
                    }
                    $parametrageFicheWeka->setIdFiche($data['@attributes']['id']);
                    $parametrageFicheWeka->setUrl(str_replace('http://atexo.weka.fr', 'http://weka.local-trust.com', (new Atexo_Util())->toHttpEncoding($data['url'])));
                    $parametrageFicheWeka->setTitre(str_replace('', "'", (new Atexo_Util())->toHttpEncoding($data['title'])));
                    $parametrageFicheWeka->save($connexion);
                    if ($new) {
                        echo "\t Fiche WEKA : id = ".$parametrageFicheWeka->getIdFiche()." inséré avec succès \r\n";
                    } else {
                        echo "\t Fiche WEKA : id = ".$parametrageFicheWeka->getIdFiche()." mis à jour avec succès \r\n";
                    }
                }
            } catch (\Exception $ex) {
                echo $ex->getMessage();
            }
        }
    }

    /**
     * Permet de recuperer tous les enregistrements de la table 'Parametrage_Fiche_Weka'.
     */
    public static function recupererTousFicheWeka()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $fichesWeka = CommonParametrageFicheWekaPeer::doSelect($c, $connexion);

        return $fichesWeka;
    }

    /**
     * Permet de generer l'xml des regles weka et les inserer en base de données.
     */
    public static function genererXmlWeka()
    {
        try {
            $fichesWeka = self::recupererTousFicheWeka();
            $xmlReglesWeka = '<?xml version="1.0" encoding="ISO-8859-1"?>';
            $xmlReglesWeka .= '<pages>';
            if (is_array($fichesWeka) && count($fichesWeka)) {
                foreach ($fichesWeka as $fiche) {
                    $xmlFiche = $fiche->getXml();
                    $xmlFiche = str_replace('<pages>', '', $xmlFiche);
                    $xmlFiche = str_replace('</pages>', '', $xmlFiche);
                    $xmlReglesWeka .= $xmlFiche;
                }
            }
            $xmlReglesWeka .= '</pages>';
            if (!is_dir(Atexo_Config::getParameter('CHEMIN_DONNEES_WEKA'))) {
                system('mkdir '.Atexo_Config::getParameter('CHEMIN_DONNEES_WEKA'));
            }
            if (Atexo_Util::writeFile(Atexo_Config::getParameter('CHEMIN_DONNEES_WEKA').'weka_regles.xml', $xmlReglesWeka)) {
                return Atexo_Config::getParameter('CHEMIN_DONNEES_WEKA').'weka_regles.xml';
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }
}
