<?php

namespace Application\Service\Atexo\Weka;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe de génération des fichiers weka.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE4
 */
class Atexo_Weka_Generateur
{
    public static function generateWekaJs()
    {
        $tab = self::xmlFileToArray(Atexo_Config::getParameter('CHEMIN_DONNEES_WEKA').'weka_regles.xml');

        $pages = self::getArray($tab['page']);

        $inst = '';
        $i = 0;

        $instPages = [];

        foreach ($pages as $page) {
            $inst = '';

            $tabInstPage = self::addIfPage($page);

            $instPages[$tabInstPage[0]]['ifPage'] = $tabInstPage[1];

            $elements = $page['elements'];

            foreach (self::getArray($elements['element']) as $element) {
                $inst .= 'var elem'.$i." = document.getElementById('".self::getValeur($element['id'])."');";
                $inst .= 'if(elem'.$i.') {';

                if ('load' == self::getValeur($element['evenement'])) {
                    $inst .= 'verifAndShow(elem'.$i.",'".self::getValeur($element['propriete'])
                    ."','".self::getValeur($element['value'])."','".self::getValeur($element['idWeka'])."',isLoad);";
                } else {
                    $inst .= 'func = function () {verifAndShow(elem'.$i.",'".self::getValeur($element['propriete'])
                    ."','".self::getValeur($element['value'])."','".self::getValeur($element['idWeka'])."',true);};";
                    $inst .= 'addListnerElem(elem'.$i.",'".self::getValeur($element['evenement'])."',func);";
                }
                $inst .= '}';
                ++$i;
            }

            $instPages[$tabInstPage[0]]['elements'] .= $inst;
        }

        $inst = '';

        foreach ($instPages as $instPage) {
            $inst .= $instPage['ifPage'].'{'.$instPage['elements'].'}';
        }

        return $inst;
    }

    public static function addIfPage($page)
    {
        $inst = "if(getParamValue('page')=='".$page['nom']."'";
        $key = $page['nom'].'#';

        $params = $page['params_get'];

        if (is_array($page['params_get']) && count($page['params_get']) > 0) {
            foreach (self::getArray($params['param']) as $param) {
                $inst .= " && getParamValue('".self::getValeur($param['cle'])
                ."')=='".self::getValeur($param['value'])."'";

                $key .= self::getValeur($param['cle']).'#'.self::getValeur($param['value']).'#';
            }
        }

        $inst .= ')';

        return ['0' => $key, '1' => $inst];
    }

    /**
     * Permet de convertir un fichier xml en array.
     *
     * @param $xml : xml à convertir
     *
     * @return : le tableau
     */
    private static function xmlFileToArray($xmlFile)
    {
        return Atexo_Util::xmlFileToArray($xmlFile);
    }

    private static function getArray($liste)
    {
        if (is_array($liste[0])) {
            return $liste;
        }

        return [$liste];
    }

    private static function getValeur($value)
    {
        if (0 == (is_countable($value) ? count($value) : 0)) {
            return '';
        }

        return $value;
    }
}
