<?php

namespace Application\Service\Atexo;

use App\Entity\Entreprise;
use App\Service\User;
use Prado\Prado;
use Prado\Security\TAuthManager;
use Prado\Web\Services\TPageService;

/*
 * Using IUserManager interface
 */
Prado::using('System.Security.IUserManager');

class MpeTAuthManager extends TAuthManager
{
    public function onAuthenticate($param)
    {
        $application = $this->getApplication();

        if (!$application->getUser()) {
            /** @var User $userService */
            $userService = Atexo_Util::getSfService(User::class);
            $sfUser = $userService->getUser();

            if (!is_null($sfUser)) {
                $user = $this->getUserManager()->mapSfUserToPradoUser($sfUser);

                $this->updateSessionUser($user);
                $application->setUser($user);
            } else {
                parent::onAuthenticate($param);
            }

            if ($sfUser instanceof Entreprise) {
                Atexo_CurrentUser::setSiretAlert();
            }
        }
    }

    public function logout()
    {
        parent::logout();
    }

    public function leave($sender, $param)
    {
        $application = $this->getApplication();
        if ($application->getResponse()->getStatusCode() === 401) {
            $service = $application->getService();
            if ($service instanceof TPageService) {
                $returnUrl = $application->getRequest()->getRequestUri();

                if (mb_strpos($returnUrl, 'page=Agent.') !== false) {
                    $redirect = '/agent/login';
                } elseif (mb_strpos($returnUrl, 'page=Administration.') !== false) {
                    $redirect = '/admin/login';
                } else {
                    $redirect = '/';
                }

                $application->getResponse()->redirect($redirect);
            }
        }
    }
}
