<?php

namespace Application\Service\Atexo;

use Application\Service\Atexo\Consultation\Atexo_Consultation_Archive;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\Document\Atexo_Document_CriteriaVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Registres\Atexo_Registres_Questions;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_DocumentField;

/**
 * Classe de gestion de document.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2016
 *
 * @version 1.0
 *
 * @since 2015-place
 */
class Atexo_Document
{
    public function getDocumentFieldByInfo(
        $id,
        $nomFichier,
        $organisme,
        $idBlob,
        $content,
        $typeFichier,
        $tailleFichier = null,
        $pathFile = null,
        $chiffrement = null,
        $signature = null,
        $empreinte = null,
        $verificationSignature = null,
        $typeChecksum = 'md5',
        $jetonHorodatage = null
    ) {
        $documentField = new Atexo_Rest_ObjectField_DocumentField();
        $documentField->setIdFichier($id);
        $documentField->setNomFichier($nomFichier);
        $documentField->setTypeFichier($typeFichier);
        if (null == $pathFile && $idBlob) {
            $pathFile = Atexo_Blob::getPathFile($idBlob, $organisme);
        }
        if (!$tailleFichier) {
            if ($idBlob) {
                $tailleFichier = Atexo_Util::getTailleBlob($organisme, $idBlob, false);
            } elseif ($pathFile) {
                $tailleFichier = Atexo_Util::getTailleFichier($pathFile, 'o', false);
            }
        }
        $documentField->setTailleFichier($tailleFichier ?: 0);

        if ($pathFile) {
            $documentField->setChecksum(hash_file($typeChecksum, $pathFile));
        } elseif ($content) {
            $documentField->setChecksum(hash($typeChecksum, $content));
        }

        $documentField->setTypeChecksum(strtoupper($typeChecksum));
        if (null !== $jetonHorodatage) {
            $documentField->setJetonHorodatage($jetonHorodatage);
        }
        if (null !== $chiffrement) {
            $documentField->setChiffrement($chiffrement);
        }
        if (null !== $signature) {
            $documentField->setSignature($signature);
        }
        if (null !== $empreinte) {
            $documentField->setEmpreinte($empreinte);
        }
        if (null !== $verificationSignature) {
            $documentField->setVerificationCertificat($verificationSignature);
        }

        return $documentField;
    }

    /**
     * Permet de remplir et retourner le criteria de document.
     *
     * @param $consultationId
     * @param $organisme
     * @param $lot
     * @param $typeDocument
     *
     * @return Atexo_Document_CriteriaVo
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getCriteraiDocument($consultationId, $organisme, $lot, $typeDocument)
    {
        $criteriaVo = new Atexo_Document_CriteriaVo();
        $criteriaVo->setConsultationId($consultationId);
        $criteriaVo->setOrganisme($organisme);
        $criteriaVo->setLot($lot);
        $criteriaVo->setIdTypeDocument($typeDocument);

        return $criteriaVo;
    }

    public function doTratementGetDocument($phase, $typeDoc, $id, $organisme, &$xmlResponse, &$contentType, &$arrayBlob, &$logger, $typeChecksum, $chiffrer, $withFile, $lot)
    {
        $messageErreur = null;
        $phase = strtoupper($phase);
        $typeDoc = strtoupper($typeDoc);
        switch ($phase) {
            case 'DEPOT':
                switch ($typeDoc) {
                    case 'MPS':
                        $messageErreur = (new Atexo_MarchePublicSimplifie())->setInfoCandidatureMPSForWs($id, $xmlResponse, $arrayBlob, $logger, $organisme, $typeChecksum);
                        $contentType = 'application/pdf';
                        break;
                    case 'PLIS':
                        $messageErreur = (new Atexo_FichierEnveloppe())->setInfoPliForWs($id, $xmlResponse, $arrayBlob, $logger, $organisme, $chiffrer, $contentType, $withFile, $typeChecksum);
                        break;
                }
                break;
            case 'CONSULTATION':
                switch ($typeDoc) {
                    case 'DCE':
                        $messageErreur = (new Atexo_Consultation_Dce())->setInfoDCEForWs($id, $xmlResponse, $arrayBlob, $logger, $contentType, $organisme, $typeChecksum);
                        break;
                    case 'REGLEMENT_CONSULTATION':
                        $messageErreur = (new Atexo_Consultation_Rg())->setInfoRGForWs($id, $xmlResponse, $arrayBlob, $logger, $contentType, $organisme, $typeChecksum);
                        break;
                    case 'AVIS_PUBLICITE':
                        $messageErreur = (new Atexo_Publicite_Avis())->setInfoAvisForWs($id, $xmlResponse, $arrayBlob, $logger, $contentType, $organisme, $typeChecksum);
                        break;
                    case 'QUESTION':
                        $messageErreur = (new Atexo_Registres_Questions())->setInfoQuestionForWs($id, $xmlResponse, $arrayBlob, $logger, $contentType, $organisme, $typeChecksum);
                        break;
                }
                break;
            case 'ORGANISME':
                switch ($typeDoc) {
                    case 'ORG_LOGO_GRAND':
                        $messageErreur = (new Atexo_Organismes())->setInfoLogoOrganismeForWs($id, $xmlResponse, $arrayBlob, $logger, $contentType, true, $typeChecksum);
                        break;
                    case 'ORG_LOGO_PETIT':
                        $messageErreur = (new Atexo_Organismes())->setInfoLogoOrganismeForWs($id, $xmlResponse, $arrayBlob, $logger, $contentType, false, $typeChecksum);
                        break;
                }
                break;
            case 'ARCHIVE':
                switch ($typeDoc) {
                    case 'AUTRE_PIECE':
                        $messageErreur = (new Atexo_Consultation_Archive())->setInfoArchiveForWs($id, $organisme, $xmlResponse, $arrayBlob, $logger, $contentType, $typeChecksum);
                        break;
                    case 'BORDEREAU_ARCHIVE':
                        $messageErreur = (new Atexo_Consultation_Archive())->setInfoBordereauForWs($id, $organisme, $lot, $xmlResponse, $arrayBlob, $logger, $contentType);
                        break;
                }
                break;
        }

        return $messageErreur;
    }

    /**
     * Permet de retourner la taille en octet.
     *
     * @param string $val Taille à transformer en octet, ex: 10M.
     */
    public static function return_bytes(string $val): int
    {
        $val = trim($val);
        $last = strtolower($val[strlen($val)-1]);
        switch($last) {
            case 'g':
                $val *= (1024 * 1024 * 1024); //1073741824
                break;
            case 'm':
                $val *= (1024 * 1024); //1048576
                break;
            case 'k':
                $val *= 1024;
                break;
        }

        return $val;
    }

}
