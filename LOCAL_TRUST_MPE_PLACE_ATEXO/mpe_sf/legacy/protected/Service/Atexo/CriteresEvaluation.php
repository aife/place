<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonCriteresEvaluationPeer;
use Application\Propel\Mpe\CommonEnveloppeCritereEvaluation;
use Application\Propel\Mpe\CommonEnveloppeCritereEvaluationPeer;
use Application\Propel\Mpe\CommonEnveloppeFormulaireConsultation;
use Application\Propel\Mpe\CommonEnveloppeFormulaireConsultationPeer;
use Application\Propel\Mpe\CommonEnveloppeItemCritereEvaluation;
use Application\Propel\Mpe\CommonEnveloppeItemCritereEvaluationPeer;
use Application\Propel\Mpe\CommonItemCritereEvaluationPeer;
use Prado\Prado;

/*
 * Created on 4 oct. 2011
 *
 * by BOUKATAYA Fatima Zahra
 */

class Atexo_CriteresEvaluation
{
    public function deleteCommonCriteresEvaluation($organisme, $consultationId, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $c = new Criteria();
        $c->add(CommonCriteresEvaluationPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonCriteresEvaluationPeer::ORGANISME, $organisme);
        CommonCriteresEvaluationPeer::doDelete($c, $connexion);
    }

    public function isCritereExists($organisme, $consultationId, $typeEnveloppe, $typeCrit, $lot = false, $objet = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCriteresEvaluationPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonCriteresEvaluationPeer::ORGANISME, $organisme);
        $c->add(CommonCriteresEvaluationPeer::TYPE_ENVELOPPE, $typeEnveloppe);
        $c->add(CommonCriteresEvaluationPeer::TYPE_CRITERE, $typeCrit);
        if ($lot) {
            $c->add(CommonCriteresEvaluationPeer::LOT, $lot);
        }
        $formAccesAgnt = CommonCriteresEvaluationPeer::doSelectOne($c, $connexion);
        if ($formAccesAgnt) {
            if ($objet) {
                return $formAccesAgnt;
            } else {
                return true;
            }
        }

        return false;
    }

    public function getListeCriteresByTypeEnveloppe($consultationId, $organisme, $typeEnv, $others = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCriteresEvaluationPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonCriteresEvaluationPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->addAscendingOrderByColumn(CommonCriteresEvaluationPeer::LOT);

        if ($others) {
            $c->add(CommonCriteresEvaluationPeer::TYPE_ENVELOPPE, $typeEnv, Criteria::NOT_EQUAL);
        } else {
            $c->add(CommonCriteresEvaluationPeer::TYPE_ENVELOPPE, $typeEnv, Criteria::EQUAL);
        }

        $arrayForms = CommonCriteresEvaluationPeer::doSelect($c, $connexionCom);

        return $arrayForms;
    }

    public function getDescriptionLot($lot, $organisme, $consultationId)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonCategorieLotPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonCategorieLotPeer::LOT, $lot, Criteria::EQUAL);
        $categorieLot = CommonCategorieLotPeer::doSelectOne($c, $connexionCom);
        if ($categorieLot instanceof CommonCategorieLot) {
            return $categorieLot->getDescriptionTraduite();
        }
    }

    public function getTitleStatutForm($formCons, $organisme = false)
    {
        $statut = $formCons->getStatut();
        if ($statut == Atexo_Config::getParameter('STATUT_BROUILLON_NON_CONFORME')) {
            return Prado::localize('TEXT_BROUILLON_NON_CONFORME');
        }
        if ($statut == Atexo_Config::getParameter('STATUT_EN_COURS')) {
            return Prado::localize('EN_COURS');
        }
        if ($statut == Atexo_Config::getParameter('STATUT_FINI_VALIDE')) {
            return Prado::localize('TEXT_FINI_VALIDE');
        }
    }

    public function getPictoStatutForm($formCons, $organisme = false)
    {
        $statut = $formCons->getStatut();
        if ($statut == Atexo_Config::getParameter('STATUT_BROUILLON_NON_CONFORME')) {
            return Atexo_Config::getParameter('PICTO_FORM_BROUILLON_NON_CONFORME');
        }
        if ($statut == Atexo_Config::getParameter('STATUT_EN_COURS')) {
            return Atexo_Config::getParameter('PICTO_FORM_EN_COURS');
        }
        if ($statut == Atexo_Config::getParameter('STATUT_FINI_VALIDE')) {
            return Atexo_Config::getParameter('PICTO_FORM_FINI_VALIDE');
        }
    }

    public function retrieveCriteresEvaluationById($id, $org = false)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonCriteresEvaluationPeer::ID, $id);
        $c->add(CommonCriteresEvaluationPeer::ORGANISME, $org);
        $critereEval = CommonCriteresEvaluationPeer::doSelectOne($c, $connexionCom);
        if ($critereEval) {
            return $critereEval;
        } else {
            return false;
        }
    }

    public function retriveItemsByIdCritereEvaluation($idCritereEvaluation, $typeItem = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonItemCritereEvaluationPeer::IDCRITEREEVALUATION, $idCritereEvaluation);
        if ($typeItem) {
            $c->add(CommonItemCritereEvaluationPeer::TYPE_ITEM, $typeItem);
        }
        $c->addAscendingOrderByColumn(CommonItemCritereEvaluationPeer::NUMERO);
        $items = CommonItemCritereEvaluationPeer::doSelect($c, $connexionCom);

        return $items;
    }

    public function deleteItemsCritereEvaluation($idCritereEvaluation, $connexionCom)
    {
        $items = self::retriveItemsByIdCritereEvaluation($idCritereEvaluation);
        foreach ($items as $oneItem) {
            if (is_object($oneItem)) {
                $oneItem->delete($connexionCom);
            }
        }
    }

    public function retrieveCriteresEvaluationByRefConsAndTypeEnvAndLot($consultationId, $org, $typeEnv, $lot)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonCriteresEvaluationPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonCriteresEvaluationPeer::TYPE_ENVELOPPE, $typeEnv, Criteria::EQUAL);
        $c->add(CommonCriteresEvaluationPeer::LOT, $lot, Criteria::EQUAL);
        $c->add(CommonCriteresEvaluationPeer::ORGANISME, $org, Criteria::EQUAL);
        $critereEval = CommonCriteresEvaluationPeer::doSelectOne($c, $connexionCom);
        if ($critereEval) {
            return $critereEval;
        } else {
            return false;
        }
    }

    public function retriveItemsByIdCritereEvaluationWithValues($idCritereEvaluation, $typeItem, $idEnv)
    {
        $results = [];
        $items = self::retriveItemsByIdCritereEvaluation($idCritereEvaluation, $typeItem);
        //Verifier si ce critere a des valeurs pour cet enveloppe
        $enVEval = self::retreiveEnveloppeCriteval($idCritereEvaluation, $idEnv);
        if ($enVEval instanceof CommonEnveloppeCritereEvaluation) {
            foreach ($items as $one) {
                $itemValues = self::retreiveItemCritereEvaluationValues($one->getId(), $enVEval->getId());
                if ($itemValues instanceof CommonEnveloppeItemCritereEvaluation) {
                    $one->setReponseFournisseur($itemValues->getReponseFournisseur());
                    $one->setNote($itemValues->getNote());
                    $one->setCommentaireAcheteur($itemValues->getCommentaireAcheteur());
                    $one->setIdenveloppecritereevaluation($itemValues->getIdenveloppecritereevaluation());
                }
                $results[] = $one;
            }
        } else {
            $results = $items;
        }

        return $results;
    }

    public function retreiveItemCritereEvaluationValues($idItemCritereEval, $idEnvEval)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEnveloppeItemCritereEvaluationPeer::IDITEMCRITEREEVALUATION, $idItemCritereEval, Criteria::EQUAL);
        $c->add(CommonEnveloppeItemCritereEvaluationPeer::IDENVELOPPECRITEREEVALUATION, $idEnvEval, Criteria::EQUAL);
        $itemValues = CommonEnveloppeItemCritereEvaluationPeer::doSelectOne($c, $connexionCom);

        return $itemValues ?: false;
    }

    public function updateEnveloppeCriteresEvaluation($statutForm, $idCrtreval, $idAgent, $totalNote, $rejet, $totalCommentAchtr, $idEnv)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $evalCritEval = self::retreiveEnveloppeCriteval($idCrtreval, $idEnv);
        if (!$evalCritEval) {
            $evalCritEval = new CommonEnveloppeCritereEvaluation();
        }
        $evalCritEval->setIdEnveloppe($idEnv);
        $evalCritEval->setIdcritereevaluation($idCrtreval);
        if ('' !== $rejet) {
            $evalCritEval->setRejet($rejet);
        }
        $evalCritEval->setStatutCritereEvaluation($statutForm);
        $evalCritEval->setNoteTotale($totalNote);
        $evalCritEval->setCommentaireTotal($totalCommentAchtr);
        $evalCritEval->setIdAgent($idAgent);
        $evalCritEval->save($connexionCom);

        return $evalCritEval->getId();
    }

    public function retreiveEnveloppeCriteval($idCritevl, $idEnv)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEnveloppeCritereEvaluationPeer::IDCRITEREEVALUATION, $idCritevl, Criteria::EQUAL);
        $c->add(CommonEnveloppeCritereEvaluationPeer::ID_ENVELOPPE, $idEnv, Criteria::EQUAL);
        $criteval = CommonEnveloppeCritereEvaluationPeer::doSelectOne($c, $connexionCom);

        return $criteval ?: false;
    }

    /**
     * Permet de recuperer les objets de "EnveloppeFormulaireConsultation" par l'id_enveloppe.
     *
     * @param int $idEnv: Identifiant de l'enveloppe
     */
    public function retreiveEnveloppeFormulaireConsultationByIdEnv($idForm, $idPli = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEnveloppeFormulaireConsultationPeer::ID_FORMULAIRE_CONSULTATION, $idForm, Criteria::EQUAL);
        if ($idPli) {
            $c->add(CommonEnveloppeFormulaireConsultationPeer::ID_ENVELOPPE, $idPli, Criteria::EQUAL);
        }
        $envFormCons = CommonEnveloppeFormulaireConsultationPeer::doSelectOne($c, $connexionCom);

        return ($envFormCons instanceof CommonEnveloppeFormulaireConsultation) ? $envFormCons : false;
    }

    /**
     * Enter description here ...
     *
     * @param unknown_type $idEnv
     */
    public function retreiveEnveloppeCritevalByIdEnv($idEnv)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEnveloppeCritereEvaluationPeer::ID_ENVELOPPE, $idEnv, Criteria::EQUAL);
        $criteval = CommonEnveloppeCritereEvaluationPeer::doSelectOne($c, $connexionCom);

        return $criteval ?: false;
    }

    /*
     * Permet de mettre à jour un enveloppe critère evaluation
     * @param $idEnvCritereEval = Identifiant de l'enveloppe critère d'evaluation,$statutForm = statut de l'enveloppe
     * @param $totaleNote = la note totale, $totalCommentAchtr = Commentaire Total
     */
    public function updateEnveloppeCriteresEvaluationById($idEnvCritereEval, $statutForm, $totaleNote, $totalCommentAchtr)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $envCritEval = CommonEnveloppeCritereEvaluationPeer::retrieveByPK($idEnvCritereEval, $connexionCom);
        if ($envCritEval instanceof CommonEnveloppeCritereEvaluation) {
            $envCritEval->setStatutCritereEvaluation($statutForm);
            $envCritEval->setNoteTotale($totaleNote);
            $envCritEval->setCommentaireTotal($totalCommentAchtr);
            $envCritEval->save($connexionCom);
        }
    }

    /*supprime  un critere d'evaluation  par un ensebme de paramatres*/

    public function deleteCriteresEvaluation($org, $lot, $typeEnv, $typeCritere, $consultationId)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonCriteresEvaluationPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonCriteresEvaluationPeer::ORGANISME, $org);
        if ($lot) {
            $c->add(CommonCriteresEvaluationPeer::LOT, $lot);
        }
        if ($typeCritere) {
            $c->add(CommonCriteresEvaluationPeer::TYPE_CRITERE, $typeCritere);
        }
        $c->add(CommonCriteresEvaluationPeer::TYPE_ENVELOPPE, $typeEnv);

        CommonCriteresEvaluationPeer::doDelete($c, $connexion);
    }

    /*Supprime les criteres d'evaluation que leurs lots ont éte supprimés */

    public function deleteCritereConsHasntlot($allLots, $consultationId, $org = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $chaineLot = [];
        $c = new Criteria();
        foreach ($allLots as $oneLot) {
            $chaineLot[] = $oneLot->getLot();
        }
        $chaineLot[] = '0';
        $query = 'lot not in ('.implode(',', $chaineLot).')';
        $c->add(CommonCriteresEvaluationPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonCriteresEvaluationPeer::ORGANISME, $org);
        $c->add(CommonCriteresEvaluationPeer::LOT, $query, Criteria::CUSTOM);
        CommonCriteresEvaluationPeer::doDelete($c, $connexion);
    }
}
