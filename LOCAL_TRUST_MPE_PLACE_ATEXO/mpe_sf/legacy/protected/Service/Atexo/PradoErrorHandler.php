<?php

namespace Application\Service\Atexo;

use Prado\Exceptions\TErrorHandler;
use Prado\Exceptions\THttpException;
use Prado;
use Prado\TApplicationMode;
use Propel\Runtime\Exception\PropelException;

class PradoErrorHandler extends TErrorHandler
{
    protected function getExceptionTemplate($exception)
    {
        $lang = Prado::getPreferredLanguage();
        $exceptionFile = $this->getErrorTemplatePath().'/'.self::EXCEPTION_FILE_NAME.'-'.$lang.'.html';
        if (!is_file($exceptionFile)) {
            $exceptionFile = $this->getErrorTemplatePath().'/'.self::EXCEPTION_FILE_NAME.'.html';
        }
        if (($content = @file_get_contents($exceptionFile)) === false) {
            exit("Unable to open exception template file '$exceptionFile'.");
        }

        return $content;
    }

    public function handleError($sender, $param)
    {
        static $handling = false;
        // We need to restore error and exception handlers,
        // because within error and exception handlers, new errors and exceptions
        // cannot be handled properly by PHP
        restore_error_handler();
        restore_exception_handler();
        // ensure that we do not enter infinite loop of error handling
        if ($handling) {
            $this->handleRecursiveError($param);
        } else {
            $handling = true;
            if (($response = $this->getResponse()) !== null) {
                $response->clear();
            }
            if (!headers_sent()) {
                header('Content-Type: text/html; charset=UTF-8');
            }
            if (TApplicationMode::Debug !== $this->getApplication()->getMode()
                && ($param instanceof \SQLiteException || $param instanceof \PDOException || $param instanceof PropelException)
               ) {
                $this->handleSQLError(500, $param);
            } elseif ($param instanceof THttpException) {
                $this->handleExternalError($param->getStatusCode(), $param);
            } elseif (TApplicationMode::Debug === $this->getApplication()->getMode()) {
                $this->displayException($param);
            } elseif ('cli' === php_sapi_name()) {
                $this->displayException($param);
            } else {
                if (str_contains($param->getMessage(), 'TActiveDropDownList')) {
                    if (extension_loaded('apc') && ini_get('apc.enabled')) {
                        apc_clear_cache();
                    }
                }
                $this->handleExternalError(500, $param);
            }
        }
    }

    protected function handleSQLError($statusCode, $exception)
    {
        if (!($exception instanceof THttpException)) {
            error_log($exception->__toString());
        }
        $message = 'SQL ERROR';
        if ($exception instanceof \PDOException) {
            $message = 'DATABASE ERROR';
        } elseif ($exception instanceof PropelException) {
            $message = 'DB ERROR';
        }

        $content = $this->getErrorTemplate($statusCode, $exception);

        $serverAdmin = $_SERVER['SERVER_ADMIN'] ?? '';
        $tokens = [
            '%%StatusCode%%' => "$statusCode",
            '%%ErrorMessage%%' => htmlspecialchars($message),
            '%%ServerAdmin%%' => $serverAdmin,
            '%%Version%%' => $_SERVER['SERVER_SOFTWARE'].' <a href="http://www.pradosoft.com/">PRADO</a>/'.Prado::getVersion(),
            '%%Time%%' => @strftime('%Y-%m-%d %H:%M', time()),
        ];
        echo strtr($content, $tokens);
    }
}
