<?php

namespace Application\Service\Atexo\Db;

use Application\Service\Atexo\Atexo_Exception;
use Exception;

/**
 * Exception spécifique du composant Db d'Atexo.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Db_Exception extends Atexo_Exception
{
}
