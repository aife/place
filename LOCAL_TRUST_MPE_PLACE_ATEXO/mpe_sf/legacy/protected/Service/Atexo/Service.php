<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonServiceQuery;

/**
 * Class Atexo_Service.
 */
class Atexo_Service
{
    /**
     * retourne un service avec son id.
     *
     * @param $serviceId
     *
     *
     * @throws PropelException
     */
    public static function retrieveServiceById($serviceId): bool|\Application\Propel\Mpe\CommonService
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonServicePeer::ID, $serviceId);
        $service = CommonServicePeer::doSelectOne($criteria, $connexion);

        if ($service instanceof CommonService) {
            return $service;
        }

        return false;
    }

    /**
     * retourne un service avec son sigle.
     *
     * @param $serviceId
     *
     *
     * @throws PropelException
     */
    public static function retrieveServiceBySigle($sigle): bool|\Application\Propel\Mpe\CommonService
    {
        $serviceQuery = new CommonServiceQuery();

        return $serviceQuery->getServiceBySigle($sigle);
    }

    /**
     * retourne un service avec son id externe.
     *
     * @param $idExterne
     *
     * @return CommonService|null
     */
    public static function retrieveServiceByIdExterne($idExterne)
    {
        $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $serviceQuery = new CommonServiceQuery();

        return $serviceQuery->filterByIdExterne($idExterne)->findOne($cnx);
    }

    /**
     * retourne les services avec leurs id externe.
     *
     * @param $idExterne
     * @param $acronyme
     *
     * @return CommonService|null
     */
    public static function retrieveServicesByIdExterne($idExterne, $acronyme, $idEntite = null, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                .
                Atexo_Config::getParameter('CONST_READ_ONLY'));
        }

        $criteria = new Criteria();
        if (!is_null($idEntite)) {
            $criteria->add(CommonServicePeer::ID, $idEntite, Criteria::NOT_EQUAL);
        }
        $criteria->add(CommonServicePeer::ORGANISME, $acronyme);
        $criteria->add(CommonServicePeer::ID_EXTERNE, $idExterne);

        return CommonServicePeer::doSelect($criteria, $connexion);
    }

    public static function retrieveServiceByListId($arServices, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
            .Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonServicePeer::ID, $arServices, Criteria::IN);
        $criteria->add(CommonServicePeer::ORGANISME, $organisme);
        $services = CommonServicePeer::doSelect($criteria, $connexion);

        if (is_array($services) && count($services) > 0) {
            return $services;
        }

        return false;
    }
}
