<?php

namespace Application\Service\Atexo;

use App\Utils\Encryption;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonDocumentServeurDocsQuery;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonTInformationModificationPasswordQuery;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\Exception\ExceptionDceLimit;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use AtexoPdf\PdfGeneratorClient;
use DateTime;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Contient des fonctions non dépendante du projet tel que la convertion des dates (iso -> fr) etc ...
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 * @TODO : elenver les getApplications->parameters['']
 */
class Atexo_Util
{
    const FC_CALLBACK = 'fc-login';

    protected static string $bodyclass = '';
    protected static bool $nonav = false;
    private array $_month = [' ', 'Janvier', 'F&eacute;vrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao&ucirc;t', 'Septembre', 'Octobre', 'Novembre', 'D&eacute;cembre'];

    protected static ?array $monthAbbreviation = null;
    protected static ?array $month = null;

    public static function setBodyClass($bodyclass)
    {
        static::$bodyclass = $bodyclass;
    }

    public static function getBodyClass()
    {
        return static::$bodyclass;
    }

    public static function setNonav($nonav)
    {
        static::$nonav = $nonav;
    }

    public static function getNonav()
    {
        return static::$nonav;
    }

    public static function getMonthAbbreviation()
    {
        if (!isset(self::$monthAbbreviation)) {
            self::$monthAbbreviation = [' '];
            array_push(
                self::$monthAbbreviation,
                Prado::localize('JS_MONTH1'),
                Prado::localize('JS_MONTH2'),
                Prado::localize('JS_MONTH3'),
                Prado::localize('JS_MONTH4'),
                Prado::localize('JS_MONTH5'),
                Prado::localize('JS_MONTH6'),
                Prado::localize('JS_MONTH7'),
                Prado::localize('JS_MONTH8'),
                Prado::localize('JS_MONTH9'),
                Prado::localize('JS_MONTH10'),
                Prado::localize('JS_MONTH11'),
                Prado::localize('JS_MONTH12')
            );
        }

        return self::$monthAbbreviation;
    }

    public static function getMonth()
    {
        if (!isset(self::$month)) {
            self::$month = [' '];
            array_push(
                self::$month,
                Prado::localize('JS_MONTH1'),
                Prado::localize('JS_MONTH2'),
                Prado::localize('JS_MONTH3'),
                Prado::localize('JS_MONTH4'),
                Prado::localize('JS_MONTH5'),
                Prado::localize('JS_MONTH6'),
                Prado::localize('JS_MONTH7'),
                Prado::localize('JS_MONTH8'),
                Prado::localize('JS_MONTH9'),
                Prado::localize('JS_MONTH10'),
                Prado::localize('JS_MONTH11'),
                Prado::localize('JS_MONTH12')
            );
        }

        return self::$month;
    }

    public static function getDateCalender($_date)
    {
        $date = [];
								$_atexo_utils = new Atexo_Util();

        $date['date'] = $_date;
        $date['num_month'] = date('n', strtotime($_date));
        $date['day'] = date('j', strtotime($_date));
        $date['month'] = $_atexo_utils->_month[$date['num_month']];
        $date['month_abbreviation'] = $_atexo_utils->getMonthAbbreviation()[$date['num_month']];
        $date['year'] = date('Y', strtotime($_date));
        $date['heure'] = date('H', strtotime($_date));
        $date['minutes'] = date('i', strtotime($_date));
        $date['heure_minutes'] = $date['heure'].':'.$date['minutes'];

        return $date;
    }

    public static function htmlspecialchars($text)
    {
        return htmlspecialchars($text, ENT_COMPAT, 'ISO-8859-1');
    }

    public static function translateHtmlspecialchars($id)
    {
        return htmlspecialchars(Prado::localize($id), ENT_COMPAT, 'ISO-8859-1');
    }

    public static function getMailRefusValidation($reference)
    {
        return Prado::localize('MADAME_MONSIEUR').' '.$reference.Prado::localize('SUITE_MADAME_MONSIEUR').' ';
    }

    /**
     * Créer un dossier sur le disque.
     *
     * @param string $directory Dossier a créer
     * @param int    $mode      Droit UNIX du dossier
     *
     * @return bool Réussite de la création du dossier
     */
    public static function createDirectory($directory, $mode = 0755, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('app');
        }
        if (mkdir($directory, $mode, true)) {
            Prado::log('Création dossier '.$directory, TLogger::INFO, __METHOD__);
            $logger->info("Repertoire '$directory' cree avec  *** SUCCES *** ");

            return true;
        } else {
            Prado::log('ECHEC Création dossier '.$directory, TLogger::ERROR, __METHOD__);
            $logger->error("Impossible de creer le repertoire '$directory' , mode = $mode");
            throw new Atexo_Exception("Impossible de creer le dossier $directory");
        }
    }

    /**
     * Fonction qui supprime recursivement un répertoire et ses sous éléments
     * Permet de compléter la fonction removeDir qui ne fonctionne pas pour les fonction impressionODT.php.
     *
     * @param $dirname
     *
     * @return bool
     */
    public static function removeDirectoryRecursively($dirname)
    {
        $dir_handle = null;
								if (is_dir($dirname)) {
            $dir_handle = @opendir($dirname);
        }
        if (!$dir_handle) {
            return false;
        }
        while ($file = readdir($dir_handle)) {
            if ('.' != $file && '..' != $file) {
                if (!is_dir($dirname . '/'.$file)) {
                    @unlink($dirname . '/' . $file);
                } else {
                    self::removeDirectoryRecursively($dirname.'/'.$file);
                }
            }
        }
        closedir($dir_handle);
        @rmdir($dirname);

        return true;
    }

    /**
     * Retourne un tableau associatif array[$source] = $target d'un fichier de libellé Xliff.
     *
     * @param string $xliff_file Fichier XML Xliff de libellé
     *
     * @return array Tableau associatif des libellés
     */
    public static function XliffToArray($xliff_file)
    {
        $dom = new \DOMDocument();
        $dom->load($xliff_file);
        $sourceElements = $dom->getElementsByTagName('source');
        $length = $sourceElements->length;
        $tmp = [];
        for ($i = 0; $i < $length; ++$i) {
            $node = $sourceElements->item($i);
            if ('target' == $node->parentNode->lastChild->nodeName) {
                $tmp[$node->firstChild->nodeValue] = $node->parentNode->lastChild->nodeValue;
            } elseif ('target' == $node->nextSibling->nodeName) {
                $tmp[$node->firstChild->nodeValue] = $node->nextSibling->nodeValue;
            } elseif ('target' == $node->parentNode->lastChild->previousSibling->nodeName) {
                $tmp[$node->firstChild->nodeValue] = $node->parentNode->lastChild->previousSibling->nodeValue;
            }
        }

        return $tmp;
    }

    /**
     * Retourne l'URL complète dynamiquement.
     */
    public static function getAbsoluteUrl()
    {
        $protocol = ('on' == $_SERVER['HTTPS']) ? 'https://' : 'http://';
        try {
            if ('ASIS' != Atexo_Config::getParameter('FORCE_PROTOCOL_FOR_AUTO_URL')) {
                $protocol = Atexo_Config::getParameter('FORCE_PROTOCOL_FOR_AUTO_URL');
            }
        } catch (\Exception $e) {
            Prado::log('Erreur Util.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Util.php');
        }
        $folder = str_replace('/index.php5', '', $_SERVER['PHP_SELF']);
        $folder = str_replace('/index.php', '', $folder);

        return $protocol.$_SERVER['HTTP_HOST'].$folder.'/';
    }

    public static function getCurrentUrl()
    {
        return self::getAbsoluteUrl().'?' . $_SERVER['QUERY_STRING'];
    }

    public static function isDecimal($valeur)
    {
        return preg_match("/^[0-9]+([\.,]?[0-9]+)?$/", $valeur);
    }

    public static function isEntier($valeur)
    {
        return preg_match('/^([0-9]+)$/', $valeur);
    }

    /**
     * retourne un nombre arrondit avec à 2 EX : 12454,5646 => 12454,56.
     * @param $montant decimal au format xxxxx.xxxxxx
     *
     * @return decimal au format xxxxxx,xx
     */
    public static function getMontantArrondit($montant, $apresVirgule = 2)
    {
        if (empty($montant)) {
            return '0,00 ';
        }
        if (!self::isEntierRelatif($montant)) {
            return $montant;
        }
        $montant = str_replace(',', '.', $montant);

        $montant = str_replace('.', ',', sprintf('%.'.$apresVirgule.'f', $montant));

        return $montant;
    }

    public static function getMontantArronditS($montant, $apresVirgule = 2)
    {
        if (empty($montant)) {
            return '0,00';
        }
        $montant = str_replace(',', '.', $montant);

        return str_replace('.', ',', sprintf('%.'.$apresVirgule.'f', $montant));
    }

    /**
     * convertit une date ISO en date française.
     * @param $_isoDate date au format yyyy-mm-dd
     *
     * @return date au format dd/mm/yyyy
     */
    public static function iso2frnDate($_isoDate)
    {
        if (!$_isoDate || '0000-00-00' == $_isoDate || '0000-00-00 00:00:00' == $_isoDate) {
            return;
        }
        if (strstr($_isoDate, '-')) {
            $_isoDate = date('Y-m-d', strtotime($_isoDate));
            [$y, $m, $d] = explode('-', $_isoDate);

            return "$d/$m/$y";
        } else {
            return $_isoDate;
        }
    }

    /**
     * convertit une date française en date ISO.
     * @param $_frnDate date au format dd/mm/yyyy
     *
     * @return date au format yyyy-mm-dd
     */
    public static function frnDate2iso($_frnDate)
    {
        if (!$_frnDate) {
            return;
        }
        if (strstr($_frnDate, '/')) {
            [$d, $m, $y] = explode('/', $_frnDate);

            return "$y-$m-$d";
        } else {
            return $_frnDate;
        }
    }

    public static function getJustReference($reference)
    {
        $tab_ref = explode(' - ', $reference);
        $result = $tab_ref[0];
        $count = count($tab_ref) - 2;
        for ($i = 1; $i < $count; ++$i) {
            $result .= ' - '.$tab_ref[$i];
        }

        return $result;
    }

    /**
     * convertit une datetime ISO en date française.
     * @param $_isoDate date au format yyyy-mm-dd hh:mm:ss
     *
     * @return date au format dd/mm/yyyy
     */
    public static function iso2frnDateTime($_isoDate, $withTime = true, $withTimeSeconds = false)
    {
        if (!$_isoDate || '0000-00-00 00:00:00' == $_isoDate) {
            return;
        }
        if (10 == strlen($_isoDate)) {
            return self::iso2frnDate($_isoDate);
        }

        [$a, $m, $j_et_heure] = explode('-', $_isoDate);
        [$j, $heure] = explode(' ', $j_et_heure);
        [$h, $min, $sec] = explode(':', $heure);

        $date = "$j/$m/$a";
        if ($withTime) {
            $date = "$j/$m/$a $h:$min";
        }
        if ($withTimeSeconds) {
            $sec = empty($sec) ? '00' : $sec;
            $date = "$j/$m/$a $h:$min:$sec";
        }

        return $date;
    }

    /**
     * convertit une datetime ISO en string date sans separateurs
     * (utilisable dans un nom de fichier).
     *
     * @param $_isoDate date au format yyyy-mm-dd hh:mm:ss
     *
     * @return date au format ddmmyyyyhhmm
     */
    public static function iso2frnDateTimeNoSep($_isoDate)
    {
        if (!$_isoDate || '0000-00-00 00:00:00' == $_isoDate) {
            return;
        }
        if (10 == strlen($_isoDate)) {
            return self::iso2frnDate($_isoDate);
        }

        [$a, $m, $j_et_heure] = explode('-', $_isoDate);
        [$j, $heure] = explode(' ', $j_et_heure);
        [$h, $min, $sec] = explode(':', $heure);

        return "$j$m$a$h$min";
    }

    /**
     * convertit une datetime ISO en string date sans separateurs
     * (utilisable dans un nom de fichier).
     *
     * @param $_isoDate date au format yyyy-mm-dd hh:mm:ss
     *
     * @return date au format ddmmyyyy_hhmm
     */
    public static function iso2frnDateTimeUnderScore($_isoDate)
    {
        if (!$_isoDate || '0000-00-00 00:00:00' == $_isoDate) {
            return;
        }
        if (10 == strlen($_isoDate)) {
            return self::iso2frnDate($_isoDate);
        }

        [$a, $m, $j_et_heure] = explode('-', $_isoDate);
        [$j, $heure] = explode(' ', $j_et_heure);
        [$h, $min, $sec] = explode(':', $heure);

        return "$j$m$a$h$min";
    }

    /**
     * convertit une datetime ISO en string date sans separateurs
     * (utilisable dans un nom de fichier).
     *
     * @param $_isoDate date au format yyyy-mm-dd hh:mm:ss
     *
     * @return date au format ddmmyyyyhhmm
     */
    public static function iso2iso8601DateTime($_isoDate)
    {
        if (!$_isoDate || '0000-00-00 00:00:00' == $_isoDate) {
            return;
        }
        [$a, $m, $j_et_heure] = explode('-', $_isoDate);
        [$j, $heure] = explode(' ', $j_et_heure);
        [$h, $min, $sec] = explode(':', $heure);

        $h = $h ?: 0;
        $min = $min ?: 0;
        $sec = $sec ?: 0;

        $dateTime = mktime($h, $min, $sec, $m, $j, $a);
        $dateIso = date('c', $dateTime);

        return $dateIso;
    }

    /**
     * retourne 1 si datefin est superieur a datedebut
     * retourne 0 si datedebut et datefin sont egales
     * retourne -1 si datefin est inferieur a datedebut.
     *
     * @param $dateDebut
     * @param $dateFin
     * @return int
     *
     * @author Gregory CHEVRET <gregory.chevret@atexo.com>
     */
    public static function diffDate($dateDebut, $dateFin)
    {
        $datetime1 = new \DateTime(self::frnDate2iso($dateDebut));
        $datetime2 = new \DateTime(self::frnDate2iso($dateFin));
        $interval = $datetime1->diff($datetime2);

        $intervalByDay = (int) $interval->format('%R%a');

        return ($intervalByDay >= 1) ? 1 : ($intervalByDay < 0 ? -1 : 0);
    }

    //retourne la différence des jours en nombre positif entre les deux dates
    public static function diffJours($date1, $date2)
    {
        if (!$date1 || !$date2) {
            return;
        }
        $chiffreDate1 = explode('/', $date1); // On tri les infos
        $chiffreDate2 = explode('/', $date2); // On tri les infos

        $time_date1 = mktime(
            0,
            0,
            0,
            $chiffreDate1[1],
            $chiffreDate1[0],
            $chiffreDate1[2]
        ); // On recupere sa date de naissance en timestamp
        $time_date2 = mktime(
            0,
            0,
            0,
            $chiffreDate2[1],
            $chiffreDate2[0],
            $chiffreDate2[2]
        ); // On recupere sa date de reference en timestamp
        $seconde = abs($time_date1 - $time_date2);
        $seconde_par_jours = (24 * 60 * 60); // On calcule le nombre de secondes

        return $seconde / $seconde_par_jours;
    }

    //retourne la différence des mois en nombre positif entre les deux dates
    public static function diffMois($date1, $date2)
    {
        $nbrjourDiff = self::diffJours(self::iso2frnDate($date1), self::iso2frnDate($date2));
        $nbreMois = $nbrjourDiff / 30;

        return $nbreMois;
    }

    //retourne la différence des jours en nombre positif ou négatif entre les deux dates
    public static function differenceJours($date1, $date2)
    {
        if (!$date1 || !$date2) {
            return;
        }
        $chiffreDate1 = explode('/', $date1); // On tri les infos
        $chiffreDate2 = explode('/', $date2); // On tri les infos

        $time_date1 = mktime(
            0,
            0,
            0,
            $chiffreDate1[1],
            $chiffreDate1[0],
            $chiffreDate1[2]
        ); // On recupere sa date de naissance en timestamp
        $time_date2 = mktime(
            0,
            0,
            0,
            $chiffreDate2[1],
            $chiffreDate2[0],
            $chiffreDate2[2]
        ); // On recupere sa date de reference en timestamp
        $seconde = $time_date1 - $time_date2;
        $seconde_par_jours = (24 * 60 * 60); // On calcule le nombre de secondes

        return $seconde / $seconde_par_jours;
    }

	/**
     * convertit une date française en date ISO.
     * @param $_frnDate date au format dd/mm/yyyy H:i:s
     *
     * @return date au format yyyy-mm-dd H:i:s
     */
    public static function frnDateTime2iso($_frnDate)
    {
        if (!$_frnDate) {
            return;
        }
        if (10 == strlen($_frnDate)) {
            return self::frnDate2iso($_frnDate);
        }
        [$j, $m, $annee_et_heure] = explode('/', $_frnDate);
        [$annee, $heure] = explode(' ', $annee_et_heure);
        [$h, $min, $sec] = explode(':', $heure);
        if (!$sec) {
            $sec = '00';
        }

        return "$annee-$m-$j $h:$min:$sec";
    }

    /**
     * @name isSiretValide
     * @param Le code SIRET dont on veut vérifier la validité.
     * @return     Un booléen qui vaut 'true' si le code SIRET passé en
     * paramètre est valide, false sinon.
     **/
    public static function isSiretValide($numSiret)
    {
        if (!(is_numeric($numSiret)) || (14 != strlen($numSiret))) {
            $estValide = false;
        } elseif ('356000000' == substr($numSiret, 0, 9)) {
            $estValide = true;
        } else {
            // Donc le SIRET est un numérique à 14 chiffres
            // Les 9 premiers chiffres sont ceux du SIREN (ou RCS), les 4 suivants
            // correspondent au numéro d'établissement
            // et enfin le dernier chiffre est une clef de LUHN.
            $somme = 0;
            for ($cpt = 0; $cpt < strlen($numSiret); ++$cpt) {
                if (($cpt % 2) == 0) { // Les positions impaires : 1er, 3è, 5è, etc...
                    $tmp = intval(substr($numSiret, $cpt, 1)) * 2; // On le multiplie par 2
                    if ($tmp > 9) {
                        $tmp -= 9; // Si le résultat est supérieur à 9, on lui soustrait 9
                    }
                } else {
                    $tmp = intval(substr($numSiret, $cpt, 1));
                }
                $somme += intval($tmp);
            }
            if (($somme % 10) == 0) {
                $estValide = true; // Si la somme est un multiple de 10 alors le SIRET est valide
            } else {
                $estValide = false;
            }
        }

        return $estValide;
    }

    public static function isSirenValide($numSiren)
    {
        //return true;
        if (!(is_numeric($numSiren)) || (9 != strlen($numSiren))) {
            $estValide = false;
        } elseif ('356000000' == $numSiren) {
            $estValide = true;
        } else {
            // Donc le SIRET est un numérique à 14 chiffres
            // Les 9 premiers chiffres sont ceux du SIREN (ou RCS), les 4 suivants
            // correspondent au numéro d'établissement
            // et enfin le dernier chiffre est une clef de LUHN.
            $somme = 0;
            for ($cpt = 0; $cpt < strlen($numSiren); ++$cpt) {
                if (($cpt % 2) != 0) { // Les positions impaires : 1er, 3è, 5è, etc...
                    $tmp = intval(substr($numSiren, $cpt, 1)) * 2; // On le multiplie par 2
                    if ($tmp > 9) {
                        $tmp -= 9; // Si le résultat est supérieur à 9, on lui soustrait 9
                    }
                } else {
                    $tmp = intval(substr($numSiren, $cpt, 1));
                }
                $somme += intval($tmp);
            }
            if (($somme % 10) == 0) {
                $estValide = true; // Si la somme est un multiple de 10 alors le SIRET est valide
            } else {
                $estValide = false;
            }
        }

        return $estValide;
    }

    //Vérification du RIB

    public static function isRIBvalide($p_bankCode, $p_agencyCode, $p_accountNumber, $p_keyCode)
    {
        if ((5 != strlen($p_bankCode)) || (5 != strlen($p_agencyCode)) || (11 != strlen($p_accountNumber)) || (2 != strlen(
                    $p_keyCode
                ))) {
            return false;
        }

        return static::getKeyCode($p_bankCode, $p_agencyCode, $p_accountNumber) == intval($p_keyCode);
    }

    /// Calcule la clé RIB à partir des code banque, code agence et numéro de compte
    /// <param name="p_bankCode">Code banque (5 chiffres)</param>
    /// <param name="p_agencyCode">Code agence (5 chiffres)</param>
    /// <param name="p_accountNumber">Numéro de compte (11 caractères)</param>
    /// <returns>La clé RIB calculéé</returns>

    public static function getKeyCode($p_bankCode, $p_agencyCode, $p_accountNumber)
    {
        // Conversion du numero de compte en chiffres
        $tab = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $tab1 = '123456789123456789234567890123456789';
        $p_accountNumber = strtoupper($p_accountNumber);

        //echo $p_bankCode." ".$p_agencyCode." ".$p_accountNumber;

        if (!ctype_digit($p_accountNumber)) {
            $tab1 = str_split($tab1);
            $p_accountNumber1 = str_split($p_accountNumber);
            for ($i = 0; $i < strlen($p_accountNumber); ++$i) {
                if (!ctype_digit($p_accountNumber1[$i])) {
                    $p_accountNumber1[$i] = $tab1[strpos($tab, $p_accountNumber1[$i])];
                }
            }
            $p_accountNumber = implode($p_accountNumber1);
        }

        $v_cp = $p_accountNumber;
        $v_bankCode = intval($p_bankCode);
        //$v_agencyCode = intval($p_agencyCode);

        $a = $v_bankCode % 97;
        $a = $a * 100000 + intval($p_agencyCode);
        $a = $a % 97;
        $a = $a * 10 ** 11 + $v_cp;

        $b = $a / 97; //ces 3 instructions  vaut $a=$a%97
        $b = intval($b) * 97;
        $a = $a - $b;

        $a = intval($a) * 100;
        $a = intval($a) % 97;
        $a = 97 - intval($a);

        return intval($a);
    }

    public static function extraireLogin($DN)
    {
        $tabDN = explode(',', $DN);
        $sn = $tabDN[0];
        $tabSN = explode('=', $sn);

        return $tabSN[1];
    }

    public static function isDate($chaine)
    {
        [$d, $m, $y] = explode('/', $chaine);

        if (4 != strlen($y) || 2 != strlen($d) || 2 != strlen($m)) {
            return false;
        }
        if ((!self::isEntier($d)) || (!self::isEntier($m)) || (!self::isEntier($y))) {
            return false;
        }
        if ($m > 12 || $d > 31) {
            return false;
        }

        return checkdate($m, $d, $y);
    }

    public static function calculAge($dateNaissance, $reference)
    {
        if (!$dateNaissance || !$reference) {
            return;
        }
        $chiffreNaissance = explode('/', $dateNaissance); // On tri les infos
        $chiffreReference = explode('/', $reference); // On tri les infos

        $time_naissance = mktime(
            0,
            0,
            0,
            $chiffreNaissance[1],
            $chiffreNaissance[0],
            $chiffreNaissance[2]
        ); // On recupere sa date de naissance en timestamp
        $time_reference = mktime(
            0,
            0,
            0,
            $chiffreReference[1],
            $chiffreReference[0],
            $chiffreReference[2]
        ); // On recupere sa date de reference en timestamp
        $seconde = $time_reference - $time_naissance;
        $seconde_par_an = (1461 * 24 * 60 * 60) / 4; // On calcule le nombre de secondes en compte les années bisextilles

        return floatval($seconde / $seconde_par_an);
    }

    public static function getDateRespectantDelai($delai, $reference)
    {
        $chiffreReference = explode('/', $reference); // On tri les infos
        $chiffreReference[2] -= $delai;

        return $chiffreReference[0].'/'.$chiffreReference[1].'/'.$chiffreReference[2];
    }

    public static function sortmddata($array, $by, $order, $type = null)
    {
        //$array: the array you want to sort
        //$by: the associative array name that is one level deep
        ////example: name
        //$order: ASC or DESC
        //$type: num or str

        //$sortby = "sort$by"; //This sets up what you are sorting by
        $firstval = current($array); //Pulls over the first array

        $vals = array_keys($firstval); //Grabs the associate Arrays

        foreach ($vals as $init) {
            $keyname = "sort$init";
            ${$keyname} = [];
        }
        //This was strange because I had problems adding
        //Multiple arrays into a variable variable
        //I got it to work by initializing the variable variables as arrays
        //Before I went any further

        foreach ($array as $key => $row) {
            foreach ($vals as $names) {
                $keyname = "sort$names";
                $test = [];
                $test[$key] = $row[$names];
                ${$keyname} = array_merge(${$keyname}, $test);
            }
        }

        //This will create dynamic mini arrays so that I can perform
        //the array multisort with no problem
        //Notice the temp array... I had to do that because I
        //cannot assign additional array elements to a
        //varaiable variable

        // le tri sera insensible à la casse
        foreach ($array as $key => $oneDimension) {
            $array_lowercase[$key] = array_map('strtolower', $oneDimension);
        }
        if ('DESC' == $order) {
            if ('num' == $type) {
                array_multisort($array_lowercase, SORT_DESC, SORT_NUMERIC, $array);
            } else {
                array_multisort($array_lowercase, SORT_DESC, SORT_STRING, $array);
            }
        } else {
            if ('num' == $type) {
                array_multisort($array_lowercase, SORT_ASC, SORT_NUMERIC, $array);
            } else {
                array_multisort($array_lowercase, SORT_ASC, SORT_STRING, $array);
            }
        }

        //This just goed through and asks the additional arguments
        //What they are doing and are doing variations of
        //the multisort
        return $array;
    }

    /*public static function checkMail($strMail)
     {
     if (preg_match("/^[a-z0-9._-]+@[a-z0-9.-]{2,}[.][a-z]{2,4}$/i",
     trim($strMail)==0))
     {
     return true;
     }
     else return false;
     }*/

    /*
     * Mise en comantaire par KBE non utilisé dans le code
     *
     * public static function checkMail($email) {
        $atom = '[-a-z0-9!#$%&\'*+\\/=?^_`{|}~]'; // caractères autorisés avant l'arobase
        $domain = '([a-z0-9]([-a-z0-9]*[a-z0-9]+)?)'; // caractères autorisés après l'arobase (nom de domaine)
        $regex = '/^' . $atom . '+' . // Une ou plusieurs fois les caractères autorisés avant l'arobase
        '(\.' . $atom . '+)*' . // Suivis par zéro point ou plus
        // séparés par des caractères autorisés avant l'arobase
        '@' . // Suivis d'un arobase
        '(' . $domain . '{1,63}\.)+' . // Suivis par 1 à 63 caractères autorisés pour le nom de domaine
        // séparés par des points
        $domain . '{2,63}$/i'; // Suivi de 2 à 63 caractères autorisés pour le nom de domaine

        if (preg_match($regex, trim($email))) // test de l'adresse e-mail
        {
        return true;
        } else {
        return false;
        }
        }*/

    public static function checkMail($email)
    {
        if (0 == preg_match('/^([a-z0-9._-]|[A-Z0-9._-])+@([a-z0-9.-]|[A-Z0-9.-]){2,}[.][a-z]{2,}$/',
                trim($email, ' ')) || '' == $email ) {
            return false;
        }

        return true;
    }

    /**
     * Verification du format de l'adresse mail utilisée dans le flux RSS.
     *
     * @param string $email L'adresse email à tester
     *
     * @return string l'adresse email si elle est correcte (exception sinon)
     *
     * @throws Atexo_Exception
     */
    public static function checkEmail($email)
    {
        if (!preg_match('`^[[:alnum:]]([-_.]?[[:alnum:]])*@[[:alnum:]]([-.]?[[:alnum:]])*\.([a-z]{2,4})$`', $email)) {
            throw new Atexo_Exception("Le format de l'adresse email ne semble pas valide ($email).");
        }

        return $email;
    }

    /**
     * Verification du format de l'adresse URL.
     *
     * @param string $email L'adresse URL à tester
     *
     * @return string l'adresse URL si elle est correcte (exception sinon)
     *
     * @throws Atexo_Exception
     */
    public static function checkUrl($url)
    {
        if (!preg_match('`((?:https?|ftp)://\S+[[:alnum:]]/?)`si', $url)) {
            throw new Atexo_Exception('Le format de l\'URL ne semble pas valide ('.$url.')');
        }

        return $url;
    }

    /**
     * fonction qui sert à vérifier si une chaine de caractères
     * respect l'expression d'un mail ou d'un ensemble de mails.
     *
     * @param $text le text à vérifier
     *
     * @return $separateur le séparateur des mails
     */
    public static function checkMails($text)
    {
        //$emails = explode($separateur,$text);
        $email = strtok($text, ',;');
        //for ($i=0;$i<count($emails);$i++)
        while (false !== $email) {
            if (!static::checkMail(trim($email))) {
                return false;
            }
            $email = strtok(',;');
        }

        return true;
    }

    /**
     * Renvoit la date au format RFC 2822 (ex: Thu, 21 Dec 2000 16:01:07 +0200).
     *
     * @param string $date date dans un format quelconque
     *
     * @return string date au format RFC 2822 (ex: Thu, 21 Dec 2000 16:01:07 +0200)
     */
    public static function getDateRFC2822($date)
    {
        return date('r', strtotime($date));
    }

    public static function getDateFromDateTime($dateTime)
    {
        if (!$dateTime) {
            return;
        }
        [$date, $time] = explode(' ', $dateTime);

        return $date;
    }

    public static function OterAccents($chaine)
    {
        $chainesAremplacer = ['À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'à', 'á', 'â', 'ã', 'ä', 'å', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'È', 'É', 'Ê', 'Ë', 'è', 'é', 'ê', 'ë', 'Ç', 'ç', 'Ì', 'Í', 'Î', 'Ï', 'ì', 'í', 'î', 'ï', 'Ù', 'Ú', 'Û', 'Ü', 'ù', 'ú', 'û', 'ü', 'ÿ', 'Ñ', 'ñ', '°'];
        $chainesRemplacantes = ['A', 'A', 'A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a', 'a', 'O', 'O', 'O', 'O', 'O', 'O', 'o', 'o', 'o', 'o', 'o', 'o', 'E', 'E', 'E', 'E', 'e', 'e', 'e', 'e', 'C', 'c', 'I', 'I', 'I', 'I', 'i', 'i', 'i', 'i', 'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 'y', 'N', 'n', '_'];

        return str_replace($chainesAremplacer, $chainesRemplacantes, $chaine);
    }

    public static function getDateAujourdhui($nomDesJour = null, $nomDesMois = null)
    {
        $nomJour = date('l');
        $jour = date('d');
        $nomMois = date('m');
        $annee = date('Y');
        if (!$nomDesJour) {
            $nomDesJour = [
                'Monday' => 'Lundi',
                'Tuesday' => 'Mardi',
                'Wednesday' => 'Mercredi',
                'Thursday' => 'Jeudi',
                'Friday' => 'Vendredi',
                'Saturday' => 'Samedi',
                'Sunday' => 'Dimanche',
                ];
        }
        if (!$nomDesMois) {
            $nomDesMois = [
            1 => 'Janv.',
            2 => 'Fév.',
            3 => 'Mars',
            4 => 'Avril',
            5 => 'Mai',
            6 => 'Juin',
            7 => 'Juil.',
            8 => 'Août',
            9 => 'Sept.',
            10 => 'Oct.',
            11 => 'Nov.',
            12 => 'Dec.',
            ];
        }

        return $nomDesJour[$nomJour].' '.$jour.' '.$nomDesMois[intval($nomMois)].' '.$annee.' '.date('H:i');
    }

    public static function isEntierRelatif($valeur)
    {
        return preg_match("/^([+\-]?)[0-9]+([\.,]?[0-9]+)?$/", $valeur);
    }

    public static function getDateDiffMois($diff, $reference)
    {
        // calcul de la date respectant le delai de trois mois
        if ($diff > 11 || $diff < 1) {
            return;
        }
        [$annee, $mois, $jour] = explode('-', $reference);
        if ($mois > $diff) {
            $mois = $mois - $diff;
        } else {
            $annee = $annee - 1;
            $mois = $mois + 12 - $diff;
        }
        if ($mois < 10) {
            $mois = '0'.$mois;
        }
        /*if ($jour<10)
            $jour = '0'.$jour;*/
        return $annee.'-'.$mois.'-'.$jour;
    }

    //Générer une chaine de caractère unique et aléatoire

    public static function random($car)
    {
        $string = '';
        $chaine = 'abcdefghijklmnpqrstuvwxy';
        mt_srand((float) microtime() * 1_000_000);
        for ($i = 0; $i < $car; ++$i) {
            $string .= $chaine[random_int(0, mt_getrandmax()) % strlen($chaine)];
        }

        return $string;
    }

    public static function MajusculeAccents($chaine)
        {
        return strtr($chaine, 'àáâãäåòóôõöøèéêëçìíîïùúûüÿñ', 'ÀÁÂÃÄÅÒÓÔÕÖØÈÉÊËÇÌÍÎÏÙÚÛÜÑ');
    }

    public static function StrToMaj($chaine)
    {
        return strtoupper(self::MajusculeAccents($chaine));
    }

    public static function FirstMaj($chaine)
    {
        $pos = $chaine[0];
        if (strpbrk('àáâãäåòóôõöøèéêëçìíîïùúûüÿñ', $pos)) {
            $maj = self::MajusculeAccents($pos);
        } else {
            $maj = strtoupper($pos);
        }
        $i = 1;
        $Suite = '';
        while ($chaine[$i]) {
            $Suite .= $chaine[$i];
            ++$i;
        }
        $ChaineConvert = $maj.$Suite;

        return $ChaineConvert;
    }

    public static function convert0ToNonAnd1ToOui($valeur)
    {
        if ('1' == $valeur) {
            return 'Oui';
        } elseif ('0' == $valeur) {
            return 'Non';
        }
        return $valeur;
    }

    public static function write_file($file_name, $data, $mode = 'w')
    {
        if (!is_string($file_name)) {
            return null;
        }
        $fp = fopen($file_name, $mode);
        if (false === $fp) {
            return null;
        }
        if (!fwrite($fp, $data) && !empty($data) && !is_file($file_name)) {
            return null;
        }
        fclose($fp);

        return 1;
    }

    public static function getDate($datemod)
    {
        $ladate = static::getDateFromDateTime($datemod);
        $ladate = strtotime(static::frnDate2iso($ladate));

        $nomJour = date('l', $ladate);
        $jour = date('d', $ladate);
        $nomMois = date('m', $ladate);
        $annee = date('Y', $ladate);

        $nomDesJour = [
            'Monday' => 'Lundi',
            'Tuesday' => 'Mardi',
            'Wednesday' => 'Mercredi',
            'Thursday' => 'Jeudi',
            'Friday' => 'Vendredi',
            'Saturday' => 'Samedi',
            'Sunday' => 'Dimanche',
            ];
        $nomDesMois = [
            '01' => 'Janvier',
            '02' => 'Février',
            '03' => 'Mars',
            '04' => 'Avril',
            '05' => 'Mai',
            '06' => 'Juin',
            '07' => 'Juillet',
            '08' => 'Août',
            '09' => 'Septembre',
            '10' => 'Octobre',
            '11' => 'Novembre',
            '12' => 'Decembre',
            ];

        return $nomDesJour[$nomJour].' '.$jour.' '.$nomDesMois[intval($nomMois)].' '.$annee;
    }

    public static function getDateAujourdhui2()
    {
        $nomJour = date('l');
        $jour = date('d');
        $nomMois = date('m');
        $annee = date('Y');

        $nomDesJour = [
            'Monday' => 'Lundi',
            'Tuesday' => 'Mardi',
            'Wednesday' => 'Mercredi',
            'Thursday' => 'Jeudi',
            'Friday' => 'Vendredi',
            'Saturday' => 'Samedi',
            'Sunday' => 'Dimanche',
            ];
        $nomDesMois = [
            1 => 'Janvier',
            2 => 'Février',
            3 => 'Mars',
            4 => 'Avril',
            5 => 'Mai',
            6 => 'Juin',
            7 => 'Juillet',
            8 => 'Août',
            9 => 'Septembre',
            10 => 'Octobre',
            11 => 'Novembre',
            12 => 'Decembre',
            ];

        return $nomDesJour[$nomJour].' '.$jour.' '.$nomDesMois[intval($nomMois)].' '.$annee;
    }

    public static function NbMois($date1, $date2)
    {
        [$d1,$m1,$y1] = explode('/', $date1);
        [$d2,$m2,$y2] = explode('/', $date2);
        if ((int) $m1 > (int) $m2) {
            $nbmois = (int) $m1 - (int) $m2 + 12;
            $nbmois += ((int) $y2 - (int) $y1 - 1) * 12;
        } else {
            $nbmois = (int) $m2 - (int) $m1;
            $nbmois += ((int) $y2 - (int) $y1) * 12;
        }
        if ($d1 > $d2) {
            --$nbmois;
            $nbmois += (30 - $d1 + $d2) / 30;
        } else {
            $nbmois += ($d2 - $d1) / 30;
        }

        if (($nbmois - floor($nbmois)) > 0.5) {
            ++$nbmois;
        }

        return floor($nbmois);
    }

    /*retourne l'année scolaire sous format yyyy/yyyy*/

    public static function getAnneeScolaire()
    {
        //$date=date("Y/m/d");
        (int) $mois = date('m');

        if ($mois < 6) {
            $Date1 = (int) date('Y') - 1;
            $Date2 = (int) date('Y');
            $Date2 = (string) $Date2;

            return (string) $Date1.'/'.(string) $Date2;
        } elseif ($mois >= 6) {
            $Date1 = (int) date('Y');
            $Date2 = (int) date('Y') + 1;
            $Date1 = (string) $Date1;
            $Date2 = (string) $Date2;

            return (string) $Date1.'/'.(string) $Date2;
        }
    }

    public static function generateCaSerialFile()
    {
        $serial = static::generateUniqueSerial();
        if ($fp = @fopen(Atexo_Config::getParameter('BASE_ROOT_DIR').'/common/ca/serial', 'w')) {
            if (!@fwrite($fp, $serial)) {
                @fclose($fp);

                return false;
            }
            if (!@fclose($fp)) {
                return false;
            }

            return true;
        }

        return false;
    }

    public static function generateUniqueSerial()
    {
        $ko = true;
        do {
            $serial = '';
            for ($i = 1; $i <= 8; ++$i) {
                $number = random_int(0, 15);
                if (10 == $number) {
                    $serial .= 'A';
                } elseif (11 == $number) {
                    $serial .= 'B';
                } elseif (12 == $number) {
                    $serial .= 'C';
                } elseif (13 == $number) {
                    $serial .= 'D';
                } elseif (14 == $number) {
                    $serial .= 'E';
                } elseif (15 == $number) {
                    $serial .= 'F';
                } else {
                    $serial .= $number;
                }
            }
            $c = new Criteria();
            $connexionCom = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
            );
            $c->add(CommonOffresPeer::UNTRUSTEDSERIAL, $serial, Criteria::EQUAL);
            $sqlResult = CommonOffresPeer::doSelect($c, $connexionCom);

            if (is_array($sqlResult)) {
                if (0 == count($sqlResult)) {
                    $ko = false;
                }
            } else {
                $ko = false;
            }
        } while ($ko);

        return $serial;
    }

    public static function getMinOrganisme()
    {
        $bd = Atexo_CurrentUser::getCurrentOrganism();
        if (1 == Atexo_Config::getParameter('DB_NAME_IN_UPPERCASE')) {
            $organisme = strtolower($bd);
        } else {
            $organisme = $bd;
        }

        return $organisme;
    }

    public static function getMajOrganisme($org)
    {
        if (1 == Atexo_Config::getParameter('DB_NAME_IN_UPPERCASE')) {
            $organisme = strtoupper($org);
        } else {
            $organisme = $org;
        }

        return $organisme;
    }

    public static function sortArrayOfObjects($arrayObject, $elementSorted, $sensTri = 'ASC', $nestedObjects = null)
    {
        $arrayObjectSorted = [];
								$arrayTmp = [];
								if ($arrayObject) {
            $nbrValNum = 0;
            foreach ($arrayObject as $key => $object) {
                $currentObject = $object;
                if (null != $nestedObjects) {
                    //pour garder la possiblité de mettre soit un tableau soit une chaine simple pour $nestedObjects
                    if (!is_array($nestedObjects)) {
                        $tmp = $nestedObjects;
                        unset($nestedObjects);
                        $nestedObjects[] = $tmp;
                    }
                    foreach ($nestedObjects as $nestedObject) {
                        $nomNestedMethod = 'get'.ucfirst($nestedObject);
                        if (!method_exists($currentObject, $nomNestedMethod)) {
                            throw new Exception("La méthode $nomNestedMethod n'existe pour les objets du tableau.");
                        }
                        $currentObject = call_user_func([$currentObject, $nomNestedMethod]);
                    }
                }
                $nomMethod = 'get'.ucfirst($elementSorted);
                if (!method_exists($currentObject, $nomMethod)) {
                    throw new \Exception("La méthode $nomMethod n'existe pour les objets du tableau.");
                }
                $valeur = call_user_func([$currentObject, $nomMethod]);
                $arrayTmp[$key] = trim($valeur);
                if (self::isDecimal($valeur)) {
                    ++$nbrValNum;
                }
            }

            if ($arrayTmp && self::isDate($arrayTmp[0])) {
                foreach ($arrayTmp as $value) {
                    $arrayToSort[] = self::frnDate2iso($value);
                }
            } else {
                $arrayToSort = $arrayTmp;
            }

            // on trie le tableau du petit au plus grand
            if ($nbrValNum == count($arrayToSort)) {
                asort($arrayToSort, SORT_NUMERIC);
            } else {
                natcasesort($arrayToSort);
            }
            $keysOfArrayObjects = array_keys($arrayToSort);
            foreach ($keysOfArrayObjects as $indice) {
                $arrayObjectSorted[$indice] = $arrayObject[$indice];
            }

            //dans ou il n'est pas ascendant on le prend descendant
            if (0 != strcmp($sensTri, 'ASC')) {
                $arrayObjectSorted = array_reverse($arrayObjectSorted);
            }

            return $arrayObjectSorted;
        }
    }

    /**
     * retourne le chemin du service.
     *
     * @param string $service     le nom du service
     * @param array  $arrayParent tableau des parents du service
     * @param string $separateor  le séparateur
     *
     * @return string chemin du service
     */
    public static function implodeArrayWithSeparator($service, $arrayParent, $separateor)
    {
        if ($arrayParent) {
            $countArray = count($arrayParent);
            $index = 0;
            foreach ($arrayParent as $oneElement) {
                if ($oneElement['libelle'] && $index < $countArray) {
                    $NewSeparateor = $separateor;
                } else {
                    $NewSeparateor = '';
                }
                $service = $oneElement['libelle'].$NewSeparateor.$service;
                ++$index;
            }
        }

        return $service;
    }

    /**
     * Génere une série de 9 caractères alphanumériques définis de façon aléatoire.
     */
    public static function generateCode($size)
    {
        $q = null;
								// Initialisation des caractères utilisables
        $characters = [
            0 => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        1 => ['a',
                'b',
                'c',
                'd',
                'e',
                'f',
                'g',
                'h',
                'i',
                'j',
                'k',
                'l',
                'm',
                'n',
                'o',
                'p',
                'q',
                'r',
                's',
                't',
                'u',
                'v',
                'w',
                'x',
                'y',
                'z'],
        2 => ['A',
                'B',
                'C',
                'D',
                'E',
                'F',
                'G',
                'H',
                'I',
                'J',
                'K',
                'L',
                'M',
                'N',
                'O',
                'P',
                'Q',
                'R',
                'S',
                'T',
                'U',
                'V',
                'W',
                'X',
                'Y',
                'Z'], ];

        $pwdDenerated = [];

        for ($i = 0; $i < $size; ++$i) {
            // On choisit au hasard entre quelle sorte de caractères choisir
            $p = random_int(0, 2);
            switch ($p) {
                case 0:
                    $q = random_int(0, 9);
                    break;
                case 1:
                    $q = random_int(0, 25);
                    break;
                case 2:
                    $q = random_int(0, 25);
                    break;
            }

            $pwdDenerated[$i] = $characters[$p][$q];
        }

        return implode('', $pwdDenerated);
    }

    /**
     * retourne le chemin de base d'un organisme pour l'accès agent.
     */
    public static function getBasePopUpUrl()
    {
        $redirectUrl = null;
								$organisme = self::getMinOrganisme();
        if ($organisme) {
            $redirectUrl = Atexo_Config::getParameter('URL_MPE_ORGANISME').'/'.$organisme.'/agent/';
        }

        return $redirectUrl;
    }

    /**
     * cette fonction retourne la date (format français jj/mm/yyyy) passé en paramètre après y avoir ajouté ou soustrait
     * le nombre de jours, de mois et d'années passés en paramètres.
     *
     * @param : $dateRef : date au format français
     *                   $apresOuAvant : negatif pour faire une soustraction, positive pour une addition
     *                   $nbrJours, $nbrMois, $nbrAnnees : respectivement le nombre de jours, de mois et d'années à ajouter/soustraire
     *
     * @return : la date modifiée au format français
     */
    public static function dateWithparameter($apresOuAvant, $dateRef, $nbrJours, $nbrMois, $nbrAnnees)
    {
        [$jour, $moi, $annee] = explode('/', $dateRef);
        // mktime fait automatiquement le calcul
        if (!$apresOuAvant || $apresOuAvant < 0) {
            $time = mktime(00, 00, 00, $moi - $nbrMois, $jour - $nbrJours, $annee - $nbrAnnees);
        } else {
            $time = mktime(00, 00, 00, $moi + $nbrMois, $jour + $nbrJours, $annee + $nbrAnnees);
        }

        return date('d/m/Y', $time);
    }

    public static function isUTF8($str)
    {
        if ($str === mb_convert_encoding(mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32')) {
            return true;
        }

        return false;
    }

    public static function utf8ToIso($text)
    {
        //echo "#".$text." ".mb_detect_encoding($text)."#";
        if (self::isUTF8($text)) {
            return utf8_decode($text);
        }

        return $text;
    }

    public static function isCurrentBrowserIe()
    {
        if (preg_match('#MSIE#', getenv('HTTP_USER_AGENT'))) {
            return true;
        }

        return false;
    }

    public static function getlistetroisdernieresannees()
    {
        $annee = (int) date('Y');

        return ['1' => (string) $annee - 3, '2' => (string) $annee - 2,  '3' => (string) $annee - 1];
    }

    public static function arrondirSizeFile($kiloOctets)
    {
        if ($kiloOctets < 1024) {
            return str_replace('.', ',', round($kiloOctets, 2)).' '.Prado::localize('TEXT_KILO_OCTET');
        } else {
            return str_replace('.', ',', round($kiloOctets / 1024, 2)).' ' . Prado::localize('TEXT_MEGA_OCTET');
        }
    }

    public static function getMontantArronditEspace($montant, $apresVirgule = 2, $sepPoint = false)
    {
        $montant = str_replace(' ', '', $montant);
        if (empty($montant)) {
            return '0,00';
        }

        if (!self::isEntierRelatif($montant)) {
            return $montant;
        }

        $montant = str_replace(',', '.', $montant);

        $montant = str_replace('.', ',', sprintf('%.'.$apresVirgule.'f', $montant));

        [$mon,$vir] = explode(',', $montant);

        $i = 0;
        $mon = strrev($mon);
        $montant = '';
        while ($i < strlen($mon)) {
            $montant = $mon[$i].$montant;
            if ((($i + 1) % 3) == 0) {
                $montant = ' '.$montant;
            }
            ++$i;
        }
        if ($sepPoint) {
            return trim($montant.'.'.$vir);
        }

        return trim($montant.(0 != $apresVirgule ? ','.$vir : ''));
    }

    public static function writeLogFile($directory_name, $file_name, $data, $writingMode = 'w')
    {
        if (null != $directory_name) {
            if (!is_dir($directory_name)) {
                mkdir($directory_name, 0777, true);
            }
        }

        return self::writeFile($file_name, $data, $writingMode);
    }

    public static function writeFile($file_name, $data, $writingMode = 'w')
    {
        $fp = fopen($file_name, $writingMode);
        if (false === $fp || !fwrite($fp, $data)) {
            return false;
        }
        fclose($fp);

        return true;
    }

    /**
     * retourne l'éxtension d'un fichier.
     */
    public static function getExtension($fichier)
    {
        // coupe le nom du fichier
        $tableau = explode('.', $fichier);
        // retourne les caractere apres le dernier .
        return $tableau[count($tableau) - 1];
    }

    /*
     * supprime un répértoire et les fichiers contenu dans le répertoire
     *
     */
    public static function removeDir($dir)
    {
        if (is_dir($dir)) {
            $files = array_slice(scandir($dir), 2);
            $count = count($files);
            for ($i = 0; $i < $count; ++$i) {
                if (is_file($dir.'/'.$files[$i])) {
                    @unlink($dir.'/' . $files[$i]);
                }
            }
        }
        @rmdir($dir);
    }

    public static function arrayUnshift($array, $chaine)
    {
        $arrayList = [];
        if (is_array($array)) {
            $arrayList[0] = $chaine;
            foreach ($array as $key => $oneElement) {
                $arrayList[$key] = $oneElement;
            }
        }

        return $arrayList;
    }

    public static function arrayUnshiftWithKey($array, $key, $chaine)
    {
        $arrayList = [];
        $arrayList[$key] = $chaine;
        if (is_array($array)) {
            foreach ($array as $key => $oneElement) {
                $arrayList[$key] = $oneElement;
            }
        }

        return $arrayList;
    }

    /**
     * Ael : fonction arrayMerge : fusionnes les tableaux passé en paramètre en un seul tableau sans modification de
     * clé (numériques ou autres) Si les tableaux ont des clés en commun, la dernière valeur rencontrée ne sera pas
     * insérée mais mise dans tableau spécial contenant la liste des élements non mérgés. Cette foncion a Deux modes
     * fonctionnmement : Mode 1 : Si le dernier paramètre fournie est égale à true le tableau retourné est sous la
     * forme : Tableau['mergedArrays'] = contenant les tableaux fusionnés Tableau['unMergedValues'] = contenant la
     * liste des élements qui non pas été inséré (clé dupliqué) Mode 2 : Si le dernier paramètre fournie est un array
     * le tableau retourné est le résultat de la fusion des tableaux. Mode 3 : Si le premier paramètre passé est égale
     * à true cette fonction aura le même comportement que la fonction php array_merge Si les tableaux ont des clés en
     * commun, la dernière valeur rencontrée écrasera l'ancienne. Pour les valeurs numériques, cela n'arrive pas  , car
     * alors, les valeurs sont ajoutées en fin de tableau.
     *
     * @param : un opu plusieurs tableaux
     *
     * @return : si en mode 1 : Tableau['mergedArrays'] = contenant les tableaux fusionnés
     *                          Tableau['unMergedValues'] = contenant la liste des élements qui non pas été inséré (clé
     *     dupliqué) si en mode 2 : les tableaux fusionnées si en mode 3 :
     */
    public static function arrayMerge()
    {
        $arrayToReturn = [];
								if (0 == func_num_args()) {
            return [];
        }
        if (!is_array(func_get_arg(0)) && true == func_get_arg(0)) {
            $usePhpArrayMerge = true;
        } else {
            $usePhpArrayMerge = false;
        }
        $globalArray = [];
        $unmergedValues = [];
        for ($indice = 0; $indice < func_num_args(); ++$indice) {
            $arrayTmp = func_get_arg($indice);
            if (is_array($arrayTmp)) {
                if ($usePhpArrayMerge) {
                    $globalArray = array_merge($globalArray, $arrayTmp);
                } else {
                    foreach ($arrayTmp as $key => $val) {
                        if (!isset($globalArray[$key])) {
                            $globalArray[$key] = $val;
                        } else {
                            $unmergedValues[] = $key.'=>'.(is_object($val) ? 'Object' : $val);
                        }
                    }
                }
            }
        }

        if (!is_array(func_get_arg(func_num_args() - 1)) && true == func_get_arg(func_num_args() - 1)) {
            $arrayToReturn['mergedArrays'] = $globalArray;
            $arrayToReturn['unMergedValues'] = $unmergedValues;

            return $arrayToReturn;
        }

        return $globalArray;
    }

    public static function fixCDATA($string)
    {
        $find = [];
								$replace = [];
								$find[] = '&lt;![CDATA[';
        $replace[] = '<![CDATA[';

        $find[] = '&amp;';
        $replace[] = '&';

        //permet le remplacement des caractères '&' dans une phrase ex : Atexo & Cie devient Atexo et Cie
        $find[] = ' &amp; ';
        $replace[] = ' et ';

        $find[] = ']]&gt;';
        $replace[] = ']]>';

        return $string = str_replace($find, $replace, $string);
    }

    /**
     * Pour insérer un retour à la ligne (<br>), tous les 22 caractères, dans une chaine.
     * @param : $texte : texte dans lequel sera ajouter les <br>
     *
     * @return : retourn la chaine après y avoir insérer un retour à la lligne tous les $everyHowManyCaracter caractères
     */
    public static function insertBrWhenTextTooLong($texte, $everyHowManyCaracter)
    {
        return wordwrap($texte, $everyHowManyCaracter, '<br>', true);
    }

    /**
     * convertit une datetime ISO en string heure et minute sans separateurs.
     * @param $_isoDate date au format yyyy-mm-dd hh:mm:ss
     *
     * @return date au format hh : mm
     */
    public static function iso2frnTimeNoSep($_isoDate, $withSec = false)
    {
        if (!$_isoDate || '0000-00-00 00:00:00' == $_isoDate) {
            return;
        }
        if (10 == strlen($_isoDate)) {
            return self::iso2frnDate($_isoDate);
        }

        [$a, $m, $j_et_heure] = explode('-', $_isoDate);
        $m = '';
        $a = '';
        [$j, $heure] = explode(' ', $j_et_heure);
        $j = '';
        [$h, $min, $sec] = explode(':', $heure);
        if (!$withSec) {
            $sec = '';
        }

        return "$h:$min".(!empty($sec) ? ":$sec" : '');
    }

    public static function escapeXmlChars($strInXml)
    {
        $carac = ['&', '<', '>', "'"];
        $valCarac = ['&amp;', '&lt;', '&gt;', '&apos;'];

        return str_replace($carac, $valCarac, $strInXml);
    }

    /**
     * remplacer les $champsARemplacer par les $valeurs dals $content.
     */
    public static function xmlStrReplace($content, $champsARemplacer, $valeurs)
    {
        //$carac = array('<','>','& ',"'");
        //$valCarac = array('&lt;','&gt;','&amp; ','&apos;');
        $valeurs = ('' == $valeurs) ? '' : $valeurs;

        return str_replace($champsARemplacer, $valeurs, $content);
    }

    /**
     * remplacer les caractère speciaux d'un xml.
     */
    public static function xmlStrReplaceSpecCarc($xml)
    {
        $valCarac = ['<', '>', '& ', "'", ''];
        $carac = ['&lt;', '&gt;', '&amp; ', '&apos;', '&#xD;'];

        return str_replace($carac, $valCarac, $xml);
    }

    public static function diffDatesMoisJoursHeuresMinutes($dateDebut, $dateFin)
    {
        $signe = '';
        if ($dateFin < $dateDebut) {
            $signe = '- ';
            $sauv = $dateFin;
            $dateFin = $dateDebut;
            $dateDebut = $sauv;
        }
        //$chaineDates="";
        if (strpos($dateDebut, '-') && strpos($dateFin, '-')) {
            [$dateD, $heuresD] = explode(' ', $dateDebut);
            [$dateF, $heuresF] = explode(' ', $dateFin);

            [$anneeD, $moiD ,$jourD] = explode('-', $dateD);
            [$anneeF, $moiF ,$jourF] = explode('-', $dateF);

            [$heureD, $minuteD] = explode(':', $heuresD);
            [$heureF, $minuteF] = explode(':', $heuresF);

            $timestampD = mktime($heureD, $minuteD, 0, $moiD, $jourD, $anneeD);
            $timestampF = mktime($heureF, $minuteF, 0, $moiF, $jourF, $anneeF);
            $timestamp = $timestampF - $timestampD;

            $nombreJours = floor(($timestamp / 86400));
            $reste1 = $timestamp - ($nombreJours * 86400);

            $nombreHeure = floor(($reste1 / 3600));
            $reste2 = $reste1 - ($nombreHeure * 3600);

            $nombreMinute = floor(($reste2 / 60));

            return $signe.(($nombreJours > 0) ? $nombreJours.'j ' : '').(($nombreHeure > 0) ? $nombreHeure.'h ' : '').(($nombreMinute > 0) ? $nombreMinute.'min' : '');
        }
    }

    public static function getLanguageAbbreviation($lang)
    {
        $languagesAbbreviation = ['en' => 'En', 'es' => 'Es', 'fr' => 'Fr'];

        return $languagesAbbreviation[$lang];
    }

    public static function getNextOpenDay($iDate, $iDays)
    {
        $aBankHolidays = [
            '1_1',
            '1_5',
            '8_5',
            '14_7',
            '15_8',
            '1_11',
            '11_11',
            '25_12',
            ];

        //      $iEaster = easter_date ((int)date('Y', $iDate));
        //
        //      $aBankHolidays[] = date ('j_n',$iEaster);
        //      $aBankHolidays[] = date ('j_n', $iEaster + (86400*39));
        //      $aBankHolidays[] = date ('j_n', $iEaster + (86400*49));

        //print_r ($aBankHolidays);
        $iEnd = $iDays * 86400;
        $i = 0;
        while ($i < $iEnd) {
            if (in_array(date('w', $iDate + $i), [0, 6]) || in_array(date('j_n', $iDate + $i), $aBankHolidays)) {
                $iEnd = strtotime('+1 day', $iEnd);
                ++$iDays;
            } else {
                break;
            }
            $i = strtotime('+1 day', $i);
        }

        return $iDate + $i;
    }

    public static function getDateToday()
    {
        return strftime('%Y%m%d');
    }

    public static function lister($chemin)
    {
        echo '<u>Contenu du dossier '.$chemin.'</u><br><blockquote>';

        //nom du répertoire à lister
        $nom_repertoire = $chemin;

        //on ouvre un pointeur sur le repertoire
        $pointeur = opendir($nom_repertoire);

        //pour chaque fichier et dossier
        while ($fichier = readdir($pointeur)) {
            //on ne traite pas les . et ..
            if (('.' != $fichier) && ('..' != $fichier)) {
                //si c'est un dossier, on le lit
                if (is_dir($nom_repertoire.'/'.$fichier)) {
                    echo '<blockquote>';
                    self::lister($nom_repertoire.'/'.$fichier);
                    echo '</blockquote>';
                } else {
                    //c'est un fichier, on l'affiche
                    echo $fichier.'<br>';
                }
            }
        }
        echo '</blockquote><u>Fin du dossier '.$nom_repertoire.'</u><br>';

        //fermeture du pointeur
        closedir($pointeur);
    }

    public static function isCurrentClientWindows()
    {
        if (preg_match('#Win#', getenv('HTTP_USER_AGENT'))) {
            return true;
        }

        return false;
    }

    //Retourne un tableau contenant toutes les années se trouvant dans la fourchette  : année courante - paramètre, année courante + paramètre
    public static function arrayAnnee($fourchette)
    {
        $anneeCourrante = date('Y');
        $arrayAnne = [];
        for ($i = -$fourchette; $i <= $fourchette; ++$i) {
            $arrayAnne[$anneeCourrante + $i] = $anneeCourrante + $i;
        }

        return $arrayAnne;
    }

    //Retourne un tableau contenant les trimèstres (4 normalement)
    public static function arrayTrimestre()
    {
        return ['1' => '1', '2' => '2', '3' => '3', '4' => '4'];
    }

    /**
     * Fonction qui calcule une date au passer.
     *
     * @param   X    le nombre de jours
     *
     * @return date Date du passer
     */
    public static function DateInXDays($X)
    {
        $XDate = getdate(mktime(0, 0, 0, date('m'), date('d') - $X, date('Y')));

        return $XDate['mday'].sprintf('%02d', $XDate['mon']).sprintf('%02d', $XDate['year']);
    }

    /**
     * retourne un timestamp UNIX correspondant aux arguments fournis.
     * @param $_isoDate date au format yyyy-mm-dd hh:mm:ss
     *
     * @return date au format ddmmyyyyhhmm
     */
    public static function iso2UnixTimeStamp($_isoDate)
    {
        if (!$_isoDate || '0000-00-00 00:00:00' == $_isoDate) {
            return;
        }
        [$a, $m, $j_et_heure] = explode('-', $_isoDate);
        [$j, $heure] = explode(' ', $j_et_heure);
        [$h, $min, $sec] = explode(':', $heure);
        $dateTime = mktime($h, $min, $sec, $m, $j, $a);

        return $dateTime;
    }

    /**
     * Initilase un tableau à des valeurs sans modifier les clés.
     */
    public static function initializeArray($array, $value)
    {
        foreach ($array as $key => $oldValue) {
            $array[$key] = $value;
        }

        return $array;
    }

    public static function getTrimestresUntilNow($parmAnnee)
    {
        $Trimestre = null;
								$boucle = null;
								$arrayTrimestre = null;
        if (date('Y') > $parmAnnee) {
            $Trimestre = 4;
        } elseif (0 == strcmp(date('Y'), $parmAnnee)) {
            $date = date('m-d H:s');
            //$date='07-05 00:00';
            if ($date >= '01-01 00:00' && $date <= '03-31 23:59') {
                $Trimestre = 0;
            } elseif ($date >= '04-01 00:00' && $date <= '06-30 23:59') {
                $Trimestre = 1;
            } elseif ($date >= '07-01 00:00' && $date <= '09-30 23:59') {
                $Trimestre = 2;
            } else {
                $Trimestre = 3;
            }
        }

        for ($i = 1; $i <= $Trimestre; ++$i) {
            $arrayTrimestre[$i] = $i;
            $boucle = true;
        }
        if (!$boucle) {
            $arrayTrimestre['Aucun_trimistre'] = 'Aucun trimistre';
        }

        return $arrayTrimestre;
    }

    //Retourne un tableau contenant toutes les années se trouvant dans la fourchette  : année courante - paramètre, année courante + paramètre
    public static function arrayAnneeLimited($fourchette)
    {
        $anneeCourrante = date('Y');
        $arrayAnne = [];
        for ($i = -$fourchette; $i <= 0; ++$i) {
            $arrayAnne[$anneeCourrante + $i] = $anneeCourrante + $i;
        }

        return $arrayAnne;
    }

    public static function getTrimestre($date)
    {
        if (!$date) {
            $date = date('Y-m-d H:s');
        } else {
            $date = $date.date('H:s');
        }

        $arrayDate = explode('-', $date);
        $annee = $arrayDate[0];
        $DatedebutFin = [];

        if ($date >= $annee.'-01-01 00:00' && $date <= $annee.'-03-31 23:59') {
            $DatedebutFin['debut'] = $annee.'-01-01';
            $DatedebutFin['fin'] = $annee.'-03-31';
            $DatedebutFin['trimestre'] = 'T1';
        } elseif ($date >= $annee.'-04-01 00:00' && $date <= $annee.'-06-30 23:59') {
            $DatedebutFin['debut'] = $annee.'-04-01';
            $DatedebutFin['fin'] = $annee.'-06-30';
            $DatedebutFin['trimestre'] = 'T2';
        } elseif ($date >= $annee.'-07-01 00:00' && $date <= $annee.'-09-30 23:59') {
            $DatedebutFin['debut'] = $annee.'-07-01';
            $DatedebutFin['fin'] = $annee.'-09-30';
            $DatedebutFin['trimestre'] = 'T3';
        } else {
            $DatedebutFin['debut'] = $annee.'-10-01';
            $DatedebutFin['fin'] = $annee.'-12-31';
            $DatedebutFin['trimestre'] = 'T4';
        }

        return $DatedebutFin;
    }

    public static function getTrimestrePrecedent($DatedebutFin)
    {
        $DatedebutFinPrec = [];
								$arrayDate = explode('-', $DatedebutFin['debut']);
        $annee = $arrayDate[0];

        if ('T4' == $DatedebutFin['trimestre']) {
            $DatedebutFinPrec['debut'] = $annee.'-07-01';
            $DatedebutFinPrec['fin'] = $annee.'-09-30';
            $DatedebutFinPrec['trimestre'] = 'T3';
        }
        if ('T3' == $DatedebutFin['trimestre']) {
            $DatedebutFinPrec['debut'] = $annee.'-04-01';
            $DatedebutFinPrec['fin'] = $annee.'-06-30';
            $DatedebutFinPrec['trimestre'] = 'T2';
        }
        if ('T2' == $DatedebutFin['trimestre']) {
            $DatedebutFinPrec['debut'] = $annee.'-01-01';
            $DatedebutFinPrec['fin'] = $annee.'-03-31';
            $DatedebutFinPrec['trimestre'] = 'T1';
        }
        if ('T1' == $DatedebutFin['trimestre']) {
            $annee = $annee - 1;
            $DatedebutFinPrec['debut'] = $annee.'-10-01';
            $DatedebutFinPrec['fin'] = $annee.'-12-31';
            $DatedebutFinPrec['trimestre'] = 'T4';
        }

        return $DatedebutFinPrec;
    }

    /*
     * content : content du XML sous format ISO
     * $pathModelXsd : chemin du fichier de verification XSD
     */
    public static function IsXmlValide($content, $pathModelXsd)
    {
        try {
            $Dom = new \DOMDocument();
            $Dom->loadXML(utf8_encode($content));
            $Dom->validateOnParse = true;
            if ($Dom->schemaValidate($pathModelXsd)) {
                return true;
            }

            return false;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function GetSizeName($octets)
    {
        $sizeName = [' octets', ' Ko', ' Mo', ' Go', ' To', 'Po'];
        $s = 0;
        $name = 0;
        while ($octets >= 1024) {
            $s = $octets;
            $octets = $octets / 1024;
            $name = $name + 1;
        }
        if ($s > 0 && $octets != intval($octets)) {
            if ($octets > 100) {
                return number_format($octets, 0).$sizeName[$name];
            } elseif ($octets > 10) {
                return number_format($octets, 1).$sizeName[$name];
            }

            return number_format($octets, 2).$sizeName[$name];
        }

        return $octets.$sizeName[$name];
    }

    /*
     * permet de retourner la taille d'un fichier from le nom de la taille du fichier
     */
    public static function GetSizeFromName($nameSize)
    {
        $sizeName = ['octets', 'ko', 'mo', 'go', 'to'];
        $nameSize = trim($nameSize);
        $infoSize = explode(' ', trim($nameSize));
        if (is_array($infoSize) && count($infoSize)) {
            $octets = floatval($infoSize[0]);
            $unite = strtolower($infoSize[1]);
            $index = array_search($unite, $sizeName);
            for ($i = 0; $i < $index; ++$i) {
                $octets *= 1024;
            }

            return $octets;
        }
    }

    /**
     * Retourne de manière "human readable" un temps en secondes (ex: 8 hrs 32 min 12.432 sec).
     *
     * @param float $seconds Temps en seconde (avec float milliseconde possible)
     *
     * @return string Chaine lisible du temps
     */
    public static function GetElapsedTime($seconds)
    {
        $time = [];
								$elapsed = '';
        $milli = $seconds - intval($seconds);
        $temp = $seconds % 3600;
        $time[0] = intval(($seconds - $temp) / 3600);
        $time[2] = $temp % 60;
        $time[1] = ($temp - $time[2]) / 60;
        if ($time[0] > 0) {
            $elapsed .= "{$time[0]} hrs";
        }
        if ($time[1] > 0) {
            $elapsed .= " {$time[1]} min ";
        }
        $elapsed .= ' '.($time[2] + $milli).' sec';

        return str_replace('  ', ' ', $elapsed);
    }

    public static function stripWordsCharacters($string)
    {
        return str_replace(
            [
                "\x80",
                "\x82",
                "\x84",
                "\x85",
                "\x8C",
                "\x91",
                "\x92",
                "\x93",
                "\x94",
                "\x96",
                "\x97",
                "\x98",
                "\x9C",
                '&#9679;',
                '&#8209;',
                '&#61607;',
                '&#61664;',
                '&#8805;',
                '',
                ''],
            [
                "\xA4",
                "\x27",
                "\x22",
                '...', 'OE',
                "\x27",
                "\x27",
                "\x22",
                "\x22",
                "\x2D",
                "\x2D",
                "\x7E",
                'oe',
                "\x2D",
                "\x2D",
                ' -',       ':',         "\x3E \x3D",
                "'", '-'],
            $string
        );
    }

    public static function getSystemUser($path = __FILE__)
    {
        return shell_exec("stat --format \"%U\" $path");
    }

    public static function getDateAWeekAgo()
    {
        $time = time() - (60 * 60 * 24 * Atexo_Config::getParameter('ACCUEIL_ATHENTIFIE_PERIODE_NOUVEAU_REGISTRE'));
        $last_week = date('Y-m-d', $time);

        return $last_week;
    }

    public static function getDateMonthsAgo($nombreMois)
    {
        $date = date('Y-m-d', mktime(0, 0, 0, date('m') - $nombreMois, date('d'), date('Y')));

        return $date;
    }

    public static function dateDansFutur($dateRef, $nbrHeures = 0, $nbrJours = 0, $nbrMois = 0, $nbrAnnees = 0, $nbrMinute = 0, $nbrSecondes = 0)
    {
        $dateArray = explode(' ', $dateRef);
        [$annee, $moi, $jour] = explode('-', $dateArray[0]);
        [$heure, $minute, $seconde] = explode(':', $dateArray[1]);
        // mktime fait automatiquement le calcul
        $time = mktime(
            (int)$heure + $nbrHeures,
            (int)$minute + $nbrMinute,
            (int)$seconde + $nbrSecondes,
            (int)$moi + $nbrMois,
            (int)$jour + $nbrJours,
            (int)$annee + $nbrAnnees
        );
        return date('Y-m-d H:i:s', $time);
    }

    public static function formateIsoDate($dateRef, $format)
    {
        $dateArray = explode(' ', $dateRef);
        [$annee, $moi, $jour] = explode('-', $dateArray[0]);
        [$heure, $minute, $seconde] = explode(':', $dateArray[1]);
        // mktime fait automatiquement le calcul
        $time = mktime($heure, $minute, $seconde, $moi, $jour, $annee);

        return date($format, $time);
    }

    public static function validateXml($xml, $pathXsd, $returnMessageErreur = false)
    {
        $domDocument = new \DOMDocument();
        $domDocument->loadXML($xml);
        try {
            return $domDocument->schemaValidate($pathXsd);
        } catch (\Exception $e) {
            if ($returnMessageErreur) {
                return $e->getMessage();
            }

            return false;
        }
    }

    /*
     * convertis une date format C (iso860) to iso.
     */
    public static function iso860TimeToIso($iso)
    {
        sscanf($iso, '%4u-%u-%uT%u:%2u:%2uZ', $annee, $mois, $jour, $heure, $minute, $seconde);
        $newTstamp = mktime($heure, $minute, $seconde, $mois, $jour, $annee);

        return date('Y-m-d H:i:s', $newTstamp);
    }

    /**
     * retourne la taille du blob.
     */
    public static function getTailleBlob($organisme, $idBlob, $withName = true)
    {
        $atexoBlob = new Atexo_Blob();
        if ($withName) {
            return self::GetSizeName($atexoBlob->getTailFile($idBlob, $organisme));
        } else {
            return $atexoBlob->getTailFile($idBlob, $organisme);
        }
    }

    /**
     * Coupe une chaine de caractère à la longueur max si elle dépasse cette taille.
     */
    public static function cutString($text, $longueurMax)
    {
        if ($longueurMax > 0) {
            if (strlen($text) > $longueurMax) {
                return substr($text, 0, $longueurMax - 3).'...';
            }

            return $text;
        }
    }

    /**
     * Si le texte est long on affiche que les 40 premiers caracteres.
     */
    public static function truncateTexte($texte, $howMach, $lieu = false)
    {
        if ($texte) {
            $truncatedText = '';
            $maximumNumberOfCaracterToReturn = $howMach;
            if ($lieu) {
                $arrayText = explode(',', $texte);
            } else {
                $arrayText = explode(' ', $texte);
            }
            $indexArrayText = 0;
            $textlenght = strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= $arrayText[$indexArrayText];
            while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
                ++$indexArrayText;
                if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                    break;
                }
                $textlenght += strlen($arrayText[$indexArrayText]) + 1;
                if ($lieu) {
                    $truncatedText .= ','.$arrayText[$indexArrayText];
                } else {
                    $truncatedText .= ' '.$arrayText[$indexArrayText];
                }
            }

            return $truncatedText;
        }
    }

    public static function toUtf8($text)
    {
        //echo "#".$text." ".mb_detect_encoding($text)."#";
        if (self::isUTF8($text)) {
            return $text;
        }

        return utf8_encode($text);
    }

    /*
     * Retourne -1 si dateRef1  est inférieure à dateRef2 ; > 0 si dateRef1  est supérieure à dateRef2 , et 0 si les deux dates sont égales.
     *
     */

    public static function cmpIsoDateTime($dateRef1, $dateRef2)
    {
        $dateArray1 = explode(' ', $dateRef1);
        [$annee1, $moi1, $jour1] = explode('-', $dateArray1[0]);
        [$heure1, $minute1, $seconde1] = explode(':', $dateArray1[1]);
        // mktime fait automatiquement le calcul
        $time1 = mktime($heure1, $minute1, $seconde1, $moi1, $jour1, $annee1);

        $dateArray2 = explode(' ', $dateRef2);
        [$annee2, $moi2, $jour2] = explode('-', $dateArray2[0]);
        [$heure2, $minute2, $seconde2] = explode(':', $dateArray2[1]);
        // mktime fait automatiquement le calcul
        $time2 = mktime($heure2, $minute2, $seconde2, $moi2, $jour2, $annee2);

        if ($time1 < $time2) {
            return -1;
        } elseif ($time1 == $time2) {
            return 0;
        }

        return 1;
    }

    public static function fromUtf8ToHttpEncoding($texte)
    {
        if ('utf-8' == Atexo_Config::getParameter('HTTP_ENCODING')) {
            return $texte;
        }

        return utf8_decode($texte);
    }

    /*
     * Permet de verifier si la fonction truncateTexte() doit etre appelée
     */
    public static function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > $nbrCaractere) ? true : false;
    }

    /**
     * Permet de savoir si le premier caractère d'une chaine est une voyelle normale (a e u i o y) ou accentué (àáâãäåòóôõöøèéêëçìíîïùúûüÿ).
     */
    public static function isFirstCharVowel($texte)
    {
        return strstr('aàáâãäåoòóôõöøeèéêëiìíîïuùúûüyÿ', strtolower(substr($texte, 0, 1)));
    }

    public static function nl2Chaine($string, $chaine)
    {
        return preg_replace("/(\r\n)+|(\n|\r)+/", $chaine, $string);
    }

    public static function createZipFile($pathDir)
    {
        $cmd = 'cd '.escapeshellarg($pathDir).' ;  zip -r '.$pathDir.'.zip'.' *';
        system("$cmd 2>&1 > ".Atexo_Config::getParameter('COMMON_TMP').session_id().'zip.log', $sys_answer);
        return $sys_answer;
    }

    public static function deleteDir($f)
    {
        if (is_dir($f)) {
            foreach (glob($f.'/*') as $sf) {
                if (is_dir($sf) && !is_link($sf)) {
                    static::deleteDir($sf);
                    rmdir($sf);
                } else {
                    @unlink($sf);
                }
            }
            rmdir($f);
        } else {
            exit("Error: not a directory - $f");
        }
    }

    public static function downloadLocalFile($pathFile, $nameFile)
    {
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$nameFile.'";');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize($pathFile));
        $fp = fopen($pathFile, 'rb');
        while (!feof($fp) && 0 == connection_status()) {
            set_time_limit(0);
            echo fread($fp, 1024 * 8);
            flush();
            ob_flush();
        }
        fclose($fp);
    }

    public static function getSemestreByWeek($week)
    {
        if ($week >= 1 && $week < 26) {
            return '1er semestre';
        }

        return '2eme semestre';
    }

    public static function deleteDirectory($current_dir)
    {
        if ($dir = @opendir($current_dir)) {
            while (($f = readdir($dir)) !== false) {
                if ($f > '0' and 'file' == filetype($current_dir.$f)) {
                    unlink($current_dir.$f);
                } elseif ($f > '0' and 'dir' == filetype($current_dir.$f)) {
                    self::deleteDirectory($current_dir.$f.'\\');
                }
            }
            closedir($dir);
        }
        rmdir($current_dir);
    }

    public static function setInIsoIfEncodingUTF8($str)
    {
        if (0 == strcmp(Atexo_Config::getParameter('HTTP_ENCODING'), 'utf-8')) {
            return $str;
        }

        return self::utf8ToIso($str);
    }

    public static function atexoHtmlEntities($str)
    {
        return htmlspecialchars($str, ENT_NOQUOTES | ENT_QUOTES, Atexo_Config::getParameter('HTTP_ENCODING'));
    }

    public static function atexoHtmlEntitiesDecode($str)
    {
        return htmlspecialchars_decode($str, ENT_NOQUOTES | ENT_QUOTES);
    }

    public static function dateDansPasse($dateRef, $nbrHeures = 0, $nbrJours = 0, $nbrMois = 0, $nbrAnnees = 0, $withoutHour = false)
    {
        $dateArray = explode(' ', $dateRef);
        [$annee, $moi, $jour] = explode('-', $dateArray[0]);
        [$heure, $minute, $seconde] = explode(':', $dateArray[1]);
        // mktime fait automatiquement le calcul
        $time = mktime((int)$heure + $nbrHeures, (int)$minute, (int)$seconde, (int)$moi - $nbrMois, (int)$jour - $nbrJours, (int)$annee - $nbrAnnees);
        if ($withoutHour) {
            return date('Y-m-d', $time).' 00:00:00';
        }

        return date('Y-m-d H:i:s', $time);
    }

    public static function getTrimestresAnnee($paramAnnee)
    {
        $annee = null;
								$tableauTrimestres = [];
        if (!$paramAnnee) {
            $date = date('Y');
            $arrayDate = explode('-', $date);
            $paramAnnee = $arrayDate[0];
        }
        /**
         * Si l'an en cours est supérieur = $paramAnnee,
         *  je récupère tous les trimestre de $paramAnnee.
         */
        if (date('Y') > $paramAnnee) {
            $tableauTrimestres[0]['debut'] = $annee.'-01-01';
            $tableauTrimestres[0]['fin'] = $annee.'-03-31';
            $tableauTrimestres[0]['trimestre'] = 'T1';

            $tableauTrimestres[1]['debut'] = $annee.'-04-01';
            $tableauTrimestres[1]['fin'] = $annee.'-06-30';
            $tableauTrimestres[1]['trimestre'] = 'T2';

            $tableauTrimestres[2]['debut'] = $annee.'-07-01';
            $tableauTrimestres[2]['fin'] = $annee.'-09-30';
            $tableauTrimestres[2]['trimestre'] = 'T3';

            $tableauTrimestres[3]['debut'] = $annee.'-10-01';
            $tableauTrimestres[3]['fin'] = $annee.'-12-31';
            $tableauTrimestres[3]['trimestre'] = 'T4';
        }
        /*
         **Retourner les trimestres précédents.
         */
        else {
            //trimestre en cours
            $TrimestreEncours = [];
            $TrimestreEncours = self::getTrimestre(null);
            //Récupérer tous les trimestres qui precedent ce trimestre.

            if ('T4' == $TrimestreEncours['trimestre']) {
                $tableauTrimestres[0]['debut'] = $annee.'-07-01';
                $tableauTrimestres[0]['fin'] = $annee.'-09-30';
                $tableauTrimestres[0]['trimestre'] = 'T3';

                $tableauTrimestres[1]['debut'] = $annee.'-04-01';
                $tableauTrimestres[1]['fin'] = $annee.'-06-30';
                $tableauTrimestres[1]['trimestre'] = 'T2';

                $tableauTrimestres[2]['debut'] = $annee.'-01-01';
                $tableauTrimestres[2]['fin'] = $annee.'-03-31';
                $tableauTrimestres[2]['trimestre'] = 'T1';
            }
            if ('T3' == $TrimestreEncours['trimestre']) {
                $tableauTrimestres[0]['debut'] = $annee.'-04-01';
                $tableauTrimestres[0]['fin'] = $annee.'-06-30';
                $tableauTrimestres[0]['trimestre'] = 'T2';

                $tableauTrimestres[1]['debut'] = $annee.'-01-01';
                $tableauTrimestres[1]['fin'] = $annee.'-03-31';
                $tableauTrimestres[1]['trimestre'] = 'T1';
            }
            if ('T2' == $TrimestreEncours['trimestre']) {
                $tableauTrimestres[0]['debut'] = $annee.'-01-01';
                $tableauTrimestres[0]['fin'] = $annee.'-03-31';
                $tableauTrimestres[0]['trimestre'] = 'T1';
            }
        }

        return $tableauTrimestres; // tableau de trimestre.
    }

    public static function clean($chaine)
    {
        $chaine = str_replace('"', '_', $chaine);
        $chaineInput = "°&'+#^<?'*-~> ¡¢£¤¥¦¨©ª«¬®¯°±²³'µ¶¸¹º»¿ÆÐ×ØÝÞßæ÷þ&,'´";
        $chaineOutput = '______________________________________________________________';

        return strtr($chaine, $chaineInput, $chaineOutput);
    }

    /*
     * Converti des caractères ASCII compris entre 128 et 255(les caractères spéciaux de word) en entité html
     * Convertir préalablement la chaine d'entrée en CP1252
     * @param: chaine contenant les caractères spéciaux
     * @return: chaine avec caractères spéciaux remplacés par leurs codes html
     */
    public static function ascii2entities($string)
    {
        for ($i = 128; $i <= 255; ++$i) {
            $entity = htmlentities(chr($i), ENT_QUOTES, 'CP1252');
            $temp = substr($entity, 0, 1);
            $temp .= substr($entity, -1, 1);
            if ('&;' != $temp) {
                $string = str_replace(chr($i), '', $string);
            } else {
                $string = str_replace(chr($i), $entity, $string);
            }
        }

        return $string;
    }

    /*
     * Convertit une chaine à l'encodage http
     * $param: fileName
     * @return: file convertit à l'encodage http
     */
    public static function convertFileToHttpEncoding($file)
    {
        if ('UTF-8' == Atexo_Config::getParameter('HTTP_ENCODING')) {
            $file = self::toUtf8($file);
        }

        return $file;
    }

    /**
     * Retour la date en ayant soustrait le nombre de jours passés en paramètre.
     */
    public static function getDateMinutesAgo($nombreMinutes)
    {
        $date = date('Y-m-d H:i:s', mktime(date('H'), date('i') - $nombreMinutes, date('s'), date('m'), date('d'), date('Y')));
        return $date;
    }

    public static function randomMdp($car)
    {
        $s = '';
        $chaine1 = 'abcdefghijklmnpqrstuvwxyz';
        $chaine2 = '0123456789';
        $chaine3 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $chaine4 = '!@#$*+';
        /*$s1=$chaine2[rand() % strlen($chaine2)];
        $s2=$chaine3[rand() % strlen($chaine3)];
        $s3=$chaine4[rand() % strlen($chaine4)];*/
        mt_srand((float) microtime() * 1_000_000);
        for ($i = 0; $i < $car; ++$i) {
            if ($i < 3) {
                $s .= $chaine1[random_int(0, mt_getrandmax()) % strlen($chaine1)];
            } elseif ($i > 3 && $i < 6) {
                $s .= $chaine2[random_int(0, mt_getrandmax()) % strlen($chaine2)];
            } elseif ($i > 6 && $i < 9) {
                $s .= $chaine3[random_int(0, mt_getrandmax()) % strlen($chaine3)];
            } elseif (9 == $i) {
                $s .= $chaine4[random_int(0, mt_getrandmax()) % strlen($chaine4)];
            } else {
                $s .= $chaine1[random_int(0, mt_getrandmax()) % strlen($chaine1)];
            }
        }

        return $s;
    }

    public static function getVersionMPE($pathFile)
    {
        $version = 'Inconnue, non maintenue par ATEXO';

        try {
            $cvs = file($pathFile);
            foreach ($cvs as $line) {
                if ('/' == substr($line, 0, 1)) {
                    $version = preg_replace("/.*\/\/T(.+)/U", '\\1', $line);
                    break;
                }
            }
        } catch (\Exception) {
            return $version;
        }
    }

    public static function getMajMPE($pathFile)
    {
        $date = 'Inconnue, non maintenue par ATEXO';
        try {
            return date('d/m/Y', filemtime($pathFile));
        } catch (\Exception) {
            return $date;
        }
    }

    public static function trierTableauxMultiDimensionnels($array, $by, $order, $type = null)
    {
        //$array: the array you want to sort
        //$by: the associative array name that is one level deep
        ////example: name
        //$order: ASC or DESC
        //$type: num or str

        $sortby = "sort$by"; //This sets up what you are sorting by
        $firstval = current($array); //Pulls over the first array

        $vals = array_keys($firstval); //Grabs the associate Arrays

        foreach ($vals as $init) {
            $keyname = "sort$init";
            ${$keyname} = [];
        }
        //This was strange because I had problems adding
        //Multiple arrays into a variable variable
        //I got it to work by initializing the variable variables as arrays
        //Before I went any further

        foreach ($array as $key => $row) {
            foreach ($vals as $names) {
                $keyname = "sort$names";
                $test = [];
                $test[$key] = $row[$names];
                ${$keyname} = array_merge(${$keyname}, $test);
            }
        }

        //This will create dynamic mini arrays so that I can perform
        //the array multisort with no problem
        //Notice the temp array... I had to do that because I
        //cannot assign additional array elements to a
        //varaiable variable

        // le tri sera insensible à la casse
        $array_lowercase = array_map('strtolower', ${$sortby});

        if ('DESC' == $order) {
            if ('num' == $type) {
                array_multisort($array_lowercase, SORT_DESC, SORT_NUMERIC, $array);
            } else {
                array_multisort($array_lowercase, SORT_DESC, SORT_STRING, $array);
            }
        } else {
            if ('num' == $type) {
                array_multisort($array_lowercase, SORT_ASC, SORT_NUMERIC, $array);
            } else {
                array_multisort($array_lowercase, SORT_ASC, SORT_STRING, $array);
            }
        }

        //This just goed through and asks the additional arguments
        //What they are doing and are doing variations of
        //the multisort
        return $array;
    }

    public static function getDiffDateMinute($date1, $date2)
    {
        $today = strtotime($date1);
        $date = strtotime($date2);

        return round(abs($today - $date) / 60);
    }

    /**
     * Permet de télécharger un fichier à la vollée.
     *
     * @param $contenuFichier: contenu du fichier à télécharger
     * @param $nomFichier: nom du fichier au téléchargement
     */
    public static function telechargerFichierNonStocke($contenuFichier, $nomFichier)
    {
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: application/octet-stream');
        header("Content-Disposition: attachment; filename=$nomFichier;");
        header('Content-Transfer-Encoding: binary');
        echo file_get_contents($contenuFichier);
        exit;
    }

    /**
     * Permet de remplacer les caractères spéciaux par leurs codes html et encode en utf8.
     */
    public static function strToUtf8Xml($string)
    {
        $string = self::replaceSpecialCharacters($string);

        return self::toUtf8(self::escapeXmlChars($string));
    }

    /**
     * Permet de remplacer dans la chaine les caractères spéciaux word par leurs codes html.
     */
    public static function replaceCaracSpec($chaine)
    {
        $carac = ['¡', '¢', '£', '¤', '¥', '¦', '§', '¨', '©', 'ª', '«', '¬', '­', '®', '¯', '°', '±', '²', '³', '´', 'µ', '¶', '·', '¸', '¹', 'º', '»', '¼', '½', '¾', '¿', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë',
            'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', '×', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', '÷', 'ø', 'ù', 'ú', 'û', 'ü', 'ý',
            'þ', 'ÿ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ];

        $value = ['&#161;', '&#162;', '&#163;', '&#164;', '&#165;', '&#166;', '&#167;', '&#168;', '&#169;', '&#170;', '&#171;', '&#172;', '&#173;', '&#174;', '&#175;', '&#176;', '&#177;', '&#178;', '&#179;',
            '&#180;', '&#181;', '&#182;', '&#183;', '&#184;', '&#185;', '&#186;', '&#187;', '&#188;', '&#189;', '&#190;', '&#191;', '&#192;', '&#193;', '&#194;', '&#195;', '&#196;', '&#197;', '&#198;', '&#199;',
            '&#200;', '&#201;', '&#202;', '&#203;', '&#204;', '&#205;', '&#206;', '&#207;', '&#208;', '&#209;', '&#210;', '&#211;', '&#212;', '&#213;', '&#214;', '&#215;', '&#216;', '&#217;', '&#218;', '&#219;',
            '&#220;', '&#221;', '&#222;', '&#223;', '&#226;', '&#227;', '&#228;', '&#229;', '&#230;', '&#231;', '&#234;', '&#235;', '&#236;', '&#237;', '&#238;', '&#239;', '&#240;', '&#241;', '&#242;', '&#243;',
            '&#244;', '&#245;', '&#246;', '&#247;', '&#248;', '&#249;', '&#250;', '&#251;', '&#252;', '&#253;', '&#254;', '&#255;', '&#338;', '&#339;', '&#352;', '&#353;', '&#376;', '&#402;', '&#8211;', '&#8212;',
            '&#8216;', '&#8217;', '&#8218;', '&#8220;', '&#8221;', '&#8222;', '&#8224;', '&#8225;', '&#8226;', '&#8230;', '&#8240;', '&#8364;', '&#8482;', ];

        return str_replace($carac, $value, $chaine);
    }

    /**
     * Permet d'obtenir l'entité normal à partir du code html.
     */
    public static function ascii_to_entities($text) // because of the html_entity_decode() bug with UTF-8
    {
        $text = html_entity_decode($text, ENT_QUOTES, 'ISO-8859-1'); //NOTE: UTF-8 does not work!
        $text = preg_replace('/&#(\d+);/m', 'chr(\\1)', $text); //decimal notation
        $text = preg_replace('/&#x([a-f0-9]+);/mi', 'chr(0x\\1)', $text);  //hex notation

        return $text;
    }

    /**
     * Permet de remplacer les caractères spéciaux microsoft word par caractères normaux claviers respectifs.
     */
    public static function replaceCharactersMsWordWithRegularCharacters($chaine)
    {
        $carac = ['«', '»', '', '', '', '"', '<', '>', '', '-', '', "'", '¨', '',  '', '', "\x80", "\x82", "\x84", "\x85", "\x8C", "\x91", "\x92", "\x93", "\x94", "\x96", "\x97", "\x98",
             "\x9C", '&#9679;', '&#8209;', '&#61607;', '&#61664;', '&#8805;',  "\u2026", "\u0085", "\u0152", "\u2019", "\u00E2\u0080\u0099", "\u2018", "\u0092", '&#x20KM;', "\u20AC",
             "\u0080",  "\uFFFD",  "\u00E2\u0082\u00AC",  "\u00AB", "\u00BB", "\u201D", "\u201C", "\u2264", "\u0153", "\u009C", "\u0095", "\u25CF", '&#x25MP;', '&#x2019;', "\u0092",
             "\u2265", "\u2013", "\u0096", "\u2022", '¼', '½', '¾', '¡', '«', '»', '£', '¥', '¿', '¢', '°', '', '', '&#61485;', '?', '', ];
        $value = ['"', '"', '...', 'OE', 'EUR', '"', '<', '>', 'oe', '-', "'", "'", '..', '-', 'Y', "'", "\xA4", "\x27", "\x22", '...', 'OE',   "\x27", "\x27", "\x22", "\x22", "\x2D", "\x2D", "\x7E",
             'oe', "\x2D", "\x2D", ' -', ':', "\x3E \x3D", '...',  '...', 'OE', "'", "'", "'", "'", 'euro(s)', 'euro(s)', 'euro(s)', 'euro(s)', 'euro(s)', '"', '"', '"',
             '"', '<=', 'oe', 'oe', '-', '-', '-', "'",  "'", '>=', '-', '-', '-', '1/4', '1/2', '3/4', 'i', '<<', '>>', 'Livre sterling', 'Yen', '-', '-', 'o', "'", '-', '-', '-', '', ];

        return str_replace($carac, $value, $chaine);
    }

    /**
     * Permet de remplacer les codes html par caractères normaux claviers respectifs.
     */
    public static function replaceHtmlCodeWithNormalCharacters($chaine)
    {
        $carac = ['\u2026', '\u0085', '\u0152', '\u00AB', '\u00BB', '\u201D', '\u201C', '\u2264', '\u2265', '\u0153', '\u009C', '\u2013', '\u0096', '\u2022', "\u2019", '\u00E2', '\u0080', '\u0099', '\u2018',
            '\u0092', '&#x2019;', "\u0092", ];
        $value = ['...', '...', 'OE', '"', '"', '"', '"', '<', '>', 'oe', 'oe', '-', '-', '-', "'", "'", "'", "'", "'", "'", "'", "'"];

        return str_replace($carac, $value, $chaine);
    }

    /**
     * Permet de remplacer les caractères spéciaux microsoft word et codes html par caractères normaux claviers
     * respectifs.
     */
    public static function replaceSpecialCharacters($chaine)
    {
        if ('utf8' != strtolower(Atexo_Config::getParameter('DB_ENCODING'))) {
            return self::replaceCharactersMsWordWithRegularCharacters(
                self::replaceHtmlCodeWithNormalCharacters($chaine)
            );
        }

        return $chaine;
    }

	/**
	 * Permet d'encoder la chaine d'entrée au meme encodage que celui de la plateforme
	 * @param $str: chaine d'entrée
	 * @return : la chaine encodée
	 */
	public function toHttpEncoding($str) {
        $encodageStr = mb_detect_encoding($str,'ASCII,UTF-8,ISO-8859-15');
        if(strtoupper(Atexo_Config::getParameter('HTTP_ENCODING')) != $encodageStr) {
            return mb_convert_encoding($str, Atexo_Config::getParameter('HTTP_ENCODING'), $encodageStr);
        }
        return $str;
	}

    /**
     * fct recurcive qui remet un tableau qui a l'encodage de la PF.
     *
     * @param mixed $value
     *
     * @return mixed l'input qui a l'encodage de la PF
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2015
     */
    public static function arrayToHttpEncoding($value)
    {
        if (is_array($value)) {
            $return = [];
            foreach ($value as $key => $element) {
                $return[$key] = self::arrayToHttpEncoding($value[$key]);
            }

            return $return;
        } elseif (is_string($value)) {
            return self::toHttpEncoding($value);
        } else {
            return $value;
        }
    }

    public static function removeMilliSecond($string)
    {
        $date = explode('.', $string);
        if ($date[1]) {
            $sousDate = explode(' ', $date[1]);
            $l = count($sousDate);
            $str = '';
            for ($i = 1; $i < $l; ++$i) {
                $str .= ' '.$sousDate[$i];
            }
            $date[1] = $str;
        }
        $newDate = implode('', $date);

        return $newDate;
    }

    /**
     * Encode la chaine d'entrée avec l'encodage http.
     */
    public static function encodeToHttp($str)
    {
        if ('ISO-8859-1' == Atexo_Config::getParameter(
                'HTTP_ENCODING'
            ) || 'iso-8859-1' == Atexo_Config::getParameter('HTTP_ENCODING')) {
            return self::utf8ToIso($str);
        }
        if ('UTF-8' == Atexo_Config::getParameter(
                'HTTP_ENCODING'
            ) || 'utf-8' == Atexo_Config::getParameter('HTTP_ENCODING')) {
            return self::toUtf8($str);
        }
    }

    /**
     * Pvérifier si une chaine de caractères respect l'expression d'un mail ou d'un ensemble de mails.
     *
     * @param string $text le text à vérifier
     *
     * @return array tableau des emails erronés || true si ok
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function checkMultipleEmail($text)
    {
        $tab_emails = explode(',', strtolower($text));
        $tab_emails_errone = [];
        foreach ($tab_emails as $email) {
            if (false == filter_var(trim($email), FILTER_VALIDATE_EMAIL)) {
                $tab_emails_errone[] = $email;
            }
        }
        if (count($tab_emails_errone) > 0) {
            return $tab_emails_errone;
        }

        return true;
    }

    /**
     * Pvérifier si une chaine de caractères respect l'expression d'un mail.
     *
     * @param string $email le text à vérifier
     *
     * @return bool
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public static function checkEmailFormat($email)
    {
        return filter_var(trim($email), FILTER_VALIDATE_EMAIL);
    }

    /**
     * Permet de calculer le decalage horaire d'une dateTime par rapport au GMT.
     *
     * @param  $dateRef: la date de référence
     *
     * @return : le decalage horaire calculée en nombre d'heure
     */
    public static function getDecalageHoraireParRapportGmt($dateRef)
    {
        $dateRefEnFormatDateTime = new DateTime(self::frnDateTime2iso($dateRef));
        //Instanciation de DateTimeZone pour 'Europe/Paris'
        $dateTimeZoneParis = new \DateTimeZone(Atexo_Config::getParameter('TIMEZONE'));
        //Calcul du decalage horaire (en secondes) de la date limite de remise des plis par rapport à l'heure paris
        $decalageHoraire = $dateTimeZoneParis->getOffset($dateRefEnFormatDateTime);
        //Décalage horaire en heure
        $decalageHoraire = $decalageHoraire / (60 * 60);

        return $decalageHoraire;
    }

    /**
     * Permet de calculer l'extension de la signature.
     *
     * @param  $signature: la signature en base64
     *
     * @return : l'extension de la signature
     */
    public static function getExtensionSignature($signature)
    {
        $signature = base64_decode($signature);
        if (strpos($signature, 'SignedInfo')) {
            return '.xml';
        }

        return '.p7s';
    }

    // return le correspondant du mois en chiffre

    public static function getMois($chaine)
    {
        $arrayMois = [
            '01' => 'janvier',
            '02' => 'fevrier',
            '03' => 'mars',
            '04' => 'avril',
            '05' => 'mai',
            '06' => 'juin',
            '07' => 'juillet',
            '08' => 'aout',
            '09' => 'septembre',
            '10' => 'octobre',
            '11' => 'novembre',
            '12' => 'decembre',
            ];
        foreach ($arrayMois as $key => $mois) {
            if (strstr($chaine, $mois)) {
                return $key;
            }
        }
    }

    /** convertit une date en ISO.
     * @param $_isoDate date au format yyyy-mm-dd
     *
     * @return date au format YYYY-mm-dd H:i:s ou un tableau de date
     */
    public static function convertDateChaineIso($chaineDate, $formatDate = true)
    {
        $chaineDate = str_replace(' ', '', $chaineDate);
        $jour = substr($chaineDate, 0, 2);
        if (!is_numeric($jour)) {
            $chaineDate = '0' . $chaineDate;
        }
        $jour = substr($chaineDate, 0, 2);
        $annee = substr($chaineDate, strlen($chaineDate) - 4, 4);
        $mois = trim(substr($chaineDate, 2, strlen($chaineDate) - 6));
        $moisE = self::getMois(self::OterAccents(strtolower($mois)));
        if ($formatDate) {
            $newTstampE = mktime(0, 0, 0, $moisE, $jour, $annee);

            return date('Y-m-d H:i:s', $newTstampE);
        }
        $arrayDate = [];
        $arrayDate[0] = $jour;
        $arrayDate[1] = $moisE;
        $arrayDate[2] = $annee;

        return $arrayDate;
    }

    public static function is_base64($s)
    {
        return (bool) preg_match("/^[a-zA-Z0-9\/\r\n+]*={0,2}$/", $s);
    }

    public static function convertHeureChaineToIso($chaineHeure)
    {
        //,à16heurs30 ou ,à16h30
        $arrayDate = [];
        $chaine = str_replace(' ', '', trim($chaineHeure));
        $chaine = rtrim($chaine, '.');
        $chaine = str_replace(',à', '', $chaine);
        $heur = substr($chaine, 0, 2);
        if (!is_numeric($heur)) {
            $heur = 0;
        }
        if ('he' == substr($chaine, 2, 2)) {
            $min = 0;
        } elseif ('h' == substr($chaine, 2, 1)) {
            $min = substr($chaine, 3, 2);
            if (!is_numeric($min)) {
                $min = 0;
            }
        }
        $arrayDate['heure'] = $heur;
        $arrayDate['minute'] = $min;

        return $arrayDate;
    }

    public static function getMessageFromListe($msgDebut, $arrayOfMsg)
    {
        $message = '<span>' . $msgDebut . '</span>'; //$msgDebut." <br/> <li>";
        $message .= '<div class="spacer-small"></div><span>';
        $msg = implode('</li><li>', $arrayOfMsg);

        return $message.'<ul><li>' . $msg . '</li></ul></span>';
    }

    public static function dateMoinJour($dateRef, $nbrJours)
    {
        [$annee, $moi, $jour] = explode('-', $dateRef);
        $date = mktime(0, 0, 0, $moi, $jour - $nbrJours, $annee);

        return date('Y-m-d', $date);
    }

    public static function replaceArrayCharactersByPourcentagage($keyword)
    {
        $arrayCaractere = ["'", '-', ' ', '"'];
        $word = self::atexoHtmlEntities(str_replace($arrayCaractere, '%', $keyword));

        return $word;
    }

    /**
     * Permet de convertir un fichier xml en array.
     *
     * @param $xml : xml à convertir
     *
     * @return : le tableau
     */
    public static function xmlFileToArray($xmlFile, $typeString = false)
    {
        if ($typeString) {
            $xml = simplexml_load_string($xmlFile);
        } else {
            $xml = simplexml_load_file($xmlFile);
        }
        $json = json_encode($xml, JSON_THROW_ON_ERROR);
        $array = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

        return $array;
    }

    /**
     * Permet de verifier l'existence d'une clé dans un array, de facon recursive.
     *
     * @param varchar       $needle
     * @param array, object $haystack
     * @return: retourne la valeur de la clé dans le tableau
     */
    public static function returnArrayKeyExistsRecursive($needle, $haystack)
    {
        $result = array_key_exists($needle, $haystack);
        if ($result) {
            return $haystack[$needle];
        }
        if (is_array($haystack) && count($haystack)) {
            foreach ($haystack as $v) {
                if (is_array($v) || is_object($v)) {
                    $result = self::returnArrayKeyExistsRecursive($needle, $v);
                }
                if ($result) {
                    return $result;
                }
            }
        }

        return $result;
    }

    /* cette fonction permet d'enlever tout autres chaines autres
     * que les caractéres alphanumérique
     * */
    public static function filter($in)
    {
        $search = ['@[^a-zA-Z0-9_]@'];
        $replace = [''];
        return preg_replace($search, $replace, $in);
    }

    /* cette fonction permet de formatter une chaine
     * en remplaçant les caractéres spéciaux par le vide
     * et enlever les accents
     * */

    public static function formatterChaine($text)
    {
        $textSansAccent = self::OterAccents($text);

        return self::filter($textSansAccent);
    }

    /**
     * Permet de retourner le mimeCode d'un fichier.
     *
     * @param string $filename
     *
     * @return mimeCode
     */
    public static function mime_content_type($filename)
    {
        $mime_types = [
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

        // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

        // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

        // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

        // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

        // ms office
            'doc' => 'application/msword',
            'docx' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'xlsx' => 'application/vnd.ms-excel',
            'pps' => 'application/vnd.ms-powerpoint',
            'ppt' => 'application/vnd.ms-powerpoint',
            'pptx' => 'application/vnd.ms-powerpoint',

        // horodatage
            'tsr' => 'application/timestamp-reply',
               'p7s' => 'application/pkcs7-signature',
            'sig' => 'application/pkcs7-signature',

        // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        ];
								$explode = explode('.', $filename);

        $ext = strtolower(array_pop($explode));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        } elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);

            return $mimetype;
        }

        return 'application/octet-stream';
    }

    /**
     * Permet de determiner l'uid de la plateforme
     * Cette information est necessaire pour la communication entre MPE et d'autres applications (FORMULAIRES).
     *
     * @param $id: identifiant
     */
    public static function builUidPlateforme($id)
    {
        return base64_encode($id.'#'.Atexo_Config::getParameter('UID_PF_MPE'));
    }

    /**
     * Permet de determiner l'id à partir de l'uid de la plateforme.
     *
     * @param $uid
     */
    public static function builIdFromUidPlateforme($uid)
    {
        if ($uid && str_contains($uid, '#')) {
            $arrayValue = explode('#', $uid);

            return $arrayValue[0];
        }

        return false;
    }

    /*************************************************************************
     * Retourner la chaine de dateTime sous forme de la date et retour à ligne
     * l'heure sous la  forme (HH:mm)
     **************************************************************************/
    public static function formatterDate($chaineDateTime)
    {
        if ($chaineDateTime) {
            $heur = substr($chaineDateTime, 10, -3);
            $chaineDate = substr($chaineDateTime, 0, -9);
            $chaineDate = self::iso2frnDate($chaineDate);
            $dateFormate = $chaineDate.' <br>   '.$heur;

            return $dateFormate;
        }
    }

    /*************************************************************************
     * Retourner juste l'anne d'une chaine de date  aaaa/mm/jj*
     **************************************************************************/
    public static function getAnneeFromDate($chaineDate)
    {
        if ($chaineDate) {
            $annee = substr($chaineDate, 0, 4);

            return $annee;
        }
    }

    /*************************************************************************
     * Retourner juste l'anne d'une chaine de date  jj/mm/aaaa*
     **************************************************************************/
    public static function getAnneeFromDate2($chaineDate)
    {
        if ($chaineDate) {
            $annee = substr($chaineDate, -4, 4);

            return $annee;
        }
    }

    /*************************************************************************
     * Arrondir un montant*
     **************************************************************************/
    public static function formatterMontant($montant, $apresVirgule = 2, $sepPoint = false)
    {
        if ('' != $montant && null != $montant) {
            $montant = self::getMontantArronditEspace($montant, $apresVirgule, $sepPoint);
            $montant = str_replace(' ', '', $montant);
        }

        return $montant;
    }

    /*************************************************************************
     * replacer les retour a la ligne par un séparateur (param)
     **************************************************************************/
    public static function replaceRetourALaLigne($chaine, $replaceBy)
    {
        $res = '';
        $i = 0;
        $trouver = false;
        while ($i < strlen($chaine)) {
            if (!$trouver && ("\n" == $chaine[$i] || "\r" == $chaine[$i])) {
                $res .= $replaceBy;
                $trouver = true;
            } elseif ("\n" != $chaine[$i] && "\r" != $chaine[$i]) {
                $res .= $chaine[$i];
                $trouver = false;
            }
            ++$i;
        }

        return $res;
    }

    // test si un nombre est negatif ou positif
    public static function isPositifInteger($var)
    {
        if (is_int($var) && $var > -1) {
            return true;
        }

        return false;
    }

    /***
     * return abrev du nom du jour en anglais (par defaut)
     */
    public static function getFormatedDate($chaineDate, $point = false)
    {
        if ('0000-00-00' != $chaineDate && '' != $chaineDate) {
            $chaineDate = new DateTime($chaineDate);
            if ($chaineDate->format('D ')) {
                return self::getAbrevJourFrancais($chaineDate->format('D '), $point);
            }
        }
    }

    /***
     * returne l'abbriviation du nom du jour en francais
     */
    public static function getAbrevJourFrancais($value, $point = false)
    {
        $arrayJour = ['Sat' => 'Sam', 'Sun' => 'Dim', 'Mon' => 'Lun', 'Tue' => 'Mar', 'Wed' => 'Mer', 'Thu' => 'Jeu', 'Fri' => 'Ven'];
        foreach ($arrayJour as $key => $jour) {
            if (strstr($value, $key)) {
                if ($point) {
                    $jour .= $point;
                }

                return $jour;
            }
        }
    }

    /***
     * Permet de d'enlever les caracters non souhaitées dans une chaine
     * returne un array
     */
    public static function deleteSpecialCharacteresFromChaine($chaine, $carateres = false)
    {
        $arrayResult = [];
        if (!$carateres) {
            $carateres = ",;.&!?'\"()[]{}°+*/\\|:%";
        }
        if ($chaine) {
            $motCle = strtr($chaine, $carateres, '                      ');
            $arrayMotsCle = explode(' ', $motCle);
            foreach ($arrayMotsCle as $mot) {
                if ('' !== $mot) {
                    $arrayResult[] = $mot;
                }
            }
        }

        return $arrayResult;
    }

    /*
     * retourne les extentions qu'on accepte à signer
     * sous la forme d'une chaine de caractères exp : "'PDF', 'DOC', 'ZIP', 'XLS', 'PPT'"
     */
    public static function getExtensionFilesSignatures()
    {
        //return "'PDF' , 'DOC', 'ZIP', 'XLS', 'PPT'";
        //return "'PDF,DOC,DOCX,ZIP,XSL,PPT'";
        return Atexo_Config::getParameter('EXTENSION_FILES_APPLET_SIGNATURE');
    }

    /*
     * retourne la taille d'un contenu fichier
     * param content
     * param nom fichier dans tmp
     * return taile (en ko /mo)
     */
    public static function getContentFileSize($fileName, $content)
    {
        $directory = Atexo_Config::getParameter('COMMON_TMP').session_name().session_id().time() . uniqid() .'/';
        if (!is_dir($directory)) {
            mkdir($directory);
        }
        self::writeFile($directory.$fileName, $content);
        if (is_file($directory.$fileName)) {
            $taille = @filesize($directory.$fileName);
        } else {
            $taille = false;
        }
        $taille = ($taille / 1024);
        $taille = self::arrondirSizeFile($taille);

        return $taille;
    }

    /*
     * return le nom d'un champs sans '_'
     */
    public static function getFormatMethodeFromChamps($nomChamps)
    {
        $champs = explode('_', $nomChamps);
        $champsName = '';
        foreach ($champs as $oneName) {
            $champsName .= ucfirst(strtolower($oneName));
        }

        return $champsName;
    }

    /*
     * retourne la valeur correspondant a un key passé en param
     * param $pathFile :  chemin du fichier
     * param $key : la clé
     * param $columnKey collone de la clé(optional)
     * param $columnValue collone de la value(optional)
     * return value
     */
    public static function returnValueFromCsv($pathFile, $key, $columnKey = false, $columnValue = false)
    {
        if (!$columnKey) {
            $columnKey = 1;
        }
        if (!$columnValue) {
            $columnValue = 2;
        }
        if (($handle = fopen($pathFile, 'r')) !== false) {
            while (($data = fgetcsv($handle, 0, '|')) !== false) {
                if ($data[$columnKey] == $key) {
                    return $data[$columnValue];
                }
            }
            fclose($handle);
        }
    }

    /*
     * Permet de retourner une chaine de caractére à partir d'un dateTime
     * param = $dateTime format yyyy-mm-ddTH:i:s+ms:mms
     * return yyyymmddhhmmss
     */
    public static function formaterDateTimeMilliSecondeToString($dateTime)
    {
        $ArrayDateTime = explode('+', $dateTime);
        if (is_array($ArrayDateTime) && count($ArrayDateTime)) {
            $dateTime = $ArrayDateTime[0];
            $finalDate = str_replace('T', '', str_replace(':', '', str_replace('-', '', $dateTime)));

            return $finalDate;
        }

        return false;
    }

    /** convertit une datetime ISO en string date sans separateurs
     * (utilisable dans un nom de fichier).
     *
     * @param $dateTime date au format yyyy-mm-dd hh:mm:ss
     *
     * @return date au format yyyymmddhhmmss
     */
    public static function isoDateTimeToString($dateTime)
    {
        if (!$dateTime || '0000-00-00 00:00:00' == $dateTime) {
            return;
        }
        $finalDate = str_replace(' ', '', str_replace(':', '', str_replace('-', '', $dateTime)));

        return $finalDate;
    }

    /*
     * Valide le numero de squence pour les flux chorus
     */
    public static function validateNumeroSequance($numSeq, &$erreursEnvoiFlux = null)
    {
        $newNumSeq = '00001';
        if (1 == strlen($numSeq)) {
            $newNumSeq = '0000'.$numSeq;
        } elseif (2 == strlen($numSeq)) {
            $newNumSeq = '000'.$numSeq;
        } elseif (3 == strlen($numSeq)) {
            $newNumSeq = '00'.$numSeq;
        } elseif (4 == strlen($numSeq)) {
            $newNumSeq = '0'.$numSeq;
        } elseif ( 5 == strlen($numSeq) && 1_000_000 != $numSeq) {
            $newNumSeq = $numSeq;
        } else {
            $newNumSeq = null;
            $erreursEnvoiFlux [] = "le numero de sequence est arrivé à la limite " . $numSeq;
        }

        return $newNumSeq;
    }

    /*
     * Valide le numero de squence pour les flux chorus
     */
    public static function validateNumeroSequanceByLenghtElement($numSeq, $lenghtElement)
    {
        if (strlen($numSeq) < $lenghtElement) {
            $newNumSeq = self::getListeZeros($lenghtElement - strlen($numSeq)).$numSeq;
        } else {
            $newNumSeq = $numSeq;
        }

        return $newNumSeq;
    }

    /**
     * @param $dateChaine
     *
     * @return string
     *                TODO
     */
    public static function getListeZeros($lenghtElement)
    {
        $resultat = '';

        for ($i = 0; $i < $lenghtElement; ++$i) {
            $resultat .= '0';
        }

        return $resultat;
    }

    /*
     * Permet de formater un string de format yyyymmdd à une date dd/mm/yyyy
     */
    public static function formaterStringToDate($dateChaine)
    {
        if (8 == strlen($dateChaine)) {
            $y = substr($dateChaine, 0, -4);
            $m = substr($dateChaine, 4, -2);
            $d = substr($dateChaine, 6);

            return $d.'/'.$m.'/'.$y;
        } else {
            return '';
        }
    }

    /*
     * Permet de formater un string de format yyyymmdd à une date
     */
    public static function formaterStringToMontant($montantTotal)
    {
        $montant = str_replace('+', '', $montantTotal);
        $montant = ltrim($montant, '0');

        return $montant;
    }

    /*
      * verifier le nom de fichier
      * param1 : le nom de zip (un seul niveau)
      * param2 : nom de fichier à verifier
      * return le nom de fichier
      */
    public static function verifierNameFile($zip, $nomFichier, &$arrayFile)
    {
        if ($arrayFile[$zip][$nomFichier]) {
            $fichier = explode('.', $nomFichier);
            $extention = '.'.$fichier[count($fichier) - 1];
            $ext = strlen($extention);
            $nomFile = substr($nomFichier, 0, -$ext);
            $nameFile = $nomFile.'-'.$arrayFile[$zip][$nomFichier].$extention;
            ++$arrayFile[$zip][$nomFichier];
        } else {
            $nameFile = $nomFichier;
            $arrayFile[$zip][$nomFichier] = 1;
        }

        return $nameFile;
    }

    /**
     * Converti une chaine en utf8 depuis l'encodage de la plateforme.
     *
     * @param string $str: chaine à encoder
     *
     * @return $str: chaine encodée
     */
    public static function httpEncodingToUtf8($str)
    {
        //La fonction json_decode doit recevoir un json encodé en utf8, c'est ce qui nous a fait penser à developper cette fonction
        if (false === mb_detect_encoding($str, 'UTF-8', true)) {
            $str = utf8_encode($str);
        }

        return $str;
    }

    /**
     * Permet de determiner le systeme d'exploitation du visiteur du site.
     */
    public static function getOsVisiteur()
    {
        $listeOs = ['Windows', 'Mac', 'Linux', 'FreeBSD', 'SunOS', 'IRIX', 'BeOS', 'OS/2', 'AIX'];
        foreach ($listeOs as $os) {
            if (stripos($_SERVER['HTTP_USER_AGENT'], $os)) {
                return $os;
            }
        }

        return 'Autre';
    }

    /**
     * Permet d'encoder une chaine vers l'encodage de l'os du visiteur.
     *
     * @param string $chaine
     */
    public static function encodeToOsVisiteur($chaine)
    {
        if (self::isWindowsOs()) {
            $chaine = iconv('CP1252', 'CP850', $chaine);
        }

        return $chaine;
    }

    public static function isWindowsOs()
    {
        return 'Windows' == self::getOsVisiteur();
    }

    /*
     * Permet d'avoir un int selon la valeur string dans le cas true false
     *
     * @param $var(String)
     * @return $var(int)
     * @author : loubna.ezziani@atexom.com
     * @version : since 4.6.0
     */
    public static function getIntFromValboolean($var)
    {
        $var = trim($var);
        if ('true' === $var) {
            return '1';
        } elseif ('false' === $var) {
            return '0';
        } else {
            return $var;
        }
    }

    /**
     * permet d'avoir la chaine "true" "false" d'aprés un int.
     *
     * @param int ou String $var,Boolean $maj metre en majuscule
     *
     * @return string $var
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public static function getBooleanStringFromValInt($var, $maj = false)
    {
        $var = trim($var);
        $resultat = $var;
        if ('0' == $var || 0 == $var) {
            $resultat = 'false';
        } elseif ('1' == $var || 1 == $var) {
            $resultat = 'true';
        }
        if ($maj) {
            return strtoupper($resultat);
        } else {
            return strtolower($resultat);
        }
    }

    /*
     * Permet de verifier l'integrite d'un fichier DCE
     *
     * @param $pathFile chemin du fichier
     * @return statut (boolean)
     * @author : loubna.ezziani@atexom.com
     * @version : since 4.6.0
     */
    public static function verifyZipIntegrity($pathFile)
    {
        $infile = Atexo_Config::getParameter('COMMON_TMP').'dce_'.session_id().uniqid().'.zip';
        if (copy($pathFile, $infile)) {
            // tester l'integrite du fichier zip
            // -T revient au zip precedent en cas d'echec
            $cmd = 'nice zip -T '.escapeshellarg($infile).'  >> ' . Atexo_Config::getParameter(
                    'COMMON_TMP'
                ) . 'zip_error_adil 2>&1';
            @system($cmd, $sys_answer);
            unlink($infile);
            if (0 == $sys_answer) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*
     * Permet de retourner la date à partir d'une date d'un fichier XML
     *
     * @param $date dateTime from XML
     * @return $date dateTime
     * @author : loubna.ezziani@atexom.com
     * @version : since 4.6.0
     */
    public static function getDateTimeValideFromDateXml($date)
    {
        if (!$date) {
            return '';
        } else {
            if ('0000-00-00 00:00:00' == $date) {
                return $date;
            }

            return self::getDateTimeByFormat($date);
        }
    }

    /*
     * Permet de retourner la date à inserer dan un fichier XML
     *
     * @param $date dateTime
     * @return $date dateTime to XML
     * @author : loubna.ezziani@atexo.com
     * @version : since 4.6.0
     */
    public static function getDateXmlFromDateTimeVal($date)
    {
        if (!$date || '0000-00-00 00:00:00' == $date) {
            return '';
        }

        return self::getDateTimeByFormat($date, 'c');
    }

    /*
     * Vérifie si le path a créé est safe
     *
     * @param string $path
     * @param string $data
     * @return boolean
     * @author : anas.rhorbal@atexo.com
     * @version : since 4.6.0
     */
    public static function isSafePath($path, $data)
    {
        $posSlash = strrpos($data, '/');
        if (false !== $posSlash) {
            $pathData = substr($data, 0, $posSlash);
        } else {
            $pathData = '';
        }
        $realpath = realpath($path);
        $realpathData = realpath($path.'/'.$pathData);
        $pos = strpos($realpathData, (string) $realpath);

        if (false !== $pos && 0 == $pos) {
            return true;
        }

        return false;
    }

    /*
     * Vérifie si le nom est safe
     *
     * @param string $name
     * @return boolean
     * @author : anas.rhorbal@atexo.com
     * @version : since 4.6.0
     */
    public static function isSafeFileName($name)
    {
        $searchs = ['%0d%0a', "\n", "\n\r", "\r\n"];

        foreach ($searchs as $search) {
            $pos = strpos($name, $search);
            if (false !== $pos) {
                return false;
            }
        }

        return true;
    }

    /*
     * Vérifie si la date est du format ISO
     *
     * @param string $date
     * @return boolean
     * @author : loubna.ezziani@atexo.com
     * @version : since 4.6.0
     */
    public static function isFormatDateIso($_isoDate)
    {
        if (strstr($_isoDate, '-')) {
            [$y, $m, $d] = explode('-', $_isoDate);
            if (4 != strlen($y) || strlen($d) > 2 || strlen($m) > 2) {
                return false;
            }
            if ((!self::isEntier($d)) || (!self::isEntier($m)) || (!self::isEntier($y))) {
                return false;
            }
            if ($m > 12 || $d > 31) {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de construire un tableau de liste des pays.
     *
     * @return array: tableau des codes pays
     */
    public static function listeCodesPays()
    {
        $listCountries = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveListCountries(false);
        $dataSource = [];
        if (is_array($listCountries) && count($listCountries)) {
            foreach ($listCountries as $oneCountrie) {
                $dataSource[$oneCountrie->getId()] = $oneCountrie->getDenomination2();
            }

            return $dataSource;
        } else {
            return false;
        }
    }

    /**
     * Permet de couper une chaine une fois on est arrivé au maximum des caracteres.
     *
     * @param string str, la chaine à trunquer
     * @param nombre $max,      le max de caracteres de la chaine
     * @param string $strToAdd, ajouter à la fin de la chaine
     * @return string xml contenant les infos de la consultation
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public static function truncateChaine($str, $max, $strToAdd = '')
    {
        if (strlen($str) > $max) {
            $str = substr($str, 0, $max).$strToAdd;
        }

        return $str;
    }

    /**
     * Retourne un tableau contenant toutes les années se trouvant dans la fourchette  : année courante -
     * $nombreAnnee,année courante -1.
     *
     * @param int $nombreAnnee le nombre max des annees
     *
     * @return array tableau des annees
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public static function arrayAnneesDerniers($nombreAnnee)
    {
        $anneeCourrante = (int)date('Y');
        $arrayAnne = [];
        for ($i = 1; $i <= (int) $nombreAnnee; ++$i) {
            $arrayAnne[$anneeCourrante - $i] = $anneeCourrante - $i;
        }

        return $arrayAnne;
    }

    /**
     * Permet de charger l'image correspond d'une statut donnees.
     *
     * @param string $nameImage nom de l'image,Boolean $statutOk statut
     * @return void
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.7.0
     * @copyright Atexo 2014
     */
    public static function setCheckImageByEtat($nameImage, $thisView, $statutOk = true)
    {
        if ($statutOk) {
            $thisView->$nameImage->ImageUrl = Atexo_Controller_Front::t().'/images/picto-check-ok-small.gif';
            $thisView->$nameImage->Attributes->alt = Prado::localize('INFORMATIONS_DECLARES_SUR_HONNEUR');
        } else {
            $thisView->$nameImage->ImageUrl = Atexo_Controller_Front::t().'/images/picto-check-not-ok.gif';
            $thisView->$nameImage->Attributes->alt = Prado::localize('INFORMATIONS_MANQUANTES');
        }
    }

    /**
     * Permet d'avoir un date avec un format precis.
     *
     * @param dateTime $date la date,String $format le format
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public static function getDateTimeByFormat($date, $format = 'Y-m-d H:i:s')
    {
        $dateIso = new DateTime($date);

        return $dateIso->format($format);
    }

    /**
     * Permet d'avoir un date avec un format precis.
     *
     * @param dateTime $date la date,String $format le format
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function timestampToFormat($timeStamp, $format = 'Y-m-d H:i:s')
    {
        $date = new DateTime("@$timeStamp");

        return $date->format($format);
    }

    /**
     * Permet de valider un xml avec un schema xsd.
     *
     * @param string $xml l'xml a verifier,string $pathXsd le chemin de l'xsd,Logger $logger
     *
     * @return bool
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public static function validateXmlBySchema($xml, $pathXsd, $logger, &$erreurMessage = false)
    {
        $logger->info("Debut de validation de l'xml par le schema XSD ");
        $dom = new \DOMDocument();
        $dom->loadXML($xml);
        $dom->xinclude();
        libxml_use_internal_errors(true);
        try {
            if (!$dom->schemaValidate($pathXsd)) {
                $erreurs = libxml_get_errors(); //array error
                if (is_array($erreurs) && count($erreurs)) {
                    foreach ($erreurs as $error) {
                        $erreurMsg = 'Error : '.trim($error->message)."\n  Line: ".$error->line."\n  Column: ".$error->column;
                        $logger->error($erreurMsg);
                        if (false !== $erreurMessage) {
                            $erreurMessage = $erreurMsg;
                        }
                    }
                }

                return false;
            }
        } catch (\Exception $e) {
            $logger->error('La structure du fichier xml ne respecte pas le schéma approprié : '.$e->getMessage());

            return false;
        }
        $logger->info('Le flux XML est VALIDE');
        $logger->info("Fin de validation de l'xml par le schema XSD ");

        return true;
    }

    /**
     * Permet de calculer la taille d'un fichier.
     *
     * @param string $fichier : chemin du fichier
     * @param string $unite : unite de calcul de la taille proposee (O, Ko, Mo, Go). La taille sera retournee avec
     *     cette unite
     * @return: la taille du fichier
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function getTailleFichier($fichier, $unite = 'Mo', $returnUnite = true)
    {
        $tailleFichier = filesize($fichier);
        $tailleFichier = match (strtolower($unite)) {
									'go' => round($tailleFichier / 1_073_741_824 * 100) / 100,
									'mo' => round($tailleFichier / 1_048_576 * 100) / 100,
									'ko' => round($tailleFichier / 1024 * 100) / 100,
									'o' => $tailleFichier,
									default => $tailleFichier,
								};
        if ($returnUnite) {
            return $tailleFichier.' '.$unite;
        }

        return $tailleFichier;
    }

    /**
     * Permet de supprimer un fichier local.
     *
     * @param string $pathFile le chemin du fichier a supprimer
     *
     * @return bool ,true si le fichier a ete supprimer , sinn false
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function deleteLocalfile($pathFile)
    {
        system('rm '.escapeshellarg($pathFile), $ret);
        if ($ret) {
            return false;
        }

        return true;
    }

    /**
     * Permet de génere et retourner un jeton unique.
     *
     * @param int $idUser l'id d'utilisateur, String $typeUser le type de l'utilisateur
     *
     * @return string $jeton le jeton génerer
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function getJetonModificationPsw($idUser, $typeUser)
    {
        $existe = true;
        do {
            $jeton = sha1($idUser.$typeUser.date('Y-m-d H:i:s'));
            $informationPswQuery = new CommonTInformationModificationPasswordQuery(
                Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
            );
            $jetonExiste = $informationPswQuery->findOneByJeton($jeton);
            if (!$jetonExiste) {
                $existe = false;
            }
        } while ($existe);

        return $jeton;
    }

    /**
     * Permet de comparer deux dates au format ISO.
     *
     * @param string|date $date1
     * @param string|date $date2
     *
     * @return : timestamp representant l'écart entre les deux dates (positif si date1 >date2, negatif si date1 <
     *     date2, 0 si les 2 dates sont égales)
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function comparerDeuxDatesFormatIso($date1, $date2)
    {
        return strtotime($date1) - strtotime($date2);
    }

    /**
     * Permet de detruire les cookies.
     *
     * @param $exceptedCookies
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public static function detruireCookies($exceptedCookies = null)
    {
        if (null != $exceptedCookies && !is_array($exceptedCookies)) {
            throw new \InvalidArgumentException(
                sprintf('%s() expected the first parameter to be an array.', __METHOD__)
            );
        }
        if (is_array(array_keys($_COOKIE))) {
            foreach (array_keys($_COOKIE) as $cookieName) {
                if (empty($exceptedCookies) or !in_array($cookieName, $exceptedCookies)) {
                    unset($_COOKIE["$cookieName"]);
                    setcookie($cookieName, null, -1);
                }
            }
        }
    }

    /**
     * Permet de valider une chaine xml.
     *
     * @param string $xml
     *
     * @return bool
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function isValidXml($xml)
    {
        libxml_use_internal_errors(true);
        $doc = new \DOMDocument('1.0', 'utf-8');
        $doc->loadXML($xml);
        $errors = libxml_get_errors();

        return empty($errors);
    }

    /**
     * Permet de valider une date.
     *
     * @param DateTime $date   : la date
     * @param string   $format : le format de verification
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2016
     */
    public static function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        try {
            $d = \DateTime::createFromFormat($format, $date);

            return $d && $d->format($format) == $date;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error(
                "Erreur validation date : $date \n Erreur : ".$e->getMessage()."\nTrace : ".$e->getTraceAsString()
            );
        }
    }

    /**
     * Permet de remplacer les dates anglais par leur correspondance en francais dans une chaine de caracteres.
     *
     * @param string $chaine : chaine de caracteres
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2016
     */
    public static function remplacerDatesAnglaisParFrancaisDansChaine($chaine)
    {
        $arraydatesAnglais = [
            'years',
            'year',
            'months',
            'month',
            'weeks',
            'week',
            'days',
            'day',
            'hours',
            'hour',
            'minutes',
            'minute',
            'seconds',
            'second'
        ];
        $arraydatesFrancais = ['années', 'année', 'mois',   'mois',  'semaines', 'semaine', 'jours', 'jour', 'heures', 'heure', 'minutes', 'minute', 'secondes', 'seconde'];
        return str_replace($arraydatesAnglais, $arraydatesFrancais, $chaine);
    }

    /**
     * convertion Oct => Mo.
     *
     * @param $tailleEnOct
     *
     * @return float
     */
    public static function getTailleEnMo($tailleEnOct)
    {
        return ceil((int) $tailleEnOct / 1_048_576);
    }

    /**
     * retourne un nombre arrondit avec à 2 EX : 12454,5646 => 12454.56.
     *
     * @param $montant decimal au format xxxxx.xxxxxx
     *
     * @return decimal au format xxxxxx.xx
     */
    public static function getMontantArronditSansEspace($montant, $apresVirgule = 2)
    {
        $montant = str_replace(' ', '', $montant);
        if (!self::isEntierRelatif($montant)) {
            return $montant;
        }
        $montant = str_replace(',', '.', $montant);

        $montant = round($montant, $apresVirgule);
        $montant = number_format($montant, $apresVirgule, '.', '');

        return $montant;
    }

    public static function getSuffixNumeroContrat($i)
    {
        return (1 === strlen($i)) ? ('0'.$i) : $i;
    }

    /**
     * genere un xml contenant les infos d'un pli.
     *
     * @param string $motCle             une chaine de caractère
     * @param bool   $searchChaqueMot    recherche par mot ou pas
     * @param bool   $motEntrePorcentage ajout des pourcentage au debut et la fin du mot cle
     *
     * @return array tableau contenant l'ensemble des mots cles
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function getListeMotsCles($motCle, $searchChaqueMot = true, $motEntrePorcentage = true)
    {
        $return = [];
        if ($motCle) {
            if ($searchChaqueMot) {
                $motCle = strtr($motCle, ",;.&!?'\"()[]{}°+*/\\|:%", '                      ');
                $arrayMotsCle = explode(' ', $motCle);
            } else {
                $arrayMotsCle = ['0' => $motCle];
            }
            $arrayMotInterdit = ' and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ';

            foreach ($arrayMotsCle as $unMotCle) {
                $unMotCle = trim((new Atexo_Db())->quote($unMotCle));
                $unMotCle = Atexo_Util::replaceArrayCharactersByPourcentagage($unMotCle);
                if ($unMotCle && !strstr($arrayMotInterdit, (string) $unMotCle) && strlen($unMotCle) >= 1) {
                    if ($motEntrePorcentage) {
                        $return[] = '%'.$unMotCle.'%';
                    } else {
                        $return[] = $unMotCle;
                    }
                }
            }
        }

        return $return;
    }

    /**
     * Retourne le message en paramettre si il est different de null
     * si non il retourne '-'.
     *
     * @param string $message
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public static function formaterMessage($message)
    {
        return $message ?: '-';
    }

    /**
     * Permet de retourner le contentType selon l'extension d'unfichier.
     *
     * @param string $extensionFile
     *
     * @return string $contentType
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public static function getContentTypeByExtension($extensionFile)
    {
        $contentTypeFile = [
                'pdf' => 'application/pdf',
                'doc' => 'application/msword',
                'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'xls' => 'application/vnd.ms-excel',
                'txt' => 'text/plain',
                'rtf' => 'application/rtf',
                'odt' => 'application/vnd.oasis.opendocument.text',
                'xml' => 'application/xml',
                'p7s' => 'application/pkcs7-signature',
                'pptx' => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
                'ppt' => 'application/vnd.ms-powerpoint',
                'csv' => 'text/csv',
                'ppsx' => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
                'odp' => 'application/vnd.oasis.opendocument.presentation',
                'xsl' => 'application/vnd.ms-excel',
                'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
                'html' => 'text/html',
                'svg' => 'image/svg+xml',
                'xhtml' => 'application/xhtml+xml',
                'jpg' => 'image/jpeg',
                'jpeg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif',
                'tiff' => 'image/tiff',
                'zip' => 'application/zip',
                'bin' => 'application/octet-stream',
                'bmp' => 'image/bmp',
                'css' => 'text/css',
                'bz2' => 'application/x-bzip2',
                'bz' => 'application/x-bzip',
                'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                '7z' => 'application/x-7z-compressed',
        ];

        return $contentTypeFile[strtolower($extensionFile)] ?: 'application/xml';
    }

    public static function castToBoolean($v)
    {
        return filter_var($v, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    }

    /**
     * retourne libelle de type d'admissibilite.
     *
     * @param int $admissibilte
     *
     * @return string
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public static function getLibelleAdmissibilite($admissibilte)
    {
        $libelleAdmissibilite = match ($admissibilte) {
									Atexo_Config::getParameter('LOT_ADMISSIBLE') => Atexo_Config::getParameter('LOT_ADMISSIBLE_LIBELLE'),
									Atexo_Config::getParameter('LOT_NON_ADMISSIBLE') => Atexo_Config::getParameter('NON_ADMISSIBLE'),
									default => Atexo_Config::getParameter('LOT_A_TRAITER_LIBELLE'),
								};
								return $libelleAdmissibilite;
    }

    /**
     * retourne libelle de type de depot.
     *
     * @param int $typeDepot
     *
     * @return string
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public static function getLibelleTypeDepot($typeDepot)
    {
        $libelleTypeDepot = '';
        $libelleTypeDepot = match ($typeDepot) {
									Atexo_Config::getParameter('DEPOT_ELECTRONIQUE') => Atexo_Config::getParameter('DEPOT_ELECTRONIQUE_LIBELLE'),
									Atexo_Config::getParameter('DEPOT_PAPIER') => Atexo_Config::getParameter('DEPOT_PAPIER_LIBELLE'),
									default => $libelleTypeDepot,
								};

        return $libelleTypeDepot;
    }

    /**
     * retourne libelle de type de decision.
     *
     * @param int $idTypeDecision
     *
     * @return string
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public static function getLibelleDecision(string $idTypeDecision)
    {
        $libelleDecision = '';
        $libelleDecision = match ($idTypeDecision) {
									Atexo_Config::getParameter('DECISION_A_RENSEIGNER') => 'A_RENSEIGNER',
									Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE') => 'ATTRIBUTION_MARCHE',
									Atexo_Config::getParameter('DECISION_DECLARATION_SANS_SUITE') => 'DECLARATION_SANS_SUITE',
									Atexo_Config::getParameter('DECISION_DECLARATION_INFRUCTUEUX') => 'DECLARATION_INFRUCTUEUX',
									Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE') => 'SELECTION_ENTREPRISES',
									Atexo_Config::getParameter('DECISION_ATTRIBUTION_ACCORD_CADRE') => 'ATTRIBUTION_DE_ACCORD_CADRE',
									Atexo_Config::getParameter('DECISION_ADMISSION_SAD') => 'ADMISSION_SAD',
									Atexo_Config::getParameter('DECISION_AUTRE') => 'AUTRE',
									default => $libelleDecision,
								};

        return $libelleDecision;
    }

    /**
     * Permet de recuperer le navigateur utilise par le visiteur.
     *
     * @return string
     */
    public static function getBrowserVisiteur()
    {
        $bs = null;
								if (preg_match('/Firefox/i', $_SERVER['HTTP_USER_AGENT'])) {
            $br = 'Firefox';
        } elseif (preg_match('/Mac/i', $_SERVER['HTTP_USER_AGENT'])) {
            $br = 'Mac';
        } elseif (preg_match('/Chrome/i', $_SERVER['HTTP_USER_AGENT'])) {
            $br = 'Chrome';
        } elseif (preg_match('/Opera/i', $_SERVER['HTTP_USER_AGENT'])) {
            $br = 'Opera';
        } elseif (preg_match('/MSIE/i', $_SERVER['HTTP_USER_AGENT'])) {
            $br = 'IE';
        } else {
            $bs = 'Unknown';
        }

        return $bs;
    }

    /**
     * Permet de recuperer les informations sur le navigateur utilise.
     *
     * @return array
     */
    public static function getInfosBrowserVisiteur()
    {
        $ub = null;
								$u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version = '';

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }

        // Next get the name of the useragent yes seperately and for good reason
        if ((preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) || (preg_match('/Trident.*rv[ :]*(\d+\.\d+)/', $u_agent, $matches))) {
            $bname = 'Internet Explorer';
            $ub = 'MSIE';
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = 'Firefox';
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = 'Chrome';
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = 'Safari';
        } elseif (preg_match('/Opera/i', $u_agent)) {
            $bname = 'Opera';
            $ub = 'Opera';
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = 'Netscape';
        }

        // finally get the correct version number
        $known = ['Version', $ub, 'other'];
        $pattern = '#(?<browser>'.join('|', $known).
            ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }

        // see how many we have
        $i = is_countable($matches['browser']) ? count($matches['browser']) : 0;
        if (1 != $i) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, 'Version') < strripos($u_agent, (string) $ub)) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }

        //Cas particulier : IE11 Windows 8 (voir http://www.javascriptkit.com/javatutors/navigator.shtml)
        if ((preg_match('/Trident.*rv[ :]*(\d+\.\d+)/', $u_agent, $matches))) {
            $version = $matches[1];
        } elseif (stripos($u_agent, 'Trident/7.0;')) {
            // IE11 in compatibility mode
            $version = '11.0';
        } elseif (stripos($u_agent, 'Trident/6.0;')) {
            // IE10 in compatibility mode
            $version = '10.0';
        } elseif (stripos($u_agent, 'Trident/5.0;')) {
            // IE9 in compatibility mode
            $version = '9.0';
        } elseif (stripos($u_agent, 'Trident/4.0;')) {
            // IE8 in compatibility mode
            $version = '8.0';
        }
        // check if we have a number
        if ( null == $version || '' ==
            $version ) {
            $version = '?';
        }

        return [
            'userAgent' => $u_agent,
            'name' => $bname,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern,
        ];
    }

    /**
     * Permet d'encoder un tableau en utf8 de facon recursive.
     *
     * @param $array
     *
     * @return mixed
     */
    public static function utf8_converter($array)
    {
        array_walk_recursive(
            $array,
            function (&$item, $key) {
            if (!mb_detect_encoding($item, 'utf-8', true)) {
                $item = utf8_encode($item);
            }
            }
        );

        return $array;
    }

    public static function supprimerCodeHtml($str)
    {
        while (preg_match('/&[a-zA-Z]*;/', $str)) {
            $str = html_entity_decode($str, ENT_NOQUOTES | ENT_QUOTES, Atexo_Config::getParameter('HTTP_ENCODING'));
        }

        return $str;
    }

    public static function formaterChaineToMessage($chaine, $seperateur = '@@')
    {
        $arrayChaine = explode($seperateur, $chaine);
        $message = '';
        if (is_array($arrayChaine)) {
            foreach ($arrayChaine as $oneChaine) {
                if ($oneChaine) {
                    $message .= Prado::Localize($oneChaine);
                }
            }
        }

        return $message;
    }



	/**
	 * Permet de generer un fichier pdf
	 *
	 * @param $fileOdt
	 * @param $fileName
	 * @throws \PdfGeneratorClientException
	 * @return String le chemin du fichier sur le disque
	 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 2016-develop
	 * @copyright Atexo 2017
	 */
	public function generatePdf($fileOdt ,$fileName){

		try{
			if ($fileOdt) {
				$filePdf = Atexo_Config :: getParameter('COMMON_TMP') . $fileName . session_id() . time();
				$pdfContent = (new PdfGeneratorClient())->genererPdf($fileOdt);
				Atexo_Util::write_file($filePdf, $pdfContent);
				if(is_file($filePdf) ){
					return $filePdf;
				}
			}
		}catch (\Exception $e){
			throw $e;
		}

	}

    /**
     * @fn getUrlIsConnected
     *
     * @var url: string
     */
    public static function getUrlIsConnected($url)
    {
        if (Atexo_CurrentUser::isConnected()) {
            return $url;
        } else {
            return 'index.php?page=Entreprise.EntrepriseHome&goto='.urlencode('?page=Entreprise.EntrepriseAccueilAuthentifie'
                );
        }
    }

    public static function truncateHTMLContents($html, $maxLength = 100)

    {
        mb_internal_encoding('UTF-8');
        $printedLength = 0;
        $position = 0;
        $tags = [];
        $newContent = '';

        $html = $content = preg_replace("/<img[^>]+\>/i", '', $html);

        while ($printedLength < $maxLength && preg_match('{</?([a-z]+)[^>]*>|&#?[a-zA-Z0-9]+;}', $html, $match, PREG_OFFSET_CAPTURE, $position)) {
            [$tag, $tagPosition] = $match[0];
            // Print text leading up to the tag.
            $str = mb_strcut($html, $position, $tagPosition - $position);
            if ($printedLength + mb_strlen($str) > $maxLength) {
                $newstr = mb_strcut($str, 0, $maxLength - $printedLength);
                $newstr = preg_replace('~\s+\S+$~', '', $newstr);
                $newContent .= $newstr;
                $printedLength = $maxLength;
                break;
            }
            $newContent .= $str;
            $printedLength += mb_strlen($str);
            if ('&' == $tag[0]) {
                // Handle the entity.
                $newContent .= $tag;
                ++$printedLength;
            } else {
                // Handle the tag.
                $tagName = $match[1][0];
                if ('/' == $tag[1]) {
                    // This is a closing tag.
                    $openingTag = array_pop($tags);
                    assert($openingTag == $tagName); // check that tags are properly nested.
                    $newContent .= $tag;
                } elseif ('/' == $tag[mb_strlen($tag) - 2]) {
                    // Self-closing tag.
                    $newContent .= $tag;
                } else {
                    // Opening tag.
                    $newContent .= $tag;
                    $tags[] = $tagName;
                }
            }

            // Continue after the tag.
            $position = $tagPosition + mb_strlen($tag);
        }

        // Print any remaining text.
        if ($printedLength < $maxLength && $position < mb_strlen($html)) {
            $newstr = mb_strcut($html, $position, $maxLength - $printedLength);
            $newstr = preg_replace('~\s+\S+$~', '', $newstr);
            $newContent .= $newstr;
        }

        // Close any open tags.
        while (!empty($tags)) {
            $newContent .= sprintf('</%s>', array_pop($tags));
        }

        return $newContent;
    }

    /**
     * @param $filename
     * @return string
     */
    public static function getWeightByFilename($filename)
    {
        $documentServeurDocsQuery = new CommonDocumentServeurDocsQuery();
        $documentServeurDocs = $documentServeurDocsQuery->findOneByNom($filename);

        if (null === $documentServeurDocs) {
            return '';
        }

        return ' (' . $documentServeurDocs->getExtension() . ' ' . str_replace(
                '.',
                ',',
                self::GetSizeName(
                    $documentServeurDocs->getPoids()
                )
            ) . ')';
    }

    /**
     * @return bool
     */
    public static function hasSiretCheckeSiretCentralise()
    {
        $validSiret = true;
        $organisme = Atexo_CurrentUser::getOrganismAcronym();
        if (is_null($organisme)) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }

        $serviceId = Atexo_CurrentUser::getCurrentServiceId();
        $organismeObjet = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);

        if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme) || empty($serviceId)) {
            if (empty($organismeObjet->getSiren()) || empty($organismeObjet->getComplement())) {
                $validSiret = false;
            }
        } else {
            $service = (new Atexo_EntityPurchase())->getEntityById($serviceId, $organisme);
            if (empty($service->getSiren()) || empty($service->getComplement())) {
                $validSiret = false;
            }
        }

        return $validSiret;
    }

    public static function formaterNomFileForZip($nomFichier)
    {
        return self::clean(
            self::OterAccents(
                self::replaceCharactersMsWordWithRegularCharacters(
                    self::atexoHtmlEntitiesDecode((new Atexo_Config())->toPfEncoding($nomFichier))
                )
            )
        );
    }

    /**
     * @param $url
     *
     * @return bool
     */
    public static function urlIsAllow($url)
    {
        $parseUrl = parse_url($url);
        if (self::isRelative($parseUrl)) {
            return true;
        }
        if (self::isUrlMPE($parseUrl)) {
            return true;
        }

        return false;
    }

    /**
     * @param $url
     *
     * @return bool
     */
    public static function isRelative($parseUrl)
    {
        if ($parseUrl['host']) {
            return false;
        }

        return true;
    }

    /**
     * @param $url
     *
     * @return bool
     */
    public static function isUrlMPE($parseUrl)
    {
        $parseUrlMpe = parse_url(Atexo_Config::getParameter('PF_URL_REFERENCE'));
        if ($parseUrl['host'] == $parseUrlMpe['host']) {
            return true;
        }

        return false;
    }

    /**
     * @var \AppKernel
     */
    private static $sfKernel;

    final public static function setSfKernel($kernel)
    {
        self::$sfKernel = $kernel;
    }

    final public static function getSfContainer()
    {
        return self::$sfKernel
            ->getContainer();
    }

    final public static function getSfService($className)
    {
        return self::getSfContainer()
            ->get($className);
    }

    /**
     * @param $etablissement
     * @param $entreprise
     *
     * @return array
     */
    public static function getSiretAlertDiv($etablissement, $entreprise)
    {
        $color = null;
        $siretAlertMsg = null;
        $siretAlertFA = null;

        if (!empty($entreprise) && !empty($etablissement)) {
            if (empty(trim($entreprise->getSiren()))) {
                $siretAlertMsg = Prado::localize('SIRET_NON_VERIFIE');
                $siretAlertFA = 'question';
                $color = 'orange';
            //regle 25 : SIRET_ETABLISSEMENT_INCORRECT
            } elseif (!Atexo_util::isSiretValide(
                trim($entreprise->getSiren() . $etablissement->getCodeEtablissement())
            )) {
                $siretAlertMsg = Prado::localize('SIRET_INCORRECT');
                $siretAlertFA = 'times';
                $color = 'red';
            // regle 26
            } elseif ($entreprise->getSaisieManuelle()) {
                $siretAlertMsg = Prado::localize('SIRET_NON_VERIFIE');
                $siretAlertFA = 'question';
                $color = 'orange';
            // regle 27
            } elseif ($etablissement->getSaisieManuelle()) {
                $siretAlertMsg = Prado::localize('SIRET_ETABLISSEMENT_NON_VERIFIE');
                $siretAlertFA = 'question';
                $color = 'orange';
            // regle 28 : SIRET_ETABLISSEMENT_FERME
            } elseif ('F' === $etablissement->getEtatAdministratif()) {
                $siretAlertMsg = Prado::localize('SIRET_ETABLISSEMENT_FERME');
                $siretAlertFA = 'times';
                $color = 'red';
            // regle 29 : ETAT_ADMINISTRATIF_ETABLISSEMENT_DIFFERENT_ACTIF
            } elseif ('A' !== $etablissement->getEtatAdministratif()) {
                $siretAlertMsg = Prado::localize('SIRET_NON_VERIFIE');
                $siretAlertFA = 'question';
                $color = 'orange';
            // regle 29 : ETAT_ADMINISTRATIF_ETABLISSEMENT_ACTIF
            } elseif ('A' === $etablissement->getEtatAdministratif()) {
                $siretAlertMsg = Prado::localize('SIRET_ETABLISSEMENT_ACTIF');
                $siretAlertFA = 'check';
                $color = 'green';
            }
        }

        if (!empty($color)) {
            return [
                'color' => $color. ' disable-siret',
                'siretAlertMsg' => $siretAlertMsg,
                'siretAlertFA' => $siretAlertFA,
            ];
        }

        return [];
    }

    /**
     * @param null $design
     *
     * @return string|string[]|null
     *
     * @throws Atexo_Config_Exception
     */
    public static function getImageBandeau($design = null)
    {
        $sourceImageBandeau = null;
        if (!empty(Atexo_Config::getParameter('SOURCE_IMAGE_BANDEAU'))) {
            $option = [
                'slash' => true,
                'multi_domaine' => Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
            ];
            $pfUrl = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_MESSAGERIE'), $option);
            $sourceImageBandeau = $pfUrl.trim(Atexo_Config::getParameter('SOURCE_IMAGE_BANDEAU'), '/');
            $codeBandeauImage = html_entity_decode(Atexo_Config::getParameter('CODE_BANDEAU_IMAGE'));
            $sourceImageBandeau = str_replace('SOURCE_IMAGE_BANDEAU', $sourceImageBandeau, $codeBandeauImage);
            if ($design) {
                $sourceImageBandeau = str_replace(
                    '/themes/images/',
                    '/themes/' . $design . '/images/',
                    $sourceImageBandeau
                );
            }
        }

        return $sourceImageBandeau;
    }

    public static function getVersion(): string
    {
        $version = Atexo_Config::getParameter('APP_BASE_ROOT_DIR') . '.version';
        $content = file($version);

        return str_replace('version=', '', $content['0']);
    }

    public static function getListeAttributaires($attributaires)
    {
        $list = '-';

        if (1 == (is_countable($attributaires) ? count($attributaires) : 0)) {
            $list = "<abbr title='".Prado::localize('TEXT_SIRET').' : '.$attributaires[0]->id."'>";
            $list .= $attributaires[0]->denominationSociale.'</abbr>';
        } elseif ((is_countable($attributaires) ? count($attributaires) : 0) > 1) {
            $list = '<ul>';
            foreach ($attributaires as $attributaire) {
                $list .= "<li><abbr title='".Prado::localize('TEXT_SIRET').' : '.$attributaire->id."'>";
                $list .= "$attributaire->denominationSociale</abbr></li>";
            }
            $list .= '</ul>';
        }

        return $list;
    }
    public static function getUrlLogoOrganisme(string $org, bool $grand = false): string
    {
        $option = ($grand) ? '&grand=true' : '';
        return Atexo_Config::getParameter('PF_URL') . "index.php?page=".Atexo_Util::getTypeUserCalledForPradoPages().".LogoOrganisme&org=" . $org . $option;
    }

    public static function isLogoOrganismeVisible(string $org, bool $grand = false): string
    {
        $lengthImage = ($grand) ? 'grand' : 'petit';
        $filePath = Atexo_Config::getParameter('BASE_ROOT_DIR') . '/' . $org .
            Atexo_Config::getParameter('PATH_ORGANISME_IMAGE') . '/logo-organisme-' . $lengthImage . '.jpg';
        return (is_file($filePath)) ? true : false;
    }

    public static function getTypeUserCalledForPradoPages(): string
    {
        return Atexo_CurrentUser::readFromSession('cachedAgent') instanceof CommonAgent ? 'Agent' : 'Entreprise';
    }
    /**
     * @param $finalDce
     * @throws ExceptionDceLimit
     */
    public function checkDceSize($finalDce)
    {
        if (is_file($finalDce)) {
            $size = filesize($finalDce);
            $dceMaxSize = (int)Atexo_Config::getParameter("MAX_UPLOAD_FILE_DCE_SIZE");
            if ($size > $dceMaxSize) {
                unlink($finalDce);
                throw new ExceptionDceLimit();
            }
        }
    }
}
