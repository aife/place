<?php

namespace Application\Service\Atexo\DossierVolumineux;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDossierVolumineux;
use Application\Propel\Mpe\CommonDossierVolumineuxQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Util;
use DateTime;
use Prado\Prado;

class Atexo_DossierVolumineux_Responses
{
    public static function retrieveDossierVolumineux($id)
    {
        $dossierVolumineuxQuery = new CommonDossierVolumineuxQuery();

        return $dossierVolumineuxQuery->getDossierVolumineux($id);
    }

    /**
     * Retourne tous les DV de l'user courant.
     *
     * @return CommonDossierVolumineux
     */
    public static function retrieveActiveDossiersVolumineuxFromCurrentUser($id, $isAgent)
    {
        $dossierVolumineuxQuery = new CommonDossierVolumineuxQuery();

        return $dossierVolumineuxQuery->getActiveDossiersVolumineuxFromCurrentUser($id, $isAgent);
    }

    public static function getListeDossiersVolumineuxActifs($id, $isAgent)
    {
        $dataDossiersVolumineux = [];
        $resultCriteria = self::retrieveActiveDossiersVolumineuxFromCurrentUser($id, $isAgent);
        $dataDossiersVolumineux[0] = Prado::localize('TEXT_SELECTIONNER').'...';
        if (is_array($resultCriteria)) {
            foreach ($resultCriteria as $dossierVolumineux) {
                $dt = new DateTime($dossierVolumineux->getDateCreation());
                $taille = Atexo_Util::GetSizeName($dossierVolumineux->getTaille());
                $dataDossiersVolumineux[$dossierVolumineux->getId()] = $dossierVolumineux->getNom().' ('
                    .Prado::localize('TEXT_IDENTIFIANT').' : '.$dossierVolumineux->getUuidReference()
                    .' - Taille : '.$taille.' - '
                    .Prado::localize('AJOUTE_LE').' '.$dt->format('d/m/Y H:i').')';
            }
        }

        return $dataDossiersVolumineux;
    }

    /**
     * Permet de sauvegarder l'historique des dossiers volumineux de la consultation.
     *
     * @param int                $idDossierVolumineuxOld
     * @param CommonConsultation $consultation
     * @param PDOConnection      $connexion
     * @param string             $org                    : l'acronyme de l'organisme
     * @param bool               $modification
     *
     * @return void
     */
    public static function saveHistoriqueDossierVolumineux($idDossierVolumineuxOld, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if ($consultation instanceof CommonConsultation) {
            $idDossierVolumineuxNew = $consultation->getIdDossierVolumineux();
            if ($idDossierVolumineuxOld != $idDossierVolumineuxNew || !$modification) {
                $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_DOSSIER_VOLUMINEUX');
                $valeur = '1';
                $detail1 = '';
                if ($idDossierVolumineuxNew) {
                    $dossierV = self::retrieveDossierVolumineux($idDossierVolumineuxNew);
                    if ($dossierV instanceof CommonDossierVolumineux) {
                        $detail1 = $dossierV->getNom().' ('.$dossierV->getUuidReference().')';
                    }
                }
                $detail2 = '';
                $lot = '0';
                if ($modification) {
                    $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
                } else {
                    $statut = Atexo_Config::getParameter('CREATION_FILE');
                }
                //Sauvegarde
                $arrayParameters[0] = $consultation->getId();
                $arrayParameters[1] = $nomElement;
                $arrayParameters[2] = $valeur;
                $arrayParameters[3] = $detail1;
                $arrayParameters[4] = $detail2;
                $arrayParameters[5] = $lot;
                $arrayParameters[6] = $org;
                $arrayParameters[7] = $statut;
                (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
            }
        }
    }
}
