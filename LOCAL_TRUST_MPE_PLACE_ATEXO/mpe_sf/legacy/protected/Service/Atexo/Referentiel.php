<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonValeurReferentiel;
use Application\Propel\Mpe\CommonValeurReferentielOrgPeer;
use Application\Propel\Mpe\CommonValeurReferentielOrgQuery;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Referentiel
{
    public function retrieveArrayValEurReferentielByReferentiel($referentiel, $organisme)
    {
        $valeurReferentielOrgQuery = new CommonValeurReferentielOrgQuery();
        $commonValeurReferentielOrgList = $valeurReferentielOrgQuery->findValeurReferentielOrg();

        $valeursReferentiel = [];
        foreach ($commonValeurReferentielOrgList as $item) {
            if (($item->getIdReferentiel() == $referentiel) && ($item->getOrganisme() == $organisme)) {
                $valeursReferentiel[] = $item;
            }
        }

        if ($valeursReferentiel) {
            $arrayValRef = [];
            foreach ($valeursReferentiel as $valRef) {
                $arrayValRef[$valRef->getId()] = $valRef->getLibelleValeurReferentiel();
            }

            return $arrayValRef;
        } else {
            return [];
        }
    }

    public function retrieveValeurReferentielByReferentiel($referentiel, $groupBy = 'id')
    {
        $valeurReferentielOrgQuery = new CommonValeurReferentielOrgQuery();
        $commonValeurReferentielOrgList = $valeurReferentielOrgQuery->findCachedValeurReferentiel();

        $valeursReferentielsObject = [];
        foreach ($commonValeurReferentielOrgList as $item) {
            if ($item->getIdReferentiel() == $referentiel) {
                $valeursReferentielsObject[] = $item;
            }
        }

        $key = 'getId';
        $groupBy = 'get'.ucfirst($groupBy);
        if (method_exists(new CommonValeurReferentiel(), $groupBy)) {
            $key = $groupBy;
        }

        $valeursReferentiels = [];

        foreach ($valeursReferentielsObject as $valRef) {
            $tmp = [];
            $tmp['id'] = $valRef->getId();
            $tmp['id_referentiel'] = $valRef->getIdReferentiel();
            $tmp['libelle_valeur_referentiel'] = $valRef->getLibelleValeurReferentielTraduit(); //getLibelleValeurReferentiel();
            $tmp['libelle_2'] = $valRef->getLibelle2();

            $valeursReferentiels[$valRef->$key()] = $tmp;
        }

        return $valeursReferentiels;
    }

    /*
     permet d'avoir la valuer referentiel par id et libelle_2
     */
    public function retrieveValeurReferentielByLibelle2($referentiel, $libelle2)
    {
        return (new Atexo_CommonReferentiel())->retrieveValeurReferentielByLibelle2($referentiel, $libelle2);
    }

    /*
     permet d'avoir la valuer referentiel par id
     */
    public function retrieveValeurReferentielById($referentiel, $organisme, $id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonValeurReferentielOrgPeer::ID_REFERENTIEL, $referentiel);
        $c->add(CommonValeurReferentielOrgPeer::ID, $id);
        $c->add(CommonValeurReferentielOrgPeer::ORGANISME, $organisme);
        $valRef = CommonValeurReferentielOrgPeer::doSelectOne($c, $connexion);
        if ($valRef) {
            return $valRef;
        } else {
            return false;
        }
    }

    /*
    permet d'avoir la valuer referentiel par id
    */
    public function retrieveValeurReferentielByValeurSub($referentiel, $organisme, $valeurSub)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonValeurReferentielOrgPeer::ID_REFERENTIEL, $referentiel);
        $c->add(CommonValeurReferentielOrgPeer::VALEUR_SUB, $valeurSub);
        $c->add(CommonValeurReferentielOrgPeer::ORGANISME, $organisme);
        $valRef = CommonValeurReferentielOrgPeer::doSelectOne($c, $connexion);
        if ($valRef) {
            return $valRef;
        } else {
            return false;
        }
    }
}
