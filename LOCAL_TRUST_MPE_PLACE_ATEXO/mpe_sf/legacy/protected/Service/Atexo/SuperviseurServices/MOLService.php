<?php

namespace Application\Service\Atexo\SuperviseurServices;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\TokenManager\Atexo_TokenManager_CurlWrapper;

/**
 * @author Mohamed Blal
 *
 * Class MOLService
 */
class Atexo_SuperviseurServices_MOLService extends Atexo_SuperviseurServices_AbstractService
{
    protected \Application\Service\Atexo\TokenManager\Atexo_TokenManager_CurlWrapper $curl;

    /**
     * Atexo_SuperviseurServices_MOLService constructor.
     */
    public function __construct()
    {
        $this->curl = new Atexo_TokenManager_CurlWrapper();
        $this->host = Atexo_Config::getParameter('URL_CONCENTRATEUR_WS');
    }

    public function verify()
    {
        $timeout = Atexo_Config::getParameter('mol_timeout');
        $result = $this->curl->get($this->host, $timeout);
        $this->name = Atexo_SuperviseurServices_Constant::PLATEFORME_MARCHES_ONLINE;
        if (!$result) {
            $this->curl->getLastError();
            $this->status = Atexo_SuperviseurServices_Constant::STATUS_KO;
            $this->description = $this->curl->getLastError();
        }
    }
}
