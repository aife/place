<?php

namespace Application\Service\Atexo\SuperviseurServices;

use Application\Service\Atexo\Atexo_Config;

/**
 * @author Mohamed Blal
 * Class SMTPService
 */
class Atexo_SuperviseurServices_SMTPService extends Atexo_SuperviseurServices_AbstractService
{
    public function __construct()
    {
        $fields = [];

        $host = Atexo_Config::getParameter('MAIL_HOST');

        $fields = explode(':', $host);

        if (2 === count($fields)) {
            $this->host = $fields[0];
            $this->port = (int) $fields[1];
        }
    }

    public function verify()
    {
        $connection = @fsockopen($this->host, $this->port, $errno, $errstr, Atexo_Config::getParameter('smtp_timeout'));
        $this->name = Atexo_SuperviseurServices_Constant::MAIL_SERVICE_NAME;
        if (!$connection) {
            $this->status = Atexo_SuperviseurServices_Constant::STATUS_KO;
            $this->description = $errstr;
        }

        if (is_resource($connection)) {
            fclose($connection);
        }
    }
}
