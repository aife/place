<?php

namespace Application\Service\Atexo\SuperviseurServices;

/**
 * @author Mohamed Blal
 *
 * Class ServiceDirectory
 */
class Atexo_SuperviseurServices_NetworkServiceDirectory
{
    public function getServices()
    {
        return [
            new Atexo_SuperviseurServices_SMTPService(),
            new Atexo_SuperviseurServices_DatabaseService(),
            new Atexo_SuperviseurServices_MOLService(),
            new Atexo_SuperviseurServices_CHORUSService(),
            new Atexo_SuperviseurServices_CryptoService(),
            //'atexo_superviseur_services.file_service'
        ];
    }
}
