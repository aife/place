<?php

namespace Application\Service\Atexo\SuperviseurServices;

/**
 * @author Mohamed Blal
 * Class Facade
 */
class Atexo_SuperviseurServices_Facade
{
    /**
     * @return array
     */
    public function getResult()
    {
        $services = new Atexo_SuperviseurServices_NetworkServiceDirectory();
        $s = $services->getServices();
        foreach ($s as $service) {
            $service->ping();
        }

        $fileService = new Atexo_SuperviseurServices_FileService();
        $accessRules = $fileService->verify();

        $list = array_merge(Atexo_SuperviseurServices_AbstractService::getState(), $accessRules);

        $output = $this->getStackedStates($list);

        return $output;
    }

    /**
     * @param array $list
     *
     * @return array
     */
    protected function getStackedStates($list = [])
    {
        $output = [];
        $report = [];
        foreach ($list as $key => $item) {
            $report[$key][Atexo_SuperviseurServices_Constant::SERVICE_NAME_TITLE] = $item->name;
            $report[$key][Atexo_SuperviseurServices_Constant::SERVICE_STATUS_TITLE] = $item->status;
            $report[$key][Atexo_SuperviseurServices_Constant::SERVICE_DESCRIPTION] = $item->description;
        }
        $output['data'] = $report;

        return $output;
    }
}
