<?php

namespace Application\Service\Atexo\SuperviseurServices;

/**
 * @author Mohamed BLAL
 *
 * Class AbstractService
 */
abstract class Atexo_SuperviseurServices_AbstractService
{
    protected static array $state = [];

    public string $host;

    public string $port;

    public string $name;

    public string $status = Atexo_SuperviseurServices_Constant::STATUS_OK;

    public string $description;

    /**
     * Test the server connection.
     *
     * @return mixed
     */
    abstract public function verify();

    public function ping()
    {
        $this->verify();
        static::$state[] = $this;
    }

    /**
     * @return array
     */
    public static function getState()
    {
        return static::$state;
    }
}
