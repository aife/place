<?php

namespace Application\Service\Atexo\SuperviseurServices;

use Application\Service\Atexo\Atexo_Config;

/**
 * @author Mohamed Blal
 * Class CryptoService
 */
class Atexo_SuperviseurServices_CryptoService extends Atexo_SuperviseurServices_AbstractService
{
    protected \Application\Service\Atexo\SuperviseurServices\AtexoCrypto\Client\Client $client;
    protected string $org;
    protected string $enLigne;

    /**
     * Atexo_SuperviseurServices_CryptoService constructor.
     */
    public function __construct()
    {
        $this->host = Atexo_Config::getParameter('URL_CRYPTO');
        $this->client = new AtexoCrypto\Client\Client($this->host, '', '');
        $this->org = Atexo_Config::getParameter('CRYPTO_ORGANISME');
        $this->enLigne = Atexo_Config::getParameter('EN_LIGNE');
    }

    public function verify()
    {
        $params = [];

        $params['MpeURLPrivee'] = Atexo_Config::getParameter('MPE_WS_URL');
        $params['ServeurCryptoURLPublic'] = Atexo_Config::getParameter('URL_CRYPTO');
        $params['MpeFilePath'] = Atexo_Config::getParameter('BASE_ROOT_DIR').$this->org.Atexo_Config::getParameter('FILES_DIR');
        $params['MpeLogin'] = Atexo_Config::getParameter('MPE_WS_LOGIN_SRV_CRYPTO');
        $params['MpePassword'] = Atexo_Config::getParameter('MPE_WS_PASSWORD_SRV_CRYPTO');
        $params['Format'] = Atexo_Config::getParameter('FORMAT_RETOUR_CRYPTO');
        $params['EnLigne'] = $this->enLigne;

        $defaultTimeout = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', Atexo_Config::getParameter('crypto_timeout'));

        $result = $this->client->chiffrementService()->verifierDisponibilite($params);

        ini_set('default_socket_timeout', $defaultTimeout);

        $this->name = Atexo_SuperviseurServices_Constant::PLATEFORME_CRYPTO;

        $returnedObject = json_decode($result, false, 512, JSON_THROW_ON_ERROR);
        if (null === $returnedObject) {
            $this->status = Atexo_SuperviseurServices_Constant::STATUS_KO;
            $this->description = Atexo_SuperviseurServices_Constant::MSG_ERROR_CRYPTO;

            return;
        }
        if (201 !== $returnedObject->code) {
            $this->status = Atexo_SuperviseurServices_Constant::STATUS_KO;
        }
        $this->description = $returnedObject->message;
    }
}
