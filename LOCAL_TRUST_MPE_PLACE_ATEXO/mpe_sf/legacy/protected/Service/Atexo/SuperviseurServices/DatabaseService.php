<?php

namespace Application\Service\Atexo\SuperviseurServices;

use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Config;

/**
 * @author Mohamed BLAL
 *
 * Class DatabaseService
 */
class Atexo_SuperviseurServices_DatabaseService extends Atexo_SuperviseurServices_AbstractService
{
    public function verify()
    {
        $this->name = Atexo_SuperviseurServices_Constant::DB_SERVICE_NAME;
        $defaultTimeout = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', Atexo_Config::getParameter('database_timeout'));
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        } catch (\Exception $e) {
            $this->status = Atexo_SuperviseurServices_Constant::STATUS_KO;
            $this->description = $e->getMessage();
        }
        ini_set('default_socket_timeout', $defaultTimeout);
    }
}
