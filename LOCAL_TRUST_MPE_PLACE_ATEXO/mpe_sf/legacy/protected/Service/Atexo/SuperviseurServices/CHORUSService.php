<?php

namespace Application\Service\Atexo\SuperviseurServices;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\TokenManager\Atexo_TokenManager_CurlWrapper;

/**
 * @author Mohamed Blal
 *
 * Class CHORUSService
 */
class Atexo_SuperviseurServices_CHORUSService extends Atexo_SuperviseurServices_AbstractService
{
    protected \Application\Service\Atexo\TokenManager\Atexo_TokenManager_CurlWrapper $curl;

    public function __construct()
    {
        $this->curl = new Atexo_TokenManager_CurlWrapper();
        $this->host = Atexo_Config::getParameter('URL_ACCES_CHORUS');
    }

    public function verify()
    {
        $timeout = Atexo_Config::getParameter('chorus_timeout');

        $result = $this->curl->get($this->host, $timeout);

        $this->name = Atexo_SuperviseurServices_Constant::PLATEFORME_CHORUS;
        if (!$result) {
            $this->curl->getLastError();
            $this->status = Atexo_SuperviseurServices_Constant::STATUS_KO;
            $this->description = $this->curl->getLastError();
        }
    }
}
