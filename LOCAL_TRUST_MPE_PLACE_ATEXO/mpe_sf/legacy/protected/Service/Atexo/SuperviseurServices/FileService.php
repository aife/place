<?php

namespace Application\Service\Atexo\SuperviseurServices;

use Application\Service\Atexo\Atexo_Config;
use stdClass;

/**
 * @author Mohamed Blal
 *
 * Class FileService
 */
class Atexo_SuperviseurServices_FileService
{
    protected array $files;

    public function __construct()
    {
        $this->files[] = Atexo_Config::getParameter('LOG_DIR');
        $this->files[] = Atexo_Config::getParameter('DOCUMENT_ROOT_DIR');
    }

    public function verify()
    {
        $rules = [];
        foreach ($this->files as $file) {
            $f1 = new stdClass();
            $isReadable = is_readable($file) ? Atexo_SuperviseurServices_Constant::STATUS_OK : Atexo_SuperviseurServices_Constant::STATUS_KO;
            $f1->status = $isReadable;
            $f1->description = '';
            $f1->name = Atexo_SuperviseurServices_Constant::FILE_READ_RIGHTS.': '.$file;

            $rules[] = $f1;

            // Ecriture

            $isWritable = is_writable($file) ? Atexo_SuperviseurServices_Constant::STATUS_OK : Atexo_SuperviseurServices_Constant::STATUS_KO;
            $f2 = new stdClass();
            $f2->status = $isWritable;
            $f2->description = '';

            $f2->name = Atexo_SuperviseurServices_Constant::FILE_WRITE_RIGHTS.': '.$file;

            $rules[] = $f2;
        }

        return $rules;
    }
}
