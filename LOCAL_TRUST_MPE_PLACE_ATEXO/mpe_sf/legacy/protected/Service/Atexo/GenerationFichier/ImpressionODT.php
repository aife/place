<?php

namespace Application\Service\Atexo\GenerationFichier;

use Application\Propel\Mpe\CommonDecisionLot;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePapier;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTDonneesConsultation;
use Application\Propel\Mpe\CommonTFormePrix;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Register;
use Application\Service\Atexo\Consultation\Atexo_Consultation_RegistreVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use AtexoDume\Dto\Lot;
use DomDocument;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * classe d'impression PDF et ODS.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_GenerationFichier_ImpressionODT
{
    public function generateOdtFile($tableKeys, $modelePath)
    {
        $tmpDir = Atexo_Config::getParameter('COMMON_TMP').session_id().time().'modeleOdt/';
        Atexo_Util::removeDirectoryRecursively($tmpDir);
        if (!@mkdir($tmpDir)) {
            //TODO Ajout d'un log
            return '';
        }
        $filePath = $tmpDir.'tmpOdtFile'.session_id().time().'.zip';
        $cmd = 'cp '.escapeshellarg($modelePath).'  '.escapeshellarg($filePath);
        // echo $cmd;echo '<br>';
        system($cmd);
        //exit;
        // tester l'intégrité du fichier zip
        // -T revient au zip precedent en cas d'echec
        // -D ne considere pas
        $sys_answer = shell_exec('nice zip -T '.escapeshellarg($filePath));
        if (0 == strlen($sys_answer)) {
            //TODO Ajout d'un log
            @unlink($filePath);
            Atexo_Util::removeDirectoryRecursively($tmpDir);

            return '';
        } // END if
        //echo $tmpDir;exit;
        // décompresser l'archive

        $cmd = 'nice unzip '.$filePath.' -d '.$tmpDir.'  > '.Atexo_Config::getParameter('COMMON_TMP').'unzip_error 2>&1';
        system($cmd, $sys_answer);
        if (0 != $sys_answer) {
            @unlink($filePath);
            Atexo_Util::removeDirectoryRecursively($tmpDir);

            return '';
        }

        //lire le ficher cotent.xml
        $filename = $tmpDir.'content.xml';
        $handle = fopen($filename, 'r');
        $contents = fread($handle, filesize($filename));
        fclose($handle);
        //echo $filename;exit;
        $Dom = new DomDocument();
        $Dom->loadXML($contents);
        //echo $contents;exit;
        // remplacer les clés dans le contedu du ficher xml
        //par le contenu du tableau $tableKeys
        foreach ($tableKeys as $key => $tableKeyStager) {
            $contents = $this->xmlStrReplace($contents, $key, Atexo_Util::toUtf8($tableKeyStager));
        }
        // faire les modif dans le fichier content.xml
        $this->setContent($filename, $contents);

        $odtFileName = Atexo_Config::getParameter('COMMON_TMP').session_id().time().'generated.odt';
        $zipFileName = (new Atexo_Zip())->addFileToZip($filename, $filePath);
        if ($zipFileName) {
            rename($zipFileName, $odtFileName);
            @unlink($filePath);
            Atexo_Util::removeDirectoryRecursively($tmpDir);

            return $odtFileName;
        } else {
            @unlink($filePath);
            Atexo_Util::removeDirectoryRecursively($tmpDir);

            return '';
        }

        //////////////////////////////////////////////////////////
    }

    /**
     * remplacer les $champsARemplacer par les $valeurs dals $content.
     */
    public function xmlStrReplace($content, $champsARemplacer, $valeurs)
    {
        $valeurs = ('' == $valeurs) ? '' : $valeurs;

        return str_replace($champsARemplacer, $valeurs, $content);
    }

    /**
     * Mettre les données $content dans le fichier dont le chemin est $tmpFilePath.
     */
    public function setContent($tmpFilePath, $content)
    {
        $e = null;
        try {
            $fl = fopen($tmpFilePath, 'w');
            fwrite($fl, $content);
            fclose($fl);
        } catch (Exception) {
            Prado::log('Erreur ImpressionODT.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'ImpressionODT.php');
        }
    }

    /**
     * générer le code xml du tableau des candidatures.
     */
    public function imprimerTableauCandidature($consultation, $organisme = null)
    {
        //entête du tableau de Candidatures
        $xmlHeaderCandidatures = ' <text:p text:style-name="P5"/><text:p text:style-name="P6"/>
                                    <table:table table:name="Tableau3" table:style-name="Tableau3">
                                    <table:table-column table:style-name="Tableau3.A"/>
                                    <table:table-row table:style-name="Tableau3.1">
                                    <table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
            						<text:p text:style-name="P7">'.Prado::localize('CANDIDATURES').'</text:p>
            						</table:table-cell></table:table-row></table:table>
                                    <text:p text:style-name="P6"/>
                                    <table:table table:name="Tableau4" table:style-name="Tableau4">
                                    <table:table-column table:style-name="Tableau4.A"/>
                                    <table:table-column table:style-name="Tableau4.B"/>
                                    <table:table-column table:style-name="Tableau4.C"/>
                                    <table:table-column table:style-name="Tableau4.D"/>
                                    <table:table-header-rows>
                                    <table:table-row table:style-name="Tableau4.1">
                                    <table:table-cell table:style-name="Tableau4.A1" office:value-type="string">
                                    <text:p text:style-name="P8">'.Prado::localize('NUM_PLI').'</text:p>
                                    <text:p text:style-name="P8">'.Prado::localize('HORODATAGE').'</text:p>
                                    </table:table-cell><table:table-cell table:style-name="Tableau4.A1" office:value-type="string">
                                    <text:p text:style-name="P8">'.Prado::localize('TEXT_ENTREPRISE').'</text:p>
                                    </table:table-cell>
                                    <table:table-cell table:style-name="Tableau4.A1" office:value-type="string">
                                    <text:p text:style-name="P8">'.Prado::localize('STATUT_ENVELOPPE').'</text:p>
                                    <text:p text:style-name="P9">'.Prado::localize('CANDIDATURE').'</text:p>
                                    </table:table-cell><table:table-cell table:style-name="Tableau4.D1" office:value-type="string">
                                    <text:p text:style-name="P7">'.Prado::localize('ADMISSIBILITE').'</text:p>
                                    </table:table-cell>
                                    </table:table-row>
                                    </table:table-header-rows> ';

        //lignes du tableau de candidature
        $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
        }
        $criteriaResponse = new Atexo_Response_CriteriaVo();
        $criteriaResponse->setReference($consultation->getId());
        $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
        $criteriaResponse->setOrganisme($organisme);
        $arEnveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);
        $xmlCandidaturesContents = '';
        $lot = $consultation->getAllLots();
        if ($arEnveloppe) {
            $xmlCandidaturesContents = $this->generateXmlContentWithAdmissibilite($arEnveloppe, $lot, Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
        }
        $arEnveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidaturePapier($consultation->getId(), null, $typeEnveloppe, '', '', $organisme, false);
        if ($arEnveloppe) {
            $xmlCandidaturesContents .= $this->generateXmlContentWithAdmissibilite($arEnveloppe, $lot, Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
        }

        /// fermeture du tableau de candidature
        $xmlFooterCandidatures = ' </table:table> ';

        return $xmlHeaderCandidatures.$xmlCandidaturesContents.$xmlFooterCandidatures;
    }

    public function imprimerTableauxOffresTechniques($consultation, $organisme = null)
    {
        //entête du tableau de Candidatures
        $xmlHeaderOffreTechnique = ' <text:p text:style-name="P5"/><text:p text:style-name="P6"/>
                                    <table:table table:name="Tableau3" table:style-name="Tableau3">
                                    <table:table-column table:style-name="Tableau3.A"/>
                                    <table:table-row table:style-name="Tableau3.1">
                                    <table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
            						<text:p text:style-name="P7">'.Prado::localize('OFFRE_TECHNIQUE').' :LOT </text:p>
            						</table:table-cell></table:table-row></table:table>
                                    <text:p text:style-name="P6"/>
                                    <table:table table:name="Tableau4" table:style-name="Tableau4">
                                    <table:table-column table:style-name="Tableau4.A"/>
                                    <table:table-column table:style-name="Tableau4.B"/>
                                    <table:table-column table:style-name="Tableau4.C"/>
                                    <table:table-column table:style-name="Tableau4.D"/>
                                    <table:table-header-rows>
                                    <table:table-row table:style-name="Tableau4.1">
                                    <table:table-cell table:style-name="Tableau4.A1" office:value-type="string">
                                    <text:p text:style-name="P8">'.Prado::localize('NUM_PLI').'</text:p>
                                    <text:p text:style-name="P8">'.Prado::localize('HORODATAGE').'</text:p>
                                    </table:table-cell><table:table-cell table:style-name="Tableau4.A1" office:value-type="string">
                                    <text:p text:style-name="P8">'.Prado::localize('TEXT_ENTREPRISE').'</text:p>
                                    </table:table-cell>
                                    <table:table-cell table:style-name="Tableau4.A1" office:value-type="string">
                                    <text:p text:style-name="P8">'.Prado::localize('STATUT_ENVELOPPE').'</text:p>
                                    <text:p text:style-name="P9">'.Prado::localize('CANDIDATURE').'</text:p>
                                    </table:table-cell><table:table-cell table:style-name="Tableau4.D1" office:value-type="string">
                                    <text:p text:style-name="P7">'.Prado::localize('ADMISSIBILITE').'</text:p>
                                    </table:table-cell>
                                    </table:table-row>
                                    </table:table-header-rows> ';

        $xmlFooterOffreTechnique = ' </table:table> ';
        //lignes du tableau de candidature
        $xmlOffreTechniqueContents = ' ';
        $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
        }
        $lesLots = $consultation->getAllLots();
        $nbrLot = is_countable($lesLots) ? count($lesLots) : 0;
        if (0 == $nbrLot) {
            $xmlOffreTechniqueContents .= str_replace(' :LOT ', '', $xmlHeaderOffreTechnique);
            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresTechniqueElectroniques($consultation->getId(), 0, $typeEnveloppe, '', '', $organisme);
            if ($enveloppe) {
                $xmlOffreTechniqueContents .= $this->generateXmlContentWithAdmissibilite($enveloppe, '', Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
            }
            //  $xmlOffreTechniqueContents.= $xmlFooterOffreTechnique;

            //  $xmlOffreTechniqueContents.=str_replace(" :LOT ", "", $xmlHeaderOffreTechnique);
            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreTechniquePapier($consultation->getId(), $typeEnveloppe, 0, '', '', $organisme, false);
            if ($enveloppe) {
                $xmlOffreTechniqueContents .= $this->generateXmlContentWithAdmissibilite($enveloppe, '', Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
            }
            $xmlOffreTechniqueContents .= $xmlFooterOffreTechnique;
        } else {
            foreach ($lesLots as $unLot) {
                $xmlOffreTechniqueContents .= str_replace(':LOT', '- '.Prado::localize('LOT').' '.$unLot->getLot(), $xmlHeaderOffreTechnique);
                $arEnveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresTechniqueElectroniques($consultation->getId(), $unLot->getLot(), $typeEnveloppe, '', '', $organisme);
                if ($arEnveloppes) {
                    $xmlOffreTechniqueContents .= $this->generateXmlContentWithAdmissibilite($arEnveloppes, $unLot->getLot(), Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                }
                // $xmlOffreTechniqueContents.= $xmlFooterOffreTechnique;

                // $xmlOffreTechniqueContents.=str_replace(":LOT", "- Lot ".$index, $xmlHeaderOffreTechnique);
                $arEnveloppesPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreTechniquePapier($consultation->getId(), $typeEnveloppe, $unLot->getLot(), '', '', $organisme, false);
                if ($arEnveloppesPapier) {
                    $xmlOffreTechniqueContents .= $this->generateXmlContentWithAdmissibilite($arEnveloppesPapier, $unLot->getLot(), Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                }
                $xmlOffreTechniqueContents .= $xmlFooterOffreTechnique;
            }
        }

        /// fermeture du tableau de candidature

        return $xmlOffreTechniqueContents;
    }

    /**
     * générer le code xml du tableau des offres.
     */
    public function imprimerTableauxOffres($consultation, $organisme = null)
    {
        //ligneS du tableau des offres
        $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());

        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
        }
        $lesLots = $consultation->getAllLots();
        $nbrLot = is_countable($lesLots) ? count($lesLots) : 0;

        $xmlOffresContents = ' ';
        if (0 == $nbrLot) {
            $criteriaResponse = new Atexo_Response_CriteriaVo();
            $criteriaResponse->setReference($consultation->getId());
            $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
            $criteriaResponse->setOrganisme($organisme);
            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
            if ($enveloppe) {
                $xmlOffresContents = $this->generateXmlContent($enveloppe, '', Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
            }
            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier($consultation->getId(), $typeEnveloppe, 0, '', '', $organisme, false, true, false);
            if ($enveloppe) {
                $xmlOffresContents .= $this->generateXmlContent($enveloppe, '', Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
            }
        } else {
            foreach ($lesLots as $unLot) {
                $criteriaResponse = new Atexo_Response_CriteriaVo();
                $criteriaResponse->setReference($consultation->getId());
                $criteriaResponse->setSousPli($unLot->getLot());
                $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
                $criteriaResponse->setOrganisme($organisme);
                $arEnveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
                if ($arEnveloppes) {
                    $xmlOffresContents .= $this->generateXmlContent($arEnveloppes, $unLot->getLot(), Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                }
                $arEnveloppesPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier($consultation->getId(), $typeEnveloppe, $unLot->getLot(), '', '', $organisme, false, true, false);

                if ($arEnveloppesPapier) {
                    $xmlOffresContents .= $this->generateXmlContent($arEnveloppesPapier, $unLot->getLot(), Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                }
            }
        }

        return $xmlOffresContents;
    }

    /**
     *générer le contenu du tableau des candidatures
     * (les lignes avec leurs contenu).
     */
    public function generateXmlContentWithAdmissibilite($arEnveloppe, $lots, $typeEnveloppe)
    {
        $xmlContents = null;
        if ($arEnveloppe) {
            $xmlContents = '';
            $total = '';
            foreach ($arEnveloppe as $oneEnv) {
                if ($oneEnv instanceof CommonEnveloppePapier) {
                    $offre = $oneEnv->getCommonOffrePapier();
                    $dateDepot = $offre->getDateDepot();
                    $formatEnveloppe = ucfirst(Prado::localize('PA_POUR_PAPIER'));
                } elseif ($oneEnv instanceof CommonEnveloppe) {
                    $offre = $oneEnv->getCommonOffres();
                    $dateDepot = $offre->getUntrusteddate();
                    $formatEnveloppe = ucfirst(Prado::localize('EL_POUR_ELECTRONIQUE'));
                }
                if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') && true === $offre->getMoreThanOneLot()) {
                    $admissible = $offre->getNombreLotsAdmissibles();
                    $nonAdmissible = $offre->getNombreLotsNonAdmissibles();
                    $aTraiter = $offre->getNombreLotsATraiter();
                    $total = $admissible + $nonAdmissible + $aTraiter.' / '.(is_countable($lots) ? count($lots) : 0);
                } else {
                    if (0 == $offre->getNombreLotsAdmissibles()) {
                        $admissible = ' - ';
                    } else {
                        $admissible = Prado::localize('TEXT_OUI');
                    }
                    if (0 == $offre->getNombreLotsNonAdmissibles()) {
                        $nonAdmissible = ' - ';
                    } else {
                        $nonAdmissible = Prado::localize('TEXT_NON');
                    }
                    if (0 == $offre->getNombreLotsATraiter()) {
                        $aTraiter = ' - ';
                    } else {
                        $aTraiter = Prado::localize('A_TRAITER');
                    }
                }

                $statutEnveloppe = $oneEnv->getLibelleStatutEnveloppe();
                $agentOuverture = '';
                if ($oneEnv->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
                || $oneEnv->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                || $oneEnv->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                || $oneEnv->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) {
                    $statutEnveloppe .= ' '.Prado::localize('TEXT_LE').' : '.Atexo_Util::iso2frnDateTime($oneEnv->getDateheureOuverture());
                    if ('' != $oneEnv->getAgentIdOuverture()) {
                        $agent = (new Atexo_Agent())->retrieveAgent($oneEnv->getAgentIdOuverture());
                        if ($agent) {
                            $agentOuverture = Prado::localize('PAR').' : '.Atexo_Util::escapeXmlChars($agent->getPrenom()).' '.Atexo_Util::escapeXmlChars($agent->getNom());
                        }
                    } elseif ('' != $oneEnv->getNomAgentOuverture()) {
                        $agentOuverture = Prado::localize('PAR').' : '.Atexo_Util::escapeXmlChars($oneEnv->getNomAgentOuverture());
                    }
                }
                $xmlContents .= '<table:table-row table:style-name="Tableau4.2">
                <table:table-cell table:style-name="Tableau4.A2" office:value-type="string">
                <text:p text:style-name="P5">'.$formatEnveloppe.' '.$oneEnv->getNumPli().'</text:p>
                <text:p text:style-name="P5">'.Atexo_Util::iso2frnDateTime($dateDepot, false).'<text:s/>
                '.Atexo_Util::iso2frnTimeNoSep($dateDepot, true).'</text:p></table:table-cell>
                <table:table-cell table:style-name="Tableau4.A2" office:value-type="string">
                <text:p text:style-name="P5">'.Atexo_Util::escapeXmlChars($offre->getNomEntreprise()).'</text:p></table:table-cell>
                <table:table-cell table:style-name="Tableau4.A2" office:value-type="string">
                <text:p text:style-name="P5">'.$statutEnveloppe.'</text:p>
                <text:p text:style-name="P5">'.$agentOuverture.'</text:p>
                </table:table-cell>
                <table:table-cell table:style-name="Tableau4.D2" office:value-type="string">
                <text:p text:style-name="P5">'.Prado::localize('ADMISSIBLE').' : '.$admissible.'</text:p>
                <text:p text:style-name="P5">'.Prado::localize('NON_ADMISSIBLE').' :  '.$nonAdmissible.'</text:p>
                <text:p text:style-name="P5">'.Prado::localize('A_TRAITER').' : '.$aTraiter.' </text:p>';

                if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') && true === $offre->getMoreThanOneLot()) {
                    $xmlContents .= '<text:p text:style-name="P10"> '.Prado::localize('TEXT_TOTAL').' : '.$total.' </text:p>';
                }
                $xmlContents .= '</table:table-cell>
                </table:table-row>';
            }
        }

        return $xmlContents;
    }

    /**
     * générer le code xml du tableau des anonymats.
     */
    public function imprimerTableauxAnonymat($consultation, $organisme = null)
    {
        //ligneS du tableau des offres
        $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
        }
        $lesLots = $consultation->getAllLots();
        $nbrLot = is_countable($lesLots) ? count($lesLots) : 0;
        $xmlOffresContents = ' ';
        if (0 == $nbrLot) {
            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatElectroniques($consultation->getId(), 0, $typeEnveloppe, '', '', $organisme, false);
            if ($enveloppe) {
                $xmlOffresContents = $this->generateXmlContent($enveloppe, '', Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
            }
            $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatPapier($consultation->getId(), $typeEnveloppe, 0, '', '', $organisme, false, true, false);
            if ($enveloppe) {
                $xmlOffresContents .= $this->generateXmlContent($enveloppe, '', Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
            }
        } else {
            foreach ($lesLots as $unLot) {
                $arEnveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatElectroniques($consultation->getId(), 0, $typeEnveloppe, '', '', $organisme, true, false);
                if ($arEnveloppes) {
                    $xmlOffresContents .= $this->generateXmlContent($arEnveloppes, $unLot->getLot(), Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                }
                $arEnveloppesPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatPapier($consultation->getId(), $typeEnveloppe, 0, '', '', $organisme, false, true, false);
                if ($arEnveloppesPapier) {
                    $xmlOffresContents .= $this->generateXmlContent($arEnveloppesPapier, $unLot->getLot(), Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                }
            }

            return $xmlOffresContents;
        }
    }

    /**
     * générer le contenu du tableau des offres(les lignes avec leurs contenu).
     *
     * @param  $enveloppes tableau d'enveloppes
     * @param  $lot numero du lot
     * @param $offre true si appel pour tabeau d'offres et false sinon
     */
    public function generateXmlContent($enveloppes, $lot, $typeEnv)
    {
        $xmlElContents = null;
        if ($enveloppes) {
            //entête du tableau des Offres
            $xmlHeader = ' <text:p text:style-name="P5"/>
                        <text:p text:style-name="P6"/>
                        <table:table table:name="Tableau5" table:style-name="Tableau5">
                        <table:table-column table:style-name="Tableau5.A"/>
                        <table:table-row table:style-name="Tableau5.1">
                        <table:table-cell table:style-name="Tableau5.A1" office:value-type="string">';
            if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                $xmlHeader .= ' <text:p text:style-name="P7">'.Prado::localize('OFFRES');
            } else {
                $xmlHeader .= ' <text:p text:style-name="P7">'.Prado::localize('ANONYMAT');
            }
            if ($lot) {
                $xmlHeader .= ' - '.Prado::localize('LOT').' '.$lot;
            }
            $xmlHeader .= ' </text:p>';
            $xmlHeader .= '</table:table-cell>
                        </table:table-row>
                        </table:table>
                        <text:p text:style-name="P6"/>
                        <table:table table:name="Tableau6" table:style-name="Tableau6">
                        <table:table-column table:style-name="Tableau6.A"/>
                        <table:table-column table:style-name="Tableau6.B"/>
                        <table:table-column table:style-name="Tableau6.C"/>
                        <table:table-header-rows>
                        <table:table-row table:style-name="Tableau6.1">
                        <table:table-cell table:style-name="Tableau6.A1" office:value-type="string">
                        <text:p text:style-name="P8">'.Prado::localize('NUM_PLI').'</text:p>
                        <text:p text:style-name="P8">'.Prado::localize('HORODATAGE').'</text:p>
                        </table:table-cell>
                        <table:table-cell table:style-name="Tableau6.A1" office:value-type="string">
                        <text:p text:style-name="P8">'.Prado::localize('TEXT_ENTREPRISE').'</text:p>
                        </table:table-cell>
                        <table:table-cell table:style-name="Tableau6.C1" office:value-type="string">
                        <text:p text:style-name="P8">'.Prado::localize('STATUT_ENVELOPPE').'</text:p>';
            if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                $xmlHeader .= '<text:p text:style-name="P13">'.Prado::localize('OFFRES');
            } else {
                $xmlHeader .= '<text:p text:style-name="P13">'.Prado::localize('ANONYMAT');
            }
            if ($lot) {
                $xmlHeader .= ' - '.Prado::localize('LOT').' '.$lot;
            }
            $xmlHeader .= ' </text:p></table:table-cell>';
            $xmlHeader .= '</table:table-row>  </table:table-header-rows>';
            /// fermeture du tableau de candidature
            $xmlFooter = ' </table:table> <text:p text:style-name="P5"/>';
            foreach ($enveloppes as $enveloppe) {
                $statutEnveloppe = $enveloppe->getLibelleStatutEnveloppe();
                $agentOuverture = '';
                if ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
                || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) {
                    $statutEnveloppe .= ' '.Prado::localize('TEXT_LE').' : '.Atexo_Util::iso2frnDateTime($enveloppe->getDateheureOuverture());
                    if ($enveloppe->getAgentIdOuverture()) {
                        $agent = (new Atexo_Agent())->retrieveAgent($enveloppe->getAgentIdOuverture());
                        if ($agent) {
                            $agentOuverture = Prado::localize('PAR').' : '.Atexo_Util::escapeXmlChars($agent->getPrenom()).' '.Atexo_Util::escapeXmlChars($agent->getNom());
                        }
                    } elseif ('' != $enveloppe->getNomAgentOuverture()) {
                        $agentOuverture = Prado::localize('PAR').' : '.Atexo_Util::escapeXmlChars($enveloppe->getNomAgentOuverture());
                    }
                }
                $entreprise = '';
                if ($enveloppe instanceof CommonEnveloppe) {
                    $untrustedDate = $enveloppe->getCommonOffres()->getUntrusteddate();
                    $typeEnveloppe = ucfirst(Prado::localize('EL_POUR_ELECTRONIQUE'));
                    if ('' != $enveloppe->getCommonOffres()->getNomEntreprise()) {
                        $entreprise = Atexo_Util::escapeXmlChars($enveloppe->getCommonOffres()->getNomEntreprise());
                    } else {
                        $entreprise = '-';
                    }
                } elseif ($enveloppe instanceof CommonEnveloppePapier) {
                    $untrustedDate = $enveloppe->getCommonOffrePapier()->getDateDepot();
                    $typeEnveloppe = ucfirst(Prado::localize('PA_POUR_PAPIER'));
                    if ('' != $enveloppe->getCommonOffrePapier()->getNomEntreprise()) {
                        $entreprise = Atexo_Util::escapeXmlChars($enveloppe->getCommonOffrePapier()->getNomEntreprise());
                    } else {
                        $entreprise = '-';
                    }
                }
                if (strstr($untrustedDate, '/')) {
                    $untrustedDate = Atexo_Util::frnDateTime2iso($untrustedDate);
                }
                $xmlElContents .= '<table:table-row table:style-name="Tableau6.2">
            <table:table-cell table:style-name="Tableau6.A2" office:value-type="string">
            <text:p text:style-name="P5">'.$typeEnveloppe.$enveloppe->getNumPli().' </text:p>
            <text:p text:style-name="P5">'.Atexo_Util::iso2frnDateTime($untrustedDate, false).'<text:s/>
             '.Atexo_Util::iso2frnTimeNoSep($untrustedDate, true).'</text:p>
            </table:table-cell>
            <table:table-cell table:style-name="Tableau6.A2" office:value-type="string">
            <text:p text:style-name="P5">'.$entreprise.'</text:p>
            </table:table-cell>
            <table:table-cell table:style-name="Tableau6.C2" office:value-type="string">
            <text:p text:style-name="P5">'.$statutEnveloppe.'</text:p>
            <text:p text:style-name="P10">'.$agentOuverture.'</text:p>
            </table:table-cell></table:table-row>';
            }

            return $xmlHeader.$xmlElContents.$xmlFooter;
        }
    }

    public function printTableauDecision($consultation, $numLot = null)
    {
        $reponses = null;
        $ligneNumLot = null;
        if ($numLot) {
            $ligneNumLot = '<table:table table:name="Tableau4" table:style-name="Tableau4">
							  <table:table-column table:style-name="Tableau4.A" /> 
							  <table:table-row table:style-name="Tableau4.1">
							  <table:table-cell table:style-name="Tableau4.A1" office:value-type="string">
							  <text:p text:style-name="P11">LOT N° : '.$numLot.'</text:p> 
							  </table:table-cell>
							  </table:table-row>
							  </table:table>';
        }
        $EntetTableauDesicion = '<table:table table:name="Tableau3" table:style-name="Tableau3">
								  <table:table-column table:style-name="Tableau3.A" /> 
								  <table:table-column table:style-name="Tableau3.B" /> 
								  <table:table-column table:style-name="Tableau3.C" /> 
								  <table:table-column table:style-name="Tableau3.D" /> 
								  <table:table-row table:style-name="Tableau3.1">
								  <table:table-cell table:style-name="Tableau3.A1" table:number-columns-spanned="4" office:value-type="string">
								  <text:p text:style-name="P9">'.Prado::localize('MONTANT_HT_ESTIME').'</text:p> 
								  </table:table-cell>
								  <table:covered-table-cell /> 
								  <table:covered-table-cell /> 
								  <table:covered-table-cell /> 
								  </table:table-row>
								  <table:table-row table:style-name="Tableau3.1">
								  <table:table-cell table:style-name="Tableau3.A2" office:value-type="string">
								  <text:p text:style-name="P9">'.Prado::localize('CLASSEMENT').'</text:p> 
								  </table:table-cell>
								  <table:table-cell table:style-name="Tableau3.A2" office:value-type="string">
								  <text:p text:style-name="P9">'.Prado::localize('NO_DEPOT').'</text:p> 
								  </table:table-cell>
								  <table:table-cell table:style-name="Tableau3.A2" office:value-type="string">
								  <text:p text:style-name="P9">'.Prado::localize('NOM_RAISON_SOCIAL_CANDIDAT').'</text:p> 
								  </table:table-cell>
								  <table:table-cell table:style-name="Tableau3.D2" office:value-type="string">
								  <text:p text:style-name="P9">'.Prado::localize('MONTANT_HT').'</text:p> 
								  </table:table-cell>
								  </table:table-row>
								  </table:table>';
        $lotOrConsultation = (new Atexo_Consultation_Responses())->retrieveLotsForDecision($consultation->getId(), $numLot, Atexo_CurrentUser::getCurrentOrganism());
        if (is_array($lotOrConsultation) && 1 == count($lotOrConsultation)) {
            $lotOrConsultation = array_shift($lotOrConsultation);
            if ($lotOrConsultation->getDecisionLot() instanceof CommonDecisionLot) {
                $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
                if ($typeEnveloppe != Atexo_Config::getParameter('CANDIDATURE_SEUL')) {
                    $criteriaResponse = new Atexo_Response_CriteriaVo();
                    $criteriaResponse->setReference($consultation->getId());
                    $criteriaResponse->setSousPli($numLot);
                    $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
                    $criteriaResponse->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $criteriaResponse->setReturnReponses(false);
                    $criteriaResponse->setNomEntrepriseVisible(true);
                    $criteriaResponse->setEnveloppeStatutOuverte(true);
                    $reponsesElectronique = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
                    $reponsesPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier($consultation->getId(), $typeEnveloppe, $numLot, '', '', Atexo_CurrentUser::getCurrentOrganism(), false, false, true, true);
                    $reponses = Atexo_Util::arrayMerge(true, $reponsesElectronique, $reponsesPapier);
                }
            }
        }
        $corpsTableauDecision = '';
        if (is_array($reponses) && count($reponses) > 0) {
            foreach ($reponses as $unPli) {
                $heureDepot = '';
                if ($unPli instanceof CommonOffrePapier) {
                    $dateDepot = $unPli->getDateDepot();
                    $format = 'PA';
                }
                if ($unPli instanceof CommonOffres) {
                    $dateDepot = Atexo_Util::iso2frnDateTime($unPli->getUntrusteddate(), false);
                    $heureDepot = Atexo_Util::iso2frnTimeNoSep($unPli->getUntrusteddate(), true);
                    $format = 'EL';
                }
                $montantMarche = '';
                if ($unPli->getCommonDecisionEnveloppe()) {
                    $montantMarche = $unPli->getCommonDecisionEnveloppe()->getMontantMarche();
                }
                $corpsTableauDecision .= '<table:table table:name="Tableau5" table:style-name="Tableau5">
										  <table:table-column table:style-name="Tableau5.A" /> 
										  <table:table-column table:style-name="Tableau5.B" /> 
										  <table:table-column table:style-name="Tableau5.C" /> 
										  <table:table-column table:style-name="Tableau5.D" /> 
										  <table:table-row>
										  <table:table-cell table:style-name="Tableau5.A1" office:value-type="string">
										  <text:p text:style-name="P10" /> 
										  </table:table-cell>
										  <table:table-cell table:style-name="Tableau5.A1" office:value-type="string">
										  <text:p text:style-name="P10">'.$format.' '.$unPli->getNumPli().'</text:p> 
										  <text:p text:style-name="P10">'.$dateDepot.'<text:s />'.$heureDepot.'</text:p>
										  </table:table-cell>
										  <table:table-cell table:style-name="Tableau5.A1" office:value-type="string">
										  <text:p text:style-name="P10">'.$unPli->getNomEntreprise().'</text:p> 
										  </table:table-cell>
										  <table:table-cell table:style-name="Tableau5.D1" office:value-type="string">
										  <text:p text:style-name="P10">'.$montantMarche.'</text:p> 
										  </table:table-cell>
										  </table:table-row>
										  </table:table>';
                $footer = '<text:p text:style-name="P4" /> ';
            }

            return $ligneNumLot.$EntetTableauDesicion.$corpsTableauDecision.$footer;
        }
    }

    /*
  * Fonction permet d'ajouter les données complementaires
  * dans le cas d'une consultation non alloti sans tranches
  * @param objet donnees complementaire
  * @param table de cles (passage pas reference)
  */
    public function ajoutTableauDonnesComplementaireNonAllotiSansTranche($donneeComplementraire, &$tableKeys)
    {
        $libelleTypePrix = null;
        $arrayDonnesComp = $this->getEnteteNonAllotiSansTranche();
        $formePrix = $donneeComplementraire->getFormePrix();
        $found = false;
        $arrayPrix = [];
        if ($formePrix instanceof CommonTFormePrix) {
            if (('bon_commande' == $formePrix->getModalite()) && !$found) {
                $tableKeys['commandeMarche'] = Prado::Localize('DEFINE_OUI');
                $found = true;
            }
            if ('PU' == $formePrix->getFormePrix()) {
                $libelleTypePrix = Prado::localize('DEFINE_UNITAIRE');
                $variationUnitaires = $formePrix->getAllVariationsPrixUnitaire();
                $arrayPrix = $this->getTypeFormPrix($variationUnitaires);
            } elseif ('PF' == $formePrix->getFormePrix()) {
                $libelleTypePrix = Prado::localize('DEFINE_FORFAITAIRE');
                $variationPrixFor = $formePrix->getAllVariationsPrixForfaitaire();
                $arrayPrix = $this->getTypeFormPrix($variationPrixFor);
            } elseif ('PM' == $formePrix->getFormePrix()) {
                $libelleTypePrix = Prado::localize('DEFINE_MIXTE');
                $variationFor = $formePrix->getAllVariationsPrixForfaitaire();
                $arrayPrix = $this->getTypeFormPrix($variationFor);
            }
            $arrayDonnesComp .= '<table:table-row table:style-name="Tableau3.2">
								<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
									<text:p text:style-name="P7">'.$libelleTypePrix.'</text:p>
								</table:table-cell>
								<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
									'.$arrayPrix.'
								</table:table-cell>
								</table:table-row>';
        }
        $arrayDonnesComp .= '</table:table>';

        return $arrayDonnesComp;
    }

    /*
    * Fonction permet d'ajouter les données complementaires
    * dans le cas d'une consultation non alloti avec tranches
    * @param objet donnees complementaire
    * @param table de cles (passage pas reference)
    */
    public function ajoutTableauDonnesComplementaireNonAllotiAvecTranche($donneCompelementaire, &$tableKeys)
    {
        $tableau = $this->getEnteteNonAllotiAvecTranche();
        $i = 1;
        $found = false;
        $tranches = $donneCompelementaire->getAllTranches();
        if ($tranches) {
            $tableKeys['tranche'] = Prado::Localize('DEFINE_OUI');
            foreach ($tranches as $tranche) {
                $intituleTranche = $tranche->getIntituleTranche();
                $formePrix = $tranche->getFormePrix();
                $arrayDonnees = [];

                if ($tranche->getNatureTranche() == Atexo_Config::getParameter('TRANCHE_FIXE')) {
                    $identifiantTranche = Prado::localize('DEFINE_FERME');
                } elseif ($tranche->getNatureTranche() == Atexo_Config::getParameter('TRANCHE_CONDITIONNELLE')) {
                    $identifiantTranche = Prado::localize('DEFINE_CONDITIONNELLE').' '.$i;
                    ++$i ;
                }
                if ($formePrix instanceof CommonTFormePrix) {
                    if (('bon_commande' == $formePrix->getModalite()) && !$found) {
                        $tableKeys['commandeMarche'] = Prado::Localize('DEFINE_OUI');
                        $found = true;
                    }
                    $arrayDonnees = $this->getArrayFormePrix($formePrix);
                }
                $tableau .= '<table:table-row table:style-name="Tableau3.2">
							<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
								<text:p text:style-name="P7">'.$identifiantTranche.'</text:p>
							</table:table-cell>
							<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
								<text:p text:style-name="P7">
									'.$intituleTranche.'
								</text:p>
							</table:table-cell>
							<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
								<text:p text:style-name="P7">'.$arrayDonnees['libelleTypePrix'].'</text:p>
							</table:table-cell>
							<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
								'.$arrayDonnees['arrayTypePrix'].'
							</table:table-cell>
							</table:table-row>';
            }
        }
        $tableau .= '</table:table>';

        return $tableau;
    }

    /*
  * Fonction permet d'ajouter les données complementaires
  * dans le cas d'une consultation alloti sans tranches
  * @param arrau des objets los
  * @param table de cles (passage pas reference)
  */
    public function ajoutTableauDonnesComplementaireAllotiSansTranche(&$tableKeys, &$lots)
    {
        $tableau = $this->getEnteteTableauAllotiSansTranche();
        $found = false;
        $foundReconductible = false;
        foreach ($lots as $lot) {
            $arrayDonnees = [];
            $numLot = $lot->getLot();
            $intituleLot = $lot->getDescription();
            $donnesCompleLot = $lot->getDonneComplementaire();
            if ($donnesCompleLot instanceof CommonTDonneeComplementaire) {
                if (('1' == $donnesCompleLot->getReconductible()) && !$foundReconductible) {
                    $foundReconductible = true;
                    $tableKeys['Marchereconductible'] = Prado::Localize('DEFINE_OUI');
                }
                $formePrix = $donnesCompleLot->getFormePrix();
                if ($formePrix instanceof CommonTFormePrix) {
                    if (('bon_commande' == $formePrix->getModalite()) && !$found) {
                        $found = true;
                        $tableKeys['commandeMarche'] = Prado::Localize('DEFINE_OUI');
                    }
                    $arrayDonnees = $this->getArrayFormePrix($formePrix);
                }
                $tableau .= '<table:table-row table:style-name="Tableau3.1">
				<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
					<text:p text:style-name="P7">'.$numLot.'</text:p>
				</table:table-cell>
				<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
					<text:p text:style-name="P7">
						<text:span text:style-name="T3">'.$intituleLot.'</text:span>
					</text:p>
				</table:table-cell>
				<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
					<text:p text:style-name="P7">'.$arrayDonnees['libelleTypePrix'].'</text:p>
				</table:table-cell>
				<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
					'.$arrayDonnees['arrayTypePrix'].'
				</table:table-cell>
				</table:table-row>';
            }
        }
        $tableau .= '</table:table>';

        return $tableau;
    }

    /*
    * Fonction permet d'ajouter les données complementaires
    * dans le cas d'une consultation alloti dont un lot ayant
    * une tranche
    * @param arrau des objets los
    * @param table de cles (passage pas reference)
    */
    public function ajoutTableauDonnesComplementaireAllotiAvecTranche(&$tableKeys, &$lots)
    {
        $tableau = $this->getEnteteTableauAllotiAvecTranche();
        $found = false;
        $foundReconductible = false;
        foreach ($lots as $lot) {
            $numLot = $lot->getLot();
            $intituleLot = $lot->getDescription();
            $tableau .= $this->insertLigneLot($numLot, $intituleLot);
            $donnesCompleLot = $lot->getDonneComplementaire();
            if ($tranches = $donnesCompleLot->getAllTranches()) {
                $i = 1;
                foreach ($tranches as $tranche) {
                    $arrayDonnees = [];
                    $identifiantTranche = '';
                    $intituleTranche = $tranche->getIntituleTranche();
                    if ($tranche->getNatureTranche() == Atexo_Config::getParameter('TRANCHE_FIXE')) {
                        $identifiantTranche = Prado::localize('DEFINE_FERME');
                    } elseif ($tranche->getNatureTranche() == Atexo_Config::getParameter('TRANCHE_CONDITIONNELLE')) {
                        $identifiantTranche = Prado::localize('DEFINE_CONDITIONNELLE').' '.$i;
                        ++$i ;
                    }
                    $formePrix = $tranche->getFormePrix();
                    if ($formePrix instanceof CommonTFormePrix) {
                        if (('bon_commande' == $formePrix->getModalite()) && !$found) {
                            $tableKeys['commandeMarche'] = Prado::Localize('DEFINE_OUI');
                            $found = true;
                        }
                        if (('1' == $donnesCompleLot->getReconductible()) && !$foundReconductible) {
                            $tableKeys['Marchereconductible'] = Prado::Localize('DEFINE_OUI');
                            $foundReconductible = true;
                        }
                        $arrayDonnees = $this->getArrayFormePrix($formePrix);
                    }
                    $tableau .= $this->insertLignelotAvecTranche($identifiantTranche, $intituleTranche, $arrayDonnees['libelleTypePrix'], $arrayDonnees['arrayTypePrix']);
                }
            } elseif ($donnesCompleLot->getIdFormePrix()) {
                $formePrix = $donnesCompleLot->getFormePrix();
                $identifiantTranche = '';
                $intituleTranche = '';
                $arrayDonnees = [];
                if ($formePrix instanceof CommonTFormePrix) {
                    if (('bon_commande' == $formePrix->getModalite()) && !$found) {
                        $tableKeys['commandeMarche'] = Prado::Localize('DEFINE_OUI');
                        $found = true;
                    }
                    if (($donnesCompleLot->getReconductible() == Atexo_Config::getParameter('DONNEE_COMPLEMENTAIRE_MARCHE_RECONDUCTIBLE')) && !$foundReconductible) {
                        $tableKeys['Marchereconductible'] = Prado::Localize('DEFINE_OUI');
                        $foundReconductible = true;
                    }
                    $arrayDonnees = $this->getArrayFormePrix($formePrix);
                    $tableau .= $this->insertLignelotAvecTranche($identifiantTranche, $intituleTranche, $arrayDonnees['libelleTypePrix'], $arrayDonnees['arrayTypePrix']);
                }
            }
        }

        $tableau .= '</table:table>';

        return $tableau;
    }

    /*
     *  Fonction permet de recuperer les types de prix d'une objet form de prix
     *  param array des objets de  variation de prix
     *  return array de donnees de type de prix
     */
    public function getTypeFormPrix($variationPrix)
    {
        $arrayPrix = [];
        foreach ($variationPrix as $typePrix) {
            if ($typePrix->getIdVariation() == Atexo_Config::getParameter('TYPE_PRIX_REVISABLES')) {
                $arrayPrix .= '<text:p text:style-name="P7">
								'.Prado::localize('DEFINE_REVISABLES').'
							</text:p>';
            } elseif ($typePrix->getIdVariation() == Atexo_Config::getParameter('TYPE_PRIX_ACTUALISABLES')) {
                $arrayPrix .= '<text:p text:style-name="P7">
								'.Prado::localize('DEFINE_ACTUALISABLES').'
							</text:p>';
            } elseif ($typePrix->getIdVariation() == Atexo_Config::getParameter('TYPE_PRIX_FERMES')) {
                $arrayPrix .= '<text:p text:style-name="P7">
								'.Prado::localize('DEFINE_FERMES').'
							</text:p>';
            } elseif ($typePrix->getIdVariation() == Atexo_Config::getParameter('TYPE_PRIX_ACTUALISABLES_REVISABLES')) {
                $arrayPrix .= '<text:p text:style-name="P7">
								'.Prado::localize('DEFINE_FERME_REVISABLE').'
							</text:p>';
            } elseif ($typePrix->getIdVariation() == Atexo_Config::getParameter('TYPE_PRIX_FERMES_ACTUALISABLES')) {
                $arrayPrix .= '<text:p text:style-name="P7">
								'.Prado::localize('DEFINE_ACTUALISABLE_REVISABLE').'
							</text:p>';
            }
        }

        return $arrayPrix;
    }

    /*
     * Fonction permet de recuperer les donnees d'une objet form de prix
     *  param objet Forme de prix
     *  return array de donnees
     */
    public function getArrayFormePrix($formePrix)
    {
        $arrayPrixDonnees = [];
        if ('PU' == $formePrix->getFormePrix()) {
            $arrayPrixDonnees['libelleTypePrix'] = Prado::localize('DEFINE_UNITAIRE');
            $arrayPrixDonnees['variationUnitaires'] = $formePrix->getAllVariationsPrixUnitaire();
            $arrayPrixDonnees['arrayTypePrix'] = $this->getTypeFormPrix($arrayPrixDonnees['variationUnitaires']);
        } elseif ('PF' == $formePrix->getFormePrix()) {
            $arrayPrixDonnees['libelleTypePrix'] = Prado::localize('DEFINE_FORFAITAIRE');
            $arrayPrixDonnees['variationUnitaires'] = $formePrix->getAllVariationsPrixForfaitaire();
            $arrayPrixDonnees['arrayTypePrix'] = $this->getTypeFormPrix($arrayPrixDonnees['variationUnitaires']);
        } elseif ('PM' == $formePrix->getFormePrix()) {
            $arrayPrixDonnees['libelleTypePrix'] = Prado::localize('DEFINE_MIXTE');
            $arrayPrixDonnees['variationUnitaires'] = $formePrix->getAllVariationsPrixForfaitaire();
            $arrayPrixDonnees['arrayTypePrix'] = $this->getTypeFormPrix($arrayPrixDonnees['variationUnitaires']);
        }

        return $arrayPrixDonnees;
    }

    /*
    * Fonction permet d'ajouter l'entete du tableau pour une consultation
    *  alloti sans tranche
    */
    public function getEnteteTableauAllotiSansTranche($tagFin = false)
    {
        $tableau = '<table:table table:name="Tableau3" table:style-name="Tableau3">
					<table:table-column table:style-name="Tableau3.A"/>
					<table:table-column table:style-name="Tableau3.B"/>
					<table:table-column table:style-name="Tableau3.C"/>
					<table:table-column table:style-name="Tableau3.D"/>
					<table:table-row table:style-name="Tableau3.1">
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">Identifiant du lot</text:p>
					</table:table-cell>
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">
							Intitulé du lot
						</text:p>
					</table:table-cell>
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">Forme de prix</text:p>
					</table:table-cell>
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">Prix</text:p>
					</table:table-cell>
				</table:table-row>';
        if ($tagFin) {
            $tableau .= '</table:table>';
        }

        return $tableau;
    }

    /*
    * Fonction permet d'ajouter l'entete du tableau pour une consultation
    *  alloti et dont les lots  ayant des tranches
    */
    public function getEnteteTableauAllotiAvecTranche($tagFin = false)
    {
        $tableau = '<table:table table:name="Tableau3" table:style-name="Tableau3">
					<table:table-column table:style-name="Tableau3.A"/>
					<table:table-column table:style-name="Tableau3.B"/>
					<table:table-column table:style-name="Tableau3.C"/>
					<table:table-column table:style-name="Tableau3.D"/>
					<table:table-row table:style-name="Tableau3.1">
						<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
							<text:p text:style-name="P7">
								Identifiant du lot / de la tranche
							</text:p>
						</table:table-cell>
						<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
							<text:p text:style-name="P7">
								<text:span text:style-name="T3">Intitulé du lot / de la tranche</text:span>
							</text:p>
						</table:table-cell>
						<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
							<text:p text:style-name="P7"><text:span text:style-name="T3">Forme de prix</text:span></text:p>
						</table:table-cell>
						<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
							<text:p text:style-name="P7">
								<text:span text:style-name="T3">Prix</text:span>
							</text:p>
						</table:table-cell>
					</table:table-row>';
        if ($tagFin) {
            $tableau .= '</table:table>';
        }

        return $tableau;
    }

    /*
     * Fonction permet d'ajouter l'entete du tableau pour une consultation
     *  non alloti avec tranches
     */
    public function getEnteteNonAllotiAvecTranche($tagFin = false)
    {
        $tableau = '<table:table table:name="Tableau3" table:style-name="Tableau3">
				<table:table-column table:style-name="Tableau3.A"/>
				<table:table-column table:style-name="Tableau3.B"/>
				<table:table-column table:style-name="Tableau3.C"/>
				<table:table-column table:style-name="Tableau3.D"/>
				<table:table-row table:style-name="Tableau3.1">
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">Tranche/ identifiant</text:p>
					</table:table-cell>
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">Intitulé</text:p>
					</table:table-cell>
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">Forme de prix</text:p>
					</table:table-cell>
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">Prix</text:p>
					</table:table-cell>
				</table:table-row>';
        if ($tagFin) {
            $tableau .= '</table:table>';
        }

        return $tableau;
    }

    /*
    * Fonction permet d'ajouter l'entete du tableau pour une consultation
    *  non alloti sans tranches
    */
    public function getEnteteNonAllotiSansTranche($tagFin = false)
    {
        $arrayDonnesComp = '<table:table table:name="Tableau3" table:style-name="Tableau3">
							<table:table-column table:style-name="Tableau3.A"/>
							<table:table-column table:style-name="Tableau3.B"/>
							<table:table-row table:style-name="Tableau3.1">
							<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
								<text:p text:style-name="P7">Forme de prix</text:p>
							</table:table-cell>
							<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
								<text:p text:style-name="P7">Prix</text:p>
							</table:table-cell>
							</table:table-row>';
        if ($tagFin) {
            $arrayDonnesComp .= '</table:table>';
        }

        return $arrayDonnesComp;
    }

    /*
     * Fonction permet d'ajouter une ligne avec le numero de lot dans les tableau des lots
     *  dans le cas alloti avec tranches
     */
    public function insertLigneLot($numLot, $intituleLot)
    {
        $tableau = '<table:table-row table:style-name="Tableau3.1">
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">
							'.$numLot.'
						</text:p>
					</table:table-cell>
					<table:table-cell table:style-name="Tableau3.A1" table:number-columns-spanned="3" office:value-type="string">
						<text:p text:style-name="P7">
							'.$intituleLot.'
						</text:p>
					</table:table-cell>
					<table:covered-table-cell/>
					<table:covered-table-cell/>
					</table:table-row>';

        return $tableau;
    }

    /*
    * Fonction permet d'ajouter une ligne avec le numero de tranche dans les tableau des lots
    *  dans le cas alloti avec tranches
    */
    public function insertLignelotAvecTranche($identifiantTranche, $intituleTranche, $arrayDonnee, $arrayTypePrix)
    {
        $tableau = null;
        $tableau .= '<table:table-row table:style-name="Tableau3.1">
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">
						'.$identifiantTranche.'
						</text:p>
					</table:table-cell>
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">
							'.$intituleTranche.'
						</text:p>
					</table:table-cell>
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						<text:p text:style-name="P7">
							'.$arrayDonnee.'
						</text:p>
					</table:table-cell>
					<table:table-cell table:style-name="Tableau3.A1" office:value-type="string">
						'.$arrayTypePrix.'
					</table:table-cell>
					</table:table-row>';

        return $tableau;
    }

    /**
     * @param int $idContrat identifiant du contrat
     *
     * @return string
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11
     *
     * @copyright Atexo 2015
     */
    public function generateFicheRessencementOdt($contrat)
    {
        $siren = [];
        $siret = [];
        $cpv1 = [];
        $cpv2 = null;
        $codeRecensementpeProc = [];
        $nbrOffresElec = [];
        $acronymeOrg = null;
        $sirenAp = [];
        $complementAp = [];
        $impr = new Atexo_GenerationFichier_ImpressionODT();
        $tableKeys = [];
        $dateNotification = $montant = $dateAchevement = $idOffre = $numMarche = $nbrOffres = $consultationId = '';
        $clauseSoc = '';
        $clauseEnv = '';
        $serviceId = '';

        if ($contrat instanceof CommonTContratTitulaire) {
            $acronymeOrg = $contrat->getOrganisme();
            $serviceId = $contrat->getServiceId();
            $donneesConsultations = (array) $contrat->getCommonTDonneesConsultations();
            $donneesConsultation = $donneesConsultations[0];

            if ($donneesConsultation instanceof CommonTDonneesConsultation) {
                $codeRecensementpeProc = (new Atexo_Consultation_ProcedureType())->retrieveCodeRecensementProcedureById($donneesConsultation->getIdTypeProcedure(), $contrat->getOrganisme());
                $nbrOffresElec = (string) $donneesConsultation->getNbreOffresDematerialisees();
                $nbrOffres = (string) $donneesConsultation->getNbreOffresRecues();
                $consultationId = $donneesConsultation->getReferenceConsultation();
            }
            if (Atexo_Module::isEnabled('OrganisationCentralisee', $contrat->getOrganisme()) || '0' == $contrat->getServiceId()) {
                $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($contrat->getOrganisme());
                $sirenAp = $organismeO->getSiren();
                $complementAp = $organismeO->getComplement();
            } else {
                $serviceObj = Atexo_EntityPurchase::retrieveEntityById($contrat->getServiceId(), $contrat->getOrganisme());
                if ($serviceObj instanceof CommonService) {
                    $sirenAp = $serviceObj->getSiren();
                    $complementAp = $serviceObj->getComplement();
                }
            }
            $dateNotification = $contrat->getDateNotification('%Y-%m-%d');
            $montant = str_replace(' ', '', str_replace(',', '.', $contrat->getMontantContrat()));
            $dateAchevement = $contrat->getDateFinContrat();
            $idOffre = $contrat->getIdOffre();
            $numMarche = $contrat->getNumeroContrat();
            if ($contrat->getTitulaireContrat()) {
                $siren = $contrat->getSirenTitulaireContrat();
                $siret = $contrat->getNicTitulaireContrat();
            } elseif ($contrat->getTypeDepotReponse() == Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE')) {
                $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($idOffre, $acronymeOrg);
                if ($offre instanceof CommonOffres) {
                    $entreprise = (new Atexo_Entreprise())->getEntrepriseByIdOffre($idOffre, $consultationId);
                    if ($entreprise) {
                        $siren = $entreprise->getSiren();
                        $siret = $entreprise->getNicSiege();
                    }
                }
            } else {
                $registreVO = (new Atexo_Consultation_Register())->retrieveOffrePapier($idOffre, [], $acronymeOrg, false);
                $registreVO = array_shift($registreVO);
                if ($registreVO instanceof Atexo_Consultation_RegistreVo) {
                    $siren = $registreVO->getSirenEntreprise();
                    $siret = $registreVO->getNicEntreprise();
                }
            }
            $cpv1 = $contrat->getCodeCpv1();
            $cpv2 = $contrat->getCodeCpv2();
            if (Atexo_Module::isEnabled('ConsultationClause')) {
                if ($contrat->getClauseSociale() == Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER')) {
                    $clauseSoc = 'X';
                }
                if ($contrat->getClauseEnvironnementale() == Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_ACTIVER')
                    && ($contrat->getClauseEnvSpecsTechniques() || $contrat->getClauseEnvCondExecution())) {
                    $clauseEnv = 'X';
                }
            }
        }

        //Siren titulaire/Mandataire
        $tableKeys['SiretCase1'] = $siren[0];
        $tableKeys['SiretCase2'] = $siren[1];
        $tableKeys['SiretCase3'] = $siren[2];
        $tableKeys['SiretCase4'] = $siren[3];
        $tableKeys['SiretCase5'] = $siren[4];
        $tableKeys['SiretCase6'] = $siren[5];
        $tableKeys['SiretCase7'] = $siren[6];
        $tableKeys['SiretCase8'] = $siren[7];
        $tableKeys['SiretCase9'] = $siren[8];
        //Nic titulaire/Mandataire
        $tableKeys['NicCase1'] = $siret[0];
        $tableKeys['NicCase2'] = $siret[1];
        $tableKeys['NicCase3'] = $siret[2];
        $tableKeys['NicCase4'] = $siret[3];
        $tableKeys['NicCase5'] = $siret[4];

        $dureeContrat = str_pad(ceil(Atexo_Util::diffMois($dateNotification, $dateAchevement)), 3, '0', STR_PAD_LEFT);
        $tableKeys['Duree1'] = $dureeContrat[0];
        $tableKeys['Duree2'] = $dureeContrat[1];
        $tableKeys['Duree3'] = $dureeContrat[2];

        $tableKeys['MontantCase1'] = ($montant / 10_000_000_000) % 10;
        $tableKeys['MontantCase2'] = ($montant / 1_000_000_000) % 10;
        $tableKeys['MontantCase3'] = ($montant / 100_000_000) % 10;
        $tableKeys['MontantCase4'] = ($montant / 10_000_000) % 10;
        $tableKeys['MontantCase5'] = ($montant / 1_000_000) % 10;
        $tableKeys['MontantCase6'] = ($montant / 100000) % 10;
        $tableKeys['MontantCase7'] = ($montant / 10000) % 10;
        $tableKeys['MontantCase8'] = ($montant / 1000) % 10;
        $tableKeys['MontantCase9'] = ($montant / 100) % 10;
        $tableKeys['MontantC1'] = ($montant / 10) % 10;
        $tableKeys['MontantC2'] = $montant % 10;

        $charControleCpv = Atexo_Util::returnValueFromCsv(Atexo_Config::getParameter('PATH_FILE_CPV_CONTROLE'), $cpv1);
        //<!-- Module referentiel CPV
        if ($cpv1) {
            $tableKeys['CpvCase1'] = $cpv1[0];
            $tableKeys['CpvCase2'] = $cpv1[1];
            $tableKeys['CpvCase3'] = $cpv1[2];
            $tableKeys['CpvCase4'] = $cpv1[3];
            $tableKeys['CpvCase5'] = $cpv1[4];
            $tableKeys['CpvCase6'] = $cpv1[5];
            $tableKeys['CpvCase7'] = $cpv1[6];
            $tableKeys['CpvCase8'] = $cpv1[7];
            $tableKeys['CpvCase0'] = $charControleCpv;
        } else {
            $tableKeys['CpvCase1'] = '';
            $tableKeys['CpvCase2'] = '';
            $tableKeys['CpvCase3'] = '';
            $tableKeys['CpvCase4'] = '';
            $tableKeys['CpvCase5'] = '';
            $tableKeys['CpvCase6'] = '';
            $tableKeys['CpvCase7'] = '';
            $tableKeys['CpvCase8'] = '';
            $tableKeys['CpvCase0'] = '';
        }

        //traitement des CPVs secondaires

        if ($cpv2) {
            $codesCpvSec = explode('#', $cpv2);
            if ($codesCpvSec && is_array($codesCpvSec)) {
                //on est dans cette partie si au moins un CPV secondaire
                $i = 0;
                foreach ($codesCpvSec as $codeCpv) {
                    if ($codeCpv) {
                        $charControleCpv2 = Atexo_Util::returnValueFromCsv(Atexo_Config::getParameter('PATH_FILE_CPV_CONTROLE'), $codeCpv);
                        $tableKeys['Cpv'.$i.'Case1'] = $codeCpv[0];
                        $tableKeys['Cpv'.$i.'Case2'] = $codeCpv[1];
                        $tableKeys['Cpv'.$i.'Case3'] = $codeCpv[2];
                        $tableKeys['Cpv'.$i.'Case4'] = $codeCpv[3];
                        $tableKeys['Cpv'.$i.'Case5'] = $codeCpv[4];
                        $tableKeys['Cpv'.$i.'Case6'] = $codeCpv[5];
                        $tableKeys['Cpv'.$i.'Case7'] = $codeCpv[6];
                        $tableKeys['Cpv'.$i.'Case8'] = $codeCpv[7];
                        $tableKeys['Cpv'.$i.'Case0'] = $charControleCpv2;
                    }
                    ++$i;
                }
                //? la fin il faut mettre ? vide ceux qui n'ont pas ?t? remplis
                for ($j = 3; $i <= $j; --$j) {
                    $tableKeys['Cpv'.$j.'Case1'] = '';
                    $tableKeys['Cpv'.$j.'Case2'] = '';
                    $tableKeys['Cpv'.$j.'Case3'] = '';
                    $tableKeys['Cpv'.$j.'Case4'] = '';
                    $tableKeys['Cpv'.$j.'Case5'] = '';
                    $tableKeys['Cpv'.$j.'Case6'] = '';
                    $tableKeys['Cpv'.$j.'Case7'] = '';
                    $tableKeys['Cpv'.$j.'Case8'] = '';
                    $tableKeys['Cpv'.$j.'Case0'] = '';
                }
            }
        }
        //si aucun CPV secondaire alors tout est ? vide
        else {
            for ($i = 1; $i < 4; ++$i) {
                $tableKeys['Cpv'.$i.'Case1'] = '';
                $tableKeys['Cpv'.$i.'Case2'] = '';
                $tableKeys['Cpv'.$i.'Case3'] = '';
                $tableKeys['Cpv'.$i.'Case4'] = '';
                $tableKeys['Cpv'.$i.'Case5'] = '';
                $tableKeys['Cpv'.$i.'Case6'] = '';
                $tableKeys['Cpv'.$i.'Case7'] = '';
                $tableKeys['Cpv'.$i.'Case8'] = '';
                $tableKeys['Cpv'.$i.'Case0'] = '';
            }
        }

        $tableKeys['Mois_Case_1'] = $dateNotification[5];
        $tableKeys['Mois_Case_2'] = $dateNotification[6];

        $tableKeys['AnneeCase1'] = $dateNotification[0];
        $tableKeys['AnneeCase2'] = $dateNotification[1];
        $tableKeys['AnneeCase3'] = $dateNotification[2];
        $tableKeys['AnneeCase4'] = $dateNotification[3];

        $tableKeys['ProcedureCase1'] = $codeRecensementpeProc[0];
        $tableKeys['ProcedureCase2'] = $codeRecensementpeProc[1];

        //Total des offres re?ues
        if ($nbrOffres < 10) {
            $tableKeys['NbrTRCase1'] = '0';
            $tableKeys['NbrTRCase2'] = '0';
            $tableKeys['NbrTRCase3'] = $nbrOffres;
        } elseif ($nbrOffres < 100) {
            $tableKeys['NbrTRCase1'] = '0';
            $tableKeys['NbrTRCase2'] = $nbrOffres[0];
            $tableKeys['NbrTRCase3'] = $nbrOffres[1];
        } elseif ($nbrOffres > 100) {
            $tableKeys['NbrTRCase1'] = $nbrOffres[0];
            $tableKeys['NbrTRCase2'] = $nbrOffres[1];
            $tableKeys['NbrTRCase3'] = $nbrOffres[2];
        }

        //Total des offres re?ues d?mat?rialis?es (?l?ctronique)
        if ($nbrOffresElec < 10) {
            $tableKeys['NbrTDCase1'] = '0';
            $tableKeys['NbrTDCase2'] = '0';
            $tableKeys['NbrTDCase3'] = $nbrOffresElec;
        } elseif ($nbrOffresElec < 100) {
            $tableKeys['NbrTDCase1'] = '0';
            $tableKeys['NbrTDCase2'] = $nbrOffresElec[0];
            $tableKeys['NbrTDCase3'] = $nbrOffresElec[1];
        } elseif ($nbrOffresElec > 100) {
            $tableKeys['NbrTDCase1'] = $nbrOffresElec[0];
            $tableKeys['NbrTDCase2'] = $nbrOffresElec[1];
            $tableKeys['NbrTDCase3'] = $nbrOffresElec[2];
        }

        $tableKeys['ClauseSociale'] = $clauseSoc;
        $tableKeys['ClauseEnv'] = $clauseEnv;

        //ancien affichage (!identification_contrat)
        $tableKeys['Annee1'] = '2';
        $tableKeys['Annee2'] = '0';
        $tableKeys['Annee3'] = '';
        $tableKeys['Annee4'] = '';
        $tableKeys['Interne1'] = '';
        $tableKeys['Interne2'] = '';
        $tableKeys['Interne3'] = '';
        $tableKeys['Interne4'] = '';
        $tableKeys['Interne5'] = '';
        $tableKeys['Interne6'] = '';
        $tableKeys['Interne7'] = '';
        $tableKeys['Interne8'] = '';
        $tableKeys['Interne9'] = '';
        $tableKeys['Interne0'] = '';
        $tableKeys['Avenant1'] = '';
        $tableKeys['Avenant2'] = '';
        $tableKeys['AS1'] = '';
        $tableKeys['AS2'] = '';
        if (!(new Atexo_Module())->isChorusAccessActivate($serviceId)) {
            if (Atexo_Module::isEnabled('IdentificationContrat', $acronymeOrg)) {
                if ($numMarche) {
                    $tableKeys['Annee1'] = $numMarche[0];
                    $tableKeys['Annee2'] = $numMarche[1];
                    $tableKeys['Annee3'] = $numMarche[2];
                    $tableKeys['Annee4'] = $numMarche[3];
                    $tableKeys['Interne1'] = $numMarche[4];
                    $tableKeys['Interne2'] = $numMarche[5];
                    $tableKeys['Interne3'] = $numMarche[6];
                    $tableKeys['Interne4'] = $numMarche[7];
                    $tableKeys['Interne5'] = $numMarche[8];
                    $tableKeys['Interne6'] = $numMarche[9];
                    $tableKeys['Interne7'] = $numMarche[10];
                    $tableKeys['Interne8'] = $numMarche[11];
                    $tableKeys['Interne9'] = $numMarche[12];
                    $tableKeys['Interne0'] = $numMarche[13];
                    $tableKeys['Avenant1'] = $numMarche[14];
                    $tableKeys['Avenant2'] = $numMarche[15];
                    $tableKeys['AS1'] = $numMarche[16];
                    $tableKeys['AS2'] = $numMarche[17];
                }
            }
        }

        //Siren
        $tableKeys['sirenAPCase1'] = $sirenAp[0];
        $tableKeys['sirenAPCase2'] = $sirenAp[1];
        $tableKeys['sirenAPCase3'] = $sirenAp[2];
        $tableKeys['sirenAPCase4'] = $sirenAp[3];
        $tableKeys['sirenAPCase5'] = $sirenAp[4];
        $tableKeys['sirenAPCase6'] = $sirenAp[5];
        $tableKeys['sirenAPCase7'] = $sirenAp[6];
        $tableKeys['sirenAPCase8'] = $sirenAp[7];
        $tableKeys['sirenAPCase9'] = $sirenAp[8];
        //Nic
        $tableKeys['nicAPCase1'] = $complementAp[0];
        $tableKeys['nicAPCase2'] = $complementAp[1];
        $tableKeys['nicAPCase3'] = $complementAp[2];
        $tableKeys['nicAPCase4'] = $complementAp[3];
        $tableKeys['nicAPCase5'] = $complementAp[4];

        $modelePath = './ressources/consultation/Fiche_recensement_PMI.zip';
        $odtFilePath = $impr->generateOdtFile($tableKeys, $modelePath);
        if ($odtFilePath) {
            return $odtFilePath;
        }
    }
}
