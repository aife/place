<?php

namespace Application\Service\Atexo\GenerationFichier;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTCAOCommissionQuery;
use Application\Propel\Mpe\CommonTCAOSeanceInvite;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Odf;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Commission\Atexo_Commission_Seance;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;

/**
 * Classe de generation des documents du module externe CAO.
 *
 * @author Ameline PLACIDE <ameline.placide@atexo.com>
 * @copyright Atexo 2014
 *
 * @since MPE4
 */
class Atexo_GenerationFichier_GenerationDocumentsCAO
{
    /**
     * Permet de generer les PV individuels des consultations a l'ordre du jour d'une seance de commission.
     *
     * @param Objet   $seance         : l'objet seance
     * @param varchar $modele         : le chemin du modele de document
     * @param varchar $directory      : le repertoire temporaire dans lequel doit etre genere le document
     * @param bool    $toFile         : permet de specifier s'il faut exporter en fichier ou retourner le chemin du document genere
     * @param Objet   $ordreDePassage :  Objet OrdreDePassage correspondant a la consultation courante a l'ordre du jour de la seance
     */
    public function generationPVConsultation($seance, $directory, $modele, $toFile, $ordreDePassage, $genererLogoEntete = false)
    {
        $langue = null;
        //Creation du repertoire si il n'existe pas deja
        if (!is_dir($directory)) {
            mkdir($directory, 0755, true);
        }

        $config = [
                'ZIP_PROXY' => \PhpZipProxy::class,
                'DELIMITER_LEFT' => '{',
                'DELIMITER_RIGHT' => '}',
                'PATH_TO_TMP' => $directory,
        ];

        //Instanciation du nouveau document a creer
        $odf = new Atexo_Odf($modele, $config);

        /** Identification du marche **/

        //Recuperation des informations sur l'entite et le service
        $acronyme = Atexo_CurrentUser::getCurrentOrganism();
        $service_id = Atexo_CurrentUser::getCurrentServiceId();
        $libelle_entite = '';
        $ville_entite = '';
        if (empty($service_id)) {
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronyme);
            $libelle_entite = $organisme->getDenominationOrg();
            $ville_entite = $organisme->getVille();
        } else {
            $libelle_entite = Atexo_EntityPurchase::getLibelleServiceAgentById($service_id, $acronyme);
            $service = Atexo_EntityPurchase::retrieveEntityById($service_id, $acronyme);
            if ($service instanceof CommonService) {
                $ville_entite = $service->getVille();
            }
        }

        //Ajout des informations sur l'entite et le service au document
        $odf->setVars('ENTITE_PUBLIQUE', $libelle_entite, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('SERVICE', $libelle_entite, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('LIEU_PV', $ville_entite, true, Atexo_Config::getParameter('HTTP_ENCODING'));

        //Recuperation des informations sur le marche

        $commission_consultation = $ordreDePassage->getCommonTCAOCommissionConsultation();

        //Recuperation de la Commission
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $caoCommission = CommonTCAOCommissionQuery::create()->findPk([$commission_consultation->getIdCommission(), $acronyme], $connexion);

        if ($commission_consultation->getConsultationId()) {
            $reference = $commission_consultation->getConsultationId();
        } else {
            $reference = $commission_consultation->getRefLibre();
        }

        $_t_types_procedures = Atexo_Consultation_ProcedureType::retrieveProcedureType(false, false, $acronyme, $langue);
        $type_procedure = $_t_types_procedures[$commission_consultation->getIdTypeProcedure()];

        $objet = $commission_consultation->getIntitule();
        $date_fin = Atexo_Util::iso2frnDateTime($commission_consultation->getDateCloture());

        //Ajout des informations sur le marche au document
        $odf->setVars('REF_CONS', $reference, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('TYPE_PROC_CONS', $type_procedure, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('OBJET_CONS', $objet, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('INTITULE_CONS', $objet, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('DATE_FIN', $date_fin, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('TYPE_COM', $caoCommission->getIntitule(), true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('DATE_DEBUT', Atexo_Util::iso2frnDateTime($commission_consultation->getDateMiseEnLigne()), true, Atexo_Config::getParameter('HTTP_ENCODING'));

        //Recuperation des informations generales sur la commission
        $date_passage = substr(Atexo_Util::iso2frnDateTime($seance->getDate()), 0, 10);

        //Ajout des informations generales sur la commission au document
        $odf->setVars('DATE_COMMISSION', $date_passage, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('DATE_PV', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));

        /** Membres de la commission **/

        //Debut liste des membres de la commission
        $t_membres = (new Atexo_Commission_Seance())->getAllMembresOrdreDePassage($seance->getIdSeance(), $ordreDePassage->getIdOrdreDePassage(), $acronyme);
        $_t_types_voix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));

        //Fin liste des membres de la commission
        $membres = $odf->setSegment('membres');

        if (count($t_membres)) {
            foreach ($t_membres as $membre) {
                if ($membre) {
                    //Recuperation du libelle du type de voix
                    $type_voix = $_t_types_voix[$membre->getIdRefValTypeVoix()];
                    //Recuperation du nom et du prenom de l'invite
                    $nom = $membre->getNom();
                    $prenom = $membre->getPrenom();

                    //Creation de la ligne dans le segment
                    $membres->setVars('nom', $nom, Atexo_Config::getParameter('HTTP_ENCODING'));
                    $membres->setVars('prenom', $prenom, Atexo_Config::getParameter('HTTP_ENCODING'));
                    $membres->setVars('type_voix', $type_voix, Atexo_Config::getParameter('HTTP_ENCODING'));

                    $membres->merge();
                }
            }
        } else {
            $membres->setVars('nom', '');
            $membres->setVars('prenom', '');
            $membres->setVars('type_voix', '');

            $membres->merge();
        }

        //Ajout du segement au document
        $odf->mergeSegment($membres);

        if ($toFile) {
            $pathFile = $directory.'/'.Atexo_Config::getParameter('CAO_NOM_FICHIER_PV_CONSULTATION').'_'.Atexo_Util::clean($reference).Atexo_Config::getParameter('CAO_EXTENSION_ODT');
            $odf->saveToDisk($pathFile);

            return $pathFile;
        } else {
            $odf->exportAsAttachedFile('test.odt');
            exit;
        }
    }

    /**
     * Permet de generer les convocations des seances de commissions.
     *
     * @param $invite: agent ou intervenant invite a la seance ou a une procedure de l'ordre du jour
     * @param $directory : chemin du repertoire dans lequel generer la convocation
     * @param varchar $modele : le chemin du modele de document
     * @param bool    $toFile : permet de specifier s'il faut exporter en fichier ou retourner le chemin du document genere
     * @param varchar $lang   :  la langue de navigation
     */
    public function generationConvocSeance($invite, $directory, $modele, $toFile, $lang, $genererLogoEntete = false)
    {
        //Creation du repertoire si il n'existe pas deja
        if (!is_dir($directory)) {
            mkdir($directory, 0755, true);
        }

        $config = [
                    'ZIP_PROXY' => \PhpZipProxy::class,
                    'DELIMITER_LEFT' => '{',
                    'DELIMITER_RIGHT' => '}',
                    'PATH_TO_TMP' => $directory,
                    ];

        //Instanciation du nouveau document a creer
        $odf = new Atexo_Odf($modele, $config);

        //Recuperation du nom de l'invite
        $id_civilite = $invite->getCivilite();
        if ($id_civilite == Atexo_Config::getParameter('CIVILITE_MONSIEUR')) {
            $civilite = Prado::localize('TEXT_MONSIEUR');
        } elseif ($id_civilite == Atexo_Config::getParameter('CIVILITE_MADAME')) {
            $civilite = Prado::localize('TEXT_MADAME');
        } else {
            $civilite = Prado::localize('TEXT_MADAME').', '.Prado::localize('TEXT_MONSIEUR');
        }
        $odf->setVars('NOM', $invite->getNom(), true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('PRENOM', $invite->getPrenom(), true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('CIVILITE', $civilite, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        /*
        //Recuperation des coordonnes de l'invite (non utilise)
        $numTel = '';
        $numFax = '';
        if($invite instanceof CommonAgent) {
            $numTel = $invite->getNumTel();
            $numFax = $invite->getNumFax();
        }
        $email = ($invite->getEmail()<> '') ? $invite->getEmail() : '';
        */

        //Recuperation des informations sur le lieu et l'horaire de la seance
        if ($invite instanceof CommonTCAOSeanceInvite) {
            //Recuperation des  informations sur la procedure en cours
            $ordreDePassage = $invite->getCommonTCAOOrdreDePassage();
            $seance = $ordreDePassage->getCommonTCAOSeanceRelatedByIdSeance();
            $date = $ordreDePassage->getDatePassage();
        } else {
            //Recuperation des informations sur la seance
            $seance = $invite->getCommonTCAOSeance();
            $date = $seance->getDate();
        }

        $odf->setVars('SALLE', $seance->getSalle(), true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('LIEU', $seance->getLieu(), true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('DATE_GENERATION', date('d/m/Y'), true);
        $odf->setVars('DATE_SEANCE', substr(Atexo_Util::iso2frnDateTime($date), 0, 10), true);
        $odf->setVars('HEURE_DEBUT', substr(Atexo_Util::iso2frnDateTime($date), 10, 6), true);
        $odf->setVars('NOM_PRESIDENT_CAO', '', true);

        //Recuperation de l'agent courant envoyant la convocation
        $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());
        $nomAgentGenerateur = ('' != $agent->getNom()) ? $agent->getNom() : '';
        $prenomAgentGenerateur = ('' != $agent->getPrenom()) ? $agent->getPrenom() : '';
        $numTelAgentGenerateur = ('' != $agent->getNumTel()) ? $agent->getNumTel() : '';
        $numFaxAgentGenerateur = ('' != $agent->getNumFax()) ? $agent->getNumFax() : '';
        $emailAgentGenerateur = ('' != $agent->getEmail()) ? $agent->getEmail() : '';

        $odf->setVars('PRENOM_AGENT', $prenomAgentGenerateur, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('NOM_AGENT', $nomAgentGenerateur, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('TELEPHONE_AGENT', $numTelAgentGenerateur, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('TELECOPIEUR_AGENT', $numFaxAgentGenerateur, true, Atexo_Config::getParameter('HTTP_ENCODING'));
        $odf->setVars('EMAIL_AGENT', $emailAgentGenerateur, true, Atexo_Config::getParameter('HTTP_ENCODING'));

        //Recuperation de la ville de l'organisme
        $objetOrganisme = Atexo_Organismes::retrieveOrganismeByAcronyme($agent->getOrganisme());
        $villeOrg = '';
        if ($objetOrganisme instanceof CommonOrganisme) {
            $villeOrg = $objetOrganisme->getVille();
        }
        $odf->setVars('VILLE_ORGANISME', $villeOrg, true, Atexo_Config::getParameter('HTTP_ENCODING'));

        //Recuperation du type de commission
        $type = $seance->getCommonTCAOCommission()->getIntitule();
        $odf->setVars('TYPE', $type);

        /*
        //Insertion d'image
        if($genererLogoEntete)
        {
            $odf->setImage('imageEntete', self::getImageEnteteDocsPv());
        }
        else
        {
            $odf->setImage('imageEntete', '');
        }
        */

        if ($toFile) {
            $pathFile = $directory.'/'.Atexo_Config::getParameter('CAO_NOM_FICHIER_CONVOCATION').'_'.$invite->getNom().'_'.$invite->getPrenom().Atexo_Config::getParameter('CAO_EXTENSION_ODT');
            $odf->saveToDisk($pathFile);

            return $pathFile;
        } else {
            $odf->exportAsAttachedFile('test.odt');
            exit;
        }
    }

    public function getImageEnteteDocsPv()
    {
        return Atexo_Controller_Front::t().'/images/entetePvCommission.gif';
    }
}
