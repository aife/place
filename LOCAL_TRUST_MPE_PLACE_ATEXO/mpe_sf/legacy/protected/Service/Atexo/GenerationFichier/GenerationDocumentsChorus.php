<?php

namespace Application\Service\Atexo\GenerationFichier;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusFicheNavette;
use Application\Propel\Mpe\CommonChorusGroupementAchatQuery;
use Application\Propel\Mpe\CommonChorusOrganisationAchatQuery;
use Application\Propel\Mpe\CommonChorusRegroupementComptableQuery;
use Application\Propel\Mpe\CommonChorusTypeGroupementQuery;
use Application\Propel\Mpe\CommonChorusTypeMarcheQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Odf;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Exception;
use Prado\Prado;

/**
 * Classe de génération des documents de l'interface Place - Chorus.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @since MPE4
 */
class Atexo_GenerationFichier_GenerationDocumentsChorus
{
    /**
     * Permet de générer la fiche navette de l'inferface Place - Chorus.
     *
     * @param CommonChorusFicheNavette $ficheNavette : l'objet fiche navette
     *                                               $consultation : l'objet consultation
     * @param varchar                  $modele       : le chemin du modèle de document
     * @param bool                     $toFile       : permet de spécifier s'il faut exporter en fichier ou retourner le chemin du document généré
     * @param varchar                  $lang         :  la langue de navigation
     */
    public function generationFicheNavette($ficheNavette, $contrat, $modele, $toFile = false, $lang = null)
    {
        if (!$lang) {
            $lang = Atexo_Languages::readLanguageFromSession();
        }
        $fileModel = $modele.$lang.'.odt';
        if ($ficheNavette instanceof CommonChorusFicheNavette) {
            if (is_file($fileModel)) {//verifier si le fichier existe pour cette langue.
                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt');
                }
                $config = [
                                'ZIP_PROXY' => \PhpZipProxy::class,
                                'DELIMITER_LEFT' => '{',
                                'DELIMITER_RIGHT' => '}',
                                'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt',
                                ];
                $odf = new Atexo_Odf($fileModel, $config);

                //Représentant du pouvoir adjudicateur
                if ($ficheNavette->getRpa()) {
                    $odf->setVars('RPA', $ficheNavette->getRpa(), true);
                } else {
                    $odf->setVars('RPA', '-', true);
                }
                //Nom de l'acheteur à l'origine de l'envoi
                if ($ficheNavette->getNomAcheteur()) {
                    $odf->setVars('NOM_ACHETEUR', $ficheNavette->getNomAcheteur(), true);
                } else {
                    $odf->setVars('NOM_ACHETEUR', '-', true);
                }
                //Intitulé du marché
                if ($ficheNavette->getIntituleMarche()) {
                    $odf->setVars('INTITULE_MARCHE', $ficheNavette->getIntituleMarche(), true);
                } else {
                    $odf->setVars('INTITULE_MARCHE', '-', true);
                }
                //Objet du marché
                if ($ficheNavette->getObjet()) {
                    $odf->setVars('OBJET', $ficheNavette->getObjet(), true);
                } else {
                    $odf->setVars('OBJET', '-', true);
                }
                //Raison sociale du tiers
                if ($ficheNavette->getRaisonSociale()) {
                    $odf->setVars('RAISON_SOCIALE', $ficheNavette->getRaisonSociale(), true);
                } else {
                    $odf->setVars('RAISON_SOCIALE', '-', true);
                }
                //Fiche immobilisation
                if ('1' == $ficheNavette->getFicheImmobilisation()) {
                    $odf->setVars('FICHE_IMMOBILISATION', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getFicheImmobilisation()) {
                    $odf->setVars('FICHE_IMMOBILISATION', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('FICHE_IMMOBILISATION', Prado::localize('NON_RENSEIGNE'), true);
                }
                //En attente de PJ
                if ('1' == $ficheNavette->getAttentePj()) {
                    $odf->setVars('EN_ATTENTE_PJ', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getAttentePj()) {
                    $odf->setVars('EN_ATTENTE_PJ', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('EN_ATTENTE_PJ', Prado::localize('NON_RENSEIGNE'), true);
                }
                //Marché partagé
                if ('1' == $ficheNavette->getMarchePartage()) {
                    $odf->setVars('MARCHE_PARTAGE', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getMarchePartage()) {
                    $odf->setVars('MARCHE_PARTAGE', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('MARCHE_PARTAGE', Prado::localize('NON_RENSEIGNE'), true);
                }
                //Identifiant accord cadre
                if ($ficheNavette->getIdAccordCadre()) {
                    $odf->setVars('IDENTIFIANT_ACCORD_CADRE', $ficheNavette->getIdAccordCadre(), true);
                } else {
                    $odf->setVars('IDENTIFIANT_ACCORD_CADRE', '-', true);
                }
                //Libellé accord cadre
                if ($ficheNavette->getLibAccordCadre()) {
                    $odf->setVars('LIBELLE_ACCORD_CADRE', $ficheNavette->getLibAccordCadre(), true);
                } else {
                    $odf->setVars('LIBELLE_ACCORD_CADRE', '-', true);
                }
                //Année de création de l'accord
                if ($ficheNavette->getAnneeCreationAccordCadre()) {
                    $odf->setVars('ANNEE_CREATION_ACCORD_CADRE', $ficheNavette->getAnneeCreationAccordCadre(), true);
                } else {
                    $odf->setVars('ANNEE_CREATION_ACCORD_CADRE', '-', true);
                }
                //Montant HT
                if ($ficheNavette->getMontantHt()) {
                    $odf->setVars('MONTANT_HT', $ficheNavette->getMontantHt(), true);
                } else {
                    $odf->setVars('MONTANT_HT', '-', true);
                }
                //Reconductible
                if ('1' == $ficheNavette->getReconductible()) {
                    $odf->setVars('RECONDUCTIBLE', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getReconductible()) {
                    $odf->setVars('RECONDUCTIBLE', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('RECONDUCTIBLE', Prado::localize('NON_RENSEIGNE'), true);
                }
                //Visa ACCF
                if ('1' == $ficheNavette->getVisaAccf()) {
                    $odf->setVars('VISA_ACCF', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getVisaAccf()) {
                    $odf->setVars('VISA_ACCF', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('VISA_ACCF', Prado::localize('NON_RENSEIGNE'), true);
                }
                //Visa préfet
                if ('1' == $ficheNavette->getVisaPrefet()) {
                    $odf->setVars('VISA_PREFET', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getVisaPrefet()) {
                    $odf->setVars('VISA_PREFET', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('VISA_PREFET', Prado::localize('NON_RENSEIGNE'), true);
                }
                //Retenue de garantie
                if ('1' == $ficheNavette->getRetenueGarantie()) {
                    $odf->setVars('RETENUE_GARANTIE', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getRetenueGarantie()) {
                    $odf->setVars('RETENUE_GARANTIE', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('RETENUE_GARANTIE', Prado::localize('NON_RENSEIGNE'), true);
                }
                //Montant mini
                if ($ficheNavette->getMontantMini()) {
                    $odf->setVars('MONTANT_MINI', $ficheNavette->getMontantMini(), true);
                } else {
                    $odf->setVars('MONTANT_MINI', '-', true);
                }
                //Montant maxi
                if ($ficheNavette->getMontantMaxi()) {
                    $odf->setVars('MONTANT_MAXI', $ficheNavette->getMontantMaxi(), true);
                } else {
                    $odf->setVars('MONTANT_MAXI', '-', true);
                }
                //Montant partie forfaitaire
                if ($ficheNavette->getMontantForfaitaire()) {
                    $odf->setVars('MONTANT_PARTIE_FORFAITAIRE', $ficheNavette->getMontantForfaitaire(), true);
                } else {
                    $odf->setVars('MONTANT_PARTIE_FORFAITAIRE', '-', true);
                }
                //Montant partie à BC
                if ($ficheNavette->getMontantBc()) {
                    $odf->setVars('MONTANT_PARTIE_A_BC', $ficheNavette->getMontantBc(), true);
                } else {
                    $odf->setVars('MONTANT_PARTIE_A_BC', '-', true);
                }
                //Taux TVA
                if ($ficheNavette->getTauxTva()) {
                    $odf->setVars('TAUX_TVA', $ficheNavette->getTauxTva(), true);
                } else {
                    $odf->setVars('TAUX_TVA', '-', true);
                }
                //Centre de coût (si unique)
                if ($ficheNavette->getCentreCout()) {
                    $odf->setVars('CENTRE_COUT_SI_UNIQUE', $ficheNavette->getCentreCout(), true);
                } else {
                    $odf->setVars('CENTRE_COUT_SI_UNIQUE', '-', true);
                }
                //Centre financier (si unique)
                if ($ficheNavette->getCentreFinancier()) {
                    $odf->setVars('CENTRE_FINANCIER_SI_UNIQUE', $ficheNavette->getCentreFinancier(), true);
                } else {
                    $odf->setVars('CENTRE_FINANCIER_SI_UNIQUE', '-', true);
                }
                //Activité (si unique)
                if ($ficheNavette->getActivite()) {
                    $odf->setVars('ACTIVITE_SI_UNIQUE', $ficheNavette->getActivite(), true);
                } else {
                    $odf->setVars('ACTIVITE_SI_UNIQUE', '-', true);
                }
                //Domaine fonctionnel (si unique)
                if ($ficheNavette->getDomaineFonctionnel()) {
                    $odf->setVars('DOMAINE_FONCTIONNEL_SI_UNIQUE', $ficheNavette->getDomaineFonctionnel(), true);
                } else {
                    $odf->setVars('DOMAINE_FONCTIONNEL_SI_UNIQUE', '-', true);
                }
                //Fond (si unique)
                if ($ficheNavette->getFond()) {
                    $odf->setVars('FOND_SI_UNIQUE', $ficheNavette->getFond(), true);
                } else {
                    $odf->setVars('FOND_SI_UNIQUE', '-', true);
                }
                //Localisation interministérielle (si unique)
                if ($ficheNavette->getLocalisationInterministerielle()) {
                    $odf->setVars('LOCALISATION_INTERMINISTERIELLE_SI_UNIQUE', $ficheNavette->getLocalisationInterministerielle(), true);
                } else {
                    $odf->setVars('LOCALISATION_INTERMINISTERIELLE_SI_UNIQUE', '-', true);
                }
                //Nature (si unique)
                if ($ficheNavette->getNature()) {
                    $odf->setVars('NATURE_SI_UNIQUE', $ficheNavette->getNature(), true);
                } else {
                    $odf->setVars('NATURE_SI_UNIQUE', '-', true);
                }
                //Axe ministériel 1 (si unique)
                if ($ficheNavette->getAxeMinisteriel1()) {
                    $odf->setVars('AXE_MINISTERIEL1_SI_UNIQUE', $ficheNavette->getAxeMinisteriel1(), true);
                } else {
                    $odf->setVars('AXE_MINISTERIEL1_SI_UNIQUE', '-', true);
                }
                //Projet analytique (si unique)
                if ($ficheNavette->getProjetAnalytique()) {
                    $odf->setVars('PROJET_ANALYTIQUE_SI_UNIQUE', $ficheNavette->getProjetAnalytique(), true);
                } else {
                    $odf->setVars('PROJET_ANALYTIQUE_SI_UNIQUE', '-', true);
                }
                //Localisation ministérielle (si unique)
                if ($ficheNavette->getLocalisationMinisterielle()) {
                    $odf->setVars('LOCALISATION_MINISTERIELLE', $ficheNavette->getLocalisationMinisterielle(), true);
                } else {
                    $odf->setVars('LOCALISATION_MINISTERIELLE', '-', true);
                }
                //Axe ministériel 2 (si unique)
                if ($ficheNavette->getAxeMinisteriel2()) {
                    $odf->setVars('AXE_MINISTERIEL2_SI_UNIQUE', $ficheNavette->getAxeMinisteriel2(), true);
                } else {
                    $odf->setVars('AXE_MINISTERIEL2_SI_UNIQUE', '-', true);
                }
                //Remarques
                if ($ficheNavette->getRemarques()) {
                    $odf->setVars('REMARQUES', $ficheNavette->getRemarques(), true);
                } else {
                    $odf->setVars('REMARQUES', '-', true);
                }
                //Nom du fichier
                $nomFichier = '';
                if ($contrat instanceof CommonTContratTitulaire) {
                    $nomFichier = (new Atexo_Chorus_Echange())->getNomFicheNavette($contrat->getReferenceLibre()).'.pdf';
                } elseif (is_array($contrat) && !empty($contrat)) {
                    $nomFichier = (new Atexo_Chorus_Echange())->getNomFicheNavette($contrat['referenceLibre']).'.pdf';
                }
                if ($nomFichier) {
                    $odf->setVars('nom_ficher', $nomFichier, true);
                } else {
                    $odf->setVars('nom_ficher', '', true);
                }
                //Génération du fichier
                if ($toFile) {
                    $pathFile = Atexo_Config::getParameter('COMMON_TMP').'Fiche_Navette_'.$lang.'_'.session_id().time().'_fichier.odt';
                    $odf->saveToDisk($pathFile);

                    return $pathFile;
                } else {
                    $odf->exportAsAttachedFile(Prado::localize('FICHE_NAVETTE'));
                    exit;
                }
            } else {
                return -1;
            }
        }
    }

    /**
     * Permet de gerer le document de demande de suppression de l'Engagement Juridique.
     *
     * @return string|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public function genererDocDemandeSuppressionEj(CommonChorusFicheNavette $ficheNavette, CommonTContratTitulaire $contrat, CommonChorusEchange $chorusEchange)
    {
        try {
            if (is_file($fileModel = str_replace('lang', Atexo_Languages::readLanguageFromSession(), Atexo_Config::getParameter('MODELE_DEMANDE_SUPPRESSION_EJ_CHORUS')))) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt');
                }
                $config = [
                    'ZIP_PROXY' => \PhpZipProxy::class,
                    'DELIMITER_LEFT' => '{',
                    'DELIMITER_RIGHT' => '}',
                    'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt',
                ];
                $odf = new Atexo_Odf($fileModel, $config);

                $numeroEj = (new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($contrat->getNumeroContrat());

                //Organisation achat
                $organisationAchat = CommonChorusOrganisationAchatQuery::create()->filterById($chorusEchange->getIdOa())->filterByOrganisme($chorusEchange->getOrganisme())->findOne($connexion);
                if ($organisationAchat && !empty($organisationAchat->getLibelle())) {
                    $odf->setVars('ORGANISATION_ACHAT', $organisationAchat->getLibelle(), true);
                } else {
                    $odf->setVars('ORGANISATION_ACHAT', '-', true);
                }

                //Groupement acheteur
                $groupementAcheteur = CommonChorusGroupementAchatQuery::create()->filterById($chorusEchange->getIdGa())->filterByOrganisme($chorusEchange->getOrganisme())->findOne($connexion);
                if ($groupementAcheteur && !empty($groupementAcheteur->getLibelle())) {
                    $odf->setVars('GROUPEMENT_ACHETEUR', $groupementAcheteur->getLibelle(), true);
                } else {
                    $odf->setVars('GROUPEMENT_ACHETEUR', '-', true);
                }

                //Type marche
                $typeMarche = CommonChorusTypeMarcheQuery::create()->filterById($chorusEchange->getIdTypeMarche())->findOne($connexion);
                if ($typeMarche && !empty($typeMarche->getLibelle())) {
                    $odf->setVars('TYPE_MARCHE', $typeMarche->getLibelle(), true);
                } else {
                    $odf->setVars('TYPE_MARCHE', '-', true);
                }

                //Type de groupement
                $typeGroupement = CommonChorusTypeGroupementQuery::create()->filterByCode($chorusEchange->getIdTypeGroupement())->filterByOrganisme($chorusEchange->getOrganisme())->findOne($connexion);
                if ($typeGroupement && !empty($typeGroupement->getLibelle())) {
                    $odf->setVars('TYPE_GROUPEMENT', $typeGroupement->getLibelle(), true);
                } else {
                    $odf->setVars('TYPE_GROUPEMENT', '-', true);
                }

                //Regroupement comptable
                $regroupementComptable = CommonChorusRegroupementComptableQuery::create()->filterByCode($chorusEchange->getIdRegroupementComptable())->filterByOrganisme($chorusEchange->getOrganisme())->findOne($connexion);
                if ($regroupementComptable && !empty($regroupementComptable->getLibelle())) {
                    $odf->setVars('REGROUPEMENT_COMPTABLE', $regroupementComptable->getLibelle(), true);
                } else {
                    $odf->setVars('REGROUPEMENT_COMPTABLE', '-', true);
                }

                //Numero EJ
                if (!empty($numeroEj)) {
                    $odf->setVars('NUMERO_EJ', $numeroEj, true);
                } else {
                    $odf->setVars('NUMERO_EJ', '-', true);
                }

                //Représentant du pouvoir adjudicateur
                if ($ficheNavette->getRpa()) {
                    $odf->setVars('RPA', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getRpa()), true);
                } else {
                    $odf->setVars('RPA', '-', true);
                }

                //Objet du marché
                if ($ficheNavette->getObjet()) {
                    $odf->setVars('OBJET', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getObjet()), true);
                } else {
                    $odf->setVars('OBJET', '-', true);
                }

                //Fiche immobilisation
                if ('1' == $ficheNavette->getFicheImmobilisation()) {
                    $odf->setVars('FICHE_IMMOBILISATION', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getFicheImmobilisation()) {
                    $odf->setVars('FICHE_IMMOBILISATION', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('FICHE_IMMOBILISATION', Prado::localize('NON_RENSEIGNE'), true);
                }

                //En attente de PJ
                if ('1' == $ficheNavette->getAttentePj()) {
                    $odf->setVars('EN_ATTENTE_PJ', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getAttentePj()) {
                    $odf->setVars('EN_ATTENTE_PJ', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('EN_ATTENTE_PJ', Prado::localize('NON_RENSEIGNE'), true);
                }

                //Marché partagé
                if ('1' == $ficheNavette->getMarchePartage()) {
                    $odf->setVars('MARCHE_PARTAGE', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getMarchePartage()) {
                    $odf->setVars('MARCHE_PARTAGE', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('MARCHE_PARTAGE', Prado::localize('NON_RENSEIGNE'), true);
                }

                //Montant HT
                if ($ficheNavette->getMontantHt()) {
                    $odf->setVars('MONTANT_HT', $ficheNavette->getMontantHt(), true);
                } else {
                    $odf->setVars('MONTANT_HT', '-', true);
                }
                //Reconductible
                if ('1' == $ficheNavette->getReconductible()) {
                    $odf->setVars('RECONDUCTIBLE', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getReconductible()) {
                    $odf->setVars('RECONDUCTIBLE', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('RECONDUCTIBLE', Prado::localize('NON_RENSEIGNE'), true);
                }

                //Visa ACCF
                if ('1' == $ficheNavette->getVisaAccf()) {
                    $odf->setVars('VISA_ACCF', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getVisaAccf()) {
                    $odf->setVars('VISA_ACCF', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('VISA_ACCF', Prado::localize('NON_RENSEIGNE'), true);
                }

                //Visa préfet
                if ('1' == $ficheNavette->getVisaPrefet()) {
                    $odf->setVars('VISA_PREFET', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getVisaPrefet()) {
                    $odf->setVars('VISA_PREFET', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('VISA_PREFET', Prado::localize('NON_RENSEIGNE'), true);
                }

                //Retenue de garantie
                if ('1' == $ficheNavette->getRetenueGarantie()) {
                    $odf->setVars('RETENUE_GARANTIE', Prado::localize('DEFINE_OUI'), true);
                } elseif ('0' == $ficheNavette->getRetenueGarantie()) {
                    $odf->setVars('RETENUE_GARANTIE', Prado::localize('TEXT_NON'), true);
                } else {
                    $odf->setVars('RETENUE_GARANTIE', Prado::localize('NON_RENSEIGNE'), true);
                }

                //Montant mini
                if ($ficheNavette->getMontantMini()) {
                    $odf->setVars('MONTANT_MINI', $ficheNavette->getMontantMini(), true);
                } else {
                    $odf->setVars('MONTANT_MINI', '-', true);
                }
                //Montant maxi
                if ($ficheNavette->getMontantMaxi()) {
                    $odf->setVars('MONTANT_MAXI', $ficheNavette->getMontantMaxi(), true);
                } else {
                    $odf->setVars('MONTANT_MAXI', '-', true);
                }
                //Montant partie forfaitaire
                if ($ficheNavette->getMontantForfaitaire()) {
                    $odf->setVars('MONTANT_PARTIE_FORFAITAIRE', $ficheNavette->getMontantForfaitaire(), true);
                } else {
                    $odf->setVars('MONTANT_PARTIE_FORFAITAIRE', '-', true);
                }
                //Montant partie à BC
                if ($ficheNavette->getMontantBc()) {
                    $odf->setVars('MONTANT_PARTIE_A_BC', $ficheNavette->getMontantBc(), true);
                } else {
                    $odf->setVars('MONTANT_PARTIE_A_BC', '-', true);
                }
                //Taux TVA
                if ($ficheNavette->getTauxTva()) {
                    $odf->setVars('TAUX_TVA', $ficheNavette->getTauxTva(), true);
                } else {
                    $odf->setVars('TAUX_TVA', '-', true);
                }
                //Centre de coût (si unique)
                if ($ficheNavette->getCentreCout()) {
                    $odf->setVars('CENTRE_COUT_SI_UNIQUE', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getCentreCout()), true);
                } else {
                    $odf->setVars('CENTRE_COUT_SI_UNIQUE', '-', true);
                }
                //Centre financier (si unique)
                if ($ficheNavette->getCentreFinancier()) {
                    $odf->setVars('CENTRE_FINANCIER_SI_UNIQUE', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getCentreFinancier()), true);
                } else {
                    $odf->setVars('CENTRE_FINANCIER_SI_UNIQUE', '-', true);
                }
                //Activité (si unique)
                if ($ficheNavette->getActivite()) {
                    $odf->setVars('ACTIVITE_SI_UNIQUE', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getActivite()), true);
                } else {
                    $odf->setVars('ACTIVITE_SI_UNIQUE', '-', true);
                }
                //Domaine fonctionnel (si unique)
                if ($ficheNavette->getDomaineFonctionnel()) {
                    $odf->setVars('DOMAINE_FONCTIONNEL_SI_UNIQUE', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getDomaineFonctionnel()), true);
                } else {
                    $odf->setVars('DOMAINE_FONCTIONNEL_SI_UNIQUE', '-', true);
                }
                //Fond (si unique)
                if ($ficheNavette->getFond()) {
                    $odf->setVars('FOND_SI_UNIQUE', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getFond()), true);
                } else {
                    $odf->setVars('FOND_SI_UNIQUE', '-', true);
                }
                //Localisation interministérielle (si unique)
                if ($ficheNavette->getLocalisationInterministerielle()) {
                    $odf->setVars('LOCALISATION_INTERMINISTERIELLE_SI_UNIQUE', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getLocalisationInterministerielle()), true);
                } else {
                    $odf->setVars('LOCALISATION_INTERMINISTERIELLE_SI_UNIQUE', '-', true);
                }
                //Nature (si unique)
                if ($ficheNavette->getNature()) {
                    $odf->setVars('NATURE_SI_UNIQUE', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getNature()), true);
                } else {
                    $odf->setVars('NATURE_SI_UNIQUE', '-', true);
                }
                //Axe ministériel 1 (si unique)
                if ($ficheNavette->getAxeMinisteriel1()) {
                    $odf->setVars('AXE_MINISTERIEL1_SI_UNIQUE', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getAxeMinisteriel1()), true);
                } else {
                    $odf->setVars('AXE_MINISTERIEL1_SI_UNIQUE', '-', true);
                }
                //Projet analytique (si unique)
                if ($ficheNavette->getProjetAnalytique()) {
                    $odf->setVars('PROJET_ANALYTIQUE_SI_UNIQUE', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getProjetAnalytique()), true);
                } else {
                    $odf->setVars('PROJET_ANALYTIQUE_SI_UNIQUE', '-', true);
                }
                //Localisation ministérielle (si unique)
                if ($ficheNavette->getLocalisationMinisterielle()) {
                    $odf->setVars('LOCALISATION_MINISTERIELLE', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getLocalisationMinisterielle()), true);
                } else {
                    $odf->setVars('LOCALISATION_MINISTERIELLE', '-', true);
                }
                //Axe ministériel 2 (si unique)
                if ($ficheNavette->getAxeMinisteriel2()) {
                    $odf->setVars('AXE_MINISTERIEL2_SI_UNIQUE', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getAxeMinisteriel2()), true);
                } else {
                    $odf->setVars('AXE_MINISTERIEL2_SI_UNIQUE', '-', true);
                }
                //Remarques
                if ($ficheNavette->getRemarques()) {
                    $odf->setVars('REMARQUES', Atexo_Util::atexoHtmlEntitiesDecode($ficheNavette->getRemarques()), true);
                } else {
                    $odf->setVars('REMARQUES', '-', true);
                }

                //Nom PF et date generation
                $odf->setVars('NOM_PLATEFORME', Atexo_Config::getParameter('PF_SHORT_NAME'), true);
                $odf->setVars('DATE_GENERATION', date('d/m/Y H:i:s'), true);
                //Génération du fichier
                $pathFile = Atexo_Config::getParameter('COMMON_TMP').'Demande_Suppression_EJ_Numero_'.$numeroEj.session_id().uniqid(true).'.odt';
                $odf->saveToDisk($pathFile);

                return $pathFile;
            } else {
                $logger = Atexo_LoggerManager::getLogger('app');
                $logger->error('[idContrat= '.$contrat->getIdContratTitulaire().' | idEchangeChorus= '.$chorusEchange->getId().' | idFicheNavette= '.$ficheNavette->getId().']: Le modele de document de demande de suppression EJ n\'est pas defini.');
            }

            return null;
        } catch (Exception) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('[idContrat= '.$contrat->getIdContratTitulaire().' | idEchangeChorus= '.$chorusEchange->getId().' | idFicheNavette= '.$ficheNavette->getId().']: Erreur generation document de demande de suppression EJ. Numero EJ = '.(new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($contrat->getNumeroContrat()));
        }
    }
}
