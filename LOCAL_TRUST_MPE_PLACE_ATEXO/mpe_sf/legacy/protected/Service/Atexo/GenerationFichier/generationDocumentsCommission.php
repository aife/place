<?php

namespace Application\Service\Atexo\GenerationFichier;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentCommission;
use Application\Propel\Mpe\CommonCommission;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePapier;
use Application\Propel\Mpe\CommonIntervenantExterne;
use Application\Propel\Mpe\CommonIntervenantExterneCommission;
use Application\Propel\Mpe\CommonIntervenantOrdreDuJour;
use Application\Propel\Mpe\CommonOrdreDuJour;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Odf;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Commission\Atexo_Commission_Commissions;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Application\Service\Atexo\Commission\Atexo_Commission_Seance;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use Prado\Prado;

/**
 * Classe de génération des documents de la commission.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE4
 */
class Atexo_GenerationFichier_generationDocumentsCommission
{
    /**
     * Permet de générer les PV des Commission.
     *
     * @param Objet   $commission  : l'objet commission
     * @param varchar $modele      : le chemin du modèle de document
     * @param bool    $toFile      : permet de spécifier s'il faut exporter en fichier ou retourner le chemin du document généré
     * @param varchar $lang        :  la langue de navigation
     * @param Objet   $ordreDuJour :  Objet OrdreDuJour
     */
    public function generationPvCommission($commission, $modele, $toFile, $lang, $ordreDuJour, $genererLogoEntete = false)
    {
        $fileModel = $modele.$lang.'.odt';
        $encoding = Atexo_Config::getParameter('HTTP_ENCODING');
        if ($commission instanceof CommonCommission && $ordreDuJour instanceof CommonOrdreDuJour) {
            if (is_file($fileModel)) {//verifier si le fichier existe pour cette langue.
                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt');
                }
                $config = [
                                'ZIP_PROXY' => 'PhpZipProxy',
                                'DELIMITER_LEFT' => '{',
                                'DELIMITER_RIGHT' => '}',
                                'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt',
                                ];
                $odf = new Atexo_Odf($fileModel, $config);

                if (!$connexion) {
                    $connexion = $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                }

                $idService = Atexo_CurrentUser::getCurrentServiceId();
                $libelleEntite = '';
                $entiteVille = '';
                if (empty($idService)) {
                    $organismeObj = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
                    $libelleEntite = $organismeObj->getDenominationOrg();
                    $entiteVille = $organismeObj->getVille();
                } else {
                    $libelleEntite = Atexo_EntityPurchase::getLibelleServiceAgentById($idService, Atexo_CurrentUser::getCurrentOrganism());
                    $service = Atexo_EntityPurchase::retrieveEntityById($idService, Atexo_CurrentUser::getCurrentOrganism());
                    if ($service instanceof CommonService) {
                        $entiteVille = $service->getVille();
                    }
                }
                $odf->setVars('ENTITE_PUBLIQUE', $libelleEntite, true, $encoding);
                //Objet du marché
                $objet = $ordreDuJour->getIntituleOrdreDuJour();
                $odf->setVars('OBJET', $objet, true);
                $odf->setVars('DATE_DEBUT', Atexo_Util::iso2frnDate($ordreDuJour->getDateDebut()), true, $encoding);
                $odf->setVars('DATE_FIN', Atexo_Util::iso2frnDateTime($ordreDuJour->getDateCloture()), true, $encoding);
                $odf->setVars('DATE_COMMISSION', substr(Atexo_Util::iso2frnDateTime($commission->getDate()), 0, 10), true, $encoding);
                $odf->setVars('NOM_PRESIDENT', Prado::localize('DEFINE_NOM_PRESIDENT_COMMISSION'), true);
                $odf->setVars('NOM_SECRETAIRE_COMMISSION', '', true);
                $odf->setVars('QUALITE_SEC', '', true);
                $odf->setVars('ENTETE_PV_SECTION2', $libelleEntite.' - '.$objet, true);
                $refCons = ($ordreDuJour->getConsultationId()) ? $ordreDuJour->getConsultationId() : $ordreDuJour->getRefLibre();
                $odf->setVars('REF_CONS', $refCons, true);
                $odf->setVars('SERVICE', $libelleEntite, true);
                $libelleTypeProcedure = '';
                if (!$ordreDuJour->getIdTypeProcedure()) {
                    $libelleTypeProcedure = $ordreDuJour->getTypeProcedureLibre();
                } else {
                    $dataProcedures = Atexo_Consultation_ProcedureType::retrieveProcedureType(false, false, Atexo_CurrentUser::getCurrentOrganism(), Atexo_Languages::readLanguageFromSession());
                    $libelleTypeProcedure = $dataProcedures[$ordreDuJour->getIdTypeProcedure()];
                }
                $odf->setVars('TYPE_PROC_CONS', $libelleTypeProcedure, true);
                $odf->setVars('INTITULE_CONS', $objet, true);
                //Objet de la consultation
                $odf->setVars('OBJET_CONS', $objet, true);

                //Insertion d'image
                if ($genererLogoEntete) {
                    $odf->setImage('imageEntete', self::getImageEnteteDocsPv());
                } else {
                    $odf->setImage('imageEntete', '');
                }
                //Date du PV
                $datePv = '';
                $odf->setVars('DATE_PV', $datePv, true, $encoding);
                //Lieu du PV
                $odf->setVars('LIEU_PV', $entiteVille, true, $encoding);

                // Début liste des membres de la commission
                $listeAgentsCommission = Atexo_Commission_Seance::listeMembreCommissionAvecTypesVoix($ordreDuJour);
                // Fin liste des membres de la commission
                $listeMembresCom = $odf->setSegment('listeMembres');
                if (is_array($listeAgentsCommission) && count($listeAgentsCommission)) {
                    foreach ($listeAgentsCommission as $agentsCommission) {
                        if ($agentsCommission) {
                            $listeTypeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('REFERENTIEL_TYPE_VOIX_INTERVENENTS'));
                            $libelleTypeVoix = $listeTypeVoix[$agentsCommission->getTypeVoix()];
                            if ($agentsCommission instanceof CommonAgentCommission) {
                                $agent = Atexo_Agent::retrieveAgent($agentsCommission->getIdAgent());
                                if ($agent instanceof CommonAgent) {
                                    $nom = $agent->getNom();
                                    $prenom = $agent->getPrenom();
                                }
                            } elseif ($agentsCommission instanceof CommonIntervenantExterneCommission) {
                                $agent = Atexo_Commission_IntervenantExterne::getIntervenantByIdAndOrg($agentsCommission->getIdIntervenantExterne(), $ordreDuJour->getOrganisme());
                                if ($agent instanceof CommonIntervenantExterne) {
                                    $nom = $agent->getNom();
                                    $prenom = $agent->getPrenom();
                                }
                            } elseif ($agentsCommission instanceof CommonIntervenantOrdreDuJour) {
                                if ($agentsCommission->getIdAgent()) {
                                    $agent = Atexo_Agent::retrieveAgent($agentsCommission->getIdAgent());
                                    if ($agent instanceof CommonAgent) {
                                        $nom = $agent->getNom();
                                        $prenom = $agent->getPrenom();
                                    }
                                } elseif ($agentsCommission->getIdIntervenant()) {
                                    $agent = Atexo_Commission_IntervenantExterne::getIntervenantByIdAndOrg($agentsCommission->getIdIntervenant(), $ordreDuJour->getOrganisme());
                                    if ($agent instanceof CommonIntervenantExterne) {
                                        $nom = $agent->getNom();
                                        $prenom = $agent->getPrenom();
                                    }
                                }
                            }
                            $listeMembresCom->membre->setVars('Nom', $nom, true, $encoding);
                            $listeMembresCom->membre->setVars('prenom', $prenom, true, $encoding);
                            $listeMembresCom->membre->setVars('type_voix', $libelleTypeVoix, true, $encoding);
                            $listeMembresCom->membre->setVars('qualite', '', true, $encoding);
                            $listeMembresCom->membre->merge();
                        }
                    }
                } else {
                    $listeMembresCom->membre->setVars('Nom', '', true);
                    $listeMembresCom->membre->setVars('prenom', '', true);
                    $listeMembresCom->membre->setVars('type_voix', '', true);
                    $listeMembresCom->membre->setVars('qualite', '', true);
                    $listeMembresCom->membre->merge();
                }
                $listeMembresCom->merge();
                $odf->mergeSegment($listeMembresCom);

                //Début remplissage tableau des candidatures
                //Récupération de la consultation

                $consultation = Atexo_Consultation::getConsultationByReferenceUser($ordreDuJour->getConsultationId(), $ordreDuJour->getOrganisme());

                $infosOffres = [];
                $infosCandidature = [];
                if ($consultation instanceof CommonConsultation) {
                    $typeEnveloppe = Atexo_Consultation_Responses::getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());

                    $criteriaResponse = new Atexo_Response_CriteriaVo();
                    $criteriaResponse->setReference($consultation->getId());
                    $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
                    $criteriaResponse->setOrganisme($organisme);
                    $arEnveloppe = Atexo_Consultation_Responses::retrieveEnveloppesCandidatureElectroniques($criteriaResponse);

                    $lot = $consultation->getAllLots();
                    if ($arEnveloppe) {
                        $infosCandidature = self::recupererDonneesCandidature($arEnveloppe, $lot, Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                    }
                    $arEnveloppe = Atexo_Consultation_Responses::retrieveEnveloppesCandidaturePapier($consultation->getId(), null, $typeEnveloppe, '', '', $organisme, false);
                    if ($arEnveloppe) {
                        $infosCandidature = array_merge($infosCandidature, self::recupererDonneesCandidature($arEnveloppe, $lot, Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')));
                    }

                    //Fin remplissage tableau des candidatures

                    // Début remplissage tableau des offres
                    $typeEnveloppe = Atexo_Consultation_Responses::getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());

                    if (!$organisme) {
                        $organisme = Atexo_CurrentUser::getOrganismAcronym();
                    }
                    $lesLots = $consultation->getAllLots();
                    $nbrLot = count($lesLots);

                    if (0 == $nbrLot) {
                        $criteriaResponse = new Atexo_Response_CriteriaVo();
                        $criteriaResponse->setReference($consultation->getId());
                        $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
                        $criteriaResponse->setOrganisme($organisme);
                        $enveloppe = Atexo_Consultation_Responses::retrieveEnveloppesOffresElectroniques($criteriaResponse);
                        if ($enveloppe) {
                            $infosOffres = array_merge($infosOffres, self::recupererDonnesOffres($enveloppe, '', Atexo_Config::getParameter('TYPE_ENV_OFFRE')));
                        }
                        $enveloppe = Atexo_Consultation_Responses::retrieveEnveloppesOffrePapier($consultation->getId(), $typeEnveloppe, 0, '', '', $organisme, false, true, false);
                        if ($enveloppe) {
                            $infosOffres = array_merge($infosOffres, self::recupererDonnesOffres($enveloppe, '', Atexo_Config::getParameter('TYPE_ENV_OFFRE')));
                        }
                    } else {
                        foreach ($lesLots as $unLot) {
                            $criteriaResponse = new Atexo_Response_CriteriaVo();
                            $criteriaResponse->setReference($consultation->getId());
                            $criteriaResponse->setSousPli($unLot->getLot());
                            $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
                            $criteriaResponse->setOrganisme($organisme);
                            $arEnveloppes = Atexo_Consultation_Responses::retrieveEnveloppesOffresElectroniques($criteriaResponse);
                            if ($arEnveloppes) {
                                $infosOffres = array_merge($infosOffres, self::recupererDonnesOffres($arEnveloppes, $unLot->getLot(), Atexo_Config::getParameter('TYPE_ENV_OFFRE')));
                            }
                            $arEnveloppesPapier = Atexo_Consultation_Responses::retrieveEnveloppesOffrePapier($consultation->getId(), $typeEnveloppe, $unLot->getLot(), '', '', $organisme, false, true, false);

                            if ($arEnveloppesPapier) {
                                $infosOffres = array_merge($infosOffres, self::recupererDonnesOffres($arEnveloppesPapier, $unLot->getLot(), Atexo_Config::getParameter('TYPE_ENV_OFFRE')));
                            }
                        }
                    }
                }
                //Fin remplissage tableau des offres

                //Début tableau des offres
                $listecandidaturesAdm = $odf->setSegment('listeCandidatsAdm');
                $listecandidaturesNonAdm = $odf->setSegment('listeCandidatsNonAdm');
                $listeOffres = $odf->setSegment('listeOffres');
                if ((int) $commission->getType() == Atexo_Config::getParameter('TYPE_COMMISSION_CTDO')) {
                    if (is_array($infosOffres) && count($infosOffres)) {
                        foreach ($infosOffres as $uneOffre) {
                            $listeOffres->offre->setVars('numPli', $uneOffre['numPli'], true, $encoding);
                            $listeOffres->offre->setVars('entreprise', $uneOffre['entreprise'], true, $encoding);
                            $listeOffres->offre->setVars('montant', $uneOffre['montant'], true, $encoding);
                            $listeOffres->offre->merge();
                        }
                    } else {
                        $listeOffres->offre->setVars('numPli', '', true);
                        $listeOffres->offre->setVars('entreprise', '', true);
                        $listeOffres->offre->setVars('montant', '', true);
                        $listeOffres->offre->merge();
                    }
                    //Fin tableau des offres

                    //Début tableau des candidatures
                    if (is_array($infosCandidature) && count($infosCandidature)) {
                        foreach ($infosCandidature as $unCandidat) {
                            if ($unCandidat['admissibilite']) {
                                $listecandidaturesAdm->candidat->setVars('numPliAdm', $unCandidat['numPli'], true, $encoding);
                                $listecandidaturesAdm->candidat->setVars('entrepriseAdm', $unCandidat['entreprise'], true, $encoding);
                                $listecandidaturesAdm->candidat->merge();
                            } else {
                                $listecandidaturesNonAdm->candidatNonAdm->setVars('numPliNonAdm', $unCandidat['numPli'], true, $encoding);
                                $listecandidaturesNonAdm->candidatNonAdm->setVars('entrepriseNonAdm', $unCandidat['entreprise'], true, $encoding);
                                $listecandidaturesNonAdm->candidatNonAdm->merge();
                            }
                        }
                    } else {
                        $listecandidaturesAdm->candidat->setVars('numPliAdm', '', true);
                        $listecandidaturesAdm->candidat->setVars('entrepriseAdm', '', true);
                        $listecandidaturesAdm->candidat->merge();
                        $listecandidaturesNonAdm->candidatNonAdm->setVars('numPliNonAdm', '', true);
                        $listecandidaturesNonAdm->candidatNonAdm->setVars('entrepriseNonAdm', '', true);
                        $listecandidaturesNonAdm->candidatNonAdm->merge();
                    }
                }
                $listecandidaturesAdm->merge();
                $odf->mergeSegment($listecandidaturesAdm);
                $listecandidaturesNonAdm->merge();
                $odf->mergeSegment($listecandidaturesNonAdm);
                $listeOffres->merge();
                $odf->mergeSegment($listeOffres);
                //Fin tableau des candidatures

                if ($toFile) {
                    $pathFile = Atexo_Config::getParameter('COMMON_TMP').'Pv_Commission_'.$lang.'_'.session_id().time().'_fichier.odt';
                    $odf->saveToDisk($pathFile);

                    return $pathFile;
                } else {
                    $odf->exportAsAttachedFile('test.odt');
                    exit;
                }
            } else {
                return -1;
            }
        }
    }

    /**
     * Permet de générer les convocations des Commissions.
     *
     * @param Objet $commission : l'objet commission
     * @param $agent: agent
     * @param varchar $modele : le chemin du modèle de document
     * @param bool    $toFile : permet de spécifier s'il faut exporter en fichier ou retourner le chemin du document généré
     * @param varchar $lang   :  la langue de navigation
     */
    public function generationConvocCommission($commission, $agent, $modele, $toFile, $lang, $genererLogoEntete = false)
    {
        $fileModel = $modele.$lang.'.odt';
        $encoding = Atexo_Config::getParameter('HTTP_ENCODING');
        if ($commission instanceof CommonCommission) {
            if (is_file($fileModel)) {//verifier si le fichier existe pour cette langue.
                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt');
                }
                $config = [
                                'ZIP_PROXY' => 'PhpZipProxy',
                                'DELIMITER_LEFT' => '{',
                                'DELIMITER_RIGHT' => '}',
                                'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt',
                                ];
                $odf = new Atexo_Odf($fileModel, $config);

                if (!$connexion) {
                    $connexion = $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                }
                /*$numTel = '';
                $numFax = '';
                $email = '';
                if($agent instanceof CommonAgent) {
                    $numTel = $agent->getNumTel();
                    $numFax = $agent->getNumFax();
                    $email = $agent->getEmail();
                } else if($agent instanceof CommonIntervenantExterne) {
                    $email = $agent->getMail();
                }*/
                $odf->setVars('SALLE', $commission->getSalle(), true);
                $odf->setVars('LIEU', $commission->getLieu(), true);
                $odf->setVars('DATE_GENERATION', date('d/m/Y'), true);
                $odf->setVars('DATE_COMMISSION', substr(Atexo_Util::iso2frnDateTime($commission->getDate()), 0, 10), true);
                $odf->setVars('HEURE_DEBUT_COM', substr(Atexo_Util::iso2frnDateTime($commission->getDate()), 10, 6), true);
                $odf->setVars('NOM_PRESIDENT_CAO', '', true);
                $agentGenerateur = Atexo_Agent::retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());
                $nomAgentGenerateur = '';
                $prenomAgentGenerateur = '';
                $numTelAgentGenerateur = '';
                $numFaxAgentGenerateur = '';
                $emailAgentGenerateur = '';
                if ($agentGenerateur instanceof CommonAgent) {
                    $nomAgentGenerateur = $agentGenerateur->getNom();
                    $prenomAgentGenerateur = $agentGenerateur->getPrenom();
                    $numTelAgentGenerateur = $agentGenerateur->getNumTel();
                    $numFaxAgentGenerateur = $agentGenerateur->getNumFax();
                    $emailAgentGenerateur = $agentGenerateur->getEmail();
                }
                $odf->setVars('PRENOM_AGENT', $prenomAgentGenerateur, true, $encoding);
                $odf->setVars('NOM_AGENT', $nomAgentGenerateur, true, $encoding);
                $odf->setVars('TELEPHONE_AGENT', $numTelAgentGenerateur, true, $encoding);
                $odf->setVars('TELECOPIEUR_AGENT', $numFaxAgentGenerateur, true, $encoding);
                $odf->setVars('EMAIL_AGENT', $emailAgentGenerateur, true, $encoding);
                $objetOrganisme = Atexo_Organismes::retrieveOrganismeByAcronyme($agent->getOrganisme());
                $villeOrg = '';
                if ($objetOrganisme instanceof CommonOrganisme) {
                    $villeOrg = $objetOrganisme->getVille();
                }
                $odf->setVars('VILLE_ORGANISME', $villeOrg, true, $encoding);
                $typeCommission = Atexo_Commission_Commissions::remplirListeTypeCommission($commission->getType());
                $odf->setVars('TYPE', $typeCommission, true, $encoding);
                //Insertion d'image
                if ($genererLogoEntete) {
                    $odf->setImage('imageEntete', self::getImageEnteteDocsPv());
                } else {
                    $odf->setImage('imageEntete', '');
                }

                if ($toFile) {
                    $pathFile = Atexo_Config::getParameter('COMMON_TMP').'Convoc_Commission_'.$lang.'_'.session_id().time().'_fichier.odt';
                    $odf->saveToDisk($pathFile);

                    return $pathFile;
                } else {
                    $odf->exportAsAttachedFile('test.odt');
                    exit;
                }
            } else {
                return -1;
            }
        }
    }

    public function recupererDonneesCandidature($arEnveloppe, $lots, $typeEnveloppe)
    {
        if ($arEnveloppe) {
            $infosCandidatures = [];
            foreach ($arEnveloppe as $oneEnv) {
                if ($oneEnv instanceof CommonEnveloppePapier) {
                    $offre = $oneEnv->getCommonOffrePapier();
                } elseif ($oneEnv instanceof CommonEnveloppe) {
                    $offre = $oneEnv->getCommonOffres();
                }
                if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') && true === $offre->getMoreThanOneLot()) {
                    $admissible = $offre->getNombreLotsAdmissibles();
                    $nonAdmissible = $offre->getNombreLotsNonAdmissibles();
                } else {
                    $admissible = '0';
                    $nonAdmissible = '0';
                }
                $infosCandidatures[]['numPli'] = $oneEnv->getNumPli();
                $infosCandidatures[]['entreprise'] = Atexo_Util::escapeXmlChars($offre->getNomEntreprise());
                $infosCandidatures[]['admissibilite'] = (0 == (int) $nonAdmissible) ? 1 : 0;
            }
        }

        return $infosCandidatures;
    }

    public function recupererDonnesOffres($enveloppes, $lot, $typeEnv)
    {
        $infosCandidatures = [];
        if ($enveloppes) {
            $increment = 0;
            foreach ($enveloppes as $enveloppe) {
                $statutEnveloppe = $enveloppe->getLibelleStatutEnveloppe();
                $agentOuverture = '';
                if ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
                || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) {
                    $statutEnveloppe .= ' '.Prado::localize('TEXT_LE').' : '.Atexo_Util::iso2frnDateTime($enveloppe->getDateheureOuverture());
                    if ($enveloppe->getAgentIdOuverture()) {
                        $agent = Atexo_Agent::retrieveAgent($enveloppe->getAgentIdOuverture());
                        if ($agent) {
                            $agentOuverture = Prado::localize('PAR').' : '.Atexo_Util::escapeXmlChars($agent->getPrenom()).' '.Atexo_Util::escapeXmlChars($agent->getNom());
                        }
                    } elseif ('' != $enveloppe->getNomAgentOuverture()) {
                        $agentOuverture = Prado::localize('PAR').' : '.Atexo_Util::escapeXmlChars($enveloppe->getNomAgentOuverture());
                    }
                }
                $entreprise = '';
                if ($enveloppe instanceof CommonEnveloppe) {
                    $untrustedDate = $enveloppe->getCommonOffres()->getUntrusteddate();
                    $typeEnveloppe = ucfirst(Prado::localize('EL_POUR_ELECTRONIQUE'));
                    if ('' != $enveloppe->getCommonOffres()->getNomEntreprise()) {
                        $entreprise = Atexo_Util::escapeXmlChars($enveloppe->getCommonOffres()->getNomEntreprise());
                    } else {
                        $entreprise = '-';
                    }
                } elseif ($enveloppe instanceof CommonEnveloppePapier) {
                    $untrustedDate = $enveloppe->getCommonOffrePapier()->getDateDepot();
                    //echo " test kbe " .$untrustedDate; exit;
                    $typeEnveloppe = ucfirst(Prado::localize('PA_POUR_PAPIER'));
                    if ('' != $enveloppe->getCommonOffrePapier()->getNomEntreprise()) {
                        $entreprise = Atexo_Util::escapeXmlChars($enveloppe->getCommonOffrePapier()->getNomEntreprise());
                    } else {
                        $entreprise = '-';
                    }
                }
                if (strstr($untrustedDate, '/')) {
                    $untrustedDate = Atexo_Util::frnDateTime2iso($untrustedDate, false, true);
                }
                $infosCandidatures[$increment]['numPli'] = $enveloppe->getNumPli();
                $infosCandidatures[$increment]['entreprise'] = $entreprise;
                $infosCandidatures[$increment]['montant'] = '';
                ++$increment;
            }

            return $infosCandidatures;
        }
    }

    public function getImageEnteteDocsPv()
    {
        return Atexo_Controller_Front::t().'/images/entetePvCommission.gif';
    }
}
