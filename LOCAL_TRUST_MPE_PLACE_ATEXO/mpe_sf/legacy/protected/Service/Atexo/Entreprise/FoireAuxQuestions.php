<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonFaqEntreprise;
use Application\Propel\Mpe\CommonFaqEntreprisePeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;

/*
 * commentaires
 *
 * @author othmane ABISOUROUR <othmane.abisourour@atexo.com>
 * @copyright Atexo 2008
 * @version 1.0
 * @since MPE-3.0
 * @package
 * @subpackage
 */

class Atexo_Entreprise_FoireAuxQuestions
{
    /**
     * Permet de retourner les infos de faq entreprise selon le theme.
     *
     * @param string $theme le theme
     *
     * @return array of CommonFaqEntreprise
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function retreiveInfoFoire($theme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonFaqEntreprisePeer::THEME, $theme);
        $c->addAscendingOrderByColumn(CommonFaqEntreprisePeer::ORDRE);
        $faqs = CommonFaqEntreprisePeer::doSelect($c, $connexionCom);
        if ($faqs) {
            return $faqs;
        } else {
            return [];
        }
    }
}
