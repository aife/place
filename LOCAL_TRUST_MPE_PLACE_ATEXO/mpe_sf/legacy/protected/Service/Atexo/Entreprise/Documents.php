<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultationDocumentCfe;
use Application\Propel\Mpe\CommonConsultationDocumentCfePeer;
use Application\Propel\Mpe\CommonDocumentsAttachesPeer;
use Application\Propel\Mpe\CommonJustificatifsPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use PDO;

/**
 * Classe de manipulation du coffre fort de l'entreprise.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise_Documents
{
    /**
     * retourne les documents attachés a une entreprise.
     */
    public function retrieveDocuments($idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonJustificatifsPeer::ID_ENTREPRISE, $idEntreprise);
        $documents = CommonJustificatifsPeer::doSelectJoinCommonDocumentsAttaches($c, $connexionCom);
        //print_r($documents);exit;
        if ($documents) {
            return $documents;
        } else {
            return [];
        }
    }

    /**
     * retourne les types des documents de la table Justificatif.
     */
    public function retieveDocumentType()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $docType = CommonDocumentsAttachesPeer::doSelect($c, $connexionCom);
        if ($docType) {
            return $docType;
        } else {
            return false;
        }
    }

    /**
     * supprime un document de la table Justificatifs.
     */
    public function deleteDocument($idDoc)
    {
        //on est dans une méthode qui fait une modification de la base donc on accède à la base lecture/ecriture
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        CommonJustificatifsPeer::doDelete($idDoc, $connexionCom);
    }

    /**
     * mettre les identifiants des document de l'entreprise dans un tableau.
     */
    public function retirieveArrayIdsDocs($idEntreprise)
    {
        $documents = self::retrieveDocuments($idEntreprise);
        $ids = [];
        foreach ($documents as $oneDoc) {
            $ids[] = $oneDoc->getId();
        }

        return $ids;
    }

    /**
     * retourne  fichier dans la base.
     */
    public function retrieveDocumentById($idDocument, $idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonJustificatifsPeer::ID, $idDocument);
        $c->add(CommonJustificatifsPeer::ID_ENTREPRISE, $idEntreprise);
        $document = CommonJustificatifsPeer::doSelectJoinCommonDocumentsAttaches($c, $connexionCom);
        if ($document) {
            return $document;
        } else {
            return false;
        }
    }

    public function retrieveDocumentByPk($idDocument)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $document = CommonJustificatifsPeer::retrieveByPK($idDocument, $connexionCom);
        if ($document) {
            return $document;
        } else {
            return false;
        }
    }

    public function retrieveDocumentJoinCfe($consultationId, $orgConsultation, $idEntreprise)
    {
        $param = [];
        $sql = 'SELECT `Justificatifs`.* , consultation_document_cfe.id AS checked FROM `Justificatifs` ';
        $sql .= ' LEFT JOIN consultation_document_cfe ON ( Justificatifs.id = consultation_document_cfe.id_justificatif ';
        $sql .= ' AND consultation_document_cfe.consultation_id =:consultation_id ';
        $sql .= ' AND consultation_document_cfe.organisme_consultation =:orgConsultation )';
        $sql .= ' WHERE  Justificatifs.`id_entreprise` =:idEntreprise';

        //echo $sql; exit;

        $param[':idEntreprise'] = $idEntreprise;
        $param[':consultation_id'] = $consultationId;
        $param[':orgConsultation'] = $orgConsultation;

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute($param);
        //print_r($statement); exit;
        $resultat = [];
        $i = 0;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $resultat[$i]['id'] = $row['id'];
            $resultat[$i]['intitule_justificatif'] = $row['intitule_justificatif'];
            $resultat[$i]['id_entreprise'] = $row['id_entreprise'];
            $resultat[$i]['taille'] = $row['taille'];
            $resultat[$i]['justificatif'] = $row['justificatif'];
            $resultat[$i]['statut'] = $row['statut'];
            $resultat[$i]['id_document'] = $row['id_document'];
            $resultat[$i]['nom'] = $row['nom'];
            $resultat[$i]['intitule_justificatif'] = $row['intitule_justificatif'];
            $resultat[$i]['date_fin_validite'] = $row['date_fin_validite'];
            $resultat[$i]['visible_par_agents'] = $row['visible_par_agents'];
            $resultat[$i]['consultation_id'] = $consultationId;
            $resultat[$i]['organisme_consultation'] = $orgConsultation;

            if ($row['checked']) {
                $resultat[$i]['checked'] = true;
            } else {
                $resultat[$i]['checked'] = false;
            }
            ++$i;
        }

        return $resultat;
    }

    /**
     * Effectue une insertion dans la table consultation_document_cfe.
     */
    public function insererConsultationDocumentCfe($consultationId, $organismeConsultation, $objetJustificatif, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $consultationDocCfe = new CommonConsultationDocumentCfe();
        $consultationDocCfe->setIdEntreprise($objetJustificatif->getIdEntreprise());
        $consultationDocCfe->setConsultationId($consultationId);
        $consultationDocCfe->setOrganismeConsultation($organismeConsultation);
        $consultationDocCfe->setIdJustificatif($objetJustificatif->getId());
        $consultationDocCfe->setNomFichier($objetJustificatif->getIntituleJustificatif());
        $consultationDocCfe->setTailleDocument($objetJustificatif->getTaille());
        $consultationDocCfe->setIdBlob($objetJustificatif->getJustificatif());
        $consultationDocCfe->setTypeDocument($objetJustificatif->getNom());
        $consultationDocCfe->setDateFinValidite($objetJustificatif->getDateFinValidite());

        $consultationDocCfe->save($connexion);
    }

    /**
     * Supprime tous les éléments de la table consultation_document_cfe pour une consultation et un organisme données.
     */
    public function supprimerConsultationDocumentCfe($idEntreprise, $consultationId, $organismeConsultation, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonConsultationDocumentCfePeer::ID_ENTREPRISE, $idEntreprise);
        $c->add(CommonConsultationDocumentCfePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonConsultationDocumentCfePeer::ORGANISME_CONSULTATION, $organismeConsultation);
        CommonConsultationDocumentCfePeer::doDelete($c, $connexion);
    }

    public function isEntrepriseJoinDocsCfeConsultation($consultationId, $orgConsultation, $idEntreprise)
    {
        $document = self::retrieveDocumentJoinCfe($consultationId, $orgConsultation, $idEntreprise);
        if (is_array($document) && count($document)) {
            foreach ($document as $unDocument) {
                if ($unDocument['checked']) {
                    return true;
                }
            }

            return false;
        }

        return false;
    }
}
