<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonInscritHistorique;
use Application\Propel\Mpe\CommonInscritHistoriquePeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Classe de manipulation des évenements sur les inscrits.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise_InscritHistorique
{
    public function retereiveHistorique($idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritHistoriquePeer::ENTREPRISE_ID, $idEntreprise);
        $c->addDescendingOrderByColumn(CommonInscritHistoriquePeer::DATE);
        $inscrit = CommonInscritHistoriquePeer::doSelect($c, $connexionCom);
        if ($inscrit) {
            return $inscrit;
        } else {
            return [];
        }
    }

    /**
     * Permet d'enregistrer les evenements dans l'historique.
     *
     * @param string $inscri2    : nom de l'inscrit au compte duquel l'action est effectuee
     * @param string $mail2:     email de l'inscrit au compte duquel l'action est effectuee
     * @param string $action:    action
     * @param string $inscrit1:  nom de l'inscrit qui effectue l'action
     * @param string $mail1:     nom de l'inscrit qui effectue l'action
     * @param string $idInscrit1 : identifiant de l'inscrit qui effectue l'action
     * @param string $idEnt:     identifiant de l'entreprise
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setHistorique($inscri2, $mail2, $action, $inscrit1 = null, $mail1 = null, $idInscrit1 = null, $idEnt = null)
    {
        //on est dans une méthode qui fait une modification de la base donc on accède à la base lecture/ecriture
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (null == $inscrit1) {
            $inscrit1 = Atexo_CurrentUser::getFirstNameInscrit().' '.Atexo_CurrentUser::getLastNameInscrit();
        }
        if (null == $mail1) {
            $mail1 = Atexo_CurrentUser::getEmailInscrit();
        }
        if (null == $idInscrit1) {
            $idInscrit1 = Atexo_CurrentUser::getIdInscrit();
        }
        if (null == $idEnt) {
            $idEnt = Atexo_CurrentUser::getIdEntreprise();
        }
        $historique = new CommonInscritHistorique();
        $historique->setIdInscrit1($idInscrit1);
        $historique->setEntrepriseId($idEnt);
        $historique->setInscrit1($inscrit1);
        $historique->setMail1($mail1);
        $historique->setProfil1((new Atexo_Entreprise_Inscrit())->getProfilByIdInscrit($idInscrit1));
        $historique->setInscrit2($inscri2);
        $historique->setMail2($mail2);
        $historique->setDate(date('Y-m-d H:i:s'));
        $historique->setAction($action);
        //print_r($historique);exit;
        $historique->Save($connexionCom);
    }
}
