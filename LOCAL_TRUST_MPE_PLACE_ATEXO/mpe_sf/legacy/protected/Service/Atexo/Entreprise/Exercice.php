<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEntrepriseInfoExercice;
use Application\Propel\Mpe\CommonEntrepriseInfoExerciceQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntrepriseQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Exception;

class Atexo_Entreprise_Exercice
{
    /**
     * Permet de synchroniser les exercices identifie par son siren avec Api Gouv Entreprise.
     *
     * @param string $siren : $siren de l'entreprise concernée
     * @param $loggerWs
     * @param $connexion
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function synchroExerciceAvecSGMAP($siren, $loggerWs = null, $connexion = null)
    {
        if (!$loggerWs) {
            $loggerWs = Atexo_LoggerManager::getLogger();
        }
        $loggerWs->info("Debut de la synchronisation des exercices de l'entreprise dont le siren = $siren avec Api Gouv Entreprise");
        try {
            if (!$connexion) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            }
            //Recuperation du siege de l'entreprise associe
            $loggerWs->info("Recuperation de l'entreprise dont le sire = $siren");
            $entreprise = EntrepriseQuery::create()->findOneBySiren($siren);
            if ($entreprise instanceof Entreprise) {
                $idEntreprise = $entreprise->getId();
                $loggerWs->info("Recuperation du siege de l'entreprise dont le sire = $siren");
                $siege = $entreprise->getEtablissementSiege();
                if ($siege instanceof CommonTEtablissement) {
                    $loggerWs->info('Etablissement siege trouve');
                    $siret = $siren.$siege->getCodeEtablissement();
                    $exercices = self::recupererExerciceSGMAP($siret, $connexion);
                    if (is_array($exercices)) {
                        foreach ($exercices as $exercice) {
                            if ($exercice instanceof CommonEntrepriseInfoExercice) {
                                $loggerWs->info("Debut de mise à jour de l'exercie de l'entreprise ".$idEntreprise." l'annnee ".$exercice->getAnneeClotureExercice());
                                $exercice->setIdEntreprise($idEntreprise);
                                self::updateInfoExercice($exercice, $loggerWs, $connexion);
                                $loggerWs->info("Fin de mise à jour de l'exercie de l'entreprise ".$idEntreprise." l'annnee ".$exercice->getAnneeClotureExercice());
                            }
                        }
                    }
                } else {
                    $loggerWs->info('Etablissement siege non trouve');
                }
            } else {
                $loggerWs->error("L'entreprise ayant le siret = $siren n'est pas enregistre en base de donnees");
            }
        } catch (Exception $e) {
            $loggerWs->error("Erreur lors de la synchronisation avec Api Gouv Entreprise  : siren = $siren \n\nErreur: ".$e->getMessage()."\n\nTrace: ".$e->getTraceAsString());
        }
        $loggerWs->info("Fin de la synchronisation des exercices pour l'entreprise dont le siren = $siren avec Api Gouv Entreprise");
    }

    /**
     * Permet de recuperer les exercice selon le siret.
     *
     * @param int $siret le siret d'etablissement de l'etablissement siége
     *
     * @return mix array  CommonEntrepriseInfoExercice s'il existe sinon boolean false
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function recupererExerciceSGMAP($siret, $connexion = null)
    {
        $token = Atexo_Config::getParameter('API_ENTREPRISE_TOKEN');

        $paramsWs = [
            'token' => $token,
            'apiEtabExerciceUrl' => Atexo_Config::getParameter('URL_API_ENTREPRISE_CHIFFRES_AFFAIRES'),
        ];
        $loggerWs = Atexo_LoggerManager::getLogger();
        $interfaceWS = new InterfaceSynchroApiGouvEntreprise($loggerWs, $paramsWs);
        $resultat = $interfaceWS->getExerciceBySiret($siret);
        if (is_array($resultat) && InterfaceSynchroApiGouvEntreprise::isResponseWsApiGouvEntrepriseSucces($resultat) && $resultat[1]) {
            $jeton = $resultat[2];
            $loggerWs->info("Debut de l'enregistrement de l'historique de la synchronisation des exercices");
            (new Atexo_Entreprise_HistoriqueSynchroSGMAP())->enregistrerHistroique('EXERCICE', $siret, $jeton, $connexion);
            $loggerWs->info("Fin de l'enregistrement de l'historique de la synchronisation des exercices");

            $exercices = $resultat[1];
            if (is_array($exercices)) {
                return $exercices;
            }
        } else {
            $loggerWs->info('Les exercices sont vide');
        }

        return false;
    }

    /**
     * Permet de recuperer les exercices.
     *
     * @param array les information des exercices
     *
     * @return array un tableau contenant l'objet CommonEntrepriseInfoExercice
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function loadFromApiGouvEntreprise($infoExercices)
    {
        if (is_array($infoExercices)) {
            $exercices = [];
            foreach ($infoExercices as $oneInfoExercice) {
                $exerciceData = $oneInfoExercice['data'];
                $dateFin = Atexo_Util::getDateTimeByFormat($exerciceData['date_fin_exercice'], 'Y-m-d');
                $annee = Atexo_Util::getAnneeFromDate($dateFin);

                $oneExercice = new CommonEntrepriseInfoExercice();
                $oneExercice->setVentesglob($exerciceData['chiffre_affaires']);
                $oneExercice->setAnneeClotureExercice($annee);
                $oneExercice->setSaisieManuelle('0');
                $oneExercice->setDebutexerciceglob($annee.'-01-01');
                $oneExercice->setFinexerciceglob($annee.'-12-31');

                $exercices[] = $oneExercice;
            }

            return $exercices;
        }

        return $infoExercices;
    }

    /**
     * Permet de recuperer l'exercice by id entreprise et annee.
     *
     * @param int    $idEntreprise l'id d'entreprise
     * @param string $annee        l'annee concernee
     *
     * @return mix Object  CommonEntrepriseInfoExercice s'il existe sinon boolean false
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function getExerciceEntrepriseByAnne($idEntreprise, $annee, $connexion = null)
    {
        $exerciceQuery = new CommonEntrepriseInfoExerciceQuery();
        $exercice = $exerciceQuery->retrieveEntrepriseInfoExerciceByIdEntreprise($idEntreprise, $annee, $connexion);

        return ($exercice instanceof CommonEntrepriseInfoExercice) ? $exercice : false;
    }

    /**
     * Permet de metre a jour un exercice s'il existe si non il sera créer.
     *
     * @param object CommonEntrepriseInfoExercice
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function updateInfoExercice($exerciceSGMAP, $loggerWs, $connexion = null)
    {
        if ($exerciceSGMAP instanceof CommonEntrepriseInfoExercice) {
            if (!$connexion) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            }
            $loggerWs->info("Recuperer l'exercice de la BD dont idEntreprise=> ".$exerciceSGMAP->getIdEntreprise()." et l'annee de cloture=> ".$exerciceSGMAP->getAnneeClotureExercice());
            $exercice = self::getExerciceEntrepriseByAnne($exerciceSGMAP->getIdEntreprise(), $exerciceSGMAP->getAnneeClotureExercice());
            if ($exercice instanceof CommonEntrepriseInfoExercice) {
                $loggerWs->info("L'exercice existe dans la BD => mise à jour de l'exercice");
                $exercice->setFinexerciceglob($exerciceSGMAP->getFinexerciceglob());
                $exercice->setVentesglob($exerciceSGMAP->getVentesglob());
                $exercice->setSaisieManuelle($exerciceSGMAP->getSaisieManuelle());
            } else {
                $loggerWs->info("L'exercice n'existe pas dans la BD => nouveau exercice à inserer");
                $exercice = $exerciceSGMAP;
            }
            $loggerWs->info("Sauvgarder l'exercice");
            $exercice->save($connexion);
        }
    }

    /**
     * Verifier si  un des exercices des 3 dernières années est vide.
     *
     * @param int $idEntreprise l'id d'entreprise
     *
     * @return bool
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function exerciceVideExiste($idEntreprise)
    {
        $listeAnnees = Atexo_Util::arrayAnneesDerniers(3);
        $exerciceVide = false;
        foreach ($listeAnnees as $annee) {
            $exercice = self::getExerciceEntrepriseByAnne($idEntreprise, $annee);
            if ($exercice instanceof CommonEntrepriseInfoExercice) {
                if (!$exercice->getVentesglob()) {
                    $exerciceVide = true;
                }
            } else {
                $exerciceVide = true;
            }
        }

        return $exerciceVide;
    }
}
