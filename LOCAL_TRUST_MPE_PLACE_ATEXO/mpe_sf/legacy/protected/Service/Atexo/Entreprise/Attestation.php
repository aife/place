<?php

namespace Application\Service\Atexo\Entreprise;

use App\Service\ApiGateway\ApiEntreprise;
use App\Traits\ConfigProxyTrait;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTAttestationsOffres;
use Application\Propel\Mpe\CommonTAttestationsOffresQuery;
use Application\Propel\Mpe\CommonTDerniersAppelsValidesWsSgmapDocuments;
use Application\Propel\Mpe\CommonTDerniersAppelsValidesWsSgmapDocumentsQuery;
use Application\Propel\Mpe\CommonTDocumentEntreprise;
use Application\Propel\Mpe\CommonTDocumentEntrepriseQuery;
use Application\Propel\Mpe\CommonTDocumentType;
use Application\Propel\Mpe\CommonTDocumentTypeQuery;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersion;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersionQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Odf;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use Exception;
use Logger;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;

/**
 * Permet de manipuler les attestations d'entreprise.
 *
 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_Entreprise_Attestation
{
    use ConfigProxyTrait;

    private $codeRetour;

    /**
     * Permet de synchroniser une attestation.
     *
     * @param int    $idEntreprise l'id de l'entreprise
     * @param string $type         le type d'attestation
     * @param string $objet
     *
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     * @copyright Atexo 2015
     */
    public function synchroAttestation($idObjet, $type, $synchroniser = false, $objet = null, $host = null, $version = false): array|bool
    {
        $idEntreprise = null;
        $idEtablissement = null;
        $document = null;
        $identifiant = null;
        $lastVersionDocument = false;
        $oversightParams = [];
        $codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
        try {
            $oversightParams['debutExecution'] = new DateTime('now');
            $oversightParams['nomBatch'] = 'synchroAttestation';
            $oversightParams['service'] = 'API_GOUV_ENTREPRISE';
            $entree = serialize(['idObjet' => $idObjet, 'type' => $type, 'synchroniser' => $synchroniser, 'objet' => $objet, 'host' => $host, 'version' => $version]);
            $oversightParams['variablesEntree'] = $entree;

            $oversightParams['typeFlux'] = 'WebService';
            $oversightParams['poids'] = 0;
            $oversightParams['nbFlux'] = 1;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        $synchronisationResult = false;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $loggerWs = Atexo_LoggerManager::getLogger();

        $typeDocumentObjet = CommonTDocumentTypeQuery::create()->filterByIdTypeDocument($type)->findOne($connexion);
        if (!($typeDocumentObjet instanceof CommonTDocumentType)) {
            $loggerWs->info("Le type de document $type n'existe pas en base de donnees");
        }
        if ($typeDocumentObjet->getSynchroActif()) {
            $synchronisationResult = [];
            if ($typeDocumentObjet->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')) {
                $loggerWs->info("Debut de la synchronisation de l'attestation de l'entreprise dont l'id_entreprise =>" . $idObjet . ' et de type_document =>' . $type);
                $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idObjet, null, false);
                if (!($entreprise instanceof Entreprise)) {
                    $loggerWs->info("L'entreprise dont l'id = " . $idObjet . " n'existe pas");

                    return false;
                }
                $identifiant = $entreprise->getSiren();
                $idEntreprise = $entreprise->getId();
            } elseif ($typeDocumentObjet->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT')) {
                $loggerWs->info("Debut de la synchronisation de l'attestation de l'etablissement dont l'id_etablissement =>" . $idObjet . ' et de type_document =>' . $type);
                $etablissement = CommonTEtablissementQuery::create()->filterByIdEtablissement($idObjet)->findOne($connexion);
                if (!($etablissement instanceof CommonTEtablissement)) {
                    $loggerWs->info("L'etablissement dont l'id = " . $idObjet . " n'existe pas");

                    return false;
                }
                $loggerWs->info("La recuperation de l'entreprise associe a l'etablissement dont l'id_etablissement = " . $idObjet);
                $entrepriseAssocieEtablissement = (new Atexo_Entreprise())->retrieveCompanyBdeById($etablissement->getIdEntreprise(), null, false);
                if (!($entrepriseAssocieEtablissement instanceof Entreprise)) {
                    $loggerWs->info("L'entreprise associe a l'etablissement dont l'id_etablissement = " . $idObjet . " n'existe pas");

                    return false;
                }
                $identifiant = $entrepriseAssocieEtablissement->getSiren() . $etablissement->getCodeEtablissement();
                $idEntreprise = $entrepriseAssocieEtablissement->getId();
                $idEtablissement = $etablissement->getIdEtablissement();
            }

            try {
                $loggerWs->info("La recuperation du document de type_document '" . $type . "'");
                if ($typeDocumentObjet->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')) {
                    $document = self::retrieveDocumentEntreprise($idEntreprise, $type);
                } elseif ($typeDocumentObjet->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT')) {
                    $document = self::retrieveDocumentEtablissement($idEtablissement, $type);
                }
                $idDocument = null;
                if ($document instanceof CommonTDocumentEntreprise) {
                    $loggerWs->info('La recuperation de la derniere version du document');
                    $lastVersionDocument = self::retrieveLastVersionDocumentEntreprise($document->getIdDerniereVersion());
                    $idDocument = $document->getIdDocument();
                }
                $loggerWs->info('La verfication de la condition de la synchronisation');

                if (self::isDocumentSynchronisable($document, $lastVersionDocument, $loggerWs, $synchroniser)) {
                    $loggerWs->info("La recuperation de l'url du document ...");
                    $resultat = self::recupererAttestationFromApiGouvEntreprise($identifiant, $type, $loggerWs, $objet, $host, $version);
                    $jsonExiste = false;
                    $contenuDocument = null;
                    $url = null;
                    if (is_array($resultat) && !empty($resultat) && 200 == (int) $resultat[1]) {
                        self::enregistrerDateDernierAppelValideWs($type, $identifiant, $idEntreprise);
                        $json = $resultat[2];
                        $loggerWs->info("Debut de l'enregistrement de l'historique de la synchronisation des exercices");
                        (new Atexo_Entreprise_HistoriqueSynchroSGMAP())->enregistrerHistroique($typeDocumentObjet->getCode(), $idDocument, $json, $connexion);
                        $loggerWs->info("Fin de l'enregistrement de l'historique de la synchronisation des exercices");
                        if ($typeDocumentObjet->getTypeRetourWs() == Atexo_Config::getParameter('TYPE_RETOUR_WS_JSON') && $json) {
                            $loggerWs->info("Il s'agit d'un metadonées");
                            $contenuDocument = $json;
                            $jsonExiste = true;
                        } elseif ($typeDocumentObjet->getTypeRetourWs() == Atexo_Config::getParameter('TYPE_RETOUR_WS_URL') && $resultat[0]) {
                            $loggerWs->info("Il s'agit d'un url fichier");
                            $url = $resultat[0];
                            $loggerWs->info("L'url de l'attestation => " . $url);
                        }
                        if ($url || $contenuDocument) {
                            self::sauvegarderDocument($document, $idEntreprise, $idEtablissement, $type, $identifiant, $url, $contenuDocument, $lastVersionDocument, $loggerWs, $connexion, $synchronisationResult, $jsonExiste);
                        } else {
                            if (!$document) {
                                $synchronisationResult = ['statut' => 'NOK', 'resultat' => Prado::Localize('AUCUNE_VERSION_DISPONIBLE'), 'erreur' => false];
                            } else {
                                $synchronisationResult = ['statut' => 'NOK', 'resultat' => Prado::Localize('VOUS_POSSEDEZ_LA_DERNIERE_VERSION'), 'erreur' => false];
                            }
                        }
                    } else {
                        $codeErreur = $resultat[1] ?? null;
                        $messageErreur = self::getMessageErreurPersonnaliseByCodeErreur($resultat, $type);
                        $synchronisationResult = ['statut' => 'NOK', 'resultat' => $messageErreur, 'erreur' => true, 'codeErreur' => $codeErreur];
                        $loggerWs->info(self::getMessageErreurPersonnaliseByCodeErreur($resultat));
                        $codeRetour = Atexo_Oversight_Oversight::$codeErrorRemoteServiceDoesNotRespond;
                        if ('404' == $codeErreur) {
                            $codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
                        }
                    }
                } else {
                    $synchronisationResult = ['statut' => 'NOK', 'resultat' => Prado::Localize('VOUS_POSSEDEZ_LA_DERNIERE_VERSION'), 'erreur' => false];
                    $loggerWs->info('Le document est deja a jour');
                }
            } catch (\Exception $e) {
                $codeRetour = Atexo_Oversight_Oversight::$codeErrorRemoteServiceDoesNotRespond;
                $connexion->rollBack();
                $loggerWs->error("Erreur lors de la synchronisation de l'attestation : " . $e->getMessage() . '  ' . $e->getTraceAsString());
                $synchronisationResult = ['statut' => 'NOK', 'resultat' => Prado::Localize('SERVICE_ACTUELLEMENT_INDISPONIBLE'), 'erreur' => true];
            }
            $loggerWs->info("Fin de la synchronisation de l'attestation dont le type_document => " . $type);
            $loggerWs->info('Retour resultat de synchronisation : ' . print_r($synchronisationResult, true));
        } else {
            $loggerWs->info("la synchonisation est désactiver pour ce type de document $type ");
        }

        try {
            $oversightParams['resultat'] = serialize($synchronisationResult);
            $oversightParams['code'] = $codeRetour;
            $oversightParams['finExecution'] = 'endScript';
            $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
            unset($oversightParams['finExecution']);
            unset($oversightParams['oversight']);
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        return $synchronisationResult;
    }

    /**
     * Permet d'enregistrer et retourner le type document.
     *
     * @param $typeDocumentInfo
     * @param $codeTypeDoc
     * @param $typeDocEntrepriseEtablissement
     * @param $connexion
     *
     * @return CommonTDocumentType
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getTypeDoumentFromData($typeDocumentInfo, $codeTypeDoc, $typeDocEntrepriseEtablissement, $connexion)
    {
        if (is_array($typeDocumentInfo)) {
            $typeDocumentObjet = new CommonTDocumentType();
            $typeDocumentObjet->setNomTypeDocument($typeDocumentInfo['libelle']);
            $typeDocumentObjet->setUri($typeDocumentInfo['uri']);
            $typeDocumentObjet->setParamsUri($typeDocumentInfo['params_uri']);
            $typeDocumentObjet->setCode($codeTypeDoc);
            $typeDocumentObjet->setTypeDocEntrepriseEtablissement($typeDocEntrepriseEtablissement);
            $typeDocumentObjet->save($connexion);

            return $typeDocumentObjet;
        }
    }

    /**
     * Permet de synchroniser les types de documents pour une entreprise ou un etablissement donne.
     *
     * @param string $idObjet             : identifiant de l'entreprise ou de l'etablissement
     * @param bool   $typeObjetEntreprise : precise si le type de l'objet est entreprise ou etablissement (true pour l'entreprise, false pour l'etablissement)
     * @param string $objet
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function synchroniserTypesDocumentsEntreprise($idObjet, $typeObjetEntreprise = true, $objet = null)
    {
        if (Atexo_Module::isEnabled('DonneesCandidat')) {
            $loggerWs = Atexo_LoggerManager::getLogger();
            $host = Atexo_Config::getParameter('URL_API_GOUV_ENTREPRISE');

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $typeDocToSynchronize = (true === $typeObjetEntreprise) ? Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE') : Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT');
            $docTypesQuery = new CommonTDocumentTypeQuery();
            $docTypes = $docTypesQuery->getTypesDocumentsByTypesEntrepriseEtablissement($typeDocToSynchronize, $connexion);
            if (is_array($docTypes) && !empty($docTypes)) {
                foreach ($docTypes as $oneType) {
                    self::synchroAttestation($idObjet, $oneType->getIdTypeDocument(), false, $objet, $host);
                }
            }
        }
    }

    /**
     * Permet de sauvegarder la version du document et le document.
     *
     * @param CommonTDocumentEntreprise $document              : objet document
     * @param string                    $idEntreprise          : identifiant de l'entreprise
     * @param string                    $type                  : type de document
     * @param string                    $siren                 : siren de l'entreprise
     * @param string                    $url                   : url du document
     * @param string                    $lastVersionDocument   : derniere version du document
     * @param Logger                    $logger
     * @param PropelPDO                 $connexion
     * @param array                     $synchronisationResult
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function sauvegarderDocument(&$document, $idEntreprise, $idEtablissement, $type, $identifiant, $url, $contenuDocument, $lastVersionDocument, $logger, $connexion, &$synchronisationResult = [], $json = false)
    {
        try {
            if (empty($contenuDocument)) {
                $logger->info("La recuperation du contenu du fichier d'attestation depuis l'url : '$url'");
                $configProxy = self::getConfigProxyForGuzzle(Atexo_Util::getSfContainer()) ?: [];
                $configProxy['timeout'] = 120;
                $client = new Client($configProxy);
                $content = $client->get($url)->getBody();
            } else {
                $content = $contenuDocument;
            }
            $logger->info('Verifier que la version du fichier est une nouvelle');
            $isNewVersion = (new self())->verifyIsNewVersion($content, $lastVersionDocument, $json);
            if ($isNewVersion) {
                $logger->info("C'est une nouvelle version du fichier");
                $nomDocument = str_replace(' ', '_', (new self())->getLibelleTypeDocument($type));
                if ($json) {
                    $newVersion = (new self())->getNewVersionDocumentJson($content, $logger);
                } else {
                    $newVersion = (new self())->getNewVersionDocument($content, $nomDocument, $logger);
                }
                if ($newVersion instanceof CommonTEntrepriseDocumentVersion) {
                    $logger->info('Debut de Sauvegarde de la version du document');
                    if (!$document) {
                        $logger->info("C'est un nouveau document");
                        $document = new CommonTDocumentEntreprise();
                        $document->setIdEntreprise($idEntreprise);
                        $document->setIdEtablissement($idEtablissement);
                        $document->setIdTypeDocument($type);
                        $document->setNomDocument($nomDocument . '_' . $identifiant);
                    }
                    $connexion->beginTransaction();
                    if (!$json) {
                        $extension = Atexo_Util::getExtension($url);
                        $newVersion->setExtensionDocument($extension);
                    }
                    $newVersion->setCommonTDocumentEntreprise($document);
                    $newVersion->save($connexion);
                    $document->setIdDerniereVersion($newVersion->getIdVersionDocument());
                    $logger->info('Sauvegarder le document');
                    $document->save($connexion);
                    $connexion->commit();
                    $logger->info("L'id de la version enregistrer est => " . $newVersion->getIdVersionDocument());
                    $synchronisationResult = ['statut' => 'OK', 'resultat' => $newVersion, 'erreur' => false];
                    $logger->info('Fin de Sauvegarde de la version du document');
                }
            } else {
                $synchronisationResult = ['statut' => 'NOK', 'resultat' => Prado::Localize('VOUS_POSSEDEZ_LA_DERNIERE_VERSION'), 'erreur' => false];
                $logger->info('Vous possédez déja la dernière version disponible de cette attestation.');
            }
        } catch (Exception $e) {
            $erreur = "Erreur lors de la sauvegarde du document et de sa version : idEntreprise=$idEntreprise , idEtablissement=$idEtablissement , type_document=$type , identifiant=$identifiant , $url=$url , lastVersionDocument: ";
            $erreur .= print_r($lastVersionDocument, true) . PHP_EOL . 'documentObjet : ' . print_r($document, true);
            $logger->error($erreur . PHP_EOL . PHP_EOL . 'Erreur: ' . $e->getMessage() . PHP_EOL . PHP_EOL . 'Trace: ' . $e->getTraceAsString());

            return false;
        }
    }

    /**
     * Permet de construire un message d'erreur personnalise en fonction du code d'erreur.
     *
     * @param string $codeErreur : code d'erreur
     * @param string $typeDoc    : le type de document
     *
     * @return mixed|string|null message d'erreur
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2016
     */
    public function getMessageErreurPersonnaliseByCodeErreur(array $response, int $typeDoc = null): ?string
    {
        $message = null;
        $codeErreur = $response[1] ?? null;
        $messageErrorFromApi = $response[0]['errors'][0]['detail'] ?? null;

        switch ($codeErreur) {
            case 404:
                $message = $messageErrorFromApiPrado::localize('MESSAGE_ATTESTATION_SOCIETE_INDISPONIBLE_ET_NE_POUVANT_ETRE_DELIVREE');
                break;
            case 503:
                if (in_array($typeDoc, [Atexo_Config::getParameter('TYPE_ATTESTATION_SOCIALE_MARCHE_PUBLIC'), Atexo_Config::getParameter('TYPE_ATTESTATION_SOCIALE_VIGILANCE')])) {
                    $message = Prado::localize('MESSAGE_PLATEFORME_ACOSS_INDISPONIBLE_ACTUELLEMENT');
                } else {
                    $message = $messageErrorFromApi ?? Prado::Localize('SERVICE_ACTUELLEMENT_INDISPONIBLE');
                }
                break;
            default:
                $message = $messageErrorFromApi ?? Prado::Localize('SERVICE_ACTUELLEMENT_INDISPONIBLE');
                break;
        }

        return $message;
    }

    /**
     * Permet de recuperer le document entreprise.
     *
     * @param int    $idEntreprise l'id de l'entreprise
     * @param string $type         le type d'attestation
     *
     * @return CommonTDocumentEntreprise le document de l'entreprise s'il existe sinon boolean false
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function retrieveDocumentEntreprise($idEntreprise, $type)
    {
        $document = CommonTDocumentEntrepriseQuery::create()->filterByIdTypeDocument($type)->filterByIdEntreprise($idEntreprise)->findOne();
        if ($document instanceof CommonTDocumentEntreprise) {
            return $document;
        }

        return false;
    }

    /**
     * Permet de recuperer le derniere version du document entreprise.
     *
     * @param int $idVersion l'id de la dernier version
     *
     * @return CommonTEntrepriseDocumentVersion la dernier version du document entreprises'il existe sinon boolean false
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function retrieveLastVersionDocumentEntreprise($idVersion)
    {
        $documentVersion = CommonTEntrepriseDocumentVersionQuery::create()
            ->filterByIdBlob(null, Criteria::ISNOTNULL)
            ->filterByIdVersionDocument($idVersion)
            ->findOne();
        if ($documentVersion instanceof CommonTEntrepriseDocumentVersion) {
            return $documentVersion;
        }

        return false;
    }

    /**
     * Permet de de savoir si le contenu du fichier est un nouveau fichier.
     *
     * @param $data le contenu du document
     * @param CommonTEntrepriseDocumentVersion $oldVersion
     *
     * @return bool
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function verifyIsNewVersion($data, $oldVersion, $json = false)
    {
        if ($oldVersion) {
            if ($json && $data === $oldVersion->getJsonDocument()) {
                return false;
            } else {
                $hashContent = md5($data);
                if ($hashContent == $oldVersion->getHash()) {
                    return false;
                }
            }
        }

        return true;
    }

    public function getAttestaionsList($idEntreprise)
    {
        $attestations = [];
        $query = <<<SQL
			SELECT distinct(TD.id_type_document),  TD.nom_type_document, DV.`date_recuperation`, DE.id_derniere_version, DE.id_document, DE.nom_document
			FROM t_document_type TD left join  t_document_entreprise DE on (DE.id_type_document = TD.id_type_document and DE.id_entreprise =:idEntreprise )
			left join `t_entreprise_document_version`  DV on DE.id_derniere_version = DV.`id_version_document` ;
SQL;
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $params = [':idEntreprise' => $idEntreprise];
        $statement->execute($params);
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($results as $row) {
            if ($row['date_recuperation']) {
                $dateRecup = Atexo_Util::iso2frnDateTime($row['date_recuperation'], false);
            } else {
                $dateRecup = '-';
            }
            $attestations[] = [
                    'id_type_document' => $row['id_type_document'],
                    'nom_type_document' => $row['nom_type_document'],
                    'date_recuperation' => $dateRecup,
                    'id_derniere_version' => $row['id_derniere_version'],
                    'id_document' => $row['id_document'],
                    'nom_document' => $row['nom_document'],
                ];
        }

        return $attestations;
    }

    /**
     * Permet d'avoir une nouvelle version d'un document.
     *
     * @param $data le contenu du document
     * @param  $nomDocument le nom du document
     * @param  $loggerWs
     *
     * @return CommonTEntrepriseDocumentVersion
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getNewVersionDocument($data, $nomDocument, $loggerWs)
    {
        $infileAttestation = Atexo_Config::getParameter('COMMON_TMP') . 'attestation_' . session_id() . time();

        try {
            if (Atexo_Util::write_file($infileAttestation, $data)) {
                $loggerWs->info("L'insertion du blob ");
                $loggerWs->info("Le contenu du fichier d'attestation est copie dans le chemin => " . $infileAttestation);
                $atexoBlob = new Atexo_Blob();
                $idBlob = $atexoBlob->insert_blob($nomDocument, $infileAttestation, Atexo_Config::getParameter('COMMON_BLOB'));
                if ('0' != $idBlob) {
                    $versionDocument = new CommonTEntrepriseDocumentVersion();
                    $tailleFile = $atexoBlob->getTailFile($idBlob, Atexo_Config::getParameter('COMMON_BLOB'));
                    $versionDocument->setTailleDocument(Atexo_Util::GetSizeName($tailleFile));
                    $versionDocument->setDateRecuperation(date('Y-m-d H:i:s'));
                    $versionDocument->setHash(md5($data));
                    $versionDocument->setIdBlob($idBlob);

                    return $versionDocument;
                } else {
                    $loggerWs->error("Erreur d'enregistrement du blob");
                }
            } else {
                $loggerWs->error("Erreur de l'insertion du blob ");
            }
        } catch (\Exception $e) {
            $loggerWs->error("Erreur d'enregistrement du blob");
        }

        return false;
    }

    /**
     * Permet d'avoir une nouvelle version d'un document.
     *
     * @param $data le contenu du document
     * @param  $nomDocument le nom du document
     * @param  $loggerWs
     *
     * @return CommonTEntrepriseDocumentVersion
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getNewVersionDocumentJson($json, $loggerWs)
    {
        $loggerWs->info("L'insertion du json ");
        $versionDocument = new CommonTEntrepriseDocumentVersion();
        $versionDocument->setDateRecuperation(date('Y-m-d H:i:s'));
        $versionDocument->setJsonDocument($json);

        return $versionDocument;
    }

    /**
     * Permet d'avoir une nouvelle version d'un document.
     *
     * @param $type le type de document
     *
     * @return string le nom de type document
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getLibelleTypeDocument($type)
    {
        $typeDocument = CommonTDocumentTypeQuery::create()->findOneByIdTypeDocument($type);
        if ($typeDocument instanceof CommonTDocumentType) {
            return $typeDocument->getNomTypeDocument();
        } else {
            return '';
        }
    }

    /**
     * recupere une version du document.
     *
     * @param int          $idDocument
     * @param connexionPDO $connexion
     *
     * @return PropelObjectCollection|mixed|bool|CommonTEntrepriseDocumentVersion
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     4.10.0
     *
     * @copyright Atexo 2015
     */
    public function retrieveDocumentVersion($idDocument, $connexion = null)
    {
        $documentVersions = CommonTEntrepriseDocumentVersionQuery::create()->filterByIdDocument($idDocument)->find($connexion);
        if ($documentVersions) {
            return $documentVersions;
        }

        return false;
    }

    /**
     * Permet d'enregistrer la date de dernier appel valide du WS SGMAP documents.
     *
     * @param string      $type         : type de document
     * @param string      $identifiant  : siren (9 caractères) ou siret (14 caractères)
     * @param string      $idEntreprise : identifiant de l'entreprise
     * @param string      $idAgent      : identifiant de l'utilisateur (agent ou entreprise)
     * @param string|null $date
     * @param $connexion : connexion
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2016
     */
    public static function enregistrerDateDernierAppelValideWs($type, $identifiant, $idEntreprise, $idAgent = null, $date = null, $connexion = null)
    {
        $logger = Atexo_LoggerManager::getLogger();
        try {
            if (empty($date)) {
                $date = date('Y-m-d H:i:s');
            }
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            }
            $dernierAppelValideWs = (new self())->recupererDernierAppelValideWs($type, $identifiant, $idAgent);
            if (empty($dernierAppelValideWs)) {
                $dernierAppelValideWs = new CommonTDerniersAppelsValidesWsSgmapDocuments();
                $dernierAppelValideWs->setIdTypeDocument($type);
                $dernierAppelValideWs->setIdEntreprise($idEntreprise);
                $dernierAppelValideWs->setIdentifiant($identifiant);
            }
            if (!empty($idAgent)) {
                $dernierAppelValideWs->setIdAgent($idAgent);
            }
            $dernierAppelValideWs->setDateDernierAppelValide($date);
            $dernierAppelValideWs->save($connexion);
            $logger->info("Date dernier appel valide du WS enregistre : $date");
        } catch (Exception $e) {
            $logger->error("Erreur enregistrement date de dernier appel valide du WS : type = $type , identifiant = $identifiant, idEntreprise = $idEntreprise , idAgent = $idAgent , date = $date \n\nErreur : " . $e->getMessage() . "\n\nTrace : " . $e->getTraceAsString());
        }
    }

    /**
     * Permet de recuperer le dernier appel valide du WS sgmap documents.
     *
     * @param string $type        : type de document
     * @param string $identifiant : siren (9 caractères) ou siret (14 caractères)
     * @param string $idAgent     : identifiant de l'utilisateur (agent ou entreprise)
     * @param $connexion : connexion
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocuments
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2016
     */
    public function recupererDernierAppelValideWs($type, $identifiant, $idAgent = null, $connexion = null)
    {
        $logger = Atexo_LoggerManager::getLogger();
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }
            $dernierAppelValideWsQuery = new CommonTDerniersAppelsValidesWsSgmapDocumentsQuery();

            return $dernierAppelValideWsQuery->recupererDateDernierAppelValideWs($type, $identifiant, $idAgent, $connexion);
        } catch (Exception $e) {
            $logger->error("Erreur recuperation dernier appel valide du WS : type = $type, identifiant = $identifiant , idAgent = $idAgent \n\nErreur : " . $e->getMessage() . "\n\nTrace : " . $e->getTraceAsString());
        }
    }

    /**
     * Permet de recuperer la date de dernier appel valide du WS sgmap documents.
     *
     * @param string $type    : type de document
     * @param string $idAgent : identifiant de l'utilisateur (agent ou entreprise)
     * @param $connexion : connexion
     *
     * @return CommonTDerniersAppelsValidesWsSgmapDocuments
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2016
     */
    public function recupererDateDernierAppelValideWs($type, $identifiant, $idAgent = null, $connexion = null)
    {
        $logger = Atexo_LoggerManager::getLogger();
        try {
            $dernierAppelValideWs = (new self())->recupererDernierAppelValideWs($type, $identifiant, $idAgent, $connexion);
            if ($dernierAppelValideWs instanceof CommonTDerniersAppelsValidesWsSgmapDocuments) {
                return $dernierAppelValideWs->getDateDernierAppelValide('Y-m-d H:i:s');
            }

            return null;
        } catch (Exception $e) {
            $logger->error("Erreur recuperation date de dernier appel valide du WS : type = $type , idAgent = $idAgent \n\nErreur : " . $e->getMessage() . "\n\nTrace : " . $e->getTraceAsString());
        }
    }

    /**
     * retourne le titre  de l'attestation.
     *
     * @param int $typeDoc le type de document
     *
     * @return string $alt alt du picto
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getTitreAttestation($typeDoc)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $objetTypeDoc = CommonTDocumentTypeQuery::create()->filterByIdTypeDocument($typeDoc)->findOne($connexion);

        return ($objetTypeDoc instanceof CommonTDocumentType) ? $objetTypeDoc->getNomTypeDocument() : '';
    }

    /**
     * Permet de recuperer une attestation depuis SGMAP selon l'identifiant (siren, siret) et le type d'attestation.
     *
     * @param string $identifiant : siren ou siret
     * @param string $type        : type d'attestation
     * @param Logger $logger      : logger
     * @param string $objet
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function recupererAttestationFromApiGouvEntreprise(
        $identifiant,
        $type,
        \Symfony\Bridge\Monolog\Logger $logger,
        $objet = null,
        $host = null,
        $version = false
    ) {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $typeDocQuery = new CommonTDocumentTypeQuery();
            if (!$host) {
                $host = Atexo_Config::getParameter('URL_API_GOUV_ENTREPRISE');
            }

            $token = Atexo_Config::getParameter('API_ENTREPRISE_TOKEN');

            [$wsClassname, $uri, $paramUri, $code, $typeDocEtpEtab] = $typeDocQuery->recupererParamsWsByTypeDoc($type, $connexion);

            $uri = ($version) ? '/' . $version . $uri : $uri;
            $uri = str_replace(['{siren}', '{siret}'], [$identifiant, $identifiant], $uri);

            $listeParamsReplace = self::getListeParamsRequeteWs($objet);
            $listeParamsUri = self::replaceParamsWs(self::getListeParamsUri($paramUri), $listeParamsReplace);
            $paramsTypeDoc = ['idTypeDoc' => $type, 'wsClassname' => $wsClassname, 'uri' => $uri, 'paramUri' => $paramUri, 'code' => $code, 'typeDocEtpEtab' => $typeDocEtpEtab, 'listeParamsUri' => $listeParamsUri];

            $logger->info("Appel du contructeur de la classe '$wsClassname' avec les parametres : " . PHP_EOL . "identifiant = $identifiant" . PHP_EOL . "host = $host" . PHP_EOL . "token = $token" . PHP_EOL . 'paramsTypeDoc = ' . print_r($paramsTypeDoc, true));

            /** @var ApiEntreprise $apiEntrepriseService */
            $apiEntrepriseService = Atexo_Util::getSfService(ApiEntreprise::class);
            $headers = $apiEntrepriseService->getHeaderParams();

            $wsObject = new $wsClassname($identifiant, $host, $token, $paramsTypeDoc, Atexo_LoggerManager::getLogger('webservices'));
            $erreur = !$wsObject->request($host . $uri, Request::METHOD_GET, $headers)->isSuccessful();
            if ($erreur) {
                $logger->info("Raison de l'echec du WS: " . $wsObject->getErrorReason());
            }
            $reponseWs = $wsObject->getContentResponse();
            $statusWs = $wsObject->getStatusCode();
            $logger->info("Retour du WS: $reponseWs");
            $logger->info("Statut du retour du WS: $statusWs");

            return $wsObject->traiterRetourWs($statusWs, json_decode($reponseWs, true, 512, JSON_THROW_ON_ERROR), (new Atexo_Config())->toPfEncoding($reponseWs));
        } catch (Exception $e) {
            $codeRetour = Atexo_Oversight_Oversight::$codeErrorRemoteServiceDoesNotRespond;
            $logger->error("Erreur recuperation du document via WS : type = $type , identifiant = $identifiant, " . PHP_EOL . PHP_EOL . 'Erreur : ' . $e->getMessage() . PHP_EOL . PHP_EOL . 'Trace : ' . $e->getTraceAsString() . PHP_EOL . PHP_EOL . 'Methode = Atexo_Entreprise_Attestation::recupererAttestationFromApiGouvEntreprise');
        }
    }

    /**
     * Permet de construire un tableau de liste de parametres.
     *
     * @param string $paramUri
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    private static function getListeParamsUri($paramUri)
    {
        $listeParamsUri = [];
        if (!empty($paramUri)) {
            $listeParamsUri1 = explode(',', $paramUri);
            if (is_array($listeParamsUri1) && !empty($listeParamsUri1)) {
                foreach ($listeParamsUri1 as $param1) {
                    if (!empty($param1)) {
                        $listeParamsUri2 = explode('=', trim($param1));
                        if (is_array($listeParamsUri2) && !empty($listeParamsUri2)) {
                            $listeParamsUri[trim($listeParamsUri2[0])] = trim($listeParamsUri2[1]);
                        }
                    }
                }
            }
        }

        return $listeParamsUri;
    }

    /**
     * Permet de recuperer le contenu d'un document dans le zip du retour du ws agregateur.
     *
     * @param array  $arrayReponseWs : le retour du ws agregateur decode en json
     * @param string $codeTypeDoc    : code du type de document
     * @param string $identifiant    : siren ou siret
     * @param Logger $loggerWs       : logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function recupererContenuDocumentWsAgregateur($zipFile, $codeTypeDoc, $identifiant, $loggerWs)
    {
        try {
            if (is_file($zipFile)) {
                $zip = new \ZipArchive();
                if (true === $zip->open($zipFile)) {
                    $nomDocumentDansArchiveZipAgregateur = $codeTypeDoc . "-$identifiant.pdf";
                    $loggerWs->info("Nom du fichier a recuperer dans l'archive zip agregateur : $nomDocumentDansArchiveZipAgregateur");
                    $contenuDocDansArchiveZipAgregateur = $zip->getFromName($nomDocumentDansArchiveZipAgregateur);
                    $zip->close();
                    if (!empty($contenuDocDansArchiveZipAgregateur)) {
                        $loggerWs->info("Le fichier '$nomDocumentDansArchiveZipAgregateur' est non vide.");

                        return $contenuDocDansArchiveZipAgregateur;
                    }
                    $loggerWs->info("Le fichier '$zipFile' est vide.");
                } else {
                    $loggerWs->info("IMPOSSIBLE d'ouvrir le fichier archive zip : $zipFile");
                }
            }

            return false;
        } catch (Exception $e) {
            $fichier = $codeTypeDoc . "-$identifiant.pdf";
            $erreur = PHP_EOL . PHP_EOL . 'Erreur: ' . $e->getMessage() . PHP_EOL . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
            $loggerWs->error("Erreur lors de la recuperation du contenu du fichier '$fichier' dans l'archive zip agregateur '$zipFile' : code_type_document=$codeTypeDoc , identifiant=$identifiant" . $erreur);

            return false;
        }
    }

    /**
     * Permet d'extraire le fichier zip agregateur depuis l'url et le copie sur le serveur.
     *
     * @param array  $arrayReponseWs : le retour du ws agregateur decode en json
     * @param Logger $loggerWs
     *
     * @return bool|string : chemin du fichier zip agregateur, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function extractZipFileFromContentsWsAgreggator($arrayReponseWs, $loggerWs): bool|string
    {
        try {
            $loggerWs->info("Extraction du zip agregateur depuis l'url et copie sur le tmp.");
            $zipUrl = $arrayReponseWs['zip_archive']['url'];
            $loggerWs->info("Url du zip = $zipUrl");
            if (!empty($zipUrl)) {
                if ($context = self::getConfigProxyForStream()) {
                    $contentZip = file_get_contents($zipUrl, false, $context);
                } else {
                    $contentZip = file_get_contents($zipUrl);
                }

                if (!empty($contentZip)) {
                    $tmpZipFile = Atexo_Config::getParameter('COMMON_TMP') . 'agreggator_' . session_id() . uniqid() . time() . '.zip';
                    Atexo_Util::writeFile($tmpZipFile, $contentZip);
                    if (is_file($tmpZipFile)) {
                        $loggerWs->info("Chemin fichier zip agregateur = '$tmpZipFile'");

                        return $tmpZipFile;
                    }
                    $loggerWs->info('Le chemin du fichier zip agregateur est non valide.');
                } else {
                    $loggerWs->info('Le contenu du zip agregateur est vide.');
                }
            } else {
                $loggerWs->info("L'Url du zip agregateur est vide.");
            }

            return false;
        } catch (Exception $e) {
            $erreur = PHP_EOL . PHP_EOL . 'Erreur: ' . $e->getMessage() . PHP_EOL . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
            $loggerWs->error("Erreur lors de l'extraction du fichier archive zip agregateur dans la reponse du WS Agregateur. : reponse ws decode=" . print_r($arrayReponseWs, true) . $erreur);

            return false;
        }
    }

    /**
     * Permet de verifier si le document verifie les conditions de synchronisation.
     *
     * @param CommonTDocumentEntreprise        $document            : le document
     * @param CommonTEntrepriseDocumentVersion $lastVersionDocument : la derniere version du document
     * @param Logger                           $logger              : logger
     *
     * @return bool : true si document respecte les conditions de synchronisation, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function isDocumentSynchronisable($document, $lastVersionDocument, $logger, $forceSynchro = false)
    {
        if (true === $forceSynchro) {
            $logger->info('La synchronisation du document a ete forcee.');

            return true;
        }
        if (!$document || ($lastVersionDocument && Atexo_Util::diffMois(date('d/m/Y'), $lastVersionDocument->getDateRecuperation('d/m/Y')) > Atexo_Config::getParameter('NBR_MOIS_TO_SYNCHRONISE_WITH_API_ATTESTATION'))) {
            $logger->info('Le document respecte les conditions de synchronisation.');

            return true;
        }
        $logger->info('Le document ne respecte pas les conditions de synchronisation.');

        return false;
    }

    /**
     * Permet de remplacer des parametres par leurs valeurs.
     *
     * @return array
     */
    public static function replaceParamsWs(array $listeParamsToReplace, array $listeParamsReplace)
    {
        if (!empty($listeParamsToReplace)) {
            foreach ($listeParamsToReplace as $keyParam => $valueParam) {
                if (!empty($valueParam)) {
                    foreach ($listeParamsReplace as $keyReplace => $valueReplace) {
                        if ($valueParam == $keyReplace) {
                            $valueParam = $valueReplace;
                        }
                    }
                    $listeParamsToReplace[$keyParam] = $valueParam;
                }
            }
        }

        return $listeParamsToReplace;
    }

    /**
     * Permet calculer le nombre de dernieres versions de documents.
     *
     * @param string $idEntreprise   : identifiant de l'entreprise
     * @param string $natureDocument : nature du document
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function recupererNombreDernieresVersionsDocuments($idEntreprise = null, $natureDocument = null)
    {
        $queryClauseNatureDocument = '';
        $queryClauseIdEntreprise = '';
        $nbrTotalDocSecondaires = 0;

        if (!empty($natureDocument)) {
            $queryClauseNatureDocument = ' AND TD.nature_document = :natureDocument';
        }
        if (!empty($idEntreprise)) {
            $queryClauseIdEntreprise = ' AND DE.id_entreprise = :idEntreprise';
        }

        $queryFinal = <<<SQL
			SELECT TD.id_type_document, COUNT(TD.id_type_document) as nbr_id_doc
			FROM t_document_type TD, t_document_entreprise DE
			WHERE TD.id_type_document = DE.id_type_document
			 AND TD.afficher_type_doc='1' $queryClauseIdEntreprise $queryClauseNatureDocument
			GROUP BY TD.id_type_document;
SQL;

        $params = [];
        if (!empty($natureDocument)) {
            $params[':natureDocument'] = $natureDocument;
        }
        if (!empty($idEntreprise)) {
            $params[':idEntreprise'] = $idEntreprise;
        }

        $statement = Atexo_Db::getLinkCommon(true)->prepare($queryFinal);
        $statement->execute($params);
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($results as $row) {
            $nbrTotalDocSecondaires += $row['nbr_id_doc'];
        }

        return $nbrTotalDocSecondaires;
    }

    /**
     * Permet de recuperer le document de l'etablissement.
     *
     * @param int    $idEtablissement l'id de l'etablissement
     * @param string $type            le type d'attestation
     *
     * @return CommonTDocumentEntreprise le document de l'etablissement s'il existe sinon boolean false
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function retrieveDocumentEtablissement($idEtablissement, $type)
    {
        $document = CommonTDocumentEntrepriseQuery::create()->filterByIdTypeDocument($type)->filterByIdEtablissement($idEtablissement)->findOne();
        if ($document instanceof CommonTDocumentEntreprise) {
            return $document;
        }

        return false;
    }

    /**
     * Permet de remplacer des parametres dans la requete.
     *
     * @param $objet
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getListeParamsRequeteWs($objet)
    {
        $listeParams = [];
        $listeParams['{__object__}'] = $objet;
        $listeParams['{__recipient__}'] = Atexo_Config::getParameter('API_ENTREPRISE_SIRET_CLIENT');

        return $listeParams;
    }

    /**
     * Permet de retourner le chemin  d'un document à partir de son json.
     *
     * @param int    $idTypeDocument
     * @param string $raisonSociale
     * @param string $siren
     * @param string $codeEtablissement
     * @param mixed  $json
     *
     * @return string le chemin du fichier sur le disque
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function getPathAttestation($document, $json, $dateRecuperation)
    {
        $idTypeDocument = $document->getIdTypeDocument();
        $pathFile = '';
        $pathFile = match ($idTypeDocument) {
            Atexo_Config::getParameter('ID_CERTIFICAT_QUALIFICATION_OPQIBI') => self::generateOdtCertificatQualificationOPQIBI($document, $json, $dateRecuperation),
            Atexo_Config::getParameter('ID_COISATION_MSA') => self::generateOdtCotisationMSA($document, $json, $dateRecuperation),
            Atexo_Config::getParameter('ID_ATTESTATION_AGEFIPH') => self::generateOdtAgefiph($document, $json, $dateRecuperation),
            default => $pathFile,
        };

        return $pathFile;
    }

    /**
     * Permet de generer et enregistrer le pd d'un document.
     *
     * @param CommonTEntrepriseDocumentVersion $versionDocument
     * @param CommonTDocumentEntreprise        $document
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function genererAttestationFromJson(&$versionDocument, $document)
    {
        $idTypeDocument = $document->getIdTypeDocument();
        try {
            if ($document instanceof CommonTDocumentEntreprise && ($versionDocument instanceof CommonTEntrepriseDocumentVersion)) {
                $fileName = $document->getNomDocument() . Atexo_Config::getParameter('PDF_EXTENTION');
                $json = Atexo_Util::toUtf8($versionDocument->getJsonDocument());
                $data = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
                $pathFile = self::getPathAttestation($document, $data, $versionDocument->getDateRecuperation('d/m/Y'));
                if ($pathFile) {
                    $filePdf = Atexo_Util::generatePdf($pathFile, $fileName);
                    if ($filePdf) {
                        $atexoBlob = new Atexo_Blob();
                        $idBlob = $atexoBlob->insert_blob($fileName, $filePdf, Atexo_Config::getParameter('COMMON_BLOB'));
                        if ('0' != $idBlob) {
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $tailleFile = $atexoBlob->getTailFile($idBlob, Atexo_Config::getParameter('COMMON_BLOB'));
                            $versionDocument->setTailleDocument(Atexo_Util::GetSizeName($tailleFile));
                            $data = file_get_contents($filePdf);
                            $versionDocument->setHash(md5($data));
                            $versionDocument->setIdBlob($idBlob);
                            $versionDocument->setExtensionDocument(Atexo_Config::getParameter('PDF_EXTENTION'));
                            $versionDocument->save($connexion);
                            @unlink($pathFile);
                            @unlink($filePdf);
                        } else {
                            throw new Exception("Erreur d'enregistrement du blob");
                        }
                    }
                }
            }
        } catch (Exception $e) {
            Prado::log(
                'Erreur lors du generation de document from json : ' . $idTypeDocument . ' Message erreur : ' . $e->getMessage() . " \n" . $e->getTraceAsString(),
                TLogger::ERROR,
                'Attestation.php'
            );
        }
    }

    /**
     * Permet de generer l'odt de la cotisation MSA.
     *
     * @param string $raisonSociale
     * @param string $siren
     * @param string $codeEtablissement
     * @param mixed  $json
     *
     * @return string le chemin du fichier odt sur le disque
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function generateOdtCotisationMSA($document, $json, $dateRecuperation)
    {
        if ($json) {
            $raisonSociale = $document->getRaisonSociale();
            $siren = $document->getSiren();
            $codeEtablissement = $document->getCodeEtablissement();
            $fileModel = Atexo_Config::getParameter('TEMPLATE_COISATION_MSA');
            if (is_file($fileModel)) {
                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP') . '/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP') . '/tmpOdt');
                }
                $config = [
                        'ZIP_PROXY' => \PhpZipProxy::class,
                        'DELIMITER_LEFT' => '{',
                        'DELIMITER_RIGHT' => '}',
                        'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP') . '/tmpOdt',
                ];
                $odf = new Atexo_Odf($fileModel, $config);
                $odf->setVars('RaisonSociale', $raisonSociale, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('SIREN', $siren, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('CodeEtablissement', $codeEtablissement, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $miseAjour = ($json['a_jour'] && Atexo_Util::castToBoolean($json['a_jour'])) ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON');
                $analyseEncours = ($json['analyse_en_cours'] && Atexo_Util::castToBoolean($json['analyse_en_cours'])) ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON');
                $odf->setVars('MiseAjour', $miseAjour, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('AnalyseEnCours', $analyseEncours, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('DateRecuperation', $dateRecuperation, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $pathFile = Atexo_Config::getParameter('COMMON_TMP') . 'Cotisation_employeur_MSA_' . '_' . session_id() . time() . '_fichier.odt';
                $odf->saveToDisk($pathFile);

                return $pathFile;
            } else {
                return '';
            }
        }
    }

    /**
     * Permet de generer l'odt d'Eligibilité cotisation retraite PROBTP.
     *
     * @param string $raisonSociale
     * @param string $siren
     * @param string $codeEtablissement
     * @param mixed  $json
     *
     * @return string le chemin du fichier odt sur le disque
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    /*
    public function generateEligibiliteCotisationRetraite($raisonSociale,$siren,$codeEtablissement,$json )
    {
        if($json){
            $fileModel = Atexo_Config::getParameter('TEMPLATE_PROBTP_ELIGIBLITE_COTISATION');
            if(is_file($fileModel)){
                if (!is_dir(Atexo_Config::getParameter("COMMON_TMP") . '/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter("COMMON_TMP") . '/tmpOdt');
                }
                $config = array(
                        'ZIP_PROXY' => 'PhpZipProxy',
                        'DELIMITER_LEFT' => '{',
                        'DELIMITER_RIGHT' => '}',
                        'PATH_TO_TMP' => Atexo_Config::getParameter("COMMON_TMP") . '/tmpOdt'
                );
                $odf = new Atexo_Odf($fileModel, $config);
                $odf->setVars('RaisonSociale', $raisonSociale, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('SIREN', $siren, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('CodeEtablissement', $codeEtablissement, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $eligible = ($json['eligible'] && Atexo_Util::castToBoolean($json['eligible']))?Prado::localize('DEFINE_OUI'):Prado::localize('DEFINE_NON');
                $odf->setVars('Eligible', $eligible, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('Message', $json['message'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $pathFile = Atexo_Config::getParameter("COMMON_TMP")."Eligibilite_cotisation_retraite_PROBTP" ."_".session_id() . time()."_fichier.odt";
                $odf->saveToDisk($pathFile);
                return $pathFile;
            }else{
                return "";
            }
        }
    }*/

    /**
     * Permet de generer l'odt de la cotisation MSA.
     *
     * @param string $raisonSociale
     * @param string $siren
     * @param string $codeEtablissement
     * @param mixed  $json
     *
     * @return string le chemin du fichier odt sur le disque
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function generateOdtCertificatQualificationOPQIBI($document, $json, $dateRecuperation)
    {
        if ($json) {
            $raisonSociale = $document->getRaisonSociale();
            $siren = $document->getSiren();
            $fileModel = Atexo_Config::getParameter('TEMPLATE_CERTIFICAT_QUALIFICATION_OPQIBI');
            if (is_file($fileModel)) {
                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP') . '/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP') . '/tmpOdt');
                }
                $config = [
                        'ZIP_PROXY' => \PhpZipProxy::class,
                        'DELIMITER_LEFT' => '{',
                        'DELIMITER_RIGHT' => '}',
                        'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP') . '/tmpOdt',
                ];
                $odf = new Atexo_Odf($fileModel, $config);
                $odf->setVars('NumeroCertificat', $json['numero_certificat'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $dateDelivrance = $json['date_de_delivrance_du_certificat'];
                $odf->setVars('DateDelivrance', $dateDelivrance, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('DureeValidite', $json['duree_de_validite_du_certificat'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('RaisonSociale', $raisonSociale, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('SIREN', $siren, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('URL', $json['url'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('NomAssurance', $json['assurance'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $dateValiditeQualification = $json['date_de_validite_des_qualifications'];
                $odf->setVars('DateValiditeQualifications', $dateValiditeQualification, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                //print_r($json);exit;
                $qualifications = $json['qualifications'];
                $segmentQualification = $odf->setSegment('ligneQualification');
                if (is_array($qualifications) && count($qualifications)) {
                    foreach ($qualifications as $qualification) {
                        $segmentQualification->setVars('CodeQualification', $qualification['code_qualification'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segmentQualification->setVars('NomQualification', $qualification['nom'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segmentQualification->setVars('Description', str_replace('<br />', "\n", $qualification['definition']), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segmentQualification->setVars('Rge', $qualification['rge'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segmentQualification->merge();
                    }
                }
                $odf->mergeSegment($segmentQualification);

                $dateValiditeQualificationProb = $json['date_de_validite_des_qualifications_probatoires'];
                $odf->setVars('DateValiditeQualificationsProbatoires', $dateValiditeQualificationProb, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $qualificationsProbatoires = $json['qualifications_probatoires'];

                $segmentProb = $odf->setSegment('ligneQualificationProbatoires');
                if (is_array($qualificationsProbatoires) && count($qualificationsProbatoires)) {
                    foreach ($qualificationsProbatoires as $qualificationsProbatoire) {
                        $segmentProb->setVars('CodeQualificationProbatoires', $qualificationsProbatoire['code_qualification'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segmentProb->setVars('NomQualificationProbatoires', $qualificationsProbatoire['nom'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segmentProb->setVars('DescriptionProbatoires', str_replace('<br />', "\n", $qualificationsProbatoire['definition']), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segmentProb->setVars('RgeProbatoires', $qualificationsProbatoire['rge'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segmentProb->merge();
                    }
                }
                $odf->mergeSegment($segmentProb);

                $odf->setVars('DateRecuperation', $dateRecuperation, true, Atexo_Config::getParameter('HTTP_ENCODING'));

                $pathFile = Atexo_Config::getParameter('COMMON_TMP') . 'Cotisation_employeur_MSA_' . '_' . session_id() . time() . '_fichier.odt';
                $odf->saveToDisk($pathFile);

                return $pathFile;
            } else {
                return '';
            }
        }
    }

    /**
     * Permet de generer l'odt de la cotisation MSA.
     *
     * @param CommonTDocumentEntreprise $document
     * @param mixed                     $json
     *
     * @return string le chemin du fichier odt sur le disque
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public function generateOdtAgefiph($document, $json, $dateRecuperation)
    {
        if ($json && $document instanceof CommonTDocumentEntreprise) {
            $raisonSociale = $document->getRaisonSociale();
            $siren = $document->getSiren();
            $codeEtablissement = $document->getCodeEtablissement();
            $fileModel = Atexo_Config::getParameter('TEMPLATE_AGEFIPH');
            if (is_file($fileModel)) {
                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP') . '/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP') . '/tmpOdt');
                }
                $config = [
                    'ZIP_PROXY' => \PhpZipProxy::class,
                    'DELIMITER_LEFT' => '{',
                    'DELIMITER_RIGHT' => '}',
                    'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP') . '/tmpOdt',
                ];
                $odf = new Atexo_Odf($fileModel, $config);
                $odf->setVars('raison_sociale', $raisonSociale, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('SIREN', $siren, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('code_etablissement', $codeEtablissement, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('DateRecuperation', $dateRecuperation, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('derniere_annee_de_conformite_connue', $json['derniere_annee_de_conformite_connue'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('dump_date', Atexo_Util::timestampToFormat($json['dump_date'], 'd/m/Y'), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $pathFile = Atexo_Config::getParameter('COMMON_TMP') . 'Agefiph_' . '_' . session_id() . time() . '_fichier.odt';
                $odf->saveToDisk($pathFile);

                return $pathFile;
            } else {
                return '';
            }
        }
    }

    /**
     * Permet d'associer les attestations de l'entreprise ou l'etablissement a une offre.
     *
     * @param int $idOffre
     * @param int $id
     * @param int $type
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function attacheAttestationToOffre($idOffre, $id, $type)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $docTypesQuery = new CommonTDocumentTypeQuery();
        $docTypes = $docTypesQuery->getTypesDocumentsByTypesEntrepriseEtablissement($type, $connexion);
        if (is_array($docTypes) && !empty($docTypes)) {
            foreach ($docTypes as $typeDocumentObjet) {
                $document = null;
                if ($typeDocumentObjet instanceof CommonTDocumentType) {
                    if ($typeDocumentObjet->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')) {
                        $document = self::retrieveDocumentEntreprise($id, $typeDocumentObjet->getIdTypeDocument());
                    } elseif ($typeDocumentObjet->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT')) {
                        $document = self::retrieveDocumentEtablissement($id, $typeDocumentObjet->getIdTypeDocument());
                    }
                }
                if ($document instanceof CommonTDocumentEntreprise && $document->getIdDerniereVersion()) {
                    $idDocument = $document->getIdDocument();
                    $idDocumentLastVersion = $document->getIdDerniereVersion();
                    $attestationOffreQuery = new CommonTAttestationsOffresQuery();
                    $atsOffre = $attestationOffreQuery->getAttestationOffre($idOffre, $typeDocumentObjet->getIdTypeDocument(), $connexion);
                    if (!$atsOffre instanceof CommonTAttestationsOffres) {
                        $atsOffre = new CommonTAttestationsOffres();
                        $atsOffre->setIdOffre($idOffre);
                        $atsOffre->setIdTypeDocument($typeDocumentObjet->getIdTypeDocument());
                    }
                    $atsOffre->setIdDocumentEntreprise($idDocument);
                    $atsOffre->setIdDocumentVersion($idDocumentLastVersion);
                    $atsOffre->save($connexion);
                }
            }
        }
    }
}
