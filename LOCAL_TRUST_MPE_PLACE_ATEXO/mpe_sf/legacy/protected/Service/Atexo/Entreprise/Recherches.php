<?php

namespace Application\Service\Atexo\Entreprise;

use App\Service\DataTransformer\SearchCriteriaTransformer;
use App\Service\PlateformeVirtuelle\Context;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTMesRecherches;
use Application\Propel\Mpe\CommonTMesRecherchesPeer;
use Application\Propel\Mpe\CommonTMesRecherchesQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Recherche\Atexo_Recherche_CriteriaVo;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe de manipulation des alertes.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise_Recherches
{
    /**
     * permet de recuperer l'url de telechargement du DIC selon le parametrage.
     *
     * @param int    $idCreateur   l'id du createur de la recherche favorite
     * @param string $typeCreateur le type du createur de la recherche favorite(agent,Entreprise)
     * @param bool   $forMenu      mettre a true pr affichage menu(15 element max)
     *
     * @return array $recherchesList liste des objet CommonTMesRecherches
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function retreiveRecherches($idCreateur, $typeCreateur, $forMenu = false, $alertesOnly = false, $typeAvis = 'all')
    {
        $result = self::getMesRecherche(
            $idCreateur,
            $typeCreateur,
            $alertesOnly,
            $typeAvis,
            $forMenu
        );

        return $result;
    }

    public function getMesRecherche($idCreateur, $typeCreateur, $alertesOnly, $typeAvisCons, $forMenu)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
            .Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonTMesRecherchesPeer::ID_CREATEUR, $idCreateur);
        $c->add(CommonTMesRecherchesPeer::TYPE_CREATEUR, $typeCreateur);
        if ($alertesOnly) {
            $c->add(CommonTMesRecherchesPeer::ALERTE, '1');
        }
        if ('consultation' == $typeAvisCons) {
            $c->add(
                CommonTMesRecherchesPeer::TYPE_AVIS,
                Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')
            );
        } elseif ('annonce' == $typeAvisCons) {
            $c->add(
                CommonTMesRecherchesPeer::TYPE_AVIS,
                Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'),
                Criteria::NOT_EQUAL
            );
        }
        if ($forMenu) {
            $c->setLimit(Atexo_Config::getParameter('NBRE_MAX_MENUS_RECHERCHES_FAVORITES'));
            $c->addDescendingOrderByColumn(CommonTMesRecherchesPeer::DATE_MODIFICATION);
        }
        $recherchesList = CommonTMesRecherchesPeer::doSelect($c, $connexionCom);
        if ($recherchesList) {
            return $recherchesList;
        } else {
            return [];
        }
    }

    public function getMesRechercheFromCache(
        $token,
        $idCreateur,
        $typeCreateur,
        $forMenu,
        $alertesOnly,
        $typeAvisCons
    ) {
        $application = Prado::getApplication();

        if (!$rechercheList = $application->Cache->get($token)) {
            $rechercheList = self::getMesRecherche(
                $idCreateur,
                $typeCreateur,
                $alertesOnly,
                $typeAvisCons,
                $forMenu
            );
            $application->Cache->add($token, $rechercheList);
        }

        return $rechercheList ?? [];
    }

    /**
     * permet de supprimer une recherche favorite sauvegardée.
     *
     * @param int $idRecherche l'id recherche favorite sauvegardée asupprimer
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function deleteMaRecherche($idRecherche)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $rechercheQuery = new CommonTMesRecherchesQuery();
        $rechercheObj = $rechercheQuery->findOneById($idRecherche);
        if ($rechercheObj) {
            self::deleteRechercheFromCache($rechercheObj);
        }
        CommonTMesRecherchesPeer::doDelete($idRecherche, $connexionCom);
    }

    /**
     * permet de Modifier le nom d'une recherche favorite sauvegardée.
     *
     * @param int    $idRecherche l'id recherche favorite sauvegardée asupprimer
     * @param string $newName     le nouveau nom de la recherche favorite
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function ModifierMaRecherche($idRecherche, $newName)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $maRecherche = self::retreiveRechercheById($idRecherche);
        $maRecherche->setDenomination($newName);
        $maRecherche->setDateModification(date('Y-m-d H:i:s'));
        $maRecherche->save($connexionCom);
    }

    /**
     * permet de recuperer une recherche favorite sauvegardée.
     *
     * @param int $idRecherche l'id recherche favorite sauvegardée a retourner
     *
     * @return CommonTMesRecherches $RechercheObj l'objet recherche
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function retreiveRechercheById($idRecherche)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTMesRecherchesPeer::ID, $idRecherche);
        $RechercheObj = CommonTMesRecherchesPeer::doSelectOne($c, $connexionCom);
        if ($RechercheObj) {
            return $RechercheObj;
        } else {
            return false;
        }
    }

    /**
     * permet de d'avoir un criteria de recherche depuis une recherche favorite sauvegardée.
     *
     * @param CommonTMesRecherches $recherche l'l'objet recherche
     *
     * @return Atexo_Consultation_CriteriaVo $criteriaObject le criteria de recherche
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getCriteriaFromObject($recherche)
    {
        if ('2018-07-19' <= $recherche->getDateModification()) {
            $serializedObject = $recherche->getxmlCriteria();
        } else {
            $serializedObject = Atexo_Util::utf8ToIso($recherche->getxmlCriteria());
        }

        return self::unserializeCriteria($serializedObject);
    }

    public function unserializeCriteria($serializedObject)
    {
        return unserialize($serializedObject);
    }

    public function castStdClass($destination, $stdObject)
    {
        if (is_string($destination)) {
            $destination = new $destination();
        }
        foreach ($stdObject as $property => &$value) {
            $set = 'set'.ucfirst(ltrim($property, '_'));
            $destination->$set($value);
        }

        return $destination;
    }

    /**
     * permet de d'enregistrer une recherche favorite.
     *
     * @param Atexo_Consultation_CriteriaVo $criteriaVo            le criteria de recherche
     * @param Atexo_Recherche_CriteriaVo    $criteriaMesRecherches le criteria complementaire de la recherche
     *
     * @return CommonTMesRecherches $rechercheObj l'objet sauvegardé
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function saveRecherche($criteriaVo, $criteriaMesRecherches)
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if ($criteriaMesRecherches->getIdRecherche()) {
                $rechercheObj = CommonTMesRecherchesPeer::retrieveByPK($criteriaMesRecherches->getIdRecherche(), $connexionCom);
            } else {
                $rechercheObj = new CommonTMesRecherches();
                $rechercheObj->setDateCreation(date('Y-m-d H:i:s'));
            }
            $rechercheObj->setRecherche($criteriaMesRecherches->getRecherche());
            if ($criteriaMesRecherches->getAlerte()) {
                $rechercheObj->setAlerte($criteriaMesRecherches->getAlerte());
                $rechercheObj->setPeriodicite($criteriaMesRecherches->getPeriodicite());
                $rechercheObj->setFormat($criteriaMesRecherches->getFormat());
            } else {
                $rechercheObj->setAlerte('0');
                $rechercheObj->setPeriodicite(Atexo_Config::getParameter('PERIODICITE_TOUTES_SEMAINES'));
            }
            $rechercheObj->setIdCreateur($criteriaMesRecherches->getIdCreateur());
            $rechercheObj->setTypeCreateur($criteriaMesRecherches->getTypeCreateur());
            $rechercheObj->setDenomination($criteriaMesRecherches->getNomRecherche());
            $serializedObject = serialize($criteriaVo);
            $rechercheObj->setTypeAvis($criteriaVo->getIdTypeAvis());
            $rechercheObj->setXmlCriteria($serializedObject);
            $transformService = Atexo_Util::getSfService(SearchCriteriaTransformer::class);
            $encodedCriteria = json_encode($transformService->transform($criteriaVo));
            $rechercheObj->setCriteria($encodedCriteria);
            $rechercheObj->setHashedCriteria(md5($encodedCriteria));

            /** @var Context $pfvContextService */
            $pfvContextService = Atexo_Util::getSfService(Context::class);
            $pfvId = $pfvContextService->getCurrentPlateformeVirtuelleId();
            $rechercheObj->setPlateformeVirtuelleId($pfvId);

            $rechercheObj->setDateModification(date('Y-m-d H:i:s'));
            $rechercheObj->save($connexionCom);
            self::deleteRechercheFromCache($rechercheObj);

            return $rechercheObj;
        } catch (Exception $e) {
            Prado::log(" Erreur lors de la sauvegarde d'une recherche favorite ".$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'advancedSearch.php');
        }
    }

    /**
     * Permet de recuperer les recherches (typeRecherche = RECHERCHE, correspond au champ "RECHERCHE" ='1' en base de donnees).
     *
     * @param string $nom:           nom de la recherche
     * @param string $idUtilisateur: identifiant technique d'agent ou de l'inscrit
     * @param string $isRecherche:   specifie si le type "RECHERCHE" est coché
     *
     * @return array si informations trouvees, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public static function retrieveRechercheByNomAndCreateur($nom, $idUtilisateur, $isRecherche)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTMesRecherchesPeer::DENOMINATION, $nom);
        $c->add(CommonTMesRecherchesPeer::ID_CREATEUR, $idUtilisateur);
        if ($isRecherche) {
            $c->add(CommonTMesRecherchesPeer::RECHERCHE, '1');
        }
        $recherchesList = CommonTMesRecherchesPeer::doSelect($c, $connexion);
        if (is_array($recherchesList) && $recherchesList[0] instanceof CommonTMesRecherches) {
            return $recherchesList;
        } else {
            return false;
        }
    }

    /**
     * permet de recuperer l'url de telechargement du DIC selon le parametrage.
     *
     * @param int    $idCreateur   l'id du createur de la recherche favorite
     * @param string $typeCreateur le type du createur de la recherche favorite(agent,Entreprise)
     * @param bool   $forMenu      mettre a true pr affichage menu(15 element max)
     *
     * @return array $recherchesList liste des objet CommonTMesRecherches
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function retreiveRecherchesByIdIdCreateur($idRecherche, $idCreateur)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTMesRecherchesPeer::ID, $idRecherche);
        $c->add(CommonTMesRecherchesPeer::ID_CREATEUR, $idCreateur);
        $resultat = CommonTMesRecherchesPeer::doSelectOne($c, $connexionCom);
        if ($resultat) {
            return $resultat;
        } else {
            return false;
        }
    }

    /**
     * Permet de supprimer une rechreche du cache.
     *
     * @param CommonTMesRecherches $recherche l'objet de la recherche
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public static function deleteRechercheFromCache($recherche)
    {
        if ($recherche instanceof CommonTMesRecherches) {
            if ($recherche->getTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                $typeAvis = 'consultation';
            } else {
                $typeAvis = 'annonce';
            }
            $token = session_id().'_retreiveRecherches_'.$recherche->getIdCreateur().
                '_'.$recherche->getTypeCreateur().'_'.$typeAvis.'_';
            $tokenAll = session_id().'_retreiveRecherches_'.$recherche->getIdCreateur().
                '_'.$recherche->getTypeCreateur().'_all_';
            if (Atexo_Config::getParameter('CACHE_RECHERCHES_FAVORITES')) {
                $application = Prado::getApplication();
                if ($application->Cache) {
                    $application->Cache->delete($token.true);
                    $application->Cache->delete($token.false);
                    $application->Cache->flush();
                    $application->Cache->delete($tokenAll.true);
                    $application->Cache->delete($tokenAll.false);
                    $application->Cache->flush();
                }
            } else {
                $arraySession = Atexo_CurrentUser::readFromSession('RECHERCHES_FAVORITES');
                if (!empty($arraySession)) {
                    Atexo_CurrentUser::deleteFromSession('RECHERCHES_FAVORITES', $token.true);
                    Atexo_CurrentUser::deleteFromSession('RECHERCHES_FAVORITES', $token.false);
                    Atexo_CurrentUser::deleteFromSession('RECHERCHES_FAVORITES', $tokenAll.true);
                    Atexo_CurrentUser::deleteFromSession('RECHERCHES_FAVORITES', $tokenAll.false);
                }
            }
        }
    }
}
