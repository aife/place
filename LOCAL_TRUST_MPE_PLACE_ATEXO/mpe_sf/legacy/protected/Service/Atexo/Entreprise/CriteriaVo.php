<?php

namespace Application\Service\Atexo\Entreprise;

/**
 * Vo pour définir les critères de recherche des entreprises.
 *
 * @author Mouslim MITALI <mouslim/mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise_CriteriaVo
{
    private string $_raisonSociale = '';
    private string $_cpSiegeSocial = '';
    private string $_villeSiegeSocial = '';
    private string $_siren = '';
    private string $_siret = '';
    private string $_pays = '';
    private string $_idNational = '';
    private string $_apeNafNace = '';
    private string $_keywordSearch = '';
    private $_offset = 0;
    private $_limit = 0;
    private string $_sortByElement = '';
    private string $_sensOrderBy = '';
    private string $_idsEntrepriseIn = '';
    private bool $_rechercheDomaines = false;
    private string $_domaineActivite = '';
    private string $_qualification = '';
    private string $_agrements = '';
    private string $_region = '';
    private string $_province = '';
    private string $_dateEnd = '';
    private string $_dateStart = '';
    private string $_compteActif = '';
    private string $_typeCollaboration = '';
    private int $_visible_bourse = 0;
    private int $_entrepriseEA = 0;
    private int $_entrepriseSIAE = 0;

    //tableau d'objet referentielVo
    private array $referentielVo = [];

    // Accesseurs

    public function getRaisonSociale()
    {
        return $this->_raisonSociale;
    }

    public function setRaisonSociale($value)
    {
        $this->_raisonSociale = $value;
    }

    public function getCpSiegeSocial()
    {
        return $this->_cpSiegeSocial;
    }

    public function setCpSiegeSocial($value)
    {
        $this->_cpSiegeSocial = $value;
    }

    public function getVilleSiegeSocial()
    {
        return $this->_villeSiegeSocial;
    }

    public function setVilleSiegeSocial($value)
    {
        $this->_villeSiegeSocial = $value;
    }

    public function getSiren()
    {
        return $this->_siren;
    }

    public function setSiren($value)
    {
        $this->_siren = $value;
    }

    public function getSiret()
    {
        return $this->_siret;
    }

    public function setSiret($value)
    {
        $this->_siret = $value;
    }

    public function getPays()
    {
        return $this->_pays;
    }

    public function setPays($value)
    {
        $this->_pays = $value;
    }

    public function getIdNational()
    {
        return $this->_idNational;
    }

    public function setIdNational($value)
    {
        $this->_idNational = $value;
    }

    public function getApeNafNace()
    {
        return $this->_apeNafNace;
    }

    public function setApeNafNace($value)
    {
        $this->_apeNafNace = $value;
    }

    public function getKeywordSearche()
    {
        return $this->_keywordSearch;
    }

    public function setKeywordSearche($value)
    {
        $this->_keywordSearch = $value;
    }

    public function getOffset()
    {
        return $this->_offset;
    }

    public function setOffset($offset)
    {
        $this->_offset = $offset;
    }

    public function getLimit()
    {
        return $this->_limit;
    }

    public function setLimit($limit)
    {
        $this->_limit = $limit;
    }

    public function getSortByElement()
    {
        return $this->_sortByElement;
    }

    public function setSortByElement($sortByElement)
    {
        $this->_sortByElement = $sortByElement;
    }

    public function getSensOrderBy()
    {
        return $this->_sensOrderBy;
    }

    public function setSensOrderBy($sensOrderBy)
    {
        $this->_sensOrderBy = $sensOrderBy;
    }

    public function getIdsEntrepriseIn()
    {
        return $this->_idsEntrepriseIn;
    }

    public function setIdsEntrepriseIn($value)
    {
        $this->_idsEntrepriseIn = $value;
    }

    public function getRechercheDomaines()
    {
        return $this->_rechercheDomaines;
    }

    public function setRechercheDomaines($value)
    {
        $this->_rechercheDomaines = $value;
    }

    public function getDomaineActivite()
    {
        return $this->_domaineActivite;
    }

    public function setDomaineActivite($value)
    {
        $this->_domaineActivite = $value;
    }

    public function getQualification()
    {
        return $this->_qualification;
    }

    public function setQualification($value)
    {
        $this->_qualification = $value;
    }

    public function getAgrements()
    {
        return $this->_agrements;
    }

    public function setAgrements($value)
    {
        $this->_agrements = $value;
    }

    public function getRegion()
    {
        return $this->_region;
    }

    public function setRegion($value)
    {
        $this->_region = $value;
    }

    public function getProvince()
    {
        return $this->_province;
    }

    public function setProvince($value)
    {
        $this->_province = $value;
    }

    public function getDateCreationEnd()
    {
        return $this->_dateEnd;
    }

    public function setDateCreationEnd($value)
    {
        $this->_dateEnd = $value;
    }

    public function getDateCreationStart()
    {
        return $this->_dateStart;
    }

    public function setDateCreationStart($value)
    {
        $this->_dateStart = $value;
    }

    public function getCompteActif()
    {
        return $this->_compteActif;
    }

    public function setCompteActif($value)
    {
        $this->_compteActif = $value;
    }

    public function setReferentielVo($value)
    {
        $this->referentielVo = $value;
    }

    public function getReferentielVo()
    {
        return $this->referentielVo;
    }

    public function getTypeCollaboration()
    {
        return $this->_typeCollaboration;
    }

    public function setTypeCollaboration($value)
    {
        $this->_typeCollaboration = $value;
    }

    public function getVisibleBourse()
    {
        return $this->_visible_bourse;
    }

    public function setVisibleBourse($value)
    {
        $this->_visible_bourse = $value;
    }

    public function getEntrepriseEA()
    {
        return $this->_entrepriseEA;
    }

    public function setEntrepriseEA($value)
    {
        $this->_entrepriseEA = $value;
    }

    public function getEntrepriseSIAE()
    {
        return $this->_entrepriseSIAE;
    }

    public function setEntrepriseSIAE($value)
    {
        $this->_entrepriseSIAE = $value;
    }
}
