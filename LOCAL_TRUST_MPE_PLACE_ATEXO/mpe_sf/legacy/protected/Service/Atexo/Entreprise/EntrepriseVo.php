<?php

namespace Application\Service\Atexo\Entreprise;

use App\Service\ApiGateway\ApiEntreprise;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInscritQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use DateTime;
use Exception;

/**
 * Classe objet virtuel pour l'entreprise.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.9.0
 *
 * @copyright Atexo 2015
 */
class Atexo_Entreprise_EntrepriseVo
{
    protected $id;
    protected $admin_id;
    protected $siren;
    protected $repmetiers;
    protected $nom;
    protected $adresse;
    protected $codepostal;
    protected $villeadresse;
    protected $paysadresse;
    protected $email;
    protected $taille;
    protected $formejuridique;
    protected $villeenregistrement;
    protected $motifnonindnum;
    protected $ordreprofouagrement;
    protected $dateconstsociete;
    protected $nomorginscription;
    protected $adrorginscription;
    protected $dateconstassoc;
    protected $dateconstassocetrangere;
    protected $nompersonnepublique;
    protected $nationalite;
    protected $redressement;
    protected $paysenregistrement;
    protected $sirenetranger;
    protected $numassoetrangere;
    protected $debutexerciceglob1;
    protected $finexerciceglob1;
    protected $debutexerciceglob2;
    protected $finexerciceglob2;
    protected $debutexerciceglob3;
    protected $finexerciceglob3;
    protected $ventesglob1;
    protected $ventesglob2;
    protected $ventesglob3;
    protected $biensglob1;
    protected $biensglob2;
    protected $biensglob3;
    protected $servicesglob1;
    protected $servicesglob2;
    protected $servicesglob3;
    protected $totalglob1;
    protected $totalglob2;
    protected $totalglob3;
    protected $codeape;
    protected $origine_compte;
    protected $telephone;
    protected $fax;
    protected $site_internet;
    protected $description_activite;
    protected $activite_domaine_defense;
    protected $annee_cloture_exercice1;
    protected $annee_cloture_exercice2;
    protected $annee_cloture_exercice3;
    protected $effectif_moyen1;
    protected $effectif_moyen2;
    protected $effectif_moyen3;
    protected $effectif_encadrement1;
    protected $effectif_encadrement2;
    protected $effectif_encadrement3;
    protected $pme1;
    protected $pme2;
    protected $pme3;
    protected $adresse2;
    protected $nicsiege;
    protected $acronyme_pays;
    protected $date_creation;
    protected $date_modification;
    protected $id_initial;
    protected $region;
    protected $province;
    protected $telephone2;
    protected $telephone3;
    protected $cnss;
    protected $rc_num;
    protected $rc_ville;
    protected $domaines_activites;
    protected $num_tax;
    protected $documents_commerciaux;
    protected $intitule_documents_commerciaux;
    protected $taille_documents_commerciaux;
    protected $qualification;
    protected $agrement;
    protected $moyens_technique;
    protected $moyens_humains;
    protected $compte_actif;
    protected $capital_social;
    protected $ifu;
    protected $id_agent_createur;
    protected $nom_agent;
    protected $prenom_agent;
    protected $adresses_electroniques;
    protected $visible_bourse;
    protected $type_collaboration;
    protected int $entreprise_ea = 0;
    protected int $entreprise_siae = 0;
    protected ?array $commonInscrits = null;
    protected bool $saisieManuelle = false;
    protected $tvaIntracommunautaire;
    protected $formejuridiqueCode;
    protected $nombreEtablissementsActifs;
    protected $nomCommercial;
    protected $procedureCollective;
    protected $raisonSociale;
    protected $siretSiegeSocial;
    protected $idCodeEffectif;
    protected $exercices;
    protected $mandataires;
    protected array $etablissements = [];
    protected $listeEtablissements = null;
    protected ?string $libelleApe = null;
    protected $etablissementSiege = null;
    protected $created_from_decision;
    protected $categorieEntreprise;
    protected $etatAdministratif = null;
    protected $dateCessation = null;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getAdminId()
    {
        return $this->admin_id;
    }

    public function setAdminId($admin_id)
    {
        $this->admin_id = $admin_id;
    }

    public function getSiren()
    {
        return $this->siren;
    }

    public function setSiren($siren)
    {
        $this->siren = $siren;
    }

    public function getRepmetiers()
    {
        return $this->repmetiers;
    }

    public function setRepmetiers($repmetiers)
    {
        $this->repmetiers = $repmetiers;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function getAdresse()
    {
        return $this->adresse;
    }

    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    public function getCodepostal()
    {
        return $this->codepostal;
    }

    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;
    }

    public function getVilleadresse()
    {
        return $this->villeadresse;
    }

    public function setVilleadresse($villeadresse)
    {
        $this->villeadresse = $villeadresse;
    }

    public function getPaysadresse()
    {
        return $this->paysadresse;
    }

    public function setPaysadresse($paysadresse)
    {
        $this->paysadresse = $paysadresse;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getTaille()
    {
        return $this->taille;
    }

    public function setTaille($taille)
    {
        $this->taille = $taille;
    }

    public function getFormejuridique()
    {
        return $this->formejuridique;
    }

    public function setFormejuridique($formejuridique)
    {
        $this->formejuridique = $formejuridique;
    }

    public function getVilleenregistrement()
    {
        return $this->villeenregistrement;
    }

    public function setVilleenregistrement($villeenregistrement)
    {
        $this->villeenregistrement = $villeenregistrement;
    }

    public function getMotifnonindnum()
    {
        return $this->motifnonindnum;
    }

    public function setMotifnonindnum($motifnonindnum)
    {
        $this->motifnonindnum = $motifnonindnum;
    }

    public function getOrdreprofouagrement()
    {
        return $this->ordreprofouagrement;
    }

    public function setOrdreprofouagrement($ordreprofouagrement)
    {
        $this->ordreprofouagrement = $ordreprofouagrement;
    }

    public function getDateconstsociete()
    {
        return $this->dateconstsociete;
    }

    public function setDateconstsociete($dateconstsociete)
    {
        $this->dateconstsociete = $dateconstsociete;
    }

    public function getNomorginscription()
    {
        return $this->nomorginscription;
    }

    public function setNomorginscription($nomorginscription)
    {
        $this->nomorginscription = $nomorginscription;
    }

    public function getAdrorginscription()
    {
        return $this->adrorginscription;
    }

    public function setAdrorginscription($adrorginscription)
    {
        $this->adrorginscription = $adrorginscription;
    }

    public function getDateconstassoc()
    {
        return $this->dateconstassoc;
    }

    public function setDateconstassoc($dateconstassoc)
    {
        $this->dateconstassoc = $dateconstassoc;
    }

    public function getDateconstassocetrangere()
    {
        return $this->dateconstassocetrangere;
    }

    public function setDateconstassocetrangere($dateconstassocetrangere)
    {
        $this->dateconstassocetrangere = $dateconstassocetrangere;
    }

    public function getNompersonnepublique()
    {
        return $this->nompersonnepublique;
    }

    public function setNompersonnepublique($nompersonnepublique)
    {
        $this->nompersonnepublique = $nompersonnepublique;
    }

    public function getNationalite()
    {
        return $this->nationalite;
    }

    public function setNationalite($nationalite)
    {
        $this->nationalite = $nationalite;
    }

    public function getRedressement()
    {
        return $this->redressement;
    }

    public function setRedressement($redressement)
    {
        $this->redressement = $redressement;
    }

    public function getPaysenregistrement()
    {
        return $this->paysenregistrement;
    }

    public function setPaysenregistrement($paysenregistrement)
    {
        $this->paysenregistrement = $paysenregistrement;
    }

    public function getSirenetranger()
    {
        return $this->sirenetranger;
    }

    public function setSirenetranger($sirenetranger)
    {
        $this->sirenetranger = $sirenetranger;
    }

    public function getNumassoetrangere()
    {
        return $this->numassoetrangere;
    }

    public function setNumassoetrangere($numassoetrangere)
    {
        $this->numassoetrangere = $numassoetrangere;
    }

    public function getDebutexerciceglob1()
    {
        return $this->debutexerciceglob1;
    }

    public function setDebutexerciceglob1($debutexerciceglob1)
    {
        $this->debutexerciceglob1 = $debutexerciceglob1;
    }

    public function getFinexerciceglob1()
    {
        return $this->finexerciceglob1;
    }

    public function setFinexerciceglob1($finexerciceglob1)
    {
        $this->finexerciceglob1 = $finexerciceglob1;
    }

    public function getDebutexerciceglob2()
    {
        return $this->debutexerciceglob2;
    }

    public function setDebutexerciceglob2($debutexerciceglob2)
    {
        $this->debutexerciceglob2 = $debutexerciceglob2;
    }

    public function getFinexerciceglob2()
    {
        return $this->finexerciceglob2;
    }

    public function setFinexerciceglob2($finexerciceglob2)
    {
        $this->finexerciceglob2 = $finexerciceglob2;
    }

    public function getDebutexerciceglob3()
    {
        return $this->debutexerciceglob3;
    }

    public function setDebutexerciceglob3($debutexerciceglob3)
    {
        $this->debutexerciceglob3 = $debutexerciceglob3;
    }

    public function getFinexerciceglob3()
    {
        return $this->finexerciceglob3;
    }

    public function setFinexerciceglob3($finexerciceglob3)
    {
        $this->finexerciceglob3 = $finexerciceglob3;
    }

    public function getVentesglob1()
    {
        return $this->ventesglob1;
    }

    public function setVentesglob1($ventesglob1)
    {
        $this->ventesglob1 = $ventesglob1;
    }

    public function getVentesglob2()
    {
        return $this->ventesglob2;
    }

    public function setVentesglob2($ventesglob2)
    {
        $this->ventesglob2 = $ventesglob2;
    }

    public function getVentesglob3()
    {
        return $this->ventesglob3;
    }

    public function setVentesglob3($ventesglob3)
    {
        $this->ventesglob3 = $ventesglob3;
    }

    public function getBiensglob1()
    {
        return $this->biensglob1;
    }

    public function setBiensglob1($biensglob1)
    {
        $this->biensglob1 = $biensglob1;
    }

    public function getBiensglob2()
    {
        return $this->biensglob2;
    }

    public function setBiensglob2($biensglob2)
    {
        $this->biensglob2 = $biensglob2;
    }

    public function getBiensglob3()
    {
        return $this->biensglob3;
    }

    public function setBiensglob3($biensglob3)
    {
        $this->biensglob3 = $biensglob3;
    }

    public function getServicesglob1()
    {
        return $this->servicesglob1;
    }

    public function setServicesglob1($servicesglob1)
    {
        $this->servicesglob1 = $servicesglob1;
    }

    public function getServicesglob2()
    {
        return $this->servicesglob2;
    }

    public function setServicesglob2($servicesglob2)
    {
        $this->servicesglob2 = $servicesglob2;
    }

    public function getServicesglob3()
    {
        return $this->servicesglob3;
    }

    public function setServicesglob3($servicesglob3)
    {
        $this->servicesglob3 = $servicesglob3;
    }

    public function getTotalglob1()
    {
        return $this->totalglob1;
    }

    public function setTotalglob1($totalglob1)
    {
        $this->totalglob1 = $totalglob1;
    }

    public function getTotalglob2()
    {
        return $this->totalglob2;
    }

    public function setTotalglob2($totalglob2)
    {
        $this->totalglob2 = $totalglob2;
    }

    public function getTotalglob3()
    {
        return $this->totalglob3;
    }

    public function setTotalglob3($totalglob3)
    {
        $this->totalglob3 = $totalglob3;
    }

    public function getCodeape()
    {
        return $this->codeape;
    }

    public function setCodeape($codeape)
    {
        $this->codeape = $codeape;
    }

    public function getOrigineCompte()
    {
        return $this->origine_compte;
    }

    public function setOrigineCompte($origine_compte)
    {
        $this->origine_compte = $origine_compte;
    }

    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    public function getFax()
    {
        return $this->fax;
    }

    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    public function getSiteInternet()
    {
        return $this->site_internet;
    }

    public function setSiteInternet($site_internet)
    {
        $this->site_internet = $site_internet;
    }

    public function getDescriptionActivite()
    {
        return $this->description_activite;
    }

    public function setDescriptionActivite($description_activite)
    {
        $this->description_activite = $description_activite;
    }

    public function getActiviteDomaineDefense()
    {
        return $this->activite_domaine_defense;
    }

    public function setActiviteDomaineDefense($activite_domaine_defense)
    {
        $this->activite_domaine_defense = $activite_domaine_defense;
    }

    public function getAnneeClotureExercice1()
    {
        return $this->annee_cloture_exercice1;
    }

    public function setAnneeClotureExercice1($annee_cloture_exercice1)
    {
        $this->annee_cloture_exercice1 = $annee_cloture_exercice1;
    }

    public function getAnneeClotureExercice2()
    {
        return $this->annee_cloture_exercice2;
    }

    public function setAnneeClotureExercice2($annee_cloture_exercice2)
    {
        $this->annee_cloture_exercice2 = $annee_cloture_exercice2;
    }

    public function getAnneeClotureExercice3()
    {
        return $this->annee_cloture_exercice3;
    }

    public function setAnneeClotureExercice3($annee_cloture_exercice3)
    {
        $this->annee_cloture_exercice3 = $annee_cloture_exercice3;
    }

    public function getEffectifMoyen1()
    {
        return $this->effectif_moyen1;
    }

    public function setEffectifMoyen1($effectif_moyen1)
    {
        $this->effectif_moyen1 = $effectif_moyen1;
    }

    public function getEffectifMoyen2()
    {
        return $this->effectif_moyen2;
    }

    public function setEffectifMoyen2($effectif_moyen2)
    {
        $this->effectif_moyen2 = $effectif_moyen2;
    }

    public function getEffectifMoyen3()
    {
        return $this->effectif_moyen3;
    }

    public function setEffectifMoyen3($effectif_moyen3)
    {
        $this->effectif_moyen3 = $effectif_moyen3;
    }

    public function getEffectifEncadrement1()
    {
        return $this->effectif_encadrement1;
    }

    public function setEffectifEncadrement1($effectif_encadrement1)
    {
        $this->effectif_encadrement1 = $effectif_encadrement1;
    }

    public function getEffectifEncadrement2()
    {
        return $this->effectif_encadrement2;
    }

    public function setEffectifEncadrement2($effectif_encadrement2)
    {
        $this->effectif_encadrement2 = $effectif_encadrement2;
    }

    public function getEffectifEncadrement3()
    {
        return $this->effectif_encadrement3;
    }

    public function setEffectifEncadrement3($effectif_encadrement3)
    {
        $this->effectif_encadrement3 = $effectif_encadrement3;
    }

    public function getPme1()
    {
        return $this->pme1;
    }

    public function setPme1($pme1)
    {
        $this->pme1 = $pme1;
    }

    public function getPme2()
    {
        return $this->pme2;
    }

    public function setPme2($pme2)
    {
        $this->pme2 = $pme2;
    }

    public function getPme3()
    {
        return $this->pme3;
    }

    public function setPme3($pme3)
    {
        $this->pme3 = $pme3;
    }

    public function getAdresse2()
    {
        return $this->adresse2;
    }

    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;
    }

    public function getNicsiege()
    {
        return $this->nicsiege;
    }

    public function setNicsiege($nicsiege)
    {
        $this->nicsiege = $nicsiege;
    }

    public function getAcronymePays()
    {
        return $this->acronyme_pays;
    }

    public function setAcronymePays($acronyme_pays)
    {
        $this->acronyme_pays = $acronyme_pays;
    }

    public function getDateCreation()
    {
        return $this->date_creation;
    }

    public function setDateCreation($date_creation)
    {
        $this->date_creation = $date_creation;
    }

    public function getDateModification()
    {
        return $this->date_modification;
    }

    public function setDateModification($date_modification)
    {
        $this->date_modification = $date_modification;
    }

    public function getIdInitial()
    {
        return $this->id_initial;
    }

    public function setIdInitial($id_initial)
    {
        $this->id_initial = $id_initial;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion($region)
    {
        $this->region = $region;
    }

    public function getProvince()
    {
        return $this->province;
    }

    public function setProvince($province)
    {
        $this->province = $province;
    }

    public function getTelephone2()
    {
        return $this->telephone2;
    }

    public function setTelephone2($telephone2)
    {
        $this->telephone2 = $telephone2;
    }

    public function getTelephone3()
    {
        return $this->telephone3;
    }

    public function setTelephone3($telephone3)
    {
        $this->telephone3 = $telephone3;
    }

    public function getCnss()
    {
        return $this->cnss;
    }

    public function setCnss($cnss)
    {
        $this->cnss = $cnss;
    }

    public function getRcNum()
    {
        return $this->rc_num;
    }

    public function setRcNum($rc_num)
    {
        $this->rc_num = $rc_num;
    }

    public function getRcVille()
    {
        return $this->rc_ville;
    }

    public function setRcVille($rc_ville)
    {
        $this->rc_ville = $rc_ville;
    }

    public function getDomainesActivites()
    {
        return $this->domaines_activites;
    }

    public function setDomainesActivites($domaines_activites)
    {
        $this->domaines_activites = $domaines_activites;
    }

    public function getNumTax()
    {
        return $this->num_tax;
    }

    public function setNumTax($num_tax)
    {
        $this->num_tax = $num_tax;
    }

    public function getDocumentsCommerciaux()
    {
        return $this->documents_commerciaux;
    }

    public function setDocumentsCommerciaux($documents_commerciaux)
    {
        $this->documents_commerciaux = $documents_commerciaux;
    }

    public function getIntituleDocumentsCommerciaux()
    {
        return $this->intitule_documents_commerciaux;
    }

    public function setIntituleDocumentsCommerciaux($intitule_documents_commerciaux)
    {
        $this->intitule_documents_commerciaux = $intitule_documents_commerciaux;
    }

    public function getTailleDocumentsCommerciaux()
    {
        return $this->taille_documents_commerciaux;
    }

    public function setTailleDocumentsCommerciaux($taille_documents_commerciaux)
    {
        $this->taille_documents_commerciaux = $taille_documents_commerciaux;
    }

    public function getQualification()
    {
        return $this->qualification;
    }

    public function setQualification($qualification)
    {
        $this->qualification = $qualification;
    }

    public function getAgrement()
    {
        return $this->agrement;
    }

    public function setAgrement($agrement)
    {
        $this->agrement = $agrement;
    }

    public function getMoyensTechnique()
    {
        return $this->moyens_technique;
    }

    public function setMoyensTechnique($moyens_technique)
    {
        $this->moyens_technique = $moyens_technique;
    }

    public function getMoyensHumains()
    {
        return $this->moyens_humains;
    }

    public function setMoyensHumains($moyens_humains)
    {
        $this->moyens_humains = $moyens_humains;
    }

    public function getCompteActif()
    {
        return $this->compte_actif;
    }

    public function setCompteActif($compte_actif)
    {
        $this->compte_actif = $compte_actif;
    }

    public function getCapitalSocial()
    {
        return $this->capital_social;
    }

    public function setCapitalSocial($capital_social)
    {
        $this->capital_social = $capital_social;
    }

    public function getIfu()
    {
        return $this->ifu;
    }

    public function setIfu($ifu)
    {
        $this->ifu = $ifu;
    }

    public function getIdAgentCreateur()
    {
        return $this->id_agent_createur;
    }

    public function setIdAgentCreateur($id_agent_createur)
    {
        $this->id_agent_createur = $id_agent_createur;
    }

    public function getNomAgent()
    {
        return $this->nom_agent;
    }

    public function setNomAgent($nom_agent)
    {
        $this->nom_agent = $nom_agent;
    }

    public function getPrenomAgent()
    {
        return $this->prenom_agent;
    }

    public function setPrenomAgent($prenom_agent)
    {
        $this->prenom_agent = $prenom_agent;
    }

    public function getAdressesElectroniques()
    {
        return $this->adresses_electroniques;
    }

    public function setAdressesElectroniques($adresses_electroniques)
    {
        $this->adresses_electroniques = $adresses_electroniques;
    }

    public function getVisibleBourse()
    {
        return $this->visible_bourse;
    }

    public function setVisibleBourse($visible_bourse)
    {
        $this->visible_bourse = $visible_bourse;
    }

    public function getTypeCollaboration()
    {
        return $this->type_collaboration;
    }

    public function setTypeCollaboration($type_collaboration)
    {
        $this->type_collaboration = $type_collaboration;
    }

    public function getEntrepriseEa()
    {
        return $this->entreprise_ea;
    }

    public function setEntrepriseEa($entreprise_ea)
    {
        $this->entreprise_ea = $entreprise_ea;
    }

    public function getEntrepriseSiae()
    {
        return $this->entreprise_siae;
    }

    public function setEntrepriseSiae($entreprise_siae)
    {
        $this->entreprise_siae = $entreprise_siae;
    }

    public function getCommonInscrits()
    {
        return $this->commonInscrits;
    }

    public function setCommonInscrits($commonInscrits)
    {
        $this->commonInscrits[] = $commonInscrits;
    }

    public function addCommonInscrit($commonInscrits)
    {
        self::setCommonInscrits($commonInscrits);
    }

    public function setSaisieManuelle($value)
    {
        $this->saisieManuelle = $value;
    }

    public function getSaisieManuelle()
    {
        return $this->saisieManuelle;
    }

    public function setTvaIntracommunautaire($value)
    {
        $this->tvaIntracommunautaire = $value;
    }

    public function getTvaIntracommunautaire()
    {
        return $this->tvaIntracommunautaire;
    }

    public function getFormejuridiqueCode()
    {
        return $this->formejuridiqueCode;
    }

    public function setFormejuridiqueCode($formejuridique)
    {
        $this->formejuridiqueCode = $formejuridique;
    }

    public function setNombreEtablissementsActifs($value)
    {
        $this->nombreEtablissementsActifs = $value;
    }

    public function getNombreEtablissementsActifs()
    {
        return $this->nombreEtablissementsActifs;
    }

    public function setNomCommercial($value)
    {
        $this->nomCommercial = $value;
    }

    public function getNomCommercial()
    {
        return $this->nomCommercial;
    }

    public function setProcedureCollective($value)
    {
        $this->procedureCollective = $value;
    }

    public function getProcedureCollective()
    {
        return $this->procedureCollective;
    }

    public function setRaisonSociale($value)
    {
        $this->raisonSociale = $value;
    }

    public function getRaisonSociale()
    {
        return $this->raisonSociale;
    }

    public function setSiretSiegeSocial($value)
    {
        $this->siretSiegeSocial = $value;
    }

    public function getSiretSiegeSocial()
    {
        return $this->siretSiegeSocial;
    }

    public function setIdCodeEffectif($value)
    {
        $this->idCodeEffectif = $value;
    }

    public function getIdCodeEffectif()
    {
        return $this->idCodeEffectif;
    }

    public function setExercices($value)
    {
        $this->exercices = $value;
    }

    public function getExercices()
    {
        return $this->exercices;
    }

    public function setMandataires($value)
    {
        $this->mandataires = $value;
    }

    public function getMandataires()
    {
        return $this->mandataires;
    }

    public function setEtablissements(Atexo_Entreprise_EtablissementVo $value)
    {
        $this->etablissements[$value->getCodeEtablissement()] = $value;
    }

    public function getEtablissements()
    {
        return $this->etablissements;
    }

    public function setListeEtablissements($value)
    {
        $this->listeEtablissements = $value;
    }

    public function getListeEtablissements()
    {
        return $this->listeEtablissements;
    }

    public function setEtablissementSiege($value)
    {
        $this->etablissementSiege = $value;
    }

    public function getEtablissementSiege()
    {
        return $this->etablissementSiege;
    }

    /**
     * @return null
     */
    public function getEtatAdministratif()
    {
        return $this->etatAdministratif;
    }

    /**
     * @param null $etatAdministratif
     */
    public function setEtatAdministratif($etatAdministratif)
    {
        $this->etatAdministratif = $etatAdministratif;
    }

    /**
     * @return null
     */
    public function getDateCessation()
    {
        return $this->dateCessation;
    }

    /**
     * @param null $dateCessation
     */
    public function setDateCessation($dateCessation)
    {
        $this->dateCessation = $dateCessation;
    }

    /**
     * Get the [libelleApe] column value.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getLibelleApe()
    {
        return $this->libelleApe;
    }

    /**
     * Set the value of [libelleApe] column.
     *
     * @param string $value new value
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setLibelleApe($value)
    {
        $this->libelleApe = $value;
    }

    /**
     * Get the [created_from_decision] column value.
     *
     * @return int
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getCreatedFromDecision()
    {
        return $this->created_from_decision;
    }

    /**
     * Set the value of [created_from_decision] column.
     *
     * @param string $value new value
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setCreatedFromDecision($value)
    {
        $this->created_from_decision = $value;
    }

    /**
     * Constructeur de la classe.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function __contruct()
    {
    }

    /**
     * Permet de remplir l'objet entrepriseVo avec des donnees.
     *
     * @param array $data : donnees
     *
     * @return Atexo_Entreprise_EntrepriseVo : objet contenant les informations
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public static function loadFromApiGouvEntreprise($data)
    {
        $entrepriseVo = new self();
        $entrepriseVo->setSiren($data['siren']);

        $capitalSocial = $data['capital']['montant'] ?: 'N.C.';
        $entrepriseVo->setCodeape(' ');
        $entrepriseVo->setCapitalSocial($capitalSocial);
        $entrepriseVo->setNomCommercial($data['nom_commercial']);
        $entrepriseVo->setTvaIntracommunautaire($data['tva_number'] ?? null);
        $entrepriseVo->setFormejuridique($data['forme_juridique']['libelle'] ?? 'Autre');
        $entrepriseVo->setFormejuridiqueCode($data['forme_juridique']['code'] ?? null);

        $raisonSociale = ' ';
        if ($data['type'] == ApiEntreprise::TYPE_PERSONNE_PHYSIQUE) {
            $personnePhysiqueAttributs = $data['personne_physique_attributs'];
            $raisonSociale = $personnePhysiqueAttributs['prenom_1'] . ' ' . $personnePhysiqueAttributs['nom_naissance'];
        } elseif ($data['type'] == ApiEntreprise::TYPE_PERSONNE_MORALE) {
            $raisonSociale = $data['personne_morale_attributs']['raison_sociale'] ?? null;
        }

        $entrepriseVo->setRaisonSociale($raisonSociale);
        $entrepriseVo->setNom($raisonSociale);
        $entrepriseVo->setSiretSiegeSocial($data['siret_siege_social']);
        $entrepriseVo->setNicSiege(
            (strlen($data['siret_siege_social']) > 9)
                ? substr($data['siret_siege_social'], 9, 5)
                : ''
        );

        $trancheEffectifSalarie = $data['tranche_effectif_salarie']['code'] ?? null;
        $idCodeEffectif = (new Atexo_ValeursReferentielles())->retrieveByPk(
            $trancheEffectifSalarie,
            Atexo_Config::getParameter('ID_REFERENTIEL_TRANCHE_EFFECTIF'),
            true
        );
        if ($idCodeEffectif) {
            $entrepriseVo->setIdCodeEffectif($idCodeEffectif);
        }

        $entrepriseVo->setMandataires($data['mandataires_sociaux']);
        $entrepriseVo->setCategorieEntreprise($data['categorie_entreprise']);
        $entrepriseVo->setPaysadresse(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
        $entrepriseVo->setSaisieManuelle(0);
        $entrepriseVo->setEtatAdministratif($data['etat_administratif']);

        if (!empty($data['date_cessation'])) {
            $date = new DateTime();
            $date->setTimestamp($data['date_cessation']);
            $entrepriseVo->setDateCessation($date);
        }

        return $entrepriseVo;
    }

    /**
     * Permet de savoir si l'entreprise est locale ou etranger.
     *
     * @return Bollean true si l'entreprise est locale,si non false
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function isEntrepriseLocale()
    {
        if ($this->getSiren() && $this->getPaysenregistrement() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')) {
            return true;
        }

        return false;
    }

    /**
     * recupere l'etablissement siege de l'entreprise.
     *
     * @return CommonTEtablissemnt l'etablissement siege
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getMonEtablissementSiege()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $etablissement = CommonTEtablissementQuery::create()->filterByEstSiege('1')->filterByIdEntreprise($this->getId())->findOne($connexion);
        if ($etablissement instanceof CommonTEtablissement) {
            return $etablissement;
        }

        return false;
    }

    /**
     * Verifie si l'entreprise a au moins 1 inscrit.
     *
     * @return bool : true si l'entreprise n'a pas d'inscrit, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2016
     */
    public function hasInscrit()
    {
        $listeInscritsQuery = null;
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $listeInscritsQuery = CommonInscritQuery::create()->filterByEntrepriseId($this->getId());
            $listeInscrits = $listeInscritsQuery->findOne($connexion);
            if (empty($listeInscrits)) {
                return false;
            }

            return true;
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de la recuperation de la liste des inscrits \n\nRequete : " . $listeInscritsQuery->toString() . "\n\nErreur : " . $e->getMessage() . "\n\trace: " . $e->getTraceAsString());
        }
    }

    /**
     * @return mixed
     */
    public function getCategorieEntreprise()
    {
        return $this->categorieEntreprise;
    }

    /**
     * @param mixed $categorie_entreprise
     */
    public function setCategorieEntreprise($categorieEntreprise)
    {
        $this->categorieEntreprise = $categorieEntreprise;
    }
}
