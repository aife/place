<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTHistoriqueSynchronisationSGMAP;
use Application\Service\Atexo\Atexo_Config;

/**
 * Class pour gerer L'historique de la synchronisation SGMAP.
 *
 * @author loubna EZZIANI <loubna.ezziani.konate@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 1.0
 *
 * @since 4.9.0
 */
class Atexo_Entreprise_HistoriqueSynchroSGMAP
{
    /**
     * Permet d'enregistrer l'historique de synchronisation avec Api Gouv Entreprise.
     *
     * @param string $typeObject
     * @param int    $code
     * @param string $jeton
     * @param  $connexion l'objet de connexion
     * @param int $idObjet
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function enregistrerHistroique($typeObject, $code, $jeton, $connexion = null, $idObjet = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $historique = new CommonTHistoriqueSynchronisationSGMAP();
        $historique->setTypeObjet($typeObject);
        $historique->setCode($code);
        $historique->setJeton($jeton);
        $historique->setIdObjet($idObjet);
        $historique->setDateCreation(date('Y-m-d H:i:s'));
        $historique->save($connexion);
    }
}
