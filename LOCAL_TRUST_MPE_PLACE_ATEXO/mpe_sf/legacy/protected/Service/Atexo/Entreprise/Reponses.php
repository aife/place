<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonBlocFichierEnveloppe;
use Application\Propel\Mpe\CommonBlocFichierEnveloppeTemporaire;
use Application\Propel\Mpe\CommonBlocFichierEnveloppeTemporairePeer;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppeFormulaireConsultation;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonEnveloppeQuery;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonReponseInscritFormulaireConsultation;
use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Propel\Mpe\CommonTEditionFormulaire;
use Application\Propel\Mpe\CommonTEditionFormulairePeer;
use Application\Propel\Mpe\CommonTEnveloppeDossierFormulaire;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\CommonTReferentielCertificat;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_FormConsultation;
use Application\Service\Atexo\Atexo_MarchePublicSimplifie;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_ReferentielCertificat;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Crypto\Atexo_Crypto_CurlSend;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Echange;
use Application\Service\Atexo\Signature\Atexo_Signature_FichierVo;
use AtexoCrypto\Dto\Enveloppe;
use AtexoCrypto\Dto\Fichier;
use AtexoCrypto\Dto\Offre;
use AtexoCrypto\Dto\Signature;
use DOMDocument;
use Exception;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise_Reponses
{
    public function recieveBloc($uid, $stringFile, $typeEnv, $sousPli, $typeFichier, $numOrdreFichier, $numOrdreBloc, $organisme)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            //Insertion du bloc dans le fichier
            if (!is_dir(Atexo_Config::getParameter('COMMON_TMP'))) {
                mkdir(Atexo_Config::getParameter('COMMON_TMP'));
            }
            $file = Atexo_Config::getParameter('COMMON_TMP').'/bloc_'.$uid.$typeEnv.$sousPli.$typeFichier.$numOrdreFichier.$numOrdreBloc.time();
            $fp = fopen($file, 'w');
            if (!fwrite($fp, $stringFile)) {
                //TODO erreur
                return -1;
            }
            fclose($fp);

            $atexoBlob = new Atexo_Blob();
            $idBlocBlob = $atexoBlob->insert_blob('bloc_'.$uid.$typeEnv.$sousPli.$typeFichier.$numOrdreFichier.$numOrdreBloc.time(), $file, $organisme);

            $blocFichierTemporaire = new CommonBlocFichierEnveloppeTemporaire();
            $blocFichierTemporaire->setUid($uid);
            $blocFichierTemporaire->setTypeEnv($typeEnv);
            $blocFichierTemporaire->setSousPli($sousPli);
            $blocFichierTemporaire->setTypeFichier($typeFichier);
            $blocFichierTemporaire->setNumeroOrdreFichier($numOrdreFichier);
            $blocFichierTemporaire->setNumeroOrdreBloc($numOrdreBloc);
            $blocFichierTemporaire->setIdBlob($idBlocBlob);
            $blocFichierTemporaire->setOrganisme($organisme);
            $blocFichierTemporaire->save($connexion);
            @unlink($file);

            return $blocFichierTemporaire->getId();
        } catch (Exception) {
            return 0;
        }
    }

    public function reponseReceieved($xmlSringBase64, $uidOffre, $consultationId, $organisme, $mailAr, $idEntreprise, $idInscrit, $offreVariante = null, $reponsePasAPas = false, $idCandidatureMps = false, $idEtablissement = false)
    {
        $arrayDataToReturn = [];
        $commonConsultation = (new Atexo_Consultation())->retrieveCommonConsultation($consultationId, $organisme);
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);

        //on est dans une methode qui fait une modification de la base donc on accede a la base lecture/ecriture
        $commonConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $commonConnexion->beginTransaction();
        //Les noms de fichiers accentues ne s'enregistrent pas dans la BD UTF-8 sans cette commande
        if (strtoupper(Atexo_Config::getParameter('DB_ENCODING')) == strtoupper('utf8')) {
            $xmlString = (base64_decode($xmlSringBase64));
        } else {
            $xmlString = utf8_decode(base64_decode($xmlSringBase64));
        }
        $xmlString = str_replace('<ReponsesAnnonce>', '', $xmlString);
        $xmlString = str_replace('</ReponsesAnnonce>', '', $xmlString);
        $domDocument = new DOMDocument();
        $domDocument->loadXML($xmlString);
        $annonces = $domDocument->createElement('Annonce', '');
        $operateurEconomique = $domDocument->createElement('OperateurEconomique', '');
        $reponseAnnonce = $domDocument->getElementsByTagName('ReponseAnnonce')->item(0);
        $enveloppes = $reponseAnnonce->getElementsByTagName('Enveloppes')->item(0);
        $reponseAnnonce->insertBefore($annonces, $enveloppes);
        $reponseAnnonce->insertBefore($operateurEconomique, $annonces);

        $xmlString = $domDocument->saveXML();

        $xmlObject = simplexml_load_string($xmlString);
        if (!$xmlObject) {
            throw new Exception('erreur lors du chargement du fichier xml');
        } else {
            try {
                $reponseAnnonceAttributes = $xmlObject->attributes();
                $chaineARemplace = '##_NUM_REPONSE_##_'.session_id();
                $xmlObject->addAttribute('indexReponse', $chaineARemplace);
                $xmlObject->addAttribute('reference', $consultationId);
                $xmlObject->addAttribute('organisme', $organisme);
                $xmlObject->addAttribute('horodatageDepot', date('Y-m-d H:i:s'));
                //add Attributes typeSignature
                $xmlObject->addAttribute('typeSignature', 'xml');
                $annonce = $xmlObject->Annonce;
                $annonce->addAttribute('chiffrement', ('1' == $consultation->getChiffrementOffre()) ? 'true' : 'false');
                $annonce->addAttribute('typeProcedure', utf8_encode((new Atexo_Consultation_ProcedureType())->retrieveAcronymeTypeProcedure($commonConsultation->getIdTypeProcedure(), true)));
                $annonce->addAttribute('allotissement', count($commonConsultation->getAllLots() >= 1) ? 'true' : 'false');
                $annonce->addAttribute('organisme', utf8_encode($organisme));
                $annonce->addAttribute('nombreLots', is_countable($commonConsultation->getAllLots() >= 1) ? count($commonConsultation->getAllLots() >= 1) : 0);
                $entite = Atexo_EntityPurchase::getPathEntityById($commonConsultation->getServiceId(), $organisme);
                $annonce->addAttribute('entiteAchat', utf8_encode(Atexo_Util::escapeXmlChars($entite)));
                $annonce->addAttribute('separationSignatureAe', ('1' == $consultation->getSignatureActeEngagement()) ? 'true' : 'false');
                $annonce->addAttribute('signature', ('1' == $consultation->getSignatureOffre()) ? 'true' : 'false');
                $annonce->addAttribute('reference', utf8_encode(Atexo_Util::escapeXmlChars($consultation->getId())));
                $annonce->addAttribute('referenceUtilisateur', utf8_encode(Atexo_Util::escapeXmlChars($consultation->getReferenceUtilisateur())));
                $annonce->addAttribute('dateLimite', utf8_encode($consultation->getDatefin()));

                $annonce->addChild('Intitule', utf8_encode(Atexo_Util::escapeXmlChars($consultation->getIntitule())));
                $annonce->addChild('Objet', utf8_encode(Atexo_Util::escapeXmlChars($consultation->getObjet())));

                $operateurEconomique = $xmlObject->OperateurEconomique;
                $operateurEconomique->addChild('Nom', utf8_encode(Atexo_Util::escapeXmlChars(Atexo_Entreprise::getNomEntreprise($idEntreprise))));

                $enveloppes = $xmlObject->Enveloppes;

                $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise);
                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($idInscrit);
                $etablissementQuery = new CommonTEtablissementQuery();
                $etablissement = $etablissementQuery->getEtabissementbyIdEseAndIdEtab($idEtablissement, $idEntreprise);

                $commonOffresObject = new CommonOffres();
                $commonOffresObject->setOrganisme($organisme);
                $commonOffresObject->setConsultationId($consultationId);
                $commonOffresObject->setEntrepriseId($idEntreprise);
                $commonOffresObject->setIdEtablissement($idEtablissement);
                $commonOffresObject->setInscritId($idInscrit);
                $commonOffresObject->setMailsignataire($mailAr);
                $commonOffresObject->setEnvoiComplet('1');
                $commonOffresObject->setUidOffre($uidOffre);
                $commonOffresObject->setNomEntrepriseInscrit($entreprise->getNom());
                $commonOffresObject->setNomInscrit($inscrit->getNom());
                $commonOffresObject->setPrenomInscrit($inscrit->getPrenom());
                $infosEtab = false;
                if ($etablissement instanceof CommonTEtablissement) {
                    $commonOffresObject->setAdresseInscrit($etablissement->getAdresse());
                    $commonOffresObject->setAdresse2Inscrit($etablissement->getAdresse2());
                    $commonOffresObject->setCodePostalInscrit($etablissement->getCodepostal());
                    $commonOffresObject->setVilleInscrit($etablissement->getVille());
                    $commonOffresObject->setPaysInscrit($etablissement->getPays());
                    $infosEtab = true;
                }
                $commonOffresObject->setTelephoneInscrit($inscrit->getTelephone());
                $commonOffresObject->setFaxInscrit($inscrit->getFax());
                $commonOffresObject->setAcronymePays($entreprise->getAcronymePays());
                $commonOffresObject->setSiretEntreprise($entreprise->getSiren().$entreprise->getSiretSiegeSocial());
                $commonOffresObject->setIdentifiantNational($entreprise->getSirenEtranger());
                $commonOffresObject->setEmailInscrit(trim($inscrit->getEmail()));
                $commonOffresObject->setSiretInscrit($entreprise->getSiren().(new Atexo_Entreprise_Etablissement())->getCodeEtablissemntByIdEtab($inscrit->getIdEtablissement()));
                if (null !== $offreVariante) {
                    $commonOffresObject->setOffreVariante($offreVariante);
                }
                if ($reponsePasAPas) {
                    $commonOffresObject->setReponsePasAPas('1');
                }

                $commonOffresObject->setStatutOffres(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                //nous sommes dans le cas où la candidature a été déposée sur le site de l'api entreprise gouv
                //le processus est identique au formulaire interne (creation d'une candidature) mais la redirection
                //est faite directement après les enregistrements en BD sans afficher le formulaire MPS interne
                if (isset($_GET['candidature_id']) && !empty($_GET['candidature_id'])) {
                    $commonOffresObject->setCandidatureIdExterne(Atexo_Util::atexoHtmlEntities($_GET['candidature_id']));
                }
                $commonOffresObject->save($commonConnexion);
                if (!$infosEtab) {
                    $log = PHP_EOL.'[ '.date('d-M-Y H:i:s')." ] : l'offres numero ".$commonOffresObject->getId()." a ete sauvegarde sans infos d'etablissement ".PHP_EOL;
                    $log .= " => l'etablissement dont l'id = ".$idEtablissement." de l'entreprise ".$idEntreprise.' est introuvable '.PHP_EOL; //,
                    Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/logEtablissements.log', $log, 'a');
                }
                if ($reponseAnnonceAttributes['idReponseAnnonce']) {
                    $reponseAnnonceAttributes['idReponseAnnonce'] = $commonOffresObject->getId();
                } else {
                    $reponseAnnonceAttributes->addAttribute('idReponseAnnonce', $commonOffresObject->getId());
                }
                if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                    $relationNumOrdreFichier = $this->getViewState('relationNumOrdreFichier');
                }
                if ($consultation->getMarchePublicSimplifie() && Atexo_Module::isEnabled('MarchePublicSimplifie', $consultation->getOrganisme()) && $idCandidatureMps) {
                    $candidatureMps = (new Atexo_MarchePublicSimplifie())->retrieveCandidatureMpsSansOffre($idCandidatureMps, $consultationId, $organisme, $idInscrit, $idEntreprise);
                    if ($candidatureMps instanceof CommonTCandidatureMps) {
                        $candidatureMps->setIdOffre($commonOffresObject->getId());
                        $candidatureMps->save($commonConnexion);
                        $arrayDataToReturn['candidatureMPS'] = Prado::localize('FICHIER_CANDIDATURE_MPS').' : '.
                                                            Prado::localize('DEFINE_CANDIDATURE_MPS').' - '.Atexo_Util::arrondirSizeFile($candidatureMps->getTailleFichier() / 1024);
                    }
                }
                $candidatureExiste = false;
                foreach ($enveloppes->children() as $enveloppe) {
                    $attributesEnveloppe = $enveloppe->attributes();
                    if ((string) $attributesEnveloppe['type'] == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                        $candidatureExiste = true;
                    }
                    $CommonEnveloppeObject = new CommonEnveloppe();
                    $CommonEnveloppeObject->setOffreId($commonOffresObject->getId());

                    $CommonEnveloppeObject->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                    $CommonEnveloppeObject->setTypeEnv((string) $attributesEnveloppe['type']);
                    $CommonEnveloppeObject->setSousPli((string) $attributesEnveloppe['numLot']);
                    $CommonEnveloppeObject->setOrganisme($organisme);
                    $CommonEnveloppeObject->save($commonConnexion);
                    if ($attributesEnveloppe['idEnveloppe']) {
                        $attributesEnveloppe['idEnveloppe'] = $CommonEnveloppeObject->getIdEnveloppeElectro();
                    } else {
                        $attributesEnveloppe->addAttribute('idEnveloppe', $CommonEnveloppeObject->getIdEnveloppeElectro());
                    }

                    //Desbut Gestion des formulaires de consultation
                    if (Atexo_Module::isEnabled('GestionModelesFormulaire')) {
                        if ((string) $attributesEnveloppe['type'] == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                            $listeFormsForenveloppe = (new Atexo_FormConsultation())->getFormsByTypeEnveloppeAndLot($consultationId, $organisme, (string) $attributesEnveloppe['type'], '0', $idInscrit);
                        } else {
                            $listeFormsForenveloppe = (new Atexo_FormConsultation())->getFormsByTypeEnveloppeAndLot($consultationId, $organisme, (string) $attributesEnveloppe['type'], (string) $attributesEnveloppe['numLot'], $idInscrit);
                        }

                        if ((is_countable($listeFormsForenveloppe) ? count($listeFormsForenveloppe) : 0) > 0) {
                            foreach ($listeFormsForenveloppe as $oneForm) {
                                $envFormcons = new CommonEnveloppeFormulaireConsultation();
                                $envFormcons->setIdFormulaireConsultation($oneForm->getId());
                                $envFormcons->setIdEnveloppe($CommonEnveloppeObject->getIdEnveloppeElectro());
                                $envFormcons->setTotalBdHt($oneForm->getTotalBdHt());
                                $envFormcons->save($commonConnexion);
                                //appel atexo forms
                                $uid = Atexo_Util::builUidPlateforme($oneForm->getId());
                                $reponseFormCons = (new Atexo_FormConsultation())->retreiveReponseInscritformulaireCons($oneForm->getId(), $idInscrit);
                                if ($reponseFormCons && ($reponseFormCons instanceof CommonReponseInscritFormulaireConsultation)) {
                                    $uidReponse = Atexo_Util::builUidPlateforme($reponseFormCons->getId());
                                    $uidEnveloppe = Atexo_Util::builUidPlateforme($envFormcons->getId());
                                    self::creerFormsReponses($uid, $uidReponse, $uidEnveloppe);
                                }
                            }
                        }
                    }

                    //Debut gestion des formulaires SUB
                    if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                        $reponseElectroniqueFormulaire = (new Atexo_FormulaireSub_Dossier())->recupererReponseElectFormulaire($consultationId, $organisme, $idInscrit, $idEntreprise);
                        if ($reponseElectroniqueFormulaire) {
                            $idRepForm = $reponseElectroniqueFormulaire->getIdReponseElecFormulaire();
                            $numLot = (string) $attributesEnveloppe['numLot'];
                            $typeEnv = (string) $attributesEnveloppe['type'];
                            $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierByIdReponseElectFormulaireAndTypeEnveloppeAndNumLot($idRepForm, $typeEnv, $numLot);
                            if ($dossier instanceof CommonTDossierFormulaire) {
                                //Copie du dossier dans la table "t_enveloppe_dossier_formulaire"
                                $enveloppeDossier = new CommonTEnveloppeDossierFormulaire();
                                $enveloppeDossier->setIdEnveloppe($CommonEnveloppeObject->getIdEnveloppeElectro());
                                $enveloppeDossier->setIdDossierFormulaire($dossier->getIdDossierFormulaire());
                                $enveloppeDossier->setOrganisme($organisme);
                                $enveloppeDossier->setIdLot($dossier->getIdLot());
                                $enveloppeDossier->setTypeEnveloppe($dossier->getTypeEnveloppe());
                                $enveloppeDossier->setLibelleForrmulaire($dossier->getLibelleForrmulaire());
                                $enveloppeDossier->setCleExterneDispositif($dossier->getCleExterneDispositif());
                                $enveloppeDossier->setCleExterneDossier($dossier->getCleExterneDossier());
                                $enveloppeDossier->setStatutValidation($dossier->getStatutValidation());
                                $enveloppeDossier->setDateCreation($dossier->getDateCreation());
                                $enveloppeDossier->setDateModif($dossier->getDateModif());
                                $enveloppeDossier->setDateValidation($dossier->getDateValidation());
                                $enveloppeDossier->setStatutGenerationGlobale($dossier->getStatutGenerationGlobale());
                                $enveloppeDossier->setTypeReponse($dossier->getTypeReponse());
                                $enveloppeDossier->setCleExterneFormulaire($dossier->getCleExterneFormulaire());
                                $enveloppeDossier->save($commonConnexion);
                                //Flaguer le dossier et mettre le champ "formulaire_depose" a 1
                                $dossier->setFormulaireDepose(Atexo_Config::getParameter('FORMULAIRE_DEPOSE_DOSSIER_TRANSMIS'));
                                $dossier->save($commonConnexion);
                                //deverouiller le dossier chez SUB
                                (new Atexo_FormulaireSub_Echange())->unlockDossier($dossier->getCleExterneDossier(), $inscrit->getLogin(), $commonConsultation->getreferenceUtilisateur());
                            }
                        }
                    }
                    //Fin gestion des formulaires SUB

                    //Fin Gestion des formulaires de consultation
                    foreach ($enveloppe->children() as $fichier) {
                        $attributesFile = $fichier->attributes();
                        $signature = trim((string) (new Atexo_Util())->toHttpEncoding($fichier->Signature));

                        $CommonFichierObject = new CommonFichierEnveloppe();
                        $CommonFichierObject->setIdEnveloppe($CommonEnveloppeObject->getIdEnveloppeElectro());
                        $CommonFichierObject->setTypeFichier((string) (new Atexo_Util())->toHttpEncoding($attributesFile['typeFichier']));
                        $CommonFichierObject->setNumOrdreFichier((string) $attributesFile['numOrdre']);
                        $CommonFichierObject->setTailleFichier((string) $attributesFile['tailleEnClair']);
                        $CommonFichierObject->setNomFichier((string) (new Atexo_Util())->toHttpEncoding($fichier->Nom));

                        // FIXME valider le certificat de signature et enregistrer le resultat, sans bloquer le traitement
                        // verifier : 1/la chaine de certification
                        // 2/ la revocation
                        // 3/ les dates de validite
                        if ($signature) {
                            $CommonFichierObject->setVerificationCertificat('3-3-3'); //Signature incertaine
                            $item = $consultation->getObjet();
                            $resultValidation = self::callModuleValidation($signature, $organisme, $item);
                            $dataSourceFromValidation = json_decode($resultValidation, true, 512, JSON_THROW_ON_ERROR);
                            if (is_array($dataSourceFromValidation)) {
                                if (0 == $dataSourceFromValidation['codeRetour']) {
                                    if ($dataSourceFromValidation['contenuFichierXMLEnBase64']) {
                                        $signature = $dataSourceFromValidation['contenuFichierXMLEnBase64'];
                                    }
                                    $resultat = $dataSourceFromValidation['resultat'];
                                    if (is_array($resultat)) {
                                        $resultValidation = $resultat['chaineCertification'].'-'.$resultat['revocation'].'-'.$resultat['periodeValidite'];
                                        if (Atexo_Module::isEnabled('SurchargeReferentiels')) {
                                            if (is_array($resultat['repertoiresChaineCertification']) && count($resultat['repertoiresChaineCertification'])) {
                                                $nomreferentiel = $resultat['repertoiresChaineCertification'][0]['nom'];
                                                $referentielCertif = (new Atexo_ReferentielCertificat())->getReferentielCertifByName($nomreferentiel);
                                                if ($referentielCertif instanceof CommonTReferentielCertificat) {
                                                    $CommonFichierObject->setNomReferentielCertificat($referentielCertif->getNomReferentielCertificat());
                                                    $CommonFichierObject->setStatutReferentielCertificat($referentielCertif->getStatutReferentielCertificat());
                                                    $CommonFichierObject->setNomReferentielFonctionnel($referentielCertif->getNomReferentielFonctionnel());
                                                    $CommonFichierObject->setMessage($referentielCertif->getMessage());
                                                } else {
                                                    $CommonFichierObject->setStatutReferentielCertificat(1);
                                                }
                                            } else {
                                                $CommonFichierObject->setStatutReferentielCertificat(1);
                                            }
                                        }
                                    }
                                    $CommonFichierObject->setVerificationCertificat($resultValidation);
                                } else {
                                    if ($dataSourceFromValidation['message']) {
                                        $filePath = 'serveurValidation.log';
                                        $log = '[ '.date('d-M-Y H:i:s').' ] : '.$dataSourceFromValidation['message']."\n";
                                        Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/'.$filePath, $log, 'a');
                                    }
                                }
                            }
                            $xmlSignature = base64_decode($signature);
                            $dateSignature = self::getDateCreationSignature($xmlSignature);
                            if ($dateSignature) {
                                $CommonFichierObject->setDateSignature($dateSignature);
                            }
                        }

                        $CommonFichierObject->setHash((string) (new Atexo_Util())->toHttpEncoding($fichier->Empreinte));
                        $CommonFichierObject->setOrganisme($organisme);
                        if (0 == strcmp((string) $attributesFile['origineFichier'], Atexo_Config::getParameter('TYPE_DOCUMENT_EDITION'))) {
                            $CommonFichierObject->setTypePiece(Atexo_Config::getParameter('TYPE_DOCUMENT_EDITION'));
                        } elseif (0 == strcmp((string) $attributesFile['origineFichier'], Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_LIBRE'))) {
                            $CommonFichierObject->setTypePiece(Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_LIBRE'));
                        } elseif (0 == strcmp((string) $attributesFile['origineFichier'], Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_TYPEE'))) {
                            $CommonFichierObject->setTypePiece(Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_TYPEE'));
                        } elseif (0 == strcmp((string) $attributesFile['origineFichier'], Atexo_Config::getParameter('TYPE_DOCUMENT_PJ'))) {
                            $CommonFichierObject->setTypePiece(Atexo_Config::getParameter('TYPE_DOCUMENT_PJ'));
                        } else {
                            $CommonFichierObject->setTypePiece(Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_LIBRE'));
                        }

                        //Recuperer l'identifiant du bloc du fichier dans le cas des editions
                        if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                            $numOrdreFichier = (string) $attributesFile['numOrdre'];
                            $fichierVo = $relationNumOrdreFichier[$numOrdreFichier];
                            if ($fichierVo instanceof Atexo_Signature_FichierVo) {
                                $edition = CommonTEditionFormulairePeer::retrieveByPK($fichierVo->getIdFichier(), $commonConnexion);
                                if ($edition instanceof CommonTEditionFormulaire) {
                                    $destination = Atexo_Config::getParameter('COMMON_TMP').'/edition_'.$uid.$typeEnv.(string) $attributesEnveloppe['numLot'].
                                                                (string) $attributesFile['origineFichier'].$numOrdreFichier.time();
                                    (new Atexo_Files())->copieFichier($edition->getPath(), $destination);
                                    if (is_file($destination)) {
                                        $idBlob = (new Atexo_Blob())->insert_blob($edition->getNomFichier(), $destination, $organisme);
                                        $CommonFichierObject->setIdBlob($idBlob);
                                    }
                                    $CommonFichierObject->setIsHash(1);
                                } else {
                                    $filePath = 'serveurValidation.log';
                                    $log = '[ '.date('d-M-Y H:i:s').' ] : Relation numero Ordre Fichier '.print_r($relationNumOrdreFichier, true)."\n";
                                    Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/'.$filePath, $log, 'a');
                                }
                            } else {
                                $filePath = 'serveurValidation.log';
                                $log = '[ '.date('d-M-Y H:i:s').' ] : Relation numero Ordre Fichier '.print_r($relationNumOrdreFichier, true)."\n";
                                Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/'.$filePath, $log, 'a');
                            }
                        }

                        $CommonFichierObject->save($commonConnexion);
                        if ($attributesFile['idFichier']) {
                            $attributesFile['idFichier'] = $CommonFichierObject->getIdFichier();
                        } else {
                            $attributesFile->addAttribute('idFichier', $CommonFichierObject->getIdFichier());
                        }
                        $arrayinfo = [];
                        $arrayinfo['nom'] = ((string) (new Atexo_Util())->toHttpEncoding($fichier->Nom));
                        $arrayinfo['fileSize'] = (Atexo_Util::arrondirSizeFile(((int) $attributesFile['tailleEnClair']) / 1024));
                        if ($signature) {
                            $arrayinfo['signature'] = true;
                            $arrayinfo['signatureDetail'] = ((string) (new Atexo_Util())->toHttpEncoding($fichier->Nom)).' - Signature 1.xml - '.(Atexo_Util::arrondirSizeFile(((int) strlen($signature) / 1024)));
                            $arrayinfo['CnSubject'] = $CommonFichierObject->getCnSubject($consultation->getSignatureOffre(), 'XML');
                            $arrayinfo['CnEmmeteur'] = $CommonFichierObject->getCnEmmeteur($consultation->getSignatureOffre(), 'XML');
                            $arrayinfo['StatutSignature'] = $CommonFichierObject->getResultatValiditeSignature($commonConnexion, 'entreprise');
                        } else {
                            $arrayinfo['StatutSignature'] = Atexo_Config::getParameter('SANS');
                            $arrayinfo['signature'] = false;
                            $arrayinfo['signatureDetail'] = '';
                        }
                        $arrayDataToReturn['enveloppe'][((string) $attributesEnveloppe['type'])][((string) $attributesEnveloppe['numLot'])][(string) (new Atexo_Util())->toHttpEncoding($attributesFile['typeFichier'])][$CommonFichierObject->getIdFichier()] = $arrayinfo;
                        foreach ($fichier->BlocsChiffrement->children() as $blocChiffrement) {
                            $attributesBloc = $blocChiffrement->attributes();

                            $commonBlocFichier = new CommonBlocFichierEnveloppe();
                            $commonBlocFichier->setIdFichier($CommonFichierObject->getIdFichier());
                            $commonBlocFichier->setNumeroOrdreBloc((int) $attributesBloc['numOrdre']);
                            $commonBlocFichier->setOrganisme($organisme);

                            $c = new Criteria();
                            $c->add(CommonBlocFichierEnveloppeTemporairePeer::UID, $uidOffre);
                            $c->add(CommonBlocFichierEnveloppeTemporairePeer::TYPE_ENV, (string) $attributesEnveloppe['type']);
                            $c->add(CommonBlocFichierEnveloppeTemporairePeer::SOUS_PLI, (string) $attributesEnveloppe['numLot']);
                            $c->add(CommonBlocFichierEnveloppeTemporairePeer::TYPE_FICHIER, (string) (new Atexo_Util())->toHttpEncoding($attributesFile['typeFichier']));
                            $c->add(CommonBlocFichierEnveloppeTemporairePeer::NUMERO_ORDRE_FICHIER, (string) $attributesFile['numOrdre']);
                            $c->add(CommonBlocFichierEnveloppeTemporairePeer::NUMERO_ORDRE_BLOC, (string) $attributesBloc['numOrdre']);
                            $c->add(CommonBlocFichierEnveloppeTemporairePeer::ORGANISME, $organisme);
                            $c->addDescendingOrderByColumn(CommonBlocFichierEnveloppeTemporairePeer::ID);

                            //FIXME verifier la taille apres chiffrement du bloc temporaire
                            $blocs = CommonBlocFichierEnveloppeTemporairePeer::doSelect($c, $commonConnexion);

                            if ($blocs && is_array($blocs)) {
                                $blocs = array_shift($blocs);
                            }

                            if ($blocs instanceof CommonBlocFichierEnveloppeTemporaire) {
                                $commonBlocFichier->setIdBlobChiffre($blocs->getIdBlob());
                                if ($attributesBloc['nomBloc']) {
                                    $attributesBloc['nomBloc'] = $blocs->getIdBlob();
                                } else {
                                    $attributesBloc->addAttribute('nomBloc', $blocs->getIdBlob());
                                }

                                $tailleBlocTmpChiffre = (new Atexo_Blob())->getTailFile($blocs->getIdBlob(), $organisme);
                                if ($tailleBlocTmpChiffre) {
                                    $tailleBlocChiffre = (string) $attributesBloc['tailleApresChiffrement'];
                                    if ($tailleBlocChiffre != $tailleBlocTmpChiffre) {
                                        //TODO
                                        throw new Exception('La taille du bloc recu ('.$tailleBlocTmpChiffre.' octets) ne correpond pas a celle du bloc attendu ('.$tailleBlocChiffre.' octets)');
                                    }
                                } else {
                                    throw new Exception('Impossible de trouver le bloc. Taille du bloc inconnu.');
                                }
                            } else {
                                throw new Exception('Impossible de trouver le bloc UID : '.$uidOffre.' numOrdre Fichier : '.(string) $attributesFile['numOrdre'].' numOrdre Bloc : '.(string) $attributesBloc['numOrdre'].'');
                            }

                            $commonBlocFichier->save($commonConnexion);
                        }
                    }
                }

                if ($consultation->getEnvCandidature() && !$candidatureExiste) {
                    $CommonEnveloppeObject = new CommonEnveloppe();
                    $CommonEnveloppeObject->setOffreId($commonOffresObject->getId());

                    $CommonEnveloppeObject->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE'));
                    $CommonEnveloppeObject->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                    $CommonEnveloppeObject->setOrganisme($organisme);
                    //$enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'));
                    $CommonEnveloppeObject->setCryptage('0');
                    $CommonEnveloppeObject->setEnveloppeFictive('1');
                    $CommonEnveloppeObject->save($commonConnexion);
                }
                $c = new Criteria();
                $c->add(CommonBlocFichierEnveloppeTemporairePeer::UID, $uidOffre);
                $c->add(CommonBlocFichierEnveloppeTemporairePeer::ORGANISME, $organisme);
                CommonBlocFichierEnveloppeTemporairePeer::doDelete($c, $commonConnexion);

                $xml = $xmlObject->asXml();

                $numeroReponses = (is_countable($consultation->getOffres($commonConnexion)) ? count($consultation->getOffres($commonConnexion)) : 0) + 1;
                $commonOffresObject->setNumeroReponse($numeroReponses);
                $xml = str_replace($chaineARemplace, $numeroReponses, $xml);
                $arrayHorodatage = (new Atexo_Crypto())->getArrayHorodatage($xml);
                if (is_array($arrayHorodatage)) {
                    $commonOffresObject->setHorodatage($arrayHorodatage['horodatage']);
                    $arrayDataToReturn['dateDepot'] = $arrayHorodatage['untrustedDate'];
                    $commonOffresObject->setUntrusteddate($arrayHorodatage['untrustedDate']);
                    $commonOffresObject->setUntrustedserial($arrayHorodatage['untrustedSerial']);
                    $commonOffresObject->setXmlString($xml);
                    $commonOffresObject->save($commonConnexion);
                //  TODO FIXME lire le numero de serie du jeton d'horodatage
                } else {
                    throw new Exception('impossible de faire un horodatage');
                }

                $commonConnexion->commit();

                return $arrayDataToReturn;
            } catch (Exception $e) {
                $commonConnexion->rollback();
                throw $e;
            }
        }
    }

    public function getReponsesAnnoncesXml(array $references, $organisme, $idsOffre = false)
    {
        $xmlReponsesAnnonces = '<?xml version="1.0" encoding="ISO-8859-1"?>';
        $xmlReponsesAnnonces .= '<ReponsesAnnonce>';

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonOffresPeer::CONSULTATION_ID, $references, Criteria::IN);
        $c->add(CommonOffresPeer::ORGANISME, $organisme);
        $c->add(CommonOffresPeer::STATUT_OFFRES, Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);

        if ($idsOffre && is_array($idsOffre)) {
            $c->add(CommonOffresPeer::ID, $idsOffre, Criteria::IN);
        }
        $offres = CommonOffresPeer::doSelect($c, $connexion);
        if ($offres && is_array($offres)) {
            foreach ($offres as $offre) {
                if ($offre instanceof CommonOffres) {
                    $xmlReponsesAnnonces .= substr($offre->getXmlString(), strpos($offre->getXmlString(), '?>') + 2);
                }
            }
        }

        $xmlReponsesAnnonces .= '</ReponsesAnnonce>';

        return $xmlReponsesAnnonces;
    }

    public static function getUrlDepotReponse($organisme)
    {
        return Atexo_Util::getAbsoluteUrl().Atexo_Config::getParameter('PAGE_REPONSE').'&org='.$organisme;
    }

    public function GenerateUniqueSerial($organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $uniqueSerial = true;
        do {
            $serial = '';
            for ($i = 0; $i < 8; ++$i) {
                $serial .= strtoupper(dechex(random_int(0, 15)));
            }
            $c = new Criteria();
            $c->add(CommonOffresPeer::UNTRUSTEDSERIAL, $serial);
            $c->add(CommonOffresPeer::ORGANISME, $organisme);
            $offre = CommonOffresPeer::doSelect($c, $connexion);
            if (!$offre) {
                $uniqueSerial = false;
            }
        } while ($uniqueSerial);

        return $serial;
    }

    public function getCorpsMessageReponseRecuePourEntreprise(
        $reference,
        $organisme,
        $arrayDataFromReponseReceieved,
        $nomEntreprise,
        $nomInscrit,
        $prenomInscrit,
        $mail,
        $acteEngagementSigne,
        $returnEnteteMail = false
    ) {
        $corpsMail = '';
        $corpsMail .= Prado::localize('REPONSE_ELECTRONIQUE_POUR_CONSULTATION_CI_DESSOUS').'

';

        $corpsMail .= (new Atexo_Message())->getInfoConsultation($reference, $organisme, false).'
';
        $corpsMail .= Prado::localize('CARACTERISTIQUE_DEPOT').' : '.'

';
        $corpsMail .= Prado::localize('DEFINE_TEXT_STATISTIQUES_ENTREPRISE').' : '.$nomEntreprise.'
';
        $corpsMail .= Prado::localize('NOM').' : '.$nomInscrit.'
';
        $corpsMail .= Prado::localize('PRENOM').' : '.$prenomInscrit.'
';
        $corpsMail .= Prado::localize('TEXT_ADRESSE_ELECTRONIQUE').' : '.$mail.'
';

        if ($returnEnteteMail) {
            return $corpsMail;
        }

        $corpsMail .= Prado::localize('HORODATAGE_DEPOT').' : '.Atexo_Util::iso2frnDateTime($arrayDataFromReponseReceieved['dateDepot'], false, true).'


';
        $corpsMail .= Prado::localize('CONTENU_REPONSE').' : '.'

';
        if ($arrayDataFromReponseReceieved['candidatureMPS']) {
            $corpsMail .= $arrayDataFromReponseReceieved['candidatureMPS'];
            $corpsMail .= '

';
        }

        $corpsMail .= self::getNomsFichiersFromArray($arrayDataFromReponseReceieved, $acteEngagementSigne, 'r', false, null);
        $corpsMail .= '

';
        //Reponse pas a pas AR.
        if (Atexo_Module::isEnabled('ReponsePasAPas') && '' != $arrayDataFromReponseReceieved['errorSign']) {
            $corpsMail .= Prado::localize('TEXT_NB').' : '.'

';
            $corpsMail .= $arrayDataFromReponseReceieved['errorSign'];
            $corpsMail .= '

';
        }
        //Reponse pas a pas AR.
        $corpsMail .= Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');

        return $corpsMail;
    }

    public function getCorpsMessageReponseRecuePourAgent($consultationId, $organisme, $arrayDataFromReponseReceieved)
    {
        $corpsMail = Prado::localize('CORPS_DEBUT').','.'

';
        $corpsMail .= Prado::localize('REPONSE_DEPOSE_POUR_CONSULTATION').' :'.'

';
        $corpsMail .= (new Atexo_Message())->getInfoConsultation($consultationId, $organisme, false).'
';
        $corpsMail .= Prado::localize('TEXT_LIEN_ACCES_DIRECT_CONSULTATION');
        $corpsMail .= '
';
        $pfUrl = Atexo_MultiDomaine::replaceDomain(
            Atexo_Config::getParameter('PF_URL_MESSAGERIE'),
            Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
        );
        $corpsMail .= $pfUrl.'?page=Agent.TableauDeBord&AS=0&id='.$consultationId;
        $corpsMail .= '

';
        $corpsMail .= Prado::localize('HORODATAGE_DEPOT').' : '.Atexo_Util::iso2frnDateTime($arrayDataFromReponseReceieved['dateDepot'], false, true).'

';
        $corpsMail .= Prado::localize('CORPS_FIN').','.'

';
        $corpsMail .= Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');

        return $corpsMail;
    }

    /**
     * Fonction qui retourne une chaine contenant la liste des fichiers deposes, le tableau transmis en parametre doit etre celui
     * emis par la fonction self::reponseReceieved.
     *
     * @param array retourne par la fonction self::reponseReceieved
     *
     * @return String, les fichiers deposee
     */
    public function getNomsFichiersFromArray($arrayData, $acteEngagementSigne, $retourChariot, $returnArrayInfo = false, $org = null)
    {
        $texte = '';
        $texteCandidature = '';
        $texteOffreTechnique = '';
        $texteOffre = '';
        $texteAnonymat = '';
        $arrayCandidature = [];
        $arrayResult = [];
        if (is_array($arrayData)) {
            foreach ($arrayData['enveloppe'] as $typeEnv => $lotsFichiers) {
                foreach ($lotsFichiers as $numLot => $typesFichiers) {
                    foreach ($typesFichiers as $type => $fichiers) {
                        foreach ($fichiers as $idFichier => $fichier) {
                            if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                                $texteCandidature .= Prado::localize('FICHIER_CANDIDATURE').' : '.Atexo_Util::utf8ToIso($fichier['nom']);
                                if ('' != $fichier['signatureDetail']) {
                                    $texteCandidature .= ' / '.$fichier['signatureDetail'];
                                }
                                if ($returnArrayInfo) {
                                    $linkCandidature = 'index.php?page=Entreprise.EntrepriseDownloadSignature&fichier='.$idFichier.'&orgAcronyme='.$org;
                                    $arrayCandidature['piece'] = Prado::localize('FICHIER_CANDIDATURE');
                                    $arrayCandidature['nomFichier'] = Atexo_Util::utf8ToIso($fichier['nom']);
                                    $arrayCandidature['lien'] = $linkCandidature;
                                    $arrayCandidature['signature'] = $fichier['signature'];
                                    $arrayCandidature['dateSignature'] = $fichier['dateSignature'];
                                    $arrayCandidature['signatureDetail'] = $fichier['signatureDetail'];
                                    $arrayCandidature['fileSize'] = $fichier['fileSize'];
                                    $arrayCandidature['CnSubject'] = $fichier['CnSubject'];
                                    $arrayCandidature['CnEmmeteur'] = $fichier['CnEmmeteur'];
                                    $arrayCandidature['StatutSignature'] = $fichier['StatutSignature'];
                                    $arrayResult[] = $arrayCandidature;
                                }
                                if ('r' == $retourChariot) {
                                    $texteCandidature .= '
';
                                } else {
                                    $texteCandidature .= $retourChariot;
                                }
                            }
                            if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                                if ($type == Atexo_Config::getParameter('REPONSE_FICHIER_PRINCIPAL') || $type == Atexo_Config::getParameter('REPONSE_FICHIER_SECONDAIRE')) {
                                    $texteOffreTechnique .= Prado::localize('FICHIER_ENVELOPPE_OFFRE_TECHNIQUE');
                                    $txtOfrTech = Prado::localize('FICHIER_ENVELOPPE_OFFRE_TECHNIQUE');
                                }
                                if ($numLot) {
                                    $texteOffreTechnique .= ' '.Prado::localize('TEXT_LOT').' '.$numLot;
                                    $txtOfrTech .= ' '.Prado::localize('TEXT_LOT').' '.$numLot;
                                }

                                $texteOffreTechnique .= ' : '.Atexo_Util::utf8ToIso($fichier['nom']);
                                if ('' != $fichier['signatureDetail']) {
                                    $texteOffreTechnique .= ' / '.$fichier['signatureDetail'];
                                }
                                if ($returnArrayInfo) {
                                    $arrayOffreTechnique = [];
                                    $linkOffreTechnique = 'index.php?page=Entreprise.EntrepriseDownloadSignature&fichier='.$idFichier.'&orgAcronyme='.$org;
                                    $arrayOffreTechnique['piece'] = $txtOfrTech;
                                    $arrayOffreTechnique['nomFichier'] = Atexo_Util::utf8ToIso($fichier['nom']);
                                    $arrayOffreTechnique['lien'] = $linkOffreTechnique;
                                    $arrayOffreTechnique['signature'] = $fichier['signature'];
                                    $arrayOffreTechnique['dateSignature'] = $fichier['dateSignature'];
                                    $arrayOffreTechnique['signatureDetail'] = $fichier['signatureDetail'];
                                    $arrayOffreTechnique['fileSize'] = $fichier['fileSize'];
                                    $arrayOffreTechnique['CnSubject'] = $fichier['CnSubject'];
                                    $arrayOffreTechnique['CnEmmeteur'] = $fichier['CnEmmeteur'];
                                    $arrayOffreTechnique['StatutSignature'] = $fichier['StatutSignature'];
                                    $arrayResult[] = $arrayOffreTechnique;
                                    $txtOfrTech = '';
                                }
                                if ('r' == $retourChariot) {
                                    $texteOffreTechnique .= '
';
                                } else {
                                    $texteOffreTechnique .= $retourChariot;
                                }
                            }

                            if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                                if ('1' == $acteEngagementSigne) {
                                    if ($type == Atexo_Config::getParameter('REPONSE_FICHIER_ACTE_ENGAGEMENT')) {
                                        $texteOffre .= Prado::localize('AE');
                                        $txtOfr = Prado::localize('AE');
                                    } elseif ($type == Atexo_Config::getParameter('REPONSE_FICHIER_PRINCIPAL') || $type == Atexo_Config::getParameter('REPONSE_FICHIER_SECONDAIRE')) {
                                        $texteOffre .= Prado::localize('AUTRE_PIECE');
                                        $txtOfr = Prado::localize('AUTRE_PIECE');
                                    }
                                } else {
                                    if ($type == Atexo_Config::getParameter('REPONSE_FICHIER_PRINCIPAL') || $type == Atexo_Config::getParameter('REPONSE_FICHIER_SECONDAIRE')) {
                                        $texteOffre .= Prado::localize('FICHIER_ENVELOPPE_OFFRE');
                                        $txtOfr = Prado::localize('FICHIER_ENVELOPPE_OFFRE');
                                    }
                                }

                                if ($numLot) {
                                    $texteOffre .= ' '.Prado::localize('TEXT_LOT').' '.$numLot;
                                    $txtOfr .= ' '.Prado::localize('TEXT_LOT').' '.$numLot;
                                }
                                $texteOffre .= ' : '.Atexo_Util::utf8ToIso($fichier['nom']);
                                if ('' != $fichier['signatureDetail']) {
                                    $texteOffre .= ' / '.$fichier['signatureDetail'];
                                }
                                if ($returnArrayInfo) {
                                    $arrayOffre = [];
                                    $linkOffre = 'index.php?page=Entreprise.EntrepriseDownloadSignature&fichier='.$idFichier.'&orgAcronyme='.$org;
                                    $arrayOffre['piece'] = $txtOfr;
                                    $arrayOffre['nomFichier'] = Atexo_Util::utf8ToIso($fichier['nom']);
                                    $arrayOffre['lien'] = $linkOffre;
                                    $arrayOffre['signature'] = $fichier['signature'];
                                    $arrayOffre['dateSignature'] = $fichier['dateSignature'];
                                    $arrayOffre['signatureDetail'] = $fichier['signatureDetail'];
                                    $arrayOffre['fileSize'] = $fichier['fileSize'];
                                    $arrayOffre['CnSubject'] = $fichier['CnSubject'];
                                    $arrayOffre['CnEmmeteur'] = $fichier['CnEmmeteur'];
                                    $arrayOffre['StatutSignature'] = $fichier['StatutSignature'];
                                    $arrayResult[] = $arrayOffre;
                                    $txtOfr = '';
                                }
                                if ('r' == $retourChariot) {
                                    $texteOffre .= '
';
                                } else {
                                    $texteOffre .= $retourChariot;
                                }
                            }
                            if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
                                $texteAnonymat .= Prado::localize('ENVELOPPE_ANONYMAT_REPONSE');
                                $txtAnoy = Prado::localize('ENVELOPPE_ANONYMAT_REPONSE');
                                if ($numLot) {
                                    $texteAnonymat .= ' '.Prado::localize('TEXT_LOT').' '.$numLot;
                                    $txtAnoy .= ' '.Prado::localize('TEXT_LOT').' '.$numLot;
                                }
                                $texteAnonymat .= ' : '.Atexo_Util::utf8ToIso($fichier['nom']);
                                if ('' != $fichier['signatureDetail']) {
                                    $texteAnonymat .= ' / '.$fichier['signatureDetail'];
                                }
                                if ($returnArrayInfo) {
                                    $arrayAnonymat = [];
                                    $linkAnonymat = 'index.php?page=Entreprise.EntrepriseDownloadSignature&fichier='.$idFichier.'&orgAcronyme='.$org;
                                    $arrayAnonymat['piece'] = $txtAnoy;
                                    $arrayAnonymat['nomFichier'] = Atexo_Util::utf8ToIso($fichier['nom']);
                                    $arrayAnonymat['lien'] = $linkAnonymat;
                                    $arrayAnonymat['signature'] = $fichier['signature'];
                                    $arrayAnonymat['dateSignature'] = $fichier['dateSignature'];
                                    $arrayAnonymat['signatureDetail'] = $fichier['signatureDetail'];
                                    $arrayAnonymat['fileSize'] = $fichier['fileSize'];
                                    $arrayAnonymat['CnSubject'] = $fichier['CnSubject'];
                                    $arrayAnonymat['CnEmmeteur'] = $fichier['CnEmmeteur'];
                                    $arrayAnonymat['StatutSignature'] = $fichier['StatutSignature'];
                                    $arrayResult[] = $arrayAnonymat;
                                    $txtAnoy = '';
                                }
                                if ('r' == $retourChariot) {
                                    $texteAnonymat .= '
';
                                } else {
                                    $texteAnonymat .= $retourChariot;
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($returnArrayInfo) {
            return $arrayResult;
        }

        $texte = $texteCandidature.$texteOffreTechnique.$texteOffre.$texteAnonymat;

        return $texte;
    }

    public function createOldXmlFromNewOne($xml)
    {
        $domDocument = new DOMDocument();
        if (!empty($xml)) {
            $domDocument->loadXML($xml);
        }

        $reponseAnnonce = $domDocument->getElementsByTagName('ReponseAnnonce')->item(0);
        if ($reponseAnnonce) {
            $reponseAnnonce->setAttribute('idReponseAnnonce', '0');
            $reponseAnnonce->removeAttribute('reference');
            $reponseAnnonce->removeAttribute('organisme');
            $reponseAnnonce->removeAttribute('horodatageDepot');
        }

        $operateurEconomique = $domDocument->getElementsByTagName('OperateurEconomique')->item(0);
        if ($operateurEconomique) {
            $nom = $operateurEconomique->getElementsByTagName('Nom')->item(0);
            if ($nom) {
                $operateurEconomique->removeChild($nom);
            }
        }

        $annonce = $domDocument->getElementsByTagName('Annonce')->item(0);
        if ($annonce) {
            $annonce->removeAttribute('chiffrement');
            $annonce->removeAttribute('typeProcedure');
            $annonce->removeAttribute('allotissement');
            $annonce->removeAttribute('organisme');
            $annonce->removeAttribute('nombreLots');
            $annonce->removeAttribute('entiteAchat');
            $annonce->removeAttribute('separationSignatureAe');
            $annonce->removeAttribute('signature');
            $annonce->removeAttribute('reference');
            $annonce->removeAttribute('referenceUtilisateur');
            $annonce->removeAttribute('dateLimite');

            $intitule = $annonce->getElementsByTagName('Intitule')->item(0);
            $annonce->removeChild($intitule);

            $objet = $annonce->getElementsByTagName('Objet')->item(0);
            $annonce->removeChild($objet);
        }
        foreach ($domDocument->getElementsByTagName('Enveloppe') as $enveloppe) {
            $enveloppe->setAttribute('idEnveloppe', '0');
        }

        foreach ($domDocument->getElementsByTagName('BlocChiffrement') as $bloc) {
            $bloc->setAttribute('nomBloc', '');
        }

        $xml = $domDocument->saveXML();
        $xml = str_replace('<OperateurEconomique/>', '<OperateurEconomique></OperateurEconomique>', $xml);
        $xml = str_replace('<Annonce/>', '<Annonce></Annonce>', $xml);

        return $xml;
    }

    public function retrieveConstitutionDossierReponse($idOffre, $organisme, $retourChariot = '<br />')
    {
        $constitutionDossierAnonymat = null;
        $constitutionDossierCandidature = null;
        $constitutionDossierOffreTechnique = null;
        $constitutionDossierOffre = null;
        $texteCandidature = self::constitutionDossier($idOffre, $organisme, Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'), $retourChariot);
        if ($texteCandidature) {
            $constitutionDossierCandidature = Prado::localize('FICHIER_CANDIDATURE').' : '.$texteCandidature.$retourChariot;
        }

        $texteOffreTechnique = self::constitutionDossier($idOffre, $organisme, Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'), $retourChariot);
        if ($texteOffreTechnique) {
            $constitutionDossierOffreTechnique = Prado::localize('FICHIER_PIECES_OFFRE_TECHNIQUE').' : '.$texteOffreTechnique.$retourChariot;
        }

        $texteOffre = self::constitutionDossier($idOffre, $organisme, Atexo_Config::getParameter('TYPE_ENV_OFFRE'), $retourChariot);
        if ($texteOffre) {
            $constitutionDossierOffre = Prado::localize('FICHIER_ENVELOPPE_OFFRE').' : '.$texteOffre.$retourChariot;
        }

        $texteAnonymat = self::constitutionDossier($idOffre, $organisme, Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'), $retourChariot);
        if ($texteAnonymat) {
            $constitutionDossierAnonymat .= Prado::localize('FICHIER_PIECES_ANONYMAT').' : '.$texteAnonymat.$retourChariot;
        }

        return $constitutionDossierCandidature.$constitutionDossierOffreTechnique.$constitutionDossierOffre.$constitutionDossierAnonymat;
    }

    public function constitutionDossier($idOffre, $organisme, $typeEnv, $retourChariot)
    {
        $texte = '';
        $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesByIdOffre($idOffre, $organisme, $typeEnv);
        if (is_array($enveloppes)) {
            foreach ($enveloppes as $enveloppe) {
                $fichiersEnveloppes = (new Atexo_Consultation_Responses())->retrieveAllFichierEnveloppeByIdEnveloppe($enveloppe->getIdEnveloppeElectro(), $organisme);
                if (is_array($fichiersEnveloppes)) {
                    foreach ($fichiersEnveloppes as $fichierEnveloppe) {
                        $texte .= $fichierEnveloppe->getNomFichier().' ('.Atexo_Util::arrondirSizeFile($fichierEnveloppe->getTailleFichier() / 1024).')'.$retourChariot;
                    }
                }
            }
        }

        return trim($texte, $retourChariot);
    }

    public function retrieveSignatureOffreFromXml($xmlString)
    {
        $xmlObject = simplexml_load_string($xmlString);
        $enveloppes = $xmlObject->Enveloppes;
        if (empty($enveloppes)) {
            return false;
        }
        foreach ($enveloppes->children() as $enveloppe) {
            if (empty($enveloppe->children())) {
                return false;
            }
            foreach ($enveloppe->children() as $fichier) {
                $signature = trim((string) $fichier->Signature);
                if ($signature) {
                    return $signature;
                }
            }
        }

        return false;
    }

    public function retrieveFichierEnveloppeByIdOffre($idOffre, $organisme)
    {
        $allFichiers = [];
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        //TODO : utiliser getOffres et getFichierenveloppes($criteria = null, $con = null)
        $c = new Criteria();
        $c->add(CommonEnveloppePeer::OFFRE_ID, $idOffre);
        $c->add(CommonEnveloppePeer::ORGANISME, $organisme);
        $enveloppes = CommonEnveloppePeer::doSelect($c, $propelConnection);

        if (is_array($enveloppes)) {
            foreach ($enveloppes as $enveloppe) {
                $fichiers = $enveloppe->getCommonFichierEnveloppes($propelConnection);
                if (is_array($fichiers)) {
                    $allFichiers = array_merge($allFichiers, $fichiers);
                }
            }
        }

        return $allFichiers;
    }

    public function retrieveUnFichierEnveloppeByIdOffre($idOffre, $organisme)
    {
        $fichiers = self::retrieveFichierEnveloppeByIdOffre($idOffre, $organisme);
        if (is_array($fichiers)) {
            $fichier = array_shift($fichiers);
            if ($fichier instanceof CommonFichierEnveloppe) {
                return $fichier;
            }
        }

        return false;
    }

    public function deleteBlobsReponse($idOffre, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $atexoBlob = new Atexo_Blob();
        $fichiers = self::retrieveFichierEnveloppeByIdOffre($idOffre, $organisme);
        if (is_array($fichiers)) {
            foreach ($fichiers as $fichier) {
                $blocs = $fichier->getBlocfichierenveloppes(null, $connexionCom);
                if (is_array($blocs)) {
                    foreach ($blocs as $bloc) {
                        $atexoBlob->deleteBlobFile($bloc->getIdBlobChiffre(), $organisme);

                        $bloc->setIdBlobChiffre(0);
                        $bloc->save($connexionCom);
                    }
                }
            }
        }
    }

    public function corpsMailAnnulation($idInscrit, $consultationId, $idOffre, $organisme, $mail, $texteConstitutionDossierReponse)
    {
        $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($idOffre, $organisme);
        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritAndEntreprise(Atexo_CurrentUser::getIdInscrit());
        $inscrit = array_shift($inscrit);
        $consultation = (new Atexo_Consultation())->retrieveCommonConsultation($consultationId, $organisme);

        $corpsMail = file_get_contents(Atexo_Config::getParameter('PATH_FILE_MAIL_TEMPLATE_ANNULATION_DEPOT'));

        $corpsMail = (new Atexo_Message())->getInfoConsultationV2($consultationId, $organisme, false, $corpsMail);
        $corpsMail = str_replace(DEFINE_TEXT_INTRODUCTION_MESSAGE, Prado::Localize('INTRODUCTION_MESSAGE_ANNULATION_DEPOT'), $corpsMail);
        $corpsMail = str_replace(DEFINE_TEXT_ENTITE_PUBLIC, Prado::Localize('DEFINE_TEXT_ENTITE_PUBLIC'), $corpsMail);
        $corpsMail = str_replace(DEFINE_ENTITE_ACHAT, Prado::Localize('DEFINE_ENTITE_ACHAT'), $corpsMail);
        $corpsMail = str_replace(LABEL_INTITULE_CONSULTATION, Prado::Localize('DEFINE_INTITULE'), $corpsMail);
        $corpsMail = str_replace(LABEL_OBJET_CONSULTATION, Prado::Localize('DEFINE_OBJET_CONSULTATION'), $corpsMail);
        $corpsMail = str_replace(LABEL_REFERENCE_CONSULTATION, Prado::Localize('DEFINE_TEXT_REFERENCE'), $corpsMail);
        $corpsMail = str_replace(LABEL_TYPE_PROCEDURE, Prado::Localize('TEXT_TYPE_PROCEDURE'), $corpsMail);
        $corpsMail = str_replace(LABEL_DATE_MISE_EN_LIGNE, Prado::Localize('DEFINE_DATE_MISE_EN_LIGNE'), $corpsMail);
        if (true) {
            if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                $corpsMail = str_replace(LABEL_HORODATE_LIMITE_REMISE_PLIS, Prado::Localize('DEFINE_DATE_LIMITE_REMISE_PLI'), $corpsMail);
            } else {
                $corpsMail = str_replace(LABEL_HORODATE_LIMITE_REMISE_PLIS, Prado::localize('DEFINE_TEXT_DATE_FIN_AFFICHAGE'), $corpsMail);
            }
        }
        $corpsMail = str_replace(LABEL_HORODATAGE_ANNULATION, Prado::Localize('DEFINE_HORODATGE_ANNULATION'), $corpsMail);
        $corpsMail = str_replace(LABEL_CARACTERISTIQUE_DEPOT_ANNULE, Prado::Localize('CARACTERISTIQUE_DEPOT_ANNULE'), $corpsMail);
        $corpsMail = str_replace(LABEL_ENTREPRISE, Prado::Localize('DEFINE_TEXT_STATISTIQUES_ENTREPRISE'), $corpsMail);
        $corpsMail = str_replace(LABEL_NOM, Prado::Localize('NOM'), $corpsMail);
        $corpsMail = str_replace(LABEL_PRENOM, Prado::Localize('PRENOM'), $corpsMail);
        $corpsMail = str_replace(LABEL_EMAIL, Prado::Localize('TEXT_ADRESSE_ELECTRONIQUE'), $corpsMail);
        $corpsMail = str_replace(LABEL_HORODATAGE_DEPOT, Prado::Localize('HORODATAGE_DEPOT'), $corpsMail);
        $corpsMail = str_replace(LABEL_CONTENU_REPONSE, Prado::Localize('CONTENU_REPONSE'), $corpsMail);

        $placeMarchePublicInterministere = Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');
        if ($consultation instanceof CommonConsultation
            && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
            $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
            $placeMarchePublicInterministere = $plateformeVirtuelle->getFooterMail();
        }

        $corpsMail = str_replace(
            PLACE_MARCHE_PUBLIC_INTERMINISTERE,
            $placeMarchePublicInterministere,
            $corpsMail
        );

        $corpsMail = str_replace('VALEUR_HORODATAGE_ANNULATION', Atexo_Util::iso2frnDateTime($offre->getDateAnnulation()), $corpsMail);
        $corpsMail = str_replace('VALEUR_ENTREPRISE', $inscrit->getEntreprise()->getNom(), $corpsMail);
        $corpsMail = str_replace('VALEUR_NOM', $inscrit->getNom(), $corpsMail);
        $corpsMail = str_replace('VALEUR_PRENOM', $inscrit->getPrenom(), $corpsMail);
        $corpsMail = str_replace('VALEUR_EMAIL', $mail, $corpsMail);
        $corpsMail = str_replace('VALEUR_HORODATAGE_DEPOT', Atexo_Util::iso2frnDateTime($offre->getUntrusteddate(), false, true), $corpsMail);
        $corpsMail = str_replace('VALEUR_CONTENU_REPONSE', $texteConstitutionDossierReponse, $corpsMail);

        return $corpsMail;
    }

    public function createArrayFromXmlOffre($xmlOffre)
    {
        $arrayDataToReturn = [];
        $xmlObject = simplexml_load_string($xmlOffre);
        $enveloppes = $xmlObject->Enveloppes;
        if ($enveloppes) {
            foreach ($enveloppes->children() as $enveloppe) {
                $attributesEnveloppe = $enveloppe->attributes();
                foreach ($enveloppe->children() as $fichier) {
                    $attributesFile = $fichier->attributes();
                    $arrayDataToReturn['enveloppe'][((string) $attributesEnveloppe['type'])][((string) $attributesEnveloppe['numLot'])][(string) $attributesFile['typeFichier']][] = ((string) $fichier->Nom).
                        ' ('.(Atexo_Util::arrondirSizeFile(((int) $attributesFile['tailleEnClair']) / 1024)).')';
                }
            }
        }

        return $arrayDataToReturn;
    }

    public static function getUrlVerifyCertificate($organisme)
    {
        return Atexo_Util::getAbsoluteUrl().Atexo_Config::getParameter('PAGE_VERIFY_CERTIFICATE').'&org='.$organisme;
    }

    /**
     * Recupere les fichiers joint a une offre electronique.
     *
     * @param $organisme: l'acronyme de l'organisme
     * @param $refCons: la reference de la consultation
     * @param $entrepriseId: l'id de l'entreprise de l'inscrit connecte
     * @param $inscritId: l'id de l'inscrit connecte
     *
     * @return tableau d'elements de fichiers
     */
    public function recupererFichiersOffre($organisme, $consultationId, $entrepriseId, $inscritId, $acteEngagement)
    {
        $arrayData = [];
        if (!$inscritId) {
            return [];
        }
        $offres = self::retrieveReponsesEntreprise($consultationId, $organisme, $inscritId, $entrepriseId);
        if (is_array($offres) && count($offres)) {
            $arrayData = [];
            foreach ($offres as $offre) {
                //$arrayData[$offre->getId()] = Atexo_Entreprise_Reponses::getNomsFichiersFromArray(self::arrayFilesFromXmlOffre($offre->getXmlString()),$acteEngagement ,'<br />',true ,$organisme);
                $arrayData[$offre->getId()] = (new Atexo_Entreprise_Reponses())->getNomsFichiersFromArray(self::arrayFilesFromOffre($offre), $acteEngagement, '<br />', true, $organisme);
            }
        }

        return $arrayData;
    }

    public function createArrayFilesFromXmlOffre($xmlOffre)
    {
        $arrayDataToReturn = [];
        $xmlObject = simplexml_load_string($xmlOffre);
        $enveloppes = $xmlObject->Enveloppes;
        if ($enveloppes) {
            foreach ($enveloppes->children() as $enveloppe) {
                $attributesEnveloppe = $enveloppe->attributes();
                foreach ($enveloppe->children() as $fichier) {
                    $attributesFile = $fichier->attributes();
                    $arrayDataToReturn['enveloppe'][((string) $attributesEnveloppe['type'])][((string) $attributesEnveloppe['numLot'])][(string) $attributesFile['typeFichier']][] = ((string) $fichier->Nom).
                    ' ('.(Atexo_Util::arrondirSizeFile(((int) $attributesFile['tailleEnClair']) / 1024)).')';
                }
            }
        }

        return $arrayDataToReturn;
    }

    /**
     * Retourne un tableau de donnees recuperees a travers l'xml de l'offre.
     * Ce tableau doit etre le premier parametre pour la fonction getNomsFichiersFromArray() de cette classe.
     *
     * @param $xmlOffre de la table Offres
     *
     * @return array() de donnees contenant les fichiers de l'offre
     */
    public function arrayFilesFromXmlOffre($xmlOffre)
    {
        $arrayDataToReturn = [];
        $arrayTypeEnv = [];
        $arrayTypeEnv[1] = Prado::localize('TYPE_ENV_CANDIDATURE');
        $arrayTypeEnv[2] = Prado::localize('TYPE_ENV_OFFRE');
        $arrayTypeEnv[3] = Prado::localize('TYPE_ENV_ANONYMAT');
        $arrayTypeEnv[4] = Prado::localize('TYPE_ENV_OFFRE_TECHNIQUE');

        $xmlObject = simplexml_load_string($xmlOffre);
        $enveloppes = $xmlObject->Enveloppes;
        if ($enveloppes) {
            foreach ($enveloppes->children() as $enveloppe) {
                $attributesEnveloppe = $enveloppe->attributes();
                $i = 0;
                foreach ($enveloppe->children() as $fichier) {
                    $attributesFile = $fichier->attributes();
                    $arrayDataToReturn['enveloppe'][((string) $attributesEnveloppe['type'])][((string) $attributesEnveloppe['numLot'])][(string) $attributesFile['typeFichier']][$i]['nom'] = (string) $fichier->Nom;
                    $arrayDataToReturn['enveloppe'][((string) $attributesEnveloppe['type'])][((string) $attributesEnveloppe['numLot'])][(string) $attributesFile['typeFichier']][$i]['taille'] = (Atexo_Util::arrondirSizeFile(((int) $attributesFile['tailleEnClair']) / 1024));
                    $arrayDataToReturn['enveloppe'][((string) $attributesEnveloppe['type'])][((string) $attributesEnveloppe['numLot'])][(string) $attributesFile['typeFichier']][$i]['signature'] = (string) $fichier->Signature;
                    $arrayDataToReturn['enveloppe'][((string) $attributesEnveloppe['type'])][((string) $attributesEnveloppe['numLot'])][(string) $attributesFile['typeFichier']][$i]['piece'] = $arrayTypeEnv[(int) $attributesEnveloppe['type']];
                    ++$i;
                }
            }
        }

        return $arrayDataToReturn;
    }

    public function arrayFilesFromOffre($Offre)
    {
        $arrayDataToReturn = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $arrayTypeEnv = [];
        $arrayTypeEnv[1] = Prado::localize('TYPE_ENV_CANDIDATURE');
        $arrayTypeEnv[2] = Prado::localize('TYPE_ENV_OFFRE');
        $arrayTypeEnv[3] = Prado::localize('TYPE_ENV_ANONYMAT');
        $arrayTypeEnv[4] = Prado::localize('TYPE_ENV_OFFRE_TECHNIQUE');

        $enveloppes = $Offre->getCommonEnveloppes();
        if ($enveloppes) {
            foreach ($enveloppes as $enveloppe) {
                $i = 0;
                $fichiersEnveloppe = $enveloppe->getCommonFichierEnveloppes($connexionCom);
                foreach ($fichiersEnveloppe as $fichier) {
                    $arrayDataToReturn['enveloppe'][((string) $enveloppe->getTypeEnv())][((string) $enveloppe->getSousPli())][(string) $fichier->getTypeFichier()][$i]['nom'] = (string) $fichier->getNomFichier();
                    $arrayDataToReturn['enveloppe'][((string) $enveloppe->getTypeEnv())][((string) $enveloppe->getSousPli())][(string) $fichier->getTypeFichier()][$i]['taille'] = (Atexo_Util::arrondirSizeFile(((int) $fichier->getTailleFichier()) / 1024));
                    $arrayDataToReturn['enveloppe'][((string) $enveloppe->getTypeEnv())][((string) $enveloppe->getSousPli())][(string) $fichier->getTypeFichier()][$i]['signature'] = (string) $fichier->getSignatureFichier();
                    $arrayDataToReturn['enveloppe'][((string) $enveloppe->getTypeEnv())][((string) $enveloppe->getSousPli())][(string) $fichier->getTypeFichier()][$i]['piece'] = $arrayTypeEnv[(int) $enveloppe->getTypeEnv()];
                    $arrayDataToReturn['enveloppe'][((string) $enveloppe->getTypeEnv())][((string) $enveloppe->getSousPli())][(string) $fichier->getTypeFichier()][$i]['dateSignature'] = $Offre->getUntrusteddate();
                    ++$i;
                }
            }
        }

        return $arrayDataToReturn;
    }

    /**
     * Retourne les offres deposees par un inscrit donne pour une consultation donnee.
     */
    public static function retrieveReponsesEntreprise($consultationId, $organisme, $idInscrit, $idEntreprise, $returnCount = false)
    {
        if (0 == $idEntreprise || 0 == $idInscrit) {
            return false;
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonOffresPeer::INSCRIT_ID, $idInscrit);
        $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonOffresPeer::ORGANISME, $organisme);
        $c->add(CommonOffresPeer::ENTREPRISE_ID, $idEntreprise);
        $c->add(CommonOffresPeer::STATUT_OFFRES, Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);

        if ($returnCount) {
            $countOffres = CommonOffresPeer::doCount($c, $connexion);

            return $countOffres;
        } else {
            $offres = CommonOffresPeer::doSelect($c, $connexion);

            return $offres;
        }
    }

    public function callModuleValidation($signature, $organisme, $item)
    {
        $postData = [];
        try {
            $postData['contenuFichierXML'] = $signature;
            $postData['typeEchange'] = 'EnrichissementSignature';
            $postData['typeAlgorithmHash'] = Atexo_Config::getParameter('TYPE_ALGORITHM_HASH');
            $postData['item'] = $item;
            $postData['organisme'] = Atexo_Config::getParameter('ORIGINE_ORGANISME');
            $postData['plateforme'] = Atexo_Config::getParameter('ORIGINE_PLATEFORME');
            $postData['contexteMetier'] = 'MPE_depot';
            $postData['item'] = $item;

            return (new Atexo_Crypto_CurlSend())->sendDataToModuleValidation($postData);
        } catch (Exception) {
            //Unknown status
            return '3-3-3';
        }
    }

    public function getEmprienteFichierFromXmlReponse($xmlStr, $fichierEnveloppe)
    {
        if ('' != $xmlStr) {
            $xmlObject = simplexml_load_string($xmlStr);
            $enveloppes = $xmlObject->Enveloppes;
            if ($enveloppes && $enveloppes->children()) {
                foreach ($enveloppes->children() as $enveloppe) {
                    $attributesEnveloppe = $enveloppe->attributes();
                    if ($attributesEnveloppe['idEnveloppe'] == $fichierEnveloppe->getIdEnveloppe()) {
                        if ($enveloppe && $enveloppe->children()) {
                            foreach ($enveloppe->children() as $fichier) {
                                $attributesFile = $fichier->attributes();
                                if ($fichierEnveloppe->getTypeFichier() == (string) $attributesFile['typeFichier'] && $fichierEnveloppe->getNumOrdreFichier() == (string) $attributesFile['numOrdre']) {
                                    return trim((string) $fichier->Empreinte);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /*
     * Methode permet d'appeler Atexo forms par curl pour creer les enveloppes de dormulaire de reponse
     * @param $uid,$uidReponse,$uidEnveloppe
     */
    public function creerFormsReponses($uid, $uidReponse, $uidEnveloppe)
    {
        $url = Atexo_Config::getParameter('URL_REPONSE_FORMS_PF').'index.php?page=frame.ReponsesFrame';
        $postfields = ['uid' => $uid, 'uidReponse' => $uidReponse, 'uidEnv' => $uidEnveloppe];

        $ch = curl_init();    // initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, CURLOPT_CONNECTTIMEOUT);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
        $result = curl_exec($ch); // run the whole process

        return $result;
    }

    public function getListeFichierDepot($organisme, $refCons, $entrepriseId, $inscritId)
    {
        $arrayData = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$inscritId) {
            return [];
        }
        $offres = self::retrieveReponsesEntreprise($refCons, $organisme, $inscritId, $entrepriseId);
        if (is_array($offres) && count($offres)) {
            $arrayData = [];
            foreach ($offres as $offre) {
                $arrayFiles = [];
                $enveloppes = $offre->getCommonEnveloppes(true, null, null, $connexionCom);
                if ($enveloppes) {
                    foreach ($enveloppes as $enveloppe) {
                        $fichiersEnveloppe = $enveloppe->getCommonFichierEnveloppes($connexionCom);
                        foreach ($fichiersEnveloppe as $fichier) {
                            $arrayFiles[$enveloppe->getSousPli()]['numLot'] = $enveloppe->getSousPli();
                            $arrayFiles[$enveloppe->getSousPli()]['enveloppe'][$enveloppe->getTypeEnv()]['typesEnv'] = $enveloppe->getTypeEnv();
                            $arrayFiles[$enveloppe->getSousPli()]['enveloppe'][$enveloppe->getTypeEnv()]['organisme'] = $enveloppe->getOrganisme();
                            $arrayFiles[$enveloppe->getSousPli()]['enveloppe'][$enveloppe->getTypeEnv()]['id_enveloppe'] = $enveloppe->getIdEnveloppeElectro();
                            $arrayFiles[$enveloppe->getSousPli()]['enveloppe'][$enveloppe->getTypeEnv()]['pieces'][$fichier->getTypePiece()]['typesPieces'] = $fichier->getTypePiece();
                            $arrayFiles[$enveloppe->getSousPli()]['enveloppe'][$enveloppe->getTypeEnv()]['pieces'][$fichier->getTypePiece()]['fichiers'][$fichier->getIdFichier()] = $fichier;
                        }
                    }
                }
                $arrayData[$offre->getId()]['idOffre'] = $offre->getId();
                $arrayData[$offre->getId()]['lot'] = $arrayFiles;
            }
        }

        return $arrayData;
    }

    /*
     * Permet de retourner la date de la signature a partir de la signature
     */
    public function getDateCreationSignature($xmlSignature)
    {
        $firstPart = strstr($xmlSignature, '<xades:SigningTime>');
        //      $secondPart = strstr($xmlSignature,'</xades:SigningTime>',true ); //PHP 5.3.0

        $positionSecondPart = strpos($firstPart, '</xades:SigningTime>');
        $longSign = strlen($firstPart);
        $secondPart = substr($firstPart, 0, -($longSign - $positionSecondPart));

        $dateCreation = trim(str_replace('</xades:SigningTime>', '', str_replace('<xades:SigningTime>', '', $secondPart)));

        $date = Atexo_Util::formaterDateTimeMilliSecondeToString($dateCreation);

        return $date;
    }

    public function getOffres($consultationId, $organisme, $idsOffre)
    {
        if (is_array($idsOffre) && count($idsOffre)) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $offresQuery = new CommonOffresQuery();

            return $offresQuery->getOffresByIds($idsOffre, $consultationId, $organisme, $connexion);
        }

        return false;
    }

    public function addEnveloppesToDechiffre(&$offres, $arrayIdsEnveloppe)
    {
        if (is_array($arrayIdsEnveloppe) && count($arrayIdsEnveloppe) && is_array($offres) && count($offres)) {
            foreach ($offres as $offre) {
                if ($offre instanceof CommonOffres) {
                    $enveloppeQuery = new CommonEnveloppeQuery();
                    foreach ($arrayIdsEnveloppe as $idEnveloppe) {
                        $enveloppeToDechiffre = $enveloppeQuery->getEnveloppeByIdOffreAndId($idEnveloppe, $offre->getId());
                        if ($enveloppeToDechiffre instanceof CommonEnveloppe) {
                            $offre->addEnveloppeToDechiffre($enveloppeToDechiffre);
                        }
                    }
                }
            }
        }
    }

    /**
     * Permet de recuperer la liste des fichiers pour les quels le hash est ko.
     *
     * @param int      $idOffre
     * @param string   $organisme
     * @param int|null $idEnveloppe
     *
     * @return array
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-place
     *
     * @copyright Atexo 2018
     */
    public function retrieveFichierEnveloppeHashKo($idOffre, $organisme, $idEnveloppe = null)
    {
        $allFichiers = [];
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $enveloppeQuery = new CommonEnveloppeQuery();
        $enveloppes = $enveloppeQuery->getEnveloppeByIdOffreAndStatutHashFiles($idOffre, $organisme, '0', $idEnveloppe, $connection);
        $i = 0;
        if (is_array($enveloppes)) {
            foreach ($enveloppes as $enveloppe) {
                if ($enveloppe instanceof CommonEnveloppe) {
                    $fichiers = $enveloppe->getCommonFichierEnveloppes($connection, null, '0');
                    if (is_array($fichiers)) {
                        $allFichiers[$i]['typeEnv'] = $enveloppe->getLibelleTypeEnveloppe($enveloppe->getTypeEnv()).' '.(($enveloppe->getSousPli()) ? (Prado::Localize('DEFINE_LOT_NUMERO').' '.$enveloppe->getSousPli()) : '');
                        $allFichiers[$i]['listeFiles'] = $fichiers;
                        ++$i;
                    }
                }
            }
        }

        return $allFichiers;
    }
}
