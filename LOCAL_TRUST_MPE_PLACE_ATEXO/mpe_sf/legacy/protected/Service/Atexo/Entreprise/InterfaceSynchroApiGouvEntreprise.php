<?php

namespace Application\Service\Atexo\Entreprise;

use App\Service\ApiGateway\ApiEntreprise;
use App\Service\Asynchronous\AsyncRequests;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use Exception;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class pour gerer l'interface de synchronisation entre MPE et SGMAP.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 1.0
 *
 * @since 4.9.0
 */
class InterfaceSynchroApiGouvEntreprise
{
    public const CODE_ERREUR_TECHNIQUE = 'cas3';

    private string $token = '';
    private string $apiEntrepriseUrl = '';
    private string $apiEtablissementUrl = '';
    private string $apiEtabExerciceUrl = '';
    private string $apiAttFiscaleUrl = '';
    private string $apiAttSocialeUrl = '';
    private $logger = null;
    public const ENTREPRISE = 1;
    public const ETABLISSEMENT = 2;
    public const EXERCICE = 3;
    public const ATTESTATION_FISCALE = 4;
    public const ATTESTATION_SOCIALE = 5;

    private ?int $codeRetour = null;

    /**
     * Initialise un objet InterfaceSynchroApiGouvEntreprise
     * exemple :
     * $params = array(
     *       'token'                 => Atexo_Config::getParameter('API_ENTREPRISE_TOKEN'),
     *       'apiEntrepriseUrl'      => Atexo_Config::getParameter('URL_API_ENTREPRISE_UNITE_LEGALE'),
     *       'apiEtablissementUrl'   => Atexo_Config::getParameter('URL_API_ENTREPRISE_ETABLISSEMENT'),
     *       'apiEtabExerciceUrl'    => Atexo_Config::getParameter('URL_API_ENTREPRISE_CHIFFRES_AFFAIRES'),
     *      );
     * $logger = Atexo_LoggerManager::getLogger();
     * $ws = new InterfaceSynchroApiGouvEntreprise($logger, $params);.
     *
     * @param array  $params
     *
     * @author Gregory CHEVRET <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     * @copyright Atexo 2015
     */
    public function __construct(LoggerInterface $logger, $params = [])
    {
        if (is_array($params) && !empty($params)) {
            foreach ($params as $key => $value) {
                $this->$key = $value;
            }
        }
        $this->logger = $logger;
    }

    /**
     * Permet de recuperer les entreprise/Etablissements et transforme le json retourne
     * exemple :
     * $sirenAtexo = '440909562';
     * $result = $ws->getEntrepriseBySiren($sirenAtexo[, true]);.
     *
     * @param string $siren             : siren de l'entreprise
     * @param bool   $withEtablissement : true si l'on souhaite egalement retourne l'etablissement siege, sinon false
     *
     * @return array un tableau contenant la reponse ou les erreurs eventuelles
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @author Gregory CHEVRET <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEntrepriseBySiren($siren, $withEtablissement = false)
    {
        $oversightParams = [];
        $this->codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
        try {
            $oversightParams['debutExecution'] = new DateTime('now');
            $oversightParams['service'] = 'API_GOUV_ENTREPRISE';
            $oversightParams['nomBatch'] = 'getEntrepriseBySiren';
            $entree = serialize(['siren' => $siren, 'withEtablissement' => $withEtablissement]);
            $oversightParams['variablesEntree'] = $entree;
            $oversightParams['typeFlux'] = 'WebService';
            $oversightParams['poids'] = 0;
            $oversightParams['nbFlux'] = 1;
        } catch (Exception $exceptionOversight) {
            $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        $this->logger->info("Recuperation donnees entreprise SGMAP : siren => $siren");

        $apiUrl = $this->apiEntrepriseUrl;
        $params = $this->getParametresRequetes($siren);
        $params['siren'] = $siren;

        $url = str_replace(
            '{siren}',
            $siren,
            Atexo_Config::getParameter('URL_API_ENTREPRISE_UNITE_LEGALE')
        );

        $returnEntreprise = $this->sendToApiGouvEntreprise($url, InterfaceSynchroApiGouvEntreprise::ENTREPRISE, $params);

        if ($withEtablissement && static::isResponseWsApiGouvEntrepriseSucces($returnEntreprise) && $returnEntreprise[1] instanceof Atexo_Entreprise_EntrepriseVo) {
            $siret = $returnEntreprise[1]->getSiretSiegeSocial();
            $this->logger->info("Recuperation de l'etablissement siège de l'entreprise dont le siret => " . $siret);
            $returnEtablissement = $this->getEtablissementBySiret($siret);
            if ($returnEtablissement[1][$siret] instanceof Atexo_Entreprise_EtablissementVo) {
                $this->logger->info("L'etablissement siège de l'entreprise existe ");
                $returnEntreprise[1]->setEtablissements($returnEtablissement[1][$siret]);
            }
            $this->logger->info("Fin de recuperation de l'etablissement siège de l'entreprise dont le siret => " . $siret);
        }

        try {
            $oversightParams['code'] = $this->codeRetour;
            $oversightParams['finExecution'] = 'endScript';
            $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
            unset($oversightParams['finExecution']);
            unset($oversightParams['oversight']);
        } catch (Exception $exceptionOversight) {
            $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        return $returnEntreprise;
    }

    /**
     * Permet de recuperer les Etablissements
     * exemple :
     * $siretAtexo = '440909562000033';
     * $result = $ws->getEtablissementBySiret($siretAtexo);.
     *
     * @param string siret dans le cas de l'etablissement
     *
     * @return array un tableau contenant la reponse ou les erreurs eventuelles
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     * @author Gregory CHEVRET <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEtablissementBySiret($siret)
    {
        $oversightParams = [];
        $this->codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
        try {
            $oversightParams['debutExecution'] = new DateTime('now');
            $oversightParams['service'] = 'API_GOUV_ENTREPRISE';
            $oversightParams['nomBatch'] = 'getEtablissementBySiret';
            $entree = serialize(['siret' => $siret]);
            $oversightParams['variablesEntree'] = $entree;
            $oversightParams['typeFlux'] = 'WebService';
            $oversightParams['poids'] = 0;
            $oversightParams['nbFlux'] = 1;
        } catch (Exception $exceptionOversight) {
            $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        $apiUrl = $this->apiEtablissementUrl;

        $this->logger->info("Recuperation donnees etablissement SGMAP : siret => $siret");
        $params = $this->getParametresRequetes($siret);

        $reponse = $this->sendToApiGouvEntreprise($apiUrl . $siret, InterfaceSynchroApiGouvEntreprise::ETABLISSEMENT, $params);

        try {
            $oversightParams['code'] = $this->codeRetour;
            $oversightParams['finExecution'] = 'endScript';
            $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
            unset($oversightParams['finExecution']);
            unset($oversightParams['oversight']);
        } catch (Exception $exceptionOversight) {
            $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        return $reponse;
    }

    /**
     * Permet de recuperer les exercices d'un etablissement donnee
     * exemple :
     * $siretAtexo = '44090956200033';
     * $result = $ws->getExerciceBySiret($siretAtexo);.
     *
     * @param string siret dans le cas de l'etablissement
     *
     * @return array un tableau contenant la reponse ou les erreurs eventuelles
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     * @author Gregory CHEVRET <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getExerciceBySiret($siret)
    {
        $oversightParams = [];
        $this->codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
        try {
            $oversightParams['debutExecution'] = new DateTime('now');
            $oversightParams['service'] = 'API_GOUV_ENTREPRISE';
            $oversightParams['nomBatch'] = 'getExerciceBySiret';
            $entree = serialize(['siret' => $siret]);
            $oversightParams['variablesEntree'] = $entree;
            $oversightParams['informationMetier'] = $entree;
            $oversightParams['typeFlux'] = 'WebService';
            $oversightParams['poids'] = 0;
            $oversightParams['nbFlux'] = 1;
        } catch (Exception $exceptionOversight) {
            $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        $apiUrl = str_replace('{siret}', $siret, $this->apiEtabExerciceUrl);

        $this->logger->info("Recuperation donnees exercices exercice SGMAP : siret => $siret");
        $params = $this->getParametresRequetes($siret);

        $reponse = $this->sendToApiGouvEntreprise($apiUrl, InterfaceSynchroApiGouvEntreprise::EXERCICE, $params);

        try {
            $oversightParams['code'] = $this->codeRetour;
            $oversightParams['finExecution'] = 'endScript';
            $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
            unset($oversightParams['finExecution']);
            unset($oversightParams['oversight']);
        } catch (Exception $exceptionOversight) {
            $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        return $reponse;
    }

    /**
     * ######################################################################################################
     * #### Attention : Ce code n'est visiblement plus utilisé !!! Il a été déporter dans attestation.php ###
     * ######################################################################################################
     * Permet de recuperer l'Attestation fiscales de l'entreprise
     * exemple :
     * $siren = '4409095620';
     * $result = $ws->getAttestationFiscale($siren);.
     *
     * @param string siren de l'entreprise
     *
     * @return array un tableau contenant la reponse ou les erreurs eventuelles
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getAttestationFiscale($siren, $refCons = null)
    {
        $oversightParams = [];
        $this->codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
        try {
            $oversightParams['debutExecution'] = new DateTime('now');
            $oversightParams['service'] = 'API_GOUV_ENTREPRISE';
            $oversightParams['nomBatch'] = 'getAttestationFiscale';
            $entree = serialize(['siren' => $siren, 'refCons' => $refCons]);
            $oversightParams['variablesEntree'] = $entree;

            $oversightParams['typeFlux'] = 'WebService';
            $oversightParams['poids'] = 0;
            $oversightParams['nbFlux'] = 1;
            $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
        } catch (Exception $exceptionOversight) {
            $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        $apiUrl = $this->apiAttFiscaleUrl;
        $endpoint = str_replace('{siren}', $siren, $apiUrl);

        $this->logger->info("Recuperation de l'attestation fiscale de l'entreprise dont le siren => $siren");
        $params = $this->getParametresRequetes($siren, Atexo_Config::getParameter('TYPE_ATTESTATION_FISCALE'), $refCons);
        $reponse = $this->sendToApiGouvEntreprise($endpoint, InterfaceSynchroApiGouvEntreprise::ATTESTATION_FISCALE, $params);

        try {
            $oversightParams['code'] = $this->codeRetour;
            $oversightParams['finExecution'] = 'endScript';
            $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
            unset($oversightParams['finExecution']);
            unset($oversightParams['oversight']);
        } catch (Exception $exceptionOversight) {
            $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        return $reponse;
    }

    /**
     * Permet de remplir l'objet entrepriseVo avec les donnees du web service.
     *
     * @param array $reponse : la reponse du web service
     *
     * @return Atexo_Entreprise_EntrepriseVo : objet contenant les informations
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private function preRemplirEntrepriseVo($reponse)
    {
        $entrepriseVo = null;
        $oversightParams = [];
        $this->codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
        try {
            $oversightParams['debutExecution'] = new DateTime('now');
            $oversightParams['service'] = 'API_GOUV_ENTREPRISE';
            $oversightParams['nomBatch'] = 'preRemplirEntrepriseVo';
            $entree = serialize(['reponse' => $reponse]);
            $oversightParams['variablesEntree'] = $entree;
            $oversightParams['typeFlux'] = 'WebService';
            $oversightParams['poids'] = 0;
            $oversightParams['nbFlux'] = 1;
        } catch (Exception $exceptionOversight) {
            $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        if (class_exists(Atexo_Entreprise_EntrepriseVo::class)) {
            $this->logger->info("Remplir l'EntrepriseVo par les donnees de json recu");

            try {
                $entrepriseVo = Atexo_Entreprise_EntrepriseVo::loadFromApiGouvEntreprise($reponse['data']);
            } catch (Exception) {
                $this->codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            }

            try {
                $oversightParams['code'] = $this->codeRetour;
                $oversightParams['finExecution'] = 'endScript';
                $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
                unset($oversightParams['finExecution']);
                unset($oversightParams['oversight']);
            } catch (Exception $exceptionOversight) {
                $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
            }

            return $entrepriseVo;
        } else {
            $reponse['entreprise']['date_creation_iso'] = date('Y-m-d H:i:s', $reponse['entreprise']['date_creation']);

            try {
                $oversightParams['code'] = $this->codeRetour;
                $oversightParams['finExecution'] = 'endScript';
                $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
                unset($oversightParams['finExecution']);
                unset($oversightParams['oversight']);
            } catch (Exception $exceptionOversight) {
                $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
            }

            return $reponse['data'];
        }
    }

    /**
     * Permet de remplir l'objet etablissementVo avec les donnees du web service.
     *
     * @param array $reponse : la reponse du web service
     *
     * @return Atexo_Entreprise_EtablissementVo : objet contenant les informations
     *
     * @author Gregory CHEVRET <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private function preRemplirEtablissementVo($reponse)
    {
        $oversightParams = [];
        $this->codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
        try {
            $oversightParams['debutExecution'] = new DateTime('now');
            $oversightParams['service'] = 'API_GOUV_ENTREPRISE';
            $oversightParams['nomBatch'] = 'preRemplirEtablissementVo';
            $entree = serialize(['reponse' => $reponse]);
            $oversightParams['variablesEntree'] = $entree;
            $oversightParams['typeFlux'] = 'WebService';
            $oversightParams['poids'] = 0;
            $oversightParams['nbFlux'] = 1;
        } catch (Exception $exceptionOversight) {
            $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        if (class_exists(Atexo_Entreprise_EtablissementVo::class)) {
            $this->logger->info("Remplir l'EtablissementVo par les donnees de json recu");

            $etablissementVo = null;
            try {
                $etablissementVo = Atexo_Entreprise_EtablissementVo::loadFromApiGouvEntreprise($reponse['data']);
            } catch (Exception) {
                $this->codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            }

            try {
                $oversightParams['code'] = $this->codeRetour;
                $oversightParams['finExecution'] = 'endScript';
                $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
                unset($oversightParams['finExecution']);
                unset($oversightParams['oversight']);
            } catch (Exception $exceptionOversight) {
                $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
            }

            return $etablissementVo;
        } else {
            try {
                $oversightParams['code'] = $this->codeRetour;
                $oversightParams['finExecution'] = 'endScript';
                $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
                unset($oversightParams['finExecution']);
                unset($oversightParams['oversight']);
            } catch (Exception $exceptionOversight) {
                $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
            }

            return $reponse['data'];
        }
    }

    /**
     * Permet de remplir l'objet exercice avec les donnees du web service.
     *
     * @param array $reponse : la reponse du web service
     *
     * @return array un tableau contenant l'objet CommonEntrepriseInfoExercice
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    private function preRemplirExercice($reponse)
    {
        $listeExercices = null;
        $oversightParams = [];
        $this->codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
        try {
            $oversightParams['debutExecution'] = new DateTime('now');
            $oversightParams['service'] = 'API_GOUV_ENTREPRISE';
            $oversightParams['nomBatch'] = 'preRemplirExercice';
            $entree = serialize(['reponse' => $reponse]);
            $oversightParams['variablesEntree'] = $entree;

            $oversightParams['typeFlux'] = 'WebService';
            $oversightParams['poids'] = 0;
            $oversightParams['nbFlux'] = 1;
        } catch (Exception $exceptionOversight) {
            $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        if (class_exists('Atexo_Entreprise_Exercice')) {
            $this->logger->info('Remplir Capacite par les donnees de json recu');
            try {
                $listeExercices = (new Atexo_Entreprise_Exercice())->loadFromApiGouvEntreprise($reponse['data']);
            } catch (Exception) {
                $this->codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            }

            try {
                $oversightParams['code'] = $this->codeRetour;
                $oversightParams['finExecution'] = 'endScript';
                $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
                unset($oversightParams['finExecution']);
                unset($oversightParams['oversight']);
            } catch (Exception $exceptionOversight) {
                $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
            }

            return $listeExercices;
        } else {
            try {
                $oversightParams['code'] = $this->codeRetour;
                $oversightParams['finExecution'] = 'endScript';
                $oversightParams['oversight'] = Atexo_Oversight_Oversight::oversight($oversightParams);
                unset($oversightParams['finExecution']);
                unset($oversightParams['oversight']);
            } catch (Exception $exceptionOversight) {
                $this->logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
            }

            return $reponse['data'];
        }
    }

    /**
     * Effectue la requete vers le WS SGMAP
     * et retourne le resultat apres traitement du statut.
     *
     * @param string $url    : Url du WS
     * @param array  $params : parametres eventuel a passer au WS
     * @param int    $type   : type d'acces 1 pour entreprise, 2 pour etablissement, ... voir constante de la classe
     *
     * @return array
     *
     * @author Gregory CHEVRET<gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private function sendToApiGouvEntreprise($url, $type = 1, $params = [])
    {
        try {
            $responseContents = [];
            $config = [
                'adapter' => Curl::class,
                'curloptions' => [
                    CURLOPT_SSL_VERIFYHOST => 0,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_TIMEOUT => 30,
                ],
            ];

            if ('' != Atexo_Config::getParameter('URL_PROXY')) {
                $config = array(
                    'proxy_host' => Atexo_Config::getParameter('URL_PROXY'),
                    'proxy_port' => Atexo_Config::getParameter('PORT_PROXY'),
                );
            }

            $this->logger->info(
                sprintf(
                    'Appel du WS avec les parametres: %s URL = %s %s token = %s',
                    PHP_EOL,
                    $url,
                    PHP_EOL,
                    $this->token,
                )
            );

            // Request
            $reponseContent = null;
            if (InterfaceSynchroApiGouvEntreprise::ENTREPRISE === $type && isset($params['siren'])) {
                $siren = $params['siren'];
                $entrepriseEndpoints = [
                    $url,
                    str_replace(
                        '{siren}',
                        $siren,
                        Atexo_Config::getParameter('URL_API_ENTREPRISE_MANDATAIRES_SOCIAUX')
                    ),
                    str_replace(
                        '{siren}',
                        $siren,
                        Atexo_Config::getParameter('URL_API_ENTREPRISE_NUMERO_TVA')
                    ),
                    str_replace(
                        '{siren}',
                        $siren,
                        Atexo_Config::getParameter('URL_API_ENTREPRISE_KBIS')
                    ),
                    str_replace(
                        '{siren}',
                        $siren,
                        Atexo_Config::getParameter('URL_API_ENTREPRISE_SIEGE_SOCIAL')
                    ),
                ];

                /** @var ApiEntreprise $apiEntreprise */
                $apiEntreprise = Atexo_Util::getSfService(ApiEntreprise::class);

                /** @var AsyncRequests $asyncRequest */
                $asyncRequest = Atexo_Util::getSfService(AsyncRequests::class);

                $multipleReponses = $asyncRequest->getMultiResponseByUrl(
                    $entrepriseEndpoints,
                    Request::METHOD_GET,
                    $apiEntreprise->getRequestOptions()
                );

                $responseContents = $apiEntreprise->mergeMultipleResponseData($multipleReponses);
                $reponseContent = json_encode($responseContents);
                $statut = $responseContents ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            } else {
                $client = new Client($config);
                $response = $client->request(
                    'GET',
                    $url,
                    [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer ' . Atexo_Config::getParameter('API_ENTREPRISE_TOKEN'),
                        ],
                        'query' => $params['get'],
                    ]
                );

                // Response content
                if (($statut = $response->getStatusCode()) == Response::HTTP_OK) {
                    $reponseContent = $response->getBody() ? $response->getBody()->getContents() : null;
                }

                try {
                    $this->logger->info(sprintf('Le json recus est %s', $reponseContent));
                    $responseContents = json_decode($reponseContent, true, 512, JSON_THROW_ON_ERROR);
                } catch (Exception $e) {
                    $this->codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    $this->logger->error('Erreur lors du decodage du json : ' . $e->getMessage());
                }
            }

            $this->logger->info(
                sprintf(
                    "Code statut retour ws InterfaceSynchroApiGouvEntreprise::sendToSgmap : %s",
                    $statut
                )
            );
            $this->logger->info(
                sprintf(
                    "Content retour ws InterfaceSynchroApiGouvEntreprise::sendToSgmap : %s",
                    $reponseContent
                )
            );

            return $this->retourWs($statut, $responseContents, $type);
        } catch (Exception $e) {
            $this->codeRetour = Atexo_Oversight_Oversight::$codeErrorRemoteServiceReturnsAnError;
            $this->logger->error("Erreur lors de la connexion au web service code [$url] : " . $e->getMessage());

            return $this->retourWs('inconnu', $responseContents, $type);
        }
    }

    /**
     * Permet de gerer le retour du web service
     * Dans les retours, en cas d'erreur:
     *    ===> le 3e element du tableau contient le code d'erreur
     *    ===> le 2e element du tableau contient les details de l'erreur.
     *
     * @param array $reponse : la reponse du web service
     *
     * @return array|int : un tableau contenant la reponse ou les erreurs eventuelles
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @author Gregory CHEVRET <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private function retourWs($statut, $reponse, $type = 1): array|int
    {
        $statut = (string) $statut;
        $return = [];
        switch ($statut) {
            case '401':
                //Json invalide
                $this->logger->error('Json invalide');
                $return = ['cas1', $reponse, json_encode($reponse, JSON_THROW_ON_ERROR), $statut];
                break;
            case '404':
                //Entreprise ou Etablissement n'a pas ete trouve
                $this->logger->error('Entreprise ou Etablissement ou Attestations non trouve(e)(s)');
                $return = ['cas2', $reponse, json_encode($reponse, JSON_THROW_ON_ERROR), $statut];
                break;
            case '200':
                if (true === $reponse['reponse']['gateway_error']) {
                    $this->logger->error('Gateway error');
                    $return = [self::CODE_ERREUR_TECHNIQUE, $reponse, json_encode($reponse, JSON_THROW_ON_ERROR), $statut]; //Gateway error
                } elseif ($reponse['errors']) {
                    $this->logger->error($reponse['errors']);
                    $return = [self::CODE_ERREUR_TECHNIQUE, $reponse, json_encode($reponse, JSON_THROW_ON_ERROR), $statut];
                } else {
                    switch ($type) {
                        case InterfaceSynchroApiGouvEntreprise::ENTREPRISE:
                            $this->logger->info('cas4 : Entreprise trouvee');
                            $return = ['cas4', $this->preRemplirEntrepriseVo($reponse), json_encode($reponse, JSON_THROW_ON_ERROR), $statut];
                            break;
                        case InterfaceSynchroApiGouvEntreprise::ETABLISSEMENT:
                            //Cas5 TODO
                            $this->logger->info('cas5 : Etablissement trouve');
                            $return = ['cas5', $this->preRemplirEtablissementVo($reponse), json_encode($reponse, JSON_THROW_ON_ERROR), $statut];
                            break;
                        case InterfaceSynchroApiGouvEntreprise::EXERCICE:
                            //Cas6 TODO
                            $this->logger->info('cas6 : Exercice trouve');
                            $return = ['cas6', $this->preRemplirExercice($reponse), json_encode($reponse, JSON_THROW_ON_ERROR), $statut];
                            break;
                        case InterfaceSynchroApiGouvEntreprise::ATTESTATION_FISCALE:
                            $this->logger->info('cas7 : Attestation fiscale trouvee');
                            $url = $reponse['data']['document_url'] ?? null;

                            $return = ['cas7', $url, json_encode($reponse, JSON_THROW_ON_ERROR), $statut];
                            break;
                        case InterfaceSynchroApiGouvEntreprise::ATTESTATION_SOCIALE:
                            $this->logger->info('cas8 : Attestation sociale trouvee');
                            $url = $reponse['data']['document_url'] ?? null;

                            $return = ['cas8', $url, json_encode($reponse, JSON_THROW_ON_ERROR), $statut];
                            break;
                    }
                }
                break;
            case '422':
                //Une erreur s'est produite lors de la résolution du WS, ou des paramètres sont manquants
                $this->logger->error('Erreur lors de la résolution du WS, ou paramètres manquants');
                $return = ['cas9', $reponse, json_encode($reponse, JSON_THROW_ON_ERROR), $statut];
                break;
            case '503':
                if (InterfaceSynchroApiGouvEntreprise::ATTESTATION_SOCIALE == $type) {
                    //La plateforme d'Acoss est indisponible (cas de l'appel ws attestations sociales)
                    $this->logger->error("La plateforme d'Acoss est indisponible (cas specifique aux appels WS attestations sociales)");
                } else {
                    //Service temporairement indisponible ou en maintenance
                    $this->logger->error('Service temporairement indisponible ou en maintenance');
                }
                $return = ['cas10', $reponse, json_encode($reponse, JSON_THROW_ON_ERROR), $statut];
                break;
            default:
                $this->logger->error('Erreur inconnue');
                $return = ['cas0', $reponse, json_encode($reponse, JSON_THROW_ON_ERROR), $statut]; //Erreur inconnue
                break;
        }

        return $return;
    }

    /**
     * Permet de retourner si la communication avec ws SGMAP est reussi selon les cas retourner.
     *
     * @param array $reponse la reponse
     *
     * @return bool true si on a reussi sinon false
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public static function isResponseWsApiGouvEntrepriseSucces($reponse)
    {
        if (is_array($reponse) && 'cas4' == $reponse[0] || 'cas5' == $reponse[0] || 'cas6' == $reponse[0] || 'cas7' == $reponse[0] || 'cas8' == $reponse[0]) {
            return true;
        }

        return false;
    }

    public static function serviceIsIndisponible($reponse)
    {
        return is_array($reponse) && 'cas10' == $reponse[0];
    }

    /**
     * Permet de retourner le type d'attestation sociale.
     *
     * @param string le type
     *
     * @return string le type d'attestation sociale
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getTypeAttestationSociale($type)
    {
        return match ($type) {
            Atexo_Config::getParameter('TYPE_ATTESTATION_SOCIALE_MARCHE_PUBLIC') => 'AMP_UR',
            Atexo_Config::getParameter('TYPE_ATTESTATION_SOCIALE_VIGILANCE') => 'AVG_UR',
            default => '',
        };
    }

    /**
     * Permet de construire les parametres de la requete.
     *
     * @param string $identifiant                : siren ou siret
     * @param string $type                       : type de document
     * @param string $refUtilisateurConsultation : reference de la consultation
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR2015
     *
     * @copyright Atexo 2016
     */
    public function getParametresRequetes($identifiant, $type = null, $refUtilisateurConsultation = null)
    {
        $params = [];
        $params['context'] = 'MPS';
        $params['object'] = 'Synchronisation donnees Entreprise, Etablissement ou Exercices';
        $params['recipient'] = Atexo_Config::getParameter('API_ENTREPRISE_SIRET_CLIENT');

        if (!empty($type) && (($type == Atexo_Config::getParameter('TYPE_ATTESTATION_SOCIALE_MARCHE_PUBLIC')) || ($type == Atexo_Config::getParameter('TYPE_ATTESTATION_SOCIALE_VIGILANCE')))) {
            $typeAttestation = $this->getTypeAttestationSociale($type);
            $params['type_attestation'] = $typeAttestation;
            $params['object'] = $refUtilisateurConsultation;
        }
        if (!empty($type) && $type == Atexo_Config::getParameter('TYPE_ATTESTATION_FISCALE')) {
            $params['object'] = $refUtilisateurConsultation;
        }

        return ['get' => $params];
    }
}
