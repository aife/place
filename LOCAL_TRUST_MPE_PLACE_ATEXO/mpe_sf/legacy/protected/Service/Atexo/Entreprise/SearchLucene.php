<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Prado\Prado;
use Prado\Util\TLogger;
use ZendSearch\Lucene\Analysis\Analyzer\Analyzer;
use ZendSearch\Lucene\Analysis\Analyzer\Common\Text\CaseInsensitive;
use ZendSearch\Lucene\Document;
use ZendSearch\Lucene\Document\Field;
use ZendSearch\Lucene\Exception as LuceneException;
use ZendSearch\Lucene\Index\Term;
use ZendSearch\Lucene\Lucene;
use ZendSearch\Lucene\Search\Query\MultiTerm;
use ZendSearch\Lucene\Search\QueryParser;

/**
 * Classe de Atexo_Consultation_SearchLucene.
 *
 * @author Thibaut DECAUDAIN <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise_SearchLucene
{
    /**
     * Chemin de l'index.
     */
    private ?string $_dirIndex = null;

    /**
     * Index de Lucene.
     */
    private ?\ZendSearch\Lucene\SearchIndexInterface $_index = null;

    /**
     * Mots ignoré dans la requete.
     */
    private ?array $_stopWords = null;

    /**
     * Constructeur : ouvre l'index Lucenne, ou le cré si il n'existe pas,
     * dans le répertoire DIR_INDEX_LUCENE.
     *
     * @throws : Atexo_Consultation_Exception : si réperoire inaccessible
     */
    public function __construct()
    {
        $configDirLucene = Atexo_Config::getParameter('DIR_INDEX_LUCENE_ENTREPRISE');
        if (isset($configDirLucene)) {
            $this->_dirIndex = $configDirLucene;
        } else {
            $this->_dirIndex = Atexo_Config::getParameter('COMMON_TMP').'indexLuceneEntreprise'; // Par défault ?
        }

        // Ouvre l'index
        try {
            $this->_index = Lucene::open($this->_dirIndex);
        } catch (\Exception $e) {
            try {
                // L'index n'existe pas, on le cré
                $this->_index = Lucene::create($this->_dirIndex);
            } catch (LuceneException $e) {
                throw new Atexo_Consultation_Exception('Unable to reach the index directory of Lucene. Please check acces rules of the directory.'.$e->getMessage());
            }
        }
        $this->init();
    }

    /**
     * Initialisation de l'index et des filtres de recherche.
     */
    private function init()
    {
        Analyzer::setDefault(new CaseInsensitive());

        // Liste de mots à exclure des recherches (meilleurs performances...)
        // TODO : ajouter les mots inutiles de toutes les langues...
        $this->_stopWords = ['le', 'la', 'les', 'de', 'des', 'du', 'au', 'aux', 'un', 'une', 'à', 'où', 'ou', 'et', 'l', 'd', ''];
    }

    /**
     * Optimisation de l'index.
     */
    public function optimizeIndex()
    {
        $this->_index->optimize();
    }

    /*
     * Retourne l'id (de l'index de Lucene) ou -1 si l'entreprise n'est pas dans l'index
     */
    private function getIdEntreprise($idEntreprise)
    {
        $query = new MultiTerm();
        $query->addTerm(new Term(utf8_encode($idEntreprise), 'idEntreprise'), true);
        $hits = $this->_index->find($query);

        if ((is_countable($hits) ? count($hits) : 0) > 0) {
            return $hits[0]->id;
        } else {
            return -1;
        }
    }

    /**
     * Ajoute une entreprise à l'index
     * A utiliser également pour modifier les information de l'index.
     *
     * @parameter : $commonConsultationO : CommonConsultation la consultation à enregistrer dans l'index
     * @parameter : $arrayComlots : tableau de CommonCategorieLot (les lots de la consultation)
     * @parameter : $avis : champs réserver à l'avis de consultation BOAMP
     *
     * @return : id de la consultation ajoutée
     */
    public function addEntreprise($Entreprise)
    {
        // Si la consultation est déjà dans l'index Lucene, on la suprime
        $idEntrepriseLucene = $this->getIdEntreprise($Entreprise->getId());
        if ($idEntrepriseLucene >= 0) {
            $this->deleteEntrepriseByIdLucene($idEntrepriseLucene);
        }

        // Ajout de l'entreprise a l'index
        $entreprise = new Document();
        $entreprise->addField(Field::Keyword('idEntreprise', utf8_encode($Entreprise->getId())));
        $entreprise->addField(Field::Text('nom', utf8_encode($this->formatString($Entreprise->getNom()))));
        $entreprise->addField(Field::Text('descriptionActivite', utf8_encode($this->formatString($Entreprise->getDescriptionActivite()))));
        $entreprise->addField(Field::Text('activiteDomaineDefense', utf8_encode($this->formatString($Entreprise->getActiviteDomaineDefense()))));
        $this->_index->addDocument($entreprise);
        Prado::log("Lucene : Ajout dans l'index de l'entreprise ".$Entreprise->getId().'(nom = '.$Entreprise->getNom().')', TLogger::INFO, 'Info');
    }

    /**
     * Suprime une entreprise.
     *
     * @param : $idLucene : id Lucene (de l'index) de l'entreprise
     */
    private function deleteEntrepriseByIdLucene($idLucene)
    {
        Prado::log("Lucene : Suppression de l'entreprise dans l'index  ".$idLucene, TLogger::INFO, 'Info');

        try {
            $this->_index->delete($idLucene);
        } catch (LuceneException $e) {
            Prado::log("Lucene : Echec de la supression d'une entreprise : id = ".$idLucene, TLogger::ERROR, 'Error');
            throw $e;
        }
    }

    /**
     * Suprime une l'entreprise de l'index.
     *
     * @param : $idCons : id de l'entreprise
     *
     * @throws : Atexo_Consultation_Exception
     */
    public function deleteEntreprise($idEntreprise)
    {
        try {
            $idEntreprise = $this->getIdEntreprise($idEntreprise);
            $this->deleteEntrepriseByIdLucene($idEntreprise);
        } catch (LuceneException $e) {
            Prado::log("Lucene : Echec de la supression d'une entreprise : id = ".$idEntreprise, TLogger::ERROR, 'Error');
            throw new Atexo_Consultation_Exception('Unable to delete the company from the Lucene index.'.$e->getMessage());
        }
    }

    /**
     * Effectue la recherche.
     *
     * @param : $requete    : chaine de caractères (mots à inclure dans la recherche)
     * @param : $précision : 0 < $précision < 1 (proche de 0 -> grande tolérance aux 'fautes')
     *
     * @return : tableau des ids des consultations trouvées
     */
    public function search($requete, $precision = 0.6)
    {
        $requete = $this->formatString($requete);
        $mots = $this->formatStringToArray($requete);
        $newRequete = $this->formatArrayToRequeteOu($mots, $precision);

        $query = QueryParser::parse($newRequete);
        $hits = $this->_index->find($query);

        // On récupère les consultations correspondants à la requète sous la forme
        // d'un tableau d'Id de consultations
        $listPrimaryKey = [];
        foreach ($hits as $hit) {
            $document = $hit->getDocument();
            $idEntreprise = utf8_decode($document->idEntreprise);
            $listPrimaryKey[] = $idEntreprise;
        }

        return array_unique($listPrimaryKey);
    }

    /**
     * Effectue la recherche.
     *
     * @param : $requete    : chaine de caractères (mots à inclure dans la recherche)
     * @param : $précision : 0 < $précision < 1 (proche de 0 -> grande tolérance aux 'fautes')
     *
     * @return : tableau des ids des consultations trouvées
     */
    public function searchEt($requete, $precision = 0.6)
    {
        $requete = $this->formatString($requete);
        $mots = $this->formatStringToArray($requete);
        $newRequete = $this->formatArrayToRequeteEt($mots, $precision);

        $query = QueryParser::parse($newRequete);
        $hits = $this->_index->find($query);

        // On récupère les consultations correspondants à la requète sous la forme
        // d'un tableau d'Id de consultations
        $listPrimaryKey = [];
        foreach ($hits as $hit) {
            $document = $hit->getDocument();
            $idEntreprise = utf8_decode($document->idEntreprise);
            $listPrimaryKey[] = $idEntreprise;
        }

        return array_unique($listPrimaryKey);
    }

    /**
     * Met une chaine en minuscule et enlève la ponctuation.
     */
    private function formatString($str)
    {
        $result = strtr($str, "àâäéèêëîïôöûùüç,.;:!?\"()[]{}'-", 'aaaeeeeiioouuuc                 ');
        $result = strtolower(rtrim($result));

        return $result;
    }

    /**
     * Transforme une chaine en tableau et supprime les mots non pertinents de la recherche
     * (stockés dans $_stopWords).
     */
    private function formatStringToArray($str)
    {
        $mots = explode(' ', $str);
        $mots = array_diff($mots, $this->_stopWords);
        $mots = array_unique($mots);

        return $mots;
    }

    public function formatArrayToRequeteEt($arrayMots, $precision)
    {
        $requete = '';
        foreach ($arrayMots as $mot) {
            $requete .= '('.$mot.'~'.$precision.' OR '.$mot.'*) AND ';
        }
        $requete = substr($requete, 0, -5);
        //echo $requete . "<br>";
        return $requete;
    }

    public function formatArrayToRequeteOu($arrayMots, $precision)
    {
        $requete = '';
        foreach ($arrayMots as $mot) {
            $requete .= '('.$mot.'~'.$precision.' OR '.$mot.') OR ';
        }
        $requete = substr($requete, 0, -4);
        //echo $requete . "<br>";
        return $requete;
    }

    /**
     * Fonctions de test.
     */
    public function printInfos($organisme, $idCons)
    {
        $idLucene = $this->getIdEntreprise($organisme);
        echo '<br>'.$idLucene;

        // Si la consultation n'est pas dans l'index
        if ($idLucene < 0) {
            return false;
        }

        // Si elle y est
        $consultation = $this->_index->getDocument($idLucene);
        echo '<br>';
        echo utf8_decode($consultation->fr);
        echo '<br>';
        echo utf8_decode($consultation->defaut);
    }

    public function printAll()
    {
        echo '<br><br>printall index : <br>';
        $query = new MultiTerm();

        $hits = $this->_index->find($query);
        foreach ($hits as $hit) {
            $document = $hit->getDocument();
            $idEntreprise = utf8_decode($document->idEntreprise);
            $nom = utf8_decode($document->nom);
            $reference = utf8_decode($document->descriptionActivite);
            $defaut = utf8_decode($document->activiteDomaineDefense);

            echo '<br>['.$hit->id.'] : '.$nom.' -> '.$idEntreprise.' / '.$reference.' / '.$defaut;
        }
    }
}
