<?php

namespace Application\Service\Atexo\Entreprise;

use App\Entity\Inscrit;
use App\Service\PradoPasswordEncoderInterface;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCertificatsEntreprisesPeer;
use Application\Propel\Mpe\CommonEntreprisePeer;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonInscritQuery;
use Application\Propel\Mpe\CommonSsoEntreprise;
use Application\Propel\Mpe\CommonSsoEntreprisePeer;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use PDO;
use Prado\Prado;

/**
 * Classe de manipulation des utilisateurs.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise_Inscrit
{
    /**
     * retourne un objet isncrit.
     *
     * @param $id identifiant de l'inscrit
     */
    public function retrieveInscritById($id)
    {
        if (0 == $id) {
            return false;
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::ID, $id);
        $inscrit = CommonInscritPeer::doSelectOne($c, $connexionCom);
        if ($inscrit) {
            return $inscrit;
        } else {
            return false;
        }
    }

    /**
     * retourne un objet isncrit.
     *
     * @param $login identifiant de l'inscrit
     */
    public function retrieveInscritByLogin($login)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::LOGIN, $login);
        $inscrit = CommonInscritPeer::doSelectOne($c, $connexionCom);
        if ($inscrit) {
            return $inscrit;
        } else {
            return false;
        }
    }

    /**
     * retourne un objet isncrit.
     *
     * @param $$mail l'adresse électronique de l'inscrit
     */
    public function retrieveInscritByMail($mail)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::EMAIL, $mail);
        $inscrit = CommonInscritPeer::doSelectOne($c, $connexionCom);
        if ($inscrit) {
            return $inscrit;
        } else {
            return false;
        }
    }

    public function retrieveInscritByMailWithJoins($email)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $criteria = new Criteria();
        $criteria->add(CommonInscritPeer::EMAIL, $email);
        $criteria->addJoin(CommonInscritPeer::ENTREPRISE_ID, CommonEntreprisePeer::ID);
        $criteria->addJoin(CommonInscritPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT);

        $inscrit = CommonInscritPeer::doSelectOne($criteria, $connexion);

        return $inscrit;
    }

    /**
     * retourne un objet isncrit.
     *
     * @param $$mail l'adresse électronique de l'inscrit
     */
    public function retrieveInscritByMailOrSiret($mail, $siret)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::EMAIL, $mail);
        $c->addOr(CommonInscritPeer::SIRET, $siret);
        $inscrit = CommonInscritPeer::doSelect($c, $connexionCom);
        if ($inscrit) {
            return $inscrit;
        } else {
            return false;
        }
    }

    /**
     * retourne l'insrcit existant avec $certif et $serial.
     *
     * @param unknown_type $certif
     * @param unknown_type $serial
     *
     * @return Inscrit
     */
    public function retrieveCertAuthUE($certif, $serial)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::NUM_CERT, $serial);
        $result = CommonInscritPeer::doSelect($c, $connexionCom);
        if ($result) {
            $searchCert = base64_decode($certif);
            foreach ($result as $oneInscrit) {
                $userCert = base64_decode($oneInscrit->getCert());
                $cmp = strcmp($searchCert, $userCert);
                if (0 == $cmp) {
                    return $oneInscrit;
                }
            }
        }

        return false;
    }

    /**
     * retourne un inscrit identifié par login et mot de passe.
     */
    public function retrieveInscritByLoginMdp($login, $password)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));

        $c = new Criteria();
        $c->add(CommonInscritPeer::LOGIN, $login);
        $c->add(CommonInscritPeer::MDP, $password);

        $inscrit = CommonInscritPeer::doSelectOne($c, $connexionCom);

        return $inscrit ?: false;
    }

    /**
     * retourne un inscrit identifié par $login autre que l'inscrit identifié par le $idInscrit.
     */
    public function retrieveInscritByLoginForModValidation($login, $idInscrit)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        // Requête
        $c = new Criteria();
        $c->add(CommonInscritPeer::LOGIN, $login);
        $c->add(CommonInscritPeer::ID, $idInscrit, Criteria::NOT_EQUAL);
        $inscrit = CommonInscritPeer::doSelect($c, $connexionCom);
        if ($inscrit) {
            return $inscrit;
        } else {
            return false;
        }
    }

    /**
     * retourne un inscrit identifié par $login autre que l'inscrit identifié par le $idInscrit.
     */
    public function retrieveInscritByMailForModValidation($mail, $idInscrit)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::EMAIL, $mail);
        $c->add(CommonInscritPeer::ID, $idInscrit, Criteria::NOT_EQUAL);
        $inscrit = CommonInscritPeer::doSelect($c, $connexionCom);
        if ($inscrit) {
            return $inscrit;
        } else {
            return false;
        }
    }

    /**
     * retourne true si l'inscrit a un compte avec certificat.
     */
    public function inscritHasCertificate($idInscrit)
    {
        $inscrit = self::retrieveInscritById($idInscrit);
        if ($inscrit) {
            if ('' != $inscrit->getCert()) {
                return true;
            }

            return false;
        }
    }

    public function retrieveInscritByCertForModValidation($serial, $idInscrit)
    {
        if (0 == $idInscrit) {
            return false;
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::NUM_CERT, $serial);
        $c->add(CommonInscritPeer::ID, $idInscrit, Criteria::NOT_EQUAL);
        $inscrit = CommonInscritPeer::doSelect($c, $connexionCom);
        if ($inscrit) {
            return $inscrit;
        } else {
            return false;
        }
    }

    /**
     * retourne les utilisateurs d'une entrpeirse autre que l'utilisateur authentifié.
     *
     * @param  $entreprise
     *
     * @return array d'utilisateur
     * @throws PropelException
     */
    public function retrieveInscritSansCurruenUser($idEntreprise)
    {
        if (0 == $idEntreprise) {
            return [];
        }

        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB')
            . Atexo_Config::getParameter('CONST_READ_ONLY')
        );
        $c = new Criteria();
        $c->add(CommonInscritPeer::ENTREPRISE_ID, $idEntreprise);
        $c->add(CommonInscritPeer::DELETED, null);

        $utilisateurs = CommonInscritPeer::doSelect($c, $connexionCom);
        if ($utilisateurs) {
            return $utilisateurs;
        } else {
            return [];
        }
    }

    public function delateInscrit($idInscrit, $connexionCom)
    {
        CommonInscritPeer::doDelete($idInscrit, $connexionCom);
    }

    public function retrieveInscritBySelectedMail($mail, $idInscrit)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::EMAIL, $mail);
        $c->add(CommonInscritPeer::ID, $idInscrit, Criteria::NOT_EQUAL);
        $inscrit = CommonInscritPeer::doSelect($c, $connexionCom);
        if ($inscrit) {
            return $inscrit;
        } else {
            return false;
        }
    }

    /**
     * retourne un inscrit identifié par $login autre que l'inscrit identifié par le $idInscrit.
     */
    public function retrieveInscritByIdCompnay($idCompany)
    {
        if (0 == $idCompany) {
            return false;
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        // Requête
        $c = new Criteria();
        $c->add(CommonInscritPeer::ENTREPRISE_ID, $idCompany);
        $inscrits = CommonInscritPeer::doSelect($c, $connexionCom);

        if ($inscrits) {
            return $inscrits;
        }

        return false;
    }

    public function retrieveInscritAndEntreprise($idInscrit)
    {
        if (0 == $idInscrit) {
            return false;
        }

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $criteria = new Criteria();
        $criteria->add(CommonInscritPeer::ID, $idInscrit);
        $inscrit = CommonInscritPeer::doSelectJoinEntreprise($criteria, $connexion);

        return $inscrit;
    }

    public function isAdmin($idInscrit)
    {
        if (0 == $idInscrit) {
            return false;
        }

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $criteria = new Criteria();
        $criteria->add(CommonInscritPeer::ID, $idInscrit);
        $inscrit = CommonInscritPeer::doSelectOne($criteria, $connexion);
        if ($inscrit instanceof CommonInscrit) {
            if ('1' == $inscrit->getProfil() || '2' == $inscrit->getProfil()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Mettre les infos récupérer depuis le web service dans les champs de la table inscrit du projet associé.
     *
     * @param array $inscrit, array $entreprise, array $wsInscrit
     */
    public static function remplirInscrit($inscrit, $entreprise, $wsInscrit)
    {
        $inscrit->setIdInitial($wsInscrit['id']);
        $inscrit->setEntrepriseId($entreprise->getId());
        if ($wsInscrit['login'] && 'NULL' != strtoupper($wsInscrit['login'])) {
            $inscrit->setLogin($wsInscrit['login']);
        }
        $inscrit->setMdp($wsInscrit['mdp']);
        $inscrit->setNumCert($wsInscrit['num_cert']);
        $inscrit->setCert(utf8_decode($wsInscrit['cert']));
        $inscrit->setCivilite($wsInscrit['civilite']);
        $inscrit->setNom(utf8_decode($wsInscrit['nom']));
        $inscrit->setPrenom(utf8_decode($wsInscrit['prenom']));
        $inscrit->setAdresse(utf8_decode($wsInscrit['adresse']));
        $inscrit->setCodepostal($wsInscrit['codepostal']);
        $inscrit->setVille(utf8_decode($wsInscrit['ville']));
        $inscrit->setPays(utf8_decode($wsInscrit['pays']));
        $inscrit->setEmail(trim($wsInscrit['email']));
        $inscrit->setTelephone($wsInscrit['telephone']);
        $inscrit->setCategorie($wsInscrit['categorie']);
        $inscrit->setMotstitreresume($wsInscrit['motstitreresume']);
        $inscrit->setPeriode($wsInscrit['periode']);
        $inscrit->setSiret($wsInscrit['siret']);
        $inscrit->setFax($wsInscrit['num_fax']);
        $inscrit->setProfil($wsInscrit['profil']);
        $inscrit->setBloque($wsInscrit['bloque']);
    }

    public function retrieveInscritByIdExterne($idExterne)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::ID_EXTERNE, $idExterne);
        $inscrit = CommonInscritPeer::doSelectOne($c, $connexionCom);
        if ($inscrit) {
            return $inscrit;
        } else {
            return false;
        }
    }

    public function isInscritAnnuaireDefense($idInscrit)
    {
        $inscrit = self::retrieveInscritById($idInscrit);
        if ($inscrit) {
            return $inscrit->getInscritAnnuaireDefense();
        }

        return false;
    }

    /**
     * retourne les inscrits de la plate-forme.
     */
    public function retreiveInscrits($fromDate = null, $defensePortalRegistered = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();

        if ($defensePortalRegistered) {
            $c->addJoin(CommonInscritPeer::ID_ETABLISSEMENT, CommonTEtablissementPeer::ID_ETABLISSEMENT);
            $c->add(CommonTEtablissementPeer::INSCRIT_ANNUAIRE_DEFENSE, '1');
        }

        if ($fromDate) {
            $fromDateIso = Atexo_Util::iso860TimeToIso($fromDate);
            $c->add(CommonInscritPeer::DATE_MODIFICATION, $fromDateIso, Criteria::GREATER_EQUAL);
        }
        if (Atexo_Config::getParameter('TEST_SOCLE_PPP')) { // Juste pour les testes Unitaires
            $c->setLimit(100);
        }
        $result = CommonInscritPeer::doSelect($c, $connexionCom);
        //print_r($result);exit;
        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    /**
     * retourne les inscrits de l'annuaire défense.
     */
    public function retreiveInscritAnnuaireDefense($fromDate = null)
    {
        return self::retreiveInscrits($fromDate, true);
    }

    public function retrieveSsoEntrepriseBySso($sso)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonSsoEntreprisePeer::ID_SSO, $sso, Criteria::EQUAL);
        $c->add(CommonSsoEntreprisePeer::DATE_LAST_REQUEST, date('Y-m-d H:i:s'), Criteria::GREATER_THAN);
        $ssoEntreprise = CommonSsoEntreprisePeer::doSelectOne($c, $connexionCom);

        return $ssoEntreprise;
    }

    // reccupère un sso valide sinon il est généré
    public function getSsoValideInscrit($idInscrit, $login = '')
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonSsoEntreprisePeer::ID_INSCRIT, $idInscrit, Criteria::EQUAL);
        if ($login) {
            $c->add(CommonSsoEntreprisePeer::LOGIN, $login, Criteria::EQUAL);
        }
        $c->add(CommonSsoEntreprisePeer::DATE_LAST_REQUEST, date('Y-m-d H:i:s'), Criteria::GREATER_THAN);
        $ssoEntreprise = CommonSsoEntreprisePeer::doSelectOne($c, $connexionCom);
        if (!($ssoEntreprise instanceof CommonSsoEntreprise)) {
            $ssoEntreprise = self::generateSsoForInscrit($idInscrit, $login);
        }

        return $ssoEntreprise->getIdSso();
    }

    /***
        * générer un sso pour l'inscrit connecté
        * @param idInscrit $idInscrit
        * @return  l'objet CommonSsoEntreprise
        */
    public function generateSsoForInscrit($idInscrit, $login = '')
    {
        if ($idInscrit) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            $ssoInscrit = new CommonSsoEntreprise();
            $sso = uniqid();
            $ssoInscrit->setIdSso($sso);
            $ssoInscrit->setIdInscrit($idInscrit);
            if ($login) {
                $ssoInscrit->setLogin($login);
            }
            $ssoInscrit->setDateConnexion(date('Y-m-d H:i:s'));
            $ssoInscrit->setDateLastRequest(Atexo_Util::dateDansFutur(date('Y-m-d H:i:s'), Atexo_Config::getParameter('NBR_HEURE_MAINTENANCE_CNX')));
            $ssoInscrit->save($connexionCom);

            return $ssoInscrit;
        }
    }

    public function retrieveAllAdmins($idEntreprise)
    {
        if (0 == $idEntreprise) {
            return false;
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::ENTREPRISE_ID, $idEntreprise);
        $c->add(CommonInscritPeer::PROFIL, Atexo_Config::getParameter('PROFIL_INSCRIT_ATES'));
        $inscrits = CommonInscritPeer::doSelect($c, $connexionCom);

        return $inscrits;
    }

    /**
     * retourne inscrits avec une jointure vers la table Certificats_Entreprises.
     */
    public static function retrieveInscritCertificatByEntrepriseId($idEntreprise, $critere = null, $order = null)
    {
        $querySelect = 'SELECT Inscrit.id as id_inscrit,Inscrit.nom,Inscrit.prenom,Inscrit.email,Inscrit.entreprise_id,Certificats_Entreprises.id ' .
                        'as id_certificat,Certificats_Entreprises.statut_revoque ' .
                       ', Certificats_Entreprises.date_fin,Certificats_Entreprises.date_debut,Certificats_Entreprises.date_revoquation ' .
                       ' FROM Inscrit LEFT JOIN Certificats_Entreprises ON' .
                       " Certificats_Entreprises.`id_inscrit` = Inscrit.Id WHERE Inscrit.entreprise_id = '" . (new Atexo_Db())->quote($idEntreprise) . "'";
        if ($critere && $order) {
            $querySelect .= ' ORDER BY ' . (new Atexo_Db())->quote($critere) . ' ' . (new Atexo_Db())->quote($order) . ' ';
        } else {
            $querySelect .= ' ORDER BY nom ASC ';
        }
        $results = Atexo_Db::getLinkCommon(true)->query($querySelect);
        $arrayResult = [];
        if ($results) {
            $index = 0;
            foreach ($results as $row) {
                $arrayResult[$index]['id_inscrit'] = $row['id_inscrit'];
                $arrayResult[$index]['nom'] = $row['nom'];
                $arrayResult[$index]['prenom'] = $row['prenom'];
                $arrayResult[$index]['email'] = $row['email'];
                $arrayResult[$index]['action_creer'] = true;
                $arrayResult[$index]['action_revoquer'] = true;
                if ('' == $row['id_certificat']) {
                    $arrayResult[$index]['statut_certificat'] = Prado::localize('NON_ATTRIBUE');
                    $arrayResult[$index]['action_creer'] = true;
                    $arrayResult[$index]['action_revoquer'] = false;
                } elseif ($row['statut_revoque'] == Atexo_Config::getParameter('STATUT_CERTIFICAT_REVOQUEE')) {
                    $arrayResult[$index]['action_creer'] = true;
                    $arrayResult[$index]['action_revoquer'] = false;
                    $arrayResult[$index]['statut_certificat'] = Prado::localize('REVOQUE_LE') . ' ' . Atexo_Util::iso2frnDateTime($row['date_revoquation']);
                } elseif ($row['date_fin'] < date('Y-m-d H:i:s')) {
                    $arrayResult[$index]['action_creer'] = true;
                    $arrayResult[$index]['action_revoquer'] = false;
                    $arrayResult[$index]['statut_certificat'] = Prado::localize('EXPIRE_LE') . ' ' . Atexo_Util::iso2frnDateTime($row['date_fin']);
                } elseif ($row['date_fin'] > date('Y-m-d H:i:s')) {
                    $arrayResult[$index]['action_creer'] = false;
                    $arrayResult[$index]['action_revoquer'] = true;
                    $arrayResult[$index]['statut_certificat'] = Prado::localize('VALABLE_JUSQU_AU') . ' ' . Atexo_Util::iso2frnDateTime($row['date_fin']);
                }
                $arrayResult[$index]['id_certificat'] = $row['id_certificat'];
                $arrayResult[$index]['statut'] = $row['statut_revoque'];
                $arrayResult[$index]['date_fin'] = $row['date_fin'];
                $arrayResult[$index]['date_debut'] = $row['date_debut'];
                $arrayResult[$index]['entreprise_id'] = $row['entreprise_id'];
                ++$index;
            }
        }

        return $arrayResult;
    }

    /**
     * retourne le statut du certificat et date.
     */
    public static function retrieveStatutCertificatInscrit($idInscrit)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCertificatsEntreprisesPeer::ID_INSCRIT, $idInscrit);
        $result = CommonCertificatsEntreprisesPeer::doSelectOne($c, $connexionCom);
        if ($result) {
            if ($result->getStatutRevoque() == Atexo_Config::getParameter('STATUT_CERTIFICAT_REVOQUEE')) {
                return Prado::localize('REVOQUE_LE') . ' ' . Atexo_Util::iso2frnDateTime($result->getDateRevoquation());
            } elseif ($result->getDateFin() < date('Y-m-d H:i:s')) {
                return Prado::localize('EXPIRE_LE') . ' ' . Atexo_Util::iso2frnDateTime($result->getDateFin());
            } elseif ($result->getDateFin() > date('Y-m-d H:i:s')) {
                return Prado::localize('VALABLE_JUSQU_AU') . ' ' . Atexo_Util::iso2frnDateTime($result->getDateFin());
            }
        }

        return Prado::localize('NON_ATTRIBUE');
    }

    public function retrieveCertificatInscritById($idInscrit)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonCertificatsEntreprisesPeer::ID_INSCRIT, $idInscrit);
        $certifObject = CommonCertificatsEntreprisesPeer::doSelectOne($c, $connexionCom);
        if ($certifObject) {
            return $certifObject;
        } else {
            return false;
        }
    }

    /**
     * retourne un objet isncrit.
     *
     * @param $uid : l'identifiant unique crée lors de la creation de l'inscrit
     */
    public function retrieveInscritByUid($uid)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::UID, '%' . $uid, Criteria::LIKE);
        $inscrit = CommonInscritPeer::doSelectOne($c, $connexionCom);
        if ($inscrit) {
            return $inscrit;
        } else {
            return false;
        }
    }

    public function getListMailInscritByEntreprise($IdEntreprise)
    {
        $inscrits = self::retrieveInscritByIdCompnay($IdEntreprise);
        $listMail = [];
        if ($inscrits) {
            foreach ($inscrits as $inscrit) {
                $listMail[] = $inscrit->getEmail();
            }
        }

        return $listMail;
    }

    public function getNomPrenomEmailInscrit($idInscrit)
    {
        $inscrit = self::retrieveInscritById($idInscrit);
        if ($inscrit instanceof CommonInscrit) {
            return $inscrit->getNom() . ' ' . $inscrit->getPrenom() . ' (' . $inscrit->getEmail() . ') ';
        }
    }

    /*
     * notifier l'inscrit de son inscription et envoi du pwd
     *
     */
    public function notifierInscrit($inscrit, $pwd)
    {
        $from = Atexo_Config::getParameter('PF_MAIL_FROM');
        $to = $inscrit->getEmail();
        $subject = Prado::localize('DEFINE_TEXT_OBJET_CREATION_MODIFICATION_MON_COMPTE');
        $message = Atexo_Message::getMessageModificationCompte($inscrit, $pwd);
        Atexo_Message::simpleMail($from, $to, $subject, $message, '', '', false, true);
    }

    /*
     * retourne un tableau des objet CommonInscrit
     *
     * @param string $siret ,siret avec laquelle on fait la recherche sur les inscrits
     * @return PropelObjectCollection, une collection des objet CommonInscrit
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     * @package Atexo_Entreprise_Inscrit
     */
    public function getInscritBySiret($siret)
    {
        $inscritQuery = new CommonInscritQuery(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

        return $inscritQuery->findBySiret($siret);
    }

    /**
     * Permet de recuperer le profil d'un utilisateur inscrit.
     *
     * @param CommonInscrit $idInscrit: objet inscrit
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getProfilByIdInscrit($idInscrit)
    {
        $inscrit = self::retrieveInscritById($idInscrit);
        if ($inscrit instanceof CommonInscrit) {
            return $inscrit->getProfil();
        }
    }

    /**
     * Permet de faire la correspondance entre l'identifiant du profil et le libellé du profil.
     *
     * @param string $idProfil: identifiant du profil
     *
     * @return string: libellé du profil
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function correspondanceIdProfilLibelle($idProfil)
    {
        $libelleProfil = '';
        $libelleProfil = match ($idProfil) {
            Atexo_Config::getParameter('PROFIL_INSCRIT_ATES') => Prado::localize('TEXT_ADMINISTRATEUR'),
            Atexo_Config::getParameter('PROFIL_INSCRIT_USER') => strtolower(Prado::localize('DEFINE_UES_SIMPLE')),
            default => $libelleProfil,
        };

        return $libelleProfil;
    }

    /**
     * Permet de recuperer les emails des administrateurs.
     *
     * @param string $idCompany: identifiant de l'entreprise
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function retrieveListeEmailsAdminByIdCompany($idCompany)
    {
        if (0 == $idCompany) {
            return false;
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInscritPeer::ENTREPRISE_ID, $idCompany);
        $c->add(CommonInscritPeer::PROFIL, Atexo_Config::getParameter('PROFIL_INSCRIT_ATES'));
        $inscrits = CommonInscritPeer::doSelect($c, $connexionCom);
        $emails = '';
        if (is_array($inscrits) && is_array($inscrits)) {
            foreach ($inscrits as $inscrit) {
                if ($inscrit instanceof CommonInscrit) {
                    $emails .= $inscrit->getEmail() . ',';
                }
            }
        }
        if ($emails) {
            return $emails;
        }

        return false;
    }

    /**
     * Permet de mettre a jour l'information de l'etablissement d'apartenance de l'inscrit.
     *
     * @param CommonInscrit $inscrit:          objet Inscrit
     * @param string        $codeEtablissement : code Etablissement
     * @param connexion     $connexion:        connexion de l'enregistrement
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setIdEtablissementForInscrit(&$inscrit, $codeEtablissement, $idEntreprise, $connexion = null)
    {
        if ($connexion = null) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $etablissementInscrit = CommonTEtablissementQuery::create()
                                    ->filterByCodeEtablissement($codeEtablissement)
                                    ->filterByIdEntreprise($idEntreprise)
                                    ->findOne($connexion);
        if ($etablissementInscrit) {
            $inscrit->setIdEtablissement($etablissementInscrit->getIdEtablissement());
        }
    }

    /**
     * Permet de verifier que l'utilisateur connecte est le seul administrateur de l'entreprise.
     *
     * @return bool : true si l'utilisateur connecte est le seul admin, false sinon
     *
     * @author OUMAR KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function isInscritDernierAdmin()
    {
        $inscritsAdmin = (new Atexo_Entreprise_Inscrit())->retrieveAllAdmins(Atexo_CurrentUser::getIdEntreprise());
        if (
            is_array($inscritsAdmin)
            && 1 == count($inscritsAdmin)
                && $inscritsAdmin[0] instanceof CommonInscrit
                    && $inscritsAdmin[0]->getId() == Atexo_CurrentUser::getIdInscrit()
        ) {
            return true;
        }

        return false;
    }

    /**
     * Permet de recuperer le login de l'inscrit.
     *
     * @param string $id : identifiant technique de l'inscrit
     *
     * @return string : login
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function retrieveInscritLogin($id)
    {
        $queryParams = [];
        $query = <<<EOF
        SELECT
            login
        FROM
            Inscrit
        WHERE
            id = :id
EOF;
        $queryParams[':id'] = (new Atexo_Db())->quote($id);
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($queryParams);
        $login = '';
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $login = $row['login'];
        }

        return $login;
    }

    /**
     * Permet de retourner l'adreese mail d'un inscrit.
     *
     * @param int $idInscrit l'id de l'inscrit
     *
     * @return string le mail de l'inscrit
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public function getMailInscrit($idInscrit)
    {
        $inscrit = self::retrieveInscritById($idInscrit);

        return ($inscrit instanceof CommonInscrit) ? $inscrit->getEmail() : false;
    }

    /**
     * Permet de mettre à jour l'inscrit avec psw en sha256 si le parametre ENABLE_SHA256_PASSWORDS est activé
     * et l'agent avais sha1.
     *
     * @param $inscrit
     * @param $passowrd
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public static function updateMdpBySHA256(&$inscrit, $passowrd, $saveInscrit = true)
    {
        if (Atexo_Config::getParameter('ENABLE_SHA256_PASSWORDS') && ($inscrit instanceof CommonInscrit) && 'SHA1' == $inscrit->getTypeHash()) {
            $inscrit->setTypeHash('SHA256');
            $inscrit->setMdp(hash('sha256', Atexo_Config::getParameter('SALT_POUR_MOT_DE_PASSE') . $passowrd));
            if ($saveInscrit) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
                $inscrit->save($connexion);
            }
        }
    }

    /**
     * Permet de mettre à jour l'inscrit avec psw en sha256 si le parametre ENABLE_SHA256_PASSWORDS est activé.
     *
     * @param $inscrit
     * @param $passowrd
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public static function updateNewInscritMdpBySHA256(&$inscrit, $passowrd)
    {
        if (Atexo_Config::getParameter('ENABLE_SHA256_PASSWORDS')) {
            $inscrit->setTypeHash('SHA256');
            $inscrit->setMdp(hash('sha256', Atexo_Config::getParameter('SALT_POUR_MOT_DE_PASSE') . $passowrd));
        } else {
            $inscrit->setTypeHash('SHA1');
            $inscrit->setMdp(sha1($passowrd));
        }
    }

    /**
     * Permet de retourner le mot de passe d'un agent sous sha1 ou sha256.
     *
     * @param $inscrit
     * @param $login
     * @param $passowrd
     *
     * @return string pwd hasher
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function getPswInscritAfterVerification(&$inscrit, $login, $passowrd)
    {
        $pwd = sha1($passowrd);
        if (Atexo_Config::getParameter('ENABLE_SHA256_PASSWORDS')) {
            $inscritPhp = new Atexo_Entreprise_Inscrit();
            $inscrit = $inscritPhp->retrieveInscritByLogin($login);
            if (($inscrit instanceof CommonInscrit) && 'SHA256' == $inscrit->getTypeHash()) {
                $pwd = hash('sha256', Atexo_Config::getParameter('SALT_POUR_MOT_DE_PASSE') . $passowrd);
            }
        }

        return $pwd;
    }

    public static function updateNewInscritPasswordByArgon2(CommonInscrit &$inscrit, string $passowrd): void
    {
        $passowrd = htmlspecialchars_decode($passowrd);
        $inscrit->setTypeHash(Inscrit::ARGON_TYPE);
        $encodedPassword = Atexo_Util::getSfService(PradoPasswordEncoderInterface::class)
            ->encodePassword(Inscrit::class, $passowrd);
        $inscrit->setMdp($encodedPassword);
    }

    public function loginUser(string $login, string $password): ?CommonInscrit
    {
        $user = $this->retrieveInscritByLogin($login);
        if (!($user instanceof CommonInscrit)) {
            return null;
        }

        if ($user->getTypeHash() == 'SHA1') {
            return null;
        }

        if ($user->getTypeHash() == 'SHA256') {
            $encodedPassword = hash('sha256', Atexo_Config::getParameter('SALT_POUR_MOT_DE_PASSE') . $password);
            $validUser = $this->retrieveInscritByLoginMdp($login, $encodedPassword);
            if (!($validUser instanceof CommonInscrit)) {
                $this->incrementTentativesMdp($user);
                return null;
            }

            return $validUser;
        }

        if ($user->getTypeHash() == 'ARGON2') {
            $isValidPassword = Atexo_Util::getSfService(PradoPasswordEncoderInterface::class)
                ->isPasswordValid(Inscrit::class, $user->getMdp(), $password);
            if (false === $isValidPassword) {
                $this->incrementTentativesMdp($user);
                return null;
            }

            return $user;
        }

        return null;
    }

    public static function updatePswInscritByARGON2(CommonInscrit &$inscrit, string $password): void
    {
        if ('SHA256' === $inscrit->getTypeHash()) {
            $inscrit->setTypeHash('ARGON2');
            $pass = Atexo_Util::getSfService(PradoPasswordEncoderInterface::class)
                ->encodePassword(Inscrit::class, $password);
            $inscrit->setMdp($pass);
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            $inscrit->save($connexion);
        }
    }

    private function incrementTentativesMdp(CommonInscrit $inscrit): void
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $tentative = $inscrit->getTentativesMdp();
        $inscrit->setTentativesMdp(++$tentative);
        $inscrit->save($connexionCom);
    }
}
