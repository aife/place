<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntrepriseQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use Exception;

/**
 * Class Atexo_Entreprise_Etablissement.
 */
class Atexo_Entreprise_Etablissement
{
    /**
     * @param $idEntreprise
     *
     * @return array
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retreiveEtablissementsByIdEntreprise($idEntreprise)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $etablissements = (array) CommonTEtablissementQuery::create()->filterByIdEntreprise($idEntreprise)->find($connexion);

        if (count($etablissements)) {
            return $etablissements;
        }

        return [];
    }

    public function getListeEtablissementFromSgmap()
    {
        //appel ws Ese
        //$result1 = array of objEtab
        //appel ws Etab
        //$result2 = array of objEtab
        //return array_merge($result1,$result2)
    }

    /**
     * Permet de recuperer les informations de l'adresse etablissement
     * la régle respecter est la suivante :
     *   La ligne où cinq chiffres sont détectés au début est enregistrée comme :
     *     Les cinq chiffres : code postal
     *     La suite : ville
     *     Les autres lignes sont enregistrées dans les champs Adresse.
     *
     * @param array $adresseInfo info adresse
     *
     * @return array un tableau contenant les informations d'adresse sous format cp,ville,adresse
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public static function getInfoAdresseEtablissement($adresseInfo)
    {
        $infoAdresse = [];

        if (is_array($adresseInfo)) {
            $infoAdresse['cp'] = (new Atexo_Config())->toPfEncoding($adresseInfo['code_postal']);
            $infoAdresse['ville'] = (new Atexo_Config())->toPfEncoding($adresseInfo['libelle_commune']);
            $infoAdresse['adresse2'] = (new Atexo_Config())->toPfEncoding($adresseInfo['complement_adresse']);
            $infoAdresse['adresse'] = (new Atexo_Config())->toPfEncoding($adresseInfo['numero_voie']).' '.(new Atexo_Config())->toPfEncoding($adresseInfo['type_voie']).' '.(new Atexo_Config())->toPfEncoding($adresseInfo['libelle_voie']);
        }

        return $infoAdresse;
    }

    /**
     * Permet de synchroniser un etablissement identifie par son siret avec Api Gouv Entreprise.
     *
     * @param string               $siret         : siret de l'etablissement sur 14 caracteres
     * @param CommonTEtablissement $etablissement : objet etablissement
     *
     * @return Atexo_Entreprise_EtablissementVo
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function synchroEtablissementAvecApiGouvEntreprise(
        $siret,
        $etablissement,
        $connexion = null,
        $idEchangesInterfaces = null
    ) {
        $params = [];

        try {
            $params['debutExecution'] = new DateTime('now');
            $params['service'] = 'API_GOUV_ENTREPRISE';
            $params['nomBatch'] = 'synchroEtablissementAvecSGMAP';
            $entree = serialize(['siret' => $siret, 'etablissement' => $etablissement]);
            $params['variablesEntree'] = $entree;
            $params['informationMetier'] = $entree;
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            Atexo_Oversight_Oversight::oversight($params);
            if (!is_null($idEchangesInterfaces)) {
                $params['idEchangesInterfaces'] = $idEchangesInterfaces;
            }
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        $loggerWs = Atexo_LoggerManager::getLogger();
        $loggerWs->info("Debut de la synchronisation de l'etablissement dont le siret = $siret avec ApiGouvEntreprise");

        try {
            if (!$connexion) {
                $connexion = Propel::getConnection(
                    Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
                );
            }

            $idEtablissement = null;

            if ($etablissement instanceof CommonTEtablissement && !empty($etablissement->getIdEtablissement())) {
                $idEtablissement = $etablissement->getIdEtablissement();
                $loggerWs->info("L'etablissement dont le siret = $siret existe en base de donnees");
            } else {
                $etablissement = new CommonTEtablissement();
                $loggerWs->info("L'etablissement dont le siret = $siret n'existe pas en base de donnees");
            }

            //Recuperation du siege de l'entreprise associe
            $loggerWs->info("Recuperation du siege de l'entreprise associe a l'etablissement dont le siret = $siret");
            $entreprise = EntrepriseQuery::create()->findOneBySiren(substr($siret, 0, 9));
            $siege = null;

            if ($entreprise instanceof Entreprise) {
                $siege = $entreprise->getEtablissementSiege();

                if ($siege instanceof CommonTEtablissement) {
                    $loggerWs->info('Etablissement siege trouve');
                }

                //Synchronisation de l'etablissement identifie par son siret
                $etablissementVoSgmap = self::recupererEtablissementApiGouvEntreprise($siret, $connexion, $idEtablissement);

                if ($etablissementVoSgmap instanceof Atexo_Entreprise_EtablissementVo) {
                    $loggerWs->info("Conversion de l'objet Atexo_Entreprise_EtablissementVo to CommonTEtablissement");
                    $etablissement = $etablissementVoSgmap->getCommonTEtablissement($etablissement);
                    $etablissement->setDateModification(date('Y-m-d H:i:s'));
                    $etablissement->setSaisieManuelle('0');
                    $etablissement->setIdEntreprise($entreprise->getId());

                    if ($etablissement->save($connexion)) {
                        $loggerWs->info("L'etablissement synchronise avec Api Gouv Entreprise est mis à jour avec SUCCES sur MPE");
                    }

                    if ($etablissement->getEstSiege() &&
                        $siege instanceof CommonTEtablissement &&
                        $siege->getCodeEtablissement() != $etablissement->getCodeEtablissement()
                    ) {
                        $siege->setEstSiege('0');
                        $siege->save($connexion);
                    }

                    $loggerWs->info("Conversion de l'etablissement en Atexo_Entreprise_EtablissementVo");
                    $loggerWs->info("Fin de la synchronisation de l'etablissement dont le siret = $siret avec ApiGouvEntreprise");
                    $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
                    try {
                        Atexo_Oversight_Oversight::oversight($params);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger('supervision');
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    return $etablissement->getEtablissementVo();
                } elseif (false === $etablissementVoSgmap) {
                    $loggerWs->info("l'etablissement n'est pas reconnu dont le siret = $siret");

                    return 'Objet non synchronise';
                } elseif ('Erreur' == $etablissementVoSgmap) {
                    return 'Erreur';
                } else {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorRemoteServiceReturnsAnError;
                    $loggerWs->error("Une erreur est survenue lors de la synchronisation de l'etablissement avec Api Gouv Entreprise : siret = $siret");
                }
            } else {
                $params['code'] = Atexo_Oversight_Oversight::$codeWarning;
                $loggerWs->error("L'entreprise rattache a l'etablissement ayant le siret = $siret n'est pas enregistre en base de donnees");
            }
        } catch (\Exception $e) {
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorRemoteServiceReturnsAnError;
            $loggerWs->error("Erreur lors de la synchronisation avec Api Gouv Entreprise  : siret = $siret \n\nErreur: ".$e->getMessage()."\n\nTrace: ".$e->getTraceAsString());
        }

        $loggerWs->info("Fin de la synchronisation de l'etablissement dont le siret = $siret avec ApiGouvEntreprise");
        try {
            Atexo_Oversight_Oversight::oversight($params);
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }
    }

    /**
     * Permet de recuperer un etablissement selon le siret.
     *
     * @param int $siret le siret d'etablissement
     *
     * @return mix Atexo_Entreprise_EtablissementVo l'etablissement s'il existe sinon boolean false
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function recupererEtablissementApiGouvEntreprise($siret, $connexion = null, $idObjet = null)
    {
        $token = Atexo_Config::getParameter('API_ENTREPRISE_TOKEN');

        $paramsWs = [
            'token' => $token,
            'apiEtablissementUrl' => Atexo_Config::getParameter('URL_API_ENTREPRISE_ETABLISSEMENT'),
        ];

        $loggerWs = Atexo_LoggerManager::getLogger();
        $interfaceWS = new InterfaceSynchroApiGouvEntreprise($loggerWs, $paramsWs);
        $resultat = $interfaceWS->getEtablissementBySiret($siret);

        if (is_array($resultat) && InterfaceSynchroApiGouvEntreprise::isResponseWsApiGouvEntrepriseSucces($resultat) && $resultat[1]) {
            $jeton = $resultat[2];
            $loggerWs->info("Debut de l'enregistrement de l'historique de la synchronisation de l'etablissement");
            (new Atexo_Entreprise_HistoriqueSynchroSGMAP())->enregistrerHistroique('ETABLISSEMENT', $siret, $jeton, $connexion, $idObjet);
            $loggerWs->info("Fin de l'enregistrement de l'historique de la synchronisation de l'etablissement");
            $etablissements = $resultat[1];

            if (is_array($etablissements)) {
                return $etablissements[$siret];
            }
        }

        if (is_array($resultat)) {
            foreach ($resultat as $key => $value) {
                if ('cas1' === $value || 'cas9' === $value || 'cas10' === $value || 'cas0' === $value) {
                    return 'Erreur';
                }
            }
        }

        return false;
    }

    /**
     * Permet de recuperer un etablissement selon le siret.
     *
     * @param Atexo_Entreprise_EtablissementVo $etablissementVo
     * @param CommonTEtablissement             $etablissment
     *
     * @return CommonTEtablissement
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function fillInfo($to, $from)
    {
        $to->setCodeEtablissement($from->getCodeEtablissement());
        $to->setEstSiege($from->getEstSiege());
        $to->setAdresse($from->getAdresse());
        $to->setAdresse2($from->getAdresse2());
        $to->setCodePostal($from->getCodePostal());
        $to->setVille($from->getVille());
        $to->setPays($from->getPays());
        $to->setSaisieManuelle($from->getSaisieManuelle());
        $to->setInscritAnnuaireDefense($from->getInscritAnnuaireDefense());
        $to->setDateModification($from->getDateModification());

        if (empty($to->getDateCreation())) {
            $to->setDateCreation(date('Y-m-d H:i:s'));
        }

        if (!empty($from->getIdEntreprise())) {
            $to->setIdEntreprise($from->getIdEntreprise());
        }

        $to->setStatutActif($from->getStatutActif());
        if (!empty($from->getIdEtablissement())) {
            $to->setIdEtablissement($from->getIdEtablissement());
        }
        $to->setTvaIntracommunautaire($from->getTvaIntracommunautaire());
        $to->setEtatAdministratif($from->getEtatAdministratif());
        $to->setDateFermeture($from->getDateFermeture());

        return $to;
    }

    /**
     * Permet d'ajouter un ensemble des objets CommonTEtablissement à un objet Entreprise depuis le tableau des Atexo_Entreprise_EtablissementVo.
     *
     * @param $etablissementsVo
     * @param $entreprise
     * @param bool $dateCreation
     * @param bool $dateModification
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     *
     * @author AMAL EL BEKKAOUI     <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setArrayCommonTEtablissementInEntreprise(
        $etablissementsVo,
        &$entreprise,
        $dateCreation = false,
        $dateModification = false
    ) {
        if ($entreprise instanceof Entreprise) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

            if (is_array($etablissementsVo)) {
                foreach ($etablissementsVo as $etablissementVo) {
                    if ($etablissementVo instanceof Atexo_Entreprise_EtablissementVo) {
                        $etablissement = CommonTEtablissementQuery::create()->findOneByIdEtablissement($etablissementVo->getIdEtablissement(), $connexion);
                        $etablissement = $etablissementVo->getCommonTEtablissement($etablissement);

                        if ($etablissement instanceof CommonTEtablissement) {
                            $etablissement->setIdEtablissement($etablissementVo->getIdEtablissement());
                            $etablissement->setIdEntreprise($etablissementVo->getIdEntreprise());

                            if ($dateCreation && !$etablissement->getIdEtablissement()) {
                                $etablissement->setDateCreation(date('Y-m-d H:i:s'));
                                $etablissement->setDateModification(date('Y-m-d H:i:s'));
                            }

                            if ($dateModification) {
                                $etablissement->setDateModification(date('Y-m-d H:i:s'));
                            }

                            $entreprise->addCommonTEtablissement($etablissement);
                        }
                    }
                }
            }
        }
    }

    /**
     * Permet d'enregistrer des objets CommonTEtablissement.
     *
     * @param $etablissementsVo
     * @param $idEntreprise
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function saveEtablissements($etablissementsVo, $idEntreprise)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        foreach ($etablissementsVo as $etablissementVo) {
            if ($etablissementVo instanceof Atexo_Entreprise_EtablissementVo) {
                $etablissementBDD = CommonTEtablissementQuery::create()->findOneByIdEtablissement($etablissementVo->getIdEtablissement(), $connexion);

                if ($etablissementBDD) {
                    if (true === self::hasEtablissementChanged($etablissementVo, $etablissementBDD)) {
                        $etablissement = $etablissementVo->getCommonTEtablissement($etablissementBDD);
                        $etablissement->save($connexion);
                        $etablissementBDD->setDateModification(date('Y-m-d H:i:s'));
                        $etablissementBDD->save($connexion);
                    }
                } else {
                    $etablissement = $etablissementVo->getCommonTEtablissement($etablissementBDD);
                    $etablissement->setIdEntreprise($idEntreprise);
                    $etablissement->setDateCreation(date('Y-m-d H:i:s'));
                    $etablissement->setDateModification(date('Y-m-d H:i:s'));
                    $etablissement->save($connexion);
                }
            }
        }
    }

    /**
     * @return bool
     */
    public function hasEtablissementChanged(
        Atexo_Entreprise_EtablissementVo $etablissementVo,
        CommonTEtablissement $etablissement
    ) {
        $getterMethods = [
            'getCodePostal',
            'getAdresse',
            'getAdresse2',
            'getVille',
            'getPays',
            'getTvaIntracommunautaire',
            'getCodeEtablissement',
            'getInscritAnnuaireDefense',
        ];

        foreach ($getterMethods as $getterMethod) {
            if ($etablissementVo->$getterMethod() != $etablissement->$getterMethod()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de retourner un ensemble d'objets etablissementVo appartir des objets CommonTEtablissement.
     *
     * @param array of CommonTEtablissement $etablissements
     *
     * @return array of Atexo_Entreprise_EtablissementVo $etablissementsVo
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function fillEtablissementsVoArray($etablissements)
    {
        $etablissementsVo = [];
        if (is_countable($etablissements) ? count($etablissements) : 0) {
            $codeEtablissement = 1;
            foreach ($etablissements as $oneEtab) {
                $etabVo = $oneEtab->getEtablissementVo();
                if ($etabVo->getCodeEtablissement() === '') {
                    $nbEtablissement = 5 - strlen($codeEtablissement);
                    if ($nbEtablissement > 0) {
                        $i = 1;
                        $zero = '0';
                        $zeros = '';
                        while ( $i <= $nbEtablissement) {
                            $zeros .=$zero;
                            $i++;
                        }
                    }

                    $etabVo->setCodeEtablissement($zeros.$codeEtablissement);

                }
                $etablissementsVo[$etabVo->getCodeEtablissement()] = $etabVo;
                $codeEtablissement++;
            }

            return $etablissementsVo;
        }
    }

    /**
     * Permet de supprimer un ensemble des objets CommonTEtablissement à partir de leurs ids.
     *
     * @return void
     *
     * @param $arrayIdEtablissement
     * @param $connexion
     *
     * @throws PropelException
     *
     * @author AMAL EL BEKKAOUI     <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function deleteEtablissements($arrayIdEtablissement, $connexion)
    {
        CommonTEtablissementPeer::doDelete($arrayIdEtablissement, $connexion);
    }

    /**
     * Permet de recuperer le code etablissement  a partir de l'id de l'etablissement.
     *
     * @param int $idEtablissement : id de l'etablissement
     *
     * @return string : code de l'etablissement
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getCodeEtablissemntByIdEtab($idEtablissement)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $etablissement = CommonTEtablissementQuery::create()->findOneByIdEtablissement($idEtablissement, $connexion);
        $codeEtablissement = '';

        if ($etablissement instanceof CommonTEtablissement) {
            $codeEtablissement = $etablissement->getCodeEtablissement();
        }

        return $codeEtablissement;
    }

    /**
     * Permet d'enregistrer l'objet etablissementVo.
     *
     * @param Atexo_Entreprise_EtablissementVo $etablissementVo : objet etablissement
     *
     * @return Atexo_Entreprise_EtablissementVo
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2016
     */
    public function enregistrerEtablissementApiGouvEntreprise($etablissementVo)
    {
        $connexion = null;
        $loggerWs = Atexo_LoggerManager::getLogger();

        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            }

            $etablissement = CommonTEtablissementQuery::create()
                ->filterByCodeEtablissement($etablissementVo->getCodeEtablissement())
                ->filterByIdEntreprise($etablissementVo->getIdEntreprise())
                ->findOne($connexion);

            if (!($etablissement instanceof CommonTEtablissement)) {
                $loggerWs->info("L'Etablisssement siege n'existe pas deja en BD");
                $etablissement = new CommonTEtablissement();
            } else {
                $loggerWs->info("L'Etablisssement siege existe deja en BD");
            }

            $etablissement = self::fillInfo($etablissement, $etablissementVo);
            $res = $etablissement->save($connexion);

            if ($res) {
                $loggerWs->info('Etablisssement siege synchronise avec Api Gouv Entreprise est mis a jour avec SUCCES sur MPE');

                return $etablissement->getEtablissementVo();
            }

            return false;
        } catch (\Exception $e) {
            $loggerWs->error("Erreur lors de l'enregistrement de l'etablissement : etablissementVo = ".print_r($etablissementVo, true)."\n\nErreur : ".$e->getMessage()." \n\nTrace : ".$e->getTraceAsString());
        }
    }

    /**
     * Permet de recuperer l'etablissementVo de l'entreprise.
     *
     * @param Entreprise $entreprise
     *
     * @return Atexo_Entreprise_AtablissmentVo
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getEtablissementVoFromEntreprise($entreprise, $code = null)
    {
        $etablissement = null;
        if ($entreprise instanceof Entreprise || $entreprise instanceof Atexo_Entreprise_EntrepriseVo) {
            if ($code) {
                $etablissementQuery = new CommonTEtablissementQuery();
                $etablissement = $etablissementQuery->getEtabissementbyIdEseAndCodeEtab($code, $entreprise->getId());
            }

            if (!$etablissement) {
                $etablissement = $entreprise->getMonEtablissementSiege();
            }

            $etablissementVo = null;

            if ($etablissement instanceof CommonTEtablissement) {
                $etablissementVo = $etablissement->getEtablissementVo();
            }

            return $etablissementVo;
        }
    }

    /**
     * recuperer l'etablissement siege d'une entreprise.
     *
     * @param $idEntreprise
     * @param null $connexion
     *
     * @return CommonTEtablissement
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function retreiveEtablissementSiegeByIdEntreprise($idEntreprise, $connexion = null)
    {
        $etablissementQuery = new CommonTEtablissementQuery();
        $etablissement = $etablissementQuery->getEtabissementSiegebyIdEntreprise($idEntreprise, $connexion);

        if ($etablissement instanceof CommonTEtablissement) {
            return $etablissement;
        }

        return false;
    }

    /**
     * Permet de recuperer un etablissement par son siret.
     *
     * @param string $siret : siret de l'etablissement
     *
     * @return CommonTEtablissement|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getEtablissementBySiret($siret)
    {
        if (!empty($siret)) {
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren(substr($siret, 0, 9));

            if ($entreprise instanceof Entreprise) {
                $etablissementQuery = new CommonTEtablissementQuery();

                return $etablissementQuery->getEtabissementbyIdEseAndCodeEtab(substr($siret, 9, 14), $entreprise->getId());
            }
        }

        return null;
    }
}
