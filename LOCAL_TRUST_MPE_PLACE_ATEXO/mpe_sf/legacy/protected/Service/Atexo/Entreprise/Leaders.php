<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonResponsableengagement;
use Application\Propel\Mpe\CommonResponsableengagementPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;

/**
 * Classe de manipulation des dirigeants.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise_Leaders
{
    /**
     * Retourne la liste des dirigeants d'une entreprise.
     *
     * @param int $idEntreprise l'id de l'entreprise
     *
     * @return array tableau d'objets des dirigeants
     */
    public static function retrieveLeaders($idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonResponsableengagementPeer::ENTREPRISE_ID, $idEntreprise, Criteria::EQUAL);
        $leaders = CommonResponsableengagementPeer::doSelect($c, $connexionCom);

        return $leaders;
    }

    /**
     * Supprime un dirigeant d'une entreprise.
     */
    public static function delete($idDirigeant, $idEntreprise, $connexion)
    {
        $leader = CommonResponsableengagementPeer::retrieveByPk($idDirigeant, $idEntreprise, $connexion);
        CommonResponsableengagementPeer::doDelete($leader, $connexion);
    }

    public function retrieveLeaderById($idLeader)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonResponsableengagementPeer::ID, $idLeader, Criteria::EQUAL);
        $leader = CommonResponsableengagementPeer::doSelectOne($c, $connexionCom);

        return $leader;
    }

    /**
     * permet de transformer un tableau de diri la liste des dirigeants d'une entreprise.
     *
     * @param array dirigeants
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setArrayCommonResponsableengagementInEntreprise($tabDirigeant, &$entreprise)
    {
        if ($entreprise instanceof Entreprise) {
            if (is_array($tabDirigeant)) {
                foreach ($tabDirigeant as $dirigeant) {
                    $commonResonsable = self::transformerLeaderObjet($dirigeant);
                    if ($commonResonsable) {
                        $entreprise->addCommonResponsableengagement($commonResonsable);
                    }
                }
            }
        }
    }

    /**
     * permet de supprimer les dirigeant d'une entreprise.
     *
     * @param array idEntreprise
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public static function deleteLeaders($idEntreprise, $connexion)
    {
        if ($idEntreprise) {
            if (!$connexion) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            }
            $c = new Criteria();
            $c->add(CommonResponsableengagementPeer::ENTREPRISE_ID, $idEntreprise, Criteria::EQUAL);
            CommonResponsableengagementPeer::doDelete($c, $connexion);
        }
    }

    /**
     * permet de supprimer les dirigeant d'une entreprise.
     *
     * @param array leaderTab retourner depuis le WS
     * @param idEntreprise
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function transformerLeaderObjet($leaderTab)
    {
        if (is_array($leaderTab)) {
            $dirigeant = new CommonResponsableengagement();
            $dirigeant->setNom($leaderTab['nom']);
            $dirigeant->setPrenom($leaderTab['prenom']);
            $dirigeant->setQualite($leaderTab['fonction']);

            return $dirigeant;
        } else {
            return false;
        }
    }
}
