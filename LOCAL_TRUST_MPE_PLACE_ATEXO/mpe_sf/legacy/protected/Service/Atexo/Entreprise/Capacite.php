<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Propel\Mpe\Entreprise;

/**
 * La class Capacite contient les informations concernant la capacite de l'entreprise.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.7.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Entreprise_Capacite
{
    private bool $_active = false;
    private ?int $_annee_cloture = null;
    private $_date_debut;
    private $_date_fin;
    private ?string $_chiffre_affaire = null;
    private ?string $_significatif = null;
    private ?string $_commentaire = null;
    private bool|string $_statutOk = false;
    private int $_saisie_manuelle = 1;

    /**
     * Get the value of [_active] column.
     *
     * @return bool
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getActive()
    {
        return $this->_active;
    }

    /**
     * Set the value of [_active] .
     *
     * @param bool $active new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setActive($active)
    {
        $this->_active = $active;
    }

    /**
     * Get the value of [_annee_cloture] column.
     *
     * @return int l'annee de cloture
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getAnneeCloture()
    {
        return $this->_annee_cloture;
    }

    /**
     * Set the value of [_annee_cloture] .
     *
     * @param int $annee_cloture new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setAnneeCloture($annee_cloture)
    {
        $this->_annee_cloture = $annee_cloture;
    }

    /**
     * Get the value of [_date_debut] column.
     *
     * @return Date la date de debut
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getDateDebut()
    {
        return $this->_date_debut;
    }

    /**
     * Set the value of [_date_debut] .
     *
     * @param Date $date_debut new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setDateDebut($date_debut)
    {
        $this->_date_debut = $date_debut;
    }

    /**
     * Get the value of [_date_fin] column.
     *
     * @return Date la date du fin
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getDateFin()
    {
        return $this->_date_fin;
    }

    /**
     * Set the value of [_date_fin] .
     *
     * @param Date $date_fin new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setDateFin($date_fin)
    {
        $this->_date_fin = $date_fin;
    }

    /**
     * Get the value of [_chiffre_affaire] column.
     *
     * @return string le chiffre d'affaire
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getChiffreAffaire()
    {
        return $this->_chiffre_affaire;
    }

    /**
     * Set the value of [_chiffre_affaire] .
     *
     * @param string $chiffre_affaire le chiffre d'affaire
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setChiffreAffaire($chiffre_affaire)
    {
        $this->_chiffre_affaire = $chiffre_affaire;
    }

    /**
     * Get the value of [_significatif] column.
     *
     * @return string significatif pour la consultation
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getSignificatif()
    {
        return $this->_significatif;
    }

    /**
     * Set the value of [_significatif] .
     *
     * @param string $significatif new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setSignificatif($significatif)
    {
        $this->_significatif = $significatif;
    }

    /**
     * Get the value of [_commentaire] column.
     *
     * @return string commentaire
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getCommentaire()
    {
        return $this->_commentaire;
    }

    /**
     * Set the value of [_commentaire] .
     *
     * @param string $commentaire new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setCommentaire($commentaire)
    {
        $this->_commentaire = $commentaire;
    }

    /**
     * Get the value of [_statutOk] column.
     *
     * @return string statutOk
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getStatutOK()
    {
        return $this->_statutOk;
    }

    /**
     * Set the value of [_statutOk] .
     *
     * @param string $statutOk new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setStatutOK($statutOk)
    {
        $this->_statutOk = $statutOk;
    }

    /**
     * @return int
     */
    public function getSaisieManuelle()
    {
        return $this->_saisie_manuelle;
    }

    /**
     * @param int $saisie_manuelle
     */
    public function setSaisieManuelle($saisie_manuelle)
    {
        $this->_saisie_manuelle = $saisie_manuelle;
    }
}
