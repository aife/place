<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use DateTime;

/**
 * Vo pour définir les critères de l'Etablissement.
 *
 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_Entreprise_EtablissementVo
{
    protected ?int $id_etablissement = null;
    protected ?int $id_entreprise = null;
    protected ?string $code_etablissement = null;
    protected ?string $est_siege = null;
    protected ?string $adresse = null;
    protected ?string $adresse2 = null;
    protected ?string $code_postal = null;
    protected ?string $ville = null;
    protected ?string $pays = null;
    protected ?string $saisie_manuelle = null;
    protected ?int $id_initial = null;
    protected $date_creation;
    protected $date_modification;
    protected $date_suppression;
    protected int|string $statut_actif = 1;
    protected int $inscrit_annuaire_defense = 0;
    protected ?string $code_ape = null;
    protected ?string $siret = null;
    protected ?string $libelleApe = null;
    protected ?string $tva_intracommunautaire = null;
    protected $etatAdministratif;
    protected $dateFermeture;

    // Accesseurs

    /**
     * Get the [id_etablissement] column value.
     *
     * @return int
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getIdEtablissement()
    {
        return $this->id_etablissement;
    }

    /**
     * Set the value of [id_etablissement] column.
     *
     * @param int $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setIdEtablissement($v)
    {
        $this->id_etablissement = $v;
    }

    // setIdEtablissement()

    /**
     * Get the [id_entreprise] column value.
     *
     * @return int
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getIdEntreprise()
    {
        return $this->id_entreprise;
    }

    /**
     * Set the value of [id_entreprise] column.
     *
     * @param int $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setIdEntreprise($v)
    {
        $this->id_entreprise = $v;
    }

    // setIdEntreprise()

    /**
     * Get the [code_etablissement] column value.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getCodeEtablissement()
    {
        return $this->code_etablissement;
    }

    /**
     * Set the value of [code_etablissement] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setCodeEtablissement($v)
    {
        $this->code_etablissement = $v;
    }

    // setCodeEtablissement()

    /**
     * Get the [est_siege] column value.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEstSiege()
    {
        return $this->est_siege;
    }

    /**
     * Set the value of [est_siege] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setEstSiege($v)
    {
        $this->est_siege = $v;
    }

    // setEstSiege()

    /**
     * Get the [adresse] column value.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set the value of [adresse] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setAdresse($v)
    {
        $this->adresse = $v;
    }

    // setAdresse()

    /**
     * Get the [adresse2] column value.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set the value of [adresse2] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setAdresse2($v)
    {
        $this->adresse2 = $v;
    }

    // setAdresse2()

    /**
     * Get the [code_postal] column value.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getCodePostal()
    {
        return $this->code_postal;
    }

    /**
     * Set the value of [code_postal] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setCodePostal($v)
    {
        $this->code_postal = $v;
    }

    // setCodePostal()

    /**
     * Get the [ville] column value.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set the value of [ville] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setVille($v)
    {
        $this->ville = $v;
    }

    // setVille()

    /**
     * Get the [pays] column value.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set the value of [pays] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setPays($v)
    {
        $this->pays = $v;
    }

    // setPays()

    /**
     * Get the [saisie_manuelle] column value.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getSaisieManuelle()
    {
        return $this->saisie_manuelle;
    }

    /**
     * Set the value of [saisie_manuelle] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setSaisieManuelle($v)
    {
        $this->saisie_manuelle = $v;
    }

    // setSaisieManuelle()

    /**
     * Get the [id_initial] column value.
     *
     * @return int
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getIdInitial()
    {
        return $this->id_initial;
    }

    /**
     * Set the value of [id_initial] column.
     *
     * @param int $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setIdInitial($v)
    {
        $this->id_initial = $v;
    }

    // setIdInitial()

    /**
     * Get the [date_creation] column.
     *
     * @return date/time value as string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getDateCreation()
    {
        return $this->date_creation;
    }

    /**
     * Sets the value of [date_creation] column.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *                 Empty strings are treated as null.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setDateCreation($v)
    {
        $this->date_creation = $v;
    }

    // setDateCreation()

    /**
     * Get the [date_modification] column value.
     *
     * @returndate/time value as string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getDateModification()
    {
        return $this->date_modification;
    }

    /**
     * Sets the value of [date_modification] column .
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *                 Empty strings are treated as null.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setDateModification($v)
    {
        $this->date_modification = $v;
    }

    // setDateModification()

    /**
     * Get the [date_suppression] column value.
     *
     * @return date/time value as string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getDateSuppression()
    {
        return $this->date_suppression;
    }

    /**
     * Sets the value of [date_suppression] column .
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *                 Empty strings are treated as null.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setDateSuppression($v)
    {
        $this->date_suppression = $v;
    }

    // setDateSuppression()

    /**
     * Get the [statut_actif] column value.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getStatutActif()
    {
        return $this->statut_actif;
    }

    /**
     * Set the value of [statut_actif] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setStatutActif($v)
    {
        $this->statut_actif = $v;
    }

    // setStatutActif()

    /**
     * Get the [inscrit_annuaire_defense] column value.
     *
     * @return int
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getInscritAnnuaireDefense()
    {
        return $this->inscrit_annuaire_defense;
    }

    /**
     * Set the value of [inscrit_annuaire_defense] column.
     *
     * @param int $v new value
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setInscritAnnuaireDefense($v)
    {
        $this->inscrit_annuaire_defense = $v;
    }

    /**
     * Get the [code_ape] column value.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getCodeape()
    {
        return $this->code_ape;
    }

    /**
     * Set the value of [code_ape] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setCodeape($v)
    {
        $this->code_ape = $v;
    }

    // setCodeape()

    /**
     * Get the [siret] column value.
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set the value of [siret] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setSiret($v)
    {
        $this->siret = $v;
    }

    // setCodeape()

    /**
     * Get the [libelleApe] column value.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getLibelleApe()
    {
        return $this->libelleApe;
    }

    /**
     * Set the value of [libelleApe] column.
     *
     * @param string $value new value
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setLibelleApe($value)
    {
        $this->libelleApe = $value;
    }

    /**
     * Get the [tva_intracommunautaire] column value.
     *
     * @return string
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function getTvaIntracommunautaire()
    {
        return $this->tva_intracommunautaire;
    }

    /**
     * Set the value of [tva_intracommunautaire] column.
     *
     * @param string $tva_intracommunautaire
     *
     * @return void
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function setTvaIntracommunautaire($tva_intracommunautaire)
    {
        $this->tva_intracommunautaire = $tva_intracommunautaire;
    }

    /**
     * @return mixed
     */
    public function getEtatAdministratif()
    {
        return $this->etatAdministratif;
    }

    /**
     * @param mixed $etatAdministratif
     */
    public function setEtatAdministratif($etatAdministratif)
    {
        $this->etatAdministratif = $etatAdministratif;
    }

    /**
     * @return mixed
     */
    public function getDateFermeture()
    {
        return $this->dateFermeture;
    }

    /**
     * @param mixed $dateFermeture
     */
    public function setDateFermeture($dateFermeture)
    {
        $this->dateFermeture = $dateFermeture;
    }

    /**
     * Permet de recuperer les Etablissements.
     *
     * @param array les information d'etablissement
     *
     * @return array un tableau contenant l'objet Atexo_Entreprise_EtablissementVo
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public static function loadFromApiGouvEntreprise($infoEtablissement)
    {
        if (is_array($infoEtablissement)) {
            $etablissements = [];
            $etablissement = new Atexo_Entreprise_EtablissementVo();
            if ($infoEtablissement['siege_social']) {
                $etablissement->setEstSiege('1');
            } else {
                $etablissement->setEstSiege('0');
            }
            $etablissement->setSaisieManuelle('0');
            $etablissement->setSiret($infoEtablissement['siret']);
            $etablissement->setCodeEtablissement(substr($infoEtablissement['siret'], 9, 5));

            $infoAdresse = Atexo_Entreprise_Etablissement::getInfoAdresseEtablissement($infoEtablissement['adresse']);
            if (is_array($infoAdresse)) {
                $etablissement->setAdresse($infoAdresse['adresse']);
                $etablissement->setAdresse2($infoAdresse['adresse2']);
                $etablissement->setCodePostal($infoAdresse['cp']);
                $etablissement->setVille($infoAdresse['ville']);
            }

            $infosEtatAdministratif = $infoEtablissement['etat_administratif_etablissement'] ?? null;
            if (is_array($infosEtatAdministratif) && !empty($infosEtatAdministratif)) {
                if (0 == strcmp((new Atexo_Config())->toPfEncoding($infosEtatAdministratif['value']), 'Actif')) {
                    $etablissement->setStatutActif('1');
                } else {
                    $etablissement->setStatutActif('0');
                }
            }

            $etablissement->setEtatAdministratif($infoEtablissement['etat_administratif']);
            if ($infoEtablissement['date_fermeture']) {
                $date = new DateTime();
                $date->setTimestamp($infoEtablissement['date_fermeture']);
                $etablissement->setDateFermeture($date);
            }

            $activitePrincipale = $infoEtablissement['activite_principale'] ?? null;
            $codeApe = (new Atexo_Config())->toPfEncoding(
                str_replace('.', '', $activitePrincipale['code']) ?? ''
            );
            $libelleNaf = (new Atexo_Config())->toPfEncoding($activitePrincipale['libelle'] ?? '');
            $etablissement->setCodeape($codeApe);
            $etablissement->setLibelleApe($libelleNaf);
            $etablissement->setPays(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
            $etablissement->setDateModification(date("Y-m-d H:i:s"));

            return [$infoEtablissement['siret'] => $etablissement];
        }

        return $infoEtablissement;
    }

    /**
     * Permet de recuperer un objet CommonTEtablissement depuis Atexo_Entreprise_EtablissementVo.
     *
     * @param Atexo_Entreprise_EtablissementVo $etablissementVo
     *
     * @return CommonTEtablissement
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getCommonTEtablissement($etablissement = null)
    {
        if (!$etablissement) {
            $etablissement = new CommonTEtablissement();
        }

        return (new Atexo_Entreprise_Etablissement())->fillInfo($etablissement, $this);
    }

    /**
     * Permet de retourner le mode d'affichage des informations de l'etablissement selon l'attribut saisieManuelle.
     *
     * @param EtablissementVo $etablissementVo : l'objet etablissementVo
     * @param string          $calledFrome     :
     * @retrun string : 'italique' ou ''
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getCssSaisieMnuelle($etablissementVo, $calledFrom)
    {
        if ($etablissementVo) {
            if (Atexo_Module::isEnabled('SynchronisationSGMAP') && (1 == $etablissementVo->getSaisieManuelle()) && strstr($calledFrom, 'Agent.')) {
                return 'italique';
            } else {
                return false;
            }
        }
    }
}
