<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonPanierEntreprise;
use Application\Propel\Mpe\CommonPanierEntreprisePeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Exception;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe de gestion des paniers des entreprises.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_Entreprise_PaniersEntreprises
{
    public static function getPanierEntreprise($org, $consultationId, $idEtp, $idInscrit)
    {
        if (0 == $idEtp || 0 == $idInscrit) {
            return false;
        }

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonPanierEntreprisePeer::ORGANISME, $org);
        $c->add(CommonPanierEntreprisePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonPanierEntreprisePeer::ID_ENTREPRISE, $idEtp);
        $c->add(CommonPanierEntreprisePeer::ID_INSCRIT, $idInscrit);

        return CommonPanierEntreprisePeer::doSelectOne($c, $connexion);
    }

    public static function deletePanierEntreprise($org, $consultationId, $idEtp, $idInscrit)
    {
        if (0 == $idEtp || 0 == $idInscrit) {
            return false;
        }

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonPanierEntreprisePeer::ORGANISME, $org);
        $c->add(CommonPanierEntreprisePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonPanierEntreprisePeer::ID_ENTREPRISE, $idEtp);
        $c->add(CommonPanierEntreprisePeer::ID_INSCRIT, $idInscrit);
        if (CommonPanierEntreprisePeer::doDelete($c, $connexion)) {
            return true;
        }

        return false;
    }

    public static function AddConsultationToPanierEntreprise($org, $consultationId, $idEtp, $idInscrit)
    {
        $panierEntreprise = self::getPanierEntreprise($org, $consultationId, $idEtp, $idInscrit);
        if (!($panierEntreprise instanceof CommonPanierEntreprise)) {
            if (!Atexo_CurrentUser::isConnected()) {
                return;
            }
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $panierEntreprise = new CommonPanierEntreprise();
            $panierEntreprise->setOrganisme($org);
            $panierEntreprise->setConsultationId($consultationId);
            $panierEntreprise->setIdEntreprise($idEtp);
            $panierEntreprise->setIdInscrit($idInscrit);
            $panierEntreprise->setDateAjout(date('Y-m-d H:i:s'));
            if ($panierEntreprise->save($connexion)) {
                return true;
            }

            return false;
        }

        return false;
    }

    /**
     * Effectue la recherche sur la fonctionnalité de panier entreprise.
     *
     * @param $criteriaVo: contient les criteres de recherche
     *
     * @return array(): tableau des (idConsultation, organisme) qui repondent aux critères
     */
    public function searchPanier($criteriaVo)
    {
        $queryParams = [];
        $query = ' SELECT PE.consultation_id, PE.organisme ';
        $query .= ' FROM Panier_Entreprise PE ';
        if ($criteriaVo->getAvecRetrait()) {
            $query .= ' , Telechargement T ';
        }
        if ($criteriaVo->getAvecQuestion()) {
            $query .= ' , questions_dce Q ';
        }
        if ($criteriaVo->getAvecDepot()) {
            $query .= ' , Offres O ';
        }
        if ($criteriaVo->getAvecEchange()) {
            $query .= ' , Echange E , EchangeDestinataire ED, Inscrit I ';
        }
        $query .= " WHERE PE.id_inscrit = '".$criteriaVo->getIdInscrit()."' ";
        if ($criteriaVo->getAvecRetrait()) {
            $query .= ' AND PE.consultation_id = T.consultation_id AND PE.organisme = T.organisme AND PE.id_inscrit = T.id_inscrit ';
        }
        if ($criteriaVo->getAvecQuestion()) {
            $query .= ' AND PE.consultation_id = Q.consultation_id AND PE.organisme = Q.organisme AND PE.id_inscrit = Q.id_inscrit ';
        }
        if ($criteriaVo->getAvecDepot()) {
            $query .= ' AND PE.consultation_id = O.consultation_id AND PE.organisme = O.organisme AND PE.id_inscrit = O.inscrit_id ';
        }
        if ($criteriaVo->getAvecEchange()) {
            $query .= ' AND PE.consultation_id = E.consultation_id AND PE.organisme = E.organisme ';
            $query .= ' AND E.id = ED.id_echange AND E.organisme = ED.organisme ';
            $query .= ' AND ( ED.mail_destinataire = I.email  OR E.email_expediteur = I.email ) AND I.id = :idInscrit ';
        }
        if ($criteriaVo->getSansRetrait()) {
            $query .= ' AND PE.`consultation_id` NOT IN ( SELECT T.consultation_id FROM Telechargement T  ';
            $query .= ' WHERE T.`id_inscrit` =  PE.`id_inscrit` AND T.`organisme` =  PE.`organisme` ) ';
        }
        if ($criteriaVo->getSansQuestion()) {
            $query .= ' AND  PE.`consultation_id` NOT IN ( SELECT Q.consultation_id FROM questions_dce Q  ';
            $query .= ' WHERE Q.id_inscrit =  PE.`id_inscrit` AND Q.`organisme` =  PE.`organisme` ) ';
        }
        if ($criteriaVo->getSansDepot()) {
            $query .= ' AND  PE.`consultation_id` NOT IN ( SELECT O.consultation_id FROM Offres O  ';
            $query .= ' WHERE O.inscrit_id =  PE.`id_inscrit` AND O.`organisme` =  PE.`organisme` ) ';
        }
        if ($criteriaVo->getSansEchange()) {
            $query .= ' AND  PE.`consultation_id` NOT IN ( SELECT E.consultation_id FROM Echange E , EchangeDestinataire ED, Inscrit I ';
            $query .= ' WHERE PE.organisme = E.organisme AND E.id = ED.id_echange AND E.organisme = ED.organisme  ';
            $query .= ' AND ( ED.mail_destinataire = I.email  OR E.email_expediteur = I.email ) AND I.id = :idInscrit  ';
            $query .= '  ) ';
        }

        $queryParams[':idInscrit'] = $criteriaVo->getIdInscrit();

        /*try {
            foreach($queryParams as $a => $b) {
                if($query) {
                    $query = str_replace($a."", "'".$b."' ", $query);
                }

            }
            echo $query;exit;
        } catch(Exception $e ) {

        }*/

        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        try {
            $statement->execute($queryParams);
        } catch (Exception $e) {
            //print_r($statement);exit;
            Prado::log('Erreur PaniersEntreprises.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'PaniersEntreprises.php');
        }
        $arrayResult = [];
        $arrayVerif = [];
        $i = 0;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $key = $row['consultation_id'].'_'.$row['organisme'];
            if (!in_array($key, $arrayVerif)) {
                $arrayVerif[] = $row['consultation_id'].'_'.$row['organisme'];
                $arrayResult[$i][$row['organisme']] = $row['consultation_id'];
            }
            ++$i;
        }
        $arrayFinal = [];
        if (is_array($arrayResult) && count($arrayResult)) {
            foreach ($arrayResult as $key => $result) {
                foreach ($result as $org => $ref) {
                    $arrayFinal[$org][] = $ref;
                }
            }
        }

        return $arrayFinal;
    }
}
