<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultationDocumentCfePeer;
use Application\Propel\Mpe\CommonJustificatifsPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Module;
use Prado\Prado;

/**
 * Classe de manipulation des alertes.
 *
 * @author Adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise_CoffreFort
{
    /**
     * Méthode de récupération des documents marqués comme visibles par une entreprise.
     */
    public function retrievePiecesCoffreFortByIdEntreprise($idEntreprise, $sortBy = null, $sortOrder = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonJustificatifsPeer::ID_ENTREPRISE, $idEntreprise);
        $c->add(CommonJustificatifsPeer::VISIBLE_PAR_AGENTS, '1');
        if ('DocumentName' == $sortBy) {
            if ('ASC' == $sortOrder) {
                $c->addAscendingOrderByColumn(CommonJustificatifsPeer::INTITULE_JUSTIFICATIF);
            } else {
                $c->addDescendingOrderByColumn(CommonJustificatifsPeer::INTITULE_JUSTIFICATIF);
            }
        }

        $piecesEntreprises = CommonJustificatifsPeer::doSelectJoinCommonDocumentsAttaches($c, $connexion);

        return $piecesEntreprises;
    }

    public function getToolTip($idEntreprise, $consultationId = null, $orgConsultation = null)
    {
        $piecesEntreprises = self::retrievePiecesCoffreFortByIdEntreprise($idEntreprise);
        $texteCoffreFort = '';
        if (Atexo_Module::isEnabled('AssocierDocumentsCfeConsultation')) {
            $piecesEntreprises = (new Atexo_Entreprise_CoffreFort())->retrievePiecesConsultationDocumentCfeByIdEntreprise($idEntreprise, $consultationId, $orgConsultation);
            $texteCoffreFort = Prado::localize('DEFINE_DOCUMENTS_COFFRE_FORT_ENTREPRISE_ASSOCIES_CONSULTATION');
        } else {
            $piecesEntreprises = (new Atexo_Entreprise_CoffreFort())->retrievePiecesCoffreFortByIdEntreprise($idEntreprise);
            $texteCoffreFort = Prado::localize('ACCEDER_COFFRE_FORT_ELECTRONIQUE');
        }
        if (!is_array($piecesEntreprises)) {
            $piecesEntreprises = [];
        }

        return $texteCoffreFort.' : '.count($piecesEntreprises).' '.Prado::localize('DOCUMENT_S');
    }

    public function getToolTipBySiret($siret, $consultationId = null, $orgConsultation = null)
    {
        if ($siret) {
            $siren = substr($siret, 0, 9);
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren($siren);
            if ($entreprise instanceof Entreprise) {
                return self::getToolTip($entreprise->getId(), $consultationId, $orgConsultation);
            }
        }

        return Prado::localize('TEXT_COFFRE_FORT_INACCESSIBLE');
    }

    /**
     * retourne les documents du coffre-fort associés à une consultation.
     */
    public function retrievePiecesConsultationDocumentCfeByIdEntreprise($idEntreprise, $consultationId, $organismeConsultation, $sortBy = null, $sortOrder = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationDocumentCfePeer::ID_ENTREPRISE, $idEntreprise);
        $c->add(CommonConsultationDocumentCfePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonConsultationDocumentCfePeer::ORGANISME_CONSULTATION, $organismeConsultation);
        if ('DocumentName' == $sortBy) {
            if ('ASC' == $sortOrder) {
                $c->addAscendingOrderByColumn(CommonConsultationDocumentCfePeer::TYPE_DOCUMENT);
            } else {
                $c->addDescendingOrderByColumn(CommonConsultationDocumentCfePeer::TYPE_DOCUMENT);
            }
        }

        return CommonConsultationDocumentCfePeer::doSelect($c, $connexion);
    }

    public function enabelCoffreFort($siret)
    {
        if ($siret) {
            $siren = substr($siret, 0, 9);
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren($siren);
            if ($entreprise instanceof Entreprise) {
                return 'true';
            }
        }

        return 'false';
    }
}
