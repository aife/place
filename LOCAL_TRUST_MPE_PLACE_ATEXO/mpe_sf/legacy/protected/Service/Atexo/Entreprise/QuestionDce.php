<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonQuestionsDcePeer;
use Application\Service\Atexo\Atexo_Config;

class Atexo_Entreprise_QuestionDce
{
    public function getQuestionsDces($consultationId, $org)
    {
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonQuestionsDcePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonQuestionsDcePeer::ORGANISME, $org);
        $results = CommonQuestionsDcePeer::doSelect($c, $connection);
        if ($results) {
            return $results;
        } else {
            return [];
        }
    }
}
