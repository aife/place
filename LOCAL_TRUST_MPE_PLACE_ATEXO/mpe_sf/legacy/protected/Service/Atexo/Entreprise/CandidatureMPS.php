<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Entreprise;

/**
 * La class CandidatureMPS contient les informations concernant la candidature de l'entreprise.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.7.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Entreprise_CandidatureMPS
{
    private ?int $_refConsultation = null;
    private ?string $_organisme = null;
    private ?string $_referenceUtilisateur = null;
    private ?string $_objetConsultation = null;
    private ?string $_dateLimiteRemise = null;

    private ?string $_horodatage = null;
    private $_untrustedDate;
    private ?string $_untrustedSerial = null;

    private ?\Application\Propel\Mpe\Entreprise $_entreprise = null;
    private ?int $_idInscrit = null;
    // info Conformite
    private bool $_obligSocialesAjour = false;
    private bool $_obligFiscalesAjour = false;
    private bool $_obligDeclaration = false;
    private bool $_entrepriseGroupeIS = false;
    private bool $_entrepriseGroupeISMere = false;
    private bool $_entrepriseGroupeISFille = false;
    private bool $_entrepriseGroupeTVA = false;
    private bool $_entrepriseGroupeTVAMere = false;
    private bool $_entrepriseGroupeTVAFille = false;

    private bool $_assuranceContracte = false;
    // info Capacite
    private $_capacites;
    private ?int $_trancheEffectifs = null;
    private bool $_attestationHonneurArticle43 = false;

    //info Declaration
    private bool $_pouvoirEngage = false;
    private ?bool $_representantLegal = null;
    private ?string $_nomRepresentantLegal = null;
    private $_prenomRepresentantLegal;
    private ?string $_qualiteRepresentantLegal = null;
    private bool $_autorisationVerification = false;
    private $_etablissement;

    // Objet Candidature
    private $_listeLots;

    /**
     * Get the value of [_refConsultation] column.
     *
     * @return int la reference de la consultation
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    /**
     * Set the value of [_refConsultation] .
     *
     * @param int $consultationId new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setRefConsultation($consultationId)
    {
        $this->_refConsultation = $consultationId;
    }

    /**
     * Get the value of [_organisme] column.
     *
     * @return string l'organisme de la consultation
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getOrganisme()
    {
        return $this->_organisme;
    }

    /**
     * Set the value of [_organisme] .
     *
     * @param string $organisme new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    /**
     * Get the value of [_referenceUtilisateur] column.
     *
     * @return string la reference utilisateur de la consultation
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getReferenceUtilisateur()
    {
        return $this->_referenceUtilisateur;
    }

    /**
     * Set the value of [_referenceUtilisateur] .
     *
     * @param string $referenceUtilisateur new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setReferenceUtilisateur($referenceUtilisateur)
    {
        $this->_referenceUtilisateur = $referenceUtilisateur;
    }

    /**
     * Get the value of [_objetConsultation] column.
     *
     * @return string l'objet de la consultation
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getObjetConsultation()
    {
        return $this->_objetConsultation;
    }

    /**
     * Set the value of [_objetConsultation] .
     *
     * @param string $objetConsultation new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setObjetConsultation($objetConsultation)
    {
        $this->_objetConsultation = $objetConsultation;
    }

    /**
     * Get the value of [_dateLimiteRemise] column.
     *
     * @return string date limite de remise des plis
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getDateLimiteRemise()
    {
        return $this->_dateLimiteRemise;
    }

    /**
     * Set the value of [_dateLimiteRemise] .
     *
     * @param string $dateLimiteRemise new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setDateLimiteRemise($dateLimiteRemise)
    {
        $this->_dateLimiteRemise = $dateLimiteRemise;
    }

    /**
     * Get the value of [_horodatage] column.
     *
     * @return string l'organisme de la consultation
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getHorodatage()
    {
        return $this->_horodatage;
    }

    /**
     * Set the value of [_organisme] .
     *
     * @param string $horodatage new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setHorodatage($horodatage)
    {
        $this->_horodatage = $horodatage;
    }

    /**
     * Get the value of [_untrustedDate] column.
     *
     * @return Date untrusted Date
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getUntrustedDate()
    {
        return $this->_untrustedDate;
    }

    /**
     * Set the value of [_untrustedDate] .
     *
     * @param Date $untrustedDate new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setUntrustedDate($untrustedDate)
    {
        $this->_untrustedDate = $untrustedDate;
    }

    /**
     * Get the value of [_untrustedSerial] column.
     *
     * @return string untrusted Serial
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getUntrustedSerial()
    {
        return $this->_untrustedSerial;
    }

    /**
     * Set the value of [_untrustedSerial] .
     *
     * @param string $untrustedSerial new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setUntrustedSerial($untrustedSerial)
    {
        $this->_untrustedSerial = $untrustedSerial;
    }

    /**
     * Get the value of [_entreprise] column.
     *
     * @return Entreprise l'entreprise lie à la candidature MPS
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getEntreprise()
    {
        return $this->_entreprise;
    }

    /**
     * Set the value of [_entreprise] .
     *
     * @param Entreprise $entreprise new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setEntreprise($entreprise)
    {
        $this->_entreprise = $entreprise;
    }

    /**
     * Get the value of [_idInscrit] column.
     *
     * @return int l'id inscrit de l'entreprise
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getIdInscrit()
    {
        return $this->_idInscrit;
    }

    /**
     * Set the value of [_idInscrit] .
     *
     * @param int $idInscrit new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setIdInscrit($idInscrit)
    {
        $this->_idInscrit = $idInscrit;
    }

    /**
     * Get the value of [_capacites] column.
     *
     * @return array Atexo_Entreprise_Capacite les capacites
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getCapacites()
    {
        return $this->_capacites;
    }

    /**
     * Set the value of [_capacites] .
     *
     * @param array of Atexo_Entreprise_Capacite $capacites new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setCapacites($capacites)
    {
        $this->_capacites = $capacites;
    }

    /**
     * Get the value of [_obligSocialesAjour] column.
     *
     * @return bool $obligSocialesAjour
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getObligSocialesAjour()
    {
        return $this->_obligSocialesAjour;
    }

    /**
     * Set the value of [_obligSocialesAjour] .
     *
     * @param bool $obligSocialesAjour new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setObligSocialesAjour($obligSocialesAjour)
    {
        $this->_obligSocialesAjour = $obligSocialesAjour;
    }

    /**
     * Get the value of [_obligFiscalesAjour] column.
     *
     * @return bool $obligFiscalesAjour
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getObligFiscalesAjour()
    {
        return $this->_obligFiscalesAjour;
    }

    /**
     * Set the value of [_obligFiscalesAjour] .
     *
     * @param bool $obligFiscalesAjour new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setObligFiscalesAjour($obligFiscalesAjour)
    {
        $this->_obligFiscalesAjour = $obligFiscalesAjour;
    }

    /**
     * Get the value of [_entrepriseGroupeIS] column.
     *
     * @return bool $entrepriseGroupeIS
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getEntrepriseGroupeIS()
    {
        return $this->_entrepriseGroupeIS;
    }

    /**
     * Set the value of [_entrepriseGroupeIS] .
     *
     * @param bool $entrepriseGroupeIS new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setEntrepriseGroupeIS($entrepriseGroupeIS)
    {
        $this->_entrepriseGroupeIS = $entrepriseGroupeIS;
    }

    /**
     * Get the value of [_entrepriseGroupeISMere] column.
     *
     * @return bool $entrepriseGroupeISMere
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getEntrepriseGroupeISMere()
    {
        return $this->_entrepriseGroupeISMere;
    }

    /**
     * Set the value of [_entrepriseGroupeISMere] .
     *
     * @param bool $entrepriseGroupeISMere new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setEntrepriseGroupeISMere($entrepriseGroupeISMere)
    {
        $this->_entrepriseGroupeISMere = $entrepriseGroupeISMere;
    }

    /**
     * Get the value of [_entrepriseGroupeISFille] column.
     *
     * @return bool $entrepriseGroupeISFille
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getEntrepriseGroupeISFille()
    {
        return $this->_entrepriseGroupeISFille;
    }

    /**
     * Set the value of [_entrepriseGroupeISFille] .
     *
     * @param bool $entrepriseGroupeISFille new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setEntrepriseGroupeISFille($entrepriseGroupeISFille)
    {
        $this->_entrepriseGroupeISFille = $entrepriseGroupeISFille;
    }

    /**
     * Get the value of [_entrepriseGroupeTVA] column.
     *
     * @return bool $entrepriseGroupeTVA
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getEntrepriseGroupeTVA()
    {
        return $this->_entrepriseGroupeTVA;
    }

    /**
     * Set the value of [_entrepriseGroupeTVA] .
     *
     * @param bool $entrepriseGroupeTVA new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setEntrepriseGroupeTVA($entrepriseGroupeTVA)
    {
        $this->_entrepriseGroupeTVA = $entrepriseGroupeTVA;
    }

    /**
     * Get the value of [_entrepriseGroupeTVAMere] column.
     *
     * @return bool $entrepriseGroupeTVAMere
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getEntrepriseGroupeTVAMere()
    {
        return $this->_entrepriseGroupeTVAMere;
    }

    /**
     * Set the value of [_entrepriseGroupeTVAMere] .
     *
     * @param bool $entrepriseGroupeTVAMere new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setEntrepriseGroupeTVAMere($entrepriseGroupeTVAMere)
    {
        $this->_entrepriseGroupeTVAMere = $entrepriseGroupeTVAMere;
    }

    /**
     * Get the value of [_entrepriseGroupeTVAFille] column.
     *
     * @return bool $entrepriseGroupeTVAFille
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getEntrepriseGroupeTVAFille()
    {
        return $this->_entrepriseGroupeTVAFille;
    }

    /**
     * Set the value of [_entrepriseGroupeTVAFille] .
     *
     * @param bool $entrepriseGroupeTVAFille new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setEntrepriseGroupeTVAFille($entrepriseGroupeTVAFille)
    {
        $this->_entrepriseGroupeTVAFille = $entrepriseGroupeTVAFille;
    }

    /**
     * Get the value of [_assuranceContracte] column.
     *
     * @return bool $assuranceContracte
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getAssuranceContracte()
    {
        return $this->_assuranceContracte;
    }

    /**
     * Set the value of [_assuranceContracte] .
     *
     * @param bool $assuranceContracte new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setAssuranceContracte($assuranceContracte)
    {
        $this->_assuranceContracte = $assuranceContracte;
    }

    /**
     * Get the value of [_pouvoirEngage] column.
     *
     * @return bool $pouvoirEngage
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getPouvoirEngage()
    {
        return $this->_pouvoirEngage;
    }

    /**
     * Set the value of [_pouvoirEngage] .
     *
     * @param bool $pouvoirEngage new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setPouvoirEngage($pouvoirEngage)
    {
        $this->_pouvoirEngage = $pouvoirEngage;
    }

    /**
     * Get the value of [_representantLegal] column.
     *
     * @return bool $representantLegal
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getRepresentantLegal()
    {
        return $this->_representantLegal;
    }

    /**
     * Set the value of [_representantLegal] .
     *
     * @param bool $representantLegal new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setRepresentantLegal($representantLegal)
    {
        $this->_representantLegal = $representantLegal;
    }

    /**
     * Get the value of [_autorisationVerification] column.
     *
     * @return bool $autorisationVerification
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getAutorisationVerification()
    {
        return $this->_autorisationVerification;
    }

    /**
     * Set the value of [_representantLegal] .
     *
     * @param bool $autorisationVerification new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setAutorisationVerification($autorisationVerification)
    {
        $this->_autorisationVerification = $autorisationVerification;
    }

    /**
     * Get the value of [_nomRepresentantLegal] column.
     *
     * @return string $nomRepresentantLegal
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getNomRepresentantLegal()
    {
        return $this->_nomRepresentantLegal;
    }

    /**
     * Set the value of [_representantLegal] .
     *
     * @param string $nomRepresentantLegal new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setNomRepresentantLegal($nomRepresentantLegal)
    {
        $this->_nomRepresentantLegal = $nomRepresentantLegal;
    }

    /**
     * Get the value of [_prenomRepresentantLegal] column.
     *
     * @return string $prenomRepresentantLegal
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getPrenomRepresentantLegal()
    {
        return $this->_prenomRepresentantLegal;
    }

    /**
     * Set the value of [_prenomRepresentantLegal] .
     *
     * @param string $representantLegal new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setPrenomRepresentantLegal($prenomRepresentantLegal)
    {
        $this->_prenomRepresentantLegal = $prenomRepresentantLegal;
    }

    /**
     * Get the value of [_qualiteRepresentantLegal] column.
     *
     * @return string $qualiteRepresentantLegal
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getQualiteRepresentantLegal()
    {
        return $this->_qualiteRepresentantLegal;
    }

    /**
     * Set the value of [_qualiteRepresentantLegal] .
     *
     * @param string $qualiteRepresentantLegal new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setQualiteRepresentantLegal($qualiteRepresentantLegal)
    {
        $this->_qualiteRepresentantLegal = $qualiteRepresentantLegal;
    }

    /**
     * Get the value of [_trancheEffectifs] column.
     *
     * @return int $trancheEffectifs
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getTrancheEffectifs()
    {
        return $this->_trancheEffectifs;
    }

    /**
     * Set the value of [_trancheEffectifs] .
     *
     * @param int $trancheEffectifs new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setTrancheEffectifs($trancheEffectifs)
    {
        $this->_trancheEffectifs = $trancheEffectifs;
    }

    /**
     * Get the value of [_attestationHonneurArticle43] column.
     *
     * @return bool $attestationHonneurArticle43
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getAttestationHonneurArticle43()
    {
        return $this->_attestationHonneurArticle43;
    }

    /**
     * Set the value of [_attestationHonneurArticle43] .
     *
     * @param bool $attestationHonneurArticle43 new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setAttestationHonneurArticle43($attestationHonneurArticle43)
    {
        $this->_attestationHonneurArticle43 = $attestationHonneurArticle43;
    }

    /**
     * Get the value of [_etablissement] column.
     *
     * @return bool $attestationHonneurArticle43
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEtablissement()
    {
        return $this->_etablissement;
    }

    /**
     * Set the value of [_etablissement] .
     *
     * @param bool $attestationHonneurArticle43 new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setEtablissement($etablissement)
    {
        $this->_etablissement = $etablissement;
    }

    /**
     * @return bool
     */
    public function getObligDeclaration()
    {
        return $this->_obligDeclaration;
    }

    /**
     * @param bool $obligDeclaration
     */
    public function setObligDeclaration($obligDeclaration)
    {
        $this->_obligDeclaration = $obligDeclaration;
    }

    /**
     * @return mixed
     */
    public function getListeLots()
    {
        return $this->_listeLots;
    }

    /**
     * @param mixed $listeLots
     */
    public function setListeLots($listeLots)
    {
        $this->_listeLots = $listeLots;
    }
}
