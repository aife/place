<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonPrestationPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;

/**
 * Classe de manipulation des prestations.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise_Prestation
{
    /**
     * Retourne la liste des prestations d'une entreprise.
     *
     * @param int $idEntreprise l'id de l'entreprise
     *
     * @return array tableau d'objets des prestations
     */
    public static function retrievePrestations($idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonPrestationPeer::ID_ENTREPRISE, $idEntreprise, Criteria::EQUAL);

        return CommonPrestationPeer::doSelect($c, $connexionCom);
    }

    public function retrievePrestationById($id, $idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonPrestationPeer::ID, $id);
        $c->add(CommonPrestationPeer::ID_ENTREPRISE, $idEntreprise);
        $prestation = CommonPrestationPeer::doSelectOne($c, $connexionCom);
        if ($prestation) {
            return $prestation;
        } else {
            return false;
        }
    }

    /**
     * Supprime une prestation d'une entreprise.
     */
    public static function delete($idPrestation, $connexion)
    {
        $prestation = CommonPrestationPeer::retrieveByPk($idPrestation, $connexion);
        CommonPrestationPeer::doDelete($prestation, $connexion);
    }
}
