<?php

namespace Application\Service\Atexo\Entreprise;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAlertePeer;
use Application\Propel\Mpe\CommonConfigurationPlateforme;
use Application\Propel\Mpe\CommonConfigurationPlateformePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_ReferentielVo;
use Exception;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe de manipulation des alertes.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Entreprise_Alertes
{
    public function retreiveAlertes($idInscrit)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAlertePeer::ID_INSCRIT, $idInscrit);
        $alertList = CommonAlertePeer::doSelect($c, $connexionCom);
        if ($alertList) {
            return $alertList;
        } else {
            return [];
        }
    }

    /**
     * supprime une alerte de la table alerte.
     */
    public function deleteAlerte($idAlerte)
    {
        //on est dans une méthode qui fait une modification de la base donc on accède à la base lecture/ecriture
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        CommonAlertePeer::doDelete($idAlerte, $connexionCom);
    }

    /**
     * retourne un objet alerte identifier par l'id.
     */
    public function retreiveAlertById($idAlerte)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAlertePeer::ID, $idAlerte);
        $alerte = CommonAlertePeer::doSelectOne($c, $connexionCom);
        if ($alerte) {
            return $alerte;
        } else {
            return false;
        }
    }

    /**
     * retourne un record set PDO contenant toutes les alertes actives (champs periodicite, cmlCriteria)  et le mail de l'inscrit.
     */
    public function retreiveAlertesJoinInscrit($limit, $offset)
    {
        $typeCreateur = Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE');
        if ('Monday' == date('l')) {
            $periodicite = " in ('".Atexo_Config::getParameter('PERIODICITE_TOUJOURS')."','".Atexo_Config::getParameter('PERIODICITE_TOUTES_SEMAINES')."') ";
        } else {
            $periodicite = "='".Atexo_Config::getParameter('PERIODICITE_TOUJOURS')."'";
        }
        $sql = <<<EOF
SELECT
    SHA1(tmr.xmlCriteria) as xmlCrit,
    tmr.xmlCriteria,
    tmr.periodicite,
    GROUP_CONCAT(CONCAT_WS('|',i.email,tmr.format,tmr.denomination,tmr.plateforme_virtuelle_id) SEPARATOR '#_#') as liste_mail,
    GROUP_CONCAT(tmr.id SEPARATOR ',') as ids
FROM
    T_MesRecherches tmr
LEFT JOIN
    Inscrit i on tmr.id_createur=i.id
WHERE
    tmr.type_createur = '$typeCreateur'
    AND tmr.periodicite $periodicite
    AND i.email IS NOT NULL
    AND i.bloque <> '1'
    AND tmr.alerte = '1'
GROUP BY
    xmlCrit, tmr.periodicite,tmr.plateforme_virtuelle_id
LIMIT $offset, $limit;
EOF;
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $res = $statement->execute();
        $result = [];
        if ($res > 0) {
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        return $result;
    }

    /**
     * retourne un objet CriteriaVo avec les critère de recherche d'une alerte à partir du string xml.
     *
     * @param  $alerte objet alerte
     */
    public function genererCriteriaVoByXml($alerte)
    {
        return self::genererCriteriaVoWithXml($alerte->getXmlcriteria());
    }

    /**
     * retourne un objet CriteriaVo avec les critère de recherche d'une alerte à partir du string xml.
     *
     * @param  $alerte objet alerte
     */
    public function genererCriteriaVoWithXml($xmlAlerte)
    {
        try {
            $modules = CommonConfigurationPlateformePeer::getConfigurationPlateforme();
            $xmlAlerte = Atexo_Util::toUtf8(Atexo_Util::supprimerCodeHtml($xmlAlerte));
            $xml = new \SimpleXMLElement($xmlAlerte);
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            if ($xml) {
                if (((string) $xml->organisme) != 'NULL') {
                    $criteriaVo->setAcronymeOrganisme((string) $xml->organisme);
                }
                if ('NULL' != (string) $xml->entiteAchat) {
                    $criteriaVo->setIdService((string) $xml->entiteAchat);
                    //Lorsque l'on a inclus la descendance du service
                    if ('oui' == (string) $xml->descendant) {
                        $acronyme = (string) $xml->organisme;
                        $service = (string) $xml->entiteAchat;
                        $listOfServicesAllowed = Atexo_EntityPurchase::retrieveAllChildrenServices($service, $acronyme);
                        $criteriaVo->setIdsService($listOfServicesAllowed);
                    }
                }

                if ('NULL' != (string) $xml->categorie) {
                    $criteriaVo->setCategorieConsultation((string) $xml->categorie);
                }
                if ($modules->getLieuxExecution()) {
                    if ('NULL' != (string) $xml->lieuExecution) {
                        $criteriaVo->setLieuxexecution((string) $xml->lieuExecution);
                    }
                }
                //Codes nuts
                if ($modules->getCodeNutLtReferentiel()) {
                    if ('NULL' != (string) $xml->codesNuts) {
                        $criteriaVo->setCodesNuts((string) $xml->codesNuts);
                    }
                }

                if ($modules->getConsultationClause()) {
                    if ((string) $xml->clauseSociale && 'NULL' != (string) $xml->clauseSociale) {
                        $criteriaVo->setClauseSociale((string) $xml->clauseSociale);
                    }
                    if ((string) $xml->atelierProtege && 'NULL' != (string) $xml->atelierProtege) {
                        $criteriaVo->setAtelierProtege((string) $xml->atelierProtege);
                    }
                    if ((string) $xml->clauseEnv && 'NULL' != (string) $xml->clauseEnv) {
                        $criteriaVo->setClauseEnv((string) $xml->clauseEnv);
                    }
                }

                //<!-- Module referentiel CPV
                if (('NULL' != (string) $xml->cpv1) || ('NULL' != (string) $xml->cpv2)) {
                    if ('NULL' != (string) $xml->cpv2) {
                        $criteriaVo->setIdCodeCpv2(utf8_decode((string) $xml->cpv2));
                    }
                }
                //-->
                if ('NULL' != (string) $xml->motCle) {
                    $criteriaVo->setKeyWordAdvancedSearch(trim(utf8_decode((string) $xml->motCle)));
                    $criteriaVo->setSearchModeExact(true);
                }

                if ($modules->getRechercheAvanceeParDomainesActivite()) {
                    if ('NULL' != (string) $xml->domainesActivite) {
                        $criteriaVo->setDomaineActivite((string) $xml->domainesActivite);
                    }
                }

                if ($modules->getRechercheAvanceeParQualification()) {
                    $criteriaVo->setQualification((string) $xml->qualifications);
                }

                if ($modules->getRechercheAvanceeParAgrement()) {
                    $criteriaVo->setAgrements((string) $xml->agrements);
                }
                $allLtRef = (new Atexo_Referentiel_Referentiel())->getAllLtReferentiel();
                $tabRefVo = [];
                $tabRefZoneTexteVo = [];
                foreach ($allLtRef as $ltRef) {
                    $balise = (new Atexo_Referentiel_Referentiel())->getNomAttribue($ltRef->getCodeLibelle());
                    $value = utf8_decode((string) $xml->$balise);
                    if ($value && '' != $value && 'NULL' != $value) {
                        $refVo = new Atexo_Referentiel_ReferentielVo();
                        if (0 == $ltRef->getType()) {
                            $refVo->setValueForReferentielVo($ltRef->getId(), '', $value, $ltRef->getTypeSearch());
                            $tabRefVo[$ltRef->getId()] = $refVo;
                        } else {
                            $refVo->setValueForReferentielVo($ltRef->getId(), $value, '', $ltRef->getTypeSearch(), $ltRef->getType());
                            $tabRefZoneTexteVo[$ltRef->getId()] = $refVo;
                        }
                    }
                }

                $referentielVo = array_merge($tabRefVo, $tabRefZoneTexteVo);
                $criteriaVo->setReferentielVo($referentielVo);
            }

            return $criteriaVo;
        } catch (Exception $e) {
            Prado::log("Erreur chargement XML d'alerte  ".$e, TLogger::ERROR, 'Atexo.Cli.AlerteEntreprises');
        }
    }

    /**
     * Permet de generer l'objet CriteriaVo pour la recherche des consultations a partir de la recherche sauvegardee.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @param Atexo_Consultation_CriteriaVo $criteriaRecherche: objet criteriaVo pour la recherche sauvegardee
     *
     * @return Atexo_Consultation_CriteriaVo: objet criteriaVo pour la recherche des consultations
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function genererCriteriaVoAlerte($criteriaRecherche, $modules = null)
    {
        try {
            if ((null == $modules) || !($modules instanceof CommonConfigurationPlateforme)) {
                $modules = CommonConfigurationPlateformePeer::getConfigurationPlateforme();
            }
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            if ($criteriaRecherche instanceof Atexo_Consultation_CriteriaVo) {
                if ('' != $criteriaRecherche->getAcronymeOrganisme()) {
                    $criteriaVo->setAcronymeOrganisme($criteriaRecherche->getAcronymeOrganisme());
                }
                if ('' != $criteriaRecherche->getIdService()) {
                    $criteriaVo->setIdService($criteriaRecherche->getIdService());
                    //Lorsque l'on a inclus la descendance du service
                    if (true === $criteriaRecherche->getInclureDescendance()) {
                        $acronyme = $criteriaRecherche->getAcronymeOrganisme();
                        $service = $criteriaRecherche->getIdService();
                        $listOfServicesAllowed = Atexo_EntityPurchase::retrieveAllChildrenServices($service, $acronyme);
                        $criteriaVo->setIdsService($listOfServicesAllowed);
                    }
                }

                if ($criteriaRecherche->getIdTypeAvis()) {
                    $criteriaVo->setIdTypeAvis($criteriaRecherche->getIdTypeAvis());
                } else {
                    $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
                }

                if ('' != $criteriaRecherche->getCategorieConsultation()) {
                    $criteriaVo->setCategorieConsultation($criteriaRecherche->getCategorieConsultation());
                }
                if ($modules->getLieuxExecution() && '' != $criteriaRecherche->getLieuxexecution()) {
                    $criteriaVo->setLieuxexecution($criteriaRecherche->getLieuxexecution());
                }
                //Codes nuts
                if ($modules->getCodeNutLtReferentiel()) {
                    if (null != $criteriaRecherche->getCodesNuts()) {
                        $criteriaVo->setCodesNuts($criteriaRecherche->getCodesNuts());
                    }
                }

                if ($modules->getConsultationClause()) {
                    if ($criteriaRecherche->getClauseSociale() && null != $criteriaRecherche->getClauseSociale()) {
                        $criteriaVo->setClauseSociale($criteriaRecherche->getClauseSociale());
                    }
                    if ($criteriaRecherche->getAtelierProtege() && null != $criteriaRecherche->getAtelierProtege()) {
                        $criteriaVo->setAtelierProtege($criteriaRecherche->getAtelierProtege());
                    }
                    if ($criteriaRecherche->getClauseEnv() && null != $criteriaRecherche->getClauseEnv()) {
                        $criteriaVo->setClauseEnv($criteriaRecherche->getClauseEnv());
                    }
                }

                if ('' != $criteriaRecherche->getIdCodeCpv2()) {
                    $criteriaVo->setIdCodeCpv2($criteriaRecherche->getIdCodeCpv2());
                }

                if ('' != $criteriaRecherche->getKeyWordAdvancedSearch()) {
                    $criteriaVo->setKeyWordAdvancedSearch(trim($criteriaRecherche->getKeyWordAdvancedSearch()));
                    $criteriaVo->setSearchModeExact(true);
                }

                if ($modules->getRechercheAvanceeParDomainesActivite() && '' != $criteriaRecherche->getDomaineActivite()) {
                    $criteriaVo->setDomaineActivite($criteriaRecherche->getDomaineActivite());
                }

                if ($modules->getRechercheAvanceeParQualification() && '' != $criteriaRecherche->getQualification()) {
                    $criteriaVo->setQualification($criteriaRecherche->getQualification());
                }

                if ($modules->getRechercheAvanceeParAgrement() && '' != $criteriaRecherche->getAgrements()) {
                    $criteriaVo->setAgrements($criteriaRecherche->getAgrements());
                }

                if ($criteriaRecherche->getReferentielVo()) {
                    $criteriaVo->setReferentielVo($criteriaRecherche->getReferentielVo());
                }

                if($criteriaRecherche->getDenominationAdapte() && is_array($criteriaRecherche->getDenominationAdapte())){
                    foreach($criteriaRecherche->getDenominationAdapte() as $value){
                        $criteriaVo->setDenominationAdapte($value);
                    }
                } elseif ($criteriaRecherche->getOrgDenomination()) {
                    $criteriaVo->setOrgDenomination($criteriaRecherche->getOrgDenomination());
                }
            }

            return $criteriaVo;
        } catch (\Exception $e) {
            Prado::log("Erreur lors de la generation de l'objet criteriaVo pour les alertes ".$e, TLogger::ERROR, 'Atexo.Cli.AlerteEntreprises');
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de la generation de l'objet criteriaVo pour les alertes ".$e->getMessage());
        }
    }
}
