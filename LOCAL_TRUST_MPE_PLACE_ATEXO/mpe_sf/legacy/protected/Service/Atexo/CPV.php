<?php

namespace Application\Service\Atexo;

/**
 * la class CPV.
 *
 * @author LEZ <loubnz.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.6.0
 *
 * @copyright Atexo 2014
 */
class Atexo_CPV
{
    protected $codePrincipal = '';
    protected $codeSecondaire1 = '';
    protected $codeSecondaire2 = '';
    protected $codeSecondaire3 = '';

    /*
     * Set the value of [codePrincipal] column.
     *
     * @param int $value new value
     * @return void
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function setCodePrincipal($value)
    {
        $this->codePrincipal = $value;
    }

    /**
     * Get the [codePrincipal] column value.
     *
     * @return string le code principal
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCodePrincipal()
    {
        return $this->codePrincipal;
    }

    /*
     * Set the value of [codeSecondaire1] column.
     *
     * @param int $value new value
     * @return void
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function setCodeSecondaire1($value)
    {
        $this->codeSecondaire1 = $value;
    }

    /**
     * Get the [codeSecondaire1] column value.
     *
     * @return string code secondaire 1
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCodeSecondaire1()
    {
        return $this->codeSecondaire1;
    }

    /*
     * Set the value of [codeSecondaire2] column.
     *
     * @param int $value new value
     * @return void
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function setCodeSecondaire2($value)
    {
        $this->codeSecondaire2 = $value;
    }

    /**
     * Get the [codeSecondaire2] column value.
     *
     * @return string code secondaire 2
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCodeSecondaire2()
    {
        return $this->codeSecondaire2;
    }

    /*
     * Set the value of [codeSecondaire3] column.
     *
     * @param int $value new value
     * @return void
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function setCodeSecondaire3($value)
    {
        $this->codeSecondaire3 = $value;
    }

    /**
     * Get the [codeSecondaire3] column value.
     *
     * @return string code secondaire 3
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCodeSecondaire3()
    {
        return $this->codeSecondaire3;
    }
}
