<?php

namespace Application\Service\Atexo\Document;

use Application\Service\Atexo\Atexo_Document;

/**
 * Vo pour définir les critères de Atexo_Document_CriteriaVo.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-megalis
 *
 * @copyright Atexo 2016
 */
class Atexo_Document_CriteriaVo
{
    private $idDocument;
    private $nom;
    private $taille;
    private $organisme;
    private $consultationId;
    private $lot;
    private $empreinte;
    private $typeEmpreinte;
    private $idTypeDocument;
    private $dateCreation;
    private $idFichier;

    /**
     * @return mixed
     */
    public function getIdDocument()
    {
        return $this->idDocument;
    }

    /**
     * @param mixed $idDocument
     */
    public function setIdDocument($idDocument)
    {
        $this->idDocument = $idDocument;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @param mixed $taille
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;
    }

    /**
     * @return mixed
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param mixed $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return mixed
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param mixed $consultationRef
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * @return mixed
     */
    public function getLot()
    {
        return $this->lot;
    }

    /**
     * @param mixed $lot
     */
    public function setLot($lot)
    {
        $this->lot = $lot;
    }

    /**
     * @return mixed
     */
    public function getEmpreinte()
    {
        return $this->empreinte;
    }

    /**
     * @param mixed $empreinte
     */
    public function setEmpreinte($empreinte)
    {
        $this->empreinte = $empreinte;
    }

    /**
     * @return mixed
     */
    public function getTypeEmpreinte()
    {
        return $this->typeEmpreinte;
    }

    /**
     * @param mixed $typeEmpreinte
     */
    public function setTypeEmpreinte($typeEmpreinte)
    {
        $this->typeEmpreinte = $typeEmpreinte;
    }

    /**
     * @return mixed
     */
    public function getIdTypeDocument()
    {
        return $this->idTypeDocument;
    }

    /**
     * @param mixed $idTypeDocument
     */
    public function setIdTypeDocument($idTypeDocument)
    {
        $this->idTypeDocument = $idTypeDocument;
    }

    /**
     * @return mixed
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param mixed $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return mixed
     */
    public function getIdFichier()
    {
        return $this->idFichier;
    }

    /**
     * @param mixed $idFichier
     */
    public function setIdFichier($idFichier)
    {
        $this->idFichier = $idFichier;
    }
}
