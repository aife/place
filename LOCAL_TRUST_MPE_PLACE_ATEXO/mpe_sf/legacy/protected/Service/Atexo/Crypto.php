<?php

namespace Application\Service\Atexo;

use App\Service\Crypto\WebServicesCrypto;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Service\Atexo\Blob\Atexo_Blob_DbManagement;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Crypto\Atexo_Crypto_CurlSend;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Service;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;
use AtexoCrypto\Dto\InfosHorodatage;
use AtexoCrypto\Dto\InfosSignature;
use AtexoCrypto\Exception\ServiceException;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe de la cryptographie.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Crypto
{
    /**
     * @return bool
     * @throws Exception
     */
    public function generateCaSerialFile()
    {
        $serial = $this->generateUniqueSerial();
        if ($fp = @fopen(Atexo_Config::getCommonPathDir()
            . Atexo_Config::getParameter('CA_SERIAL'), 'w')) {
            if (!@fwrite($fp, $serial)) {
                @fclose($fp);

                return false;
            }
            if (!@fclose($fp)) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function generateUniqueSerial()
    {
        $ko = true;
        do {
            $serial = '';
            for ($i = 1; $i <= 8; ++$i) {
                $number = random_int(0, 15);
                if (10 == $number) {
                    $serial .= 'A';
                } elseif (11 == $number) {
                    $serial .= 'B';
                } elseif (12 == $number) {
                    $serial .= 'C';
                } elseif (13 == $number) {
                    $serial .= 'D';
                } elseif (14 == $number) {
                    $serial .= 'E';
                } elseif (15 == $number) {
                    $serial .= 'F';
                } else {
                    $serial .= $number;
                }
            }
            if (!$this->isSerialExistInOffre($serial)) {
                $ko = false;
            }
        } while ($ko);

        return $serial;
    }

    /**
     * @param $serial
     * @return bool
     */
    public function isSerialExistInOffre($serial)
    {
        try {
            $c = new Criteria();
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c->add(CommonOffresPeer::UNTRUSTEDSERIAL, $serial);
            $offre = CommonOffresPeer::doSelect($c, $connexionCom);
            if (is_array($offre)) {
                if (0 == count($offre)) {
                    return false;
                }
            } else {
                return false;
            }

            return true;
        } catch (Exception) {
            return false;
        }
    }

    /**
     * @param $infile
     *
     * @return bool
     */
    public function timeStampFile($infile)
    {
        //-- Horodatage du DCE --
        $result = false;
        if (is_file($infile)) {
            $result = $this->horodatageViaCrypto($infile);
        }

        return $result;
        //-- FIN Horodatage du DCE --
    }

    /**
     * @param $data
     * @return array|bool
     * @throws Exception
     */
    public function timeStampData($data)
    {
        $token = md5(uniqid(random_int(0, mt_getrandmax()), true));
        $infile = Atexo_Config::getParameter('COMMON_TMP') . '/file_' . $token
            . session_name() . session_id() . time();
        if (!Atexo_Util::write_file($infile, $data)) {
            throw new Exception('Impossible de créer un fichier dans le répertoire : '
                . Atexo_Config::getParameter('COMMON_TMP') . '/');
        }

        $timeStampVars = [];
        if (is_file($infile)) {
            $timeStampVars = $this->horodatageViaCrypto($infile);
        }

        return $timeStampVars;
    }

    /**
     * Récupère le timestamp à partir d'un jeton d'horodatage.
     *
     * $jetonHorodatage : le jeton d'horodatage,
     * $format : 'fr' pour avoir la date au format français ou 'iso' pour le format iso
     *                           si une chaine de format est inconnu la date est retournée au format ISO
     * @param string $format
     * @return false|string : la date de l'horodatage au format Français dd/mm/YYYY H:m
     * ####################################################################################################
     * @todo :  Potentiellement du code MORT
     * ####################################################################################################
     */
    public function retrieveDateFromHorodatage($jetonHorodatage, $format = 'iso')
    {
        if ('fr' == $format) {
            $formatDate = '%d/%m/%Y %H:%M';
        } else {
            $formatDate = '%Y-%m-%d %H:%M';
        }
        $timeStamp = '';
        $timeStampFileName = Atexo_Config::getParameter('COMMON_TMP') . 'horodatage' . session_id() . time();
        $timeStampFileNameText = Atexo_Config::getParameter('COMMON_TMP') . 'horodatage'
            . session_id() . time() . 'text_envoi_differe.tsr';
        if (!Atexo_Util::write_file($timeStampFileName, base64_decode($jetonHorodatage))) {
            return '';
        }

        // On vérifie le fichier
        $cmdTestHorodatage = Atexo_Config::getParameter('OPENSSL_TS')
            . ' ts -reply -in ' . $timeStampFileName . ' -text';

        system($cmdTestHorodatage . ' > ' . $timeStampFileNameText . ' 2>&1', $result);
        if (0 != $result) {
            //TODO message d'erreur
            return '';
        } else {
            $fp = fopen($timeStampFileNameText, 'r');
            $ts = fread($fp, filesize($timeStampFileNameText));
            fclose($fp);
            $msgs = explode("\n", $ts);
            foreach ($msgs as $msg) {
                $msgTitle = explode('Time stamp:', $msg);
                if ($msgTitle[1]) {
                    $timeStamp = strftime($formatDate, strtotime(Atexo_Util::removeMilliSecond($msgTitle[1])));
                }
            }
        }
        unlink($timeStampFileName);
        unlink($timeStampFileNameText);

        return $timeStamp;
    }

    public function verifyHashValidity($idFichier, $hash, $organisme = null)
    {
        $organisme = (empty($organisme)) ? Atexo_CurrentUser::getOrganismAcronym() : $organisme;
        $blobResource = Atexo_Blob::acquire_lock_on_blob($idFichier, $organisme);
        $file = $blobResource['blob']['pointer'];
        $result = false;
        if ($file) {
            $dbManagement = new Atexo_Blob_DbManagement();
            $fileResult = Atexo_Config::getParameter('COMMON_TMP') . session_id()
                . time() . '_' . $idFichier . '_' . $organisme . '_sha1sum';
            system('sha1sum ' . $file . ' > ' . $fileResult . ' 2>&1', $verif);
            Prado::log(
                ' Commande verification sha1sum ' . $file . ' > ' . $fileResult . ' 2>&1',
                TLogger::INFO,
                'Atexo'
            );
            if (0 === $verif) {
                $resultContent = $dbManagement->db_read_file($fileResult);
                $computedHash = substr($resultContent, 0, 40);
                if (0 == strcmp(strtoupper($computedHash), strtoupper($hash))) {
                    $result = true;
                    Atexo_Util::deleteLocalfile($fileResult);
                }
            }
        }

        return $result;
    }

    /**
     * Verifie :
     * 1) la validité de la chaine de certificat
     * 2) Vérification la révoquation du certificat
     * 3) verification des dates.
     *
     * à partir d'une signature
     *
     *  @param : $signature la ignature
     *
     *  @return : un tableau de longueur 3 : array(resultat validité de la chaine de certificat,
     * résultat Vérification la révoquation du certificat, resultat verification des dates )
     *            Une valeur à 1 signifie que le certificat est valide pour ce test. 0 sinon.
     */
    public function verifierCertificatDeSignature($signature, $isFile = false, $typeSignature = null, $certificat = false)
    {
        if ('XML' == $typeSignature) {
            try {
                if ($certificat) {
                    $certificat = $signature;
                } else {
                    $certificat = (new Atexo_Crypto())->getCertificatFromSignature($signature, false, false, 'XML');
                }
                //Initialisation
                $arrayResultatVerification = [1, 1, 1];
                $cerificatFileName = Atexo_Config::getParameter('COMMON_TMP').'/certificat_'.session_name().session_id().time();
                $certificat = "-----BEGIN CERTIFICATE-----\n".$certificat."\n-----END CERTIFICATE-----";
                if (!Atexo_Util::write_file($cerificatFileName, $certificat)) {
                    throw new Exception('Impossible de créer un fichier dans le répertoire : '.Atexo_Config::getParameter('COMMON_TMP').'/');
                }

                //verification de la chaine de certificat
                $arrayResultatVerification[0] = self::verifyChaineCertf($cerificatFileName);

                //Verification de la Révocation
                $arrayResultatVerification[1] = self::verifyRevocationCertif($cerificatFileName);

                //Dates de validité
                $arrayResultatVerification[2] = self::verifyDateValiditeCertif($cerificatFileName);

                @unlink($cerificatFileName);

                return $arrayResultatVerification;
            } catch (\Exception) {
                echo 'erreur dans la verification du certificat de signature.';
                exit;
            }
        } else {
            try {
                //Initialisation
                $arrayResultatVerification = [1, 1, 1];
                if ($isFile) {
                    $cerificatFileName = self::getCertificatFromSignatureFile($signature);
                } else {
                    $cerificatFileName = self::getCertificatFromSignature($signature);
                }

                //verification de la chaine de certificat
                $arrayResultatVerification[0] = self::verifyChaineCertf($cerificatFileName);

                //Verification de la Révocation
                $arrayResultatVerification[1] = self::verifyRevocationCertif($cerificatFileName);

                //Dates de validité
                $arrayResultatVerification[2] = self::verifyDateValiditeCertif($cerificatFileName);

                @unlink($cerificatFileName);

                return $arrayResultatVerification;
            } catch (\Exception) {
                echo 'erreur dans la verification du certificat de signature.';
                exit;
            }
        }
    }

    public function getCertificatFromSignature($signature, $onErrorThrowException = true, $file = false, $typeSignature = null)
    {
        if ('XML' == $typeSignature) {
            $certificatFileName = Atexo_Config::getParameter('COMMON_TMP').'/certificat_'.session_name().session_id().time();

            $pos1 = strpos($signature, 'ds:X509Certificate');
            $pos2 = strpos($signature, '/ds:X509Certificate');
            $certificat = substr($signature, $pos1, $pos2 - $pos1);

            $pos1 = strpos($certificat, '>');
            $pos2 = strpos($certificat, '<');
            $certificat = substr($certificat, $pos1 + 1, $pos2 - $pos1 - 1);
            if ($file) {
                $certificat = "-----BEGIN CERTIFICATE-----\n".$certificat."\n-----END CERTIFICATE-----";
                if (!Atexo_Util::write_file($certificatFileName, $certificat)) {
                    throw new Exception('Impossible de créer un fichier dans le répertoire : '.Atexo_Config::getParameter('COMMON_TMP').'/'.' type signature XML, et certificatFileName est : '.$certificatFileName.' et certificat est : '.$certificat);
                }

                return $certificatFileName;
            } else {
                return $certificat;
            }
        } else {
            $signatureFileName = Atexo_Config::getParameter('COMMON_TMP').'/signature_'.session_name().session_id().time();
            if (!Atexo_Util::write_file($signatureFileName, $signature)) {
                throw new Exception('Impossible de créer un fichier dans le répertoire : '.Atexo_Config::getParameter('COMMON_TMP').'/'.' type signature p7s, et signatureFileName est : '.$signatureFileName.' et signature est : '.$signature);
            }
            $certificat = self::getCertificatFromSignatureFile($signatureFileName, $onErrorThrowException);
            @unlink($signatureFileName);

            return $certificat;
        }
    }

    public function getCertificatFromSignatureFile($signatureFileName, $onErrorThrowException = true)
    {
        $certificatFileName = Atexo_Config::getParameter('COMMON_TMP').'/certificat_'.session_name().session_id().time();
        $commande = Atexo_Config::getParameter('OPENSSL').' smime -verify -in '.escapeshellarg($signatureFileName).' -noverify -nosigs -signer '.
                    $certificatFileName.' -inform PEM -content /dev/null';
        system($commande, $resultat);
        //      @unlink($certificatFileName);
        if (0 == $resultat) {
            return $certificatFileName;
        } else {
            $content = file_get_contents($signatureFileName);
            $content = str_replace("-----BEGIN PKCS7-----\n", '', $content);
            $content = str_replace("\n-----END PKCS7-----", '', $content);
            if (!Atexo_Util::write_file($signatureFileName.'_der', $content)) {
                throw new Exception('Impossible de créer un fichier dans le répertoire : '.Atexo_Config::getParameter('COMMON_TMP').'/');
            }
            $commande = Atexo_Config::getParameter('OPENSSL').' smime -verify -in '.escapeshellarg($signatureFileName.'_der').' -noverify -nosigs -signer '.$certificatFileName.' -inform DER -content /dev/null';
            system($commande, $resultat);
            if (0 == $resultat) {
                return $certificatFileName;
            }
        }
        if ($onErrorThrowException) {
            return 'Error';
        //throw new Exception($commande." Impossible d'obtenir le certificat à partir de la signature");
        } else {
            return 'Error';
        }
    }

    /**
     * Verifie l'horodatage du fichier.
     *
     * @param : $jeton : jeton d'horodatage : en base64 ou un fichier
     * ####################################################################################################
     * @todo : Possible encore un OPEN SSL utiliser ici. Cela demande une analayse plus complet
     * @todo : Vérifier au niveau des Archives.
     * ####################################################################################################
     */
    public function verifyHorodatage($jeton, $fileOrString)
    {
        $file = null;
        $deleteJeton = false;
        $deleteFile = false;
        if (!is_file($jeton)) {
            $donneesJeton = $jeton;
            $jeton = Atexo_Config::getParameter('COMMON_TMP').'/jeton_'.session_name().session_id().time().'.tsr';
            Atexo_Util::write_file($jeton, base64_decode($donneesJeton));
            $deleteJeton = true;
        }
        if (!is_file($fileOrString)) {
            $string = $fileOrString;
            $file = Atexo_Config::getParameter('COMMON_TMP').'/file_'.session_name().session_id().time().'';
            Atexo_Util::write_file($file, $string);
            $deleteFile = true;
        }
        $verify = Atexo_Config::getParameter('OPENSSL_TS').' ts -verify -data '.$file.' -in '.$jeton.' -CApath '.Atexo_Config::getParameter('TRUST_DIR');
        $verifyTsrTsvErrorFile = Atexo_Config::getParameter('COMMON_TMP').'/verify_tsr_tsv_error';
        system($verify.'  > '.$verifyTsrTsvErrorFile.' 2>&1', $tsVerify);
        if ($deleteJeton && is_file($jeton)) {
            unlink($jeton);
        }
        if ($deleteFile && is_file($file)) {
            unlink($file);
        }

        if (0 != $tsVerify) {
            return false;
        } else {
            if (is_file($verifyTsrTsvErrorFile)) {
                unlink($verifyTsrTsvErrorFile);
            }
        }

        return true;
    }

    /**
     * @return bool|int
     * @throws Exception
     */
    public static function generateCompanyPkiOpensslConfigFile()
    {
        if (!is_file(Atexo_Config::getParameter('PKI_ENTREPRISE_OPENSSL_CONFIG_TEMPLATE'))) {
            throw new Exception('Fichier de configuration modele introuvable');
        }
        $strTemplate = file_get_contents(Atexo_Config::getParameter('PKI_ENTREPRISE_OPENSSL_CONFIG_TEMPLATE'));
        $strTemplate = str_replace('__DIR__', Atexo_Config::getParameter('DIR_PKI_ENTREPRISE'), $strTemplate);
        $strTemplate = str_replace('__CERTS_DB__', Atexo_Config::getParameter('PKI_ENTREPRISE_OPENSSL_DB'), $strTemplate);
        $strTemplate = str_replace('__CERT_PATH__', Atexo_Config::getParameter('BASE_ROOT_DIR').Atexo_Config::getParameter('COMMON_DIRECTORY').Atexo_Config::getParameter('MARCHES_CA_CERT'), $strTemplate);
        $strTemplate = str_replace('__CERT_PKEY__', Atexo_Config::getParameter('BASE_ROOT_DIR').Atexo_Config::getParameter('COMMON_DIRECTORY').Atexo_Config::getParameter('MARCHES_CA_KEY'), $strTemplate);

        return file_put_contents(Atexo_Config::getParameter('PKI_ENTREPRISE_OPENSSL_CONFIG'), $strTemplate);
    }

    /**
     * @return bool
     */
    public static function generateCompanyPkiOpensslDbFile()
    {
        if (!is_file(Atexo_Config::getParameter('PKI_ENTREPRISE_OPENSSL_DB'))) {
            return touch(Atexo_Config::getParameter('PKI_ENTREPRISE_OPENSSL_DB'));
        }

        return true;
    }

    public function verifyChaineCertf($cerificatFileName)
    {
        //verification de la chaine de certificat
        $commande = Atexo_Config::getParameter('OPENSSL').' verify -CApath '.Atexo_Config::getParameter('TRUST_DIR').'  '.escapeshellarg($cerificatFileName);
        $verificationResultatFile = Atexo_Config::getParameter('COMMON_TMP').'/'.'chaineVerif_'.session_name().session_id().time();
        system($commande.' > '.$verificationResultatFile.' 2>&1');

        $fp = fopen($verificationResultatFile, 'r');
        $resultatVerification = fread($fp, filesize($verificationResultatFile));
        fclose($fp);
        $pos1 = strpos(str_replace(' ', '', $resultatVerification), (string) str_replace(' ', '', $cerificatFileName));
        $pos2 = strpos(str_replace(' ', '', $resultatVerification), 'OK');
        if (trim((str_replace(' ', '', $resultatVerification))) == trim((str_replace(' ', '', $cerificatFileName).':OK')) || (false !== $pos1 && false !== $pos2)) {
            $verif = 0;
        } else {
            //verification de la chaine de certificat dans TRUST_DIR_ALTERNATE
            $commandeAlternate = Atexo_Config::getParameter('OPENSSL').' verify -CApath '.Atexo_Config::getParameter('TRUST_DIR_ALTERNATE').'  '.escapeshellarg($cerificatFileName);
            $verificationResultatAlternateFile = Atexo_Config::getParameter('COMMON_TMP').'/'.'chaineVerifAlternate_'.session_name().session_id().time();
            system($commandeAlternate.' > '.$verificationResultatAlternateFile.' 2>&1');

            $fpAlternate = fopen($verificationResultatAlternateFile, 'r');
            $resultatVerificationAlternate = fread($fpAlternate, filesize($verificationResultatAlternateFile));
            fclose($fpAlternate);
            $pos1 = strpos(str_replace(' ', '', $resultatVerificationAlternate), (string) str_replace(' ', '', $cerificatFileName));
            $pos2 = strpos(str_replace(' ', '', $resultatVerificationAlternate), 'OK');
            if (trim((str_replace(' ', '', $resultatVerificationAlternate))) == trim((str_replace(' ', '', $cerificatFileName).':OK')) || (false !== $pos1 && false !== $pos2)) {
                $verif = 0;
            } else {
                $verif = 2;
            }
            unlink($verificationResultatAlternateFile);
        }

        unlink($verificationResultatFile);

        return $verif;
    }

    public function verifyRevocationCertif($cerificatFileName)
    {
        //Verification de la Révocation
        $verificationResultatFile = Atexo_Config::getParameter('COMMON_TMP').'/'.'revoqueVerif_'.session_name().session_id().time();
        $commande = Atexo_Config::getParameter('OPENSSL').' verify -CApath '.Atexo_Config::getParameter('TRUST_DIR').' -crl_check '.escapeshellarg($cerificatFileName);
        system($commande.' > '.$verificationResultatFile.' 2>&1');
        $fp = fopen($verificationResultatFile, 'r');
        $resultatVerification = fread($fp, filesize($verificationResultatFile));
        fclose($fp);
        $pos1 = strpos(str_replace(' ', '', $resultatVerification), (string) str_replace(' ', '', $cerificatFileName));
        $pos2 = strpos(str_replace(' ', '', $resultatVerification), 'OK');
        if (strstr($resultatVerification, 'certificate revoked')) {
            $resVerif = 2;
        } elseif (trim((str_replace(' ', '', $resultatVerification))) == trim((str_replace(' ', '', $cerificatFileName).':OK')) || (false !== $pos1 && false !== $pos2)) {
            $resVerif = 0;
        } else {
            //verification de la Révocation dans TRUST_DIR_ALTERNATE
            $commandeAlternate = Atexo_Config::getParameter('OPENSSL').' verify -CApath '.Atexo_Config::getParameter('TRUST_DIR_ALTERNATE').' -crl_check '.escapeshellarg($cerificatFileName);
            $verificationResultatAlternateFile = Atexo_Config::getParameter('COMMON_TMP').'/'.'revoqueAlternate_'.session_name().session_id().time();
            system($commandeAlternate.' > '.$verificationResultatAlternateFile.' 2>&1');
            $fpAlternate = fopen($verificationResultatAlternateFile, 'r');
            $resultatVerificationAlternate = fread($fpAlternate, filesize($verificationResultatAlternateFile));
            fclose($fpAlternate);
            $pos1 = strpos(str_replace(' ', '', $resultatVerificationAlternate), (string) str_replace(' ', '', $cerificatFileName));
            $pos2 = strpos(str_replace(' ', '', $resultatVerificationAlternate), 'OK');
            if (trim((str_replace(' ', '', $resultatVerificationAlternate))) == trim((str_replace(' ', '', $cerificatFileName).':OK')) || (false !== $pos1 && false !== $pos2)) {
                $resVerif = 0;
            } elseif (strstr($resultatVerificationAlternate, 'certificate revoked')) {
                $resVerif = 2;
            } else {
                $resVerif = 1;
            }
            unlink($verificationResultatAlternateFile);
        }

        unlink($verificationResultatFile);

        return $resVerif;
    }

    public function verifyDateValiditeCertif($cerificatFileName)
    {
        //Dates de validité
        $verificationResultatFile = Atexo_Config::getParameter('COMMON_TMP').'/'.'datesVerif_'.session_name().session_id().time();
        $commande = Atexo_Config::getParameter('OPENSSL').' x509 -in '.escapeshellarg($cerificatFileName).' -enddate -startdate -noout';
        system($commande.' > '.$verificationResultatFile.' 2>&1');

        $fp = fopen($verificationResultatFile, 'r');
        $resultatVerification = fread($fp, filesize($verificationResultatFile));
        fclose($fp);
        $resultatVerification = explode("\n", $resultatVerification);
        unlink($verificationResultatFile);

        if ($resultatVerification[1]) {
            $dateDebut = explode('=', $resultatVerification[1]);
            $dateDebut = date('Y-m-d H:i:s', strtotime($dateDebut[1]));

            $dateFin = explode('=', $resultatVerification[0]);
            $dateFin = date('Y-m-d H:i:s', strtotime($dateFin[1]));

            if ($dateDebut <= date('Y-m-d H:i:s') && $dateFin >= date('Y-m-d H:i:s')) {
                $resVerif = 0;
            } else {
                $resVerif = 2;
            }
        } else {
            $resVerif = 1;
        }

        return $resVerif;
    }

    public function formatDate($date)
    {
        $arrayDate = explode('.', $date);
        if ($arrayDate[1]) {
            $arrayDateSansP = explode(' ', $arrayDate[1]);
            $newDate = $arrayDate[0].' '.$arrayDateSansP[1].' '.$arrayDateSansP[2];
        } else {
            $newDate = $date;
        }

        return $newDate;
    }

    public function verifySignatureValidity($idFichierBlob, $idOffre, $fichierEnveloppe, $calledFrom = false)
    {
        $verifHash = null;
        $verifIntegrite = null;
        if ($fichierEnveloppe instanceof CommonFichierEnveloppe) {
            $organisme = $fichierEnveloppe->getOrganisme();
            $signatureValide = 0;
            if (Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                $signatureInfo = $fichierEnveloppe->getSignatureInfos();
                if ($signatureInfo) {
                    $infoSignature = json_decode($signatureInfo, true, 512, JSON_THROW_ON_ERROR);
                    if (isset($infoSignature['signatureValide'])) {
                        $signatureValide = 'true' == $infoSignature['signatureValide'] ? 0 : -1;
                    } else {
                        $signatureValide = -2;
                    }
                    if ('entreprise' == $calledFrom) {
                        return $signatureValide;
                    }
                }
            }

            $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($idOffre, $organisme);
            if ($offre) {
                if ((!empty($offre->getHorodatageHashFichiers()) || !empty($offre->getIdBlobHorodatageHash())) && Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                    //Verification de l'integrité de la signature
                    $verifHash = true;
                    $verifIntegrite = self::verifyRejeuSignature($fichierEnveloppe);
                } elseif ($idFichierBlob) {
                    //Verification du hash
                    $responsesXml = $offre->getXmlString();
                    $hashSignedFile = (new Atexo_Entreprise_Reponses())->getEmprienteFichierFromXmlReponse($responsesXml, $fichierEnveloppe);
                    $verifHash = self::verifyHashValidity($idFichierBlob, $hashSignedFile, $organisme);
                    Prado::log(' Verification du hash resultat '.$verifHash.' du fichier idFichier '.$idFichierBlob.' hashSignedFile '.$hashSignedFile, TLogger::INFO, 'Atexo');
                    $verifIntegrite = self::verifyRejeuHorodatage($offre->getXmlString(), $offre->getHorodatage());
                }

                if (-2 == $signatureValide) {
                    return -2;
                } elseif (0 == $signatureValide && $verifHash && $verifIntegrite) {
                    return 0;
                } else {
                    return -1;
                }
            }
        }
    }

    public function verifyRejeuHorodatage($xmlReponsesAnnonce, $jetonHorodatage)
    {
        $pathOrgTmp = Atexo_Config::getParameter('COMMON_TMP');
        $sessId = session_id();
        $token = md5(uniqid(random_int(0, mt_getrandmax()), true));
        $infile = Atexo_Config::getParameter('COMMON_TMP').'/file_'.$token.session_name().$sessId.time();
        $infileTsr = $infile.'.tsr';

        if (!Atexo_Util::write_file($infile, $xmlReponsesAnnonce)) {
            throw new Exception('Impossible de créer un fichier dans le répertoire : '.Atexo_Config::getParameter('COMMON_TMP').'/');
        }

        if (!Atexo_Util::write_file($infileTsr, base64_decode($jetonHorodatage))) {
            throw new Exception('Impossible de créer un fichier dans le répertoire : '.Atexo_Config::getParameter('COMMON_TMP').'/');
        }
        $verify = Atexo_Config::getParameter('OPENSSL_TS')." ts -verify -data $infile -in $infile.tsr -CApath ".Atexo_Config::getParameter('TRUST_DIR');
        $tsvErrorFile = $pathOrgTmp.$sessId.'_tsv_error';
        system($verify.'  > '.$tsvErrorFile.' 2>&1', $tsVerify);
        if (0 != $tsVerify) {
            Prado::log(" verification du jeton d'horodatage verify ".$verify, TLogger::ERROR, 'Atexo');
            if (is_file($infile)) {
                unlink($infile);
            }
            if (is_file($infileTsr)) {
                unlink($infileTsr);
            }

            return [$tsVerify, null];
        } else {
            if (is_file($tsvErrorFile)) {
                unlink($tsvErrorFile);
            }

            return true;
        }
    }

    /**
     * Retourne l'hordatatge d'un fichier ou le contenu d'un fichier.
     *
     * @param string $data la donnee à horodater,Boolean $file si la donnee de type fichier ou contenu
     *
     * @return array $arrayHorodatage contient les informations de l'horodatage s'il est généré si non on retourne false
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 2.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getArrayHorodatage($data, $file = false)
    {
        $arrayHorodatage = null;
        $tryHorodatage = Atexo_Config::getParameter('TRY_HORODATAGE');
        $horodatageSuccess = false;
        while ($tryHorodatage && !$horodatageSuccess) {
            $crypto = new Atexo_Crypto();
            if ($file) {
                $arrayHorodatage = $crypto->timeStampFile($data);
            } else {
                $arrayHorodatage = $crypto->timeStampData($data);
            }
            if (is_array($arrayHorodatage) && count($arrayHorodatage)) {
                $horodatageSuccess = true;
            }
            --$tryHorodatage;
        }
        if ($horodatageSuccess) {
            return $arrayHorodatage;
        } else {
            return false;
        }
    }

    /**
     * Permet de verifier si le mode applet est activé pour l'agent.
     *
     * @param void
     *
     * @return bool
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public static function isModeAppletAgentActive()
    {
        return !Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') || Atexo_Module::isEnabled('ModeApplet');
    }

    /**
     * Map Offres to dechiffrement.
     *
     * @param int   $idAgent
     * @param array $params
     *
     * @return Dehiffrement
     */
    public function getTelechargementCrypto(array $offres, $idAgent, $organisme, $enLigne = true)
    {
        $logger = Atexo_LoggerManager::getLogger('crypto');
        $params = [];
        $params['MpeURLPrivee'] = Atexo_Config::getParameter('MPE_WS_URL');
        $params['ServeurCryptoURLPublic'] = Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('URL_CRYPTO'));
        $params['MpeFilePath'] = Atexo_Config::getParameter('BASE_ROOT_DIR').$organisme.Atexo_Config::getParameter('FILES_DIR');
        $params['MpeLogin'] = Atexo_Config::getParameter('MPE_WS_LOGIN_SRV_CRYPTO');
        $params['MpePassword'] = Atexo_Config::getParameter('MPE_WS_PASSWORD_SRV_CRYPTO');
        $params['Format'] = Atexo_Config::getParameter('FORMAT_RETOUR_CRYPTO');
        $params['EnLigne'] = $enLigne;

        $service = new Atexo_Crypto_Service(
            Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('URL_CRYPTO')),
            $logger,
            Atexo_Config::getParameter('ENABLE_SIGNATURE_SERVER_V2'),
            null,
            Atexo_Util::getSfService(WebServicesCrypto::class)
        );

        try {
            return $service->demandeDechiffrement($offres, $idAgent, null, $params);
        } catch (ServiceException $e) {
            $logger->error("Erreur Lors d'appel au ws de dechiffrement".$e->errorCode.' - '.$e->etatObjet.' - '.$e->message);
        }

        return false;
    }


    /**
     * Permet de verifier  la validité de la signature.
     *
     * @param CommonFichierEnveloppe $fichierEnveloppe
     *
     * @return bool
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function verifyRejeuSignature($fichierEnveloppe)
    {
        $logger = Atexo_LoggerManager::getLogger('crypto');
        $signatureValide = false;
        try {
            if ($fichierEnveloppe instanceof CommonFichierEnveloppe) {
                $service = new Atexo_Crypto_Service(
                    Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('URL_CRYPTO')),
                    $logger,
                    0, // ENABLE_SIGNATURE_SERVER_V2 pas prise en compte au niveau du depot
                    Atexo_Config::getParameter('UID_PF_MPE'),
                    Atexo_Util::getSfService(WebServicesCrypto::class)
                );
                $organisme = $fichierEnveloppe->getOrganisme();
                $token = md5(uniqid(random_int(0, mt_getrandmax()), true));
                $filePath = Atexo_Config::getParameter('COMMON_TMP').'/file_'.$token.session_name().session_id().time();
                Atexo_Blob::copyBlob($fichierEnveloppe->getIdBlob(), $filePath, $organisme);
                if (is_file($filePath)) {
                    $typeSignature = $fichierEnveloppe->getTypeSignatureFichier();
                    if ($typeSignature === Atexo_Config::getParameter('PADES_SIGNATURE')) {
                        $logger->info('Appele au ws de vérification de la signature de type =>'.$typeSignature);
                        $result = $service->verifierSignaturePades($filePath);
                    } else {
                        $typeSignature = Atexo_Config::getParameter('INCONNUE_SIGNATURE');
                        $hash = sha1_file($filePath);
                        $hash256 = hash_file('sha256', $filePath);
                        $logger->info('Appele au ws de vérification de la signature de type =>'.$typeSignature);
                        $result = $service->verifierSignature($hash, $fichierEnveloppe->getSignatureFichier(), $typeSignature, $hash256);
                    }
                    if ($result instanceof InfosSignature) {
                        $signatureValide = boolval($result->getSignatureValide());
                    }
                }
            }
        } catch (ServiceException $e) {
            $logger->error("Erreur Lors d'appel au ws de dechiffrement ".$e->errorCode.' - '.$e->etatObjet.' - '.$e->message);
        }

        return $signatureValide;
    }

    /**
     * @param $resources
     * @description Ce paramètre peux prendre plusieurs valeurs :
     * Soit un chemin de fichier
     * Soit une date
     * Soit une string exemple : "DCE NON RENSEIGNEE"
     *
     * @return bool
     */
    public function horodatageViaCrypto($resources)
    {
        $resultat = false;
        $hash256 = null;
        if (file_exists($resources)) {
            $hash256 = hash_file('sha256', $resources);
        } elseif (!empty($resources)) {
            $hash256 = hash('sha256', $resources);
        }

        if (null !== $hash256) {
            try {
                $cryptoService = Atexo_Util::getSfService('atexo_crypto.crypto');
                $infosHorodatage = $cryptoService->demandeHorodatage($hash256);
                $untrusteddateOffre = '';

                if ($infosHorodatage instanceof InfosHorodatage) {
                    $untrusteddateOffre = (new \DateTime(
                        date('Y-m-d H:i:s', $infosHorodatage->getHorodatage())
                    ))->format('Y-m-d H:i:s');
                    $resultat['horodatage'] = $infosHorodatage->getJetonHorodatageBase64();
                    $resultat['untrustedDate'] = $untrusteddateOffre;
                }
            } catch (Exception $exception) {
                Prado::log('Exception : '.$exception->getMessage(), TLogger::ERROR, 'Atexo');
            }
        } else {
            Prado::log('Horodatage : error ', TLogger::ERROR, 'Atexo');
        }

        return $resultat;
    }
}
