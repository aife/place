<?php

namespace Application\Service\Atexo;

use App\Service\App\XliffGenerator;
use App\Service\Twig\TwigService;
use Application\Cli\Cli_Clean;
use Application\Propel\Mpe\CommonLangueQuery;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\TPage;

/**
 * Classe de manipulation des langues de la plate-forme.
 *
 * @author     ZAKI Anas <anas.zaki@atexo.com>
 * @copyright  Atexo 2008
 *
 * @version    1.0
 *
 * @since      MPE-3.0
 */
class Atexo_Languages extends TPage
{
    public static function readLanguageFromSession()
    {
        return Atexo_CurrentUser::readFromSession('lang');
    }

    /**
     * Permet de charger le catalogue contenant les messages de la langue choisie.
     *
     * @param unknown_type $calledFromPage
     */
    public static function setLanguageCatalogue($calledFromPage)
    {
        $languesActive = Atexo_Languages::retrieveActiveLangages();
        $arrayLanguesActives = [];
        $arrayLanguesActives[] = 'atx';
        foreach ($languesActive as $lang) {
            self::generateXliffByLanguage($lang->getLangue(), $calledFromPage);
            $arrayLanguesActives[] = $lang->getLangue();
        }

        $globalization = Prado::getApplication()->Modules['globalization'];
        if ('agenthelios' === $calledFromPage || 'gestionPub' == $calledFromPage || 'annuaire' == $calledFromPage) {
            $globalization->setTranslationCatalogue('messages.fr');
        }
        if ('entreprise' === $calledFromPage || 'agent' === $calledFromPage || 'admin' === $calledFromPage) {
            if (!isset($_GET['lang']) or !in_array($_GET['lang'], $arrayLanguesActives)) {
                if (!(Atexo_CurrentUser::readFromSession('lang'))) {
                    $langueEnSession = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
                } else {
                    $langueEnSession = Atexo_CurrentUser::readFromSession('lang');
                }
            } else {
                $langueEnSession = Atexo_Util::atexoHtmlEntities($_GET['lang']);
            }

            // Enregistrement en session
            Atexo_CurrentUser::writeToSession('lang', $langueEnSession);
            if (Atexo_Module::isEnabled('MultiLinguismeEntreprise')) {
                $globalization->setTranslationCatalogue('messages.'.$langueEnSession);
            } else {
                $globalization->setTranslationCatalogue('messages.fr');
            }
        }
    }

    public static function retrieveActiveLangages()
    {
        $commonLangueQuery = new CommonLangueQuery();

        return $commonLangueQuery->findActiveLanguages();
    }

    /**
     * Vérifie qu'aucune modification de traduction n'a été faite.
     *
     * @param array  $domv2    Tableau de traductions à comparer
     * @param string $language language iso
     */
    public static function hasChanged(array $domv2, string $language): bool
    {
        $res = false;
        $md5archive = null;
        try {
            $md5 = md5(serialize($domv2));
            $pathmd5 = Atexo_Config::getParameter('COMMON_TMP') . '/language/';
            try {
                if (Atexo_Config::getParameter('APP_ENV') == 'dev') {
                    $pathmd5 = Atexo_Config::getParameter('BASE_ROOT_DIR') . '/language/';
                }
            } catch (\Exception $e) {
                $logger = Atexo_LoggerManager::getLogger('app');
                $logger->error(
                    sprintf(
                        'Erreur recuperation path file message md5  "%s" : %s !',
                        $language,
                        $e->getMessage()
                    )
                );
            }
            if (!is_dir($pathmd5)) {
                mkdir($pathmd5);
            }
            $pathfilemd5 = $pathmd5.'/'.$language.'.md5';
            if (file_exists($pathfilemd5)) {
                $md5archive = file_get_contents($pathfilemd5);
            }

            if ($md5 !== $md5archive) {
                if (is_writable($pathfilemd5) || !file_exists($pathfilemd5)) {
                    file_put_contents($pathfilemd5, $md5);
                }
                $res = true;
            }
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error(
                sprintf(
                    'Erreur generation fichiers md5 pour la langue "%s" : %s !',
                    $language,
                    $e->getMessage()
                )
            );
        }

        return $res;
    }

    /**
     * Génére les fichiers de traductions.
     *
     * @param string $language Iso code de la langue
     */
    public static function generateXliffByLanguage(string $language, ?string $calledFromPage = null): void
    {
        try {
            $file = Atexo_Config::getParameter('ATEXO_MPE_PATH')."/protected/config/messages/messages.$language.xml";
            if (!is_file($file) || Cli_Clean::CALL_FROM_CLI === $calledFromPage) {
                $domv2 = Atexo_Util::XliffToArray(
                    Atexo_Config::getParameter(
                        'ATEXO_MPE_PATH'
                    )."/protected/config/messages/messages.default.$language.xml"
                );

                $sql = 'SELECT * FROM `configuration_messages_traduction` WHERE langue_id = (SELECT id_langue FROM Langue WHERE langue = \''.$language.'\')';
                $statement = Atexo_Db::getLinkCommon(true)->query($sql);
                if ($statement->rowCount() > 0) {
                    while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                        $domv2[$row['source']] = $row['target'];
                    }
                }

                if (self::hasChanged($domv2, $language) || !is_file($file)) {
                    $xliffGenerator = Atexo_Util::getSfService(XliffGenerator::class);
                    $xml = $xliffGenerator->getXliff($domv2, $language);

                    $xml->asXML($file);

                    $cheminFileMessageMpeSf = Atexo_Config::getMessageFileMpeSf($language);
                    $xml->addAttribute('xmlns', 'urn:oasis:names:tc:xliff:document:1.2');
                    $xml->asXML($cheminFileMessageMpeSf);
                }
            }
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Erreur generation fichiers traduction : '.$e->getMessage());
        }
    }

    protected static function globRecursive($pattern, $flags = 0)
    {
        $files = glob($pattern, $flags);
        foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
            $files = array_merge($files, self::globRecursive($dir.'/'.basename($pattern), $flags));
        }

        return $files;
    }

    public static function generateLocalizedJsFiles($calledFromPage)
    {
        $activeLanguages = Atexo_Languages::retrieveActiveLangages();

        $rootPath = Atexo_Controller_Front::getRootPath().DIRECTORY_SEPARATOR;
        $mpePradoDir = Atexo_Config::getMpePradoDirectory();
        $themesDir = '/../public/themes/';
        $xmlFiles = [];

        chdir($rootPath.$mpePradoDir.$themesDir);
        $filesList = self::globRecursive('*.js.php', GLOB_BRACE);
        $service = Atexo_Util::getSfService(TwigService::class);
        foreach ($activeLanguages as $activeLanguage) {
            $lang = $activeLanguage->getLangue();
            $messagesFilePath = $rootPath.$mpePradoDir.'/protected/config/messages/messages.'.$lang.'.xml';
            $xmlFiles[$lang] = Atexo_Util::XliffToArray($messagesFilePath);

            foreach ($filesList as $file) {
                $jsFilePath = $rootPath.$mpePradoDir.$themesDir.$file;
                $destinationJsFilePath = str_replace('.js.php', '.'.$lang.'.js', $jsFilePath);
                $tempJsFilePath = str_replace('.js.php', '.'.$lang.'.js.php', $jsFilePath);
                $fileContent = file_get_contents($jsFilePath);
                $xml = $xmlFiles[$lang];
                $js = preg_replace_callback(
                    "/<\?php Prado::localize\('([^']+)'\) \?./",
                    function ($matches) use ($xml, $service) {
                        $match = $xml[$matches['1']];
                        if (!empty($match)) {
                            $match = $service->twigEscapeFilter($match, 'js');
                        }

                        return $match;
                    },
                    $fileContent
                );

                file_put_contents(
                    $tempJsFilePath,
                    '<?php'.PHP_EOL
                    .'use Application\Service\Atexo\Atexo_Util;'.PHP_EOL
                    . 'use Application\Service\Atexo\Weka\Atexo_Weka_Generateur;' . PHP_EOL
                    .'use Application\Service\Atexo\Atexo_Config;  ?>'.PHP_EOL.$js
                );

                ob_start();
                include $tempJsFilePath;
                $js = ob_get_clean();
                $js = str_replace('<?', '', $js);
                file_put_contents($destinationJsFilePath, $js);
                unlink($tempJsFilePath);
            }
        }

        foreach ($filesList as $file) {
            $jsFilePath = $rootPath.$mpePradoDir.$themesDir.$file;
            if (is_file($jsFilePath)) {
                unlink($jsFilePath);
            }
        }
    }

    public static function retrieveArrayActiveLangages()
    {
        $arrayLangues = [];
        $langues = self::retrieveActiveLangages();
        if (is_array($langues)) {
            foreach ($langues as $langue) {
                $arrayLangues[] = $langue->getLangue();
            }
        }

        return $arrayLangues;
    }

    public function getAllActiveLanguagesButDefaultOne()
    {
        $resultat = [];
        $languages = self::retrieveActiveLangages();
        if (is_array($languages)) {
            foreach ($languages as $langue) {
                if (Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE') != $langue->getLangue()) {
                    $resultat[] = $langue;
                }
            }
        }

        return $resultat;
    }

    /****
     * retourne l'abreviation de la langue disponible par rapport a la langue passee en parametre
     *
     * @param l'acronyme de la langue
     */
    public function getAvailableLanguageAbbreviation($lang)
    {
        if (0 == strcmp($lang, Atexo_Config::getParameter('ABREVIATION_LANGUE_AR'))) {
            return self::getLanguageAbbreviation(Atexo_Config::getParameter('ABREVIATION_LANGUE_AR'));
        } else {
            return '';
        }
    }

    /****
     * retourne l'abreviation de la langue avec premiere lettre majuscule
     *
     * @param l'acronyme de la langue
     */
    public static function getLanguageAbbreviation($lang)
    {
        if ($lang) {
            return ucfirst($lang);
        } else {
            return '';
        }
    }

    public function getAllActiveLanguagesButCurrentOne()
    {
        $resultat = [];
        $languages = self::retrieveActiveLangages();
        if (is_array($languages)) {
            foreach ($languages as $langue) {
                if (strtolower(Atexo_CurrentUser::readFromSession('lang')) != $langue->getLangue() && strtolower(
                    $_GET['lang']
                ) != $langue->getLangue()
                ) {
                    $resultat[] = $langue;
                }
            }
        }

        return $resultat;
    }

    public function getLangueByAbreviation($abreviation)
    {
        $langues = self::retrieveActiveLangages();
        if (is_array($langues)) {
            foreach ($langues as $langue) {
                if ($langue->getLangue() == $abreviation) {
                    return $langue;
                }
            }
        }
    }

    public function getLanguesObligatoiresPourPublicationConsultation()
    {
        $arrayLangues = [];
        $langues = self::retrieveActiveLangages();
        if (is_array($langues)) {
            foreach ($langues as $langue) {
                if ('1' == $langue->getObligatoirePourPublicationConsultation()) {
                    $arrayLangues[] = $langue;
                }
            }
        }

        return $arrayLangues;
    }

    public function estObligatoire($langue)
    {
        $languesActives = self::retrieveActiveLangages();
        if (is_array($languesActives)) {
            foreach ($languesActives as $langueActive) {
                if (strtolower($langue) == strtolower(
                    $langueActive->getLangue()
                ) && '1' == $langueActive->getObligatoirePourPublicationConsultation()
                ) {
                    return true;
                }
            }
        }

        return false;
    }
}
